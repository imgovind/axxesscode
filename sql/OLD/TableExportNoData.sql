-- --------------------------------------------------------
-- Host:                         10.0.1.61
-- Server version:               5.1.52 - MySQL Community Server (GPL) by Remi
-- Server OS:                    redhat-linux-gnu
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for accounting
CREATE DATABASE IF NOT EXISTS `accounting` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `accounting`;


-- Dumping structure for table accounting.agency
CREATE TABLE IF NOT EXISTS `agency` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Account` varchar(8) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `BranchId` varchar(20) DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `Account` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table accounting.information
CREATE TABLE IF NOT EXISTS `information` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Account` varchar(8) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `City` varchar(100) NOT NULL,
  `State` varchar(2) DEFAULT NULL,
  `Zip` int(11) DEFAULT NULL,
  `ImplementationFee` varchar(100) DEFAULT NULL,
  `PaymentSource` varchar(100) DEFAULT NULL,
  `InitialSignDate` date DEFAULT NULL,
  `SpecialNotes` text,
  `Status` tinyint(4) NOT NULL DEFAULT '1',
  `SalesPerson` char(100) DEFAULT NULL,
  `StartUp` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Account` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table accounting.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Account` varchar(8) DEFAULT NULL,
  `Sequence` int(11) DEFAULT NULL,
  `Amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Start` date NOT NULL DEFAULT '1000-01-01',
  `End` date DEFAULT '9999-12-31',
  `Package` int(10) unsigned DEFAULT '5',
  PRIMARY KEY (`id`),
  KEY `Account` (`Account`),
  KEY `Start` (`Start`),
  KEY `End` (`End`),
  KEY `IX_Payment_Dates` (`Start`,`End`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for agencymanagement
CREATE DATABASE IF NOT EXISTS `agencymanagement` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `agencymanagement`;


-- Dumping structure for table agencymanagement.agencies
CREATE TABLE IF NOT EXISTS `agencies` (
  `Id` char(36) NOT NULL,
  `SalesPerson` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Trainer` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `BackupTrainer` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `ImplementationSpecialist` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Name` varchar(100) NOT NULL,
  `TaxId` char(10) NOT NULL,
  `TaxIdType` varchar(50) DEFAULT NULL,
  `NationalProviderNumber` char(10) DEFAULT NULL,
  `MedicareProviderNumber` char(9) DEFAULT NULL,
  `MedicaidProviderNumber` char(9) DEFAULT NULL,
  `HomeHealthAgencyId` varchar(20) DEFAULT NULL,
  `SubmitterId` varchar(20) DEFAULT NULL,
  `SubmitterName` varchar(50) DEFAULT NULL,
  `SubmitterPhone` char(10) DEFAULT NULL,
  `SubmitterFax` char(10) DEFAULT NULL,
  `IsAgreementSigned` tinyint(1) unsigned DEFAULT '1',
  `IsSuspended` tinyint(1) unsigned DEFAULT '0',
  `TrialPeriod` int(10) unsigned DEFAULT '30',
  `Package` int(10) unsigned DEFAULT '5',
  `AnnualPlanId` int(10) unsigned DEFAULT '1',
  `ContactPersonFirstName` varchar(100) NOT NULL,
  `ContactPersonLastName` varchar(100) NOT NULL,
  `ContactPersonEmail` varchar(100) DEFAULT NULL,
  `ContactPersonPhone` char(10) NOT NULL,
  `Payor` varchar(100) DEFAULT NULL,
  `Comments` varchar(150) DEFAULT NULL,
  `CustomFields` varchar(250) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CahpsVendor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `CahpsVendorId` varchar(50) DEFAULT NULL,
  `CahpsSurveyDesignator` varchar(50) DEFAULT NULL,
  `IsAxxessTheBiller` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OasisAuditVendor` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `OasisAuditVendorApiKey` varchar(250) DEFAULT NULL,
  `FrozenDate` date NOT NULL DEFAULT '0001-01-01',
  `IsFrozen` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OasisAuditApiKey` varchar(250) NOT NULL,
  `PreviousSoftware` varchar(200) DEFAULT NULL,
  `Test` varchar(100) DEFAULT NULL,
  `DBServerIp` varchar(32) DEFAULT '10.0.1.61',
  `Services` int(10) unsigned NOT NULL DEFAULT '1',
  `AccountId` varchar(8) DEFAULT NULL,
  `MigrationStatus` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyadjustmentcodes
CREATE TABLE IF NOT EXISTS `agencyadjustmentcodes` (
  `Id` varchar(36) NOT NULL,
  `AgencyId` varchar(36) NOT NULL,
  `Code` varchar(100) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Adjustments_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Adjustments_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencychanges
CREATE TABLE IF NOT EXISTS `agencychanges` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `TaxId` char(10) DEFAULT NULL,
  `TaxIdType` varchar(50) DEFAULT NULL,
  `ContactPersonEmail` varchar(100) DEFAULT NULL,
  `ContactPersonPhone` char(10) DEFAULT NULL,
  `CahpsVendor` tinyint(3) unsigned DEFAULT NULL,
  `NationalProviderNumber` char(10) DEFAULT NULL,
  `MedicareProviderNumber` char(9) DEFAULT NULL,
  `MedicaidProviderNumber` char(9) DEFAULT NULL,
  `HomeHealthAgencyId` varchar(20) DEFAULT NULL,
  `ContactPersonFirstName` varchar(100) DEFAULT NULL,
  `ContactPersonLastName` varchar(100) DEFAULT NULL,
  `LocationName` varchar(100) DEFAULT NULL,
  `LocationAddressLine1` varchar(100) DEFAULT NULL,
  `LocationAddressLine2` varchar(100) DEFAULT NULL,
  `LocationAddressCity` varchar(100) DEFAULT NULL,
  `LocationAddressStateCode` char(2) DEFAULT NULL,
  `LocationAddressZipCode` char(5) DEFAULT NULL,
  `LocationPhoneWork` char(10) DEFAULT NULL,
  `LocationFaxNumber` char(10) DEFAULT NULL,
  `FullName` varchar(50) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `IsComplete` tinyint(3) unsigned DEFAULT '0',
  `Comments` text,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyChanges_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_AgencyChanges_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencycontacts
CREATE TABLE IF NOT EXISTS `agencycontacts` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `FirstName` varchar(100) DEFAULT NULL,
  `LastName` varchar(100) DEFAULT NULL,
  `CompanyName` varchar(100) DEFAULT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` varchar(5) DEFAULT NULL,
  `PhonePrimary` char(10) DEFAULT NULL,
  `PhoneAlternate` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `ContactType` varchar(50) DEFAULT NULL,
  `ContactTypeOther` varchar(100) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Comments` text,
  `PhoneExtension` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Contacts_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Contacts_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyhospitals
CREATE TABLE IF NOT EXISTS `agencyhospitals` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `ContactPersonFirstName` varchar(100) DEFAULT NULL,
  `ContactPersonLastName` varchar(100) DEFAULT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) NOT NULL,
  `AddressStateCode` char(2) NOT NULL,
  `AddressZipCode` char(5) NOT NULL,
  `Phone` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `Comment` varchar(500) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Hospitals_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Hospitals_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyinsurances
CREATE TABLE IF NOT EXISTS `agencyinsurances` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `PayorType` int(1) NOT NULL,
  `InvoiceType` int(1) NOT NULL,
  `ChargeGrouping` int(1) NOT NULL,
  `ChargeType` int(1) NOT NULL,
  `ParentInsurance` int(1) NOT NULL,
  `IsInsuranceRateUsed` tinyint(1) unsigned DEFAULT '0',
  `Name` varchar(100) NOT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `OtherProviderId` varchar(40) DEFAULT NULL,
  `ProviderId` varchar(40) DEFAULT NULL,
  `HealthPlanId` varchar(40) DEFAULT NULL,
  `ProviderSubscriberId` varchar(40) DEFAULT NULL,
  `Ub04Locator81cca` text,
  `PrintContentOnly` tinyint(1) DEFAULT '0',
  `HCFALocators` text,
  `Charge` text,
  `ClearingHouse` varchar(100) NOT NULL,
  `InterchangeReceiverId` int(1) NOT NULL,
  `PayorId` varchar(20) NOT NULL,
  `ContactPersonFirstName` varchar(100) DEFAULT NULL,
  `ContactPersonLastName` varchar(100) DEFAULT NULL,
  `ContactEmailAddress` varchar(100) DEFAULT NULL,
  `SubmitterId` varchar(20) DEFAULT NULL,
  `PhoneNumber` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `CurrentBalance` decimal(10,0) DEFAULT NULL,
  `WorkWeekStartDay` int(1) NOT NULL,
  `IsVisitAuthorizationRequired` tinyint(1) NOT NULL,
  `DefaultFiscalIntermediary` tinyint(1) NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsAxxessTheBiller` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SubmitterName` varchar(100) DEFAULT NULL,
  `SubmitterPhone` char(10) DEFAULT NULL,
  `ClearingHouseSubmitterId` varchar(10) DEFAULT NULL,
  `BillType` varchar(20) DEFAULT NULL,
  `BillData` mediumtext,
  `IsOASISMatchKeyAuth` tinyint(1) DEFAULT '0',
  `HasContractWithAgency` tinyint(1) unsigned DEFAULT '0',
  `IsUsingPatientRates` tinyint(1) unsigned DEFAULT '0',
  `RequireServiceLocation` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Insurance_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Insurance_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencylocations
CREATE TABLE IF NOT EXISTS `agencylocations` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `FullName` varchar(100) DEFAULT NULL,
  `CustomId` varchar(15) DEFAULT NULL,
  `CBSA` char(5) DEFAULT NULL,
  `MedicareProviderNumber` char(10) DEFAULT NULL,
  `AddressLine1` varchar(100) NOT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) NOT NULL,
  `AddressStateCode` char(2) NOT NULL,
  `AddressZipCode` char(5) NOT NULL,
  `AddressZipCodeFour` char(4) DEFAULT NULL,
  `ZipNineStatus` char(1) DEFAULT 'P',
  `PhoneWork` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `Comments` varchar(100) DEFAULT NULL,
  `IsMainOffice` tinyint(1) unsigned NOT NULL,
  `Cost` text,
  `BillData` mediumtext,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSubmitterInfoTheSame` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `SubmitterId` varchar(50) DEFAULT '',
  `SubmitterName` varchar(50) DEFAULT '',
  `SubmitterPhone` varchar(10) DEFAULT '',
  `SubmitterFax` varchar(10) DEFAULT '',
  `Payor` varchar(5) DEFAULT '',
  `BranchId` varchar(20) DEFAULT 'N',
  `BranchIdOther` varchar(20) DEFAULT 'N',
  `Ub04Locator81cca` text,
  `TaxId` char(10) DEFAULT NULL,
  `TaxIdType` varchar(50) DEFAULT NULL,
  `NationalProviderNumber` char(10) DEFAULT NULL,
  `MedicaidProviderNumber` char(9) DEFAULT NULL,
  `HomeHealthAgencyId` varchar(20) DEFAULT NULL,
  `ContactPersonFirstName` varchar(100) DEFAULT NULL,
  `ContactPersonLastName` varchar(100) DEFAULT NULL,
  `ContactPersonEmail` varchar(100) DEFAULT NULL,
  `ContactPersonPhone` varchar(100) DEFAULT NULL,
  `CahpsVendor` tinyint(3) DEFAULT NULL,
  `CahpsVendorId` varchar(50) DEFAULT NULL,
  `CahpsSurveyDesignator` varchar(50) DEFAULT NULL,
  `IsAxxessTheBiller` tinyint(1) DEFAULT NULL,
  `OasisAuditVendor` tinyint(3) DEFAULT NULL,
  `IsLocationStandAlone` tinyint(1) DEFAULT '0',
  `Modified` date NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Location_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Location_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencymedicareinsurances
CREATE TABLE IF NOT EXISTS `agencymedicareinsurances` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `MedicareId` int(10) NOT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(9) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyMedicareInsurances_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_AgencyMedicareInsurances_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencynonvisits
CREATE TABLE IF NOT EXISTS `agencynonvisits` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Text` text NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Nonvisit_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Nonvisit_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencypharmacies
CREATE TABLE IF NOT EXISTS `agencypharmacies` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `ContactPersonFirstName` varchar(100) DEFAULT NULL,
  `ContactPersonLastName` varchar(100) DEFAULT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `Phone` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `Comment` varchar(500) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Pharmacies_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Pharmacies_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyphysicians
CREATE TABLE IF NOT EXISTS `agencyphysicians` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `LoginId` char(36) DEFAULT NULL,
  `NPI` char(10) DEFAULT NULL,
  `UPIN` char(6) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `Credentials` varchar(50) DEFAULT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `PhoneWork` char(10) DEFAULT NULL,
  `PhoneAlternate` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `EmailAddress` varchar(50) DEFAULT NULL,
  `LicenseNumber` varchar(50) DEFAULT NULL,
  `LicenseStateCode` char(2) DEFAULT NULL,
  `Comments` varchar(100) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PhysicianAccess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Licenses` text,
  PRIMARY KEY (`Id`),
  KEY `FK_Physician_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_AgencyPhysicians_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyprofiles
CREATE TABLE IF NOT EXISTS `agencyprofiles` (
  `Id` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `AgencyId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AssetId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsSuspendPayment` tinyint(1) unsigned DEFAULT '0',
  `FrozenReason` text,
  `EffectiveDate` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyProfiles_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_AgencyProfiles_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencysubscriptionplans
CREATE TABLE IF NOT EXISTS `agencysubscriptionplans` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `AgencyLocationId` char(36) NOT NULL,
  `IsUserPlan` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `PlanLimit` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencySubscriptionPlans_AgencyId` (`AgencyId`),
  KEY `FK_AgencySubscriptionPlans_AgencyLocationId` (`AgencyLocationId`),
  CONSTRAINT `FK_AgencySubscriptionPlans_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_AgencySubscriptionPlans_AgencyLocationId` FOREIGN KEY (`AgencyLocationId`) REFERENCES `agencylocations` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencysupplies
CREATE TABLE IF NOT EXISTS `agencysupplies` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Code` char(5) DEFAULT NULL,
  `Description` varchar(350) NOT NULL,
  `UnitCost` decimal(8,2) unsigned DEFAULT '0.00',
  `RevenueCode` char(5) DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Templates_AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencysystemmessages
CREATE TABLE IF NOT EXISTS `agencysystemmessages` (
  `SystemMessageId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  PRIMARY KEY (`SystemMessageId`,`AgencyId`),
  CONSTRAINT `FK_agencysystemmessages_systemmessages` FOREIGN KEY (`SystemMessageId`) REFERENCES `systemmessages` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyteams
CREATE TABLE IF NOT EXISTS `agencyteams` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Name` varchar(80) NOT NULL,
  `Users` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencytemplates
CREATE TABLE IF NOT EXISTS `agencytemplates` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Text` text NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Templates_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Templates_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyupgrades
CREATE TABLE IF NOT EXISTS `agencyupgrades` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `AgencyLocationId` char(36) DEFAULT NULL,
  `RequestedById` char(36) NOT NULL,
  `AccountId` char(8) DEFAULT '0',
  `PreviousAmount` double DEFAULT '-1',
  `Amount` double DEFAULT '-1',
  `PreviousPackageId` int(10) unsigned NOT NULL DEFAULT '0',
  `RequestedPackageId` int(10) unsigned NOT NULL DEFAULT '0',
  `AnnualPlanId` int(10) unsigned NOT NULL DEFAULT '0',
  `IsComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsUserPlan` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Type` char(25) DEFAULT NULL,
  `Comments` tinytext,
  `EffectiveDate` date DEFAULT '0001-01-01',
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyUpgrades_AgencyId` (`AgencyId`),
  KEY `FK_AgencyUpgrades_RequestedById` (`RequestedById`),
  CONSTRAINT `FK_AgencyUpgrades_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_AgencyUpgrades_RequestedById` FOREIGN KEY (`RequestedById`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.allergyprofiles
CREATE TABLE IF NOT EXISTS `allergyprofiles` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Allergies` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AllergyProfiles_PatientId` (`PatientId`),
  KEY `FK_AllergyProfiles_AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.assessments
CREATE TABLE IF NOT EXISTS `assessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) NOT NULL,
  `IsValidated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.assets
CREATE TABLE IF NOT EXISTS `assets` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `FileName` varchar(50) NOT NULL,
  `FileSize` varchar(20) NOT NULL,
  `ContentType` varchar(20) NOT NULL,
  `Bytes` mediumblob NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Asset_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Asset_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.authorizations
CREATE TABLE IF NOT EXISTS `authorizations` (
  `Id` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `AssetId` char(36) DEFAULT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `AgencyLocationId` char(36) NOT NULL,
  `Number1` varchar(30) DEFAULT NULL,
  `Number2` varchar(30) DEFAULT NULL,
  `Number3` varchar(30) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Insurance` varchar(50) DEFAULT NULL,
  `SNVisit` varchar(20) DEFAULT NULL,
  `SNVisitCountType` varchar(20) DEFAULT NULL,
  `PTVisit` varchar(20) DEFAULT NULL,
  `PTVisitCountType` varchar(20) DEFAULT NULL,
  `OTVisit` varchar(20) DEFAULT NULL,
  `OTVisitCountType` varchar(20) DEFAULT NULL,
  `STVisit` varchar(20) DEFAULT NULL,
  `STVisitCountType` varchar(20) DEFAULT NULL,
  `MSWVisit` varchar(20) DEFAULT NULL,
  `MSWVisitCountType` varchar(20) DEFAULT NULL,
  `HHAVisit` varchar(20) DEFAULT NULL,
  `HHAVisitCountType` varchar(20) DEFAULT NULL,
  `DieticianVisit` varchar(20) DEFAULT NULL,
  `DieticianVisitCountType` varchar(20) DEFAULT NULL,
  `RNVisit` varchar(20) DEFAULT NULL,
  `RNVisitCountType` varchar(20) DEFAULT NULL,
  `LVNVisit` varchar(20) DEFAULT NULL,
  `LVNVisitCountType` varchar(20) DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `Comments` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Service` tinyint(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `FK_Authorizations_UserId` (`UserId`),
  KEY `FK_Authorizations_AgencyId` (`AgencyId`),
  KEY `FK_Authorizations_PatientId` (`PatientId`),
  CONSTRAINT `FK_Authorizations_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Authorizations_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Authorizations_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.axxesssubmitterinfos
CREATE TABLE IF NOT EXISTS `axxesssubmitterinfos` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `SubmitterId` varchar(50) DEFAULT NULL,
  `SubmitterName` varchar(50) DEFAULT NULL,
  `Phone` varchar(10) DEFAULT NULL,
  `Fax` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.careplanoversights
CREATE TABLE IF NOT EXISTS `careplanoversights` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `PhysicianLoginId` char(36) NOT NULL,
  `CptCode` text,
  `Duration` int(11) DEFAULT NULL,
  `Activity` tinyint(4) DEFAULT NULL,
  `Comments` mediumtext,
  `LogDate` date DEFAULT NULL,
  `Status` int(10) DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Modified` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.claimdatas
CREATE TABLE IF NOT EXISTS `claimdatas` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `Data` text,
  `BillIdentifers` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `ClaimType` enum('MAN','CMS','HMO') DEFAULT 'CMS',
  `Response` mediumtext,
  `IsDeprecated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.claimnumbers
CREATE TABLE IF NOT EXISTS `claimnumbers` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EntityId` char(36) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `EntityId_Unique` (`EntityId`),
  KEY `EntityId` (`EntityId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This stores claim numbers for managed claims. ';

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.communicationnotes
CREATE TABLE IF NOT EXISTS `communicationnotes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `Text` text,
  `Recipients` text,
  `Created` date NOT NULL DEFAULT '0001-01-01',
  `Modified` date NOT NULL DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(1) DEFAULT '0',
  `Status` int(4) DEFAULT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianData` text,
  PRIMARY KEY (`Id`),
  KEY `FK_Commnotes_PatientId` (`PatientId`),
  KEY `FK_Commnotes_AgencyId` (`AgencyId`),
  KEY `FK_Commnotes_UserId` (`UserId`),
  CONSTRAINT `FK_Commnotes_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Commnotes_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Commnotes_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.customernotes
CREATE TABLE IF NOT EXISTS `customernotes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `Comments` text NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Recipients` text,
  `NoteType` tinyint(3) unsigned NOT NULL,
  `Summary` varchar(80) NOT NULL,
  `CallerId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IssueId` int(10) unsigned DEFAULT NULL,
  `AttachmentId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsCompleted` tinyint(1) unsigned NOT NULL,
  `DevUpdates` text,
  `AdditionalInformation` text,
  `ModifiedBy` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `Status` tinyint(100) unsigned NOT NULL DEFAULT '0',
  `Saved` tinyint(4) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `LoginId` (`LoginId`),
  CONSTRAINT `FK_CustomerNotes_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.dashboardmessages
CREATE TABLE IF NOT EXISTS `dashboardmessages` (
  `Id` char(36) NOT NULL,
  `Title` varchar(150) NOT NULL,
  `Text` text NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.deleteditems
CREATE TABLE IF NOT EXISTS `deleteditems` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `Schedule` mediumtext NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PatientEpisode_PatientId` (`PatientId`),
  KEY `FK_PatientEpisode_AgencyId` (`AgencyId`),
  KEY `EpisodeId` (`EpisodeId`),
  CONSTRAINT `FK_DeletedItems_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DeletedItems_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DeletedItems_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.facetofaceencounters
CREATE TABLE IF NOT EXISTS `facetofaceencounters` (
  `Id` char(36) NOT NULL DEFAULT '',
  `OrderNumber` int(20) unsigned NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `Status` int(36) DEFAULT NULL,
  `Certification` varchar(36) DEFAULT NULL,
  `EncounterDate` date DEFAULT '0001-01-01',
  `ReceivedDate` date DEFAULT '0001-01-01',
  `RequestDate` date DEFAULT '0001-01-01',
  `SentDate` date DEFAULT '0001-01-01',
  `ClinicalFinding` text,
  `Services` varchar(50) DEFAULT NULL,
  `ServicesOther` varchar(50) DEFAULT NULL,
  `SignatureText` varchar(50) DEFAULT NULL,
  `SignatureDate` date DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(4) DEFAULT '0',
  `Modified` date DEFAULT '0001-01-01',
  `Created` date DEFAULT '0001-01-01',
  `MedicalReason` text,
  `PhysicianData` text,
  PRIMARY KEY (`Id`),
  KEY `PhysicianId` (`PhysicianId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.finals
CREATE TABLE IF NOT EXISTS `finals` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `IsOasisComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FirstBillableVisitDate` date DEFAULT NULL,
  `Remark` text,
  `MedicareNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `AreOrdersComplete` tinyint(1) unsigned DEFAULT '0',
  `AreVisitsComplete` tinyint(1) unsigned DEFAULT '0',
  `IsGenerated` tinyint(1) unsigned DEFAULT '0',
  `Modified` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `VerifiedVisits` mediumtext,
  `Supply` mediumtext,
  `SupplyTotal` double(10,0) DEFAULT NULL,
  `Payment` double NOT NULL DEFAULT '0',
  `PrimaryInsuranceId` int(4) unsigned NOT NULL,
  `IsSupplyVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFinalInfoVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVisitVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsRapGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Status` int(4) unsigned NOT NULL DEFAULT '0',
  `Type` int(4) unsigned DEFAULT '0',
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) unsigned DEFAULT '0',
  `ClaimDate` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `ProspectivePay` double unsigned NOT NULL DEFAULT '0',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `DischargeDate` date DEFAULT '0001-01-01',
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `GroupName` varchar(50) DEFAULT NULL,
  `GroupId` varchar(50) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `Authorization` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `AuthorizationNumber2` varchar(50) DEFAULT NULL,
  `AuthorizationNumber3` varchar(50) DEFAULT NULL,
  `ConditionCodes` text NOT NULL,
  `UB4PatientStatus` varchar(5) DEFAULT NULL,
  `CBSA` varchar(6) DEFAULT NULL,
  `Ub04Locator81cca` text,
  `Ub04Locator39` text,
  `Insurance` longtext,
  `Remittance` text,
  `IsSupplyNotBillable` tinyint(1) DEFAULT '0',
  `UseStandardSupplies` tinyint(1) DEFAULT '1',
  `Ub04Locator31` text,
  `Ub04Locator32` text,
  `Ub04Locator33` text,
  `Ub04Locator34` text,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `AgencyId_Status` (`AgencyId`,`Status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.finalsnapshots
CREATE TABLE IF NOT EXISTS `finalsnapshots` (
  `MainId` char(46) NOT NULL,
  `Id` char(36) NOT NULL,
  `BatchId` bigint(20) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `IsOasisComplete` tinyint(1) unsigned NOT NULL,
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL,
  `FirstBillableVisitDate` date DEFAULT NULL,
  `Remark` text,
  `MedicareNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `AreOrdersComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AreVisitsComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `VerifiedVisits` text,
  `Supply` text,
  `SupplyTotal` double DEFAULT NULL,
  `Payment` double unsigned DEFAULT '0',
  `PrimaryInsuranceId` tinyint(4) unsigned NOT NULL,
  `IsSupplyVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFinalInfoVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVisitVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsRapGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Status` int(4) unsigned NOT NULL DEFAULT '0',
  `Type` int(4) unsigned DEFAULT '0',
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) DEFAULT '0',
  `ClaimDate` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `ProspectivePay` double unsigned DEFAULT '0',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `DischargeDate` date DEFAULT '0001-01-01',
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `Reason` text,
  PRIMARY KEY (`MainId`),
  KEY `IX_FinalSnapShots_AgencyId_Id` (`AgencyId`,`Id`),
  CONSTRAINT `FK_FinalSnapshots_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.hospitalizationlogs
CREATE TABLE IF NOT EXISTS `hospitalizationlogs` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) NOT NULL,
  `SourceId` tinyint(3) unsigned NOT NULL,
  `Data` text NOT NULL,
  `HospitalizationDate` date NOT NULL DEFAULT '0001-01-01',
  `LastHomeVisitDate` date NOT NULL DEFAULT '0001-01-01',
  `ReturnDate` date NOT NULL DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_PatientTransferLogs_AgencyId` (`AgencyId`),
  KEY `FK_PatientTransferLogs_PatientId` (`PatientId`),
  KEY `FK_PatientTransferLogs_UserId` (`UserId`),
  CONSTRAINT `FK_PatientTransferLogs_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientTransferLogs_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientTransferLogs_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.incidents
CREATE TABLE IF NOT EXISTS `incidents` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `IndividualInvolved` varchar(150) DEFAULT NULL,
  `IndividualInvolvedOther` varchar(100) DEFAULT NULL,
  `IncidentDate` date NOT NULL,
  `IncidentType` varchar(100) DEFAULT NULL,
  `Description` text,
  `ActionTaken` text,
  `MDNotified` varchar(20) DEFAULT NULL,
  `FamilyNotified` varchar(20) DEFAULT NULL,
  `NewOrdersCreated` varchar(20) DEFAULT NULL,
  `Orders` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Status` int(10) unsigned NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FollowUp` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.infections
CREATE TABLE IF NOT EXISTS `infections` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `InfectionDate` date NOT NULL,
  `InfectionType` varchar(150) DEFAULT NULL,
  `InfectionTypeOther` varchar(50) DEFAULT NULL,
  `Treatment` text,
  `Orders` text,
  `MDNotified` varchar(20) DEFAULT NULL,
  `TreatmentPrescribed` varchar(20) DEFAULT NULL,
  `NewOrdersCreated` varchar(20) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Status` int(10) unsigned NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FollowUp` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.licenseitems
CREATE TABLE IF NOT EXISTS `licenseitems` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `AssetId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `LastName` varchar(100) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LicenseType` varchar(150) NOT NULL,
  `IssueDate` date NOT NULL,
  `ExpireDate` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  KEY `FK_LicenseItems_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_LicenseItems_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.managedclaimadjustments
CREATE TABLE IF NOT EXISTS `managedclaimadjustments` (
  `Id` varchar(36) NOT NULL,
  `AgencyId` varchar(36) NOT NULL,
  `ClaimId` varchar(36) NOT NULL,
  `PatientId` varchar(36) NOT NULL,
  `Adjustment` double NOT NULL DEFAULT '0',
  `TypeId` varchar(36) NOT NULL,
  `Comments` text NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ManagedClaimAdjustments_AgencyId` (`AgencyId`),
  KEY `FK_ManagedClaimAdjustments_PatientId` (`PatientId`),
  KEY `FK_ManagedClaimAdjustments_ClaimId` (`ClaimId`),
  CONSTRAINT `FK_ManagedClaimAdjustments_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ManagedClaimAdjustments_ClaimId` FOREIGN KEY (`ClaimId`) REFERENCES `managedclaims` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ManagedClaimAdjustments_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.managedclaimpayments
CREATE TABLE IF NOT EXISTS `managedclaimpayments` (
  `Id` varchar(36) NOT NULL,
  `AgencyId` varchar(36) NOT NULL,
  `ClaimId` varchar(36) NOT NULL,
  `PatientId` varchar(36) NOT NULL,
  `Payment` double NOT NULL DEFAULT '0',
  `PaymentDate` date NOT NULL DEFAULT '0001-01-01',
  `Comments` text,
  `Payor` int(11) NOT NULL DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ManagedClaimPayments_ClaimId` (`ClaimId`),
  KEY `FK_ManagedClaimPayments_PatientId` (`PatientId`),
  KEY `FK_ManagedClaimPayments_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_ManagedClaimPayments_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ManagedClaimPayments_ClaimId` FOREIGN KEY (`ClaimId`) REFERENCES `managedclaims` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_ManagedClaimPayments_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.managedclaims
CREATE TABLE IF NOT EXISTS `managedclaims` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `IsOasisComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FirstBillableVisitDate` date DEFAULT NULL,
  `Remark` text,
  `IsuranceIdNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `AreOrdersComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AreVisitsComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `VerifiedVisits` mediumtext,
  `Supply` text,
  `SupplyTotal` double DEFAULT NULL,
  `Payment` double unsigned DEFAULT '0',
  `PrimaryInsuranceId` int(4) unsigned NOT NULL,
  `IsSupplyVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsInfoVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVisitVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsInsuranceVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsRapGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Status` int(4) unsigned NOT NULL DEFAULT '0',
  `Type` varchar(10) DEFAULT '',
  `DischargeDate` date DEFAULT '0001-01-01',
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) DEFAULT '0',
  `ClaimDate` date NOT NULL DEFAULT '0001-01-01',
  `ProspectivePay` double unsigned DEFAULT '0',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `GroupName` varchar(50) DEFAULT NULL,
  `GroupId` varchar(50) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `Authorization` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `AuthorizationNumber2` varchar(50) DEFAULT NULL,
  `AuthorizationNumber3` varchar(50) DEFAULT NULL,
  `ConditionCodes` text,
  `SupplyCode` varchar(10) DEFAULT '0272',
  `UB4PatientStatus` varchar(5) DEFAULT NULL,
  `CBSA` varchar(6) DEFAULT NULL,
  `Ub04Locator81cca` text,
  `Ub04Locator39` text,
  `Insurance` mediumtext,
  `IsHomeHealthServiceIncluded` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `Ub04Locator31` text,
  `Ub04Locator32` text,
  `Ub04Locator33` text,
  `Ub04Locator34` text,
  `HCFALocators` text,
  PRIMARY KEY (`Id`),
  KEY `FK_Finals_AgencyId` (`AgencyId`),
  KEY `FK_Finals_PatientId` (`PatientId`),
  KEY `FK_Finals_EpisodeId` (`EpisodeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.medicareeligibilities
CREATE TABLE IF NOT EXISTS `medicareeligibilities` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `Result` mediumtext NOT NULL,
  `Status` int(3) unsigned NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsHiqh` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_MedicareEligibilities_AgencyId` (`AgencyId`),
  KEY `FK_MedicareEligibilities_PatientId` (`PatientId`),
  CONSTRAINT `FK_MedicareEligibilities_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MedicareEligibilities_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.medicareeligibilitysummaries
CREATE TABLE IF NOT EXISTS `medicareeligibilitysummaries` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Report` mediumtext,
  `Created` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_MedicareEligibilitySummaries_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_MedicareEligibilitySummaries_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.medicationprofilehistories
CREATE TABLE IF NOT EXISTS `medicationprofilehistories` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `Medication` text,
  `Allergies` varchar(150) DEFAULT NULL,
  `PrimaryDiagnosis` varchar(200) DEFAULT NULL,
  `SecondaryDiagnosis` varchar(200) DEFAULT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignedDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PharmacyPhone` char(10) DEFAULT NULL,
  `PharmacyFax` char(10) DEFAULT NULL,
  `PharmacyName` varchar(50) DEFAULT NULL,
  `SecondaryPharmacyPhone` char(10) DEFAULT NULL,
  `SecondaryPharmacyFax` char(10) DEFAULT NULL,
  `SecondaryPharmacyName` varchar(50) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Modified` date DEFAULT NULL,
  `PhysicianData` text,
  `AssociatedAssessment` char(36) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_MedicationProfiles_AgencyId` (`AgencyId`),
  KEY `FK_MedicationProfiles_PatientId` (`PatientId`),
  KEY `FK_MedicationProfiles_EpisodeId` (`EpisodeId`),
  KEY `FK_MedicationProfiles_UserId` (`UserId`),
  CONSTRAINT `FK_MedicationProfiles_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MedicationProfiles_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MedicationProfiles_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MedicationProfiles_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.medicationprofiles
CREATE TABLE IF NOT EXISTS `medicationprofiles` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Medication` text,
  `PharmacyName` varchar(50) DEFAULT NULL,
  `PharmacyPhone` char(10) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_MedicationProfileHistories_PatientId` (`PatientId`),
  KEY `FK_MedicationProfileHistories_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_MedicationProfileHistories_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MedicationProfileHistories_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.messagedetails
CREATE TABLE IF NOT EXISTS `messagedetails` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `FromId` char(36) NOT NULL,
  `FromName` varchar(100) NOT NULL,
  `AttachmentId` char(36) DEFAULT NULL,
  `RecipientNames` varchar(500) DEFAULT NULL,
  `CarbonCopyNames` varchar(500) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `Subject` varchar(100) NOT NULL,
  `Body` text NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `SenderId` (`FromId`),
  KEY `FK_Messages_PatientId` (`PatientId`),
  KEY `FK_MessageDetails_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_MessageDetails_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.messagefolders
CREATE TABLE IF NOT EXISTS `messagefolders` (
  `Id` char(36) NOT NULL,
  `ParentId` char(36) DEFAULT NULL,
  `OwnerId` char(36) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `Name` varchar(50) NOT NULL DEFAULT 'Inbox',
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ParentId` (`ParentId`),
  KEY `FK_MessageFolder_UserId` (`OwnerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `FromId` char(36) NOT NULL,
  `FromName` varchar(100) NOT NULL,
  `RecipientId` char(36) NOT NULL,
  `AttachmentId` char(36) DEFAULT NULL,
  `RecipientNames` varchar(500) DEFAULT NULL,
  `CarbonCopyNames` varchar(500) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `Subject` varchar(100) NOT NULL,
  `Body` text NOT NULL,
  `MarkAsRead` tinyint(1) unsigned NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `FolderId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `SenderId` (`FromId`),
  KEY `FK_Messages_RecipientId` (`RecipientId`),
  KEY `FK_Messages_PatientId` (`PatientId`),
  KEY `FK_Messages_AgencyId` (`AgencyId`),
  KEY `FolderId` (`FolderId`),
  CONSTRAINT `FK_Messages_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`),
  CONSTRAINT `FK_Messages_RecipientId` FOREIGN KEY (`RecipientId`) REFERENCES `users` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Messages_UserId` FOREIGN KEY (`FromId`) REFERENCES `users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.missedvisits
CREATE TABLE IF NOT EXISTS `missedvisits` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserSignatureAssetId` char(36) DEFAULT NULL,
  `Date` date NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '400',
  `Reason` varchar(75) DEFAULT NULL,
  `Comments` text,
  `IsOrderGenerated` tinyint(1) unsigned NOT NULL,
  `IsPhysicianOfficeNotified` tinyint(1) unsigned NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_MissedVisits_PatientId` (`PatientId`),
  KEY `FK_MissedVisits_EpisodeId` (`EpisodeId`),
  CONSTRAINT `FK_MissedVisits_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MissedVisits_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.noterelations
CREATE TABLE IF NOT EXISTS `noterelations` (
  `Id` char(36) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `ParentId` char(36) DEFAULT NULL,
  `Type` char(36) DEFAULT NULL,
  `IsDeprecated` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.ordernumbers
CREATE TABLE IF NOT EXISTS `ordernumbers` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientadmissiondates
CREATE TABLE IF NOT EXISTS `patientadmissiondates` (
  `Id` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `AgencyId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `PatientId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `UserId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AgencyLocationId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `MedicareNumber` char(11) DEFAULT NULL,
  `MedicaidNumber` char(20) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleInitial` varchar(1) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT '1',
  `SSN` char(9) DEFAULT NULL,
  `DOB` date NOT NULL DEFAULT '0001-01-01',
  `Gender` varchar(20) DEFAULT NULL,
  `Height` float DEFAULT NULL,
  `HeightMetric` tinyint(4) DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `WeightMetric` tinyint(4) DEFAULT NULL,
  `Ethnicities` varchar(50) NOT NULL,
  `MaritalStatus` varchar(20) DEFAULT NULL,
  `AddressLine1` varchar(100) NOT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) NOT NULL,
  `AddressCounty` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) NOT NULL,
  `AddressZipCode` char(9) NOT NULL,
  `CBSA` char(5) DEFAULT NULL,
  `EvacuationAddressLine1` varchar(100) DEFAULT NULL,
  `EvacuationAddressLine2` varchar(100) DEFAULT NULL,
  `EvacuationAddressCity` varchar(100) DEFAULT NULL,
  `EvacuationAddressStateCode` char(2) DEFAULT NULL,
  `EvacuationAddressZipCode` char(5) DEFAULT NULL,
  `PhoneHome` char(10) DEFAULT NULL,
  `PhoneMobile` char(10) DEFAULT NULL,
  `EvacuationPhoneHome` char(10) DEFAULT NULL,
  `EvacuationPhoneMobile` char(10) DEFAULT NULL,
  `EmailAddress` varchar(50) DEFAULT NULL,
  `StartofCareDate` date NOT NULL DEFAULT '0001-01-01',
  `PrimaryPharmacyId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `SecondaryPharmacyId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `PharmacyName` varchar(100) DEFAULT NULL,
  `PharmacyPhone` char(10) DEFAULT NULL,
  `PrimaryInsurance` int(11) DEFAULT '-1',
  `PrimaryHealthPlanId` varchar(50) DEFAULT NULL,
  `PrimaryGroupName` varchar(50) DEFAULT NULL,
  `PrimaryGroupId` varchar(50) DEFAULT NULL,
  `PrimaryRelationship` varchar(10) DEFAULT '2',
  `SecondaryInsurance` int(11) DEFAULT '-1',
  `SecondaryHealthPlanId` varchar(50) DEFAULT NULL,
  `SecondaryGroupName` varchar(50) DEFAULT NULL,
  `SecondaryRelationship` varchar(10) DEFAULT '2',
  `TertiaryGroupId` varchar(50) DEFAULT NULL,
  `SecondaryGroupId` varchar(50) DEFAULT NULL,
  `TertiaryInsurance` int(11) DEFAULT '-1',
  `TertiaryHealthPlanId` varchar(50) DEFAULT NULL,
  `TertiaryGroupName` varchar(50) DEFAULT NULL,
  `TertiaryRelationship` varchar(10) DEFAULT '2',
  `DME` varchar(100) DEFAULT NULL,
  `OtherDME` varchar(100) DEFAULT NULL,
  `Payer` varchar(20) DEFAULT NULL,
  `ServicesRequired` varchar(100) DEFAULT NULL,
  `PaymentSource` varchar(50) DEFAULT NULL,
  `OtherPaymentSource` varchar(50) DEFAULT NULL,
  `AdmissionSource` varchar(50) DEFAULT NULL,
  `ReferrerPhysician` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `ReferralDate` date DEFAULT '0001-01-01',
  `OtherReferralSource` varchar(30) DEFAULT NULL,
  `ReferralName` varchar(50) DEFAULT NULL,
  `ReferralContact` varchar(50) DEFAULT NULL,
  `InternalReferral` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `CaseManagerId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AuditorId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Triage` tinyint(3) unsigned DEFAULT '1',
  `EvacuationZone` varchar(2) DEFAULT NULL,
  `PhotoId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `NonAdmissionDate` date DEFAULT '0001-01-01',
  `NonAdmissionReason` varchar(50) DEFAULT NULL,
  `Comments` text,
  `LastEligibilityCheck` date DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDNR` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsHospitalized` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HospitalizationId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `DischargeDate` date DEFAULT '0001-01-01',
  `DischargeReasonId` tinyint(2) NOT NULL DEFAULT '0',
  `DischargeReason` text,
  `AdmissionId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Services` int(100) unsigned NOT NULL DEFAULT '1',
  `HomeHealthStatus` int(10) unsigned NOT NULL DEFAULT '1',
  `PrivateDutyStatus` int(10) unsigned NOT NULL DEFAULT '1',
  `ServiceLocation` char(5) DEFAULT 'Q5001',
  `Longitude` float(10,7) NOT NULL,
  `Latitude` float(10,7) NOT NULL,
  `Created` date NOT NULL DEFAULT '0001-01-01',
  `Modified` date NOT NULL DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `FK_Patient_AgencyId` (`AgencyId`),
  KEY `FK_Patient_UserId` (`UserId`),
  KEY `FK_Patient_CaseManagerId` (`CaseManagerId`),
  KEY `FK_Patient_AgencyLocationId` (`AgencyLocationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientadmissiondates1
CREATE TABLE IF NOT EXISTS `patientadmissiondates1` (
  `Id` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `StartOfCareDate` date DEFAULT '0001-01-01',
  `DischargedDate` date DEFAULT '0001-01-01',
  `PatientData` text,
  `ProfileData` text,
  `Status` int(11) unsigned DEFAULT NULL,
  `IsActive` tinyint(1) unsigned DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DischargeReasonId` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date DEFAULT '0001-01-01',
  `Modified` date DEFAULT '0001-01-01',
  `DischargeReason` text,
  PRIMARY KEY (`Id`),
  KEY `PatientId` (`PatientId`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientdocuments
CREATE TABLE IF NOT EXISTS `patientdocuments` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `AssetId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UploadTypeId` char(36) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Filename` varchar(200) NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `AssetId` (`AssetId`),
  CONSTRAINT `FK_PatientDocuments_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientDocuments_AssetId` FOREIGN KEY (`AssetId`) REFERENCES `assets` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientDocuments_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientemergencycontacts
CREATE TABLE IF NOT EXISTS `patientemergencycontacts` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `PrimaryPhone` char(10) NOT NULL,
  `AlternatePhone` char(10) DEFAULT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `IsPrimary` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Emergency_PatientId` (`PatientId`),
  CONSTRAINT `FK_Emergency_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientepisodes
CREATE TABLE IF NOT EXISTS `patientepisodes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `Details` text,
  `StartDate` date NOT NULL DEFAULT '0001-01-01',
  `EndDate` date NOT NULL DEFAULT '0001-01-01',
  `Schedule` mediumtext,
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsRecertCompleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDischarged` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsLinkedToDischarge` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AssessmentId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AssessmentType` varchar(100) DEFAULT '',
  `Created` date NOT NULL DEFAULT '0001-01-01',
  `Modified` date NOT NULL DEFAULT '0001-01-01',
  `StartOfCareDate` date NOT NULL DEFAULT '0001-01-01',
  `AdmissionId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Therapist` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `CaseManager` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `PrimaryPhysician` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `PrimaryInsurance` int(11) DEFAULT '0',
  `SecondaryInsurance` int(11) DEFAULT '0',
  `Comments` text,
  PRIMARY KEY (`Id`),
  KEY `FK_PatientEpisode_PatientId` (`PatientId`),
  KEY `FK_PatientEpisode_AgencyId` (`AgencyId`),
  KEY `AdmissionId` (`AdmissionId`),
  KEY `IX_PatientEpisodes_Active_Enddate` (`IsActive`,`EndDate`),
  CONSTRAINT `FK_PatientEpisode_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientEpisode_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientnonadmitreasons
CREATE TABLE IF NOT EXISTS `patientnonadmitreasons` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `NonAdmissionDate` date DEFAULT '0001-01-01',
  `NonAdmissionReason` text,
  `NonAdmissionComments` text,
  `IsCurrent` tinyint(1) DEFAULT '0',
  `Created` date DEFAULT '0001-01-01',
  `Modified` date DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `FK_PatientAdmissionDates_AgencyId` (`AgencyId`),
  KEY `FK_PatientAdmissionDates_PatientId` (`PatientId`),
  CONSTRAINT `FK_PatientAdmissionDates_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientnotes
CREATE TABLE IF NOT EXISTS `patientnotes` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `Notes` tinytext NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_PatientNotes_PatientId` (`PatientId`),
  CONSTRAINT `FK_PatientNotes_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientphysicians
CREATE TABLE IF NOT EXISTS `patientphysicians` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PatientId` char(36) NOT NULL,
  `PhysicianId` char(36) NOT NULL,
  `IsPrimary` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PatientPhysicians_PatientId` (`PatientId`),
  KEY `FK_PatientPhysicians_PhysicianId` (`PhysicianId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientprofiles
CREATE TABLE IF NOT EXISTS `patientprofiles` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `AgencyLocationId` char(36) DEFAULT NULL,
  `MedicareNumber` char(11) DEFAULT NULL,
  `MedicaidNumber` char(20) DEFAULT NULL,
  `PatientIdNumber` char(50) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleInitial` varchar(1) DEFAULT NULL,
  `StartofCareDate` date NOT NULL,
  `PrimaryInsurance` int(11) DEFAULT '-1',
  `PrimaryHealthPlanId` varchar(50) DEFAULT NULL,
  `PrimaryGroupName` varchar(50) DEFAULT NULL,
  `PrimaryGroupId` varchar(50) DEFAULT NULL,
  `PrimaryRelationship` varchar(10) DEFAULT '2',
  `SecondaryInsurance` int(11) DEFAULT '-1',
  `SecondaryHealthPlanId` varchar(50) DEFAULT NULL,
  `SecondaryGroupName` varchar(50) DEFAULT NULL,
  `SecondaryGroupId` varchar(50) DEFAULT NULL,
  `SecondaryRelationship` varchar(10) DEFAULT '2',
  `TertiaryInsurance` int(11) DEFAULT '-1',
  `TertiaryHealthPlanId` varchar(50) DEFAULT NULL,
  `TertiaryGroupName` varchar(50) DEFAULT NULL,
  `TertiaryGroupId` varchar(50) DEFAULT NULL,
  `TertiaryRelationship` varchar(10) DEFAULT '2',
  `Payer` varchar(20) DEFAULT NULL,
  `ServicesRequired` varchar(100) DEFAULT NULL,
  `PaymentSource` varchar(50) DEFAULT NULL,
  `OtherPaymentSource` varchar(50) DEFAULT NULL,
  `AdmissionSource` varchar(50) DEFAULT NULL,
  `ReferrerPhysician` char(36) DEFAULT NULL,
  `ReferralDate` date DEFAULT '0001-01-01',
  `OtherReferralSource` varchar(30) DEFAULT NULL,
  `ReferralName` varchar(50) DEFAULT NULL,
  `ReferralContact` varchar(50) DEFAULT NULL,
  `InternalReferral` char(36) DEFAULT NULL,
  `CaseManagerId` char(36) DEFAULT NULL,
  `AuditorId` char(36) DEFAULT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `NonAdmissionDate` date DEFAULT NULL,
  `NonAdmissionReason` varchar(50) DEFAULT NULL,
  `Comments` text,
  `LastEligibilityCheck` date DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DischargeDate` date DEFAULT '0001-01-01',
  `DischargeReasonId` tinyint(2) NOT NULL DEFAULT '0',
  `DischargeReason` text,
  `AdmissionId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `OldStatus` int(11) DEFAULT '0',
  `Created` date DEFAULT '0001-01-01',
  `Modified` date DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `FK_Patient_AgencyId` (`AgencyId`),
  KEY `FK_Patient_UserId` (`UserId`),
  KEY `FK_Patient_CaseManagerId` (`CaseManagerId`),
  KEY `FK_Patient_AgencyLocationId` (`AgencyLocationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patients
CREATE TABLE IF NOT EXISTS `patients` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `AgencyLocationId` char(36) DEFAULT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `MedicareNumber` char(11) DEFAULT NULL,
  `MedicaidNumber` char(20) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleInitial` varchar(1) DEFAULT NULL,
  `SSN` char(9) DEFAULT NULL,
  `DOB` date NOT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `Height` float DEFAULT NULL,
  `HeightMetric` tinyint(4) DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `WeightMetric` tinyint(4) DEFAULT NULL,
  `Ethnicities` varchar(50) NOT NULL,
  `MaritalStatus` varchar(20) DEFAULT NULL,
  `AddressLine1` varchar(100) NOT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) NOT NULL,
  `AddressCounty` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) NOT NULL,
  `AddressZipCode` char(9) NOT NULL,
  `CBSA` char(5) DEFAULT NULL,
  `EvacuationAddressLine1` varchar(100) DEFAULT NULL,
  `EvacuationAddressLine2` varchar(100) DEFAULT NULL,
  `EvacuationAddressCity` varchar(100) DEFAULT NULL,
  `EvacuationAddressStateCode` char(2) DEFAULT NULL,
  `EvacuationAddressZipCode` char(5) DEFAULT NULL,
  `PhoneHome` char(10) DEFAULT NULL,
  `PhoneMobile` char(10) DEFAULT NULL,
  `EvacuationPhoneHome` char(10) DEFAULT NULL,
  `EvacuationPhoneMobile` char(10) DEFAULT NULL,
  `EmailAddress` varchar(50) DEFAULT NULL,
  `StartofCareDate` date NOT NULL,
  `PrimaryPharmacyId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `SecondaryPharmacyId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `PharmacyName` varchar(100) DEFAULT NULL,
  `PharmacyPhone` char(10) DEFAULT NULL,
  `PrimaryInsurance` int(11) DEFAULT '-1',
  `PrimaryHealthPlanId` varchar(50) DEFAULT NULL,
  `PrimaryGroupName` varchar(50) DEFAULT NULL,
  `PrimaryGroupId` varchar(50) DEFAULT NULL,
  `PrimaryRelationship` varchar(10) DEFAULT '2',
  `SecondaryInsurance` int(11) DEFAULT '-1',
  `SecondaryHealthPlanId` varchar(50) DEFAULT NULL,
  `SecondaryGroupName` varchar(50) DEFAULT NULL,
  `SecondaryGroupId` varchar(50) DEFAULT NULL,
  `SecondaryRelationship` varchar(10) DEFAULT '2',
  `TertiaryInsurance` int(11) DEFAULT '-1',
  `TertiaryHealthPlanId` varchar(50) DEFAULT NULL,
  `TertiaryGroupName` varchar(50) DEFAULT NULL,
  `TertiaryGroupId` varchar(50) DEFAULT NULL,
  `TertiaryRelationship` varchar(10) DEFAULT '2',
  `DME` varchar(100) DEFAULT NULL,
  `OtherDME` varchar(100) DEFAULT NULL,
  `Payer` varchar(20) DEFAULT NULL,
  `ServicesRequired` varchar(100) DEFAULT NULL,
  `PaymentSource` varchar(50) DEFAULT NULL,
  `OtherPaymentSource` varchar(50) DEFAULT NULL,
  `AdmissionSource` varchar(50) DEFAULT NULL,
  `ReferrerPhysician` char(36) DEFAULT NULL,
  `ReferralDate` date DEFAULT '0001-01-01',
  `OtherReferralSource` varchar(30) DEFAULT NULL,
  `ReferralName` varchar(50) DEFAULT NULL,
  `ReferralContact` varchar(50) DEFAULT NULL,
  `InternalReferral` char(36) DEFAULT NULL,
  `CaseManagerId` char(36) DEFAULT NULL,
  `AuditorId` char(36) DEFAULT NULL,
  `Triage` tinyint(3) unsigned DEFAULT '1',
  `EvacuationZone` varchar(2) DEFAULT NULL,
  `PhotoId` char(36) DEFAULT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `NonAdmissionDate` date DEFAULT NULL,
  `NonAdmissionReason` varchar(50) DEFAULT NULL,
  `Comments` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `LastEligibilityCheck` date DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDNR` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsHospitalized` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HospitalizationId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `DischargeDate` date DEFAULT '0001-01-01',
  `DischargeReasonId` tinyint(2) NOT NULL DEFAULT '0',
  `DischargeReason` text,
  `AdmissionId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Services` int(100) unsigned NOT NULL DEFAULT '1',
  `HomeHealthStatus` int(10) unsigned NOT NULL DEFAULT '1',
  `PrivateDutyStatus` int(10) unsigned NOT NULL DEFAULT '1',
  `ServiceLocation` char(5) DEFAULT 'Q5001',
  `Latitude` float(10,7) NOT NULL,
  `Longitude` float(10,7) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Patient_AgencyId` (`AgencyId`),
  KEY `FK_Patient_UserId` (`UserId`),
  KEY `FK_Patient_CaseManagerId` (`CaseManagerId`),
  KEY `FK_Patient_AgencyLocationId` (`AgencyLocationId`),
  CONSTRAINT `FK_Patient_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Patient_AgencyLocationId` FOREIGN KEY (`AgencyLocationId`) REFERENCES `agencylocations` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientteams
CREATE TABLE IF NOT EXISTS `patientteams` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `TeamId` char(36) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_patientteams_patients` (`PatientId`),
  KEY `FK_patientteams_agencyteams` (`TeamId`),
  CONSTRAINT `FK_patientteams_agencyteams` FOREIGN KEY (`teamId`) REFERENCES `agencyteams` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_patientteams_patients` FOREIGN KEY (`patientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientusers
CREATE TABLE IF NOT EXISTS `patientusers` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientvisitlogs
CREATE TABLE IF NOT EXISTS `patientvisitlogs` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `TaskId` char(36) NOT NULL,
  `IsPatientUnableToSign` tinyint(1) NOT NULL DEFAULT '0',
  `PatientUnableToSignReason` varchar(500) DEFAULT NULL,
  `PatientSignatureAssetId` char(36) DEFAULT NULL,
  `VerifiedDateTime` datetime NOT NULL,
  `VerifiedLatitude` float(10,7) DEFAULT NULL,
  `VerifiedLongitude` float(10,7) DEFAULT NULL,
  `VerifiedAddress` varchar(250) DEFAULT NULL,
  `PatientAddress` varchar(250) DEFAULT NULL,
  `Created` datetime NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PatientVisitLogs_AgencyId` (`AgencyId`),
  KEY `FK_PatientVisitLogs_PatientId` (`PatientId`),
  KEY `FK_PatientVisitLogs_UserId` (`UserId`),
  KEY `FK_PatientVisitLogs_EpisodeId` (`EpisodeId`),
  CONSTRAINT `FK_PatientVisitLogs_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitLogs_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitLogs_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitLogs_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientvisitnotes
CREATE TABLE IF NOT EXISTS `patientvisitnotes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `NoteType` varchar(50) DEFAULT NULL,
  `Note` mediumtext NOT NULL,
  `Status` int(10) unsigned DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsBillable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WoundNote` mediumtext,
  `IsWoundCare` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSupplyExist` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Supply` text,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `SentDate` date DEFAULT NULL,
  `OrderNumber` bigint(20) unsigned DEFAULT '0',
  `PhysicianData` text,
  `Version` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `FK_PatientVisitNotes_AgencyId` (`AgencyId`),
  KEY `FK_PatientVisitNotes_PatientId` (`PatientId`),
  KEY `FK_PatientVisitNotes_UserId` (`UserId`),
  KEY `FK_PatientVisitNotes_EpisodeId` (`EpisodeId`),
  KEY `PhysicianId` (`PhysicianId`),
  CONSTRAINT `FK_PatientVisitNotes_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitNotes_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitNotes_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitNotes_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.payperiods
CREATE TABLE IF NOT EXISTS `payperiods` (
  `Id` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) NOT NULL,
  `PayDate` datetime DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `IsPaid` tinyint(4) DEFAULT NULL,
  `Comments` text,
  `IsDeprecated` tinyint(4) DEFAULT '0',
  `Created` datetime DEFAULT NULL,
  `Modified` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.payperiodtasks
CREATE TABLE IF NOT EXISTS `payperiodtasks` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `PayPeriodId` char(36) NOT NULL,
  `TaskId` char(36) NOT NULL,
  `IsDeprecated` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_PayPeriodId` (`PayPeriodId`),
  CONSTRAINT `FK_PayPeriodId` FOREIGN KEY (`PayPeriodId`) REFERENCES `payperiods` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.pdpatientvisitnotes
CREATE TABLE IF NOT EXISTS `pdpatientvisitnotes` (
  `AgencyId` char(36) NOT NULL,
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `NoteType` varchar(50) DEFAULT NULL,
  `Status` int(10) unsigned DEFAULT NULL,
  `IsBillable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsWoundCare` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSupplyExist` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `ReceivedDate` date DEFAULT NULL,
  `SentDate` date DEFAULT NULL,
  `OrderNumber` bigint(20) unsigned DEFAULT '0',
  `PhysicianData` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Version` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `FK_PatientVisitNotes_AgencyId` (`AgencyId`),
  KEY `FK_PatientVisitNotes_PatientId` (`PatientId`),
  KEY `FK_PatientVisitNotes_UserId` (`UserId`),
  KEY `PhysicianId` (`PhysicianId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.pdscheduleevents
CREATE TABLE IF NOT EXISTS `pdscheduleevents` (
  `EventId` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `DisciplineTask` int(5) DEFAULT NULL,
  `Status` int(5) DEFAULT NULL,
  `Discipline` varchar(20) DEFAULT NULL,
  `IsBillable` tinyint(1) NOT NULL,
  `IsMissedVisit` tinyint(1) DEFAULT NULL,
  `VTimeIn` datetime DEFAULT NULL,
  `STimeIn` datetime DEFAULT NULL,
  `VTimeOut` datetime DEFAULT NULL,
  `STimeOut` datetime DEFAULT NULL,
  `Surcharge` varchar(10) DEFAULT NULL,
  `AssociatedMileage` varchar(10) DEFAULT NULL,
  `ReturnReason` text,
  `Comments` text,
  `IsDeprecated` tinyint(1) DEFAULT NULL,
  `IsOrderForNextEpisode` tinyint(1) DEFAULT NULL,
  `IsVisitPaid` tinyint(1) DEFAULT NULL,
  `InPrintQueue` tinyint(1) DEFAULT NULL,
  `Asset` text,
  `Version` int(11) DEFAULT '1',
  PRIMARY KEY (`EventId`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `UserId` (`UserId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.physicianorders
CREATE TABLE IF NOT EXISTS `physicianorders` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `UserId` char(36) NOT NULL,
  `Summary` varchar(100) DEFAULT NULL,
  `Text` text NOT NULL,
  `OrderDate` date NOT NULL,
  `Created` date NOT NULL,
  `Modified` date DEFAULT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `SentDate` date NOT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date NOT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Status` int(10) unsigned NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsOrderForNextEpisode` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsOrderReadAndVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PhysicianData` text,
  PRIMARY KEY (`Id`),
  KEY `FK_PhysicianOrder_PatientId` (`PatientId`),
  KEY `FK_PhysicianOrder_PhysicianId` (`PhysicianId`),
  KEY `FK_PhysicianOrder_UserId` (`UserId`),
  KEY `FK_PhysicianOrder_EpisodeId` (`EpisodeId`),
  CONSTRAINT `FK_PhysicianOrder_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_PhysicianOrder_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.planofcares
CREATE TABLE IF NOT EXISTS `planofcares` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL DEFAULT '',
  `PatientId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `UserId` char(36) NOT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `Data` text,
  `Status` int(11) NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `SentDate` date DEFAULT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AssessmentId` char(36) DEFAULT NULL,
  `AssessmentType` varchar(50) DEFAULT NULL,
  `PhysicianData` text,
  `IsNonOasis` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsStandAlone` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisciplineTask` int(11) NOT NULL,
  PRIMARY KEY (`Id`,`EpisodeId`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.planofcarestandalones
CREATE TABLE IF NOT EXISTS `planofcarestandalones` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `Data` text,
  `Status` int(11) NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `SentDate` date DEFAULT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PhysicianData` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.raps
CREATE TABLE IF NOT EXISTS `raps` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `IsOasisComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FirstBillableVisitDate` date DEFAULT NULL,
  `IsGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date NOT NULL,
  `Created` date DEFAULT NULL,
  `Remark` text,
  `PaymentDate` date NOT NULL DEFAULT '0001-01-01',
  `MedicareNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) NOT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `AreOrdersComplete` tinyint(1) unsigned DEFAULT NULL,
  `PrimaryInsuranceId` int(4) unsigned NOT NULL,
  `Status` int(4) unsigned NOT NULL,
  `Type` int(4) unsigned DEFAULT NULL,
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) DEFAULT '0',
  `ClaimDate` datetime DEFAULT '0001-01-01 00:00:00',
  `Payment` double DEFAULT '0',
  `ProspectivePay` double DEFAULT '0',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `DischargeDate` date DEFAULT '0001-01-01',
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `GroupName` varchar(50) DEFAULT NULL,
  `GroupId` varchar(50) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `Authorization` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `AuthorizationNumber2` varchar(50) DEFAULT NULL,
  `AuthorizationNumber3` varchar(50) DEFAULT NULL,
  `ConditionCodes` text,
  `UB4PatientStatus` varchar(5) DEFAULT NULL,
  `CBSA` varchar(6) DEFAULT NULL,
  `Ub04Locator81cca` text,
  `Ub04Locator39` text,
  `Insurance` text,
  `Remittance` text,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.rapsnapshots
CREATE TABLE IF NOT EXISTS `rapsnapshots` (
  `MainId` char(46) NOT NULL,
  `Id` char(36) NOT NULL,
  `BatchId` bigint(20) NOT NULL DEFAULT '0',
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `IsOasisComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FirstBillableVisitDate` date DEFAULT NULL,
  `IsGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Remark` text,
  `PaymentDate` date DEFAULT NULL,
  `MedicareNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) NOT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `AreOrdersComplete` tinyint(1) unsigned DEFAULT NULL,
  `PrimaryInsuranceId` tinyint(4) unsigned NOT NULL,
  `Status` int(4) unsigned NOT NULL,
  `Type` int(4) unsigned DEFAULT NULL,
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) DEFAULT '0',
  `ClaimDate` datetime DEFAULT '0001-01-01 00:00:00',
  `Payment` double DEFAULT '0',
  `ProspectivePay` double DEFAULT '0',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `DischargeDate` date DEFAULT '0001-01-01',
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `Reason` text,
  PRIMARY KEY (`MainId`),
  KEY `MainId` (`MainId`),
  KEY `IX_RapSnapShots_AgencyId_Id` (`AgencyId`,`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.referralemergencycontacts
CREATE TABLE IF NOT EXISTS `referralemergencycontacts` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `ReferralId` char(36) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `PrimaryPhone` char(10) NOT NULL,
  `AlternatePhone` char(10) DEFAULT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `IsPrimary` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Emergency_ReferralId` (`ReferralId`),
  CONSTRAINT `FK_Emergency_ReferralId` FOREIGN KEY (`ReferralId`) REFERENCES `referrals` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.referrals
CREATE TABLE IF NOT EXISTS `referrals` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `AdmissionSource` tinyint(4) unsigned NOT NULL,
  `ReferrerPhysician` char(36) DEFAULT NULL,
  `ReferralDate` date DEFAULT NULL,
  `OtherReferralSource` varchar(50) DEFAULT NULL,
  `ReferralName` varchar(50) DEFAULT NULL,
  `ReferralContact` varchar(50) DEFAULT NULL,
  `InternalReferral` char(36) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleInitial` varchar(1) DEFAULT NULL,
  `MedicareNumber` char(12) DEFAULT NULL,
  `MedicaidNumber` char(20) DEFAULT NULL,
  `SSN` char(9) DEFAULT NULL,
  `DOB` date NOT NULL,
  `Gender` varchar(20) NOT NULL,
  `Height` float DEFAULT NULL,
  `HeightMetric` tinyint(4) DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `WeightMetric` tinyint(4) DEFAULT NULL,
  `MaritalStatus` varchar(20) DEFAULT NULL,
  `PhoneHome` char(10) NOT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `AddressLine1` varchar(100) NOT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) NOT NULL,
  `AddressStateCode` char(2) NOT NULL,
  `AddressZipCode` char(5) NOT NULL,
  `ServicesRequired` varchar(100) DEFAULT NULL,
  `PrimaryInsurance` int(11) DEFAULT '-1',
  `PrimaryHealthPlanId` varchar(50) DEFAULT NULL,
  `PrimaryGroupName` varchar(50) DEFAULT NULL,
  `PrimaryGroupId` varchar(50) DEFAULT NULL,
  `PrimaryRelationship` varchar(50) DEFAULT NULL,
  `SecondaryInsurance` int(11) DEFAULT '-1',
  `SecondaryHealthPlanId` varchar(50) DEFAULT NULL,
  `SecondaryGroupName` varchar(50) DEFAULT NULL,
  `SecondaryGroupId` varchar(50) DEFAULT NULL,
  `SecondaryRelationship` varchar(50) DEFAULT NULL,
  `TertiaryInsurance` int(11) DEFAULT '-1',
  `TertiaryHealthPlanId` varchar(50) DEFAULT NULL,
  `TertiaryGroupName` varchar(50) DEFAULT NULL,
  `TertiaryGroupId` varchar(50) DEFAULT NULL,
  `TertiaryRelationship` varchar(50) DEFAULT NULL,
  `DME` varchar(50) DEFAULT NULL,
  `OtherDME` varchar(100) DEFAULT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `NonAdmissionDate` date DEFAULT NULL,
  `NonAdmissionReason` varchar(200) DEFAULT NULL,
  `VerificationDate` date DEFAULT NULL,
  `CreatedById` char(36) DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Modified` date DEFAULT NULL,
  `Physicians` text,
  `Comments` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDNR` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Services` int(100) unsigned NOT NULL DEFAULT '1',
  `HomeHealthStatus` int(100) unsigned NOT NULL DEFAULT '1',
  `PrivateDutyStatus` int(100) unsigned NOT NULL DEFAULT '1',
  `DeprecatedServices` int(100) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Referral_AgencyId` (`AgencyId`),
  KEY `FK_Referral_UserId` (`UserId`),
  CONSTRAINT `FK_Referral_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.remitqueues
CREATE TABLE IF NOT EXISTS `remitqueues` (
  `Id` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) DEFAULT NULL,
  `Data` longtext,
  `Error` text,
  `IsUpload` tinyint(1) DEFAULT '0',
  `Priority` tinyint(3) unsigned DEFAULT NULL,
  `Status` enum('queued','processing','completed','unsucessful') DEFAULT NULL,
  `Created` date DEFAULT '0001-01-01',
  `Modified` date DEFAULT '0001-01-01',
  `UpStat` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.remittanceClaims
CREATE TABLE IF NOT EXISTS `remittanceClaims` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `RemittanceId` char(36) NOT NULL,
  `RapFinal` enum('Rap','Final') NOT NULL,
  `PayerClaimControlNumber` char(30) NOT NULL,
  `Data` longtext NOT NULL,
  `Description` text NOT NULL,
  `Value` double NOT NULL,
  `InterchangeDate` date NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.remittances
CREATE TABLE IF NOT EXISTS `remittances` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Data` longtext NOT NULL,
  `RemitId` char(30) NOT NULL,
  `TotalClaims` int(10) unsigned NOT NULL,
  `CoveredAmount` double NOT NULL,
  `ChargedAmount` double NOT NULL,
  `PaymentAmount` double NOT NULL,
  `RemittanceDate` date DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.reportdescriptions
CREATE TABLE IF NOT EXISTS `reportdescriptions` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) DEFAULT NULL,
  `Category` varchar(50) DEFAULT NULL,
  `CategoryId` int(20) DEFAULT '0',
  `Link` varchar(100) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `Parameters` varchar(500) DEFAULT NULL,
  `InformationInclude` varchar(500) DEFAULT NULL,
  `Comments` varchar(500) DEFAULT NULL,
  `IsOnclick` tinyint(1) DEFAULT '0',
  `IsProductionReady` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.reports
CREATE TABLE IF NOT EXISTS `reports` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `AssetId` char(36) DEFAULT NULL,
  `Type` varchar(150) NOT NULL,
  `Format` varchar(100) DEFAULT NULL,
  `Status` varchar(100) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Created` datetime NOT NULL,
  `Completed` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `UserId` (`UserId`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.returncomments
CREATE TABLE IF NOT EXISTS `returncomments` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `EventId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `Comments` text,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `EventId` (`EventId`),
  KEY `IsDeprecated` (`IsDeprecated`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.returncomments_staging
CREATE TABLE IF NOT EXISTS `returncomments_staging` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `EventId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `Comments` text,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  KEY `AgencyId` (`AgencyId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `EventId` (`EventId`),
  KEY `IsDeprecated` (`IsDeprecated`),
  KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.scheduleevents_old
CREATE TABLE IF NOT EXISTS `scheduleevents_old` (
  `Id` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) NOT NULL DEFAULT '',
  `UserId` char(36) DEFAULT NULL,
  `DisciplineTask` int(5) DEFAULT NULL,
  `UserName` varchar(100) DEFAULT NULL,
  `EventDate` date DEFAULT '0001-01-01',
  `VisitDate` date DEFAULT '0001-01-01',
  `Status` int(5) DEFAULT NULL,
  `Discipline` varchar(20) DEFAULT NULL,
  `IsBillable` tinyint(1) DEFAULT NULL,
  `IsPayable` tinyint(1) DEFAULT NULL,
  `IsVisitVerified` tinyint(1) DEFAULT NULL,
  `IsMissedVisit` tinyint(1) DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `TravelTimeIn` varchar(10) DEFAULT NULL,
  `TravelTimeOut` varchar(10) DEFAULT NULL,
  `Surcharge` varchar(10) DEFAULT NULL,
  `AssociatedMileage` varchar(10) DEFAULT NULL,
  `ReturnReason` text,
  `MissedVisitFormReturnReason` text,
  `Comments` text,
  `PersonalComments` text,
  `IsDeprecated` tinyint(1) DEFAULT NULL,
  `IsOrderForNextEpisode` tinyint(1) DEFAULT NULL,
  `IsVisitPaid` tinyint(1) DEFAULT NULL,
  `Version` int(11) DEFAULT '1',
  `SendAsOrder` char(3) DEFAULT '0',
  `InPrintQueue` tinyint(1) DEFAULT NULL,
  `ServiceLocation` char(6) DEFAULT NULL,
  `Asset` text,
  `StartDate` date DEFAULT '0001-01-01',
  `EndDate` date DEFAULT '0001-01-01',
  `ClaimStartDate` date DEFAULT '0001-01-01',
  `ClaimEndDate` date DEFAULT '0001-01-01',
  `BeforeMissedVisitStatus` int(5) DEFAULT '0',
  `IsQANote` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.scheduletasks
CREATE TABLE IF NOT EXISTS `scheduletasks` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `DisciplineTask` int(5) DEFAULT NULL,
  `UserName` varchar(100) DEFAULT NULL,
  `EventDate` date DEFAULT '0001-01-01',
  `VisitDate` date DEFAULT '0001-01-01',
  `Status` int(5) DEFAULT '0',
  `DisciplineText` varchar(20) DEFAULT NULL,
  `Discipline` int(11) DEFAULT '0',
  `IsBillable` tinyint(1) DEFAULT '0',
  `IsPayable` tinyint(1) DEFAULT '0',
  `IsTimeRangeExist` tinyint(1) DEFAULT '1',
  `IsVisitVerified` tinyint(1) DEFAULT '0',
  `IsMissedVisit` tinyint(1) DEFAULT '0',
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `VisitDateTimeIn` datetime DEFAULT '0001-01-01 00:00:00',
  `VisitDateTimeOut` datetime DEFAULT '0001-01-01 00:00:00',
  `TravelTimeIn` datetime DEFAULT '0001-01-01 00:00:00',
  `TravelTimeOut` datetime DEFAULT '0001-01-01 00:00:00',
  `Surcharge` varchar(10) DEFAULT NULL,
  `AssociatedMileage` varchar(10) DEFAULT NULL,
  `ReturnReason` text,
  `MissedVisitFormReturnReason` text,
  `Comments` text,
  `PersonalComments` text,
  `IsDeprecated` tinyint(1) DEFAULT '0',
  `IsOrderForNextEpisode` tinyint(1) DEFAULT '0',
  `IsVisitPaid` tinyint(1) DEFAULT '0',
  `Version` int(11) DEFAULT '1',
  `SendAsOrder` tinyint(1) DEFAULT '0',
  `IsOrder` tinyint(1) DEFAULT '0',
  `InPrintQueue` tinyint(1) DEFAULT '0',
  `ServiceLocation` char(6) DEFAULT NULL,
  `StartDate` date DEFAULT '0001-01-01',
  `EndDate` date DEFAULT '0001-01-01',
  `ClaimStartDate` date DEFAULT '0001-01-01',
  `ClaimEndDate` date DEFAULT '0001-01-01',
  `BeforeMissedVisitStatus` int(5) DEFAULT '0',
  `IsQANote` tinyint(1) DEFAULT '0',
  `Asset` text,
  `IsActive` tinyint(4) DEFAULT '1',
  `IsDischarged` tinyint(4) DEFAULT '1',
  `PrimaryInsurance` int(3) DEFAULT '0',
  `SecondaryInsurance` int(3) DEFAULT '0',
  `CommentCount` int(3) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.scheduletasks_staging
CREATE TABLE IF NOT EXISTS `scheduletasks_staging` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `DisciplineTask` int(5) DEFAULT NULL,
  `UserName` varchar(100) DEFAULT NULL,
  `EventDate` date DEFAULT '0001-01-01',
  `VisitDate` date DEFAULT '0001-01-01',
  `Status` int(5) DEFAULT NULL,
  `DisciplineText` varchar(20) DEFAULT NULL,
  `Discipline` int(11) DEFAULT '0',
  `IsBillable` tinyint(1) DEFAULT NULL,
  `IsPayable` tinyint(1) DEFAULT NULL,
  `IsVisitVerified` tinyint(1) DEFAULT NULL,
  `IsMissedVisit` tinyint(1) DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `VisitDateTimeIn` datetime DEFAULT NULL,
  `VisitDateTimeOut` datetime DEFAULT NULL,
  `TravelTimeIn` varchar(10) DEFAULT NULL,
  `TravelTimeOut` varchar(10) DEFAULT NULL,
  `Surcharge` varchar(10) DEFAULT NULL,
  `AssociatedMileage` varchar(10) DEFAULT NULL,
  `ReturnReason` text,
  `MissedVisitFormReturnReason` text,
  `Comments` text,
  `PersonalComments` text,
  `IsDeprecated` tinyint(1) DEFAULT NULL,
  `IsOrderForNextEpisode` tinyint(1) DEFAULT NULL,
  `IsVisitPaid` tinyint(1) DEFAULT NULL,
  `Version` int(11) DEFAULT '1',
  `SendAsOrder` tinyint(1) DEFAULT '0',
  `InPrintQueue` tinyint(1) DEFAULT NULL,
  `ServiceLocation` char(6) DEFAULT NULL,
  `StartDate` date DEFAULT '0001-01-01',
  `EndDate` date DEFAULT '0001-01-01',
  `ClaimStartDate` date DEFAULT '0001-01-01',
  `ClaimEndDate` date DEFAULT '0001-01-01',
  `BeforeMissedVisitStatus` int(5) DEFAULT '0',
  `IsQANote` tinyint(1) DEFAULT NULL,
  `Asset` text,
  `IsActive` tinyint(4) DEFAULT '1',
  `IsDischarged` tinyint(4) DEFAULT '1',
  `PrimaryInsurance` int(11) DEFAULT '0',
  `SecondaryInsurance` int(11) DEFAULT '0',
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `UserId` (`UserId`),
  KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.secondaryclaims
CREATE TABLE IF NOT EXISTS `secondaryclaims` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PrimaryClaimId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `RemitId` varchar(50) DEFAULT NULL,
  `RemitDate` date DEFAULT NULL,
  `TotalAdjustmentAmount` double(10,0) DEFAULT NULL,
  `IsuranceIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FirstBillableVisitDate` date DEFAULT NULL,
  `Remark` text,
  `MedicareNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `VerifiedVisits` mediumtext,
  `Supply` mediumtext,
  `SupplyTotal` double(10,0) DEFAULT NULL,
  `ProspectivePay` double unsigned NOT NULL DEFAULT '0',
  `ClaimAmount` double unsigned NOT NULL DEFAULT '0',
  `Payment` double NOT NULL DEFAULT '0',
  `SecondaryInsuranceId` int(4) unsigned NOT NULL,
  `IsInfoVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsInsuranceVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVisitVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsRemittanceVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSupplyVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Status` int(4) unsigned NOT NULL DEFAULT '0',
  `Type` int(4) unsigned DEFAULT '0',
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) unsigned DEFAULT '0',
  `ClaimDate` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `DischargeDate` date DEFAULT '0001-01-01',
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `GroupName` varchar(50) DEFAULT NULL,
  `GroupId` varchar(50) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `Authorization` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `AuthorizationNumber2` varchar(50) DEFAULT NULL,
  `AuthorizationNumber3` varchar(50) DEFAULT NULL,
  `ConditionCodes` text NOT NULL,
  `SupplyCode` varchar(10) DEFAULT '0272',
  `UB4PatientStatus` varchar(5) DEFAULT NULL,
  `CBSA` varchar(6) DEFAULT NULL,
  `Ub04Locator31` text,
  `Ub04Locator32` text,
  `Ub04Locator33` text,
  `Ub04Locator34` text,
  `Ub04Locator39` text,
  `Ub04Locator81cca` text,
  `HCFALocators` text,
  `Insurance` mediumtext,
  `Remittance` text,
  `Adjustments` text,
  `InvoiceType` int(4) unsigned DEFAULT '0',
  `IsDeprecated` tinyint(1) NOT NULL DEFAULT '0',
  `IsHomeHealthServiceIncluded` tinyint(1) NOT NULL DEFAULT '0',
  `IsGenerated` tinyint(1) NOT NULL DEFAULT '0',
  `Created` date NOT NULL DEFAULT '0001-01-01',
  `Modified` date NOT NULL DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `PatientId` (`PatientId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PrimaryClaimId` (`PrimaryClaimId`),
  CONSTRAINT `FK_SecondaryClaims_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_SecondaryClaims_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_SecondaryClaims_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.shpdatabatches
CREATE TABLE IF NOT EXISTS `shpdatabatches` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `Status` tinyint(4) unsigned NOT NULL,
  `SubmissionFormat` text,
  `LastModified` date NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ShpDataBatches_AgencyId` (`AgencyId`),
  KEY `FK_ShpDataBatches_PatientId` (`PatientId`),
  CONSTRAINT `FK_ShpDataBatches_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ShpDataBatches_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.shpdatalogins
CREATE TABLE IF NOT EXISTS `shpdatalogins` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Username` varchar(100) DEFAULT NULL,
  `PasswordHash` char(100) DEFAULT NULL,
  `ProviderNo` varchar(10) NOT NULL,
  `ActivationCode` varchar(10) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ShpDataLogins_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_ShpDataLogins_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.systemmessages
CREATE TABLE IF NOT EXISTS `systemmessages` (
  `Id` char(36) NOT NULL,
  `Subject` varchar(150) NOT NULL,
  `Body` text NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.taskaudits
CREATE TABLE IF NOT EXISTS `taskaudits` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `EntityId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `DisciplineTaskId` tinyint(3) unsigned NOT NULL,
  `Log` text NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_TaskAudit_UserId` (`EntityId`),
  KEY `FK_TaskAudit_AgencyId` (`AgencyId`),
  KEY `FK_TaskAudit_PatientId` (`PatientId`),
  CONSTRAINT `FK_TaskAudit_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TaskAudit_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.uploadtypes
CREATE TABLE IF NOT EXISTS `uploadtypes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Type` varchar(200) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  CONSTRAINT `FK_UploadTypes_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.userlocations
CREATE TABLE IF NOT EXISTS `userlocations` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `AgencyLocationId` char(36) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_UserLocations` (`UserId`),
  KEY `FK_UserLocations_AgencyId` (`AgencyId`),
  KEY `FK_UserLocations_AgencyLocationId` (`AgencyLocationId`),
  CONSTRAINT `FK_UserLocations_AgencyLocationId` FOREIGN KEY (`AgencyLocationId`) REFERENCES `agencylocations` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_UserLocations_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Intersection table that maps users to agency branches.';

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.usermessages
CREATE TABLE IF NOT EXISTS `usermessages` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `MessageId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `MessageType` int(1) NOT NULL,
  `FolderId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsRead` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ThreadId` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_SystemMessage` (`MessageId`),
  KEY `FK_User` (`UserId`),
  KEY `FK_UserMessages_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_UserMessages_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Intersection table that maps system messages to a user.';

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.usernonvisitrates
CREATE TABLE IF NOT EXISTS `usernonvisitrates` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `Rate` mediumtext NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(4) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK1_AgencyId` (`AgencyId`),
  KEY `FK2_UserId` (`UserId`),
  CONSTRAINT `FK1_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`),
  CONSTRAINT `FK2_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.usernonvisittasks
CREATE TABLE IF NOT EXISTS `usernonvisittasks` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `TaskId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `TaskDate` date DEFAULT NULL,
  `PaidStatus` tinyint(4) DEFAULT NULL,
  `PaidDate` date DEFAULT NULL,
  `TimeIn` datetime DEFAULT NULL,
  `TimeOut` datetime DEFAULT NULL,
  `Comments` varchar(2000) DEFAULT NULL,
  `IsDeprecated` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `TaskId` (`TaskId`),
  KEY `FK_UserId` (`UserId`),
  KEY `AgencyId` (`AgencyId`),
  CONSTRAINT `FK_TaskId` FOREIGN KEY (`TaskId`) REFERENCES `agencynonvisits` (`Id`),
  CONSTRAINT `FK_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.users
CREATE TABLE IF NOT EXISTS `users` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `CustomId` varchar(20) DEFAULT NULL,
  `ClinicianProviderId` varchar(20) DEFAULT NULL,
  `TitleType` varchar(50) DEFAULT NULL,
  `TitleTypeOther` varchar(100) DEFAULT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `Suffix` varchar(50) DEFAULT NULL,
  `Credentials` varchar(20) DEFAULT NULL,
  `CredentialsOther` varchar(20) DEFAULT NULL,
  `Roles` varchar(50) NOT NULL,
  `EmploymentType` varchar(50) DEFAULT 'Employee',
  `AgencyLocationId` char(36) NOT NULL,
  `Permissions` text NOT NULL,
  `Licenses` text,
  `Rates` mediumtext,
  `ProfileData` text NOT NULL,
  `Messages` text,
  `Status` tinyint(50) unsigned NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsAgencyLinked` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `AllowWeekendAccess` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `EarliestLoginTime` varchar(20) DEFAULT NULL,
  `AutomaticLogoutTime` varchar(20) DEFAULT NULL,
  `Comments` text,
  `SSN` varchar(10) DEFAULT NULL,
  `DOB` date DEFAULT '0001-01-01',
  `IsPrimary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AccountExpireDate` date NOT NULL DEFAULT '0001-01-01',
  `HireDate` date DEFAULT '0001-01-01',
  `TerminationDate` date DEFAULT '0001-01-01',
  `AccessibleServices` tinyint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `FK_User_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_User_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.userschedules
CREATE TABLE IF NOT EXISTS `userschedules` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `Visits` longtext NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_UserSchedule_UserId` (`UserId`),
  KEY `FK_UserSchedule_PatientId` (`PatientId`),
  KEY `FK_UserSchedule_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_UserSchedule_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_UserSchedule_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_UserSchedule_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for applicationmanagement
CREATE DATABASE IF NOT EXISTS `applicationmanagement` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `applicationmanagement`;


-- Dumping structure for table applicationmanagement.agencysnapshots
CREATE TABLE IF NOT EXISTS `agencysnapshots` (
  `Id` char(36) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `SalesPerson` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Trainer` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `BackupTrainer` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsAgreementSigned` tinyint(1) unsigned DEFAULT '1',
  `IsSuspended` tinyint(1) unsigned DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FrozenDate` date NOT NULL DEFAULT '0001-01-01',
  `IsFrozen` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DBServerIp` varchar(32) DEFAULT '10.0.1.61',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.applications
CREATE TABLE IF NOT EXISTS `applications` (
  `Id` tinyint(2) unsigned NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Modified` date NOT NULL,
  `Created` date NOT NULL,
  `IsDeprecated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `IX_Applications_Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.clusters
CREATE TABLE IF NOT EXISTS `clusters` (
  `Id` varchar(36) NOT NULL DEFAULT '',
  `DBServerIp` varchar(36) DEFAULT NULL,
  `WebServerIp` varchar(100) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.companies
CREATE TABLE IF NOT EXISTS `companies` (
  `Id` char(36) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `ServerIPAddress` varchar(15) NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.companyapplications
CREATE TABLE IF NOT EXISTS `companyapplications` (
  `CompanyId` char(36) NOT NULL,
  `ApplicationId` tinyint(2) unsigned NOT NULL,
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`CompanyId`,`ApplicationId`),
  KEY `FK_CompanyApplications_ApplicationId` (`ApplicationId`),
  CONSTRAINT `FK_CompanyApplications_ApplicationId` FOREIGN KEY (`ApplicationId`) REFERENCES `applications` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CompanyApplications_CompanyId` FOREIGN KEY (`CompanyId`) REFERENCES `companies` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.companyusers
CREATE TABLE IF NOT EXISTS `companyusers` (
  `CompanyId` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`CompanyId`,`LoginId`,`UserId`),
  KEY `FK_CompanyUsers_LoginId` (`LoginId`),
  CONSTRAINT `FK_CompanyUsers_CompanyId` FOREIGN KEY (`CompanyId`) REFERENCES `companies` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CompanyUsers_LoginId` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.customernotes
CREATE TABLE IF NOT EXISTS `customernotes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `Comments` text NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Recipients` text,
  `NoteType` tinyint(3) unsigned NOT NULL,
  `Summary` varchar(80) NOT NULL,
  `CallerId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IssueId` int(10) unsigned DEFAULT NULL,
  `AttachmentId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsCompleted` tinyint(1) unsigned NOT NULL,
  `DevUpdates` text,
  `AdditionalInformation` text,
  `ModifiedBy` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `Status` tinyint(100) unsigned NOT NULL DEFAULT '0',
  `Saved` tinyint(4) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `LoginId` (`LoginId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.dashboardmessages
CREATE TABLE IF NOT EXISTS `dashboardmessages` (
  `Id` char(36) NOT NULL,
  `Title` varchar(150) NOT NULL,
  `Text` text NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.errors
CREATE TABLE IF NOT EXISTS `errors` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Message` varchar(500) NOT NULL,
  `Type` varchar(30) NOT NULL,
  `Details` text NOT NULL,
  `Server` varchar(30) NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.impersonationlinks
CREATE TABLE IF NOT EXISTS `impersonationlinks` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `RepLoginId` char(36) NOT NULL,
  `RepName` varchar(50) NOT NULL,
  `IsUsed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ImpersonationLinks_LoginId` (`LoginId`),
  CONSTRAINT `impersonationlinks_ibfk_1` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.logins
CREATE TABLE IF NOT EXISTS `logins` (
  `Id` char(36) NOT NULL,
  `EmailAddress` varchar(100) NOT NULL,
  `PasswordHash` char(88) DEFAULT NULL,
  `PasswordSalt` char(20) DEFAULT NULL,
  `SignatureHash` char(88) DEFAULT NULL,
  `SignatureSalt` char(20) DEFAULT NULL,
  `DisplayName` varchar(50) NOT NULL,
  `Role` varchar(50) NOT NULL,
  `IsAxxessAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsAxxessSupport` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsActive` tinyint(1) unsigned NOT NULL,
  `IsLocked` tinyint(1) unsigned NOT NULL,
  `LastLoginDate` datetime NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `EmailAddress` (`EmailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.notes
CREATE TABLE IF NOT EXISTS `notes` (
  `Id` char(36) NOT NULL,
  `CompanyId` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `Comments` text NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Notes_CompanyId` (`CompanyId`),
  KEY `FK_Notes_LoginId` (`LoginId`),
  CONSTRAINT `FK_Notes_CompanyId` FOREIGN KEY (`CompanyId`) REFERENCES `companies` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Notes_LoginId` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.systemmessages
CREATE TABLE IF NOT EXISTS `systemmessages` (
  `Id` char(36) NOT NULL,
  `Subject` varchar(150) NOT NULL,
  `Body` text NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.therapyemployors
CREATE TABLE IF NOT EXISTS `therapyemployors` (
  `Id` char(36) NOT NULL DEFAULT '',
  `TherapyId` char(50) DEFAULT NULL,
  `AgencyId` char(50) DEFAULT NULL,
  `AgencyName` varchar(250) DEFAULT NULL,
  `State` char(2) DEFAULT NULL,
  `Comments` varchar(250) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `IsDeprecated` int(11) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.userloginagencies
CREATE TABLE IF NOT EXISTS `userloginagencies` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `LoginId` char(36) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `IsDeprecated` tinyint(1) DEFAULT NULL,
  `Status` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- Data exporting was unselected.


-- Dumping database structure for axxessforum
CREATE DATABASE IF NOT EXISTS `axxessforum` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `axxessforum`;


-- Dumping structure for table axxessforum.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedById` char(36) NOT NULL,
  `LastPostBy` varchar(50) NOT NULL,
  `LastPostById` char(36) NOT NULL,
  `LastPostDate` datetime NOT NULL,
  `Title` varchar(250) NOT NULL,
  `Body` longtext NOT NULL,
  `ViewCount` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessforum.replies
CREATE TABLE IF NOT EXISTS `replies` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `PostId` int(10) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedById` char(36) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `Body` longtext NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for axxesslogs
CREATE DATABASE IF NOT EXISTS `axxesslogs` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `axxesslogs`;


-- Dumping structure for table axxesslogs.appaudits
CREATE TABLE IF NOT EXISTS `appaudits` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `LogDomain` varchar(50) NOT NULL DEFAULT 'Patient',
  `DomainId` char(36) DEFAULT NULL,
  `EntityId` varchar(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `LogType` varchar(50) DEFAULT NULL,
  `Action` varchar(50) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Modified` date DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslogs.supportappaudits
CREATE TABLE IF NOT EXISTS `supportappaudits` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `LogDomain` varchar(50) NOT NULL DEFAULT 'Agency',
  `DomainId` char(36) DEFAULT NULL,
  `EntityId` varchar(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `LogType` varchar(50) DEFAULT NULL,
  `Action` varchar(50) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Modified` date DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslogs.taskaudits
CREATE TABLE IF NOT EXISTS `taskaudits` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EntityId` char(36) NOT NULL,
  `DisciplineTaskId` tinyint(3) unsigned NOT NULL,
  `Log` mediumtext NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_TaskAudit_UserId` (`EntityId`),
  KEY `FK_TaskAudit_AgencyId` (`AgencyId`),
  KEY `FK_TaskAudit_PatientId` (`PatientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for axxesslookup
CREATE DATABASE IF NOT EXISTS `axxesslookup` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `axxesslookup`;


-- Dumping structure for table axxesslookup.admissionsources
CREATE TABLE IF NOT EXISTS `admissionsources` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Description` varchar(100) NOT NULL,
  `Code` char(2) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.americanstates
CREATE TABLE IF NOT EXISTS `americanstates` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Code` char(2) NOT NULL,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.axxesssubmitterinfos
CREATE TABLE IF NOT EXISTS `axxesssubmitterinfos` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `SubmitterId` varchar(50) DEFAULT NULL,
  `SubmitterName` varchar(50) DEFAULT NULL,
  `Phone` varchar(10) DEFAULT NULL,
  `Fax` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.cbsacodes
CREATE TABLE IF NOT EXISTS `cbsacodes` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Zip` varchar(5) NOT NULL,
  `CityName` varchar(50) DEFAULT NULL,
  `CountyCode` varchar(5) DEFAULT NULL,
  `CountyTitle` varchar(50) DEFAULT NULL,
  `StateCode` varchar(2) NOT NULL,
  `StateName` varchar(50) NOT NULL,
  `CBSA` varchar(5) NOT NULL,
  `WITwoSeven` double unsigned NOT NULL,
  `WITwoEight` double unsigned NOT NULL,
  `WITwoNine` double unsigned NOT NULL,
  `WITwoTen` double unsigned NOT NULL,
  `WITwoEleven` double unsigned NOT NULL,
  `WITwoTwelve` double unsigned NOT NULL,
  `WITwoThirteen` double unsigned NOT NULL,
  `WITwoFourteen` double unsigned NOT NULL,
  `Variance` double NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Zip` (`Zip`),
  KEY `CountyCode` (`CountyCode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.databases
CREATE TABLE IF NOT EXISTS `databases` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.dbtables
CREATE TABLE IF NOT EXISTS `dbtables` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `OrderNumber` int(11) DEFAULT NULL,
  `DatabaseId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.diagnosiscodes
CREATE TABLE IF NOT EXISTS `diagnosiscodes` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` varchar(25) NOT NULL,
  `LongDescription` varchar(250) NOT NULL,
  `ShortDescription` varchar(250) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Code` (`Code`),
  FULLTEXT KEY `LongDescription` (`LongDescription`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.disciplinetasks
CREATE TABLE IF NOT EXISTS `disciplinetasks` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Task` varchar(50) DEFAULT NULL,
  `Discipline` varchar(50) DEFAULT NULL,
  `DisciplineId` int(11) DEFAULT '0',
  `IsBillable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsPayable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsMultiple` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsTypeChangeable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `RevenueCode` varchar(10) DEFAULT NULL,
  `GCode` varchar(10) DEFAULT NULL,
  `Unit` int(10) unsigned DEFAULT NULL,
  `Rate` double unsigned DEFAULT NULL,
  `Version` int(10) unsigned NOT NULL DEFAULT '0',
  `DefaultStatus` int(10) unsigned NOT NULL DEFAULT '410',
  `IsUsedInDropDown` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `Table` int(10) unsigned NOT NULL DEFAULT '400',
  `NeedOrderNumber` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `IsPhysicianOrder` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `NeedPhysician` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `IsTimeInAndOutExist` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.ethnicraces
CREATE TABLE IF NOT EXISTS `ethnicraces` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Description` varchar(150) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.hippsandhhrgs
CREATE TABLE IF NOT EXISTS `hippsandhhrgs` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `HHRG` varchar(6) NOT NULL,
  `HIPPS` varchar(5) NOT NULL,
  `HHRGWeight` double NOT NULL,
  `Time` date NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.insurances
CREATE TABLE IF NOT EXISTS `insurances` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.medicareadvantagecontractors
CREATE TABLE IF NOT EXISTS `medicareadvantagecontractors` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LegalEntityName` varchar(150) NOT NULL,
  `OrganizationMarketingName` varchar(150) NOT NULL,
  `Contract Number` varchar(20) NOT NULL,
  `OrganizationType` varchar(100) NOT NULL,
  `PlanType` varchar(100) NOT NULL,
  `ContractEffectiveDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TaxStatus` varchar(100) NOT NULL,
  `ParentOrganization` varchar(100) NOT NULL,
  `CMSRegionResponsible` varchar(150) NOT NULL,
  `Enrollment` varchar(50) NOT NULL,
  `LegalEntityStreet` varchar(150) NOT NULL,
  `LegalEntityStreet2` varchar(150) NOT NULL,
  `LegalEntityCity` varchar(150) NOT NULL,
  `LegalEntityStateCode` char(2) NOT NULL,
  `LegalEntityZip` char(9) NOT NULL,
  `DirectoryContactTitle` varchar(150) NOT NULL,
  `DirectoryContactLastName` varchar(150) NOT NULL,
  `DirectoryContactFirstName` varchar(150) NOT NULL,
  `DirectoryContactMiddleInitial` char(1) DEFAULT NULL,
  `DirectoryContactPhone` varchar(20) NOT NULL,
  `DirectoryContactPhoneExt` varchar(20) DEFAULT NULL,
  `DirectoryContactFax` varchar(20) DEFAULT NULL,
  `DirectoryContactEmail` varchar(150) DEFAULT NULL,
  `DirectoryContactStreetAddress` varchar(150) DEFAULT NULL,
  `DirectoryContactStreetAddress2` varchar(150) DEFAULT NULL,
  `DirectoryContactCity` varchar(150) DEFAULT NULL,
  `DirectoryContactState` char(2) DEFAULT NULL,
  `DirectoryContactZip` char(9) DEFAULT NULL,
  `DirectoryContactLastUpdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.medicareproviders
CREATE TABLE IF NOT EXISTS `medicareproviders` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProviderNumber` varchar(10) NOT NULL,
  `Name` varchar(150) NOT NULL,
  `Address` varchar(150) NOT NULL,
  `City` varchar(150) NOT NULL,
  `StateCode` char(2) NOT NULL,
  `ZipCode` varchar(5) NOT NULL,
  `Phone` varchar(10) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProviderNumber` (`ProviderNumber`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.medicarerates
CREATE TABLE IF NOT EXISTS `medicarerates` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Discipline` varchar(100) NOT NULL,
  `PerVisitRate` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.medicationroutes
CREATE TABLE IF NOT EXISTS `medicationroutes` (
  `Id` smallint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ShortName` varchar(10) DEFAULT NULL,
  `LongName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.npis
CREATE TABLE IF NOT EXISTS `npis` (
  `Id` varchar(10) NOT NULL DEFAULT '',
  `EntityTypeCode` smallint(10) unsigned DEFAULT NULL,
  `ProviderFirstName` varchar(20) DEFAULT NULL,
  `ProviderMiddleName` varchar(20) DEFAULT NULL,
  `ProviderLastName` varchar(35) DEFAULT NULL,
  `ProviderOrganizationName` varchar(70) DEFAULT NULL,
  `ProviderCredentialText` varchar(20) DEFAULT NULL,
  `ProviderFirstLineBusinessMailingAddress` varchar(55) DEFAULT NULL,
  `ProviderSecondLineBusinessMailingAddress` varchar(55) DEFAULT NULL,
  `ProviderBusinessMailingAddressCityName` varchar(40) DEFAULT NULL,
  `ProviderBusinessMailingAddressStateName` varchar(40) DEFAULT NULL,
  `ProviderBusinessMailingAddressPostalCode` varchar(20) DEFAULT NULL,
  `ProviderBusinessMailingAddressCountryCode` varchar(2) DEFAULT NULL,
  `ProviderBusinessMailingAddressTelephoneNumber` varchar(20) DEFAULT NULL,
  `ProviderBusinessMailingAddressFaxNumber` varchar(20) DEFAULT NULL,
  `ProviderFirstLineBusinessPracticeLocationAddress` varchar(55) DEFAULT NULL,
  `ProviderSecondLineBusinessPracticeLocationAddress` varchar(55) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressCityName` varchar(40) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressStateName` varchar(40) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressPostalCode` varchar(20) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressCountryCode` varchar(2) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressTelephoneNumber` varchar(20) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressFaxNumber` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.npis_copy
CREATE TABLE IF NOT EXISTS `npis_copy` (
  `Id` varchar(10) NOT NULL DEFAULT '',
  `EntityTypeCode` smallint(10) unsigned DEFAULT NULL,
  `ProviderFirstName` varchar(20) DEFAULT NULL,
  `ProviderMiddleName` varchar(20) DEFAULT NULL,
  `ProviderLastName` varchar(35) DEFAULT NULL,
  `ProviderOrganizationName` varchar(70) DEFAULT NULL,
  `ProviderCredentialText` varchar(20) DEFAULT NULL,
  `ProviderFirstLineBusinessMailingAddress` varchar(55) DEFAULT NULL,
  `ProviderSecondLineBusinessMailingAddress` varchar(55) DEFAULT NULL,
  `ProviderBusinessMailingAddressCityName` varchar(40) DEFAULT NULL,
  `ProviderBusinessMailingAddressStateName` varchar(40) DEFAULT NULL,
  `ProviderBusinessMailingAddressPostalCode` varchar(20) DEFAULT NULL,
  `ProviderBusinessMailingAddressCountryCode` varchar(2) DEFAULT NULL,
  `ProviderBusinessMailingAddressTelephoneNumber` varchar(20) DEFAULT NULL,
  `ProviderBusinessMailingAddressFaxNumber` varchar(20) DEFAULT NULL,
  `ProviderFirstLineBusinessPracticeLocationAddress` varchar(55) DEFAULT NULL,
  `ProviderSecondLineBusinessPracticeLocationAddress` varchar(55) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressCityName` varchar(40) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressStateName` varchar(40) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressPostalCode` varchar(20) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressCountryCode` varchar(2) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressTelephoneNumber` varchar(20) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressFaxNumber` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.npis_old
CREATE TABLE IF NOT EXISTS `npis_old` (
  `Id` varchar(10) NOT NULL DEFAULT '',
  `EntityTypeCode` smallint(10) unsigned DEFAULT NULL,
  `ProviderFirstName` varchar(20) DEFAULT NULL,
  `ProviderMiddleName` varchar(20) DEFAULT NULL,
  `ProviderLastName` varchar(35) DEFAULT NULL,
  `ProviderOrganizationName` varchar(70) DEFAULT NULL,
  `ProviderCredentialText` varchar(20) DEFAULT NULL,
  `ProviderFirstLineBusinessMailingAddress` varchar(55) DEFAULT NULL,
  `ProviderSecondLineBusinessMailingAddress` varchar(55) DEFAULT NULL,
  `ProviderBusinessMailingAddressCityName` varchar(40) DEFAULT NULL,
  `ProviderBusinessMailingAddressStateName` varchar(40) DEFAULT NULL,
  `ProviderBusinessMailingAddressPostalCode` varchar(20) DEFAULT NULL,
  `ProviderBusinessMailingAddressCountryCode` varchar(2) DEFAULT NULL,
  `ProviderBusinessMailingAddressTelephoneNumber` varchar(20) DEFAULT NULL,
  `ProviderBusinessMailingAddressFaxNumber` varchar(20) DEFAULT NULL,
  `ProviderFirstLineBusinessPracticeLocationAddress` varchar(55) DEFAULT NULL,
  `ProviderSecondLineBusinessPracticeLocationAddress` varchar(55) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressCityName` varchar(40) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressStateName` varchar(40) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressPostalCode` varchar(20) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressCountryCode` varchar(2) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressTelephoneNumber` varchar(20) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressFaxNumber` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.oasisbackups
CREATE TABLE IF NOT EXISTS `oasisbackups` (
  `Id` char(36) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `AssessmentId` char(36) DEFAULT NULL,
  `Data` text,
  `Created` datetime DEFAULT NULL,
  KEY `AssessmentId` (`AssessmentId`),
  KEY `Created` (`Created`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.oasisguides
CREATE TABLE IF NOT EXISTS `oasisguides` (
  `Id` char(5) NOT NULL,
  `ItemIntent` mediumtext NOT NULL,
  `Response` mediumtext NOT NULL,
  `DataSources` mediumtext NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.paymentsources
CREATE TABLE IF NOT EXISTS `paymentsources` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(150) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.pecosphysicians
CREATE TABLE IF NOT EXISTS `pecosphysicians` (
  `Id` varchar(10) NOT NULL,
  `Last` varchar(30) NOT NULL,
  `First` varchar(30) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.pecosphysicians_copy
CREATE TABLE IF NOT EXISTS `pecosphysicians_copy` (
  `Id` varchar(10) NOT NULL,
  `Last` varchar(30) NOT NULL,
  `First` varchar(30) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.pendingphysicians
CREATE TABLE IF NOT EXISTS `pendingphysicians` (
  `Id` varchar(10) NOT NULL,
  `Last` varchar(30) NOT NULL,
  `First` varchar(30) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.ppsstandards
CREATE TABLE IF NOT EXISTS `ppsstandards` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Time` date NOT NULL,
  `UrbanRate` double unsigned NOT NULL DEFAULT '0',
  `RuralRate` double unsigned NOT NULL DEFAULT '0',
  `Labor` double unsigned NOT NULL DEFAULT '0',
  `NonLabor` double unsigned NOT NULL DEFAULT '0',
  `S` double unsigned NOT NULL DEFAULT '0',
  `RuralS` double unsigned NOT NULL DEFAULT '0',
  `T` double unsigned NOT NULL DEFAULT '0',
  `RuralT` double unsigned NOT NULL DEFAULT '0',
  `U` double unsigned NOT NULL DEFAULT '0',
  `RuralU` double unsigned NOT NULL DEFAULT '0',
  `V` double unsigned NOT NULL DEFAULT '0',
  `RuralV` double unsigned NOT NULL DEFAULT '0',
  `W` double unsigned NOT NULL DEFAULT '0',
  `RuralW` double unsigned NOT NULL DEFAULT '0',
  `X` double unsigned NOT NULL DEFAULT '0',
  `RuralX` double unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Year` (`Time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.privatedutydisciplinetasks
CREATE TABLE IF NOT EXISTS `privatedutydisciplinetasks` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Task` varchar(50) DEFAULT NULL,
  `Discipline` varchar(50) DEFAULT NULL,
  `IsBillable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsMultiple` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsCreatable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsTypeChangeable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `RevenueCode` varchar(10) DEFAULT NULL,
  `GCode` varchar(10) DEFAULT NULL,
  `Unit` int(10) unsigned DEFAULT NULL,
  `Rate` double unsigned DEFAULT NULL,
  `Version` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.procedurecodes
CREATE TABLE IF NOT EXISTS `procedurecodes` (
  `Id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Code` varchar(25) NOT NULL,
  `LongDescription` varchar(250) NOT NULL,
  `ShortDescription` varchar(250) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Code` (`Code`),
  FULLTEXT KEY `LongDescription` (`LongDescription`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.referralsources
CREATE TABLE IF NOT EXISTS `referralsources` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.relationships
CREATE TABLE IF NOT EXISTS `relationships` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Code` char(2) NOT NULL,
  `Description` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.ruralcbsacodes
CREATE TABLE IF NOT EXISTS `ruralcbsacodes` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `StateName` varchar(50) NOT NULL,
  `StateCode` char(2) NOT NULL,
  `CBSA` varchar(5) NOT NULL,
  `WITwoSeven` double unsigned NOT NULL,
  `WITwoEight` double unsigned NOT NULL,
  `WITwoNine` double unsigned NOT NULL,
  `WITwoTen` double unsigned NOT NULL,
  `WITwoEleven` double unsigned NOT NULL,
  `WITwoTwelve` double unsigned NOT NULL,
  `WITwoThirteen` double unsigned NOT NULL,
  `WITwoFourteen` double unsigned NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.submissionbodyformats
CREATE TABLE IF NOT EXISTS `submissionbodyformats` (
  `Id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(255) DEFAULT NULL,
  `Length` float unsigned DEFAULT NULL,
  `Start` float unsigned DEFAULT NULL,
  `End` float unsigned DEFAULT NULL,
  `RFA01` float unsigned DEFAULT NULL,
  `RFA02` float unsigned DEFAULT NULL,
  `RFA03` float unsigned DEFAULT NULL,
  `RFA04` float unsigned DEFAULT NULL,
  `RFA05` float unsigned DEFAULT NULL,
  `RFA06` float unsigned DEFAULT NULL,
  `RFA07` float unsigned DEFAULT NULL,
  `RFA08` float unsigned DEFAULT NULL,
  `RFA09` float unsigned DEFAULT NULL,
  `RFA10` float unsigned DEFAULT NULL,
  `RFA_REQ` varchar(255) DEFAULT NULL,
  `RFA_BLANK` varchar(255) DEFAULT NULL,
  `ElementName` varchar(255) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  `IsIgnorable` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.submissionfooterformats
CREATE TABLE IF NOT EXISTS `submissionfooterformats` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.submissionheaderformats
CREATE TABLE IF NOT EXISTS `submissionheaderformats` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.submissioninactivebodyformats
CREATE TABLE IF NOT EXISTS `submissioninactivebodyformats` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.supplies
CREATE TABLE IF NOT EXISTS `supplies` (
  `Id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Code` char(5) NOT NULL,
  `Description` varchar(350) NOT NULL,
  `CategoryId` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.supplycategories
CREATE TABLE IF NOT EXISTS `supplycategories` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Description` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.taxonomies
CREATE TABLE IF NOT EXISTS `taxonomies` (
  `Id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `TaxoId` varchar(12) DEFAULT NULL,
  `TaxoClass` varchar(100) DEFAULT NULL,
  `TaxoSubclass` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `TaxoId` (`TaxoId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.therapydisciplinetasks
CREATE TABLE IF NOT EXISTS `therapydisciplinetasks` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Task` varchar(50) DEFAULT NULL,
  `Discipline` varchar(50) DEFAULT NULL,
  `IsBillable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsMultiple` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `RevenueCode` varchar(10) DEFAULT NULL,
  `GCode` varchar(10) DEFAULT NULL,
  `Unit` int(10) unsigned DEFAULT NULL,
  `Rate` double unsigned DEFAULT NULL,
  `Version` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.zipcodes
CREATE TABLE IF NOT EXISTS `zipcodes` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` char(5) DEFAULT NULL,
  `StateCode` char(2) DEFAULT NULL,
  `StateName` varchar(50) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `CBSA` char(5) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for axxessmembership
CREATE DATABASE IF NOT EXISTS `axxessmembership` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `axxessmembership`;


-- Dumping structure for table axxessmembership.ddecredentials
CREATE TABLE IF NOT EXISTS `ddecredentials` (
  `Id` char(36) NOT NULL,
  `UserId` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Created` date NOT NULL DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.errors
CREATE TABLE IF NOT EXISTS `errors` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Message` varchar(500) NOT NULL,
  `Type` varchar(30) NOT NULL,
  `Details` text NOT NULL,
  `Server` varchar(30) NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.impersonationlinks
CREATE TABLE IF NOT EXISTS `impersonationlinks` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `RepLoginId` char(36) NOT NULL,
  `RepName` varchar(50) NOT NULL,
  `IsUsed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ImpersonationLinks_LoginId` (`LoginId`),
  CONSTRAINT `FK_ImpersonationLinks_LoginId` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.logins
CREATE TABLE IF NOT EXISTS `logins` (
  `Id` char(36) NOT NULL,
  `EmailAddress` varchar(100) NOT NULL,
  `PasswordHash` char(88) DEFAULT NULL,
  `PasswordSalt` char(20) DEFAULT NULL,
  `SignatureHash` char(88) DEFAULT NULL,
  `SignatureSalt` char(20) DEFAULT NULL,
  `DisplayName` varchar(50) NOT NULL,
  `Role` varchar(50) NOT NULL,
  `IsAxxessAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsAxxessSupport` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsActive` tinyint(1) unsigned NOT NULL,
  `IsLocked` tinyint(1) unsigned NOT NULL,
  `LastLoginDate` datetime NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `EmailAddress` (`EmailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.sessiontokens
CREATE TABLE IF NOT EXISTS `sessiontokens` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `TokenId` varchar(150) NOT NULL,
  `DeviceId` varchar(150) NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for axxesssystemdata
CREATE DATABASE IF NOT EXISTS `axxesssystemdata` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `axxesssystemdata`;


-- Dumping structure for table axxesssystemdata.dashboardmessages
CREATE TABLE IF NOT EXISTS `dashboardmessages` (
  `Id` char(36) NOT NULL,
  `Title` varchar(150) NOT NULL,
  `Text` text NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesssystemdata.systemmessages
CREATE TABLE IF NOT EXISTS `systemmessages` (
  `Id` char(36) NOT NULL,
  `Subject` varchar(150) NOT NULL,
  `Body` text NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping database structure for claim_payor
CREATE DATABASE IF NOT EXISTS `claim_payor` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `claim_payor`;


-- Dumping structure for table claim_payor.availity
CREATE TABLE IF NOT EXISTS `availity` (
  `LastUpdate` char(10) NOT NULL,
  `PayerID` char(10) NOT NULL,
  `PayerName` char(255) NOT NULL,
  `GovernmentPayer` char(2) NOT NULL DEFAULT '0',
  `ClaimEnrollRequired` char(2) NOT NULL DEFAULT '0',
  `Remit835` char(2) NOT NULL DEFAULT '0',
  `NPIOption` char(2) NOT NULL DEFAULT '0',
  `ProfessionalClaim837EDI` char(2) NOT NULL DEFAULT '0',
  `ProfessionalClaim837B2B` char(2) NOT NULL DEFAULT '0',
  `InstitutionalClaim837EDI` char(2) NOT NULL DEFAULT '0',
  `InstitutionalClaim837B2B` char(2) NOT NULL DEFAULT '0',
  `EligibilityBenefits270EDI` char(2) NOT NULL DEFAULT '0',
  `EligibilityBenefits270B2B` char(2) NOT NULL DEFAULT '0',
  `ClaimStatus276EDI` char(2) NOT NULL DEFAULT '0',
  `ClaimStatus276B2B` char(2) NOT NULL DEFAULT '0',
  `AuthReferral278EDI` char(2) NOT NULL DEFAULT '0',
  `AuthReferral278B2B` char(2) NOT NULL DEFAULT '0',
  `EDIRequirements` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping database structure for customermanagement
CREATE DATABASE IF NOT EXISTS `customermanagement` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `customermanagement`;


-- Dumping structure for table customermanagement.customernotes
CREATE TABLE IF NOT EXISTS `customernotes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `Comments` text NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Recipients` text,
  `NoteType` tinyint(3) unsigned NOT NULL,
  `Summary` varchar(80) NOT NULL,
  `CallerId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IssueId` int(10) unsigned DEFAULT NULL,
  `AttachmentId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsCompleted` tinyint(1) unsigned NOT NULL,
  `DevUpdates` text,
  `AdditionalInformation` text,
  `ModifiedBy` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `Status` tinyint(100) unsigned NOT NULL DEFAULT '0',
  `Saved` tinyint(4) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `LoginId` (`LoginId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.ddecredentials
CREATE TABLE IF NOT EXISTS `ddecredentials` (
  `Id` char(36) NOT NULL,
  `UserId` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Created` date NOT NULL DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.internalusers
CREATE TABLE IF NOT EXISTS `internalusers` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `LoginId` char(36) DEFAULT '',
  `Permissions` text,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `LoginId` (`LoginId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.logins
CREATE TABLE IF NOT EXISTS `logins` (
  `Id` char(36) NOT NULL,
  `EmailAddress` varchar(100) NOT NULL,
  `PasswordHash` char(88) DEFAULT NULL,
  `PasswordSalt` char(20) DEFAULT NULL,
  `SignatureHash` char(88) DEFAULT NULL,
  `SignatureSalt` char(20) DEFAULT NULL,
  `DisplayName` varchar(50) NOT NULL,
  `Role` varchar(50) NOT NULL,
  `IsAxxessAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsAxxessSupport` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsActive` tinyint(1) unsigned NOT NULL,
  `IsLocked` tinyint(1) unsigned NOT NULL,
  `LastLoginDate` datetime NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `EmailAddress` (`EmailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `Id` varchar(100) NOT NULL DEFAULT '',
  `Name` varchar(100) DEFAULT NULL,
  `MenuName` varchar(100) DEFAULT NULL,
  `Url` varchar(100) DEFAULT NULL,
  `Onload` varchar(100) DEFAULT NULL,
  `ParentMenus` varchar(100) DEFAULT NULL,
  `Category` int(11) DEFAULT NULL,
  `OtherCategory` varchar(50) DEFAULT '',
  `Action` int(11) DEFAULT NULL,
  `StyleOptions` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.permissionactions
CREATE TABLE IF NOT EXISTS `permissionactions` (
  `Id` int(10) NOT NULL DEFAULT '0',
  `Name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.permissionactivitylinks
CREATE TABLE IF NOT EXISTS `permissionactivitylinks` (
  `Id` int(10) NOT NULL DEFAULT '0',
  `PermissionId` int(10) NOT NULL DEFAULT '0',
  `ActionId` int(10) NOT NULL DEFAULT '0',
  `Name` varchar(50) DEFAULT NULL,
  `Description` text,
  PRIMARY KEY (`Id`),
  KEY `PermissionId` (`PermissionId`),
  KEY `ActionId` (`ActionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.permissioncategories
CREATE TABLE IF NOT EXISTS `permissioncategories` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.permissionsubcategories
CREATE TABLE IF NOT EXISTS `permissionsubcategories` (
  `Id` int(10) unsigned NOT NULL DEFAULT '0',
  `CategoryId` int(10) unsigned NOT NULL DEFAULT '0',
  `Name` varchar(50) DEFAULT NULL,
  `Description` text,
  `IsDeprecated` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `IsSubCategory` tinyint(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `CategoryId` (`CategoryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.permissiontemplates
CREATE TABLE IF NOT EXISTS `permissiontemplates` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Permissions` text,
  `IsDeprecated` tinyint(1) unsigned DEFAULT '0',
  `Modified` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for lexi-data
CREATE DATABASE IF NOT EXISTS `lexi-data` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `lexi-data`;


-- Dumping structure for table lexi-data.ALLERGY_CLASS
CREATE TABLE IF NOT EXISTS `ALLERGY_CLASS` (
  `ALLERGY_CLASS_ID` int(11) NOT NULL,
  `DISPLAY_NAME` varchar(60) NOT NULL,
  `SEARCH_NAME` varchar(60) NOT NULL,
  `METAPHONE_1` varchar(60) NOT NULL,
  `METAPHONE_2` varchar(60) NOT NULL,
  `PLURAL_NAME` varchar(60) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ALLERGY_CLASS_ID`),
  UNIQUE KEY `SEARCH_NAME` (`SEARCH_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ALLERGY_CLASS_GENDRUG
CREATE TABLE IF NOT EXISTS `ALLERGY_CLASS_GENDRUG` (
  `ALLERGY_CLASS_ID` int(11) NOT NULL,
  `DRUG_ID` varchar(10) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ALLERGY_CLASS_ID`,`DRUG_ID`),
  KEY `IDX_ALLERGY_CLASS_GENDRUG` (`DRUG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ALLERGY_CLASS_TEMPLATE
CREATE TABLE IF NOT EXISTS `ALLERGY_CLASS_TEMPLATE` (
  `ALLERGY_CLASS_ID` int(11) NOT NULL,
  `MATCH_TYPE_ID` int(11) NOT NULL,
  `ALLERGY_TEMPLATE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ALLERGY_CLASS_ID`,`MATCH_TYPE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ALLERGY_CLASS_XRGROUP
CREATE TABLE IF NOT EXISTS `ALLERGY_CLASS_XRGROUP` (
  `ALLERGY_CLASS_ID` int(11) NOT NULL,
  `ALLERGY_XRGROUP_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ALLERGY_CLASS_ID`,`ALLERGY_XRGROUP_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ALLERGY_MATCH_TYPE
CREATE TABLE IF NOT EXISTS `ALLERGY_MATCH_TYPE` (
  `MATCH_TYPE_ID` int(11) NOT NULL,
  `MATCH_TYPE_DESCRIPTION` varchar(255) NOT NULL,
  `ALLERGY_TEMPLATE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MATCH_TYPE_ID`),
  UNIQUE KEY `MATCH_TYPE_DESCRIPTION` (`MATCH_TYPE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ALLERGY_TEMPLATE
CREATE TABLE IF NOT EXISTS `ALLERGY_TEMPLATE` (
  `ALLERGY_TEMPLATE_ID` int(11) NOT NULL,
  `TEMPLATE_TEXT` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ALLERGY_TEMPLATE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ALLERGY_XRGROUP
CREATE TABLE IF NOT EXISTS `ALLERGY_XRGROUP` (
  `ALLERGY_XRGROUP_ID` int(11) NOT NULL,
  `XRGROUP_DESCRIPTION` varchar(60) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ALLERGY_XRGROUP_ID`),
  UNIQUE KEY `XRGROUP_DESCRIPTION` (`XRGROUP_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ALLERGY_XRGROUP_REF
CREATE TABLE IF NOT EXISTS `ALLERGY_XRGROUP_REF` (
  `ALLERGY_XRGROUP_ID` int(11) NOT NULL,
  `REFERENCE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ALLERGY_XRGROUP_ID`,`REFERENCE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CODING_SYSTEM
CREATE TABLE IF NOT EXISTS `CODING_SYSTEM` (
  `CODING_SYSTEM_ID` int(11) NOT NULL,
  `CODING_SYSTEM_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CODING_SYSTEM_ID`),
  UNIQUE KEY `CODING_SYSTEM_DESCRIPTION` (`CODING_SYSTEM_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CONDINT_GENDRUG
CREATE TABLE IF NOT EXISTS `CONDINT_GENDRUG` (
  `DRUG_ID` varchar(10) NOT NULL,
  `CONDITION_ID` int(11) NOT NULL,
  `CONDITION_INTERACTION_ID` int(11) NOT NULL,
  `PLAUSIBILITY_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`CONDITION_ID`,`CONDITION_INTERACTION_ID`),
  KEY `IDX_CONDINT_GENDRUG` (`CONDITION_INTERACTION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CONDINT_GENDRUG_LOCAL
CREATE TABLE IF NOT EXISTS `CONDINT_GENDRUG_LOCAL` (
  `DRUG_ID` varchar(50) NOT NULL,
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `CONDITION_ID` int(11) NOT NULL,
  `CONDITION_INTERACTION_ID` int(11) NOT NULL,
  `PLAUSIBILITY_ID` int(11) DEFAULT NULL,
  `SUPPORT_URL` varchar(255) DEFAULT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  PRIMARY KEY (`DRUG_ID`,`LOCAL_DATASET_ID`,`CONDITION_ID`,`CONDITION_INTERACTION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CONDINT_INTERACTION
CREATE TABLE IF NOT EXISTS `CONDINT_INTERACTION` (
  `CONDITION_INTERACTION_ID` int(11) NOT NULL,
  `INTERACTION_TITLE` varchar(100) NOT NULL,
  `INTERACTION_DESCRIPTION` longtext NOT NULL,
  `COND_SEVERITY_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CONDITION_INTERACTION_ID`),
  UNIQUE KEY `INTERACTION_TITLE` (`INTERACTION_TITLE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CONDINT_INTERACTION_LOCAL
CREATE TABLE IF NOT EXISTS `CONDINT_INTERACTION_LOCAL` (
  `CONDITION_INTERACTION_ID` int(11) NOT NULL,
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `INTERACTION_TITLE` varchar(255) DEFAULT NULL,
  `INTERACTION_DESCRIPTION` varchar(255) DEFAULT NULL,
  `COND_SEVERITY_ID` int(11) DEFAULT NULL,
  `SUPPORT_URL` varchar(255) DEFAULT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  PRIMARY KEY (`CONDITION_INTERACTION_ID`,`LOCAL_DATASET_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CONDINT_PLAUSIBILITY
CREATE TABLE IF NOT EXISTS `CONDINT_PLAUSIBILITY` (
  `PLAUSIBILITY_ID` int(11) NOT NULL,
  `PLAUSIBILITY_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`PLAUSIBILITY_ID`),
  UNIQUE KEY `PLAUSIBILITY_DESCRIPTION` (`PLAUSIBILITY_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CONDINT_REF
CREATE TABLE IF NOT EXISTS `CONDINT_REF` (
  `CONDITION_INTERACTION_ID` int(11) NOT NULL,
  `REFERENCE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CONDITION_INTERACTION_ID`,`REFERENCE_ID`),
  KEY `IDX_CONDINT_REF` (`REFERENCE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CONDINT_SEVERITY
CREATE TABLE IF NOT EXISTS `CONDINT_SEVERITY` (
  `COND_SEVERITY_ID` int(11) NOT NULL,
  `SEVERITY_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`COND_SEVERITY_ID`),
  UNIQUE KEY `SEVERITY_DESCRIPTION` (`SEVERITY_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CONDITION_CODE
CREATE TABLE IF NOT EXISTS `CONDITION_CODE` (
  `CONDITION_ID` int(11) NOT NULL,
  `CODE` varchar(25) NOT NULL,
  `CODE_TERM` varchar(255) NOT NULL,
  `CODING_SYSTEM_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CONDITION_ID`,`CODE`,`CODING_SYSTEM_ID`),
  KEY `IDX_CONDITION_CODE_1` (`CONDITION_ID`),
  KEY `IDX_CONDITION_CODE_2` (`CODE`),
  KEY `IDX_CONDITION_CODE_3` (`CODE_TERM`),
  KEY `IDX_CONDITION_CODE_4` (`CODING_SYSTEM_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_ACTIVE_INGRED
CREATE TABLE IF NOT EXISTS `CORE_ACTIVE_INGRED` (
  `ACTIVE_INGRED_ID` int(11) NOT NULL,
  `ACTIVE_INGRED_NAME` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ACTIVE_INGRED_ID`),
  UNIQUE KEY `ACTIVE_INGRED_NAME` (`ACTIVE_INGRED_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_ALERT
CREATE TABLE IF NOT EXISTS `CORE_ALERT` (
  `DRUG_ID` varchar(10) NOT NULL,
  `IS_HIGH_ALERT` tinyint(4) NOT NULL,
  `IS_BEERS_DRUG` tinyint(4) NOT NULL,
  `HAS_BBW` tinyint(4) NOT NULL,
  `HAS_SPA` tinyint(4) NOT NULL,
  `HAS_MEDGUIDE` tinyint(4) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_BBW
CREATE TABLE IF NOT EXISTS `CORE_BBW` (
  `BBW_ID` int(11) NOT NULL,
  `BBW_SUMMARY` longtext,
  `BBW_DESCRIPTOR_ID` int(11) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`BBW_ID`),
  KEY `IDX_CORE_BBW` (`BBW_DESCRIPTOR_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_BBW_CONTEXT
CREATE TABLE IF NOT EXISTS `CORE_BBW_CONTEXT` (
  `BBW_CONTEXT_ID` int(11) NOT NULL,
  `DOSEFORM_ID` int(11) DEFAULT NULL,
  `DRUG_SYN_ID` int(11) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`BBW_CONTEXT_ID`),
  KEY `IDX_CORE_BBW_CONTEXT` (`DOSEFORM_ID`),
  KEY `IDX_CORE_BBW_CONTEXT_2` (`DRUG_SYN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_BBW_DESCRIPTOR
CREATE TABLE IF NOT EXISTS `CORE_BBW_DESCRIPTOR` (
  `BBW_DESCRIPTOR_ID` int(11) NOT NULL,
  `BBW_DESCRIPTOR` varchar(255) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`BBW_DESCRIPTOR_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_BBW_GENDRUG
CREATE TABLE IF NOT EXISTS `CORE_BBW_GENDRUG` (
  `DRUG_ID` varchar(10) NOT NULL,
  `BBW_CONTEXT_ID` int(11) NOT NULL,
  `BBW_ID` int(11) NOT NULL,
  `BBW_MESSAGE_ID` int(11) NOT NULL,
  `BBW_RECIPIENT_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`BBW_CONTEXT_ID`,`BBW_ID`,`BBW_MESSAGE_ID`,`BBW_RECIPIENT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_BBW_MESSAGE
CREATE TABLE IF NOT EXISTS `CORE_BBW_MESSAGE` (
  `BBW_MESSAGE_ID` int(11) NOT NULL,
  `BBW_MESSAGE` varchar(255) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`BBW_MESSAGE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_BBW_RECIPIENT
CREATE TABLE IF NOT EXISTS `CORE_BBW_RECIPIENT` (
  `BBW_RECIPIENT_ID` int(11) NOT NULL,
  `BBW_RECIPIENT` varchar(255) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`BBW_RECIPIENT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_CLASS_GENDRUG
CREATE TABLE IF NOT EXISTS `CORE_CLASS_GENDRUG` (
  `CLASS_ID` int(11) NOT NULL,
  `DRUG_ID` varchar(10) NOT NULL,
  `IS_DIRECT_LINK` tinyint(4) NOT NULL,
  `CLASS_SYSTEM_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CLASS_ID`,`CLASS_SYSTEM_ID`,`DRUG_ID`),
  KEY `IDX_CORE_CLASS_GENDRUG` (`DRUG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_CLASS_HIERARCHY
CREATE TABLE IF NOT EXISTS `CORE_CLASS_HIERARCHY` (
  `CLASS_ID` int(11) NOT NULL,
  `DISPLAY_NAME` varchar(255) NOT NULL,
  `SEARCH_NAME` varchar(255) NOT NULL,
  `METAPHONE_1` varchar(255) NOT NULL,
  `METAPHONE_2` varchar(255) NOT NULL,
  `PARENT_CLASS_ID` int(11) DEFAULT NULL,
  `HAS_CHILDREN` tinyint(4) NOT NULL,
  `IS_ACTIVE` tinyint(4) NOT NULL,
  `CLASS_SYSTEM_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CLASS_ID`,`CLASS_SYSTEM_ID`),
  KEY `IDX_CORE_CLASS_HIERARCHY` (`METAPHONE_1`),
  KEY `IDX_CORE_CLASS_HIERARCHY_2` (`SEARCH_NAME`),
  KEY `IDX_CORE_CLASS_HIERARCHY_3` (`METAPHONE_2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_CLASS_SYSTEM
CREATE TABLE IF NOT EXISTS `CORE_CLASS_SYSTEM` (
  `CLASS_SYSTEM_ID` int(11) NOT NULL,
  `CLASS_SYSTEM_NAME` varchar(100) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CLASS_SYSTEM_ID`),
  UNIQUE KEY `CLASS_SYSTEM_NAME` (`CLASS_SYSTEM_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_CLINICAL_ROUTE
CREATE TABLE IF NOT EXISTS `CORE_CLINICAL_ROUTE` (
  `CLINICAL_ROUTE_ID` int(11) NOT NULL,
  `CLINICAL_ROUTE_DESCRIPTION` varchar(100) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CLINICAL_ROUTE_ID`),
  UNIQUE KEY `CLINICAL_ROUTE_DESCRIPTION` (`CLINICAL_ROUTE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_CONDITION
CREATE TABLE IF NOT EXISTS `CORE_CONDITION` (
  `CONDITION_ID` int(11) NOT NULL,
  `CONDITION_DESCRIPTION` varchar(255) NOT NULL,
  `SEARCH_DESCRIPTION` varchar(255) NOT NULL,
  `METAPHONE_1` varchar(255) NOT NULL,
  `METAPHONE_2` varchar(255) NOT NULL,
  `HAS_CHILDREN` tinyint(4) NOT NULL,
  `HAS_DRUGS_TO_TREAT` tinyint(4) NOT NULL,
  `USED_IN_CONTRAINDICATIONS` tinyint(4) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CONDITION_ID`),
  UNIQUE KEY `CONDITION_DESCRIPTION` (`CONDITION_DESCRIPTION`),
  UNIQUE KEY `SEARCH_DESCRIPTION` (`SEARCH_DESCRIPTION`),
  KEY `IDX_CORE_CONDITION` (`METAPHONE_2`),
  KEY `IDX_CORE_CONDITION_2` (`METAPHONE_1`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_CONDITION_HIERARCHY
CREATE TABLE IF NOT EXISTS `CORE_CONDITION_HIERARCHY` (
  `CONDITION_ID` int(11) NOT NULL,
  `CHILD_CONDITION_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CONDITION_ID`,`CHILD_CONDITION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_DOSEFORM
CREATE TABLE IF NOT EXISTS `CORE_DOSEFORM` (
  `DOSEFORM_ID` int(11) NOT NULL,
  `DOSEFORM_DESCRIPTION` varchar(255) NOT NULL,
  `DOSEFORM_ABBREVIATION` varchar(30) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DOSEFORM_ID`),
  UNIQUE KEY `DOSEFORM_ABBREVIATION` (`DOSEFORM_ABBREVIATION`),
  UNIQUE KEY `DOSEFORM_DESCRIPTION` (`DOSEFORM_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_DRUG_INDICATOR
CREATE TABLE IF NOT EXISTS `CORE_DRUG_INDICATOR` (
  `DRUG_INDICATOR_ID` int(11) NOT NULL,
  `INDICATOR_DESCRIPTION` varchar(100) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_INDICATOR_ID`),
  UNIQUE KEY `INDICATOR_DESCRIPTION` (`INDICATOR_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_FREQUENCY
CREATE TABLE IF NOT EXISTS `CORE_FREQUENCY` (
  `FREQUENCY_ID` int(11) NOT NULL,
  `FREQUENCY_DESCRIPTION` varchar(255) NOT NULL,
  `FREQUENCY_ABBREVIATION` varchar(30) NOT NULL,
  `ADMIN_PER_DAY` decimal(10,5) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`FREQUENCY_ID`),
  UNIQUE KEY `FREQUENCY_DESCRIPTION` (`FREQUENCY_DESCRIPTION`),
  UNIQUE KEY `FREQUENCY_ABBREVIATION` (`FREQUENCY_ABBREVIATION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_GENDRUG
CREATE TABLE IF NOT EXISTS `CORE_GENDRUG` (
  `DRUG_ID` varchar(10) NOT NULL,
  `GENERIC_NAME` varchar(255) NOT NULL,
  `MANUFACTURER_GENERIC_NAME` varchar(255) DEFAULT NULL,
  `PREGNANCY_RISK_FACTOR` varchar(1) NOT NULL,
  `HALF_LIFE_HOURS` decimal(10,5) NOT NULL,
  `HALF_LIFE_IS_EMPIRICAL` tinyint(4) NOT NULL,
  `IS_INGREDIENT` tinyint(4) NOT NULL,
  `IS_PRODUCT` tinyint(4) NOT NULL,
  `RX_OTC_STATUS_CODE` varchar(6) DEFAULT NULL,
  `IS_ACTIVE` tinyint(4) NOT NULL,
  `OBSOLETE_DATE` date DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`),
  UNIQUE KEY `GENERIC_NAME` (`GENERIC_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_GENDRUG_INDICATORS
CREATE TABLE IF NOT EXISTS `CORE_GENDRUG_INDICATORS` (
  `DRUG_ID` varchar(10) NOT NULL,
  `DRUG_INDICATOR_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`DRUG_INDICATOR_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_GENDRUG_INGREDIENT
CREATE TABLE IF NOT EXISTS `CORE_GENDRUG_INGREDIENT` (
  `DRUG_ID` varchar(10) NOT NULL,
  `INGREDIENT_DRUG_ID` varchar(10) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`INGREDIENT_DRUG_ID`),
  KEY `IDX_CORE_GENDRUG_INGREDIENT` (`INGREDIENT_DRUG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_GENDRUG_SYNONYM
CREATE TABLE IF NOT EXISTS `CORE_GENDRUG_SYNONYM` (
  `DRUG_ID` varchar(10) NOT NULL,
  `DRUG_SYN_ID` int(11) NOT NULL,
  `SYNONYM_TYPE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`DRUG_SYN_ID`,`SYNONYM_TYPE_ID`),
  KEY `IDX_CORE_GENDRUG_SYNONYM` (`DRUG_SYN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_GENPRODUCT
CREATE TABLE IF NOT EXISTS `CORE_GENPRODUCT` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `GENERIC_PRODUCT_NAME` varchar(255) NOT NULL,
  `MANUFACT_GENERIC_PRODUCT_NAME` varchar(255) DEFAULT NULL,
  `DRUG_ID` varchar(10) NOT NULL,
  `ROUTE_ID` int(11) NOT NULL,
  `DOSEFORM_ID` int(11) NOT NULL,
  `STRENGTH_ID` int(11) NOT NULL,
  `CSA_CODE` varchar(1) NOT NULL,
  `RX_OTC_STATUS` varchar(6) DEFAULT NULL,
  `IS_ACTIVE` tinyint(4) NOT NULL,
  `OBSOLETE_DATE` date DEFAULT NULL,
  `JCODE` varchar(10) DEFAULT NULL,
  `JCODE_DESCRIPTION` varchar(50) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`),
  UNIQUE KEY `GENERIC_PRODUCT_NAME` (`GENERIC_PRODUCT_NAME`),
  KEY `IDX_CORE_GENPRODUCT` (`STRENGTH_ID`),
  KEY `IDX_CORE_GENPRODUCT_2` (`DRUG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_GENPRODUCT_INGREDIENT
CREATE TABLE IF NOT EXISTS `CORE_GENPRODUCT_INGREDIENT` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `ACTIVE_INGRED_ID` int(11) NOT NULL,
  `INGRED_STRENGTH_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`,`ACTIVE_INGRED_ID`,`INGRED_STRENGTH_ID`),
  KEY `IDX_CORE_GENPRODUCT_INGREDIENT` (`INGRED_STRENGTH_ID`),
  KEY `IDX_CORE_GENPRODUCT_INGREDIENT_2` (`ACTIVE_INGRED_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_GENPRODUCT_SYNONYM
CREATE TABLE IF NOT EXISTS `CORE_GENPRODUCT_SYNONYM` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `DRUG_SYN_ID` int(11) NOT NULL,
  `SYNONYM_TYPE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`,`DRUG_SYN_ID`,`SYNONYM_TYPE_ID`),
  KEY `IDX_CORE_GENPRODUCT_SYNONYM` (`DRUG_SYN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_INGREDIENT_STRENGTH
CREATE TABLE IF NOT EXISTS `CORE_INGREDIENT_STRENGTH` (
  `INGRED_STRENGTH_ID` int(11) NOT NULL,
  `NUMERATOR_AMOUNT` decimal(16,6) NOT NULL,
  `NUMERATOR_UNIT_ID` int(11) NOT NULL,
  `DENOMINATOR_AMOUNT` decimal(16,6) NOT NULL,
  `DENOMINATOR_UNIT_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`INGRED_STRENGTH_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_PRODUCTSTRENGTH
CREATE TABLE IF NOT EXISTS `CORE_PRODUCTSTRENGTH` (
  `STRENGTH_ID` int(11) NOT NULL,
  `STRENGTH_DESCRIPTION` varchar(255) NOT NULL,
  `CONCISE_STRENGTH_DESC` varchar(255) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`STRENGTH_ID`),
  UNIQUE KEY `STRENGTH_DESCRIPTION` (`STRENGTH_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_ROUTE
CREATE TABLE IF NOT EXISTS `CORE_ROUTE` (
  `ROUTE_ID` int(11) NOT NULL,
  `ROUTE_DESCRIPTION` varchar(255) NOT NULL,
  `ROUTE_ABBREVIATION` varchar(30) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ROUTE_ID`),
  UNIQUE KEY `ROUTE_ABBREVIATION` (`ROUTE_ABBREVIATION`),
  UNIQUE KEY `ROUTE_DESCRIPTION` (`ROUTE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_STATE_CSA
CREATE TABLE IF NOT EXISTS `CORE_STATE_CSA` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `STATE` varchar(10) NOT NULL,
  `CSA_CODE` varchar(1) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`,`STATE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_SYNONYM
CREATE TABLE IF NOT EXISTS `CORE_SYNONYM` (
  `DRUG_SYN_ID` int(11) NOT NULL,
  `DISPLAY_NAME` varchar(255) NOT NULL,
  `SEARCH_NAME` varchar(255) NOT NULL,
  `METAPHONE_1` varchar(255) DEFAULT NULL,
  `METAPHONE_2` varchar(255) DEFAULT NULL,
  `MANUFACTURER_DISPLAY_NAME` varchar(255) DEFAULT NULL,
  `MANUFACTURER_SEARCH_NAME` varchar(255) DEFAULT NULL,
  `MANUFACTURER_METAPHONE_1` varchar(255) DEFAULT NULL,
  `MANUFACTURER_METAPHONE_2` varchar(255) DEFAULT NULL,
  `IS_OBSOLETE` tinyint(4) NOT NULL,
  `DATE_OBSOLETE` date DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_SYN_ID`),
  KEY `IDX_CORE_SYNONYM` (`METAPHONE_2`),
  KEY `IDX_CORE_SYNONYM_2` (`METAPHONE_1`),
  KEY `IDX_CORE_SYNONYM_3` (`SEARCH_NAME`),
  KEY `IDX_CORE_SYNONYM_5` (`MANUFACTURER_METAPHONE_1`),
  KEY `IDX_CORE_SYNONYM_6` (`MANUFACTURER_METAPHONE_2`),
  KEY `IDX_CORE_SYNONYM_7` (`MANUFACTURER_SEARCH_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_SYNONYM_DERIVATION
CREATE TABLE IF NOT EXISTS `CORE_SYNONYM_DERIVATION` (
  `BASE_DRUG_SYN_ID` int(11) NOT NULL,
  `BASE_SYNONYM_TYPE_ID` int(11) NOT NULL,
  `DERIVED_DRUG_SYN_ID` int(11) NOT NULL,
  `DERIVED_SYNONYM_TYPE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  KEY `IDX_CORE_SYNONYM_DERIVATION_BD` (`BASE_DRUG_SYN_ID`),
  KEY `IDX_CORE_SYNONYM_DERIVATION_DD` (`DERIVED_DRUG_SYN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_SYNONYM_TYPE
CREATE TABLE IF NOT EXISTS `CORE_SYNONYM_TYPE` (
  `SYNONYM_TYPE_ID` int(11) NOT NULL,
  `SYN_TYPE_DESCRIPTION` varchar(60) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`SYNONYM_TYPE_ID`),
  UNIQUE KEY `SYN_TYPE_DESCRIPTION` (`SYN_TYPE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_UNIT
CREATE TABLE IF NOT EXISTS `CORE_UNIT` (
  `UNIT_ID` int(11) NOT NULL,
  `UNIT_DESCRIPTION` varchar(255) NOT NULL,
  `UNIT_ABBREVIATION` varchar(30) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`UNIT_ID`),
  UNIQUE KEY `UNIT_ABBREVIATION` (`UNIT_ABBREVIATION`),
  UNIQUE KEY `UNIT_DESCRIPTION` (`UNIT_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_UNIT_CONVERSION
CREATE TABLE IF NOT EXISTS `CORE_UNIT_CONVERSION` (
  `FROM_UNIT_ID` int(11) NOT NULL,
  `FROM_UNIT_ABBREVIATION` varchar(30) NOT NULL,
  `TO_UNIT_ID` int(11) NOT NULL,
  `TO_UNIT_ABBREVIATION` varchar(30) NOT NULL,
  `FROM_STRENGTH_ID` int(11) DEFAULT NULL,
  `FROM_STRENGTH_ABBREVIATION` varchar(30) DEFAULT NULL,
  `TO_STRENGTH_ID` int(11) DEFAULT NULL,
  `TO_STRENGTH_ABBREVIATION` varchar(30) DEFAULT NULL,
  `CONVERSION_FACTOR` decimal(18,8) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`FROM_UNIT_ID`,`TO_UNIT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CORE_UNIT_NCPDP
CREATE TABLE IF NOT EXISTS `CORE_UNIT_NCPDP` (
  `UNIT_ID` int(11) NOT NULL,
  `NCIT_SUBSET_CODE` varchar(10) NOT NULL,
  `NCIT_CODE` varchar(10) NOT NULL,
  `NCPDP_PREFERRED_TERM` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`UNIT_ID`,`NCIT_CODE`),
  KEY `IDX_CORE_UNIT_NCPDP_1` (`NCPDP_PREFERRED_TERM`),
  KEY `IDX_CORE_UNIT_NCPDP_2` (`NCIT_SUBSET_CODE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.CVX_MAP
CREATE TABLE IF NOT EXISTS `CVX_MAP` (
  `CVX_CODE` varchar(5) NOT NULL,
  `VACCINE_SHORT_DESC` varchar(255) NOT NULL,
  `VACCINE_LONG_DESC` longtext NOT NULL,
  `CVX_STATUS` varchar(20) NOT NULL,
  `CVX_UPDATE_DATE` date NOT NULL,
  `DRUG_ID` varchar(10) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CVX_CODE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DATASET_LOCAL
CREATE TABLE IF NOT EXISTS `DATASET_LOCAL` (
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(255) NOT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  `LOCAL_DATASET_TYPE_CODE` int(11) DEFAULT NULL,
  PRIMARY KEY (`LOCAL_DATASET_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_AMOUNT
CREATE TABLE IF NOT EXISTS `DOSE_AMOUNT` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `DOSE_CONTEXT_ID` int(11) NOT NULL,
  `DOSE_TYPE_ID` int(11) NOT NULL,
  `DOSE_AMOUNT_LOW` decimal(16,6) DEFAULT NULL,
  `DOSE_AMOUNT_HIGH` decimal(16,6) DEFAULT NULL,
  `DOSE_AMOUNT_UNIT_ID` int(11) DEFAULT NULL,
  `DURATION_AMOUNT` decimal(16,6) DEFAULT NULL,
  `DURATION_UNIT_ID` int(11) DEFAULT NULL,
  `SOURCE_ID` int(11) NOT NULL,
  `DOSE_AMOUNT_MAX` decimal(16,6) DEFAULT NULL,
  `DOSE_AMOUNT_MAX_UNIT_ID` int(11) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`,`DOSE_CONTEXT_ID`,`DOSE_TYPE_ID`),
  KEY `IDX_DOSE_AMOUNT` (`DOSE_CONTEXT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_AMOUNT_INGREDIENT
CREATE TABLE IF NOT EXISTS `DOSE_AMOUNT_INGREDIENT` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `DOSE_CONTEXT_ID` int(11) NOT NULL,
  `DOSE_TYPE_ID` int(11) NOT NULL,
  `ACTIVE_INGRED_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`,`DOSE_CONTEXT_ID`,`DOSE_TYPE_ID`,`ACTIVE_INGRED_ID`),
  KEY `IDX_DOSE_AMOUNT_INGREDIENT` (`DOSE_CONTEXT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_AMOUNT_LOCAL
CREATE TABLE IF NOT EXISTS `DOSE_AMOUNT_LOCAL` (
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `GENPRODUCT_ID` int(11) NOT NULL,
  `DOSE_CONTEXT_ID` int(11) NOT NULL,
  `DOSE_TYPE_ID` int(11) NOT NULL,
  `DOSE_AMOUNT_LOW` decimal(16,6) DEFAULT NULL,
  `DOSE_AMOUNT_HIGH` decimal(16,6) DEFAULT NULL,
  `DOSE_AMOUNT_UNIT_ID` int(11) DEFAULT NULL,
  `DURATION_AMOUNT` decimal(16,6) DEFAULT NULL,
  `DURATION_UNIT_ID` int(11) DEFAULT NULL,
  `DOSE_AMOUNT_MAX` decimal(16,6) DEFAULT NULL,
  `DOSE_AMOUNT_MAX_UNIT_ID` int(11) DEFAULT NULL,
  `SUPPORT_URL` varchar(255) DEFAULT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  `CREATE_DATE` date DEFAULT NULL,
  `UPDATE_DATE` date DEFAULT NULL,
  PRIMARY KEY (`LOCAL_DATASET_ID`,`GENPRODUCT_ID`,`DOSE_CONTEXT_ID`,`DOSE_TYPE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_COMMENT
CREATE TABLE IF NOT EXISTS `DOSE_COMMENT` (
  `DOSE_COMMENT_ID` int(11) NOT NULL,
  `COMMENT_TEXT` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DOSE_COMMENT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_COMMENT_GENPRODUCT
CREATE TABLE IF NOT EXISTS `DOSE_COMMENT_GENPRODUCT` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `DOSE_CONTEXT_ID` int(11) NOT NULL,
  `DOSE_COMMENT_ID` int(11) NOT NULL,
  `IS_CONTRAINDICATION` tinyint(4) NOT NULL DEFAULT '0',
  `SOURCE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`,`DOSE_CONTEXT_ID`),
  KEY `IDX_DOSE_COMMENT_GENPRODUCT` (`DOSE_CONTEXT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_COMMENT_GENPROD_LOCAL
CREATE TABLE IF NOT EXISTS `DOSE_COMMENT_GENPROD_LOCAL` (
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `GENPRODUCT_ID` int(11) NOT NULL,
  `DOSE_CONTEXT_ID` int(11) NOT NULL,
  `DOSE_COMMENT_ID` int(11) NOT NULL,
  `IS_CONTRAINDICATION` tinyint(4) NOT NULL DEFAULT '0',
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`LOCAL_DATASET_ID`,`GENPRODUCT_ID`,`DOSE_CONTEXT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_COMMENT_LOCAL
CREATE TABLE IF NOT EXISTS `DOSE_COMMENT_LOCAL` (
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `DOSE_COMMENT_ID` int(11) NOT NULL,
  `COMMENT_TEXT` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`LOCAL_DATASET_ID`,`DOSE_COMMENT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_CONTEXT
CREATE TABLE IF NOT EXISTS `DOSE_CONTEXT` (
  `DOSE_CONTEXT_ID` int(11) NOT NULL,
  `AGE_DAYS_LOW` int(11) NOT NULL,
  `AGE_DAYS_HIGH` int(11) NOT NULL,
  `WEIGHT_KG_LOW` decimal(6,2) DEFAULT NULL,
  `WEIGHT_KG_HIGH` decimal(6,2) DEFAULT NULL,
  `GENDER_CODE` varchar(1) DEFAULT NULL,
  `CONDITION_ID` int(11) DEFAULT NULL,
  `DOSE_ROUTE_ID` int(11) DEFAULT NULL,
  `CRCL_MLMIN_LOW` smallint(6) DEFAULT NULL,
  `CRCL_MLMIN_HIGH` smallint(6) DEFAULT NULL,
  `DIALYSIS_TYPE_ID` varchar(2) DEFAULT NULL,
  `HEPATIC_IMPAIRMENT_IND` tinyint(4) DEFAULT NULL,
  `DEFAULT_RANGE_IND` tinyint(4) NOT NULL,
  `GESTATIONAL_AGE_LOW` int(11) DEFAULT NULL,
  `GESTATIONAL_AGE_HIGH` int(11) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DOSE_CONTEXT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_CONTEXT_LOCAL
CREATE TABLE IF NOT EXISTS `DOSE_CONTEXT_LOCAL` (
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `DOSE_CONTEXT_ID` int(11) NOT NULL,
  `AGE_DAYS_LOW` int(11) NOT NULL,
  `AGE_DAYS_HIGH` int(11) NOT NULL,
  `WEIGHT_KG_LOW` decimal(6,2) DEFAULT NULL,
  `WEIGHT_KG_HIGH` decimal(6,2) DEFAULT NULL,
  `GENDER_CODE` varchar(1) DEFAULT NULL,
  `CONDITION_ID` int(11) DEFAULT NULL,
  `DOSE_ROUTE_ID` int(11) DEFAULT NULL,
  `CRCL_MLMIN_LOW` smallint(6) DEFAULT NULL,
  `CRCL_MLMIN_HIGH` smallint(6) DEFAULT NULL,
  `DIALYSIS_TYPE_ID` varchar(2) DEFAULT NULL,
  `HEPATIC_IMPAIRMENT_IND` tinyint(4) DEFAULT NULL,
  `GESTATIONAL_AGE_LOW` int(11) DEFAULT NULL,
  `GESTATIONAL_AGE_HIGH` int(11) DEFAULT NULL,
  `SUPPORT_URL` varchar(255) DEFAULT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  `CREATE_DATE` date DEFAULT NULL,
  `UPDATE_DATE` date DEFAULT NULL,
  PRIMARY KEY (`LOCAL_DATASET_ID`,`DOSE_CONTEXT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_DIALYSIS_TYPE
CREATE TABLE IF NOT EXISTS `DOSE_DIALYSIS_TYPE` (
  `DIALYSIS_TYPE_ID` varchar(2) NOT NULL,
  `DIALYSIS_TYPE_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DIALYSIS_TYPE_ID`),
  UNIQUE KEY `DIALYSIS_TYPE_DESCRIPTION` (`DIALYSIS_TYPE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_DOSE_TYPE
CREATE TABLE IF NOT EXISTS `DOSE_DOSE_TYPE` (
  `DOSE_TYPE_ID` int(11) NOT NULL,
  `DOSE_TYPE_DESCRIPTION` varchar(100) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DOSE_TYPE_ID`),
  UNIQUE KEY `DOSE_TYPE_DESCRIPTION` (`DOSE_TYPE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_GENPRODUCT
CREATE TABLE IF NOT EXISTS `DOSE_GENPRODUCT` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `IS_PRN_DRUG` tinyint(4) NOT NULL,
  `IS_CHRONIC_THERAPY_DRUG` tinyint(4) NOT NULL,
  `LIFETIME_DOSE_COMMENT` longtext,
  `SOURCE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_MAX_FREQUENCY
CREATE TABLE IF NOT EXISTS `DOSE_MAX_FREQUENCY` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `DOSE_CONTEXT_ID` int(11) NOT NULL,
  `MAX_FREQUENCY_ID` int(11) NOT NULL,
  `SOURCE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`,`DOSE_CONTEXT_ID`),
  KEY `IDX_DOSE_MAX_FREQUENCY` (`DOSE_CONTEXT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_MAX_FREQUENCY_LOCAL
CREATE TABLE IF NOT EXISTS `DOSE_MAX_FREQUENCY_LOCAL` (
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `GENPRODUCT_ID` int(11) NOT NULL,
  `DOSE_CONTEXT_ID` int(11) NOT NULL,
  `MAX_FREQUENCY_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`LOCAL_DATASET_ID`,`GENPRODUCT_ID`,`DOSE_CONTEXT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DOSE_ROUTE
CREATE TABLE IF NOT EXISTS `DOSE_ROUTE` (
  `DOSE_ROUTE_ID` int(11) NOT NULL,
  `DOSE_ROUTE_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DOSE_ROUTE_ID`),
  UNIQUE KEY `DOSE_ROUTE_DESCRIPTION` (`DOSE_ROUTE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DRUGINT_ADVICE
CREATE TABLE IF NOT EXISTS `DRUGINT_ADVICE` (
  `ADVICE_ID` int(11) NOT NULL,
  `ADVICE_DESCRIPTION` varchar(60) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ADVICE_ID`),
  UNIQUE KEY `ADVICE_DESCRIPTION` (`ADVICE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DRUGINT_ADVICE_ORDER
CREATE TABLE IF NOT EXISTS `DRUGINT_ADVICE_ORDER` (
  `ADVICE_ID` int(11) NOT NULL,
  `INT_SEVERITY_ID` int(11) NOT NULL,
  `DISPLAY_ORDER` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ADVICE_ID`,`INT_SEVERITY_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DRUGINT_INTERACTION
CREATE TABLE IF NOT EXISTS `DRUGINT_INTERACTION` (
  `DRUG_INTERACTION_ID` int(11) NOT NULL,
  `INTERACTION_DESCRIPTION` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_INTERACTION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DRUGINT_INTERACTION_ADVICE
CREATE TABLE IF NOT EXISTS `DRUGINT_INTERACTION_ADVICE` (
  `DRUG_INTERACTION_ID` int(11) NOT NULL,
  `ADVICE_ID` int(11) NOT NULL,
  `INT_SEVERITY_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_INTERACTION_ID`,`ADVICE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DRUGINT_INTERACTION_CONSUMER
CREATE TABLE IF NOT EXISTS `DRUGINT_INTERACTION_CONSUMER` (
  `DRUG_INTERACTION_ID` int(11) NOT NULL,
  `INTERACTION_TEXT_ID` int(11) NOT NULL,
  `SEQUENCE_NUMBER` smallint(6) NOT NULL,
  `INTERACTION_DESCRIPTION` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_INTERACTION_ID`,`INTERACTION_TEXT_ID`,`SEQUENCE_NUMBER`),
  UNIQUE KEY `INTERACTION_TEXT_ID` (`INTERACTION_TEXT_ID`),
  UNIQUE KEY `DRUG_INTERACTION_ID` (`DRUG_INTERACTION_ID`,`SEQUENCE_NUMBER`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DRUGINT_INTERACTION_GENDRUG
CREATE TABLE IF NOT EXISTS `DRUGINT_INTERACTION_GENDRUG` (
  `DRUG_ID_1` varchar(10) NOT NULL,
  `DRUG_ID_2` varchar(10) NOT NULL,
  `DRUG_INTERACTION_ID` int(11) NOT NULL,
  `INT_SEVERITY_ID` int(11) NOT NULL,
  `PRIMARY_PAIR` tinyint(4) NOT NULL DEFAULT '0',
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID_1`,`DRUG_ID_2`,`DRUG_INTERACTION_ID`),
  KEY `IDX_DRUGINT_INTERACTION_GENDRUG` (`DRUG_ID_2`),
  KEY `IDX_DRUGINT_INTERACTION_GENDRUG_2` (`DRUG_INTERACTION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DRUGINT_INTERACTION_LOCAL
CREATE TABLE IF NOT EXISTS `DRUGINT_INTERACTION_LOCAL` (
  `DRUG_INTERACTION_ID` int(11) NOT NULL,
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `INT_SEVERITY_ID` int(11) DEFAULT NULL,
  `LOCAL_VALUE` varchar(50) DEFAULT NULL,
  `SUPPORT_URL` varchar(255) DEFAULT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  PRIMARY KEY (`DRUG_INTERACTION_ID`,`LOCAL_DATASET_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DRUGINT_INTERACTION_REF
CREATE TABLE IF NOT EXISTS `DRUGINT_INTERACTION_REF` (
  `DRUG_INTERACTION_ID` int(11) NOT NULL,
  `REFERENCE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_INTERACTION_ID`,`REFERENCE_ID`),
  KEY `IDX_DRUGINT_INTERACTION_REF` (`REFERENCE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DRUGINT_SEVERITY
CREATE TABLE IF NOT EXISTS `DRUGINT_SEVERITY` (
  `INT_SEVERITY_ID` int(11) NOT NULL,
  `SEVERITY_DESCRIPTION` varchar(50) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`INT_SEVERITY_ID`),
  UNIQUE KEY `SEVERITY_DESCRIPTION` (`SEVERITY_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DUP_THERAPY_CLASS
CREATE TABLE IF NOT EXISTS `DUP_THERAPY_CLASS` (
  `DTCLASS_ID` int(11) NOT NULL,
  `CLASS_NAME` varchar(60) NOT NULL,
  `PARENT_DTCLASS_ID` int(11) DEFAULT NULL,
  `HAS_CHILDREN` tinyint(4) NOT NULL,
  `IS_ACTIVE` tinyint(4) NOT NULL,
  `MAX_DUPLICATION_COUNT` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DTCLASS_ID`),
  UNIQUE KEY `CLASS_NAME` (`CLASS_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DUP_THERAPY_CLASS_GENDRUG
CREATE TABLE IF NOT EXISTS `DUP_THERAPY_CLASS_GENDRUG` (
  `DTCLASS_ID` int(11) NOT NULL,
  `DRUG_ID` varchar(10) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DTCLASS_ID`,`DRUG_ID`),
  KEY `IDX_DUP_THERAPY_CLASS_GENDRUG` (`DRUG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.DUP_THERAPY_CLASS_LOCAL
CREATE TABLE IF NOT EXISTS `DUP_THERAPY_CLASS_LOCAL` (
  `DTCLASS_ID` int(11) NOT NULL,
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `MAX_DUPLICATION_COUNT` int(11) DEFAULT NULL,
  `LOCAL_VALUE` varchar(255) DEFAULT NULL,
  `SUPPORT_URL` varchar(255) DEFAULT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  PRIMARY KEY (`DTCLASS_ID`,`LOCAL_DATASET_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ICD9
CREATE TABLE IF NOT EXISTS `ICD9` (
  `ICD9_CODE` varchar(10) NOT NULL,
  `ICD9_DESCRIPTION` varchar(75) NOT NULL,
  `IS_VALID` tinyint(4) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ICD9_CODE`),
  KEY `IDX_ICD9` (`ICD9_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ICD9_CONDITION
CREATE TABLE IF NOT EXISTS `ICD9_CONDITION` (
  `ICD9_CODE` varchar(10) NOT NULL,
  `CONDITION_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ICD9_CODE`,`CONDITION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ICD9_SYNONYM
CREATE TABLE IF NOT EXISTS `ICD9_SYNONYM` (
  `ICD9_CODE` varchar(10) NOT NULL,
  `SEQUENCE_NUMBER` int(11) NOT NULL,
  `ICD9_DESCRIPTION` varchar(75) NOT NULL,
  `IS_VALID` tinyint(4) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ICD9_CODE`,`SEQUENCE_NUMBER`),
  KEY `IDX_ICD9_SYNONYM` (`ICD9_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INDICATIONS_GENPRODUCT
CREATE TABLE IF NOT EXISTS `INDICATIONS_GENPRODUCT` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `CLASS_ID` int(11) NOT NULL,
  `CONDITION_ID` int(11) NOT NULL,
  `CLASS_SYSTEM_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`,`CLASS_ID`,`CONDITION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INTERACT_CATEGORIES
CREATE TABLE IF NOT EXISTS `INTERACT_CATEGORIES` (
  `CATEGORY_ID` int(11) NOT NULL,
  `CATEGORY` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`CATEGORY_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INTERACT_DRUG_FOOD
CREATE TABLE IF NOT EXISTS `INTERACT_DRUG_FOOD` (
  `DRUG_ID` varchar(10) NOT NULL,
  `INTERACTION_ID` int(11) NOT NULL,
  `FOOD_SUMMARY` longtext NOT NULL,
  `SEVERITY_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`INTERACTION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INTERACT_DRUG_PAIRS
CREATE TABLE IF NOT EXISTS `INTERACT_DRUG_PAIRS` (
  `DRUG_ID_1` varchar(10) NOT NULL,
  `DRUG_ID_2` varchar(10) NOT NULL,
  `INTERACTION_ID` int(11) NOT NULL,
  `SEVERITY_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID_1`,`DRUG_ID_2`,`INTERACTION_ID`),
  KEY `IDX_INTERACT_DRUG_PAIRS_1` (`DRUG_ID_1`),
  KEY `IDX_INTERACT_DRUG_PAIRS_2` (`DRUG_ID_2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INTERACT_INTERACTION
CREATE TABLE IF NOT EXISTS `INTERACT_INTERACTION` (
  `INTERACTION_ID` int(11) NOT NULL,
  `TITLE` varchar(500) NOT NULL,
  `RISK_ID` int(11) NOT NULL,
  `SUMMARY` longtext NOT NULL,
  `SEVERITY_ID` int(11) NOT NULL,
  `ONSET_ID` int(11) DEFAULT NULL,
  `RELIABILITY_ID` int(11) NOT NULL,
  `MANAGEMENT` longtext NOT NULL,
  `DISCUSSION` longtext NOT NULL,
  `FOOTNOTES` longtext,
  `DEPENDENCIES` longtext,
  `ANALYSIS_FILTER` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`INTERACTION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INTERACT_INTERACTION_CONSUMER
CREATE TABLE IF NOT EXISTS `INTERACT_INTERACTION_CONSUMER` (
  `INTERACTION_ID` int(11) NOT NULL,
  `TITLE` varchar(500) NOT NULL,
  `CONSUMER_SUMMARY` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`INTERACTION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INTERACT_INTERACTION_LOCAL
CREATE TABLE IF NOT EXISTS `INTERACT_INTERACTION_LOCAL` (
  `INTERACTION_ID` int(11) NOT NULL,
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `SEVERITY_ID` int(11) DEFAULT NULL,
  `LOCAL_VALUE` varchar(50) DEFAULT NULL,
  `SUPPORT_URL` varchar(255) DEFAULT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  PRIMARY KEY (`INTERACTION_ID`,`LOCAL_DATASET_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INTERACT_MEMBERS
CREATE TABLE IF NOT EXISTS `INTERACT_MEMBERS` (
  `INTERACTION_ID` int(11) NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `DRUG_ID` varchar(10) NOT NULL,
  `IS_EXCEPTION` tinyint(4) NOT NULL,
  `IS_IMPLICATED` tinyint(4) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`INTERACTION_ID`,`CATEGORY_ID`,`DRUG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INTERACT_ONSET
CREATE TABLE IF NOT EXISTS `INTERACT_ONSET` (
  `ONSET_ID` int(11) NOT NULL,
  `ONSET` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ONSET_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INTERACT_RELIABILITY
CREATE TABLE IF NOT EXISTS `INTERACT_RELIABILITY` (
  `RELIABILITY_ID` int(11) NOT NULL,
  `RELIABILITY` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`RELIABILITY_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INTERACT_RISK
CREATE TABLE IF NOT EXISTS `INTERACT_RISK` (
  `RISK_ID` int(11) NOT NULL,
  `RISK` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`RISK_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.INTERACT_SEVERITY
CREATE TABLE IF NOT EXISTS `INTERACT_SEVERITY` (
  `SEVERITY_ID` int(11) NOT NULL,
  `SEVERITY` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`SEVERITY_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.IVCOMP_INTERACTION_GENDRUG
CREATE TABLE IF NOT EXISTS `IVCOMP_INTERACTION_GENDRUG` (
  `DRUG_ID_1` varchar(10) NOT NULL,
  `DRUG_ID_2` varchar(10) NOT NULL,
  `YSITE_COMPAT_ID` int(11) DEFAULT NULL,
  `MONOGRAPH_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID_1`,`DRUG_ID_2`,`MONOGRAPH_ID`),
  KEY `IDX_IVCOMP_INTERACTION_GENDRUG` (`MONOGRAPH_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.IVCOMP_MONOGRAPH
CREATE TABLE IF NOT EXISTS `IVCOMP_MONOGRAPH` (
  `MONOGRAPH_ID` int(11) NOT NULL,
  `DISPLAY_NAME` varchar(128) NOT NULL,
  `SEARCH_NAME` varchar(128) NOT NULL,
  `METAPHONE_1` varchar(128) NOT NULL,
  `METAPHONE_2` varchar(128) NOT NULL,
  `MONOGRAPH_XML` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MONOGRAPH_ID`),
  UNIQUE KEY `SEARCH_NAME` (`SEARCH_NAME`),
  KEY `IDX_IVCOMP_MONOGRAPH` (`METAPHONE_1`),
  KEY `IDX_IVCOMP_MONOGRAPH_2` (`METAPHONE_2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.IVCOMP_MONOGRAPH_GENDRUG
CREATE TABLE IF NOT EXISTS `IVCOMP_MONOGRAPH_GENDRUG` (
  `DRUG_ID` varchar(10) NOT NULL,
  `MONOGRAPH_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`MONOGRAPH_ID`),
  UNIQUE KEY `MONOGRAPH_ID` (`MONOGRAPH_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.IVCOMP_YSITE_COMPAT
CREATE TABLE IF NOT EXISTS `IVCOMP_YSITE_COMPAT` (
  `YSITE_COMPAT_ID` int(11) NOT NULL,
  `YSITE_COMPAT_DESC` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`YSITE_COMPAT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.LEXI_DRUG_XREF
CREATE TABLE IF NOT EXISTS `LEXI_DRUG_XREF` (
  `LEXI_ONLINE_DOC_ID` int(11) NOT NULL,
  `GLOBAL_ID` int(11) NOT NULL,
  `DRUG_ID` varchar(10) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`LEXI_ONLINE_DOC_ID`),
  KEY `IDX_LEXI_DRUG_XREF` (`DRUG_ID`),
  KEY `IDX_LEXI_DRUG_XREF_2` (`GLOBAL_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.LEXI_MONOGRAPH
CREATE TABLE IF NOT EXISTS `LEXI_MONOGRAPH` (
  `MONOGRAPH_ID` int(11) NOT NULL,
  `MONOGRAPH_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MONOGRAPH_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.LEXI_MONOGRAPH_FIELD_TYPE
CREATE TABLE IF NOT EXISTS `LEXI_MONOGRAPH_FIELD_TYPE` (
  `MONOGRAPH_ID` int(11) NOT NULL,
  `FIELD_TYPE_ID` int(11) NOT NULL,
  `SEQUENCE_NUMBER` int(11) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MONOGRAPH_ID`,`FIELD_TYPE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.LEXI_MONOGRAPH_LOCAL
CREATE TABLE IF NOT EXISTS `LEXI_MONOGRAPH_LOCAL` (
  `MONOGRAPH_ID` int(11) NOT NULL,
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `MONOGRAPH_DESCRIPTION` varchar(255) NOT NULL,
  `SUPPORT_URL` varchar(255) DEFAULT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  PRIMARY KEY (`MONOGRAPH_ID`,`LOCAL_DATASET_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.LEXI_MONOGRPH_FLD_TYPE_LOCAL
CREATE TABLE IF NOT EXISTS `LEXI_MONOGRPH_FLD_TYPE_LOCAL` (
  `MONOGRAPH_ID` int(11) NOT NULL,
  `FIELD_TYPE_ID` int(11) NOT NULL,
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `SEQUENCE_NUMBER` int(11) DEFAULT NULL,
  `SUPPORT_URL` varchar(255) DEFAULT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  PRIMARY KEY (`MONOGRAPH_ID`,`FIELD_TYPE_ID`,`LOCAL_DATASET_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.LEXI_MONO_FIELD
CREATE TABLE IF NOT EXISTS `LEXI_MONO_FIELD` (
  `FIELD_ID` int(11) NOT NULL,
  `FIELD_TYPE_ID` int(11) NOT NULL,
  `FIELD_CONTENT` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`FIELD_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.LEXI_MONO_FIELD_TYPE
CREATE TABLE IF NOT EXISTS `LEXI_MONO_FIELD_TYPE` (
  `FIELD_TYPE_ID` int(11) NOT NULL,
  `FIELD_TYPE_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`FIELD_TYPE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.LEXI_MONO_SECTION
CREATE TABLE IF NOT EXISTS `LEXI_MONO_SECTION` (
  `SECTION_ID` int(11) NOT NULL,
  `SECTION_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`SECTION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.LEXI_MONO_SECTION_FIELD
CREATE TABLE IF NOT EXISTS `LEXI_MONO_SECTION_FIELD` (
  `FIELD_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `SEQUENCE_NUMBER` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`FIELD_ID`,`SECTION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.LEXI_MONO_SECTION_GENDRUG
CREATE TABLE IF NOT EXISTS `LEXI_MONO_SECTION_GENDRUG` (
  `DRUG_ID` varchar(10) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`SECTION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.MED_SUP_CATEGORIES
CREATE TABLE IF NOT EXISTS `MED_SUP_CATEGORIES` (
  `MS_CATEGORY_ID` int(11) NOT NULL DEFAULT '0',
  `MS_CATEGORY_NAME` varchar(255) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MS_CATEGORY_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.MED_SUP_NDC_PKG_PRODUCT
CREATE TABLE IF NOT EXISTS `MED_SUP_NDC_PKG_PRODUCT` (
  `MS_PKG_PRODUCT_ID` varchar(11) NOT NULL DEFAULT '',
  `MS_BRAND_DESCRIPTION` varchar(255) DEFAULT NULL,
  `MS_INNER_PACKAGE_SIZE` decimal(16,6) DEFAULT NULL,
  `MS_INNER_PACKAGE_DESCRIPTION` varchar(30) DEFAULT NULL,
  `MS_OUTER_PACKAGE_SIZE` decimal(16,6) DEFAULT NULL,
  `MS_SOURCE_DESCRIPTION` varchar(120) DEFAULT NULL,
  `MS_CATEGORY_GROUP` varchar(50) DEFAULT NULL,
  `MS_CATEGORY_ID` int(11) DEFAULT NULL,
  `METAPHONE_1` varchar(255) DEFAULT NULL,
  `METAPHONE_2` varchar(255) DEFAULT NULL,
  `SEARCH_DESCRIPTION` varchar(255) DEFAULT NULL,
  `MS_RX_OTC_STATUS_CODE` varchar(3) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MS_PKG_PRODUCT_ID`),
  KEY `IDX_MED_SUP_NDC_PKG_PROD_CAT` (`MS_CATEGORY_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.MED_SUP_NDC_PKG_PRODUCT_PRICE
CREATE TABLE IF NOT EXISTS `MED_SUP_NDC_PKG_PRODUCT_PRICE` (
  `MS_PKG_PRODUCT_ID` varchar(11) NOT NULL DEFAULT '',
  `MS_PRICE_TYPE_CODE` varchar(1) NOT NULL DEFAULT '',
  `MS_PRICE` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `DATE_SET` date NOT NULL DEFAULT '0000-00-00',
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MS_PKG_PRODUCT_ID`,`MS_PRICE_TYPE_CODE`,`MS_PRICE`,`DATE_SET`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.MVX_MAP
CREATE TABLE IF NOT EXISTS `MVX_MAP` (
  `MVX_CODE` varchar(5) NOT NULL,
  `MANUFACT_NAME` varchar(255) NOT NULL,
  `MVX_STATUS` varchar(20) NOT NULL,
  `MVX_UPDATE_DATE` date NOT NULL,
  `LABELER_ID` int(11) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MVX_CODE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.NDC_ADDL_DOSEFORM
CREATE TABLE IF NOT EXISTS `NDC_ADDL_DOSEFORM` (
  `ADDL_DOSEFORM_ID` int(11) NOT NULL,
  `ADDL_DOSEFORM_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ADDL_DOSEFORM_ID`),
  UNIQUE KEY `ADDL_DOSEFORM_DESCRIPTION` (`ADDL_DOSEFORM_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.NDC_COLOR
CREATE TABLE IF NOT EXISTS `NDC_COLOR` (
  `COLOR_ID` int(11) NOT NULL,
  `COLOR_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`COLOR_ID`),
  UNIQUE KEY `COLOR_DESCRIPTION` (`COLOR_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for view lexi-data.NDC_DENORM
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `NDC_DENORM` (
	`PKG_PRODUCT_ID` VARCHAR(11) NOT NULL COLLATE 'latin1_swedish_ci',
	`TRADE_NAME` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`GENPRODUCT_ID` INT(11) NOT NULL,
	`ROUTE_DESCRIPTION` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`DOSEFORM_DESCRIPTION` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`STRENGTH_DESCRIPTION` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`DRUG_ID` VARCHAR(10) NOT NULL COLLATE 'latin1_swedish_ci',
	`ACTIVE_INGRED_NAME` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`NUMERATOR_AMOUNT` DECIMAL(16,6) NOT NULL,
	`NUMERATOR_UNIT` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`DENOMINATOR_AMOUNT` DECIMAL(16,6) NOT NULL,
	`DENOMINATOR_UNIT` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`CSA_CODE` VARCHAR(1) NOT NULL COLLATE 'latin1_swedish_ci',
	`JCODE` VARCHAR(10) NULL COLLATE 'latin1_swedish_ci',
	`JCODE_DESCRIPTION` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`RX_OTC_STATUS_CODE` VARCHAR(6) NOT NULL COLLATE 'latin1_swedish_ci',
	`INNER_PACK_SIZE` DECIMAL(16,6) NOT NULL,
	`INNER_PACK_UNIT` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`OUTER_PACK_SIZE` DECIMAL(16,6) NOT NULL,
	`DATE_OBSOLETE` DATE NULL,
	`LABELER_NAME` VARCHAR(120) NOT NULL COLLATE 'latin1_swedish_ci',
	`ADDRESS_1` VARCHAR(120) NOT NULL COLLATE 'latin1_swedish_ci',
	`ADDRESS_2` VARCHAR(120) NULL COLLATE 'latin1_swedish_ci',
	`CITY` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`STATE` VARCHAR(10) NOT NULL COLLATE 'latin1_swedish_ci',
	`PROVINCE` VARCHAR(30) NULL COLLATE 'latin1_swedish_ci',
	`POSTAL_CODE` VARCHAR(10) NOT NULL COLLATE 'latin1_swedish_ci',
	`COUNTRY` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`ORANGE_BOOK_DESCRIPTION` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`UNIT_DOSE_CODE` VARCHAR(1) NOT NULL COLLATE 'latin1_swedish_ci',
	`IS_REPACKAGED` TINYINT(4) NOT NULL,
	`BRAND_GENERIC_CODE` VARCHAR(1) NOT NULL COLLATE 'latin1_swedish_ci',
	`AWP` DECIMAL(18,4) NULL,
	`FUL` DECIMAL(18,4) NULL,
	`MDR` DECIMAL(18,4) NULL,
	`WSC` DECIMAL(18,4) NULL
) ENGINE=MyISAM;


-- Dumping structure for table lexi-data.NDC_FLAVOR
CREATE TABLE IF NOT EXISTS `NDC_FLAVOR` (
  `FLAVOR_ID` int(11) NOT NULL,
  `FLAVOR_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`FLAVOR_ID`),
  UNIQUE KEY `FLAVOR_DESCRIPTION` (`FLAVOR_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.NDC_LABELER
CREATE TABLE IF NOT EXISTS `NDC_LABELER` (
  `LABELER_ID` int(11) NOT NULL,
  `LABELER_NAME` varchar(120) NOT NULL,
  `ADDRESS_1` varchar(120) NOT NULL,
  `ADDRESS_2` varchar(120) DEFAULT NULL,
  `CITY` varchar(50) NOT NULL,
  `STATE` varchar(10) NOT NULL,
  `PROVINCE` varchar(30) DEFAULT NULL,
  `POSTAL_CODE` varchar(10) NOT NULL,
  `COUNTRY` varchar(50) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`LABELER_ID`),
  UNIQUE KEY `LABELER_NAME` (`LABELER_NAME`),
  KEY `IDX_NDC_LABELER` (`ADDRESS_1`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.NDC_ORANGE_BOOK
CREATE TABLE IF NOT EXISTS `NDC_ORANGE_BOOK` (
  `ORANGE_BOOK_ID` int(11) NOT NULL,
  `ORANGE_BOOK_DESCRIPTION` varchar(255) NOT NULL,
  `ORANGE_BOOK_ABBREVIATION` varchar(30) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ORANGE_BOOK_ID`),
  UNIQUE KEY `ORANGE_BOOK_DESCRIPTION` (`ORANGE_BOOK_DESCRIPTION`),
  UNIQUE KEY `ORANGE_BOOK_ABBREVIATION` (`ORANGE_BOOK_ABBREVIATION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.NDC_OUTER_INNER_MAP
CREATE TABLE IF NOT EXISTS `NDC_OUTER_INNER_MAP` (
  `OUTER_PKG_PRODUCT_ID` varchar(11) NOT NULL,
  `INNER_PKG_PRODUCT_ID` varchar(11) NOT NULL,
  `INNER_SIZE` decimal(16,6) NOT NULL,
  `DATE_OBSOLETE` date DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`OUTER_PKG_PRODUCT_ID`,`INNER_PKG_PRODUCT_ID`),
  UNIQUE KEY `INNER_PKG_PRODUCT_ID` (`INNER_PKG_PRODUCT_ID`),
  KEY `IDX_NDC_OUTER_INNER_MAP` (`OUTER_PKG_PRODUCT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.NDC_PKG_PRODUCT
CREATE TABLE IF NOT EXISTS `NDC_PKG_PRODUCT` (
  `PKG_PRODUCT_ID` varchar(11) NOT NULL,
  `GENPRODUCT_ID` int(11) NOT NULL,
  `DRUG_SYN_ID` int(11) DEFAULT NULL,
  `CORE_9` varchar(9) NOT NULL,
  `CORE_10` varchar(10) DEFAULT NULL,
  `TRADE_NAME_ID` int(11) NOT NULL,
  `RX_OTC_STATUS_CODE` varchar(6) NOT NULL,
  `INNER_PACK_SIZE` decimal(16,6) NOT NULL,
  `INNER_PACK_UNIT_ID` int(11) DEFAULT NULL,
  `OUTER_PACK_SIZE` decimal(16,6) NOT NULL,
  `LABELER_ID` int(11) NOT NULL,
  `ORANGE_BOOK_ID` int(11) NOT NULL,
  `UNIT_DOSE_CODE` varchar(1) NOT NULL,
  `IS_REPACKAGED` tinyint(4) NOT NULL,
  `BRAND_GENERIC_CODE` varchar(1) NOT NULL,
  `FORMATTED_NDC` varchar(13) NOT NULL,
  `FORMATTED_CORE_10` varchar(12) DEFAULT NULL,
  `DATE_OBSOLETE` date DEFAULT NULL,
  `IS_TOP_200` tinyint(4) NOT NULL,
  `SHAPE_ID` int(11) DEFAULT NULL,
  `COLOR_ID` int(11) DEFAULT NULL,
  `FLAVOR_ID` int(11) DEFAULT NULL,
  `ADDL_DOSEFORM_ID` int(11) DEFAULT NULL,
  `MARKING_SIDE_1` varchar(25) DEFAULT NULL,
  `MARKING_SIDE_2` varchar(25) DEFAULT NULL,
  `IS_SCORED` tinyint(4) DEFAULT NULL,
  `IMAGE_FILENAME` varchar(50) DEFAULT NULL,
  `SEARCH_MARKINGS` varchar(50) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`PKG_PRODUCT_ID`),
  UNIQUE KEY `FORMATTED_NDC` (`FORMATTED_NDC`),
  KEY `IDX_NDC_PKG_PRODUCT` (`IMAGE_FILENAME`),
  KEY `IDX_NDC_PKG_PRODUCT_2` (`GENPRODUCT_ID`),
  KEY `IDX_NDC_PKG_PRODUCT_3` (`TRADE_NAME_ID`),
  KEY `IDX_NDC_PKG_PRODUCT_4` (`MARKING_SIDE_2`),
  KEY `IDX_NDC_PKG_PRODUCT_5` (`MARKING_SIDE_1`),
  KEY `IDX_NDC_PKG_PRODUCT_6` (`SEARCH_MARKINGS`),
  KEY `IDX_NDC_PKG_PRODUCT_7` (`CORE_9`),
  KEY `IDX_NDC_PKG_PRODUCT_8` (`CORE_10`),
  KEY `IDX_NDC_PKG_PRODUCT_9` (`DRUG_SYN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.NDC_PKG_PRODUCT_PRICE
CREATE TABLE IF NOT EXISTS `NDC_PKG_PRODUCT_PRICE` (
  `PKG_PRODUCT_ID` varchar(11) NOT NULL,
  `PRICE_TYPE_CODE` varchar(1) NOT NULL,
  `DATE_SET` date NOT NULL,
  `PRICE` decimal(18,4) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`PKG_PRODUCT_ID`,`PRICE_TYPE_CODE`,`DATE_SET`),
  KEY `IDX_NDC_PKG_PRODUCT_PRICE` (`PRICE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.NDC_SHAPE
CREATE TABLE IF NOT EXISTS `NDC_SHAPE` (
  `SHAPE_ID` int(11) NOT NULL,
  `SHAPE_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`SHAPE_ID`),
  UNIQUE KEY `SHAPE_DESCRIPTION` (`SHAPE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.NDC_TRADE_NAME
CREATE TABLE IF NOT EXISTS `NDC_TRADE_NAME` (
  `TRADE_NAME_ID` int(11) NOT NULL,
  `TRADE_NAME` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`TRADE_NAME_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ORDER_CATEGORY
CREATE TABLE IF NOT EXISTS `ORDER_CATEGORY` (
  `ORDER_CATEGORY_ID` int(11) NOT NULL,
  `ORDER_CATEGORY_DESCRIPTION` varchar(100) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ORDER_CATEGORY_ID`),
  UNIQUE KEY `ORDER_CATEGORY_DESCRIPTION` (`ORDER_CATEGORY_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ORDER_COMMON
CREATE TABLE IF NOT EXISTS `ORDER_COMMON` (
  `ORDER_ID` int(11) NOT NULL,
  `GENPRODUCT_ID` int(11) NOT NULL,
  `ORDER_CATEGORY_ID` int(11) NOT NULL,
  `CLINICAL_ROUTE_ID` int(11) NOT NULL,
  `FREQUENCY_ID` int(11) NOT NULL,
  `DOSE_AMT` decimal(16,6) DEFAULT NULL,
  `DOSE_AMT_UNIT_ID` int(11) DEFAULT NULL,
  `QUANTITY_AMT` decimal(16,6) DEFAULT NULL,
  `QUANTITY_AMT_UNIT_ID` int(11) DEFAULT NULL,
  `DURATION_AMT` decimal(16,6) DEFAULT NULL,
  `DURATION_UNIT_ID` int(11) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`ORDER_ID`,`GENPRODUCT_ID`),
  KEY `IDX_ORDER_COMMON` (`GENPRODUCT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.ORDER_COMMON_LOCAL
CREATE TABLE IF NOT EXISTS `ORDER_COMMON_LOCAL` (
  `ORDER_ID` int(11) NOT NULL,
  `GENPRODUCT_ID` int(11) NOT NULL,
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  `ORDER_CATEGORY_ID` int(11) DEFAULT NULL,
  `CLINICAL_ROUTE_ID` int(11) DEFAULT NULL,
  `FREQUENCY_ID` int(11) DEFAULT NULL,
  `DOSE_AMT` decimal(16,6) DEFAULT NULL,
  `DOSE_AMT_UNIT_ID` int(11) DEFAULT NULL,
  `QUANTITY_AMT` decimal(16,6) DEFAULT NULL,
  `QUANTITY_AMT_UNIT_ID` int(11) DEFAULT NULL,
  `DURATION_AMT` decimal(16,6) DEFAULT NULL,
  `DURATION_UNIT_ID` int(11) DEFAULT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  `SUPPORT_URL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ORDER_ID`,`GENPRODUCT_ID`,`LOCAL_DATASET_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_DOSING
CREATE TABLE IF NOT EXISTS `REF_DOSING` (
  `DRUG_ID` varchar(10) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `SEQUENCE_NUMBER` int(11) NOT NULL,
  `DOSING_TEXT` longtext NOT NULL,
  `DISPLAY_ORDER` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`SECTION_ID`,`SEQUENCE_NUMBER`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_DOSING_CONDITION
CREATE TABLE IF NOT EXISTS `REF_DOSING_CONDITION` (
  `DRUG_ID` varchar(10) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `SEQUENCE_NUMBER` int(11) NOT NULL,
  `CONDITION_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`SECTION_ID`,`SEQUENCE_NUMBER`,`CONDITION_ID`),
  KEY `IDX_REF_DOSING_CONDITION` (`CONDITION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_DOSING_REF
CREATE TABLE IF NOT EXISTS `REF_DOSING_REF` (
  `DRUG_ID` varchar(10) NOT NULL,
  `REFERENCE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`REFERENCE_ID`),
  KEY `IDX_REF_DOSING_REF` (`REFERENCE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_DOSING_SECTION
CREATE TABLE IF NOT EXISTS `REF_DOSING_SECTION` (
  `SECTION_ID` int(11) NOT NULL,
  `SECTION_NAME` varchar(30) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`SECTION_ID`),
  UNIQUE KEY `SECTION_NAME` (`SECTION_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_DRUG_FOOD_GENDRUG
CREATE TABLE IF NOT EXISTS `REF_DRUG_FOOD_GENDRUG` (
  `DRUG_ID` varchar(10) NOT NULL,
  `DRUG_INTERACTION_ID` int(11) NOT NULL,
  `INT_SEVERITY_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`DRUG_INTERACTION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_GENERALWARNING
CREATE TABLE IF NOT EXISTS `REF_GENERALWARNING` (
  `DRUG_ID` varchar(10) NOT NULL,
  `GENERAL_WARNING_ID` int(11) NOT NULL,
  `WARNING_MESSAGE_TEXT` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`GENERAL_WARNING_ID`),
  UNIQUE KEY `GENERAL_WARNING_ID` (`GENERAL_WARNING_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_GENERALWARNING_REF
CREATE TABLE IF NOT EXISTS `REF_GENERALWARNING_REF` (
  `DRUG_ID` varchar(10) NOT NULL,
  `REFERENCE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`REFERENCE_ID`),
  KEY `IDX_REF_GENERALWARNING_REF` (`REFERENCE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_LACTWARNING
CREATE TABLE IF NOT EXISTS `REF_LACTWARNING` (
  `DRUG_ID` varchar(10) NOT NULL,
  `LACTWARNING_ID` int(11) NOT NULL,
  `SPECIFICITY_LEVEL` int(11) NOT NULL,
  `LACT_SIGNIFICANCE_ID` int(11) NOT NULL,
  `LACT_MESSAGE_TEXT` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`LACTWARNING_ID`),
  UNIQUE KEY `LACTWARNING_ID` (`LACTWARNING_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_LACTWARNING_REF
CREATE TABLE IF NOT EXISTS `REF_LACTWARNING_REF` (
  `DRUG_ID` varchar(10) NOT NULL,
  `REFERENCE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`REFERENCE_ID`),
  KEY `IDX_REF_LACTWARNING_REF` (`REFERENCE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_LACTWARNING_SIGNIFICANCE
CREATE TABLE IF NOT EXISTS `REF_LACTWARNING_SIGNIFICANCE` (
  `LACT_SIGNIFICANCE_ID` int(11) NOT NULL,
  `LACT_SIGNIFICANCE_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`LACT_SIGNIFICANCE_ID`),
  UNIQUE KEY `LACT_SIGNIFICANCE_DESCRIPTION` (`LACT_SIGNIFICANCE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_MONOGRAPH
CREATE TABLE IF NOT EXISTS `REF_MONOGRAPH` (
  `MONOGRAPH_ID` int(11) NOT NULL,
  `MONO_TYPE_ID` int(11) NOT NULL,
  `MONO_SOURCE_ID` int(11) NOT NULL,
  `MONOGRAPH_TITLE` varchar(110) NOT NULL,
  `VERSION_NUMBER` decimal(5,3) NOT NULL,
  `DATE_REVISED` date NOT NULL,
  `FILE_NAME` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MONOGRAPH_ID`,`MONO_TYPE_ID`),
  UNIQUE KEY `FILE_NAME` (`FILE_NAME`),
  KEY `IDX_REF_MONOGRAPH` (`MONOGRAPH_TITLE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_MONOGRAPH_GENDRUG
CREATE TABLE IF NOT EXISTS `REF_MONOGRAPH_GENDRUG` (
  `MONOGRAPH_ID` int(11) NOT NULL,
  `DRUG_ID` varchar(10) NOT NULL,
  `MONO_TYPE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MONOGRAPH_ID`,`DRUG_ID`,`MONO_TYPE_ID`),
  KEY `IDX_REF_MONOGRAPH_GENDRUG` (`DRUG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_MONOGRAPH_GENDRUG_LOCAL
CREATE TABLE IF NOT EXISTS `REF_MONOGRAPH_GENDRUG_LOCAL` (
  `DRUG_ID` varchar(50) NOT NULL,
  `LOCAL_MONOGRAPH_ID` int(11) NOT NULL,
  `LOCAL_DATASET_ID` int(11) NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`LOCAL_MONOGRAPH_ID`,`LOCAL_DATASET_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_MONOGRAPH_GENPRODUCT
CREATE TABLE IF NOT EXISTS `REF_MONOGRAPH_GENPRODUCT` (
  `MONOGRAPH_ID` int(11) NOT NULL,
  `GENPRODUCT_ID` int(11) NOT NULL,
  `MONO_TYPE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MONOGRAPH_ID`,`GENPRODUCT_ID`,`MONO_TYPE_ID`),
  KEY `IDX_REF_MONOGRAPH_GENPRODUCT` (`GENPRODUCT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_MONOGRAPH_LOCAL
CREATE TABLE IF NOT EXISTS `REF_MONOGRAPH_LOCAL` (
  `LOCAL_MONOGRAPH_ID` int(11) NOT NULL,
  `LOCAL_TYPE_DESCRIPTION` varchar(255) DEFAULT NULL,
  `MONOGRAPH_TITLE` varchar(255) DEFAULT NULL,
  `VERSION_NUMBER` int(11) DEFAULT NULL,
  `REVISION_DATE` date DEFAULT NULL,
  `MONOGRAPH_URL` varchar(255) DEFAULT NULL,
  `SUPPORT_URL` varchar(255) DEFAULT NULL,
  `LAST_REVISED` date DEFAULT NULL,
  PRIMARY KEY (`LOCAL_MONOGRAPH_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_MONO_SOURCE
CREATE TABLE IF NOT EXISTS `REF_MONO_SOURCE` (
  `MONO_SOURCE_ID` int(11) NOT NULL,
  `MONO_SOURCE_NAME` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MONO_SOURCE_ID`),
  UNIQUE KEY `MONO_SOURCE_NAME` (`MONO_SOURCE_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_MONO_TYPE
CREATE TABLE IF NOT EXISTS `REF_MONO_TYPE` (
  `MONO_TYPE_ID` int(11) NOT NULL,
  `MONO_TYPE_DESCRIPTION` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`MONO_TYPE_ID`),
  UNIQUE KEY `MONO_TYPE_DESCRIPTION` (`MONO_TYPE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_PHARMACOLOGY
CREATE TABLE IF NOT EXISTS `REF_PHARMACOLOGY` (
  `DRUG_ID` varchar(10) NOT NULL,
  `PHARMACOLOGY_ID` int(11) NOT NULL,
  `PHARMACOLOGY_TEXT` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`PHARMACOLOGY_ID`),
  UNIQUE KEY `PHARMACOLOGY_ID` (`PHARMACOLOGY_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_PHARMACOLOGY_REF
CREATE TABLE IF NOT EXISTS `REF_PHARMACOLOGY_REF` (
  `DRUG_ID` varchar(10) NOT NULL,
  `REFERENCE_ID` int(11) NOT NULL,
  `REFERENCE_DESCRIPTION` varchar(80) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`REFERENCE_ID`,`REFERENCE_DESCRIPTION`),
  KEY `IDX_REF_PHARMACOLOGY_REF` (`REFERENCE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_PREGWARNING
CREATE TABLE IF NOT EXISTS `REF_PREGWARNING` (
  `DRUG_ID` varchar(10) NOT NULL,
  `PREGWARNING_ID` int(11) NOT NULL,
  `SPECIFICITY_LEVEL` int(11) NOT NULL,
  `PREG_SIGNIFICANCE_ID` int(11) NOT NULL,
  `PREG_MESSAGE_TEXT` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`PREGWARNING_ID`),
  UNIQUE KEY `PREGWARNING_ID` (`PREGWARNING_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_PREGWARNING_REF
CREATE TABLE IF NOT EXISTS `REF_PREGWARNING_REF` (
  `DRUG_ID` varchar(10) NOT NULL,
  `REFERENCE_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`REFERENCE_ID`),
  KEY `IDX_REF_PREGWARNING_REF` (`REFERENCE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_PREGWARNING_SIGNIFICANCE
CREATE TABLE IF NOT EXISTS `REF_PREGWARNING_SIGNIFICANCE` (
  `PREG_SIGNIFICANCE_ID` int(11) NOT NULL,
  `PREG_SIGNIFICANCE_DESCRIPTION` varchar(50) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`PREG_SIGNIFICANCE_ID`),
  UNIQUE KEY `PREG_SIGNIFICANCE_DESCRIPTION` (`PREG_SIGNIFICANCE_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_REFERENCE
CREATE TABLE IF NOT EXISTS `REF_REFERENCE` (
  `REFERENCE_ID` int(11) NOT NULL,
  `AUTHOR_NAMES` varchar(255) DEFAULT NULL,
  `REFERENCE_TITLE` varchar(255) NOT NULL,
  `PAGES` varchar(64) DEFAULT NULL,
  `VOLUME_ISSUE` varchar(20) DEFAULT NULL,
  `YEAR_COMPLETE` varchar(15) DEFAULT NULL,
  `JOURNAL_ABBREVIATION` varchar(100) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`REFERENCE_ID`),
  KEY `IDX_REF_REFERENCE` (`REFERENCE_TITLE`),
  KEY `IDX_REF_REFERENCE_2` (`PAGES`),
  KEY `IDX_REF_REFERENCE_3` (`AUTHOR_NAMES`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_SIDE_EFFECT
CREATE TABLE IF NOT EXISTS `REF_SIDE_EFFECT` (
  `DRUG_ID` varchar(10) NOT NULL,
  `SIDE_EFFECT_ID` int(11) NOT NULL,
  `SPECIFICITY_LEVEL` int(11) NOT NULL,
  `SYSTEM_ID` int(11) NOT NULL,
  `DISPLAY_ORDER` int(11) NOT NULL,
  `SIDE_EFFECT_TEXT` longtext NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`SIDE_EFFECT_ID`),
  UNIQUE KEY `SIDE_EFFECT_ID` (`SIDE_EFFECT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_SIDE_EFFECT_REF
CREATE TABLE IF NOT EXISTS `REF_SIDE_EFFECT_REF` (
  `DRUG_ID` varchar(10) NOT NULL,
  `REFERENCE_ID` int(11) NOT NULL,
  `REFERENCE_DESCRIPTION` varchar(80) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`REFERENCE_ID`,`REFERENCE_DESCRIPTION`),
  KEY `IDX_REF_SIDE_EFFECT_REF` (`REFERENCE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_SIDE_EFFECT_SYSTEM
CREATE TABLE IF NOT EXISTS `REF_SIDE_EFFECT_SYSTEM` (
  `SYSTEM_ID` int(11) NOT NULL,
  `SYSTEM_DESCRIPTION` varchar(30) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`SYSTEM_ID`),
  UNIQUE KEY `SYSTEM_DESCRIPTION` (`SYSTEM_DESCRIPTION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_WARNING_LABEL
CREATE TABLE IF NOT EXISTS `REF_WARNING_LABEL` (
  `WARNING_LABEL_ID` int(11) NOT NULL,
  `ENGLISH_SHORT_TEXT` varchar(50) NOT NULL,
  `ENGLISH_FULL_TEXT` varchar(150) NOT NULL,
  `SPANISH_SHORT_TEXT` varchar(70) NOT NULL,
  `SPANISH_FULL_TEXT` varchar(200) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`WARNING_LABEL_ID`),
  UNIQUE KEY `SPANISH_FULL_TEXT` (`SPANISH_FULL_TEXT`),
  UNIQUE KEY `SPANISH_SHORT_TEXT` (`SPANISH_SHORT_TEXT`),
  UNIQUE KEY `ENGLISH_SHORT_TEXT` (`ENGLISH_SHORT_TEXT`),
  UNIQUE KEY `ENGLISH_FULL_TEXT` (`ENGLISH_FULL_TEXT`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.REF_WARNING_LABEL_GENPRODUCT
CREATE TABLE IF NOT EXISTS `REF_WARNING_LABEL_GENPRODUCT` (
  `WARNING_LABEL_ID` int(11) NOT NULL,
  `GENPRODUCT_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`WARNING_LABEL_ID`,`GENPRODUCT_ID`),
  KEY `IDX_REF_WARNING_LABEL_GENPRODUCT` (`GENPRODUCT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.VERSION_INFO
CREATE TABLE IF NOT EXISTS `VERSION_INFO` (
  `DB_VERSION` varchar(30) NOT NULL,
  `BUILD_VERSION` varchar(30) NOT NULL,
  `API_VERSION` varchar(30) NOT NULL,
  `CONTENT_DATE` date NOT NULL,
  `CREATE_DATE` date DEFAULT NULL,
  `UPDATE_DATE` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.XREF_GENDRUG_RXNORM
CREATE TABLE IF NOT EXISTS `XREF_GENDRUG_RXNORM` (
  `DRUG_ID` varchar(10) NOT NULL,
  `RXCUI` varchar(8) NOT NULL,
  `RXNORM_TTY` varchar(20) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`DRUG_ID`,`RXCUI`,`RXNORM_TTY`),
  KEY `IDX_XREF_GENDRUG_RXNORM` (`RXCUI`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.XREF_GENPRODUCT_RXNORM
CREATE TABLE IF NOT EXISTS `XREF_GENPRODUCT_RXNORM` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `RXCUI` varchar(8) NOT NULL,
  `RXNORM_TTY` varchar(20) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`,`RXCUI`,`RXNORM_TTY`),
  KEY `IDX_XREF_GENPRODUCT_RXNORM` (`RXCUI`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.XREF_GENPRODUCT_SYN_RXNORM
CREATE TABLE IF NOT EXISTS `XREF_GENPRODUCT_SYN_RXNORM` (
  `GENPRODUCT_ID` int(11) NOT NULL,
  `RXCUI` varchar(8) NOT NULL,
  `RXNORM_TTY` varchar(20) NOT NULL,
  `DRUG_SYN_ID` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`GENPRODUCT_ID`,`RXCUI`,`RXNORM_TTY`,`DRUG_SYN_ID`),
  KEY `IDX_XREF_GENPRODUCT_SYN_RXNORM` (`RXCUI`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table lexi-data.XREF_SYNONYM_RXNORM
CREATE TABLE IF NOT EXISTS `XREF_SYNONYM_RXNORM` (
  `SYNONYM_ID` int(11) NOT NULL,
  `RXCUI` varchar(8) NOT NULL,
  `RXNORM_TTY` varchar(20) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_DATE` date NOT NULL,
  PRIMARY KEY (`SYNONYM_ID`,`RXCUI`,`RXNORM_TTY`),
  KEY `IDX_XREF_SYNONYM_RXNORM` (`RXCUI`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for view lexi-data.NDC_DENORM
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `NDC_DENORM`;
CREATE ALGORITHM=UNDEFINED DEFINER=`@gencyAdmin`@`%` SQL SECURITY DEFINER VIEW `lexi-data`.`NDC_DENORM` AS select `NPP`.`PKG_PRODUCT_ID` AS `PKG_PRODUCT_ID`,`NTN`.`TRADE_NAME` AS `TRADE_NAME`,`NPP`.`GENPRODUCT_ID` AS `GENPRODUCT_ID`,`CR`.`ROUTE_DESCRIPTION` AS `ROUTE_DESCRIPTION`,`CD`.`DOSEFORM_DESCRIPTION` AS `DOSEFORM_DESCRIPTION`,`CP`.`STRENGTH_DESCRIPTION` AS `STRENGTH_DESCRIPTION`,`CG`.`DRUG_ID` AS `DRUG_ID`,`CAI`.`ACTIVE_INGRED_NAME` AS `ACTIVE_INGRED_NAME`,`CIS`.`NUMERATOR_AMOUNT` AS `NUMERATOR_AMOUNT`,`CU2`.`UNIT_DESCRIPTION` AS `NUMERATOR_UNIT`,`CIS`.`DENOMINATOR_AMOUNT` AS `DENOMINATOR_AMOUNT`,`CU3`.`UNIT_DESCRIPTION` AS `DENOMINATOR_UNIT`,`CG`.`CSA_CODE` AS `CSA_CODE`,`CG`.`JCODE` AS `JCODE`,`CG`.`JCODE_DESCRIPTION` AS `JCODE_DESCRIPTION`,`NPP`.`RX_OTC_STATUS_CODE` AS `RX_OTC_STATUS_CODE`,`NPP`.`INNER_PACK_SIZE` AS `INNER_PACK_SIZE`,`CU1`.`UNIT_DESCRIPTION` AS `INNER_PACK_UNIT`,`NPP`.`OUTER_PACK_SIZE` AS `OUTER_PACK_SIZE`,`NPP`.`DATE_OBSOLETE` AS `DATE_OBSOLETE`,`NL`.`LABELER_NAME` AS `LABELER_NAME`,`NL`.`ADDRESS_1` AS `ADDRESS_1`,`NL`.`ADDRESS_2` AS `ADDRESS_2`,`NL`.`CITY` AS `CITY`,`NL`.`STATE` AS `STATE`,`NL`.`PROVINCE` AS `PROVINCE`,`NL`.`POSTAL_CODE` AS `POSTAL_CODE`,`NL`.`COUNTRY` AS `COUNTRY`,`NOB`.`ORANGE_BOOK_DESCRIPTION` AS `ORANGE_BOOK_DESCRIPTION`,`NPP`.`UNIT_DOSE_CODE` AS `UNIT_DOSE_CODE`,`NPP`.`IS_REPACKAGED` AS `IS_REPACKAGED`,`NPP`.`BRAND_GENERIC_CODE` AS `BRAND_GENERIC_CODE`,`NPPP1`.`PRICE` AS `AWP`,`NPPP2`.`PRICE` AS `FUL`,`NPPP3`.`PRICE` AS `MDR`,`NPPP4`.`PRICE` AS `WSC` from (((((((((((((((((`lexi-data`.`NDC_PKG_PRODUCT` `NPP` join `lexi-data`.`NDC_TRADE_NAME` `NTN` on((`NPP`.`TRADE_NAME_ID` = `NTN`.`TRADE_NAME_ID`))) join `lexi-data`.`NDC_ORANGE_BOOK` `NOB` on((`NPP`.`ORANGE_BOOK_ID` = `NOB`.`ORANGE_BOOK_ID`))) join `lexi-data`.`NDC_LABELER` `NL` on((`NPP`.`LABELER_ID` = `NL`.`LABELER_ID`))) left join `lexi-data`.`CORE_UNIT` `CU1` on((`NPP`.`INNER_PACK_UNIT_ID` = `CU1`.`UNIT_ID`))) left join `lexi-data`.`NDC_PKG_PRODUCT_PRICE` `NPPP1` on(((`NPP`.`PKG_PRODUCT_ID` = `NPPP1`.`PKG_PRODUCT_ID`) and (`NPPP1`.`PRICE_TYPE_CODE` = 'A')))) left join `lexi-data`.`NDC_PKG_PRODUCT_PRICE` `NPPP2` on(((`NPP`.`PKG_PRODUCT_ID` = `NPPP2`.`PKG_PRODUCT_ID`) and (`NPPP2`.`PRICE_TYPE_CODE` = 'F')))) left join `lexi-data`.`NDC_PKG_PRODUCT_PRICE` `NPPP3` on(((`NPP`.`PKG_PRODUCT_ID` = `NPPP3`.`PKG_PRODUCT_ID`) and (`NPPP3`.`PRICE_TYPE_CODE` = 'M')))) left join `lexi-data`.`NDC_PKG_PRODUCT_PRICE` `NPPP4` on(((`NPP`.`PKG_PRODUCT_ID` = `NPPP4`.`PKG_PRODUCT_ID`) and (`NPPP4`.`PRICE_TYPE_CODE` = 'W')))) join `lexi-data`.`CORE_GENPRODUCT` `CG` on((`NPP`.`GENPRODUCT_ID` = `CG`.`GENPRODUCT_ID`))) join `lexi-data`.`CORE_ROUTE` `CR` on((`CG`.`ROUTE_ID` = `CR`.`ROUTE_ID`))) join `lexi-data`.`CORE_DOSEFORM` `CD` on((`CG`.`DOSEFORM_ID` = `CD`.`DOSEFORM_ID`))) join `lexi-data`.`CORE_PRODUCTSTRENGTH` `CP` on((`CG`.`STRENGTH_ID` = `CP`.`STRENGTH_ID`))) join `lexi-data`.`CORE_GENPRODUCT_INGREDIENT` `CGI` on((`CG`.`GENPRODUCT_ID` = `CGI`.`GENPRODUCT_ID`))) join `lexi-data`.`CORE_INGREDIENT_STRENGTH` `CIS` on((`CGI`.`INGRED_STRENGTH_ID` = `CIS`.`INGRED_STRENGTH_ID`))) left join `lexi-data`.`CORE_UNIT` `CU2` on((`CIS`.`NUMERATOR_UNIT_ID` = `CU2`.`UNIT_ID`))) left join `lexi-data`.`CORE_UNIT` `CU3` on((`CIS`.`DENOMINATOR_UNIT_ID` = `CU3`.`UNIT_ID`))) join `lexi-data`.`CORE_ACTIVE_INGRED` `CAI` on((`CGI`.`ACTIVE_INGRED_ID` = `CAI`.`ACTIVE_INGRED_ID`)));


-- Dumping database structure for mysql
CREATE DATABASE IF NOT EXISTS `mysql` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mysql`;


-- Dumping structure for table mysql.columns_priv
CREATE TABLE IF NOT EXISTS `columns_priv` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Table_name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Column_name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Column_priv` set('Select','Insert','Update','References') CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`Host`,`Db`,`User`,`Table_name`,`Column_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column privileges';

-- Data exporting was unselected.


-- Dumping structure for table mysql.db
CREATE TABLE IF NOT EXISTS `db` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Select_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Insert_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Update_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Delete_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Drop_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Grant_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `References_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Index_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_tmp_table_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Lock_tables_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Show_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Execute_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Event_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Trigger_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Host`,`Db`,`User`),
  KEY `User` (`User`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database privileges';

-- Data exporting was unselected.


-- Dumping structure for table mysql.event
CREATE TABLE IF NOT EXISTS `event` (
  `db` char(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `name` char(64) NOT NULL DEFAULT '',
  `body` longblob NOT NULL,
  `definer` char(77) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `execute_at` datetime DEFAULT NULL,
  `interval_value` int(11) DEFAULT NULL,
  `interval_field` enum('YEAR','QUARTER','MONTH','DAY','HOUR','MINUTE','WEEK','SECOND','MICROSECOND','YEAR_MONTH','DAY_HOUR','DAY_MINUTE','DAY_SECOND','HOUR_MINUTE','HOUR_SECOND','MINUTE_SECOND','DAY_MICROSECOND','HOUR_MICROSECOND','MINUTE_MICROSECOND','SECOND_MICROSECOND') DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_executed` datetime DEFAULT NULL,
  `starts` datetime DEFAULT NULL,
  `ends` datetime DEFAULT NULL,
  `status` enum('ENABLED','DISABLED','SLAVESIDE_DISABLED') NOT NULL DEFAULT 'ENABLED',
  `on_completion` enum('DROP','PRESERVE') NOT NULL DEFAULT 'DROP',
  `sql_mode` set('REAL_AS_FLOAT','PIPES_AS_CONCAT','ANSI_QUOTES','IGNORE_SPACE','NOT_USED','ONLY_FULL_GROUP_BY','NO_UNSIGNED_SUBTRACTION','NO_DIR_IN_CREATE','POSTGRESQL','ORACLE','MSSQL','DB2','MAXDB','NO_KEY_OPTIONS','NO_TABLE_OPTIONS','NO_FIELD_OPTIONS','MYSQL323','MYSQL40','ANSI','NO_AUTO_VALUE_ON_ZERO','NO_BACKSLASH_ESCAPES','STRICT_TRANS_TABLES','STRICT_ALL_TABLES','NO_ZERO_IN_DATE','NO_ZERO_DATE','INVALID_DATES','ERROR_FOR_DIVISION_BY_ZERO','TRADITIONAL','NO_AUTO_CREATE_USER','HIGH_NOT_PRECEDENCE','NO_ENGINE_SUBSTITUTION','PAD_CHAR_TO_FULL_LENGTH') NOT NULL DEFAULT '',
  `comment` char(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `originator` int(10) unsigned NOT NULL,
  `time_zone` char(64) CHARACTER SET latin1 NOT NULL DEFAULT 'SYSTEM',
  `character_set_client` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `collation_connection` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `db_collation` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `body_utf8` longblob,
  PRIMARY KEY (`db`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Events';

-- Data exporting was unselected.


-- Dumping structure for table mysql.func
CREATE TABLE IF NOT EXISTS `func` (
  `name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ret` tinyint(1) NOT NULL DEFAULT '0',
  `dl` char(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  `type` enum('function','aggregate') CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User defined functions';

-- Data exporting was unselected.


-- Dumping structure for table mysql.general_log
CREATE TABLE IF NOT EXISTS `general_log` (
  `event_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_host` mediumtext NOT NULL,
  `thread_id` int(11) NOT NULL,
  `server_id` int(10) unsigned NOT NULL,
  `command_type` varchar(64) NOT NULL,
  `argument` mediumtext NOT NULL
) ENGINE=CSV DEFAULT CHARSET=utf8 COMMENT='General log';

-- Data exporting was unselected.


-- Dumping structure for table mysql.help_category
CREATE TABLE IF NOT EXISTS `help_category` (
  `help_category_id` smallint(5) unsigned NOT NULL,
  `name` char(64) NOT NULL,
  `parent_category_id` smallint(5) unsigned DEFAULT NULL,
  `url` char(128) NOT NULL,
  PRIMARY KEY (`help_category_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='help categories';

-- Data exporting was unselected.


-- Dumping structure for table mysql.help_keyword
CREATE TABLE IF NOT EXISTS `help_keyword` (
  `help_keyword_id` int(10) unsigned NOT NULL,
  `name` char(64) NOT NULL,
  PRIMARY KEY (`help_keyword_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='help keywords';

-- Data exporting was unselected.


-- Dumping structure for table mysql.help_relation
CREATE TABLE IF NOT EXISTS `help_relation` (
  `help_topic_id` int(10) unsigned NOT NULL,
  `help_keyword_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`help_keyword_id`,`help_topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='keyword-topic relation';

-- Data exporting was unselected.


-- Dumping structure for table mysql.help_topic
CREATE TABLE IF NOT EXISTS `help_topic` (
  `help_topic_id` int(10) unsigned NOT NULL,
  `name` char(64) NOT NULL,
  `help_category_id` smallint(5) unsigned NOT NULL,
  `description` text NOT NULL,
  `example` text NOT NULL,
  `url` char(128) NOT NULL,
  PRIMARY KEY (`help_topic_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='help topics';

-- Data exporting was unselected.


-- Dumping structure for table mysql.host
CREATE TABLE IF NOT EXISTS `host` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Select_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Insert_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Update_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Delete_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Drop_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Grant_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `References_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Index_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_tmp_table_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Lock_tables_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Show_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Execute_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Trigger_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Host`,`Db`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Host privileges;  Merged with database privileges';

-- Data exporting was unselected.


-- Dumping structure for table mysql.ndb_binlog_index
CREATE TABLE IF NOT EXISTS `ndb_binlog_index` (
  `Position` bigint(20) unsigned NOT NULL,
  `File` varchar(255) NOT NULL,
  `epoch` bigint(20) unsigned NOT NULL,
  `inserts` bigint(20) unsigned NOT NULL,
  `updates` bigint(20) unsigned NOT NULL,
  `deletes` bigint(20) unsigned NOT NULL,
  `schemaops` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`epoch`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table mysql.plugin
CREATE TABLE IF NOT EXISTS `plugin` (
  `name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `dl` char(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='MySQL plugins';

-- Data exporting was unselected.


-- Dumping structure for table mysql.proc
CREATE TABLE IF NOT EXISTS `proc` (
  `db` char(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `name` char(64) NOT NULL DEFAULT '',
  `type` enum('FUNCTION','PROCEDURE') NOT NULL,
  `specific_name` char(64) NOT NULL DEFAULT '',
  `language` enum('SQL') NOT NULL DEFAULT 'SQL',
  `sql_data_access` enum('CONTAINS_SQL','NO_SQL','READS_SQL_DATA','MODIFIES_SQL_DATA') NOT NULL DEFAULT 'CONTAINS_SQL',
  `is_deterministic` enum('YES','NO') NOT NULL DEFAULT 'NO',
  `security_type` enum('INVOKER','DEFINER') NOT NULL DEFAULT 'DEFINER',
  `param_list` blob NOT NULL,
  `returns` longblob NOT NULL,
  `body` longblob NOT NULL,
  `definer` char(77) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sql_mode` set('REAL_AS_FLOAT','PIPES_AS_CONCAT','ANSI_QUOTES','IGNORE_SPACE','NOT_USED','ONLY_FULL_GROUP_BY','NO_UNSIGNED_SUBTRACTION','NO_DIR_IN_CREATE','POSTGRESQL','ORACLE','MSSQL','DB2','MAXDB','NO_KEY_OPTIONS','NO_TABLE_OPTIONS','NO_FIELD_OPTIONS','MYSQL323','MYSQL40','ANSI','NO_AUTO_VALUE_ON_ZERO','NO_BACKSLASH_ESCAPES','STRICT_TRANS_TABLES','STRICT_ALL_TABLES','NO_ZERO_IN_DATE','NO_ZERO_DATE','INVALID_DATES','ERROR_FOR_DIVISION_BY_ZERO','TRADITIONAL','NO_AUTO_CREATE_USER','HIGH_NOT_PRECEDENCE','NO_ENGINE_SUBSTITUTION','PAD_CHAR_TO_FULL_LENGTH') NOT NULL DEFAULT '',
  `comment` char(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `character_set_client` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `collation_connection` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `db_collation` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `body_utf8` longblob,
  PRIMARY KEY (`db`,`name`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Stored Procedures';

-- Data exporting was unselected.


-- Dumping structure for table mysql.procs_priv
CREATE TABLE IF NOT EXISTS `procs_priv` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Routine_name` char(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Routine_type` enum('FUNCTION','PROCEDURE') COLLATE utf8_bin NOT NULL,
  `Grantor` char(77) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Proc_priv` set('Execute','Alter Routine','Grant') CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Host`,`Db`,`User`,`Routine_name`,`Routine_type`),
  KEY `Grantor` (`Grantor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Procedure privileges';

-- Data exporting was unselected.


-- Dumping structure for table mysql.servers
CREATE TABLE IF NOT EXISTS `servers` (
  `Server_name` char(64) NOT NULL DEFAULT '',
  `Host` char(64) NOT NULL DEFAULT '',
  `Db` char(64) NOT NULL DEFAULT '',
  `Username` char(64) NOT NULL DEFAULT '',
  `Password` char(64) NOT NULL DEFAULT '',
  `Port` int(4) NOT NULL DEFAULT '0',
  `Socket` char(64) NOT NULL DEFAULT '',
  `Wrapper` char(64) NOT NULL DEFAULT '',
  `Owner` char(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`Server_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='MySQL Foreign Servers table';

-- Data exporting was unselected.


-- Dumping structure for table mysql.slow_log
CREATE TABLE IF NOT EXISTS `slow_log` (
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_host` mediumtext NOT NULL,
  `query_time` time NOT NULL,
  `lock_time` time NOT NULL,
  `rows_sent` int(11) NOT NULL,
  `rows_examined` int(11) NOT NULL,
  `db` varchar(512) NOT NULL,
  `last_insert_id` int(11) NOT NULL,
  `insert_id` int(11) NOT NULL,
  `server_id` int(10) unsigned NOT NULL,
  `sql_text` mediumtext NOT NULL
) ENGINE=CSV DEFAULT CHARSET=utf8 COMMENT='Slow log';

-- Data exporting was unselected.


-- Dumping structure for table mysql.tables_priv
CREATE TABLE IF NOT EXISTS `tables_priv` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Table_name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Grantor` char(77) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Table_priv` set('Select','Insert','Update','Delete','Create','Drop','Grant','References','Index','Alter','Create View','Show view','Trigger') CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Column_priv` set('Select','Insert','Update','References') CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`Host`,`Db`,`User`,`Table_name`),
  KEY `Grantor` (`Grantor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table privileges';

-- Data exporting was unselected.


-- Dumping structure for table mysql.time_zone
CREATE TABLE IF NOT EXISTS `time_zone` (
  `Time_zone_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Use_leap_seconds` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Time_zone_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Time zones';

-- Data exporting was unselected.


-- Dumping structure for table mysql.time_zone_leap_second
CREATE TABLE IF NOT EXISTS `time_zone_leap_second` (
  `Transition_time` bigint(20) NOT NULL,
  `Correction` int(11) NOT NULL,
  PRIMARY KEY (`Transition_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Leap seconds information for time zones';

-- Data exporting was unselected.


-- Dumping structure for table mysql.time_zone_name
CREATE TABLE IF NOT EXISTS `time_zone_name` (
  `Name` char(64) NOT NULL,
  `Time_zone_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Time zone names';

-- Data exporting was unselected.


-- Dumping structure for table mysql.time_zone_transition
CREATE TABLE IF NOT EXISTS `time_zone_transition` (
  `Time_zone_id` int(10) unsigned NOT NULL,
  `Transition_time` bigint(20) NOT NULL,
  `Transition_type_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Time_zone_id`,`Transition_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Time zone transitions';

-- Data exporting was unselected.


-- Dumping structure for table mysql.time_zone_transition_type
CREATE TABLE IF NOT EXISTS `time_zone_transition_type` (
  `Time_zone_id` int(10) unsigned NOT NULL,
  `Transition_type_id` int(10) unsigned NOT NULL,
  `Offset` int(11) NOT NULL DEFAULT '0',
  `Is_DST` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Abbreviation` char(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`Time_zone_id`,`Transition_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Time zone transition types';

-- Data exporting was unselected.


-- Dumping structure for table mysql.user
CREATE TABLE IF NOT EXISTS `user` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Password` char(41) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `Select_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Insert_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Update_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Delete_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Drop_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Reload_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Shutdown_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Process_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `File_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Grant_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `References_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Index_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Show_db_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Super_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_tmp_table_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Lock_tables_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Execute_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Repl_slave_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Repl_client_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Show_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_user_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Event_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Trigger_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `ssl_type` enum('','ANY','X509','SPECIFIED') CHARACTER SET utf8 NOT NULL DEFAULT '',
  `ssl_cipher` blob NOT NULL,
  `x509_issuer` blob NOT NULL,
  `x509_subject` blob NOT NULL,
  `max_questions` int(11) unsigned NOT NULL DEFAULT '0',
  `max_updates` int(11) unsigned NOT NULL DEFAULT '0',
  `max_connections` int(11) unsigned NOT NULL DEFAULT '0',
  `max_user_connections` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Host`,`User`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and global privileges';

-- Data exporting was unselected.


-- Dumping structure for table mysql.user_info
CREATE TABLE IF NOT EXISTS `user_info` (
  `User` varchar(16) COLLATE utf8_bin NOT NULL,
  `Full_name` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `Description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Email` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `Contact_information` text COLLATE utf8_bin,
  `Icon` blob,
  PRIMARY KEY (`User`),
  KEY `user_info_Full_name` (`Full_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Stores additional user information';

-- Data exporting was unselected.


-- Dumping database structure for oasisc
CREATE DATABASE IF NOT EXISTS `oasisc` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `oasisc`;


-- Dumping structure for table oasisc.assessments
CREATE TABLE IF NOT EXISTS `assessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) NOT NULL,
  `IsValidated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `PatientId` (`PatientId`),
  KEY `AgencyId` (`AgencyId`),
  KEY `Status` (`Status`),
  KEY `ExportedDate` (`ExportedDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.deathathomeassessments
CREATE TABLE IF NOT EXISTS `deathathomeassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'DischargeFromAgencyDeath',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.dischargefromagencyassessments
CREATE TABLE IF NOT EXISTS `dischargefromagencyassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'DischargeFromAgency',
  `Version` int(10) unsigned DEFAULT '1',
  `IsValidated` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.followupassessments
CREATE TABLE IF NOT EXISTS `followupassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'FollowUp',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.nonoasisdischargeassessments
CREATE TABLE IF NOT EXISTS `nonoasisdischargeassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'NonOasisDischarge',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.nonoasisrecertificationassessments
CREATE TABLE IF NOT EXISTS `nonoasisrecertificationassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned NOT NULL DEFAULT '1',
  `VersionNumber` int(10) unsigned NOT NULL DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'NonOasisRecertification',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.nonoasisstartofcareassessments
CREATE TABLE IF NOT EXISTS `nonoasisstartofcareassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'NonOasisStartOfCare',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.oasisbackups
CREATE TABLE IF NOT EXISTS `oasisbackups` (
  `Id` char(36) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `AssessmentId` char(36) DEFAULT NULL,
  `Data` text,
  `Created` datetime DEFAULT NULL,
  KEY `AssessmentId` (`AssessmentId`),
  KEY `Created` (`Created`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.oasisguides
CREATE TABLE IF NOT EXISTS `oasisguides` (
  `Id` char(5) NOT NULL,
  `ItemIntent` mediumtext NOT NULL,
  `Response` mediumtext NOT NULL,
  `DataSources` mediumtext NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.planofcares
CREATE TABLE IF NOT EXISTS `planofcares` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL DEFAULT '',
  `PatientId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `UserId` char(36) NOT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `Data` text,
  `Status` int(11) NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `SentDate` date DEFAULT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AssessmentId` char(36) NOT NULL,
  `AssessmentType` varchar(50) NOT NULL,
  `PhysicianData` text,
  `IsNonOasis` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`,`EpisodeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.planofcarestandalones
CREATE TABLE IF NOT EXISTS `planofcarestandalones` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `Data` text,
  `Status` int(11) NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `SentDate` date DEFAULT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PhysicianData` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.recertificationassessments
CREATE TABLE IF NOT EXISTS `recertificationassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned NOT NULL DEFAULT '1',
  `VersionNumber` int(10) unsigned NOT NULL DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'Recertification',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.resumptionofcareassessments
CREATE TABLE IF NOT EXISTS `resumptionofcareassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'ResumptionOfCare',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.startofcareassessments
CREATE TABLE IF NOT EXISTS `startofcareassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'StartOfCare',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.submissionbodyformats
CREATE TABLE IF NOT EXISTS `submissionbodyformats` (
  `Id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(255) DEFAULT NULL,
  `Length` float unsigned DEFAULT NULL,
  `Start` float unsigned DEFAULT NULL,
  `End` float unsigned DEFAULT NULL,
  `RFA01` float unsigned DEFAULT NULL,
  `RFA02` float unsigned DEFAULT NULL,
  `RFA03` float unsigned DEFAULT NULL,
  `RFA04` float unsigned DEFAULT NULL,
  `RFA05` float unsigned DEFAULT NULL,
  `RFA06` float unsigned DEFAULT NULL,
  `RFA07` float unsigned DEFAULT NULL,
  `RFA08` float unsigned DEFAULT NULL,
  `RFA09` float unsigned DEFAULT NULL,
  `RFA10` float unsigned DEFAULT NULL,
  `RFA_REQ` varchar(255) DEFAULT NULL,
  `RFA_BLANK` varchar(255) DEFAULT NULL,
  `ElementName` varchar(255) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  `IsIgnorable` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.submissionfooterformats
CREATE TABLE IF NOT EXISTS `submissionfooterformats` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.submissionheaderformats
CREATE TABLE IF NOT EXISTS `submissionheaderformats` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.submissioninactivebodyformats
CREATE TABLE IF NOT EXISTS `submissioninactivebodyformats` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.transferdischargeassessments
CREATE TABLE IF NOT EXISTS `transferdischargeassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'TransferInPatientDischarged',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.transfernotdischargedassessments
CREATE TABLE IF NOT EXISTS `transfernotdischargedassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'TransferInPatientNotDischarged',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for openfire
CREATE DATABASE IF NOT EXISTS `openfire` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `openfire`;


-- Dumping structure for table openfire.ofBookmark
CREATE TABLE IF NOT EXISTS `ofBookmark` (
  `bookmarkID` bigint(20) NOT NULL,
  `bookmarkType` varchar(50) NOT NULL,
  `bookmarkName` varchar(255) NOT NULL,
  `bookmarkValue` varchar(255) NOT NULL,
  `isGlobal` int(11) NOT NULL,
  PRIMARY KEY (`bookmarkID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofBookmarkPerm
CREATE TABLE IF NOT EXISTS `ofBookmarkPerm` (
  `bookmarkID` bigint(20) NOT NULL,
  `bookmarkType` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`bookmarkID`,`name`,`bookmarkType`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofBookmarkProp
CREATE TABLE IF NOT EXISTS `ofBookmarkProp` (
  `bookmarkID` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `propValue` text NOT NULL,
  PRIMARY KEY (`bookmarkID`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofExtComponentConf
CREATE TABLE IF NOT EXISTS `ofExtComponentConf` (
  `subdomain` varchar(255) NOT NULL,
  `wildcard` tinyint(4) NOT NULL,
  `secret` varchar(255) DEFAULT NULL,
  `permission` varchar(10) NOT NULL,
  PRIMARY KEY (`subdomain`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofGroup
CREATE TABLE IF NOT EXISTS `ofGroup` (
  `groupName` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`groupName`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofGroupProp
CREATE TABLE IF NOT EXISTS `ofGroupProp` (
  `groupName` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `propValue` text NOT NULL,
  PRIMARY KEY (`groupName`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofGroupUser
CREATE TABLE IF NOT EXISTS `ofGroupUser` (
  `groupName` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `administrator` tinyint(4) NOT NULL,
  PRIMARY KEY (`groupName`,`username`,`administrator`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofID
CREATE TABLE IF NOT EXISTS `ofID` (
  `idType` int(11) NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`idType`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofMucAffiliation
CREATE TABLE IF NOT EXISTS `ofMucAffiliation` (
  `roomID` bigint(20) NOT NULL,
  `jid` text NOT NULL,
  `affiliation` tinyint(4) NOT NULL,
  PRIMARY KEY (`roomID`,`jid`(70))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofMucConversationLog
CREATE TABLE IF NOT EXISTS `ofMucConversationLog` (
  `roomID` bigint(20) NOT NULL,
  `sender` text NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `logTime` char(15) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  KEY `ofMucConversationLog_time_idx` (`logTime`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofMucMember
CREATE TABLE IF NOT EXISTS `ofMucMember` (
  `roomID` bigint(20) NOT NULL,
  `jid` text NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `faqentry` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`roomID`,`jid`(70))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofMucRoom
CREATE TABLE IF NOT EXISTS `ofMucRoom` (
  `serviceID` bigint(20) NOT NULL,
  `roomID` bigint(20) NOT NULL,
  `creationDate` char(15) NOT NULL,
  `modificationDate` char(15) NOT NULL,
  `name` varchar(50) NOT NULL,
  `naturalName` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `lockedDate` char(15) NOT NULL,
  `emptyDate` char(15) DEFAULT NULL,
  `canChangeSubject` tinyint(4) NOT NULL,
  `maxUsers` int(11) NOT NULL,
  `publicRoom` tinyint(4) NOT NULL,
  `moderated` tinyint(4) NOT NULL,
  `membersOnly` tinyint(4) NOT NULL,
  `canInvite` tinyint(4) NOT NULL,
  `roomPassword` varchar(50) DEFAULT NULL,
  `canDiscoverJID` tinyint(4) NOT NULL,
  `logEnabled` tinyint(4) NOT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `rolesToBroadcast` tinyint(4) NOT NULL,
  `useReservedNick` tinyint(4) NOT NULL,
  `canChangeNick` tinyint(4) NOT NULL,
  `canRegister` tinyint(4) NOT NULL,
  PRIMARY KEY (`serviceID`,`name`),
  KEY `ofMucRoom_roomid_idx` (`roomID`),
  KEY `ofMucRoom_serviceid_idx` (`serviceID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofMucRoomProp
CREATE TABLE IF NOT EXISTS `ofMucRoomProp` (
  `roomID` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `propValue` text NOT NULL,
  PRIMARY KEY (`roomID`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofMucService
CREATE TABLE IF NOT EXISTS `ofMucService` (
  `serviceID` bigint(20) NOT NULL,
  `subdomain` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isHidden` tinyint(4) NOT NULL,
  PRIMARY KEY (`subdomain`),
  KEY `ofMucService_serviceid_idx` (`serviceID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofMucServiceProp
CREATE TABLE IF NOT EXISTS `ofMucServiceProp` (
  `serviceID` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `propValue` text NOT NULL,
  PRIMARY KEY (`serviceID`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofOffline
CREATE TABLE IF NOT EXISTS `ofOffline` (
  `username` varchar(64) NOT NULL,
  `messageID` bigint(20) NOT NULL,
  `creationDate` char(15) NOT NULL,
  `messageSize` int(11) NOT NULL,
  `stanza` text NOT NULL,
  PRIMARY KEY (`username`,`messageID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofPresence
CREATE TABLE IF NOT EXISTS `ofPresence` (
  `username` varchar(64) NOT NULL,
  `offlinePresence` text,
  `offlineDate` char(15) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofPrivacyList
CREATE TABLE IF NOT EXISTS `ofPrivacyList` (
  `username` varchar(64) NOT NULL,
  `name` varchar(100) NOT NULL,
  `isDefault` tinyint(4) NOT NULL,
  `list` text NOT NULL,
  PRIMARY KEY (`username`,`name`),
  KEY `ofPrivacyList_default_idx` (`username`,`isDefault`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofPrivate
CREATE TABLE IF NOT EXISTS `ofPrivate` (
  `username` varchar(64) NOT NULL,
  `name` varchar(100) NOT NULL,
  `namespace` varchar(200) NOT NULL,
  `privateData` text NOT NULL,
  PRIMARY KEY (`username`,`name`,`namespace`(100))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofProperty
CREATE TABLE IF NOT EXISTS `ofProperty` (
  `name` varchar(100) NOT NULL,
  `propValue` text NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofPubsubAffiliation
CREATE TABLE IF NOT EXISTS `ofPubsubAffiliation` (
  `serviceID` varchar(100) NOT NULL,
  `nodeID` varchar(100) NOT NULL,
  `jid` varchar(255) NOT NULL,
  `affiliation` varchar(10) NOT NULL,
  PRIMARY KEY (`serviceID`,`nodeID`,`jid`(70))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofPubsubDefaultConf
CREATE TABLE IF NOT EXISTS `ofPubsubDefaultConf` (
  `serviceID` varchar(100) NOT NULL,
  `leaf` tinyint(4) NOT NULL,
  `deliverPayloads` tinyint(4) NOT NULL,
  `maxPayloadSize` int(11) NOT NULL,
  `persistItems` tinyint(4) NOT NULL,
  `maxItems` int(11) NOT NULL,
  `notifyConfigChanges` tinyint(4) NOT NULL,
  `notifyDelete` tinyint(4) NOT NULL,
  `notifyRetract` tinyint(4) NOT NULL,
  `presenceBased` tinyint(4) NOT NULL,
  `sendItemSubscribe` tinyint(4) NOT NULL,
  `publisherModel` varchar(15) NOT NULL,
  `subscriptionEnabled` tinyint(4) NOT NULL,
  `accessModel` varchar(10) NOT NULL,
  `language` varchar(255) DEFAULT NULL,
  `replyPolicy` varchar(15) DEFAULT NULL,
  `associationPolicy` varchar(15) NOT NULL,
  `maxLeafNodes` int(11) NOT NULL,
  PRIMARY KEY (`serviceID`,`leaf`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofPubsubItem
CREATE TABLE IF NOT EXISTS `ofPubsubItem` (
  `serviceID` varchar(100) NOT NULL,
  `nodeID` varchar(100) NOT NULL,
  `id` varchar(100) NOT NULL,
  `jid` varchar(255) NOT NULL,
  `creationDate` char(15) NOT NULL,
  `payload` mediumtext,
  PRIMARY KEY (`serviceID`,`nodeID`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofPubsubNode
CREATE TABLE IF NOT EXISTS `ofPubsubNode` (
  `serviceID` varchar(100) NOT NULL,
  `nodeID` varchar(100) NOT NULL,
  `leaf` tinyint(4) NOT NULL,
  `creationDate` char(15) NOT NULL,
  `modificationDate` char(15) NOT NULL,
  `parent` varchar(100) DEFAULT NULL,
  `deliverPayloads` tinyint(4) NOT NULL,
  `maxPayloadSize` int(11) DEFAULT NULL,
  `persistItems` tinyint(4) DEFAULT NULL,
  `maxItems` int(11) DEFAULT NULL,
  `notifyConfigChanges` tinyint(4) NOT NULL,
  `notifyDelete` tinyint(4) NOT NULL,
  `notifyRetract` tinyint(4) NOT NULL,
  `presenceBased` tinyint(4) NOT NULL,
  `sendItemSubscribe` tinyint(4) NOT NULL,
  `publisherModel` varchar(15) NOT NULL,
  `subscriptionEnabled` tinyint(4) NOT NULL,
  `configSubscription` tinyint(4) NOT NULL,
  `accessModel` varchar(10) NOT NULL,
  `payloadType` varchar(100) DEFAULT NULL,
  `bodyXSLT` varchar(100) DEFAULT NULL,
  `dataformXSLT` varchar(100) DEFAULT NULL,
  `creator` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `replyPolicy` varchar(15) DEFAULT NULL,
  `associationPolicy` varchar(15) DEFAULT NULL,
  `maxLeafNodes` int(11) DEFAULT NULL,
  PRIMARY KEY (`serviceID`,`nodeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofPubsubNodeGroups
CREATE TABLE IF NOT EXISTS `ofPubsubNodeGroups` (
  `serviceID` varchar(100) NOT NULL,
  `nodeID` varchar(100) NOT NULL,
  `rosterGroup` varchar(100) NOT NULL,
  KEY `ofPubsubNodeGroups_idx` (`serviceID`,`nodeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofPubsubNodeJIDs
CREATE TABLE IF NOT EXISTS `ofPubsubNodeJIDs` (
  `serviceID` varchar(100) NOT NULL,
  `nodeID` varchar(100) NOT NULL,
  `jid` varchar(255) NOT NULL,
  `associationType` varchar(20) NOT NULL,
  PRIMARY KEY (`serviceID`,`nodeID`,`jid`(70))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofPubsubSubscription
CREATE TABLE IF NOT EXISTS `ofPubsubSubscription` (
  `serviceID` varchar(100) NOT NULL,
  `nodeID` varchar(100) NOT NULL,
  `id` varchar(100) NOT NULL,
  `jid` varchar(255) NOT NULL,
  `owner` varchar(255) NOT NULL,
  `state` varchar(15) NOT NULL,
  `deliver` tinyint(4) NOT NULL,
  `digest` tinyint(4) NOT NULL,
  `digest_frequency` int(11) NOT NULL,
  `expire` char(15) DEFAULT NULL,
  `includeBody` tinyint(4) NOT NULL,
  `showValues` varchar(30) DEFAULT NULL,
  `subscriptionType` varchar(10) NOT NULL,
  `subscriptionDepth` tinyint(4) NOT NULL,
  `keyword` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`serviceID`,`nodeID`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofRemoteServerConf
CREATE TABLE IF NOT EXISTS `ofRemoteServerConf` (
  `xmppDomain` varchar(255) NOT NULL,
  `remotePort` int(11) DEFAULT NULL,
  `permission` varchar(10) NOT NULL,
  PRIMARY KEY (`xmppDomain`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofRoster
CREATE TABLE IF NOT EXISTS `ofRoster` (
  `rosterID` bigint(20) NOT NULL,
  `username` varchar(64) NOT NULL,
  `jid` varchar(1024) NOT NULL,
  `sub` tinyint(4) NOT NULL,
  `ask` tinyint(4) NOT NULL,
  `recv` tinyint(4) NOT NULL,
  `nick` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rosterID`),
  KEY `ofRoster_unameid_idx` (`username`),
  KEY `ofRoster_jid_idx` (`jid`(1000))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofRosterGroups
CREATE TABLE IF NOT EXISTS `ofRosterGroups` (
  `rosterID` bigint(20) NOT NULL,
  `rank` tinyint(4) NOT NULL,
  `groupName` varchar(255) NOT NULL,
  PRIMARY KEY (`rosterID`,`rank`),
  KEY `ofRosterGroup_rosterid_idx` (`rosterID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofSASLAuthorized
CREATE TABLE IF NOT EXISTS `ofSASLAuthorized` (
  `username` varchar(64) NOT NULL,
  `principal` text NOT NULL,
  PRIMARY KEY (`username`,`principal`(200))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofSecurityAuditLog
CREATE TABLE IF NOT EXISTS `ofSecurityAuditLog` (
  `msgID` bigint(20) NOT NULL,
  `username` varchar(64) NOT NULL,
  `entryStamp` bigint(20) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `node` varchar(255) NOT NULL,
  `details` text,
  PRIMARY KEY (`msgID`),
  KEY `ofSecurityAuditLog_tstamp_idx` (`entryStamp`),
  KEY `ofSecurityAuditLog_uname_idx` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofSipPhoneLog
CREATE TABLE IF NOT EXISTS `ofSipPhoneLog` (
  `username` varchar(255) DEFAULT NULL,
  `addressFrom` varchar(255) DEFAULT NULL,
  `addressTo` varchar(255) DEFAULT NULL,
  `datetime` bigint(20) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `callType` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofSipUser
CREATE TABLE IF NOT EXISTS `ofSipUser` (
  `username` varchar(255) NOT NULL,
  `sipUsername` varchar(255) DEFAULT NULL,
  `sipAuthuser` varchar(255) DEFAULT NULL,
  `sipDisplayName` varchar(255) DEFAULT NULL,
  `sipPassword` varchar(255) DEFAULT NULL,
  `sipServer` varchar(255) DEFAULT NULL,
  `stunServer` varchar(255) DEFAULT NULL,
  `stunPort` varchar(255) DEFAULT NULL,
  `useStun` int(11) DEFAULT NULL,
  `voicemail` varchar(255) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `outboundproxy` varchar(255) DEFAULT NULL,
  `promptCredentials` int(11) DEFAULT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofUser
CREATE TABLE IF NOT EXISTS `ofUser` (
  `username` varchar(64) NOT NULL,
  `plainPassword` varchar(32) DEFAULT NULL,
  `encryptedPassword` varchar(255) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `creationDate` char(15) NOT NULL,
  `modificationDate` char(15) NOT NULL,
  PRIMARY KEY (`username`),
  KEY `ofUser_cDate_idx` (`creationDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofUserFlag
CREATE TABLE IF NOT EXISTS `ofUserFlag` (
  `username` varchar(64) NOT NULL,
  `name` varchar(100) NOT NULL,
  `startTime` char(15) DEFAULT NULL,
  `endTime` char(15) DEFAULT NULL,
  PRIMARY KEY (`username`,`name`),
  KEY `ofUserFlag_sTime_idx` (`startTime`),
  KEY `ofUserFlag_eTime_idx` (`endTime`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofUserProp
CREATE TABLE IF NOT EXISTS `ofUserProp` (
  `username` varchar(64) NOT NULL,
  `name` varchar(100) NOT NULL,
  `propValue` text NOT NULL,
  PRIMARY KEY (`username`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofVCard
CREATE TABLE IF NOT EXISTS `ofVCard` (
  `username` varchar(64) NOT NULL,
  `vcard` mediumtext NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table openfire.ofVersion
CREATE TABLE IF NOT EXISTS `ofVersion` (
  `name` varchar(50) NOT NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for privateduty
CREATE DATABASE IF NOT EXISTS `privateduty` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `privateduty`;


-- Dumping structure for table privateduty.assessments
CREATE TABLE IF NOT EXISTS `assessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) NOT NULL,
  `IsValidated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `EpisodeId` (`EpisodeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.claimdatas
CREATE TABLE IF NOT EXISTS `claimdatas` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `Data` text,
  `BillIdentifers` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `ClaimType` enum('MAN','CMS','HMO') DEFAULT 'CMS',
  `Response` mediumtext,
  `IsDeprecated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.communicationnotes
CREATE TABLE IF NOT EXISTS `communicationnotes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `Text` text,
  `Recipients` text,
  `Created` date NOT NULL DEFAULT '0001-01-01',
  `Modified` date NOT NULL DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(1) DEFAULT '0',
  `Status` int(4) DEFAULT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianData` text,
  PRIMARY KEY (`Id`),
  KEY `FK_PrivateDutyCommnotes_PatientId` (`PatientId`),
  KEY `FK_PrivateDutyCommnotes_AgencyId` (`AgencyId`),
  KEY `FK_PrivateDutyCommnotes_UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.facetofaceencounters
CREATE TABLE IF NOT EXISTS `facetofaceencounters` (
  `Id` char(36) NOT NULL DEFAULT '',
  `OrderNumber` int(20) unsigned NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `Status` int(36) DEFAULT NULL,
  `Certification` varchar(36) DEFAULT NULL,
  `EncounterDate` date DEFAULT '0001-01-01',
  `ReceivedDate` date DEFAULT '0001-01-01',
  `RequestDate` date DEFAULT '0001-01-01',
  `SentDate` date DEFAULT '0001-01-01',
  `ClinicalFinding` text,
  `Services` varchar(50) DEFAULT NULL,
  `ServicesOther` varchar(50) DEFAULT NULL,
  `SignatureText` varchar(50) DEFAULT NULL,
  `SignatureDate` date DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(4) DEFAULT '0',
  `Modified` date DEFAULT '0001-01-01',
  `Created` date DEFAULT '0001-01-01',
  `MedicalReason` text,
  `PhysicianData` text,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.incidents
CREATE TABLE IF NOT EXISTS `incidents` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `IndividualInvolved` varchar(150) DEFAULT NULL,
  `IndividualInvolvedOther` varchar(100) DEFAULT NULL,
  `IncidentDate` date NOT NULL,
  `IncidentType` varchar(100) DEFAULT NULL,
  `Description` text,
  `ActionTaken` text,
  `MDNotified` varchar(20) DEFAULT NULL,
  `FamilyNotified` varchar(20) DEFAULT NULL,
  `NewOrdersCreated` varchar(20) DEFAULT NULL,
  `Orders` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Status` int(10) unsigned NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FollowUp` text,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.infections
CREATE TABLE IF NOT EXISTS `infections` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `InfectionDate` date NOT NULL,
  `InfectionType` varchar(150) DEFAULT NULL,
  `InfectionTypeOther` varchar(50) DEFAULT NULL,
  `Treatment` text,
  `Orders` text,
  `MDNotified` varchar(20) DEFAULT NULL,
  `TreatmentPrescribed` varchar(20) DEFAULT NULL,
  `NewOrdersCreated` varchar(20) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Status` int(10) unsigned NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FollowUp` text,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.managedclaimadjustments
CREATE TABLE IF NOT EXISTS `managedclaimadjustments` (
  `Id` varchar(36) NOT NULL,
  `AgencyId` varchar(36) NOT NULL,
  `ClaimId` varchar(36) NOT NULL,
  `PatientId` varchar(36) NOT NULL,
  `Adjustment` double NOT NULL DEFAULT '0',
  `TypeId` varchar(36) NOT NULL,
  `Comments` text NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ManagedClaimAdjustments_AgencyId` (`AgencyId`),
  KEY `FK_ManagedClaimAdjustments_PatientId` (`PatientId`),
  KEY `FK_ManagedClaimAdjustments_ClaimId` (`ClaimId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.managedclaimpayments
CREATE TABLE IF NOT EXISTS `managedclaimpayments` (
  `Id` varchar(36) NOT NULL,
  `AgencyId` varchar(36) NOT NULL,
  `ClaimId` varchar(36) NOT NULL,
  `PatientId` varchar(36) NOT NULL,
  `Payment` double NOT NULL DEFAULT '0',
  `PaymentDate` date NOT NULL DEFAULT '0001-01-01',
  `Comments` text,
  `Payor` int(11) NOT NULL DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ManagedClaimPayments_ClaimId` (`ClaimId`),
  KEY `FK_ManagedClaimPayments_PatientId` (`PatientId`),
  KEY `FK_ManagedClaimPayments_AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.managedclaims
CREATE TABLE IF NOT EXISTS `managedclaims` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `IsOasisComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FirstBillableVisitDate` date DEFAULT NULL,
  `Remark` text,
  `IsuranceIdNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `AreOrdersComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AreVisitsComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `VerifiedVisits` mediumtext,
  `Supply` text,
  `SupplyTotal` double DEFAULT NULL,
  `Payment` double unsigned DEFAULT '0',
  `PrimaryInsuranceId` int(4) unsigned NOT NULL,
  `IsSupplyVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsInfoVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVisitVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsInsuranceVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsRapGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Status` int(4) unsigned NOT NULL DEFAULT '0',
  `Type` varchar(10) DEFAULT '',
  `DischargeDate` date DEFAULT '0001-01-01',
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) DEFAULT '0',
  `ClaimDate` date NOT NULL DEFAULT '0001-01-01',
  `ProspectivePay` double unsigned DEFAULT '0',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `GroupName` varchar(50) DEFAULT NULL,
  `GroupId` varchar(50) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `Authorization` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `AuthorizationNumber2` varchar(50) DEFAULT NULL,
  `AuthorizationNumber3` varchar(50) DEFAULT NULL,
  `ConditionCodes` text,
  `SupplyCode` varchar(10) DEFAULT '0272',
  `UB4PatientStatus` varchar(5) DEFAULT NULL,
  `CBSA` varchar(6) DEFAULT NULL,
  `Ub04Locator81cca` text,
  `Ub04Locator39` text,
  `Insurance` mediumtext,
  `IsHomeHealthServiceIncluded` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `Ub04Locator31` text,
  `Ub04Locator32` text,
  `Ub04Locator33` text,
  `Ub04Locator34` text,
  `HCFALocators` text,
  PRIMARY KEY (`Id`),
  KEY `FK_Finals_AgencyId` (`AgencyId`),
  KEY `FK_Finals_PatientId` (`PatientId`),
  KEY `FK_Finals_EpisodeId` (`EpisodeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.missedvisits
CREATE TABLE IF NOT EXISTS `missedvisits` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `Date` date NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '400',
  `Reason` varchar(75) DEFAULT NULL,
  `Comments` text,
  `IsOrderGenerated` tinyint(1) unsigned NOT NULL,
  `IsPhysicianOfficeNotified` tinyint(1) unsigned NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `PatientId` (`PatientId`),
  KEY `AgencyId` (`AgencyId`),
  KEY `EpisodeId` (`EpisodeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.patientadmissiondates
CREATE TABLE IF NOT EXISTS `patientadmissiondates` (
  `Id` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `StartOfCareDate` date DEFAULT '0001-01-01',
  `DischargedDate` date DEFAULT '0001-01-01',
  `PatientData` text,
  `ProfileData` text,
  `Status` int(11) unsigned DEFAULT NULL,
  `IsActive` tinyint(1) unsigned DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DischargeReasonId` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date DEFAULT '0001-01-01',
  `Modified` date DEFAULT '0001-01-01',
  `Reason` text,
  PRIMARY KEY (`Id`),
  KEY `PatientId` (`PatientId`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.patientepisodes
CREATE TABLE IF NOT EXISTS `patientepisodes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `Details` text,
  `StartDate` date NOT NULL DEFAULT '0001-01-01',
  `EndDate` date NOT NULL DEFAULT '0001-01-01',
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsRecertCompleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDischarged` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsLinkedToDischarge` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AssessmentId` char(36) DEFAULT NULL,
  `AssessmentType` varchar(100) DEFAULT NULL,
  `Created` date NOT NULL DEFAULT '0001-01-01',
  `Modified` date NOT NULL DEFAULT '0001-01-01',
  `StartOfCareDate` date NOT NULL DEFAULT '0001-01-01',
  `AdmissionId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  PRIMARY KEY (`Id`),
  KEY `FK_PatientEpisode_PatientId` (`PatientId`),
  KEY `FK_PatientEpisode_AgencyId` (`AgencyId`),
  KEY `AdmissionId` (`AdmissionId`),
  KEY `IX_PatientEpisodes_Active_Enddate` (`IsActive`,`EndDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.patientprofiles
CREATE TABLE IF NOT EXISTS `patientprofiles` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `AgencyLocationId` char(36) DEFAULT NULL,
  `MedicareNumber` char(11) DEFAULT NULL,
  `MedicaidNumber` char(20) DEFAULT NULL,
  `PatientIdNumber` char(50) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleInitial` varchar(1) DEFAULT NULL,
  `StartofCareDate` date NOT NULL,
  `PrimaryInsurance` int(11) DEFAULT '-1',
  `PrimaryHealthPlanId` varchar(50) DEFAULT NULL,
  `PrimaryGroupName` varchar(50) DEFAULT NULL,
  `PrimaryGroupId` varchar(50) DEFAULT NULL,
  `PrimaryRelationship` varchar(10) DEFAULT '2',
  `SecondaryInsurance` int(11) DEFAULT '-1',
  `SecondaryHealthPlanId` varchar(50) DEFAULT NULL,
  `SecondaryGroupName` varchar(50) DEFAULT NULL,
  `SecondaryGroupId` varchar(50) DEFAULT NULL,
  `SecondaryRelationship` varchar(10) DEFAULT '2',
  `TertiaryInsurance` int(11) DEFAULT '-1',
  `TertiaryHealthPlanId` varchar(50) DEFAULT NULL,
  `TertiaryGroupName` varchar(50) DEFAULT NULL,
  `TertiaryGroupId` varchar(50) DEFAULT NULL,
  `TertiaryRelationship` varchar(10) DEFAULT '2',
  `Payer` varchar(20) DEFAULT NULL,
  `ServicesRequired` varchar(100) DEFAULT NULL,
  `PaymentSource` varchar(50) DEFAULT NULL,
  `OtherPaymentSource` varchar(50) DEFAULT NULL,
  `AdmissionSource` varchar(50) DEFAULT NULL,
  `ReferrerPhysician` char(36) DEFAULT NULL,
  `ReferralDate` date DEFAULT '0001-01-01',
  `OtherReferralSource` varchar(30) DEFAULT NULL,
  `ReferralName` varchar(50) DEFAULT NULL,
  `ReferralContact` varchar(50) DEFAULT NULL,
  `InternalReferral` char(36) DEFAULT NULL,
  `CaseManagerId` char(36) DEFAULT NULL,
  `AuditorId` char(36) DEFAULT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `NonAdmissionDate` date DEFAULT NULL,
  `NonAdmissionReason` varchar(50) DEFAULT NULL,
  `Comments` text,
  `LastEligibilityCheck` date DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DischargeDate` date DEFAULT '0001-01-01',
  `DischargeReasonId` tinyint(2) NOT NULL DEFAULT '0',
  `DischargeReason` text,
  `AdmissionId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `OldStatus` int(11) DEFAULT '0',
  `Created` date DEFAULT '0001-01-01',
  `Modified` date DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `FK_Patient_AgencyId` (`AgencyId`),
  KEY `FK_Patient_UserId` (`UserId`),
  KEY `FK_Patient_CaseManagerId` (`CaseManagerId`),
  KEY `FK_Patient_AgencyLocationId` (`AgencyLocationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.patientvisitnotes
CREATE TABLE IF NOT EXISTS `patientvisitnotes` (
  `AgencyId` char(36) NOT NULL,
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `NoteType` varchar(50) DEFAULT NULL,
  `Note` mediumtext,
  `WoundNote` mediumtext,
  `Supply` mediumtext,
  `Status` int(10) unsigned DEFAULT NULL,
  `IsBillable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsWoundCare` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSupplyExist` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `ReceivedDate` date DEFAULT NULL,
  `SentDate` date DEFAULT NULL,
  `OrderNumber` bigint(20) unsigned DEFAULT '0',
  `PhysicianData` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Version` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `PhysicianId` (`PhysicianId`),
  KEY `FK_PrivateDutyPatientVisitNotes_AgencyId` (`AgencyId`),
  KEY `FK_PrivateDutyPatientVisitNotes_PatientId` (`PatientId`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.physicianorders
CREATE TABLE IF NOT EXISTS `physicianorders` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `UserId` char(36) NOT NULL,
  `Summary` varchar(100) DEFAULT NULL,
  `Text` text NOT NULL,
  `OrderDate` date NOT NULL,
  `Created` date NOT NULL,
  `Modified` date DEFAULT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `SentDate` date NOT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date NOT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Status` int(10) unsigned NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsOrderForNextEpisode` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsOrderReadAndVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PhysicianData` text,
  PRIMARY KEY (`Id`),
  KEY `PatientId` (`PatientId`),
  KEY `PhysicianId` (`PhysicianId`),
  KEY `UserId` (`UserId`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.planofcares
CREATE TABLE IF NOT EXISTS `planofcares` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL DEFAULT '',
  `PatientId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `UserId` char(36) NOT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `Data` text,
  `Status` int(11) NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `SentDate` date DEFAULT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AssessmentId` char(36) DEFAULT NULL,
  `AssessmentType` varchar(50) DEFAULT NULL,
  `PhysicianData` text,
  `IsNonOasis` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsStandAlone` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisciplineTask` int(11) NOT NULL,
  PRIMARY KEY (`Id`,`EpisodeId`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.returncomments
CREATE TABLE IF NOT EXISTS `returncomments` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `EventId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `Comments` text,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `EventId` (`EventId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table privateduty.scheduletasks
CREATE TABLE IF NOT EXISTS `scheduletasks` (
  `Id` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `DisciplineTask` int(5) DEFAULT NULL,
  `EventStartTime` datetime DEFAULT '2001-01-01 00:00:00',
  `EventEndTime` datetime DEFAULT '2001-01-01 00:00:00',
  `VisitStartTime` datetime DEFAULT NULL,
  `VisitEndTime` datetime DEFAULT NULL,
  `Status` int(5) DEFAULT '1000',
  `BeforeMissedVisitStatus` int(5) DEFAULT '0',
  `Discipline` varchar(20) DEFAULT NULL,
  `Surcharge` varchar(10) DEFAULT NULL,
  `AssociatedMileage` varchar(10) DEFAULT NULL,
  `Comments` text,
  `Asset` text,
  `SendAsOrder` char(3) DEFAULT '0',
  `Version` int(11) DEFAULT '1',
  `InPrintQueue` tinyint(1) DEFAULT NULL,
  `IsBillable` tinyint(1) DEFAULT NULL,
  `IsMissedVisit` tinyint(1) DEFAULT NULL,
  `IsOrderForNextEpisode` tinyint(1) DEFAULT NULL,
  `IsVisitPaid` tinyint(1) DEFAULT NULL,
  `IsQANote` tinyint(1) DEFAULT '0',
  `IsAllDay` tinyint(1) DEFAULT '0',
  `IsHidden` tinyint(1) DEFAULT '0',
  `IsDeprecated` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `UserId` (`UserId`),
  KEY `EpisodeId` (`EpisodeId`),
  CONSTRAINT `FK_PrivateDutyScheduleTasks_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencymanagement`.`agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PrivateDutyScheduleTasks_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `agencymanagement`.`patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping database structure for tom_temp
CREATE DATABASE IF NOT EXISTS `tom_temp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tom_temp`;


-- Dumping structure for table tom_temp.backup_cbsacodes
CREATE TABLE IF NOT EXISTS `backup_cbsacodes` (
  `ZipFive` varchar(5) DEFAULT NULL,
  `ZipFour` varchar(4) DEFAULT NULL,
  `ZipNine` varchar(10) DEFAULT NULL,
  `StateCode` varchar(2) DEFAULT NULL,
  `CBSA` varchar(5) DEFAULT NULL,
  `WITwoTen` double unsigned DEFAULT NULL,
  `WITwoEleven` double unsigned DEFAULT NULL,
  `WITwoTwelve` double unsigned DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table tom_temp.backup_ruralwagecbsa
CREATE TABLE IF NOT EXISTS `backup_ruralwagecbsa` (
  `ZipFive` varchar(5) DEFAULT NULL,
  `ZipFour` varchar(4) DEFAULT NULL,
  `ZipNine` varchar(10) DEFAULT NULL,
  `StateCode` varchar(2) DEFAULT NULL,
  `CBSA` varchar(5) DEFAULT NULL,
  `WITwoTen` double unsigned DEFAULT NULL,
  `WITwoEleven` double unsigned DEFAULT NULL,
  `WITwoTwelve` double unsigned DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table tom_temp.backup_wagecbsa
CREATE TABLE IF NOT EXISTS `backup_wagecbsa` (
  `ZipFive` varchar(5) DEFAULT NULL,
  `ZipFour` varchar(4) DEFAULT NULL,
  `ZipNine` varchar(10) DEFAULT NULL,
  `StateCode` varchar(2) DEFAULT NULL,
  `CBSA` varchar(5) DEFAULT NULL,
  `WITwoTen` double unsigned DEFAULT NULL,
  `WITwoEleven` double unsigned DEFAULT NULL,
  `WITwoTwelve` double unsigned DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table tom_temp.backup_zipcbsa
CREATE TABLE IF NOT EXISTS `backup_zipcbsa` (
  `ZipFive` varchar(5) DEFAULT NULL,
  `ZipFour` varchar(4) DEFAULT NULL,
  `ZipNine` varchar(10) DEFAULT NULL,
  `StateCode` varchar(2) DEFAULT NULL,
  `CBSA` varchar(5) DEFAULT NULL,
  `WITwoTen` double unsigned DEFAULT NULL,
  `WITwoEleven` double unsigned DEFAULT NULL,
  `WITwoTwelve` double unsigned DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table tom_temp.backup_zipninecodes
CREATE TABLE IF NOT EXISTS `backup_zipninecodes` (
  `Id` int(10) unsigned NOT NULL DEFAULT '0',
  `Code` char(5) DEFAULT NULL,
  `CodeNine` char(10) DEFAULT NULL,
  `StateCode` char(2) DEFAULT NULL,
  `StateName` varchar(50) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `CBSA` varchar(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table tom_temp.cbsacodes
CREATE TABLE IF NOT EXISTS `cbsacodes` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Zip` int(5) NOT NULL,
  `CityName` varchar(50) DEFAULT NULL,
  `CountyCode` varchar(5) DEFAULT NULL,
  `CountyTitle` varchar(50) DEFAULT NULL,
  `StateCode` varchar(2) NOT NULL,
  `StateName` varchar(50) NOT NULL,
  `CBSA` varchar(5) NOT NULL,
  `WITwoSeven` double unsigned NOT NULL,
  `WITwoEight` double unsigned NOT NULL,
  `WITwoNine` double unsigned NOT NULL,
  `WITwoTen` double unsigned NOT NULL,
  `WITwoEleven` double unsigned NOT NULL,
  `WITwoTwelve` double unsigned NOT NULL,
  `Variance` double NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Zip` (`Zip`),
  KEY `CountyCode` (`CountyCode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table tom_temp.cbsacodes old
CREATE TABLE IF NOT EXISTS `cbsacodes old` (
  `ZipFive` int(5) DEFAULT NULL,
  `ZipFour` int(4) DEFAULT NULL,
  `ZipNine` varchar(10) DEFAULT NULL,
  `StateCode` varchar(2) DEFAULT NULL,
  `CBSA` varchar(5) DEFAULT NULL,
  `WITwoTen` double unsigned DEFAULT NULL,
  `WITwoEleven` double unsigned DEFAULT NULL,
  `WITwoTwelve` double unsigned DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table tom_temp.cbsafourcodes
CREATE TABLE IF NOT EXISTS `cbsafourcodes` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Zip` int(5) NOT NULL,
  `ZipFour` int(4) DEFAULT NULL,
  `CityName` varchar(50) DEFAULT NULL,
  `CountyCode` varchar(5) DEFAULT NULL,
  `CountyTitle` varchar(50) DEFAULT NULL,
  `StateCode` varchar(2) NOT NULL,
  `StateName` varchar(50) NOT NULL,
  `CBSA` varchar(5) NOT NULL,
  `WITwoSeven` double unsigned NOT NULL,
  `WITwoEight` double unsigned NOT NULL,
  `WITwoNine` double unsigned NOT NULL,
  `WITwoTen` double unsigned NOT NULL,
  `WITwoEleven` double unsigned NOT NULL,
  `WITwoTwelve` double unsigned NOT NULL,
  `Variance` double NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Zip` (`Zip`),
  KEY `CountyCode` (`CountyCode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table tom_temp.wageindex
CREATE TABLE IF NOT EXISTS `wageindex` (
  `CBSA` varchar(5) DEFAULT NULL,
  `WITwoTwelve` double unsigned DEFAULT NULL,
  `StateCode` varchar(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table tom_temp.zipcodes
CREATE TABLE IF NOT EXISTS `zipcodes` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` char(5) DEFAULT NULL,
  `StateCode` char(2) DEFAULT NULL,
  `StateName` varchar(50) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `CBSA` char(5) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Code` (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table tom_temp.zipfourcodes
CREATE TABLE IF NOT EXISTS `zipfourcodes` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` int(5) DEFAULT NULL,
  `CodeFour` int(4) DEFAULT NULL,
  `StateCode` char(2) DEFAULT NULL,
  `StateName` varchar(50) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `CBSA` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Code` (`Code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table tom_temp.zipninecodes
CREATE TABLE IF NOT EXISTS `zipninecodes` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` int(5) DEFAULT NULL,
  `CodeFour` int(4) DEFAULT NULL,
  `CodeNine` char(10) DEFAULT NULL,
  `StateCode` char(2) DEFAULT NULL,
  `StateName` varchar(50) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `CBSA` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Code` (`Code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
