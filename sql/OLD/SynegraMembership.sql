-- --------------------------------------------------------
-- Host:                         10.0.1.61
-- Server version:               5.1.52 - MySQL Community Server (GPL) by Remi
-- Server OS:                    redhat-linux-gnu
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for axxessmembership
CREATE DATABASE IF NOT EXISTS `axxessmembership` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `axxessmembership`;


-- Dumping structure for table axxessmembership.agencysnapshots
CREATE TABLE IF NOT EXISTS `agencysnapshots` (
  `Id` char(36) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `IsSuspended` tinyint(1) unsigned DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFrozen` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FrozenDate` date NOT NULL DEFAULT '0001-01-01',
  `ClusterId` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.ddecredentials
CREATE TABLE IF NOT EXISTS `ddecredentials` (
  `Id` char(36) NOT NULL,
  `UserId` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Created` date NOT NULL DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.errors
CREATE TABLE IF NOT EXISTS `errors` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Message` varchar(500) NOT NULL,
  `Type` varchar(30) NOT NULL,
  `Details` text NOT NULL,
  `Server` varchar(30) NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.impersonationlinks
CREATE TABLE IF NOT EXISTS `impersonationlinks` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `RepLoginId` char(36) NOT NULL,
  `RepName` varchar(50) NOT NULL,
  `IsUsed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ImpersonationLinks_LoginId` (`LoginId`),
  CONSTRAINT `FK_ImpersonationLinks_LoginId` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.logins
CREATE TABLE IF NOT EXISTS `logins` (
  `Id` char(36) NOT NULL,
  `EmailAddress` varchar(100) NOT NULL,
  `PasswordHash` char(88) DEFAULT NULL,
  `PasswordSalt` char(20) DEFAULT NULL,
  `SignatureHash` char(88) DEFAULT NULL,
  `SignatureSalt` char(20) DEFAULT NULL,
  `DisplayName` varchar(50) NOT NULL,
  `Role` varchar(50) NOT NULL,
  `IsAxxessAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsAxxessSupport` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsActive` tinyint(1) unsigned NOT NULL,
  `IsLocked` tinyint(1) unsigned NOT NULL,
  `LastLoginDate` datetime NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `EmailAddress` (`EmailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.logintokens
CREATE TABLE IF NOT EXISTS `logintokens` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `IsUsed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_LoginTokens_AgencyId` (`AgencyId`),
  KEY `FK_LoginTokens_UserId` (`UserId`),
  CONSTRAINT `FK_LoginTokens_UserId` FOREIGN KEY (`UserId`) REFERENCES `usersnapshots` (`Id`),
  CONSTRAINT `FK_LoginTokens_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencysnapshots` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.physiciansnapshots
CREATE TABLE IF NOT EXISTS `physiciansnapshots` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `NPI` char(10) DEFAULT NULL,
  `HasPhysicianAccess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Physicians_AgencyId` (`AgencyId`),
  KEY `FK_Physicians_LoginId` (`LoginId`),
  CONSTRAINT `FK_Physicians_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencysnapshots` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Physicians_LoginId` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.sessiontokens
CREATE TABLE IF NOT EXISTS `sessiontokens` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `TokenId` varchar(150) NOT NULL,
  `DeviceId` varchar(150) NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.usersnapshots
CREATE TABLE IF NOT EXISTS `usersnapshots` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `TitleType` varchar(50) DEFAULT NULL,
  `Status` tinyint(50) unsigned NOT NULL,
  `Created` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Users_AgencyId` (`AgencyId`),
  KEY `FK_Users_LoginId` (`LoginId`),
  CONSTRAINT `FK_Users_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencysnapshots` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Users_LoginId` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
