-- --------------------------------------------------------
-- Host:                         10.0.1.61
-- Server version:               5.1.52 - MySQL Community Server (GPL) by Remi
-- Server OS:                    redhat-linux-gnu
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for accounting
CREATE DATABASE IF NOT EXISTS `accounting` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `accounting`;


-- Dumping structure for table accounting.agencies
CREATE TABLE IF NOT EXISTS `agencies` (
  `Id` char(36) NOT NULL,
  `SalesPerson` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `Trainer` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `BackupTrainer` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `ImplementationSpecialist` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `Name` varchar(100) NOT NULL,
  `TaxId` char(10) NOT NULL,
  `TaxIdType` varchar(50) DEFAULT NULL,
  `NationalProviderNumber` char(10) DEFAULT NULL,
  `MedicareProviderNumber` char(9) DEFAULT NULL,
  `MedicaidProviderNumber` char(9) DEFAULT NULL,
  `HomeHealthAgencyId` varchar(20) DEFAULT NULL,
  `SubmitterId` varchar(20) DEFAULT NULL,
  `SubmitterName` varchar(50) DEFAULT NULL,
  `SubmitterPhone` char(10) DEFAULT NULL,
  `SubmitterFax` char(10) DEFAULT NULL,
  `IsAgreementSigned` tinyint(1) unsigned DEFAULT '1',
  `IsSuspended` tinyint(1) unsigned DEFAULT '0',
  `TrialPeriod` int(10) unsigned DEFAULT '30',
  `Package` int(10) unsigned DEFAULT '5',
  `AnnualPlanId` int(10) unsigned DEFAULT '0',
  `ContactPersonFirstName` varchar(100) NOT NULL,
  `ContactPersonLastName` varchar(100) NOT NULL,
  `ContactPersonEmail` varchar(100) DEFAULT NULL,
  `ContactPersonPhone` char(10) NOT NULL,
  `Payor` varchar(100) NOT NULL DEFAULT '1',
  `Comments` varchar(150) DEFAULT NULL,
  `CustomFields` varchar(250) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CahpsVendor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `CahpsVendorId` varchar(50) DEFAULT NULL,
  `CahpsSurveyDesignator` varchar(50) DEFAULT NULL,
  `IsAxxessTheBiller` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OasisAuditVendor` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `OasisAuditVendorApiKey` varchar(250) DEFAULT NULL,
  `FrozenDate` date NOT NULL DEFAULT '0001-01-01',
  `IsFrozen` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PreviousSoftware` varchar(100) DEFAULT NULL,
  `Services` int(10) unsigned NOT NULL DEFAULT '1',
  `DBServerIp` varchar(32) NOT NULL DEFAULT '10.0.5.62',
  `AccountId` char(8) DEFAULT NULL,
  `ClusterId` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table accounting.agency
CREATE TABLE IF NOT EXISTS `agency` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Account` varchar(8) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `BranchId` varchar(20) DEFAULT 'N',
  `ClusterId` int(11) DEFAULT '0',
  `IsDeprecated` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Account` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table accounting.agencychanges
CREATE TABLE IF NOT EXISTS `agencychanges` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `TaxId` char(10) DEFAULT NULL,
  `TaxIdType` varchar(50) DEFAULT NULL,
  `ContactPersonEmail` varchar(100) DEFAULT NULL,
  `ContactPersonPhone` char(10) DEFAULT NULL,
  `CahpsVendor` tinyint(3) unsigned DEFAULT NULL,
  `NationalProviderNumber` char(10) DEFAULT NULL,
  `MedicareProviderNumber` char(9) DEFAULT NULL,
  `MedicaidProviderNumber` char(9) DEFAULT NULL,
  `HomeHealthAgencyId` varchar(20) DEFAULT NULL,
  `ContactPersonFirstName` varchar(100) DEFAULT NULL,
  `ContactPersonLastName` varchar(100) DEFAULT NULL,
  `LocationName` varchar(100) DEFAULT NULL,
  `LocationAddressLine1` varchar(100) DEFAULT NULL,
  `LocationAddressLine2` varchar(100) DEFAULT NULL,
  `LocationAddressCity` varchar(100) DEFAULT NULL,
  `LocationAddressStateCode` char(2) DEFAULT NULL,
  `LocationAddressZipCode` char(5) DEFAULT NULL,
  `LocationPhoneWork` char(10) DEFAULT NULL,
  `LocationFaxNumber` char(10) DEFAULT NULL,
  `FullName` varchar(50) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `IsComplete` tinyint(3) unsigned DEFAULT '0',
  `Comments` text,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyChanges_AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table accounting.agencycopy
CREATE TABLE IF NOT EXISTS `agencycopy` (
  `Id` char(36) NOT NULL,
  `SalesPerson` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `Trainer` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `BackupTrainer` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `ImplementationSpecialist` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `Name` varchar(100) NOT NULL,
  `TaxId` char(10) NOT NULL,
  `TaxIdType` varchar(50) DEFAULT NULL,
  `NationalProviderNumber` char(10) DEFAULT NULL,
  `MedicareProviderNumber` char(9) DEFAULT NULL,
  `MedicaidProviderNumber` char(9) DEFAULT NULL,
  `HomeHealthAgencyId` varchar(20) DEFAULT NULL,
  `SubmitterId` varchar(20) DEFAULT NULL,
  `SubmitterName` varchar(50) DEFAULT NULL,
  `SubmitterPhone` char(10) DEFAULT NULL,
  `SubmitterFax` char(10) DEFAULT NULL,
  `IsAgreementSigned` tinyint(1) unsigned DEFAULT '1',
  `IsSuspended` tinyint(1) unsigned DEFAULT '0',
  `TrialPeriod` int(10) unsigned DEFAULT '30',
  `Package` int(10) unsigned DEFAULT '5',
  `AnnualPlanId` int(10) unsigned DEFAULT '0',
  `ContactPersonFirstName` varchar(100) NOT NULL,
  `ContactPersonLastName` varchar(100) NOT NULL,
  `ContactPersonEmail` varchar(100) DEFAULT NULL,
  `ContactPersonPhone` char(10) NOT NULL,
  `Payor` varchar(100) NOT NULL DEFAULT '1',
  `Comments` varchar(150) DEFAULT NULL,
  `CustomFields` varchar(250) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CahpsVendor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `CahpsVendorId` varchar(50) DEFAULT NULL,
  `CahpsSurveyDesignator` varchar(50) DEFAULT NULL,
  `IsAxxessTheBiller` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OasisAuditVendor` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `OasisAuditVendorApiKey` varchar(250) DEFAULT NULL,
  `FrozenDate` date NOT NULL DEFAULT '0001-01-01',
  `IsFrozen` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PreviousSoftware` varchar(100) DEFAULT NULL,
  `Services` int(10) unsigned NOT NULL DEFAULT '1',
  `DBServerIp` varchar(32) NOT NULL DEFAULT '10.0.5.62',
  `AccountId` char(8) DEFAULT NULL,
  `ClusterId` tinyint(3) unsigned NOT NULL DEFAULT '2',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table accounting.agencyprofiles
CREATE TABLE IF NOT EXISTS `agencyprofiles` (
  `Id` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `AgencyId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AssetId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsSuspendPayment` tinyint(1) unsigned DEFAULT '0',
  `FrozenReason` text,
  `EffectiveDate` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyProfiles_AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table accounting.agencysubscriptionplans
CREATE TABLE IF NOT EXISTS `agencysubscriptionplans` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `AgencyLocationId` char(36) NOT NULL,
  `IsUserPlan` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `PlanLimit` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencySubscriptionPlans_AgencyId` (`AgencyId`),
  KEY `FK_AgencySubscriptionPlans_AgencyLocationId` (`AgencyLocationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table accounting.agencyupgrades
CREATE TABLE IF NOT EXISTS `agencyupgrades` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `AgencyLocationId` char(36) DEFAULT NULL,
  `RequestedById` char(36) NOT NULL,
  `AccountId` char(8) DEFAULT '0',
  `PreviousAmount` double DEFAULT '-1',
  `Amount` double DEFAULT '-1',
  `PreviousPackageId` int(10) unsigned NOT NULL DEFAULT '0',
  `RequestedPackageId` int(10) unsigned NOT NULL DEFAULT '0',
  `AnnualPlanId` int(10) unsigned NOT NULL DEFAULT '0',
  `IsComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsUserPlan` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Type` char(25) DEFAULT NULL,
  `Comments` tinytext,
  `EffectiveDate` date DEFAULT '0001-01-01',
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyUpgrades_AgencyId` (`AgencyId`),
  KEY `FK_AgencyUpgrades_RequestedById` (`RequestedById`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table accounting.assets
CREATE TABLE IF NOT EXISTS `assets` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `FileName` varchar(50) NOT NULL,
  `FileSize` varchar(20) NOT NULL,
  `ContentType` varchar(20) NOT NULL,
  `Bytes` mediumblob NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Asset_AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table accounting.information
CREATE TABLE IF NOT EXISTS `information` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Account` varchar(8) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `City` varchar(100) NOT NULL,
  `State` varchar(2) DEFAULT NULL,
  `Zip` int(11) DEFAULT NULL,
  `ImplementationFee` varchar(100) DEFAULT NULL,
  `PaymentSource` varchar(100) DEFAULT NULL,
  `InitialSignDate` date DEFAULT NULL,
  `SpecialNotes` text,
  `Status` tinyint(4) NOT NULL DEFAULT '1',
  `SalesPerson` char(100) DEFAULT NULL,
  `StartUp` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Account` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table accounting.informationbuckup
CREATE TABLE IF NOT EXISTS `informationbuckup` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Account` varchar(8) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `City` varchar(100) NOT NULL,
  `State` varchar(2) DEFAULT NULL,
  `Zip` int(11) DEFAULT NULL,
  `ImplementationFee` varchar(100) DEFAULT NULL,
  `PaymentSource` varchar(100) DEFAULT NULL,
  `InitialSignDate` date DEFAULT NULL,
  `SpecialNotes` text,
  `Status` tinyint(4) NOT NULL DEFAULT '1',
  `SalesPerson` char(100) DEFAULT NULL,
  `StartUp` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Account` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table accounting.informationnews
CREATE TABLE IF NOT EXISTS `informationnews` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Account` varchar(8) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `City` varchar(100) NOT NULL,
  `State` varchar(2) DEFAULT NULL,
  `Zip` int(11) DEFAULT NULL,
  `ImplementationFee` varchar(100) DEFAULT NULL,
  `PaymentSource` varchar(100) DEFAULT NULL,
  `InitialSignDate` date DEFAULT NULL,
  `SpecialNotes` text,
  `Status` tinyint(4) NOT NULL DEFAULT '1',
  `SalesPerson` char(100) DEFAULT NULL,
  `StartUp` tinyint(4) NOT NULL DEFAULT '0',
  `updated` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Account` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table accounting.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Account` varchar(8) DEFAULT NULL,
  `Sequence` int(11) DEFAULT NULL,
  `Amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Start` date NOT NULL DEFAULT '1000-01-01',
  `End` date DEFAULT '9999-12-31',
  `Package` int(10) unsigned DEFAULT '5',
  PRIMARY KEY (`id`),
  KEY `Account` (`Account`),
  KEY `Start` (`Start`),
  KEY `End` (`End`),
  KEY `IX_Payment_Dates` (`Start`,`End`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for agencymanagement
CREATE DATABASE IF NOT EXISTS `agencymanagement` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `agencymanagement`;


-- Dumping structure for table agencymanagement.agencies
CREATE TABLE IF NOT EXISTS `agencies` (
  `Id` char(36) NOT NULL,
  `SalesPerson` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Trainer` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `BackupTrainer` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `ImplementationSpecialist` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Name` varchar(100) NOT NULL,
  `TaxId` char(10) NOT NULL,
  `TaxIdType` varchar(50) DEFAULT NULL,
  `NationalProviderNumber` char(10) DEFAULT NULL,
  `MedicareProviderNumber` char(9) DEFAULT NULL,
  `MedicaidProviderNumber` char(9) DEFAULT NULL,
  `HomeHealthAgencyId` varchar(20) DEFAULT NULL,
  `SubmitterId` varchar(20) DEFAULT NULL,
  `SubmitterName` varchar(50) DEFAULT NULL,
  `SubmitterPhone` char(10) DEFAULT NULL,
  `SubmitterFax` char(10) DEFAULT NULL,
  `IsAgreementSigned` tinyint(1) unsigned DEFAULT '1',
  `IsSuspended` tinyint(1) unsigned DEFAULT '0',
  `TrialPeriod` int(10) unsigned DEFAULT '30',
  `Package` int(10) unsigned DEFAULT '5',
  `AnnualPlanId` int(10) unsigned DEFAULT '1',
  `ContactPersonFirstName` varchar(100) NOT NULL,
  `ContactPersonLastName` varchar(100) NOT NULL,
  `ContactPersonEmail` varchar(100) DEFAULT NULL,
  `ContactPersonPhone` char(10) NOT NULL,
  `Payor` varchar(100) DEFAULT NULL,
  `Comments` varchar(150) DEFAULT NULL,
  `CustomFields` varchar(250) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CahpsVendor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `CahpsVendorId` varchar(50) DEFAULT NULL,
  `CahpsSurveyDesignator` varchar(50) DEFAULT NULL,
  `IsAxxessTheBiller` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OasisAuditVendor` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `OasisAuditVendorApiKey` varchar(250) DEFAULT '',
  `FrozenDate` date NOT NULL DEFAULT '0001-01-01',
  `IsFrozen` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OasisAuditApiKey` varchar(250) NOT NULL DEFAULT '',
  `PreviousSoftware` varchar(200) DEFAULT NULL,
  `Test` varchar(100) DEFAULT NULL,
  `DBServerIp` varchar(32) DEFAULT '10.0.1.61',
  `Services` int(10) unsigned NOT NULL DEFAULT '1',
  `AccountId` varchar(8) DEFAULT NULL,
  `MigrationStatus` tinyint(1) DEFAULT '0',
  `ClusterId` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyadjustmentcodes
CREATE TABLE IF NOT EXISTS `agencyadjustmentcodes` (
  `Id` varchar(36) NOT NULL,
  `AgencyId` varchar(36) NOT NULL,
  `Code` varchar(100) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Adjustments_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Adjustments_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencychanges
CREATE TABLE IF NOT EXISTS `agencychanges` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `TaxId` char(10) DEFAULT NULL,
  `TaxIdType` varchar(50) DEFAULT NULL,
  `ContactPersonEmail` varchar(100) DEFAULT NULL,
  `ContactPersonPhone` char(10) DEFAULT NULL,
  `CahpsVendor` tinyint(3) unsigned DEFAULT NULL,
  `NationalProviderNumber` char(10) DEFAULT NULL,
  `MedicareProviderNumber` char(9) DEFAULT NULL,
  `MedicaidProviderNumber` char(9) DEFAULT NULL,
  `HomeHealthAgencyId` varchar(20) DEFAULT NULL,
  `ContactPersonFirstName` varchar(100) DEFAULT NULL,
  `ContactPersonLastName` varchar(100) DEFAULT NULL,
  `LocationName` varchar(100) DEFAULT NULL,
  `LocationAddressLine1` varchar(100) DEFAULT NULL,
  `LocationAddressLine2` varchar(100) DEFAULT NULL,
  `LocationAddressCity` varchar(100) DEFAULT NULL,
  `LocationAddressStateCode` char(2) DEFAULT NULL,
  `LocationAddressZipCode` char(5) DEFAULT NULL,
  `LocationPhoneWork` char(10) DEFAULT NULL,
  `LocationFaxNumber` char(10) DEFAULT NULL,
  `FullName` varchar(50) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `IsComplete` tinyint(3) unsigned DEFAULT '0',
  `Comments` text,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyChanges_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_AgencyChanges_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencycontacts
CREATE TABLE IF NOT EXISTS `agencycontacts` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `FirstName` varchar(100) DEFAULT NULL,
  `LastName` varchar(100) DEFAULT NULL,
  `CompanyName` varchar(100) DEFAULT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` varchar(5) DEFAULT NULL,
  `PhonePrimary` char(10) DEFAULT NULL,
  `PhoneAlternate` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `ContactType` varchar(50) DEFAULT NULL,
  `ContactTypeOther` varchar(100) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Comments` text,
  `PhoneExtension` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Contacts_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Contacts_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyhospitals
CREATE TABLE IF NOT EXISTS `agencyhospitals` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `ContactPersonFirstName` varchar(100) DEFAULT NULL,
  `ContactPersonLastName` varchar(100) DEFAULT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) NOT NULL,
  `AddressStateCode` char(2) NOT NULL,
  `AddressZipCode` char(5) NOT NULL,
  `Phone` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `Comment` varchar(500) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Hospitals_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Hospitals_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyinsurances
CREATE TABLE IF NOT EXISTS `agencyinsurances` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `PayorType` int(1) NOT NULL,
  `InvoiceType` int(1) NOT NULL,
  `ChargeGrouping` int(1) NOT NULL,
  `ChargeType` int(1) NOT NULL,
  `ParentInsurance` int(1) NOT NULL,
  `IsInsuranceRateUsed` tinyint(1) unsigned DEFAULT '0',
  `Name` varchar(100) NOT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `OtherProviderId` varchar(40) DEFAULT NULL,
  `ProviderId` varchar(40) DEFAULT NULL,
  `HealthPlanId` varchar(40) DEFAULT NULL,
  `ProviderSubscriberId` varchar(40) DEFAULT NULL,
  `Ub04Locator81cca` text,
  `PrintContentOnly` tinyint(1) DEFAULT '0',
  `HCFALocators` text,
  `Charge` text,
  `ClearingHouse` varchar(100) NOT NULL,
  `InterchangeReceiverId` int(1) NOT NULL,
  `PayorId` varchar(20) NOT NULL,
  `ContactPersonFirstName` varchar(100) DEFAULT NULL,
  `ContactPersonLastName` varchar(100) DEFAULT NULL,
  `ContactEmailAddress` varchar(100) DEFAULT NULL,
  `SubmitterId` varchar(20) DEFAULT NULL,
  `PhoneNumber` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `CurrentBalance` decimal(10,0) DEFAULT NULL,
  `WorkWeekStartDay` int(1) NOT NULL,
  `IsVisitAuthorizationRequired` tinyint(1) NOT NULL,
  `DefaultFiscalIntermediary` tinyint(1) NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsAxxessTheBiller` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SubmitterName` varchar(100) DEFAULT NULL,
  `SubmitterPhone` char(10) DEFAULT NULL,
  `ClearingHouseSubmitterId` varchar(10) DEFAULT NULL,
  `BillType` varchar(20) DEFAULT NULL,
  `BillData` mediumtext,
  `IsOASISMatchKeyAuth` tinyint(1) DEFAULT '0',
  `HasContractWithAgency` tinyint(1) unsigned DEFAULT '0',
  `IsUsingPatientRates` tinyint(1) unsigned DEFAULT '0',
  `RequireServiceLocation` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Insurance_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Insurance_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencylocations
CREATE TABLE IF NOT EXISTS `agencylocations` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `FullName` varchar(100) DEFAULT NULL,
  `CustomId` varchar(15) DEFAULT NULL,
  `CBSA` char(5) DEFAULT NULL,
  `MedicareProviderNumber` char(10) DEFAULT NULL,
  `AddressLine1` varchar(100) NOT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) NOT NULL,
  `AddressStateCode` char(2) NOT NULL,
  `AddressZipCode` char(5) NOT NULL,
  `AddressZipCodeFour` char(4) DEFAULT NULL,
  `ZipNineStatus` char(1) DEFAULT 'P',
  `PhoneWork` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `Comments` varchar(100) DEFAULT NULL,
  `IsMainOffice` tinyint(1) unsigned NOT NULL,
  `Cost` text,
  `BillData` mediumtext,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSubmitterInfoTheSame` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `SubmitterId` varchar(50) DEFAULT '',
  `SubmitterName` varchar(50) DEFAULT '',
  `SubmitterPhone` varchar(10) DEFAULT '',
  `SubmitterFax` varchar(10) DEFAULT '',
  `Payor` varchar(5) DEFAULT '',
  `BranchId` varchar(20) DEFAULT 'N',
  `BranchIdOther` varchar(20) DEFAULT 'N',
  `Ub04Locator81cca` text,
  `TaxId` char(10) DEFAULT NULL,
  `TaxIdType` varchar(50) DEFAULT NULL,
  `NationalProviderNumber` char(10) DEFAULT NULL,
  `MedicaidProviderNumber` char(9) DEFAULT NULL,
  `HomeHealthAgencyId` varchar(20) DEFAULT NULL,
  `ContactPersonFirstName` varchar(100) DEFAULT NULL,
  `ContactPersonLastName` varchar(100) DEFAULT NULL,
  `ContactPersonEmail` varchar(100) DEFAULT NULL,
  `ContactPersonPhone` varchar(100) DEFAULT NULL,
  `CahpsVendor` tinyint(3) DEFAULT NULL,
  `CahpsVendorId` varchar(50) DEFAULT NULL,
  `CahpsSurveyDesignator` varchar(50) DEFAULT NULL,
  `IsAxxessTheBiller` tinyint(1) DEFAULT NULL,
  `OasisAuditVendor` tinyint(3) DEFAULT NULL,
  `IsLocationStandAlone` tinyint(1) DEFAULT '0',
  `Modified` date NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Location_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Location_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencymedicareinsurances
CREATE TABLE IF NOT EXISTS `agencymedicareinsurances` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `MedicareId` int(10) NOT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(9) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyMedicareInsurances_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_AgencyMedicareInsurances_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencynonvisits
CREATE TABLE IF NOT EXISTS `agencynonvisits` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Text` text NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Nonvisit_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Nonvisit_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencypharmacies
CREATE TABLE IF NOT EXISTS `agencypharmacies` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `ContactPersonFirstName` varchar(100) DEFAULT NULL,
  `ContactPersonLastName` varchar(100) DEFAULT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `Phone` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `Comment` varchar(500) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Pharmacies_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Pharmacies_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyphysicians
CREATE TABLE IF NOT EXISTS `agencyphysicians` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `LoginId` char(36) DEFAULT NULL,
  `NPI` char(10) DEFAULT NULL,
  `UPIN` char(6) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `Credentials` varchar(50) DEFAULT NULL,
  `AddressLine1` varchar(100) DEFAULT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(9) DEFAULT NULL,
  `PhoneWork` char(10) DEFAULT NULL,
  `PhoneAlternate` char(10) DEFAULT NULL,
  `FaxNumber` char(10) DEFAULT NULL,
  `EmailAddress` varchar(50) DEFAULT NULL,
  `LicenseNumber` varchar(50) DEFAULT NULL,
  `LicenseStateCode` char(2) DEFAULT NULL,
  `Comments` varchar(100) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PhysicianAccess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Licenses` text,
  PRIMARY KEY (`Id`),
  KEY `FK_Physician_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_AgencyPhysicians_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyprofiles
CREATE TABLE IF NOT EXISTS `agencyprofiles` (
  `Id` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `AgencyId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AssetId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsSuspendPayment` tinyint(1) unsigned DEFAULT '0',
  `FrozenReason` text,
  `EffectiveDate` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyProfiles_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_AgencyProfiles_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencysubscriptionplans
CREATE TABLE IF NOT EXISTS `agencysubscriptionplans` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `AgencyLocationId` char(36) NOT NULL,
  `IsUserPlan` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `PlanLimit` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencySubscriptionPlans_AgencyId` (`AgencyId`),
  KEY `FK_AgencySubscriptionPlans_AgencyLocationId` (`AgencyLocationId`),
  CONSTRAINT `FK_AgencySubscriptionPlans_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_AgencySubscriptionPlans_AgencyLocationId` FOREIGN KEY (`AgencyLocationId`) REFERENCES `agencylocations` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencysupplies
CREATE TABLE IF NOT EXISTS `agencysupplies` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Code` char(5) DEFAULT NULL,
  `Description` varchar(350) NOT NULL,
  `UnitCost` decimal(8,2) unsigned DEFAULT '0.00',
  `RevenueCode` char(5) DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Templates_AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencysystemmessages
CREATE TABLE IF NOT EXISTS `agencysystemmessages` (
  `SystemMessageId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  PRIMARY KEY (`SystemMessageId`,`AgencyId`),
  CONSTRAINT `FK_agencysystemmessages_systemmessages` FOREIGN KEY (`SystemMessageId`) REFERENCES `systemmessages` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyteams
CREATE TABLE IF NOT EXISTS `agencyteams` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Name` varchar(80) NOT NULL,
  `Users` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencytemplates
CREATE TABLE IF NOT EXISTS `agencytemplates` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Text` text NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Templates_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Templates_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.agencyupgrades
CREATE TABLE IF NOT EXISTS `agencyupgrades` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `AgencyLocationId` char(36) DEFAULT NULL,
  `RequestedById` char(36) NOT NULL,
  `AccountId` char(8) DEFAULT '0',
  `PreviousAmount` double DEFAULT '-1',
  `Amount` double DEFAULT '-1',
  `PreviousPackageId` int(10) unsigned NOT NULL DEFAULT '0',
  `RequestedPackageId` int(10) unsigned NOT NULL DEFAULT '0',
  `AnnualPlanId` int(10) unsigned NOT NULL DEFAULT '0',
  `IsComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsUserPlan` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Type` char(25) DEFAULT NULL,
  `Comments` tinytext,
  `EffectiveDate` date DEFAULT '0001-01-01',
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyUpgrades_AgencyId` (`AgencyId`),
  KEY `FK_AgencyUpgrades_RequestedById` (`RequestedById`),
  CONSTRAINT `FK_AgencyUpgrades_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_AgencyUpgrades_RequestedById` FOREIGN KEY (`RequestedById`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.allergyprofiles
CREATE TABLE IF NOT EXISTS `allergyprofiles` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Allergies` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AllergyProfiles_PatientId` (`PatientId`),
  KEY `FK_AllergyProfiles_AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.assessments
CREATE TABLE IF NOT EXISTS `assessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) NOT NULL,
  `IsValidated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.assets
CREATE TABLE IF NOT EXISTS `assets` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `FileName` varchar(50) NOT NULL,
  `FileSize` varchar(20) NOT NULL,
  `ContentType` varchar(20) NOT NULL,
  `Bytes` mediumblob NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Asset_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_Asset_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.authorizations
CREATE TABLE IF NOT EXISTS `authorizations` (
  `Id` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `AssetId` char(36) DEFAULT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `AgencyLocationId` char(36) NOT NULL,
  `Number1` varchar(30) DEFAULT NULL,
  `Number2` varchar(30) DEFAULT NULL,
  `Number3` varchar(30) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Insurance` varchar(50) DEFAULT NULL,
  `SNVisit` varchar(20) DEFAULT NULL,
  `SNVisitCountType` varchar(20) DEFAULT NULL,
  `PTVisit` varchar(20) DEFAULT NULL,
  `PTVisitCountType` varchar(20) DEFAULT NULL,
  `OTVisit` varchar(20) DEFAULT NULL,
  `OTVisitCountType` varchar(20) DEFAULT NULL,
  `STVisit` varchar(20) DEFAULT NULL,
  `STVisitCountType` varchar(20) DEFAULT NULL,
  `MSWVisit` varchar(20) DEFAULT NULL,
  `MSWVisitCountType` varchar(20) DEFAULT NULL,
  `HHAVisit` varchar(20) DEFAULT NULL,
  `HHAVisitCountType` varchar(20) DEFAULT NULL,
  `DieticianVisit` varchar(20) DEFAULT NULL,
  `DieticianVisitCountType` varchar(20) DEFAULT NULL,
  `RNVisit` varchar(20) DEFAULT NULL,
  `RNVisitCountType` varchar(20) DEFAULT NULL,
  `LVNVisit` varchar(20) DEFAULT NULL,
  `LVNVisitCountType` varchar(20) DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `Comments` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Service` tinyint(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `FK_Authorizations_UserId` (`UserId`),
  KEY `FK_Authorizations_AgencyId` (`AgencyId`),
  KEY `FK_Authorizations_PatientId` (`PatientId`),
  CONSTRAINT `FK_Authorizations_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Authorizations_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Authorizations_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.axxesssubmitterinfos
CREATE TABLE IF NOT EXISTS `axxesssubmitterinfos` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `SubmitterId` varchar(50) DEFAULT NULL,
  `SubmitterName` varchar(50) DEFAULT NULL,
  `Phone` varchar(10) DEFAULT NULL,
  `Fax` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.careplanoversights
CREATE TABLE IF NOT EXISTS `careplanoversights` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `PhysicianLoginId` char(36) NOT NULL,
  `CptCode` text,
  `Duration` int(11) DEFAULT NULL,
  `Activity` tinyint(4) DEFAULT NULL,
  `Comments` mediumtext,
  `LogDate` date DEFAULT NULL,
  `Status` int(10) DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Modified` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.claimdatas
CREATE TABLE IF NOT EXISTS `claimdatas` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `Data` text,
  `BillIdentifers` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `ClaimType` enum('MAN','CMS','HMO') DEFAULT 'CMS',
  `Response` mediumtext,
  `IsDeprecated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.claimnumbers
CREATE TABLE IF NOT EXISTS `claimnumbers` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EntityId` char(36) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `EntityId_Unique` (`EntityId`),
  KEY `EntityId` (`EntityId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This stores claim numbers for managed claims. ';

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.communicationnotes
CREATE TABLE IF NOT EXISTS `communicationnotes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `Text` text,
  `Recipients` text,
  `Created` date NOT NULL DEFAULT '0001-01-01',
  `Modified` date NOT NULL DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(1) DEFAULT '0',
  `Status` int(4) DEFAULT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianData` text,
  PRIMARY KEY (`Id`),
  KEY `FK_Commnotes_PatientId` (`PatientId`),
  KEY `FK_Commnotes_AgencyId` (`AgencyId`),
  KEY `FK_Commnotes_UserId` (`UserId`),
  CONSTRAINT `FK_Commnotes_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Commnotes_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Commnotes_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.customernotes
CREATE TABLE IF NOT EXISTS `customernotes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `Comments` text NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Recipients` text,
  `NoteType` tinyint(3) unsigned NOT NULL,
  `Summary` varchar(80) NOT NULL,
  `CallerId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IssueId` int(10) unsigned DEFAULT NULL,
  `AttachmentId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsCompleted` tinyint(1) unsigned NOT NULL,
  `DevUpdates` text,
  `AdditionalInformation` text,
  `ModifiedBy` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `Status` tinyint(100) unsigned NOT NULL DEFAULT '0',
  `Saved` tinyint(4) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `LoginId` (`LoginId`),
  CONSTRAINT `FK_CustomerNotes_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.dashboardmessages
CREATE TABLE IF NOT EXISTS `dashboardmessages` (
  `Id` char(36) NOT NULL,
  `Title` varchar(150) NOT NULL,
  `Text` text NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.deleteditems
CREATE TABLE IF NOT EXISTS `deleteditems` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `Schedule` mediumtext NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PatientEpisode_PatientId` (`PatientId`),
  KEY `FK_PatientEpisode_AgencyId` (`AgencyId`),
  KEY `EpisodeId` (`EpisodeId`),
  CONSTRAINT `FK_DeletedItems_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DeletedItems_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DeletedItems_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.facetofaceencounters
CREATE TABLE IF NOT EXISTS `facetofaceencounters` (
  `Id` char(36) NOT NULL DEFAULT '',
  `OrderNumber` int(20) unsigned NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `Status` int(36) DEFAULT NULL,
  `Certification` varchar(36) DEFAULT NULL,
  `EncounterDate` date DEFAULT '0001-01-01',
  `ReceivedDate` date DEFAULT '0001-01-01',
  `RequestDate` date DEFAULT '0001-01-01',
  `SentDate` date DEFAULT '0001-01-01',
  `ClinicalFinding` text,
  `Services` varchar(50) DEFAULT NULL,
  `ServicesOther` varchar(50) DEFAULT NULL,
  `SignatureText` varchar(50) DEFAULT NULL,
  `SignatureDate` date DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(4) DEFAULT '0',
  `Modified` date DEFAULT '0001-01-01',
  `Created` date DEFAULT '0001-01-01',
  `MedicalReason` text,
  `PhysicianData` text,
  PRIMARY KEY (`Id`),
  KEY `PhysicianId` (`PhysicianId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.finals
CREATE TABLE IF NOT EXISTS `finals` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `IsOasisComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FirstBillableVisitDate` date DEFAULT NULL,
  `Remark` text,
  `MedicareNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(9) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `AreOrdersComplete` tinyint(1) unsigned DEFAULT '0',
  `AreVisitsComplete` tinyint(1) unsigned DEFAULT '0',
  `IsGenerated` tinyint(1) unsigned DEFAULT '0',
  `Modified` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `VerifiedVisits` mediumtext,
  `Supply` mediumtext,
  `SupplyTotal` double(10,0) DEFAULT NULL,
  `Payment` double NOT NULL DEFAULT '0',
  `PrimaryInsuranceId` int(4) unsigned NOT NULL,
  `IsSupplyVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFinalInfoVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVisitVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsRapGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Status` int(4) unsigned NOT NULL DEFAULT '0',
  `Type` int(4) unsigned DEFAULT '0',
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) unsigned DEFAULT '0',
  `ClaimDate` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `ProspectivePay` double unsigned NOT NULL DEFAULT '0',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `DischargeDate` date DEFAULT '0001-01-01',
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `GroupName` varchar(50) DEFAULT NULL,
  `GroupId` varchar(50) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `Authorization` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `AuthorizationNumber2` varchar(50) DEFAULT NULL,
  `AuthorizationNumber3` varchar(50) DEFAULT NULL,
  `ConditionCodes` text NOT NULL,
  `UB4PatientStatus` varchar(5) DEFAULT NULL,
  `CBSA` varchar(6) DEFAULT NULL,
  `Ub04Locator81cca` text,
  `Ub04Locator39` text,
  `Insurance` longtext,
  `Remittance` text,
  `IsSupplyNotBillable` tinyint(1) DEFAULT '0',
  `UseStandardSupplies` tinyint(1) DEFAULT '1',
  `Ub04Locator31` text,
  `Ub04Locator32` text,
  `Ub04Locator33` text,
  `Ub04Locator34` text,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `AgencyId_Status` (`AgencyId`,`Status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.finalsnapshots
CREATE TABLE IF NOT EXISTS `finalsnapshots` (
  `MainId` char(46) NOT NULL,
  `Id` char(36) NOT NULL,
  `BatchId` bigint(20) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `IsOasisComplete` tinyint(1) unsigned NOT NULL,
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL,
  `FirstBillableVisitDate` date DEFAULT NULL,
  `Remark` text,
  `MedicareNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(9) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `AreOrdersComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AreVisitsComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `VerifiedVisits` text,
  `Supply` text,
  `SupplyTotal` double DEFAULT NULL,
  `Payment` double unsigned DEFAULT '0',
  `PrimaryInsuranceId` tinyint(4) unsigned NOT NULL,
  `IsSupplyVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFinalInfoVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVisitVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsRapGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Status` int(4) unsigned NOT NULL DEFAULT '0',
  `Type` int(4) unsigned DEFAULT '0',
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) DEFAULT '0',
  `ClaimDate` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `ProspectivePay` double unsigned DEFAULT '0',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `DischargeDate` date DEFAULT '0001-01-01',
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `Reason` text,
  PRIMARY KEY (`MainId`),
  KEY `IX_FinalSnapShots_AgencyId_Id` (`AgencyId`,`Id`),
  CONSTRAINT `FK_FinalSnapshots_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.hospitalizationlogs
CREATE TABLE IF NOT EXISTS `hospitalizationlogs` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) NOT NULL,
  `SourceId` tinyint(3) unsigned NOT NULL,
  `Data` text NOT NULL,
  `HospitalizationDate` date NOT NULL DEFAULT '0001-01-01',
  `LastHomeVisitDate` date NOT NULL DEFAULT '0001-01-01',
  `ReturnDate` date NOT NULL DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_PatientTransferLogs_AgencyId` (`AgencyId`),
  KEY `FK_PatientTransferLogs_PatientId` (`PatientId`),
  KEY `FK_PatientTransferLogs_UserId` (`UserId`),
  CONSTRAINT `FK_PatientTransferLogs_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientTransferLogs_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientTransferLogs_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.incidents
CREATE TABLE IF NOT EXISTS `incidents` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `IndividualInvolved` varchar(150) DEFAULT NULL,
  `IndividualInvolvedOther` varchar(100) DEFAULT NULL,
  `IncidentDate` date NOT NULL,
  `IncidentType` varchar(100) DEFAULT NULL,
  `Description` text,
  `ActionTaken` text,
  `MDNotified` varchar(20) DEFAULT NULL,
  `FamilyNotified` varchar(20) DEFAULT NULL,
  `NewOrdersCreated` varchar(20) DEFAULT NULL,
  `Orders` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Status` int(10) unsigned NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FollowUp` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.infections
CREATE TABLE IF NOT EXISTS `infections` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `InfectionDate` date NOT NULL,
  `InfectionType` varchar(150) DEFAULT NULL,
  `InfectionTypeOther` varchar(50) DEFAULT NULL,
  `Treatment` text,
  `Orders` text,
  `MDNotified` varchar(20) DEFAULT NULL,
  `TreatmentPrescribed` varchar(20) DEFAULT NULL,
  `NewOrdersCreated` varchar(20) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Status` int(10) unsigned NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FollowUp` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.licenseitems
CREATE TABLE IF NOT EXISTS `licenseitems` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `AssetId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `LastName` varchar(100) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LicenseType` varchar(150) NOT NULL,
  `IssueDate` date NOT NULL,
  `ExpireDate` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  KEY `FK_LicenseItems_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_LicenseItems_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.managedclaimadjustments
CREATE TABLE IF NOT EXISTS `managedclaimadjustments` (
  `Id` varchar(36) NOT NULL,
  `AgencyId` varchar(36) NOT NULL,
  `ClaimId` varchar(36) NOT NULL,
  `PatientId` varchar(36) NOT NULL,
  `Adjustment` double NOT NULL DEFAULT '0',
  `TypeId` varchar(36) NOT NULL,
  `Comments` text NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ManagedClaimAdjustments_AgencyId` (`AgencyId`),
  KEY `FK_ManagedClaimAdjustments_PatientId` (`PatientId`),
  KEY `FK_ManagedClaimAdjustments_ClaimId` (`ClaimId`),
  CONSTRAINT `FK_ManagedClaimAdjustments_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ManagedClaimAdjustments_ClaimId` FOREIGN KEY (`ClaimId`) REFERENCES `managedclaims` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ManagedClaimAdjustments_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.managedclaimpayments
CREATE TABLE IF NOT EXISTS `managedclaimpayments` (
  `Id` varchar(36) NOT NULL,
  `AgencyId` varchar(36) NOT NULL,
  `ClaimId` varchar(36) NOT NULL,
  `PatientId` varchar(36) NOT NULL,
  `Payment` double NOT NULL DEFAULT '0',
  `PaymentDate` date NOT NULL DEFAULT '0001-01-01',
  `Comments` text,
  `Payor` int(11) NOT NULL DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ManagedClaimPayments_ClaimId` (`ClaimId`),
  KEY `FK_ManagedClaimPayments_PatientId` (`PatientId`),
  KEY `FK_ManagedClaimPayments_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_ManagedClaimPayments_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ManagedClaimPayments_ClaimId` FOREIGN KEY (`ClaimId`) REFERENCES `managedclaims` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_ManagedClaimPayments_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.managedclaims
CREATE TABLE IF NOT EXISTS `managedclaims` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `IsOasisComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FirstBillableVisitDate` date DEFAULT NULL,
  `Remark` text,
  `IsuranceIdNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(9) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `AreOrdersComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AreVisitsComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `VerifiedVisits` mediumtext,
  `Supply` text,
  `SupplyTotal` double DEFAULT NULL,
  `Payment` double unsigned DEFAULT '0',
  `PrimaryInsuranceId` int(4) unsigned NOT NULL,
  `IsSupplyVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsInfoVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVisitVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsInsuranceVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsRapGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Status` int(4) unsigned NOT NULL DEFAULT '0',
  `Type` varchar(10) DEFAULT '',
  `DischargeDate` date DEFAULT '0001-01-01',
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) DEFAULT '0',
  `ClaimDate` date NOT NULL DEFAULT '0001-01-01',
  `ProspectivePay` double unsigned DEFAULT '0',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `GroupName` varchar(50) DEFAULT NULL,
  `GroupId` varchar(50) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `Authorization` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `AuthorizationNumber2` varchar(50) DEFAULT NULL,
  `AuthorizationNumber3` varchar(50) DEFAULT NULL,
  `ConditionCodes` text,
  `SupplyCode` varchar(10) DEFAULT '0272',
  `UB4PatientStatus` varchar(5) DEFAULT NULL,
  `CBSA` varchar(6) DEFAULT NULL,
  `Ub04Locator81cca` text,
  `Ub04Locator39` text,
  `Insurance` mediumtext,
  `IsHomeHealthServiceIncluded` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `Ub04Locator31` text,
  `Ub04Locator32` text,
  `Ub04Locator33` text,
  `Ub04Locator34` text,
  `HCFALocators` text,
  PRIMARY KEY (`Id`),
  KEY `FK_Finals_AgencyId` (`AgencyId`),
  KEY `FK_Finals_PatientId` (`PatientId`),
  KEY `FK_Finals_EpisodeId` (`EpisodeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.medicareeligibilities
CREATE TABLE IF NOT EXISTS `medicareeligibilities` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `Result` mediumtext NOT NULL,
  `Status` int(3) unsigned NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsHiqh` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_MedicareEligibilities_AgencyId` (`AgencyId`),
  KEY `FK_MedicareEligibilities_PatientId` (`PatientId`),
  CONSTRAINT `FK_MedicareEligibilities_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MedicareEligibilities_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.medicareeligibilitysummaries
CREATE TABLE IF NOT EXISTS `medicareeligibilitysummaries` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Report` mediumtext,
  `Created` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_MedicareEligibilitySummaries_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_MedicareEligibilitySummaries_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.medicationprofilehistories
CREATE TABLE IF NOT EXISTS `medicationprofilehistories` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `Medication` text,
  `Allergies` varchar(150) DEFAULT NULL,
  `PrimaryDiagnosis` varchar(200) DEFAULT NULL,
  `SecondaryDiagnosis` varchar(200) DEFAULT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignedDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PharmacyPhone` char(10) DEFAULT NULL,
  `PharmacyFax` char(10) DEFAULT NULL,
  `PharmacyName` varchar(50) DEFAULT NULL,
  `SecondaryPharmacyPhone` char(10) DEFAULT NULL,
  `SecondaryPharmacyFax` char(10) DEFAULT NULL,
  `SecondaryPharmacyName` varchar(50) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Modified` date DEFAULT NULL,
  `PhysicianData` text,
  `AssociatedAssessment` char(36) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_MedicationProfiles_AgencyId` (`AgencyId`),
  KEY `FK_MedicationProfiles_PatientId` (`PatientId`),
  KEY `FK_MedicationProfiles_EpisodeId` (`EpisodeId`),
  KEY `FK_MedicationProfiles_UserId` (`UserId`),
  CONSTRAINT `FK_MedicationProfiles_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MedicationProfiles_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MedicationProfiles_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MedicationProfiles_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.medicationprofiles
CREATE TABLE IF NOT EXISTS `medicationprofiles` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Medication` text,
  `PharmacyName` varchar(50) DEFAULT NULL,
  `PharmacyPhone` char(10) DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_MedicationProfileHistories_PatientId` (`PatientId`),
  KEY `FK_MedicationProfileHistories_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_MedicationProfileHistories_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MedicationProfileHistories_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.messagedetails
CREATE TABLE IF NOT EXISTS `messagedetails` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `FromId` char(36) NOT NULL,
  `FromName` varchar(100) NOT NULL,
  `AttachmentId` char(36) DEFAULT NULL,
  `RecipientNames` varchar(500) DEFAULT NULL,
  `CarbonCopyNames` varchar(500) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `Subject` varchar(100) NOT NULL,
  `Body` text NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `SenderId` (`FromId`),
  KEY `FK_Messages_PatientId` (`PatientId`),
  KEY `FK_MessageDetails_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_MessageDetails_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.messagefolders
CREATE TABLE IF NOT EXISTS `messagefolders` (
  `Id` char(36) NOT NULL,
  `ParentId` char(36) DEFAULT NULL,
  `OwnerId` char(36) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `Name` varchar(50) NOT NULL DEFAULT 'Inbox',
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ParentId` (`ParentId`),
  KEY `FK_MessageFolder_UserId` (`OwnerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `FromId` char(36) NOT NULL,
  `FromName` varchar(100) NOT NULL,
  `RecipientId` char(36) NOT NULL,
  `AttachmentId` char(36) DEFAULT NULL,
  `RecipientNames` varchar(500) DEFAULT NULL,
  `CarbonCopyNames` varchar(500) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `Subject` varchar(100) NOT NULL,
  `Body` text NOT NULL,
  `MarkAsRead` tinyint(1) unsigned NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `FolderId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `SenderId` (`FromId`),
  KEY `FK_Messages_RecipientId` (`RecipientId`),
  KEY `FK_Messages_PatientId` (`PatientId`),
  KEY `FK_Messages_AgencyId` (`AgencyId`),
  KEY `FolderId` (`FolderId`),
  CONSTRAINT `FK_Messages_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`),
  CONSTRAINT `FK_Messages_RecipientId` FOREIGN KEY (`RecipientId`) REFERENCES `users` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Messages_UserId` FOREIGN KEY (`FromId`) REFERENCES `users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.missedvisits
CREATE TABLE IF NOT EXISTS `missedvisits` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserSignatureAssetId` char(36) DEFAULT NULL,
  `Date` date NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '400',
  `Reason` varchar(75) DEFAULT NULL,
  `Comments` text,
  `IsOrderGenerated` tinyint(1) unsigned NOT NULL,
  `IsPhysicianOfficeNotified` tinyint(1) unsigned NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_MissedVisits_PatientId` (`PatientId`),
  KEY `FK_MissedVisits_EpisodeId` (`EpisodeId`),
  CONSTRAINT `FK_MissedVisits_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MissedVisits_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.noterelations
CREATE TABLE IF NOT EXISTS `noterelations` (
  `Id` char(36) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `ParentId` char(36) DEFAULT NULL,
  `Type` char(36) DEFAULT NULL,
  `IsDeprecated` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.ordernumbers
CREATE TABLE IF NOT EXISTS `ordernumbers` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientadmissiondates
CREATE TABLE IF NOT EXISTS `patientadmissiondates` (
  `Id` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `StartOfCareDate` date DEFAULT '0001-01-01',
  `DischargedDate` date DEFAULT '0001-01-01',
  `PatientData` text,
  `ProfileData` text,
  `Status` int(11) unsigned DEFAULT NULL,
  `IsActive` tinyint(1) unsigned DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DischargeReasonId` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date DEFAULT '0001-01-01',
  `Modified` date DEFAULT '0001-01-01',
  `Reason` text,
  PRIMARY KEY (`Id`),
  KEY `PatientId` (`PatientId`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientadmissiondates2
CREATE TABLE IF NOT EXISTS `patientadmissiondates2` (
  `Id` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `AgencyId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `PatientId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `UserId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AgencyLocationId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `MedicareNumber` char(11) DEFAULT NULL,
  `MedicaidNumber` char(20) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleInitial` varchar(1) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT '1',
  `SSN` char(9) DEFAULT NULL,
  `DOB` date NOT NULL DEFAULT '0001-01-01',
  `Gender` varchar(20) DEFAULT NULL,
  `Height` float DEFAULT NULL,
  `HeightMetric` tinyint(4) DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `WeightMetric` tinyint(4) DEFAULT NULL,
  `Ethnicities` varchar(50) NOT NULL,
  `MaritalStatus` varchar(20) DEFAULT NULL,
  `AddressLine1` varchar(100) NOT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) NOT NULL,
  `AddressCounty` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) NOT NULL,
  `AddressZipCode` char(9) NOT NULL,
  `CBSA` char(5) DEFAULT NULL,
  `EvacuationAddressLine1` varchar(100) DEFAULT NULL,
  `EvacuationAddressLine2` varchar(100) DEFAULT NULL,
  `EvacuationAddressCity` varchar(100) DEFAULT NULL,
  `EvacuationAddressStateCode` char(2) DEFAULT NULL,
  `EvacuationAddressZipCode` char(5) DEFAULT NULL,
  `PhoneHome` char(10) DEFAULT NULL,
  `PhoneMobile` char(10) DEFAULT NULL,
  `EvacuationPhoneHome` char(10) DEFAULT NULL,
  `EvacuationPhoneMobile` char(10) DEFAULT NULL,
  `EmailAddress` varchar(50) DEFAULT NULL,
  `StartofCareDate` date NOT NULL DEFAULT '0001-01-01',
  `PrimaryPharmacyId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `SecondaryPharmacyId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `PharmacyName` varchar(100) DEFAULT NULL,
  `PharmacyPhone` char(10) DEFAULT NULL,
  `PrimaryInsurance` int(11) DEFAULT '-1',
  `PrimaryHealthPlanId` varchar(50) DEFAULT NULL,
  `PrimaryGroupName` varchar(50) DEFAULT NULL,
  `PrimaryGroupId` varchar(50) DEFAULT NULL,
  `PrimaryRelationship` varchar(10) DEFAULT '2',
  `SecondaryInsurance` int(11) DEFAULT '-1',
  `SecondaryHealthPlanId` varchar(50) DEFAULT NULL,
  `SecondaryGroupName` varchar(50) DEFAULT NULL,
  `SecondaryRelationship` varchar(10) DEFAULT '2',
  `TertiaryGroupId` varchar(50) DEFAULT NULL,
  `SecondaryGroupId` varchar(50) DEFAULT NULL,
  `TertiaryInsurance` int(11) DEFAULT '-1',
  `TertiaryHealthPlanId` varchar(50) DEFAULT NULL,
  `TertiaryGroupName` varchar(50) DEFAULT NULL,
  `TertiaryRelationship` varchar(10) DEFAULT '2',
  `DME` varchar(100) DEFAULT NULL,
  `OtherDME` varchar(100) DEFAULT NULL,
  `Payer` varchar(20) DEFAULT NULL,
  `ServicesRequired` varchar(100) DEFAULT NULL,
  `PaymentSource` varchar(50) DEFAULT NULL,
  `OtherPaymentSource` varchar(50) DEFAULT NULL,
  `AdmissionSource` varchar(50) DEFAULT NULL,
  `ReferrerPhysician` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `ReferralDate` date DEFAULT '0001-01-01',
  `OtherReferralSource` varchar(30) DEFAULT NULL,
  `ReferralName` varchar(50) DEFAULT NULL,
  `ReferralContact` varchar(50) DEFAULT NULL,
  `InternalReferral` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `CaseManagerId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AuditorId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Triage` tinyint(3) unsigned DEFAULT '1',
  `EvacuationZone` varchar(2) DEFAULT NULL,
  `PhotoId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `NonAdmissionDate` date DEFAULT '0001-01-01',
  `NonAdmissionReason` varchar(50) DEFAULT NULL,
  `Comments` text,
  `LastEligibilityCheck` date DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDNR` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsHospitalized` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HospitalizationId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `DischargeDate` date DEFAULT '0001-01-01',
  `DischargeReasonId` tinyint(2) NOT NULL DEFAULT '0',
  `DischargeReason` text,
  `AdmissionId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Services` int(100) unsigned NOT NULL DEFAULT '1',
  `HomeHealthStatus` int(10) unsigned NOT NULL DEFAULT '1',
  `PrivateDutyStatus` int(10) unsigned NOT NULL DEFAULT '1',
  `ServiceLocation` char(5) DEFAULT 'Q5001',
  `Longitude` float(10,7) NOT NULL,
  `Latitude` float(10,7) NOT NULL,
  `Created` date NOT NULL DEFAULT '0001-01-01',
  `Modified` date NOT NULL DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `FK_Patient_AgencyId` (`AgencyId`),
  KEY `FK_Patient_UserId` (`UserId`),
  KEY `FK_Patient_CaseManagerId` (`CaseManagerId`),
  KEY `FK_Patient_AgencyLocationId` (`AgencyLocationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientdocuments
CREATE TABLE IF NOT EXISTS `patientdocuments` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `AssetId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UploadTypeId` char(36) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Filename` varchar(200) NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `AssetId` (`AssetId`),
  CONSTRAINT `FK_PatientDocuments_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientDocuments_AssetId` FOREIGN KEY (`AssetId`) REFERENCES `assets` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientDocuments_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientemergencycontacts
CREATE TABLE IF NOT EXISTS `patientemergencycontacts` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `PrimaryPhone` char(10) NOT NULL,
  `AlternatePhone` char(10) DEFAULT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `IsPrimary` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Emergency_PatientId` (`PatientId`),
  CONSTRAINT `FK_Emergency_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientepisodes
CREATE TABLE IF NOT EXISTS `patientepisodes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `Details` text,
  `StartDate` date NOT NULL DEFAULT '0001-01-01',
  `EndDate` date NOT NULL DEFAULT '0001-01-01',
  `Schedule` mediumtext,
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsRecertCompleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDischarged` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsLinkedToDischarge` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AssessmentId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AssessmentType` varchar(100) DEFAULT '',
  `Created` date NOT NULL DEFAULT '0001-01-01',
  `Modified` date NOT NULL DEFAULT '0001-01-01',
  `StartOfCareDate` date NOT NULL DEFAULT '0001-01-01',
  `AdmissionId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Therapist` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `CaseManager` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `PrimaryPhysician` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `PrimaryInsurance` int(11) DEFAULT '0',
  `SecondaryInsurance` int(11) DEFAULT '0',
  `Comments` text,
  PRIMARY KEY (`Id`),
  KEY `FK_PatientEpisode_PatientId` (`PatientId`),
  KEY `FK_PatientEpisode_AgencyId` (`AgencyId`),
  KEY `AdmissionId` (`AdmissionId`),
  KEY `IX_PatientEpisodes_Active_Enddate` (`IsActive`,`EndDate`),
  CONSTRAINT `FK_PatientEpisode_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientEpisode_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientnonadmitreasons
CREATE TABLE IF NOT EXISTS `patientnonadmitreasons` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `NonAdmissionDate` date DEFAULT '0001-01-01',
  `NonAdmissionReason` text,
  `NonAdmissionComments` text,
  `IsCurrent` tinyint(1) DEFAULT '0',
  `Created` date DEFAULT '0001-01-01',
  `Modified` date DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `FK_PatientAdmissionDates_AgencyId` (`AgencyId`),
  KEY `FK_PatientAdmissionDates_PatientId` (`PatientId`),
  CONSTRAINT `FK_PatientAdmissionDates_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientnotes
CREATE TABLE IF NOT EXISTS `patientnotes` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `Notes` tinytext NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_PatientNotes_PatientId` (`PatientId`),
  CONSTRAINT `FK_PatientNotes_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientphysicians
CREATE TABLE IF NOT EXISTS `patientphysicians` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PatientId` char(36) NOT NULL,
  `PhysicianId` char(36) NOT NULL,
  `IsPrimary` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PatientPhysicians_PatientId` (`PatientId`),
  KEY `FK_PatientPhysicians_PhysicianId` (`PhysicianId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientprofiles
CREATE TABLE IF NOT EXISTS `patientprofiles` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `AgencyLocationId` char(36) DEFAULT NULL,
  `MedicareNumber` char(11) DEFAULT NULL,
  `MedicaidNumber` char(20) DEFAULT NULL,
  `PatientIdNumber` char(50) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleInitial` varchar(1) DEFAULT NULL,
  `StartofCareDate` date NOT NULL,
  `PrimaryInsurance` int(11) DEFAULT '-1',
  `PrimaryHealthPlanId` varchar(50) DEFAULT NULL,
  `PrimaryGroupName` varchar(50) DEFAULT NULL,
  `PrimaryGroupId` varchar(50) DEFAULT NULL,
  `PrimaryRelationship` varchar(10) DEFAULT '2',
  `SecondaryInsurance` int(11) DEFAULT '-1',
  `SecondaryHealthPlanId` varchar(50) DEFAULT NULL,
  `SecondaryGroupName` varchar(50) DEFAULT NULL,
  `SecondaryGroupId` varchar(50) DEFAULT NULL,
  `SecondaryRelationship` varchar(10) DEFAULT '2',
  `TertiaryInsurance` int(11) DEFAULT '-1',
  `TertiaryHealthPlanId` varchar(50) DEFAULT NULL,
  `TertiaryGroupName` varchar(50) DEFAULT NULL,
  `TertiaryGroupId` varchar(50) DEFAULT NULL,
  `TertiaryRelationship` varchar(10) DEFAULT '2',
  `Payer` varchar(20) DEFAULT NULL,
  `ServicesRequired` varchar(100) DEFAULT NULL,
  `PaymentSource` varchar(50) DEFAULT NULL,
  `OtherPaymentSource` varchar(50) DEFAULT NULL,
  `AdmissionSource` varchar(50) DEFAULT NULL,
  `ReferrerPhysician` char(36) DEFAULT NULL,
  `ReferralDate` date DEFAULT '0001-01-01',
  `OtherReferralSource` varchar(30) DEFAULT NULL,
  `ReferralName` varchar(50) DEFAULT NULL,
  `ReferralContact` varchar(50) DEFAULT NULL,
  `InternalReferral` char(36) DEFAULT NULL,
  `CaseManagerId` char(36) DEFAULT NULL,
  `AuditorId` char(36) DEFAULT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `NonAdmissionDate` date DEFAULT NULL,
  `NonAdmissionReason` varchar(50) DEFAULT NULL,
  `Comments` text,
  `LastEligibilityCheck` date DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DischargeDate` date DEFAULT '0001-01-01',
  `DischargeReasonId` tinyint(2) NOT NULL DEFAULT '0',
  `DischargeReason` text,
  `AdmissionId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `OldStatus` int(11) DEFAULT '0',
  `Created` date DEFAULT '0001-01-01',
  `Modified` date DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `FK_Patient_AgencyId` (`AgencyId`),
  KEY `FK_Patient_UserId` (`UserId`),
  KEY `FK_Patient_CaseManagerId` (`CaseManagerId`),
  KEY `FK_Patient_AgencyLocationId` (`AgencyLocationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patients
CREATE TABLE IF NOT EXISTS `patients` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `AgencyLocationId` char(36) DEFAULT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `MedicareNumber` char(11) DEFAULT NULL,
  `MedicaidNumber` char(20) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleInitial` varchar(1) DEFAULT NULL,
  `SSN` char(9) DEFAULT NULL,
  `DOB` date NOT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `Height` float DEFAULT NULL,
  `HeightMetric` tinyint(4) DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `WeightMetric` tinyint(4) DEFAULT NULL,
  `Ethnicities` varchar(50) NOT NULL,
  `MaritalStatus` varchar(20) DEFAULT NULL,
  `AddressLine1` varchar(100) NOT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) NOT NULL,
  `AddressCounty` varchar(100) DEFAULT NULL,
  `AddressStateCode` char(2) NOT NULL,
  `AddressZipCode` char(9) NOT NULL,
  `CBSA` char(5) DEFAULT NULL,
  `EvacuationAddressLine1` varchar(100) DEFAULT NULL,
  `EvacuationAddressLine2` varchar(100) DEFAULT NULL,
  `EvacuationAddressCity` varchar(100) DEFAULT NULL,
  `EvacuationAddressStateCode` char(2) DEFAULT NULL,
  `EvacuationAddressZipCode` char(5) DEFAULT NULL,
  `PhoneHome` char(10) DEFAULT NULL,
  `PhoneMobile` char(10) DEFAULT NULL,
  `EvacuationPhoneHome` char(10) DEFAULT NULL,
  `EvacuationPhoneMobile` char(10) DEFAULT NULL,
  `EmailAddress` varchar(50) DEFAULT NULL,
  `StartofCareDate` date NOT NULL,
  `PrimaryPharmacyId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `SecondaryPharmacyId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `PharmacyName` varchar(100) DEFAULT NULL,
  `PharmacyPhone` char(10) DEFAULT NULL,
  `PrimaryInsurance` int(11) DEFAULT '-1',
  `PrimaryHealthPlanId` varchar(50) DEFAULT NULL,
  `PrimaryGroupName` varchar(50) DEFAULT NULL,
  `PrimaryGroupId` varchar(50) DEFAULT NULL,
  `PrimaryRelationship` varchar(10) DEFAULT '2',
  `SecondaryInsurance` int(11) DEFAULT '-1',
  `SecondaryHealthPlanId` varchar(50) DEFAULT NULL,
  `SecondaryGroupName` varchar(50) DEFAULT NULL,
  `SecondaryGroupId` varchar(50) DEFAULT NULL,
  `SecondaryRelationship` varchar(10) DEFAULT '2',
  `TertiaryInsurance` int(11) DEFAULT '-1',
  `TertiaryHealthPlanId` varchar(50) DEFAULT NULL,
  `TertiaryGroupName` varchar(50) DEFAULT NULL,
  `TertiaryGroupId` varchar(50) DEFAULT NULL,
  `TertiaryRelationship` varchar(10) DEFAULT '2',
  `DME` varchar(100) DEFAULT NULL,
  `OtherDME` varchar(100) DEFAULT NULL,
  `Payer` varchar(20) DEFAULT NULL,
  `ServicesRequired` varchar(100) DEFAULT NULL,
  `PaymentSource` varchar(50) DEFAULT NULL,
  `OtherPaymentSource` varchar(50) DEFAULT NULL,
  `AdmissionSource` varchar(50) DEFAULT NULL,
  `ReferrerPhysician` char(36) DEFAULT NULL,
  `ReferralDate` date DEFAULT '0001-01-01',
  `OtherReferralSource` varchar(30) DEFAULT NULL,
  `ReferralName` varchar(50) DEFAULT NULL,
  `ReferralContact` varchar(50) DEFAULT NULL,
  `InternalReferral` char(36) DEFAULT NULL,
  `CaseManagerId` char(36) DEFAULT NULL,
  `AuditorId` char(36) DEFAULT NULL,
  `Triage` tinyint(3) unsigned DEFAULT '1',
  `EvacuationZone` varchar(2) DEFAULT NULL,
  `PhotoId` char(36) DEFAULT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `NonAdmissionDate` date DEFAULT NULL,
  `NonAdmissionReason` varchar(50) DEFAULT NULL,
  `Comments` text,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `LastEligibilityCheck` date DEFAULT '0001-01-01',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDNR` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsHospitalized` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HospitalizationId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `DischargeDate` date DEFAULT '0001-01-01',
  `DischargeReasonId` tinyint(2) NOT NULL DEFAULT '0',
  `DischargeReason` text,
  `AdmissionId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Services` int(100) unsigned NOT NULL DEFAULT '1',
  `HomeHealthStatus` int(10) unsigned NOT NULL DEFAULT '1',
  `PrivateDutyStatus` int(10) unsigned NOT NULL DEFAULT '1',
  `ServiceLocation` char(5) DEFAULT 'Q5001',
  `Latitude` float(10,7) NOT NULL,
  `Longitude` float(10,7) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Patient_AgencyId` (`AgencyId`),
  KEY `FK_Patient_UserId` (`UserId`),
  KEY `FK_Patient_CaseManagerId` (`CaseManagerId`),
  KEY `FK_Patient_AgencyLocationId` (`AgencyLocationId`),
  CONSTRAINT `FK_Patient_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Patient_AgencyLocationId` FOREIGN KEY (`AgencyLocationId`) REFERENCES `agencylocations` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientteams
CREATE TABLE IF NOT EXISTS `patientteams` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `TeamId` char(36) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_patientteams_patients` (`PatientId`),
  KEY `FK_patientteams_agencyteams` (`TeamId`),
  CONSTRAINT `FK_patientteams_agencyteams` FOREIGN KEY (`teamId`) REFERENCES `agencyteams` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_patientteams_patients` FOREIGN KEY (`patientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientusers
CREATE TABLE IF NOT EXISTS `patientusers` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientvisitlogs
CREATE TABLE IF NOT EXISTS `patientvisitlogs` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `TaskId` char(36) NOT NULL,
  `IsPatientUnableToSign` tinyint(1) NOT NULL DEFAULT '0',
  `PatientUnableToSignReason` varchar(500) DEFAULT NULL,
  `PatientSignatureAssetId` char(36) DEFAULT NULL,
  `VerifiedDateTime` datetime NOT NULL,
  `VerifiedLatitude` float(10,7) DEFAULT NULL,
  `VerifiedLongitude` float(10,7) DEFAULT NULL,
  `VerifiedAddress` varchar(250) DEFAULT NULL,
  `PatientAddress` varchar(250) DEFAULT NULL,
  `Created` datetime NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PatientVisitLogs_AgencyId` (`AgencyId`),
  KEY `FK_PatientVisitLogs_PatientId` (`PatientId`),
  KEY `FK_PatientVisitLogs_UserId` (`UserId`),
  KEY `FK_PatientVisitLogs_EpisodeId` (`EpisodeId`),
  CONSTRAINT `FK_PatientVisitLogs_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitLogs_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitLogs_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitLogs_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.patientvisitnotes
CREATE TABLE IF NOT EXISTS `patientvisitnotes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `NoteType` varchar(50) DEFAULT NULL,
  `Note` mediumtext NOT NULL,
  `Status` int(10) unsigned DEFAULT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsBillable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WoundNote` mediumtext,
  `IsWoundCare` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSupplyExist` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Supply` text,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `SentDate` date DEFAULT NULL,
  `OrderNumber` bigint(20) unsigned DEFAULT '0',
  `PhysicianData` text,
  `Version` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `FK_PatientVisitNotes_AgencyId` (`AgencyId`),
  KEY `FK_PatientVisitNotes_PatientId` (`PatientId`),
  KEY `FK_PatientVisitNotes_UserId` (`UserId`),
  KEY `FK_PatientVisitNotes_EpisodeId` (`EpisodeId`),
  KEY `PhysicianId` (`PhysicianId`),
  CONSTRAINT `FK_PatientVisitNotes_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitNotes_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitNotes_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PatientVisitNotes_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.payperiods
CREATE TABLE IF NOT EXISTS `payperiods` (
  `Id` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) NOT NULL,
  `PayDate` datetime DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `IsPaid` tinyint(4) DEFAULT NULL,
  `Comments` text,
  `IsDeprecated` tinyint(4) DEFAULT '0',
  `Created` datetime DEFAULT NULL,
  `Modified` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.payperiodtasks
CREATE TABLE IF NOT EXISTS `payperiodtasks` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `PayPeriodId` char(36) NOT NULL,
  `TaskId` char(36) NOT NULL,
  `IsDeprecated` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_PayPeriodId` (`PayPeriodId`),
  CONSTRAINT `FK_PayPeriodId` FOREIGN KEY (`PayPeriodId`) REFERENCES `payperiods` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.pdpatientvisitnotes
CREATE TABLE IF NOT EXISTS `pdpatientvisitnotes` (
  `AgencyId` char(36) NOT NULL,
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `NoteType` varchar(50) DEFAULT NULL,
  `Status` int(10) unsigned DEFAULT NULL,
  `IsBillable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsWoundCare` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSupplyExist` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `ReceivedDate` date DEFAULT NULL,
  `SentDate` date DEFAULT NULL,
  `OrderNumber` bigint(20) unsigned DEFAULT '0',
  `PhysicianData` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Version` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `FK_PatientVisitNotes_AgencyId` (`AgencyId`),
  KEY `FK_PatientVisitNotes_PatientId` (`PatientId`),
  KEY `FK_PatientVisitNotes_UserId` (`UserId`),
  KEY `PhysicianId` (`PhysicianId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.pdscheduleevents
CREATE TABLE IF NOT EXISTS `pdscheduleevents` (
  `EventId` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `DisciplineTask` int(5) DEFAULT NULL,
  `Status` int(5) DEFAULT NULL,
  `Discipline` varchar(20) DEFAULT NULL,
  `IsBillable` tinyint(1) NOT NULL,
  `IsMissedVisit` tinyint(1) DEFAULT NULL,
  `VTimeIn` datetime DEFAULT NULL,
  `STimeIn` datetime DEFAULT NULL,
  `VTimeOut` datetime DEFAULT NULL,
  `STimeOut` datetime DEFAULT NULL,
  `Surcharge` varchar(10) DEFAULT NULL,
  `AssociatedMileage` varchar(10) DEFAULT NULL,
  `ReturnReason` text,
  `Comments` text,
  `IsDeprecated` tinyint(1) DEFAULT NULL,
  `IsOrderForNextEpisode` tinyint(1) DEFAULT NULL,
  `IsVisitPaid` tinyint(1) DEFAULT NULL,
  `InPrintQueue` tinyint(1) DEFAULT NULL,
  `Asset` text,
  `Version` int(11) DEFAULT '1',
  PRIMARY KEY (`EventId`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `UserId` (`UserId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.physicianorders
CREATE TABLE IF NOT EXISTS `physicianorders` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `UserId` char(36) NOT NULL,
  `Summary` varchar(100) DEFAULT NULL,
  `Text` text NOT NULL,
  `OrderDate` date NOT NULL,
  `Created` date NOT NULL,
  `Modified` date DEFAULT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `SentDate` date NOT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date NOT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Status` int(10) unsigned NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsOrderForNextEpisode` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsOrderReadAndVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PhysicianData` text,
  PRIMARY KEY (`Id`),
  KEY `FK_PhysicianOrder_PatientId` (`PatientId`),
  KEY `FK_PhysicianOrder_PhysicianId` (`PhysicianId`),
  KEY `FK_PhysicianOrder_UserId` (`UserId`),
  KEY `FK_PhysicianOrder_EpisodeId` (`EpisodeId`),
  CONSTRAINT `FK_PhysicianOrder_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_PhysicianOrder_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.planofcares
CREATE TABLE IF NOT EXISTS `planofcares` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL DEFAULT '',
  `PatientId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `UserId` char(36) NOT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `Data` text,
  `Status` int(11) NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `SentDate` date DEFAULT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AssessmentId` char(36) DEFAULT NULL,
  `AssessmentType` varchar(50) DEFAULT NULL,
  `PhysicianData` text,
  `IsNonOasis` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsStandAlone` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisciplineTask` int(11) NOT NULL,
  PRIMARY KEY (`Id`,`EpisodeId`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.planofcarestandalones
CREATE TABLE IF NOT EXISTS `planofcarestandalones` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `Data` text,
  `Status` int(11) NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `SentDate` date DEFAULT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PhysicianData` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.raps
CREATE TABLE IF NOT EXISTS `raps` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `IsOasisComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FirstBillableVisitDate` date DEFAULT NULL,
  `IsGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date NOT NULL,
  `Created` date DEFAULT NULL,
  `Remark` text,
  `PaymentDate` date NOT NULL DEFAULT '0001-01-01',
  `MedicareNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) NOT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(9) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `AreOrdersComplete` tinyint(1) unsigned DEFAULT NULL,
  `PrimaryInsuranceId` int(4) unsigned NOT NULL,
  `Status` int(4) unsigned NOT NULL,
  `Type` int(4) unsigned DEFAULT NULL,
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) DEFAULT '0',
  `ClaimDate` datetime DEFAULT '0001-01-01 00:00:00',
  `Payment` double DEFAULT '0',
  `ProspectivePay` double DEFAULT '0',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `DischargeDate` date DEFAULT '0001-01-01',
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `GroupName` varchar(50) DEFAULT NULL,
  `GroupId` varchar(50) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `Authorization` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `AuthorizationNumber2` varchar(50) DEFAULT NULL,
  `AuthorizationNumber3` varchar(50) DEFAULT NULL,
  `ConditionCodes` text,
  `UB4PatientStatus` varchar(5) DEFAULT NULL,
  `CBSA` varchar(6) DEFAULT NULL,
  `Ub04Locator81cca` text,
  `Ub04Locator39` text,
  `Insurance` text,
  `Remittance` text,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.rapsnapshots
CREATE TABLE IF NOT EXISTS `rapsnapshots` (
  `MainId` char(46) NOT NULL,
  `Id` char(36) NOT NULL,
  `BatchId` bigint(20) NOT NULL DEFAULT '0',
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `IsOasisComplete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FirstBillableVisitDate` date DEFAULT NULL,
  `IsGenerated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Remark` text,
  `PaymentDate` date DEFAULT NULL,
  `MedicareNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) NOT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(9) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `AreOrdersComplete` tinyint(1) unsigned DEFAULT NULL,
  `PrimaryInsuranceId` tinyint(4) unsigned NOT NULL,
  `Status` int(4) unsigned NOT NULL,
  `Type` int(4) unsigned DEFAULT NULL,
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) DEFAULT '0',
  `ClaimDate` datetime DEFAULT '0001-01-01 00:00:00',
  `Payment` double DEFAULT '0',
  `ProspectivePay` double DEFAULT '0',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `DischargeDate` date DEFAULT '0001-01-01',
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `Reason` text,
  PRIMARY KEY (`MainId`),
  KEY `MainId` (`MainId`),
  KEY `IX_RapSnapShots_AgencyId_Id` (`AgencyId`,`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.referralemergencycontacts
CREATE TABLE IF NOT EXISTS `referralemergencycontacts` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `ReferralId` char(36) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `PrimaryPhone` char(10) NOT NULL,
  `AlternatePhone` char(10) DEFAULT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `IsPrimary` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Emergency_ReferralId` (`ReferralId`),
  CONSTRAINT `FK_Emergency_ReferralId` FOREIGN KEY (`ReferralId`) REFERENCES `referrals` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.referrals
CREATE TABLE IF NOT EXISTS `referrals` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `AdmissionSource` tinyint(4) unsigned NOT NULL,
  `ReferrerPhysician` char(36) DEFAULT NULL,
  `ReferralDate` date DEFAULT NULL,
  `OtherReferralSource` varchar(50) DEFAULT NULL,
  `ReferralName` varchar(50) DEFAULT NULL,
  `ReferralContact` varchar(50) DEFAULT NULL,
  `InternalReferral` char(36) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleInitial` varchar(1) DEFAULT NULL,
  `MedicareNumber` char(12) DEFAULT NULL,
  `MedicaidNumber` char(20) DEFAULT NULL,
  `SSN` char(9) DEFAULT NULL,
  `DOB` date NOT NULL,
  `Gender` varchar(20) NOT NULL,
  `Height` float DEFAULT NULL,
  `HeightMetric` tinyint(4) DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `WeightMetric` tinyint(4) DEFAULT NULL,
  `MaritalStatus` varchar(20) DEFAULT NULL,
  `PhoneHome` char(10) NOT NULL,
  `EmailAddress` varchar(100) DEFAULT NULL,
  `AddressLine1` varchar(100) NOT NULL,
  `AddressLine2` varchar(100) DEFAULT NULL,
  `AddressCity` varchar(100) NOT NULL,
  `AddressStateCode` char(2) NOT NULL,
  `AddressZipCode` char(5) NOT NULL,
  `ServicesRequired` varchar(100) DEFAULT NULL,
  `PrimaryInsurance` int(11) DEFAULT '-1',
  `PrimaryHealthPlanId` varchar(50) DEFAULT NULL,
  `PrimaryGroupName` varchar(50) DEFAULT NULL,
  `PrimaryGroupId` varchar(50) DEFAULT NULL,
  `PrimaryRelationship` varchar(50) DEFAULT NULL,
  `SecondaryInsurance` int(11) DEFAULT '-1',
  `SecondaryHealthPlanId` varchar(50) DEFAULT NULL,
  `SecondaryGroupName` varchar(50) DEFAULT NULL,
  `SecondaryGroupId` varchar(50) DEFAULT NULL,
  `SecondaryRelationship` varchar(50) DEFAULT NULL,
  `TertiaryInsurance` int(11) DEFAULT '-1',
  `TertiaryHealthPlanId` varchar(50) DEFAULT NULL,
  `TertiaryGroupName` varchar(50) DEFAULT NULL,
  `TertiaryGroupId` varchar(50) DEFAULT NULL,
  `TertiaryRelationship` varchar(50) DEFAULT NULL,
  `DME` varchar(50) DEFAULT NULL,
  `OtherDME` varchar(100) DEFAULT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `NonAdmissionDate` date DEFAULT NULL,
  `NonAdmissionReason` varchar(200) DEFAULT NULL,
  `VerificationDate` date DEFAULT NULL,
  `CreatedById` char(36) DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Modified` date DEFAULT NULL,
  `Physicians` text,
  `Comments` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDNR` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Services` int(100) unsigned NOT NULL DEFAULT '1',
  `HomeHealthStatus` int(100) unsigned NOT NULL DEFAULT '1',
  `PrivateDutyStatus` int(100) unsigned NOT NULL DEFAULT '1',
  `DeprecatedServices` int(100) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Referral_AgencyId` (`AgencyId`),
  KEY `FK_Referral_UserId` (`UserId`),
  CONSTRAINT `FK_Referral_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.remitqueues
CREATE TABLE IF NOT EXISTS `remitqueues` (
  `Id` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) DEFAULT NULL,
  `Data` longtext,
  `Error` text,
  `IsUpload` tinyint(1) DEFAULT '0',
  `Priority` tinyint(3) unsigned DEFAULT NULL,
  `Status` enum('queued','processing','completed','unsucessful') DEFAULT NULL,
  `Created` date DEFAULT '0001-01-01',
  `Modified` date DEFAULT '0001-01-01',
  `UpStat` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.remittanceClaims
CREATE TABLE IF NOT EXISTS `remittanceClaims` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `RemittanceId` char(36) NOT NULL,
  `RapFinal` enum('Rap','Final') NOT NULL,
  `PayerClaimControlNumber` char(30) NOT NULL,
  `Data` longtext NOT NULL,
  `Description` text NOT NULL,
  `Value` double NOT NULL,
  `InterchangeDate` date NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.remittances
CREATE TABLE IF NOT EXISTS `remittances` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Data` longtext NOT NULL,
  `RemitId` char(30) NOT NULL,
  `TotalClaims` int(10) unsigned NOT NULL,
  `CoveredAmount` double NOT NULL,
  `ChargedAmount` double NOT NULL,
  `PaymentAmount` double NOT NULL,
  `RemittanceDate` date DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `IsDeprecated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.reportdescriptions
CREATE TABLE IF NOT EXISTS `reportdescriptions` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) DEFAULT NULL,
  `Category` varchar(50) DEFAULT NULL,
  `CategoryId` int(20) DEFAULT '0',
  `Link` varchar(100) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `Parameters` varchar(500) DEFAULT NULL,
  `InformationInclude` varchar(500) DEFAULT NULL,
  `Comments` varchar(500) DEFAULT NULL,
  `IsOnclick` tinyint(1) DEFAULT '0',
  `IsProductionReady` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.reports
CREATE TABLE IF NOT EXISTS `reports` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `AssetId` char(36) DEFAULT NULL,
  `Type` varchar(150) NOT NULL,
  `Format` varchar(100) DEFAULT NULL,
  `Status` varchar(100) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Created` datetime NOT NULL,
  `Completed` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `UserId` (`UserId`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.returncomments
CREATE TABLE IF NOT EXISTS `returncomments` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `EventId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `Comments` text,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `EventId` (`EventId`),
  KEY `IsDeprecated` (`IsDeprecated`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.returncomments_staging
CREATE TABLE IF NOT EXISTS `returncomments_staging` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `EventId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `Comments` text,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  KEY `AgencyId` (`AgencyId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `EventId` (`EventId`),
  KEY `IsDeprecated` (`IsDeprecated`),
  KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.scheduleevents_old
CREATE TABLE IF NOT EXISTS `scheduleevents_old` (
  `Id` char(36) NOT NULL DEFAULT '',
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) NOT NULL DEFAULT '',
  `UserId` char(36) DEFAULT NULL,
  `DisciplineTask` int(5) DEFAULT NULL,
  `UserName` varchar(100) DEFAULT NULL,
  `EventDate` date DEFAULT '0001-01-01',
  `VisitDate` date DEFAULT '0001-01-01',
  `Status` int(5) DEFAULT NULL,
  `Discipline` varchar(20) DEFAULT NULL,
  `IsBillable` tinyint(1) DEFAULT NULL,
  `IsPayable` tinyint(1) DEFAULT NULL,
  `IsVisitVerified` tinyint(1) DEFAULT NULL,
  `IsMissedVisit` tinyint(1) DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `TravelTimeIn` varchar(10) DEFAULT NULL,
  `TravelTimeOut` varchar(10) DEFAULT NULL,
  `Surcharge` varchar(10) DEFAULT NULL,
  `AssociatedMileage` varchar(10) DEFAULT NULL,
  `ReturnReason` text,
  `MissedVisitFormReturnReason` text,
  `Comments` text,
  `PersonalComments` text,
  `IsDeprecated` tinyint(1) DEFAULT NULL,
  `IsOrderForNextEpisode` tinyint(1) DEFAULT NULL,
  `IsVisitPaid` tinyint(1) DEFAULT NULL,
  `Version` int(11) DEFAULT '1',
  `SendAsOrder` char(3) DEFAULT '0',
  `InPrintQueue` tinyint(1) DEFAULT NULL,
  `ServiceLocation` char(6) DEFAULT NULL,
  `Asset` text,
  `StartDate` date DEFAULT '0001-01-01',
  `EndDate` date DEFAULT '0001-01-01',
  `ClaimStartDate` date DEFAULT '0001-01-01',
  `ClaimEndDate` date DEFAULT '0001-01-01',
  `BeforeMissedVisitStatus` int(5) DEFAULT '0',
  `IsQANote` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.scheduletasks
CREATE TABLE IF NOT EXISTS `scheduletasks` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `DisciplineTask` int(5) DEFAULT '0',
  `UserName` varchar(100) DEFAULT NULL,
  `EventDate` date DEFAULT '0001-01-01',
  `Status` int(5) DEFAULT '0',
  `Discipline` int(11) DEFAULT '0',
  `IsBillable` tinyint(1) DEFAULT '0',
  `IsPayable` tinyint(1) DEFAULT '0',
  `IsTimeRangeExist` tinyint(1) DEFAULT '1',
  `IsVisitVerified` tinyint(1) DEFAULT '0',
  `IsMissedVisit` tinyint(1) DEFAULT '0',
  `VisitDateTimeIn` datetime DEFAULT '0001-01-01 00:00:00',
  `VisitDateTimeOut` datetime DEFAULT '0001-01-01 00:00:00',
  `TravelTimeIn` datetime DEFAULT '0001-01-01 00:00:00',
  `TravelTimeOut` datetime DEFAULT '0001-01-01 00:00:00',
  `Surcharge` varchar(10) DEFAULT NULL,
  `AssociatedMileage` varchar(10) DEFAULT NULL,
  `ReturnReason` text,
  `MissedVisitFormReturnReason` text,
  `Comments` text,
  `PersonalComments` text,
  `IsDeprecated` tinyint(1) DEFAULT '0',
  `IsOrderForNextEpisode` tinyint(1) DEFAULT '0',
  `IsVisitPaid` tinyint(1) DEFAULT '0',
  `Version` int(11) DEFAULT '1',
  `SendAsOrder` tinyint(1) DEFAULT '0',
  `IsOrder` tinyint(1) DEFAULT '0',
  `InPrintQueue` tinyint(1) DEFAULT '0',
  `ServiceLocation` char(6) DEFAULT NULL,
  `StartDate` date DEFAULT '0001-01-01',
  `EndDate` date DEFAULT '0001-01-01',
  `IsQANote` tinyint(1) DEFAULT '0',
  `Asset` text,
  `IsActive` tinyint(4) DEFAULT '1',
  `IsDischarged` tinyint(4) DEFAULT '1',
  `PrimaryInsurance` int(3) DEFAULT '0',
  `SecondaryInsurance` int(3) DEFAULT '0',
  `CommentCount` int(3) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.scheduletasks_staging
CREATE TABLE IF NOT EXISTS `scheduletasks_staging` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) DEFAULT NULL,
  `EpisodeId` char(36) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  `DisciplineTask` int(5) DEFAULT NULL,
  `UserName` varchar(100) DEFAULT NULL,
  `EventDate` date DEFAULT '0001-01-01',
  `VisitDate` date DEFAULT '0001-01-01',
  `Status` int(5) DEFAULT NULL,
  `DisciplineText` varchar(20) DEFAULT NULL,
  `Discipline` int(11) DEFAULT '0',
  `IsBillable` tinyint(1) DEFAULT NULL,
  `IsPayable` tinyint(1) DEFAULT NULL,
  `IsVisitVerified` tinyint(1) DEFAULT NULL,
  `IsMissedVisit` tinyint(1) DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `VisitDateTimeIn` datetime DEFAULT NULL,
  `VisitDateTimeOut` datetime DEFAULT NULL,
  `TravelTimeIn` varchar(10) DEFAULT NULL,
  `TravelTimeOut` varchar(10) DEFAULT NULL,
  `Surcharge` varchar(10) DEFAULT NULL,
  `AssociatedMileage` varchar(10) DEFAULT NULL,
  `ReturnReason` text,
  `MissedVisitFormReturnReason` text,
  `Comments` text,
  `PersonalComments` text,
  `IsDeprecated` tinyint(1) DEFAULT NULL,
  `IsOrderForNextEpisode` tinyint(1) DEFAULT NULL,
  `IsVisitPaid` tinyint(1) DEFAULT NULL,
  `Version` int(11) DEFAULT '1',
  `SendAsOrder` tinyint(1) DEFAULT '0',
  `InPrintQueue` tinyint(1) DEFAULT NULL,
  `ServiceLocation` char(6) DEFAULT NULL,
  `StartDate` date DEFAULT '0001-01-01',
  `EndDate` date DEFAULT '0001-01-01',
  `ClaimStartDate` date DEFAULT '0001-01-01',
  `ClaimEndDate` date DEFAULT '0001-01-01',
  `BeforeMissedVisitStatus` int(5) DEFAULT '0',
  `IsQANote` tinyint(1) DEFAULT NULL,
  `Asset` text,
  `IsActive` tinyint(4) DEFAULT '1',
  `IsDischarged` tinyint(4) DEFAULT '1',
  `PrimaryInsurance` int(11) DEFAULT '0',
  `SecondaryInsurance` int(11) DEFAULT '0',
  KEY `AgencyId` (`AgencyId`),
  KEY `PatientId` (`PatientId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `UserId` (`UserId`),
  KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.secondaryclaims
CREATE TABLE IF NOT EXISTS `secondaryclaims` (
  `Id` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PrimaryClaimId` char(36) NOT NULL,
  `PatientIdNumber` varchar(50) DEFAULT NULL,
  `RemitId` varchar(50) DEFAULT NULL,
  `RemitDate` date DEFAULT NULL,
  `TotalAdjustmentAmount` double(10,0) DEFAULT NULL,
  `IsuranceIdNumber` varchar(50) DEFAULT NULL,
  `EpisodeStartDate` date DEFAULT NULL,
  `EpisodeEndDate` date DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `IsFirstBillableVisit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FirstBillableVisitDate` date DEFAULT NULL,
  `Remark` text,
  `MedicareNumber` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `AddressLine1` varchar(50) DEFAULT NULL,
  `AddressLine2` varchar(50) DEFAULT NULL,
  `AddressCity` varchar(50) DEFAULT NULL,
  `AddressStateCode` char(2) DEFAULT NULL,
  `AddressZipCode` char(5) DEFAULT NULL,
  `StartofCareDate` date DEFAULT NULL,
  `PhysicianNPI` char(10) DEFAULT NULL,
  `PhysicianFirstName` varchar(50) DEFAULT NULL,
  `PhysicianLastName` varchar(50) DEFAULT NULL,
  `DiagnosisCode` text,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `VerifiedVisits` mediumtext,
  `Supply` mediumtext,
  `SupplyTotal` double(10,0) DEFAULT NULL,
  `ProspectivePay` double unsigned NOT NULL DEFAULT '0',
  `ClaimAmount` double unsigned NOT NULL DEFAULT '0',
  `Payment` double NOT NULL DEFAULT '0',
  `SecondaryInsuranceId` int(4) unsigned NOT NULL,
  `IsInfoVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsInsuranceVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsVisitVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsRemittanceVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSupplyVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Status` int(4) unsigned NOT NULL DEFAULT '0',
  `Type` int(4) unsigned DEFAULT '0',
  `AdmissionSource` varchar(4) DEFAULT NULL,
  `PatientStatus` int(4) unsigned DEFAULT '0',
  `ClaimDate` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `AssessmentType` varchar(50) DEFAULT NULL,
  `Comment` text,
  `DischargeDate` date DEFAULT '0001-01-01',
  `HealthPlanId` varchar(50) DEFAULT NULL,
  `GroupName` varchar(50) DEFAULT NULL,
  `GroupId` varchar(50) DEFAULT NULL,
  `Relationship` varchar(50) DEFAULT NULL,
  `Authorization` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `AuthorizationNumber` varchar(50) DEFAULT NULL,
  `AuthorizationNumber2` varchar(50) DEFAULT NULL,
  `AuthorizationNumber3` varchar(50) DEFAULT NULL,
  `ConditionCodes` text NOT NULL,
  `SupplyCode` varchar(10) DEFAULT '0272',
  `UB4PatientStatus` varchar(5) DEFAULT NULL,
  `CBSA` varchar(6) DEFAULT NULL,
  `Ub04Locator31` text,
  `Ub04Locator32` text,
  `Ub04Locator33` text,
  `Ub04Locator34` text,
  `Ub04Locator39` text,
  `Ub04Locator81cca` text,
  `HCFALocators` text,
  `Insurance` mediumtext,
  `Remittance` text,
  `Adjustments` text,
  `InvoiceType` int(4) unsigned DEFAULT '0',
  `IsDeprecated` tinyint(1) NOT NULL DEFAULT '0',
  `IsHomeHealthServiceIncluded` tinyint(1) NOT NULL DEFAULT '0',
  `IsGenerated` tinyint(1) NOT NULL DEFAULT '0',
  `Created` date NOT NULL DEFAULT '0001-01-01',
  `Modified` date NOT NULL DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `PatientId` (`PatientId`),
  KEY `EpisodeId` (`EpisodeId`),
  KEY `AgencyId` (`AgencyId`),
  KEY `PrimaryClaimId` (`PrimaryClaimId`),
  CONSTRAINT `FK_SecondaryClaims_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_SecondaryClaims_EpisodeId` FOREIGN KEY (`EpisodeId`) REFERENCES `patientepisodes` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_SecondaryClaims_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.shpdatabatches
CREATE TABLE IF NOT EXISTS `shpdatabatches` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `Status` tinyint(4) unsigned NOT NULL,
  `SubmissionFormat` text,
  `LastModified` date NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ShpDataBatches_AgencyId` (`AgencyId`),
  KEY `FK_ShpDataBatches_PatientId` (`PatientId`),
  CONSTRAINT `FK_ShpDataBatches_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ShpDataBatches_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.shpdatalogins
CREATE TABLE IF NOT EXISTS `shpdatalogins` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Username` varchar(100) DEFAULT NULL,
  `PasswordHash` char(100) DEFAULT NULL,
  `ProviderNo` varchar(10) NOT NULL,
  `ActivationCode` varchar(10) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ShpDataLogins_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_ShpDataLogins_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.systemmessages
CREATE TABLE IF NOT EXISTS `systemmessages` (
  `Id` char(36) NOT NULL,
  `Subject` varchar(150) NOT NULL,
  `Body` text NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.taskaudits
CREATE TABLE IF NOT EXISTS `taskaudits` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `EntityId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `DisciplineTaskId` tinyint(3) unsigned NOT NULL,
  `Log` text NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_TaskAudit_UserId` (`EntityId`),
  KEY `FK_TaskAudit_AgencyId` (`AgencyId`),
  KEY `FK_TaskAudit_PatientId` (`PatientId`),
  CONSTRAINT `FK_TaskAudit_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TaskAudit_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.uploadtypes
CREATE TABLE IF NOT EXISTS `uploadtypes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Type` varchar(200) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  CONSTRAINT `FK_UploadTypes_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.userlocations
CREATE TABLE IF NOT EXISTS `userlocations` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `AgencyLocationId` char(36) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_UserLocations` (`UserId`),
  KEY `FK_UserLocations_AgencyId` (`AgencyId`),
  KEY `FK_UserLocations_AgencyLocationId` (`AgencyLocationId`),
  CONSTRAINT `FK_UserLocations_AgencyLocationId` FOREIGN KEY (`AgencyLocationId`) REFERENCES `agencylocations` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_UserLocations_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Intersection table that maps users to agency branches.';

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.usermessages
CREATE TABLE IF NOT EXISTS `usermessages` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `MessageId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `MessageType` int(1) NOT NULL,
  `FolderId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsRead` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ThreadId` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_SystemMessage` (`MessageId`),
  KEY `FK_User` (`UserId`),
  KEY `FK_UserMessages_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_UserMessages_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Intersection table that maps system messages to a user.';

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.usernonvisitrates
CREATE TABLE IF NOT EXISTS `usernonvisitrates` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `Rate` mediumtext NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(4) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK1_AgencyId` (`AgencyId`),
  KEY `FK2_UserId` (`UserId`),
  CONSTRAINT `FK1_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`),
  CONSTRAINT `FK2_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.usernonvisittasks
CREATE TABLE IF NOT EXISTS `usernonvisittasks` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `TaskId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `TaskDate` date DEFAULT NULL,
  `PaidStatus` tinyint(4) DEFAULT NULL,
  `PaidDate` date DEFAULT NULL,
  `TimeIn` datetime DEFAULT NULL,
  `TimeOut` datetime DEFAULT NULL,
  `Comments` varchar(2000) DEFAULT NULL,
  `IsDeprecated` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `TaskId` (`TaskId`),
  KEY `FK_UserId` (`UserId`),
  KEY `AgencyId` (`AgencyId`),
  CONSTRAINT `FK_TaskId` FOREIGN KEY (`TaskId`) REFERENCES `agencynonvisits` (`Id`),
  CONSTRAINT `FK_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.users
CREATE TABLE IF NOT EXISTS `users` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `CustomId` varchar(20) DEFAULT NULL,
  `ClinicianProviderId` varchar(20) DEFAULT NULL,
  `TitleType` varchar(50) DEFAULT NULL,
  `TitleTypeOther` varchar(100) DEFAULT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `Suffix` varchar(50) DEFAULT NULL,
  `Credentials` varchar(20) DEFAULT NULL,
  `CredentialsOther` varchar(20) DEFAULT NULL,
  `Roles` varchar(50) NOT NULL,
  `EmploymentType` varchar(50) DEFAULT 'Employee',
  `AgencyLocationId` char(36) NOT NULL,
  `Permissions` text NOT NULL,
  `Licenses` text,
  `Rates` mediumtext,
  `ProfileData` text NOT NULL,
  `Messages` text,
  `Status` tinyint(50) unsigned NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsAgencyLinked` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `AllowWeekendAccess` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `EarliestLoginTime` varchar(20) DEFAULT NULL,
  `AutomaticLogoutTime` varchar(20) DEFAULT NULL,
  `Comments` text,
  `SSN` varchar(10) DEFAULT NULL,
  `DOB` date DEFAULT '0001-01-01',
  `IsPrimary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AccountExpireDate` date NOT NULL DEFAULT '0001-01-01',
  `HireDate` date DEFAULT '0001-01-01',
  `TerminationDate` date DEFAULT '0001-01-01',
  `AccessibleServices` tinyint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `FK_User_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_User_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table agencymanagement.userschedules
CREATE TABLE IF NOT EXISTS `userschedules` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `Visits` longtext NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_UserSchedule_UserId` (`UserId`),
  KEY `FK_UserSchedule_PatientId` (`PatientId`),
  KEY `FK_UserSchedule_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_UserSchedule_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencies` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_UserSchedule_PatientId` FOREIGN KEY (`PatientId`) REFERENCES `patients` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_UserSchedule_UserId` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for applicationmanagement
CREATE DATABASE IF NOT EXISTS `applicationmanagement` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `applicationmanagement`;


-- Dumping structure for table applicationmanagement.agencysnapshots
CREATE TABLE IF NOT EXISTS `agencysnapshots` (
  `Id` char(36) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `SalesPerson` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `Trainer` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `BackupTrainer` char(36) DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsAgreementSigned` tinyint(1) unsigned DEFAULT '1',
  `IsSuspended` tinyint(1) unsigned DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FrozenDate` date NOT NULL DEFAULT '0001-01-01',
  `IsFrozen` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DBServerIp` varchar(32) DEFAULT '10.0.1.61',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.applications
CREATE TABLE IF NOT EXISTS `applications` (
  `Id` tinyint(2) unsigned NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Modified` date NOT NULL,
  `Created` date NOT NULL,
  `IsDeprecated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `IX_Applications_Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.clusters
CREATE TABLE IF NOT EXISTS `clusters` (
  `Id` varchar(36) NOT NULL DEFAULT '',
  `DBServerIp` varchar(36) DEFAULT NULL,
  `WebServerIp` varchar(100) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.companies
CREATE TABLE IF NOT EXISTS `companies` (
  `Id` char(36) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `ServerIPAddress` varchar(15) NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.companyapplications
CREATE TABLE IF NOT EXISTS `companyapplications` (
  `CompanyId` char(36) NOT NULL,
  `ApplicationId` tinyint(2) unsigned NOT NULL,
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Modified` date NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`CompanyId`,`ApplicationId`),
  KEY `FK_CompanyApplications_ApplicationId` (`ApplicationId`),
  CONSTRAINT `FK_CompanyApplications_ApplicationId` FOREIGN KEY (`ApplicationId`) REFERENCES `applications` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CompanyApplications_CompanyId` FOREIGN KEY (`CompanyId`) REFERENCES `companies` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.companyusers
CREATE TABLE IF NOT EXISTS `companyusers` (
  `CompanyId` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `IsActive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`CompanyId`,`LoginId`,`UserId`),
  KEY `FK_CompanyUsers_LoginId` (`LoginId`),
  CONSTRAINT `FK_CompanyUsers_CompanyId` FOREIGN KEY (`CompanyId`) REFERENCES `companies` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CompanyUsers_LoginId` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.customernotes
CREATE TABLE IF NOT EXISTS `customernotes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `Comments` text NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Recipients` text,
  `NoteType` tinyint(3) unsigned NOT NULL,
  `Summary` varchar(80) NOT NULL,
  `CallerId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IssueId` int(10) unsigned DEFAULT NULL,
  `AttachmentId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsCompleted` tinyint(1) unsigned NOT NULL,
  `DevUpdates` text,
  `AdditionalInformation` text,
  `ModifiedBy` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `Status` tinyint(100) unsigned NOT NULL DEFAULT '0',
  `Saved` tinyint(4) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `LoginId` (`LoginId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.dashboardmessages
CREATE TABLE IF NOT EXISTS `dashboardmessages` (
  `Id` char(36) NOT NULL,
  `Title` varchar(150) NOT NULL,
  `Text` text NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.errors
CREATE TABLE IF NOT EXISTS `errors` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Message` varchar(500) NOT NULL,
  `Type` varchar(30) NOT NULL,
  `Details` text NOT NULL,
  `Server` varchar(30) NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.impersonationlinks
CREATE TABLE IF NOT EXISTS `impersonationlinks` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `RepLoginId` char(36) NOT NULL,
  `RepName` varchar(50) NOT NULL,
  `IsUsed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ImpersonationLinks_LoginId` (`LoginId`),
  CONSTRAINT `impersonationlinks_ibfk_1` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.logins
CREATE TABLE IF NOT EXISTS `logins` (
  `Id` char(36) NOT NULL,
  `EmailAddress` varchar(100) NOT NULL,
  `PasswordHash` char(88) DEFAULT NULL,
  `PasswordSalt` char(20) DEFAULT NULL,
  `SignatureHash` char(88) DEFAULT NULL,
  `SignatureSalt` char(20) DEFAULT NULL,
  `DisplayName` varchar(50) NOT NULL,
  `Role` varchar(50) NOT NULL,
  `IsAxxessAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsAxxessSupport` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsActive` tinyint(1) unsigned NOT NULL,
  `IsLocked` tinyint(1) unsigned NOT NULL,
  `LastLoginDate` datetime NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `EmailAddress` (`EmailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.notes
CREATE TABLE IF NOT EXISTS `notes` (
  `Id` char(36) NOT NULL,
  `CompanyId` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `Comments` text NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Notes_CompanyId` (`CompanyId`),
  KEY `FK_Notes_LoginId` (`LoginId`),
  CONSTRAINT `FK_Notes_CompanyId` FOREIGN KEY (`CompanyId`) REFERENCES `companies` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Notes_LoginId` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.systemmessages
CREATE TABLE IF NOT EXISTS `systemmessages` (
  `Id` char(36) NOT NULL,
  `Subject` varchar(150) NOT NULL,
  `Body` text NOT NULL,
  `Created` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.therapyemployors
CREATE TABLE IF NOT EXISTS `therapyemployors` (
  `Id` char(36) NOT NULL DEFAULT '',
  `TherapyId` char(50) DEFAULT NULL,
  `AgencyId` char(50) DEFAULT NULL,
  `AgencyName` varchar(250) DEFAULT NULL,
  `State` char(2) DEFAULT NULL,
  `Comments` varchar(250) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `IsDeprecated` int(11) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table applicationmanagement.userloginagencies
CREATE TABLE IF NOT EXISTS `userloginagencies` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `LoginId` char(36) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `IsDeprecated` tinyint(1) DEFAULT NULL,
  `Status` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- Data exporting was unselected.


-- Dumping database structure for axxessforum
CREATE DATABASE IF NOT EXISTS `axxessforum` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `axxessforum`;


-- Dumping structure for table axxessforum.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedById` char(36) NOT NULL,
  `LastPostBy` varchar(50) NOT NULL,
  `LastPostById` char(36) NOT NULL,
  `LastPostDate` datetime NOT NULL,
  `Title` varchar(250) NOT NULL,
  `Body` longtext NOT NULL,
  `ViewCount` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessforum.replies
CREATE TABLE IF NOT EXISTS `replies` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `PostId` int(10) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedById` char(36) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `Body` longtext NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for axxesslogs
CREATE DATABASE IF NOT EXISTS `axxesslogs` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `axxesslogs`;


-- Dumping structure for table axxesslogs.appaudits
CREATE TABLE IF NOT EXISTS `appaudits` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `LogDomain` varchar(50) NOT NULL DEFAULT 'Patient',
  `DomainId` char(36) DEFAULT NULL,
  `EntityId` varchar(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `LogType` varchar(50) DEFAULT NULL,
  `Action` varchar(50) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Modified` date DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslogs.supportappaudits
CREATE TABLE IF NOT EXISTS `supportappaudits` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `LogDomain` varchar(50) NOT NULL DEFAULT 'Agency',
  `DomainId` char(36) DEFAULT NULL,
  `EntityId` varchar(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `LogType` varchar(50) DEFAULT NULL,
  `Action` varchar(50) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Modified` date DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslogs.taskaudits
CREATE TABLE IF NOT EXISTS `taskaudits` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EntityId` char(36) NOT NULL,
  `DisciplineTaskId` tinyint(3) unsigned NOT NULL,
  `Log` mediumtext NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_TaskAudit_UserId` (`EntityId`),
  KEY `FK_TaskAudit_AgencyId` (`AgencyId`),
  KEY `FK_TaskAudit_PatientId` (`PatientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for axxesslookup
CREATE DATABASE IF NOT EXISTS `axxesslookup` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `axxesslookup`;


-- Dumping structure for table axxesslookup.admissionsources
CREATE TABLE IF NOT EXISTS `admissionsources` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Description` varchar(100) NOT NULL,
  `Code` char(2) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.americanstates
CREATE TABLE IF NOT EXISTS `americanstates` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Code` char(2) NOT NULL,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.axxesssubmitterinfos
CREATE TABLE IF NOT EXISTS `axxesssubmitterinfos` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `SubmitterId` varchar(50) DEFAULT NULL,
  `SubmitterName` varchar(50) DEFAULT NULL,
  `Phone` varchar(10) DEFAULT NULL,
  `Fax` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.cbsacodes
CREATE TABLE IF NOT EXISTS `cbsacodes` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Zip` varchar(5) NOT NULL,
  `CityName` varchar(50) DEFAULT NULL,
  `CountyCode` varchar(5) DEFAULT NULL,
  `CountyTitle` varchar(50) DEFAULT NULL,
  `StateCode` varchar(2) NOT NULL,
  `StateName` varchar(50) NOT NULL,
  `CBSA` varchar(5) NOT NULL,
  `WITwoSeven` double unsigned NOT NULL,
  `WITwoEight` double unsigned NOT NULL,
  `WITwoNine` double unsigned NOT NULL,
  `WITwoTen` double unsigned NOT NULL,
  `WITwoEleven` double unsigned NOT NULL,
  `WITwoTwelve` double unsigned NOT NULL,
  `WITwoThirteen` double unsigned NOT NULL,
  `WITwoFourteen` double unsigned NOT NULL,
  `Variance` double NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Zip` (`Zip`),
  KEY `CountyCode` (`CountyCode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.databases
CREATE TABLE IF NOT EXISTS `databases` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.dbtables
CREATE TABLE IF NOT EXISTS `dbtables` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `OrderNumber` int(11) DEFAULT NULL,
  `DatabaseId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.diagnosiscodes
CREATE TABLE IF NOT EXISTS `diagnosiscodes` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` varchar(25) NOT NULL,
  `LongDescription` varchar(250) NOT NULL,
  `ShortDescription` varchar(250) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Code` (`Code`),
  FULLTEXT KEY `LongDescription` (`LongDescription`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.disciplinetasks
CREATE TABLE IF NOT EXISTS `disciplinetasks` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Task` varchar(50) DEFAULT NULL,
  `Discipline` varchar(50) DEFAULT NULL,
  `DisciplineId` int(11) DEFAULT '0',
  `IsBillable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsPayable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsMultiple` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsTypeChangeable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `RevenueCode` varchar(10) DEFAULT NULL,
  `GCode` varchar(10) DEFAULT NULL,
  `Unit` int(10) unsigned DEFAULT NULL,
  `Rate` double unsigned DEFAULT NULL,
  `Version` int(10) unsigned NOT NULL DEFAULT '0',
  `DefaultStatus` int(10) unsigned NOT NULL DEFAULT '410',
  `IsUsedInDropDown` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `Table` int(10) unsigned NOT NULL DEFAULT '400',
  `NeedOrderNumber` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `IsPhysicianOrder` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `NeedPhysician` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `IsTimeInAndOutExist` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.ethnicraces
CREATE TABLE IF NOT EXISTS `ethnicraces` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Description` varchar(150) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.hippsandhhrgs
CREATE TABLE IF NOT EXISTS `hippsandhhrgs` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `HHRG` varchar(6) NOT NULL,
  `HIPPS` varchar(5) NOT NULL,
  `HHRGWeight` double NOT NULL,
  `Time` date NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.insurances
CREATE TABLE IF NOT EXISTS `insurances` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.medicareadvantagecontractors
CREATE TABLE IF NOT EXISTS `medicareadvantagecontractors` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LegalEntityName` varchar(150) NOT NULL,
  `OrganizationMarketingName` varchar(150) NOT NULL,
  `Contract Number` varchar(20) NOT NULL,
  `OrganizationType` varchar(100) NOT NULL,
  `PlanType` varchar(100) NOT NULL,
  `ContractEffectiveDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TaxStatus` varchar(100) NOT NULL,
  `ParentOrganization` varchar(100) NOT NULL,
  `CMSRegionResponsible` varchar(150) NOT NULL,
  `Enrollment` varchar(50) NOT NULL,
  `LegalEntityStreet` varchar(150) NOT NULL,
  `LegalEntityStreet2` varchar(150) NOT NULL,
  `LegalEntityCity` varchar(150) NOT NULL,
  `LegalEntityStateCode` char(2) NOT NULL,
  `LegalEntityZip` char(9) NOT NULL,
  `DirectoryContactTitle` varchar(150) NOT NULL,
  `DirectoryContactLastName` varchar(150) NOT NULL,
  `DirectoryContactFirstName` varchar(150) NOT NULL,
  `DirectoryContactMiddleInitial` char(1) DEFAULT NULL,
  `DirectoryContactPhone` varchar(20) NOT NULL,
  `DirectoryContactPhoneExt` varchar(20) DEFAULT NULL,
  `DirectoryContactFax` varchar(20) DEFAULT NULL,
  `DirectoryContactEmail` varchar(150) DEFAULT NULL,
  `DirectoryContactStreetAddress` varchar(150) DEFAULT NULL,
  `DirectoryContactStreetAddress2` varchar(150) DEFAULT NULL,
  `DirectoryContactCity` varchar(150) DEFAULT NULL,
  `DirectoryContactState` char(2) DEFAULT NULL,
  `DirectoryContactZip` char(9) DEFAULT NULL,
  `DirectoryContactLastUpdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.medicareproviders
CREATE TABLE IF NOT EXISTS `medicareproviders` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProviderNumber` varchar(10) NOT NULL,
  `Name` varchar(150) NOT NULL,
  `Address` varchar(150) NOT NULL,
  `City` varchar(150) NOT NULL,
  `StateCode` char(2) NOT NULL,
  `ZipCode` varchar(5) NOT NULL,
  `Phone` varchar(10) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProviderNumber` (`ProviderNumber`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.medicarerates
CREATE TABLE IF NOT EXISTS `medicarerates` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Discipline` varchar(100) NOT NULL,
  `PerVisitRate` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.medicationroutes
CREATE TABLE IF NOT EXISTS `medicationroutes` (
  `Id` smallint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ShortName` varchar(10) DEFAULT NULL,
  `LongName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.npis
CREATE TABLE IF NOT EXISTS `npis` (
  `Id` varchar(10) NOT NULL DEFAULT '',
  `EntityTypeCode` smallint(10) unsigned DEFAULT NULL,
  `ProviderFirstName` varchar(20) DEFAULT NULL,
  `ProviderMiddleName` varchar(20) DEFAULT NULL,
  `ProviderLastName` varchar(35) DEFAULT NULL,
  `ProviderOrganizationName` varchar(70) DEFAULT NULL,
  `ProviderCredentialText` varchar(20) DEFAULT NULL,
  `ProviderFirstLineBusinessMailingAddress` varchar(55) DEFAULT NULL,
  `ProviderSecondLineBusinessMailingAddress` varchar(55) DEFAULT NULL,
  `ProviderBusinessMailingAddressCityName` varchar(40) DEFAULT NULL,
  `ProviderBusinessMailingAddressStateName` varchar(40) DEFAULT NULL,
  `ProviderBusinessMailingAddressPostalCode` varchar(20) DEFAULT NULL,
  `ProviderBusinessMailingAddressCountryCode` varchar(2) DEFAULT NULL,
  `ProviderBusinessMailingAddressTelephoneNumber` varchar(20) DEFAULT NULL,
  `ProviderBusinessMailingAddressFaxNumber` varchar(20) DEFAULT NULL,
  `ProviderFirstLineBusinessPracticeLocationAddress` varchar(55) DEFAULT NULL,
  `ProviderSecondLineBusinessPracticeLocationAddress` varchar(55) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressCityName` varchar(40) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressStateName` varchar(40) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressPostalCode` varchar(20) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressCountryCode` varchar(2) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressTelephoneNumber` varchar(20) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressFaxNumber` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.npis_copy
CREATE TABLE IF NOT EXISTS `npis_copy` (
  `Id` varchar(10) NOT NULL DEFAULT '',
  `EntityTypeCode` smallint(10) unsigned DEFAULT NULL,
  `ProviderFirstName` varchar(20) DEFAULT NULL,
  `ProviderMiddleName` varchar(20) DEFAULT NULL,
  `ProviderLastName` varchar(35) DEFAULT NULL,
  `ProviderOrganizationName` varchar(70) DEFAULT NULL,
  `ProviderCredentialText` varchar(20) DEFAULT NULL,
  `ProviderFirstLineBusinessMailingAddress` varchar(55) DEFAULT NULL,
  `ProviderSecondLineBusinessMailingAddress` varchar(55) DEFAULT NULL,
  `ProviderBusinessMailingAddressCityName` varchar(40) DEFAULT NULL,
  `ProviderBusinessMailingAddressStateName` varchar(40) DEFAULT NULL,
  `ProviderBusinessMailingAddressPostalCode` varchar(20) DEFAULT NULL,
  `ProviderBusinessMailingAddressCountryCode` varchar(2) DEFAULT NULL,
  `ProviderBusinessMailingAddressTelephoneNumber` varchar(20) DEFAULT NULL,
  `ProviderBusinessMailingAddressFaxNumber` varchar(20) DEFAULT NULL,
  `ProviderFirstLineBusinessPracticeLocationAddress` varchar(55) DEFAULT NULL,
  `ProviderSecondLineBusinessPracticeLocationAddress` varchar(55) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressCityName` varchar(40) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressStateName` varchar(40) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressPostalCode` varchar(20) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressCountryCode` varchar(2) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressTelephoneNumber` varchar(20) DEFAULT NULL,
  `ProviderBusinessPracticeLocationAddressFaxNumber` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.oasisbackups
CREATE TABLE IF NOT EXISTS `oasisbackups` (
  `Id` char(36) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `AssessmentId` char(36) DEFAULT NULL,
  `Data` text,
  `Created` datetime DEFAULT NULL,
  KEY `AssessmentId` (`AssessmentId`),
  KEY `Created` (`Created`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.oasisguides
CREATE TABLE IF NOT EXISTS `oasisguides` (
  `Id` char(5) NOT NULL,
  `ItemIntent` mediumtext NOT NULL,
  `Response` mediumtext NOT NULL,
  `DataSources` mediumtext NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.paymentsources
CREATE TABLE IF NOT EXISTS `paymentsources` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(150) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.pecosphysicians
CREATE TABLE IF NOT EXISTS `pecosphysicians` (
  `Id` varchar(10) NOT NULL,
  `Last` varchar(30) NOT NULL,
  `First` varchar(30) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.pecosphysicians_copy
CREATE TABLE IF NOT EXISTS `pecosphysicians_copy` (
  `Id` varchar(10) NOT NULL,
  `Last` varchar(30) NOT NULL,
  `First` varchar(30) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.pendingphysicians
CREATE TABLE IF NOT EXISTS `pendingphysicians` (
  `Id` varchar(10) NOT NULL,
  `Last` varchar(30) NOT NULL,
  `First` varchar(30) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.ppsstandards
CREATE TABLE IF NOT EXISTS `ppsstandards` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Time` date NOT NULL,
  `UrbanRate` double unsigned NOT NULL DEFAULT '0',
  `RuralRate` double unsigned NOT NULL DEFAULT '0',
  `Labor` double unsigned NOT NULL DEFAULT '0',
  `NonLabor` double unsigned NOT NULL DEFAULT '0',
  `S` double unsigned NOT NULL DEFAULT '0',
  `RuralS` double unsigned NOT NULL DEFAULT '0',
  `T` double unsigned NOT NULL DEFAULT '0',
  `RuralT` double unsigned NOT NULL DEFAULT '0',
  `U` double unsigned NOT NULL DEFAULT '0',
  `RuralU` double unsigned NOT NULL DEFAULT '0',
  `V` double unsigned NOT NULL DEFAULT '0',
  `RuralV` double unsigned NOT NULL DEFAULT '0',
  `W` double unsigned NOT NULL DEFAULT '0',
  `RuralW` double unsigned NOT NULL DEFAULT '0',
  `X` double unsigned NOT NULL DEFAULT '0',
  `RuralX` double unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Year` (`Time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.privatedutydisciplinetasks
CREATE TABLE IF NOT EXISTS `privatedutydisciplinetasks` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Task` varchar(50) DEFAULT NULL,
  `Discipline` varchar(50) DEFAULT NULL,
  `IsBillable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsMultiple` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsCreatable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsTypeChangeable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `RevenueCode` varchar(10) DEFAULT NULL,
  `GCode` varchar(10) DEFAULT NULL,
  `Unit` int(10) unsigned DEFAULT NULL,
  `Rate` double unsigned DEFAULT NULL,
  `Version` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.procedurecodes
CREATE TABLE IF NOT EXISTS `procedurecodes` (
  `Id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Code` varchar(25) NOT NULL,
  `LongDescription` varchar(250) NOT NULL,
  `ShortDescription` varchar(250) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Code` (`Code`),
  FULLTEXT KEY `LongDescription` (`LongDescription`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.referralsources
CREATE TABLE IF NOT EXISTS `referralsources` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.relationships
CREATE TABLE IF NOT EXISTS `relationships` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Code` char(2) NOT NULL,
  `Description` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.ruralcbsacodes
CREATE TABLE IF NOT EXISTS `ruralcbsacodes` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `StateName` varchar(50) NOT NULL,
  `StateCode` char(2) NOT NULL,
  `CBSA` varchar(5) NOT NULL,
  `WITwoSeven` double unsigned NOT NULL,
  `WITwoEight` double unsigned NOT NULL,
  `WITwoNine` double unsigned NOT NULL,
  `WITwoTen` double unsigned NOT NULL,
  `WITwoEleven` double unsigned NOT NULL,
  `WITwoTwelve` double unsigned NOT NULL,
  `WITwoThirteen` double unsigned NOT NULL,
  `WITwoFourteen` double unsigned NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.submissionbodyformats
CREATE TABLE IF NOT EXISTS `submissionbodyformats` (
  `Id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(255) DEFAULT NULL,
  `Length` float unsigned DEFAULT NULL,
  `Start` float unsigned DEFAULT NULL,
  `End` float unsigned DEFAULT NULL,
  `RFA01` float unsigned DEFAULT NULL,
  `RFA02` float unsigned DEFAULT NULL,
  `RFA03` float unsigned DEFAULT NULL,
  `RFA04` float unsigned DEFAULT NULL,
  `RFA05` float unsigned DEFAULT NULL,
  `RFA06` float unsigned DEFAULT NULL,
  `RFA07` float unsigned DEFAULT NULL,
  `RFA08` float unsigned DEFAULT NULL,
  `RFA09` float unsigned DEFAULT NULL,
  `RFA10` float unsigned DEFAULT NULL,
  `RFA_REQ` varchar(255) DEFAULT NULL,
  `RFA_BLANK` varchar(255) DEFAULT NULL,
  `ElementName` varchar(255) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  `IsIgnorable` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.submissionfooterformats
CREATE TABLE IF NOT EXISTS `submissionfooterformats` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.submissionheaderformats
CREATE TABLE IF NOT EXISTS `submissionheaderformats` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.submissioninactivebodyformats
CREATE TABLE IF NOT EXISTS `submissioninactivebodyformats` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.supplies
CREATE TABLE IF NOT EXISTS `supplies` (
  `Id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Code` char(5) NOT NULL,
  `Description` varchar(350) NOT NULL,
  `CategoryId` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.supplycategories
CREATE TABLE IF NOT EXISTS `supplycategories` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Description` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.taxonomies
CREATE TABLE IF NOT EXISTS `taxonomies` (
  `Id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `TaxoId` varchar(12) DEFAULT NULL,
  `TaxoClass` varchar(100) DEFAULT NULL,
  `TaxoSubclass` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `TaxoId` (`TaxoId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.therapydisciplinetasks
CREATE TABLE IF NOT EXISTS `therapydisciplinetasks` (
  `Id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Task` varchar(50) DEFAULT NULL,
  `Discipline` varchar(50) DEFAULT NULL,
  `IsBillable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsMultiple` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `RevenueCode` varchar(10) DEFAULT NULL,
  `GCode` varchar(10) DEFAULT NULL,
  `Unit` int(10) unsigned DEFAULT NULL,
  `Rate` double unsigned DEFAULT NULL,
  `Version` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxesslookup.zipcodes
CREATE TABLE IF NOT EXISTS `zipcodes` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Code` char(5) DEFAULT NULL,
  `StateCode` char(2) DEFAULT NULL,
  `StateName` varchar(50) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `CBSA` char(5) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for axxessmembership
CREATE DATABASE IF NOT EXISTS `axxessmembership` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `axxessmembership`;


-- Dumping structure for table axxessmembership.agencysnapshots
CREATE TABLE IF NOT EXISTS `agencysnapshots` (
  `Id` char(36) NOT NULL,
  `Trainer` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `BackupTrainer` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `SalesPerson` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `ImplementationSpecialist` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `Name` varchar(100) NOT NULL,
  `AccountId` varchar(100) DEFAULT NULL,
  `IsSuspended` tinyint(1) unsigned DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsFrozen` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FrozenDate` date NOT NULL DEFAULT '0001-01-01',
  `Created` date NOT NULL DEFAULT '0001-01-01',
  `ClusterId` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.ddecredentials
CREATE TABLE IF NOT EXISTS `ddecredentials` (
  `Id` char(36) NOT NULL,
  `UserId` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Created` date NOT NULL DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.errors
CREATE TABLE IF NOT EXISTS `errors` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Message` varchar(500) NOT NULL,
  `Type` varchar(30) NOT NULL,
  `Details` text NOT NULL,
  `Server` varchar(30) NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.impersonationlinks
CREATE TABLE IF NOT EXISTS `impersonationlinks` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `RepLoginId` char(36) NOT NULL,
  `RepName` varchar(50) NOT NULL,
  `IsUsed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ImpersonationLinks_LoginId` (`LoginId`),
  CONSTRAINT `FK_ImpersonationLinks_LoginId` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.logins
CREATE TABLE IF NOT EXISTS `logins` (
  `Id` char(36) NOT NULL,
  `EmailAddress` varchar(100) NOT NULL,
  `PasswordHash` char(88) DEFAULT NULL,
  `PasswordSalt` char(20) DEFAULT NULL,
  `SignatureHash` char(88) DEFAULT NULL,
  `SignatureSalt` char(20) DEFAULT NULL,
  `DisplayName` varchar(50) NOT NULL,
  `Role` varchar(50) NOT NULL,
  `IsAxxessAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsAxxessSupport` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsActive` tinyint(1) unsigned NOT NULL,
  `IsLocked` tinyint(1) unsigned NOT NULL,
  `LastLoginDate` datetime NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `EmailAddress` (`EmailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.logintokens
CREATE TABLE IF NOT EXISTS `logintokens` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `IsUsed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_LoginTokens_AgencyId` (`AgencyId`),
  KEY `FK_LoginTokens_UserId` (`UserId`),
  CONSTRAINT `FK_LoginTokens_UserId` FOREIGN KEY (`UserId`) REFERENCES `usersnapshots` (`Id`),
  CONSTRAINT `FK_LoginTokens_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencysnapshots` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.physiciansnapshots
CREATE TABLE IF NOT EXISTS `physiciansnapshots` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `NPI` char(10) DEFAULT NULL,
  `HasPhysicianAccess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Physicians_AgencyId` (`AgencyId`),
  KEY `FK_Physicians_LoginId` (`LoginId`),
  CONSTRAINT `FK_Physicians_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencysnapshots` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Physicians_LoginId` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.sessiontokens
CREATE TABLE IF NOT EXISTS `sessiontokens` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `TokenId` varchar(150) NOT NULL,
  `DeviceId` varchar(150) NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.shpdatalogins
CREATE TABLE IF NOT EXISTS `shpdatalogins` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `Username` varchar(100) DEFAULT NULL,
  `PasswordHash` char(100) DEFAULT NULL,
  `ProviderNo` varchar(10) NOT NULL,
  `ActivationCode` varchar(10) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ShpDataLogins_AgencyId` (`AgencyId`),
  CONSTRAINT `FK_ShpDataLogins_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencysnapshots` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table axxessmembership.usersnapshots
CREATE TABLE IF NOT EXISTS `usersnapshots` (
  `Id` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `TitleType` varchar(50) DEFAULT NULL,
  `Status` tinyint(50) unsigned NOT NULL,
  `Created` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_Users_AgencyId` (`AgencyId`),
  KEY `FK_Users_LoginId` (`LoginId`),
  CONSTRAINT `FK_Users_AgencyId` FOREIGN KEY (`AgencyId`) REFERENCES `agencysnapshots` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Users_LoginId` FOREIGN KEY (`LoginId`) REFERENCES `logins` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for customermanagement
CREATE DATABASE IF NOT EXISTS `customermanagement` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `customermanagement`;


-- Dumping structure for table customermanagement.assets
CREATE TABLE IF NOT EXISTS `assets` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `FileName` varchar(50) NOT NULL,
  `FileSize` varchar(20) NOT NULL,
  `ContentType` varchar(20) NOT NULL,
  `Bytes` mediumblob NOT NULL,
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Asset_AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.customernotes
CREATE TABLE IF NOT EXISTS `customernotes` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `LoginId` char(36) NOT NULL,
  `Comments` text NOT NULL,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Recipients` text,
  `NoteType` tinyint(3) unsigned NOT NULL,
  `Summary` varchar(80) NOT NULL,
  `CallerId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IssueId` int(10) unsigned DEFAULT NULL,
  `AttachmentId` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `IsCompleted` tinyint(1) unsigned NOT NULL,
  `DevUpdates` text,
  `AdditionalInformation` text,
  `ModifiedBy` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `Status` tinyint(100) unsigned NOT NULL DEFAULT '0',
  `Saved` tinyint(4) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AgencyId` (`AgencyId`),
  KEY `LoginId` (`LoginId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.ddecredentials
CREATE TABLE IF NOT EXISTS `ddecredentials` (
  `Id` char(36) NOT NULL,
  `UserId` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL,
  `Created` date NOT NULL DEFAULT '0001-01-01',
  PRIMARY KEY (`Id`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.internalusers
CREATE TABLE IF NOT EXISTS `internalusers` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `LoginId` char(36) DEFAULT '',
  `Permissions` text,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `LoginId` (`LoginId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.logins
CREATE TABLE IF NOT EXISTS `logins` (
  `Id` char(36) NOT NULL,
  `EmailAddress` varchar(100) NOT NULL,
  `PasswordHash` char(88) DEFAULT NULL,
  `PasswordSalt` char(20) DEFAULT NULL,
  `SignatureHash` char(88) DEFAULT NULL,
  `SignatureSalt` char(20) DEFAULT NULL,
  `DisplayName` varchar(50) NOT NULL,
  `Role` varchar(50) NOT NULL,
  `IsAxxessAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsAxxessSupport` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsActive` tinyint(1) unsigned NOT NULL,
  `IsLocked` tinyint(1) unsigned NOT NULL,
  `LastLoginDate` datetime NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `EmailAddress` (`EmailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `Id` varchar(100) NOT NULL DEFAULT '',
  `Name` varchar(100) DEFAULT NULL,
  `MenuName` varchar(100) DEFAULT NULL,
  `Url` varchar(100) DEFAULT NULL,
  `Onload` varchar(100) DEFAULT NULL,
  `ParentMenus` varchar(100) DEFAULT NULL,
  `Category` int(11) DEFAULT NULL,
  `OtherCategory` varchar(50) DEFAULT '',
  `Action` int(11) DEFAULT NULL,
  `StyleOptions` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.permissionactions
CREATE TABLE IF NOT EXISTS `permissionactions` (
  `Id` int(10) NOT NULL DEFAULT '0',
  `Name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.permissionactivitylinks
CREATE TABLE IF NOT EXISTS `permissionactivitylinks` (
  `Id` int(10) NOT NULL DEFAULT '0',
  `PermissionId` int(10) NOT NULL DEFAULT '0',
  `ActionId` int(10) NOT NULL DEFAULT '0',
  `Name` varchar(50) DEFAULT NULL,
  `Description` text,
  PRIMARY KEY (`Id`),
  KEY `PermissionId` (`PermissionId`),
  KEY `ActionId` (`ActionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.permissioncategories
CREATE TABLE IF NOT EXISTS `permissioncategories` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.permissionsubcategories
CREATE TABLE IF NOT EXISTS `permissionsubcategories` (
  `Id` int(10) unsigned NOT NULL DEFAULT '0',
  `CategoryId` int(10) unsigned NOT NULL DEFAULT '0',
  `Name` varchar(50) DEFAULT NULL,
  `Description` text,
  `IsDeprecated` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `IsSubCategory` tinyint(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `CategoryId` (`CategoryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.


-- Dumping structure for table customermanagement.permissiontemplates
CREATE TABLE IF NOT EXISTS `permissiontemplates` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Permissions` text,
  `IsDeprecated` tinyint(1) unsigned DEFAULT '0',
  `Modified` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for oasisc
CREATE DATABASE IF NOT EXISTS `oasisc` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `oasisc`;


-- Dumping structure for table oasisc.assessments
CREATE TABLE IF NOT EXISTS `assessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `DisciplineTask` int(10) unsigned DEFAULT '0',
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) NOT NULL,
  `IsValidated` tinyint(1) NOT NULL DEFAULT '0',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `PatientId` (`PatientId`),
  KEY `AgencyId` (`AgencyId`),
  KEY `Status` (`Status`),
  KEY `ExportedDate` (`ExportedDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.deathathomeassessments
CREATE TABLE IF NOT EXISTS `deathathomeassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'DischargeFromAgencyDeath',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.dischargefromagencyassessments
CREATE TABLE IF NOT EXISTS `dischargefromagencyassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'DischargeFromAgency',
  `Version` int(10) unsigned DEFAULT '1',
  `IsValidated` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.followupassessments
CREATE TABLE IF NOT EXISTS `followupassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'FollowUp',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.nonoasisdischargeassessments
CREATE TABLE IF NOT EXISTS `nonoasisdischargeassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'NonOasisDischarge',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.nonoasisrecertificationassessments
CREATE TABLE IF NOT EXISTS `nonoasisrecertificationassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned NOT NULL DEFAULT '1',
  `VersionNumber` int(10) unsigned NOT NULL DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'NonOasisRecertification',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.nonoasisstartofcareassessments
CREATE TABLE IF NOT EXISTS `nonoasisstartofcareassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'NonOasisStartOfCare',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.oasisbackups
CREATE TABLE IF NOT EXISTS `oasisbackups` (
  `Id` char(36) DEFAULT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `AssessmentId` char(36) DEFAULT NULL,
  `Data` text,
  `Created` datetime DEFAULT NULL,
  KEY `AssessmentId` (`AssessmentId`),
  KEY `Created` (`Created`),
  KEY `AgencyId` (`AgencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.oasisguides
CREATE TABLE IF NOT EXISTS `oasisguides` (
  `Id` char(5) NOT NULL,
  `ItemIntent` mediumtext NOT NULL,
  `Response` mediumtext NOT NULL,
  `DataSources` mediumtext NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.planofcares
CREATE TABLE IF NOT EXISTS `planofcares` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL DEFAULT '',
  `PatientId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `UserId` char(36) NOT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `Data` text,
  `Status` int(11) NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `SentDate` date DEFAULT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AssessmentId` char(36) NOT NULL,
  `AssessmentType` varchar(50) NOT NULL,
  `PhysicianData` text,
  `IsNonOasis` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsStandAlone` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`,`EpisodeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.planofcarestandalones
CREATE TABLE IF NOT EXISTS `planofcarestandalones` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) NOT NULL,
  `EpisodeId` char(36) NOT NULL,
  `PatientId` char(36) NOT NULL,
  `UserId` char(36) NOT NULL,
  `PhysicianId` char(36) DEFAULT NULL,
  `OrderNumber` bigint(20) unsigned NOT NULL,
  `Data` text,
  `Status` int(11) NOT NULL,
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `PhysicianSignatureText` varchar(150) DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `PhysicianSignatureDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `SentDate` date DEFAULT NULL,
  `Modified` date NOT NULL,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PhysicianData` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.recertificationassessments
CREATE TABLE IF NOT EXISTS `recertificationassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned NOT NULL DEFAULT '1',
  `VersionNumber` int(10) unsigned NOT NULL DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'Recertification',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.resumptionofcareassessments
CREATE TABLE IF NOT EXISTS `resumptionofcareassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'ResumptionOfCare',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.startofcareassessments
CREATE TABLE IF NOT EXISTS `startofcareassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'StartOfCare',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.submissionbodyformats
CREATE TABLE IF NOT EXISTS `submissionbodyformats` (
  `Id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(255) DEFAULT NULL,
  `Length` float unsigned DEFAULT NULL,
  `Start` float unsigned DEFAULT NULL,
  `End` float unsigned DEFAULT NULL,
  `RFA01` float unsigned DEFAULT NULL,
  `RFA02` float unsigned DEFAULT NULL,
  `RFA03` float unsigned DEFAULT NULL,
  `RFA04` float unsigned DEFAULT NULL,
  `RFA05` float unsigned DEFAULT NULL,
  `RFA06` float unsigned DEFAULT NULL,
  `RFA07` float unsigned DEFAULT NULL,
  `RFA08` float unsigned DEFAULT NULL,
  `RFA09` float unsigned DEFAULT NULL,
  `RFA10` float unsigned DEFAULT NULL,
  `RFA_REQ` varchar(255) DEFAULT NULL,
  `RFA_BLANK` varchar(255) DEFAULT NULL,
  `ElementName` varchar(255) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  `IsIgnorable` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.submissionfooterformats
CREATE TABLE IF NOT EXISTS `submissionfooterformats` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.submissionheaderformats
CREATE TABLE IF NOT EXISTS `submissionheaderformats` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.submissioninactivebodyformats
CREATE TABLE IF NOT EXISTS `submissioninactivebodyformats` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Item` varchar(50) DEFAULT NULL,
  `Length` int(4) DEFAULT NULL,
  `Start` int(4) DEFAULT NULL,
  `End` int(4) DEFAULT NULL,
  `DataType` varchar(50) DEFAULT NULL,
  `PadType` varchar(1) DEFAULT NULL,
  `DefaultValue` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.transferdischargeassessments
CREATE TABLE IF NOT EXISTS `transferdischargeassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` text,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'TransferInPatientDischarged',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table oasisc.transfernotdischargedassessments
CREATE TABLE IF NOT EXISTS `transfernotdischargedassessments` (
  `Id` char(36) NOT NULL,
  `AgencyId` char(36) DEFAULT NULL,
  `PatientId` char(36) NOT NULL,
  `EpisodeId` char(36) DEFAULT NULL,
  `UserId` char(36) DEFAULT NULL,
  `OasisData` mediumtext,
  `HippsVersion` char(5) DEFAULT NULL,
  `HippsCode` char(5) DEFAULT NULL,
  `ClaimKey` char(18) DEFAULT NULL,
  `SubmissionFormat` text,
  `CancellationFormat` text,
  `Status` int(10) unsigned DEFAULT NULL,
  `VersionNumber` int(10) unsigned DEFAULT '0',
  `AssessmentDate` date DEFAULT NULL,
  `ExportedDate` date DEFAULT '0001-01-01',
  `Created` date NOT NULL,
  `Modified` date NOT NULL,
  `Supply` text,
  `IsDeprecated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SignatureText` varchar(150) DEFAULT NULL,
  `SignatureDate` date DEFAULT NULL,
  `TimeIn` varchar(10) DEFAULT NULL,
  `TimeOut` varchar(10) DEFAULT NULL,
  `Type` varchar(30) DEFAULT 'TransferInPatientNotDischarged',
  `IsValidated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
