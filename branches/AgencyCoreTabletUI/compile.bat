@ECHO OFF
ECHO Compressing Files...
FOR /f %%f IN ('dir /b .\AgencyCore\Axxess.AgencyManagement.WebSite\Scripts\Modules\*.js') DO java -jar compiler.jar --js AgencyCore\Axxess.AgencyManagement.WebSite\Scripts\Modules\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore\Axxess.AgencyManagement.WebSite\Scripts\Modules\min\%%f
FOR /f %%f IN ('dir /b .\AgencyCore\Axxess.AgencyManagement.WebSite\Scripts\Plugins\Custom\*.js') DO java -jar compiler.jar --js AgencyCore\Axxess.AgencyManagement.WebSite\Scripts\Plugins\Custom\%%f --create_source_map ./~compile.tmp --js_output_file AgencyCore\Axxess.AgencyManagement.WebSite\Scripts\Plugins\Custom\min\%%f
del ~compile.tmp
ECHO Complete
pause