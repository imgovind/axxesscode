﻿namespace Axxess.OasisC.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Enums;
    using Domain;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    

    public static class Databases
    {
        public static void InsertAny(this SimpleRepository repository, IAssessment assessment)
        {
            switch (assessment.Type)
            {
                case AssessmentType.StartOfCare:
                    repository.Add<StartOfCareAssessment>((StartOfCareAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgency:
                    repository.Add<DischargeFromAgencyAssessment>((DischargeFromAgencyAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgencyDeath:
                    repository.Add<DeathAtHomeAssessment>((DeathAtHomeAssessment)assessment);
                    break;
                case AssessmentType.FollowUp:
                    repository.Add<FollowUpAssessment>((FollowUpAssessment)assessment);
                    break;
                case AssessmentType.Recertification:
                    repository.Add<RecertificationAssessment>((RecertificationAssessment)assessment);
                    break;
                case AssessmentType.ResumptionOfCare:
                    repository.Add<ResumptionofCareAssessment>((ResumptionofCareAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientDischarged:
                    repository.Add<TransferDischargeAssessment>((TransferDischargeAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientNotDischarged:
                    repository.Add<TransferNotDischargedAssessment>((TransferNotDischargedAssessment)assessment);
                    break;
                case AssessmentType.NonOasisStartOfCare:
                    repository.Add<NonOasisStartOfCareAssessment>((NonOasisStartOfCareAssessment)assessment);
                    break;
                case AssessmentType.NonOasisRecertification:
                    repository.Add<NonOasisRecertificationAssessment>((NonOasisRecertificationAssessment)assessment);
                    break;
                case AssessmentType.NonOasisDischarge:
                    repository.Add<NonOasisDischargeAssessment>((NonOasisDischargeAssessment)assessment);
                    break;
                default:
                    break;
            }
        }

        public static void UpdateAny(this SimpleRepository repository, IAssessment assessment)
        {
            switch (assessment.Type)
            {
                case AssessmentType.StartOfCare:
                    repository.Update<StartOfCareAssessment>((StartOfCareAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgency:
                    repository.Update<DischargeFromAgencyAssessment>((DischargeFromAgencyAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgencyDeath:
                    repository.Update<DeathAtHomeAssessment>((DeathAtHomeAssessment)assessment);
                    break;
                case AssessmentType.FollowUp:
                    repository.Update<FollowUpAssessment>((FollowUpAssessment)assessment);
                    break;
                case AssessmentType.Recertification:
                    repository.Update<RecertificationAssessment>((RecertificationAssessment)assessment);
                    break;
                case AssessmentType.ResumptionOfCare:
                    repository.Update<ResumptionofCareAssessment>((ResumptionofCareAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientDischarged:
                    repository.Update<TransferDischargeAssessment>((TransferDischargeAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientNotDischarged:
                    repository.Update<TransferNotDischargedAssessment>((TransferNotDischargedAssessment)assessment);
                    break;
                case AssessmentType.NonOasisStartOfCare:
                    repository.Update<NonOasisStartOfCareAssessment>((NonOasisStartOfCareAssessment)assessment);
                    break;
                case AssessmentType.NonOasisRecertification:
                    repository.Update<NonOasisRecertificationAssessment>((NonOasisRecertificationAssessment)assessment);
                    break;
                case AssessmentType.NonOasisDischarge:
                    repository.Update<NonOasisDischargeAssessment>((NonOasisDischargeAssessment)assessment);
                    break;
                default:
                    break;
            }
        }

        public static Assessment FindAny(this SimpleRepository repository, string assessmentType, Guid assessmentId, Guid agencyId)
        {
            Assessment assessment = null;
            var type = new AssessmentType();
            switch (assessmentType)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                    assessment = repository.Single<StartOfCareAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
                    type = AssessmentType.StartOfCare;
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    assessment = repository.Single<ResumptionofCareAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
                    type = AssessmentType.ResumptionOfCare;
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    assessment = repository.Single<FollowUpAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
                    type = AssessmentType.FollowUp;
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    assessment = repository.Single<RecertificationAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
                    type = AssessmentType.Recertification;
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "TransferInPatientNotDischarged":
                    assessment = repository.Single<TransferNotDischargedAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
                    type = AssessmentType.TransferInPatientNotDischarged;
                    break;
                case "OASISCTransferDischarge":
                case "TransferInPatientDischarged":
                    assessment = repository.Single<TransferDischargeAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
                    type = AssessmentType.TransferInPatientDischarged;
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    assessment = repository.Single<DeathAtHomeAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
                    type = AssessmentType.DischargeFromAgencyDeath;
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    assessment = repository.Single<DischargeFromAgencyAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
                    type = AssessmentType.DischargeFromAgency;
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    assessment = repository.Single<NonOasisDischargeAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
                    type = AssessmentType.NonOasisDischarge;
                    break;
                case "SNAssessment":
                case "NonOasisStartOfCare":
                case "NonOASISStartofCare":
                    assessment = repository.Single<NonOasisStartOfCareAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
                    type = AssessmentType.NonOasisStartOfCare;
                    break;
                case "SNAssessmentRecert":
                case "NonOasisRecertification":
                case "NonOASISRecertification":
                    assessment = repository.Single<NonOasisRecertificationAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
                    type = AssessmentType.NonOasisRecertification;
                    break;
                default:
                    break;
            }
            if (assessment != null)
            {
                assessment.Type = type;
            }
            return assessment;
        }

        public static Assessment FindAny(this SimpleRepository repository, Guid agencyId, Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType)
        {
            Assessment assessment = null;
            switch (assessmentType)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                    assessment = repository.Single<StartOfCareAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId );
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    assessment = repository.Single<ResumptionofCareAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId );
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    assessment = repository.Single<FollowUpAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId );
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    assessment = repository.Single<RecertificationAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId );
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "TransferInPatientNotDischarged":
                    assessment = repository.Single<TransferNotDischargedAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId );
                    break;
                case "OASISCTransferDischarge":
                case "TransferInPatientDischarged":
                    assessment = repository.Single<TransferDischargeAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId );
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    assessment = repository.Single<DeathAtHomeAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId );
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    assessment = repository.Single<DischargeFromAgencyAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId );
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    assessment = repository.Single<NonOasisDischargeAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId );
                    break;
                case "SNAssessment":
                case "NonOasisStartOfCare":
                case "NonOASISStartofCare":
                    assessment = repository.Single<NonOasisStartOfCareAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId );
                    break;
                case "SNAssessmentRecert":
                case "NonOasisRecertification":
                case "NonOASISRecertification":
                    assessment = repository.Single<NonOasisRecertificationAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId );
                    break;
                default:
                    break;
            }
            return assessment;
        }

        public static IList<Assessment> FindAnyByStatus(this SimpleRepository repository, Guid agencyId, string assessmentType, int status)
        {
            IList<Assessment> assessments = new List<Assessment>();
            switch (assessmentType)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                    assessments = repository.Find<StartOfCareAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    assessments = repository.Find<ResumptionofCareAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    assessments = repository.Find<FollowUpAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    assessments = repository.Find<RecertificationAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "TransferInPatientNotDischarged":
                    assessments = repository.Find<TransferNotDischargedAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "OASISCTransferDischarge":
                case "TransferInPatientDischarged":
                    assessments = repository.Find<TransferDischargeAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    assessments = repository.Find<DeathAtHomeAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    assessments = repository.Find<DischargeFromAgencyAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "SNAssessment":
                case "NonOasisStartOfCare":
                case "NonOASISStartofCare":
                    assessments = repository.Find<NonOasisStartOfCareAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "SNAssessmentRecert":
                case "NonOasisRecertification":
                case "NonOASISRecertification":
                    assessments = repository.Find<NonOasisRecertificationAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    assessments = repository.Find<NonOasisDischargeAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                default:
                    break;
            }
            return assessments;
        }

        public static IList<AssessmentExport> FindAnyByStatusLean(this SimpleRepository repository, Guid agencyId, Guid branchId, string assessmentType, int status, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var assessments = new List<AssessmentExport>();
            var table = string.Empty;
            var type = string.Empty;
            var name = string.Empty;
            switch (assessmentType)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                    table = "startofcareassessments";
                    type = AssessmentType.StartOfCare.ToString();
                    name = AssessmentType.StartOfCare.GetDescription();
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    table = "resumptionofcareassessments";
                    type = AssessmentType.ResumptionOfCare.ToString();
                    name = AssessmentType.ResumptionOfCare.GetDescription();
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    table = "followupassessments";
                    type = AssessmentType.FollowUp.ToString();
                    name = AssessmentType.FollowUp.GetDescription();
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    table = "recertificationassessments";
                    type = AssessmentType.Recertification.ToString();
                    name = AssessmentType.Recertification.GetDescription();
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "TransferInPatientNotDischarged":
                    table = "transfernotdischargedassessments";
                    type = AssessmentType.TransferInPatientNotDischarged.ToString();
                    name = AssessmentType.TransferInPatientNotDischarged.GetDescription();
                    break;
                case "OASISCTransferDischarge":
                case "TransferInPatientDischarged":
                    table = "transferdischargeassessments";
                    type = AssessmentType.TransferInPatientDischarged.ToString();
                    name = AssessmentType.TransferInPatientDischarged.GetDescription();
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    table = "deathathomeassessments";
                    type = AssessmentType.DischargeFromAgencyDeath.ToString();
                    name = AssessmentType.DischargeFromAgencyDeath.GetDescription();
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    table = "dischargefromagencyassessments";
                    type = AssessmentType.DischargeFromAgency.ToString();
                    name = AssessmentType.DischargeFromAgency.GetDescription();
                    break;
                case "SNAssessment":
                case "NonOasisStartOfCare":
                case "NonOASISStartofCare":
                    table = "nonoasisstartofcareassessments";
                    type = AssessmentType.NonOasisStartOfCare.ToString();
                    name = AssessmentType.NonOasisStartOfCare.GetDescription();
                    break;
                case "SNAssessmentRecert":
                case "NonOasisRecertification":
                case "NonOASISRecertification":
                    table = "nonoasisrecertificationassessments";
                    type = AssessmentType.NonOasisRecertification.ToString();
                    name = AssessmentType.NonOasisRecertification.GetDescription();
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    table = "nonoasisdischargeassessments";
                    type = AssessmentType.NonOasisDischarge.ToString();
                    name = AssessmentType.NonOasisDischarge.GetDescription();
                    break;
                default:
                    break;
            }

            if (table.IsNotNullOrEmpty())
            {
                var patientStatusQuery = string.Empty; ;
                if (patientStatus <= 0)
                {
                    patientStatusQuery = " AND ( agencymanagement.patients.Status = 1 OR agencymanagement.patients.Status = 2 )";
                }
                else
                {
                    patientStatusQuery = " AND agencymanagement.patients.Status = @statusid";
                }

                var dateRange = string.Empty;
                if (status == 225)
                {
                    dateRange = string.Format("AND oasisc.{0}.ExportedDate between @startdate and @enddate ",table);
                }
                else if (status == 220)
                {
                    dateRange = string.Format("AND oasisc.{0}.AssessmentDate between @startdate and @enddate ",table);
                }

                var script = string.Format(@"SELECT agencymanagement.patients.Id as PatientId, agencymanagement.patients.FirstName as FirstName, agencymanagement.patients.LastName as LastName, agencymanagement.patients.PrimaryInsurance as InsuranceId , oasisc.{0}.Id  as Id , oasisc.{0}.VersionNumber as VersionNumber, oasisc.{0}.AssessmentDate as AssessmentDate , oasisc.{0}.Modified as Modified , oasisc.{0}.ExportedDate as ExportedDate , oasisc.{0}.EpisodeId as EpisodeId, " +
                  "agencymanagement.patientepisodes.schedule, agencymanagement.patientepisodes.EndDate as EndDate, agencymanagement.patientepisodes.StartDate as StartDate  " +
                  "FROM oasisc.{0} INNER JOIN agencymanagement.patients ON oasisc.{0}.PatientId = agencymanagement.patients.Id INNER JOIN agencymanagement.patientepisodes ON  oasisc.{0}.EpisodeId = agencymanagement.patientepisodes.Id " +
                  "WHERE oasisc.{0}.AgencyId = @agencyid {2} {3} {4} AND agencymanagement.patients.IsDeprecated = 0 AND agencymanagement.patientepisodes.IsDischarged = 0 AND agencymanagement.patientepisodes.IsActive = 1 AND oasisc.{0}.Status=@status ", table, status, patientStatusQuery, !branchId.IsEmpty() ? " AND agencymanagement.patients.AgencyLocationId = @branchId " : string.Empty, dateRange);

                using (var cmd = new FluentCommand<AssessmentExport>(script))
                {
                    assessments = cmd.SetConnection("AgencyManagementConnectionString")
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .AddInt("statusid", patientStatus)
                       .AddDateTime("startDate", startDate)
                       .AddDateTime("endDate", endDate)
                       .SetMap(reader => new AssessmentExport
                       {
                           AssessmentId = reader.GetGuid("Id"),
                           PatientId = reader.GetGuid("PatientId"),
                           EpisodeId = reader.GetGuid("EpisodeId"),
                           AssessmentDate = reader.GetDateTime("AssessmentDate"),
                           EpisodeStartDate = reader.GetDateTime("StartDate"),
                           EpisodeEndDate = reader.GetDateTime("EndDate"),
                           AssessmentName = name,
                           AssessmentType = type,
                           EpisodeData = reader.GetString("Schedule"),
                           CorrectionNumber = reader.GetInt("VersionNumber"),
                           PatientName = string.Format("{1}, {0}", reader.GetString("FirstName").ToUpperCase(), reader.GetString("LastName").ToUpperCase()),
                           InsuranceId = reader.GetIntNullable("InsuranceId") != null ? reader.GetInt("InsuranceId") : -1,
                           ExportedDate = reader.GetDateTime("ExportedDate").Date > DateTime.MinValue ? reader.GetDateTime("ExportedDate") : reader.GetDateTime("Modified")

                       }).AsList();
                }
            }
            return assessments;
        }

        public static IList<AssessmentExport> FindAnyByStatusLean(this SimpleRepository repository, Guid agencyId, Guid branchId, string assessmentType, int status, string insuranceId)
        {
            var assessments = new List<AssessmentExport>();
            var table = string.Empty;
            var type = string.Empty;
            var name = string.Empty;
            switch (assessmentType)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                    table = "startofcareassessments";
                    type = AssessmentType.StartOfCare.ToString();
                    name = AssessmentType.StartOfCare.GetDescription();
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    table = "resumptionofcareassessments";
                    type = AssessmentType.ResumptionOfCare.ToString();
                    name = AssessmentType.ResumptionOfCare.GetDescription();
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    table = "followupassessments";
                    type = AssessmentType.FollowUp.ToString();
                    name = AssessmentType.FollowUp.GetDescription();
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    table = "recertificationassessments";
                    type = AssessmentType.Recertification.ToString();
                    name = AssessmentType.Recertification.GetDescription();
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "TransferInPatientNotDischarged":
                    table = "transfernotdischargedassessments";
                    type = AssessmentType.TransferInPatientNotDischarged.ToString();
                    name = AssessmentType.TransferInPatientNotDischarged.GetDescription();
                    break;
                case "OASISCTransferDischarge":
                case "TransferInPatientDischarged":
                    table = "transferdischargeassessments";
                    type = AssessmentType.TransferInPatientDischarged.ToString();
                    name = AssessmentType.TransferInPatientDischarged.GetDescription();
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    table = "deathathomeassessments";
                    type = AssessmentType.DischargeFromAgencyDeath.ToString();
                    name = AssessmentType.DischargeFromAgencyDeath.GetDescription();
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    table = "dischargefromagencyassessments";
                    type = AssessmentType.DischargeFromAgency.ToString();
                    name = AssessmentType.DischargeFromAgency.GetDescription();
                    break;
                case "SNAssessment":
                case "NonOasisStartOfCare":
                case "NonOASISStartofCare":
                    table = "nonoasisstartofcareassessments";
                    type = AssessmentType.NonOasisStartOfCare.ToString();
                    name = AssessmentType.NonOasisStartOfCare.GetDescription();
                    break;
                case "SNAssessmentRecert":
                case "NonOasisRecertification":
                case "NonOASISRecertification":
                    table = "nonoasisrecertificationassessments";
                    type = AssessmentType.NonOasisRecertification.ToString();
                    name = AssessmentType.NonOasisRecertification.GetDescription();
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    table = "nonoasisdischargeassessments";
                    type = AssessmentType.NonOasisDischarge.ToString();
                    name = AssessmentType.NonOasisDischarge.GetDescription();
                    break;
                default:
                    break;
            }

            if (table.IsNotNullOrEmpty())
            {
                var insurance = string.Empty;
                if (insuranceId.IsNotNullOrEmpty() && insuranceId.IsInteger() && insuranceId.ToInteger() <= 0)
                {
                   
                }
                else
                {
                    insurance = " AND agencymanagement.patients.PrimaryInsurance = @insuranceId";
                }
                var script = string.Format(@"SELECT agencymanagement.patients.Id as PatientId, agencymanagement.patients.FirstName as FirstName, agencymanagement.patients.LastName as LastName, agencymanagement.patients.PrimaryInsurance as InsuranceId , oasisc.{0}.Id  as Id , oasisc.{0}.VersionNumber as VersionNumber, oasisc.{0}.AssessmentDate as AssessmentDate , oasisc.{0}.Modified as Modified , oasisc.{0}.ExportedDate as ExportedDate , oasisc.{0}.EpisodeId as EpisodeId, " +
                  "agencymanagement.patientepisodes.schedule, agencymanagement.patientepisodes.EndDate as EndDate, agencymanagement.patientepisodes.StartDate as StartDate  " +
                  "FROM oasisc.{0} INNER JOIN agencymanagement.patients ON oasisc.{0}.PatientId = agencymanagement.patients.Id INNER JOIN agencymanagement.patientepisodes ON  oasisc.{0}.EpisodeId = agencymanagement.patientepisodes.Id " +
                  "WHERE oasisc.{0}.AgencyId = @agencyid  AND ( agencymanagement.patients.Status = 1 OR agencymanagement.patients.Status = 2 ) {1} {2} AND agencymanagement.patients.IsDeprecated = 0 AND agencymanagement.patientepisodes.IsDischarged = 0 AND agencymanagement.patientepisodes.IsActive = 1 AND oasisc.{0}.Status=@status ", table, !branchId.IsEmpty() ? " AND agencymanagement.patients.AgencyLocationId = @branchId " : string.Empty,insurance);

                using (var cmd = new FluentCommand<AssessmentExport>(script))
                {
                    assessments = cmd.SetConnection("AgencyManagementConnectionString")
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .AddString("insuranceId", insuranceId)
                       .SetMap(reader => new AssessmentExport
                       {
                           AssessmentId = reader.GetGuid("Id"),
                           PatientId = reader.GetGuid("PatientId"),
                           EpisodeId = reader.GetGuid("EpisodeId"),
                           AssessmentDate = reader.GetDateTime("AssessmentDate"),
                           EpisodeStartDate = reader.GetDateTime("StartDate"),
                           EpisodeEndDate = reader.GetDateTime("EndDate"),
                           AssessmentName = name,
                           AssessmentType = type,
                           EpisodeData = reader.GetString("Schedule"),
                           CorrectionNumber = reader.GetInt("VersionNumber"),
                           PatientName = string.Format("{1}, {0}", reader.GetString("FirstName").ToUpperCase(), reader.GetString("LastName").ToUpperCase()),
                           InsuranceId = reader.GetIntNullable("InsuranceId") != null ? reader.GetInt("InsuranceId") : -1,
                           ExportedDate = reader.GetDateTime("ExportedDate").Date > DateTime.MinValue ? reader.GetDateTime("ExportedDate") : reader.GetDateTime("Modified")

                       }).AsList();
                }
            }
            return assessments;
        }

        public static IList<Assessment> FindMany(this SimpleRepository repository, Guid agencyId, Guid patientId, string assessmentType)
        {
            IList<Assessment> assessments = new List<Assessment>();
            switch (assessmentType)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                    assessments = repository.Find<StartOfCareAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    assessments = repository.Find<ResumptionofCareAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    assessments = repository.Find<FollowUpAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    assessments = repository.Find<RecertificationAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "TransferInPatientNotDischarged":
                    assessments = repository.Find<TransferNotDischargedAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "OASISCTransferDischarge":
                case "TransferInPatientDischarged":
                    assessments = repository.Find<TransferDischargeAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    assessments = repository.Find<DeathAtHomeAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    assessments = repository.Find<DischargeFromAgencyAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "SNAssessment":
                case "NonOasisStartOfCare":
                case "NonOASISStartofCare":
                    assessments = repository.Find<NonOasisStartOfCareAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "SNAssessmentRecert":
                case "NonOasisRecertification":
                case "NonOASISRecertification":
                    assessments = repository.Find<NonOasisRecertificationAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    assessments = repository.Find<NonOasisDischargeAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
                    break;
                default:
                    break;
            }
            return assessments;
        }
    }
}
