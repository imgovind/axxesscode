﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;

    public class PlanofCare
    {
        public PlanofCare()
        {
            this.Questions = new List<Question>();
        }
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid AssessmentId { get; set; }
        public string AssessmentType { get; set; }
        public int Status { get; set; }
        public string Data { get; set; }
        public string PhysicianData { get; set; }
        public long OrderNumber { get; set; }
        public DateTime Created { get; set; }
        public DateTime SentDate { get; set; }
        public string PhysicianSignatureText { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime Modified { get; set; }
        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }
        public bool IsDeprecated { get; set; }
        public bool IsNonOasis { get; set; }

        [SubSonicIgnore]
        public string AgencyData { get; set; }

        [SubSonicIgnore]
        public string PatientData { get; set; }

        [SubSonicIgnore]
        public string MedicationProfileData { get; set; }

        [SubSonicIgnore]
        public List<Question> Questions { get; set; }

        [SubSonicIgnore]
        public string PatientName { get; set; }

        [SubSonicIgnore]
        public string PhysicianName { get; set; }

        [SubSonicIgnore]
        public string EpisodeStart { get; set; }

        [SubSonicIgnore]
        public string EpisodeEnd { get; set; }

        [SubSonicIgnore]
        public string OrderDateFormatted { get; set; }
        [SubSonicIgnore]
        public string ReceivedDateFormatted { get { return this.ReceivedDate.IsValid() && this.ReceivedDate > DateTime.MinValue ? this.ReceivedDate.ToString("MM/dd/yyyy") : string.Empty; } }
        [SubSonicIgnore]
        public string SentDateFormatted { get { return this.SentDate.IsValid() && this.SentDate > DateTime.MinValue ? this.SentDate.ToString("MM/dd/yyyy") : string.Empty; } }
        [SubSonicIgnore]
        public List<String> PdfPages { get; set; }

        public override string ToString()
        {
            return string.Format("PlanofCare: AgencyId: {0}  EpisodeId: {1}  PatientId: {2}  Assessment Type: {3}", 
                this.AgencyId, this.EpisodeId, this.PatientId, this.AssessmentType);
        }
    }
}
