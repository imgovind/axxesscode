﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Domain;

    public interface IPlanofCareRepository
    {
        bool Add(PlanofCare planofCare);
        bool Update(PlanofCare planofCare);
        PlanofCare Get(Guid id);
        PlanofCare Get(Guid agencyId, Guid eventId);
        PlanofCare Get(Guid agencyId, Guid assessmentId, string assessmentType);
        PlanofCare Get(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        bool MarkAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated);
        bool ReassignUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
        List<PlanofCare> GetPlanofCareByStatus(Guid agencyId, int status);
        List<PlanofCare> GetPlanOfCareOrders(Guid agencyId);
        List<PlanofCare> GetPatientPlanOfCare(Guid agencyId, Guid patientId);

        PlanofCareStandAlone GetStandAlone(Guid agencyId, Guid id);
        PlanofCareStandAlone GetStandAlone(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        bool UpdateStandAlone(PlanofCareStandAlone planOfCare);
        bool AddStandAlone(PlanofCareStandAlone planofCare);
        List<PlanofCareStandAlone> GetPatientPlanOfCareStandAlone(Guid agencyId, Guid patientId);
        List<PlanofCareStandAlone> GetPlanofCareStandAloneByStatus(Guid agencyId, int status);
        List<PlanofCareStandAlone> GetPendingSignaturePlanofCaresStandAlone(Guid agencyId, string orderIds);
        List<PlanofCareStandAlone> GetPlanofCaresStandAloneByStatus(Guid agencyId, int status, string orderIds);
        List<PlanofCareStandAlone> GetPlanofCaresStandAlones(Guid agencyId, string orderIds);
        List<PlanofCareStandAlone> GetPatientPlanofCaresStandAlones(Guid agencyId, Guid patientId, string orderIds);
        bool MarkStandAloneAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated);

        List<PlanofCare> GetByPhysicianId(List<Guid> physicianIdentifiers, int status);
        List<PlanofCareStandAlone> GetStandAloneByPhysicianId(List<Guid> physicianIdentifiers, int status);
        List<PlanofCare> GetPlanofCares(Guid agencyId, int status, string orderIds);
        List<PlanofCare> GetPlanofCares(Guid agencyId, string orderIds);
        List<PlanofCare> GetPatientPlanofCares(Guid agencyId, Guid patientId, string orderIds);
        List<PlanofCare> GetPendingSignaturePlanofCares(Guid agencyId, string orderIds);

        List<PlanofCare> GetAllPlanOfCareOrders();
        List<PlanofCareStandAlone> GetAllPlanofCareStandAlones();

    }
}
