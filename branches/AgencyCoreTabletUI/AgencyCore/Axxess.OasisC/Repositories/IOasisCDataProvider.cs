﻿namespace Axxess.OasisC.Repositories
{
    using System;

    public interface IOasisCDataProvider
    {
        IPlanofCareRepository PlanofCareRepository
        {
            get;
        }

        IAssessmentRepository OasisAssessmentRepository
        {
            get;
        }

        ICachedDataRepository CachedDataRepository
        {
            get;
        }
    }
}
