/**
* Vertical tabs, aka drawers
* @author Bevan Rudge drupal.geek.nz
* @license GPL 2
*/

$(function() {
//    /* Activate vertical tabs */
//   
//    $('.vertical-tabs > ul').tabs({
//        fxSlide: true,
//        fxSpeed: 'fast'
//    });
//   
//    /* Set the height of the container to a fixed size */
//    $('.vertical-tabs').css('backgroundColor', '#F8F8F8');

$(function() {
    $("#dischargeTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#dischargeTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
    
     $("#editDischargeTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#editDischargeTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
    
    $("#deathTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#deathTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left'); 
    
    $("#editDeathTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#editDeathTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left'); 
    
    $("#transferInPatientNotDischargedTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#transferInPatientNotDischargedTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left'); 
    
    $("#editTransferInPatientNotDischargedTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#editTransferInPatientNotDischargedTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
    
    $("#transferInPatientDischargedTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#transferInPatientDischargedTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left'); 
    
    $("#editTransferInPatientDischargedTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#editTransferInPatientDischargedTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left'); 
    
    $("#followUpTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#followUpTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left'); 
    
    $("#editFollowUpTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#editFollowUpTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');     
    
    $("#recertificationTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#recertificationTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left'); 
    
    $("#editRecertificationTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#editRecertificationTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');    
    
    $("#rocTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $("#rocTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');  
    
 
    
    $(".tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    $(".tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
});
