﻿var ROC = {
    _ROCId: "",
    _patientId: "",
    _EpisodeId: "",
    GetId: function() {
        return ROC._patientId;
    },
    SetId: function(patientId) {
        ROC._patientId = patientId;
    },
    GetROCId: function() {
        return ROC._ROCId;
    },
    SetROCId: function(ROCId) {
        ROC._ROCId = ROCId;
    },
    GetSOCEpisodeId: function() {
        return ROC._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        ROC._EpisodeId = EpisodeId;
    },
    Init: function() {

        $("input[name=ResumptionOfCare_M1000InpatientFacilitiesNone][type=checkbox]").click(function() {

            if ($(this).is(':checked')) {

                $("#roc_M1005").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });

                $("#roc_M1010").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#roc_M1012").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#roc_M1005").find(':input').each(function() { $(this).val(''); });
                $('input[name=ResumptionOfCare_M1005InpatientDischargeDateUnknown]').attr('checked', false);
                $("#roc_M1010").find(':input').each(function() { $(this).val(''); });
                $("#roc_M1012").find(':input').each(function() { $(this).val(''); });


            }
            else {
                $("#roc_M1005").unblock();
                $("#roc_M1010").unblock();
                $("#roc_M1012").unblock();
            }
        });

        $("input[name=ResumptionOfCare_M1300PressureUlcerAssessment]").click(function() {
            if ($(this).val() == "00") {

                $("#roc_M1302").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=ResumptionOfCare_M1302RiskDevelopingPressureUlcers ]").attr('checked', false);

            }
            else {
                $("#roc_M1302").unblock();
            }
        });

        $("input[name=ResumptionOfCare_M1306UnhealedPressureUlcers]").click(function() {
            if ($(this).val() == 0) {


                $("#roc_M1308").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#roc_M13010_12_14").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#roc_M1320").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });

                $("#roc_M1308").find(':input').each(function() { $(this).val(''); });
                $("#roc_M13010_12_14").find(':input').each(function() { $(this).val(''); });
                $("input[name=ResumptionOfCare_M1320MostProblematicPressureUlcerStatus ]").attr('checked', false);
            }
            else if ($(this).val() == 1) {

                $("#roc_M1308").unblock();
                $("#roc_M13010_12_14").unblock();
                $("#roc_M1320").unblock();
            }
        });

        $("input[name=ResumptionOfCare_M1330StasisUlcer]").click(function() {
            if ($(this).val() == "00") {
                $("#roc_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=ResumptionOfCare_M1332CurrentNumberStasisUlcer ]").attr('checked', false);
                $("input[name=ResumptionOfCare_M1334StasisUlcerStatus ]").attr('checked', false);
            }
            else {
                $("#roc_M1332AndM1334").unblock();
            }
        });


        $("input[name=ResumptionOfCare_M1340SurgicalWound]").click(function() {
            if ($(this).val() == "00") {
                $("#roc_M1342").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });

                $("input[name=ResumptionOfCare_M1342SurgicalWoundStatus]").attr('checked', false);

            }
            else {
                $("#roc_M1342").unblock();
            }
        });

        $("input[name=ResumptionOfCare_M1610UrinaryIncontinence]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02") {
                $("#roc_M1615").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=ResumptionOfCare_M1615UrinaryIncontinenceOccur]").attr('checked', false);
            }
            else {
                $("#roc_M1615").unblock();
            }
        });


        $("input[name=ResumptionOfCare_M2000DrugRegimenReview]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "01") {

                $("#roc_M2002").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=ResumptionOfCare_M2002MedicationFollowup ]").attr('checked', false);

                $("#roc_M2010").unblock();
                $("#roc_M2020").unblock();
                $("#roc_M2030").unblock();

            }
            else if ($(this).val() == "NA") {


                $("#roc_M2002").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#roc_M2010").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#roc_M2020").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#roc_M2030").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=ResumptionOfCare_M2002MedicationFollowup ]").attr('checked', false);
                $("input[name=ResumptionOfCare_M2010PatientOrCaregiverHighRiskDrugEducation ]").attr('checked', false);
                $("input[name=ResumptionOfCare_M2020ManagementOfOralMedications ]").attr('checked', false);
                $("input[name=ResumptionOfCare_M2030ManagementOfInjectableMedications ]").attr('checked', false);

            }
            else {
                $("#roc_M2002").unblock();
                $("#roc_M2010").unblock();
                $("#roc_M2020").unblock();
                $("#roc_M2030").unblock();
            }
        });


    }
    ,
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {

                    var actionType = control.val();
                    if (actionType == "Save/Continue") {
                        $("input[name='ResumptionOfCare_Id']").val(resultObject.Assessment.Id);
                        $("input[name='ResumptionOfCare_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='ResumptionOfCare_Action']").val('Edit');
                        Oasis.NextTab("#rocTabs.tabs");
                    }
                    else if (actionType == "Save/Exit") {
                        Oasis.Close(control);
                        Oasis.RebindActivity();
                    }
                    else if (actionType == "Save") {
                        $("input[name='ResumptionOfCare_Id']").val(resultObject.Assessment.Id);
                        $("input[name='ResumptionOfCare_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='ResumptionOfCare_Action']").val('Edit');
                    }

                }
                else {
                    alert(resultObject.errorMessage);
                }


            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control) {
        var form = control.closest("form");
        form.validate();
        ROC.HandlerHelper(form, control);

    },
    Validate: function(id) {
        OasisValidation.Validate(id, "ResumptionOfCare");
    },
    loadRoc: function(id, patientId, assessmentType) {
        $('#resumptionOfCareResult').load('Oasis/NewROCContent', { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#resumptionOfCareResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#roc');
            }
            else if (textStatus == "success") {
                JQD.open_window('#roc');
                $("#rocTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
                $("#rocTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
                $("#rocTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                    ROC.loadRocParts(event, ui);
                });
                ROC.Init();
            }
        }
);
    }
    ,
    loadRocParts: function(event, ui) {
        $($(ui.tab).attr('href')).load('Oasis/NewROCPartContent', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
            }
        }
);
    }
    ,
    ResumptionOfCare: function() {
        var id = Oasis.GetId();
        var data = 'id=' + id;
        $("#roc").clearForm();
        $("#roc div").unblock();
        Oasis.BlockAssessmentType();
        $.ajax({
            url: '/Patient/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var patient = eval(result);
                var patientName = (patient.FirstName !== null ? patient.FirstName : "") + " " + (patient.LastName != null ? patient.LastName : "");
                $("#ResumptionofCareTitle").text("Resumption Of Care - " + patientName);
                $("input[name='ResumptionOfCare_Id']").val("");
                $("input[name='ResumptionOfCare_PatientGuid']").val(id);
                $("input[name='ResumptionOfCare_Action']").val('New');
                $("#ResumptionOfCare_M0040FirstName").val(patient.FirstName);
                $("#ResumptionOfCare_M0040MI").val(patient.MiddleInitial);
                $("#ResumptionOfCare_M0040LastName").val(patient.LastName);
                $("#ResumptionOfCare_M0020PatientIdNumber").val(patient.PatientIdNumber);
                $("#ResumptionOfCare_M0030SocDate").val(patient.StartOfCareDateFormatted);
                $("#ResumptionOfCare_M0050PatientState").val(patient.AddressStateCode);
                $("#ResumptionOfCare_M0060PatientZipCode").val(patient.AddressZipCode);
                $("#ResumptionOfCare_M0063PatientMedicareNumber").val(patient.MedicareNumber);
                $("#ResumptionOfCare_M0064PatientSSN").val(patient.SSN);
                $("#ResumptionOfCare_M0065PatientMedicaidNumber").val(patient.MedicaidNumber);
                $("#ResumptionOfCare_M0066PatientDoB").val(patient.DOBFormatted);
                $('input[name=ResumptionOfCare_M0069Gender][value=' + patient.Gender.toString() + ']').attr('checked', true);
                $("input[name='ResumptionOfCare_M0100AssessmentType'][value='03']").attr('checked', true);
                if (patient.EthnicRace !== null) {
                    var EthnicRaceArray = (patient.EthnicRace).split(';');
                    var i = 0;
                    for (i = 0; i < EthnicRaceArray.length; i++) {

                        if (EthnicRaceArray[i] == 1) {
                            $('input[name=ResumptionOfCare_M0140RaceAMorAN][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 2) {
                            $('input[name=ResumptionOfCare_M0140RaceAsia][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 3) {
                            $('input[name=ResumptionOfCare_M0140RaceBalck][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 4) {
                            $('input[name=ResumptionOfCare_M0140RaceHispanicOrLatino][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 5) {
                            $('input[name=ResumptionOfCare_M0140RaceNHOrPI][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 6) {
                            $('input[name=ResumptionOfCare_M0140RaceWhite][value=1]').attr('checked', true);
                        }
                    }
                }
                if (patient.PaymentSource !== null) {
                    var PaymentSourceArray = (patient.PaymentSource).split(';');
                    var i = 0;
                    for (i = 0; i < PaymentSourceArray.length; i++) {
                        if (PaymentSourceArray[i] == 0) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourceNone][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 1) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourceMCREFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 2) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourceMCREHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 3) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourceMCAIDFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 4) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourceMACIDHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 5) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourceWRKCOMP][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 6) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourceTITLPRO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 7) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourceOTHGOVT][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 8) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourcePRVINS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 9) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourcePRVHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 10) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourceSelfPay][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 11) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourceUnknown][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 12) {
                            $('input[name=ResumptionOfCare_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                            $("#ResumptionOfCare_M0150PaymentSourceOther").val(patient.OtherPaymentSource);
                        }
                    }
                }
            }
        });
    }
    ,
    EditResumptionOfCare: function(id, patientId, assessmentType) {
        // var patientId = Oasis.GetId();
        $("#roc").clearForm();
        Oasis.BlockAssessmentType();
        var data = 'Id=' + id + "&assessmentType=" + assessmentType;
        $.ajax({
            url: '/Oasis/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                getNewRows(result);
            }
        });
        getNewRows = function(result) {
            var patient = eval(result);
            var firstName = result["M0040FirstName"] != null && result["M0040FirstName"] != undefined ? result["M0040FirstName"].Answer : "";
            var lastName = result["M0040LastName"] != null && result["M0040LastName"] != undefined ? result["M0040LastName"].Answer : "";
            $("#ResumptionOfCareTitle").text("Edit Start Of Care - " + firstName + " " + lastName);
            $("input[name='ResumptionOfCare_Id']").val(id);
            ROC.SetROCId(id);
            $("input[name='ResumptionOfCare_Action']").val('Edit');
            $("input[name='ResumptionOfCare_PatientGuid']").val(patientId);
            $("#ResumptionOfCare_M0010CertificationNumber").val(result["M0010CertificationNumber"] != null && result["M0010CertificationNumber"] != undefined ? result["M0010CertificationNumber"].Answer : "");
            $("#ResumptionOfCare_M0014BranchState").val(result["M0014BranchState"] != null && result["M0014BranchState"] != undefined ? result["M0014BranchState"].Answer : "");
            $("#ResumptionOfCare_M0016BranchId").val(result["M0016BranchId"] != null && result["M0016BranchId"] != undefined ? result["M0016BranchId"].Answer : "");
            var nationalProviderIdUK = result["M0018NationalProviderIdUnknown"];
            if (nationalProviderIdUK != null && nationalProviderIdUK != undefined) {
                if (nationalProviderIdUK.Answer == 1) {

                    $('input[name=ResumptionOfCare_M0018NationalProviderIdUnknown][value=1]').attr('checked', true);
                    $("#ResumptionOfCare_M0018NationalProviderId").val(" ");
                }
                else {
                    $('input[name=ResumptionOfCare_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                    $("#ResumptionOfCare_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
                }
            }
            else {
                $('input[name=ResumptionOfCare_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                $("#ResumptionOfCare_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
            }
            $("#ResumptionOfCare_M0020PatientIdNumber").val(result["M0020PatientIdNumber"] != null && result["M0020PatientIdNumber"] != undefined ? result["M0020PatientIdNumber"].Answer : "");
            $("#ResumptionOfCare_M0030SocDate").val(result["M0030SocDate"] != null && result["M0030SocDate"] != undefined ? result["M0030SocDate"].Answer : "");
            $("#ResumptionOfCare_GenericEpisodeStartDate").val(result["GenericEpisodeStartDate"] != null && result["GenericEpisodeStartDate"] != undefined ? result["GenericEpisodeStartDate"].Answer : "");
            var rOCDateNotApplicable = result["M0032ROCDateNotApplicable"];
            if (rOCDateNotApplicable != null && rOCDateNotApplicable != undefined) {
                if (rOCDateNotApplicable.Answer == 1) {

                    $('input[name=ResumptionOfCare_M0032ROCDateNotApplicable][value=1]').attr('checked', true);
                    $("#ResumptionOfCare_M0032ROCDate").val(" ");
                }
                else {
                    $('input[name=ResumptionOfCare_M0032ROCDateNotApplicable][value=1]').attr('checked', false);
                    $("#ResumptionOfCare_M0032ROCDate").val(result["M0032ROCDate"] != null && result["M0032ROCDate"] != undefined ? result["M0032ROCDate"].Answer : "");
                }
            }
            else {
                $('input[name=ResumptionOfCare_M0032ROCDateNotApplicable][value=1]').attr('checked', false);
                $("#ResumptionOfCare_M0032ROCDate").val(result["M0032ROCDate"] != null && result["M0032ROCDate"] != undefined ? result["M0032ROCDate"].Answer : "");
            }
            $("#ResumptionOfCare_M0040FirstName").val(firstName);
            $("#ResumptionOfCare_M0040MI").val(result["M0040MI"] != null && result["M0040MI"] != undefined ? result["M0040MI"].Answer : "");
            $("#ResumptionOfCare_M0040LastName").val(lastName);
            $("#ResumptionOfCare_M0050PatientState").val(result["M0050PatientState"] != null && result["M0050PatientState"] != undefined ? result["M0050PatientState"].Answer : "");
            $("#ResumptionOfCare_M0060PatientZipCode").val(result["M0060PatientZipCode"] != null && result["M0060PatientZipCode"] != undefined ? result["M0060PatientZipCode"].Answer : "");
            var patientMedicareNumberUK = result["M0063PatientMedicareNumberUnknown"];
            if (patientMedicareNumberUK != null && patientMedicareNumberUK != undefined) {
                if (patientMedicareNumberUK.Answer == 1) {

                    $('input[name=ResumptionOfCare_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', true);
                    $("#ResumptionOfCare_M0063PatientMedicareNumber").val(" ");
                }
                else {
                    $('input[name=ResumptionOfCare_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', false);
                    $("#ResumptionOfCare_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
                }
            }
            else {
                $('input[name=ResumptionOfCare_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', false);
                $("#ResumptionOfCare_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
            }
            var patientSSNUK = result["M0064PatientSSNUnknown"];
            if (patientSSNUK != null && patientSSNUK != undefined) {
                if (patientSSNUK.Answer == 1) {

                    $('input[name=ResumptionOfCare_M0064PatientSSNUnknown][value=1]').attr('checked', true);
                    $("#ResumptionOfCare_M0064PatientSSN").val(" ");
                }
                else {
                    $('input[name=ResumptionOfCare_M0064PatientSSNUnknown][value=1]').attr('checked', false);
                    $("#ResumptionOfCare_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
                }
            }
            else {
                $('input[name=ResumptionOfCare_M0064PatientSSNUnknown][value=1]').attr('checked', false);
                $("#ResumptionOfCare_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
            }
            var patientMedicaidNumberUK = result["M0065PatientMedicaidNumberUnknown"];
            if (patientMedicaidNumberUK != null && patientMedicaidNumberUK != undefined) {
                if (patientMedicaidNumberUK.Answer == 1) {

                    $('input[name=ResumptionOfCare_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', true);
                    $("#ResumptionOfCare_M0065PatientMedicaidNumber").val(" ");
                }
                else {
                    $('input[name=ResumptionOfCare_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', false);
                    $("#ResumptionOfCare_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
                }
            }
            else {
                $('input[name=ResumptionOfCare_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', false);
                $("#ResumptionOfCare_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
            }
            $("#ResumptionOfCare_M0066PatientDoB").val(result["M0066PatientDoB"] != null && result["M0066PatientDoB"] != undefined ? result["M0066PatientDoB"].Answer : "");
            $('input[name=ResumptionOfCare_M0069Gender][value=' + (result["M0069Gender"] != null && result["M0069Gender"] != undefined ? result["M0069Gender"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M0080DisciplinePerson][value=' + (result["M0080DisciplinePerson"] != null && result["M0080DisciplinePerson"] != undefined ? result["M0080DisciplinePerson"].Answer : "") + ']').attr('checked', true);
            $("#ResumptionOfCare_M0090AssessmentCompleted").val(result["M0090AssessmentCompleted"] != null && result["M0090AssessmentCompleted"] != undefined ? result["M0090AssessmentCompleted"].Answer : "");
            $("input[name='ResumptionOfCare_M0100AssessmentType'][value='03']").attr('checked', true);
            var physicianOrderedDateNotApplicable = result["M0102PhysicianOrderedDateNotApplicable"];
            if (physicianOrderedDateNotApplicable != null && physicianOrderedDateNotApplicable != undefined) {
                if (physicianOrderedDateNotApplicable.Answer == 1) {

                    $('input[name=ResumptionOfCare_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', true);
                    $("#ResumptionOfCare_M0102PhysicianOrderedDate").val(" ");
                }
                else {
                    $('input[name=ResumptionOfCare_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                    $("#ResumptionOfCare_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
                }
            }
            else {
                $('input[name=ResumptionOfCare_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                $("#ResumptionOfCare_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
            }

            $("#ResumptionOfCare_M0104ReferralDate").val(result["M0104ReferralDate"] != null && result["M0104ReferralDate"] != undefined ? result["M0104ReferralDate"].Answer : "");

            $('input[name=ResumptionOfCare_M0110EpisodeTiming][value=' + (result["M0110EpisodeTiming"] != null && result["M0110EpisodeTiming"] != undefined ? result["M0110EpisodeTiming"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0140RaceAMorAN][value=' + (result["M0140RaceAMorAN"] != null && result["M0140RaceAMorAN"] != undefined ? result["M0140RaceAMorAN"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0140RaceAsia][value=' + (result["M0140RaceAsia"] != null && result["M0140RaceAsia"] != undefined ? result["M0140RaceAsia"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0140RaceBalck][value=' + (result["M0140RaceBalck"] != null && result["M0140RaceBalck"] != undefined ? result["M0140RaceBalck"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0140RaceHispanicOrLatino][value=' + (result["M0140RaceHispanicOrLatino"] != null && result["M0140RaceHispanicOrLatino"] != undefined ? result["M0140RaceHispanicOrLatino"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0140RaceNHOrPI][value=' + (result["M0140RaceNHOrPI"] != null && result["M0140RaceNHOrPI"] != undefined ? result["M0140RaceNHOrPI"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0140RaceWhite][value=' + (result["M0140RaceWhite"] != null && result["M0140RaceWhite"] != undefined ? result["M0140RaceWhite"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0150PaymentSourceNone][value=' + (result["M0150PaymentSourceNone"] != null && result["M0150PaymentSourceNone"] != undefined ? result["M0150PaymentSourceNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0150PaymentSourceMCREFFS][value=' + (result["M0150PaymentSourceMCREFFS"] != null && result["M0150PaymentSourceMCREFFS"] != undefined ? result["M0150PaymentSourceMCREFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0150PaymentSourceMCREHMO][value=' + (result["M0150PaymentSourceMCREHMO"] != null && result["M0150PaymentSourceMCREHMO"] != undefined ? result["M0150PaymentSourceMCREHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0150PaymentSourceMCAIDFFS][value=' + (result["M0150PaymentSourceMCAIDFFS"] != null && result["M0150PaymentSourceMCAIDFFS"] != undefined ? result["M0150PaymentSourceMCAIDFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0150PaymentSourceMACIDHMO][value=' + (result["M0150PaymentSourceMACIDHMO"] != null && result["M0150PaymentSourceMACIDHMO"] != undefined ? result["M0150PaymentSourceMACIDHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0150PaymentSourceWRKCOMP][value=' + (result["M0150PaymentSourceWRKCOMP"] != null && result["M0150PaymentSourceWRKCOMP"] != undefined ? result["M0150PaymentSourceWRKCOMP"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0150PaymentSourceTITLPRO][value=' + (result["M0150PaymentSourceTITLPRO"] != null && result["M0150PaymentSourceTITLPRO"] != undefined ? result["M0150PaymentSourceTITLPRO"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0150PaymentSourceOTHGOVT][value=' + (result["M0150PaymentSourceOTHGOVT"] != null && result["M0150PaymentSourceOTHGOVT"] != undefined ? result["M0150PaymentSourceOTHGOVT"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0150PaymentSourcePRVINS][value=' + (result["M0150PaymentSourcePRVINS"] != null && result["M0150PaymentSourcePRVINS"] != undefined ? result["M0150PaymentSourcePRVINS"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0150PaymentSourcePRVHMO][value=' + (result["M0150PaymentSourcePRVHMO"] != null && result["M0150PaymentSourcePRVHMO"] != undefined ? result["M0150PaymentSourcePRVHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M0150PaymentSourceSelfPay][value=' + (result["M0150PaymentSourceSelfPay"] != null && result["M0150PaymentSourceSelfPay"] != undefined ? result["M0150PaymentSourceSelfPay"].Answer : "") + ']').attr('checked', true);

            var paymentSourceOtherSRS = result["M0150PaymentSourceOtherSRS"];
            if (paymentSourceOtherSRS != null && paymentSourceOtherSRS != undefined) {
                if (paymentSourceOtherSRS.Answer == 1) {

                    $('input[name=ResumptionOfCare_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                    $("#ResumptionOfCare_M0150PaymentSourceOther").val(result["M0150PaymentSourceOther"] != null && result["M0150PaymentSourceOther"] != undefined ? result["M0150PaymentSourceOther"].Answer : "");

                }
                else {
                    $('input[name=ResumptionOfCare_M0150PaymentSourceOtherSRS][value=1]').attr('checked', false);

                }
            }

            $('input[name=ResumptionOfCare_M0150PaymentSourceUnknown][value=' + (result["M0150PaymentSourceUnknown"] != null && result["M0150PaymentSourceUnknown"] != undefined ? result["M0150PaymentSourceUnknown"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1000InpatientFacilitiesLTC][value=' + (result["M1000InpatientFacilitiesLTC"] != null && result["M1000InpatientFacilitiesLTC"] != undefined ? result["M1000InpatientFacilitiesLTC"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1000InpatientFacilitiesSNF][value=' + (result["M1000InpatientFacilitiesSNF"] != null && result["M1000InpatientFacilitiesSNF"] != undefined ? result["M1000InpatientFacilitiesSNF"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1000InpatientFacilitiesIPPS][value=' + (result["M1000InpatientFacilitiesIPPS"] != null && result["M1000InpatientFacilitiesIPPS"] != undefined ? result["M1000InpatientFacilitiesIPPS"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1000InpatientFacilitiesLTCH][value=' + (result["M1000InpatientFacilitiesLTCH"] != null && result["M1000InpatientFacilitiesLTCH"] != undefined ? result["M1000InpatientFacilitiesLTCH"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1000InpatientFacilitiesIRF][value=' + (result["M1000InpatientFacilitiesIRF"] != null && result["M1000InpatientFacilitiesIRF"] != undefined ? result["M1000InpatientFacilitiesIRF"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1000InpatientFacilitiesPhych][value=' + (result["M1000InpatientFacilitiesPhych"] != null && result["M1000InpatientFacilitiesPhych"] != undefined ? result["M1000InpatientFacilitiesPhych"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1000InpatientFacilitiesOTHR][value=' + (result["M1000InpatientFacilitiesOTHR"] != null && result["M1000InpatientFacilitiesOTHR"] != undefined ? result["M1000InpatientFacilitiesOTHR"].Answer : "") + ']').attr('checked', true);

            var inpatientFacilitiesOTHR = result["M1000InpatientFacilitiesOTHR"];
            if (inpatientFacilitiesOTHR != null && inpatientFacilitiesOTHR != undefined) {
                if (inpatientFacilitiesOTHR.Answer == 1) {

                    $('input[name=ResumptionOfCare_M1000InpatientFacilitiesOTHR][value=1]').attr('checked', true);
                    $("#ResumptionOfCare_M1000InpatientFacilitiesOther").val(result["M1000InpatientFacilitiesOther"] != null && result["M1000InpatientFacilitiesOther"] != undefined ? result["M1000InpatientFacilitiesOther"].Answer : "");

                }
                else {
                    $('input[name=ResumptionOfCare_M1000InpatientFacilitiesOTHR][value=1]').attr('checked', false);

                }
            }
            $('input[name=ResumptionOfCare_M1000InpatientFacilitiesNone][value=' + (result["M1000InpatientFacilitiesNone"] != null && result["M1000InpatientFacilitiesNone"] != undefined ? result["M1000InpatientFacilitiesNone"].Answer : "") + ']').attr('checked', true);

            var inpatientFacilitiesNone = result["M1000InpatientFacilitiesNone"];

            if (inpatientFacilitiesNone == 1) {

                $("#roc_M1005").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });

                $("#roc_M1012").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#roc_M1010").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });

            }
            else {
                var inpatientDischargeDateUnknown = result["M1005InpatientDischargeDateUnknown"];
                if (inpatientDischargeDateUnknown != null && inpatientDischargeDateUnknown != undefined && inpatientDischargeDateUnknown.Answer != null) {
                    if (inpatientDischargeDateUnknown.Answer == 1) {
                        $('input[name=ResumptionOfCare_M1005InpatientDischargeDateUnknown][value=1]').attr('checked', true);
                    }
                    else {
                        $('input[name=ResumptionOfCare_M1005InpatientDischargeDateUnknown][value=1]').attr('checked', false);
                        $("#ResumptionOfCare_M1005InpatientDischargeDate").val(result["M1005InpatientDischargeDate"] != null && result["M1005InpatientDischargeDate"] != undefined ? result["M1005InpatientDischargeDate"].Answer : "");
                    }
                }
                var inpatientFacilityDiagnosisCode1 = result["M1010InpatientFacilityDiagnosisCode1"];
                if (inpatientFacilityDiagnosisCode1 != null && inpatientFacilityDiagnosisCode1 != undefined && inpatientFacilityDiagnosisCode1.Answer != null) {
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosisCode1]').val(result["M1010InpatientFacilityDiagnosisCode1"] != null && result["M1010InpatientFacilityDiagnosisCode1"] != undefined ? result["M1010InpatientFacilityDiagnosisCode1"].Answer : "");
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosis1]').val(result["M1010InpatientFacilityDiagnosis1"] != null && result["M1010InpatientFacilityDiagnosis1"] != undefined ? result["M1010InpatientFacilityDiagnosis1"].Answer : "");
                }

                var inpatientFacilityDiagnosisCode2 = result["M1010InpatientFacilityDiagnosisCode2"];
                if (inpatientFacilityDiagnosisCode2 != null && inpatientFacilityDiagnosisCode2 != undefined && inpatientFacilityDiagnosisCode2.Answer != null) {
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosisCode2]').val(result["M1010InpatientFacilityDiagnosisCode2"] != null && result["M1010InpatientFacilityDiagnosisCode2"] != undefined ? result["M1010InpatientFacilityDiagnosisCode2"].Answer : "");
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosis2]').val(result["M1010InpatientFacilityDiagnosis2"] != null && result["M1010InpatientFacilityDiagnosis2"] != undefined ? result["M1010InpatientFacilityDiagnosis2"].Answer : "");
                }

                var inpatientFacilityDiagnosisCode3 = result["M1010InpatientFacilityDiagnosisCode3"];
                if (inpatientFacilityDiagnosisCode3 != null && inpatientFacilityDiagnosisCode3 != undefined && inpatientFacilityDiagnosisCode3.Answer != null) {
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosisCode3]').val(result["M1010InpatientFacilityDiagnosisCode3"] != null && result["M1010InpatientFacilityDiagnosisCode3"] != undefined ? result["M1010InpatientFacilityDiagnosisCode3"].Answer : "");
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosis3]').val(result["M1010InpatientFacilityDiagnosis3"] != null && result["M1010InpatientFacilityDiagnosis3"] != undefined ? result["M1010InpatientFacilityDiagnosis3"].Answer : "");
                }

                var inpatientFacilityDiagnosisCode4 = result["M1010InpatientFacilityDiagnosisCode4"];
                if (inpatientFacilityDiagnosisCode4 != null && inpatientFacilityDiagnosisCode4 != undefined && inpatientFacilityDiagnosisCode4.Answer != null) {
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosisCode4]').val(result["M1010InpatientFacilityDiagnosisCode4"] != null && result["M1010InpatientFacilityDiagnosisCode4"] != undefined ? result["M1010InpatientFacilityDiagnosisCode4"].Answer : "");
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosis4]').val(result["M1010InpatientFacilityDiagnosis4"] != null && result["M1010InpatientFacilityDiagnosis4"] != undefined ? result["M1010InpatientFacilityDiagnosis4"].Answer : "");
                }

                var inpatientFacilityDiagnosisCode5 = result["M1010InpatientFacilityDiagnosisCode5"];
                if (inpatientFacilityDiagnosisCode5 != null && inpatientFacilityDiagnosisCode5 != undefined && inpatientFacilityDiagnosisCode5.Answer != null) {
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosisCode5]').val(result["M1010InpatientFacilityDiagnosisCode5"] != null && result["M1010InpatientFacilityDiagnosisCode5"] != undefined ? result["M1010InpatientFacilityDiagnosisCode5"].Answer : "");
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosis5]').val(result["M1010InpatientFacilityDiagnosis5"] != null && result["M1010InpatientFacilityDiagnosis5"] != undefined ? result["M1010InpatientFacilityDiagnosis5"].Answer : "");
                }

                var inpatientFacilityDiagnosisCode6 = result["M1010InpatientFacilityDiagnosisCode6"];
                if (inpatientFacilityDiagnosisCode6 != null && inpatientFacilityDiagnosisCode6 != undefined && inpatientFacilityDiagnosisCode6.Answer != null) {
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosisCode6]').val(result["M1010InpatientFacilityDiagnosisCode6"] != null && result["M1010InpatientFacilityDiagnosisCode6"] != undefined ? result["M1010InpatientFacilityDiagnosisCode6"].Answer : "");
                    $('input[name=ResumptionOfCare_M1010InpatientFacilityDiagnosis6]').val(result["M1010InpatientFacilityDiagnosis6"] != null && result["M1010InpatientFacilityDiagnosis6"] != undefined ? result["M1010InpatientFacilityDiagnosis6"].Answer : "");
                }


                var inpatientFacilityProcedureCode1 = result["M1012InpatientFacilityProcedureCode1"];
                if (inpatientFacilityProcedureCode1 != null && inpatientFacilityProcedureCode1 != undefined && inpatientFacilityProcedureCode1.Answer != null) {
                    $('input[name=ResumptionOfCare_M1012InpatientFacilityProcedureCode1]').val(result["M1012InpatientFacilityProcedureCode1"] != null && result["M1012InpatientFacilityProcedureCode1"] != undefined ? result["M1012InpatientFacilityProcedureCode1"].Answer : "");
                    $('input[name=ResumptionOfCare_M1012InpatientFacilityProcedure1]').val(result["M1012InpatientFacilityProcedure1"] != null && result["M1012InpatientFacilityProcedure1"] != undefined ? result["M1012InpatientFacilityProcedure1"].Answer : "");
                }

                var inpatientFacilityProcedureCode2 = result["M1012InpatientFacilityProcedureCode2"];
                if (inpatientFacilityProcedureCode2 != null && inpatientFacilityProcedureCode2 != undefined && inpatientFacilityProcedureCode2.Answer != null) {
                    $('input[name=ResumptionOfCare_M1012InpatientFacilityProcedureCode2]').val(result["M1012InpatientFacilityProcedureCode2"] != null && result["M1012InpatientFacilityProcedureCode2"] != undefined ? result["M1012InpatientFacilityProcedureCode2"].Answer : "");
                    $('input[name=ResumptionOfCare_M1012InpatientFacilityProcedure2]').val(result["M1012InpatientFacilityProcedure2"] != null && result["M1012InpatientFacilityProcedure2"] != undefined ? result["M1012InpatientFacilityProcedure2"].Answer : "");
                }
                var inpatientFacilityProcedureCode3 = result["M1012InpatientFacilityProcedureCode3"];
                if (inpatientFacilityProcedureCode3 != null && inpatientFacilityProcedureCode3 != undefined && inpatientFacilityProcedureCode3.Answer != null) {
                    $('input[name=ResumptionOfCare_M1012InpatientFacilityProcedureCode3]').val(result["M1012InpatientFacilityProcedureCode3"] != null && result["M1012InpatientFacilityProcedureCode3"] != undefined ? result["M1012InpatientFacilityProcedureCode3"].Answer : "");
                    $('input[name=ResumptionOfCare_M1012InpatientFacilityProcedure3]').val(result["M1012InpatientFacilityProcedure3"] != null && result["M1012InpatientFacilityProcedure3"] != undefined ? result["M1012InpatientFacilityProcedure3"].Answer : "");
                }
                var inpatientFacilityProcedureCode4 = result["M1012InpatientFacilityProcedureCode4"];
                if (inpatientFacilityProcedureCode4 != null && inpatientFacilityProcedureCode4 != undefined && inpatientFacilityProcedureCode4.Answer != null) {
                    $('input[name=ResumptionOfCare_M1012InpatientFacilityProcedureCode4]').val(result["M1012InpatientFacilityProcedureCode4"] != null && result["M1012InpatientFacilityProcedureCode4"] != undefined ? result["M1012InpatientFacilityProcedureCode4"].Answer : "");
                    $('input[name=ResumptionOfCare_M1012InpatientFacilityProcedure4]').val(result["M1012InpatientFacilityProcedure4"] != null && result["M1012InpatientFacilityProcedure4"] != undefined ? result["M1012InpatientFacilityProcedure4"].Answer : "");
                }

                $('input[name=ResumptionOfCare_M1012InpatientFacilityProcedureCodeNotApplicable][value=' + (result["M1012InpatientFacilityProcedureCodeNotApplicable"] != null && result["M1012InpatientFacilityProcedureCodeNotApplicable"] != undefined ? result["M1012InpatientFacilityProcedureCodeNotApplicable"].Answer : "") + ']').attr('checked', true);

                $('input[name=ResumptionOfCare_M1012InpatientFacilityProcedureCodeUnknown][value=' + (result["M1012InpatientFacilityProcedureCodeUnknown"] != null && result["M1012InpatientFacilityProcedureCodeUnknown"] != undefined ? result["M1012InpatientFacilityProcedureCodeUnknown"].Answer : "") + ']').attr('checked', true);
            }



            var medicalRegimenDiagnosisCode1 = result["M1016MedicalRegimenDiagnosisCode1"];
            if (medicalRegimenDiagnosisCode1 != null && medicalRegimenDiagnosisCode1 != undefined && medicalRegimenDiagnosisCode1.Answer != null) {
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosisCode1]').val(result["M1016MedicalRegimenDiagnosisCode1"] != null && result["M1016MedicalRegimenDiagnosisCode1"] != undefined ? result["M1016MedicalRegimenDiagnosisCode1"].Answer : "");
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosis1]').val(result["M1016MedicalRegimenDiagnosis1"] != null && result["M1016MedicalRegimenDiagnosis1"] != undefined ? result["M1016MedicalRegimenDiagnosis1"].Answer : "");
            }
            var medicalRegimenDiagnosisCode2 = result["M1016MedicalRegimenDiagnosisCode2"];
            if (medicalRegimenDiagnosisCode2 != null && medicalRegimenDiagnosisCode2 != undefined && medicalRegimenDiagnosisCode2.Answer != null) {
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosisCode2]').val(result["M1016MedicalRegimenDiagnosisCode2"] != null && result["M1016MedicalRegimenDiagnosisCode2"] != undefined ? result["M1016MedicalRegimenDiagnosisCode2"].Answer : "");
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosis2]').val(result["M1016MedicalRegimenDiagnosis2"] != null && result["M1016MedicalRegimenDiagnosis2"] != undefined ? result["M1016MedicalRegimenDiagnosis2"].Answer : "");
            }
            var medicalRegimenDiagnosisCode3 = result["M1016MedicalRegimenDiagnosisCode3"];
            if (medicalRegimenDiagnosisCode3 != null && medicalRegimenDiagnosisCode3 != undefined && medicalRegimenDiagnosisCode3.Answer != null) {
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosisCode3]').val(result["M1016MedicalRegimenDiagnosisCode3"] != null && result["M1016MedicalRegimenDiagnosisCode3"] != undefined ? result["M1016MedicalRegimenDiagnosisCode3"].Answer : "");
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosis3]').val(result["M1016MedicalRegimenDiagnosis3"] != null && result["M1016MedicalRegimenDiagnosis3"] != undefined ? result["M1016MedicalRegimenDiagnosis3"].Answer : "");
            }
            var medicalRegimenDiagnosisCode4 = result["M1016MedicalRegimenDiagnosisCode4"];
            if (medicalRegimenDiagnosisCode4 != null && medicalRegimenDiagnosisCode4 != undefined && medicalRegimenDiagnosisCode4.Answer != null) {
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosisCode4]').val(result["M1016MedicalRegimenDiagnosisCode4"] != null && result["M1016MedicalRegimenDiagnosisCode4"] != undefined ? result["M1016MedicalRegimenDiagnosisCode4"].Answer : "");
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosis4]').val(result["M1016MedicalRegimenDiagnosis4"] != null && result["M1016MedicalRegimenDiagnosis4"] != undefined ? result["M1016MedicalRegimenDiagnosis4"].Answer : "");
            }
            var medicalRegimenDiagnosisCode5 = result["M1016MedicalRegimenDiagnosisCode5"];
            if (medicalRegimenDiagnosisCode5 != null && medicalRegimenDiagnosisCode5 != undefined && medicalRegimenDiagnosisCode5.Answer != null) {
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosisCode5]').val(result["M1016MedicalRegimenDiagnosisCode5"] != null && result["M1016MedicalRegimenDiagnosisCode5"] != undefined ? result["M1016MedicalRegimenDiagnosisCode5"].Answer : "");
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosis5]').val(result["M1016MedicalRegimenDiagnosis5"] != null && result["M1016MedicalRegimenDiagnosis5"] != undefined ? result["M1016MedicalRegimenDiagnosis5"].Answer : "");
            }
            var medicalRegimenDiagnosisCode6 = result["M1016MedicalRegimenDiagnosisCode6"];
            if (medicalRegimenDiagnosisCode6 != null && medicalRegimenDiagnosisCode6 != undefined && medicalRegimenDiagnosisCode6.Answer != null) {
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosisCode6]').val(result["M1016MedicalRegimenDiagnosisCode6"] != null && result["M1016MedicalRegimenDiagnosisCode6"] != undefined ? result["M1016MedicalRegimenDiagnosisCode6"].Answer : "");
                $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosis6]').val(result["M1016MedicalRegimenDiagnosis6"] != null && result["M1016MedicalRegimenDiagnosis6"] != undefined ? result["M1016MedicalRegimenDiagnosis6"].Answer : "");
            }
            $('input[name=ResumptionOfCare_M1016MedicalRegimenDiagnosisNotApplicable][value=' + (result["M1016MedicalRegimenDiagnosisNotApplicable"] != null && result["M1016MedicalRegimenDiagnosisNotApplicable"] != undefined ? result["M1016MedicalRegimenDiagnosisNotApplicable"].Answer : "") + ']').attr('checked', true);

            var ICD9M = result["M1020ICD9M"];
            if (ICD9M != null && ICD9M != undefined && ICD9M.Answer != null) {
                $("#ResumptionOfCare_M1020ICD9M").val(result["M1020ICD9M"] != null && result["M1020ICD9M"] != undefined ? result["M1020ICD9M"].Answer : "");
                $("#ResumptionOfCare_M1020PrimaryDiagnosis").val(result["M1020PrimaryDiagnosis"] != null && result["M1020PrimaryDiagnosis"] != undefined ? result["M1020PrimaryDiagnosis"].Answer : "");
                $("#ResumptionOfCare_M1020SymptomControlRating").val(result["M1020SymptomControlRating"] != null && result["M1020SymptomControlRating"] != undefined ? result["M1020SymptomControlRating"].Answer : "");
            }
            var ICD9MA3 = result["M1024ICD9MA3"];
            if (ICD9MA3 != null && ICD9MA3 != undefined && ICD9MA3.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9MA3").val(result["M1024ICD9MA3"] != null && result["M1024ICD9MA3"] != undefined ? result["M1024ICD9MA3"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesA3").val(result["M1024PaymentDiagnosesA3"] != null && result["M1024PaymentDiagnosesA3"] != undefined ? result["M1024PaymentDiagnosesA3"].Answer : "");
            }
            var ICD9MA4 = result["M1024ICD9MA4"];
            if (ICD9MA4 != null && ICD9MA4 != undefined && ICD9MA4.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9MA4").val(result["M1024ICD9MA4"] != null && result["M1024ICD9MA4"] != undefined ? result["M1024ICD9MA4"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesA4").val(result["M1024PaymentDiagnosesA4"] != null && result["M1024PaymentDiagnosesA4"] != undefined ? result["M1024PaymentDiagnosesA4"].Answer : "");
            }

            var ICD9M1 = result["M1022ICD9M1"];
            if (ICD9M1 != null && ICD9M1 != undefined && ICD9M1.Answer != null) {
                $("#ResumptionOfCare_M1022ICD9M1").val(result["M1022ICD9M1"] != null && result["M1022ICD9M1"] != undefined ? result["M1022ICD9M1"].Answer : "");
                $("#ResumptionOfCare_M1022PrimaryDiagnosis1").val(result["M1022PrimaryDiagnosis1"] != null && result["M1022PrimaryDiagnosis1"] != undefined ? result["M1022PrimaryDiagnosis1"].Answer : "");
                $("#ResumptionOfCare_M1022OtherDiagnose1Rating").val(result["M1022OtherDiagnose1Rating"] != null && result["M1022OtherDiagnose1Rating"] != undefined ? result["M1022OtherDiagnose1Rating"].Answer : "");
            }
            var ICD9MB3 = result["M1024ICD9MB3"];
            if (ICD9MB3 != null && ICD9MB3 != undefined && ICD9MB3.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9MB3").val(result["M1024ICD9MB3"] != null && result["M1024ICD9MB3"] != undefined ? result["M1024ICD9MB3"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesB3").val(result["M1024PaymentDiagnosesB3"] != null && result["M1024PaymentDiagnosesB3"] != undefined ? result["M1024PaymentDiagnosesB3"].Answer : "");
            }
            var ICD9MB4 = result["M1024ICD9MB4"];
            if (ICD9MB4 != null && ICD9MB4 != undefined && ICD9MB4.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9MB4").val(result["M1024ICD9MB4"] != null && result["M1024ICD9MB4"] != undefined ? result["M1024ICD9MB4"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesB4").val(result["M1024PaymentDiagnosesB4"] != null && result["M1024PaymentDiagnosesB4"] != undefined ? result["M1024PaymentDiagnosesB4"].Answer : "");
            }


            var ICD9M2 = result["M1022ICD9M2"];
            if (ICD9M2 != null && ICD9M2 != undefined && ICD9M2.Answer != null) {
                $("#ResumptionOfCare_M1022ICD9M2").val(result["M1022ICD9M2"] != null && result["M1022ICD9M2"] != undefined ? result["M1022ICD9M2"].Answer : "");
                $("#ResumptionOfCare_M1022PrimaryDiagnosis2").val(result["M1022PrimaryDiagnosis2"] != null && result["M1022PrimaryDiagnosis2"] != undefined ? result["M1022PrimaryDiagnosis2"].Answer : "");
                $("#ResumptionOfCare_M1022OtherDiagnose2Rating").val(result["M1022OtherDiagnose2Rating"] != null && result["M1022OtherDiagnose2Rating"] != undefined ? result["M1022OtherDiagnose2Rating"].Answer : "");
            }

            var ICD9MC3 = result["M1024ICD9MC3"];
            if (ICD9MC3 != null && ICD9MC3 != undefined && ICD9MC3.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9MC3").val(result["M1024ICD9MC3"] != null && result["M1024ICD9MC3"] != undefined ? result["M1024ICD9MC3"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesC3").val(result["M1024PaymentDiagnosesC3"] != null && result["M1024PaymentDiagnosesC3"] != undefined ? result["M1024PaymentDiagnosesC3"].Answer : "");
            }
            var ICD9MC4 = result["M1024ICD9MC4"];
            if (ICD9MC4 != null && ICD9MC4 != undefined && ICD9MC4.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9MC4").val(result["M1024ICD9MC4"] != null && result["M1024ICD9MC4"] != undefined ? result["M1024ICD9MC4"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesC4").val(result["M1024PaymentDiagnosesC4"] != null && result["M1024PaymentDiagnosesC4"] != undefined ? result["M1024PaymentDiagnosesC4"].Answer : "");
            }

            var ICD9M3 = result["M1022ICD9M3"];
            if (ICD9M3 != null && ICD9M3 != undefined && ICD9M3.Answer != null) {
                $("#ResumptionOfCare_M1022ICD9M3").val(result["M1022ICD9M3"] != null && result["M1022ICD9M3"] != undefined ? result["M1022ICD9M3"].Answer : "");
                $("#ResumptionOfCare_M1022PrimaryDiagnosis3").val(result["M1022PrimaryDiagnosis3"] != null && result["M1022PrimaryDiagnosis3"] != undefined ? result["M1022PrimaryDiagnosis3"].Answer : "");
                $("#ResumptionOfCare_M1022OtherDiagnose3Rating").val(result["M1022OtherDiagnose3Rating"] != null && result["M1022OtherDiagnose3Rating"] != undefined ? result["M1022OtherDiagnose3Rating"].Answer : "");
            }
            var ICD9MD3 = result["M1024ICD9MD3"];
            if (ICD9MD3 != null && ICD9MD3 != undefined && ICD9MD3.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9MD3").val(result["M1024ICD9MD3"] != null && result["M1024ICD9MD3"] != undefined ? result["M1024ICD9MD3"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesD3").val(result["M1024PaymentDiagnosesD3"] != null && result["M1024PaymentDiagnosesD3"] != undefined ? result["M1024PaymentDiagnosesD3"].Answer : "");
            }
            var ICD9MD4 = result["M1024ICD9MD4"];
            if (ICD9MD4 != null && ICD9MD4 != undefined && ICD9MD4.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9MD4").val(result["M1024ICD9MD4"] != null && result["M1024ICD9MD4"] != undefined ? result["M1024ICD9MD4"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesD4").val(result["M1024PaymentDiagnosesD4"] != null && result["M1024PaymentDiagnosesD4"] != undefined ? result["M1024PaymentDiagnosesD4"].Answer : "");
            }

            var ICD9M4 = result["M1022ICD9M4"];
            if (ICD9M4 != null && ICD9M4 != undefined && ICD9M4.Answer != null) {
                $("#ResumptionOfCare_M1022ICD9M4").val(result["M1022ICD9M4"] != null && result["M1022ICD9M4"] != undefined ? result["M1022ICD9M4"].Answer : "");
                $("#ResumptionOfCare_M1022PrimaryDiagnosis4").val(result["M1022PrimaryDiagnosis4"] != null && result["M1022PrimaryDiagnosis4"] != undefined ? result["M1022PrimaryDiagnosis4"].Answer : "");
                $("#ResumptionOfCare_M1022OtherDiagnose4Rating").val(result["M1022OtherDiagnose4Rating"] != null && result["M1022OtherDiagnose4Rating"] != undefined ? result["M1022OtherDiagnose4Rating"].Answer : "");
            }
            var ICD9ME3 = result["M1024ICD9ME3"];
            if (ICD9ME3 != null && ICD9ME3 != undefined && ICD9ME3.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9ME3").val(result["M1024ICD9ME3"] != null && result["M1024ICD9ME3"] != undefined ? result["M1024ICD9ME3"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesE3").val(result["M1024PaymentDiagnosesE3"] != null && result["M1024PaymentDiagnosesE3"] != undefined ? result["M1024PaymentDiagnosesE3"].Answer : "");
            }
            var ICD9ME4 = result["M1024ICD9ME4"];
            if (ICD9ME4 != null && ICD9ME4 != undefined && ICD9ME4.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9ME4").val(result["M1024ICD9ME4"] != null && result["M1024ICD9ME4"] != undefined ? result["M1024ICD9ME4"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesE4").val(result["M1024PaymentDiagnosesE4"] != null && result["M1024PaymentDiagnosesE4"] != undefined ? result["M1024PaymentDiagnosesE4"].Answer : "");
            }

            var ICD9M5 = result["M1022ICD9M5"];
            if (ICD9M5 != null && ICD9M5 != undefined && ICD9M5.Answer != null) {
                $("#ResumptionOfCare_M1022ICD9M5").val(result["M1022ICD9M5"] != null && result["M1022ICD9M5"] != undefined ? result["M1022ICD9M5"].Answer : "");
                $("#ResumptionOfCare_M1022PrimaryDiagnosis5").val(result["M1022PrimaryDiagnosis5"] != null && result["M1022PrimaryDiagnosis5"] != undefined ? result["M1022PrimaryDiagnosis5"].Answer : "");
                $("#ResumptionOfCare_M1022OtherDiagnose5Rating").val(result["M1022OtherDiagnose5Rating"] != null && result["M1022OtherDiagnose5Rating"] != undefined ? result["M1022OtherDiagnose5Rating"].Answer : "");
            }
            var ICD9MF3 = result["M1024ICD9MF3"];
            if (ICD9MF3 != null && ICD9MF3 != undefined && ICD9MF3.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9MF3").val(result["M1024ICD9MF3"] != null && result["M1024ICD9MF3"] != undefined ? result["M1024ICD9MF3"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesF3").val(result["M1024PaymentDiagnosesF3"] != null && result["M1024PaymentDiagnosesF3"] != undefined ? result["M1024PaymentDiagnosesF3"].Answer : "");
            }
            var ICD9MF4 = result["M1024ICD9MF4"];
            if (ICD9MF4 != null && ICD9MF4 != undefined && ICD9MF4.Answer != null) {
                $("#ResumptionOfCare_M1024ICD9MF4").val(result["M1024ICD9MF4"] != null && result["M1024ICD9MF4"] != undefined ? result["M1024ICD9MF4"].Answer : "");
                $("#ResumptionOfCare_M1024PaymentDiagnosesF4").val(result["M1024PaymentDiagnosesF4"] != null && result["M1024PaymentDiagnosesF4"] != undefined ? result["M1024PaymentDiagnosesF4"].Answer : "");
            }


            $('input[name=ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUI][value=' + (result["M1018ConditionsPriorToMedicalRegimenUI"] != null && result["M1018ConditionsPriorToMedicalRegimenUI"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenUI"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenCATH][value=' + (result["M1018ConditionsPriorToMedicalRegimenCATH"] != null && result["M1018ConditionsPriorToMedicalRegimenCATH"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenCATH"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenPain][value=' + (result["M1018ConditionsPriorToMedicalRegimenPain"] != null && result["M1018ConditionsPriorToMedicalRegimenPain"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenPain"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDECSN][value=' + (result["M1018ConditionsPriorToMedicalRegimenDECSN"] != null && result["M1018ConditionsPriorToMedicalRegimenDECSN"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenDisruptive][value=' + (result["M1018ConditionsPriorToMedicalRegimenDisruptive"] != null && result["M1018ConditionsPriorToMedicalRegimenDisruptive"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenMemLoss][value=' + (result["M1018ConditionsPriorToMedicalRegimenMemLoss"] != null && result["M1018ConditionsPriorToMedicalRegimenMemLoss"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNone][value=' + (result["M1018ConditionsPriorToMedicalRegimenNone"] != null && result["M1018ConditionsPriorToMedicalRegimenNone"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenNA][value=' + (result["M1018ConditionsPriorToMedicalRegimenNA"] != null && result["M1018ConditionsPriorToMedicalRegimenNA"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenNA"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1018ConditionsPriorToMedicalRegimenUK][value=' + (result["M1018ConditionsPriorToMedicalRegimenUK"] != null && result["M1018ConditionsPriorToMedicalRegimenUK"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenUK"].Answer : "") + ']').attr('checked', true);



            $('input[name=ResumptionOfCare_M1030HomeTherapiesInfusion][value=' + (result["M1030HomeTherapiesInfusion"] != null && result["M1030HomeTherapiesInfusion"] != undefined ? result["M1030HomeTherapiesInfusion"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1030HomeTherapiesParNutrition][value=' + (result["M1030HomeTherapiesParNutrition"] != null && result["M1030HomeTherapiesParNutrition"] != undefined ? result["M1030HomeTherapiesParNutrition"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1030HomeTherapiesEntNutrition][value=' + (result["M1030HomeTherapiesEntNutrition"] != null && result["M1030HomeTherapiesEntNutrition"] != undefined ? result["M1030HomeTherapiesEntNutrition"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1030HomeTherapiesNone][value=' + (result["M1030HomeTherapiesNone"] != null && result["M1030HomeTherapiesNone"] != undefined ? result["M1030HomeTherapiesNone"].Answer : "") + ']').attr('checked', true);


            $('input[name=ResumptionOfCare_M1032HospitalizationRiskRecentDecline][value=' + (result["M1032HospitalizationRiskRecentDecline"] != null && result["M1032HospitalizationRiskRecentDecline"] != undefined ? result["M1032HospitalizationRiskRecentDecline"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1032HospitalizationRiskMultipleHosp][value=' + (result["M1032HospitalizationRiskMultipleHosp"] != null && result["M1032HospitalizationRiskMultipleHosp"] != undefined ? result["M1032HospitalizationRiskMultipleHosp"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1032HospitalizationRiskHistoryOfFall][value=' + (result["M1032HospitalizationRiskHistoryOfFall"] != null && result["M1032HospitalizationRiskHistoryOfFall"] != undefined ? result["M1032HospitalizationRiskHistoryOfFall"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1032HospitalizationRiskMedications][value=' + (result["M1032HospitalizationRiskMedications"] != null && result["M1032HospitalizationRiskMedications"] != undefined ? result["M1032HospitalizationRiskMedications"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1032HospitalizationRiskFrailty][value=' + (result["M1032HospitalizationRiskFrailty"] != null && result["M1032HospitalizationRiskFrailty"] != undefined ? result["M1032HospitalizationRiskFrailty"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1032HospitalizationRiskOther][value=' + (result["M1032HospitalizationRiskOther"] != null && result["M1032HospitalizationRiskOther"] != undefined ? result["M1032HospitalizationRiskOther"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1032HospitalizationRiskNone][value=' + (result["M1032HospitalizationRiskNone"] != null && result["M1032HospitalizationRiskNone"] != undefined ? result["M1032HospitalizationRiskNone"].Answer : "") + ']').attr('checked', true);


            $('input[name=ResumptionOfCare_M1034OverallStatus][value=' + (result["M1034OverallStatus"] != null && result["M1034OverallStatus"] != undefined ? result["M1034OverallStatus"].Answer : "") + ']').attr('checked', true);


            $('input[name=ResumptionOfCare_M1036RiskFactorsSmoking][value=' + (result["M1036RiskFactorsSmoking"] != null && result["M1036RiskFactorsSmoking"] != undefined ? result["M1036RiskFactorsSmoking"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1036RiskFactorsObesity][value=' + (result["M1036RiskFactorsObesity"] != null && result["M1036RiskFactorsObesity"] != undefined ? result["M1036RiskFactorsObesity"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1036RiskFactorsAlcoholism][value=' + (result["M1036RiskFactorsAlcoholism"] != null && result["M1036RiskFactorsAlcoholism"] != undefined ? result["M1036RiskFactorsAlcoholism"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1036RiskFactorsDrugs][value=' + (result["M1036RiskFactorsDrugs"] != null && result["M1036RiskFactorsDrugs"] != undefined ? result["M1036RiskFactorsDrugs"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1036RiskFactorsNone][value=' + (result["M1036RiskFactorsNone"] != null && result["M1036RiskFactorsNone"] != undefined ? result["M1036RiskFactorsNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1036RiskFactorsUnknown][value=' + (result["M1036RiskFactorsUnknown"] != null && result["M1036RiskFactorsUnknown"] != undefined ? result["M1036RiskFactorsUnknown"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1100LivingSituation][value=' + (result["M1100LivingSituation"] != null && result["M1100LivingSituation"] != undefined ? result["M1100LivingSituation"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1200Vision][value=' + (result["M1200Vision"] != null && result["M1200Vision"] != undefined ? result["M1200Vision"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1210Hearing][value=' + (result["M1210Hearing"] != null && result["M1210Hearing"] != undefined ? result["M1210Hearing"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1220VerbalContent][value=' + (result["M1220VerbalContent"] != null && result["M1220VerbalContent"] != undefined ? result["M1220VerbalContent"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1230SpeechAndOral][value=' + (result["M1230SpeechAndOral"] != null && result["M1230SpeechAndOral"] != undefined ? result["M1230SpeechAndOral"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1240FormalPainAssessment][value=' + (result["M1240FormalPainAssessment"] != null && result["M1240FormalPainAssessment"] != undefined ? result["M1240FormalPainAssessment"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1242PainInterferingFrequency][value=' + (result["M1242PainInterferingFrequency"] != null && result["M1242PainInterferingFrequency"] != undefined ? result["M1242PainInterferingFrequency"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1300PressureUlcerAssessment][value=' + (result["M1300PressureUlcerAssessment"] != null && result["M1300PressureUlcerAssessment"] != undefined ? result["M1300PressureUlcerAssessment"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=ResumptionOfCare_M1300PressureUlcerAssessment]:checked').val() == "00") {
                $("#roc_M1302").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=ResumptionOfCare_M1302RiskDevelopingPressureUlcers][value=' + (result["M1302RiskDevelopingPressureUlcers"] != null && result["M1302RiskDevelopingPressureUlcers"] != undefined ? result["M1302RiskDevelopingPressureUlcers"].Answer : "") + ']').attr('checked', true);
            }
            $('input[name=ResumptionOfCare_M1306UnhealedPressureUlcers][value=' + (result["M1306UnhealedPressureUlcers"] != null && result["M1306UnhealedPressureUlcers"] != undefined ? result["M1306UnhealedPressureUlcers"].Answer : "") + ']').attr('checked', true);
            var unhealedPressureUlcers = $('input[name=ResumptionOfCare_M1306UnhealedPressureUlcers]:checked').val();
            if (unhealedPressureUlcers == 0) {



                $("#roc_M1308").block(
                                {
                                    message: '',
                                    overlayCSS: {
                                        "cursor": "default"
                                    }
                                });
                $("#roc_M13010_12_14").block(
                                {
                                    message: '',
                                    overlayCSS: {
                                        "cursor": "default"
                                    }
                                });
                $("#roc_M1320").block(
                                {
                                    message: '',
                                    overlayCSS: {
                                        "cursor": "default"
                                    }
                                });
            }
            else if (unhealedPressureUlcers == 1) {
                $("#ResumptionOfCare_M1308NumberNonEpithelializedStageTwoUlcerCurrent").val(result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : "");
                $("#ResumptionOfCare_M1308NumberNonEpithelializedStageTwoUlcerAdmission").val(result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : "");
                $("#ResumptionOfCare_M1308NumberNonEpithelializedStageThreeUlcerCurrent").val(result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : "");
                $("#ResumptionOfCare_M1308NumberNonEpithelializedStageThreeUlcerAdmission").val(result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : "");
                $("#ResumptionOfCare_M1308NumberNonEpithelializedStageFourUlcerCurrent").val(result["M1308NumberNonEpithelializedStageFourUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageFourUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : "");
                $("#ResumptionOfCare_M1308NumberNonEpithelializedStageIVUlcerAdmission").val(result["M1308NumberNonEpithelializedStageIVUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageIVUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : "");
                $("#ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "");
                $("#ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : "");
                $("#ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "");
                $("#ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : "");
                $("#ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer : "");
                $("#ResumptionOfCare_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : "");
                $("#ResumptionOfCare_M1310PressureUlcerLength").val(result["M1310PressureUlcerLength"] != null && result["M1310PressureUlcerLength"] != undefined ? result["M1310PressureUlcerLength"].Answer : "");
                $("#ResumptionOfCare_M1312PressureUlcerWidth").val(result["M1312PressureUlcerWidth"] != null && result["M1312PressureUlcerWidth"] != undefined ? result["M1312PressureUlcerWidth"].Answer : "");
                $("#ResumptionOfCare_M1314PressureUlcerDepth").val(result["M1314PressureUlcerDepth"] != null && result["M1314PressureUlcerDepth"] != undefined ? result["M1314PressureUlcerDepth"].Answer : "");
                $('input[name=ResumptionOfCare_M1320MostProblematicPressureUlcerStatus][value=' + (result["M1320MostProblematicPressureUlcerStatus"] != null && result["M1320MostProblematicPressureUlcerStatus"] != undefined ? result["M1320MostProblematicPressureUlcerStatus"].Answer : "") + ']').attr('checked', true);

                $("#roc_M1308").unblock();
                $("#roc_M13010_12_14").unblock();
                $("#roc_M1320").unblock();
            }

            $('input[name=ResumptionOfCare_M1322CurrentNumberStageIUlcer][value=' + (result["M1322CurrentNumberStageIUlcer"] != null && result["M1322CurrentNumberStageIUlcer"] != undefined ? result["M1322CurrentNumberStageIUlcer"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1324MostProblematicUnhealedStage][value=' + (result["M1324MostProblematicUnhealedStage"] != null && result["M1324MostProblematicUnhealedStage"] != undefined ? result["M1324MostProblematicUnhealedStage"].Answer : "") + ']').attr('checked', true);

            $('input[name=ResumptionOfCare_M1330StasisUlcer][value=' + (result["M1330StasisUlcer"] != null && result["M1330StasisUlcer"] != undefined ? result["M1330StasisUlcer"].Answer : "") + ']').attr('checked', true);
            var stasisUlcer = $('input[name=ResumptionOfCare_M1330StasisUlcer]:checked').val();
            if (stasisUlcer == "00" || stasisUlcer == "03") {
                $("#roc_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=ResumptionOfCare_M1332CurrentNumberStasisUlcer][value=' + (result["M1332CurrentNumberStasisUlcer"] != null && result["M1332CurrentNumberStasisUlcer"] != undefined ? result["M1332CurrentNumberStasisUlcer"].Answer : "") + ']').attr('checked', true);
                $('input[name=ResumptionOfCare_M1334StasisUlcerStatus][value=' + (result["M1334StasisUlcerStatus"] != null && result["M1334StasisUlcerStatus"] != undefined ? result["M1334StasisUlcerStatus"].Answer : "") + ']').attr('checked', true);
                $("#roc_M1332AndM1334").unblock();
            }

            $('input[name=ResumptionOfCare_M1340SurgicalWound][value=' + (result["M1340SurgicalWound"] != null && result["M1340SurgicalWound"] != undefined ? result["M1340SurgicalWound"].Answer : "") + ']').attr('checked', true);
            var surgicalWound = $('input[name=ResumptionOfCare_M1340SurgicalWound]:checked').val();
            if (surgicalWound == "00" || surgicalWound == "02") {
                $("#roc_M1342").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=ResumptionOfCare_M1342SurgicalWoundStatus][value=' + (result["M1342SurgicalWoundStatus"] != null && result["M1342SurgicalWoundStatus"] != undefined ? result["M1342SurgicalWoundStatus"].Answer : "") + ']').attr('checked', true);
                $("#roc_M1342").unblock();
            }
            $('input[name=ResumptionOfCare_M1350SkinLesionOpenWound][value=' + (result["M1350SkinLesionOpenWound"] != null && result["M1350SkinLesionOpenWound"] != undefined ? result["M1350SkinLesionOpenWound"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1400PatientDyspneic][value=' + (result["M1400PatientDyspneic"] != null && result["M1400PatientDyspneic"] != undefined ? result["M1400PatientDyspneic"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1410HomeRespiratoryTreatmentsOxygen][value=' + (result["M1410HomeRespiratoryTreatmentsOxygen"] != null && result["M1410HomeRespiratoryTreatmentsOxygen"] != undefined ? result["M1410HomeRespiratoryTreatmentsOxygen"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1410HomeRespiratoryTreatmentsVentilator][value=' + (result["M1410HomeRespiratoryTreatmentsVentilator"] != null && result["M1410HomeRespiratoryTreatmentsVentilator"] != undefined ? result["M1410HomeRespiratoryTreatmentsVentilator"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1410HomeRespiratoryTreatmentsContinuous][value=' + (result["M1410HomeRespiratoryTreatmentsContinuous"] != null && result["M1410HomeRespiratoryTreatmentsContinuous"] != undefined ? result["M1410HomeRespiratoryTreatmentsContinuous"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1410HomeRespiratoryTreatmentsNone][value=' + (result["M1410HomeRespiratoryTreatmentsNone"] != null && result["M1410HomeRespiratoryTreatmentsNone"] != undefined ? result["M1410HomeRespiratoryTreatmentsNone"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1600UrinaryTractInfection][value=' + (result["M1600UrinaryTractInfection"] != null && result["M1600UrinaryTractInfection"] != undefined ? result["M1600UrinaryTractInfection"].Answer : "") + ']').attr('checked', true);


            $('input[name=ResumptionOfCare_M1610UrinaryIncontinence][value=' + (result["M1610UrinaryIncontinence"] != null && result["M1610UrinaryIncontinence"] != undefined ? result["M1610UrinaryIncontinence"].Answer : "") + ']').attr('checked', true);
            var urinaryIncontinence = $('input[name=ResumptionOfCare_M1610UrinaryIncontinence]:checked').val();
            if (urinaryIncontinence == "00" || urinaryIncontinence == "02") {
                $("#roc_M1615").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=ResumptionOfCare_M1615UrinaryIncontinenceOccur][value=' + (result["M1615UrinaryIncontinenceOccur"] != null && result["M1615UrinaryIncontinenceOccur"] != undefined ? result["M1615UrinaryIncontinenceOccur"].Answer : "") + ']').attr('checked', true);
                $("#roc_M1615").unblock();
            }
            $('input[name=ResumptionOfCare_M1620BowelIncontinenceFrequency][value=' + (result["M1620BowelIncontinenceFrequency"] != null && result["M1620BowelIncontinenceFrequency"] != undefined ? result["M1620BowelIncontinenceFrequency"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1630OstomyBowelElimination][value=' + (result["M1630OstomyBowelElimination"] != null && result["M1630OstomyBowelElimination"] != undefined ? result["M1630OstomyBowelElimination"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1700CognitiveFunctioning][value=' + (result["M1700CognitiveFunctioning"] != null && result["M1700CognitiveFunctioning"] != undefined ? result["M1700CognitiveFunctioning"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1710WhenConfused][value=' + (result["M1710WhenConfused"] != null && result["M1710WhenConfused"] != undefined ? result["M1710WhenConfused"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1720WhenAnxious][value=' + (result["M1720WhenAnxious"] != null && result["M1720WhenAnxious"] != undefined ? result["M1720WhenAnxious"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1730DepressionScreening][value=' + (result["M1730DepressionScreening"] != null && result["M1730DepressionScreening"] != undefined ? result["M1730DepressionScreening"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=ResumptionOfCare_M1730DepressionScreening]:checked').val() == '01') {
                $('input[name=ResumptionOfCare_M1730DepressionScreeningInterest][value=' + (result["M1730DepressionScreeningInterest"] != null && result["M1730DepressionScreeningInterest"] != undefined ? result["M1730DepressionScreeningInterest"].Answer : "") + ']').attr('checked', true);
                $('input[name=ResumptionOfCare_M1730DepressionScreeningHopeless][value=' + (result["M1730DepressionScreeningHopeless"] != null && result["M1730DepressionScreeningHopeless"] != undefined ? result["M1730DepressionScreeningHopeless"].Answer : "") + ']').attr('checked', true);
            }
            $('input[name=ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsImpDes][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsVerbal][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsPhysical][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsSIB][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsSIB"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsSIB"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsDelusional][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsNone][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsNone"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsNone"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency][value=' + (result["M1745DisruptiveBehaviorSymptomsFrequency"] != null && result["M1745DisruptiveBehaviorSymptomsFrequency"] != undefined ? result["M1745DisruptiveBehaviorSymptomsFrequency"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1750PsychiatricNursingServicing][value=' + (result["M1750PsychiatricNursingServicing"] != null && result["M1750PsychiatricNursingServicing"] != undefined ? result["M1750PsychiatricNursingServicing"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1800Grooming][value=' + (result["M1800Grooming"] != null && result["M1800Grooming"] != undefined ? result["M1800Grooming"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1810CurrentAbilityToDressUpper][value=' + (result["M1810CurrentAbilityToDressUpper"] != null && result["M1810CurrentAbilityToDressUpper"] != undefined ? result["M1810CurrentAbilityToDressUpper"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1820CurrentAbilityToDressLower][value=' + (result["M1820CurrentAbilityToDressLower"] != null && result["M1820CurrentAbilityToDressLower"] != undefined ? result["M1820CurrentAbilityToDressLower"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1830CurrentAbilityToBatheEntireBody][value=' + (result["M1830CurrentAbilityToBatheEntireBody"] != null && result["M1830CurrentAbilityToBatheEntireBody"] != undefined ? result["M1830CurrentAbilityToBatheEntireBody"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1840ToiletTransferring][value=' + (result["M1840ToiletTransferring"] != null && result["M1840ToiletTransferring"] != undefined ? result["M1840ToiletTransferring"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1845ToiletingHygiene][value=' + (result["M1845ToiletingHygiene"] != null && result["M1845ToiletingHygiene"] != undefined ? result["M1845ToiletingHygiene"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1850Transferring][value=' + (result["M1850Transferring"] != null && result["M1850Transferring"] != undefined ? result["M1850Transferring"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1860AmbulationLocomotion][value=' + (result["M1860AmbulationLocomotion"] != null && result["M1860AmbulationLocomotion"] != undefined ? result["M1860AmbulationLocomotion"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1870FeedingOrEating][value=' + (result["M1870FeedingOrEating"] != null && result["M1870FeedingOrEating"] != undefined ? result["M1870FeedingOrEating"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1880AbilityToPrepareLightMeal][value=' + (result["M1880AbilityToPrepareLightMeal"] != null && result["M1880AbilityToPrepareLightMeal"] != undefined ? result["M1880AbilityToPrepareLightMeal"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1890AbilityToUseTelephone][value=' + (result["M1890AbilityToUseTelephone"] != null && result["M1890AbilityToUseTelephone"] != undefined ? result["M1890AbilityToUseTelephone"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1900SelfCareFunctioning][value=' + (result["M1900SelfCareFunctioning"] != null && result["M1900SelfCareFunctioning"] != undefined ? result["M1900SelfCareFunctioning"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1900Ambulation][value=' + (result["M1900Ambulation"] != null && result["M1900Ambulation"] != undefined ? result["M1900Ambulation"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1900Transfer][value=' + (result["M1900Transfer"] != null && result["M1900Transfer"] != undefined ? result["M1900Transfer"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1900HouseHoldTasks][value=' + (result["M1900HouseHoldTasks"] != null && result["M1900HouseHoldTasks"] != undefined ? result["M1900HouseHoldTasks"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M1910FallRiskAssessment][value=' + (result["M1910FallRiskAssessment"] != null && result["M1910FallRiskAssessment"] != undefined ? result["M1910FallRiskAssessment"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2000DrugRegimenReview][value=' + (result["M2000DrugRegimenReview"] != null && result["M2000DrugRegimenReview"] != undefined ? result["M2000DrugRegimenReview"].Answer : "") + ']').attr('checked', true);
            var drugRegimenReview = $('input[name=ResumptionOfCare_M2000DrugRegimenReview]:checked').val();
            if (drugRegimenReview == "00" || drugRegimenReview == "01") {

                $("#roc_M2002").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $('input[name=ResumptionOfCare_M2010PatientOrCaregiverHighRiskDrugEducation][value=' + (result["M2010PatientOrCaregiverHighRiskDrugEducation"] != null && result["M2010PatientOrCaregiverHighRiskDrugEducation"] != undefined ? result["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer : "") + ']').attr('checked', true);
                $('input[name=ResumptionOfCare_M2020ManagementOfOralMedications][value=' + (result["M2020ManagementOfOralMedications"] != null && result["M2020ManagementOfOralMedications"] != undefined ? result["M2020ManagementOfOralMedications"].Answer : "") + ']').attr('checked', true);
                $('input[name=ResumptionOfCare_M2030ManagementOfInjectableMedications][value=' + (result["M2030ManagementOfInjectableMedications"] != null && result["M2030ManagementOfInjectableMedications"] != undefined ? result["M2030ManagementOfInjectableMedications"].Answer : "") + ']').attr('checked', true);
            }
            else if (drugRegimenReview == "NA") {


                $("#roc_M2002").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#roc_M2010").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#roc_M2020").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#roc_M2030").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=ResumptionOfCare_M2002MedicationFollowup][value=' + (result["M2002MedicationFollowup"] != null && result["M2002MedicationFollowup"] != undefined ? result["M2002MedicationFollowup"].Answer : "") + ']').attr('checked', true);
                $('input[name=ResumptionOfCare_M2010PatientOrCaregiverHighRiskDrugEducation][value=' + (result["M2010PatientOrCaregiverHighRiskDrugEducation"] != null && result["M2010PatientOrCaregiverHighRiskDrugEducation"] != undefined ? result["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer : "") + ']').attr('checked', true);
                $('input[name=ResumptionOfCare_M2020ManagementOfOralMedications][value=' + (result["M2020ManagementOfOralMedications"] != null && result["M2020ManagementOfOralMedications"] != undefined ? result["M2020ManagementOfOralMedications"].Answer : "") + ']').attr('checked', true);
                $('input[name=ResumptionOfCare_M2030ManagementOfInjectableMedications][value=' + (result["M2030ManagementOfInjectableMedications"] != null && result["M2030ManagementOfInjectableMedications"] != undefined ? result["M2030ManagementOfInjectableMedications"].Answer : "") + ']').attr('checked', true);

            }


            $('input[name=ResumptionOfCare_M2040PriorMedicationOral][value=' + (result["M2040PriorMedicationOral"] != null && result["M2040PriorMedicationOral"] != undefined ? result["M2040PriorMedicationOral"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2040PriorMedicationInject][value=' + (result["M2040PriorMedicationInject"] != null && result["M2040PriorMedicationInject"] != undefined ? result["M2040PriorMedicationInject"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2100ADLAssistance][value=' + (result["M2100ADLAssistance"] != null && result["M2100ADLAssistance"] != undefined ? result["M2100ADLAssistance"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2100IADLAssistance][value=' + (result["M2100IADLAssistance"] != null && result["M2100IADLAssistance"] != undefined ? result["M2100IADLAssistance"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2100MedicationAdministration][value=' + (result["M2100MedicationAdministration"] != null && result["M2100MedicationAdministration"] != undefined ? result["M2100MedicationAdministration"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2100MedicalProcedures][value=' + (result["M2100MedicalProcedures"] != null && result["M2100MedicalProcedures"] != undefined ? result["M2100MedicalProcedures"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2100ManagementOfEquipment][value=' + (result["M2100ManagementOfEquipment"] != null && result["M2100ManagementOfEquipment"] != undefined ? result["M2100ManagementOfEquipment"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2100SupervisionAndSafety][value=' + (result["M2100SupervisionAndSafety"] != null && result["M2100SupervisionAndSafety"] != undefined ? result["M2100SupervisionAndSafety"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2100FacilitationPatientParticipation][value=' + (result["M2100FacilitationPatientParticipation"] != null && result["M2100FacilitationPatientParticipation"] != undefined ? result["M2100FacilitationPatientParticipation"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2110FrequencyOfADLOrIADLAssistance][value=' + (result["M2110FrequencyOfADLOrIADLAssistance"] != null && result["M2110FrequencyOfADLOrIADLAssistance"] != undefined ? result["M2110FrequencyOfADLOrIADLAssistance"].Answer : "") + ']').attr('checked', true);


            var therapyNeed = result["M2200TherapyNeed"];
            if (therapyNeed !== null && therapyNeed != undefined) {
                if (therapyNeed.Answer == 1) {

                    $('input[name=ResumptionOfCare_M2200TherapyNeed][value=1]').attr('checked', true);
                    $("#ResumptionOfCare_M2200NumberOfTherapyNeed").val("");
                }
                else {
                    $('input[name=ResumptionOfCare_M2200TherapyNeed][value=1]').attr('checked', false);
                    $("#ResumptionOfCare_M2200NumberOfTherapyNeed").val(result["M2200NumberOfTherapyNeed"] != null && result["M2200NumberOfTherapyNeed"] != undefined ? result["M2200NumberOfTherapyNeed"].Answer : "");
                }
            }

            $('input[name=ResumptionOfCare_M2250PatientParameters][value=' + (result["M2250PatientParameters"] != null && result["M2250PatientParameters"] != undefined ? result["M2250PatientParameters"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2250DiabeticFoot][value=' + (result["M2250DiabeticFoot"] != null && result["M2250DiabeticFoot"] != undefined ? result["M2250DiabeticFoot"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2250FallsPrevention][value=' + (result["M2250FallsPrevention"] != null && result["M2250FallsPrevention"] != undefined ? result["M2250FallsPrevention"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2250DepressionPrevention][value=' + (result["M2250DepressionPrevention"] != null && result["M2250DepressionPrevention"] != undefined ? result["M2250DepressionPrevention"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2250MonitorMitigatePainIntervention][value=' + (result["M2250MonitorMitigatePainIntervention"] != null && result["M2250MonitorMitigatePainIntervention"] != undefined ? result["M2250MonitorMitigatePainIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2250PressureUlcerIntervention][value=' + (result["M2250PressureUlcerIntervention"] != null && result["M2250PressureUlcerIntervention"] != undefined ? result["M2250PressureUlcerIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=ResumptionOfCare_M2250PressureUlcerTreatment][value=' + (result["M2250PressureUlcerTreatment"] != null && result["M2250PressureUlcerTreatment"] != undefined ? result["M2250PressureUlcerTreatment"].Answer : "") + ']').attr('checked', true);

        };
    }

}