﻿var Message = {
    _tokenlist: null,
    Init: function() {
        $("#messageList").show();
        $("#messageContent").show();
        $("#recipientList").hide();
        $("#newMessageContent").hide();

        JQD.open_window('#messageInbox');

        $("#composeNewMessage").click(function() {
            $("#messageList").hide();
            $("#messageContent").hide();
            $("#recipientList").show();
            $("#newMessageContent").show();
        });

        $("#newMessageCancelButton").click(function() {
            $("#messageList").show();
            $("#messageContent").show();
            $("#recipientList").hide();
            $("#newMessageContent").hide();
            $("#newMessageForm").clearForm();
        });

        $('#selectAllContacts').change(function() {
            if ($('#selectAllContacts').attr('checked')) {
                $('.contact').each(function() {
                    $(this).attr('checked', true);
                });
            }
            else {
                $('.contact').each(function() {
                    $(this).attr('checked', false);
                });
            }
        });

        $(".messagePanel").find('div').each(function() {
            $(this).click(function() {
                $(".messagePanel").find('div').each(function() {
                    $(this).removeClass('selectedMessage');
                });
                $(this).addClass('selectedMessage');
                $(this).find('div span').removeClass('bold');
            });
        });

        Message._tokenlist = $.fn.tokenInput("#newMessageRecipents", "/Message/Contacts", {
            classes: {
                tokenList: "token-input-list-facebook",
                token: "token-input-token-facebook",
                tokenDelete: "token-input-delete-token-facebook",
                selectedToken: "token-input-selected-token-facebook",
                highlightedToken: "token-input-highlighted-token-facebook",
                dropdown: "token-input-dropdown-facebook",
                dropdownItem: "token-input-dropdown-item-facebook",
                dropdownItem2: "token-input-dropdown-item2-facebook",
                selectedDropdownItem: "token-input-selected-dropdown-item-facebook",
                inputToken: "token-input-input-token-facebook"
            }
        });
    },
    Load: function() {
        $('#messageInboxResult').load('Message/Inbox', { pagenumber: 1 }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#masterCalendarResult').html('<p>There was an error loading your inbox</p>');
            }
            else if (textStatus == "success") {
                Message.Init();
            }
        });
    },
    AddContacts: function() {
        $("input[name='contact']:checked").each(function() {
            var id = $(this).attr('id');
            var data = $(this).attr('value');
            var recipients = $("#newMessageRecipientIds").val();

            if (recipients.length == 0) {
                $("#newMessageRecipientIds").val(id);
                Message._tokenlist.insertToken(id, data);
            } else if (recipients.indexOf(id) == -1) {
                $("#newMessageRecipientIds").val(recipients + ", " + id);
                Message._tokenlist.insertToken(id, data);
            }
        });
    }
}