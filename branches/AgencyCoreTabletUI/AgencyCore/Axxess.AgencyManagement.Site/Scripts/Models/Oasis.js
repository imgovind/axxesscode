﻿var Oasis = {
    _patientId: "",
    _patientName: "",
    _patientRowIndex: 0,
    GetId: function() {
        return Oasis._patientId;
    },
    SetId: function(patientId) {
        Oasis._patientId = patientId;
    },
    SetName: function(patientName) {
        Oasis._patientName = patientName;
    }
    ,
    SetPatientRowIndex: function(patientRowIndex) {
        Oasis._patientRowIndex = patientRowIndex;
    }
    ,
    CalculateGridHeight: function() {
        setTimeout(function() {
            $('#Oasis_Spliter1').children().height($('#Oasis_Spliter1').height() + 4);
            var sideContainer = $('#OasisLeftSide').height();
            var filterContainer = $('#OasisFilterContainer').height();
            var gridContentHeight = sideContainer - filterContainer - 25 - 23 - 20 + 10;
            $('#OasisPatientSelectionGrid').find(".t-grid-content").height(gridContentHeight);

        }, 1000);
    },
    CalculateActivityGrid: function() {
        setTimeout(function() {
            var activityGridRow = $('#oasisBottomPanel').height() - $('#OasisActivityGrid').find(".t-grid-header").height() - 9;
            $('#OasisActivityGrid').find(".t-grid-content").height(activityGridRow);
            $("#Oasis_window").hide();
        }, 1250);
    }
    ,
    Init: function() {
    
        JQD.open_window('#Oasis_window');
        Oasis.CalculateActivityGrid();
        $(".tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
        $(".tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
        $("select.oasisPatientStatusDropDown").change(function() {
            var patientGrid = $('#OasisPatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.oasisPatientStatusDropDown").val(), paymentSourceId: $("select.oasisPatientPaymentDropDown").val(), name: $("#txtSearch_Oasis_Patient_Selection").val() });
        });

        $("select.oasisPatientPaymentDropDown").change(function() {
            var patientGrid = $('#OasisPatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.oasisPatientStatusDropDown").val(), paymentSourceId: $("select.oasisPatientPaymentDropDown").val(), name: $("#txtSearch_Oasis_Patient_Selection").val() });
        });

        $('#txtSearch_Oasis_Patient_Selection').keypress(function() {
            var patientGrid = $('#OasisPatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.oasisPatientStatusDropDown").val(), paymentSourceId: $("select.oasisPatientPaymentDropDown").val(), name: $("#txtSearch_Oasis_Patient_Selection").val() });
        });


        $("select.oasisActivityDropDown").change(function() {
            var PatientActivityGrid = $('#OasisActivityGrid').data('tGrid');
            PatientActivityGrid.rebind({ patientId: Oasis._patientId, discipline: $("select.oasisActivityDropDown").val(), dateRangeId: $("select.oasisActivityDateDropDown").val() });
        });

        if ($("select.oasisActivityDateDropDown").val() == "DateRange") {
            $("#oasisDateRangeText").hide();
            $("div.oasisCustomDateRange").show();
        }
        else if ($("select.oasisActivityDateDropDown").val() == "All") {
            $("#oasisDateRangeText").hide();
            $("div.oasisCustomDateRange").hide();
        }
        else {
            $("#oasisDateRangeText").show();
            $("div.oasisCustomDateRange").hide();
        }
        $("select.oasisActivityDateDropDown").change(function() {

            if ($("select.oasisActivityDateDropDown").val() == "DateRange") {
                $("#oasisDateRangeText").hide();
                $("div.oasisCustomDateRange").show();
            }
            else if ($("select.oasisActivityDateDropDown").val() == "All") {
                Oasis.DateRange($("select.oasisActivityDateDropDown").val());
                $("#oasisDateRangeText").hide();
                $("div.oasisCustomDateRange").hide();
            }
            else {
                Oasis.DateRange($("select.oasisActivityDateDropDown").val());
                $("#oasisDateRangeText").show();
                $("div.oasisCustomDateRange").hide();
            }

            var PatientActivityGrid = $('#OasisActivityGrid').data('tGrid');
            PatientActivityGrid.rebind({ patientId: Oasis._patientId, discipline: $("select.oasisActivityDropDown").val(), dateRangeId: $("select.oasisActivityDateDropDown").val() });
        });



        $.ajaxSetup({ type: "POST" });
        $(".diagnosis").autocomplete('/LookUp/DiagnosisCode', {
            max: 50,
            minChars: 2,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "DIAGNOSIS" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val(row.ShortDescription)
                $(this).parent().parent().find('.ICD').val((row.FormatCode));
            }
        });

        $(".ICD").autocomplete('/LookUp/DiagnosisCode', {
            max: 50,

            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "ICD" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val((row.FormatCode))
                $(this).parent().parent().find('.diagnosis').val(row.ShortDescription);
            }
        });

        $(".procedureICD").autocomplete('/LookUp/ProcedureCode', {
            max: 50,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "PROCEDUREICD" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val((row.FormatCode))
                $(this).parent().parent().find('.procedureDiagnosis').val(row.ShortDescription);
            }
        });

        $(".procedureDiagnosis").autocomplete('/LookUp/ProcedureCode', {
            max: 50,
            minChars: 2,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "PROCEDURE" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val(row.ShortDescription)
                $(this).parent().parent().find('.procedureICD').val((row.FormatCode));
            }
        });

        $(".ICDM1024").autocomplete('/LookUp/DiagnosisCode', {
            max: 50,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "ICD" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val((row.FormatCode))
                $(this).parent().find('.diagnosisM1024').val(row.ShortDescription);
            }
        });

        $(".diagnosisM1024").autocomplete('/LookUp/DiagnosisCode', {
            max: 20,
            minChars: 2,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "DIAGNOSIS" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val(row.ShortDescription)
                $(this).parent().find('.ICDM1024').val((row.FormatCode));
            }
        });

    },
    stripDecimal: function(data) {
        var decimalPos = data.indexOf('.');
        var newData = '';
        for (i = 0; i < data.length; i++) {
            if (i != decimalPos && data.charAt(i) != ' ')
                newData = newData + data.charAt(i);
        }
        return newData;
    }
    ,
    EditStartOfCare: function(id, assessmentType) {
        var patientId = Oasis.GetId();
        $("#soc").clearForm();
        Oasis.BlockAssessmentType();
        var data = 'Id=' + id + "&assessmentType=" + assessmentType;
        $.ajax({
            url: '/Oasis/Generate485',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                loadPlanOfCareEdit(result);
            }
        });
        loadPlanOfCareEdit = function(result) {
            var patient = eval(result);

            $("#txtEdit_PlanOfCare_Medications").val(result.MedicationText);
        }
    }
   ,
    OnPatientRowSelected: function(e) {
        var PatientID = e.row.cells[3].innerHTML;
        Oasis.SetId(PatientID);
        Oasis.SetName(e.row.cells[0].innerHTML + " " + e.row.cells[1].innerHTML);
        Oasis.SetPatientRowIndex(e.row.rowIndex);
        Oasis.LoadPatientInfo();
        $("select.oasisActivityDropDown").val("All");
        $("select.oasisActivityDateDropDown").val("All");
        Oasis.RebindActivity();

    }
    ,
    RebindActivity: function() {
        var patientActivityGrid = $('#OasisActivityGrid').data('tGrid');
        patientActivityGrid.rebind({ patientId: Oasis._patientId, discipline: $("select.oasisActivityDropDown").val(), dateRangeId: $("select.oasisActivityDateDropDown").val() });
    }
    ,
    LoadPatientInfo: function() {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Patient/Get",
            data: "id=" + Oasis._patientId,
            beforeSend: function() {
                U.blockUI('#Oasis_Spliter12');
            },
            success: function(result) {
                var data = eval(result);
                $("#Oasis_PatientID").text((data.PatientIdNumber !== null ? data.PatientIdNumber : ""));
                $("#Oasis_PatientName").text((data.FirstName !== null ? data.FirstName : "") + " " + (data.LastName != null ? data.LastName : ""));
                $("#Oasis_PatientGender").text(data.Gender !== null ? data.Gender : "");
                $("#Oasis_PatientMedicareNo").text(data.MedicareNumber !== null ? data.MedicareNumber : "");
                $("#Oasis_PatientMedicaidNo").text(data.MedicaidNumber !== null ? data.MedicaidNumber : "");
                Oasis.GetNote();
                U.unBlockUI('#Oasis_Spliter12');
            }
        });
    }
    ,
    GetPrimaryPhysician: function(id) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Patient/GetPhysicianContact",
            data: "PhysicianContactId=" + id,
            success: function(result) {
                var resultObject = eval(result);
                $("#patientPhysicianName").text((resultObject.FirstName !== null ? resultObject.FirstName : "") + " " + (resultObject.LastName != null ? resultObject.LastName : ""));
                $("#patientPhysicianEmail").text(resultObject.Email !== null ? resultObject.Email : "");
                $("#patientPhysicianPhone").text(resultObject.PhoneWork !== null ? (resultObject.PhoneWork.substring(0, 3) + "-" + resultObject.PhoneWork.substring(3, 6) + "-" + resultObject.PhoneWork.substring(6)) : "");
            }
        });
    }
    ,
    DateRange: function(id) {
        var data = 'dateRangeId=' + id + "&patientId=" + Oasis._patientId;
        $.ajax({
            url: '/Patient/DateRange',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var reportObject = eval(result);
                $("#oasisDateRangeText").empty();
                $("#oasisDateRangeText").append(reportObject.StartDateFormatted + "-" + reportObject.EndDateFormatted);
            }
        });
    }
    ,
    CustomDateRange: function() {
        var PatientActivityGrid = $('#OasisActivityGrid').data('tGrid');
        PatientActivityGrid.rebind({ patientId: Patient._patientId, discipline: $("select.oasisActivityDropDown").val(), dateRangeId: $("select.oasisActivityDateDropDown").val(), rangeStartDate: $("#oasisActivityFromDate-input").val(), rangeEndDate: $("#oasisActivityToDate-input").val() });
    }
    ,
    Close: function(control) {
        control.closest('div.window').hide();
    }
    ,
    BlockAssessmentType: function() {
        $("div.assessmentType").block({ message: '',
            overlayCSS: {
                "backgroundColor": "transparent",
                "cursor": "default"
            }
        });
    }
    ,
    GetNote: function() {
        var data = "patientId=" + Oasis._patientId;
        $.ajax({
            url: '/Patient/GetNote',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var reportObject = eval(result);
                $("#txt_OasisLanding_Note").val("");
                if (reportObject !== null && reportObject !== "") {
                    $("#txt_OasisLanding_Note").val(reportObject.Note);
                }
                else {
                    $("#txt_OasisLanding_Note").val("");
                }

            }
        });
    }
    ,
    Note: function() {
        var data = "patientId=" + Oasis._patientId;
        $.ajax({
            url: '/Patient/GetNote',
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                $("#notes").val("");
            }
            ,
            success: function(result) {
                var reportObject = eval(result);
                $("#EditNoteTitle").text("Customer Notes : " + Oasis._patientName);
                $("#txt_PatientNote_PatientID").val(Oasis._patientId);
                var date = new Date();
                var note = date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear() + ":\n";
                if (reportObject !== null && reportObject !== "") {
                    $("#notes").val(note + reportObject.Note);
                }
                else {
                    $("#notes").val(note);
                }
            }
        });
    }
    ,
    NextTab: function(id) {
        var $tabs = $(id).tabs();
        var selected = $tabs.data("selected.tabs");
        $tabs.tabs('select', selected + 1);
    },
    Refresh: function(id) {
        var $tabs = $(id).tabs();
        $tabs.tabs('select', 0);
        $($tabs.find(id + " .general")).each(function() { $(this).scrollTop(0) });
    }
    ,
    SaveClose: function(control) {
        var formId = control.closest("form").attr("id");
        $("#" + formId).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Oasis.Close(control);
                            alert("close");
                        }
                        else {
                            alert(resultObject.errorMessage);
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Delete: function(cont, id) {
        var row = cont.parents('tr:first');
        if (confirm("Are you sure you want to delete")) {

            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Oasis/Delete",
                data: "id=" + id,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        $(row).remove();
                    }
                }
            });
        }
    },
    addTableRow: function(jQtable) {
        $(jQtable).each(function() {
            var $table = $(this);
            var tds = '<tr>';
            tds += '<td class="ICDText">  <input  value="" type="text" onfocus="Oasis.SupplyDescription($(this));"  class="suppliesDescription"/> </td>';
            tds += '<td><input  value="" type="text" onfocus="Oasis.SupplyCode($(this));" class="suppliesCode"/> </td>';
            tds += '<td><input  value="" type="text" class="supplyQuantity"/> </td>';
            tds += '<td><input type="button" onclick="Oasis.DeleteRow($(this));" value="Delete" /></td>';
            tds += '</tr>';
            if ($('tbody', this).length > 0) {
                $('tbody', this).append(tds);
            } else {
                $(this).append(tds);
            }
        });
    },

    addToSupplyTable: function(data, table) {
        Oasis.ClearRows(table);
        var tableRow = "";
        for (var i = 0; i < data.length; i++) {
            var tds = '<tr>';
            tds += '<td class="ICDText">  <input  value="' + data[i].suppliesDescription + '" type="text" onfocus="Oasis.SupplyDescription($(this));"  class="suppliesDescription"/> </td>';
            tds += '<td><input  value="' + data[i].suppliesCode + '" type="text" onfocus="Oasis.SupplyCode($(this));" class="suppliesCode"/> </td>';
            tds += '<td><input  value="' + data[i].supplyQuantity + '" type="text" class="supplyQuantity"/> </td>';
            tds += '<td><input type="button" onclick="Oasis.DeleteRow($(this));" value="Delete" /> </td>';
            tds += '</tr>';
            tableRow += tds;
        }
        if ($('tbody', table).length > 0) {
            $('tbody', table).append(tableRow);
        } else {
            $(table).append(tableRow);
        }
    }
    ,
    SupplyCode: function(control) {
        $(control).autocomplete('/LookUp/Supplies', {
            max: 20,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Code + '-' + row.Description;
            },
            width: 400,
            extraParams: { "type": "Code" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val(Oasis.stripDecimal(row.Code))
                $(this).parent().parent().find('.suppliesDescription').val(row.Description);
            }
        });

    },
    SupplyDescription: function(control) {
        $(control).autocomplete('/LookUp/Supplies', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Code + '-' + row.Description;
            },
            width: 500,
            extraParams: { "type": "Description" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val(row.Description)
                $(this).parent().parent().find('.suppliesCode').val(Oasis.stripDecimal(row.Code));
            }
        });
    }
    ,
    supplyInputFix: function(assessmentType, tableName) {

        var tableTr = $('tbody tr', $(tableName));
        var len = tableTr.length;
        var i = 1;
        var jsonData = '{ "Supply": [';
        $(tableTr).each(function() {
            jsonData += '{"suppliesDescription":"' + $(this).find('.suppliesDescription').val() + '","suppliesCode":"' + $(this).find('.suppliesCode').val() + '","supplyQuantity":"' + $(this).find('.supplyQuantity').val() + '"}';
            if (len > i) {
                jsonData += ',';
            }
            i++
        });
        jsonData += '] }';
        $("#" + assessmentType + "_GenericSupply").val(jsonData);
    }
    ,
    ClearRows: function(table) {
        $('tbody tr', table).each(function() {
            $(this).remove();
        });
    },
    DeleteRow: function(control) {
        $(control).parent().parent().remove();
    },
    BradenScaleScore: function(assessmentType) {
        var first = $("#" + assessmentType + "_485BradenScaleSensory").val();
        var second = $("#" + assessmentType + "_485BradenScaleMoisture").val();
        var third = $("#" + assessmentType + "_485BradenScaleActivity").val();
        var fourth = $("#" + assessmentType + "_485BradenScaleMobility").val();
        var fifth = $("#" + assessmentType + "_485BradenScaleNutrition").val();
        var sixth = $("#" + assessmentType + "_485BradenScaleFriction").val();
        var total = parseInt(first) + parseInt(second) + parseInt(third) + parseInt(fourth) + parseInt(fifth) + parseInt(sixth); //if you require floating point numbers try parseFloat
        $("#" + assessmentType + "_485BradenScaleTotal").val(total);
        Oasis.displayRisk(assessmentType, total);

    },
    displayRisk: function(assessmentType, total) {

        var resultText = '';
        if (total >= 19)
            resultText = 'Patients with this value are generally considered not at risk';
        else if (total >= 15 && total <= 18)
            resultText = 'At risk to develop pressure ulcers';
        else if (total >= 13 && total <= 14)
            resultText = 'Moderate risk for developing pressure ulcers';
        else if (total >= 10 && total <= 12)
            resultText = 'High risk for developing pressure ulcers';
        else if (total <= 9)
            resultText = 'Very high risk for developing pressure ulcers';
        $("#" + assessmentType + "_485ResultBox").html(resultText);
    },
    BradenScaleOnchange: function(assessmentType, tableId) {
        $('tr td select.scale', $(tableId)).change(function() {
            Oasis.BradenScaleScore(assessmentType);
        })

    }
     ,
    CalculateNutritionScore: function(assessmentType) {

        $("input[name=" + assessmentType + "_GenericGoodNutritionScore][type=checkbox]").each(function() {
            $(this).attr('disabled', 'disabled');
        });
        var score = 0;
        $("input[name=" + assessmentType + "_GenericNutritionalHealth][type=checkbox]").each(function() {
            var itemScore = parseInt($(this).parents('td').next().text());

            if ($(this).attr('checked') && !isNaN(itemScore)) {

                score = score + itemScore;
            }

        });
        if (score <= 25) {

            $("input[name=" + assessmentType + "_GenericGoodNutritionScore][type=checkbox]").each(function() {
                $(this).removeAttr('checked');
            });

            $("input[name='" + assessmentType + "_GenericGoodNutritionScore'][type='checkbox'][value='1']").attr('checked', 'checked');
        }
        else if (score <= 55) {
            $("input[name=" + assessmentType + "_GenericGoodNutritionScore][type=checkbox]").each(function() {
                $(this).removeAttr('checked');
            });
            $("input[name='" + assessmentType + "_GenericGoodNutritionScore'][value='2']").attr('checked', 'checked');
        }
        else if (score <= 100) {
            $("input[name=" + assessmentType + "_GenericGoodNutritionScore][type=checkbox]").each(function() {

                $(this).removeAttr('checked');

            });
            $("input[name='" + assessmentType + "_GenericGoodNutritionScore'][value='3']").attr('checked', 'checked');
        }
    }
    ,
    Validate: function(id, assessmentType) {
        $("#validation").clearForm();
        var control = $('tbody', $("#validationErrorTable"));
        control.empty();
        var data = 'Id=' + id + "&assessmentType=" + assessmentType;
        $.ajax({
            url: '/Oasis/Validate',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                $("#numberOfErrors").text('');
                $("#numberOfErrors").text(result.length);
                if (result.Count == 0) {
                    control.append('<tr><td>' + result.Message + '</td></tr>');
                }
                else if (result.Count > 0) {
                    $.each(result.validationError, function(index, itemData) {
                        control.append('<tr><td>' + itemData.ErrorDup + '</td><td>' + itemData.Description + '</td></tr>');
                    });
                }

            }
        });

    }
}
