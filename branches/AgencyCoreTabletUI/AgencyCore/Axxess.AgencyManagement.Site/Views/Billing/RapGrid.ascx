﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<span style="padding: 50px;"><b style="font-weight: bolder;">Rap</b> </span>
<table class="claim">
    <thead>
        <tr>
            <th>
            </th>
            <th>
            </th>
            <th>
                Patient Name
            </th>
            <th>
                Patient ID
            </th>
            <th>
                Episode Date
            </th>
            <th>
                Oasis
            </th>
            <th>
                First Billable Visit
            </th>
            <th>
                Verified
            </th>
        </tr>
    </thead>
    <tbody>
        <% var raps = Model.Raps.Where(c => !c.IsGenerated); %>
        <% int i = 1;

           foreach (var rap in raps)
           {%>
        <% if (i % 2 == 0)
           {%>
        <tr class="even">
            <%}
           else
           { %>
            <tr class="odd">
                <%} %>
                <td>
                    <%=i %>
                </td>
                <td>
                   
                    <% if (rap.IsVerified)
                       { %>
                    <input name="RapSelected" type="checkbox" value='<%=rap.Id%>' />
                    <% }
                       else
                       { %>
                    <input name="RapSelected" type="checkbox" value='<%=rap.Id%>' disabled="disabled" />
                    <% }%>
                </td>
                <td>
                    <% if (rap.IsOasisComplete && rap.IsFirstBillableVisit)
                       {%>
                    <a onclick="Billing.loadRap('<%=rap.EpisodeId%>','<%=rap.PatientId%>');">
                        <%=rap.DisplayName %></a>
                    <%}
                       else
                       { %>
                    <a onclick="alert('It is not Complete.');">
                        <%=rap.DisplayName%></a>
                    <%} %>
                </td>
                <td>
                    <%=rap.PatientIdNumber %>
                </td>
                <td>
                    <% if (rap.EpisodeStartDate != null)
                       {%>
                    <%=rap.EpisodeStartDate.ToShortDateString()%>
                    <%} %>
                    <% if (rap.EpisodeEndDate != null)
                       {%>
                    -
                    <%=rap.EpisodeEndDate.ToShortDateString()%>
                    <%} %>
                </td>
                <td>
                    <%if (rap.IsOasisComplete)
                      { %>
                    <img src="../../Images/GreenIcon.png" width="16" height="16" alt="" border="0">
                    <%}
                      else
                      { %>
                    <img src="../../Images/RedIcon.png" width="16" height="16" alt="" border="0">
                    <%} %>
                </td>
                <td>
                    <%if (rap.IsFirstBillableVisit)
                      { %>
                    <img src="../../images/GreenIcon.png" width="16" height="16" alt="" border="0">
                    <%}
                      else
                      { %>
                    <img src="../../Images/RedIcon.png" width="16" height="16" alt="" border="0">
                    <%} %>
                </td>
                <td>
                    <% if (rap.IsVerified)
                       {%>
                    <img src="../../images/GreenIcon.png" width="16" height="16" alt="" border="0">
                    <%}
                       else
                       { %>
                    <img src="../../Images/RedIcon.png" width="16" height="16" alt="" border="0">
                    <%} %>
                </td>
            </tr>
            <%i++;
           } %>
    </tbody>
</table>
