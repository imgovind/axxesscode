﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>

<div id="Billing_CenterContent">
    <div class="row">
        <% Html.RenderPartial("RapGrid", Model); %>
    </div>
    <div class="row">
        <% Html.RenderPartial("FinalGrid", Model); %>
    </div>
</div>
