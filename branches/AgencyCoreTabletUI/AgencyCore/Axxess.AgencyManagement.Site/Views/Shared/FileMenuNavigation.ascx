﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="abs" id="bar_top">
    <span class="float_right" id="accountLinks">Welcome<strong>
        <%= Axxess.AgencyManagement.App.Current.User.DisplayName %></strong> | <a href="javascript:void(0);" onclick="Message.Load();">Inbox</a> | <a href="/Logout">Logout</a></span>
    <div>
        <div class="arrowlistmenu abs">
            <ul id="topMenu">
                <li><a href="javascript:void(0);">My Profile</a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);">Homepage</a> </li>
                        <li><a href="javascript:void(0);" onclick="Employee.Profile();JQD.open_window('#userprofile_window');">
                            Edit Profile</a> </li>
                        <li><a href="/ChangePassword">Change Password</a> </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="m3">Patients</a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);" onclick="Patient.loadPatientCenter();">Patient
                            Center</a></li>
                        <li><a href="javascript:void(0);" onclick="Patient.loadNewPatient();">
                            New Patient</a> </li>
                        <li><a href="javascript:void(0);" onclick="Referral.loadNewReferral();">
                            New Referral</a> </li>
                        <li><a href="javascript:void(0);" onclick="Referral.loadExistingReferral();">Existing
                            Referral</a> </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="m4">OASIS</a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);" onclick="alert('schedule oasis');">Schedule Oasis</a>
                        </li>
                        <li><a href="javascript:void(0);" onclick="JQD.open_window('#submitoasis');">Submit
                            Oasis</a> </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="m5">Billing</a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);" onclick="Billing.loadBillingCenter();">Billing Center</a>
                        </li>
                         <li><a href="javascript:void(0);" onclick="Billing.loadBillingHistory();">Billing History</a>
                        </li>
                         <li><a href="javascript:void(0);" onclick="Billing.loadPendingClaims();">Pending Claims</a>
                        </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="m6">Schedules</a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);" onclick="Schedule.loadMasterSchedulingCenter();">Schedule
                            Center</a> </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);">Reports</a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);" onclick="JQD.open_window_external('report', function() { Report.init(); });">
                            Report Center</a> </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="m7">Administration</a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);" class="m10">Add
                            <img src="/Images/gui/next.gif" style="float: right;" alt="employee" /></a>
                            <ul>
                            
                                <li><a href="javascript:void(0);" onclick="Agency.New(); JQD.open_window('#newagency');">
                                    Agency</a></li>
                                <li><a href="javascript:void(0);" onclick="Employee.New(); JQD.open_window('#newemployee');">
                                    User</a> </li>
                                <li><a href="javascript:void(0);" onclick="Patient.New(); JQD.open_window('#newpatient');">
                                    Patient</a></li>
                                <li><a href="javascript:void(0);" onclick="Patient.NewPhysicianContact('<%=Guid.Empty %>');">
                                    Physician</a> </li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0);" class="m10">Lists<img src="/Images/gui/next.gif"
                            style="float: right;" alt="employee" /></a>
                            <ul>
                                <li><a href="javascript:void(0);" onclick="JQD.open_window('#existingPatientList');">
                                    Patients</a></li>
                                <li><a href="javascript:void(0);" onclick="JQD.open_window('#agencyUserList');">
                                    Users</a> </li>
                                    <li><a href="javascript:void(0);" onclick="JQD.open_window('#existingPhysicianList');">
                                    Physicians</a> </li>
                                <li><a href="javascript:void(0);" onclick="JQD.open_window('#diagnosisList');">ICD-9
                                    Diagnosis Codes</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
