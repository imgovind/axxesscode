﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PasswordChange>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Change Password - Axxess Home Health Management System</title>
    <link type="text/css" href="/Content/account.css" rel="Stylesheet" />
</head>
<body>
    <div id="changePassword-wrapper">
        <div class="box-header changePassword">AXXESS&trade; CHANGE PASSWORD
        </div>
        <div class="box">
            <div id="messages" class="notification info">
                New passwords are required to be a minimum of 8 characters in length.
            </div>
            <% using (Html.BeginForm("ChangePassword", "Account", FormMethod.Post, new { @id = "changePasswordForm", @class = "changePassword" })) %>
            <% { %>
            <div class="row">
                <%= Html.LabelFor(m => m.OldPassword) %>
                <%= Html.PasswordFor(m => m.OldPassword, new { @class = "required" }) %>
            </div>
            <div class="row">
                <%= Html.LabelFor(m => m.NewPassword) %>
                <%= Html.PasswordFor(m => m.NewPassword, new { @class = "required" }) %>
                <div id="iSM">
                    <ul class="weak">
                        <li id="iWeak">WEAK</li>
                        <li id="iMedium">MEDIUM</li>
                        <li id="iStrong"">STRONG</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <%= Html.LabelFor(m => m.ConfirmPassword) %>
                <%= Html.PasswordFor(m => m.ConfirmPassword, new { @class = "required" }) %>
            </div>
            <div class="row tr">
                <input type="submit" value="Change" class="button" style="width: 90px!important;" />
            </div>
            <% } %>
        </div>
    </div>
    <% Html.Telerik().ScriptRegistrar().jQuery(true)
         .DefaultGroup(group => group
             .Add("/Plugins/jquery.validate.js")
             .Add("/Plugins/jquery.form.js")
             .Add("/Plugins/jquery.blockUI.js")
             .Add("/Plugins/jquery.passwordstrength.js")
             .Add("/Models/Utility.js")
             .Add("/Models/Account.js")
             .Compress(true))
        .OnDocumentReady(() =>
        { 
    %>
    ChangePassword.Init();
    <% 
        }).Render(); %>
</body>
</html>
