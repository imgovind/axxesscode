﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationRespiratoryForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Respiratory
            </th>
        </tr>
        <%string[] respiratoryCondition = data.ContainsKey("GenericRespiratoryCondition") && data["GenericRespiratoryCondition"].Answer != "" ? data["GenericRespiratoryCondition"].Answer.Split(',') : null; %>
        <tr>
            <td colspan="2">
                <input type="hidden" name="Recertification_GenericRespiratoryCondition" value=" " />
                <input name="Recertification_GenericRespiratoryCondition" value="1" type="checkbox" '<% if(  respiratoryCondition!=null && respiratoryCondition.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                WNL (Within Normal Limits)
            </td>
        </tr>
        <tr>
            <td width="50%" rowspan="2">
                <input name="Recertification_GenericRespiratoryCondition" value="2" type="checkbox" '<% if(  respiratoryCondition!=null && respiratoryCondition.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                <strong>Lung Sounds:</strong>
                <%string[] respiratorySounds = data.ContainsKey("GenericRespiratorySounds") && data["GenericRespiratorySounds"].Answer != "" ? data["GenericRespiratorySounds"].Answer.Split(',') : null; %>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">
                        <input type="hidden" name="Recertification_GenericRespiratorySounds" value=" " />
                        <input name="Recertification_GenericRespiratorySounds" value="1" type="checkbox" '<% if(  respiratorySounds!=null && respiratorySounds.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        CTA</li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericLungSoundsCTAText", data.ContainsKey("GenericLungSoundsCTAText") ? data["GenericLungSoundsCTAText"].Answer : "", new { @id = "Recertification_GenericLungSoundsCTAText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">
                        <input name="Recertification_GenericRespiratorySounds" value="2" type="checkbox" '<% if(  respiratorySounds!=null && respiratorySounds.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Rales</li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericLungSoundsRalesText", data.ContainsKey("GenericLungSoundsRalesText") ? data["GenericLungSoundsRalesText"].Answer : "", new { @id = "Recertification_GenericLungSoundsRalesText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">
                        <input name="Recertification_GenericRespiratorySounds" value="3" type="checkbox" '<% if(  respiratorySounds!=null && respiratorySounds.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Rhonchi </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericLungSoundsRhonchiText", data.ContainsKey("GenericLungSoundsRhonchiText") ? data["GenericLungSoundsRhonchiText"].Answer : "", new { @id = "Recertification_GenericLungSoundsRhonchiText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">
                        <input name="Recertification_GenericRespiratorySounds" value="4" type="checkbox" '<% if(  respiratorySounds!=null && respiratorySounds.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Wheezes</li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericLungSoundsWheezesText", data.ContainsKey("GenericLungSoundsWheezesText") ? data["GenericLungSoundsWheezesText"].Answer : "", new { @id = "Recertification_GenericLungSoundsWheezesText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">
                        <input name="Recertification_GenericRespiratorySounds" value="5" type="checkbox" '<% if(  respiratorySounds!=null && respiratorySounds.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Crackles </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericLungSoundsCracklesText", data.ContainsKey("GenericLungSoundsCracklesText") ? data["GenericLungSoundsCracklesText"].Answer : "", new { @id = "Recertification_GenericLungSoundsCracklesText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">
                        <input name="Recertification_GenericRespiratorySounds" value="6" type="checkbox" '<% if(  respiratorySounds!=null && respiratorySounds.Contains("6")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Diminished </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericLungSoundsDiminishedText", data.ContainsKey("GenericLungSoundsDiminishedText") ? data["GenericLungSoundsDiminishedText"].Answer : "", new { @id = "Recertification_GenericLungSoundsDiminishedText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">
                        <input name="Recertification_GenericRespiratorySounds" value="7" type="checkbox" '<% if(  respiratorySounds!=null && respiratorySounds.Contains("7")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Absent </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericLungSoundsAbsentText", data.ContainsKey("GenericLungSoundsAbsentText") ? data["GenericLungSoundsAbsentText"].Answer : "", new { @id = "Recertification_GenericLungSoundsAbsentText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">
                        <input name="Recertification_GenericRespiratorySounds" value="8" type="checkbox" '<% if(  respiratorySounds!=null && respiratorySounds.Contains("8")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Stridor </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericLungSoundsStridorText", data.ContainsKey("GenericLungSoundsStridorText") ? data["GenericLungSoundsStridorText"].Answer : "", new { @id = "Recertification_GenericLungSoundsStridorText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
            </td>
            <td width="50%">
                <ul>
                    <li>
                        <input name="Recertification_GenericRespiratoryCondition" value="3" type="checkbox" '<% if(  respiratoryCondition!=null && respiratoryCondition.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        <strong>Sputum:</strong>&nbsp; </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">Enter amount: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericSputumAmount", data.ContainsKey("GenericSputumAmount") ? data["GenericSputumAmount"].Answer : "", new { @id = "Recertification_GenericSputumAmount", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">Describe color, consistency, and odor: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericSputumColorConsistencyOdor", data.ContainsKey("GenericSputumColorConsistencyOdor") ? data["GenericSputumColorConsistencyOdor"].Answer : "", new { @id = "Recertification_GenericSputumColorConsistencyOdor", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericRespiratoryCondition" value="4" type="checkbox" '<% if(  respiratoryCondition!=null && respiratoryCondition.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        <strong>02</strong> </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">At: </li>
                    <li>
                        <%=Html.TextBox("Recertification_Generic02AtText", data.ContainsKey("Generic02AtText") ? data["Generic02AtText"].Answer : "", new { @id = "Recertification_Generic02AtText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer"></li>
                    <li class="spacer">LPM via:</li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericLPMVia", data.ContainsKey("GenericLPMVia") ? data["GenericLPMVia"].Answer : "", new { @id = "Recertification_GenericLPMVia", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li></li>
                    <li class="spacer">
                        <input name="Recertification_GenericRespiratoryCondition" value="5" type="checkbox" '<% if(  respiratoryCondition!=null && respiratoryCondition.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        02 Sat:</li>
                    <li>
                        <%=Html.TextBox("Recertification_Generic02SatText", data.ContainsKey("Generic02SatText") ? data["Generic02SatText"].Answer : "", new { @id = "Recertification_Generic02SatText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li></li>
                    <li class="spacer"></li>
                    <li>
                        <%var satList = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Room Air", Value = "Room Air" }, new SelectListItem { Text = "02", Value = "02" } }, "Value", "Text", data.ContainsKey("Generic02SatList") && data["Generic02SatList"].Answer != "" ? data["Generic02SatList"].Answer : "0");%>
                        <%= Html.DropDownList("Recertification_Generic02SatList", satList)%>
                    </li>
                </ul>
                <ul class="columns">
                    <li></li>
                    <li class="spacer">
                        <input name="Recertification_GenericRespiratoryCondition" value="6" type="checkbox" '<% if(  respiratoryCondition!=null && respiratoryCondition.Contains("6")  ){ %>checked="checked"<% }%>'" />&nbsp;Nebulizer:
                    </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericNebulizerText", data.ContainsKey("GenericNebulizerText") ? data["GenericNebulizerText"].Answer : "", new { @id = "Recertification_GenericNebulizerText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ul class="columns">
                    <li></li>
                    <li class="spacer">
                        <input name="Recertification_GenericRespiratoryCondition" value="7" type="checkbox" '<% if(  respiratoryCondition!=null && respiratoryCondition.Contains("7")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Cough:</li>
                    <li>
                        <%var coughList = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "Productive", Value = "Productive" }, new SelectListItem { Text = "Nonproductive", Value = "Nonproductive" } }, "Value", "Text", data.ContainsKey("GenericCoughList") && data["GenericCoughList"].Answer != "" ? data["GenericCoughList"].Answer : "0");%>
                        <%= Html.DropDownList("Recertification_GenericCoughList", coughList)%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Comments:<br />
                <%=Html.TextArea("Recertification_GenericRespiratoryComments", data.ContainsKey("GenericRespiratoryComments") ? data["GenericRespiratoryComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericRespiratoryComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1400) When is the patient dyspneic or noticeably Short of Breath?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("Recertification_M1400PatientDyspneic", " ", new { @id = "" })%>
            <%=Html.RadioButton("Recertification_M1400PatientDyspneic", "00", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Patient is not short of breath<br />
            <%=Html.RadioButton("Recertification_M1400PatientDyspneic", "01", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - When walking more than 20 feet, climbing stairs<br />
            <%=Html.RadioButton("Recertification_M1400PatientDyspneic", "02", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - With moderate exertion (e.g., while dressing, using commode or bedpan, walking
            distances less than 20 feet)<br />
            <%=Html.RadioButton("Recertification_M1400PatientDyspneic", "03", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - With minimal exertion (e.g., while eating, talking, or performing other ADLs)
            or with agitation<br />
            <%=Html.RadioButton("Recertification_M1400PatientDyspneic", "04", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - At rest (during day or night)<br />
        </div>
    </div>
</div>

<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <%string[] respiratoryInterventions = data.ContainsKey("485RespiratoryInterventions") && data["485RespiratoryInterventions"].Answer != "" ? data["485RespiratoryInterventions"].Answer.Split(',') : null;  %>
            <td width="15px">
                <input type="hidden" name="Recertification_485RespiratoryInterventions" value=" " />
                <input name="Recertification_485RespiratoryInterventions" value="1" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("1")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct caregiver on pulmonary toilet including percussion therapy and postural
                drainage (freq)
                <%=Html.TextBox("Recertification_485InstructPulmonaryToiletFrequency", data.ContainsKey("485InstructPulmonaryToiletFrequency") ? data["485InstructPulmonaryToiletFrequency"].Answer : "", new { @id = "Recertification_485InstructPulmonaryToiletFrequency", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="2" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("2")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to perform pulmonary toilet including percussion therapy and postural drainage
                (freq)
                <%=Html.TextBox("Recertification_485PerformPulmonaryToiletFrequency", data.ContainsKey("485PerformPulmonaryToiletFrequency") ? data["485PerformPulmonaryToiletFrequency"].Answer : "", new { @id = "Recertification_485PerformPulmonaryToiletFrequency", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="3" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("3")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructNebulizerUsePerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructNebulizerUsePerson") && data["485InstructNebulizerUsePerson"].Answer != "" ? data["485InstructNebulizerUsePerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructNebulizerUsePerson", instructNebulizerUsePerson)%>
                on proper use of nebulizer/inhaler, and assess return demonstration
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="4" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("4")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess O2 saturation on room air (freq)
                <%=Html.TextBox("Recertification_485AssessOxySaturationFrequency", data.ContainsKey("485AssessOxySaturationFrequency") ? data["485AssessOxySaturationFrequency"].Answer : "", new { @id = "Recertification_485AssessOxySaturationFrequency", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="5" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("5")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess O2 saturation on O2 @
                <%=Html.TextBox("Recertification_485AssessOxySatOnOxyAt", data.ContainsKey("485AssessOxySatOnOxyAt") ? data["485AssessOxySatOnOxyAt"].Answer : "", new { @id = "Recertification_485AssessOxySatOnOxyAt", @size = "15", @maxlength = "15" })%>
                LPM/
                <%=Html.TextBox("Recertification_485AssessOxySatLPM", data.ContainsKey("485AssessOxySatLPM") ? data["485AssessOxySatLPM"].Answer : "", new { @id = "Recertification_485AssessOxySatLPM", @size = "15", @maxlength = "15" })%>
                (freq)
                <%=Html.TextBox("Recertification_485AssessOxySatOnOxyFrequency", data.ContainsKey("485AssessOxySatOnOxyFrequency") ? data["485AssessOxySatOnOxyFrequency"].Answer : "", new { @id = "Recertification_485AssessOxySatOnOxyFrequency", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="6" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("6")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructSobFactorsPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructSobFactorsPerson") && data["485InstructSobFactorsPerson"].Answer != "" ? data["485InstructSobFactorsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructSobFactorsPerson", instructSobFactorsPerson)%>
                on factors that contribute to SOB, including avoiding outdoors on poor air quality
                days. Avoid leaving windows open when outside temperature is above
                <%=Html.TextBox("Recertification_485InstructSobFactorsTemp", data.ContainsKey("485InstructSobFactorsTemp") ? data["485InstructSobFactorsTemp"].Answer : "", new { @id = "Recertification_485InstructSobFactorsTemp", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="7" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("7")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructAvoidSmokingPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructAvoidSmokingPerson") && data["485InstructAvoidSmokingPerson"].Answer != "" ? data["485InstructAvoidSmokingPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructAvoidSmokingPerson", instructAvoidSmokingPerson)%>
                to avoid smoking or allowing people to smoke in patient's home. Instruct patient
                to avoid irritants/allergens known to increase SOB
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="8" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("8")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient on pursed lip breathing techniques
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="9" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("9")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient on energy conserving measures including frequent rest periods,
                small frequent meals, avoiding large meals/overeating, controlling stress
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="10" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("10")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient on proper use of nebulizer treatment with
                <%=Html.TextBox("Recertification_485InstructNebulizerTreatmentType", data.ContainsKey("485InstructNebulizerTreatmentType") ? data["485InstructNebulizerTreatmentType"].Answer : "", new { @id = "Recertification_485InstructNebulizerTreatmentType", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="11" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("11")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient on proper use of
                <%=Html.TextBox("Recertification_485InstructProperUseOfType", data.ContainsKey("485InstructProperUseOfType") ? data["485InstructProperUseOfType"].Answer : "", new { @id = "Recertification_485InstructProperUseOfType", @size = "15", @maxlength = "15" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="12" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("12")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct caregiver on proper suctioning technique
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="13" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("13")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var recognizePulmonaryDysfunctionPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485RecognizePulmonaryDysfunctionPerson") && data["485RecognizePulmonaryDysfunctionPerson"].Answer != "" ? data["485RecognizePulmonaryDysfunctionPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485RecognizePulmonaryDysfunctionPerson", recognizePulmonaryDysfunctionPerson)%>
                on methods to recognize pulmonary dysfunction and relieve complications
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryInterventions" value="14" type="checkbox" '<% if(  respiratoryInterventions!=null && respiratoryInterventions.Contains("14")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Report to physician O2 saturation less than
                <%=Html.TextBox("Recertification_485OxySaturationLessThanPercent", data.ContainsKey("485OxySaturationLessThanPercent") ? data["485OxySaturationLessThanPercent"].Answer : "", new { @id = "Recertification_485OxySaturationLessThanPercent", @size = "15", @maxlength = "15" })%>
                %
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Orders: &nbsp;
                <%var respiratoryOrderTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485RespiratoryOrderTemplates") && data["485RespiratoryOrderTemplates"].Answer != "" ? data["485RespiratoryOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485RespiratoryOrderTemplates", respiratoryOrderTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485RespiratoryInterventionComments", data.ContainsKey("485RespiratoryInterventionComments") ? data["485RespiratoryInterventionComments"].Answer : "", 2, 70, new { @id = "Recertification_485RespiratoryInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table bprder="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <%string[] respiratoryGoals = data.ContainsKey("485RespiratoryGoals") && data["485RespiratoryGoals"].Answer != "" ? data["485RespiratoryGoals"].Answer.Split(',') : null; %>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485RespiratoryGoals" value=" " />
                <input name="Recertification_485RespiratoryGoals" value="1" type="checkbox" '<% if(  respiratoryGoals!=null && respiratoryGoals.Contains("1")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient's respiratory rate will remain within established parameters during the
                episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryGoals" value="2" type="checkbox" '<% if(  respiratoryGoals!=null && respiratoryGoals.Contains("2")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will be free from signs and symptoms of respiratory distress during the
                episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryGoals" value="3" type="checkbox" '<% if(  respiratoryGoals!=null && respiratoryGoals.Contains("3")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient and caregiver will verbalize an understanding of factors that contribute
                to shortness of breath by:
                <%=Html.TextBox("Recertification_485VerbalizeFactorsSobDate", data.ContainsKey("485VerbalizeFactorsSobDate") ? data["485VerbalizeFactorsSobDate"].Answer : "", new { @id = "Recertification_485VerbalizeFactorsSobDate", @size = "10", @maxlength = "10" })%>
               </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryGoals" value="4" type="checkbox" '<% if(  respiratoryGoals!=null && respiratoryGoals.Contains("4")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will demonstrate proper pursed lip breathing techniques by
                 <%=Html.TextBox("Recertification_485DemonstrateLipBreathingDate", data.ContainsKey("485DemonstrateLipBreathingDate") ? data["485DemonstrateLipBreathingDate"].Answer : "", new { @id = "Recertification_485DemonstrateLipBreathingDate", @size = "10", @maxlength = "10" })%>
                  </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryGoals" value="5" type="checkbox" '<% if(  respiratoryGoals!=null && respiratoryGoals.Contains("5")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will verbalize an understanding of energy conserving measures by:
               <%=Html.TextBox("Recertification_485VerbalizeEnergyConserveDate", data.ContainsKey("485VerbalizeEnergyConserveDate") ? data["485VerbalizeEnergyConserveDate"].Answer : "", new { @id = "Recertification_485VerbalizeEnergyConserveDate", @size = "10", @maxlength = "10" })%>
               </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryGoals" value="6" type="checkbox" '<% if(  respiratoryGoals!=null && respiratoryGoals.Contains("6")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeSafeOxyManagementPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485VerbalizeSafeOxyManagementPerson") && data["485VerbalizeSafeOxyManagementPerson"].Answer != "" ? data["485VerbalizeSafeOxyManagementPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485VerbalizeSafeOxyManagementPerson", verbalizeSafeOxyManagementPerson)%>
                will verbalize and demonstrate safe management of oxygen by:
              <%=Html.TextBox("Recertification_485VerbalizeSafeOxyManagementPersonDate", data.ContainsKey("485VerbalizeSafeOxyManagementPersonDate") ? data["485VerbalizeSafeOxyManagementPersonDate"].Answer : "", new { @id = "Recertification_485VerbalizeSafeOxyManagementPersonDate", @size = "10", @maxlength = "10" })%>
                </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryGoals" value="7" type="checkbox" '<% if(  respiratoryGoals!=null && respiratoryGoals.Contains("7")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will return demonstrate proper use of nebulizer treatment by
                 <%=Html.TextBox("Recertification_485DemonstrateNebulizerUseDate", data.ContainsKey("485DemonstrateNebulizerUseDate") ? data["485DemonstrateNebulizerUseDate"].Answer : "", new { @id = "Recertification_485DemonstrateNebulizerUseDate", @size = "10", @maxlength = "10" })%>
               </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485RespiratoryGoals" value="8" type="checkbox" '<% if(  respiratoryGoals!=null && respiratoryGoals.Contains("8")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will demonstrate proper use of
                <%=Html.TextBox("Recertification_485DemonstrateProperUseOfType", data.ContainsKey("485DemonstrateProperUseOfType") ? data["485DemonstrateProperUseOfType"].Answer : "", new { @id = "Recertification_485DemonstrateProperUseOfType", @size = "15", @maxlength = "15" })%>
                 by:
                 <%=Html.TextBox("Recertification_485DemonstrateNebulizerUseDate", data.ContainsKey("485DemonstrateProperUseOfTypeDate") ? data["485DemonstrateProperUseOfTypeDate"].Answer : "", new { @id = "Recertification_485DemonstrateProperUseOfTypeDate", @size = "10", @maxlength = "10" })%>
               </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Goals: &nbsp;
                <%var respiratoryGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485RespiratoryGoalTemplates") && data["485RespiratoryGoalTemplates"].Answer != "" ? data["485RespiratoryGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485RespiratoryGoalTemplates", respiratoryGoalTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485RespiratoryGoalComments", data.ContainsKey("485RespiratoryGoalComments") ? data["485RespiratoryGoalComments"].Answer : "", 2, 70, new { @id = "Recertification_485RespiratoryGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
