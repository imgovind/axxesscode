﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencySensoryStatusForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>




<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1230) Speech and Oral (Verbal) Expression of Language (in patient's own language):
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1230SpeechAndOral", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "00", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Expresses complex ideas, feelings, and needs clearly, completely, and
                easily in all situations with no observable impairment.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "01", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional
                errors in word choice, grammar or speech intelligibility; needs minimal prompting
                or assistance).<br />
                <%=Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "02", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Expresses simple ideas or needs with moderate difficulty (needs prompting or assistance,
                errors in word choice, organization or speech intelligibility). Speaks in phrases
                or short sentences.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "03", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Has severe difficulty expressing basic ideas or needs and requires maximal assistance
                or guessing by listener. Speech limited to single words or short phrases.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "04", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Unable to express basic needs even with maximal prompting or assistance but is
                not comatose or unresponsive (e.g., speech is nonsensical or unintelligible).<br />
                <%=Html.RadioButton("DischargeFromAgency_M1230SpeechAndOral", "05", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Patient nonresponsive or unable to speak.
            </div>
        </div>
    </div>
</div>


<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Discharge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
