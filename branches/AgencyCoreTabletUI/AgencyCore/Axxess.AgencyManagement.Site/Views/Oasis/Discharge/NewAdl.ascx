﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyAdlForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1800) Grooming: Current ability to tend safely to personal hygiene needs (i.e.,
                washing face and hands, hair care, shaving or make up, teeth or denture care, fingernail
                care).
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1800Grooming", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1800Grooming", "00", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to groom self unaided, with or without the use of assistive devices or adapted
                methods.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1800Grooming", "01", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Grooming utensils must be placed within reach before able to complete grooming
                activities.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1800Grooming", "02", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Someone must assist the patient to groom self.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1800Grooming", "03", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Patient depends entirely upon someone else for grooming needs.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1810) Current Ability to Dress Upper Body safely (with or without dressing aids)
                including undergarments, pullovers, front-opening shirts and blouses, managing zippers,
                buttons, and snaps:
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1810CurrentAbilityToDressUpper", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1810CurrentAbilityToDressUpper", "00", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to get clothes out of closets and drawers, put them on and remove them from
                the upper body without assistance.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1810CurrentAbilityToDressUpper", "01", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to dress upper body without assistance if clothing is laid out or handed
                to the patient.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1810CurrentAbilityToDressUpper", "02", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Someone must help the patient put on upper body clothing.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1810CurrentAbilityToDressUpper", "03", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Patient depends entirely upon another person to dress the upper body.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1820) Current Ability to Dress Lower Body safely (with or without dressing aids)
                including undergarments, slacks, socks or nylons, shoes:
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1820CurrentAbilityToDressLower", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1820CurrentAbilityToDressLower", "00", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to obtain, put on, and remove clothing and shoes without assistance.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1820CurrentAbilityToDressLower", "01", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to dress lower body without assistance if clothing and shoes are laid out
                or handed to the patient.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1820CurrentAbilityToDressLower", "02", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Someone must help the patient put on undergarments, slacks, socks or nylons, and
                shoes.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1820CurrentAbilityToDressLower", "03", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Patient depends entirely upon another person to dress lower body.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing
                face, washing hands, and shampooing hair).
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "00", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to bathe self in shower or tub independently, including getting in and out
                of tub/shower.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "01", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - With the use of devices, is able to bathe self in shower or tub independently,
                including getting in and out of the tub/shower.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "02", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Able to bathe in shower or tub with the intermittent assistance of another person:<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) for intermittent supervision or encouragement
                or reminders, OR
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) to get in and out of the shower or
                tub, OR<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c) for washing difficult to reach areas.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "03", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Able to participate in bathing self in shower or tub, but requires presence of
                another person throughout the bath for assistance or supervision.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "04", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Unable to use the shower or tub, but able to bathe self independently with or
                without the use of devices at the sink, in chair, or on commode.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "05", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Unable to use the shower or tub, but able to participate in bathing self in bed,
                at the sink, in bedside chair, or on commode, with the assistance or supervision
                of another person throughout the bath.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "06", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "06" ? true : false, new { @id = "" })%>&nbsp;6
                - Unable to participate effectively in bathing and is bathed totally by another
                person.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside
                commode safely and transfer on and off toilet/commode.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1840ToiletTransferring", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1840ToiletTransferring", "00", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to get to and from the toilet and transfer independently with or without
                a device.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1840ToiletTransferring", "01", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - When reminded, assisted, or supervised by another person, able to get to and from
                the toilet and transfer.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1840ToiletTransferring", "02", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Unable to get to and from the toilet but is able to use a bedside commode (with
                or without assistance).<br />
                <%=Html.RadioButton("DischargeFromAgency_M1840ToiletTransferring", "03", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal
                independently.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1840ToiletTransferring", "04", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Is totally dependent in toileting.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1845) Toileting Hygiene: Current ability to maintain perineal hygiene safely,
                adjust clothes and/or incontinence pads before and after using toilet, commode,
                bedpan, urinal. If managing ostomy, includes cleaning area around stoma, but not
                managing equipment.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1845ToiletingHygiene", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1845ToiletingHygiene", "00", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to manage toileting hygiene and clothing management without assistance.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1845ToiletingHygiene", "01", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to manage toileting hygiene and clothing management without assistance if
                supplies/implements are laid out for the patient.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1845ToiletingHygiene", "02", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Someone must help the patient to maintain toileting hygiene and/or adjust clothing.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1845ToiletingHygiene", "03", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Patient depends entirely upon another person to maintain toileting hygiene.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1850) Transferring: Current ability to move safely from bed to chair, or ability
                to turn and position self in bed if patient is bedfast.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1850Transferring", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "00", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to independently transfer.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "01", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to transfer with minimal human assistance or with use of an assistive device.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "02", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Able to bear weight and pivot during the transfer process but unable to transfer
                self.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "03", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Unable to transfer self and is unable to bear weight or pivot when transferred
                by another person.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "04", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Bedfast, unable to transfer but is able to turn and position self in bed.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "05", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Bedfast, unable to transfer and is unable to turn and position self.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing
                position, or use a wheelchair, once in a seated position, on a variety of surfaces.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1860AmbulationLocomotion", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "00", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to independently walk on even and uneven surfaces and negotiate stairs with
                or without railings (i.e., needs no human assistance or assistive device).<br />
                <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "01", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able
                to independently walk on even and uneven surfaces and negotiate stairs with or without
                railings.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "02", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Requires use of a two-handed device (e.g., walker or crutches) to walk alone on
                a level surface and/or requires human supervision or assistance to negotiate stairs
                or steps or uneven surfaces.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "03", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Able to walk only with the supervision or assistance of another person at all
                times.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "04", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Chairfast, unable to ambulate but is able to wheel self independently.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "05", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Chairfast, unable to ambulate and is unable to wheel self.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "06", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "06" ? true : false, new { @id = "" })%>&nbsp;6
                - Bedfast, unable to ambulate or be up in a chair.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1870) Feeding or Eating: Current ability to feed self meals and snacks safely.
                Note: This refers only to the process of eating, chewing, and swallowing, not preparing
                the food to be eaten.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1870FeedingOrEating", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "00", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to independently feed self.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "01", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to feed self independently but requires:<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) meal set-up; OR
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) intermittent assistance or supervision
                from another person; OR<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c) a liquid, pureed or ground meat diet.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "02", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Unable to feed self and must be assisted or supervised throughout the meal/snack.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "03", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Able to take in nutrients orally and receives supplemental nutrients through a
                nasogastric tube or gastrostomy.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "04", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Unable to take in nutrients orally and is fed nutrients through a nasogastric
                tube or gastrostomy.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "05", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Unable to take in nutrients orally or by tube feeding.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1880) Current Ability to Plan and Prepare Light Meals (e.g., cereal, sandwich)
                or reheat delivered meals safely:
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1880AbilityToPrepareLightMeal", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1880AbilityToPrepareLightMeal", "00", data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - (a) Able to independently plan and prepare all light meals for self or reheat
                delivered meals; OR<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b)
                Is physically, cognitively, and mentally able to prepare light meals on a regular
                basis but has not routinely performed light meal preparation in the past (i.e.,
                prior to this home care admission).<br />
                <%=Html.RadioButton("DischargeFromAgency_M1880AbilityToPrepareLightMeal", "01", data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Unable to prepare light meals on a regular basis due to physical, cognitive, or
                mental limitations.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1880AbilityToPrepareLightMeal", "02", data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Unable to prepare any light meals or reheat any delivered meals.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1890) Ability to Use Telephone: Current ability to answer the phone safely, including
                dialing numbers, and effectively using the telephone to communicate.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1890AbilityToUseTelephone", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "00", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to dial numbers and answer calls appropriately and as desired.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "01", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to use a specially adapted telephone (i.e., large numbers on the dial, teletype
                phone for the deaf) and call essential numbers.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "02", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Able to answer the telephone and carry on a normal conversation but has difficulty
                with placing calls.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "03", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Able to answer the telephone only some of the time or is able to carry on only
                a limited conversation.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "04", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Unable to answer the telephone at all but can listen if assisted with equipment.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "05", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Totally unable to use the telephone.<br />
                <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "NA", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
                - Patient does not have a telephone.
            </div>
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Discharge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
