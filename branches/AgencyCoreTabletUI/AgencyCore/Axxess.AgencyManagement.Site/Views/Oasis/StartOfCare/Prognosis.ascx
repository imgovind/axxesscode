﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCarePrognosisForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id)%>
<%= Html.Hidden("StartOfCare_Action", "Edit")%>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "StartOfCare")%>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>
                Prognosis (locator #20)
            </th>
        </tr>
        <tr>
            <td>
                <%=Html.Hidden("StartOfCare_485Prognosis", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485Prognosis", "Guarded", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Guarded" ? true : false, new { @id = "" })%>&nbsp;Guarded
                <%=Html.RadioButton("StartOfCare_485Prognosis", "Poor", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Poor" ? true : false, new { @id = "" })%>&nbsp;Poor
                <%=Html.RadioButton("StartOfCare_485Prognosis", "Fair", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Fair" ? true : false, new { @id = "" })%>&nbsp;Fair
                <%=Html.RadioButton("StartOfCare_485Prognosis", "Good", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Good" ? true : false, new { @id = "" })%>&nbsp;Good
                <%=Html.RadioButton("StartOfCare_485Prognosis", "Excellent", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Excellent" ? true : false, new { @id = "" })%>&nbsp;Excellent
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Advanced Directives
            </th>
        </tr>
        <tr>
            <td>
                Are there any advanced Directives?
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485AdvancedDirectives", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485AdvancedDirectives", "Yes", data.ContainsKey("485AdvancedDirectives") && data["485AdvancedDirectives"].Answer == "Yes" ? true : false, new { @id = "" })%>&nbsp;Yes
                <%=Html.RadioButton("StartOfCare_485AdvancedDirectives", "No", data.ContainsKey("485AdvancedDirectives") && data["485AdvancedDirectives"].Answer == "No" ? true : false, new { @id = "" })%>&nbsp;No
            </td>
        </tr>
        <tr>
            <td>
                Intent:&nbsp;
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485AdvancedDirectivesIntent", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485AdvancedDirectivesIntent", "DNR", data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "DNR" ? true : false, new { @id = "" })%>
                &nbsp;DNR<br />
                <%=Html.RadioButton("StartOfCare_485AdvancedDirectivesIntent", "Living Will", data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "Living Will" ? true : false, new { @id = "" })%>
                &nbsp;Living Will<br />
                <%=Html.RadioButton("StartOfCare_485AdvancedDirectivesIntent", "Medical Power of Attorney", data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "Medical Power of Attorney" ? true : false, new { @id = "" })%>
                &nbsp;Medical Power of Attorney<br />
                <%=Html.RadioButton("StartOfCare_485AdvancedDirectivesIntent", "Other", data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "Other" ? true : false, new { @id = "" })%>&nbsp;Other:
                (specify)
                <%=Html.TextBox("StartOfCare_485AdvancedDirectivesIntentOther", data.ContainsKey("485AdvancedDirectivesIntentOther") ? data["485AdvancedDirectivesIntentOther"].Answer : "", new { @id = "StartOfCare_485AdvancedDirectivesIntentOther",@maxlength="50",@size="50" })%>
            </td>
        </tr>
        <tr>
            <td>
                Copy on file at agency?
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485AdvancedDirectivesCopyOnFile", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485AdvancedDirectivesCopyOnFile", "Yes", data.ContainsKey("485AdvancedDirectivesCopyOnFile") && data["485AdvancedDirectivesCopyOnFile"].Answer == "Yes" ? true : false, new { @id = "" })%>&nbsp;Yes
                <%=Html.RadioButton("StartOfCare_485AdvancedDirectivesCopyOnFile", "No", data.ContainsKey("485AdvancedDirectivesCopyOnFile") && data["485AdvancedDirectivesCopyOnFile"].Answer == "No" ? true : false, new { @id = "" })%>&nbsp;No
            </td>
        </tr>
        <tr>
            <td>
                Patient was provided written and verbal information on Advance Directives
            </td>
            <td>
                <%=Html.Hidden("StartOfCare_485AdvancedDirectivesWrittenAndVerbal", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_485AdvancedDirectivesWrittenAndVerbal", "Yes", data.ContainsKey("485AdvancedDirectivesWrittenAndVerbal") && data["485AdvancedDirectivesWrittenAndVerbal"].Answer == "Yes" ? true : false, new { @id = "" })%>&nbsp;Yes
                <%=Html.RadioButton("StartOfCare_485AdvancedDirectivesWrittenAndVerbal", "No", data.ContainsKey("485AdvancedDirectivesWrittenAndVerbal") && data["485AdvancedDirectivesWrittenAndVerbal"].Answer == "No" ? true : false, new { @id = "" })%>&nbsp;No
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>
                Is the Patient DNR (Do Not Resuscitate)?
            </th>
        </tr>
        <tr>
            <td>
                <%=Html.Hidden("StartOfCare_GenericPatientDNR", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_GenericPatientDNR", "Yes", data.ContainsKey("GenericPatientDNR") && data["GenericPatientDNR"].Answer == "Yes" ? true : false, new { @id = "" })%>&nbsp;Yes
                <%=Html.RadioButton("StartOfCare_GenericPatientDNR", "No", data.ContainsKey("GenericPatientDNR") && data["GenericPatientDNR"].Answer == "No" ? true : false, new { @id = "" })%>&nbsp;No
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <th colspan="5">
                    Functional Limitations (locator #18.A)
                </th>
            </tr>
            <tr>
                <td>
                    <input name="StartOfCare_485FunctionLimitations" value=" " type="hidden" />
                    <input name="StartOfCare_485FunctionLimitations" value="1" type="checkbox" '<% if( data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer == "1" ){ %>checked="checked"<% }%>' />
                    &nbsp; Amputation
                </td>
                <td>
                    <input name="StartOfCare_485FunctionLimitations" value="5" type="checkbox" '<% if( data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer == "5" ){ %>checked="checked"<% }%>'/>
                    &nbsp; Paralysis
                </td>
                <td>
                    <input name="StartOfCare_485FunctionLimitations" value="9" type="checkbox" '<% if( data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer == "9" ){ %>checked="checked"<% }%>'/>
                    &nbsp; Legally Blind
                </td>
                <td>
                    <input name="StartOfCare_485FunctionLimitations" value="2" type="checkbox" '<% if( data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer == "2" ){ %>checked="checked"<% }%>'/>
                    &nbsp; Bowel/Bladder Incontinence
                </td>
                <td>
                    <input name="StartOfCare_485FunctionLimitations" value="6" type="checkbox" '<% if( data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer == "6" ){ %>checked="checked"<% }%>'/>
                    &nbsp; Endurance
                </td>
            </tr>
            <tr>
                <td>
                    <input name="StartOfCare_485FunctionLimitations" value="A" type="checkbox" '<% if( data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer == "A" ){ %>checked="checked"<% }%>'/>
                    &nbsp; Dyspnea
                </td>
                <td>
                    <input name="StartOfCare_485FunctionLimitations" value="3" type="checkbox" '<% if( data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer == "3" ){ %>checked="checked"<% }%>'/>
                    &nbsp; Contracture
                </td>
                <td>
                    <input name="StartOfCare_485FunctionLimitations" value="7" type="checkbox" '<% if( data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer == "7" ){ %>checked="checked"<% }%>'/>
                    &nbsp; Ambulation
                </td>
                <td>
                    <input name="StartOfCare_485FunctionLimitations" value="4" type="checkbox" '<% if( data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer == "4" ){ %>checked="checked"<% }%>'/>
                    &nbsp; Hearing
                </td>
                <td>
                    <input name="StartOfCare_485FunctionLimitations" value="8" type="checkbox" '<% if( data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer == "8" ){ %>checked="checked"<% }%>'/>
                    &nbsp; Speech
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <ul>
                        <li>
                            <input name="StartOfCare_485FunctionLimitations" value="B" type="checkbox" '<% if( data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer == "B" ){ %>checked="checked"<% }%>'/>
                            &nbsp; Other </li>
                        <li>
                           <%=Html.TextArea("StartOfCare_485FunctionLimitationsOther", data.ContainsKey("485FunctionLimitationsOther") ? data["485FunctionLimitationsOther"].Answer : "", 5, 70, new { @id = "StartOfCare_485FunctionLimitationsOther", @style = "width: 99.5%;" })%>
                           
                        </li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="SOC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="SOC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
