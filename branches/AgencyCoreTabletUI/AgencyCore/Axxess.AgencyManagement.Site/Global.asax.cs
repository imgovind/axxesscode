﻿using System;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using System.Threading;
using System.Reflection;
using System.Web.Routing;
using System.Globalization;
using System.Web.SessionState;
using System.Collections.Generic;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.App;
using Axxess.AgencyManagement.App.Security;

namespace Axxess.AgencyManagement.Site
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.RouteExistingFiles = true;
            routes.IgnoreRoute("{file}.txt");
            routes.IgnoreRoute("{file}.htm");
            routes.IgnoreRoute("{file}.html");
            routes.IgnoreRoute("{handler}.axd");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("Scripts/{*pathInfo}");
            routes.IgnoreRoute("Images/{*pathInfo}");
            routes.IgnoreRoute("Content/{*pathInfo}");

            routes.MapRoute(
                "ViewPlanOfCareRoute",
                "Oasis/485/View/{id}",
                new { controller = "Oasis", action = "PlanOfCarePrint", id = Guid.Empty });

            routes.MapRoute(
                "Login",
                "Login",
                new { controller = "Account", action = "LogOn", id = "" }
            );

            routes.MapRoute(
                "Logout",
                "Logout",
                new { controller = "Account", action = "LogOff", id = "" }
            );

            routes.MapRoute(
               "ChangePassword",
               "ChangePassword",
               new { controller = "Account", action = "ChangePassword", id = "" }
           );

            routes.MapRoute(
               "Forgot",
               "Forgot",
               new { controller = "Account", action = "ForgotPassword", id = "" }
           );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            Bootstrapper.Run();

            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (Context.User != null)
            {
                string username = HttpContext.Current.User.Identity.Name;
                IMembershipService membershipService = Container.Resolve<IMembershipService>();

                AxxessPrincipal principal = membershipService.Get(username);

                Thread.CurrentPrincipal = principal;
                HttpContext.Current.User = principal;
            }
        }
    }
}
