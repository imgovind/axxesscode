﻿namespace Axxess.Core.Infrastructure
{
    using MySql.Data.MySqlClient;
    using System.Configuration;

    internal class DatabaseConnection
    {
        private readonly string connectionString;
        private readonly MySqlConnection connection;

        private DatabaseConnection(string connectionString)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            this.connectionString = connectionString;
        }

        private static DatabaseConnection instance;

        public static MySqlConnection GetConnection(string connectionStringName)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            if (instance == null)
            {
                instance = new DatabaseConnection(connectionString);
            }
            return instance.connection;
        }
    }

}
