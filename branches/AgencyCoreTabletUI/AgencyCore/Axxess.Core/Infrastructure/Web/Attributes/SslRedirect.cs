﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Web;
    using System.Web.Mvc;

    public class SslRedirect : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (CoreSettings.UseSSL)
            {
                if (!filterContext.HttpContext.Request.IsSecureConnection)
                {
                    filterContext.Result = new RedirectResult(filterContext.HttpContext.Request.Url.ToString().Replace("http:", "https:"));
                    filterContext.Result.ExecuteResult(filterContext);
                }
                base.OnActionExecuting(filterContext);
            }
        }
    }

}
