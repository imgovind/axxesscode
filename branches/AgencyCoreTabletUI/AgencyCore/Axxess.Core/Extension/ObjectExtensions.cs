﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.ComponentModel;
    using System.Data.Common;
    using System.Data;

    public static class ObjectExtensions
    {
        public static object ChangeTypeTo<T>(this object value)
        {
            Type conversionType = typeof(T);
            return ChangeTypeTo(value, conversionType);
        }

        public static object ChangeTypeTo(this object value, Type conversionType)
        {
            if (conversionType == null)
                throw new ArgumentNullException("conversionType");

            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                    return null;

                NullableConverter nullableConverter = new NullableConverter(conversionType);
                conversionType = nullableConverter.UnderlyingType;
            }
            else if (conversionType == typeof(Guid))
            {
                return new Guid(value.ToString());
            }
            else if (conversionType == typeof(Int64) && value.GetType() == typeof(int))
            {
                throw new InvalidOperationException("Can't convert an Int64 (long) to Int32(int). If you're using SQLite - this is probably due to your PK being an INTEGER, which is 64bit. You'll need to set your key to long.");
            }

            return Convert.ChangeType(value, conversionType);
        }

        public static T Load<T>(this DbDataReader reader, T item)
        {
            Type iType = typeof(T);

            FieldInfo[] cachedFields = iType.GetFields();
            PropertyInfo[] cachedProps = iType.GetProperties();

            PropertyInfo currentProp;
            FieldInfo currentField = null;

            for (int i = 0; i < reader.FieldCount; i++)
            {
                string pName = reader.GetName(i);
                currentProp = cachedProps.SingleOrDefault(x => x.Name.Equals(pName, StringComparison.InvariantCultureIgnoreCase));

                if (currentProp == null)
                    currentField = cachedFields.SingleOrDefault(x => x.Name.Equals(pName, StringComparison.InvariantCultureIgnoreCase));

                if (currentProp != null && !DBNull.Value.Equals(reader.GetValue(i)))
                {
                    Type valueType = reader.GetValue(i).GetType();
                    if (valueType == typeof(Boolean))
                    {
                        string value = reader.GetValue(i).ToString();
                        currentProp.SetValue(item, value == "1" || value == "True", null);
                    }
                    else if (currentProp.PropertyType == typeof(Guid))
                    {
                        currentProp.SetValue(item, reader.GetGuid(i), null);
                    }
                    else if (IsNullableEnum(currentProp.PropertyType))
                    {
                        var nullEnumObjectValue = Enum.ToObject(Nullable.GetUnderlyingType(currentProp.PropertyType), reader.GetValue(i));
                        currentProp.SetValue(item, nullEnumObjectValue, null);
                    }
                    else if (currentProp.PropertyType.IsEnum)
                    {
                        var enumValue = Enum.ToObject(currentProp.PropertyType, reader.GetValue(i));
                        currentProp.SetValue(item, enumValue, null);
                    }
                    else
                    {
                        var val = reader.GetValue(i);
                        var valType = val.GetType();
                        if (currentProp.PropertyType.IsAssignableFrom(valueType))
                        {
                            currentProp.SetValue(item, val, null);
                        }
                        else
                        {
                            currentProp.SetValue(item, val.ChangeTypeTo(currentProp.PropertyType), null);
                        }
                    }
                }
                else if (currentField != null && !DBNull.Value.Equals(reader.GetValue(i)))
                {
                    Type valueType = reader.GetValue(i).GetType();
                    if (valueType == typeof(Boolean))
                    {
                        string value = reader.GetValue(i).ToString();
                        currentField.SetValue(item, value == "1" || value == "True");
                    }
                    else if (currentField.FieldType == typeof(Guid))
                    {
                        currentField.SetValue(item, reader.GetGuid(i));
                    }
                    else if (IsNullableEnum(currentField.FieldType))
                    {
                        var nullEnumObjectValue = Enum.ToObject(Nullable.GetUnderlyingType(currentField.FieldType), reader.GetValue(i));
                        currentField.SetValue(item, nullEnumObjectValue);
                    }
                    else
                    {
                        var val = reader.GetValue(i);
                        var valType = val.GetType();
                        if (currentField.FieldType.IsAssignableFrom(valueType))
                        {
                            currentField.SetValue(item, val);
                        }
                        else
                        {
                            currentField.SetValue(item, val.ChangeTypeTo(currentField.FieldType));
                        }
                    }
                }
            }
            return item;
        }

        internal static bool IsNullableEnum(Type type)
        {
            var enumType = Nullable.GetUnderlyingType(type);

            return enumType != null && enumType.IsEnum;
        }
    }
}
