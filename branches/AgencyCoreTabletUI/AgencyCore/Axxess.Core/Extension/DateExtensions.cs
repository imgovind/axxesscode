﻿namespace Axxess.Core.Extension
{
    using System;

    public static class DateTimeExtensions
    {
        private static readonly DateTime MinDate = new DateTime(1900, 1, 1);
        private static readonly DateTime MaxDate = new DateTime(9999, 12, 31, 23, 59, 59, 999);

        public static bool IsValid(this DateTime target)
        {
            return (target >= MinDate) && (target <= MaxDate);
        }

        public static bool IsValid(this DateTime target, DateTime time)
        {
            return (target >= time) && (target <= MaxDate);
        }

        public static bool IsWeekend(this DateTime target)
        {
            return (target.DayOfWeek == DayOfWeek.Sunday || target.DayOfWeek == DayOfWeek.Saturday);
        }

        public static string ToZeroFilled(this string target)
        {
            if (target.IsNotNullOrEmpty())
            {
                return DateTime.Parse(target).ToString("MM/dd/yyyy");
            }
            return string.Empty;
        }

        public static string ToJavascriptFormat(this DateTime dateTime)
        {
            return dateTime.ToString("MMM d, yyyy");
        }

        public static bool HourToDateTime(this string target, ref DateTime time)
        {
            if (target.IsNotNullOrEmpty())
            {
                return DateTime.TryParse(target, out time); ;
            }
            time = DateTime.Now;
            return false;
        }
            
    }
}
