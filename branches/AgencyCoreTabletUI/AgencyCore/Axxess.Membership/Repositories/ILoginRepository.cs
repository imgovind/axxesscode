﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core.Infrastructure;

    public interface ILoginRepository
    {
        Login Find(Guid id);
        bool Add(Login login);
        bool Delete(Guid loginId);
        ICollection<Login> GetAll();
        Login Find(string emailAddress);
        ICollection<Login> AutoComplete(string searchString);
        bool Update(Login login);

        IList<Login> GetAllByRole(Roles role);
    }
}
