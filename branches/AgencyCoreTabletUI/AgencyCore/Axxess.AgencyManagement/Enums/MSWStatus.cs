﻿namespace Axxess.AgencyManagement.Enums
{
    public enum MSWStatus
    {
        Evaluation,
        Discharge,
        Visit
    }
}
