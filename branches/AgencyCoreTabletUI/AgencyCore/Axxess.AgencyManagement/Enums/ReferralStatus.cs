﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum ReferralStatus : byte
    {
        [Description("Pending")]
        Pending = 1,
        [Description("Admitted")]
        Admitted = 2,
        [Description("Rejected")]
        Rejected = 3,
        [Description("Not Admitted")]
        NonAdmission = 4
    }
}
