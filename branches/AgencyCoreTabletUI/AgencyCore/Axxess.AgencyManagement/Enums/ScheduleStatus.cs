﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;

    public enum ScheduleStatus
    {
        [GroupDescription("Not Yet Started", "None")]
        CommonNotStarted = 0,

        //Orders status
        [GroupDescription("Not Yet Started", "Orders")]
        OrderNotYetStarted = 100,
        [GroupDescription("Not Yet Due", "Orders")]
        OrderNotYetDue = 105,
        [GroupDescription("Saved", "Orders")]
        OrderSaved = 110,
        [GroupDescription("Submitted (Pending QA Review)", "Orders")]
        OrderSubmittedPendingReview = 115,
        [GroupDescription("Returned For Review", "Orders")]
        OrderReturnedForClinicianReview = 120,
        [GroupDescription("To Be Sent To Physician", "Orders")]
        OrderToBeSentToPhysician = 125,
        [GroupDescription("Sent To Physician (Manually)", "Orders")]
        OrderSentToPhysician = 130,
        [GroupDescription("Returned W/ Physician Signature", "Orders")]
        OrderReturnedWPhysicianSignature = 135,
        [GroupDescription("Reopened", "Orders")]
        OrderReopened = 140,
        [GroupDescription("Sent To Physician (Electronically)", "Orders")]
        OrderSentToPhysicianElectronically = 145,
        [GroupDescription("Saved By Physician", "Orders")]
        OrderSavedByPhysician = 150,

        //OASIS assessment status     
        [GroupDescription("Not Yet Due", "Assessment")]
        OasisNotYetDue = 200,
        [GroupDescription("Not Started", "Assessment")]
        OasisNotStarted = 205,
        [GroupDescription("Saved", "Assessment")]
        OasisSaved = 210,
        [GroupDescription("Completed (Pending QA Review)", "Assessment")]
        OasisCompletedPendingReview = 215,
        [GroupDescription("Completed (Export Ready)", "Assessment")]
        OasisCompletedExportReady = 220,
        [GroupDescription("Exported", "Assessment")]
        OasisExported = 225,
        [GroupDescription("Returned For Review", "Assessment")]
        OasisReturnedForClinicianReview = 230,
        [GroupDescription("Reopened", "Assessment")]
        OasisReopened = 235,

        //Billing Status      
        [GroupDescription("Created", "Claims")]
        ClaimCreated = 300,
        [GroupDescription("ReOpenend", "Claims")]
        ClaimReOpen = 301,
        [GroupDescription("Submitted", "Claims")]
        ClaimSubmitted = 305,
        [GroupDescription("Rejected", "Claims")]
        ClaimRejected = 310,
        [GroupDescription("Payment Pending", "Claims")]
        ClaimPaymentPending = 315,
        [GroupDescription("Accepted/Processing", "Claims")]
        ClaimAccepted = 320,
        [GroupDescription("Returned", "Claims")]
        ClaimWithErrors = 325,
        [GroupDescription("Paid", "Claims")]
        ClaimPaidClaim = 330,
        [GroupDescription("Cancelled", "Claims")]
        ClaimCancelledClaim = 335,

        //VisitNote
        [GroupDescription("Missed Visit", "Notes")]
        NoteMissedVisit = 400,
        [GroupDescription("Not Yet Started", "Notes")]
        NoteNotStarted = 405,
        [GroupDescription("Not Yet Due", "Notes")]
        NoteNotYetDue = 410,
        [GroupDescription("Saved", "Notes")]
        NoteSaved = 415,
        [GroupDescription("Submitted With Signature", "Notes")]
        NoteSubmittedWithSignature = 420,
        [GroupDescription("Completed", "Notes")]
        NoteCompleted = 425,
        [GroupDescription("Returned For Review", "Notes")]
        NoteReturned = 430,
        [GroupDescription("Reopened", "Notes")]
        NoteReopened = 435,
        [GroupDescription("Awaiting Clinician Signature", "Notes")]
        AwaitingClinicianSignature = 440,
        
        [GroupDescription("To Be Sent To Physician", "Notes")]
        EvalToBeSentToPhysician = 445,
        [GroupDescription("Sent To Physician (Fax, Mail, etc)", "Notes")]
        EvalSentToPhysician = 450,
        [GroupDescription("Returned W/ Physician Signature", "Notes")]
        EvalReturnedWPhysicianSignature = 455,
        [GroupDescription("Sent To Physician (Electronically)", "Notes")]
        EvalSentToPhysicianElectronically = 460,

        //Notes & Reports
        [GroupDescription("Created", "ReportsAndNotes")]
        ReportAndNotesCreated = 500,
        [GroupDescription("Saved", "ReportsAndNotes")]
        ReportAndNotesSaved = 515,
        [GroupDescription("Submitted With Signature", "ReportsAndNotes")]
        ReportAndNotesSubmittedWithSignature = 520,
        [GroupDescription("Completed", "ReportsAndNotes")]
        ReportAndNotesCompleted = 525,
        [GroupDescription("Returned For Review", "ReportsAndNotes")]
        ReportAndNotesReturned = 530,
        [GroupDescription("Reopened", "ReportsAndNotes")]
        ReportAndNotesReopen = 535,

        [GroupDescription("No Status", "None")]
        NoStatus = 1000
    }
}
