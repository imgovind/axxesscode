﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class AgencyLite
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public string Title { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ContactPersonDisplayName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonPhoneFormatted { get; set; }
        public bool IsSuspended { get; set; }
        public bool IsDeprecated { get; set; }
        public string ActionText { get; set; }
    }
}
