﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.SqlGeneration.Schema;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Enums;

namespace Axxess.AgencyManagement.Domain
{
    [KnownType(typeof(RapSnapShot))]
    public class FinalSnapShot
    {
        [SubSonicPrimaryKey]
        public string MainId { get; set; }
        public Guid Id { get; set; }
        public long BatchId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string PatientIdNumber { get; set; }
        public string MedicareNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public DateTime StartofCareDate { get; set; }
        public string PhysicianNPI { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public bool AreOrdersComplete { get; set; }
        public bool IsOasisComplete { get; set; }
        public bool IsFirstBillableVisit { get; set; }
        public DateTime FirstBillableVisitDate { get; set; }
        public bool AreVisitsComplete { get; set; }
        public bool IsGenerated { get; set; }
        public string DiagnosisCode { get; set; }
        public string HippsCode { get; set; }
        public string ClaimKey { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        public bool IsRapGenerated { get; set; }
        public string VerifiedVisits { get; set; }
        public string Supply { get; set; }
        public double SupplyTotal { get; set; }
        public int PrimaryInsuranceId { get; set; }
        public bool IsVisitVerified { get; set; }
        public bool IsSupplyVerified { get; set; }
        public bool IsFinalInfoVerified { get; set; }
        public string Remark { get; set; }
        [UIHint("Status")]
        public int Status { get; set; }
        public double ProspectivePay { get; set; }
        public double Payment { get; set; }
        [DataType(DataType.Date)]
        public DateTime PaymentDate { get; set; }
        public int Type { get; set; }
        public string AssessmentType { get; set; }
        public string AdmissionSource { get; set; }
        public int PatientStatus { get; set; }
        [DataType(DataType.Date)]
        public DateTime ClaimDate { get; set; }
        public string Comment { get; set; }
        public string Reason { get; set; }
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }
        [SubSonicIgnore]
        public string EpisodeRange
        {
            get
            {
                return string.Concat(this.EpisodeStartDate.ToString("MM/dd/yyyy"), " - ", this.EpisodeEndDate.ToString("MM/dd/yyyy"));
            }
        }
        [SubSonicIgnore]
        public string Visits { get; set; }
        [SubSonicIgnore]
        public string Primary { get; set; }
        [SubSonicIgnore]
        public string Second { get; set; }
        [SubSonicIgnore]
        public string Third { get; set; }
        [SubSonicIgnore]
        public string Fourth { get; set; }
        [SubSonicIgnore]
        public string Fifth { get; set; }
        [SubSonicIgnore]
        public string Sixth { get; set; }
        [SubSonicIgnore]
        public Dictionary<string, BillInfo> BillInformations { get; set; }
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                return EnumExtensions.GetDescription((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), this.Status));

            }
        }
        [SubSonicIgnore]
        public string PaymentDateFormatted
        {
            get
            {
                return PaymentDate != DateTime.MinValue ? PaymentDate.ToShortDateString().ToZeroFilled() : string.Empty;
            }
        }
    }
}
