﻿namespace Axxess.AgencyManagement.Domain
{
    public class BillInfo
    {
        public int Unit { get; set; }
        public string Amount { get; set; }
        public string CodeOne { get; set; }
        public string CodeTwo { get; set; }
        public string Discipline { get; set; }
    }
}
