﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.Core.Extension;

    public class PatientSelection
    {
        public Guid Id { get; set; }
        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public bool IsDischarged { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DOB { get; set; }
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }

        public string ShortName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.MI.IsNotNullOrEmpty() ? this.MI + "." : string.Empty);
            }
        }
    }
}
