﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain
{
   public class EpisodeFiltered
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public string Schedule { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
    }
}
