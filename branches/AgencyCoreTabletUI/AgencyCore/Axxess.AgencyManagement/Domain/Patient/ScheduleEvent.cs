﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    using Enums;
    using Extensions;

    using Axxess.Core.Extension;

    [XmlRoot()]
    [DataContract]
    public class ScheduleEvent
    {
        public ScheduleEvent()
        {
            this.UserId = Guid.Empty;
            this.EventId = Guid.Empty;
            this.EventDate = string.Empty;
            this.Assets = new List<Guid>();
        }

        [XmlElement]
        [DataMember]
        public Guid EventId { get; set; }

        [XmlElement]
        [DataMember]
        public int DisciplineTask { get; set; }

        [XmlElement]
        [DataMember]
        public Guid UserId { get; set; }

        [XmlElement]
        [DataMember]
        public string UserName { get; set; }

        [XmlElement]
        [DataMember]
        public string EventDate { get; set; }

        [XmlElement]
        [DataMember]
        public string VisitDate { get; set; }

        [XmlElement]
        [DataMember]
        public string Status { get; set; }

        [XmlElement]
        [DataMember]
        public string Discipline { get; set; }

        [XmlElement]
        [DataMember]
        public Guid EpisodeId { get; set; }

        [XmlElement]
        [DataMember]
        public Guid PatientId { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsBillable { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsMissedVisit { get; set; }

        [XmlElement]
        [DataMember]
        public string TimeIn { get; set; }

        [XmlElement]
        [DataMember]
        public string TimeOut { get; set; }
       
        [XmlElement]
        [DataMember]
        public string Surcharge { get; set; }

        [XmlElement]
        [DataMember]
        public string AssociatedMileage { get; set; }

        [XmlElement]
        [DataMember]
        public string ReturnReason { get; set; }

        [XmlElement]
        [DataMember]
        public string Comments { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsDeprecated { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsOrderForNextEpisode { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsVisitPaid { get; set; }

        [XmlElement]
        [DataMember]
        public bool InPrintQueue { get; set; }

        [XmlIgnore]
        public List<NotesQuestion> Questions { get; set; }

        [XmlIgnore]
        public string StatusName
        {
            get
            {
                if (this.Status.IsNotNullOrEmpty())
                {
                    if (this.IsMissedVisit)
                    {
                        return "Missed Visit";
                    }
                    else
                    {
                        int check = -1;
                        ScheduleStatus status = ScheduleStatus.NoStatus;
                        if (int.TryParse(this.Status, out check)) { status = Enum.IsDefined(typeof(ScheduleStatus), check) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), this.Status) : ScheduleStatus.NoStatus; }
                        if ((status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue) && this.EventDate.IsValidDate() && this.EventDate.ToDateTime().Date < DateTime.Now.Date)
                        {
                            return ScheduleStatus.CommonNotStarted.GetDescription();
                        }
                        else
                        {
                            return status.GetDescription();
                        }
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        [XmlIgnore]
        public string DisciplineTaskName
        {
            get
            {
                if (Enum.IsDefined(typeof(DisciplineTasks), this.DisciplineTask)) { return ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), this.DisciplineTask)).GetDescription(); } else { return string.Empty; };
            }
        }

        [XmlIgnore]
        public string PatientName { get; set; }

        [XmlIgnore]
        public string Url { get; set; }

        [XmlIgnore]
        public string PrintUrl { get; set; }

        [XmlIgnore]
        public string ActionUrl { get; set; }

        [XmlIgnore]
        public Guid PhysicianId { get; set; }

        [XmlIgnore]
        public string AttachmentUrl
        {
            get
            {
                if (this.Assets != null && this.Assets.Count > 0)
                {
                    return "<span class=\"img icon paperclip\"></span>";
                }
                return string.Empty;
            }
        }

        [XmlIgnore]
        public string MissedVisitComments { get; set; }

        [XmlIgnore]
        public string EpisodeNotes { get; set; }

        public List<Guid> Assets { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [XmlIgnore]
        public string StatusComment
        {
            get
            {
                var result = string.Empty;
                if (this.IsMissedVisit && this.MissedVisitComments.IsNotNullOrEmpty())
                {
                    result += this.MissedVisitComments +"\r\n";
                }
                if (this.ReturnReason.IsNotNullOrEmpty())
                {
                    result += "Reason:\r\n" + this.ReturnReason;
                }
                return result.Clean();
            }
        }

        [XmlIgnore]
        public bool IsPastDue
        {
            get
            {
                return (this.EventDate.IsNotNullOrEmpty() && this.EventDate.IsValidDate() && EventDate.ToDateTime().Date < DateTime.Now.Date && (this.Status == ((int)ScheduleStatus.NoteNotYetDue).ToString() || this.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() || this.Status == ((int)ScheduleStatus.OrderNotYetDue).ToString()));
            }
        }

        [XmlIgnore]
        public bool IsComplete
        {
            get
            {
                return this.IsCompletelyFinished() || this.IsCompleted();
            }
        }

        [XmlIgnore]
        public bool IsOrphaned
        {
            get
            {
                var result = false;
                if (this.EventDate.IsValidDate())
                {
                    var eventDate = this.EventDate.ToDateTime();
                    if (eventDate.Date < this.StartDate.Date || eventDate.Date > this.EndDate.Date)
                    {
                        result = true;
                    }
                }
                return result;
            }
        }

        [XmlIgnore]
        public string PatientIdNumber { get; set; }

        [XmlIgnore]
        public string EventDateSortable
        {
            get
            {
                return EventDate.IsNotNullOrEmpty() ? "<div class='float-left'><span class='float-right'>" + EventDate.Split('/')[2] + "</span><span class='float-right'>/</span><span class='float-right'>" + EventDate.Split('/')[0] + "/" + EventDate.Split('/')[1] + "</span></div>" : "";
            }
        }

        [XmlIgnore]
        public int Unit
        {
            get
            {
                var timeIn = DateTime.Now;
                var timeOut = DateTime.Now;
                if (this.TimeOut.IsNotNullOrEmpty() && this.TimeOut.HourToDateTime(ref timeOut) && this.TimeIn.IsNotNullOrEmpty() && this.TimeIn.HourToDateTime(ref timeIn) && timeOut >= timeIn)
                {
                    var min = (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);
                    if (min > 0)
                    {
                        return (int)Math.Ceiling((double)min / 15);
                    }
                    return 0;
                }
                return 0;
            }
        }

        [XmlIgnore]
        public int MinSpent
        {
            get
            {
                var timeIn = DateTime.Now;
                var timeOut = DateTime.Now;
                if (this.TimeOut.IsNotNullOrEmpty() && this.TimeOut.HourToDateTime(ref timeOut) && this.TimeIn.IsNotNullOrEmpty() && this.TimeIn.HourToDateTime(ref timeIn) && timeOut >= timeIn)
                {
                    return (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);
                }
                return 0;
            }
        }

        [XmlIgnore]
        public Guid NewEpisodeId { get; set; }

        [XmlIgnore]
        public bool IsEpisodeReassiged { get; set; }

        [XmlIgnore]
        public string OasisProfileUrl { get; set; }
      
    }
}
