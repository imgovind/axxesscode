﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;

    public class NewEpisodeData
    {

        public Guid PatientId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string DisplayName { get; set; }
        public DateTime StartOfCareDate { get; set; }
        public string CaseManager { get; set; }
        public string PrimaryInsurance { get; set; }
        public string SecondaryInsurance { get; set; }
        public string PrimaryPhysician { get; set; }

        public string EndDateFormatted { get { return this.StartDate.ToShortDateString(); } }

        public string StartDateFormatted { get { return this.StartDate.ToShortDateString(); } }

        public List<SelectListItem> AdmissionDates { get; set; }
        public Guid AdmissionId { get; set; }

    }
}
