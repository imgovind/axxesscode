﻿namespace Axxess.AgencyManagement.Domain
{
    public class PatientEpisodeEvent
    {
        public string Status { get; set; }
        public string UserName { get; set; }
        public string TaskName { get; set; }
        public string PrintUrl { get; set; }
        public string EventDate { get; set; }
        public string PatientName { get; set; }
        public string CustomValue { get; set; }
        public string RedNote { get; set; }
        public string BlueNote { get; set; }
        public string YellowNote { get; set; }
    }
}
