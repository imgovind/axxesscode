﻿namespace Axxess.AgencyManagement.Domain
{
    public interface IAddress
    {
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string AddressCity { get; set; }
        string AddressStateCode { get; set; }
        string AddressZipCode { get; set; }
        string EmailAddress { get; set; }
    }
}
