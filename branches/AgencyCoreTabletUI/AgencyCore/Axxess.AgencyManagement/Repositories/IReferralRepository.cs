﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Enums;

    public interface IReferralRepository
    {
        IEnumerable<Referral> GetAll(Guid agencyId, ReferralStatus status);
        IEnumerable<Referral> GetAllByCreatedUser(Guid agencyId, Guid userId, ReferralStatus status);

        bool Add(Referral referral);
        Referral Get(Guid agencyId,Guid id);
        bool Edit(Referral referral);
        bool Delete(Guid agencyId,Guid id);
        bool SetStatus(Guid agencyId, Guid id, ReferralStatus status);
        bool NonAdmitReferral(PendingPatient pending);

        List<ReferralData> All(Guid agencyId, ReferralStatus referralStatus);
        List<ReferralData> AllByUser(Guid agencyId, Guid userId, ReferralStatus status);
    }
}
