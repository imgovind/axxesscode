﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class MessageRepository : IMessageRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public MessageRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IMessageRepository Members

        public bool Add(Message message)
        {
            bool result = false;
            if (message != null)
            {
                message.Id = Guid.NewGuid();
                message.Created = DateTime.Now;
                database.Add<Message>(message);
                result = true;
            }

            return result;
        }

        public bool AddSystemMessage(SystemMessage message)
        {
            bool result = false;
            if (message != null)
            {
                message.Id = Guid.NewGuid();
                message.Created = DateTime.Now;
                database.Add<SystemMessage>(message);
                result = true;
            }

            return result;
        }

        public bool AddDashboardMessage(DashboardMessage message)
        {
            bool result = false;
            if (message != null)
            {
                message.Id = Guid.NewGuid();
                message.Created = DateTime.Now;
                database.Add<DashboardMessage>(message);
                result = true;
            }

            return result;
        }

        public bool Delete(Guid id, Guid agencyId)
        {
            var message = database.Single<Message>(m => m.Id == id && m.AgencyId == agencyId);
            if (message != null)
            {
                message.IsDeprecated = true;
                database.Update<Message>(message);
                return true;
            }
            return false;
        }

        public List<Message> GetCurrentMessages(Guid userId, Guid agencyId)
        {
            var patientScript = @"SELECT `Id`, `FromName`, `Subject`, `MarkAsRead`, `Created` FROM messages WHERE `RecipientId` = @userid and `AgencyId` = @agencyid and `IsDeprecated` = 0 ORDER BY Created DESC LIMIT 0, 5";

            return new FluentCommand<Message>(patientScript)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("userid", userId)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new Message
                {
                    Id = reader.GetGuid("Id"),
                    Subject = reader.GetString("Subject"),
                    MarkAsRead = reader.GetBoolean("MarkAsRead"),
                    FromName = reader.GetString("FromName")
                })
                .AsList();
        }

        public IList<Message> GetUserMessages(Guid userId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");

            var messages = database.Find<Message>(m => m.RecipientId == userId && m.AgencyId == agencyId && m.IsDeprecated == false)
                .OrderByDescending(m => m.Created).ToList();

            messages.ForEach(m =>
            {
                m.Type = MessageType.User;
                if (!m.PatientId.IsEmpty())
                {
                    var patient = database.Single<Patient>(p => p.Id == m.PatientId && p.AgencyId == agencyId);
                    if (patient != null)
                    {
                        m.PatientName = patient.DisplayName;
                    }
                }
            });

            return messages;
        }

        public IList<Message> GetSentMessages(Guid userId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");

            var sentMessages = new List<Message>();
            var query = database.Find<Message>(m => m.FromId == userId && m.AgencyId == agencyId && m.IsDeprecated == false)
                .OrderByDescending(m => m.Created);

            var subject = string.Empty;
            var date = DateTime.MinValue;
            query.ForEach(message =>
            {
                if (!subject.IsEqual(message.Subject) && date != message.Created)
                {
                    if (!message.PatientId.IsEmpty())
                    {
                        var patient = database.Single<Patient>(p => p.Id == message.PatientId);
                        if (patient != null)
                        {
                            message.PatientName = patient.DisplayName;
                        }
                    }
                    sentMessages.Add(message);
                }
                date = message.Created;
                subject = message.Subject;
            });

            return sentMessages;
        }

        public Message GetMessage(Guid id, Guid agencyId, bool markAsRead)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var message = database.Single<Message>(m => m.Id == id && m.AgencyId == agencyId);

            if (message != null)
            {
                if (!message.MarkAsRead)
                {
                    message.MarkAsRead = markAsRead;
                    database.Update<Message>(message);
                }

                var patient = database.Single<Patient>(p => p.Id == message.PatientId && p.AgencyId == agencyId);
                if (patient != null)
                {
                    message.PatientName = patient.DisplayName;
                }
                message.Type = MessageType.User;
            }

            return message;
        }

        public int MessageCount(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");

            return database.Find<Message>(m => m.MarkAsRead == false && m.RecipientId == userId).Count;
        }

        public List<SystemMessage> GetSystemMessages()
        {
            return database.All<SystemMessage>().ToList();
        }

        public SystemMessage GetSystemMessage(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            return database.Single<SystemMessage>(m => m.Id == id);
        }

        public DashboardMessage GetCurrentDashboardMessage()
        {
            return database.All<DashboardMessage>().OrderByDescending(m => m.Created).FirstOrDefault();
        }

        #endregion
    }
}
