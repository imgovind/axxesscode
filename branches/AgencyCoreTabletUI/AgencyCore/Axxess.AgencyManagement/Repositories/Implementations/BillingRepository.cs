﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;

    public class BillingRepository : IBillingRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public BillingRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IBillingRepository Members

        public bool AddRap(Rap rap)
        {
            bool result = false;
            if (rap != null)
            {
                rap.Created = DateTime.Now;
                rap.Modified = DateTime.Now;
                database.Add<Rap>(rap);
                result = true;
            }
            return result;
        }

        public Rap GetRap(Guid agencyId, Guid claimId)
        {
            Check.Argument.IsNotEmpty(claimId, "claimId");
            return database.Single<Rap>(r => (r.AgencyId == agencyId && r.Id == claimId));
        }

        public Final GetFinal(Guid agencyId, Guid claimId) {
            Final final = null;
            var script = string.Format(@"
                SELECT
	                finals.`Id`						as `Id`,                    finals.`AgencyId`				as `AgencyId`,
	                finals.`PatientId`				as `PatientId`,             finals.`EpisodeId`				as `EpisodeId`,
	                finals.`PatientIdNumber`		as `PatientIdNumber`,       patientepisodes.`StartDate` 	as `EpisodeStartDate`,
	                patientepisodes.`EndDate`		as `EpisodeEndDate`,        finals.`IsOasisComplete`		as `IsOasisComplete`,
	                finals.`IsFirstBillableVisit`	as `IsFirstBillableVisit`,  finals.`FirstBillableVisitDate` as `FirstBillableVisitDate`,
	                finals.`Remark`					as `Remark`,                finals.`MedicareNumber`			as `MedicareNumber`,
	                finals.`FirstName`				as `FirstName`,             finals.`LastName`				as `LastName`,
	                finals.`DOB`					as `DOB`,                   finals.`Gender`					as `Gender`,
	                finals.`PaymentDate`			as `PaymentDate`,           finals.`AddressLine1`			as `AddressLine1`,
	                finals.`AddressLine2`			as `AddressLine2`,          finals.`AddressCity`			as `AddressCity`,
	                finals.`AddressStateCode`		as `AddressStateCode`,      finals.`AddressZipCode`			as `AddressZipCode`,
	                finals.`StartofCareDate`		as `StartofCareDate`,       finals.`PhysicianNPI`			as `PhysicianNPI`,
	                finals.`PhysicianFirstName`		as `PhysicianFirstName`,    finals.`PhysicianLastName`		as `PhysicianLastName`,
	                finals.`DiagnosisCode`			as `DiagnosisCode`,         finals.`HippsCode`				as `HippsCode`,
	                finals.`ClaimKey`				as `ClaimKey`,              finals.`AreOrdersComplete`		as `AreOrdersComplete`,
	                finals.`AreVisitsComplete`		as `AreVisitsComplete`,     finals.`IsGenerated`			as `IsGenerated`,
	                finals.`Modified`				as `Modified`,              finals.`Created`				as `Created`,
	                finals.`VerifiedVisits`			as `VerifiedVisits`,        finals.`Supply`					as `Supply`,
	                finals.`SupplyTotal`			as `SupplyTotal`,           finals.`Payment`				as `Payment`,
	                finals.`PrimaryInsuranceId`		as `PrimaryInsuranceId`,    finals.`IsSupplyVerified`		as `IsSupplyVerified`,
	                finals.`IsFinalInfoVerified`	as `IsFinalInfoVerified`,   finals.`IsVisitVerified`		as `IsVisitVerified`,
	                finals.`IsRapGenerated`			as `IsRapGenerated`,        finals.`Status`					as `Status`,
	                finals.`Type`					as `Type`,                  finals.`AdmissionSource`		as `AdmissionSource`,
	                finals.`PatientStatus`			as `PatientStatus`,         finals.`ClaimDate`				as `ClaimDate`,
	                finals.`ProspectivePay`			as `ProspectivePay`,        finals.`AssessmentType`			as `AssessmentType`,
	                finals.`Comment`				as `Comment`,               finals.`DischargeDate`			as `DischargeDate`,
	                finals.`HealthPlanId`			as `HealthPlanId`,          finals.`AuthorizationNumber`	as `AuthorizationNumber`,
	                finals.`ConditionCodes`			as `ConditionCodes`,        finals.`UB4PatientStatus`       as `UB4PatientStatus` 
	            FROM finals INNER JOIN patientepisodes ON patientepisodes.`AgencyId` = @agencyId AND finals.`EpisodeId` = patientepisodes.Id
	            WHERE finals.`AgencyId` = @agencyId AND finals.`Id` = @claimId;");
            using (var cmd = new FluentCommand<Final>(script))
                final = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("claimId", claimId)
                    .SetMap(r => new Final {
                        Id =                    r.GetGuid("Id"),                                AgencyId =                  r.GetGuid("AgencyId"),
                        PatientId =             r.GetGuid("PatientId"),                         EpisodeId =                 r.GetGuid("EpisodeId"),
                        PatientIdNumber =       r.GetStringNullable("PatientIdNumber"),         EpisodeStartDate =          r.GetDateTime("EpisodeStartDate"),
                        EpisodeEndDate =        r.GetDateTime("EpisodeEndDate"),                IsOasisComplete =           r.GetBoolean("IsOasisComplete"),
                        IsFirstBillableVisit =  r.GetBoolean("IsFirstBillableVisit"),           FirstBillableVisitDate =    r.GetDateTime("FirstBillableVisitDate"),
                        Remark =                r.GetStringNullable("Remark"),                  MedicareNumber =            r.GetStringNullable("MedicareNumber"),
                        FirstName =             r.GetStringNullable("FirstName"),               LastName =                  r.GetStringNullable("LastName"),
                        DOB =                   r.GetDateTime("DOB"),                           Gender =                    r.GetStringNullable("Gender"),
                        PaymentDate =           r.GetDateTime("PaymentDate"),                   AddressLine1 =              r.GetStringNullable("AddressLine1"),
                        AddressLine2 =          r.GetStringNullable("AddressLine2"),            AddressCity =               r.GetStringNullable("AddressCity"),
                        AddressStateCode =      r.GetStringNullable("AddressStateCode"),        AddressZipCode =            r.GetStringNullable("AddressZipCode"),
                        StartofCareDate =       r.GetDateTime("StartofCareDate"),               PhysicianNPI =              r.GetStringNullable("PhysicianNPI"),
                        PhysicianFirstName =    r.GetStringNullable("PhysicianFirstName"),      PhysicianLastName =         r.GetStringNullable("PhysicianLastName"),
                        DiagnosisCode =         r.GetStringNullable("DiagnosisCode"),           HippsCode =                 r.GetStringNullable("HippsCode"),
                        ClaimKey =              r.GetStringNullable("ClaimKey"),                AreOrdersComplete =         r.GetBoolean("AreOrdersComplete"),
                        AreVisitsComplete =     r.GetBoolean("AreVisitsComplete"),              IsGenerated =               r.GetBoolean("IsGenerated"),
                        Modified =              r.GetDateTime("Modified"),                      Created =                   r.GetDateTime("Created"),
                        VerifiedVisits =        r.GetStringNullable("VerifiedVisits"),          Supply =                    r.GetStringNullable("Supply"),
                        SupplyTotal =           (double)r.GetDecimalNullable("SupplyTotal"),    Payment =                   (double)r.GetDecimalNullable("Payment"),
                        PrimaryInsuranceId =    (int)r.GetIntNullable("PrimaryInsuranceId"),    IsSupplyVerified =          r.GetBoolean("IsSupplyVerified"),
                        IsFinalInfoVerified =   r.GetBoolean("IsFinalInfoVerified"),            IsVisitVerified =           r.GetBoolean("IsVisitVerified"),
                        IsRapGenerated =        r.GetBoolean("IsRapGenerated"),                 Status =                    (int)r.GetIntNullable("Status"),
                        Type =                  (int)r.GetIntNullable("Type"),                  AdmissionSource =           r.GetStringNullable("AdmissionSource"),
                        PatientStatus =         (int)r.GetIntNullable("PatientStatus"),         ClaimDate =                 r.GetDateTime("ClaimDate"),
                        ProspectivePay =        (double)r.GetDecimalNullable("ProspectivePay"), AssessmentType =            r.GetStringNullable("AssessmentType"),
                        Comment =               r.GetStringNullable("Comment"),                 DischargeDate =             r.GetDateTime("DischargeDate"),
                        HealthPlanId =          r.GetStringNullable("HealthPlanId"),            AuthorizationNumber =       r.GetStringNullable("AuthorizationNumber"),
                        ConditionCodes = r.GetStringNullable("ConditionCodes"),
                        UB4PatientStatus = r.GetStringNullable("UB4PatientStatus") 
                    }).AsSingle();
            return final;
        }

        public Rap GetRap(Guid agencyId, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            return database.Single<Rap>(r => (r.AgencyId == agencyId && r.PatientId == patientId && r.EpisodeId == episodeId));
        }

        public bool AddFinal(Final final)
        {
            Check.Argument.IsNotNull(final, "final");
            bool result = false;
            try
            {
                final.Created = DateTime.Now;
                final.Modified = DateTime.Now;
                database.Add<Final>(final);
                result = true;
            }
            catch (Exception e)
            {
                return false;
            }
            return result;
        }

        public Final GetFinal(Guid agencyId, Guid patientId, Guid episodeId) {
            Final final = null;
            var script = string.Format(@"
                SELECT
	                finals.`Id`						as `Id`,                    finals.`AgencyId`				as `AgencyId`,
	                finals.`PatientId`				as `PatientId`,             finals.`EpisodeId`				as `EpisodeId`,
	                finals.`PatientIdNumber`		as `PatientIdNumber`,       patientepisodes.`StartDate` 	as `EpisodeStartDate`,
	                patientepisodes.`EndDate`		as `EpisodeEndDate`,        finals.`IsOasisComplete`		as `IsOasisComplete`,
	                finals.`IsFirstBillableVisit`	as `IsFirstBillableVisit`,  finals.`FirstBillableVisitDate` as `FirstBillableVisitDate`,
	                finals.`Remark`					as `Remark`,                finals.`MedicareNumber`			as `MedicareNumber`,
	                finals.`FirstName`				as `FirstName`,             finals.`LastName`				as `LastName`,
	                finals.`DOB`					as `DOB`,                   finals.`Gender`					as `Gender`,
	                finals.`PaymentDate`			as `PaymentDate`,           finals.`AddressLine1`			as `AddressLine1`,
	                finals.`AddressLine2`			as `AddressLine2`,          finals.`AddressCity`			as `AddressCity`,
	                finals.`AddressStateCode`		as `AddressStateCode`,      finals.`AddressZipCode`			as `AddressZipCode`,
	                finals.`StartofCareDate`		as `StartofCareDate`,       finals.`PhysicianNPI`			as `PhysicianNPI`,
	                finals.`PhysicianFirstName`		as `PhysicianFirstName`,    finals.`PhysicianLastName`		as `PhysicianLastName`,
	                finals.`DiagnosisCode`			as `DiagnosisCode`,         finals.`HippsCode`				as `HippsCode`,
	                finals.`ClaimKey`				as `ClaimKey`,              finals.`AreOrdersComplete`		as `AreOrdersComplete`,
	                finals.`AreVisitsComplete`		as `AreVisitsComplete`,     finals.`IsGenerated`			as `IsGenerated`,
	                finals.`Modified`				as `Modified`,              finals.`Created`				as `Created`,
	                finals.`VerifiedVisits`			as `VerifiedVisits`,        finals.`Supply`					as `Supply`,
	                finals.`SupplyTotal`			as `SupplyTotal`,           finals.`Payment`				as `Payment`,
	                finals.`PrimaryInsuranceId`		as `PrimaryInsuranceId`,    finals.`IsSupplyVerified`		as `IsSupplyVerified`,
	                finals.`IsFinalInfoVerified`	as `IsFinalInfoVerified`,   finals.`IsVisitVerified`		as `IsVisitVerified`,
	                finals.`IsRapGenerated`			as `IsRapGenerated`,        finals.`Status`					as `Status`,
	                finals.`Type`					as `Type`,                  finals.`AdmissionSource`		as `AdmissionSource`,
	                finals.`PatientStatus`			as `PatientStatus`,         finals.`ClaimDate`				as `ClaimDate`,
	                finals.`ProspectivePay`			as `ProspectivePay`,        finals.`AssessmentType`			as `AssessmentType`,
	                finals.`Comment`				as `Comment`,               finals.`DischargeDate`			as `DischargeDate`,
	                finals.`HealthPlanId`			as `HealthPlanId`,          finals.`AuthorizationNumber`	as `AuthorizationNumber`,
	                finals.`ConditionCodes`			as `ConditionCodes`,        finals.`UB4PatientStatus`	    as `UB4PatientStatus`
	            FROM finals INNER JOIN patientepisodes ON patientepisodes.`AgencyId` = @agencyId AND finals.`EpisodeId` = patientepisodes.Id
	            WHERE finals.`AgencyId` = @agencyId AND finals.`PatientId` = @patientId AND finals.`EpisodeId` = @episodeId;");
            using (var cmd = new FluentCommand<Final>(script))
                final = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddGuid("episodeId", episodeId)
                    .SetMap(r => new Final {
                        Id =                    r.GetGuid("Id"),                                AgencyId =                  r.GetGuid("AgencyId"),
                        PatientId =             r.GetGuid("PatientId"),                         EpisodeId =                 r.GetGuid("EpisodeId"),
                        PatientIdNumber =       r.GetStringNullable("PatientIdNumber"),         EpisodeStartDate =          r.GetDateTime("EpisodeStartDate"),
                        EpisodeEndDate =        r.GetDateTime("EpisodeEndDate"),                IsOasisComplete =           r.GetBoolean("IsOasisComplete"),
                        IsFirstBillableVisit =  r.GetBoolean("IsFirstBillableVisit"),           FirstBillableVisitDate =    r.GetDateTime("FirstBillableVisitDate"),
                        Remark =                r.GetStringNullable("Remark"),                  MedicareNumber =            r.GetStringNullable("MedicareNumber"),
                        FirstName =             r.GetStringNullable("FirstName"),               LastName =                  r.GetStringNullable("LastName"),
                        DOB =                   r.GetDateTime("DOB"),                           Gender =                    r.GetStringNullable("Gender"),
                        PaymentDate =           r.GetDateTime("PaymentDate"),                   AddressLine1 =              r.GetStringNullable("AddressLine1"),
                        AddressLine2 =          r.GetStringNullable("AddressLine2"),            AddressCity =               r.GetStringNullable("AddressCity"),
                        AddressStateCode =      r.GetStringNullable("AddressStateCode"),        AddressZipCode =            r.GetStringNullable("AddressZipCode"),
                        StartofCareDate =       r.GetDateTime("StartofCareDate"),               PhysicianNPI =              r.GetStringNullable("PhysicianNPI"),
                        PhysicianFirstName =    r.GetStringNullable("PhysicianFirstName"),      PhysicianLastName =         r.GetStringNullable("PhysicianLastName"),
                        DiagnosisCode =         r.GetStringNullable("DiagnosisCode"),           HippsCode =                 r.GetStringNullable("HippsCode"),
                        ClaimKey =              r.GetStringNullable("ClaimKey"),                AreOrdersComplete =         r.GetBoolean("AreOrdersComplete"),
                        AreVisitsComplete =     r.GetBoolean("AreVisitsComplete"),              IsGenerated =               r.GetBoolean("IsGenerated"),
                        Modified =              r.GetDateTime("Modified"),                      Created =                   r.GetDateTime("Created"),
                        VerifiedVisits =        r.GetStringNullable("VerifiedVisits"),          Supply =                    r.GetStringNullable("Supply"),
                        SupplyTotal =           (double)r.GetDecimalNullable("SupplyTotal"),    Payment =                   (double)r.GetDecimalNullable("Payment"),
                        PrimaryInsuranceId =    (int)r.GetIntNullable("PrimaryInsuranceId"),    IsSupplyVerified =          r.GetBoolean("IsSupplyVerified"),
                        IsFinalInfoVerified =   r.GetBoolean("IsFinalInfoVerified"),            IsVisitVerified =           r.GetBoolean("IsVisitVerified"),
                        IsRapGenerated =        r.GetBoolean("IsRapGenerated"),                 Status =                    (int)r.GetIntNullable("Status"),
                        Type =                  (int)r.GetIntNullable("Type"),                  AdmissionSource =           r.GetStringNullable("AdmissionSource"),
                        PatientStatus =         (int)r.GetIntNullable("PatientStatus"),         ClaimDate =                 r.GetDateTime("ClaimDate"),
                        ProspectivePay =        (double)r.GetDecimalNullable("ProspectivePay"), AssessmentType =            r.GetStringNullable("AssessmentType"),
                        Comment =               r.GetStringNullable("Comment"),                 DischargeDate =             r.GetDateTime("DischargeDate"),
                        HealthPlanId =          r.GetStringNullable("HealthPlanId"),            AuthorizationNumber =       r.GetStringNullable("AuthorizationNumber"),
                        ConditionCodes = r.GetStringNullable("ConditionCodes"),
                        UB4PatientStatus = r.GetStringNullable("UB4PatientStatus") 
                    }).AsSingle();
            return final;
        }

        public bool UpdateFinal(Final final)
        {
            Check.Argument.IsNotNull(final, "final");
            if (final != null )
            {
                try
                {
                    database.Update<Final>(final);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateRap(Rap rap)
        {
            Check.Argument.IsNotNull(rap, "rap");
            if ( rap != null)
            {
                try
                {
                    database.Update<Rap>(rap);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }
        
        public bool UpdateFinalStatus(Final final)
        {
            Check.Argument.IsNotNull(final, "final");
            var currentFinal = database.Single<Final>(r => (r.AgencyId == final.AgencyId && r.PatientId == final.PatientId && r.EpisodeId == final.EpisodeId));

            if (currentFinal != null && final != null)
            {
                try
                {
                    currentFinal.PatientIdNumber = final.PatientIdNumber;
                    currentFinal.EpisodeStartDate = final.EpisodeStartDate;
                    currentFinal.EpisodeEndDate = final.EpisodeEndDate;
                    currentFinal.IsOasisComplete = final.IsOasisComplete;
                    currentFinal.IsFirstBillableVisit = final.IsFirstBillableVisit;
                    currentFinal.FirstBillableVisitDate = final.FirstBillableVisitDate;
                    currentFinal.Remark = final.Remark;
                    currentFinal.MedicareNumber = final.MedicareNumber;
                    currentFinal.FirstName = final.FirstName;
                    currentFinal.LastName = final.LastName;
                    currentFinal.DOB = final.DOB;
                    currentFinal.Gender = final.Gender;
                    currentFinal.AddressLine1 = final.AddressLine1;
                    currentFinal.AddressLine2 = final.AddressLine2;
                    currentFinal.AddressCity = final.AddressCity;
                    currentFinal.AddressStateCode = final.AddressStateCode;
                    currentFinal.AddressZipCode = final.AddressZipCode;
                    currentFinal.StartofCareDate = final.StartofCareDate;
                    currentFinal.PhysicianNPI = final.PhysicianNPI;
                    currentFinal.PhysicianFirstName = final.PhysicianFirstName;
                    currentFinal.PhysicianLastName = final.PhysicianLastName;
                    currentFinal.DiagnosisCode = final.DiagnosisCode;
                    currentFinal.HippsCode = final.HippsCode;
                    currentFinal.ClaimKey = final.ClaimKey;
                    currentFinal.AreOrdersComplete = final.AreOrdersComplete;
                    currentFinal.AreVisitsComplete = final.AreVisitsComplete;
                    currentFinal.Created = final.Created;
                    currentFinal.VerifiedVisits = final.VerifiedVisits;
                    currentFinal.PrimaryInsuranceId = final.PrimaryInsuranceId;
                    currentFinal.Supply = final.Supply;
                    currentFinal.SupplyTotal = final.SupplyTotal;
                    currentFinal.IsSupplyVerified = final.IsSupplyVerified;
                    currentFinal.IsFinalInfoVerified = final.IsFinalInfoVerified;
                    currentFinal.IsVisitVerified = final.IsVisitVerified;
                    currentFinal.AgencyId = final.AgencyId;
                    currentFinal.IsRapGenerated = final.IsRapGenerated;
                    currentFinal.Status = final.Status;
                    currentFinal.IsGenerated = final.IsGenerated;
                    currentFinal.ClaimDate = final.ClaimDate;
                    currentFinal.Comment = final.Comment;
                    currentFinal.Payment = final.Payment;
                    currentFinal.PaymentDate = final.PaymentDate;
                    database.Update<Final>(currentFinal);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateRapStatus(Rap rap)
        {
            Check.Argument.IsNotNull(rap, "rap");
            var currentRap = database.Single<Rap>(r => (r.AgencyId == rap.AgencyId && r.PatientId == rap.PatientId && r.EpisodeId == rap.EpisodeId));
            if (currentRap != null && rap != null)
            {
                try
                {
                    currentRap.PatientIdNumber = rap.PatientIdNumber;
                    currentRap.EpisodeStartDate = rap.EpisodeStartDate;
                    currentRap.EpisodeEndDate = rap.EpisodeEndDate;
                    currentRap.IsOasisComplete = rap.IsOasisComplete;
                    currentRap.IsFirstBillableVisit = rap.IsFirstBillableVisit;
                    currentRap.FirstBillableVisitDate = rap.FirstBillableVisitDate;
                    currentRap.IsGenerated = rap.IsGenerated;
                    currentRap.IsVerified = rap.IsVerified;
                    currentRap.Modified = rap.Modified;
                    currentRap.Remark = rap.Remark;
                    currentRap.MedicareNumber = rap.MedicareNumber;
                    currentRap.FirstName = rap.FirstName;
                    currentRap.LastName = rap.LastName;
                    currentRap.DOB = rap.DOB;
                    currentRap.Gender = rap.Gender;
                    currentRap.AddressLine1 = rap.AddressLine1;
                    currentRap.AddressLine2 = rap.AddressLine2;
                    currentRap.AddressCity = rap.AddressCity;
                    currentRap.AddressStateCode = rap.AddressStateCode;
                    currentRap.AddressZipCode = rap.AddressZipCode;
                    currentRap.StartofCareDate = rap.StartofCareDate;
                    currentRap.PhysicianNPI = rap.PhysicianNPI;
                    currentRap.PhysicianFirstName = rap.PhysicianFirstName;
                    currentRap.PhysicianLastName = rap.PhysicianLastName;
                    currentRap.DiagnosisCode = rap.DiagnosisCode;
                    currentRap.HippsCode = rap.HippsCode;
                    currentRap.ClaimKey = rap.ClaimKey;
                    currentRap.AreOrdersComplete = rap.AreOrdersComplete;
                    currentRap.Created = rap.Created;
                    currentRap.PrimaryInsuranceId = rap.PrimaryInsuranceId;
                    currentRap.Status = rap.Status;
                    currentRap.AgencyId = rap.AgencyId;
                    currentRap.ClaimDate = rap.ClaimDate;
                    currentRap.ProspectivePay = rap.ProspectivePay;
                    currentRap.AssessmentType = rap.AssessmentType;
                    currentRap.Comment = rap.Comment;
                    currentRap.Payment = rap.Payment;
                    currentRap.PaymentDate = rap.PaymentDate;
                    database.Update<Rap>(currentRap);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool VerifyRap(Guid agencyId , Rap rap)
        {
            Check.Argument.IsNotNull(rap, "rap");
            var currentClaim = database.Single<Rap>(r => (r.AgencyId == agencyId && r.Id == rap.Id));
            if (currentClaim != null && rap != null)
            {
                try
                {
                    currentClaim.FirstName = rap.FirstName;
                    currentClaim.LastName = rap.LastName;
                    currentClaim.MedicareNumber = rap.MedicareNumber;
                    if (rap.FirstBillableVisitDateFormat.IsNotNullOrEmpty() && rap.FirstBillableVisitDateFormat.IsValidDate())
                    {
                        currentClaim.FirstBillableVisitDate = rap.FirstBillableVisitDateFormat.ToDateTime();
                    }
                    currentClaim.EpisodeStartDate = rap.EpisodeStartDate;
                    currentClaim.StartofCareDate = rap.StartofCareDate;
                    currentClaim.PatientIdNumber = rap.PatientIdNumber;
                    currentClaim.Gender = rap.Gender;
                    currentClaim.DOB = rap.DOB;
                    currentClaim.AddressCity = rap.AddressLine1;
                    currentClaim.AddressLine2 = rap.AddressLine2;
                    currentClaim.AddressCity = rap.AddressCity;
                    currentClaim.AddressStateCode = rap.AddressStateCode;
                    currentClaim.AddressZipCode = rap.AddressZipCode;
                    currentClaim.PhysicianLastName = rap.PhysicianLastName;
                    currentClaim.PhysicianNPI = rap.PhysicianNPI;
                    currentClaim.PhysicianFirstName = rap.PhysicianFirstName;
                    currentClaim.DiagnosisCode = string.Format("<DiagonasisCodes><code1>{0}</code1><code2>{1}</code2><code3>{2}</code3><code4>{3}</code4><code5>{4}</code5><code6>{5}</code6></DiagonasisCodes>", rap.Primary, rap.Second, rap.Third, rap.Fourth, rap.Fifth, rap.Sixth);
                    currentClaim.ConditionCodes = string.Format("<ConditionCodes><ConditionCode18>{0}</ConditionCode18><ConditionCode19>{1}</ConditionCode19><ConditionCode20>{2}</ConditionCode20><ConditionCode21>{3}</ConditionCode21><ConditionCode22>{4}</ConditionCode22><ConditionCode23>{5}</ConditionCode23><ConditionCode24>{6}</ConditionCode24><ConditionCode25>{7}</ConditionCode25><ConditionCode26>{8}</ConditionCode26><ConditionCode27>{9}</ConditionCode27><ConditionCode28>{10}</ConditionCode28></ConditionCodes>", rap.ConditionCode18, rap.ConditionCode19, rap.ConditionCode20, rap.ConditionCode21, rap.ConditionCode22, rap.ConditionCode23, rap.ConditionCode24, rap.ConditionCode25, rap.ConditionCode26, rap.ConditionCode27, rap.ConditionCode28);
                    currentClaim.HippsCode = rap.HippsCode;
                    currentClaim.ClaimKey = rap.ClaimKey;
                    currentClaim.Remark = rap.Remark;
                    currentClaim.ProspectivePay = rap.ProspectivePay;
                    currentClaim.AssessmentType = rap.AssessmentType;
                    currentClaim.AdmissionSource = rap.AdmissionSource;
                    currentClaim.PatientStatus = rap.PatientStatus;
                    currentClaim.UB4PatientStatus = rap.UB4PatientStatus;
                    if (rap.UB4PatientStatus != ((int)UB4PatientStatus.StillPatient).ToString())
                    {
                        currentClaim.DischargeDate = rap.DischargeDate;
                    }
                    currentClaim.PrimaryInsuranceId = rap.PrimaryInsuranceId;
                    if (rap.PrimaryInsuranceId >= 1000)
                    {
                        currentClaim.HealthPlanId = rap.HealthPlanId;
                        currentClaim.AuthorizationNumber = rap.AuthorizationNumber;
                    }
                    currentClaim.IsVerified = true;
                    currentClaim.Modified = DateTime.Now;
                    database.Update<Rap>(currentClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool VerifyInfo(Guid agencyId, Final final)
        {
            Check.Argument.IsNotNull(final, "final");
            var currentClaim = database.Single<Final>(r => (r.AgencyId == agencyId && r.Id == final.Id));
            if (currentClaim != null && final != null)
            {
                try
                {
                    currentClaim.FirstName = final.FirstName;
                    currentClaim.LastName = final.LastName;
                    currentClaim.MedicareNumber = final.MedicareNumber;
                    currentClaim.PatientIdNumber = final.PatientIdNumber;
                    currentClaim.Gender = final.Gender;
                    currentClaim.DOB = final.DOB;
                    currentClaim.EpisodeStartDate = final.EpisodeStartDate;
                    currentClaim.StartofCareDate = final.StartofCareDate;
                    currentClaim.AddressLine1 = final.AddressLine1;
                    currentClaim.AddressLine2 = final.AddressLine2;
                    currentClaim.AddressCity = final.AddressCity;
                    currentClaim.AddressStateCode = final.AddressStateCode;
                    currentClaim.AddressZipCode = final.AddressZipCode;
                    currentClaim.HippsCode = final.HippsCode;
                    currentClaim.ClaimKey = final.ClaimKey;
                    if (final.FirstBillableVisitDateFormat.IsNotNullOrEmpty() && final.FirstBillableVisitDateFormat.IsValidDate())
                    {
                        currentClaim.FirstBillableVisitDate = final.FirstBillableVisitDateFormat.ToDateTime();
                    }
                    currentClaim.PhysicianLastName = final.PhysicianLastName;
                    currentClaim.PhysicianFirstName = final.PhysicianFirstName;
                    currentClaim.PhysicianNPI = final.PhysicianNPI;
                    currentClaim.DiagnosisCode = string.Format("<DiagonasisCodes><code1>{0}</code1><code2>{1}</code2><code3>{2}</code3><code4>{3}</code4><code5>{4}</code5><code6>{5}</code6></DiagonasisCodes>", final.Primary, final.Second, final.Third, final.Fourth, final.Fifth, final.Sixth);
                    currentClaim.ConditionCodes = string.Format("<ConditionCodes><ConditionCode18>{0}</ConditionCode18><ConditionCode19>{1}</ConditionCode19><ConditionCode20>{2}</ConditionCode20><ConditionCode21>{3}</ConditionCode21><ConditionCode22>{4}</ConditionCode22><ConditionCode23>{5}</ConditionCode23><ConditionCode24>{6}</ConditionCode24><ConditionCode25>{7}</ConditionCode25><ConditionCode26>{8}</ConditionCode26><ConditionCode27>{9}</ConditionCode27><ConditionCode28>{10}</ConditionCode28></ConditionCodes>", final.ConditionCode18, final.ConditionCode19, final.ConditionCode20, final.ConditionCode21, final.ConditionCode22, final.ConditionCode23, final.ConditionCode24, final.ConditionCode25, final.ConditionCode26, final.ConditionCode27, final.ConditionCode28);
                    currentClaim.Remark = final.Remark;
                    currentClaim.IsFinalInfoVerified = true;
                    currentClaim.ProspectivePay = final.ProspectivePay;
                    currentClaim.AssessmentType = final.AssessmentType;
                    currentClaim.AdmissionSource = final.AdmissionSource;
                    currentClaim.UB4PatientStatus = final.UB4PatientStatus;
                    if (final.UB4PatientStatus != ((int)UB4PatientStatus.StillPatient).ToString())
                    {
                        currentClaim.DischargeDate = final.DischargeDate;
                    }
                    currentClaim.PrimaryInsuranceId = final.PrimaryInsuranceId;
                    if (final.PrimaryInsuranceId >= 1000)
                    {
                        currentClaim.HealthPlanId = final.HealthPlanId;
                        currentClaim.AuthorizationNumber = final.AuthorizationNumber;
                    }
                    currentClaim.AreOrdersComplete = final.AreOrdersComplete;
                    currentClaim.Modified = DateTime.Now;
                    database.Update<Final>(currentClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }
       
        public List<RapLean> GetRapsByIds(Guid agencyId, List<Guid> rapIds)
        {
            var list = new List<RapLean>();
            if (rapIds != null && rapIds.Count > 0)
            {
                var ids = rapIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT patientepisodes.StartDate as StartDate, patientepisodes.EndDate as EndDate , " +
                    "raps.Id as Id, raps.EpisodeId as EpisodeId , raps.FirstName as FirstName , raps.LastName as LastName , raps.PatientId as PatientId , raps.PatientIdNumber as PatientIdNumber , raps.MedicareNumber as MedicareNumber , raps.EpisodeStartDate as EpisodeStartDate, raps.EpisodeEndDate as EpisodeEndDate , raps.ProspectivePay as ProspectivePay " +
                    "FROM raps INNER JOIN patientepisodes ON patientepisodes.AgencyId = @agencyid AND raps.EpisodeId = patientepisodes.Id " +
                    "WHERE raps.AgencyId = @agencyid " +
                    "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0  AND raps.Id IN ( {0} )  ", ids);
          
                using (var cmd = new FluentCommand<RapLean>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new RapLean
                     {
                         Id = reader.GetGuid("Id"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                         EpisodeEndDate = reader.GetDateTime("EndDate"),
                         EpisodeStartDate = reader.GetDateTime("StartDate"),
                         ProspectivePay = (double)reader.GetDecimalNullable("ProspectivePay")
                     })
                     .AsList();
                }
            }
            return list;
        }

        public List<FinalLean> GetFinalsByIds(Guid agencyId, List<Guid> finalIds)
        {
            var list = new List<FinalLean>();
            if (finalIds != null && finalIds.Count > 0)
            {
                var ids = finalIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT patientepisodes.StartDate as StartDate, patientepisodes.EndDate as EndDate , " +
                    "finals.Id as Id, finals.EpisodeId as EpisodeId , finals.FirstName as FirstName , finals.LastName as LastName , finals.PatientId as PatientId , finals.PatientIdNumber as PatientIdNumber , finals.MedicareNumber as MedicareNumber , finals.EpisodeStartDate as EpisodeStartDate, finals.EpisodeEndDate as EpisodeEndDate , finals.ProspectivePay as ProspectivePay " +
                    "FROM finals INNER JOIN patientepisodes ON patientepisodes.AgencyId = @agencyid AND finals.EpisodeId = patientepisodes.Id " +
                    "WHERE finals.AgencyId = @agencyid " +
                    "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0  AND finals.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<FinalLean>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new FinalLean
                     {
                         Id = reader.GetGuid("Id"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                         EpisodeEndDate = reader.GetDateTime("EndDate"),
                         EpisodeStartDate = reader.GetDateTime("StartDate"),
                         ProspectivePay = (double)reader.GetDecimalNullable("ProspectivePay")
                     })
                     .AsList();
                }
            }
            return list;
        }

        public List<Rap> GetRapsToGenerateByIds(Guid agencyId, List<Guid> rapIds)
        {
            var list = new List<Rap>();
            if (rapIds != null && rapIds.Count > 0)
            {
                var ids = rapIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT raps.Id as Id , raps.AgencyId as AgencyId , raps.PatientId as PatientId , raps.EpisodeId as EpisodeId , raps.PatientIdNumber as PatientIdNumber, raps.EpisodeStartDate as EpisodeStartDate, raps.EpisodeEndDate as EpisodeEndDate ,raps.IsOasisComplete as IsOasisComplete, raps.IsFirstBillableVisit as IsFirstBillableVisit , raps.FirstBillableVisitDate as FirstBillableVisitDate , raps.IsGenerated as IsGenerated , raps.IsVerified as IsVerified , raps.Modified as Modified , raps.Created as Created , raps.Remark as Remark, raps.PaymentDate as PaymentDate , raps.MedicareNumber as MedicareNumber , "+
                    " raps.FirstName as FirstName , raps.LastName as LastName ,  raps.DOB as DOB , raps.Gender as Gender , " +
                    " raps.AddressLine1 as AddressLine1 , raps.AddressLine2 as AddressLine2 ,  raps.AddressCity as AddressCity , raps.AddressStateCode as AddressStateCode , raps.AddressZipCode as AddressZipCode , " +
                    " raps.StartofCareDate as StartofCareDate , raps.PhysicianNPI as PhysicianNPI ,  raps.PhysicianFirstName as PhysicianFirstName , raps.PhysicianLastName as PhysicianLastName ," +
                    " raps.DiagnosisCode as DiagnosisCode , raps.HippsCode as HippsCode ,  raps.ClaimKey as ClaimKey , raps.AreOrdersComplete as AreOrdersComplete ," +
                    " raps.PrimaryInsuranceId as PrimaryInsuranceId , raps.Status as Status ,  raps.Type as Type , raps.AdmissionSource as AdmissionSource , " +
                    " raps.PatientStatus as PatientStatus , raps.ClaimDate as ClaimDate ,  raps.Payment as Payment , raps.ProspectivePay as ProspectivePay , " +
                    " raps.AssessmentType as AssessmentType , raps.Comment as Comment ,  raps.DischargeDate as DischargeDate , raps.HealthPlanId as HealthPlanId ,  raps.AuthorizationNumber as AuthorizationNumber , raps.ConditionCodes as ConditionCodes , raps.UB4PatientStatus as UB4PatientStatus  " +
                    "FROM raps " +
                    "WHERE raps.AgencyId = @agencyid " +
                    "AND raps.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<Rap>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new Rap
                     {
                         Id = reader.GetGuid("Id"),
                         AgencyId = reader.GetGuid("AgencyId"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                         EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                         EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                         IsOasisComplete = reader.GetBoolean("IsOasisComplete"),
                         IsFirstBillableVisit = reader.GetBoolean("IsFirstBillableVisit"),
                         FirstBillableVisitDate = reader.GetDateTime("FirstBillableVisitDate"),
                         IsGenerated = reader.GetBoolean("IsGenerated"),
                         IsVerified = reader.GetBoolean("IsVerified"),
                         Modified = reader.GetDateTime("Modified"),
                         Created = reader.GetDateTime("Created"),
                         Remark = reader.GetStringNullable("Remark"),
                         PaymentDate = reader.GetDateTime("PaymentDate"),
                         MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         DOB = reader.GetDateTime("DOB"),
                         Gender = reader.GetStringNullable("Gender"),
                         AddressLine1 = reader.GetStringNullable("AddressLine1"),
                         AddressLine2 = reader.GetStringNullable("AddressLine2"),
                         AddressCity = reader.GetStringNullable("AddressCity"),
                         AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                         AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                         StartofCareDate = reader.GetDateTime("StartofCareDate"),
                         PhysicianNPI = reader.GetStringNullable("PhysicianNPI"),
                         PhysicianFirstName = reader.GetStringNullable("PhysicianFirstName"),
                         PhysicianLastName = reader.GetStringNullable("PhysicianLastName"),
                         DiagnosisCode = reader.GetStringNullable("DiagnosisCode"),
                         HippsCode = reader.GetStringNullable("HippsCode"),
                         ClaimKey = reader.GetStringNullable("ClaimKey"),
                         AreOrdersComplete = reader.GetBoolean("AreOrdersComplete"),
                         PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId"),
                         Status = reader.GetInt("Status"),
                         Type = reader.GetInt("Type"),
                         AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                         PatientStatus = reader.GetInt("PatientStatus"),
                         ClaimDate = reader.GetDateTime("ClaimDate"),
                         Payment = (double)reader.GetDecimalNullable("Payment"),
                         ProspectivePay = (double)reader.GetDecimalNullable("ProspectivePay"),
                         AssessmentType = reader.GetStringNullable("AssessmentType"),
                         Comment = reader.GetStringNullable("Comment"),
                         DischargeDate = reader.GetDateTime("DischargeDate"),
                         HealthPlanId = reader.GetStringNullable("HealthPlanId"),
                         AuthorizationNumber = reader.GetStringNullable("AuthorizationNumber"),
                         ConditionCodes = reader.GetStringNullable("ConditionCodes"),
                         UB4PatientStatus = reader.GetStringNullable("UB4PatientStatus")
                         
                     }).AsList();
                }
            }
            return list;
        }

        public List<Final> GetFinalsToGenerateByIds(Guid agencyId, List<Guid> finalIds)
        {
            var list = new List<Final>();
            if (finalIds != null && finalIds.Count > 0)
            {
                var ids = finalIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT finals.Id as Id , finals.AgencyId as AgencyId , finals.PatientId as PatientId , finals.EpisodeId as EpisodeId , finals.PatientIdNumber as PatientIdNumber, finals.EpisodeStartDate as EpisodeStartDate, finals.EpisodeEndDate as EpisodeEndDate ,finals.IsOasisComplete as IsOasisComplete, finals.IsFirstBillableVisit as IsFirstBillableVisit , finals.FirstBillableVisitDate as FirstBillableVisitDate ,  finals.Remark as Remark , finals.MedicareNumber as MedicareNumber , " +
                    " finals.FirstName as FirstName , finals.LastName as LastName ,  finals.DOB as DOB , finals.Gender as Gender , finals.PaymentDate as PaymentDate , " +
                    " finals.AddressLine1 as AddressLine1 , finals.AddressLine2 as AddressLine2 , finals.AddressCity as AddressCity , finals.AddressStateCode as AddressStateCode , finals.AddressZipCode as AddressZipCode , " +
                    " finals.StartofCareDate as StartofCareDate , finals.PhysicianNPI as PhysicianNPI , finals.PhysicianFirstName as PhysicianFirstName , finals.PhysicianLastName as PhysicianLastName ," +
                    " finals.DiagnosisCode as DiagnosisCode , finals.HippsCode as HippsCode , finals.ClaimKey as ClaimKey , finals.AreOrdersComplete as AreOrdersComplete ,  finals.AreVisitsComplete as AreVisitsComplete ," +
                    " finals.IsGenerated as IsGenerated , finals.Modified as Modified , finals.Created as Created , "+
                    " finals.VerifiedVisits as VerifiedVisits , finals.Supply as Supply , finals.SupplyTotal as SupplyTotal , finals.Payment as Payment ,  " +
                    " finals.PrimaryInsuranceId as PrimaryInsuranceId , finals.IsSupplyVerified as IsSupplyVerified , finals.IsFinalInfoVerified as IsFinalInfoVerified , finals.IsVisitVerified as IsVisitVerified , finals.IsRapGenerated as IsRapGenerated , " +
                    " finals.Status as Status , finals.Type as Type , finals.AdmissionSource as AdmissionSource , " +
                    " finals.PatientStatus as PatientStatus , finals.ClaimDate as ClaimDate ,  finals.ProspectivePay as ProspectivePay , " +
                    " finals.AssessmentType as AssessmentType , finals.Comment as Comment , finals.DischargeDate as DischargeDate , finals.HealthPlanId as HealthPlanId ,  finals.AuthorizationNumber as AuthorizationNumber , finals.ConditionCodes as ConditionCodes  , finals.UB4PatientStatus as UB4PatientStatus " +
                    "FROM finals " +
                    "WHERE finals.AgencyId = @agencyid " +
                    "AND finals.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<Final>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new Final
                     {
                         Id = reader.GetGuid("Id"),
                         AgencyId = reader.GetGuid("AgencyId"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                         EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                         EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                         IsOasisComplete = reader.GetBoolean("IsOasisComplete"),
                         IsFirstBillableVisit = reader.GetBoolean("IsFirstBillableVisit"),
                         FirstBillableVisitDate = reader.GetDateTime("FirstBillableVisitDate"),
                         Remark = reader.GetStringNullable("Remark"),
                         MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         DOB = reader.GetDateTime("DOB"),
                         Gender = reader.GetStringNullable("Gender"),
                         PaymentDate = reader.GetDateTime("PaymentDate"),
                         AddressLine1 = reader.GetStringNullable("AddressLine1"),
                         AddressLine2 = reader.GetStringNullable("AddressLine2"),
                         AddressCity = reader.GetStringNullable("AddressCity"),
                         AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                         AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                         StartofCareDate = reader.GetDateTime("StartofCareDate"),
                         PhysicianNPI = reader.GetStringNullable("PhysicianNPI"),
                         PhysicianFirstName = reader.GetStringNullable("PhysicianFirstName"),
                         PhysicianLastName = reader.GetStringNullable("PhysicianLastName"),
                         DiagnosisCode = reader.GetStringNullable("DiagnosisCode"),
                         HippsCode = reader.GetStringNullable("HippsCode"),
                         ClaimKey = reader.GetStringNullable("ClaimKey"),
                         AreOrdersComplete = reader.GetBoolean("AreOrdersComplete"),
                         AreVisitsComplete = reader.GetBoolean("AreVisitsComplete"),
                         IsGenerated = reader.GetBoolean("IsGenerated"),
                         Modified = reader.GetDateTime("Modified"),
                         Created = reader.GetDateTime("Created"),
                         VerifiedVisits = reader.GetStringNullable("VerifiedVisits"),
                         Supply = reader.GetStringNullable("Supply"),
                         SupplyTotal = (double)reader.GetDecimalNullable("SupplyTotal"),
                         Payment = (double)reader.GetDecimalNullable("Payment"),
                         PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId"),
                         IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                         IsFinalInfoVerified = reader.GetBoolean("IsFinalInfoVerified"),
                         IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                         IsRapGenerated = reader.GetBoolean("IsRapGenerated"),
                         Status = reader.GetInt("Status"),
                         Type = reader.GetInt("Type"),
                         AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                         PatientStatus = reader.GetInt("PatientStatus"),
                         ClaimDate = reader.GetDateTime("ClaimDate"),
                         ProspectivePay = (double)reader.GetDecimalNullable("ProspectivePay"),
                         AssessmentType = reader.GetStringNullable("AssessmentType"),
                         Comment = reader.GetStringNullable("Comment"),
                         DischargeDate = reader.GetDateTime("DischargeDate"),
                         HealthPlanId = reader.GetStringNullable("HealthPlanId"),
                         AuthorizationNumber = reader.GetStringNullable("AuthorizationNumber"),
                         ConditionCodes = reader.GetStringNullable("ConditionCodes"),
                         UB4PatientStatus = reader.GetStringNullable("UB4PatientStatus")
                     })
                     .AsList();
                }
            }
            return list;
        }

        public List<ClaimHistoryLean> GetRapsHistory(Guid agencyId, Guid patientId, int insuranceId)
        {
            //var insurance = string.Empty;
            //if (insuranceId > 0)
            //{
            //    insurance = "AND raps.PrimaryInsuranceId = @insuranceId";
            //}
            //else
            //{
            //    insurance = "AND raps.PrimaryInsuranceId <= 0";
            //}
            var script = string.Format(@"SELECT patientepisodes.StartDate as StartDate, patientepisodes.EndDate as EndDate , " +
                "raps.Id as Id, raps.EpisodeId as EpisodeId , raps.Status as Status, raps.ClaimDate as ClaimDate, raps.PaymentDate as PaymentDate, raps.Payment as Payment , raps.EpisodeStartDate as EpisodeStartDate, raps.EpisodeEndDate as EpisodeEndDate , raps.ProspectivePay as ProspectivePay " +
                "FROM raps INNER JOIN patientepisodes ON patientepisodes.AgencyId = @agencyid AND raps.EpisodeId = patientepisodes.Id " +
                "WHERE raps.AgencyId = @agencyid AND raps.PatientId = @patientId " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 ");

            return new FluentCommand<ClaimHistoryLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddInt("insuranceId", insuranceId)
                .SetMap(reader => new ClaimHistoryLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientId=patientId,
                    EpisodeId=reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    Type = "RAP",
                    ClaimDate = reader.GetDateTime("ClaimDate"),
                    ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay")
                })
                .AsList();
        }

        public List<ClaimHistoryLean> GetFinalsHistory(Guid agencyId, Guid patientId, int insuranceId)
        {
            //var insurance = string.Empty;
            //if (insuranceId > 0)
            //{
            //    insurance = "AND finals.PrimaryInsuranceId = @insuranceId";
            //}
            //else
            //{
            //    insurance = "AND finals.PrimaryInsuranceId <= 0";
            //}
            var script = string.Format(@"SELECT patientepisodes.StartDate as StartDate, patientepisodes.EndDate as EndDate , " +
                "finals.Id as Id, finals.EpisodeId as EpisodeId , finals.Status as Status, finals.ClaimDate as ClaimDate, finals.PaymentDate as PaymentDate, finals.Payment as Payment , finals.EpisodeStartDate as EpisodeStartDate, finals.EpisodeEndDate as EpisodeEndDate , finals.ProspectivePay as ProspectivePay " +
                "FROM finals INNER JOIN patientepisodes ON patientepisodes.AgencyId = @agencyid AND finals.EpisodeId = patientepisodes.Id " +
                "WHERE finals.AgencyId = @agencyid AND finals.PatientId = @patientId " +
                "AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 ");

            return new FluentCommand<ClaimHistoryLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddInt("insuranceId", insuranceId)
                .SetMap(reader => new ClaimHistoryLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    Type = "FINAL",
                    ClaimDate = reader.GetDateTime("ClaimDate"),
                    ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay")
                })
                .AsList();
        }

        public IList<TypeOfBill> GetOutstandingRaps(Guid agencyId)
        {
            var script = @"SELECT patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName , patients.PatientIdNumber  " +
                "FROM raps INNER JOIN patients ON raps.PatientId = patients.Id INNER JOIN patientepisodes ON raps.EpisodeId = patientepisodes.Id " +
                "WHERE raps.AgencyId = @agencyid  AND patients.Status = 1 AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND raps.Status IN (300, 335, 301, 310) ORDER BY patientepisodes.EndDate ASC";

            return new FluentCommand<TypeOfBill>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new TypeOfBill
                {
                    Type = "RAP",
                    SortData = reader.GetDateTime("EndDate").ToShortDateString().ToOrderedDate(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    EpisodeRange = reader.GetDateTime("StartDate").ToString("MM/dd/yyyy") + " - " + reader.GetDateTime("EndDate").ToString("MM/dd/yyyy")
                })
                .AsList();
        }

        public List<TypeOfBill> GetRapsByStatus(Guid agencyId, int status)
        {
            var script = @"SELECT patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName , patients.PatientIdNumber  " +
                "FROM raps " +
                "INNER JOIN patients ON raps.PatientId = patients.Id " +
                "INNER JOIN patientepisodes ON raps.EpisodeId = patientepisodes.Id " +
                "WHERE raps.AgencyId = @agencyid  AND ( patients.Status = 1 || patients.Status = 2 ) AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND raps.Status = @status ORDER BY patientepisodes.EndDate ASC";

            return new FluentCommand<TypeOfBill>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .SetMap(reader => new TypeOfBill
                {
                    Type = "RAP",
                    SortData = reader.GetDateTime("EndDate").ToShortDateString().ToOrderedDate(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    EpisodeRange = reader.GetDateTime("StartDate").ToString("MM/dd/yyyy") + " - " + reader.GetDateTime("EndDate").ToString("MM/dd/yyyy")
                })
                .AsList();
        }

        public IList<ClaimBill> GetOutstandingRapClaims(Guid agencyId, Guid branchId, int insuranceId)
        {
            var list = new List<Claim>();
            var insurance = string.Empty;
            if (insuranceId > 0)
            {
                insurance = "AND  patients.PrimaryInsurance > 0 AND raps.PrimaryInsuranceId = @insuranceId";
            }
            else
            {
                insurance = "AND patients.PrimaryInsurance > 0 AND raps.PrimaryInsuranceId <= 0";
            }
            var script = string.Format(@"SELECT patientepisodes.Id as EpisodeId, patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName, patients.PatientIdNumber, patientepisodes.Schedule, patients.Id as PatientId, raps.Id, raps.Status, raps.IsFirstBillableVisit, raps.IsOasisComplete, raps.IsVerified , raps.HippsCode as HippsCode , raps.ProspectivePay as ProspectivePay  " +
                "FROM raps " +
                "INNER JOIN patients ON raps.PatientId = patients.Id " +
                "INNER JOIN patientepisodes ON raps.EpisodeId = patientepisodes.Id " +
                "WHERE raps.AgencyId = @agencyid AND patients.AgencyLocationId = @branchId {0} AND (patients.Status = 1 OR patients.Status = 2)  AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND raps.Status IN (300, 335, 301, 310) AND patientepisodes.StartDate <=  CURDATE()  ORDER BY patients.LastName ASC , patients.FirstName ASC ",
                insurance);

            return new FluentCommand<ClaimBill>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("insuranceId", insuranceId)
                .SetMap(reader => new ClaimBill
                {
                    Id = reader.GetGuid("Id"),
                    Status = reader.GetInt("Status"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    IsVerified = reader.GetBoolean("IsVerified"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    IsOasisComplete = reader.GetBoolean("IsOasisComplete"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    IsFirstBillableVisit = reader.GetBoolean("IsFirstBillableVisit"),
                    HippsCode = reader.GetStringNullable("HippsCode"),
                    ProspectivePay = (double)reader.GetDecimal("ProspectivePay")
                })
                .AsList();
        }

        public List<TypeOfBill> GetFinalsByStatus(Guid agencyId, int status)
        {
            var script = @"SELECT patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName , patients.PatientIdNumber  " +
                "FROM finals INNER JOIN patients ON finals.PatientId = patients.Id INNER JOIN patientepisodes ON finals.EpisodeId = patientepisodes.Id " +
                "WHERE finals.AgencyId = @agencyid AND ( patients.Status = 1 || patients.Status = 2 )  AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND finals.Status = @status ORDER BY patientepisodes.EndDate ASC";

            return new FluentCommand<TypeOfBill>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .SetMap(reader => new TypeOfBill
                {
                    Type = "Final",
                    SortData = reader.GetDateTime("EndDate").ToShortDateString().ToOrderedDate(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    EpisodeRange = reader.GetDateTime("StartDate").ToString("MM/dd/yyyy") + " - " + reader.GetDateTime("EndDate").ToString("MM/dd/yyyy"),
                })
                .AsList();
        }

        public IList<TypeOfBill> GetOutstandingFinals(Guid agencyId)
        {
            var script = @"SELECT patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName , patients.PatientIdNumber  " +
                "FROM finals INNER JOIN patients ON finals.PatientId = patients.Id INNER JOIN patientepisodes ON finals.EpisodeId = patientepisodes.Id " +
                "WHERE finals.AgencyId = @agencyid AND patients.Status = 1 AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
                "AND finals.Status IN (300, 335, 301, 310) ORDER BY patientepisodes.EndDate ASC";

            return new FluentCommand<TypeOfBill>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new TypeOfBill
                {
                    Type = "Final",
                    SortData = reader.GetDateTime("EndDate").ToShortDateString().ToOrderedDate(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    EpisodeRange = reader.GetDateTime("StartDate").ToString("MM/dd/yyyy") + " - " + reader.GetDateTime("EndDate").ToString("MM/dd/yyyy"),
                })
                .AsList();
        }

        public IList<ClaimBill> GetOutstandingFinalClaims(Guid agencyId, Guid branchId, int insuranceId)
        {
            var insurance = string.Empty;
            if (insuranceId != 0)
            {
                insurance = " AND patients.PrimaryInsurance > 0 AND  finals.PrimaryInsuranceId = @insuranceId";
            }
            else
            {
                insurance = " AND patients.PrimaryInsurance > 0 AND  finals.PrimaryInsuranceId <= 0";
            }
            var script = string.Format(@"SELECT patientepisodes.Id as EpisodeId, patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName, patients.PatientIdNumber, patientepisodes.Schedule, patients.Id as PatientId, finals.Id, finals.AreVisitsComplete, finals.IsRapGenerated, finals.IsFinalInfoVerified, finals.IsVisitVerified, finals.Status, finals.IsFirstBillableVisit, finals.IsOasisComplete, finals.AreOrdersComplete, finals.IsSupplyVerified , finals.HippsCode as HippsCode , finals.ProspectivePay as  ProspectivePay  " +
                "FROM finals " +
                "INNER JOIN patients ON finals.PatientId = patients.Id " +
                "INNER JOIN patientepisodes ON finals.EpisodeId = patientepisodes.Id " +
                "WHERE finals.AgencyId = @agencyid AND patients.AgencyLocationId = @branchId {0} AND (patients.Status = 1 OR patients.Status = 2)  AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND patientepisodes.EndDate <= Curdate() AND finals.Status IN (300, 335, 301, 310) ORDER BY patients.LastName ASC , patients.FirstName ASC",
                insurance);

            return new FluentCommand<ClaimBill>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("insuranceId", insuranceId)
                .SetMap(reader => new ClaimBill
                {
                    Id = reader.GetGuid("Id"),
                    Status = reader.GetInt("Status"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    Schedule = reader.GetStringNullable("Schedule"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    IsRapGenerated = reader.GetBoolean("IsRapGenerated"),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    IsOasisComplete = reader.GetBoolean("IsOasisComplete"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    AreVisitsComplete = reader.GetBoolean("AreVisitsComplete"),
                    AreOrdersComplete = reader.GetBoolean("AreOrdersComplete"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    IsFirstBillableVisit = reader.GetBoolean("IsFirstBillableVisit"),
                    IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                    IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                    IsFinalInfoVerified = reader.GetBoolean("IsFinalInfoVerified"),
                    HippsCode = reader.GetStringNullable("HippsCode"),
                    ProspectivePay = (double)reader.GetDecimal("ProspectivePay")
                }).AsList();
        }

        public IList<ClaimBill> GetPotentialCliamAutoCancels(Guid agencyId, Guid branchId)
        {
            var script = string.Format(@"SELECT finals.Id as Id , finals.PatientId as PatientId, finals.EpisodeId as EpisodeId, patientepisodes.StartDate as StartDate , patientepisodes.EndDate as EndDate, finals.FirstName as FirstName, finals.LastName as LastName, finals.PatientIdNumber as PatientIdNumber, finals.Status as Status , finals.ClaimDate as ClaimDate , finals.HippsCode as HippsCode , finals.ProspectivePay as  ProspectivePay  " +
                "FROM finals " +
                "INNER JOIN patients ON finals.PatientId = patients.Id " +
                "INNER JOIN patientepisodes ON finals.EpisodeId = patientepisodes.Id " +
                "INNER JOIN raps ON finals.PatientId = raps.PatientId AND finals.EpisodeId = raps.EpisodeId " +
                "WHERE raps.AgencyId = @agencyid {0}  AND ( patients.Status = 1 OR patients.Status = 2 )  AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND DATEDIFF( Curdate() ,raps.ClaimDate ) >= 76 AND finals.Status IN (300, 301, 310 ,325) AND raps.Status IN ( 305, 315 , 320, 330 ) ORDER BY finals.LastName ASC ,finals.FirstName ASC ", !branchId.IsEmpty() ? "AND patients.AgencyLocationId=@branchId" : string.Empty);

            return new FluentCommand<ClaimBill>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new ClaimBill
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    FirstName = reader.GetString("FirstName").ToUpperCase(),
                    LastName = reader.GetString("LastName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    Status = reader.GetInt("Status"),
                    ClaimDate = reader.GetDateTime("ClaimDate"),
                    HippsCode = reader.GetStringNullable("HippsCode"),
                    ProspectivePay = (double)reader.GetDecimal("ProspectivePay")
                }).AsList();
        }

        public IList<ClaimBill> GetPPSRapClaims(Guid agencyId, Guid branchId)
        {
            var script = string.Format(@"SELECT patientepisodes.Id as EpisodeId, patientepisodes.StartDate as StartDate , patientepisodes.EndDate as EndDate , raps.FirstName as FirstName , raps.LastName as LastName , raps.PatientIdNumber as PatientIdNumber ,  raps.PatientId as PatientId, raps.Id as Id, raps.Status as Status,  raps.HippsCode as HippsCode , raps.ProspectivePay as ProspectivePay  " +
                "FROM raps " +
                "INNER JOIN patients ON raps.PatientId = patients.Id " +
                "INNER JOIN patientepisodes ON raps.EpisodeId = patientepisodes.Id " +
                "WHERE raps.AgencyId = @agencyid {0} AND (patients.Status = 1 OR patients.Status = 2)  AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND raps.Status IN (300, 335, 301, 310) ORDER BY patientepisodes.EndDate ASC",
                !branchId.IsEmpty() ? "AND patients.AgencyLocationId=@branchId" : string.Empty);

            return new FluentCommand<ClaimBill>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new ClaimBill
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    LastName = reader.GetString("LastName").ToUpperCase(),
                    FirstName = reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    HippsCode = reader.GetStringNullable("HippsCode"),
                    ProspectivePay = (double)reader.GetDecimal("ProspectivePay"),
                    Status = reader.GetInt("Status")
                })
                .AsList();
        }

        public IList<ClaimBill> GetPPSFinalClaims(Guid agencyId, Guid branchId)
        {
            var script = string.Format(@"SELECT patientepisodes.Id as EpisodeId, patientepisodes.StartDate, patientepisodes.EndDate, finals.FirstName, finals.LastName, finals.PatientIdNumber,  finals.PatientId as PatientId, finals.Id as Id, finals.Status as Status , finals.HippsCode as HippsCode , finals.ProspectivePay as  ProspectivePay  " +
                "FROM finals " +
                "INNER JOIN patients ON finals.PatientId = patients.Id " +
                "INNER JOIN patientepisodes ON finals.EpisodeId = patientepisodes.Id " +
                "WHERE finals.AgencyId = @agencyid {0} AND (patients.Status = 1 OR patients.Status = 2)  AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND patientepisodes.EndDate <= Curdate() AND finals.Status IN (300, 335, 301, 310) ORDER BY patientepisodes.EndDate ASC",
                !branchId.IsEmpty() ? "AND patients.AgencyLocationId=@branchId" : string.Empty);

            return new FluentCommand<ClaimBill>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .SetMap(reader => new ClaimBill
                {
                    Id = reader.GetGuid("Id"),
                    Status = reader.GetInt("Status"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EpisodeEndDate = reader.GetDateTime("EndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate"),
                    LastName = reader.GetString("LastName").ToUpperCase(),
                    FirstName = reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    HippsCode = reader.GetStringNullable("HippsCode"),
                    ProspectivePay = (double)reader.GetDecimal("ProspectivePay")
                }).AsList();
        }
        
        public bool DeleteRap(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var rap = database.Single<Rap>(r => r.AgencyId == agencyId && r.PatientId == patientId && r.Id == episodeId);
            try
            {
                if (rap != null)
                {
                    database.Delete<Rap>(rap.Id);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteFinal(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var final = database.Single<Final>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == episodeId);
            try
            {
                if (final != null)
                {
                    database.Delete<Final>(final.Id);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public long AddClaimData(ClaimData claimData)
        {
            long claimId = -1;
            if (claimData != null)
            {
                claimData.Created = DateTime.Now;
                claimData.Modified = DateTime.Now;
                try
                {
                    database.Add<ClaimData>(claimData);
                    claimId = claimData.Id;
                }
                catch (Exception ex)
                {
                    return claimId;
                }
            }
            return claimId;
        }

        public bool UpdateClaimData(ClaimData claimData)
        {
            bool result = false;
            if (claimData != null)
            {
                try
                {
                    var claimDataToEdit = database.Single<ClaimData>(c => c.AgencyId == claimData.AgencyId && c.Id == claimData.Id);
                    if (claimDataToEdit != null)
                    {
                        claimDataToEdit.Data = claimData.Data;
                        claimDataToEdit.BillIdentifers = claimData.BillIdentifers;
                        claimDataToEdit.ClaimType = claimData.ClaimType;
                        claimDataToEdit.Modified = DateTime.Now;
                        database.Update<ClaimData>(claimDataToEdit);
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool DeleteClaimData(Guid agencyId, long claimId)
        {
            bool result = false;
            try
            {
                var claimDataToDelete = database.Single<ClaimData>(c => c.AgencyId == agencyId && c.Id == claimId);
                if (claimDataToDelete != null)
                {
                    database.Delete<ClaimData>(claimDataToDelete.Id);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public List<Guid> GetClaimIds(Guid agencyId, Guid patientId, string type)
        {
            List<Guid> ids = null;
            if (type.IsEqual("rap"))
            {
                ids = database.Find<Rap>(r => r.AgencyId == agencyId && r.PatientId == patientId).Select(rs => rs.Id).ToList();
            }
            else if (type.IsEqual("final"))
            {
                ids = database.Find<Final>(f => f.AgencyId == agencyId && f.PatientId == patientId).Select(fs => fs.Id).ToList();
            }

            return ids;
        }

        public ClaimData GetClaimData(Guid  agencyId, int ansiId)
        {
            return database.Single<ClaimData>(c => c.AgencyId == agencyId && c.Id == ansiId);
        }

        public List<ClaimData> GetClaimData(Guid agencyId, string claimType, DateTime batchDate)
        {
            if (claimType.IsEqual("ALL"))
            {
                return database.Find<ClaimData>(c => c.AgencyId == agencyId).Where(c => c.Created.ToShortDateString().IsEqual(batchDate.Date.ToShortDateString())).ToList();
            }
            return database.Find<ClaimData>(c => c.AgencyId == agencyId && c.ClaimType == claimType).Where(c => c.Created.ToShortDateString().IsEqual(batchDate.Date.ToShortDateString())).ToList();
        }

        public AxxessSubmitterInfo SubmitterInfo(int payerId)
        {
            return database.Single<AxxessSubmitterInfo>(s => s.Id == payerId);
        }

        public void MarkRapsAsSubmitted(Guid agencyId, List<Rap> raps)
        {
            raps.ForEach(rap =>
            {
                var rapToEdt = database.Single<Rap>(r => r.AgencyId == agencyId && r.Id == rap.Id);
                if (rapToEdt != null)
                {
                    rapToEdt.Status = (int)ScheduleStatus.ClaimSubmitted;
                    rapToEdt.IsGenerated = true;
                    rapToEdt.ClaimDate = DateTime.Now;
                    database.Update<Rap>(rapToEdt);
                }
            });
        }

        public void MarkFinalsAsSubmitted(Guid agencyId, List<Final> finals)
        {
            finals.ForEach(final =>
            {
                var finalToEdit = database.Single<Final>(f => f.AgencyId == agencyId && f.Id == final.Id);
                if (finalToEdit != null)
                {
                    finalToEdit.Status = (int)ScheduleStatus.ClaimSubmitted;
                    finalToEdit.IsGenerated = true;
                    finalToEdit.ClaimDate = DateTime.Now;
                    database.Update<Final>(finalToEdit);
                }
            });
        }

        public bool AddRapSnapShots(List<Rap> raps, long batchId)
        {
            bool result = false;
            if (raps != null && raps.Count > 0)
            {
                try
                {
                    raps.ForEach(rap =>
                    {
                        var rapSnapShot = new RapSnapShot
                        {
                            MainId = string.Format("{0}-{1}", rap.Id, batchId),
                            Id = rap.Id,
                            BatchId = batchId,
                            AgencyId = rap.AgencyId,
                            PatientId = rap.PatientId,
                            EpisodeId = rap.EpisodeId,
                            PatientIdNumber = rap.PatientIdNumber,
                            EpisodeStartDate = rap.EpisodeStartDate,
                            EpisodeEndDate = rap.EpisodeEndDate,
                            IsOasisComplete = true,
                            IsFirstBillableVisit = true,
                            FirstBillableVisitDate = rap.FirstBillableVisitDate,
                            IsGenerated = true,
                            IsVerified = true,
                            Remark = rap.Remark,
                            MedicareNumber = rap.MedicareNumber,
                            FirstName = rap.FirstName,
                            LastName = rap.LastName,
                            DOB = rap.DOB,
                            Gender = rap.Gender,
                            AddressLine1 = rap.AddressLine1,
                            AddressLine2 = rap.AddressLine2,
                            AddressCity = rap.AddressCity,
                            AddressStateCode = rap.AddressStateCode,
                            AddressZipCode = rap.AddressZipCode,
                            StartofCareDate = rap.StartofCareDate,
                            PhysicianNPI = rap.PhysicianNPI,
                            PhysicianFirstName = rap.PhysicianFirstName,
                            PhysicianLastName = rap.PhysicianLastName,
                            DiagnosisCode = rap.DiagnosisCode,
                            HippsCode = rap.HippsCode,
                            ClaimKey = rap.ClaimKey,
                            AreOrdersComplete = true,
                            PrimaryInsuranceId = rap.PrimaryInsuranceId,
                            Status = (int)ScheduleStatus.ClaimSubmitted,
                            Type = rap.Type,
                            AdmissionSource = rap.AdmissionSource,
                            PatientStatus = rap.PatientStatus,
                            
                            ClaimDate = DateTime.Now,
                            Payment = 00.00,
                            ProspectivePay = rap.ProspectivePay,
                            AssessmentType = rap.AssessmentType,
                            Comment = rap.Comment,
                            Reason = string.Empty,
                            Modified = DateTime.Now,
                            Created = DateTime.Now
                        };
                        database.Add<RapSnapShot>(rapSnapShot);

                    });
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool AddFinaSnapShots(List<Final> finals, long batchId)
        {
            bool result = false;
            if (finals != null && finals.Count > 0)
            {
                try
                {
                    finals.ForEach(final =>
                    {

                        var finalSnapShot = new FinalSnapShot
                        {
                            MainId = string.Format("{0}-{1}", final.Id, batchId),
                            Id = final.Id,
                            BatchId = batchId,
                            AgencyId = final.AgencyId,
                            PatientId = final.PatientId,
                            EpisodeId = final.EpisodeId,
                            PatientIdNumber = final.PatientIdNumber,
                            EpisodeStartDate = final.EpisodeStartDate,
                            EpisodeEndDate = final.EpisodeEndDate,
                            IsOasisComplete = true,
                            IsFirstBillableVisit = true,
                            FirstBillableVisitDate = final.FirstBillableVisitDate,
                            IsGenerated = true,
                            AreVisitsComplete = true,
                            VerifiedVisits = final.VerifiedVisits,
                            Supply=final.Supply,
                            SupplyTotal=final.SupplyTotal,
                            IsSupplyVerified=true,
                            IsFinalInfoVerified=true,
                            IsVisitVerified=true,
                            IsRapGenerated=true,
                            Remark = final.Remark,
                            MedicareNumber = final.MedicareNumber,
                            FirstName = final.FirstName,
                            LastName = final.LastName,
                            DOB = final.DOB,
                            Gender = final.Gender,
                            AddressLine1 = final.AddressLine1,
                            AddressLine2 = final.AddressLine2,
                            AddressCity = final.AddressCity,
                            AddressStateCode = final.AddressStateCode,
                            AddressZipCode = final.AddressZipCode,
                            StartofCareDate = final.StartofCareDate,
                            PhysicianNPI = final.PhysicianNPI,
                            PhysicianFirstName = final.PhysicianFirstName,
                            PhysicianLastName = final.PhysicianLastName,
                            DiagnosisCode = final.DiagnosisCode,
                            HippsCode = final.HippsCode,
                            ClaimKey = final.ClaimKey,
                            AreOrdersComplete = true,
                            PrimaryInsuranceId = final.PrimaryInsuranceId,
                            Status = (int)ScheduleStatus.ClaimSubmitted,
                            Type = final.Type,
                            AdmissionSource = final.AdmissionSource,
                            PatientStatus = final.PatientStatus,
                            ClaimDate = DateTime.Now,
                            Payment = 00.00,
                            ProspectivePay = final.ProspectivePay,
                            AssessmentType = final.AssessmentType,
                            Comment = final.Comment,
                            Reason = string.Empty,
                            Modified = DateTime.Now,
                            Created = DateTime.Now
                        };
                        database.Add<FinalSnapShot>(finalSnapShot);

                    });
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool DeleteRapSnapShots(long batchId)
        {
            bool result = false;
            try
            {
                database.DeleteMany<RapSnapShot>(r => r.BatchId == batchId);
                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteFinaSnapShots(long batchId)
        {
            bool result = false;
            try
            {
                database.DeleteMany<FinalSnapShot>(r => r.BatchId == batchId);
                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public List<RapSnapShot> GetRapSnapShots(Guid agencyId, Guid Id)
        {
            return database.Find<RapSnapShot>(r => r.AgencyId == agencyId && r.Id==Id).ToList();
        }

        public List<FinalSnapShot> GetFinalSnapShots(Guid agencyId, Guid Id)
        {
            return database.Find<FinalSnapShot>(r => r.AgencyId == agencyId && r.Id == Id).ToList();
        }

        public RapSnapShot GetRapSnapShot(Guid agencyId, Guid Id,long batchId)
        {
            return database.Single<RapSnapShot>(r => r.AgencyId == agencyId && r.Id == Id && r.BatchId==batchId);
        }

        public RapSnapShot GetLastRapSnapShot(Guid agencyId, Guid Id)
        {
            return database.Find<RapSnapShot>(r => r.AgencyId == agencyId && r.Id == Id).OrderBy(r => r.BatchId).LastOrDefault();
        }

        public long GetLastRapSnapShotBatchId(Guid agencyId, Guid Id)
        {
            return database.Find<RapSnapShot>(r => r.AgencyId == agencyId && r.Id == Id).Max(r=>r.BatchId);
        }

        public FinalSnapShot GetFinalSnapShot(Guid agencyId, Guid Id, long batchId)
        {
            return database.Single<FinalSnapShot>(r => r.AgencyId == agencyId && r.Id == Id && r.BatchId == batchId);
        }

        public FinalSnapShot GetLastFinalSnapShot(Guid agencyId, Guid Id)
        {
           return database.Find<FinalSnapShot>(f=>f.AgencyId==agencyId && f.Id==Id).OrderBy(f=>f.BatchId).LastOrDefault();
        }

        public long GetLastFinalSnapShotBatchId(Guid agencyId, Guid Id)
        {
            return database.Find<FinalSnapShot>(f => f.AgencyId == agencyId && f.Id == Id).Max(f=>f.BatchId);
        }

        public bool UpdateRapSnapShots(RapSnapShot rapSnapShot)
        {
            bool result = false;
            if (rapSnapShot != null)
            {
                try
                {
                    database.Update<RapSnapShot>(rapSnapShot);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateFinalSnapShots(FinalSnapShot finalSnapShot)
        {
            bool result = false;
            if (finalSnapShot != null)
            {
                try
                {
                    database.Update<FinalSnapShot>(finalSnapShot);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool AddManagedClaim(ManagedClaim managedClaim)
        {
            bool result = false;
            if (managedClaim != null)
            {
                try
                {
                    managedClaim.Created = DateTime.Now;
                    managedClaim.Modified = DateTime.Now;
                    database.Add<ManagedClaim>(managedClaim);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public void MarkManagedClaimsAsSubmitted(Guid agencyId, List<ManagedClaim> managedClaims)
        {
            managedClaims.ForEach(claim =>
            {
                var claimToEdit = database.Single<ManagedClaim>(m => m.AgencyId == agencyId && m.Id == claim.Id);
                if (claimToEdit != null)
                {
                    claimToEdit.Status = (int)ManagedClaimStatus.ClaimSubmitted;
                    claimToEdit.IsGenerated = true;
                    claimToEdit.ClaimDate = DateTime.Now;
                    database.Update<ManagedClaim>(claimToEdit);
                }
            });
        }

        public IList<ManagedClaimLean> GetManagedClaimsPerPatient(Guid agencyId, Guid patientId, int insuranceId)
        {

            var insurance = string.Empty;
            if (insuranceId > 0)
            {
                insurance = "AND managedclaims.PrimaryInsuranceId = @insuranceId";
            }
            else if (insuranceId == -1)
            {
                insurance = "AND managedclaims.PrimaryInsuranceId <= 0";
            }
            else
            {
            }

            var script =string.Format(@"SELECT managedclaims.Id as Id, managedclaims.PatientId as PatientId, managedclaims.FirstName as FirstName, managedclaims.LastName as LastName, managedclaims.IsuranceIdNumber as IsuranceIdNumber, " +
               "managedclaims.PaymentDate as PaymentDate, managedclaims.Payment as Payment, managedclaims.Status as Status, managedclaims.EpisodeStartDate as EpisodeStartDate, " +
               "managedclaims.EpisodeEndDate as EpisodeEndDate, managedclaims.ClaimDate as ClaimDate, managedclaims.ProspectivePay as ProspectivePay, managedclaims.IsInfoVerified as IsInfoVerified, " +
               "managedclaims.IsSupplyVerified as IsSupplyVerified, managedclaims.IsVisitVerified as IsVisitVerified, agencyinsurances.PayorType as PayorType " +
               "FROM managedclaims INNER JOIN agencyinsurances ON managedclaims.PrimaryInsuranceId = agencyinsurances.Id  Where managedclaims.PatientId=@patientid AND managedclaims.AgencyId = @agencyid AND agencyinsurances.AgencyId = @agencyid {0}",insurance);
            var list = new List<ManagedClaimLean>();
            using (var cmd = new FluentCommand<ManagedClaimLean>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddInt("insuranceId", insuranceId)
                .SetMap(reader => new ManagedClaimLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    LastName = reader.GetStringNullable("LastName"),
                    IsuranceIdNumber = reader.GetStringNullable("IsuranceIdNumber"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                    Status = reader.GetInt("Status"),
                    EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                    EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                    ClaimDate = reader.GetDateTime("ClaimDate").ToShortDateString().ToZeroFilled(),
                    ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                    IsInfoVerified = reader.GetBoolean("IsInfoVerified"),
                    IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                    IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                    IsHMO = reader.GetInt("PayorType") == 2
                })
                .AsList();
            }
            return list;
        }

        public IList<ManagedBill> GetManagedClaims(Guid agencyId,  Guid branchId, int insuranceId, int status)
        {
            var script = string.Format(@"SELECT managedclaims.Id as Id, managedclaims.PatientId as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, patients.PatientIdNumber as PatientIdNumber , managedclaims.EpisodeStartDate as EpisodeStartDate, managedclaims.EpisodeEndDate as EpisodeEndDate,  managedclaims.IsInfoVerified as IsInfoVerified, managedclaims.IsSupplyVerified as IsSupplyVerified, managedclaims.IsVisitVerified as IsVisitVerified " +
               "FROM managedclaims INNER JOIN patients ON managedclaims.PatientId = patients.Id  Where (patients.Status = 1 OR patients.Status = 2) AND managedclaims.PrimaryInsuranceId = @insuranceId AND managedclaims.AgencyId = @agencyid AND  managedclaims.Status = @status {0} ", branchId.IsEmpty() ? string.Empty : "AND patients.AgencyLocationId = @branchId");
            var list = new List<ManagedBill>();
            using (var cmd = new FluentCommand<ManagedBill>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("insuranceId", insuranceId)
                .AddInt("status", status)
                .SetMap(reader => new ManagedBill
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    LastName = reader.GetStringNullable("LastName"),
                    EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                    EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                    IsInfoVerified = reader.GetBoolean("IsInfoVerified"),
                    IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                    IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                })
                .AsList();
            }
            return list;
        }

        public List<ManagedBill> GetManagedClaimByIds(Guid agencyId, Guid branchId, int insuranceId, List<Guid> claimIds)
        {
            var list = new List<ManagedBill>();
            if (claimIds != null && claimIds.Count > 0)
            {
                var ids=claimIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT managedclaims.Id as Id, managedclaims.PatientId as PatientId, patients.FirstName as FirstName, patients.LastName as LastName, patients.PatientIdNumber as PatientIdNumber , managedclaims.EpisodeStartDate as EpisodeStartDate, managedclaims.EpisodeEndDate as EpisodeEndDate " +
                "FROM managedclaims INNER JOIN patients ON managedclaims.PatientId = patients.Id  Where (patients.Status = 1 OR patients.Status = 2) AND managedclaims.PrimaryInsuranceId = @insuranceId AND managedclaims.AgencyId = @agencyid {0} AND  managedclaims.Id IN ( {1} ) ", branchId.IsEmpty() ? string.Empty : "AND patients.AgencyLocationId = @branchId", ids);
                using (var cmd = new FluentCommand<ManagedBill>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("branchId", branchId)
                    .AddInt("insuranceId", insuranceId)
                    .SetMap(reader => new ManagedBill
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                        EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                    })
                    .AsList();
                }
            }
            return list;
        }

        public List<ManagedClaim> GetManagedClaimsToGenerateByIds(Guid agencyId, List<Guid> managedClaimIds)
        {
            var list = new List<ManagedClaim>();
            if (managedClaimIds != null && managedClaimIds.Count > 0)
            {
                var ids = managedClaimIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT managedclaims.Id as Id , managedclaims.AgencyId as AgencyId , managedclaims.PatientId as PatientId , managedclaims.EpisodeId as EpisodeId , managedclaims.PatientIdNumber as PatientIdNumber, managedclaims.EpisodeStartDate as EpisodeStartDate, managedclaims.EpisodeEndDate as EpisodeEndDate ,managedclaims.IsOasisComplete as IsOasisComplete, managedclaims.IsFirstBillableVisit as IsFirstBillableVisit , managedclaims.FirstBillableVisitDate as FirstBillableVisitDate ,  managedclaims.Remark as Remark , managedclaims.IsuranceIdNumber as IsuranceIdNumber , " +
                    " managedclaims.FirstName as FirstName , managedclaims.LastName as LastName ,  managedclaims.DOB as DOB , managedclaims.Gender as Gender , managedclaims.PaymentDate as PaymentDate , " +
                    " managedclaims.AddressLine1 as AddressLine1 , managedclaims.AddressLine2 as AddressLine2 , managedclaims.AddressCity as AddressCity , managedclaims.AddressStateCode as AddressStateCode , managedclaims.AddressZipCode as AddressZipCode , " +
                    " managedclaims.StartofCareDate as StartofCareDate , managedclaims.PhysicianNPI as PhysicianNPI , managedclaims.PhysicianFirstName as PhysicianFirstName , managedclaims.PhysicianLastName as PhysicianLastName ," +
                    " managedclaims.DiagnosisCode as DiagnosisCode , managedclaims.HippsCode as HippsCode , managedclaims.ClaimKey as ClaimKey , managedclaims.AreOrdersComplete as AreOrdersComplete ,  managedclaims.AreVisitsComplete as AreVisitsComplete ," +
                    " managedclaims.IsGenerated as IsGenerated , managedclaims.Modified as Modified , managedclaims.Created as Created , " +
                    " managedclaims.VerifiedVisits as VerifiedVisits , managedclaims.Supply as Supply , managedclaims.SupplyTotal as SupplyTotal , managedclaims.Payment as Payment ,  " +
                    " managedclaims.PrimaryInsuranceId as PrimaryInsuranceId , managedclaims.IsSupplyVerified as IsSupplyVerified , managedclaims.IsInfoVerified as IsInfoVerified , managedclaims.IsVisitVerified as IsVisitVerified , managedclaims.IsRapGenerated as IsRapGenerated , " +
                    " managedclaims.Status as Status , managedclaims.Type as Type , managedclaims.DischargeDate as DischargeDate , managedclaims.AdmissionSource as AdmissionSource , " +
                    " managedclaims.PatientStatus as PatientStatus , managedclaims.ClaimDate as ClaimDate ,  managedclaims.ProspectivePay as ProspectivePay , " +
                    " managedclaims.AssessmentType as AssessmentType , managedclaims.Comment as Comment  , managedclaims.HealthPlanId as HealthPlanId ,  managedclaims.AuthorizationNumber as AuthorizationNumber , managedclaims.ConditionCodes as ConditionCodes , managedclaims.UB4PatientStatus as UB4PatientStatus  " +
                    "FROM managedclaims " +
                    "WHERE managedclaims.AgencyId = @agencyid " +
                    "AND managedclaims.Id IN ( {0} )  ", ids);

                using (var cmd = new FluentCommand<ManagedClaim>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new ManagedClaim
                     {
                         Id = reader.GetGuid("Id"),
                         AgencyId = reader.GetGuid("AgencyId"),
                         PatientId = reader.GetGuid("PatientId"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                         EpisodeEndDate = reader.GetDateTime("EpisodeEndDate"),
                         EpisodeStartDate = reader.GetDateTime("EpisodeStartDate"),
                         IsOasisComplete = reader.GetBoolean("IsOasisComplete"),
                         IsFirstBillableVisit = reader.GetBoolean("IsFirstBillableVisit"),
                         FirstBillableVisitDate = reader.GetDateTime("FirstBillableVisitDate"),
                         Remark = reader.GetStringNullable("Remark"),
                         IsuranceIdNumber = reader.GetStringNullable("IsuranceIdNumber"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         DOB = reader.GetDateTime("DOB"),
                         Gender = reader.GetStringNullable("Gender"),
                         PaymentDate = reader.GetDateTime("PaymentDate"),
                         AddressLine1 = reader.GetStringNullable("AddressLine1"),
                         AddressLine2 = reader.GetStringNullable("AddressLine2"),
                         AddressCity = reader.GetStringNullable("AddressCity"),
                         AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                         AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                         StartofCareDate = reader.GetDateTime("StartofCareDate"),
                         PhysicianNPI = reader.GetStringNullable("PhysicianNPI"),
                         PhysicianFirstName = reader.GetStringNullable("PhysicianFirstName"),
                         PhysicianLastName = reader.GetStringNullable("PhysicianLastName"),
                         DiagnosisCode = reader.GetStringNullable("DiagnosisCode"),
                         HippsCode = reader.GetStringNullable("HippsCode"),
                         ClaimKey = reader.GetStringNullable("ClaimKey"),
                         AreOrdersComplete = reader.GetBoolean("AreOrdersComplete"),
                         AreVisitsComplete = reader.GetBoolean("AreVisitsComplete"),
                         IsGenerated = reader.GetBoolean("IsGenerated"),
                         Modified = reader.GetDateTime("Modified"),
                         Created = reader.GetDateTime("Created"),
                         VerifiedVisits = reader.GetStringNullable("VerifiedVisits"),
                         Supply = reader.GetStringNullable("Supply"),
                         SupplyTotal = (double)reader.GetDecimalNullable("SupplyTotal"),
                         Payment = (double)reader.GetDecimalNullable("Payment"),
                         PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId"),
                         IsSupplyVerified = reader.GetBoolean("IsSupplyVerified"),
                         IsInfoVerified = reader.GetBoolean("IsInfoVerified"),
                         IsVisitVerified = reader.GetBoolean("IsVisitVerified"),
                         IsRapGenerated = reader.GetBoolean("IsRapGenerated"),
                         Status = reader.GetInt("Status"),
                         Type = reader.GetStringNullable("Type"),
                         AdmissionSource = reader.GetStringNullable("AdmissionSource"),
                         PatientStatus = reader.GetInt("PatientStatus"),
                         ClaimDate = reader.GetDateTime("ClaimDate"),
                         ProspectivePay = (double)reader.GetDecimalNullable("ProspectivePay"),
                         AssessmentType = reader.GetStringNullable("AssessmentType"),
                         Comment = reader.GetStringNullable("Comment"),
                         DischargeDate = reader.GetDateTime("DischargeDate"),
                         HealthPlanId = reader.GetStringNullable("HealthPlanId"),
                         AuthorizationNumber = reader.GetStringNullable("AuthorizationNumber"),
                         ConditionCodes = reader.GetStringNullable("ConditionCodes"),
                         UB4PatientStatus = reader.GetStringNullable("UB4PatientStatus")
                     })
                     .AsList();
                }
            }
            return list;
        }

        public ManagedClaim GetManagedClaim(Guid agencyId, Guid Id)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(Id, "Id");
            return database.Single<ManagedClaim>(r => (r.AgencyId == agencyId && r.Id == Id ));
        }

        public ManagedClaim GetManagedClaim(Guid agencyId, Guid patientId, Guid Id)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(Id, "Id");
            return database.Single<ManagedClaim>(r => (r.AgencyId == agencyId && r.Id == Id && r.PatientId == patientId));
        }

        public bool ManagedVerifyInfo(ManagedClaim claim, Guid agencyId)
        {
            Check.Argument.IsNotNull(claim, "claim");
            var currentClaim = database.Single<ManagedClaim>(r => (r.AgencyId == agencyId && r.Id == claim.Id));
            if (currentClaim != null && claim != null)
            {
                try
                {
                    currentClaim.Type = claim.Type;
                    currentClaim.FirstName = claim.FirstName;
                    currentClaim.LastName = claim.LastName;
                    currentClaim.IsuranceIdNumber = claim.IsuranceIdNumber;
                    currentClaim.PatientIdNumber = claim.PatientIdNumber;
                    currentClaim.Gender = claim.Gender;
                    currentClaim.DOB = claim.DOB;
                    currentClaim.EpisodeStartDate = claim.EpisodeStartDate;
                    currentClaim.EpisodeEndDate = claim.EpisodeEndDate;
                    currentClaim.StartofCareDate = claim.StartofCareDate;
                    currentClaim.AddressLine1 = claim.AddressLine1;
                    currentClaim.AddressLine2 = claim.AddressLine2;
                    currentClaim.AddressCity = claim.AddressCity;
                    currentClaim.AddressStateCode = claim.AddressStateCode;
                    currentClaim.AddressZipCode = claim.AddressZipCode;
                    currentClaim.HippsCode = claim.HippsCode;
                    currentClaim.ClaimKey = claim.ClaimKey;
                    currentClaim.FirstBillableVisitDate = claim.FirstBillableVisitDate;
                    currentClaim.PhysicianLastName = claim.PhysicianLastName;
                    currentClaim.PhysicianFirstName = claim.PhysicianFirstName;
                    currentClaim.PhysicianNPI = claim.PhysicianNPI;
                    currentClaim.DiagnosisCode = string.Format("<DiagonasisCodes><code1>{0}</code1><code2>{1}</code2><code3>{2}</code3><code4>{3}</code4><code5>{4}</code5><code6>{5}</code6></DiagonasisCodes>", claim.Primary, claim.Second, claim.Third, claim.Fourth, claim.Fifth, claim.Sixth);
                    currentClaim.ConditionCodes = string.Format("<ConditionCodes><ConditionCode18>{0}</ConditionCode18><ConditionCode19>{1}</ConditionCode19><ConditionCode20>{2}</ConditionCode20><ConditionCode21>{3}</ConditionCode21><ConditionCode22>{4}</ConditionCode22><ConditionCode23>{5}</ConditionCode23><ConditionCode24>{6}</ConditionCode24><ConditionCode25>{7}</ConditionCode25><ConditionCode26>{8}</ConditionCode26><ConditionCode27>{9}</ConditionCode27><ConditionCode28>{10}</ConditionCode28></ConditionCodes>", claim.ConditionCode18, claim.ConditionCode19, claim.ConditionCode20, claim.ConditionCode21, claim.ConditionCode22, claim.ConditionCode23, claim.ConditionCode24, claim.ConditionCode25, claim.ConditionCode26, claim.ConditionCode27, claim.ConditionCode28);
                    currentClaim.Remark = claim.Remark;
                    currentClaim.IsInfoVerified = true;
                    currentClaim.ProspectivePay = claim.ProspectivePay;
                    currentClaim.AdmissionSource = claim.AdmissionSource;
                    currentClaim.PatientStatus = claim.PatientStatus;
                    currentClaim.UB4PatientStatus = claim.UB4PatientStatus;
                    if (claim.IsManagedClamDischage())
                    {
                        currentClaim.DischargeDate = claim.DischargeDate;
                    }
                    currentClaim.PrimaryInsuranceId = claim.PrimaryInsuranceId;
                    currentClaim.HealthPlanId = claim.HealthPlanId;
                    currentClaim.AuthorizationNumber = claim.AuthorizationNumber;
                    currentClaim.Modified = DateTime.Now;
                    database.Update<ManagedClaim>(currentClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateManagedClaim(ManagedClaim managedClaim)
        {
            Check.Argument.IsNotNull(managedClaim, "managedClaim");
            var currentManagedClaim = database.Single<ManagedClaim>(r => (r.AgencyId == managedClaim.AgencyId && r.PatientId == managedClaim.PatientId && r.Id == managedClaim.Id));

            if (currentManagedClaim != null && managedClaim != null)
            {
                try
                {
                    currentManagedClaim.PatientIdNumber = managedClaim.PatientIdNumber;
                    currentManagedClaim.EpisodeStartDate = managedClaim.EpisodeStartDate;
                    currentManagedClaim.EpisodeEndDate = managedClaim.EpisodeEndDate;
                    currentManagedClaim.IsOasisComplete = managedClaim.IsOasisComplete;
                    currentManagedClaim.IsFirstBillableVisit = managedClaim.IsFirstBillableVisit;
                    currentManagedClaim.FirstBillableVisitDate = managedClaim.FirstBillableVisitDate;
                    currentManagedClaim.Remark = managedClaim.Remark;
                    currentManagedClaim.IsuranceIdNumber = managedClaim.IsuranceIdNumber;
                    currentManagedClaim.FirstName = managedClaim.FirstName;
                    currentManagedClaim.LastName = managedClaim.LastName;
                    currentManagedClaim.DOB = managedClaim.DOB;
                    currentManagedClaim.Gender = managedClaim.Gender;
                    currentManagedClaim.AddressLine1 = managedClaim.AddressLine1;
                    currentManagedClaim.AddressLine2 = managedClaim.AddressLine2;
                    currentManagedClaim.AddressCity = managedClaim.AddressCity;
                    currentManagedClaim.AddressStateCode = managedClaim.AddressStateCode;
                    currentManagedClaim.AddressZipCode = managedClaim.AddressZipCode;
                    currentManagedClaim.StartofCareDate = managedClaim.StartofCareDate;
                    currentManagedClaim.PhysicianNPI = managedClaim.PhysicianNPI;
                    currentManagedClaim.PhysicianFirstName = managedClaim.PhysicianFirstName;
                    currentManagedClaim.PhysicianLastName = managedClaim.PhysicianLastName;
                    currentManagedClaim.DiagnosisCode = managedClaim.DiagnosisCode;
                    currentManagedClaim.HippsCode = managedClaim.HippsCode;
                    currentManagedClaim.ClaimKey = managedClaim.ClaimKey;
                    currentManagedClaim.AreOrdersComplete = managedClaim.AreOrdersComplete;
                    currentManagedClaim.AreVisitsComplete = managedClaim.AreVisitsComplete;
                    currentManagedClaim.Created = managedClaim.Created;
                    currentManagedClaim.VerifiedVisits = managedClaim.VerifiedVisits;
                    currentManagedClaim.PrimaryInsuranceId = managedClaim.PrimaryInsuranceId;
                    currentManagedClaim.Supply = managedClaim.Supply;
                    currentManagedClaim.SupplyTotal = managedClaim.SupplyTotal;
                    currentManagedClaim.IsSupplyVerified = managedClaim.IsSupplyVerified;
                    currentManagedClaim.IsInfoVerified = managedClaim.IsInfoVerified;
                    currentManagedClaim.IsVisitVerified = managedClaim.IsVisitVerified;
                    currentManagedClaim.AgencyId = managedClaim.AgencyId;
                    currentManagedClaim.IsRapGenerated = managedClaim.IsRapGenerated;
                    currentManagedClaim.Status = managedClaim.Status;
                    currentManagedClaim.IsGenerated = managedClaim.IsGenerated;
                    currentManagedClaim.ClaimDate = managedClaim.ClaimDate;
                    currentManagedClaim.Comment = managedClaim.Comment;
                    currentManagedClaim.Payment = managedClaim.Payment;
                    currentManagedClaim.PaymentDate = managedClaim.PaymentDate;
                    currentManagedClaim.Modified = DateTime.Now;
                    database.Update<ManagedClaim>(currentManagedClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateManagedClaimForVisitVerify(ManagedClaim managedClaim)
        {
            Check.Argument.IsNotNull(managedClaim, "managedClaim");
            var currentManagedClaim = database.Single<ManagedClaim>(r => (r.AgencyId == managedClaim.AgencyId && r.PatientId == managedClaim.PatientId && r.Id == managedClaim.Id));
            if (currentManagedClaim != null && managedClaim != null)
            {
                try
                {
                    currentManagedClaim.VerifiedVisits = managedClaim.VerifiedVisits;
                    currentManagedClaim.IsVisitVerified = managedClaim.IsVisitVerified;
                    currentManagedClaim.Modified = managedClaim.Modified;
                    database.Update<ManagedClaim>(currentManagedClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateManagedClaimForSupplyVerify(ManagedClaim managedClaim)
        {
            Check.Argument.IsNotNull(managedClaim, "managedClaim");
            var currentManagedClaim = database.Single<ManagedClaim>(r => (r.AgencyId == managedClaim.AgencyId && r.PatientId == managedClaim.PatientId && r.Id == managedClaim.Id));

            if (currentManagedClaim != null && managedClaim != null)
            {
                try
                {
                    currentManagedClaim.Supply = managedClaim.Supply;
                    currentManagedClaim.SupplyTotal = managedClaim.SupplyTotal;
                    currentManagedClaim.SupplyCode = managedClaim.SupplyCode;
                    currentManagedClaim.IsSupplyVerified = managedClaim.IsSupplyVerified;
                    currentManagedClaim.Modified = managedClaim.Modified;
                    database.Update<ManagedClaim>(currentManagedClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool DeleteManagedClaim(Guid agencyId, Guid patientId, Guid Id)
        {
            var final = database.Single<ManagedClaim>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == Id);
            try
            {
                if (final != null)
                {
                    database.Delete<ManagedClaim>(final.Id);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IList<RemittanceLean> GetRemittances(Guid agencyId, DateTime startDate, DateTime endDate)
        {
            var insurance = string.Empty;
            var script = string.Format(@"SELECT remittances.Id as Id, remittances.RemitId as RemitId ,remittances.TotalClaims as TotalClaims ,remittances.ChargedAmount as ChargedAmount,remittances.CoveredAmount as CoveredAmount,remittances.PaymentAmount as PaymentAmount ,remittances.RemittanceDate as RemittanceDate " +
                "FROM remittances " +
                "WHERE remittances.AgencyId = @agencyid AND DATE(remittances.RemittanceDate) between DATE(@startdate) and DATE(@enddate) AND remittances.IsDeprecated = 0 ");
            var list = new List<RemittanceLean>();
            using (var cmd = new FluentCommand<RemittanceLean>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new RemittanceLean
                 {
                     Id = reader.GetGuid("Id"),
                     RemitId = reader.GetString("RemitId"),
                     TotalClaims = reader.GetInt("TotalClaims"),
                     ChargedAmount = (double)reader.GetDecimalNullable("ChargedAmount"),
                     CoveredAmount = (double)reader.GetDecimalNullable("CoveredAmount"),
                     PaymentAmount = (double)reader.GetDecimalNullable("PaymentAmount"),
                     RemittanceDate = reader.GetDateTimeNullable("RemittanceDate") != null ? reader.GetDateTime("RemittanceDate") : DateTime.MinValue
                 })
                 .AsList();
            }
            return list;
        }

        public Remittance GetRemittance(Guid agencyId, Guid Id)
        {
            return database.Single<Remittance>(r => r.AgencyId == agencyId && r.Id == Id);
        }

        public bool DeleteRemittance(Guid agencyId, Guid Id)
        {
            var result = false;
            var remittance = database.Single<Remittance>(r => r.AgencyId == agencyId && r.Id == Id);
            if (remittance != null)
            {
                remittance.IsDeprecated = true;
                database.Update<Remittance>(remittance);
                result = true;
            }
            return result;
        }

        public List<ClaimLean> GetFinalClaims(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT patientepisodes.Id as EpisodeId, patientepisodes.StartDate, patientepisodes.EndDate, " +
              "patients.FirstName, patients.LastName, patients.PatientIdNumber, " +
              "finals.ProspectivePay, finals.Payment,finals.PaymentDate, finals.ClaimDate, finals.Status " +
              "FROM finals INNER JOIN patients ON finals.PatientId = patients.Id INNER JOIN patientepisodes ON finals.EpisodeId = patientepisodes.Id " +
              "WHERE finals.AgencyId = @agencyid {0}  AND  (patients.Status = 1 OR patients.Status = 2)  AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
              "AND DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) AND finals.Status=@status ORDER BY patientepisodes.StartDate ASC", !branchId.IsEmpty() ? "AND patients.AgencyLocationId=@branchId" : string.Empty);
            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("status", status)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     PaymentDate = reader.GetDateTime("PaymentDate"),
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Type = "Final"
                 })
                 .AsList();
            }
            return list;
        }

        public List<ClaimLean> GetFinalClaimsBySubmissionDate(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT patientepisodes.Id as EpisodeId, patientepisodes.StartDate, patientepisodes.EndDate, " +
              "patients.FirstName, patients.LastName, patients.PatientIdNumber, " +
              "finals.ProspectivePay, finals.Payment,finals.PaymentDate, finals.ClaimDate, finals.Status " +
              "FROM finals INNER JOIN patients ON finals.PatientId = patients.Id INNER JOIN patientepisodes ON finals.EpisodeId = patientepisodes.Id " +
              "WHERE finals.AgencyId = @agencyid {0}  AND  (patients.Status = 1 OR patients.Status = 2)  AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
              "AND DATE(finals.ClaimDate) between DATE(@startdate) and DATE(@enddate)  AND finals.Status IN ( 305 , 315 , 320 , 330 ) ORDER BY patientepisodes.StartDate ASC", !branchId.IsEmpty() ? "AND patients.AgencyLocationId=@branchId" : string.Empty);
            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     PaymentDate =reader.GetInt("Status")==330? reader.GetDateTime("PaymentDate"):DateTime.MinValue,
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Type = "Final"
                 })
                 .AsList();
            }
            return list;
        }

        public List<ClaimLean> GetRapClaims(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT patientepisodes.Id as EpisodeId, patientepisodes.StartDate, patientepisodes.EndDate, " +
             "patients.FirstName, patients.LastName, patients.PatientIdNumber," +
             "raps.ProspectivePay, raps.Payment, raps.PaymentDate , raps.ClaimDate, raps.Status " +
             "FROM raps INNER JOIN patients ON raps.PatientId = patients.Id INNER JOIN patientepisodes ON raps.Id = patientepisodes.Id " +
             "WHERE raps.AgencyId = @agencyid {0}  AND (patients.Status = 1 OR patients.Status = 2) AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
             "AND DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) AND raps.Status=@status ORDER BY patientepisodes.StartDate ASC", !branchId.IsEmpty() ? "AND patients.AgencyLocationId=@branchId" : string.Empty);
            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("status", status)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     PaymentDate = reader.GetDateTime("PaymentDate"),
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Type = "RAP"
                 })
                 .AsList();
            }
            return list;
        }

        public List<ClaimLean> GetRapClaimsBySubmissionDate(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT patientepisodes.Id as EpisodeId, patientepisodes.StartDate as StartDate, patientepisodes.EndDate as EndDate, " +
             "patients.FirstName as FirstName, patients.LastName as LastName, patients.PatientIdNumber as PatientIdNumber," +
             "raps.ProspectivePay as ProspectivePay, raps.Payment as Payment, raps.PaymentDate as PaymentDate , raps.ClaimDate as ClaimDate, raps.Status as Status " +
             "FROM raps INNER JOIN patients ON raps.PatientId = patients.Id INNER JOIN patientepisodes ON raps.Id = patientepisodes.Id " +
             "WHERE raps.AgencyId = @agencyid {0}  AND (patients.Status = 1 OR patients.Status = 2) AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 " +
             "AND DATE(raps.ClaimDate) between DATE(@startdate) and DATE(@enddate) AND raps.Status IN ( 305 , 315 , 320 , 330 ) ORDER BY patientepisodes.StartDate ASC", !branchId.IsEmpty() ? "AND patients.AgencyLocationId=@branchId" : string.Empty);
            
            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     ClaimAmount = (double)reader.GetDecimalNullable("ProspectivePay"),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     PaymentDate = reader.GetInt("Status")== 330? reader.GetDateTime("PaymentDate"):DateTime.MinValue,
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Type = "RAP"
                 })
                 .AsList();
            }
            return list;
        }

        public List<ClaimLean> GetAccountsReceivableRaps(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var insuranceScript = string.Empty;
            if (insurance <= 0)
            {
            }
            else if (insurance < 1000)
            {
                insuranceScript = " AND patients.PrimaryInsurance < 1000";
            }
            else
            {
                insuranceScript = " AND patients.PrimaryInsurance = @insuranceId";
            }
            var script = string.Format(@"SELECT patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName, patients.PatientIdNumber, patients.AgencyLocationId, raps.AssessmentType, raps.HippsCode, raps.AddressZipCode,  raps.Id, raps.Status, raps.ClaimDate, raps.Payment  " +
                "FROM raps INNER JOIN patients ON patients.AgencyId = @agencyid AND raps.PatientId = patients.Id  " +
                "INNER JOIN patientepisodes ON patientepisodes.AgencyId = @agencyid AND raps.EpisodeId = patientepisodes.Id  " +
                "WHERE raps.AgencyId = @agencyid {0} {1} AND ( patients.Status = 1 OR patients.Status = 2 ) AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND raps.Status IN (305, 315, 320)" +
                "AND ( DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) )",
                !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId" : string.Empty, insuranceScript);

            var list = new List<ClaimLean>();
            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("insuranceId", insurance)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Type = "RAP",
                     AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                     AssessmentType = reader.GetStringNullable("AssessmentType"),
                     HippsCode = reader.GetStringNullable("HippsCode").ToUpperCase(),
                     AddressZipCode = reader.GetStringNullable("AddressZipCode").ToUpperCase()
                 }).AsList();
            }
            return list;
        }

        public List<ClaimLean> GetAccountsReceivableFinals(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var insuranceScript = string.Empty;
            if (insurance <= 0)
            {
            }
            else if (insurance < 1000)
            {
                insuranceScript = " AND patients.PrimaryInsurance < 1000";
            }
            else
            {
                insuranceScript = " AND patients.PrimaryInsurance = @insuranceId";
            }
            var script = string.Format(@"SELECT patientepisodes.StartDate, patientepisodes.EndDate, patients.FirstName, patients.LastName, patients.PatientIdNumber, patients.AgencyLocationId, finals.AssessmentType, finals.HippsCode, finals.AddressZipCode, finals.Id, finals.Status, finals.ClaimDate, finals.Payment " +
                "FROM finals INNER JOIN patients ON patients.AgencyId = @agencyid AND finals.PatientId = patients.Id " +
                "INNER JOIN patientepisodes ON patientepisodes.AgencyId = @agencyid AND finals.EpisodeId = patientepisodes.Id " +
                "WHERE finals.AgencyId = @agencyid {0} {1} AND ( patients.Status = 1 OR patients.Status = 2 ) AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND finals.Status IN (305, 315, 320)" +
                "AND ( DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) )",
                !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId" : string.Empty, insuranceScript);
            var list = new List<ClaimLean>();

            using (var cmd = new FluentCommand<ClaimLean>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchId", branchId)
                 .AddInt("insuranceId", insurance)
                 .AddDateTime("startDate", startDate)
                 .AddDateTime("endDate", endDate)
                 .SetMap(reader => new ClaimLean
                 {
                     PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                     Status = reader.GetInt("Status"),
                     EpisodeEndDate = reader.GetDateTime("EndDate"),
                     EpisodeStartDate = reader.GetDateTime("StartDate"),
                     LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                     FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                     PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                     ClaimDate = reader.GetDateTime("ClaimDate"),
                     Type = "Final",
                     AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                     AssessmentType = reader.GetStringNullable("AssessmentType"),
                     HippsCode = reader.GetStringNullable("HippsCode").ToUpperCase(),
                     AddressZipCode = reader.GetStringNullable("AddressZipCode").ToUpperCase()
                 }).AsList();
            }
            return list;
        }

        public List<PendingClaimLean> PendingClaimRaps(Guid agencyId, Guid branchId, int insurance)
        {
            var insuranceScript = "AND patients.PrimaryInsurance = @insuranceId";
            var script = string.Format(@"SELECT patientepisodes.StartDate, patientepisodes.EndDate, " +
                "patients.FirstName, patients.LastName, patients.PatientIdNumber,  patients.AddressZipCode as PatientZipCode, patients.AgencyLocationId, raps.AssessmentType, raps.MedicareNumber, raps.HippsCode, raps.AddressZipCode as RapZipCode,  " +
                "raps.Id as Id, raps.Status as Status, raps.PaymentDate, raps.Payment, raps.EpisodeStartDate, raps.EpisodeEndDate " +
                "FROM raps INNER JOIN patients ON patients.AgencyId = @agencyid AND raps.PatientId = patients.Id " +
                "INNER JOIN patientepisodes ON patientepisodes.AgencyId = @agencyid AND raps.EpisodeId = patientepisodes.Id " +
                "WHERE raps.AgencyId = @agencyid {0} {1} AND (patients.Status = 1 OR patients.Status = 2) AND patients.IsDeprecated = 0 " +
                "AND patientepisodes.IsActive = 1 AND raps.Status IN (305, 315, 320) ORDER BY  patients.LastName ASC ,  patients.FirstName ASC ", !branchId.IsEmpty() ? "AND patients.AgencyLocationId=@branchId" : string.Empty, insuranceScript);
            var list = new List<PendingClaimLean>();
            using (var cmd = new FluentCommand<PendingClaimLean>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                  .AddGuid("agencyid", agencyId)
                  .AddGuid("branchId", branchId)
                  .AddInt("insuranceId", insurance)
                  .SetMap(reader => new PendingClaimLean
                  {
                      Id = reader.GetGuid("Id"),
                      PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                      FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                      LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                      MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                      Status = reader.GetInt("Status"),
                      EpisodeEndDate = reader.GetDateTime("EndDate").IsValid() ? reader.GetDateTime("EndDate") : reader.GetDateTime("EpisodeEndDate"),
                      EpisodeStartDate = reader.GetDateTime("StartDate").IsValid() ? reader.GetDateTime("StartDate") : reader.GetDateTime("EpisodeStartDate"),
                      PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                      PaymentDate = reader.GetDateTime("PaymentDate"),
                      Type = "RAP",
                      AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                      AssessmentType = reader.GetStringNullable("AssessmentType"),
                      HippsCode = reader.GetStringNullable("HippsCode").ToUpperCase(),
                      AddressZipCode = reader.GetStringNullable("RapZipCode").IsNotNullOrEmpty() ? reader.GetStringNullable("RapZipCode") : reader.GetStringNullable("PatientZipCode").ToUpperCase()
                  })
                  .AsList();
            }
            return list;
        }

        public List<PendingClaimLean> PendingClaimFinals(Guid agencyId, Guid branchId, int insurance)
        {
            var insuranceScript = "AND patients.PrimaryInsurance = @insuranceId";
            var script = string.Format(@"SELECT patientepisodes.StartDate, patientepisodes.EndDate, " +
                "patients.FirstName, patients.LastName, patients.PatientIdNumber,  patients.AddressZipCode as PatientZipCode, patients.AgencyLocationId, finals.AssessmentType, finals.MedicareNumber, finals.HippsCode, finals.AddressZipCode as FianlZipCode,  " +
                "finals.Id as Id, finals.Status as Status, finals.PaymentDate, finals.Payment, finals.EpisodeStartDate, finals.EpisodeEndDate " +
                "FROM finals INNER JOIN patients ON patients.AgencyId = @agencyid AND finals.PatientId = patients.Id " +
                "INNER JOIN patientepisodes ON patientepisodes.AgencyId = @agencyid AND finals.EpisodeId = patientepisodes.Id " +
                "WHERE finals.AgencyId = @agencyid {0} {1} AND (patients.Status = 1 OR patients.Status = 2) AND patients.IsDeprecated = 0 " +
                "AND patientepisodes.IsActive = 1 AND finals.Status IN (305, 315, 320) ORDER BY  patients.LastName ASC ,  patients.FirstName ASC", !branchId.IsEmpty() ? "AND patients.AgencyLocationId=@branchId" : string.Empty, insuranceScript);
            var list = new List<PendingClaimLean>();
            using (var cmd = new FluentCommand<PendingClaimLean>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("insuranceId", insurance)
                .SetMap(reader => new PendingClaimLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                    Status = reader.GetInt("Status"),
                    EpisodeEndDate = reader.GetDateTime("EndDate").IsValid() ? reader.GetDateTime("EndDate") : reader.GetDateTime("EpisodeEndDate"),
                    EpisodeStartDate = reader.GetDateTime("StartDate").IsValid() ? reader.GetDateTime("StartDate") : reader.GetDateTime("EpisodeStartDate"),
                    PaymentAmount = (double)reader.GetDecimalNullable("Payment"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    Type = "Final",
                    AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                    AssessmentType = reader.GetStringNullable("AssessmentType"),
                    HippsCode = reader.GetStringNullable("HippsCode").ToUpperCase(),
                    AddressZipCode = reader.GetStringNullable("FianlZipCode").IsNotNullOrEmpty() ? reader.GetStringNullable("FianlZipCode") : reader.GetStringNullable("PatientZipCode").ToUpperCase()
                })
                .AsList();
            }
            return list;
        }

        public bool AddRemitQueue(RemitQueue remitQueue)
        {
            Check.Argument.IsNotNull(remitQueue, "remitQueue");
            bool result = false;
            try
            {
                remitQueue.Created = DateTime.Now;
                remitQueue.Modified = DateTime.Now;
                database.Add<RemitQueue>(remitQueue);
                result = true;
            }
            catch (Exception e)
            {
                return false;
            }
            return result;
        }

        public List<ClaimDataLean> ClaimDatas(Guid agencyId, DateTime startDate, DateTime endDate, string claimType)
        {
            var script = string.Format("Select Id , ClaimType , BillIdentifers , Created , Modified FROM claimdatas WHERE  AgencyId=@agencyId AND claimdatas.Created >= @startdate AND claimdatas.Created <= @enddate {0} ", claimType.ToUpperCase() == "ALL" ? string.Empty : " AND claimdatas.ClaimType = @claimtype ");
            var list = new List<ClaimDataLean>();

            using (var cmd = new FluentCommand<ClaimDataLean>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                  .AddGuid("agencyid", agencyId)
                  .AddDateTime("startdate", startDate.AddDays(-1))
                  .AddDateTime("enddate", endDate.Date.AddDays(1))
                  .AddString("claimtype", claimType.ToUpperCase())
                  .SetMap(reader => new ClaimDataLean
                  {
                      Id = reader.GetInt("Id"),
                      ClaimType = reader.GetStringNullable("ClaimType"),
                      Claims = reader.GetStringNullable("BillIdentifers").IsNotNullOrEmpty() ? reader.GetStringNullable("BillIdentifers").ToObject<List<ClaimInfo>>() : new List<ClaimInfo>(),
                      Created = reader.GetDateTime("Created"),
                  })
                  .AsList();
            }
            return list;
        }

        public List<ClaimInfoDetail> GetManagedClaimInfoDetails(Guid agencyId, List<Guid> claimIds)
        {
            var list = new List<ClaimInfoDetail>();
            if (claimIds != null && claimIds.Count > 0)
            {
                var ids = claimIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT patients.FirstName as FirstName, patients.LastName as LastName, patients.PatientIdNumber as PatientIdNumber, " +
                    "managedclaims.EpisodeStartDate as EpisodeStartDate, managedclaims.EpisodeEndDate as EpisodeEndDate, managedclaims.ProspectivePay as ProspectivePay " +
                    "FROM managedclaims INNER JOIN patients ON managedclaims.PatientId = patients.Id  " +
                    "Where managedclaims.AgencyId = @agencyid AND  managedclaims.Id IN ({0}) ", ids);
                using (var cmd = new FluentCommand<ClaimInfoDetail>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new ClaimInfoDetail
                    {
                        BillType = "MANAGED",
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        StartDate = reader.GetDateTime("EpisodeStartDate"),
                        EndDate = reader.GetDateTime("EpisodeEndDate"),
                        ProspectivePay = reader.GetDouble("ProspectivePay"),
                        MedicareNumber = string.Empty
                    })
                    .AsList();
                }
            }
            return list;
        }

        public List<ClaimInfoDetail> GetMedicareClaimInfoDetails(Guid agencyId, List<Guid> claimIds, string type)
        {
            var list = new List<ClaimInfoDetail>();
            if (type.IsNotNullOrEmpty() && (type.ToUpperCase() == "RAP" || type.ToUpperCase() == "FINAL"))
            {
                if (claimIds != null && claimIds.Count > 0)
                {
                    var script = string.Empty;
                    var ids = claimIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                    var table = type.ToUpperCase() == "RAP" ? "raps" : "finals";
                    script = string.Format(@"SELECT patientepisodes.StartDate as EpisodeStartDate, patientepisodes.EndDate as EpisodeEndDate, " +
                        "{0}.FirstName as FirstName, {0}.LastName as LastName, {0}.PatientIdNumber as PatientIdNumber, " +
                        "{0}.MedicareNumber as MedicareNumber, {0}.ProspectivePay as ProspectivePay " +
                        "FROM {0} INNER JOIN patientepisodes ON {0}.EpisodeId = patientepisodes.Id " +
                        "WHERE {0}.AgencyId = @agencyid " +
                        "AND {0}.Id IN ({1}) ", table, ids);

                    using (var cmd = new FluentCommand<ClaimInfoDetail>(script))
                    {
                        list = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                        .SetMap(reader => new ClaimInfoDetail
                        {
                            BillType = type.ToUpperCase(),
                            PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                            FirstName = reader.GetStringNullable("FirstName"),
                            LastName = reader.GetStringNullable("LastName"),
                            StartDate = reader.GetDateTime("EpisodeStartDate"),
                            EndDate = reader.GetDateTime("EpisodeEndDate"),
                            MedicareNumber = reader.GetStringNullable("MedicareNumber"),
                            ProspectivePay = reader.GetDouble("ProspectivePay")
                        }).AsList();
                    }
                }
            }
            return list;
        }

        public List<ClaimEpisode> GetEpisodeNeedsClaim(Guid agencyId, Guid patientId, string type)
        {
            var list = new List<ClaimEpisode>();
            if (type.IsNotNullOrEmpty() && (type.ToUpperCase() == "RAP" || type.ToUpperCase() == "FINAL"))
            {
               
                    var script = string.Empty;
                    var table = type.ToUpperCase() == "RAP" ? "raps" : "finals";
                    script = string.Format(@"SELECT patientepisodes.Id as Id, patientepisodes.StartDate as EpisodeStartDate, patientepisodes.EndDate as EpisodeEndDate  " +
                                 "FROM  patientepisodes  " +
                                 "WHERE patientepisodes.AgencyId = @agencyid AND patientepisodes.PatientId = @patientId AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND patientepisodes.Id NOT IN ( SELECT {0}.Id FROM {0} WHERE {0}.AgencyId = @agencyid AND {0}.PatientId = @patientId )", table);

                    using (var cmd = new FluentCommand<ClaimEpisode>(script))
                    {
                        list = cmd.SetConnection("AgencyManagementConnectionString")
                        .AddGuid("agencyid", agencyId)
                         .AddGuid("patientId", patientId)
                        .SetMap(reader => new ClaimEpisode
                        {
                            Id = reader.GetGuid("Id"),
                            StartDate = reader.GetDateTime("EpisodeStartDate"),
                            EndDate = reader.GetDateTime("EpisodeEndDate"),
                            Type = type.ToUpperCase()
                        }).AsList();
                    }
            }
            return list;
        }

        #endregion
    }
}
