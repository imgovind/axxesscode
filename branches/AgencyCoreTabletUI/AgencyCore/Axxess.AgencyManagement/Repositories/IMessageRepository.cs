﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;

    using Domain;

    public interface IMessageRepository
    {
        bool Delete(Guid id, Guid agencyId);
        bool Add(Message message);
        Message GetMessage(Guid id, Guid agencyId, bool markAsRead);
        int MessageCount(Guid userId);

        IList<Message> GetSentMessages(Guid userId, Guid agencyId);
        IList<Message> GetUserMessages(Guid userId, Guid agencyId);
        List<Message> GetCurrentMessages(Guid userId, Guid agencyId);

        List<SystemMessage> GetSystemMessages();
        bool AddSystemMessage(SystemMessage message);
        SystemMessage GetSystemMessage(Guid messageId);

        DashboardMessage GetCurrentDashboardMessage();
        bool AddDashboardMessage(DashboardMessage message);
    }
}
