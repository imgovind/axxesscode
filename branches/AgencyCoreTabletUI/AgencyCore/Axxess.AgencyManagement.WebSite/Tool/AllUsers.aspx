﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>
<script runat="server">
    public void RefreshBtn_Click(Object sender, EventArgs e)
    {
        UserEngine.Refresh();
    }
</script>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess All Users Page</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
    <% if (!Current.IsIpAddressRestricted) { %>
        <% foreach(Axxess.Api.Contracts.UserData user in UserEngine.Cache) { %>
            User Id: <%= user.Id %><br />
            User Name: <%= user.Name %><br />
            User Agency: <%= AgencyEngine.Get(user.AgencyId).Name %><br />
            <br />
        <% } %>
        <asp:Button ID="refreshButton" runat="server" OnClick="RefreshBtn_Click" Text="Refresh" />
    <% } %>
</asp:Content>