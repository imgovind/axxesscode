(function($) {
    $.extend($.fn, {
        PhoneAutoTab: function() {
            return this.each(function() {
                var $npa = $(this),
                    $nxx = $(this).closest(".row").find(".phone-short:eq(1)"),
                    $num = $(this).closest(".row").find(".phone-long");
                $npa.autotab({ target: $nxx, format: "numeric" });
                $nxx.autotab({ target: $num, previous: $npa, format: "numeric" });
                $num.autotab({ previous: $nxx, format: "numeric" });
            })
        }
    })
})(jQuery);