var AcoreGrids = { };
(function($) {
    $.extend($.fn, {
        AcoreGrid: function(Options, Arguments) {
            return this.each(function() {
                // If initializing new grid
                if (typeof Options == "object" && !$(this).hasClass("acore-grid")) {
                    // Set variables in grid registry
                    AcoreGrids[Options.Id] = {
                        Url: Options.Url,
                        PostVars: Options.PostVars,
                        Layout: Options.Layout,
                        Data: Options.Data
                    };
                    // Create pointer to grid element
                    var Grid = $(this);
                    // Initialize grid layout
                    Grid.attr("id", Options.Id).addClass("acore-grid").append(
                        $("<ul/>").append(
                            $("<li/>"))).append(
                        $("<ol/>", { "class": "loading" }));
                    if (Options.Title != undefined) $("ul", Grid).prepend(
                        $("<li/>", { "class": "ac" }).append(
                            $("<h3/>", { "text": Options.Title })));
                    for (var i = 0; i < Options.Layout.length; i++) {
                        $("ul li:last", Grid).append(
                            $("<span/>", { "text": Options.Layout[i].Title, "style": "width:" + Options.Layout[i].Width }).attr("data", (Options.Layout[i].Data == undefined ? "" : Options.Layout[i].Data)).addClass(Options.Layout[i].CssClass));
                        if (Options.Layout[i].Sortable != false) $("ul li:last span:last", Grid).addClass("sortable").click(function() {
                            $(this).closest(".acore-grid").AcoreGrid("Sort", $(this))
                        })
                    }
                    // If AJAX enabled, make the request for data
                    if (Options.Url) U.PostUrl(Options.Url, Options.PostVars, function(Data) {
                        // Set data in registry
                        AcoreGrids[Options.Id].Data = Data.data;
                        // Populate data in the grid
                        Grid.AcoreGrid("Populate");
                    });
                    // If data is passed as an argument, populate data in the grid
                    else if (Options.Data) Grid.AcoreGrid("Populate");
                    // Function for grouping by similar columns
                } else if (typeof Options == "string" && Options == "Group") {
/* -- Incomplete, will finish later --
                    var Grid = $(this),
                        ColumnNum = Arguments,
                        Key = $("ul li:last > span:eq(" + ColumnNum + ")").attr("data"),
                        Header = $("ul").clone();
                    // Function for populating data in the grid
                    AcoreGrids[Grid.attr("id")].Data.sort(function(a, b) { return a[Key] > b[Key] ? 1 : -1 });
                    var Match = "", Group = [];
                    for (var i = 0; i < AcoreGrids[Grid.attr("id")].Data.length; i++) {
                        var Data = AcoreGrids[Grid.attr("id")].Data[i];
                        if (Data[Key] == Match) Group.push(Data);
                        else if (i == 0) {
                            if ($("ul h3", Grid).length) $("ul h3", Grid).append(" &8214; " + Key + ": <span class='group-key'>" + Data[Key] + "</span>");
                            else $("ul", Grid).prepend
                            Match = Data[Key];
                            Group.push(Data);
                        } else {

                        }
                    }
-- */
                } else if (typeof Options == "string" && Options == "Populate") {
                    // Set variables
                    var Grid = $(this),
                        Data = AcoreGrids[$(this).attr("id")].Data,
                        Layout = AcoreGrids[$(this).attr("id")].Layout,
                        ListNum = Arguments == undefined ? 0 : Arguments;
                    // Clear previous data and set chart to loading
                    $("ol:eq(" + ListNum + ")", Grid).empty().addClass("loading");
                    // Cycle through all data entry sets/rows
                    for (var i = 0; i < Data.length; i++) {
                        $("ol:eq(" + ListNum + ")", Grid).append($("<li/>").addClass(i % 2 ? "even" : "odd"));
                        var Row = Data[i];
                        // Cycle through each variable/cells
                        for (var j = 0; j < Layout.length; j++) {
                            if (Layout[j].Data != undefined && Layout[j].Data.length) var Html = Row[Layout[j].Data];
                            else if (Layout[j].Format != undefined && Layout[j].Format.length) var Html = Layout[j].Format.replace(/<#=\s*([^#\s]*)\s*#>/g, function(m, n) { return typeof Row[n] != "undefined" ? Row[n] : m });
                            else var Html = "";
                            $("ol:eq(" + ListNum + ") li:last", Grid).append(
                                $("<span/>", { "html": Html, "style": "width:" + Layout[j].Width }).addClass(Layout[j].CssClass)
                            );
                        }
                    }
                    // Set hover class toggling
                    $("ol:eq(" + ListNum + ") li", Grid).mouseover(function() { $(this).addClass("hover") }).mouseout(function() { $(this).removeClass("hover") });
                    // Remove loading glyph
                    $("ol:eq(" + ListNum + ")", Grid).removeClass("loading")
                    // Function to rebind data from the server
                } else if (typeof Options == "string" && Options == "Rebind") {
                    // If URL is in the regisrty
                    if (AcoreGrids[$(this).attr("id")].Url) {
                        // Clear previous data and set chart to loading and make AJAX request for data
                        $("ol", Grid).empty().addClass("loading");
                        U.PostUrl(AcoreGrids[$(this).attr("id")].Url, typeof Arguments == "object" ? Arguments : AcoreGrids[$(this).attr("id")].PostVars, function(Data) {
                            // Update data in the registry
                            AcoreGrids[Options.Id].Data = Data;
                            // Populate data in the grid
                            Grid.AcoreGrid("Populate", { Data: Data, Layout: Options.Layout })
                        })
                    }
                    // Function to remove grid from registry and UI
                } else if (typeof Options == "string" && Options == "Remove") {
                    AcoreGrids[$(this).attr("id")] = null;
                    $(this).remove()
                    // Function called when a sort click is made
                } else if (typeof Options == "string" && Options == "Sort") {
                    // Set variables
                    var Grid = $(this),
                        Column = Arguments,
                        Key = Column.attr("data"),
                        Ascending = true;
                    // Clear previous data and set chart to loading
                    $("ol", Grid).empty().addClass("loading");
                    // Cycle through columns to set arrow icons properly
                    $("ul li:last > span", Grid).each(function() {
                        if ($(this).attr("data") == Key && $(this).hasClass("sort")) {
                            $(this).toggleClass("descending");
                            if ($(this).hasClass("descending")) $(this).find(".arrow").addClass("ui-icon-circle-triangle-n").removeClass("ui-icon-circle-triangle-s");
                            else $(this).find(".arrow").addClass("ui-icon-circle-triangle-s").removeClass("ui-icon-circle-triangle-n");
                        } else if ($(this).attr("data") == Key) $(this).addClass("sort").append($("<span/>").addClass("fr arrow ui-icon ui-icon-circle-triangle-s"));
                        else $(this).removeClass("sort").find(".arrow").remove();
                    });
                    // Change bool for ascending if applicable
                    if (Column.hasClass("descending")) Ascending = false;
                    // Sort JSON data in the registry
                    AcoreGrids[Grid.attr("id")].Data.sort(function(a, b) { return (a[Key] > b[Key] ? 1 : -1) * (Ascending ? 1 : -1) });
                    // Repopulate data
                    Grid.AcoreGrid("Populate");
                }
            })
        }
    })
})(jQuery);