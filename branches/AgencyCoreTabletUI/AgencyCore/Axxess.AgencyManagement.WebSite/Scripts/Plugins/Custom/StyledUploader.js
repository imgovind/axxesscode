(function($) {
    $.extend($.fn, {
        StyledUploader: function() {
            return this.each(function() {
                var FileInput = $(this);
                $(this).addClass("abs transparent").after(
                    $("<div/>", { "class": "styled-uploader-input" }).append(
                        $("<input/>", { "type": "text", "class": "fill" })).after(
                        $("<div/>").addClass("styled-uploader-button fr").Buttons([{ Text: "Browse", Click: function() { FileInput.click(); return false } }])));
                FileInput.siblings(".styled-uploader-input").find("input").focus(function() { FileInput.click() });
                FileInput.change(function() { FileInput.next().find("input").val(FileInput.val()) });
                FileInput.siblings(".styled-uploader-input").find("input").keyup(function() { if ($(this).val() != $(this).parent().prev().val()) $(this).val($(this).parent().prev().val()) });
            })
        }
    })
})(jQuery);