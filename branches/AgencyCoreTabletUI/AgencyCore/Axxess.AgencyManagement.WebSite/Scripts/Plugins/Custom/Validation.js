$.validator.messages.required = "* Required";
$.validator.addMethod("notzero", function(value, element) { return value.trim().length && !/^0$/.test(value) && !/^0{8}-0{4}-0{4}-0{4}-0{12}$/.test(value) }, $.validator.messages.required);
$.validator.addMethod("phone-short", function(value, element) { return !value.length || !/^\d{3}$/.test(value) }, "");
$.validator.addMethod("phone-long", function(value, element) { return !value.length || !/^\d{4}$/.test(value) }, "");
(function($) {
    $.extend($.fn, {
        Validate: function(Options) {
            return this.each(function() {
                if (Options == undefined) Options = new Object();
                var form = this,
                    AfterSubmit = function(Result) {
                        var Message = Result.errorMessage;
                        if (Result.isSuccessful) {
                            if (!Message) Message = "Your information was successfully saved.";
                            if ($(form).attr("success") != undefined || Options.Success != undefined) try {
                                if ($(form).attr("success") != undefined) eval($(form).attr("success"));
                                if (typeof Options.Success == "function") Options.Success(Result);
                            } catch (E) {
                                U.Growl(U.MessageErrorJS(true), "error");
                                console.log("Error in #" + $(form).attr("id") + " success function: " + E.toString());
                            }
                            U.Growl(Message, "success");
                            $(".t-grid").data('tGrid').rebind();
                        } else {
                            if (!Message) Message = "There was an error while trying to save your information.";
                            if ($(form).attr("fail") != undefined || Options.Success != undefined) try {
                                if ($(form).attr("fail") != undefined) eval($(form).attr("fail"));
                                if (typeof Options.Fail == "function") Options.Fail(Result);
                            } catch (E) {
                                U.Growl(U.MessageErrorJS(true), "error");
                                console.log("Error in #" + $(form).attr("id") + " fail function: " + E.toString());
                            }
                            U.Growl(Message, "error");
                            $(".t-grid").data('tGrid').rebind();
                        }
                    };
                $(form).validate(
                    $.extend({
                        invalidHandler: function() {
                            $(".required", form).each(function() { $(this).focus() });
                            $(".error:eq(0)", form).focus();
                        },
                        submitHandler: function() {
                            if ($(form).attr("beforesubmit")) eval($(form).attr("beforesubmit"));
                            if (typeof Options.BeforeSubmit == "function") Options.BeforeSubmit();
                            $(form).ajaxSubmit({
                                dataType: "json",
                                success: AfterSubmit,
                                error: AfterSubmit
                            })
                            return false
                        }
                    }, Options)
                )
            })
        }
    })
})(jQuery);