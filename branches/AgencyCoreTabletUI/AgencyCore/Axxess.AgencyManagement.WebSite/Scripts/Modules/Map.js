﻿var GoogleMap = {
    dirDisplay: '',
    dirService: '',
    end: '',
    firstload: true,
    obj: '',
    start: '',
    init: function(start, end) {
        this.start = start, this.end = end;
        $("#map-start-addressess").hide();
        $("#map-start-address-select").change(function() {
            if ($(this).val() == "specify") {
                GoogleMap.updateHidden("start");
                $("#map-start-addressess").show();
                $("#directions").css("top", "22.5em");
            } else {
                $("input[name=start]").val($("#map-start-address-select").val())
                $("#map-start-addressess").hide();
                $("#directions").css("top", "17.5em");
            }
        });
        $(".end input").keyup(function() { GoogleMap.updateHidden("end"); });
        $(".end select").change(function() { GoogleMap.updateHidden("end"); });
        $("#map-start-addressess input").keyup(function() { GoogleMap.updateHidden("start"); });
        $("#map-start-addressess select").change(function() { GoogleMap.updateHidden("start"); });
        $("#map-recalculate").click(function() {
            GoogleMap.loadDirections($("input[name=start]").val(), $("input[name=end]").val());
            return false
        });
        if (this.firstload) this.loadScript();
        else this.renderMap();
    },
    renderMap: function() {
        this.dirService = new google.maps.DirectionsService();
        this.dirDisplay = new google.maps.DirectionsRenderer();
        this.obj = new google.maps.Map(document.getElementById("map"), {
            zoom: 12,
            center: new google.maps.LatLng(32.909169, -96.728363),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        this.dirDisplay.setMap(this.obj);
        this.dirDisplay.setPanel(document.getElementById("directions"));
        this.loadDirections(this.start, this.end);
    },
    loadScript: function() {
        $('body').append(unescape("%3Cscript type=%22text/javascript%22 src=%22http://maps.google.com/maps/api/js?sensor=false&callback=GoogleMap.rende" +
            "rMap%22%3E%3C/script%3E"));
        this.firstload = false;
    },
    loadDirections: function(start, end) {
        this.dirService.route({
            origin: start,
            destination: end,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        }, function(result, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                GoogleMap.dirDisplay.setDirections(result);
            }
        });
    },
    updateHidden: function(type) {
        $("input[name=" + type + "]").val(
            $("#Map_" + type + "addr").val() + ", " +
            $("#Map_" + type + "city").val() + ", " +
            $("#Map_" + type + "state").val() + " " +
            $("#Map_" + type + "zip").val());
    }
}