﻿var Contact = {
    Delete: function(Id) { U.DeleteTemplate("Contact", Id); },
    InitEdit: function() {
        Forms.ShowIfSelectEquals($("#Edit_Contact_Type"), "Other", $("#Edit_Contact_OtherContactType"));
        U.InitEditTemplate("Contact");
    },
    InitNew: function() {
    Forms.ShowIfSelectEquals($("#New_Contact_Type"), "Other", $("#New_Contact_OtherContactType"));
        U.InitNewTemplate("Contact");
    },
    RebindList: function() { U.RebindGrid($('#List_Contact')); }
}