var Patient = {
    // Variables
    _patientId: "",
    _patientName: "",
    _EpisodeId: "",
    _patientRowIndex: 0,
    // Initialization functions
    InitNew: function(responseText, textStatus, XMLHttpRequest, Element) {
        Forms.ShowIfChecked($("#New_Patient_DMEOther"), $("#New_Patient_OtherDME"));
        Forms.ShowIfChecked($("#New_Patient_PaymentSource"), $("#New_Patient_OtherPaymentSource"));
        Patient.InitInsuranceSelect(Element);
    },
    InitEdit: function(responseText, textStatus, XMLHttpRequest, Element) {
        Forms.ShowIfChecked($("#Edit_Patient_DMEOther"), $("#New_Patient_OtherDME"));
        Forms.ShowIfChecked($("#New_Patient_PaymentSource"), $("#New_Patient_OtherPaymentSource"));
        Patient.InitInsuranceSelect(Element);
        Patient.InitAddPhysicianForm(Element);
    },
    InitList: function(responseText, textStatus, XMLHttpRequest, Element) {
        $(".t-grid-toolbar", Element).empty().append(
            $("<div/>").GridSearch()
        )
    },
    InitInsuranceSelect: function(Element) {
        $(".insurances", Element).each(function() {
            if ($(":selected", this).attr("ishmo") == "1") $(this).closest(".row").next(".row").show().find("input").addClass("required");
            else $(this).closest(".row").next(".row").hide().find("input").removeClass("required");
        });
        $(".insurances", Element).change(function() {
            if ($(":selected", this).attr("ishmo") == "1") $(this).closest(".row").next(".row").show().find("input").addClass("required");
            else {
                $(this).closest(".row").next(".row").hide().find("input").removeClass("required");
                if ($(":selected", this).val() == "new") {
                    Acore.Open('newinsurance');
                    this.selectedIndex = 0;
                } else if ($(":selected", this).val() == "spacer") this.selectedIndex = 0;
            }
        })
    },
    InitAddPhysicianForm: function(Element) {
        $(".add-physician", Element).click(function() {
            var PhysicianId = $(this).closest(".row").find("input[type=hidden]").val();
            if (U.IsGuid(PhysicianId)) Patient.AddPhysician(PhysicianId, $(".PatientId", Element).val());
            else U.Growl("Please Select a Physician", "error");
            $(this).closest(".row").find(".physician").AjaxAutocomplete("reset");
        })
    },
    InitCenter: function() {
        $("#txtSearch_Patient_Selection").keyup(function() {
            if ($(this).val()) {
                Patient.Filter($(this).val())
                if ($("tr.match:even", "#PatientSelectionGrid .t-grid-content").length) $("tr.match:first", "#PatientSelectionGrid .t-grid-content").click();
                else $("#PatientMainResult").html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Ch1%3ENo Patients Found to Meet Your Search Requirements%3C/h1%3E%3C" +
                    "div class=%22buttons%22%3E%3Cul%3E%3Cli%3E%3Ca title=%22Add New Patient%22 onclick=%22javascript:Acore.Open('newpatient');%22 href=%22javasc" +
                    "ript:void(0);%22%3EAdd New Patient%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E%3C/div%3E"));
            } else {
                $("tr", "#PatientSelectionGrid .t-grid-content").removeClass("match t-alt").show();
                $("tr:even", "#PatientSelectionGrid .t-grid-content").addClass("t-alt");
                $("tr:first", "#PatientSelectionGrid .t-grid-content").click();
            }
        });
        $("#window_patientcenter .top select").change(function() { Patient._patientId = ""; U.FilterResults("Patient"); });
        $("#patientActivityDropDown").change(function() {
            var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
            PatientActivityGrid.rebind({ patientId: Patient._patientId, discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val() });
        });
        $('#window_patientcenter .layout').layout();
    },
    InitPatientReadmitted: function() { U.InitTemplate($("#readmitForm"), function() { UserInterface.CloseModal(); Patient.Rebind(); }, "Patient Re-admitted successfully."); },
    InitChangePatientStatus: function() { U.InitTemplate($("#changePatientStatusForm"), function() { UserInterface.CloseModal(); Patient.Rebind(); }, "Patient status updated successfully."); },
    InitDischargePatient: function() { U.InitTemplate($("#dischargePatientForm"), function() { UserInterface.CloseModal(); Patient._patientId = ""; U.FilterResults("Patient"); }, "Patient is discharged successfully."); },
    InitActivatePatient: function() { U.InitTemplate($("#activatePatientForm"), function() { UserInterface.CloseModal(); Patient._patientId = ""; U.FilterResults("Patient"); }, "Patient is activated successfully."); },
    InitNewPhoto: function() {
        $("#changePatientPhotoForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("Patient photo updated successfully.", "success");
                            Patient.Rebind();
                            UserInterface.CloseModal();
                        }
                        else {
                            alert(result.responseText);
                        }
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("Patient photo updated successfully.", "success");
                            Patient.Rebind();
                            UserInterface.CloseModal();
                        }
                        else {
                            alert(result.responseText);
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNewOrder: function() {
        $("#newOrderForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            Agency.RebindOrders();
                            UserInterface.CloseWindow('neworder');
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNewFaceToFaceEncounter: function() {
        $("#newFaceToFaceEncounterForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            Agency.RebindOrders();
                            UserInterface.CloseWindow('newfacetofaceencounter');
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNewCommunicationNote: function() {
        $("#newCommunicationNoteForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("New Communication Note saved successfully.", "success");
                            UserInterface.CloseWindow('newcommnote');
                            Patient.Rebind();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNewAuthorization: function() {
        $("#newAuthorizationForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseWindow('newauthorization');
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditAuthorization: function() {
        $("#editAuthorizationForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseWindow('editauthorization');
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditOrder: function() {
    },
    InitAdmit: function(patientId) {
        $("#newAdmitPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Patient admission successful.", "success");
                            UserInterface.CloseWindow('admitpatient');
                            UserInterface.CloseModal();
                            Patient.Rebind();
                            Schedule.Rebind();
                            U.RebindGrid($('#List_Patient_NonAdmit_Grid'));
                            U.RebindGrid($('#List_PatientPending_Grid'));
                            UserInterface.ShowPatientPrompt();
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    InitNonAdmit: function() {
        $("#NonAdmit_Patient_Date").datepicker("hide");
        $("#newNonAdmitPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Patient non-admission successful.", "success");
                            UserInterface.CloseWindow('nonadmitpatient');
                            UserInterface.CloseModal();
                            U.RebindGrid($('#List_PatientPending_Grid'));
                            Patient.Rebind();
                            Schedule.Rebind();
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    InitMedicationProfileSnapshot: function() {
        $("#newMedicationProfileSnapShotForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseWindow('medicationprofilesnapshot');
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    // Form submission handlers
    NewSuccess: function() {
        if ($("#New_Patient_Status").val() == "1") {
            Patient.Rebind();
            UserInterface.ShowPatientPrompt();
        }
    },
    // Grid Rebinds
    Rebind: function() {
        U.RebindGrid($('#PatientSelectionGrid'));
        U.RebindGrid($('#List_Patient_Grid'));
    },
    RebindList: function() {
        U.RebindGrid($('#List_Patient_Grid'));
        U.RebindGrid($('#List_PatientPending_Grid'));
        U.RebindGrid($('#List_Referral'));
    },
    RebindVitalSigns: function() {
        var grid = $('#List_PatientVitalSigns').data('tGrid');
        if (grid != null) {
            grid.rebind({ patientId: $("#Patient_VitalSigns_PatientId").val(), StartDate: $("#Patient_VitalSigns_StartDate").val(), EndDate: $("#Patient_VitalSigns_EndDate").val() });
        }
        var $exportLink = $('#Patient_VitalSigns_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/patientId=([^&]*)/, 'patientId=' + $("#Patient_VitalSigns_PatientId").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#Patient_VitalSigns_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#Patient_VitalSigns_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindOrdersHistory: function() {
        var grid = $('#List_PatientOrdersHistory').data('tGrid');
        if (grid != null) { grid.rebind({ patientId: $("#PatientOrdersHistory_PatientId").val(), StartDate: $("#PatientOrdersHistory_StartDate-input").val(), EndDate: $("#PatientOrdersHistory_EndDate-input").val() }); }
        var $exportLink = $('#PatientOrdersHistory_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/patientId=([^&]*)/, 'patientId=' + $("#PatientOrdersHistory_PatientId").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#PatientOrdersHistory_StartDate-input").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#PatientOrdersHistory_EndDate-input").val());
        $exportLink.attr('href', href);
    },
    // Grid events
    activityRowDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
        if (dataItem.IsComplete) { $(e.row).addClass('darkgreen'); }
        if (dataItem.IsOrphaned) {
            $(e.row).addClass('black').tooltip({
                track: true,
                showURL: false,
                top: 5,
                left: 5,
                extraClass: "calday error",
                bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
            });
            $(e.row.cells[1]).addClass('darkred');
        }
        $(e.row).bind("contextmenu", function(Event) {
            var Menu = $("<ul/>");
            if (dataItem.IsComplete) Menu.append($("<li/>", { "text": "Reopen Task" }).click(function() {
                $(e.row).find("a:contains('Reopen Task')").click();
            }));
            else if (!dataItem.IsOrphaned) Menu.append($("<li/>", { "text": "Edit Note" }).click(function() {
                $(e.row).find("a:first").click();
            }));
            Menu.append($("<li/>", { "text": "Details" }).click(function() {
                $(e.row).find("a:contains('Details')").click();
            })).append($("<li/>", { "text": "Delete" }).click(function() {
                $(e.row).find("a:contains('Delete')").click();
            })).append($("<li/>", { "text": "Print" }).click(function() {
                $(e.row).find(".print").parent().click();
            }));
            Menu.ContextMenu(Event);
        });
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
            $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            var patientId = e.row.cells[2].innerHTML;
            Patient.loadInfoAndActivity(patientId);
        }
    },
    onMedicationProfileEdit: function(e) {
        if (e.dataItem != undefined) {
            $(e.form).find('.MedicationType').data('tDropDownList').select(function(dataItem) {
                return dataItem.Value == e.dataItem['MedicationType'].Value;
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest('td').prevAll().length == 7) {
                    if (e.dataItem['MedicationCategory'] != 'DC') {
                        $(this).html('');
                    }
                }
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest('td').prevAll().length == 6) {
                    if (e.dataItem['Classification'] == null || e.dataItem['Classification'] == '') {
                        $(this).find("input[name=MedicationClassification]").val('');
                    }
                }
            });
        }
    },
    onPlanofCareMedicationEdit: function(e) {
        if (e.dataItem != undefined) {
            $(e.form).find('.MedicationType').data('tDropDownList').select(function(dataItem) {
                return dataItem.Value == e.dataItem['MedicationType'].Value;
            });
        }
    },
    onMedicationProfileRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem.MedicationCategory == "DC") {
            $(e.row).removeClass('t-alt').addClass('red');
            e.row.cells[7].innerHTML = dataItem.DCDateFormated;
        } else e.row.cells[7].innerHTML = '<a class="t-grid-action t-button t-state-default" onclick="' + dataItem.DischargeUrl + 'return false">D/C</a>';
        if (dataItem.DrugId != null && dataItem.DrugId.length > 0) {
            var medline = 'http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.69&mainSearchCriteria.v.c=' + dataItem.DrugId;
            e.row.cells[2].innerHTML = '<a href="' + medline + '" target="_blank">' + dataItem.MedicationDosage + '</a>';
        }
        e.row.cells[1].innerHTML = dataItem.StartDateSortable;
    },
    onMedicationProfileSnapShotRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem != undefined) {
            e.row.cells[1].innerHTML = dataItem.StartDateSortable;
        }
    },
    onMedProfileSnapShotHistoryRowDataBound: function(e) {
        var dataItem = e.dataItem;
        e.row.cells[1].innerHTML = dataItem.SignedDateFormatted;
    },
    OnDataBound: function(e) {
        var id = $("#medicationProfileHistroyId").val();
        var medicatonProfileGridDC = $('#MedicatonProfileGridDC').data('tGrid');
        medicatonProfileGridDC.rebind({ medId: id, medicationCategry: "DC", assessmentType: "" });
    },
    PatientListDataBound: function() {
        if ($("#PatientSelectionGrid .t-grid-content tr").length) {
            if (Patient._patientId == "") $('#PatientSelectionGrid .t-grid-content tr').eq(0).click();
            else $('td:contains(' + Patient._patientId + ')', $('#PatientSelectionGrid')).closest('tr').click();
        } else $("#PatientMainResult").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
        if ($("#PatientSelectionGrid .t-state-selected").length) $("#PatientSelectionGrid .t-grid-content").scrollTop($("#PatientSelectionGrid .t-state-selected").position().top - 50);
        Patient.Filter($("#txtSearch_Patient_Selection").val());
    },
    // AcoreWindow Openers
    loadEditPatient: function(id) {
        Acore.Open("editpatient", 'Patient/EditPatientContent', function() {
            Patient.InitEdit();
            Lookup.loadAdmissionSources();
            Lookup.loadRaces();
            Lookup.loadUsers();
            Lookup.loadReferralSources();
        }, { patientId: id });
    },
    loadEditAuthorization: function(patientId, id) {
        Acore.Open("editauthorization", 'Authorization/Edit', function() {
            Patient.InitEditAuthorization();
        }, { patientId: patientId, Id: id });
    },
    LoadNewOrder: function(patientId) {
        Acore.Open("neworder", 'Order/New', function() {
            Patient.InitNewOrder();
        }, { patientId: patientId });
    },
    LoadNewCommunicationNote: function(patientId) {
        Acore.Open("newcommnote", 'CommunicationNote/New', function() {
            Patient.InitNewCommunicationNote();
        }, { patientId: patientId });
    },
    LoadNewAuthorization: function(patientId) {
        Acore.Open("newauthorization", 'Authorization/New', function() {
            Patient.InitNewAuthorization();
        }, { patientId: patientId });
    },
    LoadNewInfectionReport: function(patientId) {
        Acore.Open("newinfectionreport", 'Infection/New', function() {
            InfectionReport.InitNew();
        }, { patientId: patientId });
    },
    LoadNewIncidentReport: function(patientId) {
        Acore.Open("newincidentreport", 'Incident/New', function() { IncidentReport.InitNew(); }, { patientId: patientId });
    },
    InfoPopup: function(PatientId) {
        Acore.Dialog({
            Name: "Patient Info",
            Url: "Patient/InfoPopup",
            Input: { patientId: PatientId },
            Width: "500px"
        })
    },
    loadMedicationProfileSnapshot: function(patientId) {
        Acore.Open("medicationprofilesnapshot", 'Patient/MedicationProfileSnapShot', function() { Patient.InitMedicationProfileSnapshot(); }, { patientId: patientId });
    },
    loadMedicationProfile: function(patientId) {
        Acore.Open("medicationprofile", 'Patient/MedicationProfile', function() { }, { patientId: patientId });
    },
    loadMedicationProfileSnapshotHistory: function(patientId) {
        Acore.Open("medicationprofilesnapshothistory", 'Patient/MedicationProfileSnapShotHistory', function() { }, { patientId: patientId });
    },
    loadPatientCommunicationNotes: function(patientId) {
        Acore.Open("patientcommunicationnoteslist", 'Patient/PatientCommunicationNotesView', function() { }, { patientId: patientId });
    },
    loadEditCommunicationNote: function(patientId, id) {
        Acore.Open("editcommunicationnote", 'Patient/EditCommunicationNote', function() {
            $("#editCommunicationNoteForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            var resultObject = eval(result);
                            if (resultObject.isSuccessful) {
                                U.Growl(resultObject.errorMessage, "success");
                                UserInterface.CloseWindow('editcommunicationnote');
                                Patient.Rebind();
                                Schedule.Rebind();
                            } else U.Growl(resultObject.errorMessage, "error");
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });

        }, { patientId: patientId, Id: id });
    },
    loadEditEmergencyContact: function(patientId, id) {
        Acore.Open("editemergencycontact", 'Patient/EditEmergencyContactContent', function() {
            $("#editEmergencyContactForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            if (result.isSuccessful) {
                                U.Growl("Edit emergency contact is successful.", "success");
                                var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                                if (emergencyContact != null) {
                                    emergencyContact.rebind({ PatientId: patientId });
                                }
                                UserInterface.CloseWindow('editemergencycontact');
                            } else U.Growl(result.errorMessage, "error");
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });

        }, { patientId: patientId, Id: id });
    },
    loadNewEmergencyContact: function(id) {
        Acore.Open("newemergencycontact", 'Patient/NewEmergencyContactContent', function() {
            $("#newEmergencyContactForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            if (result.isSuccessful) {
                                U.Growl("New emergency contact added successful.", "success");
                                var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                                if (emergencyContact != null) {
                                    emergencyContact.rebind({ PatientId: id });
                                }
                                UserInterface.CloseWindow('newemergencycontact');
                            } else U.Growl(result.errorMessage, "error");
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });
        }, { patientId: id });
    },
    // Delete functions
    RemovePhoto: function(patientId) {
        if (confirm("Are you sure you want to remove this photo?")) {
            U.PostUrl("/Patient/RemovePhoto", "patientId=" + patientId, function(result) {
                if (result.isSuccessful) {
                    U.Growl("Patient photo has been removed successfully.", "success");
                    Patient.Rebind();
                    UserInterface.CloseModal();
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    DeleteOrder: function(id, patientId, episodeId) {
        if (confirm("Are you sure you want to delete this task?")) {
            U.PostUrl("/Patient/DeleteOrder", { id: id, patientId: patientId, episodeId: episodeId }, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    var grid = $('#List_PatientOrdersHistory').data('tGrid');
                    if (grid != null) { grid.rebind({ patientId: patientId }); }

                    var grid = $('#List_EpisodeOrders').data('tGrid');
                    if (grid != null) { grid.rebind({ patientId: patientId, episodeId: episodeId }); }

                    if ($("#Billing_CenterContent").val() != null && $("#Billing_CenterContent").val() != undefined) {
                        Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val());
                    }

                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    removePhysician: function(el) { $(el).closest('.row').addClass('hidden').appendTo($(el).closest('.column')).find('select').val('00000000-0000-0000-0000-000000000000'); },
    DeleteSchedule: function(episodeId, patientId, eventId, employeeId) {
        if (confirm("Are you sure you want to delete this task?")) {
            var input = "patientId=" + patientId + "&eventId=" + eventId + "&employeeId=" + employeeId + "&episodeId=" + episodeId;
            U.PostUrl("/Schedule/Delete", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    Patient.CustomDateRange();
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    if (scheduleActivityGrid != null) {
                        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    }
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                }
            });
        }
    },
    Delete: function(patientId) {
        if (confirm("Are you sure you want to delete this patient?")) {
            var input = "patientId=" + patientId;
            U.PostUrl("/Patient/Delete", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    Patient.Rebind();
                    Schedule.Rebind();
                    U.Growl(resultObject.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    DeleteEmergencyContact: function(id, patientId) {
        if (confirm("Are you sure you want to delete")) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Patient/DeleteEmergencyContact",
                data: "id=" + id + "&patientId=" + patientId,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                        if (emergencyContact != null) {
                            emergencyContact.rebind({ PatientId: patientId });
                        }
                    }
                }
            }
            );
        }
    },
    DeleteCommunicationNote: function(Id, patientId) {
        if (confirm("Are you sure you want to delete this task?")) {
            U.PostUrl("/Patient/DeleteCommunicationNote", { Id: Id, patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    var communicationNoteGrid = $('#List_CommunicationNote').data('tGrid');
                    if (communicationNoteGrid != null) {
                        U.Growl(result.errorMessage, "success");
                        communicationNoteGrid.rebind();
                    }
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },
    DeletePhysician: function(id, patientId) {
        if (confirm("Are you sure you want to delete this physician?")) U.RebindAjaxGrid("/Patient/DeletePhysicianContact", { "id": id, "patientId": patientId }, $('#EditPatient_PhysicianGrid'));
    },
    DeleteAuthorization: function(patientId, id) {
        if (confirm("Are you sure you want to delete this authorization?")) {
            U.PostUrl("/Patient/DeleteAuthorization", { Id: id, patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    var authorizationGrid = $('#List_Authorizations').data('tGrid');
                    if (authorizationGrid != null) {
                        U.Growl(result.errorMessage, "success");
                        authorizationGrid.rebind({ patientId: patientId });
                    }
                }
                else {
                    U.Growl(result.errorMessage, "error");
                }
            });
        }
    },


    
    Filter: function(text) {
        search = text.split(" ");
        $("tr", "#PatientSelectionGrid .t-grid-content").removeClass("match").hide();
        for (var i = 0; i < search.length; i++) {
            $("td", "#PatientSelectionGrid .t-grid-content").each(function() {
                if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
            });
        }
        $("tr.match", "#PatientSelectionGrid .t-grid-content").removeClass("t-alt").show();
        $("tr.match:even", "#PatientSelectionGrid .t-grid-content").addClass("t-alt");
    },
    Verify: function(medicareNumber, lastName, firstName, dob, gender) {
        var input = "medicareNumber=" + medicareNumber + "&lastName=" + lastName + "&firstName=" + firstName + "&dob=" + dob + "&gender=" + gender;
        U.PostUrl("/Patient/Verify", input, function(result) {
            alert(result);
        });
    },
    loadInfoAndActivity: function(patientId) {
        $("#PatientMainResult").Load("Patient/Data", { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            Patient._patientId = patientId;
        })
    },
    SubmitOrder: function(control, page) {
        control.closest("form").validate();
        if (control.closest("form").valid()) {
            var options = {
                dataType: 'json',
                beforeSubmit: function(values, form, options) {
                },
                success: function(result) {
                    if (result.isSuccessful) {
                        if (control.html() == "Save") {
                            U.Growl("The Physician Order has been saved successfully.", "success");
                        }
                        if (control.html() == "Save &amp; Close") {
                            UserInterface.CloseAndRefresh(page);
                            U.Growl("The Physician Order has been saved successfully.", "success");
                        }
                        if (control.html() == "Complete") {
                            UserInterface.CloseAndRefresh(page);
                            U.Growl("The Physician Order has been saved and completed successfully.", "success");
                        }
                    } else U.Growl(result.errorMessage, "error");
                }
            };
            $(control.closest("form")).ajaxSubmit(options);
            return false;
        } else U.ValidationError(control);
    },
    addPhysician: function(el) { $(el).closest('.column').find('.row.hidden').first().removeClass('hidden').find("input:first").blur(); },
    DateRange: function(id) {
        var data = 'dateRangeId=' + id + "&patientId=" + $("#PatientCenter_PatientId").val();
        $.ajax({
            url: '/Patient/DateRange',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                $("#dateRangeText").empty();
                if (result.StartDateFormatted == "01/01/0001" || result.EndDateFormatted == "01/01/0001") {
                    $("#dateRangeText").append("No Episodes Found");
                }
                else {
                    $("#dateRangeText").append(result.StartDateFormatted + "-" + result.EndDateFormatted);
                }
            }
        });
    },
    CustomDateRange: function() {
        var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
        if (PatientActivityGrid != null) {
            PatientActivityGrid.rebind({ patientId: $("#PatientCenter_PatientId").val(), discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val(), rangeStartDate: $("#patientActivityFromDate").val(), rangeEndDate: $("#patientActivityToDate").val() });
        }
    },
    AddPhysician: function(id, patientId) {
        U.RebindAjaxGrid("/Patient/AddPatientPhysicain", { "id": id, "patientId": patientId }, $('#EditPatient_PhysicianGrid'));
        $("#EditPatient_PhysicianSelector").AjaxAutocomplete("reset").next("input[type=hidden]").val("");
    },
    SetPrimaryPhysician: function(id, patientId) {
        if (confirm("Are you sure you want to set this physician as primary?")) U.RebindAjaxGrid("/Physician/SetPrimary", { "id": id, "patientId": patientId }, $('#EditPatient_PhysicianGrid'));
    },
    OnSocChange: function() {
        var date = $("#New_Patient_StartOfCareDate").datepicker("getDate");
        $("#New_Patient_EpisodeStartDate").datepicker("setDate", date)
        $("#New_Patient_EpisodeStartDate").datepicker("option", "mindate", date);
    },
    UpdateOrderStatus: function(eventId, patientId, episodeId, orderType, actionType) {
        var reason = "";
        if (actionType == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() { if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide(); });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl('Patient/UpdateOrderStatus', { eventId: eventId, patientId: patientId, episodeId: episodeId, orderType: orderType, actionType: actionType, reason: reason }, function(data) {
                    if (data.isSuccessful) {
                        UserInterface.CloseModal();
                        U.Growl(data.errorMessage, "success");
                        Patient.Rebind();
                        Schedule.Rebind();
                    } else U.Growl(data.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl('Patient/UpdateOrderStatus', { eventId: eventId, patientId: patientId, episodeId: episodeId, orderType: orderType, actionType: actionType, reason: reason }, function(data) {
                if (data.isSuccessful) {
                    UserInterface.CloseModal();
                    U.Growl(data.errorMessage, "success");
                    Patient.Rebind();
                    Schedule.Rebind();
                } else U.Growl(data.errorMessage, "error");
            });
        }
    },
    ProcessCommunicationNote: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Patient/ProcessCommunicationNotes", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        U.Growl(result.errorMessage, "success");
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Patient/ProcessCommunicationNotes", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    U.Growl(result.errorMessage, "success");
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    loadPatientManagedDates: function(Id) { Acore.Open("patientmanageddates", 'Patient/ManagedDates', function() { }, { patientId: Id }); }
}
