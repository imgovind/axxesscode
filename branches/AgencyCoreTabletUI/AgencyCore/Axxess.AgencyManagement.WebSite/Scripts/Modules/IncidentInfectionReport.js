﻿var IncidentReport = {
    Delete: function(Id) { U.DeleteTemplate("IncidentReport", Id); },
    InitEdit: function() {
        U.InitEditTemplate("IncidentReport", function() {
            Patient.Rebind();
            Schedule.Rebind();
        });
        Forms.ShowIfChecked($("#Edit_Incident_IndividualInvolved4"), $("#Edit_Incident_IndividualInvolvedOther"));
    },
    InitNew: function() {
        U.InitNewTemplate("IncidentReport", function() {
            Patient.Rebind();
            Schedule.Rebind();

        });
        Forms.ShowIfChecked($("#New_Incident_IndividualInvolved4"), $("#New_Incident_IndividualInvolvedOther"));
    },
    RebindList: function() { U.RebindGrid($('#List_IncidentReport')); },
    ProcessIncident: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Agency/ProcessIncident", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Agency/ProcessIncident", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    }

}

var InfectionReport = {
    Delete: function(Id) { U.DeleteTemplate("InfectionReport", Id); },
    InitEdit: function() {
        U.InitEditTemplate("InfectionReport", function() {
            Patient.Rebind();
            Schedule.Rebind();
        });
        Forms.ShowIfChecked($("#Edit_Infection_InfectionType6"), $("#Edit_Infection_InfectionTypeOther"));
    },
    InitNew: function() {
        U.InitNewTemplate("InfectionReport", function() {
            Patient.Rebind();
            Schedule.Rebind();
        });
        Forms.ShowIfChecked($("#New_Infection_InfectionType6"), $("#New_Infection_InfectionTypeOther"));
    },
    RebindList: function() { U.RebindGrid($('#List_InfectionReport')); },
    ProcessInfection: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Agency/ProcessInfection", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Agency/ProcessInfection", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    }
}