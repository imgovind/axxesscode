﻿var User = {
    InitNew: function() {
        if ($("#newUserForm").length) {
            $(".names").alpha({ nocaps: false });
            Forms.ShowIfSelectEquals($("#New_User_TitleType"), "Other", $("#New_User_OtherTitleType"));
            Forms.ShowIfSelectEquals($("#New_User_Credentials"), "Other", $("#New_User_OtherCredentials"));
            $("#New_User_AllPermissions").change(function() {
                if ($("#New_User_AllPermissions").prop("checked")) $("input[name=PermissionsArray]").each(function() {
                    if (!$(this).prop("checked")) $(this).attr("checked", true)
                });
                else $("input[name=PermissionsArray]").each(function() {
                    if ($(this).prop("checked")) $(this).attr("checked", false)
                })
            });
            $("#newUserForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        clearForm: false,
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            var resultObject = eval(result);
                            if (resultObject.isSuccessful) {
                                User.RebindList();
                                U.Growl("New user successfully added.", "success");
                                UserInterface.CloseWindow('newuser');
                            } else U.Growl(resultObject.errorMessage, "error");
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return true;
                }
            })
        }
    },
    InitEdit: function() {
        $('#Edit_User_AllPermissions').change(function() {
            if ($('#Edit_User_AllPermissions').attr('checked')) {
                $('input[name="PermissionsArray"]').each(function() {
                    if (!$(this).attr('checked')) {
                        $(this).attr('checked', true);
                    }
                });
            }
            else {
                $('input[name="PermissionsArray"]').each(function() {
                    if ($(this).attr('checked')) {
                        $(this).attr('checked', false);
                    }
                });
            }
        });

        Forms.ShowIfSelectEquals($("#Edit_User_TitleType"), "Other", $("#Edit_User_OtherTitleType"));
        Forms.ShowIfSelectEquals($("#Edit_User_Credentials"), "Other", $("#Edit_User_OtherCredentials"));

        $("#editUserForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            User.RebindList();
                            U.Growl("User successfully updated.", "success");
                            UserInterface.CloseWindow('edituser');
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        var file_input_index = 0;
        $('#editUserLicenseForm input[type=file]').each(function() {
            file_input_index++;
            $(this).wrap('<div id="Edit_UserLicense_Attachment_Container_' + file_input_index + '"></div>');
        });
        $("#editUserLicenseForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("License added successfully.", "success");
                            var grid = $('#List_User_Licenses').data('tGrid');
                            if (grid != null) {
                                grid.rebind();
                            }
                            $("#editUserLicenseForm").clearForm();
                            $("#Edit_UserLicense_Attachment_Container_1").html($("#Edit_UserLicense_Attachment_Container_1").html());
                        } else U.Growl($.trim(result.responseText), "error");
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("License added successfully.", "success");
                            var grid = $('#List_User_Licenses').data('tGrid');
                            if (grid != null) {
                                grid.rebind();
                            }
                            $("#editUserLicenseForm").clearForm();
                            $("#Edit_UserLicense_Attachment_Container_1").html($("#Edit_UserLicense_Attachment_Container_1").html());
                        } else U.Growl($.trim(result.responseText), "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#editUserPermissionsForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl("User permissions updated successfully.", "success");
                        } else U.Growl($.trim(result.errorMessage), "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitProfile: function() {
        $("#editProfileForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Profile updated successfully.", "success");
                            UserInterface.CloseWindow('editprofile');
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitForgotSignature: function() {
        $("#lnkRequestSignatureReset").click(function() {
            U.PostUrl("/Signature/Email", null, function(result) {
                if (result.isSuccessful) {
                    U.Growl("Your request to reset your signature was successful. Please check your e-mail.", "success");
                    UserInterface.CloseWindow('forgotsignature');
                }
                else {
                    $("#resetSignatureMessage").addClass("errormessage").html(result.errorMessage);
                }
            });
        });
    },
    InitMissedVisit: function() {
        $("#newMissedVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            U.Growl("Missed Visit successfully created.", "success");
                            UserInterface.CloseWindow('newmissedvisit');
                            User.RebindScheduleList();
                            UserInterface.CloseModal();
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    SelectPermissions: function(control, className) {
        if (control.attr('checked')) {
            $(className).each(function() {
                if (!$(this).attr('checked')) {
                    $(this).attr('checked', true);
                }
            });
        }
        else {
            $(className).each(function() {
                if ($(this).attr('checked')) {
                    $(this).attr('checked', false);
                }
            });
        }
    },
    RebindList: function() {
        var activegrid = $('#List_User').data('tGrid');
        if (activegrid != null) {
            activegrid.rebind();
        }

        var inactivegrid = $('#List_UserInactive').data('tGrid');
        if (inactivegrid != null) {
            inactivegrid.rebind();
        }
    },
    RebindScheduleList: function() {
        var grid = $('#List_User_Schedule').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    },
    LoadUserSchedule: function(groupName) {
        $("#myScheduledTasksContentId").empty();
        $("#myScheduledTasksContentId").addClass("loading");
        $("#myScheduledTasksContentId").Load("User/ScheduleGrouped", { groupName: groupName });
    },
    Delete: function(userId) {
        if (confirm("Are you sure you want to delete this user?")) {
            var input = "userId=" + userId;
            U.PostUrl("/User/Delete", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    Activate: function(userId) {
        if (confirm("Are you sure you want to activate this user?")) {
            var input = "userId=" + userId;
            U.PostUrl("/User/Activate", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    Deactivate: function(userId) {
        if (confirm("Are you sure you want to deactivate this user?")) {
            var input = "userId=" + userId;
            U.PostUrl("/User/Deactivate", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    NavigateMonthlyCalendar: function(month, year) {
        $("#window_userschedulemonthlycalendar_content").Load("/User/UserCalendarNavigate", { month: month, year: year })
    }
}