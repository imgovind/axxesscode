﻿var PlanOfCare = {
    InitNew: function() {
    },
    Submit: function(control, page) {
        if (control.closest("form").valid()) {
            var options = {
                dataType: 'json',
                beforeSubmit: function(values, form, options) { },
                success: function(result) {
                    if (result.isSuccessful) {
                        if (control.html() == "Save") U.Growl("The Plan of Care (485) has been saved successfully.", "success");
                        else if (control.html() == "Save &amp; Close") {
                            UserInterface.CloseAndRefresh(page);
                            U.Growl("The Plan of Care (485) has been saved successfully.", "success");
                        } else if (control.html() == "Complete") {
                            UserInterface.CloseAndRefresh(page);
                            U.Growl("The Plan of Care (485) has been saved and completed successfully.", "success");
                        }
                    } else U.Growl(result.errorMessage, "error");
                }
            };
            $(control.closest("form")).ajaxSubmit(options);
            return false;
        } else U.ValidationError(control);
    },
    InitEdit: function() {
        U.InitEditTemplate("485");
    },
    SaveMedications: function(patientId) {
        U.PostUrl("/Patient/LastestMedications", { patientId: patientId }, function(data) {
            if ($("#PlanofCareMedicationIsNew").val() == "true") $("#New_485_Medications").val(data);
            else $("#Edit_485_Medications").val(data);
            UserInterface.CloseModal();
        });
    }
}