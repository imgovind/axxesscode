﻿var Forms = {
    EnableIfChecked: function(Checkbox, Field) { this.IfChecked(true, Checkbox, Field, true) },
    EnableIfRadioEquals: function(Group, Value, Field) { this.IfRadioEquals(true, Group, Value, Field, true) },
    HideIfChecked: function(Checkbox, Field) { this.IfChecked(false, Checkbox, Field) },
    HideIfRadioEquals: function(Group, Value, Field) { this.IfRadioEquals(false, Group, Value, Field) },
    IfChecked: function(Show, Checkbox, Field, Enable) {
        if (Enable == undefined) Enable = false;
        if (Checkbox.prop("checked") == Show) Forms.ToggleOn(Field, Enable);
        else Forms.ToggleOff(Field, Enable);
        Checkbox.change(function() {
            if (Checkbox.prop("checked") == Show) Forms.ToggleOn(Field, Enable);
            else Forms.ToggleOff(Field, Enable);
        })
    },
    IfRadioEquals: function(Show, Group, Value, Field, Enable) {
        Value = Value.split("|");
        if (($.inArray($(":radio[name=" + Group + "]:checked").val(), Value) >= 0) == Show) Forms.ToggleOn(Field, Enable);
        else Forms.ToggleOff(Field, Enable);
        $(":radio[name=" + Group + "]").click(function() {
            if (($.inArray($(":radio[name=" + Group + "]:checked").val(), Value) >= 0) == Show) Forms.ToggleOn(Field, Enable);
            else Forms.ToggleOff(Field, Enable);
        })
    },
    IfSelectEquals: function(Show, Select, Value, Field, Enable) {
        Value = Value.split("|");
        if (($.inArray(Select.val(), Value) >= 0) == Show) Forms.ToggleOn(Field, Enable);
        else Forms.ToggleOff(Field, Enable);
        Select.change(function() {
            if (($.inArray(Select.val(), Value) >= 0) == Show) Forms.ToggleOn(Field, Enable);
            else Forms.ToggleOff(Field, Enable);
        })
    },
    NoneOfTheAbove: function(Checkbox, Group) {
        if (Checkbox.prop("checked")) Group.each(function() {
            if ($(this).prop("id") != Checkbox.prop("id")) $(this).prop("checked", false).change();
        });
        Group.change(function() {
            if ($(this).prop("id") != Checkbox.prop("id") && $(this).prop("checked")) Checkbox.prop("checked", false).change();
        });
        Checkbox.change(function() {
            if ($(this).prop("checked")) Group.each(function() {
                if ($(this).prop("id") != Checkbox.prop("id")) $(this).prop("checked", false).change();
            })
        })
    },
    ShowIfChecked: function(Checkbox, Field) { this.IfChecked(true, Checkbox, Field) },
    ShowIfRadioEquals: function(Group, Value, Field) { this.IfRadioEquals(true, Group, Value, Field) },
    ShowIfSelectEquals: function(Select, Value, Field) { this.IfSelectEquals(true, Select, Value, Field) },
    ToggleOn: function(Field, Enable) {
        if (Enable) Field.removeAttr("disabled").removeClass("form-omitted");
        else Field.show().removeClass("form-omitted");
        if (Field.hasClass("required-disabled")) Field.removeClass("required-disabled").addClass("required");
        if ($(".required-disabled", Field).length) $(".required-disabled", Field).removeClass("required-disabled").addClass("required");
    },
    ToggleOff: function(Field, Enable) {
        if (Enable) Field.attr("disabled", true).addClass("form-omitted");
        else Field.hide().addClass("form-omitted");
        if (Field.hasClass("required")) Field.removeClass("required").addClass("required-disabled");
        if ($(".required", Field).length) $(".required", Field).removeClass("required").addClass("required-disabled");
    }
};