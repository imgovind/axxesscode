﻿var Payroll = {
    InitSearch: function() {
        $("#searchPayrollForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'html',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        $("#payrollSearchResultDetails").hide();
                        $("#payrollSearchResultDetail").hide();
                        $("#payrollMarkAsPaidButton").hide();
                        $("#payrollSearchResult").show();
                        $('#payrollSearchResult').html(result);
                        $('#payroll_print').removeClass('hidden').bind("mouseup", Payroll.Print);
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Print: function() {
        if ($("#payrollSearchResult").is(":visible")) $("a", this).click(function() {
            U.GetAttachment("Payroll/SummaryPdf", { 'payrollStartDate': $("#payrollStartDate").val(), 'payrollEndDate': $("#payrollEndDate").val(), 'payrollStatus': $("#payrollStatus").val() });
        });
        else if ($("#payrollSearchResultDetails ul").length > 1) $("a", this).click(function() {
            U.GetAttachment("Payroll/SummaryDetailsPdf", { 'payrollStartDate': $("#payrollStartDate").val(), 'payrollEndDate': $("#payrollEndDate").val(), 'payrollStatus': $("#payrollStatus").val() });
        });
        else $("a", this).click(function() {
            U.GetAttachment("Payroll/SummaryDetailPdf", { 'userId': $("#payrollUserId").text(), 'payrollStartDate': $("#payrollStartDate").val(), 'payrollEndDate': $("#payrollEndDate").val(), 'payrollStatus': $("#payrollStatus").val() });
        });
    },
    LoadDetail: function(userId) {
        $("#payrollSearchType").val(userId);
        $("#payrollSearchResultDetail").Load("Payroll/Detail", { userId: userId, payrollStartDate: $("#payrollStartDate").val(), payrollEndDate: $("#payrollEndDate").val(), payrollStatus: $("#payrollStatus").val() }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == "success") {
                $("#payrollSearchResult,#payrollSearchResultDetails").hide();
                $("#markAsPaidButtonId").val("#payrollSearchResultDetail")
            }
            $("#payrollSearchResultDetail,#payrollMarkAsPaidButton").show()
        })
    },
    LoadDetails: function() {
        $("#payrollSearchType").val("");
        $("#payrollSearchResultDetails").Load("Payroll/Details", { payrollStartDate: $("#payrollStartDate").val(), payrollEndDate: $("#payrollEndDate").val(), payrollStatus: $("#payrollStatus").val() }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == "success") {
                $("#payrollSearchResult,#payrollSearchResultDetail").hide();
                $("#markAsPaidButtonId").val("#payrollSearchResultDetails")
            }
            $("#payrollSearchResultDetails,#payrollMarkAsPaidButton").show()
        })
    },
    MarkAsPaid: function() {
        var control = $("#markAsPaidButtonId").val();
        if ($("input[name=visitSelected]:checked").length > 0) {
            U.PostUrl('Payroll/PayVisit', $("input[name=visitSelected]:checked", $(control)).serializeArray(), function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    if ($("#payrollSearchType").val().length > 0) Payroll.LoadDetail($("#payrollSearchType").val());
                    else Payroll.LoadDetails();
                } else { U.Growl(result.errorMessage, "error"); }
            }, null);
        } else { U.Growl("Select at least one visit to mark as paid.", "error"); }
    },
    MarkAsUnpaid: function() {
        var control = $("#markAsPaidButtonId").val();
        if ($("input[name=visitSelected]:checked").length > 0) {
            U.PostUrl('Payroll/UnpayVisit', $("input[name=visitSelected]:checked", $(control)).serializeArray(), function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    if ($("#payrollSearchType").val().length > 0) Payroll.LoadDetail($("#payrollSearchType").val());
                    else Payroll.LoadDetails();
                } else { U.Growl(result.errorMessage, "error"); }
            }, null);
        } else { U.Growl("Select at least one visit to set as unpaid.", "error"); }
    }
}