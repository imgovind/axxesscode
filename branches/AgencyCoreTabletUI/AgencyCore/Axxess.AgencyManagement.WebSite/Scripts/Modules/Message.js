﻿var Message = {
    CKELoaded: false,
    CKI: 0,
    DefaultMessage: "",
    TokenList: null,
    AddRemoveRecipient: function(input) {
        if ($('#' + input).attr('checked')) $.data($('#' + input).get(0), "tokenbox", { "token": Message._tokenlist.insertToken($('#' + input).attr('value'), $('#' + input).attr('title')) });
        else Message._tokenlist.removeToken($.data($('#' + input).get(0), "tokenbox").token);
        Message.positionBottom();
    },
    Cancel: function() { UserInterface.CloseWindow("newmessage") },
    Delete: function(id, type, bypass) { U.Delete("Message", "Message/Delete", { Id: id, Type: type }, function() { Message.Rebind(); }, bypass); },
    Init: function() {
        $(".layout", "#window_messageinbox").layout({ west: { paneSelector: ".ui-layout-west", size: 300} });
        $(".folder-selector").hide().find("li").click(function() {
            var Index = $(this).prevAll().length;
            if (!$(".folder-selection li").eq(Index).hasClass("selected")) U.RebindGrid($("#list-messages"), { inboxType: Index ? "sent" : "inbox" });
            $(".folder-selection li").removeClass("selected").eq(Index).addClass("selected");
            $(".folder-selector").hide()
        })
        $(".folder-selection").click(function() { $(".folder-selector").show() })
    },
    InitWithMessage: function(MessageId) {
        this.DefaultMessage = MessageId;
        UserInterface.ShowMessages();
    },
    InitCKE: function() {
        CKEDITOR.replace('new-message-body', {
            skin: 'office2003',
            resize_enabled: false,
            height: '100%',
            removePlugins: 'elementspath',
            toolbarCanCollapse: false,
            toolbar: [['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                'JustifyBlock', '-', 'Link', 'Unlink', '-', 'Font', 'FontSize', 'TextColor', '-', 'SpellChecker', 'Scayt']]
        });
    },
    LoadMessage: function(e) {
        $("#message-view").Load("Message/Data", { id: $("td > div", e.row).attr("id"), type: $("td > div", e.row).attr("type") }, function() {
            $("td > div", e.row).removeClass("read-false").addClass("read-true");
            $(".message-body-container", "#message-content-panel").css("top",
                String($(".message-header-container", "#message-content-panel").height() + 70) + "px"
            )
            $(".winmenu .reply", "#message-content").click(function() {
                Message.Reply($(".t-state-selected > td > div", "#list-messages").attr("id"))
            })
            $(".winmenu .reply-all", "#message-content").click(function() {
                Message.ReplyAll($(".t-state-selected > td > div", "#list-messages").attr("id"))
            })
            $(".winmenu .forward", "#message-content").click(function() {
                Message.Forward($(".t-state-selected > td > div", "#list-messages").attr("id"))
            })
            $(".winmenu .delete", "#message-content").click(function() {
                Message.Delete($(".t-state-selected > td > div", "#list-messages").attr("id"), $(".t-state-selected > td > div", "#list-messages").attr("type"))
            })
        })
    },
    InitNew: function(type, id) {
        $(".layout", "#window_newmessage").layout();
        $("#New_Message_CheckallRecipients").change(function() {
            $("input[name=Recipients]", "#recipient-list").each(function() {
                if ($("#New_Message_CheckallRecipients").prop("checked") != $(this).prop("checked")) {
                    $(this).prop("checked", $("#New_Message_CheckallRecipients").prop("checked"));
                    Message.AddRemoveRecipient($(this).attr("id"));
                }
            })
        });
        $("#new-message-body-wrapper").html(
            $("<div/>").append(
                $("<textarea/>", { "id": "new-message-body", "name": "Body" })));
        Message.TokenList = $.fn.tokenInput("#New_Message_Recipents", "Message/Recipients", {
            classes: { tokenList: "recipient-list-input", token: "recipient-list-token", selectedToken: "recipient-list-token-selected", dropdown: "recipient-list-select", inputToken: "recipient-list-token-input" }
        });
        if (type == "reply" || type == "forward") U.PostUrl("Message/Get", { id: id }, function(message) {
            if (type == "reply") {
                $("#message-type-header").html("Reply Message");
                if (message.Subject.substring(0, 3) != "RE:") message.Subject = "RE: " + message.Subject;
                if ($("input[name=Recipients][value=" + message.FromId + "]").length) Message.AddRemoveRecipient($("input[name=Recipients][value=" + message.FromId + "]").prop("checked", true).attr("id"));
            } else {
                $("#message-type-header").html("Forward Message");
                if (message.Subject.substring(0, 3) != "FW:") message.Subject = "FW: " + message.Subject;
            }
            $("#New_Message_Subject").val(message.Subject);
            $("#New_Message_PatientId").val(message.PatientId);
            $("#new-message-body").val(message.ReplyForwardBody);
            if (!this.CKELoaded) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() {
                Message.CKELoaded = true;
                Message.InitCKE()
            });
            else Message.InitCKE();
        });
        else {
            if (!this.CKELoaded) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() {
                Message.CKELoaded = true;
                Message.InitCKE()
            });
            else Message.InitCKE();
        }
    },
    InitWidget: function() {
        $(".widget-more,li:eq(0)", "#messages-widget").click(UserInterface.ShowMessages);
        $("li:eq(1)", "#messages-widget").click(UserInterface.ShowNewMessage);
        $("li:eq(2)", "#messages-widget").click(function() {
            if ($("input:checked", "#messages-widget .widget-message-list").length) {
                if (confirm("Are you sure you want to delete all selected messages?")) $("input:checked", "#messages-widget .widget-message-list").each(function() {
                    Message.Delete($(this).val(), $(this).attr("messagetype"), true);
                    $(this).parent().remove();
                })
            } else U.Growl("Please select the messages you would like to delete", "error")
        });
        U.PostUrl("Message/WidgetList", {}, function(data) {
            if (data != undefined && data.Data != undefined && data.Data.length > 0) {
                var Id = "";
                for (var i = 0; i < data.Data.length; i++) {
                    $(".widget-message-list", "#messages-widget").append(
                        $("<li/>", { "class": (data.Data[i].MarkAsRead ? "" : "strong"), "guid": data.Data[i].Id }).append(
                            $("<input/>", { "type": "checkbox" }).attr("messagetype", data.Data[i].Type)).append(
                            $("<a/>", { "html": data.Data[i].FromName + " &#8211; " + data.Data[i].Subject }).click(function() {
                                $(this).parent().removeClass("strong");
                                if (Acore.Windows.messageinbox.IsOpen) {
                                    $("#window_messageinbox").WinFocus();
                                    $("#" + $(this).closest("li").attr("guid")).closest("tr").click()
                                } else Message.InitWithMessage($(this).closest("li").attr("guid"))
                            })
                        ).mouseover(function() { $(this).addClass("t-state-hover") }).mouseout(function() { $(this).removeClass("t-state-hover") }));
                }
            } else $(".widget-content", "#messages-widget").html(
                $("<h1/>", { "class": "ac", "text": "No Messages Found" }));
        });
    },
    OnMessageListReady: function(e) {
        if ($("#" + Message.DefaultMessage).length) $("#" + Message.DefaultMessage).closest("tr").click();
        else if ($("#list-messages .t-grid-content tr").length) $("#list-messages .t-grid-content tr").eq(0).click();
        else $("#messageInfoResult").html(
            $("<div/>", { "class": "ajaxerror" }).append(
                $("<h1/>", { "text": "No Messages found." })))
        Message.DefaultMessage = "";
    },
    PositionBottom: function() { $('#new-message-body-wrapper').css({ top: (parseInt($('.recipient-list-input').attr('clientHeight') + 100).toString() + "px") }); },
    Rebind: function() { U.RebindGrid($("#list-messages"), { inboxType: $("#inboxType").val() }); }
}