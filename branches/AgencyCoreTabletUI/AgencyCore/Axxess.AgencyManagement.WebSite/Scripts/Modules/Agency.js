﻿var Agency = {
    Edit: function(Id) { Acore.Open("editagency", 'Agency/Edit', function() { Agency.InitEdit(); }, { id: Id }); },
    InitEdit: function() { U.InitEditTemplate("Agency"); },
    InitEditCost: function() { U.InitTemplate($("#editVisitCostForm"), function() { UserInterface.CloseWindow('visitrates'); }, "Visit rate successfully updated"); },
    InitNew: function() {
        Lookup.loadStates();
        $(".names").alpha({ nocaps: false });

        $("input[name=New_Agency_SameAsAdmin]").click(function() {
            if ($(this).is(':checked')) {
                $("#New_Agency_ContactPersonEmail").val($("#New_Agency_AdminUsername").val());
                $("#New_Agency_ContactPersonFirstName").val($("#New_Agency_AdminFirstName").val());
                $("#New_Agency_ContactPersonLastName").val($("#New_Agency_AdminLastName").val());
            } else $("#New_Agency_ContactPersonEmail").add("#New_Agency_ContactPersonFirstName").add("#New_Agency_ContactPersonLastName").val('');
        });
        $("input[name=New_Agency_AxxessBiller]").click(function() {
            if ($(this).is(':checked')) {
                $("#New_Agency_SubmitterId").val("SW23071");
                $("#New_Agency_SubmitterName").val("Axxess Healthcare Consult");
                $("#New_Agency_SubmitterPhone1").val("214");
                $("#New_Agency_SubmitterPhone2").val("575");
                $("#New_Agency_SubmitterPhone3").val("7711");
                $("#New_Agency_SubmitterFax1").val("214");
                $("#New_Agency_SubmitterFax2").val("575");
                $("#New_Agency_SubmitterFax3").val("7722");
            }
            else {
                $("#New_Agency_SubmitterId").add("#New_Agency_SubmitterName").add("input[name=SubmitterPhoneArray]").add("input[name=SubmitterFaxArray]").val("");
            }
        });
        $("input[name=IsAgreementSigned]").change(function() {
            if ($(this).val() == "true") $("#New_Agency_TrialPeriod").val("").attr("disabled", "disabled");
            else $("#New_Agency_TrialPeriod").removeAttr("disabled");
        });
        U.InitNewTemplate("Agency");
    },
    loadCaseManagement: function(groupName) {
        $("#caseManagementContentId").Load("Agency/CaseManagementContent", { groupName: groupName, BranchId: $("#CaseManagement_BranchCode").val(), Status: $("#CaseManagement_Status").val() }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == "success") {
                $("#caseManagementContentId .t-group-indicator").hide();
                $("#caseManagementContentId .t-grouping-header").remove();
                var href = $("#CaseManagement_ExportLink").attr("href");
                if (href != null) {
                    href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#CaseManagement_BranchCode").val());
                    href = href.replace(/Status=([^&]*)/, 'Status=' + $("#CaseManagement_Status").val());
                    $exportLink.attr('href', href);
                }
            }
        })
    },
    tooltip: function() {
        $("a.tooltip", $("#caseManagementGrid")).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    loadPrintQueue: function(groupName) {
        $("#printQueueContentId").Load("Agency/PrintQueueContent", { groupName: groupName })
    },
    loadVisitRate: function() { Acore.Open("visitrates", 'Agency/VisitRates', function() { Agency.InitEditCost(); }); },
    loadVisitRateContent: function(branchId) {
        $("#Edit_VisitRate_Container").Load("Agency/VisitRateContent", { branchId: branchId })
    },
    MarkOrder: function(mark, control) {
        fields = $("select.SendAutomatically, input.OrdersToBeSent:checked", $(control)).serializeArray();
        U.PostUrl("/Agency/MarkOrdersAs" + mark, fields, function(data) {
            if (data.isSuccessful) {
                Agency.RebindOrders();
                Patient.Rebind();
                Schedule.Rebind();
            } else U.Growl(data.errorMessage, 'error');
        });
    },
    MarkOrdersAsReturned: function(container) { Agency.MarkOrder("Returned", container); },
    MarkOrdersAsSent: function(container) { Agency.MarkOrder("Sent", container); },
    RebindCaseManagement: function() { Agency.loadCaseManagement($("#CaseManagement_GroupName").val()); },
    RebindPrintQueue: function() { U.RebindGrid($('#printQueueGrid')); },
    RebindList: function() { U.RebindGrid($('#List_Agencies')); },
    RebindOrders: function() { U.RebindGrid($('#List_OrdersToBeSent')); U.RebindGrid($('#List_OrdersPendingSignature')); },
    OrderHistoryEditInit: function() { U.InitTemplate($("#updateOrderHistry"), function() { Agency.RebindOrdersHistory(); UserInterface.CloseModal(); }, "Order successfully updated"); },
    PendingSignatureOrdersOnEdit: function(e) {
        var form = e.form;
        var dataItem = e.dataItem;
        if (dataItem != null) {
            dataItem.StartDate = $("#OrdersPendingSignature_StartDate").val();
            dataItem.EndDate = $("#OrdersPendingSignature_EndDate").val();
            dataItem.BranchId = $("#OrdersPendingSignature_BranchId").val();
            e.dateItem = dataItem;
        }
    },
    OrderCenterOnload: function(e) { $('.t-grid-edit', e.row).html('Receive Order'); },
    RebindOrdersHistory: function() { Agency.GenerateClick("OrdersHistory"); },
    RebindPendingOrders: function() { Agency.GenerateClick("OrdersPendingSignature"); },
    RebindOrdersToBeSent: function() { Agency.GenerateClick("OrdersToBeSent"); },
    GenerateClick: function(type) {
        var grid = $("#List_" + type).data('tGrid'),
            data = { BranchId: $("#" + type + "_BranchId").val(), StartDate: $("#" + type + "_StartDate").val(), EndDate: $("#" + type + "_EndDate").val() };
        if (type == "OrdersToBeSent") data.sendAutomatically = $("#List_OrdersToBeSent_SendType").val();
        if (grid != null) grid.rebind(data);
        var link = $("#" + type + "_ExportLink").attr("href");
        if (link) {
            link = link.replace(/BranchId=([^&]*)/, "BranchId=" + $("#" + type + "_BranchId").val());
            link = link.replace(/StartDate=([^&]*)/, "StartDate=" + escape($("#" + type + "_StartDate").val()));
            link = link.replace(/EndDate=([^&]*)/, "EndDate=" + escape($("#" + type + "_EndDate").val()));
            if (type == "OrdersToBeSent") { link = link.replace(/sendAutomatically=([^&]*)/, "sendAutomatically=" + $("#List_OrdersToBeSent_SendType").val()); }
            $("#" + type + "_ExportLink").attr("href", link);
        }
    },
    RebindAgencyCommunicationNotes: function() {
        var grid = $('#List_CommunicationNote').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AgencyCommunicationNote_BranchCode").val(), Status: $("#AgencyCommunicationNote_Status").val(), StartDate: $("#AgencyCommunicationNote_StartDate-input").val(), EndDate: $("#AgencyCommunicationNote_EndDate-input").val() }); }
        var $exportLink = $('#AgencyCommunicationNote_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyCommunicationNote_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#AgencyCommunicationNote_Status").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#AgencyCommunicationNote_StartDate-input").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#AgencyCommunicationNote_EndDate-input").val());
        $exportLink.attr('href', href);
    },
    RebindAgencyPastDueRecet: function() {
        var grid = $('#List_PastDueRecerts').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AgencyPastDueRecet_BranchCode").val(), InsuranceId: $("#AgencyPastDueRecet_InsuranceId").val(), StartDate: $("#AgencyPastDueRecet_StartDate").val() }); }
        var $exportLink = $('#AgencyPastDueRecet_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyPastDueRecet_BranchCode").val());
        href = href.replace(/InsuranceId=([^&]*)/, 'InsuranceId=' + $("#AgencyPastDueRecet_InsuranceId").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#AgencyPastDueRecet_StartDate").val());
        $exportLink.attr('href', href);
    },
    RebindAgencyUpcomingRecet: function() {
        var grid = $('#List_UpcomingRecerts').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AgencyUpcomingRecet_BranchCode").val(), InsuranceId: $("#AgencyUpcomingRecet_InsuranceId").val() }); }
        var $exportLink = $('#AgencyUpcomingRecet_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyUpcomingRecet_BranchCode").val());
        href = href.replace(/InsuranceId=([^&]*)/, 'InsuranceId=' + $("#AgencyUpcomingRecet_InsuranceId").val());
        $exportLink.attr('href', href);
    },
    RebindExportedOasis: function() {
        var grid = $('#exportedOasisGrid').data('tGrid'); if (grid != null) { grid.rebind({ BranchId: $("#ExportedOasis_BranchCode").val(), Status: $("#ExportedOasis_Status").val(), StartDate: $("#ExportedOasis_StartDate").val(), EndDate: $("#ExportedOasis_EndDate").val() }); }
        var $exportLink = $('#ExportedOasis_ExportLink');
        var href = $exportLink.attr('href');
        if (href != null) {
            href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ExportedOasis_BranchCode").val());
            href = href.replace(/Status=([^&]*)/, 'Status=' + $("#ExportedOasis_Status").val());
            href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#ExportedOasis_StartDate").val());
            href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#ExportedOasis_EndDate").val());
            $exportLink.attr('href', href);
        }
    },
    RebindOasisToExport: function() {
        var grid = $('#generateOasisGrid').data('tGrid'); if (grid != null) { grid.rebind({ BranchId: $("#OasisExport_BranchCode").val(), InsuranceId: $("#OasisExport_InsuranceId").val() }); }
        var $exportLink = $('#OasisExport_ExportLink');
        var href = $exportLink.attr('href');
        if (href != null) {
            href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#OasisExport_BranchCode").val());
            href = href.replace(/InsuranceId=([^&]*)/, 'InsuranceId=' + $("#OasisExport_InsuranceId").val());
            $exportLink.attr('href', href);
        }
    }
}