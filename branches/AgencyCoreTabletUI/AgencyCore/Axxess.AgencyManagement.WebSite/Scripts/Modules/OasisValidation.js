﻿var OasisValidation = {
    Validate: function(id, patientId, episodeId, assessmentType) {
        Acore.OpenPrintView({
            Url: "Validate/" + id + "/" + patientId + "/" + episodeId + "/" + assessmentType,
            Buttons: [{
                Text: 'OASIS Scrubber',
                Click: function() { U.GetAttachment('Oasis/AuditPdf', { id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType }); } }]
            });
            $("#print-controls a:last").text("Return to OASIS");
            $("#print-controls li:first").addClass("red");
        }
    }
    