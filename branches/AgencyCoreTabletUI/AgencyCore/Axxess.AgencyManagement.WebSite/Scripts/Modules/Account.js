﻿var Logon = {
    Promo: false,
    AccountInUse: function(Id, Email) {
        $(".loading-modal").hide();
        $(".login-promt").empty().append(
            $("<h3/>", { "text": "Account Already in Use" })).append(
            $("<p/>", { "text": "This user is already logged in on another computer. If you choose to proceed, the user will be automatically logged off the other computer and their work will not be saved." })).append(
            $("<div/>", { "class": "buttons" }).append(
                $("<ul/>").append(
                    $("<li/>").append(
                        $("<a/>", { "text": "Continue" }).click(function() {
                            $(".login-promt").fadeOut(500, function() { $(".loading-modal").show() });
                            U.PostUrl("Account/Kick", { loginId: Id, emailAddress: Email }, function(result) {
                                if (result.isSuccessful) $(location).attr("href", result.redirectUrl);
                                else {
                                    U.Growl(result.errorMessage, "error");
                                    U.Loading(false);
                                    $(".login-bar").animate({ right: "0em" }, 800, function() { if (Logon.Promo) $(".promo-container").fadeIn(500) });
                                }
                            });
                            return false;
                        }))).append(
                    $("<li/>").append(
                        $("<a/>", { "text": "Cancel" }).click(function() {
                            U.Loading(false);
                            $(".login-promt").fadeOut(500);
                            $(".login-bar").animate({ right: "0em" }, 800, function() { if (Logon.Promo) $(".promo-container").fadeIn(500) });
                            return false;
                        })
                    )
                )
            )
        );
        $(".login-promt").fadeIn(1000)
    },
    ActivateInit: function() {
        $(".login-promt").fadeIn(1000);
        $("#activate-accountForm").validate({
            rules: { Password: { required: true, minlength: 8} },
            messages: { Password: { required: "* required", minlength: "8 characters minimum"} },
            submitHandler: function(form) {
                $(".login-promt").fadeOut(1000, function() { $(".shade").remove(); U.Loading(true); })
                $(form).ajaxSubmit({
                    dataType: "json",
                    success: function(result) {
                        if (result.isSuccessful) window.location.replace(result.redirectUrl);
                        else {
                            $("#login-promt").empty().append(
                                $("<h3/>", { "text": "Activation Failed" })).append(
                                $("<p/>", { "text": result.errorMessage }));
                            $(".loading-modal").remove();
                            $(".login-promt").fadeIn(1000);
                        }
                    }
                });
                return false;
            }
        })
    },
    AgencySelect: function(Id) {
        $(".loading-modal").hide();
        $(".login-promt").empty().load("/Account/Agencies", { loginId: Id }, function() { $(".login-promt").fadeIn(1000) })
    },
    ExpireInit: function() {
        $(".login-promt").fadeIn(1000);
        $(".login").click(function() {
            $(".login-promt").fadeOut(500, function() { $(location).attr("href", "Logout") });
            return false;
        })
    },
    Fail: function() {
        $(".loading-modal").hide();
        $(".login-promt").empty().append(
            $("<h3/>", { "text": "Login Failed" })).append(
            $("<p/>", { "text": "We could not validate your email address and password. Please check this information and try again." })).append(
            $("<div/>", { "class": "buttons" }).append(
                $("<ul/>").append(
                    $("<li/>").append(
                        $("<a/>", { "text": "OK" }).click(function() {
                            U.Loading(false);
                            $(".login-promt").fadeOut(500);
                            $(".login-bar").animate({ right: "0em" }, 800, function() { if (Logon.Promo) $(".promo-container").fadeIn(500) });
                        })
                    )
                )
            )
        );
        $(".login-promt").fadeIn(1000)
    },
    ForgotPassword: function(Id) {
        U.Loading(true);
        if (Logon.Promo) $(".promo-container").fadeOut(1000);
        $(".login-bar").animate({ right: "-50em" }, 1000, function() { $(location).attr("href", "Forgot") });
        return false;
    },
    ForgotPasswordInit: function() {
        $(".login-promt").fadeIn(1000);
        $(".login-promt .send").click(function() {
            if ($("#forgot-user").val().length && U.IsEmail($("#forgot-user").val()) && $("#recaptcha_response_field").val().length) $(this).closest("form").ajaxSubmit({
                dataType: "json",
                success: function(result) {
                    if (result.isSuccessful) $(".login-promt").fadeOut(500, function() { $(location).attr("href", "Login") });
                    else U.Growl(result.errorMessage, "error")
                }
            });
            else U.Growl("Please Enter your Email Address and Security Check", "error");
            return false;
        });
        $(".login-promt .cancel").click(function() {
            $(".login-promt").fadeOut(500, function() { $(location).attr("href", "Login") });
            return false;
        })
    },
    Init: function() {
        $("html").bind("keypress", function(e) { if (e.keyCode == 13) $(".submit").click() });
        $(".login-bar").animate({ right: "0em" }, 800, function() { if (Logon.Promo) $(".promo-container").fadeIn(500) });
        $(".submit").click(Logon.PerformLogin);
        $(".forgot").click(Logon.ForgotPassword);
    },
    LinkInit: function() {
        $(".login-promt").fadeIn(1000);
        $(".login").click(function() {
            $(".login-promt").fadeOut(500, function() { $(location).attr("href", "Login") });
            return false;
        })
    },
    PerformLogin: function() {
        if ($("#login-user").val().length && U.IsEmail($("#login-user").val()) && $("#login-pass").val().length) {
            U.Loading(true);
            $(".login-bar").animate({ right: "-50em" }, 1000);
            if (Logon.Promo) $(".promo-container").fadeOut(1000)
            U.PostUrl("Account/LogOn", {
                UserName: $("#login-user").val(),
                Password: $("#login-pass").val(),
                RememberMe: $("#login-remember").prop("checked")
            }, function(result) {
                if (result.isSuccessful) {
                    if (result.hasMultiple) Logon.AgencySelect(result.id);
                    else if (result.isAccountInUse) Logon.AccountInUse(result.id, result.email);
                    else $(location).attr("href", result.redirectUrl);
                } else Logon.Fail();
            })
        } else U.Growl("Please Enter your Email Address and Password", "error");
        return false;
    },
    Select: function(agencyId, userId) {
        U.PostUrl("/Account/Agency", { agencyId: agencyId, userId: userId }, function(result) {
            if (result.isSuccessful) $(location).attr("href", result.redirectUrl);
            else if (result.isAccountInUse) Logon.AccountInUse(result.id, result.email);
            else Logon.Fail();
        })
    }
}