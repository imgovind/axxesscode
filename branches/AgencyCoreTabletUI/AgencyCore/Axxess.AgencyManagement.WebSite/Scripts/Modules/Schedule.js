﻿var Schedule = {
    _scheduleWindow: "",
    _patientId: "",
    _EpisodeId: "",
    _patientName: "",
    _EpisodeStartDate: "",
    _EpisodeEndDate: "",
    _tableId: "NursingScheduleTable",
    _Discipline: "all",
    _DisciplineIndex: 0,
    GetTableId: function() { return Schedule._tableId; },
    SetTableId: function(tableId) { Schedule._tableId = tableId; },
    GetId: function() { return Schedule._patientId; },
    SetId: function(patientId) { Schedule._patientId = patientId; },
    SetName: function(patientName) { Schedule._patientName = patientName; },
    SetPatientRowIndex: function(patientRowIndex) { Schedule._patientRowIndex = patientRowIndex; },
    GetEpisodeId: function() { return Schedule._EpisodeId; },
    SetEpisodeId: function(episodeId) { Schedule._EpisodeId = episodeId; },
    SetStartDate: function(episodeStartDate) { Schedule._EpisodeStartDate = episodeStartDate; },
    SetEndDate: function(episodeEndDate) { Schedule._EpisodeEndDate = episodeEndDate; },
    SetDiscipline: function(discipline) { Schedule._Discipline = discipline; },
    SetDisciplineIndex: function(disciplineIndex) { Schedule._DisciplineIndex = disciplineIndex; },
    Filter: function(text) {
        search = text.split(" ");
        $("tr", "#ScheduleSelectionGrid .t-grid-content").removeClass("match").hide();
        for (var i = 0; i < search.length; i++) {
            $("td", "#ScheduleSelectionGrid .t-grid-content").each(function() {
                if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
            });
        }
        $("tr.match", "#ScheduleSelectionGrid .t-grid-content").removeClass("t-alt").show();
        $("tr.match:even", "#ScheduleSelectionGrid .t-grid-content").addClass("t-alt");
    },
    InitCenter: function(patient) {
        $('#window_schedulecenter .layout').layout();
        Lookup.loadMultipleDisciplines("#multipleScheduleTable");
        $("#window_schedulecenter .top input").keyup(function() {
            if ($(this).val()) {
                Schedule.Filter($(this).val())
                if ($("tr.match:even", "#ScheduleSelectionGrid .t-grid-content").length) $("tr.match:first", "#ScheduleSelectionGrid .t-grid-content").click();
                else $("#ScheduleMainResult").html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Ch1%3ENo Patients Found to Meet Your Search Requirements%3C/h1%3E%3" +
                    "Cdiv class=%22buttons%22%3E%3Cul%3E%3Cli%3E%3Ca title=%22Add New Patient%22 onclick=%22javascript:Acore.Open('newpatient');%22 href=%22javas" +
                    "cript:void(0);%22%3EAdd New Patient%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E%3C/div%3E"));
            } else {
                $("tr", "#ScheduleSelectionGrid .t-grid-content").removeClass("match t-alt").show();
                $("tr:even", "#ScheduleSelectionGrid .t-grid-content").addClass("t-alt");
                $("tr:first", "#ScheduleSelectionGrid .t-grid-content").click();
            }
        });
        $("#window_schedulecenter .top select").change(function() { Schedule._patientId = ""; U.FilterResults("Schedule"); });
        if (U.IsGuid(patient)) Schedule._patientId = patient;
        else $("#ScheduleSelectionGrid .t-grid-content tr:first").click();
    },
    Add: function(date) {
        if ($(".scheduleTables.purgable tbody tr").length > 0) Schedule.CurrentTableAdd(date);
        else Schedule.AllTableAdd(date);
    },
    CurrentTableAdd: function(currentday) {
        if ($('#ScheduleTabStrip').is(':hidden')) Schedule.ShowScheduler();
        if ($('tbody  tr', $("#" + Schedule.GetTableId())).length < 4) Schedule.addTableRow("#" + Schedule.GetTableId(), currentday, Schedule._Discipline);
    },
    AllTableAdd: function(date) {
        if (date == undefined) date = "";
        Schedule.addTableRow("#NursingScheduleTable", date, "Nursing");
        Schedule.addTableRow("#TherapyScheduleTable", date, "Therapy");
        Schedule.addTableRow("#HHAScheduleTable", date, "HHA");
        Schedule.addTableRow("#MSWScheduleTable", date, "MSW");
        Schedule.addTableRow("#OrdersScheduleTable", date, "Orders");
        Schedule.ShowScheduler();
    },
    RebindActivity: function() {
        var grid = $('#ScheduleActivityGrid').data('tGrid');
        if (grid != null) grid.rebind({ episodeId: Schedule._EpisodeId, patientId: Schedule._patientId, discipline: Schedule._Discipline });
    },
    RebindCalendar: function() {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Schedule/GetEpisode",
            data: "patientId=" + Schedule._patientId + "&episodeId=" + Schedule._EpisodeId + "&discipline=" + Schedule._Discipline,
            success: function(result) {
                var data = eval(result);
                $("#EpisodeStartDate").text((data.StartDateFormatted !== null ? data.StartDateFormatted : "________"));
                $("#EpisodeEndDate").text((data.EndDateFormatted !== null && data.EndDateFormatted !== undefined ? data.EndDateFormatted : "________"));
                if (data.HasNext) {
                    if (data.NextEpisode != null) {
                        $("#nextEpisode").val(data.NextEpisode.Id);
                        $("#nextEpisode").show();
                    } else $("#nextEpisode").hide();
                } else $("#nextEpisode").hide();
                if (data.HasPrevious) {
                    if (data.PreviousEpisode != null) {
                        $("#previousEpisode").val(data.PreviousEpisode.Id);
                        $("#previousEpisode").show();
                    } else $("#previousEpisode").hide();
                } else $("#previousEpisode").hide();
            }
        });
    },
    MissedVisitPopup: function(e, missedVisitId) {
        Acore.Dialog({
            Name: "Missed Visit",
            Url: "Schedule/MissedVisitInfo",
            Input: { id: missedVisitId },
            Width: "500px"
        })
    },
    ShowAll: function(patientId) {
        Schedule.SetDiscipline('all');
        if ($("#scheduleTop").html() != null) Schedule.loadCalendar(patientId, Schedule._Discipline);
        else Schedule.loadCalendarAndActivities(patientId);
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
            $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            var patientId = e.row.cells[2].innerHTML;
            Schedule.loadCalendarAndActivities(patientId);
            Lookup.loadMultipleDisciplines("#multipleScheduleTable");
        }
    },
    loadCalendarAndActivities: function(patientId) {
        $("#ScheduleMainResult").Load("Schedule/Data", { patientId: patientId }, function() {
            Schedule.SetId(patientId);
        })
    },
    RefreshCurrentEpisode: function(patientId, episodeId) {
        $('#ScheduleMainResult').Load("Schedule/RefreshData", { patientId: patientId, episodeId: episodeId }, function() {
            Schedule.SetId(patientId);
        })
    },
    NoPatientBind: function(id) {
        Schedule.loadCalendar(id, Schedule._Discipline);
        Schedule.loadFirstActivity(id);
    },
    addTableRow: function(table, currentday, disp) {
        if ($(table + " tbody tr").length && $(table + " tbody tr:first input.currentDate").val() == "") $(table + " tbody tr:first input.currentDate").val(currentday);
        else {
            var row = "%3Ctr%3E%3Ctd%3E%3Cselect class=%22DisciplineTask required notzero%22%3E%3Coption value=%220%22%3E-- Select Discipline --%3C/option%3E%3C/select%3E%3C/td%3" +
                "E%3Ctd%3E%3Cselect class=%22supplies Code Users required guid%22%3E%3Coption value=%2200000000-0000-0000-0000-000000000000%22%3ESelect Employee%3C/option%3E%3" +
                "C/select%3E%3C/td%3E%3Ctd%3E%3Cinput type=%22text%22 class=%22currentDate%22 value=%22" + currentday + "%22 readonly=%22readonly%22 /%3E%3C/td%3E%3Ctd%3E%3Ca cla" +
                "ss=%22action%22 onclick=%22Oasis.DeleteRow($(this));return false%22%3EDelete%3C/a%3E%3C/td%3E%3C/tr%3E";
            $(table).find('tbody').append(unescape(row));
            Lookup.loadDiscipline(table, disp);
            Lookup.loadUsers(table);
            Schedule.positionBottom();
        }
    },
    ScheduleInputFix: function(control, Type, tableName) {
        var submit = true;
        var tableTr = $('tbody tr', $(tableName));
        var len = tableTr.length;
        var i = 1;
        var jsonData = '[';
        $(tableTr).each(function() {
            if ($(this).find("select.DisciplineTask").val() != "0" && $(this).find("select.Users").val() != "00000000-0000-0000-0000-000000000000" && $(this).find("input.currentDate").val() != "") {
                if (len + 1 > i) jsonData += '{"DisciplineTask":"' + $(this).find('.DisciplineTask').val() + '","UserId":"' + $(this).find('.Users').val() + '","EventDate":"' + $(this).find('.currentDate').val() + '","Discipline":"' + $(this).find('.DisciplineTask').find(":selected").attr("data") + '","IsBillable":"' + $(this).find('.DisciplineTask').find(":selected").attr("isbillable") + ' "}';
                if (len > i) jsonData += ',';
                i++
            } else {
                U.Growl("Unable to schedule this task. Make sure all required fields are entered in row " + ($(this).parent().children().index($(this)) + 1) + ". ", "error");
                submit = false;
            }
        });
        jsonData += ']';
        control.closest('form').find('input[name= ' + Type + '_Schedule][type=hidden]').val(jsonData.toString());
        control.closest('form').find('input[name=episodeId][type=hidden]').val($("#ScheduleEpisodeID").val());
        control.closest('form').find('input[name=patientId][type=hidden]').val($("#SchedulePatientID").val());
        if (submit) Schedule.FormSubmit(control);
    },
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var patientId = $("#SchedulePatientID").val();
                    var episodeId = $("#ScheduleEpisodeID").val()
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                    control.closest("table").find('tbody').empty();
                    Schedule.CurrentTableAdd('');
                    Schedule.CloseNewEvent(control);
                    U.Growl("Task(s) successfully added to the patient's episode.", "success");
                } else U.Growl(resultObject.errorMessage, "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    HandlerHelperMultiple: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var tabstrip = $("#ScheduleTabStrip").data("tTabStrip");
                    var item = $("li", tabstrip.element)[Schedule._DisciplineIndex];
                    tabstrip.select(item);
                    var patientId = $("#SchedulePatientID").val();
                    var episodeId = $("#ScheduleEpisodeID").val();
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                    Schedule.CloseNewEvent(control);
                    U.Growl("Task(s) successfully added to the patient's episode.", "success");
                } else U.Growl(resultObject.errorMessage, "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    FormSubmit: function(control) {
        var form = control.closest("form");
        form.validate();
        Schedule.HandlerHelper(form, control);
    },
    FormSubmitMultiple: function(control) {
        control.closest('form').find('input[name=episodeId][type=hidden]').val($("#ScheduleEpisodeID").val());
        control.closest('form').find('input[name=patientId][type=hidden]').val($("#SchedulePatientID").val());
        control.closest('form').find('input[name=Discipline][type=hidden]').val($('#multipleDisciplineTask').find(":selected").attr("data"));
        control.closest('form').find('input[name=IsBillable][type=hidden]').val($('#multipleDisciplineTask').find(":selected").attr("IsBillable"));

        var form = control.closest("form");
        form.validate();
        Schedule.HandlerHelperMultiple(form, control);
    },
    ReassignHelper: function(form, control, patientId, episodeId) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if (result.isSuccessful) {
                    if ($('#ScheduleActivityGrid').length) $('#ScheduleActivityGrid').data('tGrid').rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    if ($('#PatientActivityGrid').length) $('#PatientActivityGrid').data('tGrid').rebind({ patientId: Patient._patientId, discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val() });
                } else U.Growl(result.errorMessage, "error");
            },
            error: function() {
                U.Growl("Unable to reassign task to this user", "error")
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    SubmitReassign: function(control, patientId, episodeId) {
        var form = control.closest("form");
        form.validate();
        Schedule.ReassignHelper(form, control, patientId, episodeId);
    },
    ClearRows: function(table) {
        $('tbody tr', table).each(function() {
            $(this).remove();
        });
    },
    OnSelect: function(e) {
        var content = $(e.contentElement);
        var tableControl = $('table', content);
        if ($(tableControl).attr('id') == "multipleScheduleTable") return true;
        Schedule.SetDisciplineIndex($(e.item).index());
        Schedule.SetTableId($(tableControl).attr('id'));
        Schedule.SetDiscipline($(tableControl).attr('data'));
        if (Schedule._Discipline == "Multiple") return;
        else {
            var patientId = $("#SchedulePatientID").val();
            var episodeId = $("#ScheduleEpisodeID").val()
            Schedule.loadCalendarNavigation(episodeId, patientId);
            var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
            scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
            var name = "#" + Schedule.GetTableId();
            var $table = $(name);
        }
    },
    Delete: function(patientId, episodeId, eventId, employeeId, task) {
        if (confirm("Are you sure you want to delete this task?")) {
            var input = "patientId=" + patientId + "&eventId=" + eventId + "&employeeId=" + employeeId + "&episodeId=" + episodeId + "&task=" + task;
            U.PostUrl("/Schedule/Delete", input, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    if (scheduleActivityGrid != null) {
                        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    }
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                    Patient.CustomDateRange();
                } else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    Restore: function(episodeId, patientId, eventId) {
        if (confirm("Are you sure you want to restore this task?")) {
            var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId;
            U.PostUrl("/Schedule/Restore", input, function(result) {
                if (result.isSuccessful) {
                    Schedule.Rebind();
                    Patient.Rebind();
                    var deletedGrid = $('#List_Patient_DeletedTasks').data('tGrid');
                    if (deletedGrid != null) {
                        deletedGrid.rebind({ patientId: patientId });
                    }
                    U.Growl(result.errorMessage, "success");
                } else { U.Growl(result.errorMessage, "error"); }
            });
        }
    },
    ReAssign: function(control, episodeId, patientId, id, oldEmployeeId) {
        control.hide();
        control.parent().append(unescape("%3Cform action=%22/Schedule/ReAssign%22 method=%22post%22%3E%3Cinput name=%22episodeId%22 type=%22hidden%22 value=%22" +
            episodeId + "%22 /%3E%3Cinput name=%22oldUserId%22 type=%22hidden%22 value=%22" + oldEmployeeId + "%22 /%3E%3Cinput name=%22patientId%22 type=%22hidd" +
            "en%22 value=%22" + patientId + "%22 /%3E%3Cinput name=%22eventId%22 type=%22hidden%22 value=%22" + id + "%22 /%3E%3Cselect class=%22Users%22 name=%2" +
            "2userId%22 style=%22width:130px;%22 class=%22Users%22%3E%3C/select%3E%3Cinput type=%22button%22 value=%22Save%22 onclick=%22Schedule.SubmitReassign(" +
            "$(this),'" + patientId + "','" + episodeId + "');%22/%3E %3Cinput type=%22button%22 value=%22Cancel%22 onclick=%22Schedule.CancelReassign($(this));%" +
            "22 /%3E%3C/form%3E"));
        Lookup.loadUsers("#" + control.closest(".t-grid").attr("id"));
    },
    ReOpen: function(episodeId, patientId, eventId) {
        var input = "patientId=" + patientId + "&eventId=" + eventId + "&episodeId=" + episodeId;
        U.PostUrl("/Schedule/Reopen", input, function(result) {
            if (result.isSuccessful) {
                if ($("#scheduleTop").html() != null) {
                    Schedule.loadCalendar(patientId, Schedule._Discipline);

                } else {
                    Schedule.loadCalendarAndActivities(patientId);
                }
                Lookup.loadMultipleDisciplines("#multipleScheduleTable");
                Patient.Rebind();
            }
        });
    },
    CancelReassign: function(control) {
        var reassignLink = control.parent().parent().find('a.reassign').show();
        control.parent().remove();
    },
    CloseNewEvent: function(control) {
        $('.scheduleTables.purgable').each(function() { $(this).find('tbody').empty(); })
        Schedule.ShowScheduler();
    },
    NavigateEpisode: function(episodeId, patientId) {
        Schedule.RefreshCurrentEpisode(patientId, episodeId)
    },
    loadCalendarNavigation: function(EpisodeId, PatientId) {
        $("#scheduleTop").Load("Schedule/CalendarNav", { patientId: PatientId, episodeId: EpisodeId, discipline: Schedule._Discipline }, function() {
            Schedule.positionBottom()
        })
    },
    loadMasterCalendar: function(_patientId, _EpisodeId) {
        Acore.Open("masterCalendarMain", 'Schedule/MasterCalendarMain', function() {
            $("table.masterCalendar tbody tr td.lastTd .events").each(function() {
                $(this).css({
                    position: 'relative',
                    left: -100,
                    top: 0
                });
            });
        }, { patientId: _patientId, episodeId: _EpisodeId });
    },
    loadMasterCalendarNavigation: function(EpisodeId, PatientId) {
        $("#window_masterCalendarMain_content").Load("/Schedule/MasterCalendar", { patientId: PatientId, episodeId: EpisodeId })
    },
    loadCalendar: function(patientId, discipline) {
        $("#scheduleTop").Load("Schedule/Calendar", { patientId: patientId, discipline: discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == "success") Schedule.loadActivity(patientId)
        })
    },
    loadActivity: function(patientId) {
        $("#scheduleBottomPanel").Load("Schedule/ActivityFirstTime", { patientId: patientId, discipline: Schedule._Discipline })
    },
    loadFirstActivity: function(PatientId) {
        $("#scheduleBottomPanel").Load("Schedule/ActivityFirstTime", { patientId: PatientId, discipline: Schedule._Discipline })
    },
    InitEpisode: function() {
        $("#Edit_Episode_StartDate").blur(function() { setTimeout(Schedule.editEpisodeStartDateOnChange, 200) });
        $("#editEpisodeForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseModal();
                            Patient.Rebind();
                            Schedule.Rebind();
                            Schedule.ReLoadInactiveEpisodes($("#editEpisodeForm").find("#PatientId").val());
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function(result) {

                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    gatherMultiSchedulerDates: function() {
        var visitDates = '';
        $('.multiDayScheduleTable td.selectdate').each(function() {
            visitDates += $(this).attr("date") + ",";
        });
        $("#multiDayScheduleVisitDates").val(visitDates);
    },
    InitMultiDayScheduler: function() {
        $("#multiDayScheduleForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseModal();
                            Patient.Rebind();
                            var patientId = $("#SchedulePatientID").val();
                            var episodeId = $("#ScheduleEpisodeID").val()
                            var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                            scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                            Schedule.loadCalendarNavigation(episodeId, patientId);
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function(result) { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNewEpisode: function() {
        $("#New_Episode_StartDate").blur(function() { setTimeout(Schedule.newEpisodeStartDateOnChange, 200) });
        $('#New_Episode_PatientId').change(function() {
            $("#New_Episode_PrimaryPhysician").PhyscianInput();
            var patientId = $(this).val();
            U.PostUrl("/Patient/GetPatientForEpisode", "patientId=" + patientId, function(data) {
                if (data != null) {
                    $("#newEpisodeTargetDateDiv").show();
                    $("#newEpisodeTargetDate").html(data.StartOfCareDateFormatted);
                    $("#newEpisodeTip").html("<label class=\"bold\">Tip:</label><em> Last Episode end date is: " + data.EndDateFormatted + "</em>");
                } else {
                    $("#newEpisodeTip").html("");
                    $("#newEpisodeTargetDate").html("");
                    $("#newEpisodeTargetDateDiv").hide();
                }
            });
        });
        $("#newEpisodeForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseModal();
                            Patient.Rebind();
                            Schedule.Rebind();
                            UserInterface.CloseWindow("newepisode");
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function(result) {

                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitTopMenuNewEpisode: function() {
        $("#TopMenuNew_Episode_PatientId").change(function() {
            var patientId = $(this).val();
            $("#topMenuNewEpisodeContent").Load("Schedule/NewPatientEpisodeContent", { patientId: patientId })
        });
        $("#topMenuNewEpisodeForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseWindow("newepisode");
                            Patient.Rebind();
                            Schedule.Rebind();
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function(result) { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    GetEpisode: function(episodeId, patientId) { Acore.Open("editepisode", 'Schedule/EditEpisode', function() { Schedule.InitEpisode(); }, { episodeId: episodeId, patientId: patientId }); },
    InitTaskDetails: function() {
        $("#scheduleDetailsForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("Task details updated successfully.", "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            UserInterface.CloseWindow('scheduledetails');
                        } else U.Growl($.trim(result.responseText), "error");
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("Task details updated successfully.", "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            UserInterface.CloseWindow('scheduledetails');
                        }
                        else U.Growl($.trim(result.responseText), "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    GetTaskDetails: function(episodeId, patientId, eventId) {
        Acore.Open("scheduledetails", 'Schedule/EditDetails', function() {
            Schedule.InitTaskDetails();
        }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    EventMouseOver: function(control) {
        var currentControl = $('.events', $(control));
        currentControl.show();
    },
    EventMouseOut: function(control) {
        $('.events', $(control)).hide();
    },
    positionBottom: function() {
        $('#window_schedulecenter .ui-layout-center .bottom').css({ top: $('#window_schedulecenter .ui-layout-center .wrapper').height() + 25 });
    },
    ProcessNote: function(button, episodeId, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("/Schedule/ProcessNotes", { button: button, episodeId: episodeId, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl("/Schedule/ProcessNotes", { button: button, episodeId: episodeId, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
        }
    },
    tooltip: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    activityRowDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
        if (dataItem.IsComplete) { $(e.row).addClass('darkgreen'); }
        if (dataItem.IsOrphaned) {
            $(e.row).addClass('black').tooltip({
                track: true,
                showURL: false,
                top: 5,
                left: 5,
                extraClass: "calday error",
                bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
            });
            $(e.row.cells[1]).addClass('darkred');
        }
        $(e.row).bind("contextmenu", function(Event) {
            var Menu = $("<ul/>");
            if (dataItem.IsComplete) Menu.append(
                $("<li/>").append(
                    $("<a/>", { "text": "Reopen Task" }).click(function() {
                        $(e.row).find("a:contains('Reopen Task')").click();
                        return false
                    })));
            else if (!dataItem.IsOrphaned) Menu.append(
                $("<li/>").append(
                    $("<a/>", { "text": "Edit Note" }).click(function() {
                        $(e.row).find("a:first").click();
                        return false
                    })));
            if ($(e.row).find("a:contains('Details')").length) Menu.append(
                $("<li/>").append(
                    $("<a/>", { "text": "Details" }).click(function() {
                        $(e.row).find("a:contains('Details')").click();
                        return false
                    })));
            if ($(e.row).find("a:contains('Delete')").length) Menu.append(
                $("<li/>").append(
                    $("<a/>", { "text": "Delete" }).click(function() {
                        $(e.row).find("a:contains('Delete')").click();
                        return false
                    })));
            Menu.append(
                $("<li/>").append(
                    $("<a/>", { "text": "Print" }).click(function() {
                        $(e.row).find(".print").parent().click();
                        return false
                    })));
            Menu.ContextMenu(Event);
        });
        $("[status]").mouseover(function() {
            $(this).closest(".window").Status($(this).attr("status"))
        }).mouseout(function() {
            $(this).closest(".window").Status("")
        });
    },
    WoundCareDeleteAsset: function(control, name, assetId) {
        if (confirm("Are you sure you want to delete this asset?")) {
            var input = "episodeId=" + $("#WoundCare_EpisodeId").val() + "&patientId=" + $("#WoundCare_PatientId").val() + "&eventId=" + $("#WoundCare_EventId").val() + "&name=" + name + "&assetId=" + assetId;
            U.PostUrl("/Schedule/DeleteWoundCareAsset", input, function(result) {
                if (result.isSuccessful) {
                    $(control).closest('td').empty().append("<input type=\"file\" name=\"WoundCare_" + name + "\" value=\"Upload\" size=\"13.75\" class = \"float-left upload-width\" />");
                }
            });
        }
    },
    DeleteScheduleEventAsset: function(control, patientId, episodeId, eventId, assetId) {
        if (confirm("Are you sure you want to delete this asset?")) {
            U.PostUrl("/Schedule/DeleteScheduleEventAsset", { patientId: patientId, episodeId: episodeId, eventId: eventId, assetId: assetId }, function(result) {
                if (result.isSuccessful) {
                    $(control).closest('span').remove();
                    $("#scheduleEvent_Assest_Count").html($("#scheduleEvent_Assest_Count").html() - 1);
                }
            });
        }
    },
    Rebind: function() {
        U.RebindGrid($('#ScheduleSelectionGrid'));
        U.RebindGrid($('#caseManagementGrid'));
    },
    AddSupply: function(control, episodeId, patientId, eventId, type, supplyId) {
        var quantity = $(control).closest('tr').find('.quantity').val();
        var date = $(control).closest('tr').find('.date').val();
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId + "&supplyId=" + supplyId + "&quantity=" + quantity + "&date=" + date;
        U.PostUrl("/Schedule/AddNoteSupply", input, function(result) {
            var gridfilter = $("#" + type + "_SupplyFilterGrid").data('tGrid');
            if (gridfilter != null) {
                $("#" + type + "_GenericSupplyDescription").val('');
                $("#" + type + "_GenericSupplyCode").val('');
                gridfilter.rebind({ q: "", limit: 0, type: "" });
            }
            var grid = $("#" + type + "_SupplyGrid").data('tGrid');
            if (grid != null) {
                grid.rebind({ episodeId: episodeId, patientId: patientId, eventId: eventId });
            }
        });
    },
    ShowScheduler: function() {
        if ($('#ScheduleTabStrip').is(':visible')) {
            $('#schedule_collapsed').find(".show-scheduler").html("Show Scheduler");
            $('#ScheduleTabStrip').hide();
            Schedule.showAll();
        } else {
            if ($("#Nursing_Tab").hasClass("t-state-active")) {
                Schedule.SetDiscipline('Nursing');
                var patientId = $("#SchedulePatientID").val();
                var episodeId = $("#ScheduleEpisodeID").val()
                Schedule.loadCalendarNavigation(episodeId, patientId);
                var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
            } else ($("#Nursing_Tab").click());
            if ($(".scheduleTables.purgable tbody tr").length == 0) Schedule.AllTableAdd();
            else {
                $('#ScheduleTabStrip').show();
                $('#schedule_collapsed').find(".show-scheduler").html("Hide Scheduler");
            }
        }
        Schedule.positionBottom();
    },
    newEpisodeStartDateOnChange: function(e) {
        Schedule.EpisodeStartDateChange("New")
    },
    editEpisodeStartDateOnChange: function(e) {
        Schedule.EpisodeStartDateChange("Edit")
    },
    EpisodeStartDateChange: function(prefix) {
        var startDate = $("#" + prefix + "_Episode_StartDate"), endDate = $("#" + prefix + "_Episode_EndDate");
        if (startDate.val()) {
            var newStartDate = new Date(startDate.val());
            var newEndDate = new Date(startDate.val());
            newEndDate.setDate(newStartDate.getDate() + 59);
            var month = newEndDate.getMonth() + 1, day = newEndDate.getDate(), year = newEndDate.getFullYear();
            endDate.val(month + "/" + day + "/" + year);
        }
    },
    SuppliesLoad: function() { $("#SkilledNurseVisit_SupplyGrid .t-grid-toolbar").html(unescape("%3Cdiv class=%22align-center%22%3E%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3ECurrent Supplies%3C/div%3E%3C/div%3E")); },
    showAll: function() {
        Schedule.SetDiscipline('all');
        var patientId = $("#SchedulePatientID").val();
        var episodeId = $("#ScheduleEpisodeID").val()
        Schedule.loadCalendarNavigation(episodeId, patientId);
        var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
    },
    PatientListDataBound: function() {
        if ($("#ScheduleSelectionGrid .t-grid-content tr").length) {
            if (Schedule._patientId == "") $('#ScheduleSelectionGrid .t-grid-content tr').eq(0).click();
            else $('td:contains(' + Schedule._patientId + ')', $('#ScheduleSelectionGrid')).closest('tr').click();
        } else $("#ScheduleMainResult").removeClass("loading").html(U.MessageWarn("No Patients Found", "No patients found that fit your search criteria. Please try again."));
        if ($("#ScheduleSelectionGrid .t-state-selected").length) $("#ScheduleSelectionGrid .t-grid-content").scrollTop($("#ScheduleSelectionGrid .t-state-selected").position().top - 50);
        Schedule.Filter($("#txtSearch_Schedule_Selection").val());
    },
    PhlebotomyLab: function(selector) {
        $(selector).Autocomplete({ source: ["CBC", "Chem 7", "BMP", "PT/INR", "PT", "Chem 8", "K+", "Urinalysis", "TSH", "CBC W/ diff", "Hemoglobin A1c", "Lipid Panel", "Comp Metabolic Panel  (CMP 14)", "TSH", "ALT/SGOT", "ALT/SGBOT", "Iron (Fe)"] })
    },
    reassignScheduleInit: function(identifier) {
        $("#reassign" + identifier + "Form").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Schedule.Rebind();
                            Patient.Rebind();
                            User.RebindScheduleList();
                            if (identifier == "All") { UserInterface.CloseWindow('schedulereassign'); } else { UserInterface.CloseModal(); }
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    loadEpisodeDropDown: function(dropDown, control) {
        U.PostUrl("/Schedule/EpisodeRangeList", { patientId: $(control).val() }, function(data) {
            var s = $("select#" + dropDown);
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select Episode --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Range, itemData.Id, false, false);
            });
        });
    },
    BulkUpdate: function(control) {
        var fields = $(":input", $(control)).serializeArray();
        U.PostUrl("Schedule/BulkUpdate", fields, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Rebind();
                Schedule.Rebind();
                Agency.loadCaseManagement($("#CaseManagement_GroupName").val());
                Agency.RebindPrintQueue();

            } else U.Growl(data.errorMessage, "error");
        });
    },
    LoadLog: function(patientId, eventId, task) {
        Acore.Open("schdeuleeventlogs", 'Schedule/ScheduleLogs', function() { }, { patientId: patientId, eventId: eventId, task: task });
    },
    GetPlanofCareUrl: function(episodeId, patientId, eventId) {
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId;
        U.PostUrl("/Oasis/GetPlanofCareUrl", input, function(result) {
            if (result.isSuccessful) U.GetAttachment(result.url, { 'episodeId': result.episodeId, 'patientId': result.patientId, 'eventId': result.eventId });
            else { U.Growl(result.errorMessage, "error"); }
        });
    },
    loadInactiveEpisodes: function(patientId) { Acore.Open("inactiveepisode", 'Schedule/Inactive', function() { }, { patientId: patientId }); },
    ActivateEpisode: function(episodeId, patientId) {
        U.PostUrl('Schedule/ActivateEpisode', { 'episodeId': episodeId, 'patientId': patientId }, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Rebind();
                Schedule.Rebind();
                Agency.RebindCaseManagement();
                User.RebindScheduleList();
                Schedule.ReLoadInactiveEpisodes(patientId);
            } else U.Growl(data.errorMessage, "error");
        });
    },
    ReLoadInactiveEpisodes: function(patientId) {
        $("#InactiveEpisodesContent ol").addClass('loading');
        $("#InactiveEpisodesContent").Load("Schedule/InactiveGrid", { patientId: patientId });
    },
    LoadEpisodeLog: function(episodeId, patientId) { Acore.Open("episodelogs", 'Schedule/EpisodeLogs', function() { }, { episodeId: episodeId, patientId: patientId }); }
}

