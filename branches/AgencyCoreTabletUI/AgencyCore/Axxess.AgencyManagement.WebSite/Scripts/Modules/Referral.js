﻿var Referral = {
    Delete: function(Id) { U.DeleteTemplate("Referral", Id); },
    InitEdit: function() {
        U.InitEditTemplate("Referral");
        $("#EditReferral_NewPhysician").click(function() {
            var PhysId = $("#EditReferral_PhysicianSelector").next("input[type=hidden]").val();
            $("#EditReferral_PhysicianSelector").AjaxAutocomplete("reset");
            if (U.IsGuid(PhysId)) Patient.AddPhysician(PhysId, $("#Edit_Referral_Id").val());
            else U.Growl("Please Select a Physician", "error");
        });
    },
    InitNew: function() {
        Forms.ShowIfChecked($("#New_Referral_DMEOther"), $("#New_Referral_OtherDME"));
        U.InitNewTemplate("Referral");
    },
    InitNonAdmit: function() {
        $("#NonAdmit_Referral_Date").datepicker("hide");
        $("#newNonAdmitReferralForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl("Referral non-admission successful.", "success");
                            Referral.RebindList();
                            UserInterface.CloseModal();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    RebindList: function() { U.RebindGrid($('#List_Referral')); },
    Admit: function(id) {
        Acore.Open("newpatient", 'Patient/New', function() { Patient.InitNew(); }, { referralId: id });
    },
    AddPhysician: function(id, referralId) {
        U.RebindAjaxGrid("/Referral/AddPhysician", { "id": id, "referralId": referralId }, $('#EditReferral_PhysicianGrid'));
        $("#EditReferral_PhysicianSelector").AjaxAutocomplete("reset").next("input[type=hidden]").val("");
    },
    DeletePhysician: function(id, referralId) {
        if (confirm("Are you sure you want to delete")) U.RebindAjaxGrid("/Referral/DeletePhysician", { "id": id, "referralId": referralId }, $('#EditReferral_PhysicianGrid'));
    },
    SetPrimaryPhysician: function(id, referralId) {
        if (confirm("Are you sure you want to set this physician as primary?")) U.RebindAjaxGrid("/Referral/SetPrimaryPhysician", { "id": id, "referralId": referralId }, $('#EditReferral_PhysicianGrid'));
    }
}