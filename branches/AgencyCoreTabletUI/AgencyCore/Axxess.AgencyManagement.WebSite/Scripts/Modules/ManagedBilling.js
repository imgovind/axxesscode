﻿var ManagedBilling = {
    _patientId: "",
    _ClaimId: "",
    _patientRowIndex: 0,
    _sort: {},
    BillingHandler: function(index, form) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) { if (result.isSuccessful) { var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); var $link = $(item).find('.t-link'); var contentUrl = $link.data('ContentUrl'); if (contentUrl != null) { ManagedBilling.LoadContent(contentUrl, $("#ManagedClaimTabStrip").find("div.t-content.t-state-active")); } ManagedBilling.RebindActivity(ManagedBilling._patientId); } else U.Growl(result.errorMessage, 'error'); },
            error: function() { }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    managedClaimTabStripOnSelect: function(e) { ManagedBilling.LoadContent($(e.item).find('.t-link').data('ContentUrl'), e.contentElement); },
    InitCenter: function() {
        $("#txtSearch_managedBilling_Selection").keyup(function() { $('#managedBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#ManagedBillingActivityGrid').find('.t-grid-content tbody').empty(); ManagedBilling._patientId = ""; ManagedBilling.RebindPatientList(); });
        $("select.managedBillingBranchCode").change(function() { $('#managedBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#ManagedBillingActivityGrid').find('.t-grid-content tbody').empty(); ManagedBilling._patientId = ""; ManagedBilling.RebindPatientList(); });
        $("select.managedBillingStatusDropDown").change(function() { $('#managedBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#ManagedBillingActivityGrid').find('.t-grid-content tbody').empty(); ManagedBilling._patientId = ""; ManagedBilling.RebindPatientList(); });
        $("select.managedBillingInsuranceDropDown").change(function() { $('#managedBillingClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#ManagedBillingActivityGrid').find('.t-grid-content tbody').empty(); ManagedBilling._patientId = ""; ManagedBilling.RebindPatientList(); });
    },
    ReLoadUnProcessedManagedClaim: function(branchId, insuranceId) {
        $("#Billing_ManagedClaimCenterContent").Load("Billing/ManagedGrid", { branchId: branchId, insuranceId: insuranceId })
    },
    SubmitClaimDirectly: function(control) {
        U.PostUrl('Billing/SubmitManagedClaimDirectly', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimsummary');
                ManagedBilling.ReLoadUnProcessedManagedClaim($('#Billing_ManagedClaimCenterBranchCode').val(), $('#Billing_ManagedClaimCenterPrimaryInsurance').val());
                U.Growl(result.errorMessage, "success")
            } else U.Growl(result.errorMessage, "error");
        }, null)
    },
    LoadGeneratedManagedClaim: function(control) {
        U.PostUrl("/Billing/CreateManagedANSI", $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) U.PostUrl("/Billing/Generate", { ansiId: result.Id, });
            else U.Growl(result.errorMessage, "error")
        }, function(result) { })
    },
    LoadGeneratedSingleManagedClaim: function(id) {
        U.PostUrl("/Billing/CreateSingleManagedANSI", { Id: id }, function(result) {
            if (result != null && result.isSuccessful) U.PostUrl("/Billing/Generate", { ansiId: result.Id });
            else U.Growl(result.errorMessage, "error")
        }, function(result) { })
    },
    UpdateStatus: function(control) {
        U.PostUrl('Billing/SubmitManagedClaims', $(":input", $(control)).serializeArray(), function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimsummary');
                ManagedBilling.ReLoadUnProcessedManagedClaim($('#Billing_ManagedClaimCenterBranchCode').val(),$('#Billing_ManagedClaimCenterPrimaryInsurance').val());
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null)
    },
    RebindPatientList: function() {
        var patientGrid = $('#ManagedBillingSelectionGrid').data('tGrid');
        if (patientGrid != null) patientGrid.rebind({ branchId: $("select.managedBillingBranchCode").val(), statusId: $("select.managedBillingStatusDropDown").val(), insurnace: $("select.managedBillingInsuranceDropDown").val(), name: $("#txtSearch_managedBilling_Selection").val() })
    },
    HideOrShowNewClaimMenu: function() {
        var gridTr = $('#ManagedBillingSelectionGrid').find('.t-grid-content tbody tr');
        if (gridTr != undefined && gridTr != null) {
            if (gridTr.length > 0) $("#managedBillingTopMenu").show();
            else $("#managedBillingTopMenu").hide();
        }
    },
    LoadContent: function(contentUrl, contentElement) {
        if (contentUrl) $(contentElement).Load(contentUrl, {})
    },
    Navigate: function(index, id, patientId) {
        $(id).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) { if (result.isSuccessful) { var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); var $link = $(item).find('.t-link'); var contentUrl = $link.data('ContentUrl'); if (contentUrl != null) { ManagedBilling.LoadContent(contentUrl, $("#ManagedClaimTabStrip").find("div.t-content.t-state-active")); } ManagedBilling.RebindActivity(patientId); ManagedBilling.ReLoadUnProcessedManagedClaim($("#Billing_ManagedClaimCenterBranchCode").val(), $("#Billing_ManagedClaimCenterPrimaryInsurance").val()); } else U.Growl(result.errorMessage, 'error'); },
                    error: function() { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    NavigateBack: function(index) {
        var tabstrip = $("#ManagedClaimTabStrip").data("tTabStrip");
        var item = $("li", tabstrip.element)[index];
        tabstrip.select(item)
    },
    loadGenerate: function(control) {
        Acore.Open("managedclaimsummary", 'Billing/ManagedClaimSummary', function() { }, $(":input", $(control)).serializeArray())
    },
    GenerateAllCompleted: function(control) {
        $("input[name=ManagedClaimSelected]", $(control)).each(function() {
            $(this).attr("checked", true)
        });
        ManagedBilling.loadGenerate(control)
    },
    NoPatientBind: function(id) {
        ManagedBilling.RebindActivity(id)
    },
    PatientListDataBound: function() {
        if ($("#ManagedBillingSelectionGrid .t-grid-content tr").length) {
            if (ManagedBilling._patientId == "") $('#ManagedBillingSelectionGrid .t-grid-content tr').eq(0).click();
            else $('td:contains(' + ManagedBilling._patientId + ')', $('#ManagedBillingSelectionGrid')).closest('tr').click();
            $("#managedBillingTopMenu").show();
        } else {
            $("#managedBillingClaimData").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
            $("#managedBillingTopMenu").hide();
        }
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            var patientId = e.row.cells[2].innerHTML;
            ManagedBilling._patientId = patientId;
            ManagedBilling._ClaimId = "";
            ManagedBilling.RebindActivity(patientId);
        }
        $("#managedBillingClaimData").empty();
    },
    RebindActivity: function(id) {
        var activityGrid = $('#ManagedBillingActivityGrid').data('tGrid');
        if (activityGrid != null) {
            activityGrid.rebind({ patientId: id, insuranceId: $("#ManagedBillingHistory_InsuranceId").val() });
            if ($('#ManagedBillingActivityGrid').find('.t-grid-content tbody').is(':empty')) $("#managedBillingClaimData").empty();
        }
    },
    ManagedComplete: function(id, patientId) {
        U.PostUrl('Billing/ManagedComplete', { id: id, patientId: patientId }, function(result) {
            if (result.isSuccessful) {
                UserInterface.CloseWindow('managedclaimedit');
                U.Growl(result.errorMessage, "success");
            } else U.Growl(result.errorMessage, "error");
        }, null)
    },
    InitClaim: function() {
        U.InitTemplate($("#updateManagedClaimForm"), function() { UserInterface.CloseModal(); ManagedBilling.RebindActivity(ManagedBilling._patientId); }, "Claim update is successfully.")
    },
    InitNewClaim: function() {
        U.InitTemplate($("#createManagedClaimForm"), function() { UserInterface.CloseModal(); ManagedBilling.RebindActivity(ManagedBilling._patientId); }, "Claim is created successfully.")
    },
    DeleteClaim: function(patientId, id) {
        if (confirm("Are you sure you want to delete this claim?")) {
            U.PostUrl("/Billing/DeleteManagedClaim", { patientId: patientId, id: id }, function(result) {
                if (result.isSuccessful) {
                    ManagedBilling.RebindActivity(patientId);
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error")
            })
        }
    },
    OnClaimRowSelected: function(e) {
        if (e.row.cells[12] != undefined && ManagedBilling._patientId != undefined && ManagedBilling._patientId != "") {
            var claimId = e.row.cells[12].innerHTML;
            ManagedBilling._ClaimId = claimId;
            ManagedBilling.loadClaimInfo(ManagedBilling._patientId, claimId)
        }
    },
    OnCliamDataBound: function() {
        if ($("#ManagedBillingActivityGrid .t-grid-content tr").length) $('#ManagedBillingActivityGrid .t-grid-content tbody tr').eq(0).click();
        else $("#managedBillingClaimData").empty();
    },
    loadClaimInfo: function(patientId, claimId) {
        $("#managedBillingClaimData").Load("Billing/ManagedSnapShotClaimInfo", { patientId: patientId, claimId: claimId })
    }
}