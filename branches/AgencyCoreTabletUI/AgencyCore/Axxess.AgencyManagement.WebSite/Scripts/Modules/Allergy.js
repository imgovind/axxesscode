﻿var Allergy = {
    assessmentType: "",
    Add: function(allergyProfileId, assessmentType) {
        Acore.Modal({
            "Name": "Add New Allergy",
            "Url": "Patient/NewAllergy",
            "Input": { allergyProfileId: allergyProfileId },
            "OnLoad": function() { Allergy.InitNew(assessmentType) },
            "Width": "650px"
        })
    },
    Delete: function(allergyProfileId, allergyId, assessmentType) {
        if (confirm("Are you sure you want to delete this allergy?")) {
            U.PostUrl('Patient/UpdateAllergyStatus', { allergyProfileId: allergyProfileId, allergyId: allergyId, isDeprecated: true }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Allergy.Refresh(allergyProfileId);
                } else U.Growl(result.errorMessage, "error");
            })
        }
    },
    Edit: function(allergyProfileId, allergyId, assessmentType) {
        Acore.Modal({
            "Name": "Add New Allergy",
            "Url": "Patient/EditAllergy",
            "Input": { allergyProfileId: allergyProfileId, allergyId: allergyId },
            "OnLoad": function() { Allergy.InitEdit(assessmentType) },
            "Width": "650px"
        })
    },
    InitAutocomplete: function() {
        $("input[name=Type]").Autocomplete({
            source: [
                "Medication",
                "Food",
                "Animals",
                "Plants",
                "Latex",
                "Environmental"
            ]
        })
    },
    InitNew: function(assessmentType) {
        if (assessmentType != undefined) this.assessmentType = assessmentType;
        this.InitAutocomplete();
        $("#newAllergyForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if (result.isSuccessful) {
                            var allergyProfileId = $("#New_Allergy_ProfileId").val();
                            Allergy.Refresh(allergyProfileId);
                            U.Growl(result.errorMessage, "success");
                            if ($("#New_Allergy_AddAnother").val().length > 0) {
                                $("#newAllergyForm").clearForm();
                                $("#New_Allergy_StartDate").removeAttr("disabled");
                                $("#New_Allergy_ProfileId").val(allergyProfileId);
                            } else {
                                $("#window_DialogWindow").Close();
                            }
                            //if ($("#New_Allergy_AddAnother").val() == "AddAnother") $("#newAllergyForm").clearForm().find("#New_Allergy_ProfileId").val(allergyProfileId);
                            //else $("#window_DialogWindow").Close();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    InitEdit: function(assessmentType) {
        if (assessmentType != undefined) this.assessmentType = assessmentType;
        this.InitAutocomplete();
        $("#editAllergyForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if (result.isSuccessful) {
                            var allergyProfileId = $("#Edit_Allergy_ProfileId").val();
                            Allergy.Refresh(allergyProfileId);
                            U.Growl(result.errorMessage, "success");
                            $("#window_DialogWindow").Close();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    Refresh: function(allergyProfileId, prefix) {
        if (prefix == undefined && this.assessmentType.length) prefix = this.assessmentType
        else if (prefix == undefined) prefix = "AllergyProfile";
        $("#" + prefix + "_list").Load((prefix == "AllergyProfile" ? "Patient/Allergies" : "Patient/AllergiesForOasis"), (prefix == "AllergyProfile" ? { "allergyProfileId": allergyProfileId} : { "allergyProfileId": allergyProfileId, "assessmentType": prefix }))
    },
    Restore: function(allergyProfileId, allergyId, assessmentType) {
        if (confirm("Are you sure you want to restore this allergy?")) {
            U.PostUrl('Patient/UpdateAllergyStatus', { allergyProfileId: allergyProfileId, allergyId: allergyId, isDeprecated: false }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Allergy.Refresh(allergyProfileId);
                } else U.Growl(result.errorMessage, "error");
            })
        }
    }
}