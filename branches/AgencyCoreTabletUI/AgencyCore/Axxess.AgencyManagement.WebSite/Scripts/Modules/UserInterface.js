﻿var UserInterface = {
    ShowNoteModal: function(Text, Color) {
        if (Color != "red" && Color != "blue") Color = "yellow";
        $("body").append(unescape("%3Cdiv id=%22shade%22%3E%3C/div%3E%3Cdiv class=%22note-modal bottom " + Color + "%22%3E%3C/div%3E%3Cdiv class=%22not" +
            "e_modal middle " + Color + "%22%3E%3C/div%3E%3Cdiv class=%22note-modal top " + Color + "%22%3E%3Cdiv id=%22note_close%22%3EX%3C/div%3E%3Cp" +
            "%3E" + Text + "%3C/p%3E%3C/div%3E"));
        $("#note_close").click(function() { $("#shade").remove(); $(".note-modal").remove() });
    },
    ShowScheduleCenter: function(Id) {
        if (Id != undefined) Schedule._patientId = Id;
        if (Acore.Windows.schedulecenter.IsOpen) {
            $("#window_schedulecenter").WinFocus();
            Schedule.Rebind();
        } else Acore.Open("schedulecenter", "Schedule/Center", function() { Schedule.InitCenter(Id); });
    },
    ShowMessages: function() {
        Acore.Open("messageinbox", 'Message/Inbox', function() { Message.Init(); });
    },
    ShowNewMessage: function() {
        Acore.Open("newmessage", 'Message/New', function() { Message.InitNew(); });
    },
    ShowUserTasks: function() {
        Acore.Open("listuserschedule", 'User/Schedule', function() { });
    },
    ShowPastDueRecerts: function() {
        Acore.Open("listpastduerecerts", 'Agency/RecertsPastDueGrid', function() { });
    },
    ShowUpcomingRecerts: function() {
        Acore.Open("listupcomingrecerts", 'Agency/RecertsUpcomingGrid', function() { });
    },
    ShowPatientCenter: function() {
        Acore.Open("patientcenter", 'Patient/Center', function() { Patient.InitCenter(); });
    },
    ShowNewPatient: function() {
        Acore.Open("newpatient", 'Patient/New', function() { Patient.InitNew(); });
    },
    ShowEditPatient: function(Id) {
        Acore.Open("editpatient", 'Patient/EditPatientContent', function() { Patient.InitEdit(); }, { patientId: Id });
    },
    ShowNewReferral: function() {
        Acore.Open("newreferral", 'Referral/New', function() { Referral.InitNew(); });
    },
    ShowEditReferral: function(Id) {
        Acore.Open("editreferral", 'Referral/Edit', function() { Referral.InitEdit(); }, { Id: Id });
    },
    ShowNewContact: function() {
        Acore.Open("newcontact", 'Contact/New', function() { Contact.InitNew(); });
    },
    ShowEditContact: function(Id) {
        Acore.Open("editcontact", "Contact/Edit", function() { Contact.InitEdit(); }, { Id: Id });
    },
    ShowNewLocation: function() {
        Acore.Open("newlocation", 'Location/New', function() { Location.InitNew(); });
    },
    ShowEditLocation: function(Id) {
        Acore.Open("editlocation", 'Location/Edit', function() { Location.InitEdit(); }, { Id: Id });
    },
    ShowNewPhysician: function() {
        Acore.Open("newphysician", 'Physician/New', function() { Physician.InitNew(); });
    },
    ShowEditPhysician: function(Id) {
        Acore.Open("editphysician", 'Physician/Edit', function() { Physician.InitEdit(); }, { Id: Id });
    },
    ShowNewHospital: function() {
        Acore.Open("newhospital", "Hospital/New", function() { Hospital.InitNew(); });
    },
    ShowEditHospital: function(Id) {
        Acore.Open("edithospital", "Hospital/Edit", function() { Hospital.InitEdit(); }, { Id: Id });
    },
    ShowNewInsurance: function() {
        Acore.Open("newinsurance", "Insurance/New", function() { Insurance.InitNew(); });
    },
    ShowEditInsurance: function(Id) {
        Acore.Open("editinsurance", "Insurance/Edit", function() { Insurance.InitEdit(); }, { Id: Id });
    },
    ShowNewTemplate: function() {
        Acore.Open("newtemplate", 'Template/New', function() { Template.InitNew(); });
    },
    ShowEditTemplate: function(id) {
        Acore.Open("edittemplate", "Template/Edit", function() { Template.InitEdit(); }, { Id: id });
    },
    ShowRap: function(episodeId, patientId) {
        Acore.Open("rap", "Billing/Rap", function() { Billing.InitRap(); }, { episodeId: episodeId, patientId: patientId });
    },
    ShowFinal: function(episodeId, patientId) {
        Acore.Open("final", "Billing/Final", function() { }, { episodeId: episodeId, patientId: patientId });
    },
    ShowEditPlanofCare: function(episodeId, patientId, eventId) {
        Acore.Open("editplanofcare", 'Oasis/Edit485', function() { PlanOfCare.InitEdit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ShowEditPlanofCareStandAlone: function(episodeId, patientId, eventId) {
        Acore.Open("newplanofcare", 'Oasis/New485', function() { PlanOfCare.InitNew(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ShowNewUser: function() {
        Acore.Open("newuser", "User/New", function() { User.InitNew(); });
    },
    ShowEditUser: function(Id) {
        Acore.Open("edituser", "User/Edit", function() { User.InitEdit(); }, { Id: Id });
    },
    ShowEditOrder: function(id, patientId) {
        Acore.Open("editorder", "Order/Edit", function() { Patient.InitEditOrder(); }, { id: id, patientId: patientId });
    },
    ShowReportCenter: function() {
        Acore.Open("reportcenter", 'Report/Center', function() { Report.Init(); });
    },
    ShowBillingCenter: function() {
        Acore.Open("billingcenter", "Billing/Center", function() { Billing.InitCenter(); });
    },
    ShowPatientPrompt: function() {
        Acore.Modal({
            Name: "Next Step",
            Url: "Patient/NextStep",
            Width: "350px"
        })
    },
    ShowNewIncidentReport: function() {
        Acore.Open("newincidentreport", 'Incident/New', function() { IncidentReport.InitNew(); });
    },
    ShowEditIncident: function(Id) {
        Acore.Open("editincidentreport", 'Incident/Edit', function() { IncidentReport.InitEdit(); }, { Id: Id });
    },
    ShowNewInfectionReport: function() {
        Acore.Open("newinfectionreport", 'Infection/New', function() { InfectionReport.InitNew(); });
    },
    ShowEditInfection: function(Id) {
        Acore.Open("editinfectionreport", 'Infection/Edit', function() { InfectionReport.InitEdit(); }, { Id: Id });
    },
    ShowPatientBirthdays: function() {
        Acore.Open("reportcenter", 'Report/Center', function() { Report.Init(); Report.Show(1, '#patient_reports', '/Report/Patient/Birthdays'); return false; });
    },
    ShowPatientAuthorizations: function(patientId) {
        Acore.Open("listauthorizations", 'Authorization/Grid', function() { }, { patientId: patientId });
    },
    ShowDeletedTaskHistory: function(patientId) {
        Acore.Open("patientdeletedtaskhistory", 'Patient/DeletedTaskHistory', function() { }, { patientId: patientId });
    },
    ShowPatientOrdersHistory: function(patientId) {
        Acore.Open("patientordershistory", 'Patient/OrdersHistory', function() { }, { patientId: patientId });
    },
    ShowPatientSixtyDaySummary: function(patientId) {
        Acore.Open("patientsixtydaysummary", 'Patient/SixtyDaySummary', function() { }, { patientId: patientId });
    },
    ShowPatientVitalSigns: function(patientId) {
        Acore.Open("patientvitalsigns", 'Patient/VitalSigns', function() { }, { patientId: patientId });
    },
    ShowMedicareEligibilityReports: function(patientId) {
        Acore.Open("medicareeligibilitylist", 'Patient/MedicareEligibilityList', function() { }, { patientId: patientId });
    },
    ShowPatientAllergies: function(patientId) {
        Acore.Open("allergyprofile", 'Patient/AllergyProfile', function() { }, { patientId: patientId });
    },
    ShowPatientHospitalizationLogs: function(Id) {
        Acore.Open("patienthospitalizationlogs", 'Patient/HospitalizationLogs', function() { }, { patientId: Id });
    },
    RefreshPlanofCare: function(episodeId, patientId, eventId) {
        $("#planofCareContentId").Load("Oasis/PlanofCareContent", { episodeId: episodeId, patientId: patientId, eventId: eventId })
    },
    ShowAgencySelectionModal: function(loginId) {
        $("#Agency_Selection_Container").load("/User/Agencies", { loginId: loginId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == "success") U.showDialog("#Agency_Selection_Container", function() { User.InitChangeSignature() })
        })
    },
    ShowEpisodeOrders: function(episodeId, patientId) {
        Acore.Open("patientepisodeorders", 'Patient/EpisodeOrdersView', function() { }, { episodeId: episodeId, patientId: patientId });
    },
    ShowPatientEligibility: function(medicareNumber, lastName, firstName, dob, gender) {
        if (medicareNumber == "" || lastName == "" || firstName == "" || dob == "" || gender == undefined) {
            var error = "Unable to process Medicare eligibility request:";
            if (medicareNumber == "") error += "<br /> &#8226; Medicare Number is Required";
            if (lastName == "") error += "<br /> &#8226; Last Name is Required";
            if (firstName == "") error += "<br /> &#8226; First Name is Required";
            if (dob == "") error += "<br /> &#8226; Date of Birth is Required";
            if (gender == undefined) error += "<br /> &#8226; Gender is Required";
            U.Growl(error, "error");
        } else {
            Acore.Modal({
                Name: "Medicare Eligibility",
                Url: "Patient/Verify",
                Input: {
                    medicareNumber: medicareNumber,
                    lastName: lastName,
                    firstName: firstName,
                    dob: dob,
                    gender: gender
                },
                Width: "800px"
            })
        }
    },
    ShowMedicareEligibility: function(medEligibilityId, patientId) {
        Acore.Modal({
            Name: "Medicare Eligibility",
            Url: "Patient/MedicareEligibility",
            Input: {
                medicareEligibilityId: medEligibilityId,
                patientId: patientId
            },
            Width: "800px"
        })
    },
    ShowMissedVisitModal: function(episodeId, patientId, eventId) {
        Acore.Modal({
            Name: "New Physician",
            Url: "Visit/Miss",
            Input: { episodeId: episodeId, patientId: patientId, eventId: eventId },
            OnLoad: User.InitMissedVisit,
            Width: "800px"
        })
    },
    ShowNewPhysicianModal: function() {
        Acore.Modal({
            Name: "New Physician",
            Url: "Physician/New",
            OnLoad: function() { Physician.InitNew(true) },
            Width: "900px"
        })
    },
    ShowNewPhotoModal: function(patientId) {
        Acore.Modal({
            Name: "New Photo",
            Url: "Patient/NewPhoto",
            Input: { patientId: patientId },
            OnLoad: Patient.InitNewPhoto,
            Width: "315px"
        })
    },
    ShowMultipleDayScheduleModal: function(episodeId, patientId) {
        Acore.Modal({
            Name: "Quick Employee Scheduler",
            Url: "Schedule/MultiDay",
            Input: { episodeId: episodeId, patientId: patientId },
            OnLoad: Schedule.InitMultiDayScheduler,
            Width: "900px"
        })
    },
    ShowNewEpisodeModal: function(Id) {
        Acore.Modal({
            Name: "New Episode",
            Url: "Schedule/NewEpisode",
            Input: { patientId: Id },
            OnLoad: Schedule.InitNewEpisode,
            Width: "800px"
        })
    },
    ShowEditEpisodeModal: function(episodeId, patientId) {
        Acore.Modal({
            Name: "Edit Episode",
            Url: "Schedule/EditEpisode",
            Input: { episodeId: episodeId, patientId: patientId },
            OnLoad: Schedule.InitEpisode,
            Width: "800px"
        })
    },
    ShowAdmitPatientModal: function(id, type) {
        Acore.Modal({
            Name: "Admit Patient",
            Url: "Patient/NewAdmit",
            Input: { id: id, type: type },
            OnLoad: Patient.InitAdmit,
            Width: "850px"
        })
    },
    ShowNonAdmitPatientModal: function(Id) {
        Acore.Modal({
            Name: "Non-Admit Patient",
            Url: "Patient/NewNonAdmit",
            Input: { patientId: Id },
            OnLoad: Patient.InitNonAdmit,
            Width: "800px"
        })
    },
    ShowNonAdmitReferralModal: function(Id) {
        Acore.Modal({
            Name: "Non-Admit Referral",
            Url: "Referral/NewNonAdmit",
            Input: { referralId: Id },
            OnLoad: Referral.InitNonAdmit,
            Width: "800px"
        })
    },
    ShowMedicationModal: function(patientId, isnew) {
        Acore.Modal({
            Name: "Medications",
            Url: "485/Medication",
            Input: { patientId: patientId },
            OnLoad: function() {
                $("#PlanofCareMedicationIsNew").val(isnew);
                $("window_DialogWindow_content").css("overflow", "hidden");
                $('#MedicationGrid485 .t-grid-content').css("top", "60px")
            },
            Width: "800px"
        })
    },
    ShowOasisValidationModal: function(Id, patientId, episodeId, assessmentType) {
        OasisValidation.Validate(Id, patientId, episodeId, assessmentType);
    },
    ShowModalEditClaim: function(patientId, Id, type) {
        Acore.Modal({
            Name: "Edit Claim",
            Url: "Billing/Update",
            Input: { patientId: patientId, id: Id, type: type },
            OnLoad: Billing.InitClaim,
            Width: "800px"
        })
    },
    ShowNewClaimModal: function(type) {
        Acore.Modal({
            Name: "New Claim",
            Url: "Billing/NewClaim",
            Input: { patientId: Billing._patientId, type: type },
            OnLoad: Billing.InitNewClaim,
            Width: "500px"
        })
    },
    ShowChangeStatusModal: function(Id) {
        Acore.Modal({
            Name: "Change Status",
            Url: "Patient/Status",
            Input: { patientId: Id },
            OnLoad: Patient.InitChangePatientStatus,
            Width: "800px"
        })
    },
    ShowReadmitPatientModal: function(Id) {
        Acore.Modal({
            Name: "Patient Re-admit",
            Url: "Patient/Readmit",
            Input: { patientId: Id },
            OnLoad: Patient.InitPatientReadmitted,
            Width: "800px"
        })
    },
    ShowNewManagedClaimModal: function() {
        Acore.Modal({
            Name: "New Managed Claim",
            Url: "Billing/NewManagedClaim",
            Input: { patientId: ManagedBilling._patientId },
            OnLoad: ManagedBilling.InitNewClaim,
            Width: "700px"
        })
    },
    ShowModalUpdateStatusManagedClaim: function(patientId, Id) {
        Acore.Modal({
            Name: "Update Status of Managed Claim",
            Url: "Billing/UpdateManagedClaim",
            Input: { patientId: patientId, id: Id },
            OnLoad: ManagedBilling.InitClaim,
            Width: "800px"
        })
    },
    ShowManagedClaim: function(Id, patientId) {
        Acore.Open("managedclaimedit", "Billing/ManagedClaim", function() { }, { Id: Id, patientId: patientId });
    },
    ShowMultipleReassignModal: function(episodeId, patientId, type) {
        Acore.Modal({
            Name: "Reasign Multiple Tasks",
            Url: "Schedule/ReAssignSchedulesContent",
            Input: { episodeId: episodeId, patientId: patientId, type: type },
            OnLoad: function() { Schedule.reassignScheduleInit(type) },
            Width: "500px"
        })
    },
    ShowCorrectionNumberModal: function(Id, patientId, episodeId, type, correctionNumber) {
        Acore.Modal({
            Name: "Correction Number",
            Url: "Oasis/Correction",
            Input: { Id: Id, PatientId: patientId, EpisodeId: episodeId, Type: type, CorrectionNumber: correctionNumber },
            OnLoad: Oasis.InitEditCorrectionNumber,
            Width: "675px"
        })
    },
    ShowRemittanceDetail: function(Id) {
        Acore.Open("remittancedetail", 'Billing/RemittanceDetail', function() { }, { Id: Id });
    },
    ShowOrdersHistoryModal: function(id, patientId, type) {
        Acore.Modal({
            Name: "Orders History",
            Url: "Agency/OrderHistoryEdit",
            Input: { id: id, patientId: patientId, type: type },
            OnLoad: Agency.OrderHistoryEditInit,
            Width: "500px"
        })
    },
    Refresh: function() {
        Patient.Rebind();
        Schedule.Rebind();
    },
    CloseModal: function() {
        if (Acore.Windows.DialogWindow != null && Acore.Windows.DialogWindow.IsOpen) $("#window_DialogWindow").Close();
    },
    CloseAndRefresh: function(window) {
        Patient.Rebind();
        Schedule.Rebind();
        Agency.RebindCaseManagement();
        User.RebindScheduleList();
        $("#window_" + window).Close();
    },
    CloseWindow: function(window) {
        if (Acore.Windows[window].IsOpen) $("#window_" + window).Close();
    }
}