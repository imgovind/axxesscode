﻿var Template = {
    Delete: function(id) { U.DeleteTemplate("Template", id) },
    InitEdit: function() { U.InitEditTemplate("Template") },
    InitNew: function() { U.InitNewTemplate("Template") },
    RebindList: function() { U.RebindGrid($('#List_Template')) }
}