﻿var Lookup = {
    _Users: "",
    _Discipline: new Array(),
    load: function(u, p, v, n, a) {
        U.PostUrl(u, v, function(data) {
            $(p).each(function() {
                var s = this;
                $(this).children('option').remove();
                if (n != undefined) s[0] = new Option("-- Select " + n + " --", "", false, false);
                if (a) s[1] = new Option("** Add New " + n + " **", "new", false, false);
                $.each(data, function(index, itemData) {
                    s[s.options.length] = new Option(
                            (n == "Admission Source" ? "(" + itemData.Code + ") - " + itemData.Description : "") +
                            (n == "Task" ? itemData.Task : "") +
                            (n == "Physician" || n == "User" ? itemData.DisplayName : "") +
                            (n == "Race" || n == "Referral Source" ? itemData.Description : "") +
                            (n == "Insurance" || n == "State" ? itemData.Name : ""),
                            (n == "State" || n == "Admission Source" ? itemData.Code : itemData.Id), false, false);
                    if (n == "Task") $("option:last", s).attr("data", itemData.Discipline).attr("IsBillable", itemData.IsBillable);
                });
            });
        });
    },
    loadAdmissionSources: function() { Lookup.load("/LookUp/AdmissionSources", "select.AdmissionSource", null, "Admission Source"); },
    loadDiscipline: function(control, input) { Lookup.load("/LookUp/DisciplineTasks", control + " select.DisciplineTask", "Discipline=" + input, "Task"); },
    loadInsurance: function() { Lookup.load("/Agency/GetInsurances", "select.Insurances", null, "Insurance", true); },
    loadMultipleDisciplines: function(control) { Lookup.load("/LookUp/MultipleDisciplineTasks", control + " select.MultipleDisciplineTask", null, "Task"); },
    loadPhysicians: function() { Lookup.load("/Agency/GetPhysicians", "select.Physicians", null, "Physician", true); },
    loadRaces: function() { Lookup.load("/LookUp/Races", "select.EthnicRaces", null, "Race"); },
    loadReferralSources: function() { Lookup.load("/LookUp/ReferralSources", "select.ReferralSources", null, "Referral Source"); },
    loadStates: function() { Lookup.load("/LookUp/States", "select.States", null, "State"); },
    loadUsers: function(control) { Lookup.load("/User/All", control + " select.Users", null, "User"); },
    SetDiscipline: function(name, discipline) { Lookup._Discipline[name] = discipline; },
    language: function(selector) { $(selector).Autocomplete({ source: ["Spanish", "Chinese", "French", "German", "Filipino", "Vietnamese", "Italian", "English"] }) }
}