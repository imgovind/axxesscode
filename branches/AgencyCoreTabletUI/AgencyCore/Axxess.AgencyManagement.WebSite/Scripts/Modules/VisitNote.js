﻿var V = {
    CoordinationOfCare: {
        Init: function() { V.TransferSummary.SharedInit("CoordinationOfCare") },
        Load: function(episodeId, patientId, eventId) { Acore.Open("CoordinationOfCare", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    COTAVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("COTAVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    DischargeSummary: {
        Init: function() {
            Forms.ShowIfRadioEquals("DischargeSummary_IsNotificationDC", "1", $("#DischargeSummary_NotificationDate").parent());
            Forms.ShowIfSelectEquals($("#DischargeSummary_NotificationDate"), "3", $("#DischargeSummary_NotificationDateOther"));
            Forms.ShowIfChecked($("#DischargeSummary_ServiceProvidedOther"), $("#DischargeSummary_ServiceProvidedOtherValue"));
            Forms.ShowIfChecked($("#DischargeSummary_DischargeInstructionsGivenTo4"), $("#DischargeSummary_DischargeInstructionsGivenToOther"));
        },
        Load: function(episodeId, patientId, eventId) { Acore.Open("DischargeSummary", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    DriverOrTransportationNote: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("DriverOrTransportationNote", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    HHAideCarePlan: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("HHAideCarePlan", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    HHAideSupervisoryVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("HHAideSupervisoryVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    HHAideVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("HHAideVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    LVNSupervisoryVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("lvnSupVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    MSWAssessment: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("MSWAssessment", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    MSWDischarge: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("MSWDischarge", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    MSWEvaluationAssessment: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("MSWEvaluationAssessment", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    MSWProgressNote: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("MSWProgressNote", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    MSWVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("MSWVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    OTDischarge: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("OTDischarge", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    OTEvaluation: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("OTEvaluation", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    OTMaintenance: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("OTMaintenance", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    OTReEvaluation: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("OTReEvaluation", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    OTVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("OTVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    PASCarePlan: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("PASCarePlan", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    PASVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("PASVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    PTAVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("PTAVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    PTDischarge: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("PTDischarge", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    PTEvaluation: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("PTEvaluation", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    PTMaintenance: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("PTMaintenance", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    PTReEvaluation: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("PTReEvaluation", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    PTVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("PTVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    SixtyDaySummary: {
        Init: function() {
            Forms.ShowIfRadioEquals("SixtyDaySummary_DNR", "1", $("#SixtyDaySummary_NotificationDate"));
            Forms.ShowIfSelectEquals($("#SixtyDaySummary_NotificationDate"), "3", $("#SixtyDaySummary_NotificationDateOther"));
            Forms.ShowIfChecked($("#SixtyDaySummary_HomeboundStatusOtherCheck"), $("#SixtyDaySummary_HomeboundStatusOther"));
            Forms.ShowIfChecked($("#SixtyDaySummary_ServiceProvidedOther"), $("#SixtyDaySummary_ServiceProvidedOtherValue"));
            Forms.ShowIfChecked($("#SixtyDaySummary_RecommendedServiceOther"), $("#SixtyDaySummary_RecommendedServiceOtherValue"))
        },
        Load: function(episodeId, patientId, eventId) { Acore.Open("SixtyDaySummary", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    SkilledNurseVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("SkilledNurseVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    SNDiabeticDailyVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("SNDiabeticDailyVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    STDischarge: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("STDischarge", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    STEvaluation: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("STEvaluation", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    STMaintenance: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("STMaintenance", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    STReEvaluation: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("STReEvaluation", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    STVisit: {
        Init: function() { },
        Load: function(episodeId, patientId, eventId) { Acore.Open("STVisit", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    TransferSummary: {
        Init: function() { V.TransferSummary.SharedInit("TransferSummary") },
        SharedInit: function() {
        },
        Load: function(episodeId, patientId, eventId) { Acore.Open("TransferSummary", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    },
    WoundCare: {
        Init: function() {
            $(".WoundType").Autocomplete({ source: ["Trauma", "Pressure Ulcer", "Surgical Wound", "Diabetic Ulcer", "Venous Status Ulcer", "Arterial Ulcer"] });
            $(".DeviceType").Autocomplete({ source: ["J.P.", "Wound Vac", "None"] });
        },
        Load: function(episodeId, patientId, eventId) { Acore.Open("WoundCare", { episodeId: episodeId, patientId: patientId, eventId: eventId }) }
    }
}