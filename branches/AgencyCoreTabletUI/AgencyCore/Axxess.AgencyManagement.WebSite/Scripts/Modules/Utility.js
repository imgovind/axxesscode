﻿var U = {
    Ajax: function(Post, Url, Input, Success, Failure) {
        $.ajax({
            url: Url,
            data: Input,
            type: Post ? "POST" : "GET",
            dataType: "json",
            success: function(Data) {
                if (typeof Success == "function") Success(Data)
            },
            error: function(Data) {
                if (typeof Failure == "function") Failure(Data)
            }
        })
    },
    Delete: function(Name, Url, Data, Callback, BypassAlerts) {
        if (BypassAlerts || confirm("Are you sure you want to delete this " + Name.toLowerCase() + "?")) {
            U.PostUrl(Url, Data, function(Result) {
                if (Result.isSuccessful) {
                    if (Callback != undefined && typeof Callback == "function") Callback();
                    if (!BypassAlerts) U.Growl(Name + " has been successfully deleted.", "success");
                } else U.Growl(Result.errorMessage, "error")
            })
        }
    },
    DisplayDate: function() {
        var CurrentDate = new Date(),
                Days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                Months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                DateSuffix = "th";
        if (CurrentDate.getDate() % 10 == 1 && CurrentDate.getDate() != 11) DateSuffix = "st";
        else if (CurrentDate.getDate() % 10 == 2 && CurrentDate.getDate() != 12) DateSuffix = "nd";
        else if (CurrentDate.getDate() % 10 == 3 && CurrentDate.getDate() != 13) DateSuffix = "rd";
        return Days[CurrentDate.getDay()] + ", " + Months[CurrentDate.getMonth()] + " " + CurrentDate.getDate() + DateSuffix + ", " + CurrentDate.getFullYear()
    },
    EnableDatePicker: function(e) { $(".date", e.row).DatePicker(); },
    FilterResults: function(Prefix) {
        $("#" + Prefix + "MainResult").empty().addClass("loading");
        U.RebindGrid($("#" + Prefix + "SelectionGrid"), {
            branchId: $("select." + Prefix + "BranchCode").val(),
            statusId: $("select." + Prefix + "StatusDropDown").val(),
            paymentSourceId: $("select." + Prefix + "PaymentDropDown").val(),
            name: $("#txtSearch_" + Prefix + "_Selection").val()
        })
    },
    FormatMoney: function(Num) {
        if (isNaN(Number(Num))) return "";
        var Result = String(Math.round(Math.abs(Num) * 100));
        Result = Result.substr(0, Result.length - 2) + "." + Result.substr(Result.length - 2);
        for (var i = 0; i * 4 + 6 < Result.length; i++) Result = Result.substr(0, Result.length - (i * 4 + 6)) + "," + Result.substr(Result.length - (i * 4 + 6));
        Result = "$" + Result;
        if (Num < 0) Result = "-" + Result;
        return Result;
    },
    GetAttachment: function(Url, Data) {
        $("body").append(
            $("<form/>", { "id": "AjaxAttachmentForm", "method": "post", "action": Url })
        );
        $.each(Data, function(Key, Val) {
            $("#AjaxAttachmentForm").append(
                $("<input/>", { "type": "hidden", "name": Key, "value": Val })
            )
        });
        $("#AjaxAttachmentForm").submit().remove();
    },
    GetUrl: function(Url, Input, Success, Failure) {
        this.Ajax(false, Url, Input, Success, Failure)
    },
    Growl: function(Message, Theme) {
        if (typeof Message == "object") Message = Message.toSource();
        else if (typeof Message != "string") Message = String(Message);
        $.jGrowl($.trim(Message), { theme: Theme, life: 5000 })
    },
    HandlerHelperTemplate: function(AssessmentType, $form, $control, Action) {
        var options = {
            dataType: "json",
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if ($.trim(result.responseText) == "Success") {
                    if ($control.text() == "Save") {
                        U.Growl("Your assessment has been saved.", "success");
                    }
                    if ($control.text() == "Save & Exit") {
                        UserInterface.CloseAndRefresh(AssessmentType);
                        U.Growl("Your assessment has been saved successfully.", "success");
                    } else if ($control.text() == "Save & Continue") {
                        U.Growl("Your assessment has been saved.", "success");
                        Oasis.NextTab("#" + AssessmentType + "_Tabs");
                    } else if ($control.text() == "Save & Check for Errors") {
                        U.Growl("Your assessment has been saved.", "success");
                        if (typeof Action == "function") Action();
                    } else if ($control.text() == "Check for Errors") Action();
                    else if ($control.text() == "Save & Complete") {
                        if (typeof Action == "function") Action();
                    }
                } else U.Growl(result.responseText, "error");
            },
            error: function(result) {
                if ($.trim(result.responseText) == "Success") {
                    if ($control.text() == "Save") {
                        U.Growl("Your assessment has been saved.", "success");
                    }
                    if ($control.text() == "Save & Exit") {
                        UserInterface.CloseAndRefresh(AssessmentType);
                        U.Growl("Your assessment has been saved successfully.", "success");
                    } else if ($control.text() == "Save & Continue") {
                        U.Growl("Your assessment has been saved.", "success");
                        Oasis.NextTab("#" + AssessmentType + "_Tabs");
                    } else if ($control.text() == "Save & Check for Errors") {
                        U.Growl("Your assessment has been saved.", "success");
                        if (typeof Action == "function") Action();
                    } else if ($control.text() == "Check for Errors") Action();
                    else if ($control.text() == "Save & Complete") {
                        if (typeof Action == "function") Action();
                    }
                } else U.Growl(result.responseText, "error");
            }
        };
        $form.find(".form-omitted :input,.form-omitted:input").val("")
        $form.ajaxSubmit(options);
        return false;
    },
    HideExtras: function(Element) {
        $(".option .extra", Element).each(function() {
            if ($(this).siblings("input[type=radio],input[type=checkgroup]").prop("checked")) $(this).show();
            else $(this).hide();
            $(this).siblings("input[type=radio],input[type=checkgroup]").change(function() {
                if ($(this).prop("checked")) $(this).siblings(".extra").show();
                else $(this).siblings(".extra").hide();
            })
        })
    },
    IsDate: function(Date) {
        if (/^(0?[1-9]|1[012])\/(0?[1-9]|[12]\d|3[01])\/(19|20)?\d\d$/.test(Date)) {
            Date = Date.split("/");
            if (/^(0?[469]|11)$/.test(Date[0]) && parseInt(Date[1]) > 30) return false; // More than 30 days in April, June, September, or November
            if (/^(0?2)$/.test(Date[0])) {
                if (parseInt(Date[2]) % 4 && parseInt(Date[1]) > 28) return false; // More than 28 days in February on non-Leap Year
                if (parseInt(Date[2]) % 4 == 0 && parseInt(Date[1]) > 29) return false; // More than 29 days in February on a Leap Year
            }
            return true;
        }
        return false;
    },
    IsEmail: function(Email) {
        return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(Email)
    },
    IsGuid: function(Guid) {
        return /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(Guid) && /^[0-]*$/.test(Guid)
    },
    IsTime: function(Time) {
        return /^(0?[1-9]|1[012]):[0-5][0-9] [AP]M$/.test(Time)
    },
    Loading: function(Show) {
        if (Show) $("body").append(
            $("<div/>", { "id": "shade" })).append(
            $("<div/>", { "class": "loading loading-modal" }));
        else $("#shade,.loading-modal").remove()
    },
    Message: function(Title, Text, Class) {
        return $("<div/>", { "class": "error-box" }).addClass(Class).append(
                    $("<div/>", { "class": "logo" })).append(
                    $("<span/>", { "class": "img icon float-left" }).addClass(Class)).append(
                    $("<h1/>", { "text": Title })).append(
                    $("<p/>", { "text": Text }))
    },
    MessageError: function(Title, Text) {
        return $("<div/>", { "class": "error-box" }).append(
                    $("<div/>", { "class": "logo" })).append(
                    $("<span/>", { "class": "img icon error float-left" })).append(
                    $("<h1/>", { "text": Title })).append(
                    $("<p/>", { "text": Text }))
    },
    MessageErrorAjax: function() {
        return U.MessageError("Request Error", "An error has been detected while procuring data. Please check your connection and try again, if problem persists, contact Axxess for assistance.");
    },
    MessageErrorJS: function(Growl) {
        var Title = "Browser Error",
            Text = "An error has been detected in your browser. Please refresh the page and try again, if problem persists, contact Axxess for assistance.";
        if (Growl) return Title + "<br />" + Text;
        else return U.MessageError(Title, Text);
    },
    MessageInfo: function(Title, Text) {
        return U.Message(Title, Text, "info")
    },
    MessageSuccess: function(title, text) {
        return U.Message(Title, Text, "success")
    },
    MessageWarn: function(Title, Text) {
        return U.Message(Title, Text, "warning")
    },
    PostUrl: function(Url, Input, Success, Failure) {
        this.Ajax(true, Url, Input, Success, Failure)
    },
    RebindAjaxGrid: function(Url, Input, Grid) {
        U.PostUrl(Url, Input, function(Result) {
            if (Result.isSuccessful) {
                U.RebindGrid(Grid);
                U.Growl(Result.errorMessage, "success");
            } else U.Growl(Result.errorMessage, "error")
        })
    },
    RebindGrid: function(Grid, Input) {
        if (Grid.data("tGrid") != null) Grid.data("tGrid").rebind(Input)
    },
    ToolTip: function(Element, CssClass) {
        Element.each(function() {
            if ($(this).attr("tooltip") != undefined) $(this).tooltip({
                track: true,
                showURL: false,
                top: 10,
                left: 10,
                extraClass: CssClass,
                bodyHandler: function() {
                    return $(this).attr("tooltip")
                }
            })
        })
    },
    ToTitleCase: function(Text) {
        var TextArray = Text.toLowerCase().split(" ");
        if (TextArray.length > 1) {
            Text = "";
            for (var i = 0; i < TextArray.length; i++) Text += TextArray[i].substr(0, 1).toUpperCase() + TextArray[i].substr(1) + " ";
        } else Text = Text.substr(0, 1).toUpperCase() + Text.toLowerCase().substr(1);
        return Text;
    },
    ValidationError: function(control) {
        if (control.closest(".window-content").find(".error:first").closest("fieldset").length) var scroll = control.closest(".window-content").find(".error:first").closest("fieldset").position().top;
        else if (control.closest(".window-content").find(".error:first").closest("td").length) var scroll = control.closest(".window-content").find(".error:first").closest("td").position().top;
        else if (control.closest(".window-content").find(".error:first").closest("div").length) var scroll = control.closest(".window-content").find(".error:first").closest("div").position().top;
        else var scroll = 0;
        control.closest(".window-content").scrollTop(scroll);
        U.Growl("Error: There was a problem validating your form, please review your information and try again.", "error");
    },
    Y2KConvert: function(ShortYear) {
        var CurrentYear = String(new Date().getFullYear()),
            CurrentPrefix = parseInt(CurrentYear.substr(0, 2)),
            CurrentShortYear = parseInt(CurrentYear.substr(2, 2));
        if (parseInt(ShortYear) > CurrentShortYear) return String(--CurrentPrefix) + String(ShortYear);
        else return String(CurrentPrefix) + String(ShortYear);
    },

    DeleteTemplate: function(name, id) { U.Delete(name, name + "/Delete", { Id: id }, function() { eval(name + ".RebindList()"); }); },
    InitTemplate: function(formobj, callback, message, before) {
        $(".names").alpha({ nocaps: false });
        formobj.validate({
            submitHandler: function(form) {
                if (before != undefined && typeof (before) == 'function') before();
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            if (callback != undefined && typeof (callback) == 'function') callback();
                            U.Growl(message, "success");
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditTemplate: function(type, callback) {
        U.InitTemplate($("#edit" + type + "Form"), function() {
            eval(type + ".RebindList()");
            if (callback != undefined && typeof (callback) == 'function') callback();
            UserInterface.CloseWindow("edit" + type.toLowerCase());
        }, type + " successfully updated");
    },
    InitNewTemplate: function(type, callback) {
        U.InitTemplate($("#new" + type + "Form"), function() {
            eval(type + ".RebindList()");
            if (callback != undefined && typeof (callback) == 'function') callback();
            UserInterface.CloseWindow("new" + type.toLowerCase());
        }, "New " + type.toLowerCase() + " successfully added");
    },
    showDialog: function(popupBox, onReady) {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: $(popupBox),
            css: {
                top: ($(window).height() - $(popupBox).height()) / 2 + 'px',
                left: ($(window).width() - $(popupBox).width()) / 2 + 'px'
            },
            onBlock: function() {
                if (typeof (onReady) == 'function') {
                    onReady();
                }
            }
        });
    },
    closeDialog: function() {
        $.unblockUI();
    }
};
$.fn.contains = function(txt) { return jQuery(this).indexOf(txt) >= 0; }
