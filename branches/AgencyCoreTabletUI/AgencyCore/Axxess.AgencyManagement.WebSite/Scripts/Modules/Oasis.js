﻿var Oasis = {
    DeleteAsset: function(control, assessmentType, name, assetId) {
        U.Delete("Asset", "/Oasis/DeleteWoundCareAsset", {
            episodeId: $("#" + assessmentType + "_EpisodeId").val(),
            patientId: $("#" + assessmentType + "_PatientGuid").val(),
            eventId: $("#" + assessmentType + "_Id").val(),
            assessmentType: assessmentType,
            name: name,
            assetId: assetId
        }, function() {
            $(control).closest('td').empty().append(
                $("<input/>", {
                    "type": "file",
                    "name": assessmentType + "_" + name,
                    "value": "Upload",
                    "size": "1",
                    "class": "float-left upload-width"
                })
            )
        })
    },
    Load: function(Id, PatientId, AssessmentType) {
        Acore.Open(AssessmentType, "Oasis/" + AssessmentType, function() {
            Oasis.InitTabs(Id, PatientId, AssessmentType)
        }, {
            Id: Id,
            PatientId: PatientId,
            AssessmentType: AssessmentType
        })
    },
    InitTabs: function(Id, PatientId, AssessmentType) {
        U.ToolTip($(".vertical-tabs a", "#" + AssessmentType + "_Tabs"), "calday");
        $("#" + AssessmentType + "_Tabs").addClass("ui-tabs-vertical ui-helper-clearfix").tabs().bind("tabsselect", {
            Id: Id,
            PatientId: PatientId,
            AssessmentType: AssessmentType
        }, function(event, ui) {
            Oasis.LoadParts(AssessmentType, event, ui)
        }).find("li").removeClass("ui-corner-top").addClass("ui-corner-left");
        this.InitSection("", "", "", $(".general:eq(0)", "#" + AssessmentType + "_Tabs"))
    },
    LoadParts: function(AssessmentType, event, ui) {
        $($(ui.tab).attr("href")).Load("Oasis/Category", {
            Id: event.data.Id,
            PatientId: event.data.PatientId,
            AssessmentType: event.data.AssessmentType,
            Category: $(ui.tab).attr("href")
        }, this.InitSection)
    },
    NonOasisSignature: function(id, patientId, episodeId, assessmentType) {
        Acore.Modal({
            Name: "Non-OASIS Signature",
            Url: "Oasis/NonOasisSignature",
            Input: {
                Id: id,
                PatientId: patientId,
                EpisodeId: episodeId,
                AssessmentType: assessmentType
            },
            OnLoad: function() { Oasis.NonOasisSignatureSubmit($("#NonOasisSignatureForm")) },
            Width: "800px",
            WindowFrame: false
        })
    },
    FallAssessment: function(AssessmentType) {
        $("#" + AssessmentType + "_485FallAssessmentScore").val(String($("#" + AssessmentType + "_FallAssessment .option input[type='checkbox']:checked").length));
        $("#" + AssessmentType + "_FallAssessment .option input").change(function() {
            $("#" + AssessmentType + "_485FallAssessmentScore").val(String($("#" + AssessmentType + "_FallAssessment .option input[type='checkbox']:checked").length));
        })
    },
    NonOasisSignatureSubmit: function(form) {
        form.validate({
            submitHandler: function(form) {
                var options = {
                    dataTpe: "json",
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            var window = $("input[name=OasisValidationType]").val().toLowerCase();
                            U.Growl(resultObject.errorMessage, "success");
                            Patient.Rebind();
                            Schedule.Rebind();
                            Agency.RebindCaseManagement();
                            UserInterface.CloseModal();
                            UserInterface.CloseWindow(window);
                        } else U.Growl(resultObject.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    InitSection: function(responseText, textStatus, XMLHttpRequest, Element) {
        var AssessmentType = Element.prop("id").split("_")[0],
            Category = Element.prop("id").split("_")[1];
        if (/^NonOasis.*$/.test(AssessmentType)) {
            $("fieldset.oasis", Element).removeClass("oasis");
            $("a.green,.oasis-tip", Element).remove();
        } else $("fieldset.oasis.loc485", Element).removeClass("loc485");
        $(".interventions input[type=checkbox],.goals input[type=checkbox]", Element).each(function() {
            if ($(this).parent().find("span.radio").length) {
                if ($(this).is(":not(:checked)")) $(this).parent().find("span.radio :input").attr("disabled", true);
                $(this).change(function() {
                    if ($(this).is(":checked")) $(this).parent().find("span.radio :input").attr("disabled", false);
                    else $(this).parent().find("span.radio :input").attr("disabled", true);
                })
            }
        })
        if (typeof Oasis.Tabs[Category] == "function") Oasis.Tabs[Category](AssessmentType, Oasis.GetAssessmentTypeNumber(AssessmentType), Element);
    },
    GetAssessmentTypeNumber: function(AssessmentType) {
        switch (AssessmentType) {
            case "StartOfCare": return 1;
            case "ResumptionOfCare": return 3;
            case "Recertification": return 4;
            case "FollowUp": return 5;
            case "TransferInPatientNotDischarged": return 6;
            case "TransferInPatientDischarged": return 7;
            case "DischargeFromAgencyDeath": return 8;
            case "DischargeFromAgency": return 9;
            case "NonOasisStartOfCare": return 11;
            case "NonOasisRecertification": return 14;
            case "NonOasisDischarge": return 19;
        }
    },
    stripDecimal: function(data) {
        var newData = "";
        for (i = 0; i < data.length; i++) if (i != data.indexOf(".") && data.charAt(i) != " ") newData += data.charAt(i);
        return newData;
    },
    GetPrimaryPhysician: function(id) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "Patient/GetPhysicianContact",
            data: { "PhysicianContactId": id },
            success: function(result) {
                var resultObject = eval(result);
                $("#patientPhysicianName").text(
                    (resultObject.FirstName !== null ? resultObject.FirstName : "") + " " +
                    (resultObject.LastName != null ? resultObject.LastName : ""));
                $("#patientPhysicianEmail").text(resultObject.Email !== null ? resultObject.Email : "");
                $("#patientPhysicianPhone").text(resultObject.PhoneWork !== null ? (
                    resultObject.PhoneWork.substring(0, 3) + "-" +
                    resultObject.PhoneWork.substring(3, 6) + "-" +
                    resultObject.PhoneWork.substring(6)) : "")
            }
        })
    },
    NextTab: function(id) {
        $(id).tabs().tabs("select", $(".vertical-tabs", id).children("li").index($(".ui-tabs-selected")) + 1)
    },
    Refresh: function(id) {
        $(id).tabs().tabs("select", 0).find(id + " .general").each(function() {
            $(this).scrollTop(0)
        })
    },
    Delete: function(cont, id) {
        U.Delete("Assessment", "Oasis/Delete", { Id: id }, function() {
            cont.parents("tr:first").remove()
        })
    },
    supplyInputFix: function(assessmentType, tableName) {
        var tableTr = $("tbody tr", $(tableName)), len = tableTr.length, i = 0, jsonData = '{ "Supply": [';
        $(tableTr).each(function() {
            jsonData += '{' +
                '"suppliesDescription":"' + $(this).find(".suppliesDescription").val() + '",' +
                '"suppliesCode":"' + $(this).find('.suppliesCode').val() + '",' +
                '"supplyQuantity":"' + $(this).find('.supplyQuantity').val() + '"}';
            if (len > ++i) jsonData += ',';
        });
        jsonData += '] }';
        $("#" + assessmentType + "_GenericSupply").val(jsonData);
    },
    ClearRows: function(table) {
        $("tbody tr", table).remove()
    },
    DeleteRow: function(control) {
        if ($(control).closest("tbody").find("tr").length == 1) {
            $(".scheduleTables.purgable").each(function() { $(this).find("tbody").empty() });
            Schedule.ShowScheduler();
        } else $(control).closest("tr").remove();
        Schedule.positionBottom();
    },
    BradenScale: function(AssessmentType) {
        Oasis.BradenScaleCalc(AssessmentType);
        $("#" + AssessmentType + "_BradenScale .active input").prop("checked", true);
        $("#" + AssessmentType + "_BradenScale td").click(function() {
            $(this).closest("tr").find("td").removeClass("active").find("input").prop("checked", false);
            $(this).addClass("active").find("input").prop("checked", true).closest("tr").find("input[type=hidden]").val($(this).attr("value"));
            Oasis.BradenScaleCalc(AssessmentType);
        });
    },
    BradenScaleCalc: function(AssessmentType) {
        var sensory = $("#" + AssessmentType + "_485BradenScaleSensoryHidden").val();
        var moisture = $("#" + AssessmentType + "_485BradenScaleMoistureHidden").val();
        var activity = $("#" + AssessmentType + "_485BradenScaleActivityHidden").val();
        var mobility = $("#" + AssessmentType + "_485BradenScaleMobilityHidden").val();
        var nutrition = $("#" + AssessmentType + "_485BradenScaleNutritionHidden").val();
        var friction = $("#" + AssessmentType + "_485BradenScaleFrictionHidden").val();
        var total = (sensory.length ? parseInt(sensory) : 0) +
                    (moisture.length ? parseInt(moisture) : 0) +
                    (activity.length ? parseInt(activity) : 0) +
                    (mobility.length ? parseInt(mobility) : 0) +
                    (nutrition.length ? parseInt(nutrition) : 0) +
                    (friction.length ? parseInt(friction) : 0);
        $("#" + AssessmentType + "_BradenScale").find("input[type=text]").val(total);
        if (total == 0) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong");
        else if (total >= 19) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong").eq(0).addClass("strong");
        else if (total >= 15 && total <= 18) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong").eq(1).addClass("strong");
        else if (total >= 13 && total <= 14) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong").eq(2).addClass("strong");
        else if (total >= 10 && total <= 12) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong").eq(3).addClass("strong");
        else if (total <= 9) $("#" + AssessmentType + "_BradenScale").find("li").removeClass("strong").eq(4).addClass("strong");
    },
    CalculateNutritionScore: function(assessmentType) {
        var score = 0;
        $("input[name=" + assessmentType + "_GenericNutritionalHealth][type=checkbox]:checked").each(function() {
            score += parseInt($(this).parent('.option').find('label:first').text());
        });
        if (score <= 25) {
            $("#" + assessmentType + "_GoodNutritionalStatus").addClass("strong");
            $("#" + assessmentType + "_ModerateNutritionalRisk").removeClass("strong");
            $("#" + assessmentType + "_HighNutritionalRisk").removeClass("strong");
        } else if (score <= 55) {
            $("#" + assessmentType + "_GoodNutritionalStatus").removeClass("strong");
            $("#" + assessmentType + "_ModerateNutritionalRisk").addClass("strong");
            $("#" + assessmentType + "_HighNutritionalRisk").removeClass("strong");
        } else if (score <= 100) {
            $("#" + assessmentType + "_GoodNutritionalStatus").removeClass("strong");
            $("#" + assessmentType + "_ModerateNutritionalRisk").removeClass("strong");
            $("#" + assessmentType + "_HighNutritionalRisk").addClass("strong");
        }
        $("#" + assessmentType + "_GenericGoodNutritionScore").val(score);
    },
    TinettiAssessment: function(assessmentType) {
        Oasis.CalculateTinettiScore(assessmentType);
        $("#" + assessmentType + "_Tinetti input").change(function() { Oasis.CalculateTinettiScore(assessmentType) });
    },
    CalculateTinettiScore: function(assessmentType) {
        var balance = 0, gait = 0, total = 0;
        $("#" + assessmentType + "_Tinetti :checked").each(function() {
            if ($(this).hasClass("TinettiBalance")) balance += parseInt($(this).val());
            if ($(this).hasClass("TinettiGait")) gait += parseInt($(this).val());
            total += parseInt($(this).val());
        });
        $("input[name=" + assessmentType + "_TinettiBalanceTotal]").val(String(balance));
        $("input[name=" + assessmentType + "_TinettiGaitTotal]").val(String(gait));
        $("input[name=" + assessmentType + "_TinettiTotal]").val(String(total));
        $("#" + assessmentType + "_Tinetti li").removeClass("strong");
        if (total < 19) $("#" + assessmentType + "_Tinetti li:eq(1)").addClass("strong");
        else if (total < 25) $("#" + assessmentType + "_Tinetti li:eq(2)").addClass("strong");
        else $("#" + assessmentType + "_Tinetti li:eq(3)").addClass("strong");
    },
    Tip: function(Question) {
        U.PostUrl("/Oasis/Guide", { mooCode: Question }, function(data) {
            $(".oasis-tipbox").remove();
            Acore.Dialog({
                Name: "",
                Content: "",
                Width: "40em",
                Height: "35em"
            })
            $("#window_DialogWindow").addClass("oasis-tipbox").find(".window-content").remove();
            $("#window_DialogWindow").append(
                $("<h3/>", { "text": "Item Intent" })).append(
                $("<div/>", { "class": "content intent", "text": data.ItemIntent })).append(
                $("<h3/>", { "text": "Response" })).append(
                $("<div/>", { "class": "content response", "text": data.Response })).append(
                $("<h3/>", { "text": "Data Sources" })).append(
                $("<div/>", { "class": "content sources", "text": data.DataSources }))
        })
    },
    OasisStatusAction: function(id, patientId, episodeId, assessmentType, actionType, pageName) {
        var reason = "";
        if (actionType == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() { if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide(); });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown('slow');
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl('Oasis/Submit', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, actionType: actionType, reason: reason }, function(data) {
                    if (data.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        Oasis.RebindToExport();
                        Oasis.RebindExported();
                        UserInterface.CloseWindow(pageName);
                        U.Growl(data.errorMessage, "success");
                    } else U.Growl(data.errorMessage, "error");
                });
            }
        } else {
            U.PostUrl('Oasis/Submit', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, actionType: actionType, reason: reason }, function(data) {
                if (data.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    Oasis.RebindToExport();
                    Oasis.RebindExported();
                    UserInterface.CloseWindow(pageName);
                    U.Growl(data.errorMessage, "success");
                } else U.Growl(data.errorMessage, "error");
            });
        }
    },
    OasisSubmitOnlyAction: function(id, patientId, episodeId, assessmentType, pageName, signature, date) {
        U.PostUrl('Oasis/SubmitOnly', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, signature: signature, date: date }, function(data) {
            if (data.isSuccessful) {
                UserInterface.CloseModal();
                Patient.Rebind();
                Schedule.Rebind();
                Oasis.RebindToExport();
                Oasis.RebindExported();
                Agency.RebindCaseManagement();
                UserInterface.CloseWindow(pageName);
                U.Growl(data.errorMessage, "success");
            } else U.Growl(data.errorMessage, "error");
        });
    },
    MarkAsExported: function(control) {
        var fields = $(":input", $(control)).serializeArray();
        U.PostUrl('Oasis/MarkExported', fields, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Rebind();
                Schedule.Rebind();
                Oasis.RebindToExport();
                Oasis.RebindExported();
                Agency.RebindCaseManagement();
            } else U.Growl(data.errorMessage, "error");
        });
    },
    Reopen: function(id, patientId, episodeId, assessmentType, actionType) {
        Acore.Modal({
            Name: "Reopen OASIS",
            Content: $("<div/>", { "id": "oasisReopenDialog", "class": "wrapper main" }).append(
                        $("<fieldset/>").append(
                            $("<div/>").append(
                                $("<strong/>", { "text": "Note: " })).append(
                                $("<span/>", { "text": "If this OASIS assessment was already accepted by CMS (Center for Medicare & Medicaid Services), you will have to re-submit this OASIS assessment to CMS if changes are made to OASIS items." }))).append(
                            $("<div/>").addClass("strong").append(
                                $("<label/>", { "for": "Oasis_OasisReopenReason", "text": "Reason" })).append(
                                $("<textarea/>", { "id": "Oasis_OasisReopenReason", "name": "Oasis_OasisReopenReason" }))).append(
                            $("<div/>").addClass("strong align-center").html("Are you sure you want to reopen the assessment?"))).append(
                        $("<div/>").Buttons([
                            {
                                Text: "Yes, Reopen",
                                Click: function() {
                                    var reason = $("#Oasis_OasisReopenReason").val();
                                    U.PostUrl('Oasis/Submit', {
                                        Id: id,
                                        patientId: patientId,
                                        episodeId: episodeId,
                                        assessmentType: assessmentType,
                                        actionType: actionType,
                                        reason: reason
                                    }, function(data) {
                                        if (data.isSuccessful) {
                                            U.Growl(data.errorMessage, "success");
                                            Patient.Rebind();
                                            Schedule.Rebind();
                                            Oasis.RebindToExport();
                                            Oasis.RebindExported();
                                            Agency.RebindCaseManagement();
                                            UserInterface.CloseModal();
                                        } else U.Growl(data.errorMessage, "error");
                                    })
                                }
                            }, {
                                Text: "No, Cancel",
                                Click: function() { $(this).closest(".window").Close() }
                            }
                        ])
                    ),
            Width: "700px",
            WindowFrame: false
        })
    },
    RebindToExport: function() { if ($('#generateOasisGrid').data('tGrid') != null) $('#generateOasisGrid').data('tGrid').rebind(); },
    RebindExported: function() { if ($('#exportedOasisGrid').data('tGrid') != null) $('#exportedOasisGrid').data('tGrid').rebind(); },
    AddSupply: function(control, episodeId, patientId, eventId, assessmentType, supplyId) {
        var quantity = $(control).closest('tr').find('.quantity').val();
        var date = $(control).closest('tr').find('.date').val();
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId + "&assessmentType=" + assessmentType + "&supplyId=" + supplyId +
            "&quantity=" + quantity + "&date=" + date;
        U.PostUrl("/Oasis/AddSupply", input, function(result) {
            var gridfilter = $("#" + assessmentType + "_SupplyFilterGrid").data('tGrid');
            if (gridfilter != null) {
                $("#" + assessmentType + "_GenericSupplyDescription").val('');
                $("#" + assessmentType + "_GenericSupplyCode").val('');
                gridfilter.rebind({ q: "", limit: 0, type: "" });
            }
            var grid = $("#" + assessmentType + "_SupplyGrid").data('tGrid');
            if (grid != null) grid.rebind({ episodeId: episodeId, patientId: patientId, eventId: eventId, assessmentType: assessmentType });
        });
    },
    SupplyDescription: function(type) {
        $("#" + type + "_GenericSupplyDescription").keyup(function(event) {
            var message = $("#" + type + "_GenericSupplyDescription").val().length;
            var grid = $("#" + type + "_SupplyFilterGrid").data('tGrid');
            if (grid != null) grid.rebind({ q: $("#" + type + "_GenericSupplyDescription").val(), limit: 20, type: "Description" });
            if (event.keyCode == 13) return false;
            else return true;
        });
    },
    SupplyCode: function(type) {
        $("#" + type + "_GenericSupplyCode").keyup(function(event) {
            var grid = $("#" + type + "_SupplyFilterGrid").data('tGrid');
            if (grid != null) grid.rebind({ q: $("#" + type + "_GenericSupplyCode").val(), limit: 20, type: "Code" });
            if (event.keyCode == 13) return false;
            else return true;
        });
    },
    blockText: function(control, id) {
        if ($(control).is(':checked')) $(id).attr("disabled", "disabled").val("");
        else $(id).attr("disabled", "");
    },
    unBlockText: function(id) { $(id).attr("disabled", ""); },
    LoadBlankMasterCalendar: function(id, episodeId, patientId) {
        $("#" + id + "_BlankMasterCalendar").Load("Oasis/BlankMasterCalendar", { episodeId: episodeId, patientId: patientId, assessmentType: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $("#" + id + "_Show").hide();
                $("#" + id + "_Hide").show();
            } else if (textStatus == "success") {
                $("#" + id + "_Show").hide();
                $("#" + id + "_Hide").show();
            }
        })
    },
    HideBlankMasterCalendar: function(id) {
        $("#" + id + "_BlankMasterCalendar").empty();
        $("#" + id + "_Show").show();
        $("#" + id + "_Hide").hide();
    },
    gotoQuestion: function(question, assessmentType) {
        var tab = 1, q = parseInt(question.replace(/^[M0]*/, ""));
        if (assessmentType == "StartOfCare" || assessmentType == "ResumptionOfCare") {
            if (q >= 1000 && q <= 1030) tab = 2;
            if (q >= 1032 && q <= 1036) tab = 3;
            if (q == 1100) tab = 5;
            if (q >= 1200 && q <= 1230) tab = 6;
            if (q >= 1240 && q <= 1242) tab = 7;
            if (q >= 1300 && q <= 1350) tab = 8;
            if (q >= 1400 && q <= 1410) tab = 9;
            if (q >= 1600 && q <= 1630) tab = 12;
            if (q >= 1700 && q <= 1750) tab = 14;
            if (q >= 1800 && q <= 1910) tab = 15;
            if (q >= 2000 && q <= 2040) tab = 17;
            if (q >= 2100 && q <= 2110) tab = 18;
            if (q >= 2200 && q <= 2250) tab = 19;
        } else if (assessmentType == "Recertification") {
            if (q >= 1020 && q <= 1030) tab = 2;
            if (q == 1200) tab = 5;
            if (q == 1242) tab = 6;
            if (q >= 1306 && q <= 1350) tab = 7;
            if (q >= 1400 && q <= 1410) tab = 8;
            if (q >= 1610 && q <= 1630) tab = 11;
            if (q >= 1810 && q <= 1910) tab = 14;
            if (q == 2030) tab = 16;
            if (q >= 2200) tab = 17;
        } else if (assessmentType == "FollowUp") {
            if (q >= 1020 && q <= 1030) tab = 2;
            if (q == 1200) tab = 3;
            if (q == 1242) tab = 4;
            if (q >= 1306 && q <= 1350) tab = 5;
            if (q == 1400) tab = 6;
            if (q >= 1610 && q <= 1630) tab = 7;
            if (q >= 1810 && q <= 1860) tab = 8;
            if (q == 2030) tab = 9;
            if (q == 2200) tab = 10;
        } else if (assessmentType == "TransferInPatientNotDischarged" || assessmentType == "TransferInPatientDischarged") {
            if (q >= 1040 && q <= 1055) tab = 2;
            if (q >= 1500 && q <= 1510) tab = 3;
            if (q >= 2004 && q <= 2015) tab = 4;
            if (q >= 2300 && q <= 2310) tab = 5;
            if (q == 903 || q == 906 || (q >= 2400 && q <= 2440)) tab = 6;
        } else if (assessmentType == "DischargeFromAgencyDeath") {
            if (q == 903 || q == 906) tab = 2;
        } else if (assessmentType == "DischargeFromAgency") {
            if (q >= 1040 && q <= 1050) tab = 2;
            if (q == 1230) tab = 3;
            if (q >= 1242) tab = 4;
            if (q >= 1306 && q <= 1350) tab = 5;
            if (q >= 1400 && q <= 1410) tab = 6;
            if (q >= 1500 && q <= 1510) tab = 7;
            if (q >= 1600 && q <= 1620) tab = 8;
            if (q >= 1700 && q <= 1750) tab = 9;
            if (q >= 1800 && q <= 1890) tab = 10;
            if (q >= 2004 && q <= 2030) tab = 11;
            if (q >= 2100 && q <= 2110) tab = 12;
            if (q >= 2300 && q <= 2310) tab = 13;
            if (q == 903 || q == 906 || (q >= 2400 && q <= 2420)) tab = 14;
        }
        if (Acore.Windows[assessmentType].IsOpen) {
            $("#window_" + assessmentType).WinFocus();
            $('.vertical-tabs li:nth-child(' + tab + ') a', "#window_" + assessmentType).click();
            setTimeout(function() {
                if ($("#" + assessmentType + "_" + question).length)
                    $(".general:nth-child(" + (tab + 1) + ")", "#window_" + assessmentType + "_content").scrollTop(
                        $("#" + assessmentType + "_" + question).closest("fieldset").position().top
                    );
            }, 1000);
        }
    },
    oasisSignatureSubmit: function(form) {
        form.validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            window.parent.UserInterface.CloseModal();
                            window.parent.UserInterface.CloseWindow($("#oasisPageName").val());
                            window.parent.Patient.Rebind();
                            window.parent.Schedule.Rebind();
                            window.parent.Oasis.RebindToExport();
                            window.parent.Oasis.RebindExported();
                            window.parent.Agency.RebindCaseManagement();
                            window.parent.U.Growl(resultObject.errorMessage, 'success');
                        } else window.parent.U.Growl(resultObject.errorMessage, 'error');
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditCorrectionNumber: function() {
        $("#oasisCorrectionChange").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseModal();
                            Oasis.RebindToExport();
                        } else U.Growl(result.errorMessage, "error");
                    },
                    error: function(result) { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    LoadCancel: function(Id, type) {
        U.PostUrl("/Oasis/Inactivate", { Id: Id, type: type }, function(result) {
            if (result.isValid) U.PostUrl("/Oasis/GenerateForCancel", { Id: Id, type: type });
            else U.Growl(result.Message, "error");
        }, function(result) { })
    },
    Tabs: {
        AdlIadl: function(AssessmentType, AssessmentTypeNum, Element) {
            if (AssessmentTypeNum % 10 < 5) {
                Forms.ShowIfChecked($("#" + AssessmentType + "_485ActivitiesPermittedD"), $("#" + AssessmentType + "_485ActivitiesPermittedDMore"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericMusculoskeletal2"), $("#" + AssessmentType + "_GenericMusculoskeletal2More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericMusculoskeletal3"), $("#" + AssessmentType + "_GenericMusculoskeletalImpairedMotorSkills"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericMusculoskeletal4"), $("#" + AssessmentType + "_GenericMusculoskeletal4More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericMusculoskeletal5"), $("#" + AssessmentType + "_GenericMusculoskeletalMobility"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericMusculoskeletal6"), $("#" + AssessmentType + "_GenericMusculoskeletal6More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericAssistiveDevice5"), $("#" + AssessmentType + "_GenericAssistiveDeviceOther"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericMusculoskeletal7"), $("#" + AssessmentType + "_GenericMusculoskeletal7More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericMusculoskeletal9"), $("#" + AssessmentType + "_GenericMusculoskeletal9More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericMusculoskeletal12"), $("#" + AssessmentType + "_GenericMusculoskeletal12More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericMusculoskeletal13"), $("#" + AssessmentType + "_GenericMusculoskeletal13More"));
                Oasis.FallAssessment(AssessmentType);
                Oasis.TinettiAssessment(AssessmentType);
            }
        },
        Cardiac: function(AssessmentType, AssessmentTypeNum, Element) {
            if (AssessmentTypeNum % 10 < 5) {
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericCardioVascular2"), $("#" + AssessmentType + "_GenericCardioVascular2More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericCardioVascular3"), $("#" + AssessmentType + "_GenericCardioVascular3More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericCardioVascular4"), $("#" + AssessmentType + "_GenericCardioVascular4More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericCardioVascular5"), $("#" + AssessmentType + "_GenericCardioVascular5More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericPittingEdemaType1"), $("#" + AssessmentType + "_GenericPittingEdemaType1More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericPittingEdemaType2"), $("." + AssessmentType + "_GenericPittingEdemaType2More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericPittingEdemaType2"), $("#" + AssessmentType + "_GenericPittingEdemaType2More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericCardioVascular6"), $("#" + AssessmentType + "_GenericCardioVascular6More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericCardioVascular7"), $("#" + AssessmentType + "_GenericCardioVascular7More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericCardioVascular8"), $("#" + AssessmentType + "_GenericCardioVascular8More"));
            } else Forms.HideIfRadioEquals(AssessmentType + "_M1500HeartFailureSymptons", "00|NA", $("#" + AssessmentType + "_M1510"));
        },
        Demographics: function(AssessmentType, AssessmentTypeNum, Element) {
            $("input[name=" + AssessmentType + "_M0100AssessmentType]").click(function() {
                $("input[name=" + AssessmentType + "_M0100AssessmentType]").eq(AssessmentTypeNum % 10 > 1 ? AssessmentTypeNum % 10 - 1 : AssessmentTypeNum % 10).prop("checked", true)
            })
            Forms.HideIfChecked($("#" + AssessmentType + "_M0064PatientSSNUnknown"), $("#" + AssessmentType + "_M0064PatientSSN"));
            Forms.HideIfChecked($("#" + AssessmentType + "_M0063PatientMedicareNumberUnknown"), $("#" + AssessmentType + "_M0063PatientMedicareNumber"));
            Forms.HideIfChecked($("#" + AssessmentType + "_M0065PatientMedicaidNumberUnknown"), $("#" + AssessmentType + "_M0065PatientMedicaidNumber"));
            Forms.HideIfChecked($("#" + AssessmentType + "_M0032ROCDateNotApplicable"), $("#" + AssessmentType + "_M0032ROCDate").parent());
            Forms.HideIfChecked($("#" + AssessmentType + "_M0018NationalProviderIdUnknown"), $("#" + AssessmentType + "_M0018NationalProviderId"));
            if (AssessmentTypeNum < 6) {
                Forms.HideIfChecked($("#" + AssessmentType + "_M0102PhysicianOrderedDateNotApplicable"), $("#" + AssessmentType + "_M0102PhysicianOrderedDate").parent());
                $("#" + AssessmentType + "_M0102PhysicianOrderedDateNotApplicable").change(function() {
                    var date = $("#" + AssessmentType + "_M0102PhysicianOrderedDate").val();
                    if ($(this).prop("checked")) $("#" + AssessmentType + "_M0104").show().removeClass("form-omitted");
                    else if (date && date.length) $("#" + AssessmentType + "_M0104").hide().addClass("form-omitted");
                    else $("#" + AssessmentType + "_M0104").show().removeClass("form-omitted");
                });
                $("#" + AssessmentType + "_M0102PhysicianOrderedDate").blur(function() {
                    if ($(this).val() && $(this).val().length) $("#" + AssessmentType + "_M0104").hide().addClass("form-omitted");
                    else $("#" + AssessmentType + "_M0104").show().removeClass("form-omitted")
                })
            }
            if (AssessmentTypeNum != 8) {
                Forms.NoneOfTheAbove($("#" + AssessmentType + "_M0150PaymentSourceUnknown"), $("#" + AssessmentType + "_M0150 .M0150"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_M0150PaymentSourceOtherSRS"), $("#" + AssessmentType + "_M0150PaymentSourceOtherSRSMore"));
            }
            $("#" + AssessmentType + "_PreviousAssessmentButton").click(function() {
                var previousAssessmentArray = $("#" + AssessmentType + "_PreviousAssessments").val().split('_');
                if (previousAssessmentArray.length > 1) {
                    if (confirm("Are you sure you want to load this assessment?")) {
                        var episodeId = $("#" + AssessmentType + "_EpisodeId").val(),
                        patientId = $("#" + AssessmentType + "_PatientGuid").val(),
                        assessmentId = $("#" + AssessmentType + "_Id").val(),
                        assessmentType = $("#" + AssessmentType + "_AssessmentType").val();
                        U.PostUrl("Oasis/LoadPrevious", {
                            episodeId: episodeId,
                            patientId: patientId,
                            assessmentId: assessmentId,
                            assessmentType: assessmentType,
                            previousAssessmentId: previousAssessmentArray[0],
                            previousAssessmentType: previousAssessmentArray[1]
                        }, function(result) {
                            if (result.isSuccessful) {
                                Schedule.Rebind();
                                U.Growl(result.errorMessage, "success");
                            } else U.Growl(result.errorMessage, "error");
                        })
                    }
                } else U.Growl("Please select a previous assessment first", "error")
            })
        },
        Elimination: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericGU10"), $("." + AssessmentType + "_GenericGU10More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericGU11"), $("#" + AssessmentType + "_GenericGU11More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericGUUrine5"), $("#" + AssessmentType + "_GenericGUUrine5More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericGU12"), $("#" + AssessmentType + "_GenericGU12More"));
            Forms.ShowIfRadioEquals(AssessmentType + "_GenericPatientOnDialysis", "1", $("#" + AssessmentType + "_Dialysis"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDialysis5"), $("#" + AssessmentType + "_GenericDialysis5More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDialysisHemodialysis1"), $("#" + AssessmentType + "_GenericDialysisHemodialysis1More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDialysisHemodialysis2"), $("#" + AssessmentType + "_GenericDialysisHemodialysis2More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDialysis7"), $("#" + AssessmentType + "_GenericDialysis7More"));
            Forms.ShowIfRadioEquals(AssessmentType + "_M1610UrinaryIncontinence", "01", $("#" + AssessmentType + "_M1615"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDigestive2"), $("#" + AssessmentType + "_GenericDigestive2More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDigestive3"), $("#" + AssessmentType + "_GenericDigestive3More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDigestive8"), $("#" + AssessmentType + "_GenericDigestive8More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDigestive11"), $("#" + AssessmentType + "_GenericDigestive11More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDigestiveLastBM2"), $("#" + AssessmentType + "_GenericDigestiveLastBM2More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDigestiveLastBM3"), $("#" + AssessmentType + "_GenericDigestiveLastBM3More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDigestiveLastBM4"), $("#" + AssessmentType + "_GenericDigestiveLastBM4More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDigestiveOstomy1"), $("#" + AssessmentType + "_GenericDigestiveOstomy1More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDigestiveOstomy2"), $("#" + AssessmentType + "_GenericDigestiveOstomy2More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericDigestiveOstomy3"), $("#" + AssessmentType + "_GenericDigestiveOstomy3More"));
        },
        EmergentCare: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.HideIfRadioEquals(AssessmentType + "_M2300EmergentCare", "00|UK", $("#" + AssessmentType + "_M2310"));
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M2310ReasonForEmergentCareUK"), $(".M2310", Element));
        },
        Endocrine: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.ShowIfRadioEquals(AssessmentType + "_GenericPatientDiabetic", "1", $("#window_" + AssessmentType + " .diabetic"));
            Forms.ShowIfRadioEquals(AssessmentType + "_GenericInsulinDependent", "1", $(AssessmentType + "_GenericInsulinDependentMore"));
            $("#" + AssessmentType + "_GenericBloodSugarSiteText").Autocomplete({ source: ["Thumb", "Index", "Middle", "Forearm", "1st  digit", "2nd  digit", "3rd  digit", "4th digit", "5th digit"] });
        },
        Integumentary: function(AssessmentType, AssessmentTypeNum, Element) {
            if (AssessmentTypeNum % 10 < 5) {
                Oasis.BradenScale(AssessmentType);
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericSkinColor5"), $("#" + AssessmentType + "_GenericSkinColor5More"));
                Forms.ShowIfChecked($("#" + AssessmentType + "_GenericSkinCondition6"), $("#" + AssessmentType + "_GenericSkinCondition6More"));
                Forms.ShowIfRadioEquals(AssessmentType + "_GenericNails", "Problem", $("#" + AssessmentType + "_GenericNailsProblemMore"));
                Forms.ShowIfRadioEquals(AssessmentType + "_GenericPressureRelievingDevice", "1", $("#" + AssessmentType + "_GenericPressureRelievingDeviceMore"));
                Forms.ShowIfSelectEquals($("#" + AssessmentType + "_GenericPressureRelievingDeviceType"), "4", $("#" + AssessmentType + "_GenericPressureRelievingDeviceTypeOther"));
                Forms.ShowIfRadioEquals(AssessmentType + "_GenericIVAccess", "1", $("." + AssessmentType + "_IVAccess"));
                Forms.ShowIfRadioEquals(AssessmentType + "_GenericFlush", "1", $("#" + AssessmentType + "_GenericFlushMore"));
            }
            if (AssessmentTypeNum != 11 && AssessmentTypeNum != 14) {
                if (AssessmentTypeNum % 10 < 5) Forms.HideIfRadioEquals(AssessmentType + "_M1300PressureUlcerAssessment", "00", $("#" + AssessmentType + "_M1302"));
                Forms.HideIfRadioEquals(AssessmentType + "_M1306UnhealedPressureUlcers", "0", $("." + AssessmentType + "_M1306_00"));
                if (AssessmentTypeNum % 10 > 8) Forms.ShowIfRadioEquals(AssessmentType + "_M1307NonEpithelializedStageTwoUlcer", "02", $("#" + AssessmentType + "_M1307NonEpithelializedStageTwoUlcer02More"));
                Forms.HideIfRadioEquals(AssessmentType + "_M1330StasisUlcer", "00|03", $("#" + AssessmentType + "_M1332"));
                Forms.HideIfRadioEquals(AssessmentType + "_M1330StasisUlcer", "00|03", $("#" + AssessmentType + "_M1334"));
                Forms.HideIfRadioEquals(AssessmentType + "_M1340SurgicalWound", "00|02", $("#" + AssessmentType + "_M1342"));
                $("#" + AssessmentType + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent,#" + AssessmentType + "_M1308NumberNonEpithelializedStageFourUlcerCurrent").keyup(function() {
                    var stage3 = $("#" + AssessmentType + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent").val(),
                        stage4 = $("#" + AssessmentType + "_M1308NumberNonEpithelializedStageFourUlcerCurrent").val();
                    if (parseInt(stage3) > 0 || parseInt(stage4) > 0) $("#" + AssessmentType + "_M1310,#" + AssessmentType + "_M1312,#" + AssessmentType + "_M1314").show().each(function() { $(this).val("") });
                    else $("#" + AssessmentType + "_M1310,#" + AssessmentType + "_M1312,#" + AssessmentType + "_M1314").hide().each(function() { $(this).val("0") });
                });
                $("input[name=" + AssessmentType + "_M1306UnhealedPressureUlcers]").change(function() {
                    $("#" + AssessmentType + "_M1308NumberNonEpithelializedStageThreeUlcerCurrent").keyup()
                });
                $("input[name=" + AssessmentType + "_M1306UnhealedPressureUlcers]").change();
            }
            if (AssessmentTypeNum % 10 < 5) {
                $(".WoundType").Autocomplete({ source: ["Trauma", "Pressure Ulcer", "Surgical Wound", "Diabetic Ulcer", "Venous Status Ulcer", "Arterial Ulcer"] });
                $(".DeviceType").Autocomplete({ source: ["J.P.", "Wound Vac", "None"] });
                $(".IVSiteDressingUnit").Autocomplete({ source: ["Normal Saline", "Heparin"] });
            }
        },
        Medications: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.HideIfRadioEquals(AssessmentType + "_M2000DrugRegimenReview", "00|01|NA", $("#" + AssessmentType + "_M2002"));
            Forms.HideIfRadioEquals(AssessmentType + "_M2000DrugRegimenReview", "NA", $("#" + AssessmentType + "_M2010"));
            Forms.HideIfRadioEquals(AssessmentType + "_M2000DrugRegimenReview", "NA", $("#" + AssessmentType + "_M2020"));
            Forms.HideIfRadioEquals(AssessmentType + "_M2000DrugRegimenReview", "NA", $("#" + AssessmentType + "_M2030"));
        },
        Nutrition: function(AssessmentType, AssessmentTypeNum, Element) {
            Oasis.CalculateNutritionScore(AssessmentType);
            $("input[name=" + AssessmentType + "_GenericNutritionalHealth]").change(function() {
                Oasis.CalculateNutritionScore(AssessmentType)
            });
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNutrition4"), $("#" + AssessmentType + "_GenericNutrition4More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNutrition5"), $("#" + AssessmentType + "_GenericNutrition5More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNutrition6"), $("#" + AssessmentType + "_GenericNutrition6More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNutrition8"), $("#" + AssessmentType + "_GenericNutrition8More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNutrition9"), $("#" + AssessmentType + "_GenericNutrition9More"));
            Forms.EnableIfChecked($("#" + AssessmentType + "_485NutritionalReqs6"), $("#" + AssessmentType + "_485NutritionalReqs6More"));
            Forms.EnableIfChecked($("#" + AssessmentType + "_485NutritionalReqs8"), $("#" + AssessmentType + "_485NutritionalReqs8More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_485NutritionalReqs12"), $("#" + AssessmentType + "_485NutritionalReqs12More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_485NutritionalReqs13"), $("#" + AssessmentType + "_485NutritionalReqs13More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_485NutritionalReqs14"), $("#" + AssessmentType + "_485NutritionalReqs14More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_485NutritionalReqs15"), $("#" + AssessmentType + "_485NutritionalReqs15More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_485NutritionalReqsEnteralPer4"), $("#" + AssessmentType + "_485NutritionalReqsEnteralPer4More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_485NutritionalReqs16"), $("#" + AssessmentType + "_485NutritionalReqs16More"));
        },
        NeuroBehavioral: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNeurologicalStatus1"), $("#" + AssessmentType + "_GenericNeurologicalStatus1More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNeurologicalStatus2"), $("." + AssessmentType + "_GenericNeurologicalStatus2More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNeurologicalStatus3"), $("#" + AssessmentType + "_GenericNeurologicalStatus3More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNeurologicalStatus4"), $("#" + AssessmentType + "_GenericNeurologicalStatus4More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNeurologicalStatus5"), $("#" + AssessmentType + "_GenericNeurologicalStatus5More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNeurologicalStatus9"), $("#" + AssessmentType + "_GenericNeurologicalStatus9More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_485MentalStatus8"), $("#" + AssessmentType + "_485MentalStatus8More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericBehaviour8"), $("#" + AssessmentType + "_GenericBehaviour8More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericPsychosocial5"), $("#" + AssessmentType + "_GenericPsychosocial5More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_485PatientStrengths5"), $("#" + AssessmentType + "_485PatientStrengths5More"));
            Forms.ShowIfRadioEquals(AssessmentType + "_M1730DepressionScreening", "01", $("#" + AssessmentType + "_Phq2"));
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1740CognitiveBehavioralPsychiatricSymptomsNone"), $(AssessmentType + "_M1740 .M1740"));
        },
        OrdersDiscipline: function(AssessmentType, AssessmentTypeNum, Element) {
            $("#" + AssessmentType + "_485SIVerbalizedUnderstandingPercent").Autocomplete({ source: ["GOOD", "NO", "FAIR", "POOR", "0%", "100%", "50%", "FULL", "PARTIAL"] })
        },
        PatientHistory: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.ShowIfRadioEquals(AssessmentType + "_485Allergies", "Yes", $("#" + AssessmentType + "_485AllergiesDescription"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_M1000InpatientFacilitiesOTHR"), $("#" + AssessmentType + "_M1000InpatientFacilitiesOTHRMore"));
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1000InpatientFacilitiesNone"), $("#" + AssessmentType + "_M1000 .M1000"));
            Forms.HideIfChecked($("#" + AssessmentType + "_M1000InpatientFacilitiesNone"), $("#" + AssessmentType + "_M1005").closest("fieldset"));
            Forms.HideIfChecked($("#" + AssessmentType + "_M1000InpatientFacilitiesNone"), $("#" + AssessmentType + "_M1010").closest("fieldset"));
            Forms.HideIfChecked($("#" + AssessmentType + "_M1000InpatientFacilitiesNone"), $("#" + AssessmentType + "_M1012").closest("fieldset"));
            Forms.HideIfChecked($("#" + AssessmentType + "_M1005InpatientDischargeDateUnknown"), $("#" + AssessmentType + "_M1005InpatientDischargeDate").parent());
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1012InpatientFacilityProcedureCodeNotApplicable"), $("#" + AssessmentType + "_M1012InpatientFacilityProcedureCodeUnknown"));
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1012InpatientFacilityProcedureCodeUnknown"), $("#" + AssessmentType + "_M1012InpatientFacilityProcedureCodeNotApplicable"));
            Forms.HideIfChecked($("#" + AssessmentType + "_M1012InpatientFacilityProcedureCodeNotApplicable"), $("#" + AssessmentType + "_M1012 .sm-diagnoses-list"));
            Forms.HideIfChecked($("#" + AssessmentType + "_M1012InpatientFacilityProcedureCodeUnknown"), $("#" + AssessmentType + "_M1012 .sm-diagnoses-list"));
            Forms.HideIfChecked($("#" + AssessmentType + "_M1016MedicalRegimenDiagnosisNotApplicable"), $("#" + AssessmentType + "_M1016 .sm-diagnoses-list"));
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1018ConditionsPriorToMedicalRegimenNone"), $("#" + AssessmentType + "_M1018 .M1018"));
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1018ConditionsPriorToMedicalRegimenNA"), $("#" + AssessmentType + "_M1018 .M1018"));
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1018ConditionsPriorToMedicalRegimenUK"), $("#" + AssessmentType + "_M1018 .M1018"));
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1030HomeTherapiesNone"), $("#" + AssessmentType + "_M1030 .M1030"));
            $("#" + AssessmentType + "_OasisDiagnoses").DiagnosesList()
        },
        Prognosis: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.ShowIfChecked($("#" + AssessmentType + "_485AdvancedDirectivesIntentOther"), $("#" + AssessmentType + "_485AdvancedDirectivesIntentOtherMore"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_485FunctionLimitationsB"), $("#" + AssessmentType + "_485FunctionLimitationsBMore"));
        },
        Respiratory: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratoryCondition2"), $("#" + AssessmentType + "_GenericRespiratoryCondition2More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratoryCondition3"), $("#" + AssessmentType + "_GenericRespiratoryCondition3More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratorySounds1"), $("#" + AssessmentType + "_GenericLungSoundsCTAText"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratorySounds2"), $("#" + AssessmentType + "_GenericLungSoundsRalesText"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratorySounds3"), $("#" + AssessmentType + "_GenericLungSoundsRhonchiText"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratorySounds4"), $("#" + AssessmentType + "_GenericLungSoundsWheezesText"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratorySounds5"), $("#" + AssessmentType + "_GenericLungSoundsCracklesText"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratorySounds6"), $("#" + AssessmentType + "_GenericLungSoundsDiminishedText"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratorySounds7"), $("#" + AssessmentType + "_GenericLungSoundsAbsentText"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratorySounds8"), $("#" + AssessmentType + "_GenericLungSoundsStridorText"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratoryCondition4"), $("#" + AssessmentType + "_GenericRespiratoryCondition4More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratoryCondition5"), $("#" + AssessmentType + "_GenericRespiratoryCondition5More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericRespiratoryCondition6"), $("#" + AssessmentType + "_GenericNebulizerText"));
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1410HomeRespiratoryTreatmentsNone"), $("#" + AssessmentType + "_M1410 .M1410"));
        },
        RiskAssessment: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1032HospitalizationRiskNone"), $("#" + AssessmentType + "_M1032 .M1032"));
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1036RiskFactorsNone"), $("#" + AssessmentType + "_M1036 .M1036"));
            Forms.NoneOfTheAbove($("#" + AssessmentType + "_M1036RiskFactorsUnknown"), $("#" + AssessmentType + "_M1036 .M1036"));
        },
        Sensory: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericEyes13"), $("#" + AssessmentType + "_GenericEyes13More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericEars2"), $("#" + AssessmentType + "_GenericEars2More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericEars6"), $("#" + AssessmentType + "_GenericEars6More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericEars7"), $("#" + AssessmentType + "_GenericEars7More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNose4"), $("#" + AssessmentType + "_GenericNose4More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericNose5"), $("#" + AssessmentType + "_GenericNose5More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericMouth5"), $("#" + AssessmentType + "_GenericMouth5More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericThroat4"), $("#" + AssessmentType + "_GenericThroat4More"));
        },
        SupportiveAssistance: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericHazardsIdentified9"), $("#" + AssessmentType + "_GenericHazardsIdentified9More"));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericUseOfInterpreter4"), $("#" + AssessmentType + "_GenericUseOfInterpreter4More"));
            Forms.ShowIfRadioEquals(AssessmentType + "_GenericCulturalPractices", "1", $("#" + AssessmentType + "_GenericCulturalPracticesYesMore"));
            Forms.ShowIfRadioEquals(AssessmentType + "_GenericIsHomeBound", "Yes", $(".HomeBound", Element));
            Forms.ShowIfChecked($("#" + AssessmentType + "_GenericHomeBoundReason7"), $("#" + AssessmentType + "_GenericHomeBoundReason7More"));
            Lookup.language("#StartOfCare_GenericPrimaryLanguage");
        },
        TherapyNeed: function(AssessmentType, AssessmentTypeNum, Element) {
            Forms.HideIfChecked($("#" + AssessmentType + "_M2200TherapyNeedNA"), $("#" + AssessmentType + "_M2200Right"));
        },
        TransferDischargeDeath: function(AssessmentType, AssessmentTypeNum, Element) {
            if (AssessmentTypeNum == 9) $("input[name=" + AssessmentType + "_M2410TypeOfInpatientFacility]").click(function() {
                $("input[name=" + AssessmentType + "_M2410TypeOfInpatientFacility]").eq(5).prop("checked", true)
            });
            else {
                Forms.HideIfRadioEquals(AssessmentType + "_M2410TypeOfInpatientFacility", "02|03|04", $("#" + AssessmentType + "_M2430"));
                Forms.HideIfRadioEquals(AssessmentType + "_M2410TypeOfInpatientFacility", "01|02|04", $("#" + AssessmentType + "_M2440"));
            }
        }
    }
}

var StartOfCare = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("StartOfCare", control.closest("form"), control, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}
var ResumptionOfCare = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("ResumptionOfCare", control.closest("form"), control, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var Recertification = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("Recertification", control.closest("form"), control, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var FollowUp = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("FollowUp", control.closest("form"), control, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var TransferInPatientNotDischarged = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("TransferInPatientNotDischarged", control.closest("form"), control, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var TransferInPatientDischarged = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("TransferInPatientDischarged", control.closest("form"), control, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var DischargeFromAgencyDeath = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("DischargeFromAgencyDeath", control.closest("form"), control, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var DischargeFromAgency = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("DischargeFromAgency", control.closest("form"), control, action); },
    Load: function(Id, PatientId, AssessmentType) {
        Oasis.Load(Id, PatientId, AssessmentType);
    }
}

var NonOasisStartOfCare = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("NonOasisStartOfCare", control.closest("form"), control, action); },
    Load: function(id, patientId, assessmentType) {
        Acore.Open("NonOasisStartOfCare", "Oasis/NonOasisStartOfCare", function() {
            Oasis.InitTabs(id, patientId, assessmentType)
        }, {
            Id: id,
            PatientId: patientId,
            AssessmentType: assessmentType
        })
    },
    Submit: function(id, patientId, episodeId, assessmentType) {
        U.PostUrl("Oasis/Submit", {
            Id: id,
            patientId: patientId,
            episodeId: episodeId,
            assessmentType: assessmentType,
            actionType: "Submit",
            reason: ""
        }, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Rebind();
                Schedule.Rebind();
                Agency.RebindCaseManagement();
                UserInterface.CloseWindow("NonOasisStartOfCare");
            } else U.Growl(data.errorMessage, "error");
        })
    }
}

var NonOasisRecertification = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("NonOasisRecertification", control.closest("form"), control, action); },
    Load: function(id, patientId, assessmentType) {
        Acore.Open("NonOasisRecertification", "Oasis/NonOasisRecertification", function() {
            Oasis.InitTabs(id, patientId, assessmentType)
        }, {
            Id: id,
            PatientId: patientId,
            AssessmentType: assessmentType
        })
    },
    Submit: function(id, patientId, episodeId, assessmentType) {
        U.PostUrl("Oasis/Submit", {
            Id: id,
            patientId: patientId,
            episodeId: episodeId,
            assessmentType: assessmentType,
            actionType: "Submit",
            reason: ""
        }, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Rebind();
                Schedule.Rebind();
                Agency.RebindCaseManagement();
                UserInterface.CloseWindow("NonOasisRecertification");
            } else U.Growl(data.errorMessage, "error");
        })
    }
}

var NonOasisDischarge = {
    FormSubmit: function(control, action) { control.closest("form").validate(); U.HandlerHelperTemplate("NonOasisDischarge", control.closest("form"), control, action); },
    Load: function(id, patientId, assessmentType) {
        Acore.Open("NonOasisDischarge", "Oasis/NonOasisDischarge", function() {
            Oasis.InitTabs(id, patientId, assessmentType)
        }, {
            Id: id,
            PatientId: patientId,
            AssessmentType: assessmentType
        })
    },
    Submit: function(id, patientId, episodeId, assessmentType) {
        U.PostUrl("Oasis/Submit", {
            Id: id,
            patientId: patientId,
            episodeId: episodeId,
            assessmentType: assessmentType,
            actionType: "Submit",
            reason: ""
        }, function(data) {
            if (data.isSuccessful) {
                U.Growl(data.errorMessage, "success");
                Patient.Rebind();
                Schedule.Rebind();
                Agency.RebindCaseManagement();
                UserInterface.CloseWindow("NonOasisDischarge");
            } else U.Growl(data.errorMessage, "error");
        })
    }
}