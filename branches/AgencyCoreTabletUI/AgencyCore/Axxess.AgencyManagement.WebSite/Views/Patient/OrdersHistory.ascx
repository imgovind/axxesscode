﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Patient Order History | <%= Model.DisplayName.Clean() %></span>
<div class="wrapper main blue">
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    <div class="float-right">
        <div class="buttons">
            <ul>
                <li><%= Html.ActionLink("Excel Export", "PatientOrdersHistory", "Export", new { patientId = Model.Id ,StartDate=DateTime.Now.AddDays(-59), EndDate=DateTime.Now}, new { @id = "PatientOrdersHistory_ExportLink", @class = "excel" })%></li>
            </ul>
        </div>
    </div>
<%  } %>
    <fieldset class="grid-controls">
        <%= Html.Hidden("PatientOrdersHistory_PatientId", Model.Id, new { @id = "PatientOrdersHistory_PatientId" })%>
        <label class="strong" for="PatientOrdersHistory_StartDate">Date Range:</label>
        <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="PatientOrdersHistory_StartDate" class="short" />
        <label class="strong" for="PatientOrdersHistory_EndDate">To</label>
        <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="PatientOrdersHistory_EndDate" class="short" />
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Patient.RebindOrdersHistory();return false">Generate</a></li>
            </ul>
        </div>
    </fieldset>
    <%= Html.Telerik().Grid<Order>().Name("List_PatientOrdersHistory").HtmlAttributes(new { @class = "bottom-bar" }).DataKeys(keys => {
            keys.Add(o => o.Id).RouteKey("id");
        }).Columns(columns => {
            columns.Bound(o => o.Number).Title("Order Number").Width(110).Sortable(false).ReadOnly();
            columns.Bound(o => o.Text).Title("Type").Sortable(false).ReadOnly();
            columns.Bound(o => o.StatusName).Title("Status").Sortable(false).ReadOnly();
            columns.Bound(o => o.PhysicianName).Title("Physician").Width(150).Sortable(false).ReadOnly();
            columns.Bound(o => o.OrderDate).Title("Order Date").Width(90).Sortable(true).ReadOnly();
            columns.Bound(o => o.SendDateFormatted).Title("Sent Date").Width(90).Sortable(false);
            columns.Bound(o => o.ReceivedDateFormatted).Title("Received Date").Width(100).Sortable(false);
            columns.Bound(o => o.PrintUrl).Title(" ").Width(100).Sortable(false);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersHistoryList", "Patient", new { patientId = Model.Id, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Pageable(paging => paging.PageSize(50)).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>