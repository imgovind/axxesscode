﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<span class="wintitle">Physician Face-to-face Encounter | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("Add", "FaceToFaceEncounter", FormMethod.Post, new { @id = "newFaceToFaceEncounterForm" })) { %>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Physician Face-to-Face Encounter</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <label for="New_FaceToFaceEncounter_PatientName" class="float-left">Patient Name:</label>
                        <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", !Model.IsEmpty() ? Model.ToString() : "" , new { @id = "New_FaceToFaceEncounter_PatientName", @class="required notzero" })%></div>
                    </div>
                    <div class="row">
                        <label for="New_FaceToFaceEncounter_EpisodeId" class="float-left">Episode Associated:</label>
                        <div class="float-right"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_FaceToFaceEncounter_EpisodeId", @class = "required notzero" })%></div>
                    </div>
                    <div class="row">
                        <label for="New_FaceToFaceEncounter_Date" class="float-left">Request Date:</label>
                        <div class="float-right"><input type="date" name="RequestDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now %>" id="New_FaceToFaceEncounter_Date" class="required" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <label for="New_FaceToFaceEncounter_PhysicianDropDown" class="float-left">Physician:</label>
                        <div class="float-right">
                            <%= Html.TextBox("PhysicianId", "", new { @id = "New_FaceToFaceEncounter_PhysicianDropDown", @class = "physicians requiredphysician" })%>
                            <br />
                            <div class="button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ol>
        <li>
            <div class="wrapper">
                <div class="wide-column">
                        <div class="row">
                            <div class="checkgroup">
                                <div class="option">
                                    <%= Html.RadioButton("Certification", "1",true, new { @id = "New_FaceToFaceEncounter_Certification1", @class = "radio float-left" })%>
                                    <label for="New_FaceToFaceEncounter_Certification1" class="radio">POC Certifying Physician</label>
                                </div>                                    
                                <div class="option">
                                    <%= Html.RadioButton("Certification", "2", new { @id = "New_FaceToFaceEncounter_Certification2", @class = "radio float-left" })%>
                                    <label for="New_FaceToFaceEncounter_Certification2" class="radio">Non POC Certifying Physician</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <em><strong> * Note:</strong> Completing this document creates a Physician Face to Face Encounter request document that must be submitted to the physician. The physician will be required to certify that the patient is homebound and the home health services provided are medically necessary. The physician will certify by signing the face to face encounter document and returning to the home health agency.</em>
                        </div>
                    </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <%= Html.Hidden("Status", "", new { @id = "New_FaceToFaceEncounter_Status" })%>
    <div class="buttons">
        <ul>
            <li><a onclick="$('#New_FaceToFaceEncounter_Status').val('115')" class="save">Submit</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    Schedule.loadEpisodeDropDown('New_FaceToFaceEncounter_EpisodeId', $('#New_FaceToFaceEncounter_PatientName'));
    $('#New_FaceToFaceEncounter_PatientName').change(function() { Schedule.loadEpisodeDropDown('New_FaceToFaceEncounter_EpisodeId', $(this)); }); 
</script>