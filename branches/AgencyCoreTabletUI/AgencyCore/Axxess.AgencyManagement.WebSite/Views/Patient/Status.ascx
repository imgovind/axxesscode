﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("UpdateStatus", "Patient", FormMethod.Post, new { @id = "changePatientStatusForm" })) { %>
<%= Html.Hidden("Id", Model.Id)%>
    <div class="wrapper main">
        <div class="align-center"><span class="bigtext"><%= Model.DisplayName %></span></div>
        <% if (Model.IsDischarged) { %>
        <fieldset> 
            <div class="column"><div class="row"><label for="Change_PatientStatus_Status" class="float-left">Status:</label><div class="float-right"><%= Html.StatusPatients("Status", "2", new { @id = "Change_PatientStatus_Status", @class = "" })%></div></div></div>
            <div id="activePatientContainer"><div class="wide-column align-center"><div class="row"><label><b>This patient will be added to your list of active patients. If the patient to be readmitted you have to exit and readmit with a new start of care but reactivating will take the last start of care date. <br /><br />Are you sure you want to activate this patient?</b></label></div></div></div>
        </fieldset>
        <div class="buttons">
            <ul>
                <li><a id="ChangeStatus_SaveButton" class="save">Update</a></li>
                <li><a id="ChangeStatus_CancelButton" class="close">Exit</a></li>
            </ul>
        </div>
        <% } else { %>
        <fieldset><div class="column"><div class="row"><label for="Change_PatientStatus_Status" class="float-left">Status:</label><div class="float-right"><%= Html.StatusPatients("Status", "1", new { @id = "Change_PatientStatus_Status", @class = "" })%></div></div></div></fieldset>
        <div id="dischargeContainer" class="hidden">
            <fieldset>
                <legend>Discharge Information</legend>
                <table class="form"><tbody>   
                    <tr><td><label for="ChangeStatus_DischargeDate" class="bold">Discharge Date:</label><br /><input type="date" name="DateOfDischarge" value="<%= DateTime.Today.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="ChangeStatus_DischargeDate" class="text required" /></td></tr>        
                    <tr class="linesep vert"><td><label for="Comment"><strong>Reason for Discharge:</strong></label><div><%= Html.TextArea("Comments", "")%></div></td></tr>
                </tbody></table>
                <div class="wide-column align-center"><div class="row"><label><b>This patient will be added to your list of discharged patients. To provide services to this patient in the future, you will have to re-admit the patient. <br /><br />Are you sure you want to discharge this patient?</b></label></div></div>
            </fieldset>
        </div>
        <% Html.RenderPartial("NonAdmission"); %>
        <div class="buttons">
            <ul>
                <li><a id="ChangeStatus_SaveButton" class="save">Update</a></li>
                <li><a id="ChangeStatus_CancelButton" class="close">Exit</a></li>
            </ul>
        </div>
         <% } %>
    </div>
<% } %>
<script type="text/javascript">
    function displayStatusContainer(statusId) {
        if (statusId == "1") {
            $("#activePatientContainer").show();
            $("#nonAdmissionContainer").hide();
            $("#ChangeStatus_SaveButton").text("Yes, Activate");
            $("#ChangeStatus_CancelButton").text("No, Exit");
        }
       else if (statusId == "2") {
            $("#dischargeContainer").show();
            $("#nonAdmissionContainer").hide();
            $("#activePatientContainer").hide();
            $("#ChangeStatus_SaveButton").text("Yes, Discharge");
            $("#ChangeStatus_CancelButton").text("No, Exit");

            $("#ChangeStatus_DischargeDate").removeClass('required').addClass('required');
            $("#ChangeStatus_NonAdmitDate").removeClass('required');
            $("input[name=Reason]").removeClass('required');
        } else if (statusId == "4") {
            $("#nonAdmissionContainer").show();
            $("#dischargeContainer").hide();
            $("#activePatientContainer").hide();

            $("#ChangeStatus_SaveButton").text("Yes, Non-Admit");
            $("#ChangeStatus_CancelButton").text("No, Exit");
            
            $("#ChangeStatus_DischargeDate").removeClass('required');
            $("#ChangeStatus_NonAdmitDate").removeClass('required').addClass('required');
            $("input[name=Reason]").removeClass('required').addClass('required');
        } else {
            $("#nonAdmissionContainer").hide();
            $("#dischargeContainer").hide();
            $("#activePatientContainer").hide();

            $("#ChangeStatus_SaveButton").text("Yes, Pending");
            $("#ChangeStatus_CancelButton").text("No, Exit");
            
            $("#ChangeStatus_DischargeDate").removeClass('required');
            $("#ChangeStatus_NonAdmitDate").removeClass('required');
            $("input[name=Reason]").removeClass('required');
        }
    }
    $("#Change_PatientStatus_Status").change(function() {
        displayStatusContainer($(this).val());
    });
    displayStatusContainer($("#Change_PatientStatus_Status").val());
</script>