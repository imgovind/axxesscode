﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Patients | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main">
    <%= Html.Telerik().Grid<PatientData>().Name("List_Patient_Grid").HtmlAttributes(new { @class = "top-gap bottom-bar" }).ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(p => p.PatientIdNumber).Title("MR#").Width(90);
            columns.Bound(p => p.DisplayName).Title("Name").Width(180);
            columns.Bound(p => p.Address).Title("Address").Sortable(false);
            columns.Bound(p => p.DateOfBirth).Title("Date of Birth").Width(100).Sortable(true);
            columns.Bound(p => p.Gender).Width(80).Sortable(true);
            columns.Bound(p => p.Phone).Title("Phone").Width(120).Sortable(false);
            columns.Bound(p => p.Status).Title("Status").Width(80).Sortable(true);
            columns.Bound(p => p.Id).Width(90).Sortable(false).ClientTemplate("<a onclick=\"UserInterface.ShowEditPatient('<#=Id#>');return false\">Edit</a> | <a onclick=\"Patient.Delete('<#=Id#>');return false\" class=\"deletePatient\">Delete</a>").Title("Action");
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Patient")).ClientEvents(events => events.OnDataBound("Patient.BindGridButton")).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
<%  if (Current.HasRight(Permissions.ManagePatients)) { %>
    $("#List_Patient_Grid .t-grid-toolbar").append(
        $("<div/>").addClass("fl").Buttons([ { Text: "New Patient", Click: UserInterface.ShowNewPatient } ])
    );
<%  } %>
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("#List_Patient_Grid .t-grid-toolbar").append(
        $("<div/>").addClass("fr").Buttons([ { Text: "Excel Export", Click: function() { U.GetAttachment("Export/Patients", {}) } } ])
    );
<% } %>
</script>
