﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="winmenu">
    <ul>
        <% if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %><li><a onclick="Patient.LoadNewOrder('<%=Model.Id %>');return false">New Order</a></li><% } %>
        <li><a onclick="Patient.LoadNewCommunicationNote('<%=Model.Id %>');return false">New Communication Note</a></li>
        <% if (Current.HasRight(Permissions.ManageInsurance)) { %><li><a onclick="Patient.LoadNewAuthorization('<%=Model.Id %>');return false">New Authorization</a></li><% } %>
        <% if (Current.HasRight(Permissions.ScheduleVisits)) { %><li><a onclick="UserInterface.ShowMultipleReassignModal('<%= Guid.Empty %>', '<%= Model.Id %>','Patient');return false" status="Reassign Schedules">Reassign Schedules</a></li><%}%>
    </ul>
</div>
<fieldset class="patient-summary fl">
    <div class="abs-left">
        <%  if (!Model.PhotoId.IsEmpty()) { %>
        <img src="/Asset/<%= Model.PhotoId %>" alt="User Photo" />
        <%  } else { %>
        <img src="/Images/blank_user.jpeg" alt="User Photo" />
        <%  } %>
        <%  if (Current.HasRight(Permissions.ManagePatients)) { %>
        <div class="ac">[ <a onclick="UserInterface.ShowNewPhotoModal('<%= Model.Id%>');return false">Change Photo</a> ]</div>
        <%  } %>
    </div>
    <div class="buttons abs-right">
        <ul><li><a onclick="Patient.loadInfoAndActivity('<%= Model.Id %>');return false">Refresh</a></li></ul>
        <%  if (Current.HasRight(Permissions.ManagePatients)) { %>
        <br />
        <ul><li><a onclick="UserInterface.ShowChangeStatusModal('<%= Model.Id%>');return false">Change Status</a></li></ul>
        <%  } %>
        <%  if (Current.HasRight(Permissions.ManagePatients) && Model.IsDischarged) { %>
        <br />
        <ul><li><a onclick="UserInterface.ShowReadmitPatientModal('<%= Model.Id%>');return false">Re-Admit</a></li></ul>
        <%  } %>
    </div>
    <div class="column">
        <div class="row ac"><span class="bigtext"><%= Model.DisplayName  %></span></div>    
        <div class="row">
            <label class="fl strong">MR #:</label>
            <div class="fr"><%= Model.PatientIdNumber %></div>
        </div>    
        <div class="row">
            <label class="fl strong">Birthday:</label>
            <div class="fr"><%= Model.DOB.ToString("MMMM dd, yyyy") %></div>
        </div>    
        <div class="row">
            <label class="fl strong">Start of Care Date:</label>
            <div class="fr"><%= Model.StartofCareDate.ToString("MMMM dd, yyyy")%></div>
        </div>    
        <%  if (Model.PhoneHome.IsNotNullOrEmpty()) { %>
        <div class="row">
            <label class="fl strong">Primary Phone:</label>
            <div class="fr"><%= Model.PhoneHome.ToPhone() %></div>
        </div>
        <%  } %>
        <%  var physician = Model.PhysicianContacts != null && Model.PhysicianContacts.Count > 0 ? Model.PhysicianContacts.SingleOrDefault(p => p.Primary) : null; %>
        <%  if (physician != null) { %>
        <div class="row">
            <label class="fl strong">Physician Name:</label>
            <div class="fr"><%= physician != null ? physician.DisplayName.Trim() : string.Empty %></div>
        </div>    
        <%  } %>
        <div class="row ac">
        <%  if (Current.HasRight(Permissions.ManagePatients)) { %>
            [ <a onclick="UserInterface.ShowEditPatient('<%= Model.Id%>');return false">Edit</a> ]&#160;
        <%  } %>
            [ <a onclick="Patient.InfoPopup('<%= Model.Id %>');return false">More</a> ]&#160;
            [ <a href="http://<%= Request.ServerVariables["HTTP_HOST"] %>/Map/Google/<%= Model.Id %>" target="_blank">Directions</a> ]&#160;
        </div>
    </div>
</fieldset>
<div class="quick reports fr">
    <div class="reports-head">
        <h5>Quick Reports</h5>
    </div>
    <ul>
        <li><a onclick="Acore.OpenPrintView({ Url: '/Patient/Profile/<%=Model.Id %>', PdfUrl: '/Patient/PatientProfilePdf', PdfData: { 'id': '<%=Model.Id %>' } });return false">Patient Profile</a></li>
        <li><a onclick="Patient.loadMedicationProfile('<%= Model.Id%>');return false">Medication Profile(s)</a></li>
        <li><a onclick="UserInterface.ShowPatientAllergies('<%= Model.Id%>');return false">Allergy Profile</a></li>
        <%  if (Current.HasRight(Permissions.ManageInsurance)) { %>
        <li><a onclick="UserInterface.ShowPatientAuthorizations('<%= Model.Id%>');return false">Authorizations Listing</a></li>
        <%  } %>
        <li><a onclick="Patient.loadPatientCommunicationNotes('<%= Model.Id%>');return false">Communication Notes</a></li>
        <li><a onclick="UserInterface.ShowPatientOrdersHistory('<%= Model.Id%>');return false">Orders And Care Plans</a></li>
        <li><a onclick="UserInterface.ShowPatientSixtyDaySummary('<%= Model.Id%>');return false">60 Day Summaries</a></li>
        <li><a onclick="UserInterface.ShowPatientVitalSigns('<%= Model.Id%>');return false">Vital Signs Log</a></li>
        <li><a onclick="Acore.OpenPrintView({ Url: '/Patient/TriageClassification/<%=Model.Id %>' });return false">Triage Classification</a></li>
        <%  if (Current.HasRight(Permissions.DeleteTasks)) { %>
        <li><a onclick="UserInterface.ShowDeletedTaskHistory('<%= Model.Id%>');return false">Deleted Tasks/Documents</a></li>
        <%  } %>
    </ul>
</div>
<%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
<div class="buttons fl">
    <ul>
        <li><a onclick="UserInterface.ShowScheduleCenter('<%= Model.Id %>');return false">Schedule Activity</a></li>
    </ul>
</div>
<%  } %>
<fieldset class="legend fl">
    <ul>
        <li><span class="img icon note"></span>Visit Comments</li>
        <li><span class="img icon note-blue"></span>Episode Comments</li>
        <li><span class="img icon note-red"></span>Missed/Returned</li>
    </ul>
</fieldset>