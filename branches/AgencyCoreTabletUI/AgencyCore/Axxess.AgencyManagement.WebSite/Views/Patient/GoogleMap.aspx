﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Patient>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Axxess&#8482; Technology Solutions - Driving Directions to Patient Powered by Google&#174;</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group
        .Add("globals.css")
        .Add("forms.css")
        .Add("maps.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion))
    %>
</head>
<body>
    <div class="wrapper layout">
        <div class="ui-layout-west">
            <div class="navigation">
                <div class="start address-block">
                    <div>
                        <label class="strong">Starting Address:</label><input name="start" type="hidden" value="" />
                        <%= Html.LookupSelectList(SelectListTypes.MapAddress,"startaddress_select",Current.UserAddress, new {  @id = "map-start-address-select", @class = "float-right required" }) %>
                        <div class="clear"></div>
                    </div>
                    <div id="map-start-addressess">
                        <input type="text" id="map-start-address" value="" class="mapaddr" /><br />
                        <input type="text" id="map-start-address-city" value="" class="mapcity" />
                        <%= Html.LookupSelectList(SelectListTypes.States, "startstate", Model.AddressStateCode, new { @id = "map-start-address-city", @class = "mapcity" })%>
                        <%= Html.TextBox("startzip", "", new { @id = "map-start-address-zip", @class = "mapzip", @maxlength = "5" })%>
                    </div>
                </div><div class="end address-block">
                    <div><label class="strong">Patient&#8217;s Address</label><input name="end" type="hidden" value="<%= Model.AddressFull %>" /></div> 
                    <div>
                        <input type="text" id="map-end-address" value="<%= Model.AddressLine1 %> <%= Model.AddressLine2 %>" class="mapaddr" /><br />
                        <input type="text" id="map-end-address-city" value="<%= Model.AddressCity %>" class="end mapcity" />
                        <%= Html.LookupSelectList(SelectListTypes.States, "endstate", Model.AddressStateCode, new { @id = "map-end-address-state", @class = "mapcity" })%>
                        <%= Html.TextBox("endzip", Model.AddressZipCode, new { @id = "map-end-address-zip", @class = "mapzip", @maxlength = "5" })%>
                    </div>
                </div>
                <div class="buttons">
                    <ul>
                        <li><a id="map-recalculate">Recalculate Directions</a></li>
                    </ul>
                </div>
            </div>
            <div id="directions"></div>
        </div><div class="ui-layout-center">
            <div id="map"></div>
        </div>
    </div>
    <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Scripts/Modules/<%= AppSettings.UseMinifiedJs ? "min/" : "" %>Map.js"></script>
    <script type="text/javascript">
        GoogleMap.init($("#map-start-address-select option:selected").val(), $("input[name=end]").val());
    </script>
</body>
</html>
