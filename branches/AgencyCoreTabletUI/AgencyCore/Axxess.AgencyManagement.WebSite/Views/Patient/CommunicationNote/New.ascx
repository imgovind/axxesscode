﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<span class="wintitle">New Communication Note | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("Add", "CommunicationNote", FormMethod.Post, new { @id = "newCommunicationNoteForm" })) { %>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Communication Note</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <label class="float-left" for="New_CommunicationNote_PatientName">Patient Name:</label>
                        <div class="fr"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", (!Model.IsEmpty()) ? Model.ToString() : "", new { @id = "New_CommunicationNote_PatientName", @class="required notzero" })%></div>
                    </div>
                    <div class="row">
                        <label class="float-left" for="New_CommunicationNote_EpisodeList">Episode Associated:</label>
                        <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_CommunicationNote_EpisodeList", @class = "required notzero" })%></div>
                    </div>
                    <div class="row">
                        <label class="float-left" for="New_CommunicationNote_Date">Date:</label>
                        <div class="fr"><input type="date" name="Created" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="New_CommunicationNote_Date" class="required" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <label class="float-left" for="New_CommunicationNote_PhysicianDropDown">Physician:</label>
                        <div class="fr">
                            <%= Html.TextBox("PhysicianId", "", new { @id = "New_CommunicationNote_PhysicianDropDown", @class = "physicians" })%>
                            <br />
                            <div class="button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ol>
        <li>
            <div class="wrapper">
                <div class="wide-column">
                    <label for="New_CommunicationNote_Templates" class="strong">Communication Text</label>
                    <%= Html.Templates("New_CommunicationNote_Templates", new { @class = "templates mobile_fr", @template = "#New_CommunicationNote_Text" })%>
                    <div class="align-center">
                        <%= Html.TextArea("Text", string.Empty, 8, 20, new { @class = "fill", @id = "New_CommunicationNote_Text", @maxcharacters = "1500" })%>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <div class="checkgroup">
                            <div class="option">
                                <%= Html.CheckBox("SendAsMessage", false, new { @id = "New_CommunicationNote_SendAsMessage", @class = "bigradio float-left radio" })%>
                                <label for="New_CommunicationNote_SendAsMessage" class="radio">Send note as Message:</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="New_CommunicationNote_Recipients"><%= Html.Recipients("New_CommunicationNote", new List<Guid>()) %></div>
                    </div>
                </div>
            </div>
        </li>
    </ol>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Electronic Signature</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label for="New_CommunicationNote_ClinicianSignature" class="float-left">Staff Signature:</label>
                        <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "New_CommunicationNote_ClinicianSignature" })%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label for="New_CommunicationNote_ClinicianSignatureDate" class="float-left">Signature Date:</label>
                        <div class="float-right"><input type="date" name="SignatureDate" id="New_CommunicationNote_ClinicianSignatureDate" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <%= Html.Hidden("Status", "", new { @id = "New_CommunicationNote_Status" })%>
    <div class="buttons">
        <ul>
            <li><a onclick="NewCommNoteRemove();$('#New_CommunicationNote_Status').val('415')" class="save">Save</a></li>
            <li><a onclick="NewCommNoteAdd();$('#New_CommunicationNote_Status').val('420')" class="save">Complete</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    $("#New_CommunicationNote_Recipients").hide();
    $("#New_CommunicationNote_SendAsMessage").click(function() { $("#New_CommunicationNote_Recipients").toggle(); });
    Schedule.loadEpisodeDropDown('New_CommunicationNote_EpisodeList', $('#New_CommunicationNote_PatientName'));
    $('#New_CommunicationNote_PatientName').change(function() { Schedule.loadEpisodeDropDown('New_CommunicationNote_EpisodeList', $(this)); });
    function NewCommNoteAdd() {
        $("#New_CommunicationNote_ClinicianSignature").removeClass('required').addClass('required');
        $("#New_CommunicationNote_ClinicianSignatureDate").removeClass('required').addClass('required');
    }
    function NewCommNoteRemove() {
        $("#New_CommunicationNote_ClinicianSignature").removeClass('required');
        $("#New_CommunicationNote_ClinicianSignatureDate").removeClass('required');
    }
</script>
