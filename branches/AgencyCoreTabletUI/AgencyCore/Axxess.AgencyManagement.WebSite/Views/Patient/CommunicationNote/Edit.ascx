﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CommunicationNote>" %>
<span class="wintitle">Edit Communication Note | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("Update", "CommunicationNote", FormMethod.Post, new { @id = "editCommunicationNoteForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_CommunicationNote_Id" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_CommunicationNote_UserId" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_CommunicationNote_PatientId" })%>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Communication Note</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <label class="fl strong" for="Edit_CommunicationNote_PatientName">Patient Name:</label>
                        <div class="fr"><%= Model != null && Model.DisplayName.IsNotNullOrEmpty() ? Model.DisplayName : string.Empty %></div>
                    </div>
                    <div class="row">
                        <label class="fl strong" for="Edit_CommunicationNote_EpisodeList">Episode Associated:</label>
                        <div class="fr">
                        <%  if (Model.EpisodeId.IsEmpty()) { %>
                            <%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "Edit_CommunicationNote_EpisodeList", @class = "required notzero" })%>
                        <%  } else { %>
                            <%= Model.EpisodeStartDate %>&#8212;<%= Model.EpisodeEndDate %>
                            <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_CommunicationNote_EpisodeId" })%>
                        <%  } %>
                        </div>
                    </div>
                    <div class="row">
                        <label class="fl strong" for="Edit_CommunicationNote_Date">Date:</label>
                        <div class="fr"><input type="date" name="Created" value="<%= Model.Created.ToShortDateString() %>" id="Edit_CommunicationNote_Date" class="required" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <label class="float-left" for="Edit_CommunicationNote_PhysicianDropDown">Physician:</label>
                        <div class="fr">
                            <%= Html.TextBox("PhysicianId", !Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : "", new { @id = "Edit_CommunicationNote_PhysicianDropDown", @class = "physicians" })%>
                            <br />
                            <div class="button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ol>
        <li>
            <div class="wrapper">
                <div class="wide-column">
                    <label for="Edit_CommunicationNote_Templates" class="strong">Communication Text</label>
                    <%= Html.Templates("Edit_CommunicationNote_Templates", new { @class = "templates mobile_fr", @template = "#Edit_CommunicationNote_Text" })%>
                    <div class="align-center"><%= Html.TextArea("Text", Model.Text, 8, 20, new { @class = "fill", @id = "Edit_CommunicationNote_Text", @maxcharacters = "1500" })%></div>
                </div>
            </div>
        </li>
        <li>
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <div class="checkgroup">
                            <div class="option">
                                <%= Html.CheckBox("SendAsMessage", Model.RecipientArray.Count > 0, new { @id = "Edit_CommunicationNote_SendAsMessage", @class = "bigradio float-left radio" })%>
                                <label for="Edit_CommunicationNote_SendAsMessage" class="radio">Send note as Message:</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="Edit_CommunicationNote_Recipients"><%= Html.Recipients("Edit_CommunicationNote", Model.RecipientArray) %></div>
                    </div>
                </div>
            </div>
        </li>
    </ol>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Electronic Signature</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label for="Edit_CommunicationNote_ClinicianSignature" class="float-left">Staff Signature:</label>
                        <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "Edit_CommunicationNote_ClinicianSignature" })%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label for="Edit_CommunicationNote_ClinicianSignatureDate" class="float-left">Signature Date:</label>
                        <div class="float-right"><input type="date" name="SignatureDate" id="Edit_CommunicationNote_ClinicianSignatureDate" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <%= Html.Hidden("Status", "", new { @id = "Edit_CommunicationNote_Status" })%>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="complete">Complete</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    Forms.ShowIfChecked($("#Edit_CommunicationNote_SendAsMessage"), $("#Edit_CommunicationNote_Recipients"))
</script>