﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Communication Notes | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
    <div class="float-right">
        <div class="buttons">
            <ul>
                <li><%= Html.ActionLink("Excel Export", "CommunicationNotes", "Export", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "AgencyCommunicationNote_ExportLink", @class = "excel" })%></li>
            </ul>
        </div>
    </div>
    <fieldset class="grid-controls">
        <label class="float-left" for="AgencyCommunicationNote_BranchCode">Branch:</label>
        <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "AgencyCommunicationNote_BranchCode" })%></div>
        <label class="strong" for="AgencyCommunicationNote_StartDate">Date Range:</label>
        <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="AgencyCommunicationNote_StartDate" class="short" />
        <label class="strong" for="AgencyCommunicationNote_EndDate">To</label>
        <input type="date" name="EndtDate" value="<%= DateTime.Now.ToShortDateString() %>" id="AgencyCommunicationNote_EndDate" class="short" />
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Agency.RebindAgencyCommunicationNotes();return false">Generate</a></li>
            </ul>
        </div>
        <div class="clear"></div>
        <label class="float-left" for="AgencyCommunicationNote_Status">Filter by:</label>
        <select id="AgencyCommunicationNote_Status" name="Status" class="fl">
            <option value="0">All</option>
            <option value="1" selected>Active</option>
            <option value="2">Discharged</option>
        </select>
        <div id="AgencyCommunicationNote_Search" class="fl"></div>
    </fieldset>
    <%= Html.Telerik()
            .Grid<CommunicationNote>().Name("List_CommunicationNote").HtmlAttributes(new { @class = "bottom-bar" })
            .DataKeys(keys => { keys.Add(o => o.Id).RouteKey("id"); })
            .Columns(columns =>
            {
                columns.Bound(c => c.DisplayName).Title("Patient Name").Sortable(true).ReadOnly();
                columns.Bound(c => c.UserDisplayName).Title("Employee Name").Sortable(true).ReadOnly();
                columns.Bound(c => c.Created).Title("Date").Format("{0:MM/dd/yyyy}").Sortable(true).ReadOnly();
                columns.Bound(c => c.StatusName).Title("Status").Sortable(true).ReadOnly();
                columns.Bound(c => c.Id).Title(" ").ClientTemplate("<a onclick=\"Acore.OpenPrintView({ Url: '/CommunicationNote/View/<#=PatientId#>/<#=Id#>', PdfUrl: 'Patient/CommunicationNotePdf', PdfData: {  'patientId': '<#=PatientId#>', 'eventId': '<#=Id#>' }});return false\"><span class=\"img icon print\"></span></a>").Width(35).Sortable(false);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("CommunicationNotes", "Patient", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
            .Sortable()
            .Scrollable(scrolling => scrolling.Enabled(true))%>
</div>
<script type="text/javascript">
$("#AgencyCommunicationNote_Search").append(
    $("<div/>").GridSearchById("#List_CommunicationNote")
);
</script>
