﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Patient Vital Signs | <%= Model.DisplayName.Clean() %></span>
<div class="wrapper main blue">
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    <div class="float-right">
        <div class="buttons">
            <ul>
                <li><%= Html.ActionLink("Excel Export", "VitalSigns", "Export", new { patientId = Model.Id, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "Patient_VitalSigns_ExportLink", @class = "excel" })%></li>
            </ul>
        </div>
    </div>
<%  } %>
    <fieldset class="grid-controls">
        <%= Html.Hidden("Patient_VitalSigns_PatientId",Model.Id) %>
        <label class="strong" for="Patient_VitalSigns_StartDate">Date Range:</label>
        <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="Patient_VitalSigns_StartDate" class="short" />
        <label class="strong" for="Patient_VitalSigns_EndDate">To</label>
        <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="Patient_VitalSigns_EndDate" class="short" />
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Patient.RebindVitalSigns();return false">Generate</a></li>
            </ul>
        </div>
    </fieldset>
    <%= Html.Telerik().Grid<VitalSign>().Name("List_PatientVitalSigns").Columns(columns => {
           columns.Bound(s => s.VisitDate).Width(75).Title("Visit Date").Sortable(true).ReadOnly();
           columns.Bound(s => s.DisciplineTask).Title("Task").Sortable(true).ReadOnly();
           columns.Bound(s => s.UserDisplayName).Title("Employee Name").Sortable(true).ReadOnly();
           columns.Bound(s => s.BPLying).Title("BP Lying").Sortable(false).Width(70).ReadOnly();
           columns.Bound(s => s.BPSitting).Title("BP Sit").Sortable(false).Width(70).ReadOnly();
           columns.Bound(s => s.BPStanding).Title("BP Stand").Sortable(false).Width(70).ReadOnly();
           columns.Bound(s => s.Temp).Title("Temp").Sortable(false).Width(40);
           columns.Bound(s => s.Resp).Title("Resp").Sortable(false).Width(40).ReadOnly();
           columns.Bound(s => s.ApicalPulse).Sortable(false).Title("Apical").Width(45);
           columns.Bound(s => s.RadialPulse).Sortable(false).Title("Radial").Width(45).Sortable(false).ReadOnly();
           columns.Bound(s => s.BS).Sortable(false).Title("BS").Width(30);
           columns.Bound(s => s.Weight).Sortable(false).Title("Weight").Width(50);
           columns.Bound(s => s.PainLevel).Sortable(false).Title("Pain").Width(40);
       }).DataBinding(dataBinding => dataBinding.Ajax().Select("PatientVitalSigns", "Report", new { patientId = Model.Id, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable().Footer(false) %>
</div>