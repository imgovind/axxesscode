﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Pending Patient Admissions | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<PendingPatient>()
        .Name("List_PatientPending_Grid")
        .HtmlAttributes(new { @class = "top-gap bottom-bar" })
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(p => p.DisplayName).Title("Name").Sortable(false);
            columns.Bound(p => p.PatientIdNumber).Title("MR#").Width(120);
            columns.Bound(p => p.PrimaryInsuranceName).Title("Primary Insurance").Sortable(false);
            columns.Bound(p => p.Branch).Title("Branch").Sortable(false);
            columns.Bound(p => p.CreatedDateFormatted).Title("Date Added").Width(100).Sortable(false);
            columns.Bound(p => p.Id).Sortable(false).ClientTemplate("<a onclick=\"UserInterface.ShowEditPatient('<#=Id#>');return false\">Edit</a> | <a onclick=\"UserInterface.ShowAdmitPatientModal('<#=Id#>', '<#=Type#>');return false\" >Admit</a> | <a onclick=\"UserInterface.ShowNonAdmitPatientModal('<#=Id#>');return false\">Non-Admit</a>").Title("Action").Width(180);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("PendingList", "Patient"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true))%>
</div>
<script type="text/javascript">
    $("#List_PatientPending_Grid .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ManagePatients)) { %>.append(
        $("<div/>").addClass("float-left").Buttons([ { Text: "New Patient", Click: UserInterface.ShowNewPatient } ])
    )<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>