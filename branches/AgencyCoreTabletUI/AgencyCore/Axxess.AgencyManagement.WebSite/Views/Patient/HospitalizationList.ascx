﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Hospitalized Patients | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<PatientHospitalizationData>()
        .Name("List_Patient_Hospitalized_Grid")
        .HtmlAttributes(new { @class = "top-gap bottom-bar" })
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(p => p.PatientIdNumber).Title("MR#").Width(90);
            columns.Bound(p => p.DisplayName).Title("Patient");
            columns.Bound(p => p.Source).Title("Source");
            columns.Bound(p => p.HospitalizationDate).Title("Hospitalization Date").Width(150);
            columns.Bound(p => p.LastHomeVisitDate).Title("Last Home Visit Date").Width(150);
            columns.Bound(p => p.User).Title("User");
            columns.Bound(p => p.Id).Width(100).Sortable(false).ClientTemplate("<a onclick=\"UserInterface.ShowPatientHospitalizationLogs('<#=PatientId#>');return false\" class=\"\">Show Logs</a>").Title("Action");
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("HospitalizationList", "Patient"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_Patient_Hospitalized_Grid .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ManagePatients)) { %>.append(
        $("<div/>").addClass("float-left").Buttons([ { Text: "New Hospitalization", Click: function() { HospitalizationLog.Add('<%= Guid.Empty %>'); } } ])
    )<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>