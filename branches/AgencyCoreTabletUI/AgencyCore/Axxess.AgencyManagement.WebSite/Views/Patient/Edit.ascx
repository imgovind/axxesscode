﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Edit Patient | <%= Model != null ? (Model.LastName + ", " + Model.FirstName + " " + Model.MiddleInitial).ToTitleCase().Clean() : "" %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Edit", "Patient", FormMethod.Post, new { @id = "editPatientForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Patient_Id", @class = "PatientId" })%>     
    <fieldset>  
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Patient_FirstName" class="float-left"><span class="green">(M0040)</span> First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_Patient_FirstName", @class = "text  input_wrapper required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_MiddleInitial" class="float-left">MI:</label>
                <div class="float-right"><%= Html.TextBox("MiddleInitial",(Model.MiddleInitial), new { @id = "Edit_Patient_MiddleInitial", @class = "text input_wrapper", @style = "width:20px;", @maxlength="1" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_LastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_Patient_LastName", @class = "text input_wrapper required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label class="float-left"><span class="green">(M0069)</span> Gender:</label>
                <div class="float-right"><%= Html.RadioButton("Gender", "Female", Model.Gender == "Female" ? true : false, new { @id = "", @class = "required radio" })%><label for="" class="inline-radio">Female</label><%= Html.RadioButton("Gender", "Male", Model.Gender == "Male" ? true : false, new { @id = "", @class = "required radio" })%><label for="" class="inline-radio">Male</label></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_DOB" class="float-left"><span class="green">(M0066)</span> Date of Birth:</label>
                <div class="float-right"><input type="date" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="Edit_Patient_DOB" class="required" /></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_MaritalStatus" class="float-left">Marital Status:</label>
                <div class="float-right"><%var maritalStatus = new SelectList(new[] { new SelectListItem { Text = "** Select **", Value = "0" }, new SelectListItem { Text = "Married", Value = "Married" }, new SelectListItem { Text = "Divorce", Value = "Divorce" }, new SelectListItem { Text = "Widowed", Value = "Widowed"}, new SelectListItem { Text = "Single", Value = "Single" } }, "Value", "Text", Model.MaritalStatus); %><%= Html.DropDownList("MaritalStatus", maritalStatus, new { @id = "Edit_Patient_MaritalStatus", @class = "input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_LocationId" class="float-left">Agency Branch:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.AgencyLocationId.ToString(), new { @id = "Edit_Patient_LocationId", @class = "BranchLocation" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Patient_PatientID" class="float-left"><span class="green">(M0020)</span> Patient ID:</label>
                <div class="float-right"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "Edit_Patient_PatientID", @class = "text required input_wrapper", @maxlength = "15" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_MedicareNumber" class="float-left"><span class="green">(M0063)</span> Medicare Number:</label>
                <div class="float-right"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "Edit_Patient_MedicareNumber", @class = "text input_wrapper", @maxlength = "11" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_MedicaidNumber" class="float-left"><span class="green">(M0065)</span> Medicaid Number:</label>
                <div class="float-right"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "Edit_Patient_MedicaidNumber", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_SSN" class="float-left"><span class="green">(M0064)</span> SSN:</label>
                <div class="float-right"><%= Html.TextBox("SSN", Model.SSN, new { @id = "Edit_Patient_SSN", @class = "text numeric input_wrapper", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_StartOfCareDate" class="float-left"><span class="green">(M0030)</span> Start of Care Date:</label>
                <div class="float-right"><input type="date" name="StartOfCareDate" value="<%= !Model.StartofCareDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.StartofCareDate.ToString("MM/dd/yyyy") : string.Empty %>" id="Edit_Patient_StartOfCareDate" class="required" /></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_CaseManager" class="float-left">Case Manager:</label>
                <div class="float-right"><%= Html.CaseManagers("CaseManagerId", Model.CaseManagerId.ToString(), new { @id = "Edit_Patient_CaseManager", @class = "Users required notzero" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Patient_Assign" class="float-left">Clinician:</label>
                <div class="float-right"><%= Html.Clinicians("UserId",  Model.UserId.ToString(), new { @id = "Edit_Patient_Assign", @class = "Employees input_wrapper required notzero" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0140)</span> Race/Ethnicity</legend>
        <div class="wide-column">
            <div class="row">
                <%  string[] ethnicities = Model.Ethnicities != null && Model.Ethnicities != "" ? Model.Ethnicities.Split(';') : null; %>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces0' type='checkbox' value='0' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("0") ? "checked='checked'" : "")%>
                        <label for="EditPatient_EthnicRaces0" class="radio">American Indian or Alaska Native</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces1' type='checkbox' value='1' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("1") ? "checked='checked'" : "")%>
                        <label for="EditPatient_EthnicRaces1" class="radio">Asian</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces2' type='checkbox' value='2' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("2") ? "checked='checked'" : "")%>
                        <label for="EditPatient_EthnicRaces2" class="radio">Black or African-American</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces3' type='checkbox' value='3' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("3") ? "checked='checked'" : "")%>
                        <label for="EditPatient_EthnicRaces3" class="radio">Hispanic or Latino</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces4' type='checkbox' value='4' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("4") ? "checked='checked'" : "")%>
                        <label for="EditPatient_EthnicRaces4" class="radio">Native Hawaiian or Pacific Islander</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces5' type='checkbox' value='5' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("5") ? "checked='checked'" : "")%>
                        <label for="EditPatient_EthnicRaces5" class="radio">White</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='EditPatient_EthnicRaces6' type='checkbox' value='6' name='EthnicRaces' class='required radio float-left' {0} />", ethnicities != null && ethnicities.Contains("6") ? "checked='checked'" : "")%>
                        <label for="EditPatient_EthnicRaces6" class="radio">Unknown</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0150)</span> Payment Source <span class="light">(Mark all that apply)</span></legend>
        <div class="wide-column">
            <div class="row">
                <%  string[] paymentSources = Model.PaymentSource != null && Model.PaymentSource != "" ? Model.PaymentSource.Split(';') : null; %>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourceNone' type='checkbox' value='0' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("0") ? "checked='checked'" : "" )%>
                        <label for="Edit_Patient_PaymentSourceNone" class="radio">None; no charge for current services</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourceMedicare' type='checkbox' value='1' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("1") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSourceMedicare" class="radio">Medicare (traditional fee-for-service)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourceMedicareHmo' type='checkbox' value='2' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("2") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSourceMedicareHmo" class="radio">Medicare (HMO/ managed care)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourceMedicaid' type='checkbox' value='3' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("3") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSourceMedicaid" class="radio">Medicaid (traditional fee-for-service)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourceMedicaidHmo' type='checkbox' value='4' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("4") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSourceMedicaidHmo" class="radio">Medicaid (HMO/ managed care)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourceWorkers' type='checkbox' value='5' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("5") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSourceWorkers" class="radio">Workers&#8217; compensation</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourceTitleProgram' type='checkbox' value='6' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("6") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSourceTitleProgram" class="radio">Title programs (e.g., Titile III,V, or XX)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourceOtherGovernment' type='checkbox' value='7' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("7") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSourceOtherGovernment" class="radio">Other government (e.g.,CHAMPUS,VA,etc)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourcePrivate' type='checkbox' value='8' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("8") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSourcePrivate" class="radio">Private insurance</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourcePrivateHmo' type='checkbox' value='9' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("9") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSourcePrivateHmo" class="radio">Private HMO/ managed care</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourceSelf' type='checkbox' value='10' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("10") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSourceSelf" class="radio">Self-pay</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSourceUnknown' type='checkbox' value='11' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("11") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSourceUnknown" class="radio">Unknown</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Patient_PaymentSource' type='checkbox' value='12' name='PaymentSources' class='required radio float-left' {0} />", paymentSources != null && paymentSources.Contains("12") ? "checked='checked'" : "")%>
                        <label for="Edit_Patient_PaymentSource" class="radio more">Other</label>
                        <%= Html.TextBox("OtherPaymentSource", Model.OtherPaymentSource, new { @id = "Edit_Patient_OtherPaymentSource", @class = "text", @style = "display:none;" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Address</legend>
        <div class="marginBreak">
            <div class="column">
                <div class="row">
                    <label for="Edit_Patient_AddressLine1" class="float-left">Address Line 1:</label>
                    <div class="float-right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Patient_AddressLine1", @class = "text required input_wrapper", @maxlength = "50" })%></div>
                </div>
                <div class="row">
                    <label for="Edit_Patient_AddressLine2" class="float-left">Address Line 2:</label>
                    <div class="float-right"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Patient_AddressLine2", @class = "text input_wrapper", @maxlength = "50" })%></div>
                </div>
                <div class="row">
                    <label for="Edit_Patient_AddressCity" class="float-left">City:</label>
                    <div class="float-right"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Patient_AddressCity", @class = "text required input_wrapper", @maxlength = "50" })%></div>
                </div>
                <div class="row">
                    <label for="Edit_Patient_AddressStateCode" class="float-left"><span class="green">(M0050)</span> State, <span class="green">(M0060)</span> Zip:</label>
                    <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Patient_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Patient_AddressZipCode", @class = "text required digits isValidUSZip zip", @size = "5", @maxlength = "5" })%></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label class="float-left">Home Phone:</label>
                    <div class="float-right"><span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >=3 ? Model.PhoneHome.Substring(0, 3) : "", new { @id = "Edit_Patient_HomePhone1", @class = "autotext required digits phone-short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >=6  ? Model.PhoneHome.Substring(3, 3) : "", new { @id = "Edit_Patient_HomePhone2", @class = "autotext required digits phone-short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 10  ? Model.PhoneHome.Substring(6, 4) : "", new { @id = "Edit_Patient_HomePhone3", @class = "autotext required digits phone-long", @maxlength = "4", @size = "5" })%></span></div>
                </div>
                <div class="row">
                    <label class="float-left">Mobile Phone:</label>
                    <div class="float-right"><span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 3 ? Model.PhoneMobile.Substring(0, 3) : "", new { @id = "Edit_Patient_MobilePhone1", @class = "autotext digits phone-short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 6 ? Model.PhoneMobile.Substring(3, 3) : "", new { @id = "Edit_Patient_MobilePhone2", @class = "autotext digits phone-short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 10 ? Model.PhoneMobile.Substring(6, 4) : "", new { @id = "Edit_Patient_MobilePhone3", @class = "autotext digits phone-long", @maxlength = "4", @size = "5" })%></span></div>
                </div>
                <div class="row">
                    <label for="Edit_Patient_Email" class="float-left">Email:</label>
                    <div class="float-right"><%= Html.TextBox("Email", Model.EmailAddress, new { @id = "Edit_Patient_Email", @class = "text input_wrapper", @maxlength = "50" })%></div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Pharmacy</legend>
        <div class="halfRow">
            <div class="marginBreak">
                <div class="column">
                    <div class="row">
                        <label for="Edit_Patient_PharmacyName" class="float-left">Name:</label>
                        <div class="float-right"><%= Html.TextBox("PharmacyName", Model.PharmacyName, new { @id = "Edit_Patient_PharmacyName", @class = "text input_wrapper", @maxlength = "100" })%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label for="Edit_Patient_PharmacyPhone1" class="float-left">Phone:</label>
                        <div class="float-right"><span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 3 ? Model.PharmacyPhone.Substring(0, 3) : "", new { @id = "Edit_Patient_PharmacyPhone1", @class = "autotext digits phone-short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 6 ? Model.PharmacyPhone.Substring(3, 3) : "", new { @id = "Edit_Patient_PharmacyPhone2", @class = "autotext digits phone-short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 10 ? Model.PharmacyPhone.Substring(6, 4) : "", new { @id = "Edit_Patient_PharmacyPhone3", @class = "autotext digits phone-long", @maxlength = "4", @size = "5" })%></span></div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Insurance / Payor Information</legend>
        <div class="column">
            <div class="row">
                <% var htmlAttributesPrimary = new Dictionary<string, string>(); htmlAttributesPrimary.Add("id", "Edit_Patient_PrimaryInsurance"); htmlAttributesPrimary.Add("class", "insurances required notzero");%>
                <label for="Edit_Patient_PrimaryInsurance" class="float-left">Primary:</label>
                <div class="float-right"><%= Html.Insurances("PrimaryInsurance", Model.PrimaryInsurance, true, htmlAttributesPrimary)%></div>
            </div>
            <div class="row hidden" id="Edit_Patient_PrimaryHealthPlanIdContent">
                <label for="Edit_Patient_PrimaryHealthPlanId" class="float-left">Primary Insurance Health Plan Id:</label>
                <div class="float-right"><%= Html.TextBox("PrimaryHealthPlanId", Model.PrimaryHealthPlanId, new { @id = "Edit_Patient_PrimaryHealthPlanId", @class = "" })%></div>
            </div>
            <div class="row">
                <% var htmlAttributesSecondary = new Dictionary<string, string>(); htmlAttributesSecondary.Add("id", "Edit_Patient_SecondaryInsurance"); htmlAttributesSecondary.Add("class", "insurances"); %>
                <label for="Edit_Patient_SecondaryInsurance" class="float-left">Secondary:</label>
                <div class="float-right"><%= Html.Insurances("SecondaryInsurance", Model.SecondaryInsurance, true, htmlAttributesSecondary)%></div>
            </div>
            <div class="row hidden" id="Edit_Patient_SecondaryHealthPlanIdContent">
                <label for="Edit_Patient_SecondaryHealthPlanId" class="float-left">Secondary Insurance Health Plan Id:</label>
                <div class="float-right"><%= Html.TextBox("SecondaryHealthPlanId", Model.SecondaryHealthPlanId, new { @id = "Edit_Patient_SecondaryHealthPlanId", @class = "" })%></div>
            </div>
            <div class="row">
                <% var htmlAttributesTertiary = new Dictionary<string, string>(); htmlAttributesTertiary.Add("id", "Edit_Patient_TertiaryInsurance"); htmlAttributesTertiary.Add("class", "insurances"); %>
                <label for="Edit_Patient_TertiaryInsurance" class="float-left">Tertiary:</label>
                <div class="float-right"><%= Html.Insurances("TertiaryInsurance", Model.TertiaryInsurance, true, htmlAttributesTertiary)%></div>
            </div>
            <div class="row hidden" id="Edit_Patient_TertiaryHealthPlanIdContent">
                <label for="Edit_Patient_TertiaryHealthPlanId" class="float-left">Tertiary Insurance Health Plan Id:</label>
                <div class="float-right"><%= Html.TextBox("TertiaryHealthPlanId", Model.TertiaryHealthPlanId, new { @id = "Edit_Patient_TertiaryHealthPlanId", @class = "" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-right">
        <legend>Emergency Triage <span class="light">(Select one)</span></legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton("Triage", "1", Model.Triage == 1 ? true : false, new { @id = "Edit_Patient_Triage1", @class = "required radio Triage float-left" })%>
                        <label class="radio" for="Edit_Patient_Triage1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Life threatening (or potential) and requires ongoing medical treatment. When necessary, appropriate arrangements for evacuation to an acute care facility will be made.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton("Triage", "2", Model.Triage == 2 ? true : false, new { @id = "Edit_Patient_Triage2", @class = "required radio Triage float-left" })%>
                        <label class="radio" for="Edit_Patient_Triage2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Not life threatening but would suffer severe adverse effects from interruption of services (i.e., daily insulin, IV medications, sterile wound care of a wound with a large amount of drainage.)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton("Triage", "3", Model.Triage == 3 ? true : false, new { @id = "Edit_Patient_Triage3", @class = "required radio Triage float-left" })%>
                        <label class="radio" for="Edit_Patient_Triage3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Visits could be postponed 24-48 hours without adverse effects (i.e., new insulin dependent diabetic able to self inject, sterile wound care with a minimal amount to no drainage)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton("Triage", "4", Model.Triage == 4 ? true : false, new { @id = "Edit_Patient_Triage4", @class = "required radio Triage float-left" })%>
                        <label class="radio" for="Edit_Patient_Triage4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Visits could be postponed 72-96 hours without adverse effects (i.e., post op with no open wound, anticipated discharge within the next 10-14 days, routine catheter changes)</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Services Required <span class="light">(Optional)</span></legend>
        <div class="wide-column">
            <div class="row">
                <%  string[] servicesRequired = Model.ServicesRequired != null && Model.ServicesRequired != "" ? Model.ServicesRequired.Split(';') : null; %>
                <input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection0' type='checkbox' value='0' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("0") ? "checked='checked'" : "" )%>
                        <label for="ServicesRequiredCollection0" class="radio">SNV</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection1' type='checkbox' value='1' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("1") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection1" class="radio">HHA</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection2' type='checkbox' value='2' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("2") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection2" class="radio">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection3' type='checkbox' value='3' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("3") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection3" class="radio">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection4' type='checkbox' value='4' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("4") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection4" class="radio">ST</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection5' type='checkbox' value='5' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("5") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection5" class="radio">MSW</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>DME Needed <span class="light">(Optional)</span></legend>
        <div class="wide-column">
            <div class="row">
                <%  string[] DME = Model.DME != null && Model.DME != "" ? Model.DME.Split(';') : null; %>
                <input type="hidden" value=" " class="radio" name="DMECollection" />
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='DMECollection0' type='checkbox' value='0' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("0") ? "checked='checked'" : "")%>
                        <label for="DMECollection0" class="radio">Bedside Commode</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection1' type='checkbox' value='1' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("1") ? "checked='checked'" : "")%>
                        <label for="DMECollection1" class="radio">Cane</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection2' type='checkbox' value='2' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("2") ? "checked='checked'" : "")%>
                        <label for="DMECollection2" class="radio">Elevated Toilet Seat</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection3' type='checkbox' value='3' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("3") ? "checked='checked'" : "")%>
                        <label for="DMECollection3" class="radio">Grab Bars</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection4' type='checkbox' value='4' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("4") ? "checked='checked'" : "")%>
                        <label for="DMECollection4" class="radio">Hospital Bed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection5' type='checkbox' value='5' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("5") ? "checked='checked'" : "")%>
                        <label for="DMECollection5" class="radio">Nebulizer</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection6' type='checkbox' value='6' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("6") ? "checked='checked'" : "")%>
                        <label for="DMECollection6" class="radio">Oxygen</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection7' type='checkbox' value='7' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("7") ? "checked='checked'" : "")%>
                        <label for="DMECollection7" class="radio">Tub/Shower Bench</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection8' type='checkbox' value='8' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("8") ? "checked='checked'" : "")%>
                        <label for="DMECollection8" class="radio">Walker</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection9' type='checkbox' value='9' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("9") ? "checked='checked'" : "")%>
                        <label for="DMECollection9" class="radio">Wheelchair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection10' type='checkbox' value='10' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("10") ? "checked='checked'" : "")%>
                        <label for="DMECollection10" class="radio">Other</label>
                        <%= Html.TextBox("OtherDME", Model.OtherDME, new { @id = "Edit_Patient_OtherDME", @class = "text", @style = "display:none;" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_ReferralPhysician" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("ReferrerPhysician",Model.ReferrerPhysician.ToString(), new { @id = "Edit_Patient_ReferrerPhysician", @class = "physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
            </div>
            <div class="row">
                <label for="New_Patient_AdmissionSource" class="float-left">Admission Source:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", Model.AdmissionSource, new { @id = "Edit_Patient_AdmissionSource", @class = "AdmissionSource" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Patient_OtherReferralSource" class="float-left">Other Referral Source:</label>
                <div class="float-right"><%= Html.TextBox("OtherReferralSource", Model.OtherReferralSource, new { @id = "Edit_Patient_OtherReferralSource", @class = "text input_wrapper", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_PatientReferralDate" class="float-left">Referral Date:</label>
                <div class="float-right"><input type="date" name="ReferralDate" value="<%= !Model.ReferralDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.ReferralDate.ToString("MM/dd/yyyy") : string.Empty %>" id="Edit_Patient_PatientReferralDate" /></div>
            </div>
            <div class="row">
                <label for="New_Patient_InternalReferral" class="float-left">Internal Referral:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", Model.InternalReferral.ToString(), new { @id = "Edit_Patient_InternalReferral", @class = "Users valid" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Emergency Contact</legend>
        <div class="wide-column">
            <div class="row">
                <div class="buttons al">
                    <ul>
                        <li><a onclick="Patient.loadNewEmergencyContact('<%=Model.Id %>');return false">Add New Emergency Contact</a></li>
                    </ul>
                </div>
                <%= Html.Telerik().Grid<PatientEmergencyContact>().Name("Edit_patient_EmergencyContact_Grid").Columns(columns => {
                        columns.Bound(c => c.FirstName);
                        columns.Bound(c => c.LastName);
                        columns.Bound(c => c.PrimaryPhoneFormatted);
                        columns.Bound(c => c.Relationship);
                        columns.Bound(c => c.EmailAddress);
                        columns.Bound(c => c.Id).ClientTemplate("<a onclick=\" Patient.loadEditEmergencyContact('" + Model.Id + "','<#=Id#>');return false\">Edit</a> | <a onclick=\"Patient.DeleteEmergencyContact('<#=Id#>','" + Model.Id + "');return false\" class=\"deleteReferral\">Delete</a>").Title("Action").Width(100);
                    }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetEmergencyContacts", "Patient", new { PatientId = Model.Id })).Sortable().Footer(false) %>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physicians</legend>
        <div class="wide-column">
            <div class="row">
                <div class="float-left"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "EditPatient_PhysicianSelector", @class = "physicians" })%></div>
                <div class="buttons float-left">
                    <ul>
                        <li>
                            <a class="add-physician">Add Selected Physician</a>
                        </li>
                    </ul>
                </div>
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
                <div class="clear"></div>
                <%= Html.Telerik().Grid<AgencyPhysician>().Name("EditPatient_PhysicianGrid").Columns(columns => {
                        columns.Bound(c => c.FirstName);
                        columns.Bound(c => c.LastName);
                        columns.Bound(c => c.PhoneWorkFormatted).Title("Work Phone");
                        columns.Bound(c => c.FaxNumberFormatted);
                        columns.Bound(c => c.EmailAddress);
                        columns.Bound(c => c.Id).ClientTemplate("<a onclick=\"Patient.DeletePhysician('<#=Id#>','" + Model.Id + "');return false\" class=\"deleteReferral\">Delete</a> | <a onclick=\"Patient.SetPrimaryPhysician('<#=Id #>','" + Model.Id + "');return false\" class=<#= !Primary ? \"\" : \"hidden\" #>><#=Primary ? \"\" : \"Make Primary\" #></a>").Title("Action").Width(135);
                    }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetPhysicians", "Patient", new { PatientId = Model.Id })).Sortable().Footer(false) %>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac">
                <textarea id="Edit_Patient_Comments" name="Comments" cols="5" rows="6" class="tall" maxcharacters="500"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>