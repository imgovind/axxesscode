﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("SaveDischarge", "Patient", FormMethod.Post, new { @id = "dischargePatientForm" }))%><%  { %>
<div class="wrapper main">

    <%= Html.Hidden("PatientId", Model)%>
    <fieldset>
            <legend>Patient Discharge Information</legend>
            <div class="column"><div class="row"><label for="DischargeDate" class="float-left">&#160;&#160;Date of discharge:</label><div class="float-right"><input type="date" name="DischargeDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" class="required" /></div></div></div>
            <div class="clear"></div>
            <div class="wide-column" ><label for="Comment" class="float-left">Reason for discharge:</label><div class="row"><%= Html.TextArea("Comment", "", new { @class = "required" })%></div></div>
    </fieldset>
    <div class="buttons"><ul><li><a class="save">Discharge</a></li><li><a class="close">Cancel</a></li></ul></div>
</div>
<% } %>