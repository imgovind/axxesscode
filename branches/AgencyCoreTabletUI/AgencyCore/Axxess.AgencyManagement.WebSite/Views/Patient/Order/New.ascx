﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<span class="wintitle">New Order | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main note">
<%  using (Html.BeginForm("Add", "Order", FormMethod.Post, new { @id = "newOrderForm" })) { %>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Order</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <label for="New_Order_PatientName" class="float-left">Patient Name:</label>
                        <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", !Model.IsEmpty() ? Model.ToString() : "" , new { @id = "New_Order_PatientName", @class="required notzero" })%></div>
                    </div>
                    <div class="row">
                        <label class="float-left">Episode Associated:</label>
                        <div class="float-right"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_Order_EpisodeList", @class = "required notzero" })%></div>
                    </div>
                    <div class="row">
                        <label for="New_Order_Date" class="float-left">Date:</label>
                        <div class="float-right"><input type="date" name="OrderDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="New_Order_Date" class="required" /></div>
                    </div>
                    <div class="row">
                        <div class="wide checkgroup">
                            <div class="option">
                                <%= Html.CheckBox("IsOrderForNextEpisode", false, new { @id = "New_Order_IsOrderForNextEpisode", @class = "radio float-left" }) %>
                                <label for="New_Order_IsOrderForNextEpisode" class="radio">Check here if this order is for the next episode</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <label for="New_Order_PhysicianDropDown" class="float-left">Physician:</label>
                        <div class="float-right">
                            <%= Html.TextBox("PhysicianId", "", new { @id = "New_Order_PhysicianDropDown", @class = "physicians" })%>
                            <br />
                            <div class="button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ol>
        <li>
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <label for="New_Order_Summary" class="float-left">Summary/Title</label>
                        <div class="fr"><%= Html.TextBox("Summary", "", new { @id = "New_Order_Summary", @class = "required longest", @maxlength = "100" })%></div>
                    </div>
                    <div class="row">
                        <label for="New_Order_Text" class="strong">Order Description</label>
                        <%= Html.Templates("New_Order_OrderTemplates", new { @class = "templates mobile_fr", @template = "#New_Order_Text" })%>
                        <div class="align-center"><textarea id="New_Order_Text" name="Text" cols="5" rows="12" style="height: auto;"></textarea></div>
                    </div>
                    <div class="row">
                        <%= Html.CheckBox("IsOrderReadAndVerified", false, new { @id = "New_Order_IsOrderReadAndVerified", @class = "radio float-left" })%>
                        <label for="New_Order_IsOrderReadAndVerified" class="float-left">Order read back and verified.</label>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Electronic Signature</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label for="New_Order_ClinicianSignature" class="float-left">Clinician Signature:</label>
                        <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "New_Order_ClinicianSignature", @class = "" })%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label for="New_Order_ClinicianSignatureDate" class="float-left">Signature Date:</label>
                        <div class="float-right"><input type="date" name="SignatureDate" id="New_Order_ClinicianSignatureDate" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <%= Html.Hidden("Status", "", new { @id = "New_Order_Status" })%>
    <div class="buttons">
        <ul>
            <li><a onclick="$('#New_Order_ClinicianSignatureDate').removeClass('required');$('#New_Order_Status').val('110')" class="save">Save</a></li>
            <li><a onclick="$('#New_Order_ClinicianSignatureDate').addClass('required');$('#New_Order_Status').val('115')" class="save">Create Order</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    Schedule.loadEpisodeDropDown('New_Order_EpisodeList', $('#New_Order_PatientName'));
    $('#New_Order_PatientName').change(function() { Schedule.loadEpisodeDropDown('New_Order_EpisodeList', $(this)); });
</script>