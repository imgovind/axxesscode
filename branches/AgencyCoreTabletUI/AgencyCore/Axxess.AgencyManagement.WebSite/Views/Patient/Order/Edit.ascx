﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PhysicianOrder>" %>
<% if( Model!=null){ %>
<% using (Html.BeginForm("Update", "Order", FormMethod.Post, new { @id = "editOrderForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Order_Id" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_Order_PatientId" })%>
<%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_Order_UserId" })%>
<% if (Model != null) Model.SignatureDate = DateTime.Today; %>
<div class="wrapper main">
    <fieldset>
        <div class="column">
            <div class="row"><label for="Edit_Order_PatientName" class="float-left">Patient Name:</label><div class="float-right"><span class="bigtext"><%= Model.DisplayName %></span></div></div>
            <% if (Model.EpisodeId.IsEmpty()) { %>
                <div class="row">
                    <label class="float-left">Episode Associated</label>
                    <div class="float-right">
                        <%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "Edit_Order_EpisodeList", @class = "required notzero" }) %>
                    </div>
                </div>
            <% } else { %>
                <div class="row">
                    <label for="Edit_Order_PatientName" class="float-left">Episode Associated:</label>
                    <div class="float-right">
                        <span class="bigtext">
                            <%= string.Format("{0}-{1}", Model.EpisodeStartDate, Model.EpisodeEndDate) %>
                        </span>
                    </div>
                </div>
                <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_Order_EpisodeId" }) %>
            <% } %>
            <div class="row">
                <label for="Edit_Order_PhysicianDropDown" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianId", Model.PhysicianId.ToString(), new { @id = "Edit_Order_PhysicianDropDown", @class = "physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
            </div>
            <div class="row"><label for="Edit_Order_Date" class="float-left">Date:</label><div class="float-right"><input type="date" name="OrderDate" value="<%= Model.OrderDate.ToShortDateString() %>" id="Edit_Order_Date" class="required" /></div></div>
            <div class="row"><label for="Edit_Order_IsOrderForNextEpisode" class="float-left">Check here if this order is for the next episode</label><div class="float-right"><%= Html.CheckBox("IsOrderForNextEpisode", Model.IsOrderForNextEpisode, new { @id = "Edit_Order_IsOrderForNextEpisode", @class = "radio float-left" })%></div></div>
        </div>
        <div class="clear"></div>
       <div class="wide-column">
            <div class="row"><label for="Edit_Order_Summary">Summary /Title</label><br /><%= Html.TextBox("Summary", Model.Summary, new { @id = "Edit_Order_Summary", @class = "", @maxlength = "70", @style = "width: 400px;" })%></div>
            <div class="row"><label for="Edit_Order_Text">Order Description</label><%= Html.Templates("Edit_Order_OrderTemplates", new { @class = "templates", @template = "#Edit_Order_Text" })%><br /><textarea id="Edit_Order_Text" name="Text" cols="5" rows="12" style="height: 120px;"><%= Model.Text %></textarea></div>
            <div class="row"><%= Html.CheckBox("IsOrderReadAndVerified", Model.IsOrderReadAndVerified, new { @id = "Edit_Order_IsOrderReadAndVerified", @class = "radio float-left" })%><label for="Edit_Order_IsOrderReadAndVerified" class="float-left">Order read back and verified.</label></div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="Edit_Order_ClinicianSignature" class="bigtext float-left">Clinician Signature:</label>
                <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "Edit_Order_ClinicianSignature", @class = "" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Order_ClinicianSignatureDate" class="bigtext float-left">Signature Date:</label>
                <div class="float-right"><input type="date" name="SignatureDate" id="Edit_Order_ClinicianSignatureDate" /></div>
            </div>
        </div>
    </fieldset><%= Html.Hidden("Status", Model.Status, new { @id = "Edit_Order_Status" })%>
    <div class="buttons"><ul>
        <li><a class="save">Save</a></li>
        <li><a class="save close">Save &#38; Close</a></li>
        <li><a class="complete">Complete</a></li>
        <li><a class="close">Cancel</a></li>
    </ul></div>
</div>
<%} %>
<%} else{%>
The order does not exist.
<%} %>