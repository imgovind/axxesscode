﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("AddPhoto", "Patient", FormMethod.Post, new { @id = "changePatientPhotoForm" }))   { %>
   <%= Html.Hidden("patientId", Model.Id)%>
<div class="wrapper main">
     <fieldset>
        <legend>Change Patient Photo</legend>
        <div class="wide-column">
            <div class="row ac">
                <%= Model.DisplayName %>
                <br />
                <% if (!Model.PhotoId.IsEmpty()) { %>
                <img src="/Asset/<%= Model.PhotoId.ToString() %>" alt="User Photo" />
                <% } else { %>
                <img src="/Images/blank_user.jpeg" alt="User Photo" />
                <% } %>
            </div>
            <div class="row">
                <label for="Change_Patient_Photo" class="float-left">Browse Photo</label>
                <div class="fr"><input id="Change_Patient_Photo" type="file" name="Photo1" onchange = "validateImage();" /></div>
            </div>
        </div>
    </fieldset>
    <div id="changePatientPhotoError" class="errormessage"></div>
    <div class="buttons"><ul>
        <li><a id="Change_PatientPhoto_Submit" class="save">Upload</a></li>
        <% if (!Model.PhotoId.IsEmpty()) { %><li><a onclick="Patient.RemovePhoto('<%= Model.Id %>');return false">Remove</a></li><% } %>
        <li><a class="close">Exit</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $("#Change_PatientPhoto_Submit").hide();
    function validateImage() {
        var ext = $('#Change_Patient_Photo').val().split('.').pop().toLowerCase();
        var allow = new Array('gif', 'png', 'jpg', 'jpeg');
        if (jQuery.inArray(ext, allow) == -1) {
            alert('Please select a valid image extension!');
            $('#Change_Patient_Photo').val('');
            $("#Change_PatientPhoto_Submit").hide();
        } else {
            $("#Change_PatientPhoto_Submit").show();
        }
    }
</script>
<% } %>