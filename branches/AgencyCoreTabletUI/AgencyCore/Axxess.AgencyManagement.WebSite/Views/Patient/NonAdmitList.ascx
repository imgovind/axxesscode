﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Non-Admitted Patients | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<NonAdmit>()
        .Name("List_Patient_NonAdmit_Grid")
        .HtmlAttributes(new { @class = "top-gap bottom-bar" })
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(p => p.PatientIdNumber).Title("MR#").Width(120);
            columns.Bound(p => p.DisplayName).Title("Name").Width(180);
            columns.Bound(p => p.DateOfBirth).Title("Date of Birth").Width(100).Sortable(true);
            columns.Bound(p => p.Phone).Title("Phone").Width(120).Sortable(false);
            columns.Bound(p => p.NonAdmissionReason).Title("Non-Admit Reason").Sortable(false);
            columns.Bound(p => p.NonAdmitDate).Title("Non-Admit Date").Width(110).Sortable(true);
            columns.Bound(p => p.Id).Width(60).Sortable(false).ClientTemplate("<a onclick=\"UserInterface.ShowAdmitPatientModal('<#=Id#>', '<#=Type#>');return false\">Admit</a>").Title("Action");
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("NonAdmitList", "Patient"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_Patient_NonAdmit_Grid .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ManagePatients)) { %>.append(
        $("<div/>").addClass("float-left").Buttons([ { Text: "New Patient", Click: UserInterface.ShowNewPatient } ])
    )<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>