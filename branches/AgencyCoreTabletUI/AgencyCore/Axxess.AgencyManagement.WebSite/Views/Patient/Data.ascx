﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientCenterViewData>" %>
<% if (Model.Patient != null) { %>
    <div class="top"><% Html.RenderPartial("/Views/Patient/Info.ascx", Model.Patient); %></div>
    <div class="bottom"><% Html.RenderPartial("/Views/Patient/Activities.ascx", Model.Patient.Id); %></div>
    <%= Html.Hidden("PatientCenter_PatientId", Model.Patient.Id) %> 
    <script type="text/javascript">
        $("select.patientActivityDropDown").change(function() { var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid'); PatientActivityGrid.rebind({ patientId: $("#PatientCenter_PatientId").val(), discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val() }); });
        if ($("select.patientActivityDateDropDown").val() == "DateRange") { $("#dateRangeText").hide(); $("div.CustomDateRange").show(); }
        else if ($("select.patientActivityDateDropDown").val() == "All") { $("#dateRangeText").hide(); $("div.CustomDateRange").hide(); }
        else {$("#dateRangeText").show(); $("div.CustomDateRange").hide(); }
        $("select.patientActivityDateDropDown").change(function() {
            if ($("select.patientActivityDateDropDown").val() == "DateRange") { $("#dateRangeText").hide(); $("div.CustomDateRange").show(); }
            else if ($("select.patientActivityDateDropDown").val() == "All") { Patient.DateRange($("select.patientActivityDateDropDown").val()); $("#dateRangeText").hide(); $("div.CustomDateRange").hide(); }
            else { Patient.DateRange($("select.patientActivityDateDropDown").val()); $("#dateRangeText").show(); $("div.CustomDateRange").hide(); }
            var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
            PatientActivityGrid.rebind({ patientId: $("#PatientCenter_PatientId").val(), discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val() });
        });
     </script>
     <% } else { %> <div class="abs center">No Patient Data Found</div><% } %>
