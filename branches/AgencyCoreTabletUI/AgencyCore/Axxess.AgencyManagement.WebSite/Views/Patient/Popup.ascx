﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="wrapper main">
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="Patient_Info_Id" class="fl strong">Id:</label>
                <div class="float-right"><%= Model.PatientIdNumber %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_Name" class="fl strong">Name:</label>
                <div class="float-right"><%= Model.DisplayName %></div>
                </div>
            <div class="row">
                <label for="Patient_Info_Gender" class="fl strong">Gender:</label>
                <div class="float-right"><%= Model.Gender %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_Address1" class="fl strong">Address:</label>
                <div class="float-right"><%= Model.AddressFirstRow.ToTitleCase() %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_Address2" class="fl strong">City, State, Zip:</label>
                <div class="float-right"><%= Model.AddressSecondRow.IsNotNullOrEmpty() ? Model.AddressSecondRow.ToTitleCase() : string.Empty %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_HomePhone" class="fl strong">Home Phone:</label>
                <div class="float-right"><%= Model.PhoneHome.ToPhone() %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_AltPhone" class="fl strong">Mobile Phone:</label>
                <div class="float-right"><%= Model.PhoneMobile.ToPhone() %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_SocDate" class="fl strong">Start of Care Date:</label>
                <div class="float-right"><%= Model.StartOfCareDateFormatted %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_DOB" class="fl strong">Date of Birth:</label>
                <div class="float-right"><%= Model.DOBFormatted %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_Medicare" class="fl strong">Medicare #:</label>
                <div class="float-right"><%= Model.MedicareNumber %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_Medicaid" class="fl strong">Medicaid #:</label>
                <div class="float-right"><%= Model.MedicaidNumber %></div>
            </div>
            <%  if (Model.PhysicianContacts != null && Model.PhysicianContacts.Count > 0) { %>
            <div class="row">
                <label for="Patient_Info_Physician" class="fl strong">Physician Name:</label>
                <div class="float-right"><%= Model.PhysicianContacts[0].DisplayName%></div>
            </div>
            <div class="row">
                <label for="Patient_Info_PhysicianPhone" class="fl strong">Physician Phone:</label>
                <div class="float-right"><%= Model.PhysicianContacts[0].PhoneWork.ToPhone()%></div>
            </div>
            <%  } %>
            <%  if (Model.EmergencyContacts != null && Model.EmergencyContacts.Count > 0) { %>
            <div class="row">
                <label for="Patient_Info_EmergencyContact" class="fl strong">Emergency Name:</label>
                <div class="float-right"><%= Model.EmergencyContacts[0].DisplayName %></div>
            </div>
            <div class="row">
                <label for="Patient_Info_EmergencyContactPhone" class="fl strong">Emergency Phone:</label>
                <div class="float-right"><%= Model.EmergencyContacts[0].PrimaryPhone.ToPhone() %></div>
            </div>
            <%  } %>
        </div>
    </fieldset>
</div>
