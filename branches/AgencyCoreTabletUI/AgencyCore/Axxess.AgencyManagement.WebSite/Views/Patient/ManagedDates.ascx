﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Patient Managed Dates | <%= Model.DisplayName %></span>
<div class="wrapper">
<%= Html.Telerik().Grid<PatientManagedDate>().Name("List_PatientManagedDates")
                .DataKeys(keys => { keys.Add(o => o.Id).RouteKey("Id"); keys.Add(o => o.PatientId).RouteKey("PatientId"); })
        .Columns(columns => {
            columns.Bound(c => c.TypeName).Title("Type Of Date").Sortable(true).ReadOnly();
            columns.Bound(c => c.Date).Title("Date").Format("{0:MM/dd/yyyy}").Sortable(false); columns.Command(commands =>
            {
                commands.Edit();
                commands.Delete() ;
            }).Title("Action");
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("PatientManagedDates", "Patient", new { patientId = Model.Id }).Update("UpdatePatientManagedDates", "Patient").Delete("DeletePatientManagedDates", "Patient"))
          .Editable(editing => editing.Mode(GridEditMode.InLine))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_PatientManagedDates .t-grid-content").css("height", "auto");
</script>
