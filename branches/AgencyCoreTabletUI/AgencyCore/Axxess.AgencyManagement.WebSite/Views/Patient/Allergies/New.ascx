﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<%  using (Html.BeginForm("InsertAllergy", "Patient", FormMethod.Post, new { @id = "newAllergyForm" })) { %>
<%= Html.Hidden("ProfileId", Model, new { @id = "New_Allergy_ProfileId" })%>
<div class="wrapper main">

    <fieldset>
        <legend>New Allergy</legend>
        <div class="wide-column">
            <div class="row">
                <label for="New_Allergy_Name" class="block strong">Name:</label>
                <%= Html.TextBox("Name", "", new { @id = "New_Allergy_Name", @class = "longtext input_wrapper required", @maxlength = "120" })%>
            </div>
            <div class="row">
                <label for="New_Allergy_Type" class="block strong">Type:</label>
                <%= Html.TextBox("Type", "", new { @id = "New_Allergy_Type", @class = "input_wrapper", @maxlength = "50" })%>
                <br />
                <em>(e.g. Medication, Food, Animal, Plants, Environmental)</em>
            </div>
         </div>   
    </fieldset>
    <%= Html.Hidden("AddAnother", "", new { @id="New_Allergy_AddAnother" })%>   
    <div class="buttons">
        <ul>
            <li><a class="save close">Save &#38; Exit</a></li>
            <li><a class="save clear-form">Save &#38; Add Another</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<%  } %>