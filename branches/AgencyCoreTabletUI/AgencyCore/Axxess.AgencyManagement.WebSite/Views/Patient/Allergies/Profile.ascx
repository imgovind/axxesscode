﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AllergyProfileViewData>" %>
<span class="wintitle">Allergy Profile | <%= Model.Patient.DisplayName.Clean() %></span>
<%= Html.Hidden("Id", Model.AllergyProfile.Id, new {@id = "allergyProfileId" })%>
<%= Html.Hidden("PatientId", Model.AllergyProfile.PatientId)%>
<div class="wrapper main">
    <div class="note">
        <ul>
            <li>
                <div class="wrapper">
                    <h3>Allergy Profile</h3>
                </div>
            </li>
        </ul>
        <ol>
            <li>
                <div class="wrapper ac bigtext">
                    <%= Model.Patient.DisplayName %>
                </div>
            </li>
        </ol>
        <div class="buttons">
            <ul>
                <li><a onclick="Allergy.Add('<%= Model.AllergyProfile.Id %>');return false">Add Allergy</a></li>
                <li><a onclick="Allergy.Refresh('<%= Model.AllergyProfile.Id %>');return false">Refresh Allergies</a></li>
                <li><a onclick="Acore.OpenPrintView({ Url: '/Patient/AllergyProfilePrint/<%=Model.AllergyProfile.PatientId %>', PdfUrl: '/Patient/AllergyProfilePdf', PdfData: { 'id': '<%=Model.AllergyProfile.PatientId %>' } });return false">Print Allergy Profile</a></li>
            </ul>
        </div>
    </div>
    <div id="AllergyProfile_list"><% Html.RenderPartial("~/Views/Patient/Allergies/List.ascx", Model.AllergyProfile); %></div>
</div>
