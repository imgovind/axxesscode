﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Allergy>" %>
<%  using (Html.BeginForm("UpdateAllergy", "Patient", FormMethod.Post, new { @id = "editAllergyForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Allergy_Id" })%>
<%= Html.Hidden("ProfileId", Model.ProfileId, new { @id = "Edit_Allergy_ProfileId" })%>
<div class="wrapper main">

    <fieldset class="newallergy">
        <legend>Edit Allergy</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_Allergy_Name" class="block strong">Name:</label>
                <%= Html.TextBox("Name", Model != null && Model.Name.IsNotNullOrEmpty() ? Model.Name : string.Empty, new { @id = "Edit_Allergy_Name", @class = "longtext input_wrapper required", @maxlength = "120" })%>
            </div>
            <div class="row">
                <label for="Edit_Allergy_Type" class="block strong">Type:</label>
                <%= Html.TextBox("Type", Model != null && Model.Type.IsNotNullOrEmpty() ? Model.Type : string.Empty, new { @id = "Edit_Allergy_Type", @class = "input_wrapper", @maxlength = "50" })%>
                <br />
                <em>(e.g. Medication, Food, Animal, Plants, Environmental)</em>
            </div>
         </div>   
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Update</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
</div>
<%  } %>