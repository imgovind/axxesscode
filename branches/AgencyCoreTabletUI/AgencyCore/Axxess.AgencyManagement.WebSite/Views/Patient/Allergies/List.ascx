﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AllergyProfile>" %>
<div id="AllergyProfile_active" class="acore-grid">
    <ul>
        <li class="align-center"><h3>Active Allergies</h3></li>
        <li>
            <span class="allergy-name">Name</span>
            <span class="allergy-type">Type</span>
            <span class="allergy-action">Action</span>
        </li>
    </ul><ol><%
    if (Model != null) {
        int i = 1;
        var allergies = Model.Allergies.ToObject<List<Allergy>>().FindAll(a => a.IsDeprecated == false).OrderBy(a => a.Name).ToList();
        if (allergies != null && allergies.Count > 0)
        {
            foreach (var allergy in allergies) { %>
                <%= string.Format("<li class=\"{0}\">", (i % 2 != 0 ? "odd" : "even")) %>
                    <span class="allergy-name"><%= allergy.Name %></span>
                    <span class="allergy-type"><%= allergy.Type %></span>
                    <span class="allergy-action"><a onclick="Allergy.Edit('<%=Model.Id %>', '<%= allergy.Id %>');return false" >Edit</a> | <a onclick="Allergy.Delete('<%=Model.Id %>', '<%= allergy.Id %>');return false">Delete</a></span>
            </li><%
                i++;
            }
        } else { %>
            <li class="align-center"><span class="darkred">No Active Allergies</span></li>
    <% } } %>
    </ol>
</div>
<div id="AllergyProfile_inactive" class="acore-grid">
    <ul>
        <li class="align-center"><h3>Deleted Allergies</h3></li>
        <li>
            <span class="allergy-name">Name</span>
            <span class="allergy-type">Type</span>
            <span class="allergy-action">Action</span>
        </li>
    </ul><ol class="dc"><%
    if (Model != null) {
        int j = 1;
        var allergies = Model.Allergies.ToObject<List<Allergy>>().FindAll(a => a.IsDeprecated == true).OrderBy(a => a.Name).ToList();
        if (allergies != null && allergies.Count > 0) {
            foreach (var allergy in allergies) { %>
                <%= string.Format("<li class=\"{0}\">", (j % 2 != 0 ? "odd" : "even")) %>
                    <span class="allergy-name"><%= allergy.Name %></span>
                    <span class="allergy-type"><%= allergy.Type %></span>
                    <span class="allergy-action"><a onclick="Allergy.Edit('<%=Model.Id %>', '<%= allergy.Id %>');return false" >Edit</a> | <a onclick="Allergy.Restore('<%=Model.Id %>', '<%= allergy.Id %>');return false">Restore</a></span>
            </li><%
                j++;
            }
        } else { %>
            <li class="align-center"><span class="darkred">No Deleted Allergies</span></li>
    <% } } %>
    </ol>
</div>