﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("SaveActivate", "Patient", FormMethod.Post, new { @id = "activatePatientForm" }))%>
<%  { %>
<div class="wrapper main">
    <%= Html.Hidden("PatientId", Model)%>
    <fieldset> 
        <div class="wide-column align-center">
            <div class="row"><label for="">  <b>Are you sure you want to activate this patient? </b></label></div>
        </div>
        <div class="buttons"><ul><li><a class="save">Yes</a></li><li><a class="close">No</a></li></ul></div>
    </fieldset>
</div>
<% } %>

