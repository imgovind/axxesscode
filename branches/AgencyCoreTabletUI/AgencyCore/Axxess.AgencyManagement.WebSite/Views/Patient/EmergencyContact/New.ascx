﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("NewEmergencyContact", "Patient", FormMethod.Post, new { @id = "newEmergencyContactForm" }))%><%  { %>
<%= Html.Hidden("PatientId", Model, new { @id = "New_EmergencyContact_PatientID" })%>
<div class="wrapper main">
    <fieldset>
        <div class="column">
            <div class="row"><label for="New_EmergencyContact_FirstName" class="float-left">First Name:</label><div class="float-right"> <%= Html.TextBox("FirstName", "", new { @id = "New_EmergencyContact_FirstName", @class = "text input_wrapper required", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_EmergencyContact_LastName" class="float-left">Last Name:</label><div class="float-right"> <%= Html.TextBox("LastName", "", new { @id = "New_EmergencyContact_LastName", @class = "text input_wrapper required", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_EmergencyContact_Relationship" class="float-left">Relationship:</label><div class="float-right"><%= Html.TextBox("Relationship", "", new { @id = "New_EmergencyContact_Relationship", @class = "text input_wrapper" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_EmergencyContact_PrimaryPhoneArray1" class="float-left">Primary Phone:</label><div class="float-right"><span class="input_wrappermultible"><%= Html.TextBox("PhonePrimaryArray", "", new { @id = "New_EmergencyContact_PrimaryPhoneArray1", @class = "autotext required digits phone-short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhonePrimaryArray", "", new { @id = "New_EmergencyContact_PrimaryPhoneArray2", @class = "autotext required digits phone-short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhonePrimaryArray", "", new { @id = "New_EmergencyContact_PrimaryPhoneArray3", @class = "autotext required digits phone-long", @maxlength = "4", @size = "5" })%></span></div></div>
            <div class="row"><label for="New_EmergencyContact_AlternatePhoneArray1" class="float-left">Primary Phone:</label><div class="float-right"><span class="input_wrappermultible"><%= Html.TextBox("PhoneAlternateArray", "", new { @id = "New_EmergencyContact_AlternatePhoneArray1", @class = "autotext digits phone-short", @maxlength = "3", @size = "3" })%></span> - <span class="input_wrappermultible"><%= Html.TextBox("PhoneAlternateArray", "", new { @id = "New_EmergencyContact_AlternatePhoneArray2", @class = "autotext digits phone-short", @maxlength = "3", @size = "3" })%></span> - <span class="input_wrappermultible"><%= Html.TextBox("PhoneAlternateArray", "", new { @id = "New_EmergencyContact_AlternatePhoneArray3", @class = "autotext digits phone-long", @maxlength = "4", @size = "5" })%></span></div></div>
            <div class="row"><label for="New_EmergencyContact_Email" class="float-left">Email:</label><div class="float-right"> <%= Html.TextBox("EmailAddress", "", new { @id = "New_EmergencyContact_Email", @class = "text email input_wrapper", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_EmergencyContact_SetPrimary" class="float-left">Set Primary:</label><div class="float-left"> <%= Html.CheckBox("IsPrimary",true,new { @id = "New_EmergencyContact_SetPrimary" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>