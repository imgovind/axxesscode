﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEmergencyContact>" %>
<div class="wrapper main">
<%  using (Html.BeginForm("EditEmergencyContact", "Patient", FormMethod.Post, new { @id = "editEmergencyContactForm" }))%><%  { %>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_EmergencyContactPatientID" })%>
    <%= Html.Hidden("Id", Model.Id, new { @id ="Edit_EmergencyContactID" })%>
    <fieldset>
        <legend>Emergency Contact</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_EmergencyContact_FirstName" class="float-left">First Name</label>
                <div class="fr"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_EmergencyContact_FirstName", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_EmergencyContact_LastName" class="float-left">Last Name</label>
                <div class="fr"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_EmergencyContact_LastName", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_EmergencyContact_Relationship" class="float-left">Relationship</label>
                <div class="float-right"><%= Html.TextBox("Relationship", Model.Relationship, new { @id = "Edit_EmergencyContact_Relationship", @class = "text input_wrapper" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_EmergencyContact_PrimaryPhoneArray1" class="float-left">Primary Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length >= 3 ? Model.PrimaryPhone.Substring(0, 3) : "", new { @id = "Edit_EmergencyContact_PrimaryPhoneArray1", @class = "autotext required digits phone-short", @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length >= 6 ? Model.PrimaryPhone.Substring(3, 3) : "", new { @id = "Edit_EmergencyContact_PrimaryPhoneArray2", @class = "autotext required digits phone-short",  @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length > 6 ? Model.PrimaryPhone.Substring(6) : "", new { @id = "Edit_EmergencyContact_PrimaryPhoneArray3", @class = "autotext required digits phone-long",@maxlength = "4", @size = "5" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_EmergencyContact_AlternatePhoneArray1" class="float-left">Alternate Phone</label>
                <div class="fr">
                    <%= Html.TextBox("PhoneAlternateArray", Model.AlternatePhone != null && Model.AlternatePhone != "" && Model.AlternatePhone.Length >= 3 ? Model.AlternatePhone.Substring(0, 3) : "", new { @id = "Edit_EmergencyContact_AlternatePhoneArray1", @class = "autotext digits phone-short", @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("PhoneAlternateArray", Model.AlternatePhone != null && Model.AlternatePhone != "" && Model.AlternatePhone.Length >= 6 ? Model.AlternatePhone.Substring(3, 3) : "", new { @id = "Edit_EmergencyContact_AlternatePhoneArray2", @class = "autotext digits phone-short",  @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("PhoneAlternateArray", Model.AlternatePhone != null && Model.AlternatePhone != "" && Model.AlternatePhone.Length > 6 ? Model.AlternatePhone.Substring(6) : "", new { @id = "Edit_EmergencyContact_AlternatePhoneArray3", @class = "autotext digits phone-long",@maxlength = "4", @size = "5" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_EmergencyContact_Email" class="float-left">Email</label>
                <div class="fr"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_EmergencyContact_Email", @class = "text email input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <div class="wide_checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsPrimary",Model.IsPrimary ,new { @id = "Edit_EmergencyContact_SetPrimary" })%>
                        <label for="Edit_EmergencyContact_SetPrimary" class="radio">Set Primary:</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>