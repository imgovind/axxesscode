﻿<div class="wrapper main">
    <div>
        <h3>Patient saved successfully.</h3>
        What would you like to do next?
    </div>
    <ul id="NextStep_Options"></ul>
</div>
<script type="text/javascript">
    $("#window_DialogWindow").addClass("modal-blue");
    $("#NextStep_Options").append(
        $("<li/>").append(
            $("<span/>", { "class": "img icon greenarrow" })
        ).append(
            $("<a/>", { "text": "Add Another Patient" }).click(function() {
                UserInterface.CloseModal();
                UserInterface.CloseWindow('newpatient');
                UserInterface.ShowNewPatient();
                return false
            })
        )
    ).append(
        $("<li/>").append(
            $("<span/>", { "class": "img icon greenarrow" })
        ).append(
            $("<a/>", { "text": "Go to Patient Charts" }).click(function() {
                UserInterface.CloseModal();
                UserInterface.CloseWindow('newpatient');
                UserInterface.ShowPatientCenter();
                return false
            })
        )
    ).append(
        $("<li/>").append(
            $("<span/>", { "class": "img icon greenarrow" })
        ).append(
            $("<a/>", { "text": "Close Window" }).click(function() {
                UserInterface.CloseModal();
                UserInterface.CloseWindow('newpatient');
                return false
            })
        )
    );
</script>
