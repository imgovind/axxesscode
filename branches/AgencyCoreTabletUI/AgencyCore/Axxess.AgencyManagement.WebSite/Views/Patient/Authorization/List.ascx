﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<span class="wintitle">Authorization List | <%= Model != null ? Model.DisplayName.Clean() : "" %></span>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<Authorization>()
        .Name("List_Authorizations")
        .HtmlAttributes(new { @class = "top-gap bottom-bar" })
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(a => a.Number).Title("Number").Width(100).Sortable(false);
            columns.Bound(a => a.Branch).Width(200).Sortable(false);
            columns.Bound(a => a.StartDateFormatted).Title("Start Date").Width(120).Sortable(false);
            columns.Bound(a => a.EndDateFormatted).Title("End Date").Width(120).Sortable(false);
            columns.Bound(a => a.Status).Width(120).Sortable(false);
            columns.Bound(a => a.Url).ClientTemplate("<#=Url#>").Title("Action").Sortable(false).Width(180);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Authorization", new { patientId = Model != null ? Model.Id : Guid.Empty}))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_Authorizations .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
        $("<div/>").addClass("fr").Buttons([ { Text: "Excel Export", Click: function() { U.GetAttachment("Export/Authorizations", { patientId: "<%= Model.Id %>" }) } } ])
    )<% } %>;
</script>