﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("PatientReadmit", "Patient", FormMethod.Post, new { @id = "readmitForm" })) { %>
<%= Html.Hidden("PatientId", Model.Id)%>
 <fieldset>
   <legend>Re-admission  Information</legend>
   <div class="column"><div class="row"><label for="ChangeStatus_ReadmissionDate" class="float-left">Re-admission Date (Start Of Care):</label><div class="float-right"><%= Html.Telerik().DatePicker().Name("ReadmissionDate").Value(DateTime.Today).MinDate(DateTime.MinValue).MaxDate(DateTime.Now).HtmlAttributes(new { @id = "ChangeStatus_ReadmissionDate", @class = "text required" })%></div></div></div>
   <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a class="save">Re-admit</a></li>
            <li><a class="close">Exit</a></li>
        </ul>
    </div>
</fieldset>
<%} %>
