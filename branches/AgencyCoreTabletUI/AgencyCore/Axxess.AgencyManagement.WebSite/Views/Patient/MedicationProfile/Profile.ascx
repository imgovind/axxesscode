﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<span class="wintitle">Medication Profile | <%= Model.Patient.DisplayName.Clean() %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, Question>(); %>
<%= Html.Hidden("Id", Model.MedicationProfile.Id, new  {@id = "medicationProfileId" })%>
<%= Html.Hidden("PatientId", Model.MedicationProfile.PatientId)%>
<div class="wrapper main">
    <div class="note">
        <ul>
            <li>
                <div class="wrapper">
                    <h3>Medication Profile</h3>
                </div>
            </li>
        </ul>
        <ol>
            <li>
                <div class="wrapper ac bigtext">
                    <%= Model.Patient.DisplayName %>
                </div>
            </li>
            <li class="half">
                <div class="wrapper">
                    <div class="wide-column">
                        <div class="row">
                            <label for="MedProfile_EpisodeRange" class="float-left">Current Episode:</label>
                            <div class="float-right"><span id="MedProfile_EpisodeRange"><%= string.Format(" {0} – {1}", Model != null && Model.StartDate != null ? Model.StartDate.ToShortDateString() : string.Empty, Model != null && Model.StartDate != null ? Model.EndDate.ToShortDateString() : "No current episode") %></span></div>
                        </div>
                        <div class="row">
                            <label for="MedProfile_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                            <div class="float-right"><span id="MedProfile_PrimaryDiagnosis"><%= data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : string.Empty %></span></div>
                        </div>
                        <div class="row">
                            <label for="MedProfile_SecondaryDiagnosis" class="float-left">Secondary Diagnosis:</label>
                            <div class="float-right"><span id="MedProfile_SecondaryDiagnosis"><%= data.ContainsKey("M1022PrimaryDiagnosis1") ? data["M1022PrimaryDiagnosis1"].Answer : string.Empty %></span></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </li>
            <li class="half">
                <div class="wrapper">
                    <div class="wide-column">
                        <div class="row">
                            <label for="MedProfile_Allergies" class="float-left">Allergies:</label>
                            <div class="float-right"><span id="MedProfile_Allergies"><%= Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies : string.Empty %></span></div>
                        </div>
                        <div class="row">
                            <label for="MedProfile_PharmacyName" class="float-left">Pharmacy Name:</label>
                            <div class="float-right"><span id="MedProfile_PharmacyName"><%= Model.Patient != null ? Model.Patient.PharmacyName : string.Empty %></span></div>
                        </div>
                        <div class="row">
                            <label for="MedProfile_PharmacyPhone" class="float-left">Pharmacy Phone:</label>
                            <div class="float-right"><span id="MedProfile_PharmacyPhone"><%= Model.Patient != null ? Model.Patient.PharmacyPhone.ToPhone() : string.Empty%></span></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </li>
        </ol>
        <div class="buttons fl">
            <ul>
                <li><a onclick="Medication.Add('<%= Model.MedicationProfile.Id %>');return false">Add Medication</a></li>
                <li><a onclick="Medication.Refresh('<%= Model.MedicationProfile.Id %>');return false">Refresh Medications</a></li>
                <li><a onclick="Patient.loadMedicationProfileSnapshot('<%= Model.Patient.Id%>');return false">Sign Medication Profile</a></li>
                <li><a onclick="Acore.OpenPrintView({ Url: '/Patient/MedicationProfilePrint/<%=Model.Patient.Id %>', PdfUrl: '/Patient/MedicationProfilePdf', PdfData: { 'id': '<%=Model.Patient.Id %>' } });return false">Print Medication Profile</a></li>
            </ul>
        </div>
        <div class="buttons fr">
            <ul>
                <li><a onclick="Patient.loadMedicationProfileSnapshotHistory('<%= Model.Patient.Id%>');return false">Signed Medication Profiles</a></li>
            </ul>
        </div>
    </div>
    <div class="clear"></div>
    <div id="MedProfile_medications"><%  Html.RenderPartial("~/Views/Patient/MedicationProfile/Medication/List.ascx", Model.MedicationProfile); %></div>
    <div class="buttons">
        <ul>
            <li><a onclick="Medication.Refresh('<%= Model.MedicationProfile.Id %>');return false">Refresh Medications</a></li>
        </ul>
    </div>
    <div class="activity-log"><% = string.Format("<span class=\"img icon activity\" onclick=\"Log.LoadMedicationLog('{0}')\" title=\"Activity Logs\"></span>", Model.MedicationProfile.PatientId)%></div>
</div>