﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OasisMedicationProfileViewData>" %>
<div class="buttons">
    <ul class="float-left">
        <li><a onclick="Medication.Add('<%= Model.Profile.Id %>', '<%= Model.AssessmentType %>');return false">Add New Medication</a></li>
        <li><a onclick="Medication.Refresh('<%= Model.Profile.Id %>', '#<%= Model.AssessmentType %>');return false">Refresh Medications</a></li>
    </ul>
    <ul class="float-right">
        <li><a onclick="Patient.loadMedicationProfileSnapshot('<%= Model.Profile.PatientId%>');return false">Sign Medication Profile</a></li>
        <li><a onclick="Acore.OpenPrintView({ Url: '/Patient/MedicationProfilePrint/<%=Model.Profile.PatientId %>', PdfUrl: '/Patient/MedicationProfilePrintPdf', PdfData: { 'id': '<%=Model.Profile.PatientId %>' } });return false">View Medication Profile</a></li>
    </ul>
</div><div class="clear"></div>
<div id="<%= string.Format("{0}_activeMeds", Model.AssessmentType) %>" class="acore-grid">
    <ul>
        <li class="align-center"><h3>Active Medication(s)</h3></li>
        <li>
            <span class="longstanding">LS</span>
            <span class="startdate">Start Date</span>
            <span class="medicationdosage">Medication &#38; Dosage</span>
            <span class="frequency">Frequency</span>
            <span class="route">Route</span>
            <span class="type">Type</span>
            <span class="classification">Classification</span>
            <span class="action">Action</span>
        </li>
    </ul>
    <ol>
<%  if (Model != null && Model.Profile != null && Model.Profile.Medication != null) { %>
    <%  int i = 1; %>
    <%  var medications = Model.Profile.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable); %>
    <%  foreach (var med in medications) { %>
        <li class="<%= i % 2 != 0 ? "odd" : "even" %>">
            <span class="longstanding"><%= string.Format("<input name='LongStanding' class='radio' disabled='true' type='checkbox' value='{0}' {1} />", med.Id, med.IsLongStanding ? "checked='checked'" : string.Empty) %></span>
            <span class="startdate"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
            <span class="medicationdosage"><%= med.MedicationDosage %></span>
            <span class="frequency"><%= med.Frequency %></span>
            <span class="route"><%= med.Route %></span>
            <span class="type"><%= med.MedicationType.Text %></span>
            <span class="classification"><%= med.Classification %></span>
            <span class="action"><a onclick="Medication.Edit('<%=Model.Id %>', '<%= med.Id %>', '<%= Model.AssessmentType %>');return false" >Edit</a> | <a onclick="Medication.Delete('<%=Model.Id %>', '<%= med.Id %>', '<%= Model.AssessmentType %>');return false" >Delete</a> | <a onclick="Medication.Discontinue('<%= Model.Id %>','<%= med.Id %>', '<%= Model.AssessmentType %>');return false">Discontinue</a></span>
        </li>
        <%  i++; %>
    <%  } %>
<%  } %>
    </ol>
</div>
<div id="<%= string.Format("{0}_dischargeMeds", Model.AssessmentType) %>" class="acore-grid">
    <ul>
        <li class="align-center"><h3>Discontinued Medication(s)</h3></li>
        <li>
            <span class="longstanding">LS</span>
            <span class="startdate">Start Date</span>
            <span class="dcmedicationdosage">Medication & Dosage</span>
            <span class="dcfrequency">Frequency</span>
            <span class="dcroute">Route</span>
            <span class="type">Type</span>
            <span class="dcclassification">Classification</span>
            <span class="dischargedate">D/C Date</span>
            <span class="action">Action</span>
        </li>
    </ul>
    <ol class="dc">
<%  if (Model != null && Model.Profile != null && Model.Profile.Medication != null) { %>
    <%  int j = 1; %>
    <%  var medications = Model.Profile.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "DC").OrderByDescending(m => m.StartDateSortable); %>
    <%  foreach (var med in medications) { %>
        <li class="<%= j % 2 != 0 ? "odd" : "even" %>">
            <span class="longstanding"><%= string.Format("<input name='LongStanding' class='radio' disabled='true' type='checkbox' value='{0}' {1} />", med.Id, med.IsLongStanding ? "checked='checked'" : string.Empty) %></span>
            <span class="startdate"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
            <span class="dcmedicationdosage"><%= med.MedicationDosage %></span>
            <span class="dcfrequency"><%= med.Frequency %></span>
            <span class="dcroute"><%= med.Route %></span>
            <span class="type"><%= med.MedicationType.Text %></span>
            <span class="dcclassification"><%= med.Classification %></span>
            <span class="dischargedate"><%= med.DCDateFormated %></span>
            <span class="action"><a onclick="Medication.Edit('<%=Model.Id %>', '<%= med.Id %>', '<%= Model.AssessmentType %>');return false" >Edit</a> | <a onclick="Medication.Delete('<%=Model.Id %>', '<%= med.Id %>', '<%= Model.AssessmentType %>');return false" >Delete</a> | <a onclick="Medication.Activate('<%= Model.Id %>','<%= med.Id %>', '<%= Model.AssessmentType %>');return false">Activate</a></span>
        </li>
        <%  j++; %>
    <%  } %>
<%  } %>
    </ol>
</div>
<div class="buttons">
    <ul class="float-left">
        <li><a onclick="Medication.Add('<%= Model.Profile.Id %>', '<%= Model.AssessmentType %>');return false">Add New Medication</a></li>
        <li><a onclick="Medication.Refresh('<%= Model.Profile.Id %>', '#<%= Model.AssessmentType %>');return false">Refresh Medications</a></li>
    </ul>
    <ul class="float-right">
        <li><a onclick="Patient.loadMedicationProfileSnapshot('<%= Model.Profile.PatientId%>');return false">Sign Medication Profile</a></li>
        <li><a onclick="Acore.OpenPrintView({ Url: '/Patient/MedicationProfilePrint/<%=Model.Profile.PatientId %>', PdfUrl: '/Patient/MedicationProfilePrintPdf', PdfData: { 'id': '<%=Model.Profile.PatientId %>' } });return false">View Medication Profile</a></li>
    </ul>
</div><div class="clear"></div>