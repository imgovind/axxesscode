﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfile>" %>
<%  using (Html.BeginForm("DrugDrugInteractionsPdf", "Patient", FormMethod.Post, new { @id = "drugDrugInteractionForm" })) { %>
<%= Html.Hidden("patientId", Model.PatientId)%>
<div id="MedProfile_DrugDrug" class="medprofile">
    <ul>
        <li class="align-center"><h3>Active Medication(s)</h3></li>
        <li>
            <span class="dcheck"></span>
            <span class="medanddosage">Medication & Dosage</span>
            <span class="dfrequency">Frequency</span>
            <span class="droute">Route</span>
            <span class="dclassification">Classification</span>
        </li>
    </ul><ol><%
    if (Model != null) {
        int i = 1;
        var medications = Model.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable);
        if (medications != null && medications.Count() > 0) {
            foreach (var med in medications) {
                if (med.LexiDrugId.IsNotNullOrEmpty()) { %>
            <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd" : "even")) %>
            <%= string.Format("<span class=\"dcheck\"><input name=\"drugsSelected\" type=\"checkbox\" value=\"{0}\" /></span>", med.LexiDrugId)%>
                    <span class="medanddosage"><%= med.MedicationDosage %></span>
                    <span class="dfrequency"><%= med.Frequency %></span>
                    <span class="droute"><%= med.Route %></span>
                    <span class="dclassification"><%= med.Classification%></span>
            </li><%
                i++;
                }
            }
        } else { %>
                <li class="align-center"><span class="darkred">No Active Medications</span></li>
        <% } } %>
    </ol>
</div>
<div class="buttons">
    <ul>
        <li><a onclick="GetDrugDrugInteractions($(this));return false">Screen</a></li>
        <li><a onclick="close">Close</a></li>
    </ul>
</div>
<% } %>
<script type="text/javascript">
    $(".medprofile ol").each(function() {
        $("li:first", this).addClass("first");
        $("li:last", this).addClass("last")
    });
    function GetDrugDrugInteractions(control) {
        if ($("input[name=drugsSelected]:checked").length > 1) {
            $(control).closest('form').submit();
        } else U.Growl("Select at least two medications to screen.", "error");
    }
</script>