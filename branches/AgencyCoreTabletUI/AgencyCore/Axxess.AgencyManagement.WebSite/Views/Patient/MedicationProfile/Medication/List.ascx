﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfile>" %>
<div id="MedProfile_activeMeds" class="acore-grid">
    <ul>
        <li class="align-center"><h3>Active Medication(s)</h3></li>
        <li>
            <span class="med-ls">LS</span>
            <span class="med-startdate">Start Date</span>
            <span class="med-dosage">Medication & Dosage</span>
            <span class="med-frequency">Frequency</span>
            <span class="med-route">Route</span>
            <span class="med-type">Type</span>
            <span class="med-classification">Classification</span>
            <span class="med-action">Action</span>
        </li>
    </ul><ol><%
    if (Model != null) {
        int i = 1;
        var medications = Model.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable);
        if (medications != null && medications.Count() > 0) {
            foreach (var med in medications) { %>
            <%= string.Format("<li class=\"{0}\">", (i % 2 != 0 ? "odd" : "even")) %>
                    <span class="med-ls"><input name="LongStanding" class="radio" disabled="true" type="checkbox" value="<%= med.Id %>" id="ActiveLongStanding<%= i %>" <%= med.IsLongStanding ? "checked='checked'" : string.Empty %> /></span>
                    <span class="med-startdate"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
                    <span class="med-dosage"><%= med.MedicationDosage %></span>
                    <span class="med-frequency"><%= med.Frequency %></span>
                    <span class="med-route"><%= med.Route %></span>
                    <span class="med-type"><%= med.MedicationType.Text %></span>
                    <span class="med-classification"><%= med.Classification %></span>
                    <span class="med-action"><a onclick="Medication.Edit('<%=Model.Id %>', '<%= med.Id %>');return false" >Edit</a> | <a onclick="Medication.Delete('<%=Model.Id %>', '<%= med.Id %>');return false" >Delete</a> | <a onclick="Medication.Discontinue('<%= Model.Id %>','<%= med.Id %>');return false">Discontinue</a></span>
            </li><%
                i++;
            }
        } else { %>
                <li class="align-center"><span class="darkred">No Active Medications</span></li>
        <% } } %>
    </ol>
</div>
<div id="MedProfile_dischargeMeds" class="acore-grid">
    <ul>
        <li class="align-center"><h3>Discontinued Medication(s)</h3></li>
        <li>
            <span class="med-ls">LS</span>
            <span class="med-startdate">Start Date</span>
            <span class="med-dcdosage">Medication & Dosage</span>
            <span class="med-dcfrequency">Frequency</span>
            <span class="med-dcroute">Route</span>
            <span class="med-type">Type</span>
            <span class="med-dcclassification">Classification</span>
            <span class="med-dischargedate">D/C Date</span>
            <span class="med-action">Action</span>
        </li>
    </ul><ol class="dc"><%
    if (Model != null) {
        int j = 1;
        var medications = Model.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "DC").OrderByDescending(m => m.StartDateSortable);
        if (medications != null && medications.Count() > 0) {
            foreach (var med in medications) { %>
            <%= string.Format("<li class=\"{0}\">", (j % 2 != 0 ? "odd" : "even")) %>
                    <span class="med-ls"><input name="LongStanding" class="radio" disabled="true" type="checkbox" value="<%= med.Id %>" id="DcLongStanding<%= j %>" <%= med.IsLongStanding ? "checked='checked'" : string.Empty %> /></span>
                    <span class="med-startdate"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
                    <span class="med-dcdosage"><%= med.MedicationDosage %></span>
                    <span class="med-dcfrequency"><%= med.Frequency %></span>
                    <span class="med-dcroute"><%= med.Route %></span>
                    <span class="med-type"><%= med.MedicationType.Text %></span>
                    <span class="med-dcclassification"><%= med.Classification %></span>
                    <span class="med-dischargedate"><%= med.DCDateFormated %></span>
                    <span class="med-action"><a onclick="Medication.Edit('<%=Model.Id %>', '<%= med.Id %>');return false" >Edit</a> | <a onclick="Medication.Delete('<%=Model.Id %>', '<%= med.Id %>');return false" >Delete</a> | <a onclick="Medication.Activate('<%= Model.Id %>','<%= med.Id %>');return false">Activate</a></span>
            </li><%
                j++;
            }
        } else { %>
                <li class="align-center"><span class="darkred">No D/C Medications</span></li>
        <% } } %>
    </ol>
</div>