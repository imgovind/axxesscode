﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("InsertNewMedication", "Patient", FormMethod.Post, new { @id = "newMedicationForm" })) { %>
<%= Html.Hidden("medicationProfileId", Model, new { @id = "New_Medication_ProfileId" })%>
<%= Html.Hidden("LexiDrugId", "", new { @id = "New_Medication_DrugId" })%>   
<div class="wrapper main">
    <fieldset>
        <legend>New Medication</legend>
        <div class="wide-column">
            <div class="row">
                <div class="block fl">
                    <%= Html.CheckBox("IsLongStanding", false, new { @id = "New_Medication_IsLongStanding", @class = "bigradio" })%>
                    <label for="New_Medication_IsLongStanding" class="strong">Long Standing</label>
                </div>
                <div id="New_Medication_StartDateRow" class="fl">
                    <label class="strong" for="New_Medication_StartDate">Start Date:</span>
                    <input type="date" name="StartDate" id="New_Medication_StartDate" class="short" />
                </div>
            </div>
            <div class="row">
                <label for="New_Medication_MedicationDosage" class="block strong">Medication &#38; Dosage:</label>
                <%= Html.TextBox("MedicationDosage", "", new { @id = "New_Medication_MedicationDosage", @class = "longtext input_wrapper required", @maxlength = "120" })%>
            </div>
            <div class="row">
                <label for="New_Medication_Frequency" class="block strong">Frequency:</label>
                <%= Html.TextBox("Frequency", "", new { @id = "New_Medication_Frequency", @class = "text input_wrapper Frequency", @maxlength = "100" })%>
            </div>
            <div class="row">
                <label for="New_Medication_Route" class="block strong">Route:</label>
                <%= Html.TextBox("Route", "", new { @id = "New_Medication_Route", @class = "text input_wrapper", @maxlength = "100" })%>
            </div>
            <div class="row">
                <label for="New_Medication_Type" class="block strong">Type:</label>
                <% var medicationTypes = new SelectList(new[] { 
                       new SelectListItem { Text = "New", Value = "N" },
                       new SelectListItem { Text = "Changed", Value = "C" },               
                       new SelectListItem { Text = "Unchanged", Value = "U" }               
                    }, "Value", "Text");%>
                <%= Html.DropDownList("medicationType", medicationTypes)%>
            </div>
            <div class="row">
                <label for="New_Medication_Classification" class="block strong">Classification:</label>
                <%= Html.TextBox("Classification", "", new { @id = "New_Medication_Classification", @class = "text input_wrapper", @maxlength = "100" })%>
            </div>
         </div>   
    </fieldset>
    <%= Html.Hidden("AddAnother", "", new { @id="New_Medication_AddAnother" })%>   
    <div class="buttons">
        <ul>
            <li><a class="save close">Save &#38; Exit</a></li>
            <li><a class="save clear-form'>Save &#38; Add Another</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<%  } %>