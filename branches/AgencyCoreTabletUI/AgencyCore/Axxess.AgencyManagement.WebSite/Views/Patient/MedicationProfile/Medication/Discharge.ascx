﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Medication>" %>
<div class="wrapper main">
    <form id="dischargeMedicationProfileForm" action="/Patient/UpdateMedicationStatus" method="post">
        <fieldset>
            <legend>Discontinue Medication</legend>
            <div class="wide-column">
                <div class="row">
                    <label for="Discharge_Medication_Date" class="float-left">Medication:</label>
                    <div class="float-right"><%= Model.MedicationDosage %></div>
                </div>
                <div class="row">
                    <label for="Discharge_Medication_Date" class="float-left">D/C date:</label>
                    <div class="float-right"><input type="date" name="dischargeDate" id="Discharge_Medication_Date" class="required" /></div>
                </div>
            </div>
        </fieldset>
        <input type="hidden" id="Discharge_Medication_AssessmentType" />
        <div class="buttons">
            <ul>
                <li><a onclick="Medication.Discharge('<%= Model.ProfileId %>','<%= Model.Id %>', $(this));return false">Discontinue</a></li>
                <li><a class="close">Cancel</a></li>
            </ul>
        </div>
    </form>
</div>