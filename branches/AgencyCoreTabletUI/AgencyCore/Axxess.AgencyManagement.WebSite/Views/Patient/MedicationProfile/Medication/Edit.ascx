﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Medication>" %>
<% using (Html.BeginForm("UpdatePatientMedication", "Patient", FormMethod.Post, new { @id = "editMedicationForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Medication_Id" })%>
<%= Html.Hidden("ProfileId", Model.ProfileId, new { @id = "Edit_Medication_ProfileId" })%>
<div class="wrapper main">
    <fieldset>
        <legend>Edit Medication</legend>
        <div class="wide-column">
            <div class="row">
                <div class="block fl">
                    <%= Html.CheckBox("IsLongStanding", Model.IsLongStanding, new { @id = "Edit_Medication_IsLongStanding", @class = "bigradio" })%>
                    <label for="Edit_Medication_IsLongStanding" class="strong">Long Standing</label>
                </div>
                <div id="Edit_Medication_StartDateRow" class="fl">
                    <label class="strong" for="Edit_Medication_StartDate">Start Date:</label>
                    <input type="date" name="StartDate" value="<%= Model.StartDate.IsValid() ? Model.StartDate.ToShortDateString() : string.Empty %>" id="Edit_Medication_StartDate" class="short" />
                </div>
            </div>
            <div class="row">
                <label for="Edit_Medication_MedicationDosage" class="block strong">Medication &#38; Dosage:</label>
                <%= Html.TextBox("MedicationDosage", Model.MedicationDosage.IsNotNullOrEmpty() ? Model.MedicationDosage : string.Empty, new { @id = "Edit_Medication_MedicationDosage", @class = "longtext input_wrapper required", @maxlength = "120" })%>
            </div>
            <div class="row">
                <label for="Edit_Medication_Frequency" class="block strong">Frequency:</label>
                <%= Html.TextBox("Frequency", Model.Frequency.IsNotNullOrEmpty() ? Model.Frequency : string.Empty, new { @id = "Edit_Medication_Frequency", @class = "text input_wrapper Frequency", @maxlength = "100" })%>
            </div>
            <div class="row">
                <label for="Edit_Medication_Route" class="block strong">Route:</label>
                <%= Html.TextBox("Route", Model.Route.IsNotNullOrEmpty() ? Model.Route : string.Empty, new { @id = "Edit_Medication_Route", @class = "text input_wrapper", @maxlength = "100" })%>
            </div>
            <div class="row">
                <label for="Edit_Medication_Type" class="block strong">Type:</label>
                <%  var medicationTypes = new SelectList(new[] { 
                       new SelectListItem { Text = "New", Value = "N" },
                       new SelectListItem { Text = "Changed", Value = "C" },
                       new SelectListItem { Text = "Unchanged", Value = "U" }
                    }, "Value", "Text", Model.MedicationType.Value.IsNotNullOrEmpty() ? Model.MedicationType.Value : "N" ); %>
                    <%= Html.DropDownList("medicationType", medicationTypes)%>
            </div>
            <div class="row">
                <label for="Edit_Medication_Classification" class="block strong">Classification:</label>
                <%= Html.TextBox("Classification", Model.Classification.IsNotNullOrEmpty() ? Model.Classification : string.Empty, new { @id = "Edit_Medication_Classification", @class = "text input_wrapper", @maxlength = "100" })%>
            </div>
         </div>   
    </fieldset>
    <%= Html.Hidden("AddAnother", "", new { @id="Edit_Medication_AddAnother" })%>   
    <div class="buttons">
        <ul>
            <li><a class="save">Update</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
</div>
<%  } %>