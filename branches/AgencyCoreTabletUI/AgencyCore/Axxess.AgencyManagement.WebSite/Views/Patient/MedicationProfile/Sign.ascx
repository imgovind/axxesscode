﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<span class="wintitle">Medication Profile Snapshot | <%= Model.Patient.DisplayName.Clean() %></span>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, Question>(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("SignMedicationHistory", "Patient", FormMethod.Post, new { @id = "newMedicationProfileSnapShotForm" })) { %>
<%= Html.Hidden("ProfileId", Model.MedicationProfile.Id)%>
<%= Html.Hidden("PatientId", Model.MedicationProfile.PatientId)%>
    <div class="note">
        <ul>
            <li>
                <div class="wrapper">
                    <h3>Medication Profile SnapShot</h3>
                </div>
            </li>
        </ul>
        <ol>
            <li>
                <div class="wrapper ac bigtext"><%= Model.Patient.DisplayName %></div>
            </li>
            <li class="half">
                <div class="wrapper">
                    <div class="wide-column">
                        <div class="row">
                            <label for="MedProfile_EpisodeRange" class="float-left">Episode Associated:</label>
                            <div class="fr"><%= Html.PatientEpisodes("EpisodeId", Model.EpisodeId.ToString(), Model.Patient.Id, "-- Select Episode --", new { @id = "MedProfileSnapShot_EpisodeRange", @class = "required notzero" })%></div>
                        </div>
                        <div class="row">
                            <label for="MedProfile_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label>
                            <div class="fr"><%= Html.TextBox("PrimaryDiagnosis", data.ContainsKey("M1020PrimaryDiagnosis") && data["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data["M1020PrimaryDiagnosis"].Answer : string.Empty, new { @id = "MedProfile_PrimaryDiagnosis", @Style = "width: 250px;" })%></div>
                        </div>
                        <div class="row">
                            <label for="MedProfile_SecondaryDiagnosis" class="float-left">Secondary Diagnosis:</label>
                            <div class="fr"><%= Html.TextBox("SecondaryDiagnosis", data.ContainsKey("M1022PrimaryDiagnosis1") && data["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis1"].Answer : string.Empty, new { @id = "MedProfile_SecondaryDiagnosis", @Style = "width: 250px;" })%></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </li>
            <li class="half">
                <div class="wrapper">
                    <div class="wide-column">
                        <div class="row">
                            <label for="MedProfile_Physician" class="float-left">Physician:</label>
                            <div class="fr"><%= Html.TextBox("PhysicianId", (Model != null && !Model.PhysicianId.IsEmpty()) ? Model.PhysicianId.ToString() : "", new { @id = "MedProfile_Physician", @class = "physicians" })%></div>
                        </div>
                        <div class="row">
                            <label for="MedProfile_PharmacyName" class="float-left">Pharmacy Name:</label>
                            <div class="fr"><%= Html.TextBox("PharmacyName", Model.Patient != null ? Model.Patient.PharmacyName : string.Empty, new { @id = "MedProfile_PharmacyName", @Style = "width: 250px;" })%></div>
                        </div>
                        <div class="row">
                            <label for="MedProfile_PharmacyPhone1" class="float-left">Pharmacy Phone:</label>
                            <div class="fr"><span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 3 ? Model.PharmacyPhone.Substring(0, 3) : "", new { @id = "MedProfile_PharmacyPhone1", @class = "autotext digits phone-short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 6 ? Model.PharmacyPhone.Substring(3, 3) : "", new { @id = "MedProfile_PharmacyPhone2", @class = "autotext digits phone-short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 10 ? Model.PharmacyPhone.Substring(6, 4) : "", new { @id = "MedProfile_PharmacyPhone3", @class = "autotext digits phone-long", @maxlength = "4", @size = "5" })%></span></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </li>
        </ol>
        <ul>
            <li>
                <div class="wrapper">
                    <h3>Allergies</h3>
                </div>
            </li>
        </ul>
        <ol>
            <li>
                <div class="wrapper">
                    <div class="wide-column">
                        <div class="row ac">
                            <%= Html.TextArea("Allergies", Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies : string.Empty, new { @id = "MedProfileSnapShot_Allergies", @class = "fill" })%>
                        </div>
                    </div>
                </div>
            </li>
        </ol>
    </div>
    <div class="acore-grid">
        <ul>
            <li class="align-center"><h3>Medication(s)</h3></li>
            <li>
                <span class="med-ls">LS</span>
                <span class="med-startdate">Start Date</span>
                <span class="med-dosage">Medication &#38; Dosage</span>
                <span class="med-frequency">Frequency</span>
                <span class="med-route">Route</span>
                <span class="med-type">Type</span>
                <span class="med-classification">Classification</span>
            </li>
        </ul>
        <ol>
    <%  if (Model != null) { %>
        <%  int i = 1; %>
        <%  var medications = Model.MedicationProfile.Medication.ToObject<List<Medication>>().FindAll(m => m.MedicationCategory == "Active").OrderByDescending(m => m.StartDateSortable); %>
        <%  foreach (var med in medications) { %>
            <li class="<%= i % 2 != 0 ? "odd" : "even" %>">
                <span class="med-ls"><%= string.Format("<input name='LongStanding' class='radio' disabled='true' type='checkbox' value='{0}' {1} />", med.Id, med.IsLongStanding ? "checked='checked'" : string.Empty) %></span>
                <span class="med-startdate"><%= med.StartDate != DateTime.MinValue ? med.StartDate.ToShortDateString().ToZeroFilled() : string.Empty %></span>
                <span class="med-dosage"><%= med.MedicationDosage %></span>
                <span class="med-frequency"><%= med.Frequency %></span>
                <span class="med-route"><%= med.Route %></span>
                <span class="med-type"><%= med.MedicationType.Text %></span>
                <span class="med-classification"><%= med.Classification %></span>
            </li>
            <%  i++; %>
        <%  } %>
    <%  } %>
        </ol>
    </div>
    <div class="note">
        <ul>
            <li>
                <div class="wrapper">
                    <h3>Signature</h3>
                </div>
            </li>
        </ul>
        <ol>
            <li>
                <div class="wrapper">
                    <div class="wide-column">
                        <div class="row">
                            <p>
                                <strong>Drug Regimen Review Acknowledgment:</strong>
                                I have reviewed all the listed medications for potential adverse effects, drug reactions, including
                                ineffective drug therapy, significant side effects, significant drug interactions, duplicate drug
                                therapy, and noncompliance with drug therapy.
                            </p>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="column">
                        <div class="row">
                            <label for="Signature" class="float-left">Clinician Signature:</label>
                            <div class="fr"><%= Html.Password("Signature", "", new { @id = "MedicationProfile_ClinicianSignature", @class="required" }) %></div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="row">
                            <label for="SignedDate" class="float-left">Date:</label>
                            <div class="fr"><input type="date" name="SignedDate" id="MedicationProfile_ClinicianSignatureDate" class="required" /></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </li>
        </ol>
        <div class="buttons">
            <ul>
                <li><a class="save">Sign</a></li>
                <li><a class="close">Close</a></li>
            </ul>
        </div>
    </div>
</div>
<%  } %>