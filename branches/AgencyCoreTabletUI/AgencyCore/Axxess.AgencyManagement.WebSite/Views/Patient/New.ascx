﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<span class="wintitle">New Patient | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "Patient", FormMethod.Post, new { @id = "newPatientForm", @success = "Patient.NewSuccess()" })) { %>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_FirstName" class="float-left"><span class="green">(M0040)</span> First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", (Model != null && Model.FirstName.IsNotNullOrEmpty()) ? Model.FirstName : "", new { @id = "New_Patient_FirstName", @class = "text input_wrapper required", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_MiddleInitial" class="float-left"><span class="green">(M0040)</span> MI:</label>
                <div class="float-right"><%= Html.TextBox("MiddleInitial", "", new { @id = "New_Patient_MiddleInitial", @class = "text input_wrapper mi", @maxlength = "1" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_LastName" class="float-left"><span class="green">(M0040)</span> Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", (Model != null && Model.LastName.IsNotNullOrEmpty()) ? Model.LastName : "", new { @id = "New_Patient_LastName", @class = "text input_wrapper required", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label class="float-left"><span class="green">(M0069)</span> Gender:</label>
                <div class="float-right"><%= Html.RadioButton("Gender", "Female", new { @id = "New_Patient_Gender_F", @class = "required radio" }) %><label for="New_Patient_Gender_F" class="inline-radio">Female</label><%= Html.RadioButton("Gender", "Male", new { @id = "New_Patient_Gender_M", @class = "required radio" }) %><label for="New_Patient_Gender_M" class="inline-radio">Male</label></div>
            </div>
            <div class="row">
                <label for="New_Patient_DOB" class="float-left"><span class="green">(M0066)</span> Date of Birth:</label>
                <div class="float-right"><%= Html.TextBox("DOB", (Model != null && Model.DOB != DateTime.MinValue) ? Model.DOBFormatted : "", new { @id = "New_Patient_DOB", @class = "required", @type="date" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_MaritalStatus" class="float-left">Marital Status:</label>
                <div class="float-right">
                <div class="float-right">
                    <%  var maritalStatus = new SelectList(new[] {
                            new SelectListItem { Text = "** Select **", Value = "0" },
                            new SelectListItem { Text = "Married", Value = "Married" },
                            new SelectListItem { Text = "Divorce", Value = "Divorce" },
                            new SelectListItem { Text = "Widowed", Value = "Widowed"},
                            new SelectListItem { Text = "Single", Value = "Single" }
                        }, "Value", "Text", "0"); %>
                    <%= Html.DropDownList("MaritalStatus", maritalStatus, new { @id = "New_Patient_MaritalStatus", @class = "input_wrapper" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_Patient_LocationId" class="float-left">Agency Branch:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "New_Patient_LocationId", @class = "BranchLocation required notzero" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_CaseManager" class="float-left">Case Manager:</label>
                <div class="float-right"><%= Html.CaseManagers("CaseManagerId", "", new { @id = "New_Patient_CaseManager", @class = "Users required notzero valid" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_Assign" class="float-left">Assign to Clinician/Case Manager:</label>
                <div class="float-right"><%= Html.Clinicians("UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : "", new { @id = "New_Patient_Assign", @class = "required notzero Users  valid" })%></div>
            </div>
            <div class="row">
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowPatientEligibility($('#New_Patient_MedicareNumber').val(), $('#New_Patient_LastName').val(),$('#New_Patient_FirstName').val(),$('#New_Patient_DOB').val(),$('input[name=Gender]:checked').val());return false">Verify Medicare Eligibility</a></div>
            </div>
        </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Patient_PatientID" class="float-left"><span class="green">(M0020)</span> Patient ID/MR Number:</label>
                <div class="float-right">
                    <%= Html.TextBox("PatientIdNumber", "", new { @id = "New_Patient_PatientID", @class = "text input_wrapper required", @maxlength = "15" })%><br />
                    <span>Last MR Number Used: <b><%= Current.LastUsedPatientId %></b></span>
                </div>
            </div>
            <div class="row">
                <label for="New_Patient_MedicareNumber" class="float-left"><span class="green">(M0063)</span> Medicare Number:</label>
                <div class="float-right"><%= Html.TextBox("MedicareNumber", (Model != null && Model.MedicareNumber.IsNotNullOrEmpty()) ? Model.MedicareNumber : "", new { @id = "New_Patient_MedicareNumber", @class = "text input_wrapper", @maxlength = "11" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_MedicaidNumber" class="float-left"><span class="green">(M0065)</span> Medicaid Number:</label>
                <div class="float-right"><%= Html.TextBox("MedicaidNumber", (Model != null && Model.MedicaidNumber.IsNotNullOrEmpty()) ? Model.MedicaidNumber : "", new { @id = "New_Patient_MedicaidNumber", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_SSN" class="float-left"><span class="green">(M0064)</span> SSN:</label>
                <div class="float-right"><%= Html.TextBox("SSN", (Model != null && Model.SSN.IsNotNullOrEmpty()) ? Model.SSN : "", new { @id = "New_Patient_SSN", @class = "text digits input_wrapper", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_StartOfCareDate" class="float-left"><span class="green">(M0030)</span> Start of Care Date:</label>
                <div class="float-right"><input type="date" name="StartOfCareDate" id="New_Patient_StartOfCareDate" class="required" onchange="Patient.OnSocChange()" /></div>
            </div>
            <div class="row">
                <label for="New_Patient_EpisodeStartDate" class="float-left">Episode Start Date:</label>
                <div class="float-right"><input type="date" name="EpisodeStartDate" id="New_Patient_EpisodeStartDate" class="required" /></div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input id="New_Patient_CreateEpisode" type="checkbox" checked="checked" value="true" name="ShouldCreateEpisode" class="radio float-left" />
                        <label for="New_Patient_CreateEpisode" class="radio">Create Episode &#38; Schedule Start of Care Visit after saving</label>
                        <em>(If this box is unchecked, patient will be added to list of pending admissions)</em>
                    </div>
                    <div class="option">
                        <input id="New_Patient_IsFaceToFaceEncounterCreated" type="checkbox" checked="checked" value="true" name="IsFaceToFaceEncounterCreated" class="radio float-left" />
                        <label for="New_Patient_IsFaceToFaceEncounterCreated" class="radio">Create a face to face encounter</label>
                        <em>(this is applicable for SOC date after 01/01/2011)</em>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0140)</span> Race/Ethnicity</legend>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <input id="New_Patient_RaceAmericanIndian" type="checkbox" value="0" name="EthnicRaces" class="required radio float-left" />
                        <label for="New_Patient_RaceAmericanIndian" class="radio">American Indian or Alaska Native</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_RaceAsian" type="checkbox" value="1" name="EthnicRaces" class="required radio float-left" />
                        <label for="New_Patient_RaceAsian" class="radio">Asian</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_RaceBlack" type="checkbox" value="2" name="EthnicRaces" class="required radio float-left" />
                        <label for="New_Patient_RaceBlack" class="radio">Black or African-American</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_RaceHispanic" type="checkbox" value="3" name="EthnicRaces" class="required radio float-left" />
                        <label for="New_Patient_RaceHispanic" class="radio">Hispanic or Latino</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_RaceHawaiian" type="checkbox" value="4" name="EthnicRaces" class="required radio float-left" />
                        <label for="New_Patient_RaceHawaiian" class="radio">Native Hawaiian or Pacific Islander</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_RaceWhite" type="checkbox" value="5" name="EthnicRaces" class="required radio float-left" />
                        <label for="New_Patient_RaceWhite" class="radio">White</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_RaceUnknown" type="checkbox" value="6" name="EthnicRaces" class="required radio float-left" />
                        <label for="New_Patient_RaceUnknown" class="radio">Unknown</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0150)</span> Payment Source <span class="light">(Mark all that apply)</span></legend>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <input id="New_Patient_PaymentSourceNone" type="checkbox" value="0" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourceNone" class="radio">None; no charge for current services</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_PaymentSourceMedicare" type="checkbox" value="1" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourceMedicare" class="radio">Medicare (traditional fee-for-service)</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_PaymentSourceMedicareHmo" type="checkbox" value="2" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourceMedicareHmo" class="radio">Medicare (HMO/ managed care)</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_PaymentSourceMedicaid" type="checkbox" value="3" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourceMedicaid" class="radio">Medicaid (traditional fee-for-service)</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_PaymentSourceMedicaidHmo" type="checkbox" value="4" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourceMedicaidHmo" class="radio">Medicaid (HMO/ managed care)</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_PaymentSourceWorkers" type="checkbox" value="5" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourceWorkers" class="radio">Workers&#8217; compensation</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_PaymentSourceTitleProgram" type="checkbox" value="6" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourceTitleProgram" class="radio">Title programs (e.g., Titile III,V, or XX)</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_PaymentSourceOtherGovernment" type="checkbox" value="7" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourceOtherGovernment" class="radio">Other government (e.g.,CHAMPUS,VA,etc)</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_PaymentSourcePrivate" type="checkbox" value="8" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourcePrivate" class="radio">Private insurance</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_PaymentSourcePrivateHmo" type="checkbox" value="9" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourcePrivateHmo" class="radio">Private HMO/ managed care</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_PaymentSourceSelf" type="checkbox" value="10" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourceSelf" class="radio">Self-pay</label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_PaymentSourceUnknown" type="checkbox" value="11" name="PaymentSources" class="required radio float-left" />
                        <label for="New_Patient_PaymentSourceUnknown" class="radio">Unknown</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" id="New_Patient_PaymentSource" value="12" name="PaymentSources" class="radio float-left" />
                        <label for="New_Patient_PaymentSource" class="radio more">Other (specify)</label>
                        <%= Html.TextBox("OtherPaymentSource", "", new { @id = "New_Patient_OtherPaymentSource", @class = "text", @style = "display:none;" }) %>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Address</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_AddressLine1" class="float-left">Address Line 1:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine1", (Model != null && Model.AddressLine1.IsNotNullOrEmpty()) ? Model.AddressLine1.ToString() : "", new { @id = "New_Patient_AddressLine1", @class = "text required input_wrapper", @maxlength="50" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_AddressLine2" class="float-left">Address Line 2:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine2", (Model != null && Model.AddressLine2.IsNotNullOrEmpty()) ? Model.AddressLine2.ToString() : "", new { @id = "New_Patient_AddressLine2", @class = "text input_wrapper", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_AddressCity" class="float-left">City:</label>
                <div class="float-right"><%= Html.TextBox("AddressCity", (Model != null && Model.AddressCity.IsNotNullOrEmpty()) ? Model.AddressCity.ToString() : "", new { @id = "New_Patient_AddressCity", @class = "text required input_wrapper", @maxlength = "50" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_AddressStateCode" class="float-left"><span class="green">(M0050)</span> State, <span class="green">(M0060)</span> Zip:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", (Model != null && Model.AddressStateCode.IsNotNullOrEmpty()) ? Model.AddressStateCode.ToString() : "", new { @id = "New_Patient_AddressStateCode", @class = "AddressStateCode required notzero valid" })%><%= Html.TextBox("AddressZipCode", (Model != null && Model.AddressZipCode.IsNotNullOrEmpty()) ? Model.AddressZipCode.ToString() : "", new { @id = "New_Patient_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Patient_HomePhone1" class="float-left">Home Phone:</label>
                <div class="float-right"><%= Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() ? Model.PhoneHome.Substring(0, 3) : "", new { @id = "New_Patient_HomePhone1", @class = "input_wrappermultible autotext required digits phone-short", @maxlength = "3", @size = "3" })%> - <%= Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() ? Model.PhoneHome.Substring(3, 3) : "", new { @id = "New_Patient_HomePhone2", @class = "autotext required digits phone-short", @maxlength = "3", @size = "3" })%> - <%= Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() ? Model.PhoneHome.Substring(6, 4) : "", new { @id = "New_Patient_HomePhone3", @class = "autotext required digits phone-long", @maxlength = "4", @size = "5" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_MobilePhone1" class="float-left">Mobile Phone:</label>
                <div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone-short" name="PhoneMobileArray" id="New_Patient_MobilePhone1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone-short" name="PhoneMobileArray" id="New_Patient_MobilePhone2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone-long" name="PhoneMobileArray" id="New_Patient_MobilePhone3" maxlength="4" /></div>
            </div>
            <div class="row">
                <label for="New_Patient_Email" class="float-left">Email:</label>
                <div class="float-right"><%= Html.TextBox("EmailAddress", (Model != null && Model.EmailAddress.IsNotNullOrEmpty()) ? Model.EmailAddress : "", new { @id = "New_Patient_Email", @class = "text input_wrapper", @maxlength = "50" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_PhysicianDropDown1" class="float-left">Primary Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Patient_PhysicianDropDown1", @class = "physicians" })%><a class="abs addrem" onclick="Patient.addPhysician(this);return false" status="Add Row">+</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Patient_PhysicianDropDown2" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Patient_PhysicianDropDown2", @class = "physicians" })%><a class="abs addrem" onclick="Patient.removePhysician(this);return false" status="Remove Row">-</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Patient_PhysicianDropDown3" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Patient_PhysicianDropDown3", @class = "physicians" })%><a class="abs addrem" onclick="Patient.removePhysician(this);return false" status="Remove Row">-</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Patient_PhysicianDropDown4" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Patient_PhysicianDropDown4", @class = "physicians" })%><a class="abs addrem" onclick="Patient.removePhysician(this);return false" status="Remove Row">-</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Patient_PhysicianDropDown5" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Patient_PhysicianDropDown5", @class = "physicians" })%><a class="abs addrem" onclick="Patient.removePhysician(this);return false" status="Remove Row">-</a></div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Insurance / Payor Information</legend>
        <div class="column">
            <div class="row">
                <% var htmlAttributesPrimary = new Dictionary<string, string>();htmlAttributesPrimary.Add("id", "New_Patient_PrimaryInsurance");htmlAttributesPrimary.Add("class", "insurances required notzero");%>
                <label for="New_Patient_PrimaryInsurance" class="float-left">Primary:</label>
                <div class="float-right"><%= Html.Insurances("PrimaryInsurance", "", true, htmlAttributesPrimary)%></div>
            </div>
            <div class="row hidden" id="New_Patient_PrimaryHealthPlanIdContent">
                <label for="New_Patient_PrimaryHealthPlanId" class="float-left">Primary Insurance Health Plan Id:</label>
                <div class="float-right"><%= Html.TextBox("PrimaryHealthPlanId", "", new { @id = "New_Patient_PrimaryHealthPlanId", @class = "" })%></div>
            </div>
            <div class="row">
                <% var htmlAttributesSecondary = new Dictionary<string, string>(); htmlAttributesSecondary.Add("id", "New_Patient_SecondaryInsurance"); htmlAttributesSecondary.Add("class", "insurances"); %>
                <label for="New_Patient_SecondaryInsurance" class="float-left">Secondary:</label>
                <div class="float-right"><%= Html.Insurances("SecondaryInsurance", "", true, htmlAttributesSecondary)%></div>
            </div>
            <div class="row hidden" id="New_Patient_SecondaryHealthPlanIdContent">
                <label for="New_Patient_SecondaryHealthPlanId" class="float-left">Secondary Insurance Health Plan Id:</label>
                <div class="float-right"><%= Html.TextBox("SecondaryHealthPlanId", "", new { @id = "New_Patient_SecondaryHealthPlanId", @class = "" })%></div>
            </div>
            <div class="row">
                <% var htmlAttributesTertiary = new Dictionary<string, string>();htmlAttributesTertiary.Add("id", "New_Patient_TertiaryInsurance"); htmlAttributesTertiary.Add("class", "insurances"); %>
                <label for="New_Patient_TertiaryInsurance" class="float-left">Tertiary:</label>
                <div class="float-right"><%= Html.Insurances("TertiaryInsurance", "", true, htmlAttributesTertiary)%></div>
            </div>
            <div class="row hidden" id="New_Patient_TertiaryHealthPlanIdContent">
                <label for="New_Patient_TertiaryHealthPlanId" class="float-left">Tertiary Insurance Health Plan Id:</label>
                <div class="float-right"><%= Html.TextBox("TertiaryHealthPlanId", "", new { @id = "New_Patient_TertiaryHealthPlanId", @class = "" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-right">
        <legend>Emergency Triage <span class="light">(Select one)</span></legend>
        <div class="column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input id="New_Patient_Triage1" type="radio" value="1" name="Triage" class="radio Triage float-left" />
                        <label for="New_Patient_Triage1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Life threatening (or potential) and requires ongoing medical treatment. When necessary, appropriate arrangements for evacuation to an acute care facility will be made.</span>
                        </label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_Triage2" type="radio" value="2" name="Triage" class="radio Triage float-left" />
                        <label for="New_Patient_Triage2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Not life threatening but would suffer severe adverse effects from interruption of services (i.e., daily insulin, IV medications, sterile wound care of a wound with a large amount of drainage.)</span>
                        </label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_Triage3" type="radio" value="3" name="Triage" class="radio Triage float-left" />
                        <label for="New_Patient_Triage3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Visits could be postponed 24-48 hours without adverse effects (i.e., new insulin dependent diabetic able to self inject, sterile wound care with a minimal amount to no drainage)</span>
                        </label>
                    </div>
                    <div class="option">
                        <input id="New_Patient_Triage4" type="radio" value="4" name="Triage" class="radio Triage float-left" />
                        <label for="New_Patient_Triage4">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Visits could be postponed 72-96 hours without adverse effects (i.e., post op with no open wound, anticipated discharge within the next 10-14 days, routine catheter changes)</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-left">
        <legend>Primary Emergency Contact</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_EmergencyContactFirstName" class="float-left">First Name:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.FirstName", "", new { @id = "New_Patient_EmergencyContactFirstName", @class = "text input_wrapper", @maxlength = "100" }) %></div>
            </div><div class="row">
                <label for="New_Patient_EmergencyContactLastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.LastName", "", new { @id = "New_Patient_EmergencyContactLastName", @class = "text input_wrapper", @maxlength = "100" }) %></div>
            </div><div class="row">
                <label for="New_Patient_EmergencyContactRelationship" class="float-left">Relationship:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.Relationship", "", new { @id = "New_Patient_EmergencyContactRelationship", @class = "text input_wrapper" }) %></div>
            </div><div class="row">
                <label for="New_Patient_EmergencyContactPhonePrimary1" class="float-left">Primary Phone:</label>
                <div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone-short" name="EmergencyContact.PhonePrimaryArray" id="New_Patient_EmergencyContactPhonePrimary1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone-short" name="EmergencyContact.PhonePrimaryArray" id="New_Patient_EmergencyContactPhonePrimary2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone-long" name="EmergencyContact.PhonePrimaryArray" id="New_Patient_EmergencyContactPhonePrimary3" maxlength="4" /></div>
            </div><div class="row">
                <label for="New_Patient_EmergencyContactPhoneAlternate1" class="float-left">Alternate Phone:</label>
                <div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone-short" name="EmergencyContact.PhoneAlternateArray" id="New_Patient_EmergencyContactPhoneAlternate1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone-short" name="EmergencyContact.PhoneAlternateArray" id="New_Patient_EmergencyContactPhoneAlternate2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone-long" name="EmergencyContact.PhoneAlternateArray" id="New_Patient_EmergencyContactPhoneAlternate3" maxlength="4" /></div>
            </div><div class="row">
                <label for="New_Patient_EmergencyContactEmail" class="float-left">Email:</label>
                <div class="float-right"><%= Html.TextBox("EmergencyContact.EmailAddress", "", new { @id = "New_Patient_EmergencyContactEmail", @class = "text email input_wrapper", @maxlength = "100" }) %></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float-right">
        <legend>Pharmacy Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_PharmacyName" class="float-left">Name:</label>
                <div class="float-right"><%= Html.TextBox("PharmacyName", "", new { @id = "New_Patient_PharmacyName", @class = "text input_wrapper", @maxlength = "100" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_PharmacyPhone" class="float-left">Phone:</label>
                <div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone-short" name="PharmacyPhoneArray" id="New_Patient_PharmacyPhone1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone-short" name="PharmacyPhoneArray" id="New_Patient_PharmacyPhone2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone-long" name="PharmacyPhoneArray" id="New_Patient_PharmacyPhone3" maxlength="4" /></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_ReferralPhysician" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("ReferrerPhysician", (Model != null && !Model.ReferrerPhysician.IsEmpty()) ? Model.ReferrerPhysician.ToString() : "", new { @id = "New_Patient_ReferrerPhysician", @class = "physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
            </div>
            <div class="row">
                <label for="New_Patient_AdmissionSource" class="float-left">Admission Source:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", (Model != null && Model.AdmissionSource > 0) ? Model.AdmissionSource.ToString() : "", new { @id = "New_Patient_AdmissionSource", @class = "AdmissionSource" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_Patient_OtherReferralSource" class="float-left">Other Referral Source:</label>
                <div class="float-right"><%= Html.TextBox("OtherReferralSource", (Model != null && Model.OtherReferralSource.IsNotNullOrEmpty()) ? Model.OtherReferralSource : "", new { @id = "New_Patient_OtherReferralSource", @class = "text input_wrapper", @maxlength = "30" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_PatientReferralDate" class="float-left">Referral Date:</label>
                <div class="float-right"><input type="date" name="ReferralDate" id="New_Patient_PatientReferralDate" class="required" /></div>
            </div>
            <div class="row">
                <label for="New_Patient_InternalReferral" class="float-left">Internal Referral:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", (Model != null && !Model.InternalReferral.IsEmpty()) ? Model.InternalReferral.ToString() : "", new { @id = "New_Patient_InternalReferral", @class = "Users valid" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Services Required <span class="light">(Optional)</span></legend>
        <div class="wide-column">
            <div class="row">
                <%string[] servicesRequired = Model != null && Model.ServicesRequired != null && Model.ServicesRequired.IsNotNullOrEmpty() ? Model.ServicesRequired.Split(';') : null;  %>
                <input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id ='New_Patient_ServicesRequiredCollection0' type='checkbox' value='0' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("0") ? "checked='checked'" : "" )%>
                        <label for="New_Patient_ServicesRequiredCollection0" class="radio">SNV</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='New_Patient_ServicesRequiredCollection1' type='checkbox' value='1' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("1") ? "checked='checked'" : "")%>
                        <label for="New_Patient_ServicesRequiredCollection1" class="radio">HHA</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='New_Patient_ServicesRequiredCollection2' type='checkbox' value='2' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("2") ? "checked='checked'" : "")%>
                        <label for="New_Patient_ServicesRequiredCollection2" class="radio">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='New_Patient_ServicesRequiredCollection3' type='checkbox' value='3' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("3") ? "checked='checked'" : "")%>
                        <label for="New_Patient_ServicesRequiredCollection3" class="radio">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='New_Patient_ServicesRequiredCollection4' type='checkbox' value='4' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("4") ? "checked='checked'" : "")%>
                        <label for="New_Patient_ServicesRequiredCollection4" class="radio">ST</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='New_Patient_ServicesRequiredCollection5' type='checkbox' value='5' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("5") ? "checked='checked'" : "")%>
                        <label for="New_Patient_ServicesRequiredCollection5" class="radio">MSW</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>DME Needed <span class="light">(Optional)</span></legend>
        <div class="wide-column">
            <div class="row">
                <%  string[] DME = (Model != null && Model.DME.IsNotNullOrEmpty()) ? Model.DME.Split(';') : null;  %>
                <input type="hidden" value=" " class="radio" name="DMECollection" />
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='New_Patient_DMECollection0' type='checkbox' value='0' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("0") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollection0" class="radio">Bedside Commode</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='New_Patient_DMECollection1' type='checkbox' value='1' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("1") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollection1" class="radio">Cane</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='New_Patient_DMECollection2' type='checkbox' value='2' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("2") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollection2" class="radio">Elevated Toilet Seat</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='New_Patient_DMECollection3' type='checkbox' value='3' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("3") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollection3" class="radio">Grab Bars</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='New_Patient_DMECollection4' type='checkbox' value='4' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("4") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollection4" class="radio">Hospital Bed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='New_Patient_DMECollection5' type='checkbox' value='5' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("5") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollection5" class="radio">Nebulizer</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='New_Patient_DMECollection6' type='checkbox' value='6' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("6") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollection6" class="radio">Oxygen</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='New_Patient_DMECollection7' type='checkbox' value='7' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("7") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollection7" class="radio">Tub/Shower Bench</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='New_Patient_DMECollection8' type='checkbox' value='8' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("8") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollection8" class="radio">Walker</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='New_Patient_DMECollection9' type='checkbox' value='9' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("9") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollection9" class="radio">Wheelchair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='New_Patient_DMECollection10' type='checkbox' value='10' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("10") ? "checked='checked'" : "")%>
                        <label for="New_Patient_DMECollection10" class="radio">Other</label>
                        <%= Html.TextBox("OtherDME", (Model != null && Model.OtherDME.IsNotNullOrEmpty()) ? Model.OtherDME : "", new { @id = "New_Patient_OtherDME", @class = "text", @style = "display:none;" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac">
                <%= Html.TextArea("Comments", (Model != null && Model.Comments.IsNotNullOrEmpty()) ? Model.Comments : "", new { @id = "New_Patient_Comments", @class = "text tall", @maxcharacters = "500" })%>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "1", new { @id = "New_Patient_Status" })%>     
    <%= Html.Hidden("ReferralId", (Model != null && !Model.Id.IsEmpty()) ? Model.Id.ToString() : "false", new { @id = "New_Patient_ReferralId" })%>     
    <div class="buttons">
        <ul>
            <li><a onclick="$('#New_Patient_Status').val('3')" class="save">Save</a></li>
            <li><a onclick="$('#New_Patient_Status').val('1')" class="save">Admit Patient</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<% } %>