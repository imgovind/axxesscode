﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main blue">
    <div class="float-right">
        <div class="buttons">
            <ul><li><a href="javascript:void(0);" onclick="User.RebindScheduleList();">Refresh</a></li></ul>
            <br />
            <ul><li><a href="Export/ScheduleList">Excel Export</a></li></ul>
        </div>
    </div>
    <fieldset class="grid-controls">
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="User.LoadUserSchedule('PatientName');">Group By Patient</a></li>
                <li><a href="javascript:void(0);" onclick="User.LoadUserSchedule('VisitDate');">Group By Date</a></li>
                <li><a href="javascript:void(0);" onclick="User.LoadUserSchedule('TaskName');">Group By Task</a></li>
            </ul>
        </div>
        <div class="ac">
            <em>Note: This list shows you items/tasks dated 3 months into the past and 2 weeks into the future. To find older items, look in the Patient's Chart or Schedule Center.</em>
        </div>
    </fieldset>
    <div id="myScheduledTasksContentId"><% Html.RenderPartial("~/Views/User/ScheduleGrid.ascx"); %></div>
</div>