﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<fieldset class="medication">
    <legend>License</legend>
<%  using (Html.BeginForm("AddLicense", "User", FormMethod.Post, new { @id = "editUserLicenseForm" })) { %>
    <%= Html.Hidden("UserId", Model, new { @id = "Edit_UserLicense_Id" }) %>
    <div class="column">
        <div class="row">
            <label class="float-left" for="Edit_UserLicense_LicenseType">License Type</label>
            <div class="float-right">
                <%= Html.LookupSelectList(SelectListTypes.LicenseTypes, "LicenseType", "", new { @id = "Edit_UserLicense_Type", @class = "valid fill" })%>
                <%= Html.TextBox("OtherLicenseType", "", new { @id = "Edit_UserLicense_TypeOther", @class = "valid fill", @maxlength = "25" }) %>
            </div>
        </div>
        <div class="row">
            <label class="float-left" for="Edit_UserLicense_Attachment">Attachment</label>
            <div class="float-right"><input type="file" name="Attachment" id="Edit_UserLicense_Attachment" /></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="float-left" for="Edit_UserLicense_InitDate">Issue Date</label>
            <div class="float-right"><input type="date" name="InitiationDate" id="Edit_UserLicense_InitDate" class="required" /></div>
        </div>
        <div class="row">
            <label class="float-left" for="Edit_UserLicense_ExpDate">Expiration Date</label>
            <div class="float-right"><input type="date" name="ExpirationDate" id="Edit_UserLicense_ExpDate" class="required" /></div>
        </div>
    </div>
    <div class="buttons">
        <ul>
            <li><a class="save">Add License</a></li>
        </ul>
    </div>
    <div class="wide-column">
        <div class="row currentlicenses">
            <%= Html.Telerik().Grid<License>().Name("List_User_Licenses").DataKeys(keys => {
                    keys.Add(M => M.Id);
                }).Columns(columns => {
                    columns.Bound(l => l.LicenseType).ReadOnly().Sortable(false);
                    columns.Bound(l => l.InitiationDateFormatted).ReadOnly().Title("Issue Date").Sortable(false);
                    columns.Bound(l => l.ExpirationDate).Title("Expiration Date").Sortable(false);
                    columns.Bound(l => l.AssetUrl).Title("Attachment").ReadOnly().Sortable(false);
                    columns.Command(commands => {
                        commands.Edit();
                        commands.Delete();
                    }).Width(135).Title("Action");
                }).DataBinding(dataBinding => dataBinding.Ajax()
                    .Select("LicenseList", "User", new { userId = Model })
                    .Update("UpdateLicense", "User", new { userId = Model })
                    .Delete("DeleteLicense", "User", new { userId = Model })
                ).Sortable().Footer(false).Scrollable(scrolling => scrolling.Enabled(true)) %>
        </div>
    </div>
<%  } %>
</fieldset>
<script type="text/javascript">
    Forms.ShowIfSelectEquals(
        $("#Edit_UserLicense_Type"), "Other",
        $("#Edit_UserLicense_TypeOther"));
</script>