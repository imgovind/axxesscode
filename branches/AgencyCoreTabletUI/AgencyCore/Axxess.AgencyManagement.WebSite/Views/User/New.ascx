﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<bool>" %>
<span class="wintitle">New User | <%= Current.AgencyName.Clean() %></span>
<div id="newUserContent" class="wrapper main">
<%  using (Html.BeginForm("Add", "User", FormMethod.Post, new { @id = "newUserForm" })) { %>

    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_User_EmailAddress" class="float-left">E-mail Address:</label>
                <div class="float-right"><%= Html.TextBox("EmailAddress", "", new { @id = "New_User_EmailAddress", @class = "text required input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="New_User_FirstName" class="float-left">First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", "", new { @id = "New_User_FirstName", @class = "text required names input_wrapper", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="New_User_LastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", "", new { @id = "New_User_LastName", @class = "text required names input_wrapper", @maxlength = "75" })%></div>
            </div>
            <div class="row">
                <label for="New_User_Credentials" class="float-left">Credentials:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.CredentialTypes, "Credentials", "", new { @id = "New_User_Credentials", @class = "required" })%></div>
            </div>
            <div class="row">
                <label for="New_User_OtherCredentials" class="float-left">Other Credentials (specify):</label>
                <div class="float-right"><%= Html.TextBox("CredentialsOther", "", new { @id = "New_User_OtherCredentials", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="New_User_TitleType" class="float-left">Title:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.TitleTypes, "TitleType", "", new { @id = "New_User_TitleType", @class = "TitleType required" })%></div>
            </div>
            <div class="row">
                <label for="New_User_OtherTitleType" class="float-left">Other Title (specify):</label>
                <div class="float-right"><%= Html.TextBox("TitleTypeOther", "", new { @id = "New_User_OtherTitleType", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Employment Type:</label>
                <div class="float-right">
                    <%= Html.RadioButton("EmploymentType", "Employee", new { @id = "New_User_EmploymentType_E", @class = "required radio" })%>
                    <label for="New_User_EmploymentType_E" class="inline-radio">Employee</label>
                    <%= Html.RadioButton("EmploymentType", "Contractor", new { @id = "New_User_EmploymentType_C", @class = "required radio" })%>
                    <label for="New_User_EmploymentType_C" class="inline-radio">Contractor</label>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_User_CustomId" class="float-left">Agency Custom Employee Id:</label>
                <div class="float-right"><%= Html.TextBox("CustomId", "", new { @id = "New_User_CustomId", @class = "text input_wrapper", @maxlength = "20" })%></div>
            </div>
            <div class="row">
                <label for="New_User_LocationId" class="float-left">Agency Branch:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "New_User_LocationId", @class = "BranchLocation" })%></div>
            </div>
            <div class="row">
                <label for="New_User_AddressLine1" class="float-left">Address Line 1:</label>
                <div class="float-right"><%= Html.TextBox("Profile.AddressLine1", "", new { @id = "New_User_AddressLine1", @maxlength = "75", @class = "text input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="New_User_AddressLine2" class="float-left">Address Line 2:</label>
                <div class="float-right"><%= Html.TextBox("Profile.AddressLine2", "", new { @id = "New_User_AddressLine2", @maxlength = "75", @class = "text input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="New_User_AddressCity" class="float-left">City:</label>
                <div class="float-right"><%= Html.TextBox("Profile.AddressCity", "", new { @id = "New_User_AddressCity", @maxlength = "75", @class = "text input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="New_User_AddressStateCode" class="float-left">State, Zip:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", "", new { @id = "New_User_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("Profile.AddressZipCode", "", new { @id = "New_User_AddressZipCode", @class = "text numeric input_wrapper zip", @size = "5", @maxlength = "5" })%></div>
            </div>
            <div class="row">
                <label for="New_User_HomePhoneArray1" class="float-left">Home Phone:</label>
                <div class="float-right">
                    <%= Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray1", @class = "input_wrappermultible autotext digits phone-short", @maxlength = "3", @size = "4" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray2", @class = "input_wrappermultible autotext digits phone-short", @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray3", @class = "input_wrappermultible autotext digits phone-long", @maxlength = "4", @size = "5" })%>
                </div>
            </div>
            <div class="row">
                <label for="New_User_MobilePhoneArray1" class="float-left">Mobile Phone:</label>
                <div class="float-right">
                    <%= Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray1", @class = "input_wrappermultible autotext digits phone-short", @maxlength = "3", @size = "4" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray2", @class = "input_wrappermultible autotext digits phone-short", @maxlength = "3", @size = "3" })%>
                    -
                    <%= Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray3", @class = "input_wrappermultible autotext digits phone-long", @maxlength = "4", @size = "5" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Roles</legend>
        <div class="wide-column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <input id="New_User_Role_1" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="1" />
                        <label for="New_User_Role_1" class="radio">Administrator</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_2" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="2" />
                        <label for="New_User_Role_2" class="radio">Director of Nursing</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_3" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="3" />
                        <label for="New_User_Role_3" class="radio">Case Manager</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_4" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="4" />
                        <label for="New_User_Role_4" class="radio">Nursing</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_5" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="5" />
                        <label for="New_User_Role_5" class="radio">Clerk (non-clinical)</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_6" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="6" />
                        <label for="New_User_Role_6" class="radio">Physical Therapist</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_7" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="7" />
                        <label for="New_User_Role_7" class="radio">Occupational Therapist</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_8" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="8" />
                        <label for="New_User_Role_8" class="radio">Speech Therapist</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_9" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="9" />
                        <label for="New_User_Role_9" class="radio">Medical Social Worker</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_10" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="10" />
                        <label for="New_User_Role_10" class="radio">Home Health Aide</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_11" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="11" />
                        <label for="New_User_Role_11" class="radio">Scheduler</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_12" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="12" />
                        <label for="New_User_Role_12" class="radio">Biller</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_13" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="13" />
                        <label for="New_User_Role_13" class="radio">Quality Assurance</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_14" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="14" />
                        <label for="New_User_Role_14" class="radio">Physician</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_15" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="15" />
                        <label for="New_User_Role_15" class="radio">Office Manager</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_16" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="16" />
                        <label for="New_User_Role_16" class="radio">Community Liason Officer/ Marketer</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_17" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="17" />
                        <label for="New_User_Role_17" class="radio">External Referral Source</label>
                    </div>
                    <div class="option">
                        <input id="New_User_Role_18" type="checkbox" class="radio float-left required" name="AgencyRoleList" value="18" />
                        <label for="New_User_Role_18" class="radio">Driver/Transportation</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Permissions</legend>
        <div class="wide-column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <input id="New_User_AllPermissions" type="checkbox" class="radio float-left" value="" />
                        <label for="New_User_AllPermissions" class="radio">Select all Permissions</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
             <%= Html.PermissionList("Clerical", new List<string>())%>
             <%= Html.PermissionList("Clinical", new List<string>())%>
             <%= Html.PermissionList("OASIS", new List<string>())%>
             <%= Html.PermissionList("Reporting", new List<string>())%>
         </div>
         <div class="column">
             <%= Html.PermissionList("QA", new List<string>())%>
             <%= Html.PermissionList("Schedule Management", new List<string>())%>
             <%= Html.PermissionList("Billing", new List<string>())%>
             <%= Html.PermissionList("Administration", new List<string>())%>
         </div>
    </fieldset>
    <fieldset>
        <legend>Access &#38; Restrictions</legend>
        <div class="column">
            <div class="row">
                <label for="New_User_AllowWeekendAccess" class="float-left">Allow Weekend Access?</label>
                <div class="float-right"><%= Html.CheckBox("AllowWeekendAccess", true, new { @id="New_User_AllowWeekendAccess" })%></div>
            </div>
            <div class="row">
                <label for="New_User_EarliestLoginTime" class="float-left">Earliest Login Time:</label>
                <div class="float-right">
                    <input type="text" size="10" id="New_User_EarliestLoginTime" name="EarliestLoginTime" />
                </div>
            </div>
            <div class="row">
                <label for="New_User_AutomaticLogoutTime" class="float-left">Automatic Logout Time:</label>
                <div class="float-right">
                    <input type="text" size="10" id="New_User_AutomaticLogoutTime" name="AutomaticLogoutTime" />
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac">
                <textarea id="New_User_Comments" name="Comments" cols="5" rows="6" maxcharacters="500" class="taller"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons"><ul>
        <li><a class="save">Add User</a></li>
        <li><a class="close">Cancel</a></li>
    </ul></div>
<%  } %>
</div>
<%  if (Model) { %>
<script type="text/javascript">
    $("#newUserContent").html(U.MessageWarn("Maximum User Accounts", "You have reached your maximum number of user accounts.  Please contact Axxess about upgrading your account."));
</script>
<%  } %>