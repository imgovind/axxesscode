﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Reset <%= Current.DisplayName.Clean() %>&#8217;s Signature | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main">
    <fieldset>
        <legend>Reset Signature</legend>
        <div class="wide-column">
            <div class="row">Click on the button below to reset your signature. An e-mail with instructions on how to reset your signature will be sent to <%= Model %>.</div>
            <div id="resetSignatureMessage" class="errormessage"></div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a id="lnkRequestSignatureReset" href="javascript:void(0);">Reset Signature</a></li>
        </ul>
    </div>
</div>



