<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle"><%= Current.DisplayName.Clean() %>&#8217;s Schedule and Tasks | <%= Current.AgencyName.Clean() %></span>
<%= Html.Telerik().Grid<UserVisit>().Name("List_User_Schedule").HtmlAttributes(new  { @class = "bottom-bar"}).Columns(columns => {
    columns.Bound(v => v.PatientName).Width("18%");
    columns.Bound(v => v.TaskName).ClientTemplate("<#=Url#>").Title("Task").Width("20%");
    columns.Bound(v => v.VisitDate).Title("Date").Width("10%");
    columns.Bound(v => v.StatusName).Width("25%").Title("Status");
    columns.Bound(v => v.StatusComment).Title(" ").Width("3%").ClientTemplate("<a class=\"tooltip red-note\" href=\"javascript:void(0);\" tooltip=\"<#=StatusComment#>\"> </a>");
    columns.Bound(v => v.VisitNotes).Title(" ").Width("3%").ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=VisitNotes#>\"></a>");
    columns.Bound(v => v.EpisodeNotes).Title(" ").Width("3%").ClientTemplate("<a class=\"tooltip blue-note\" href=\"javascript:void(0);\" tooltip=\"<#=EpisodeNotes#>\"></a>");
    columns.Bound(v => v.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowMissedVisitModal('<#=EpisodeId#>','<#=PatientId#>','<#=Id#>');\">Missed Visit Form</a>").Title(" ").Width("15%");
})
    .Groupable(settings => settings.Groups(groups =>
    {
        var data = ViewData["UserScheduleGroupName"].ToString();
        if (data == "PatientName")
        {
            groups.Add(s => s.PatientName);
        }
        else if (data == "VisitDate")
        {
            groups.Add(s => s.VisitDate);
        }
        else if (data == "TaskName")
        {
            groups.Add(s => s.TaskName);
        }
        else
        {
            groups.Add(s => s.VisitDate);
        }
    }))
    .ClientEvents(c => c.OnRowDataBound("Schedule.tooltip"))
    .DataBinding(dataBinding => dataBinding.Ajax().Select("ScheduleList", "User"))
    .Scrollable().Sortable()%>
<script type="text/javascript">
    $("#listuserschedule .t-group-indicator").hide();
    $("#listuserschedule .t-grouping-header").remove();
</script>