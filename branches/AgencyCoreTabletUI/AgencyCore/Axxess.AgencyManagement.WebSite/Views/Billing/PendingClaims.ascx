﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
<span class="wintitle">Pending Claims | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
    <fieldset class="grid-controls">
        <label class="float-left" for="PendingClaim_BranchCode">Branch:</label>
        <div class="float-left"><%= Html.DropDownList("BranchId", Model.Branches, new { @id = "PendingClaim_BranchCode", @class = "valid" })%></div>
        <label class="float-left" for="PendingClaim_PrimaryInsurance">Branch:</label>
        <div class="float-left"><%= Html.DropDownList("PrimaryInsurance", Model.Insurances, new { @id = "PendingClaim_PrimaryInsurance", @class = "Insurances" })%></div>
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Billing.ReLoadPendingClaimRap($('#PendingClaim_BranchCode').val(), $('#PendingClaim_PrimaryInsurance').val()); Billing.ReLoadPendingClaimFinal($('#PendingClaim_BranchCode').val(), $('#PendingClaim_PrimaryInsurance').val());return false">Refresh</a></li>
            </ul>
        </div>
    </fieldset>
    <div id="pendingClaim_rapContent" class="grid-stack top pending-claims"><% Html.RenderPartial("/Views/Billing/PendingClaimRap.ascx",Model); %></div>
    <div id="pendingClaim_finalContent" class="grid-stack bottom pending-claims"><% Html.RenderPartial("/Views/Billing/PendingClaimFinal.ascx", Model); %></div>
</div>
<script type="text/javascript">
    $('#PendingClaim_BranchCode').change(function() { Billing.ReLoadPendingClaimRap($(this).val(), $('#PendingClaim_PrimaryInsurance').val()); Billing.ReLoadPendingClaimFinal($(this).val(), $('#PendingClaim_PrimaryInsurance').val()); });
    $('#PendingClaim_PrimaryInsurance').change(function() { Billing.ReLoadPendingClaimRap($('#PendingClaim_BranchCode').val(), $(this).val()); Billing.ReLoadPendingClaimFinal($('#PendingClaim_BranchCode').val(), $(this).val()); });
</script>