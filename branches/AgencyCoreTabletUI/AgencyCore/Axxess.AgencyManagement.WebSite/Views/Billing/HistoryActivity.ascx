﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<%  var val = Model != null && !Model.IsEmpty() ? Model : Guid.Empty;
    Html.Telerik().Grid<ClaimHistoryLean>().Name("BillingHistoryActivityGrid").Columns(columns =>
    {
        columns.Bound(p => p.Type).Title("Type").Width(50);
        columns.Bound(p => p.EpisodeRange).Title("Episode Range").Width(150);
        columns.Bound(p => p.StatusName).Title("Status");
        columns.Bound(p => p.ClaimAmount).Title("Claim Amount").Format("${0:#0.00}").Width(90);
        columns.Bound(p => p.PaymentAmount).Title("Payment Amount").Format("${0:#0.00}").Width(110);
        columns.Bound(p => p.PaymentDateFormatted).Title("Payment Date").Width(100);
        columns.Bound(p => p.Id).Title("").ClientTemplate("<a onclick=\"U.GetAttachment('Billing/UB04Pdf', { 'patientId': '<#=PatientId#>', 'Id': '<#=Id#>', 'type': '<#=Type#>' });return false\">Print View</a>").Width(75);
        columns.Bound(p => p.Id).ClientTemplate("<a onclick=\"UserInterface.ShowModalEditClaim('<#=PatientId#>','<#=Id#>','<#=Type#>');return false\">Update</a>&nbsp;|&nbsp;<a onclick=\"Billing.DeleteClaim('<#=PatientId#>','<#=Id#>','<#=Type==\"FINAL\"?\"Final\":Type#>');return false\">Delete</a>").Title("Action").Width(120);
        columns.Bound(p => p.EpisodeId).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    }).DetailView(details => details.ClientTemplate(
                    Html.Telerik().Grid<ClaimSnapShotViewData>().HtmlAttributes(new { @style = "position:relative;" })
                                    .Name("ClaimSnapShot_<#= Id #><#= Type #>")
                                         .DataKeys(keys =>
                                         {
                                             keys.Add(r => r.Id).RouteKey("Id");
                                             keys.Add(r => r.BatchId).RouteKey("BatchId");
                                             keys.Add(r => r.Type).RouteKey("Type");
                                         })
                                    .Columns(columns =>
                                    {
                                        columns.Bound(o => o.BatchId).Title("Batch Id").Width(80).ReadOnly();
                                        columns.Bound(o => o.EpisodeRange).Width(150);
                                        columns.Bound(o => o.ClaimDate).Width(140).Title("Claim Date").ReadOnly();
                                        columns.Bound(o => o.PaymentDate).Format("{0:MM/dd/yyyy}").Width(90).Title("Payment Date");
                                        columns.Bound(o => o.PaymentAmount).Format("${0:#0.00}").Title("Payment Amount").Width(130);
                                        columns.Bound(o => o.Status).ClientTemplate("<label><#= StatusName #></label>");
                                        columns.Command(commands =>
                                        {
                                            commands.Edit();
                                        }).Width(70);
                                    })
                                    .DataBinding(dataBinding => dataBinding.Ajax()
                                            .Select("SnapShotClaims", "Billing", new
                                            {
                                                Id = "<#= Id #>",
                                                Type = "<#= Type #>"
                                            }).Update("UpdateSnapShotClaim", "Billing")).ClientEvents(events => events.OnEdit("Billing.onEditSnapShotClaim").OnRowDataBound("onEditSnapShotDataBound")).Footer(false).ToHtmlString()))
    .DataBinding(dataBinding => dataBinding.Ajax().Select("HistoryActivity", "Billing", new { patientId = val, insuranceId = 0 })).ClientEvents(events => events.OnDataBound("Billing.OnClaimDataBound").OnRowSelected("Billing.OnClaimRowSelected").OnDetailViewCollapse("Billing.onClaimDetailViewCollapse").OnDetailViewExpand("Billing.onClaimDetailViewExpand")).Sortable().Selectable().Scrollable().Footer(false).HtmlAttributes(new { Style = "min-width:100px;top:30px;" }).Render(); %>
    <script type="text/javascript">
        function onEditSnapShotDataBound(e) {
            var dataItem = e.dataItem;
            if (dataItem != undefined && dataItem.PaymentDate != undefined) {
                if ($.format.date(dataItem.PaymentDate, "MM/dd/yyyy") == "1/1/1") {
                    e.row.cells[3].innerHTML = '&#160;';
                }
            }
        }
    </script>