﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RemittanceLean>>" %>
<ul>
    <li>
        <span class="remit-count"></span>
		<span class="remit-id">Remittance Id</span>
		<span class="remit-date">Remittance Date</span>
		<span class="remit-payment">Provider Payment</span>
		<span class="remit-claims">Claim Count</span>
		<span class="remit-action">Action</span>
	</li>
</ul>
<ol>
<%  int count = 1; %>
    <%  foreach (var remittance in Model) { %>
    <li class="<%= count % 2 != 0 ? "odd" : "even" %>">
        <span class="remit-count"><%= count++ %>.</span>
        <span class="remit-id"><%= remittance.RemitId %></span>
        <span class="remit-date"><%=  remittance.RemittanceDate > DateTime.MinValue ? remittance.RemittanceDate.ToString("MM/dd/yyyy") : string.Empty%></span>
        <span class="remit-payment"><%= string.Format("${0:#,0.00}", remittance.PaymentAmount) %></span>
        <span class="remit-claims"><%= remittance.TotalClaims %></span>
        <span class="remit-subaction"><a onclick="UserInterface.ShowRemittanceDetail('<%= remittance.Id %>');return false">View Details</a></span>
        <span class="remit-subaction"><a onclick="Billing.RemittanceDelete('<%= remittance.Id %>');return false">Delete</a></span>
    </li>
    <%  } %>
</ol>
