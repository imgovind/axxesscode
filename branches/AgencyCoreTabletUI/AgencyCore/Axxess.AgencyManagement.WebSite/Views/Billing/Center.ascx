﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<span class="wintitle">Create Claims | <%= Current.AgencyName.Clean() %></span>
<div class="main wrapper">
    <fieldset class="grid-controls">
        <label class="float-left" for="Billing_CenterBranchCode">Branch:</label>
        <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.Branches, "BranchId", Model.BranchId.ToString(), new { @id = "Billing_CenterBranchCode", @class = "report_input valid" })%></div>
        <label class="float-left" for="Billing_CenterPrimaryInsurance">Insurance:</label>
        <div class="float-left"><%= Html.InsurancesMedicare("PrimaryInsurance", Model.Insurance.ToString(),true,"Unassigend Insurance", new { @id = "Billing_CenterPrimaryInsurance", @class = "Insurances required notzero" })%></div>
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Billing.ReLoadUpProcessedRap($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val());return false">Refresh</a></li>
                <li><a href="Billing/ClaimsPdf" target="_blank">Print</a></li>
            </ul>
        </div>
    </fieldset>
    <div class="acore-grid">
        <% Html.RenderPartial("RapGrid", Model.RapClaims); %>
        <% Html.RenderPartial("FinalGrid", Model.FinalClaims); %>
    </div>
    <%  if (Current.HasRight(Permissions.GenerateClaimFiles)) { %>
    <div class="buttons" id="Billing_CenterButtons">
        <ul>
            <li><a onclick="Billing.loadGenerate('#Billing_CenterContent');return false">Generate Selected</a></li>
            <li><a onclick="Billing.GenerateAllCompleted('#Billing_CenterContent');return false">Generate All Completed</a></li>
        </ul>
    </div>
    <%  } %>
</div>
<script type="text/javascript">
    $('#Billing_CenterBranchCode').change(function() { Billing.ReLoadUpProcessedRap($(this).val(), $('#Billing_CenterPrimaryInsurance').val()); Billing.ReLoadUpProcessedFinal($(this).val(), $('#Billing_CenterPrimaryInsurance').val()); });
    if ($("#Billing_CenterPrimaryInsurance").val() == "0") { $("#Billing_CenterButtons").hide(); } else { $("#Billing_CenterButtons").show(); }
    $('#Billing_CenterPrimaryInsurance').change(function() { if ($(this).val() == "0") { $("#Billing_CenterButtons").hide(); } else { $("#Billing_CenterButtons").show(); } Billing.ReLoadUpProcessedRap($('#Billing_CenterBranchCode').val(), $(this).val()); Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $(this).val()); }); 
</script>