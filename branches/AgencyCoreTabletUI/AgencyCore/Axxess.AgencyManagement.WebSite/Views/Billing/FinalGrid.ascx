﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimBill>>" %>
<ul>
    <li class="align-center"><h3>FINAL(S)</h3></li>
    <li>
        <span class="final-check"></span>
        <span class="final-patient pointer" onclick="Billing.Sort('Final', 'rapname');">Patient Name</span>
        <span class="final-mr pointer" onclick="Billing.Sort('Final', 'rapid');">MR#</span>
        <span class="final-episode pointer" onclick="Billing.Sort('Final', 'rapeps');">Episode Date</span>
        <span class="final-icon">RAP</span>
        <span class="final-icon">Visit</span>
        <span class="final-icon">Order</span>
        <span class="final-icon">Verified</span>
    </li>
</ul>
<ol>
<%  if (Model != null) { %>
    <%  var finals = Model.Where(c =>c.EpisodeEndDate < DateTime.Now); %>
    <%  int i = 1; %>
    <%  foreach (var final in finals) { %>
    <li class="<%=  (i % 2 != 0 ? "odd" : "even") + (final.IsRapGenerated ? " ready" : " not-ready") %>">
        <span class="final-check">
            <%= i %>.
            <%= (final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified && final.AreOrdersComplete && final.IsRapGenerated) ? "<input name='FinalSelected' class='radio' type='checkbox' value='" + final.Id + "' id='FinalSelected" + final.Id + "' />" : string.Empty %>
        </span>
        <a class="fr" onclick="U.GetAttachment('Billing/FinalPdf', { 'episodeId': '<%= final.EpisodeId %>', 'patientId': '<%= final.PatientId %>' });return false">
            <span class="img icon print"></span>
        </a>
        <a onclick="<%= (final.IsRapGenerated) ? "UserInterface.ShowFinal('" + final.EpisodeId + "','" + final.PatientId + "');return false\" title=\"Final Ready" : "U.Growl('Error: Rap Not Generated', 'error');return false\" title=\"Not Complete" %>">
            <span class="final-patient strong"><%= final.LastName %>, <%= final.FirstName %></span>
        </a>
        <span class="final-mr"><%= final.PatientIdNumber%></span>
        <span class="final-episode">
            <span class="very-hidden"><%= final.EpisodeStartDate != null ? final.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span>
            <%= final.EpisodeStartDate != null ? final.EpisodeStartDate.ToString("MM/dd/yyy") : string.Empty %><%= final.EpisodeEndDate != null ? "&#8211;" + final.EpisodeEndDate.ToString("MM/dd/yyy") : string.Empty %>
        </span>
        <span class="final-icon"><span class="img icon <%= final.IsRapGenerated ? "success-small" : "error-small" %>"></span></span>
        <span class="final-icon"><span class="img icon <%= final.AreVisitsComplete? "success-small" : "error-small" %>"></span></span>
        <span class="final-icon"><span class="img icon <%= final.AreOrdersComplete ? "success-small" : "error-small" %>"></span></span>
        <span class="final-icon"><span class="img icon <%= final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified ? "success-small" : "error-small" %>"></span></span>
    </li>
        <%  i++; %>
    <%  } %>
<%  } %>
</ol>