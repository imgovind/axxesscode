﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<% using (Html.BeginForm("SupplyVerify", "Billing", FormMethod.Post, new { @id = "billingSupply" })) { %>
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("episodeId",Model.EpisodeId) %>
    <%= Html.Hidden("patientId",Model.PatientId) %><%
    var verifiedSupply = Model.Supply.IsNotNullOrEmpty() ? Model.Supply.ToObject<List<Supply>>(): new List<Supply>();
    List<Supply>[] a = new List<Supply>[] { verifiedSupply.Where(v => v.IsBillable).ToList(), verifiedSupply.Where(v => (!v.IsBillable)).ToList()};
    String[] b = new String[] {"Billable Supply","Non-Billable Supply"}; %>
<div id="supplyTab" class="wrapper main">
    <div class="billing">
        <h3 class="align-center">Episode: <%= Model.EpisodeStartDate.ToString("MM/dd/yyyy")%> &#8211; <%= Model.EpisodeEndDate.ToString("MM/dd/yyyy")%></h3>
       
        <%
    for (int g = 0; g < 2; g++) {
        if (a[g] != null && a[g].Count > 0) { %>
        <ul>
            <li class="align-center"><h3><%= b[g] %></h3></li>
            <li><span class="rapcheck"></span><span class="supply">Supply</span><span class="visitdate">Date</span><span class="rapicon">Quantity</span><span class="rapicon">Unit Cost</span><span class="rapicon">Total</span></li>
        </ul>
        <% if (a[g] != null) {
                var supplies = a[g];
                if (supplies != null && supplies.Count > 0) { %>
        <ol><% int i = 1;
                    foreach (var supply in supplies) { %>
            <li class="<%= i % 2 != 0 ? "odd not-ready" : "even not-ready" %>">
                <label for="<%= "Supply" + supply.UniqueIdentifier.ToString() %>">
                    <span class="rapcheck"><%= i%>. <input class="radio" name="SupplyId" type="checkbox" id="<%= "Supply" + supply.UniqueIdentifier.ToString() %>" value="<%= g == 0 ? supply.UniqueIdentifier.ToString() + "' checked='checked" : supply.UniqueIdentifier.ToString() %>" /></span>
                    <span class="supply"><%= supply.Description %></span>
                    <span class="visitdate"><%= supply.Date %></span>
                    <span class="rapicon"><%= supply.Quantity %></span>
                </label>
                $<span class="rapicon"><%= Html.TextBox("UnitCost"+ supply.UniqueIdentifier, supply.UnitCost, new { @class = "text UnitCost st", @maxlength = "7" })%></span>
                $<span class="rapicon"><%= Html.TextBox("Total" + supply.UniqueIdentifier, supply.Total, new { @class = string.Format("text Total st {0}",g==0?"billable":string.Empty), @maxlength = "7" })%></span>
            </li><%
                        i++;
                    } %>
        </ol><%
                    }
                }
            }
        } %>
        <ul class="total"><li class="align-right"><label for="SupplyTotal">Supply Total:</label> $<%= Html.TextBox("SupplyTotal", Model.SupplyTotal, new { @class = "text SupplyTotal st", @maxlength = "7" })%></li></ul>
    </div>
    <div class="buttons"><ul><li><a onclick="Billing.NavigateBack(1);return false">Back</a></li><li><a class="save">Verify and Next</a></li></ul></div>
</div>
<% } %>
<script type="text/javascript">
    Billing.Navigate(3, '#billingSupply');
    $(".Total").attr('readonly', true);
    $("#supplyTab ol li a").each(function() { $(this).find("span .Total").val(parseFloat($.trim($(this).find("span .UnitCost").val())) * parseInt($.trim($(this).find("span.quantity").html()))); });
    $("#supplyTab ol li a").each(function() { $(this).find("span .UnitCost").bind("change", function() { $(this).closest("li a").find("span .Total").val(parseFloat($.trim($(this).val())) * parseInt($.trim($(this).closest("li a").find("span.quantity").html()))); sum(); }); });
    $("input[name=SupplyId]").each(function() { $(this).bind("click", function() { if ($(this).attr("checked") == true) { $(this).closest("li a").find("span .Total").removeClass('billable').addClass('billable'); } else { $(this).closest("li a").find("span .Total").removeClass('billable'); } sum(); }); });
    function sum() {
        var total = 0;
        $("#supplyTab ol li a span .Total.billable").each(function() { total = parseFloat(total) + parseFloat($(this).val()); });
        $(".SupplyTotal").val(total);
    }
    $("#FinalTabStrip-3 ol").each(function() { $("li:first", $(this)).addClass("first");  $("li:last", $(this)).addClass("last"); });
</script>