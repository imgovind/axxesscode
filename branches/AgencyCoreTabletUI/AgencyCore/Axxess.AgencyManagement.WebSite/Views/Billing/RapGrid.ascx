﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimBill>>" %>
<ul>
    <li class="ac"><h3>RAP(S)</h3></li>
    <li>
        <span class="rap-check"></span>
        <span class="rap-patient pointer" onclick="Billing.Sort('Rap', 'rapname');">Patient Name</span>
        <span class="rap-mr pointer" onclick="Billing.Sort('Rap', 'rapid');">MR#</span>
        <span class="rap-episode pointer" onclick="Billing.Sort('Rap', 'rapeps');">Episode Date</span>
        <span class="rap-icon">OASIS</span>
        <span class="rap-icon">Billable Visit</span>
        <span class="rap-icon">Verified</span>
    </li>
</ul>
<ol>
<%  if (Model != null) { %>
    <%  int i = 1; %>
    <%  foreach (var rap in Model) { %>
    <li class="<%= (i % 2 != 0 ? "odd" : "even") + (rap.IsOasisComplete && rap.IsFirstBillableVisit ? " ready" : " not-ready") %>">
        <span class="rap-check">
            <%= i %>.
            <%= rap.IsOasisComplete && rap.IsFirstBillableVisit && rap.IsVerified ? "<input name='RapSelected' class='radio' type='checkbox' value='" + rap.Id + "' id='RapSelected" + rap.Id + "' />" : string.Empty %>
        </span>
        <a class="fr" onclick="U.GetAttachment('Billing/RapPdf', { 'episodeId': '<%= rap.EpisodeId %>', 'patientId': '<%= rap.PatientId %>' });return false">
            <span class="img icon print"></span>
        </a>
        <a onclick="<%= (rap.IsOasisComplete && rap.IsFirstBillableVisit) ? "UserInterface.ShowRap('" + rap.EpisodeId + "','" + rap.PatientId + "');return false\" title=\"Ready" : "U.Growl('Error: Not Completed', 'error');return false\" title=\"Not Complete" %>">
            <span class="rap-patient strong"><%= rap.LastName %>, <%= rap.FirstName %></span>
        </a>
        <span class="rap-mr"><%= rap.PatientIdNumber%></span>
        <span class="rap-episode">
            <span class="very-hidden"><%= rap.EpisodeStartDate != null ? rap.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span>
            <%= rap.EpisodeStartDate != null ? rap.EpisodeStartDate.ToString("MM/dd/yyyy") : string.Empty %><%= rap.EpisodeEndDate != null ? "&#8211;" + rap.EpisodeEndDate.ToString("MM/dd/yyyy") : string.Empty %>
        </span>
        <span class="rap-icon"><span class="img icon <%= rap.IsOasisComplete ? "success-small" : "error-small" %>"></span></span>
        <span class="rap-icon"><span class="img icon <%= rap.IsFirstBillableVisit ? "success-small" : "error-small" %>"></span></span>
        <span class="rap-icon"><span class="img icon <%= rap.IsOasisComplete && rap.IsFirstBillableVisit && rap.IsVerified ? "success-small" : "error-small" %>"></span></span>
    </li>
        <%  i++; %>
    <%  } %>
<%  } %>
</ol>
<script type="text/javascript">
    $("#Billing_CenterContentRap ol li:first").addClass("first");
    $("#Billing_CenterContentRap ol li:last").addClass("last");
</script>
