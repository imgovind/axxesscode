﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<MedicareEligibility>>" %>
<%  if (Model != null && Model.Count > 0) { %>
<ul>
    <li>
        <span class="elig-task">Task</span>
        <span class="elig-episode">Episode</span>
        <span class="elig-assigned">Assigned</span>
        <span class="elig-status">Status</span>
        <span class="elig-date">Date</span>
        <span class="elig-icon"></span>
    </li>
</ul>
<ol>
    <%  int i = 1; %>
    <%  foreach (var item in Model) { %>
    <li class="<%= i % 2 != 0 ? "odd" : "even" %>">
        <span class="elig-task"><%= item.TaskName %></span>
        <span class="elig-episode"><%= item.EpisodeRange %></span>
        <span class="elig-assigned"><%= item.AssignedTo %></span>
        <span class="elig-status"><%= item.StatusName %></span>
        <span class="elig-date"><%= item.Created.ToShortDateString().ToZeroFilled() %></span>
        <span class="elig-icon"><%= item.PrintUrl %></span>
    </li>
        <%  i++; %>
    <%  } %>
</ol>
<%  } else { %>
    <div id="elig-error"></div>
    <script type="text/javascript">
        $("#elig-error").html(U.MessageWarn("No Reports Found", "No medicare eligibility reports found for this patient."));
    </script>
<%  } %>