﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
<span class="wintitle">Billing History | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top">
            <div class="row"><label>Branch:</label><div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new { @class = "billingBranchCode" })%></div></div>
            <div class="row"><label>View:</label><div><select name="list" class="billingStatusDropDown"><option value="1">Active Patients</option><option value="2">Discharged Patients</option></select></div></div>
            <div class="row"><label>Filter:</label><div><%= Html.DropDownList("list", Model.Insurances, new {  @class = "billingPaymentDropDown" })%></div></div>
            <div class="row"><label>Find:</label><div><input id="txtSearch_billing_Selection" class="text" name="" value="" type="text" /></div></div>
        </div>
        <div class="bottom"><%Html.RenderPartial("PatientList", Model.SelecetdInsurance); %></div>
    </div>
    <div id="billingMainResult" class="ui-layout-center">
        <div class="top">
            <div class="winmenu">
                <ul id="billingHistoryTopMenu">
                    <li><a onclick="UserInterface.ShowNewClaimModal('RAP');return false">New RAP</a></li>
                    <li><a onclick="UserInterface.ShowNewClaimModal('Final');return false">New Final</a></li>
                </ul>
            </div>
            <div id="billingHistoryClaimData"></div>
        </div>
        <div class="bottom"><% Html.RenderPartial("HistoryActivity", Guid.Empty); %></div>   
    </div>
</div>
<script type="text/javascript">
    $('#window_billingHistory .layout').layout({ west__paneSelector: '.ui-layout-west' });
    $('#window_billingHistory .t-grid-content').css({ height: 'auto', top: "26px" });
</script>