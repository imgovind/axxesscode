﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<% var billType = new SelectList(new[] {new SelectListItem { Text = "Initial Final", Value = "0" },new SelectListItem { Text = "Final Cancellation", Value = "1" }}, "Value", "Text",Model.Type); %>
<% var diagnosis = XElement.Parse(Model.DiagnosisCode); %>
<% var conditionCodes = Model.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(Model.ConditionCodes) : null; %>
<% using (Html.BeginForm("InfoVerify", "Billing", FormMethod.Post, new { @id = "billingInfo" })) { %>
<fieldset>
    <div class="column">
        <%= Html.Hidden("Id",Model.Id) %>
        <%= Html.Hidden("PatientId", Model.PatientId)%>
        <%= Html.Hidden("IsMedicareHMO", Model.IsMedicareHMO)%>
        <div class="row"><label for="FirstName" class="float-left">Patient First Name:</label><div class="float-right"><%= Html.TextBox("FirstName", Model.FirstName, new { @class = "text input_wrapper required", @maxlength = "20" })%></div></div>
        <div class="row"><label for="LastName" class="float-left">Patient Last Name:</label><div class="float-right"><%= Html.TextBox("LastName", Model.LastName, new { @class = "text input_wrapper required", @maxlength = "20" })%></div></div>
        <div class="row"><label for="MedicareNumber" class="float-left">Medicare #:</label><div class="float-right"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber , new { @class = "text input_wrapper required", @maxlength = "11" })%></div></div>
        <div class="row"><label for="PrimaryInsuranceId" class="float-left">Insurance:</label><div class="float-right"><%= Html.InsurancesMedicare("PrimaryInsuranceId", (Model != null ? Model.PrimaryInsuranceId.ToString() : "0"), true, "-- Select Insurnace --", new { @class = "required notzero", @id = "Final_PrimaryInsuranceId" })%></div></div>
        <div id="Final_MedicareHMOInformations">
             <div class="row"><label for="HealthPlanId" class="float-left">Primary Insurance Health Plan Id:</label><div class="float-right"><%= Html.TextBox("HealthPlanId", Model.HealthPlanId, new { @class = "hmoflag" })%></div></div>
             <div class="row"><label for="AuthorizationNumber" class="float-left">Authorization Number:</label><div class="float-right"><%= Html.TextBox("AuthorizationNumber", Model.AuthorizationNumber, new { @class = "text input_wrapper hmoflag", @maxlength = "30" })%></div></div>
        </div>
        <div class="row"><label for="PatientIdNumber" class="float-left">Patient Record #:</label><div class="float-right"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @class = "text input_wrapper required", @maxlength = "11" })%></div></div>
        <div class="row"><label class="float-left">Gender:</label><div class="float-right"><%= Html.RadioButton("Gender", "Female",  Model.Gender == "Female" ? true : false, new { @id = "GenderF", @class = "radio" })%><label for="GenderF" class="inline-radio">Female</label><%= Html.RadioButton("Gender", "Male", Model.Gender == "Male" ? true : false, new { @id = "GenderM", @class = "radio" })%><label for="GenderM" class="inline-radio">Male</label></div></div>
        <div class="row"><label for="DOB" class="float-left">Date of Birth:</label><div class="float-right"><input type="date" name="DOB" value="<%= Model.DOB.IsValid() ? Model.DOB.ToShortDateString() : string.Empty %>" id="FinalDOB" class="required" /></div></div>
        <div class="row"><label for="EpisodeStartDate" class="float-left">Episode Start Date:</label><div class="float-right"><input type="date" name="EpisodeStartDate" value="<%= Model.EpisodeStartDate.ToShortDateString() %>" class="required" id="Final_EpisodeStartDate" /></div></div>
        <div class="row"><span class="float-right">Recommended/ Previously Entered First Billable Date: <%= Model.IsFinalInfoVerified ? Model.FirstBillableVisitDate.ToShortDateString() : Model.FirstBillableVisitDateFormat %></span>
            <div class="clear"></div>
            <label for="FirstBillableVisitDateFormatInput" class="float-left">First Billable Visit Date:</label>
            <div class="float-right"><input type="date" name="FirstBillableVisitDateFormatInput" value="<%= Model.IsFinalInfoVerified ? Model.FirstBillableVisitDate.ToShortDateString() : Model.FirstBillableVisitDateFormat %>" class="required" id="NewFinal_FirstBillableVisitDateFormatInput" /></div>
            <br />
            <em>Please Verifiy the first billable visit date from the schedule.</em>
            <div class="buttons float-right"><ul><li><a onclick="UserInterface.ShowScheduleCenter('<%= Model.PatientId %>');return false">View Schedule</a></li></ul></div>
        </div>
        <div class="row"><label for="StartOfCareDate" class="float-left">Admission Date:</label><div class="float-right"><input type="date" name="StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" class="required" id="Final_StartOfCareDate" /></div></div>
        <div class="row"><label for="AdmissionSource" class="float-left">Admission Source:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", (Model != null && Model.AdmissionSource.IsNotNullOrEmpty() && int.Parse(Model.AdmissionSource) > 0) ? Model.AdmissionSource.ToString() : "", new { @class = "AdmissionSource requireddropdown" })%></div></div>
        <div class="row"><label for="PatientStatus" class="float-left">Patient Status:</label><div class="float-right"><%=Html.UB4PatientStatus("UB4PatientStatus", Model.UB4PatientStatus, new { @id = "Final_PatientStatus", @class = "requireddropdown" })%></div></div>
        <div class="row" id="FinalPatientStatusRow" style="<%= Model.UB4PatientStatus != "30" && Model.UB4PatientStatus != "0" ? string.Empty : "display:none;" %>"><label for="DischargeDate" class="float-left">Discharge Date:</label><div class="float-right"><input type="date" name="DischargeDate" value="<%= Model.DischargeDate.ToShortDateString() %>" id="Final_DischargeDate" /></div></div>
        <div class="row"><label for="AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("AddressLine1",Model.AddressLine1 , new { @class = "text required" }) %></div></div>
        <div class="row"><label for="AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("AddressLine2",Model.AddressLine2, new { @class = "text" }) %></div></div>
        <div class="row"><label for="AddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("AddressCity",Model.AddressCity, new { @class = "text required" }) %></div></div>
        <div class="row"><label for="AddressStateCode" class="float-left">State, Zip Code:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @class = "input_wrapper AddressStateCode" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @class = "required digits isValidUSZip zip", @maxlength = "5" }) %></div></div>
    </div>
    <div class="column">
        <%= Html.Hidden("AssessmentType", Model.AssessmentType) %>
        <div class="row"><label for="Type" class="float-left">Bill Type:</label><div class="float-right"><%= Html.DropDownList("Type", billType )%></div></div>
        <div class="row"><label for="HippsCode" class="float-left">HIPPS Code:</label><div class="float-right"><%= Html.TextBox("HippsCode", Model.HippsCode, new { @class = "text required", @maxlength = "5" }) %></div></div>
        <div class="row"><label for="ClaimKey" class="float-left">OASIS Matching Key:</label><div class="float-right"><%= Html.TextBox("ClaimKey", Model.ClaimKey, new { @class = "text required", @maxlength = "18" }) %></div></div>
        <div class="row"><label for="PhysicianLastName" class="float-left">Physician Last Name:</label><div class="float-right"><%= Html.TextBox("PhysicianLastName", Model.PhysicianLastName, new { @class = "text required", @maxlength = "20" }) %></div></div>
        <div class="row"><label for="PhysicianFirstName" class="float-left">Physician First Name:</label><div class="float-right"><%= Html.TextBox("PhysicianFirstName", Model.PhysicianFirstName, new { @class = "text required", @maxlength = "20" })%></div></div>
        <div class="row"><label for="PhysicianNPI" class="float-left">Physician NPI #:</label><div class="float-right"><%= Html.TextBox("PhysicianNPI", Model.PhysicianNPI, new { @class = "text required", @maxlength = "10" }) %></div></div>
        <div class="row"><label for="ProspectivePay" class="float-left">HIPPS Code Payment:</label><div class="float-right"><%= Html.TextBox("ProspectivePay", Model.ProspectivePay, new { @class = "text ", @maxlength = "20" })%></div></div>
        <div class="row">
            <div><strong>Diagnosis Codes:</strong></div>
            <div class="margin">
                <label for="Primary" class="float-left">Primary</label><div class="float-right"><%= Html.TextBox("Primary", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : "")%></div>
                <div class="clear"></div>
                <label for="Second" class="float-left">Second</label><div class="float-right"><%= Html.TextBox("Second", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : "")%></div>
                <div class="clear"></div>
                <label for="Third" class="float-left">Third</label><div class="float-right"><%= Html.TextBox("Third", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : "")%></div>
                <div class="clear"></div>
                <label for="Fourth" class="float-left">Fourth</label><div class="float-right"><%= Html.TextBox("Fourth", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : "")%></div>
                <div class="clear"></div>
                <label for="Fifth" class="float-left">Fifth</label><div class="float-right"><%= Html.TextBox("Fifth", diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value : "")%></div>
                <div class="clear"></div>
                <label for="Sixth" class="float-left">Sixth</label><div class="float-right"><%= Html.TextBox("Sixth", diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value : "")%></div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="row"> <%= Html.CheckBox("AreOrdersComplete", Model.AreOrdersComplete, new { @id = "Final_AreOrdersComplete", @class = "radio float-left" })%><label for="Final_AreOrdersComplete" class="radio more">Check this box if you want to by pass the Orders. </label></div>
    </div>
    <div class="clear"></div> 
    <div class="wide-column">
        <label class="float-left">Condition Codes:</label>
        <div class="clear"></div>
        <table>
             <tbody>
                  <tr>
                        <td><label class="float-left">18.</label><div class="float-left"><%= Html.TextBox("ConditionCode18", conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                        <td><label class="float-left">19.</label><div class="float-left"><%= Html.TextBox("ConditionCode19", conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                        <td><label class="float-left">20.</label><div class="float-left"><%= Html.TextBox("ConditionCode20", conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                        <td><label class="float-left">21.</label><div class="float-left"><%= Html.TextBox("ConditionCode21", conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                        <td><label class="float-left">22.</label><div class="float-left"><%= Html.TextBox("ConditionCode22", conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                        <td><label class="float-left">23.</label><div class="float-left"><%= Html.TextBox("ConditionCode23", conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                        <td><label class="float-left">24.</label><div class="float-left"><%= Html.TextBox("ConditionCode24", conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                        <td><label class="float-left">25.</label><div class="float-left"><%= Html.TextBox("ConditionCode25", conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                        <td><label class="float-left">26.</label><div class="float-left"><%= Html.TextBox("ConditionCode26", conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                        <td><label class="float-left">27.</label><div class="float-left"><%= Html.TextBox("ConditionCode27", conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                        <td><label class="float-left">28.</label><div class="float-left"><%= Html.TextBox("ConditionCode28", conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                  </tr>
             </tbody>
        </table>
        <div class="row"><label for="RapRemark">Remark:</label><%= Html.TextArea("Remark", Model.Remark, new { }) %></div>
    </div>     
</fieldset>
<div class="buttons"><ul><li><a class="save">Verify and Next</a></li></ul></div>
<% } %>
<script type="text/javascript">
    Billing.Navigate(1, '#billingInfo');
    if ($("#Final_PatientStatus").val() != '30' && $("#Final_PatientStatus").val() != '0') { $("#Final_DischargeDate").addClass("required"); }
    $("#Final_PatientStatus").change(function() { if ($(this).val() != '30' && $(this).val() != '0') { $("#FinalPatientStatusRow").show(); $("#Final_DischargeDate").addClass("required"); } else { $("#FinalPatientStatusRow").hide(); $("#Final_DischargeDate").removeClass("required"); } });
    if ($("#Final_PrimaryInsuranceId").val() > 1000) { $("#Final_MedicareHMOInformations").show(); $("#Final_MedicareHMOInformations input.hmoflag").addClass("required"); } else { $("#Final_MedicareHMOInformations").hide(); $("#Final_MedicareHMOInformations input.hmoflag").removeClass("required"); }
    $("#Final_PrimaryInsuranceId").change(function() { if ($(this).val() > 1000) { $("#Final_MedicareHMOInformations").show(); $("#Final_MedicareHMOInformations input.hmoflag").addClass("required"); } else { $("#Final_MedicareHMOInformations").hide(); $("#Final_MedicareHMOInformations input.hmoflag").removeClass("required"); } });
</script>