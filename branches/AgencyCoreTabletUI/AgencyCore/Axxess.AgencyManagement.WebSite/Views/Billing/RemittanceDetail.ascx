﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Remittance>" %>
<span class="wintitle">Remittance Detail | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main">
    <div class="buttons">
        <ul>
            <li>
                <a onclick="U.GetAttachment('Billing/RemittancePdf', { 'Id': '<%= Model.Id %>' });return false">Print</a>
            </li>
        </ul>
    </div>
<%  if (Model != null && Model.Data.IsNotNullOrEmpty()) { %>
    <%  var data = Model.Data.ToObject<RemittanceData>(); %>
    <%  if (data == null) data = new RemittanceData(); %>
    <table class="remittance">
        <tr class="reminfo">
            <td>
                <label class="float-left">Check Number:</label>
                <label class="float-right"><%= data.CheckNo %></label>
            </td><td>
                <label class="float-left">Payment Total:</label>
                <label class="float-right"><%= String.Format("${0:#,0.00}", Model.PaymentAmount) %></label>
            </td><td>
                <label class="float-left">Total Claims:</label>
                <label class="float-right"><%= Model.TotalClaims %></label>
            </td><td>
                <label class="float-left">Remittance Date:</label>
                <label class="float-right"><%= Model.RemittanceDate > DateTime.MinValue ? Model.RemittanceDate.ToShortDateString() : string.Empty %></label>
            </td>
        </tr><tr class="payinfo">
            <th colspan="2">Payer</th>
            <th colspan="2">Payee</th>
        </tr><tr class="payinfo">
            <td colspan="2">
                <table>
                    <tr>
                        <td><label class="float-right strong">Name:</label></td>
                        <td><%= data.Payer != null ? data.Payer.Name : string.Empty %></td>
                    </tr>
    <%  if (data.Payer != null && data.Payer.RefType.IsNotNullOrEmpty()) { %>
                    <tr>
                        <td><label class="float-right strong"><%= data.Payer.RefType %>:</label></td>
                        <td><%= data.Payer.RefNum %></td>
                    </tr>
    <%  } %>
                    <tr>
                        <td><label class="float-right strong">Address 1:</label></td>
                        <td><%= data.Payer != null ? data.Payer.Add1 : string.Empty %></td>
                    </tr><tr>
                        <td><label class="float-right strong">Address 2:</label></td>
                        <td><%= data.Payer != null ? data.Payer.Add2 : string.Empty %></td>
                    </tr><tr>
                        <td><label class="float-right strong">City:</label></td>
                        <td><%= data.Payer != null ? data.Payer.City : string.Empty %></td>
                    </tr><tr>
                        <td><label class="float-right strong">State:</label></td>
                        <td><%= data.Payer != null ? data.Payer.State : string.Empty %></td>
                    </tr><tr>
                        <td><label class="float-right strong">Zip:</label></td>
                        <td><%= data.Payer != null ? data.Payer.Zip : string.Empty %></td>
                    </tr>
                </table>
            </td><td colspan="2">
                <table>
                    <tr>
                        <td><label class="float-right strong">Name:</label></td>
                        <td><%= data.Payee != null ? data.Payee.Name : string.Empty %></td>
                    </tr>
    <%  if (data.Payee != null && data.Payee.RefType.IsNotNullOrEmpty()) { %>
                    <tr>
                        <td><label class="float-right strong"><%= data.Payee.RefType %>:</label></td>
                        <td><%= data.Payee.RefNum %></td>
                    </tr>
    <%  } %>
    <%  if (data.Payee != null && data.Payee.IdType.IsNotNullOrEmpty()) { %>
                    <tr>
                        <td><label class="float-right strong"><%= data.Payee.IdType %>:</label></td>
                        <td><%= data.Payee.Id %></td>
                    </tr>
    <%  } %>
                    <tr>
                        <td><label class="float-right strong">Address 1:</label></td>
                        <td><%= data.Payee != null ? data.Payee.Add1 : string.Empty %></td>
                    </tr><tr>
                        <td><label class="float-right strong">Address 2:</label></td>
                        <td><%= data.Payee != null ? data.Payee.Add2 : string.Empty %></td>
                    </tr><tr>
                        <td><label class="float-right strong">City:</label></td>
                        <td><%= data.Payee != null ? data.Payee.City : string.Empty %></td>
                    </tr><tr>
                        <td><label class="float-right strong">State:</label></td>
                        <td><%= data.Payee != null ? data.Payee.State : string.Empty %></td>
                    </tr><tr>
                        <td><label class="float-right strong">Zip:</label></td>
                        <td><%= data.Payee != null ? data.Payee.Zip : string.Empty %></td>
                    </tr>
                </table>
            </td>
        </tr>
    <%  if (data.Claim != null && data.Claim.Count > 0) { %>
        <%  int zebra = 0, count = 0; %>
        <tr class="claiminfo">
            <th colspan="4">Claim(s)</th>
        </tr>
        <%  foreach (var claim in data.Claim) { %>
            <%  if (claim.ProviderLevelAdjustment != null) { %>
        <tr class="<%= zebra % 2 > 0 ? "even" : "odd" %>">
            <td colspan="4">
                <table class="claim">
                    <tbody>
                        <tr class="top">
                            <th colspan="4">Provider Level Adjustment</th>
                        </tr><tr class="bottom">
                            <td>
                                <label class="float-left">Provider Identifier:</label>
                                <label class="float-right"><%=claim.ProviderLevelAdjustment.ProviderIdentifier%></label>
                            </td><td>
                                <label class="float-left">Fiscal Period Date:</label>
                                <label class="float-right"><%=claim.ProviderLevelAdjustment.FiscalPeriodDate.IsValidPHPDate() ? claim.ProviderLevelAdjustment.FiscalPeriodDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></label>
                            </td><td>
                                <label class="float-left">Adjustment Reason:</label>
                                <label class="float-right"><%= claim.ProviderLevelAdjustment.AdjustmentReasonDesc%></label>
                            </td><td>
                                <label class="float-left">Adjustment Amount:</label>
                                <label class="float-right"><%=string.Format("${0:#,0.00}", claim.ProviderLevelAdjustment.ProviderAdjustmentAmount)%></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
                <%  zebra++; %>
            <%  } %>
            <%  if (claim.ClaimPaymentInformation != null && claim.ClaimPaymentInformation.Count > 0) { %>
                <%  foreach (var claimPaymentInfo in claim.ClaimPaymentInformation) { %>
        <tr class="<%= zebra % 2 > 0 ? "even" : "odd" %>">
            <td colspan="4">
                <table class="claim">
                    <tbody>
                        <tr class="top">
                            <td>
                                <label class="float-left">Patient Name:</label>
                                <label class="float-right"><%= claimPaymentInfo.Patient != null ? string.Format("{0} {1}", claimPaymentInfo.Patient.FirstName, claimPaymentInfo.Patient.LastName) : string.Empty%></label>
                            </td><td>
                    <%  if (claimPaymentInfo.Patient != null && claimPaymentInfo.Patient.IdQualifierName.IsNotNullOrEmpty()) { %>
                                <label class="float-left"><%= claimPaymentInfo.Patient.IdQualifierName%>:</label>
                                <label class="float-right"><%= claimPaymentInfo.Patient.Id%></label>
                    <%  } %>
                            </td><td>
                                <label class="float-left">Patient Control Number:</label>
                                <label class="float-right"><%= claimPaymentInfo.PatientControlNumber%></label>
                            </td><td>
                                <label class="float-left">ICN Number:</label>
                                <label class="float-right"><%= claimPaymentInfo.PayerClaimControlNumber%></label>
                            </td>
                        </tr><tr>
                            <td>
                                <label class="float-left">Start Date:</label>
                                <label class="float-right"><%= claimPaymentInfo.ClaimStatementPeriodStartDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodStartDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></label>
                            </td><td>
                                <label class="float-left">End Date:</label>
                                <label class="float-right"><%= claimPaymentInfo.ClaimStatementPeriodEndDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodEndDate.ToDateTimePHP().ToString("MM/dd/yyyy") : string.Empty%></label>
                            </td><td>
                                <label class="float-left">Type Of Bill:</label>
                                <label class="float-right"><%= claimPaymentInfo.TypeOfBill%></label>
                            </td><td>
                                <label class="float-left">Claim Status:</label>
                                <label class="float-right"><%= string.Format("{0} ({1})", claimPaymentInfo.ClaimStatusDescription, claimPaymentInfo.ClaimStatusCode)%></label>
                            </td>
                        </tr><tr>
                            <td>
                                <label class="float-left">Claim Number:</label>
                                <label class="float-right"><%= count + 1 %></label>
                            </td><td>
                                <label class="float-left">Reported Charge:</label>
                                <label class="float-right"><%=string.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount)%></label>
                            </td><td>
                                <label class="float-left">Remittance:</label>
                                <label class="float-right"><%=string.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount)%></label>
                            </td><td>
                                <label class="float-left">Line Adjustment Amount:</label>
                                <label class="float-right"><%= string.Format("${0:#,0.00}", claimPaymentInfo.ServiceAdjustmentTotal)%></label>
                            </td>
                        </tr><tr class="bottom">
                            <td>
                            </td><td>
                            </td><td>
                            </td><td>
                                <label class="float-left">Paid Amount:</label>
                                <label class="float-right"><%= string.Format("${0:#,0.00}", claimPaymentInfo.ClaimPaymentAmount) %></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
                    <%  count++; %>
                    <%  zebra++; %>
                <%  } %>
            <%  } %>
        <%  } %>
    <%  } %>
    </table>
<%  } %>
</div>