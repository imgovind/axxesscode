﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<BillingHistoryViewData>" %>
<% if (Model.ClaimInfo != null) { %>
<div id="Billing_CenterContentClaimInfo" class="top">
    <div class="winmenu">
        <ul>
            <li><a onclick="UserInterface.ShowNewClaimModal('RAP');return false">New RAP</a></li>
            <li><a onclick="UserInterface.ShowNewClaimModal('Final');return false">New Final</a></li>
        </ul>
    </div>
    <div id="billingHistoryClaimData"><% Html.RenderPartial("ClaimInfo", Model.ClaimInfo); %></div>
</div>
<div class="bottom" style="top: 220px;"><% Html.RenderPartial("HistoryActivity", Model.ClaimInfo.PatientId); %></div>   
<% } else { %> <div class="abs center">No Patient Data Found</div><% } %>