﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<% using (Html.BeginForm("VisitVerify", "Billing", FormMethod.Post, new { @id = "billingVisitForm" })) { %>
<%= Html.Hidden("Id",Model.Id) %>
<%= Html.Hidden("episodeId",Model.EpisodeId) %>
<%= Html.Hidden("patientId",Model.PatientId) %><%
                                                   var notVerifiedVisits = Model.Visits.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.VisitDate.IsValidDate() && s.VisitDate.ToDateTime().Date >= Model.EpisodeStartDate.Date && s.VisitDate.ToDateTime().Date <= Model.EpisodeEndDate.Date && s.DisciplineTask > 0).OrderBy(s => s.VisitDate.ToDateTime().Date).ToList();
    List<ScheduleEvent>[] a = new List<ScheduleEvent>[] { notVerifiedVisits.Where(v => v.IsBillable && (v.Status == "420" || v.Status == "425" || v.Status == "430" || v.Status == "435" || v.Status == "215" || v.Status == "220" || v.Status == "225" || v.Status == "230" || v.Status == "235") && !v.IsMissedVisit).ToList(), notVerifiedVisits.Where(v => (!v.IsBillable && (v.Status == "420" || v.Status == "425" || v.Status == "435" || v.Status == "215" || v.Status == "220" || v.Status == "225" || v.Status == "235") && !v.IsMissedVisit) || (!v.IsBillable && (v.Status == "420" || v.Status == "425" || v.Status == "435" || v.Status == "215" || v.Status == "220" || v.Status == "225" || v.Status == "235") && !v.IsMissedVisit)).ToList(), notVerifiedVisits.Where(v => v.IsMissedVisit).ToList() };
    String[] b = new String[] {"Billable Visits","Non-Billable Visits","Missed Visit"};
    String[] c = new String[] {"PT", "Nursing", "ST", "OT", "MSW","HHA"};
    String[] d = new String[] {"Physical Therapy (0421)", "Skilled Nursing (0551)", "Speech Therapy (0440)", "Occupational Therapy (0431)", "Social Worker (0561)","HHA (0571)"};
    var billInfo = Model.BillInformations != null ? Model.BillInformations : new Dictionary<string, BillInfo>();
       %>
<div class="wrapper main">
    <div class="billing">
        <h3 class="align-center">Episode: <%= Model.EpisodeStartDate.ToString("MM/dd/yyyy")%> &#8211; <%= Model.EpisodeEndDate.ToString("MM/dd/yyyy")%></h3><%
    for (int g = 0; g < 3; g++) {
        if (a[g] != null && a[g].ToList().Count > 0) { %>
        <ul>
            <li class="align-center"><h3><%= b[g] %></h3></li>
            <li>
                <span class="rapcheck"></span>
                <span class="visittype">Visit Type</span>
                <span class="visitdate">Scheduled Date</span>
                <span class="visitdate">Visit Date</span>
                <span class="visithcpcs">HCPCS</span>
                <span class="visitstatus">Status</span>
                <span class="visitunits">Units</span>
                <span class="visitcharge">Charge</span>
            </li>
        </ul><%
            for (int h = 0; h < 6; h++) {
                if (a[g] != null) {
                    var visits = a[g].Where(f => f.Discipline == c[h]).ToList();
                    if (visits != null && visits.Count > 0) { %>
        <ol>
            <li class="discpiline-title"><span><%=  d[h] %></span></li><% 
                        int i = 1;
                        foreach (var visit in visits) { %> <% var discipline = visit.Discipline == "Nursing" ? "SN" : visit.Discipline;  %>
            <li class="<%= i % 2 != 0 ? "odd not-ready" : "even not-ready" %>">
                <label for="<%= "Visit" + visit.EventId.ToString() %>">
                    <% var code = visit.GIdentify(); %>
                    <% var unit=Model.IsMedicareHMO? ( Model.UnitType == 1 ? 1 : (Model.UnitType == 2 ? (int)Math.Ceiling((double)visit.MinSpent / 60) : (Model.UnitType == 3 ?(int)Math.Ceiling((double)visit.MinSpent / 15) :0 ))): visit.Unit; %>
                    <% var amount = code.IsNotNullOrEmpty() && billInfo.ContainsKey(code) && billInfo[code] != null && billInfo[code].Amount.IsNotNullOrEmpty() && billInfo[code].Amount.IsDouble() ? billInfo[code].Amount.ToDouble() : 0;%>
                    <span class="rapcheck"><%= i%>. <input class="radio" name="Visit" type="checkbox" id="<%= "Visit" + visit.EventId.ToString() %>" value="<%= g == 0 ? visit.EventId.ToString() + "\" checked=\"checked" : visit.EventId.ToString() %>" /></span>
                    <span class="visittype"><%= visit.DisciplineTaskName%></span>
                    <span class="visitdate"><%= visit.EventDate.IsNotNullOrEmpty() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyy") : string.Empty %></span>
                    <span class="visitdate"><%= visit.VisitDate.IsNotNullOrEmpty() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyy") : string.Empty %></span>
                    <span class="visithcpcs"><%= code.IsNotNullOrEmpty() && billInfo.ContainsKey(code) && billInfo[code]!=null? billInfo[code].CodeTwo :"" %></span>
                    <span class="visitstatus"><%= visit.StatusName %></span>
                    <span class="visitunits"><% = unit.ToString() %></span>
                    <span class="visitcharge"><% =string.Format("${0:#0.00}",Model.IsMedicareHMO?amount*unit:amount ) %></span>
                </label>
            </li><% i++;} %>
        </ol><%
                    }
                }
            }
        }
    } %>
    </div>
    <div class="buttons">
        <ul>
            <li><a onclick="Billing.NavigateBack(0);return false">Back</a></li>
            <li><a class="save">Verify and Next</a></li>
        </ul>
    </div>
</div>
<% } %>