﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<span class="wintitle">Final | <%= Model != null ? (Model.LastName + ", " + Model.FirstName).ToTitleCase().Clean() : "" %></span>
<% Html.Telerik().TabStrip().HtmlAttributes(new { @style = "height:auto" }).Name("FinalTabStrip")
        .Items(parent => {
            parent.Add().Text("Step 1 of 4:^Demographics").LoadContentFrom("Info", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId });
            parent.Add().Text("Step 2 of 4:^Verify Visit").LoadContentFrom("Visit", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId });
            parent.Add().Text("Step 3 of 4:^Verify Supply").LoadContentFrom("Supply", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId });
            parent.Add().Text("Step 4 of 4:^Summary").LoadContentFrom("Summary", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId });
        }).ClientEvents(events => events.OnLoad("Billing.finalTabStripOnLoad").OnSelect("Billing.finalTabStripOnSelect")).Render(); %>
<script type="text/javascript">
    $("#FinalTabStrip li.t-item a").each(function() { $(this).html("<span><strong>" + ($(this).html().substring(0, $(this).html().indexOf(":^") + 1) + "</strong><br/>" + $(this).html().substring($(this).html().indexOf(":^") + 2)) + "</span>") });
    $("#FinalTabStrip li.t-item a:first").click();
</script>