﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<span class="wintitle">Managed Claim | <%= Model != null ? (Model.LastName + ", " + Model.FirstName).ToTitleCase().Clean() : "" %></span>
<%  Html.Telerik().TabStrip().HtmlAttributes(new { @style = "height:auto" }).Name("ManagedClaimTabStrip")
        .Items(parent => {
            parent.Add().Text("Step 1 of 4:^Demographics").LoadContentFrom("ManagedClaimInfo", "Billing", new { Id = Model.Id, patientId = Model.PatientId }).Selected(true);
            parent.Add().Text("Step 2 of 4:^Verify Visit").LoadContentFrom("ManagedClaimVisit", "Billing", new { Id = Model.Id, patientId = Model.PatientId });
            parent.Add().Text("Step 3 of 4:^Verify Supply").LoadContentFrom("ManagedClaimSupply", "Billing", new { Id = Model.Id, patientId = Model.PatientId });
            parent.Add().Text("Step 4 of 4:^Summary").LoadContentFrom("ManagedSummary", "Billing", new { Id = Model.Id, patientId = Model.PatientId });
        }).ClientEvents(events => events.OnSelect("ManagedBilling.managedClaimTabStripOnSelect")).Render(); %>
<script type="text/javascript">$("#ManagedClaimTabStrip li.t-item a").each(function() { $(this).html("<span><strong>" + ($(this).html().substring(0, $(this).html().indexOf(":^") + 1) + "</strong><br/>" + $(this).html().substring($(this).html().indexOf(":^") + 2)) + "</span>") });</script>
