﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl <NewManagedClaimViewData>" %>
<% using (Html.BeginForm("CreateManagedClaim", "Billing", FormMethod.Post, new { @id = "createManagedClaimForm" })) { %>
 <%= Html.Hidden("PatientId",Model.PatientId) %>
 <div class="wrapper main">
    <fieldset>
        <legend>Claim Information</legend>
         <div class="wide-column">
            <div class="row">
                <label class="float-left">Insurances:</label>
                <div class="float-right"><%= Html.DropDownList("InsuranceId", Model != null && Model.Insurances != null ? Model.Insurances : new List<SelectListItem>()) %></div>
            </div><div ="row">
            </div><div class="row">
                <label class="float-left">Date Range:</label>
                <div class="float-right">
                    <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="StartDate" class="short" />
                    To
                    <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="EndDate" class="short" />
                </div>
            </div>
         </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Add Claim</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<% } %>