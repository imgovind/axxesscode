﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaimSnapShotViewData>" %>
<div class="twothirds">
    <table width="100%">
        <tbody>
            <tr>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td class="align-right">Patient Name :</td>
                                <td ><label class="strong"><%=Model.PatientName %></label></td>
                            </tr><tr>
                                <td class="align-right">Patient ID # :</td>
                                <td><label class="strong"><%=Model.PatientIdNumber %></label></td>
                            </tr><tr>
                                <td class="align-right">Patient Insurance Id # :</td>
                                <td><label class="strong"><%=Model.IsuranceIdNumber%></label></td>
                            </tr>
                        </tbody>
                    </table>
                </td><td>
                    <% if (Model.Visible){ %>
                    <table>
                        <tbody>
                            <tr>
                                <td class="align-right">Insurance/Payer :</td>
                                <td><label class="strong"><%=Model.PayorName %></label></td>
                            </tr><tr>
                                <td class="align-right">Health Plan Id :&#160; </td>
                                <td class="align-left"><label class="strong"><%=Model.HealthPlainId%></label></td>
                            </tr><tr>
                                <td class="align-right">Authorization Number :&#160;</td>
                                <td class="align-left"><label class="strong"><%=Model.AuthorizationNumber%></label></td>
                            </tr>
                        </tbody>
                    </table>
                    <%} %>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="activity-log"><% = string.Format("<span class=\"img icon activity\" onclick=\"Log.LoadClaimLog('ManagedClaim','{0}','{1}')\" title=\"Activity Logs\"></span>", Model.Id, Model.PatientId)%></div>
</div>
        
  
