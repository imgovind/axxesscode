﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<%  var val = Model != null && !Model.IsEmpty() ? Model : Guid.Empty;
    Html.Telerik().Grid<ManagedClaimLean>().Name("ManagedBillingActivityGrid").Columns(columns =>
    {
        columns.Bound(p => p.Id).ClientTemplate("<a onclick=\"UserInterface.ShowManagedClaim('<#=Id#>','<#=PatientId#>');return false\">Open Claim</a>&nbsp;").Title("").Width(90);
        columns.Bound(p => p.ClaimRange).Title("Claim Date Range").Width(140);
        columns.Bound(p => p.StatusName).Title("Status").Width(70);
        columns.Bound(p => p.ClaimAmount).Title("Claim").Format("${0:#0.00}").Width(50);
        columns.Bound(p => p.PaymentAmount).Title("Payment").Format("${0:#0.00}").Width(60);
        columns.Bound(p => p.PaymentDateFormatted).Title("Payment Date").Width(90);
        columns.Bound(p => p.IsInfoVerified).Title("Details").ClientTemplate("<span class=\"img icon <#= IsInfoVerified ? 'success-small' : 'error-small' #>\">").Width(50);
        columns.Bound(p => p.IsVisitVerified).Title("Visits").ClientTemplate("<span class=\"img icon <#= IsVisitVerified ? 'success-small' : 'error-small' #>\">").Width(50);
        columns.Bound(p => p.IsSupplyVerified).Title("Supply").ClientTemplate("<span class=\"img icon <#= IsSupplyVerified ? 'success-small' : 'error-small' #>\">").Width(50);
        columns.Bound(p => p.PrintUrl).Title("").Width(100);
        columns.Bound(p => p.Id).ClientTemplate("<a onclick=\"UserInterface.ShowModalUpdateStatusManagedClaim('<#=PatientId#>','<#=Id#>');return false\">Update</a>").Title("Status").Width(55);
        columns.Bound(p => p.Id).ClientTemplate("&nbsp;<a onclick=\"ManagedBilling.DeleteClaim('<#=PatientId#>','<#=Id#>');return false\">Delete</a>").Title("Action").Width(55);
        columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "font-size:0;" }).HtmlAttributes(new { style = "font-size:0;" }).Width(0);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("ManagedClaimsActivity", "Billing", new { patientId = val, insuranceId = 0 })).ClientEvents(events => events.OnDataBound("ManagedBilling.OnCliamDataBound").OnRowSelected("ManagedBilling.OnClaimRowSelected")).Sortable().Selectable().Scrollable().Footer(false).Render(); %>
<script type="text/javascript">$("#ManagedBillingActivityGrid .t-grid-content").css("overflow-x", "auto");</script>