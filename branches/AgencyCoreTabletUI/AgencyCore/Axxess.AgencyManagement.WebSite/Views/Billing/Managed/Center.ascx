﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedBillViewData>" %>
<span class="wintitle">Create Managed Care Claims | <%= Current.AgencyName.Clean() %></span>
<div id="Billing_ManagedClaimCenterContentMain" class="main wrapper">
    <div class="buttons float-right">
        <ul>
            <li><a href="Billing/ClaimsPdf" target="_blank">Print</a></li>
        </ul>
    </div>
    <fieldset class="grid-controls">
        <label class="float-left" for="Billing_ManagedClaimCenterBranchCode">Branch:</label>
        <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.Branches, "BranchId", Model.BranchId.ToString(), new { @id = "Billing_ManagedClaimCenterBranchCode", @class = "report_input valid" })%></div>
        <label class="float-left" for="Billing_ManagedClaimCenterPrimaryInsurance">Insurance:</label>
        <div class="float-left"><%= Html.InsurancesNoneMedicare("PrimaryInsurance", Model.Insurance.ToString(), false,"", new { @id = "Billing_ManagedClaimCenterPrimaryInsurance", @class = "Insurances required notzero" })%></div>
        <div class="buttons float-right">
            <ul>
                <li><a onclick="ManagedBilling.ReLoadUnProcessedManagedClaim($('#Billing_ManagedClaimCenterBranchCode').val(), $('#Billing_ManagedClaimCenterPrimaryInsurance').val());return false">Generate</a></li>
            </ul>
        </div>
    </fieldset>
    <div id="Billing_ManagedClaimCenterContent" class="acore-grid">
        <%  Html.RenderPartial("Managed/ManagedGrid", Model.Bills); %>
    </div>
    <%  if (Current.HasRight(Permissions.GenerateClaimFiles)) { %>
    <div class="buttons" id="Billing_ManagedClaimCenterButtons">
        <ul>
            <li><a onclick="ManagedBilling.loadGenerate('#Billing_ManagedClaimCenterContentMain');return false">Generate Selected</a></li>
            <li><a onclick="ManagedBilling.GenerateAllCompleted('#Billing_ManagedClaimCenterContentMain');return false">Generate All Completed</a></li>
        </ul>
    </div>
    <%  } %>
</div>
<script type="text/javascript">
    if ($("#Billing_ManagedClaimCenterPrimaryInsurance").val() == "0") $("#Billing_ManagedClaimCenterButtons").hide();
    else $("#Billing_ManagedClaimCenterButtons").show();
    $("#Billing_ManagedClaimCenterPrimaryInsurance").change(function() {
        if ($(this).val() == "0") $("#Billing_ManagedClaimCenterButtons").hide();
        else $("#Billing_ManagedClaimCenterButtons").show()
    }); 
</script>
