﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
<span class="wintitle">Managed Claim History | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top">
            <div class="row">
                <label>Branch:</label>
                <div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new { @class = "managedBillingBranchCode" })%></div>
            </div>
            <div class="row">
                <label>View:</label>
                <div>
                    <select name="list" class="managedBillingStatusDropDown">
                        <option value="1">Active Patients</option>
                        <option value="2">Discharged Patients</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label>Filter:</label>
                <div><%= Html.DropDownList("list", Model.Insurances, new { @class = "managedBillingInsuranceDropDown" })%></div>
            </div>
            <div class="row">
                <label>Find:</label>
                <div><input id="txtSearch_managedBilling_Selection" class="text" name="" value="" type="text" /></div>
            </div>
        </div>
        <div class="bottom">
            <%  Html.RenderPartial("~/Views/Billing/Managed/PatientList.ascx", Model.SelecetdInsurance.IsNotNullOrEmpty() ? Model.SelecetdInsurance : string.Empty); %>
        </div>
    </div>
    <div id="managedBillingMainResult" class="ui-layout-center">
        <div class="top">
            <div class="winmenu"><ul id="managedBillingTopMenu"><li><a onclick="UserInterface.ShowNewManagedClaimModal();return false">New Claim</a></li></ul></div>
            <div id="managedBillingClaimData"></div>
        </div>
        <div class="bottom">
            <div class="above">
                <label class="float-left">Insurance:</label>
                <div class="float-left"><%= Html.InsurancesFilter("PrimaryInsurance", "0", new { @id = "ManagedBillingHistory_InsuranceId", @class = "Insurances" })%></div>
            </div>
            <% Html.RenderPartial("~/Views/Billing/Managed/Claims.ascx", Guid.Empty); %>
        </div>   
    </div>
</div>
<script type="text/javascript">
    $('#window_managedclaims .layout').layout({ west__paneSelector: '.ui-layout-west' });
    $("#ManagedBillingHistory_InsuranceId").change(function() { ManagedBilling.RebindActivity(ManagedBilling._patientId); });
</script>
<div id="ManagedClaim_New_Container" class="claimupdatemodal hidden"></div>
<div id="ManagedClaim_UpdateStatus_Container" class="claimupdatemodal hidden"></div>

