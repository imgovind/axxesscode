﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ManagedBill>>" %>
<%  if (Model != null && Model.Count > 0) { %>
<ul>
    <li class="align-center"><h3>Managed Claim(s)</h3></li>
    <li>
        <span class="final-check"></span>
        <span class="final-patient pointer" onclick="Billing.Sort('Final', 'rapname');">Patient Name</span>
        <span class="final-mr pointer" onclick="Billing.Sort('Final', 'rapid');">Patient ID</span>
        <span class="final-episode pointer" onclick="Billing.Sort('Final', 'rapeps');">Episode Date</span>
        <span class="final-icon">Detail</span>
        <span class="final-icon">Visit</span>
        <span class="final-icon">Supply</span>
        <span class="final-icon">Verified</span>
    </li>
</ul>
<ol>
    <%  var claims = Model.Where(c => c.EpisodeEndDate < DateTime.Now); %>
    <%  int i = 1; %>
    <%  foreach (var claim in claims) { %>
    <li class="<%= (i % 2 != 0 ? "odd" : "even") + " ready" %>">
        <span class="final-check"><%= i %>. <%= (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) ? "<input name='ManagedClaimSelected' class='radio' type='checkbox' value='" + claim.Id + "' id='ManagedClaimSelected" + claim.Id + "' />" : string.Empty %></span>
        <%= (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) ? string.Format("<a class='float-right' onclick=\"U.GetAttachment('Billing/ManagedUB04Pdf', {{ 'Id': '{0}' ,'patientId': '{1}' }});return false\"><span class='img icon print'></span></a>", claim.Id, claim.PatientId) : string.Empty%>
        <a onclick="UserInterface.ShowManagedClaim('<%= claim.Id  %>','<%= claim.PatientId %>');return false" status="Ready" >
            <span class="final-patient"><%= claim.LastName%>, <%= claim.FirstName%></span>
            <span class="final-mr"><%= claim.PatientIdNumber%></span>
            <span class="final-episode"><span class="very-hidden"><%= claim.EpisodeStartDate != null ? claim.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span><%= claim.EpisodeStartDate != null ? claim.EpisodeStartDate.ToString("MM/dd/yyy") : string.Empty %><%= claim.EpisodeEndDate != null ? "&#8211;" + claim.EpisodeEndDate.ToString("MM/dd/yyy") : string.Empty %></span>
            <span class="final-icon"><span class="img icon <%= claim.IsInfoVerified? "success-small" : "error-small" %>"></span></span>
            <span class="final-icon"><span class="img icon <%= claim.IsVisitVerified? "success-small" : "error-small" %>"></span></span>
            <span class="final-icon"><span class="img icon <%= claim.IsSupplyVerified ? "success-small" : "error-small" %>"></span></span>
            <span class="final-icon"><span class="img icon <%= claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified ? "success-small" : "error-small" %>"></span></span>
        </a>
    </li>
        <%  i++; %>
    <%  } %>
</ol>
<%  } %>
