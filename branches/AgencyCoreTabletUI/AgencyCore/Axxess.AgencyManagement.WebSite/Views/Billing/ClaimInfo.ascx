﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimInfoSnapShotViewData>" %>
<fieldset class="patient-summary fl">
    <div class="column">
        <div class="row ac bigtext"><%=Model.Type %></div>
        <div class="row">
            <label class="fl strong">Patient Name</label>
            <div class="fr"><%= Model.PatientName %></div>
        </div>
        <div class="row">
            <label class="fl strong">Patient ID #</label>
            <div class="fr"><%= Model.PatientIdNumber%></div>
        </div>
        <div class="row">
            <label class="fl strong">Medicare #</label>
            <div class="fr"><%= Model.MedicareNumber%></div>
        </div>
        <div class="row">
            <label class="fl strong">Insurance/Payer</label>
            <div class="fr"><%= Model.PayorName%></div>
        </div>
<%  if (Model.Visible) { %>
        <div class="row">
            <label class="fl strong">HHRG (Grouper)</label>
            <div class="fr"><%= Model.HHRG %></div>
        </div>
        <div class="row">
            <label class="fl strong">HIPPS</label>
            <div class="fr"><%= Model.HIPPS %></div>
        </div>
        <div class="row">
            <label class="fl strong">AUTH</label>
            <div class="fr"><%= Model.ClaimKey %></div>
        </div>
        <div class="row">
            <label class="fl strong">Episode Payment Rate</label>
            <div class="fr"><%= Model.StandardEpisodeRate != 0 ? string.Format("{0:c}", Model.StandardEpisodeRate) : string.Empty %></div>
        </div>
        <div class="row">
            <label class="fl strong">Supply Reimbursement</label>
            <div class="fr"><%= Model.SupplyReimbursement != 0 ? string.Format("{0:c}", Model.SupplyReimbursement) : string.Empty %></div>
        </div>
        <div class="row">
            <label class="fl strong">Prospective Pay</label>
            <div class="fr"><%= Model.ProspectivePay != 0 ? string.Format("{0:c}", Model.ProspectivePay) : string.Empty %></div>
        </div>
<%  } %>
    </div>
</fieldset>
<div class="quick reports fr">
    <div class="reports-head">
        <h5>Billing Reports</h5>
    </div>
    <ul>
        <li><a onclick="return false">PPS Analysis</a></li>
        <li><a onclick="return false">Claim to be transmited</a></li>
        <li><a onclick="return false">Claim transmitted and paid</a></li>
        <li><a onclick="return false">Claim transmited only</a></li>
    </ul>
</div>
<div class="cr"></div>
<div class="activity-log"><% = string.Format("<span class=\"img icon activity\" onclick=\"Log.LoadReferralLog('{0}','{1}','{2}')\" title=\"Activity Logs\"></span>", Model.Type, Model.Id, Model.PatientId)%></div>