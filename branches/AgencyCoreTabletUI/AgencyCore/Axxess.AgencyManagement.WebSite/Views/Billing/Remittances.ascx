﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RemittanceLean>>" %>
<span class="wintitle">Remittance | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main">
    <div class="float-right">
        <div class="buttons">
            <ul><li><%= Html.ActionLink("Print", "RemittancesPdf", "Billing", new { StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = "BillingRemittance_PrintLink", @target = "_blank" })%></li></ul>
        </div>
    </div>
    <fieldset class="grid-controls">
        <label class="strong" for="BillingRemittance_StartDate">Date Range:</label>
        <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="BillingRemittance_StartDate" class="short" />
        <label class="strong" for="BillingRemittance_EndDate">To</label>
        <input type="date" name="EndtDate" value="<%= DateTime.Now.ToShortDateString() %>" id="BillingRemittance_EndDate" class="short" />
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Billing.RemittanceContentReload();return false">Generate</a></li>
            </ul>
        </div>
        <div class="clear"></div>
        <%  using (Html.BeginForm("RemittanceUpload", "Billing", FormMethod.Post, new { @id = "externalRemittanceUploadForm" })) { %>
        <label class="strong" for="Billing_ExternalRemittanceUpload">Upload a Remittance File</label>
        <input id="Billing_ExternalRemittanceUpload" type="file" name="Upload" />
        <div class="buttons float-right">
            <ul>
                <li><a class="save">Upload the File</a></li>
            </ul>
        </div>
        <%  } %>
    </fieldset>
    <div id="BillingRemittance_Content" class="acore-grid">
        <%  Html.RenderPartial("RemittanceContent", Model); %>
    </div>
</div>    
