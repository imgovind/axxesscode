﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Claim Submission History | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
    <div class="float-right">
        <div class="buttons">
            <ul><li><%= Html.ActionLink("Excel Export", "SubmittedBatchClaims", "Export", new { ClaimType = "ALL", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "SubmittedClaims_ExportLink", @class = "excel" })%></li></ul>
        </div>
    </div>
    <fieldset class="grid-controls">
        <label class="float-left" for="SubmittedClaims_ClaimType">Type:</label>
        <div class="float-left"><%= Html.ClaimTypes("ClaimType", new { @id = "SubmittedClaims_ClaimType" })%></div>
        <label class="strong" for="SubmittedClaims_StartDate">Date Range:</label>
        <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="SubmittedClaims_StartDate" class="short" />
        <label class="strong" for="SubmittedClaims_EndDate">To</label>
        <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="SubmittedClaims_EndDate" class="short" />
        <div class="buttons float-right">
            <ul><li><a onclick="Billing.RebindSubmittedBatchClaims();return false">Generate</a></li></ul>
        </div>
    </fieldset>
    <%= Html.Telerik().Grid<ClaimDataLean>().Name("List_SubmittedClaims").Columns(columns => {
            columns.Bound(o => o.Id).Title("Batch Id").Width(80).Sortable(true).ReadOnly();
            columns.Bound(o => o.Created).Format("{0:MM/dd/yyyy}").Title("Submission Date").Width(120).Sortable(true).ReadOnly();
            columns.Bound(o => o.Count).Title("# of claims").Sortable(false).ReadOnly();
            columns.Bound(o => o.RAPCount).Title("# of RAPs").Sortable(false).ReadOnly();
            columns.Bound(o => o.FinalCount).Title("# of Finals").Sortable(false).ReadOnly();
            columns.Bound(o => o.Id).ClientTemplate("<a onclick=\"Billing.SubmittedClaimDetail('<#=Id#>');return false\">View Claims</a>").Title("Action").Width(90);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("ClaimSubmittedList", "Billing", new { ClaimType="ALL", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Footer(false) %>
</div>