﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Medicare Eligibility Report | <%= Current.AgencyName.Clean() %></span>
<div id="MedicareEligiblity_Report" class="main wrapper">
    <fieldset class="grid-controls">
        <label class="float-left" for="MedicareEligiblity_Report_Patients">Patient:</label>
        <div class="float-left"><%= Html.Patients("Patients", Guid.Empty.ToString(), (int)PatientStatus.Active, "-- Select Patient --", new { @id = "MedicareEligiblity_Report_Patients", @class = "report_input valid" })%></div>
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Billing.ReloadEligibilityList($('#MedicareEligiblity_Report_Patients').val());return false">Generate</a></li>
            </ul>
        </div>
    </fieldset>
    <div id="MedicareEligiblity_ReportContent" class="acore-grid"></div>
</div>