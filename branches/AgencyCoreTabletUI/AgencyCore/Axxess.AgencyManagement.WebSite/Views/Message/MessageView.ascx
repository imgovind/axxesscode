﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Message>" %>
<%  if (Model != null) { %>
<div class="message-content" id="message-content">
    <div class="winmenu">
        <ul>
            <li><a class="reply">Reply</a></li>
            <li><a class="reply">Reply to All</a></li>
            <li><a class="reply">Forward</a></li>
            <li><a class="reply">Delete</a></li>
        </ul>
    </div>
    <div class="message-content-panel" id="message-content-panel">
        <input type="hidden" id="messageId" value="<%= Model.Id %>" />
        <input type="hidden" id="messageType" value="<%= (int)Model.Type %>" />
        <div class="message-header-container">
            <div class="message-header-row">
                <label for="messageSender" class="strong">From:</label>
                <span id="messageSender"><%= Model.FromName %></span>
            </div>
            <div class="message-header-row">
                <label for="messageRecipients" class="strong">To:</label>
                <span id="messageRecipients"><%= Model.RecipientNames %></span>
            </div>
            <div class="message-header-row">
                <label for="messageDate" class="strong">Sent:</label>
                <span id="messageDate"><%= Model.MessageDate %></span>
            </div>
            <div class="message-header-row">
                <label for="messageSubject" class="strong">Subject:</label>
                <span id="messageSubject"><%= Model.Subject %></span>
            </div>
    <%  if (Model.Type == MessageType.User) { %>
            <div class="message-header-row" class="strong">
                <label for="messageRegarding">Regarding:</label>
                <span id="messageRegarding"><%= Model.PatientName %></span>
            </div>
    <%  } %>
    <%  if (Model.HasAttachment) { %>
            <div class="message-header-row">
                <label for="messageAttachment" class="strong">Attachment:</label>
                <span id="messageAttachment"><%= Html.Asset(Model.AttachmentId) %></span>
            </div>
    <%  } %>
        </div>
        <div id="message-body-container" class="message-body-container">
            <div id="message-body">
                <%= Model.Body %>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#message-content .message-header-row").each(function() {
        if ($(this).find("span").prop("scrollHeight") > $(this).find("span").prop("offsetHeight")) $(this).append(unescape("%3Ca onclick=%22return false%22 class=%22more%22 tooltip=%22" + $(this).find("span").html() + "%22%3EMore&#187;%3C/a%3E"));
        $(this).find("span").css("overflow", "hidden")
    });
    U.ToolTip($("#message-content .message-header-row a.more"), "calday");
</script>
<% } else { %> 
<script type="text/javascript">
    $("#messageInfoResult").html(
        $("<div/>", { "class": "ajaxerror" }).append(
            $("<h1/>", { "text": "No Messages found." })).append(
            $("<div/>", { "class": "heading" }).Buttons([{
                Text: "Add New Message",
                Click: function() {
                    UserInterface.ShowNewMessage()
                }
            }])
        )
    );
</script>
<% } %>