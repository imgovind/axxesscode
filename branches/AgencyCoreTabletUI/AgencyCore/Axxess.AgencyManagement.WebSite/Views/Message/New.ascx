﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Message | <%= Current.AgencyName.Clean() %></span>
<%  using (Html.BeginForm("New", "Message", FormMethod.Post, new { @id = "newMessageForm" })) { %>
<div id="messaging-container" class="wrapper layout">
    <div class="ui-layout-west"><%= Html.Recipients() %></div>
    <div class="new-message-content ui-layout-center" id="new-message-content">
        <div id="message-type-header" class="compose-header">
            <div class="buttons fr">
                <ul>
                    <li><a onclick="Message.Cancel();return false">Cancel</a></li>
                </ul>
            </div>
            New Message
        </div>
        <div class="compose message-header-container">
            <div class="buttons abs">
                <ul>
                    <li class="send">
                        <a class="save">
                            <span class="img icon send"></span>
                            <br />Send
                        </a>
                    </li>
                </ul>
            </div>
            <div class="message-header-row">
                <label for="New_Message_Recipents" class="strong">To</label>
                <div class="compose-input to">
                    <input type="text" id="New_Message_Recipents" name="newMessageRecipents" />
                </div>
            </div>
            <div class="message-header-row">
                <label for="New_Message_Subject" class="strong">Subject</label>
                <div class="compose-input">
                    <input type="text" id="New_Message_Subject" name="Subject" maxlength="75" class="fill" />
                </div>
            </div>
    <%  if (!Current.IfOnlyRole(AgencyRoles.ExternalReferralSource) && !Current.IfOnlyRole(AgencyRoles.CommunityLiasonOfficer)) { %>
            <div class="message-header-row">
                <label for="New_Message_PatientId" class="strong">Regarding</label>
                <div class="compose-input">
                    <%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "New_Message_PatientId", @class = "fill" })%>
                </div>
            </div>
    <%  } %>
            <div class="message-header-row">
                <label for="New_Message_Attachment" class="strong">Attachments</label>
                <div class="compose-input">
                    <input type="file" id="New_Message_Attachment" name="Attachment1" class="fill" />
                </div>
            </div>
        </div>
        <div class="new-message-row" id="new-message-body-wrapper"></div>
    </div>
</div>
<%  } %>