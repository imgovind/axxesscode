﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main blue">
    <div class="buttons">
        <ul>
            <li><a class="new-message">New Message</a></li>
        </ul>
    </div>
    <div class="message-list">
    <% Html.Telerik().Grid<Message>().Name("list-messages").HtmlAttributes(new { @class = "inbox" }).Columns(columns => {
            columns.Bound(m => m.FromName)
                .ClientTemplate("<div id='<#=Id#>' type='<#=Type#>' class='message read-<#=MarkAsRead#>'>"
                    + "<div><span class='float-right'>"
                    + "<#=Date#></span></div>"
                    + "<div><span><#=FromName#></span>"
                    + "<br /><span class='normal'><#=Subject#></span></div></div>").Title("Inbox");
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Message", new { inboxType = "inbox" }))
        .ClientEvents(events => events.OnDataBound("Message.OnMessageListReady").OnRowSelected("Message.LoadMessage")).Selectable().Footer(false)
        .Scrollable(scrolling => scrolling.Enabled(true)).Render(); %>
    </div>
    <ul class="folder-selector">
        <li class="hover-toggle">Inbox</li>
        <li class="hover-toggle">Sent Messages</li>
    </ul>
    <ul class="folder-selection">
        <span class="ui-icon ui-icon-circle-triangle-s fr"></span>
        <li class="selected">Inbox</li>
        <li>Sent Messages</li>
    </ul>
</div>