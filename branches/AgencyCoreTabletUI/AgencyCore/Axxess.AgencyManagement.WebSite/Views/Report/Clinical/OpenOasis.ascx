﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "ClinicalOpenOasis"; %>
<div class="wrapper">
    <fieldset>
        <legend>All Open OASIS Assessments</legend>
        <div class="column">
             <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
             <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"> <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="short" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" class="short" /></div></div>
        </div>
         <div class="buttons"><ul><li><a onclick="Report.RebindClinicalOpenOasis();return false">Generate Report</a></li></ul></div>
         <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportClinicalOpenOasis", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now}, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<OpenOasis>().Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(e => e.PatientIdNumber).Title("MR#").Width(90);
               columns.Bound(e => e.PatientName);
               columns.Bound(e => e.AssessmentName).Title("Assessment Type");
               columns.Bound(e => e.Date).Width(80);
               columns.Bound(e => e.Status);
               columns.Bound(e => e.CurrentlyAssigned).Title("Employee");
           }).DataBinding(dataBinding => dataBinding.Ajax().Select("ClinicalOpenOasis", "Report", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now}))
           .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>