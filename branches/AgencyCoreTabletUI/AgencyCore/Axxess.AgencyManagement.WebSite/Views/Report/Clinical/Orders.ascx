﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
    <fieldset>
        <legend>Orders Management</legend>
        <div class="column">
            <div class="row">
                <label for="Report_Patient_Orders_Status" class="float-left">Status:</label>
                <div class="float-right">
                    <select id="Report_Patient_Orders_Status" name="StatusId" class="PatientStatusDropDown">
                        <option value="5">To be Sent</option>
                        <option value="6">Pending Signature</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="buttons">
                <ul>
                    <li><a onclick="Report.RebindClinicalOrders();return false">Generate Report</a></li>
                </ul>
            </div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<ClinicalOrder>().Name("Report_Patient_Orders_Grid").Columns(columns => {
               columns.Bound(p => p.Number);
               columns.Bound(p => p.PatientName);
               columns.Bound(p => p.Type);
               columns.Bound(p => p.Physician);
               columns.Bound(p => p.CreatedDate);
           }).DataBinding(dataBinding => dataBinding.Ajax().Select("ClinicalOrders", "Report", new { StatusId = 5 })).Sortable().Scrollable().Footer(false) %>
    </div>
</div>