﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "ClinicalThirteenAndNineteenVisitException"; %>
<div class="wrapper">
    <fieldset>
        <legend> 13th And 19th Visit Exception </legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", "", new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindClinicalThirteenAndNineteenVisitException();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportClinicalThirteenAndNineteenVisitException", new { BranchId = Guid.Empty }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<PatientEpisodeTherapyException>().Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(o => o.PatientIdNumber).Title("MRN").Width(70).Sortable(false).ReadOnly();
               columns.Bound(o => o.PatientName).Title("Name").Sortable(false).ReadOnly();
               columns.Bound(o => o.EpisodeRange).Title("Episode").Format("{0:MM/dd/yyyy}").Sortable(false).ReadOnly();
               columns.Bound(o => o.ScheduledTherapy).Title("Scheduled").Width(70).Sortable(false).ReadOnly();
               columns.Bound(o => o.CompletedTherapy).Title("Completed").Width(70).Sortable(false).ReadOnly();
               columns.Bound(o => o.EpisodeDay).Title("Day").Width(30).Sortable(false).ReadOnly();
               columns.Bound(o => o.ThirteenVisit).Title("13th Visit").Sortable(false).ReadOnly();
               columns.Bound(o => o.NineteenVisit).Title("19th Visit").Sortable(false).ReadOnly();
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>

