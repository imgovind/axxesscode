﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "ClinicalNineteenTherapyReevaluationException"; %>
<div class="wrapper">
    <fieldset>
        <legend> 19th Therapy Re-evaluation Exception </legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("BranchCode", "", new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a onclick="Report.RebindClinicalNineteenTherapyReevaluationException();return false">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportClinicalNineteenTherapyReevaluationException", new { BranchId = Guid.Empty }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<PatientEpisodeTherapyException>().Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(o => o.PatientIdNumber).Title("MRN").Width(70).Sortable(false).ReadOnly();
               columns.Bound(o => o.PatientName).Title("Name").Sortable(false).ReadOnly();
               columns.Bound(o => o.EpisodeRange).Title("Episode").Sortable(false).ReadOnly();
               columns.Bound(o => o.ScheduledTherapy).Title("Scheduled").Width(70).Sortable(false).ReadOnly();
               columns.Bound(o => o.CompletedTherapy).Title("Completed").Width(70).Sortable(false).ReadOnly();
               columns.Bound(o => o.EpisodeDay).Title("Day").Width(30).Sortable(false).ReadOnly();
               columns.Bound(o => o.PTEval).Title("PT Re-eval").Width(70).Sortable(false).ReadOnly();
               columns.Bound(o => o.STEval).Title("ST Re-eval").Width(70).Sortable(false).ReadOnly();
               columns.Bound(o => o.OTEval).Title("OT Re-eval").Width(70).Sortable(false).ReadOnly();
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>