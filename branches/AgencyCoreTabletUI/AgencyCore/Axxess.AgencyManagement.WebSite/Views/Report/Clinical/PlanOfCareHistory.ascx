﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%  string pagename = "ClinicalPlanOfCareHistory"; %>
<div class="wrapper main">
    <fieldset>
        <legend>Plan of Care History</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Branch:</label>
                <div class="float-right"><%= Html.ReportBranchList("AddressBranchCode", "", new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Status:</label>
                <div class="float-right">
                    <%  var status = new SelectList(new[] {
                            new SelectListItem { Value = "000", Text = "All" },
                            new SelectListItem { Value = "100", Text = "Not Yet Started" },
                            new SelectListItem { Value = "105", Text = "Not Yet Due" },
                            new SelectListItem { Value = "110", Text = "Saved" },
                            new SelectListItem { Value = "115", Text = "Submitted (Pending QA Review)" },
                            new SelectListItem { Value = "120", Text = "Returned for Review" },
                            new SelectListItem { Value = "125", Text = "To Be Sent to Physician" },
                            new SelectListItem { Value = "130", Text = "Sent to Physician" },
                            new SelectListItem { Value = "135", Text = "Returned with Physician Signature" }
                        }, "Value", "Text", 000); %>
                    <%= Html.DropDownList(pagename+"_Status", status)%>
                </div>
            </div> 
            <div class="row">
                <label class="float-left">Date Range:</label>
                <div class="float-right">
                    <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="short" />
                    To
                    <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" class="short" />
                </div>
            </div>
        </div>
        <div class="column">
            <div class="buttons">
                <ul>
                    <li><a onclick="Report.RebindClinicalPlanOfCareHistory();return false">Generate Report</a></li>
                </ul>
                <ul>
                    <li><%= Html.ActionLink("Export to Excel", "ExportClinicalPlanOfCareHistory", new { BranchId = Guid.Empty, Status = 000, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li>
                </ul>
            </div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<Order>().Name(pagename+"Grid").Columns(columns => {
               columns.Bound(o => o.Number).Title("Order").Width(70).Sortable(false).ReadOnly();
               columns.Bound(o => o.PatientName).Title("Patient").Sortable(false).ReadOnly();
               columns.Bound(o => o.PhysicianName).Title("Physician").Sortable(false).ReadOnly();
               columns.Bound(o => o.CreatedDate).Title("Order Date").Format("{0:MM/dd/yyyy}").Width(100).Sortable(false).ReadOnly();
               columns.Bound(o => o.SendDateFormatted).Title("Sent Date").Format("{0:MM/dd/yyyy}").Width(100).Sortable(false).ReadOnly();
               columns.Bound(o => o.ReceivedDateFormatted).Format("{0:MM/dd/yyyy}").Title("Received Date").Width(120).Sortable(false);
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Status = 000, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable().Footer(false) %>
    </div>
</div>