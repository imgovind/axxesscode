﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "ScheduleCaseManagerTask"; %>
<div class="wrapper main">
    <fieldset>
         <legend> Case Manager Tasks</legend>
         <div class="column">
            <div class="row"><label for="" class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label  class="float-left">Date Range:</label><div class="float-right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="ScheduleCaseManagerTask_StartDate" class="short" /><label > To </label><input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="ScheduleCaseManagerTask_EndDate" class="short" /></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindScheduleCaseManagerTask();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportScheduleCaseManagerTask", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>Result" class="report-grid">
        <% =Html.Telerik().Grid<ScheduleEvent>().Name(pagename + "Grid")        
             .Columns(columns =>
             {
             columns.Bound(m => m.PatientIdNumber).Title("ID").Width(70);
             columns.Bound(m => m.PatientName).Title("Patient Name");
             columns.Bound(m => m.EventDate).Title("Schedule Date").Format("{0:MM/dd/yyyy}").Width(90);
             columns.Bound(m => m.DisciplineTaskName).Title("Task");
             columns.Bound(p => p.UserName).Title("User Name").Width(155);
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
           .Sortable().Selectable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); </script>
