﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<% string pagename = "SchedulePastDueRecet"; %>
<div class="wrapper main">
    <fieldset>
    <legend> Past Due Recet.</legend>
         <div class="column">
            <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label class="float-left">Insurance:</label><div class="float-right"><%= Html.Insurances("InsuranceId", Model, new { @id = pagename + "_InsuranceId", @class = "Insurances" })%></div></div>
            <div class="row"><label class="float-left">Due Date From:</label><div class="float-right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="short" /> </div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindSchedulePastDueRecet();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportSchedulePastDueRecet", new { BranchId = Guid.Empty, InsuranceId=Model,StartDate = DateTime.Now.AddDays(-60) }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>_report-grid" class="report-grid">
         <%= Html.Telerik().Grid<RecertEvent>().Name(pagename + "Grid").Columns(columns =>
{
    columns.Bound(r => r.PatientName).Sortable(true);
    columns.Bound(r => r.PatientIdNumber).Title("MR#").Sortable(true).Width(120);
    columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true);
    columns.Bound(r => r.Status).Title("Status").Sortable(true);
    columns.Bound(r => r.TargetDate).Format("{0:MM/dd/yyyy}").Title("Due Date").Width(120).Sortable(true);
    columns.Bound(r => r.DateDifference).Title("Past Dates").Width(60);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("ScheduleRecertsPastDue", "Report", new { BranchId = Guid.Empty, InsuranceId=Model, StartDate = DateTime.Now.AddDays(-60) })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Footer(false)
    %>
    </div>
</div>
<script type="text/javascript">    $("#<%= pagename %>_report-grid").css({ 'top': '190px' }); $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); </script>
