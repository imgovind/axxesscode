﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "ScheduleEmployeeWeekly"; %>
<div class="wrapper">
    <fieldset>
        <legend>Employee Weekly Schedule</legend>
        <div class="column">
              <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
              <div class="row"><label class="float-left">Status:</label><div class="float-right"><select id="ScheduleEmployeeWeekly_Status" name="ScheduleEmployeeWeekly_Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div></div>
              <div class="row"><label class="float-left">Employees:</label> <div class="float-right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, pagename + "_Users", "",Guid.Empty,1, new { @id = pagename + "_Users", @class = "report_input valid" })%></div> </div>
              <div class="row"><label class="float-left">Date Range:</label><div class="float-right"><input type="date" name="StartDate" value="<%= DateTime.Today.ToShortDateString() %>" id="<%= pagename %>_StartDate" class="short" /> To <input type="date" name="EndDate" value="<%= DateTime.Today.AddDays(7).ToShortDateString() %>" id="<%= pagename %>_EndDate" class="short" /></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindScheduleEmployeeWeekly();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportScheduleEmployeeWeekly", new { BranchId = Guid.Empty, userId = Guid.Empty, StartDate = DateTime.Today, EndDate = DateTime.Today.AddDays(7) }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>_report-grid" class="report-grid">
        <% =Html.Telerik().Grid<UserVisit>().Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(s => s.PatientName).Title("Patient");
         columns.Bound(s => s.TaskName).Title("Task");
         columns.Bound(p => p.StatusName).Title("Status");
         columns.Bound(p => p.ScheduleDate).Title("Schedule Date").Width(120);
         columns.Bound(p => p.VisitDate).Title("Visit Date").Width(110);
         }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { BranchId = Guid.Empty, userId = Guid.Empty, StartDate = DateTime.Today, EndDate = DateTime.Today.AddDays(7) })).Sortable().Selectable().Scrollable().Footer(false)%>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $("#<%= pagename %>_report-grid").css({ 'top': '190px' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_Status').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
</script>
