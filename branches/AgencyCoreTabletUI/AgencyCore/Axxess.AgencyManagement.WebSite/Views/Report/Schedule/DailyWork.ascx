﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "ScheduleDailyWork"; %>
<div class="wrapper">
    <fieldset>
        <legend>Daily Work Schedule</legend>
        <div class="column">
              <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
              <div class="row"><label  class="float-left">Date:</label><div class="float-right"><input type="date" name="Date" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_Date" class="short" /></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindScheduleDailyWork();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportScheduleDailyWork", new { BranchId = Guid.Empty, Date = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>Result" class="report-grid">
        <% =Html.Telerik().Grid<ScheduleEvent>().Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(m => m.PatientIdNumber).Title("ID").Width(70);
         columns.Bound(m => m.PatientName).Title("Patient Name");
         columns.Bound(s => s.DisciplineTaskName).Title("Task");
         columns.Bound(p => p.StatusName).Title("Status");
         columns.Bound(p => p.EventDate).Title("Schedule Date").Width(100);
         columns.Bound(s => s.UserName).Title("Employee");
       }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { BranchId = Guid.Empty, Date = DateTime.Now })).Sortable().Selectable().Scrollable().Footer(false)%>
    </div>
</div>
<script type="text/javascript"> $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); </script>
 
