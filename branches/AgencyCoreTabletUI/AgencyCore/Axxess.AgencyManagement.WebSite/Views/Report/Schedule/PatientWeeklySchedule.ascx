﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "SchedulePatientWeeklySchedule"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Weekly Schedule</legend>
        <div class="column">
              <div class="row"><label for="BranchCode" class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
              <div class="row"><label for="SchedulePatientWeeklySchedule_Status" class="float-left">Status:</label><div class="float-right"><select id="SchedulePatientWeeklySchedule_Status" name="SchedulePatientWeeklySchedule_Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
              <div class="row"><label for="" class="float-left">Patient:</label> <div class="float-right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Patients, pagename + "_Patients", "",Guid.Empty,1, new { @id = pagename + "_Patients", @class = "report_input valid" })%></div> </div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindSchedulePatientWeeklySchedule();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportSchedulePatientWeeklySchedule", new { patientId = Guid.Empty }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>Result" class="report-grid">
        <% =Html.Telerik().Grid<ScheduleEvent>().Name(pagename + "Grid")        
             .Columns(columns =>
                 {
                 columns.Bound(s => s.DisciplineTaskName).Title("Task");
                 columns.Bound(p => p.StatusName).Title("Status");
                 columns.Bound(p => p.EventDate).Title("Schedule Date").Width(100);
                 columns.Bound(p => p.VisitDate).Title("Visit Date").Width(80);
                 columns.Bound(s => s.UserName).Title("Employee");
               })
               .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { patientId = Guid.Empty}))
               .Sortable().Selectable().Scrollable() .Footer(false)%>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_Status').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); }); 
</script>
