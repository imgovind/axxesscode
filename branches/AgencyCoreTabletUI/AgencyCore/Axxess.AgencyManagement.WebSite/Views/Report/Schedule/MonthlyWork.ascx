﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "ScheduleMonthlyWork"; %>
<div class="wrapper">
    <fieldset>
        <legend>Monthly Work Schedule</legend>
        <div class="column">
              <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
              <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="ScheduleMonthlyWork_Status" name="ScheduleMonthlyWork_Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div></div>
              <div class="row"><label  class="float-left">Employees:</label><div class="float-right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, pagename + "_Users", "",Guid.Empty,1, new { @id = pagename + "_Users", @class = "report_input valid" })%></div> </div>
              <div class="row"><label  class="float-left">Month:</label><div class="float-right"><% var months = new SelectList(new[] { new SelectListItem { Text = "Select Month", Value = "0" }, new SelectListItem { Text = "January", Value = "1" }, new SelectListItem { Text = "February", Value = "2" }, new SelectListItem { Text = "March", Value = "3" }, new SelectListItem { Text = "April", Value = "4" }, new SelectListItem { Text = "May", Value = "5" }, new SelectListItem { Text = "June", Value = "6" }, new SelectListItem { Text = "July", Value = "7" }, new SelectListItem { Text = "August", Value = "8" }, new SelectListItem { Text = "September", Value = "9" }, new SelectListItem { Text = "October", Value = "10" }, new SelectListItem { Text = "November", Value = "11" }, new SelectListItem { Text = "December", Value = "12" } }, "Value", "Text", DateTime.Now.Month);%><%= Html.DropDownList(pagename + "_Month", months, new { @id = pagename + "_Month", @class = "oe" })%></div> </div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindScheduleMonthlyWork();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportScheduleMonthlyWork", new { userId = Guid.Empty, BranchId = Guid.Empty, month = DateTime.Now.Month }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>_report-grid" class="report-grid">
        <% =Html.Telerik().Grid<UserVisit>().Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(s => s.PatientName).Title("Patient");
         columns.Bound(s => s.TaskName).Title("Task");
         columns.Bound(p => p.StatusName).Title("Status");
         columns.Bound(p => p.ScheduleDate).Title("Schedule Date").Width(110);
         columns.Bound(p => p.VisitDate).Title("Visit Date").Width(100);
         }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { userId = Guid.Empty, month = DateTime.Now.Month, BranchId = Guid.Empty })).Sortable().Selectable().Scrollable().Footer(false)%>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $("#<%= pagename %>_report-grid").css({ 'top': '190px' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_Status').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
</script>

