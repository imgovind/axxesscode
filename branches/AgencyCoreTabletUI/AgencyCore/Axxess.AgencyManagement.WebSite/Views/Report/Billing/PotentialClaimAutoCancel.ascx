﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PotentialClaimAutoCancel"; %>
<div class="wrapper">
    <fieldset>
        <legend>Potential Claim Auto Cancel</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("AddressBranchCode", "", new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a onclick="Report.RebindPotentialClaimAutoCancel();return false">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPotentialClaimAutoCancel", new { BranchId = Guid.Empty }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<Rap>().Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("ID").Width(80);
               columns.Bound(p => p.LastName).Title("Last Name");
               columns.Bound(p => p.FirstName).Title("First Name");
               columns.Bound(p => p.EpisodeRange).Title("Episode Range");
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty})).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>


