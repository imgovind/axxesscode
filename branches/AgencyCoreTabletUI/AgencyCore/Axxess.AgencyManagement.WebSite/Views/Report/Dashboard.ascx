﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="fl">
    <div class="reports">
        <div class="reports-head">
            <h5>Patient Reports</h5>
        </div>
    </li>
    <li class="widget">
        <div class="widget-head"><h5>Billing / Financial Reports</h5></div>
        <div class="widget-content" >
            <div class="report_links">
                <ul> 
                     <li class="link"><a href="/Report/Billing/OutstandingClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Outstanding Claims</span></a></li> 
                     <li class="link"><a href="/Report/Billing/SubmittedClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Submitted Claims</span></a></li> 
                     <li class="link"><a href="/Report/Billing/ByStatusSummary" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Claims History By Status</span></a></li> 
                     <li class="link"><a href="/Report/Billing/AccountsReceivable" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Accounts Receivable Report</span></a></li> 
                     <li class="link"><a href="/Report/Billing/AgedAccountsReceivable" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Aged Accounts Receivable Report</span></a></li> 
                     <li class="link"><a href="/Report/Billing/PPSRAPClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">PPS RAP Claims Needed</span></a></li> 
                     <li class="link"><a href="/Report/Billing/PPSFinalClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">PPS Final Claims Needed</span></a></li> 
                     <li class="link"><a href="/Report/Billing/PotentialClaimAutoCancel" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Potential Claim Auto Cancel</span></a></li> 
                     <li class="link"><a href="/Report/Billing/BillingBatch" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Billing Batch Report</span></a></li> 
                </ul>
            </div>
        </div>
        <ul>
            <li><a href="/Report/Employee/Roster" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Employee Roster</a></li>
            <li><a href="/Report/Employee/License" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Employee License Listing</a></li>
            <li><a href="/Report/Employee/ExpiringLicense" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Expiring Licenses</a></li>
            <li><a href="/Report/Employee/VisitByDateRange" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Employee Visit By Date Range</a></li>
        </ul>
    </div>
</div>
<div class="fl">
    <div class="reports">
        <div class="reports-head">
            <h5>Clinical Reports</h5>
        </div>
        <ul>
            <li><a href="/Report/Clinical/OpenOasis" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Open OASIS</a></li>
            <li><a href="/Report/Clinical/MissedVisits" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Missed Visit Report</a></li>
            <li><a href="javascript:void(0);" onclick="Acore.Open('orderstobesent');">Orders To Be Sent</a></li>
            <li><a href="javascript:void(0);" onclick="Acore.Open('orderspendingsignature');">Orders Pending Signature</a></li>
            <li><a href="/Report/Clinical/PhysicianOrderHistory" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Physician Order History</a></li>
            <li><a href="/Report/Clinical/PlanOfCareHistory" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Plan Of Care History</a></li>
            <li><a href="/Report/Clinical/ThirteenAndNineteenVisitException" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">13th And 19th Therapy Visit Exception</a></li>
            <li><a href="/Report/Clinical/ThirteenTherapyReevaluationException" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">13th Therapy Re-evaluation Exception Reports</a></li>
            <li><a href="/Report/Clinical/NineteenTherapyReevaluationException" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">19th Therapy Re-evaluation Exception Reports</a></li>
        </ul>
    </div>
    <div class="reports">
        <div class="reports-head">
            <h5>Billing / Financial Reports</h5>
        </div>
        <ul>
            <li><a href="/Report/Billing/OutstandingClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Outstanding Claims</a></li>
            <li><a href="/Report/Billing/SubmittedClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Submitted Claims</a></li>
            <li><a href="/Report/Billing/ByStatusSummary" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Claims History By Status</a></li>
            <li><a href="/Report/Billing/AccountsReceivable" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Accounts Receivable Report</a></li>
            <li><a href="/Report/Billing/AgedAccountsReceivable" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Aged Accounts Receivable Report</a></li>
            <li><a href="/Report/Billing/PPSRAPClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">PPS RAP Claims Needed</a></li>
            <li><a href="/Report/Billing/PPSFinalClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">PPS Final Claims Needed</a></li>
            <li><a href="/Report/Billing/PotentialClaimAutoCancel" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Potential Claim Auto Cancel</a></li>
        </ul>
    </div>
</div>
<div class="fl">
    <div class="reports">
        <div class="reports-head">
            <h5>Schedule Reports</h5>
        </div>
        <ul>
            <li><a href="/Report/Schedule/PatientWeekly" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Patient Weekly Schedule</a></li>
            <li><a href="/Report/Schedule/PatientMonthlySchedule" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Patient Monthly Schedule</a></li>
            <li><a href="/Report/Schedule/EmployeeWeekly" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Employee Weekly Schedule</a></li>
            <li><a href="/Report/Schedule/DailySchedule" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Employee Daily Work Schedule</a></li>
            <li><a href="/Report/Schedule/MonthlySchedule" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Employee Monthly Work Schedule</a></li>
            <li><a href="/Report/Schedule/PastDueVisits" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Past Due Visits</a></li>
            <li><a href="/Report/Schedule/PastDueVisitsByDiscipline" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Past Due Visits By Discipline</a></li>
            <li><a href="/Report/Schedule/CaseManagerTask" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Case Manager Task List</a></li>
            <li><a href="/Report/Schedule/Deviation" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Schedule Deviation</a></li>
            <li><a href="/Report/Schedule/PastDueRecet" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Past Due Recertification</a></li>
            <li><a href="/Report/Schedule/UpcomingRecet" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Upcoming Recertification</a></li>
        </ul>
    </div>
    <div class="reports">
        <div class="reports-head">
            <h5>Statistical Reports</h5>
        </div>
        <ul>
            <li><a href="/Report/Statistical/PatientVisits" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Patient Visit History</a></li>
            <li><a href="/Report/Statistical/EmployeeVisits" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Employee Visit History</a></li>
            <li><a href="/Report/Statistical/CensusByPrimaryInsurance" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Census By Primary Insurance</a></li>
            <li><a href="/Report/Statistical/MonthlyAdmission" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Monthly Admission Report</a></li>
            <li><a href="/Report/Statistical/AnnualAdmission" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Annual Admission Report</a></li>
            <li><a href="/Report/Statistical/UnduplicatedCensusReport" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;">Unduplicated Census Report</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $('#ClaimsByStatusGrid .t-grid-content').css({ 'height': 'auto' });
    $('#OutstandingClaimsGrid .t-grid-content').css({ 'height': 'auto' });
    
    $('#ClinicalMissedVisitGrid .t-grid-content').css({ 'height': 'auto' });
    $("#ClinicalOpenOasisGrid .t-grid-content").css({ 'height': 'auto' });
    $('#ClinicalPhysicianOrderHistoryGrid .t-grid-content').css({ 'height': 'auto' });
    $('#ClinicalPlanOfCareHistoryGrid .t-grid-content').css({ 'height': 'auto' });

    $('#EmployeeBirthdayListGrid .t-grid-content').css({ 'height': 'auto' });
    $('#EmployeeLicenseListingGrid .t-grid-content').css({ 'height': 'auto' });
    $('#EmployeeLicenseListing_BranchCode').change(function() { Report.loadUsersDropDown('EmployeeLicenseListing'); });
    $('#EmployeeLicenseListing_Status').change(function() { Report.loadUsersDropDown('EmployeeLicenseListing'); });
    $('#EmployeeRosterGrid .t-grid-content').css({ 'height': 'auto' });

    $('#PatientRosterGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientEmergencyListGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientBirthdayListGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientAddressListGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientByPhysiciansGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientSocCertPeriodListingGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientByResponsibleEmployeeListingGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientByResponsibleEmployeeListing_BranchCode').change(function() { Report.loadUsersDropDown('PatientByResponsibleEmployeeListing'); });
    $('#PatientByResponsibleEmployeeListing_Status').change(function() { Report.loadUsersDropDown('PatientByResponsibleEmployeeListing'); });

    $('#SchedulePatientWeeklyScheduleGrid .t-grid-content').css({ 'height': 'auto' });
    $('#SchedulePatientWeeklySchedule_BranchCode').change(function() { Report.loadPatientsDropDown('SchedulePatientWeeklySchedule'); });
    $('#SchedulePatientWeeklySchedule_Status').change(function() { Report.loadPatientsDropDown('SchedulePatientWeeklySchedule'); });
    $('#ScheduleEmployeeWeeklyGrid .t-grid-content').css({ 'height': 'auto' });
    $('#ScheduleEmployeeWeekly_BranchCode').change(function() { Report.loadUsersDropDown('ScheduleEmployeeWeekly'); });
    $('#ScheduleEmployeeWeekly_Status').change(function() { Report.loadUsersDropDown('ScheduleEmployeeWeekly'); });
    $('#ScheduleDailyWorkGrid .t-grid-content').css({ 'height': 'auto' });
    $('#ScheduleMonthlyWorkGrid .t-grid-content').css({ 'height': 'auto' });
    $('#ScheduleMonthlyWork_BranchCode').change(function() { Report.loadUsersDropDown('ScheduleMonthlyWork'); });
    $('#ScheduleMonthlyWork_Status').change(function() { Report.loadUsersDropDown('ScheduleMonthlyWork'); });
    $('#SchedulePastDueVisitsGrid .t-grid-content').css({ 'height': 'auto' });

    $('#StatisticalEmployeeVisitHistoryGrid .t-grid-content').css({ 'height': 'auto' });
    $('#StatisticalEmployeeVisitHistory_BranchCode').change(function() { Report.loadUsersDropDown('StatisticalEmployeeVisitHistory'); });
    $('#StatisticalEmployeeVisitHistory_Status').change(function() { Report.loadUsersDropDown('StatisticalEmployeeVisitHistory'); });
    $('#StatisticalPatientVisitHistoryGrid .t-grid-content').css({ 'height': 'auto' });
    $('#StatisticalPatientVisitHistory_BranchCode').change(function() { Report.loadPatientsDropDown('StatisticalPatientVisitHistory'); });
    $('#StatisticalPatientVisitHistory_Status').change(function() { Report.loadPatientsDropDown('StatisticalPatientVisitHistory'); }); 
    
</script>
