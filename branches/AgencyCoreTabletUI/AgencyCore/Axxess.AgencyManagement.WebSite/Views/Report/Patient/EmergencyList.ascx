﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientEmergencyList"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Emergency Contact</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("AddressBranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="PatientEmergencyList_Status" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientEmergencyListing();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientEmergencyList", new { BranchCode = Guid.Empty, StatusId = 1 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<EmergencyContactInfo>().Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientName).Title("Patient");
               columns.Bound(p => p.Triage).Title("Triage").Width(50);
               columns.Bound(p => p.ContactName).Title("Contact");
               columns.Bound(p => p.ContactRelation).Title("Relationship");
               columns.Bound(p => p.ContactPhoneHome).Title("Contact Phone").Width(110);
               columns.Bound(p => p.ContactEmailAddress).Title("Contact E-mail").Width(120);

           }).DataBinding(dataBinding => dataBinding.Ajax().Select("PatientEmergencyList", "Report", new { StatusId = 1, BranchCode = Guid.Empty })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>