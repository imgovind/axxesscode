﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientSurveyCensus"; %>
<div class="wrapper">
  <fieldset>
    <legend> Survey Census</legend>
    <div class="column">
        <div class="row"><label for="AddressBranchCode" class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div></div>
        <div class="row"><label for="PatientSurveyCensus_Status" class="float-left">Status:</label><div class="float-right"><select id="PatientSurveyCensus_Status" name="Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected >Active</option><option value="2">Discharged</option></select></div></div>
    </div>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientSurveyCensus();">Generate Report</a></li></ul></div>
    <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientSurveyCensus", new { BranchId = Guid.Empty, Status = 1 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
</fieldset>
  <div id="<%= pagename %>Result" class="report-grid">
    <% =Html.Telerik().Grid<SurveyCensus>().Name(pagename + "Grid").Columns(columns =>
     {
     columns.Bound(p => p.PatientIdNumber).Title("MR #").Width(70);
     columns.Bound(p => p.PatientDisplayName).Title("Patient Name");
     columns.Bound(p => p.MedicareNumber).Title("Medicare #").Width(80);
     columns.Bound(p => p.DOB).Format("{0:MM/dd/yyyy}").Title("DOB").Width(75);
     columns.Bound(p => p.SOC).Format("{0:MM/dd/yyyy}").Title("SOC Date").Width(75);
     columns.Bound(p => p.CertPeriod).Title("Cert Period(Recent)").Width(155);
     columns.Bound(p => p.PrimaryDiagnosis).Title("Primary Diagnosis");
     columns.Bound(p => p.SecondaryDiagnosis).Title("Secondary Diagnosis");
     columns.Bound(p => p.Triage).Title("Triage").Width(50);
     columns.Bound(p => p.Discipline).Title("Disciplines");
     }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new {BranchId = Guid.Empty, Status = 1 })).Sortable().Selectable().Scrollable().Footer(false)
    %>
</div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>
