﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientAddressList"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Address Listing</legend>
        <div class="column">
            <div class="row"><label class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("AddressBranchCode", "", new { @id = "PatientAddressList_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label class="float-left">Status:</label><div class="float-right"><select id="PatientAddressList_Status" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientAddressListing();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientAddressList", new { BranchId = Guid.Empty, StatusId = 1 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<AddressBookEntry>().Name(pagename +"Grid").Columns(columns =>
           {
               columns.Bound(e => e.Name).Width(140);
               columns.Bound(e => e.AddressFirstRow).Title("Address");
               columns.Bound(e => e.AddressCity).Title("City").Width(90);
               columns.Bound(e => e.AddressStateCode).Title("State").Width(50);
               columns.Bound(e => e.AddressZipCode).Title("Zip Code").Width(60);
               columns.Bound(e => e.PhoneHome).Title("Home Phone").Width(110);
               columns.Bound(e => e.PhoneMobile).Title("Mobile Phone").Width(110);
               columns.Bound(e => e.EmailAddress).Width(110);
           })
           .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty,  StatusId=1 }))
           .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>
