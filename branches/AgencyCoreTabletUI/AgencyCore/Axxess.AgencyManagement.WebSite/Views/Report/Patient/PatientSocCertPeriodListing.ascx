﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientSocCertPeriod>>" %>
<% string pagename = "PatientSocCertPeriodListing"; %>
<div class="wrapper">
<fieldset>
<legend> Patient SOC Cert Period Listing</legend>
     <div class="column">
        <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div></div>
        <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="PatientSocCertPeriodListing_Status" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected >Active</option><option value="2">Discharged</option></select></div></div>
        <div class="row"><label  class="float-left">SOC Cert. Start Date:</label><div class="float-right">From : <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="shortdate" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" class="shortdate" /></div></div>
    </div>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientSocCertPeriod();">Generate Report</a></li></ul></div>
    <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientSocCertPeriodListing", new { StatusId = 1, BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
</fieldset>
<div id="<%= pagename %>Result" class="report-grid">
    <% =Html.Telerik().Grid<PatientSocCertPeriod>()
                                 .Name(pagename + "Grid")        
         .Columns(columns =>
     {
     columns.Bound(p => p.PatientPatientID).Title("ID").Width(60);
     columns.Bound(p => p.PatientLastName).Title("Last Name");
     columns.Bound(p => p.PatientFirstName).Title("First Name");
     columns.Bound(p => p.PatientSoC).Title("SOC Date").Width(80);
     columns.Bound(p => p.SocCertPeriod).Title("SOC Cert Period").Width(155);
     columns.Bound(p => p.PhysicianName);
     columns.Bound(p => p.respEmp).Title("Employee");
     }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { StatusId = 1, BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
   .Sortable().Selectable().Scrollable().Footer(false)
  
    %>
</div>
</div>
<script type="text/javascript"> $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); </script>