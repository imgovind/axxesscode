﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientByPhysicians"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient By Physician Listing</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox(pagename + "_PhysicanId", "", new { @id = pagename + "_PhysicanId", @class = "physicians" })%></div>
            </div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientPhysicians();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientByPhysicians", new { AgencyPhysicianId = Guid.Empty }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
         <%= Html.Telerik().Grid<PatientRoster>().Name(pagename + "Grid").Columns(columns =>
            {
                columns.Bound(r => r.PatientDisplayName).Title("Patient");
                columns.Bound(r => r.PatientAddressLine1).Title("Address");
                columns.Bound(r => r.PatientAddressCity).Title("City");
                columns.Bound(r => r.PatientAddressStateCode).Title("State");
                columns.Bound(r => r.PatientAddressZipCode).Title("Zip Code");
                columns.Bound(r => r.PatientPhone).Title("Home Phone");
                columns.Bound(r => r.PatientGender).Title("Patient Gender");
            }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { AgencyPhysicianId = Guid.Empty }))
           .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>