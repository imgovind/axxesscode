﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "EmployeeBirthdayList"; %>
<div class="wrapper">
    <fieldset>
        <legend>Employee Birthday Listing</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("AddressBranchCode", "", new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="EmployeeBirthdayList_Status" name="EmployeeBirthdayList_Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div></div>
            <div class="row"><label  class="float-left">Month:</label><div class="float-right"><% var months = new SelectList(new[] { new SelectListItem { Text = "Select Month", Value = "0" }, new SelectListItem { Text = "January", Value = "1" }, new SelectListItem { Text = "February", Value = "2" }, new SelectListItem { Text = "March", Value = "3" }, new SelectListItem { Text = "April", Value = "4" }, new SelectListItem { Text = "May", Value = "5" }, new SelectListItem { Text = "April", Value = "6" }, new SelectListItem { Text = "June", Value = "7" }, new SelectListItem { Text = "August", Value = "8" }, new SelectListItem { Text = "September", Value = "9" }, new SelectListItem { Text = "October", Value = "10" }, new SelectListItem { Text = "November", Value = "11" }, new SelectListItem { Text = "December", Value = "12" } }, "Value", "Text", DateTime.Now.Month);%><%= Html.DropDownList(pagename + "_Month", months, new { @id = pagename + "_Month", @class = "oe" })%></div> </div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindEmployeeBirthdayList();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportEmployeeBirthdayList", new { BranchId = Guid.Empty, Status=1 ,month = DateTime.Now.Month }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<Birthday>().Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.Name).Width(150);
               columns.Bound(p => p.Age).Width(50);
               columns.Bound(p => p.BirthDay).Width(130);
               columns.Bound(p => p.AddressFirstRow);
               columns.Bound(p => p.AddressSecondRow);
               columns.Bound(p => p.PhoneHomeFormatted).Title("Home Phone").Width(110);
           })
                                   .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Status = 1, month = DateTime.Now.Month }))
           .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>