﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "EmployeeLicenseListing"; %>
<div class="wrapper">
    <fieldset>
    <legend> Employee License Listing</legend>
        <div class="column">
          <div class="row"><label for="BranchCode" class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
          <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="EmployeeLicenseListing_Status" name="EmployeeLicenseListing_Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div></div>
          <div class="row"><label for="" class="float-left">Employees:</label> <div class="float-right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, pagename + "_Users", "",Guid.Empty,1, new { @id = pagename + "_Users", @class = "report_input valid" })%></div> </div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindEmployeeLicenseListing();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportEmployeeLicenseListing", new {userId = Guid.Empty }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>Result" class="report-grid">
        <% =Html.Telerik().Grid<License>().Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(l => l.LicenseType).Title("Name");
         columns.Bound(l => l.InitiationDateFormatted).Title("Initiation Date");
         columns.Bound(l => l.ExpirationDateFormatted).Title("Expiration Date");
       }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new {userId = Guid.Empty})).Sortable().Selectable().Scrollable() .Footer(false)%>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_Status').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
 </script>

