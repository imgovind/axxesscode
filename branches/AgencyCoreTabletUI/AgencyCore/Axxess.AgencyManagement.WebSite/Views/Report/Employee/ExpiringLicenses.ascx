﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "EmployeeExpiringLicense"; %>
<div class="wrapper">
    <fieldset>
    <legend> Employee Expiring Licenses</legend>
        <div class="column">
          <div class="row"><label for="BranchCode" class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
          <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="EmployeeExpiringLicense_Status" name="EmployeeExpiringLicense_Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindEmployeeExpiringLicense();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportEmployeeExpiringLicense", new { BranchId = Guid.Empty, Status = 1 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>Result" class="report-grid">
        <% =Html.Telerik().Grid<License>().Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(l => l.UserDispalyName).Title("Employee");
         columns.Bound(l => l.LicenseType).Title("License Name");
         columns.Bound(l => l.InitiationDateFormatted).Title("Initiation Date");
         columns.Bound(l => l.ExpirationDateFormatted).Title("Expiration Date");
         }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty , Status=1})).Sortable().Selectable().Scrollable().Footer(false)%>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
 </script>
