﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<% string pagename = "EmployeeRoster"; %>
<div class="wrapper">
    <fieldset>
        <legend>Employee Roster</legend>
        <div class="column">
            <div class="row"><label for="EmployeeRoster_BranchCode" class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "EmployeeRoster_BranchCode", "", new { @id = "EmployeeRoster_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label for="EmployeeRoster_Status" class="float-left">Status:</label><div class="float-right"><select id="EmployeeRoster_Status" name="EmployeeRoster_Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindEmployeeRoster();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportEmployeeRoster", new { BranchId = Guid.Empty, Status = 1 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
         <%= Html.Telerik().Grid<User>().Name(pagename + "Grid").Columns(columns =>
            {
                columns.Bound(r => r.DisplayName).Title("Name");
                columns.Bound(r => r.Profile.Address).Title("Address");
                columns.Bound(r => r.Profile.AddressCity).Title("City");
                columns.Bound(r => r.Profile.AddressStateCode).Title("State").Width(40);
                columns.Bound(r => r.Profile.AddressZipCode).Title("Zip Code").Width(80);
                columns.Bound(r => r.HomePhone).Title("Home Phone").Width(110);
                columns.Bound(r => r.Profile.Gender).Title("Gender").Width(50);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new {BranchId = Guid.Empty , Status=1})).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); </script>