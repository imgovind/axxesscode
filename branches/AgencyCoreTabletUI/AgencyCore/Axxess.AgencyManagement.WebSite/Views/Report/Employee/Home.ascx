﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul>
    <li class="widget">
        <div class="widget-head"><h5>Employee Reports</h5></div>
        <div class="widget-content" style="height: 100%;">
            <div class="report_links">
                <ul class="float-left half"> 
                    <li class="link"><a href="/Report/Employee/Roster" onclick="Report.Show(5, '#employee_reports', $(this).attr('href')); return false;"><span class="title">Employee Roster</span></a></li> 
                    <li class="link"><a href="/Report/Employee/Birthdays" onclick="Report.Show(5, '#employee_reports', $(this).attr('href')); return false;"><span class="title">Employee Birthday Listing</span></a></li> 
                </ul>
                <ul class="float-left half"> 
                    <li class="link"><a href="/Report/Employee/License" onclick="Report.Show(5, '#employee_reports', $(this).attr('href')); return false;"><span class="title">Employee License Listing</span></a></li> 
                </ul>
                <div class="clear">&#160;</div>
            </div>
        </div>
    </li>
</ul>