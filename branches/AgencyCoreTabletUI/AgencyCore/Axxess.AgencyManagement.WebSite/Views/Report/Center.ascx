﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Report Center | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main">
    <div class="buttons">
        <ul>
            <li><a onclick="Reports.Home();return false">Reports Home</a></li>
        </ul>
    </div>
    <% Html.RenderPartial("~/Views/Report/Dashboard.ascx"); %>
</div>