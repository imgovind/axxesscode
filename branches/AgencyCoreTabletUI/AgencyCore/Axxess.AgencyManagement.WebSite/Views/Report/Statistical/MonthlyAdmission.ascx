﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%  string pagename = "StatisticalMonthlyAdmission"; %>
<div class="wrapper main">
    <fieldset>
        <legend>Monthly Admission Patients</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Branch:</label>
                <div class="float-right"><%= Html.ReportBranchList("AddressBranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Status:</label>
                <div class="float-right">
                    <select id="StatisticalMonthlyAdmission_Status" name="StatusId" class="PatientStatusDropDown">
                        <option value="0">All</option>
                        <option value="1" selected>Active</option>
                        <option value="2">Discharged</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Month, Year:</label>
                <div class="float-right">
                    <%  var months = new SelectList(new[] {
                            new SelectListItem { Text = "Select Month", Value = "0" },
                            new SelectListItem { Text = "January", Value = "1" },
                            new SelectListItem { Text = "February", Value = "2" },
                            new SelectListItem { Text = "March", Value = "3" },
                            new SelectListItem { Text = "April", Value = "4" },
                            new SelectListItem { Text = "May", Value = "5" },
                            new SelectListItem { Text = "June", Value = "6" },
                            new SelectListItem { Text = "July", Value = "7" },
                            new SelectListItem { Text = "August", Value = "8" },
                            new SelectListItem { Text = "September", Value = "9" }, new SelectListItem { Text = "October", Value = "10" }, new SelectListItem { Text = "November", Value = "11" }, new SelectListItem { Text = "December", Value = "12" } }, "Value", "Text", DateTime.Now.Month);%><%= Html.DropDownList(pagename + "_Month", months, new { @id = pagename + "_Month", @class = "oe" })%>, <%= Html.Months(pagename + "_Year", DateTime.Now.Year.ToString(), 1999, new { @id = pagename + "_Year", @class = "oe" })%></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindStatisticalMonthlyAdmission();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportStatisticalMonthlyAdmission", new { BranchId = Guid.Empty, Status = 1, month = DateTime.Now.Month, year = DateTime.Now.Year }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<PatientRoster>().Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientFirstName).Title("First Name");
               columns.Bound(p => p.PatientLastName).Title("Last Name");
               columns.Bound(p => p.AdmissionSourceName).Title("Admission Source");
               columns.Bound(p => p.PatientSoC).Title("Admission Date").Width(120);
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Status = 1, month = DateTime.Now.Month, year = DateTime.Now.Year }))
           .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>
