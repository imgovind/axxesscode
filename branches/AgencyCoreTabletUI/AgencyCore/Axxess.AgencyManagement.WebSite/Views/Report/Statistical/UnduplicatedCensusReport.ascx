﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%  string pagename = "StatisticalUnduplicatedCensusReport"; %>
<div class="wrapper main">
    <fieldset>
        <legend>Unduplicated Census Report</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Branch:</label>
                <div class="float-right"><%= Html.ReportBranchList("AddressBranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div>
            </div>
            <div class="row">
                <label class="float-left">Status:</label>
                <div class="float-right">
                    <select id="StatisticalUnduplicatedCensusReport_Status" name="StatusId" class="PatientStatusDropDown">
                        <option value="0">All</option>
                        <option value="1" selected>Active</option>
                        <option value="2">Discharged</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Date Range:</label>
                <div class="float-right">
                    <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="shortdate" />
                    To
                    <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" class="shortdate" />
                </div>
            </div>
        </div>
        <div class="column">
            <div class="buttons">
                <ul>
                    <li><a href="javascript:void(0);" onclick="Report.RebindStatisticalUnduplicatedCensusReport();">Generate Report</a></li>
                </ul>
                <ul>
                    <li><%= Html.ActionLink("Export to Excel", "ExportStatisticalUnduplicatedCensusReport", new { BranchId = Guid.Empty, Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now.Date }, new { id = pagename + "_ExportLink" })%></li>
                </ul>
            </div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<PatientRoster>().Name(pagename+"Grid").Columns(columns => {
               columns.Bound(p => p.PatientFirstName).Title("First Name");
               columns.Bound(p => p.PatientLastName).Title("Last Name");
               columns.Bound(p => p.AdmissionSourceName).Title("Admission Source");
               columns.Bound(p => p.PatientSoC).Title("Admission Date").Width(120);
               columns.Bound(p => p.PatientDischargeDate).Title("Discharge Date").Width(120);
               columns.Bound(p => p.PatientStatusName).Title("Staus").Width(120);
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now.Date })).Sortable().Scrollable().Footer(false) %>
    </div>
</div>