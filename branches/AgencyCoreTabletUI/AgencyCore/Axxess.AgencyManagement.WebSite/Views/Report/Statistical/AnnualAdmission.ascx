﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "StatisticalAnnualAdmission"; %>
<div class="wrapper">
    <fieldset>
        <legend>Annual Admission Patients</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.ReportBranchList("AddressBranchCode", Guid.Empty.ToString(), new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="StatisticalAnnualAdmission_Status" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
            <div class="row"><label  class="float-left">Year:</label><div class="float-right"> <%= Html.Months(pagename + "_Year", DateTime.Now.Year.ToString(), 1900, new { @id = pagename + "_Year", @class = "oe" })%></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindStatisticalAnnualAdmission();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportStatisticalAnnualAdmission", new { BranchId = Guid.Empty, Status = 1,  year = DateTime.Now.Year }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
        <%= Html.Telerik().Grid<PatientRoster>().Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientFirstName).Title("First Name");
               columns.Bound(p => p.PatientLastName).Title("Last Name");
               columns.Bound(p => p.AdmissionSourceName).Title("Admission Source");
               columns.Bound(p => p.PatientSoC).Title("Admission Date").Width(120);
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Status = 1, year = DateTime.Now.Year }))
           .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>

