﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "CensusByPrimaryInsurance"; %>
<div class="wrapper">
    <fieldset>
        <legend>Census By Primary Insurance</legend>
        <div class="column">
            <div class="row"><label  class="float-left">Branch:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Branches, "PatientRoster_BranchCode", "", new { @id = "PatientRoster_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float-left">Insurance:</label><div class="float-right"><%= Html.Insurances(pagename + "_Insurance", "", false, new { @id = pagename + "_Insurance", @class = "Insurances " })%></div></div>
            <div class="row"><label  class="float-left">Status:</label><div class="float-right"><select id="CensusByPrimaryInsurance_Status" name="Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindCensusByPrimaryInsurance();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportCensusByPrimaryInsurance", new { BranchCode = Guid.Empty, Status = 1, insurance = 0 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="report-grid">
         <%= Html.Telerik().Grid<PatientRoster>().Name(pagename + "Grid").Columns(columns =>
            {
                columns.Bound(r => r.PatientDisplayName).Title("Patient");
                columns.Bound(r => r.PatientAddressLine1).Title("Address");
                columns.Bound(r => r.PatientAddressCity).Title("City");
                columns.Bound(r => r.PatientAddressStateCode).Title("State").Width(50);
                columns.Bound(r => r.PatientAddressZipCode).Title("Zip Code").Width(60);
                columns.Bound(r => r.PatientPhone).Title("Home Phone").Width(110);
                columns.Bound(r => r.PatientGender).Title("Gender").Width(60);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchCode = Guid.Empty, Status = 1, insurance = 0 })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>