﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Infections | <%= Current.AgencyName.Clean() %></span>
<% using (Html.BeginForm("Infections", "Export", FormMethod.Post)) { %>
<div class="wrapper">
    <%= Html.Telerik().Grid<Infection>().Name("List_InfectionReport").HtmlAttributes(new { @class = "top-gap bottom-bar" }).ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(i => i.PatientName).Title("Patient Name").Sortable(true);
    columns.Bound(i => i.PhysicianName).Title("Physician").Sortable(true);
    columns.Bound(i => i.InfectionType).Title("Type of Infection").Sortable(true);
    columns.Bound(i => i.InfectionDateFormatted).Title("Infection Date").Sortable(true);
    columns.Bound(i => i.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35).Sortable(false);
    columns.Bound(i => i.Id).ClientTemplate("<a onclick=\"UserInterface.ShowEditInfection('<#=Id#>');return false\">Edit</a>").Title("Action").Width(100);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Infection")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
$("#List_InfectionReport .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageIncidentAccidentInfectionReport)) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Infection Report", Click: UserInterface.ShowNewInfectionReport } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Excel Export", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>