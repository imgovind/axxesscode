﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Infection Report | <%= Current.AgencyName.Clean() %></span>
<% using (Html.BeginForm("Add", "Infection", FormMethod.Post, new { @id = "newInfectionReportForm" })) { %>
<div class="wrapper main">

    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Infection_PatientId" class="float-left">Patient:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "New_Infection_PatientId", @class = "required notzero" })%></div>
            </div><div class="row">
                <label class="float-left">Episode Associated:</label>
                <div class="float-right"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_Infection_EpisodeList", @class = "required notzero" })%></div>
            </div><div class="row">
                <label for="New_Infection_PhysicianId" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianId", "", new { @id = "New_Infection_PhysicianId", @class = "physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
            </div>                
         </div><div class="column">
            <div class="row">
                <label for="New_Infection_InfectionDate" class="float-left">Date of Infection:</label>
                <div class="float-right"><input type="date" name="InfectionDate" id="New_Infection_InfectionDate" class="required" /></div>
            </div><div class="row">
                <label for="New_Infection_TreatmentPrescribedYes" class="float-left">Treatment Prescribed?</label>
                <div class="float-right">
                    <%= Html.RadioButton("TreatmentPrescribed", "Yes", new { @id = "New_Infection_TreatmentPrescribedYes", @class = "radio" })%>
                    <label for="New_Infection_TreatmentPrescribedYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "No", new { @id = "New_Infection_TreatmentPrescribedNo", @class = "radio" })%>
                    <label for="New_Infection_TreatmentPrescribedNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "NA", new { @id = "New_Infection_TreatmentPrescribedNA", @class = "radio" })%>
                    <label for="New_Infection_TreatmentPrescribedNA" class="inline-radio">N/A</label>
                </div>
            </div><div class="row">
                <label for="New_Infection_MDNotifiedYes" class="float-left">M.D. Notified?</label>
                <div class="float-right">
                    <%= Html.RadioButton("MDNotified", "Yes", new { @id = "New_Infection_MDNotifiedYes", @class = "radio" })%>
                    <label for="New_Infection_MDNotifiedYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("MDNotified", "No", new { @id = "New_Infection_MDNotifiedNo", @class = "radio" })%>
                    <label for="New_Infection_MDNotifiedNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("MDNotified", "NA", new { @id = "New_Infection_MDNotifiedNA", @class = "radio" })%>
                    <label for="New_Infection_MDNotifiedNA" class="inline-radio">N/A</label>
                </div>
            </div><div class="row">
                <label for="New_Infection_NewOrdersYes" class="float-left">New Orders?</label>
                <div class="float-right">
                    <%= Html.RadioButton("NewOrdersCreated", "Yes", new { @id = "New_Infection_NewOrdersYes", @class = "radio" })%>
                    <label for="New_Infection_NewOrdersYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("NewOrdersCreated", "No", new { @id = "New_Infection_NewOrdersNo", @class = "radio" })%>
                    <label for="New_Infection_NewOrdersNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("NewOrdersCreated", "NA", new { @id = "New_Infection_NewOrdersNA", @class = "radio" })%>
                    <label for="New_Infection_NewOrdersNA" class="inline-radio">N/A</label>
                </div>
            </div>
         </div>
         <div class="buttons">
            <ul>
                <li><a onclick="Patient.LoadNewOrder('<%= Guid.Empty %>');return false" status="Add New Order">Add New Order</a></li>
            </ul>
        </div>
    </fieldset><fieldset>
        <legend>Type of Infection</legend>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <input id="New_Infection_InfectionType1" type="checkbox" value="Gastrointestinal" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType1" class="radio">Gastrointestinal</label>
                    </div>
                    <div class="option">
                        <input id="New_Infection_InfectionType2" type="checkbox" value="Respiratory" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType2" class="radio">Respiratory</label>
                    </div>
                    <div class="option">
                        <input id="New_Infection_InfectionType3" type="checkbox" value="Skin" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType3" class="radio">Skin</label>
                    </div>
                    <div class="option">
                        <input id="New_Infection_InfectionType4" type="checkbox" value="Wound" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType4" class="radio">Wound</label>
                    </div>
                    <div class="option">
                        <input id="New_Infection_InfectionType5" type="checkbox" value="Urinary" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType5" class="radio">Urinary</label>
                    </div>
                    <div class="option">
                        <input id="New_Infection_InfectionType6" type="checkbox" value="Other" name="InfectionTypeArray" class="radio float-left" />
                        <label for="New_Infection_InfectionType6" class="radio">Other</label>
                        <%= Html.TextBox("InfectionTypeOther", "", new { @id = "New_Infection_InfectionTypeOther", @class = "text input_wrapper", @maxlength = "100" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Treatment</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Treatment" class="strong">Treatment/Antibiotic:</label>
                <%= Html.Templates("TreatmentTemplates", new { @class = "templates", @template = "#New_Infection_Treatment" })%>
                <div><%= Html.TextArea("Treatment", new { @id="New_Infection_Treatment", @class = "taller" })%></div>
            </div><div class="row">
                <label for="Orders" class="strong">Narrative: </label>
                <%= Html.Templates("NarrativeTemplates", new { @class = "templates", @template = "#New_Infection_Narrative" })%>
                <div><%= Html.TextArea("Orders", new { @id = "New_Infection_Narrative", @class = "taller" })%></div>
            </div><div class="row">
                <label for="FollowUp" class="strong">Follow Up: </label>
                <div><%= Html.TextArea("FollowUp", new { @id = "New_Infection_FollowUp", @class = "taller" })%></div>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="New_Infection_ClinicianSignature" class="float-left">Clinician Signature:</label>
                <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "New_Infection_ClinicianSignature" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_Infection_SignatureDate" class="float-left">Date:</label>
                <div class="float-right"><input type="date" name="SignatureDate" id="New_Infection_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_Infection_Status" })%>
    <div class="buttons">
        <ul>
            <li><a onclick="$('#New_Infection_Status').val('515')" class="save">Save</a></li>
            <li><a onclick="$('#New_Infection_Status').val('520')" class="complete">Complete</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    Schedule.loadEpisodeDropDown('New_Infection_EpisodeList', $('#New_Infection_PatientId'));
    $('#New_Infection_PatientId').change(function() { Schedule.loadEpisodeDropDown('New_Infection_EpisodeList', $(this)); });
</script>
<% } %>