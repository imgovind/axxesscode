﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Infection>" %>
<span class="wintitle">Edit Infection Report | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Infection", FormMethod.Post, new { @id = "editInfectionReportForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Infection_Id" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_Infection_PatientId" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_Infection_UserId" })%>
    <%  string[] infectionTypes = Model.InfectionType != null && Model.InfectionType != "" ? Model.InfectionType.Split(';') : null; %>
    <%  if (Model != null) Model.SignatureDate = DateTime.Today; %>

    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Infection_PatientId" class="float-left">Patient:</label>
                <div class="float-right"><%= Model.PatientName %></div>
            </div>
            <div class="row">
                <label class="float-left">Episode Associated:</label>
                <div class="float-right">
        <%  if (Model.EpisodeId.IsEmpty()) { %>
                    <%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "Edit_Infection_EpisodeId", @class = "required notzero" })%>
        <%  } else { %>
                    <%= string.Format("{0}-{1}",Model.EpisodeStartDate,Model.EpisodeEndDate) %>
                    <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_Infection_EpisodeId" })%>
        <%  } %>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Infection_PhysicianId" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianId", Model.PhysicianId != Guid.Empty ? Model.PhysicianId.ToString() : string.Empty, new { @id = "Edit_Infection_PhysicianId", @class = "physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Infection_InfectionDate" class="float-left">Date of Infection:</label>
                <div class="float-right"><input type="date" name="InfectionDate" value="<%= Model.InfectionDate.IsValid() ? Model.InfectionDate.ToShortDateString() : string.Empty %>" id="Edit_Infection_InfectionDate" class="required" /></div>
            </div>
            <div class="row">
                <label for="Edit_Infection_TreatmentPrescribedYes" class="float-left">Treatment Prescribed?</label>
                <div class="float-right">
                    <%= Html.RadioButton("TreatmentPrescribed", "Yes", Model.TreatmentPrescribed == "Yes" ? true : false, new { @id = "Edit_Infection_TreatmentPrescribedYes", @class = "radio" })%>
                    <label for="Edit_Infection_TreatmentPrescribedYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "No", Model.TreatmentPrescribed == "No" ? true : false, new { @id = "Edit_Infection_TreatmentPrescribedNo", @class = "radio" })%>
                    <label for="Edit_Infection_TreatmentPrescribedNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "NA", Model.TreatmentPrescribed == "NA" ? true : false, new { @id = "Edit_Infection_TreatmentPrescribedNA", @class = "radio" })%>
                    <label for="Edit_Infection_TreatmentPrescribedNA" class="inline-radio">N/A</label>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Infection_MDNotifiedYes" class="float-left">M.D. Notified?</label>
                <div class="float-right">
                    <%= Html.RadioButton("MDNotified", "Yes", Model.MDNotified == "Yes" ? true : false, new { @id = "Edit_Infection_MDNotifiedYes", @class = "radio" })%>
                    <label for="Edit_Infection_MDNotifiedYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("MDNotified", "No", Model.MDNotified == "No" ? true : false, new { @id = "Edit_Infection_MDNotifiedNo", @class = "radio" })%>
                    <label for="Edit_Infection_MDNotifiedNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("MDNotified", "NA", Model.MDNotified == "NA" ? true : false, new { @id = "Edit_Infection_MDNotifiedNA", @class = "radio" })%>
                    <label for="Edit_Infection_MDNotifiedNA" class="inline-radio">N/A</label>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Infection_NewOrdersYes" class="float-left">New Orders?</label>
                <div class="float-right">
                    <%= Html.RadioButton("NewOrdersCreated", "Yes", Model.NewOrdersCreated == "Yes" ? true : false, new { @id = "Edit_Infection_NewOrdersYes", @class = "radio" })%>
                    <label for="Edit_Infection_NewOrdersYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("NewOrdersCreated", "No", Model.NewOrdersCreated == "No" ? true : false, new { @id = "Edit_Infection_NewOrdersNo", @class = "radio" })%>
                    <label for="Edit_Infection_NewOrdersNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("NewOrdersCreated", "NA", Model.NewOrdersCreated == "NA" ? true : false, new { @id = "Edit_Infection_NewOrdersNA", @class = "radio" })%>
                    <label for="Edit_Infection_NewOrdersNA" class="inline-radio">N/A</label>
                </div>
            </div>
         </div>
    </fieldset>
    <fieldset>
        <legend>Type of Infection</legend>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='Edit_Infection_InfectionType1' type='checkbox' value='Gastrointestinal' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Gastrointestinal") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType1" class="radio">Gastrointestinal</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Infection_InfectionType2' type='checkbox' value='Respiratory' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Respiratory") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType2" class="radio">Respiratory</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Infection_InfectionType3' type='checkbox' value='Skin' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Skin") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType3" class="radio">Skin</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Infection_InfectionType4' type='checkbox' value='Wound' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Wound") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType4" class="radio">Wound</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Infection_InfectionType5' type='checkbox' value='Urinary' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Urinary") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType5" class="radio">Urinary</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Infection_InfectionType6' type='checkbox' value='Other' name='InfectionTypeArray' class='required radio float-left' {0} />", infectionTypes != null && infectionTypes.Contains("Other") ? "checked='checked'" : "")%>
                        <label for="Edit_Infection_InfectionType6" class="radio">Other (specify)</label>
                        <%= Html.TextBox("InfectionTypeOther", Model.InfectionTypeOther, new { @id = "Edit_Infection_InfectionTypeOther", @class = "text input_wrapper", @maxlength = "100" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Treatment</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Treatment" class="strong">Treatment/Antibiotic:</label>
                <%= Html.Templates("TreatmentTemplates", new { @class = "templates", @template = "#Edit_Infection_Treatment" })%>
                <div><%= Html.TextArea("Treatment", Model.Treatment, new { @id="Edit_Infection_Treatment", @class = "taller" })%></div>
            </div><div class="row">
                <label for="Orders" class="strong">Narrative: </label>
                <%= Html.Templates("NarrativeTemplates", new { @class = "templates", @template = "#Edit_Infection_Narrative" })%>
                <div><%= Html.TextArea("Orders", Model.Orders, new { @id = "Edit_Infection_Narrative", @class = "taller" })%></div>
            </div><div class="row">
                <label for="FollowUp" class="strong">Follow Up: </label>
                <div><%= Html.TextArea("FollowUp", Model.FollowUp, new { @id = "Edit_Infection_FollowUp", @class = "taller" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="Edit_Infection_ClinicianSignature" class="float-left">Clinician Signature:</label>
                <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "Edit_Infection_ClinicianSignature" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Infection_SignatureDate" class="float-left">Date:</label>
                <div class="float-right"><input type="date" name="SignatureDate" id="Edit_Infection_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "Edit_Infection_Status" })%>
    <div class="buttons">
        <ul>
            <li><a onclick="$('#Edit_Infection_Status').val('515')" class="save">Save</a></li>
            <li><a onclick="$('#Edit_Infection_Status').val('520')" class="save">Complete</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>