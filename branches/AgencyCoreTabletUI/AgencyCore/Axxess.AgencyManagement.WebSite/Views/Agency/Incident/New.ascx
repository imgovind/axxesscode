﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Incident/Accident Report | <%= Current.AgencyName.Clean() %></span>
<%  using (Html.BeginForm("Add", "Incident", FormMethod.Post, new { @id = "newIncidentReportForm" })) { %>
<div class="wrapper main">

    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Incident_PatientId" class="float-left">Patient:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "New_Incident_PatientId", @class="required notzero" })%></div>
            </div><div class="row">
                <label class="float-left">Episode Associated:</label>
                <div class="float-right"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_Incident_EpisodeList", @class = "required notzero" })%></div>
            </div><div class="row">
                <label for="New_Incident_PhysicianId" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("PhysicianId", "", new { @id = "New_Incident_PhysicianId", @class = "physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right button-with-arrow">
                    <a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a>
                </div>
            </div>
         </div><div class="column">
            <div class="row">
                <label for="New_Incident_IncidentDate" class="float-left">Date of Incident:</label>
                <div class="float-right"><input type="date" name="IncidentDate" id="New_Incident_IncidentDate" class="required" /></div>
            </div><div class="row">
                <label for="New_Incident_IncidentType" class="float-left">Type of Incident:</label>
                <div class="float-right"><%= Html.TextBox("IncidentType", "", new { @id = "New_Incident_IncidentType", @class = "text input_wrapper required", @maxlength = "100" })%></div>
            </div><div class="row">
                <label for="New_Incident_MDNotifiedYes" class="float-left">M.D. Notified?</label>
                <div class="float-right">
                    <%= Html.RadioButton("MDNotified", "Yes", new { @id = "New_Incident_MDNotifiedYes", @class = "radio" })%>
                    <label for="New_Incident_MDNotifiedYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("MDNotified", "No", new { @id = "New_Incident_MDNotifiedNo", @class = "radio" })%>
                    <label for="New_Incident_MDNotifiedNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("MDNotified", "NA", new { @id = "New_Incident_MDNotifiedNA", @class = "radio", @checked = "checked" })%>
                    <label for="New_Incident_MDNotifiedNA" class="inline-radio">N/A</label>
                </div>
            </div><div class="row">
                <label for="New_Incident_FamilyNotifiedYes" class="float-left">Family/CG Notified?</label>
                <div class="float-right">
                    <%= Html.RadioButton("FamilyNotified", "Yes", new { @id = "New_Incident_FamilyNotifiedYes", @class = "radio" })%>
                    <label for="New_Incident_FamilyNotifiedYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("FamilyNotified", "No", new { @id = "New_Incident_FamilyNotifiedNo", @class = "radio" })%>
                    <label for="New_Incident_FamilyNotifiedNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("FamilyNotified", "NA", new { @id = "New_Incident_FamilyNotifiedNA", @class = "radio", @checked = "checked" })%>
                    <label for="New_Incident_FamilyNotifiedNA" class="inline-radio">N/A</label>
                </div>
            </div><div class="row">
                <label for="New_Incident_NewOrdersYes" class="float-left">New Orders?</label>
                <div class="float-right">
                    <%= Html.RadioButton("NewOrdersCreated", "Yes", new { @id = "New_Incident_NewOrdersYes", @class = "radio" })%>
                    <label for="New_Incident_NewOrdersYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton("NewOrdersCreated", "No", new { @id = "New_Incident_NewOrdersNo", @class = "radio" })%>
                    <label for="New_Incident_NewOrdersNo" class="inline-radio">No</label>
                    <%= Html.RadioButton("NewOrdersCreated", "NA", new { @id = "New_Incident_NewOrdersNA", @class = "radio", @checked = "checked" })%>
                    <label for="New_Incident_NewOrdersNA" class="inline-radio">N/A</label>
                </div>
            </div>
         </div><div class="buttons">
            <ul>
                <li><a onclick="Patient.LoadNewOrder('<%= Guid.Empty %>');return false" status="Add New Order">Add New Order</a></li>
            </ul>
        </div>
    </fieldset>
    <fieldset>
        <legend>Individual(s) involved:</legend>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <input id="New_Incident_IndividualInvolved1" type="checkbox" value="Patient" name="IndividualInvolvedArray" class="radio float-left" />
                        <label for="New_Incident_IndividualInvolved1" class="radio">Patient</label>
                    </div>
                    <div class="option">
                        <input id="New_Incident_IndividualInvolved2" type="checkbox" value="Caregiver" name="IndividualInvolvedArray" class="radio float-left" />
                        <label for="New_Incident_IndividualInvolved2" class="radio">Caregiver</label>
                    </div>
                    <div class="option">
                        <input id="New_Incident_IndividualInvolved3" type="checkbox" value="Employee/Contractor" name="IndividualInvolvedArray" class="radio float-left" />
                        <label for="New_Incident_IndividualInvolved3" class="radio">Employee/Contractor</label>
                    </div>
                    <div class="option">
                        <input id="New_Incident_IndividualInvolved4" type="checkbox" value="Other" name="IndividualInvolvedArray" class="radio float-left" />
                        <label for="New_Incident_IndividualInvolved4" class="radio">Other</label>
                        <%= Html.TextBox("IndividualInvolvedOther", "", new { @id = "New_Incident_IndividualInvolvedOther", @class = "text input_wrapper", @maxlength = "100" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Description</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Description" class="strong">Describe Incident/Accident:</label>
                <%= Html.Templates("DescriptionTemplates", new { @class = "templates", @template = "#New_Incident_Description" })%>
                <div><%= Html.TextArea("Description", new { @id = "New_Incident_Description", @class = "taller" })%></div>
            </div>
            <div class="row">
                <label for="ActionTaken" class="strong">Action Taken/Interventions Performed:</label>
                <%= Html.Templates("ActionTakenTemplates", new { @class = "templates", @template = "#New_Incident_ActionTaken" })%>
                <div><%= Html.TextArea("ActionTaken", new { @id = "New_Incident_ActionTaken", @class = "taller" })%></div>
            </div>
            <div class="row">
                <label for="NarrativeTemplates" class="strong">Narrative: </label>
                <%= Html.Templates("NarrativeTemplates", new { @class = "templates", @template = "#New_Incident_Narrative" })%>
                <div><%= Html.TextArea("Orders", new { @id = "New_Incident_Narrative", @class = "taller" })%></div>
            </div>
            <div class="row">
                <label for="FollowUp" class="strong">Follow Up: </label>
                <div><%= Html.TextArea("FollowUp", new { @id = "New_Incident_FollowUp", @class = "taller" })%></div>
            </div>
        </div>  
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="New_Incident_ClinicianSignature" class="float-left">Clinician Signature:</label>
                <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "New_Incident_ClinicianSignature" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_Incident_SignatureDate" class="float-left">Date:</label>
                <div class="float-right"><input type="date" name="SignatureDate" id="New_Incident_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_Incident_Status" })%>
    <div class="buttons">
        <ul>
            <li><a onclick="$('#New_Incident_Status').val('515')" class="save">Save</a></li>
            <li><a onclick="$('#New_Incident_Status').val('520')" class="save">Complete</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    Schedule.loadEpisodeDropDown('New_Incident_EpisodeList', $('#New_Incident_PatientId'));
    $('#New_Incident_PatientId').change(function() { Schedule.loadEpisodeDropDown('New_Incident_EpisodeList', $(this)); });
</script>
<% } %>