﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Incident>" %>
<span class="wintitle">Edit Incident/Accident Report | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Incident", FormMethod.Post, new { @id = "editIncidentReportForm" })) { %>
    <%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Incident_Id" })%>
    <%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_Incident_PatientId" })%>
    <%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_Incident_UserId" })%>
    <%  if (Model != null) Model.SignatureDate = DateTime.Today; %>

    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Incident_PatientId" class="float-left">Patient Name:</label>
                <div class="float-right"><%= Model.PatientName %></div>
            </div>
            <div class="row">
                <label class="float-left">Episode Associated:</label>
                <div class="float-right">
        <% if (Model.EpisodeId.IsEmpty()) { %>
                    <%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "Edit_Incident_EpisodeId", @class = "required notzero" })%>
        <%  } else { %>
                    <%= string.Format("{0}-{1}",Model.EpisodeStartDate,Model.EpisodeEndDate) %>
                    <%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_Incident_EpisodeId" })%>
        <%  } %>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Incident_PhysicianId" class="float-left">Physician:</label>
                <div class="float-right">
                    <%= Html.TextBox("PhysicianId", Model.PhysicianId.ToString(), new { @id = "Edit_Incident_PhysicianId", @class = "physicians" })%>
                </div>
                <div class="clear"></div>
                <div class="float-right button-with-arrow">
                    <a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Incident_IncidentDate" class="float-left">Date of Incident:</label>
                <div class="float-right"><input type="date" name="IncidentDate" id="Edit_Incident_IncidentDate" value="<%= Model.IncidentDate.IsValid() ? Model.IncidentDate.ToShortDateString() : string.Empty %>" class="required" /></div>
            </div>
            <div class="row">
                <label for="Edit_Incident_IncidentType" class="float-left">Type of Incident:</label>
                <div class="float-right"><%= Html.TextBox("IncidentType", Model.IncidentType, new { @id = "Edit_Incident_IncidentType", @class = "text input_wrapper required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Incident_MDNotifiedYes" class="float-left">M.D. Notified?</label>
                <div class="float-right"><%= Html.RadioButton("MDNotified", "Yes", Model.MDNotified == "Yes" ? true : false, new { @id = "Edit_Incident_MDNotifiedYes", @class = "radio" })%><label for="Edit_Incident_MDNotifiedYes" class="inline-radio">Yes</label><%= Html.RadioButton("MDNotified", "No", Model.MDNotified == "No" ? true : false, new { @id = "Edit_Incident_MDNotifiedNo", @class = "radio" })%><label for="Edit_Incident_MDNotifiedNo" class="inline-radio">No</label><%= Html.RadioButton("MDNotified", "NA", Model.MDNotified == "NA" ? true : false, new { @id = "Edit_Incident_MDNotifiedNA", @class = "radio", @checked = "checked" })%><label for="Edit_Incident_MDNotifiedNA" class="inline-radio">N/A</label></div>
            </div>
            <div class="row">
                <label for="Edit_Incident_FamilyNotifiedYes" class="float-left">Family/CG Notified ?</label>
                <div class="float-right"><%= Html.RadioButton("FamilyNotified", "Yes", Model.FamilyNotified == "Yes" ? true : false, new { @id = "Edit_Incident_FamilyNotifiedYes", @class = "radio" })%><label for="Edit_Incident_FamilyNotifiedYes" class="inline-radio">Yes</label><%= Html.RadioButton("FamilyNotified", "No", Model.FamilyNotified == "No" ? true : false, new { @id = "Edit_Incident_FamilyNotifiedNo", @class = "radio" })%><label for="Edit_Incident_FamilyNotifiedNo" class="inline-radio">No</label><%= Html.RadioButton("FamilyNotified", "NA", Model.FamilyNotified == "NA" ? true : false, new { @id = "Edit_Incident_FamilyNotifiedNA", @class = "radio", @checked = "checked" })%><label for="Edit_Incident_FamilyNotifiedNA" class="inline-radio">N/A</label></div>
            </div>
            <div class="row">
                <label for="Edit_Incident_NewOrdersYes" class="float-left">New Orders?</label>
                <div class="float-right"><%= Html.RadioButton("NewOrdersCreated", "Yes", Model.NewOrdersCreated == "Yes" ? true : false, new { @id = "Edit_Incident_NewOrdersYes", @class = "radio" })%><label for="Edit_Incident_NewOrdersYes" class="inline-radio">Yes</label><%= Html.RadioButton("NewOrdersCreated", "No", Model.NewOrdersCreated == "No" ? true : false, new { @id = "Edit_Incident_NewOrdersNo", @class = "radio" })%><label for="Edit_Incident_NewOrdersNo" class="inline-radio">No</label><%= Html.RadioButton("NewOrdersCreated", "NA", Model.NewOrdersCreated == "NA" ? true : false, new { @id = "Edit_Incident_NewOrdersNA", @class = "radio", @checked = "checked" })%><label for="Edit_Incident_NewOrdersNA" class="inline-radio">N/A</label></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Individual(s) involved:</legend>
        <div class="wide-column">
            <div class="row">
                <%  string[] individualsInvolved = Model.IndividualInvolved != null && Model.IndividualInvolved != "" ? Model.IndividualInvolved.Split(';') : null;  %>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='Edit_Incident_IndividualInvolved1' type='checkbox' value='Patient' name='IndividualInvolvedArray' class='required radio float-left' {0} />", individualsInvolved != null && individualsInvolved.Contains("Patient") ? "checked='checked'" : "")%>
                        <label for="Edit_Incident_IndividualInvolved1" class="radio">Patient</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Incident_IndividualInvolved2' type='checkbox' value='Caregiver' name='IndividualInvolvedArray' class='required radio float-left' {0} />", individualsInvolved != null && individualsInvolved.Contains("Caregiver") ? "checked='checked'" : "")%>
                        <label for="Edit_Incident_IndividualInvolved2" class="radio">Caregiver</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Incident_IndividualInvolved3' type='checkbox' value='Employee/Contractor' name='IndividualInvolvedArray' class='required radio float-left' {0} />", individualsInvolved != null && individualsInvolved.Contains("Employee/Contractor") ? "checked='checked'" : "")%>
                        <label for="Edit_Incident_IndividualInvolved3" class="radio">Employee/Contractor</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='Edit_Incident_IndividualInvolved4' type='checkbox' value='Other' name='IndividualInvolvedArray' class='required radio float-left' {0} />", individualsInvolved != null && individualsInvolved.Contains("Other") ? "checked='checked'" : "")%>
                        <label for="Edit_Incident_IndividualInvolved4" class="radio">Other (specify) &#160;</label>
                        <%= Html.TextBox("IndividualInvolvedOther", Model.IndividualInvolvedOther, new { @id = "Edit_Incident_IndividualInvolvedOther", @class = "text input_wrapper", @maxlength = "100" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Description</legend>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_Incident_Description" class="strong">Describe Incident/Accident:</label>
                <%= Html.Templates("DescriptionTemplates", new { @class = "templates", @template = "#Edit_Incident_Description" })%>
                <div><%= Html.TextArea("Description", Model.Description, new { @id = "Edit_Incident_Description", @class = "taller" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Incident_ActionTaken" class="strong">Action Taken/Interventions Performed:</label>
                <%= Html.Templates("ActionTakenTemplates", new { @class = "templates", @template = "#Edit_Incident_ActionTaken" })%>
                <div><%= Html.TextArea("ActionTaken", Model.ActionTaken, new { @id = "Edit_Incident_ActionTaken", @class = "taller" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Incident_Narrative" class="strong">Narrative: </label>
                <%= Html.Templates("NarrativeTemplates", new { @class = "templates", @template = "#Edit_Incident_Narrative" })%>
                <div><%= Html.TextArea("Orders", Model.Orders, new { @id = "Edit_Incident_Narrative", @class = "taller" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Incident_FollowUp" class="strong">Follow Up: </label>
                <div><%= Html.TextArea("FollowUp", Model.FollowUp, new { @id = "Edit_Incident_FollowUp", @class = "taller" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="Edit_Incident_ClinicianSignature" class="float-left">Clinician Signature:</label>
                <div class="float-right"><%= Html.Password("SignatureText", "", new { @id = "Edit_Incident_ClinicianSignature" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Incident_SignatureDate" class="float-left">Date:</label>
                <div class="float-right"><input type="date" name="SignatureDate" id="Edit_Incident_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "Edit_Incident_Status" })%>
    <div class="buttons"><ul>
        <li><a onclick="$('#Edit_Incident_Status').val('515')" class="save">Save</a></li>
        <li><a onclick="$('#Edit_Incident_Status').val('520')" class="save">Complete</a></li>
        <li><a class="close">Cancel</a></li>
    </ul></div>
<%  } %>
</div>