﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<span class="wintitle">Edit Visit Rates | <%= Current.AgencyName.Clean() %></span>
<% using (Html.BeginForm("EditCost", "Agency", FormMethod.Post, new { @id = "editVisitCostForm" })){ %>
<div class="wrapper main">

    <fieldset>
        <div class="column">
            <div class="row">
                <label for="Edit_VisitRate_LocationId" class="float-left">Agency Branch:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.Id.ToString(), new { @id = "Edit_VisitRate_LocationId", @class = "BranchLocation required" }) %></div>
            </div>
        </div>
        <div class="clear"></div>
        <div id="Edit_VisitRate_Container"><% Html.RenderPartial("~/Views/Agency/VisitRateContent.ascx", Model); %></div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $('#Edit_VisitRate_LocationId').change(function() {
        Agency.loadVisitRateContent($(this).val());
    });
</script>