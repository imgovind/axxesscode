﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyTemplate>" %>
<span class="wintitle">Edit Template | <%= Current.AgencyName.Clean() %></span>
<%  using (Html.BeginForm("Update", "Template", FormMethod.Post, new { @id = "editTemplateForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Template_Id" }) %>
<div class="wrapper main">

    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_Template_Title" class="strong">Name</label>
                <br />
                <%= Html.TextBox("Title", Model.Title, new { @id = "Edit_Template_Title", @class = "required longest", @maxlength = "100" })%>
            </div>
            <div class="row">
                <label for="Edit_Template_Text" class="strong">Text</label>
                <div class="ac">
                    <textarea id="Edit_Template_Text" name="Text" cols="5" rows="6" class="tallest"  maxcharacters="5000"><%= Model.Text %></textarea>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<span class=\"img icon activity\" onclick=\"Log.LoadTemplateLog('{0}')\" title=\"Activity Logs\"></span>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a class="save">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul></div>
</div>
<% } %>