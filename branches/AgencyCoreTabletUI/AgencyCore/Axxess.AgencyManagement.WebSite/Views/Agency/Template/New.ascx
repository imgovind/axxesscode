﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Template | <%= Current.AgencyName.Clean() %></span>
<% using (Html.BeginForm("Add", "Template", FormMethod.Post, new { @id = "newTemplateForm" })) { %>
<div class="wrapper main">

    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="New_Template_Title" class="strong">Name</label>
                <br />
                <%= Html.TextBox("Title", "", new { @id = "New_Template_Title", @class = "required longest", @maxlength = "100" })%>
            </div>
            <div class="row">
                <label for="New_Template_Text" class="strong">Text</label>
                <div class="ac">
                    <textarea id="New_Template_Text" name="Text" cols="5" rows="6" maxcharacters="5000" class="tallest"></textarea>
                </div>
            </div>
       </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a class="save">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul></div>
</div>
<%} %>
