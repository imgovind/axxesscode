﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Contact | <%= Current.AgencyName.Clean() %></span>
<%  using (Html.BeginForm("Contacts", "Export", FormMethod.Post)) { %>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyContact>().Name("List_Contact").HtmlAttributes(new { @class = "top-gap bottom-bar" }).ToolBar(commnds => commnds.Custom()).Columns(columns => {
            columns.Bound(c => c.DisplayName).Title("Name").Width(150).Sortable(true);
            columns.Bound(c => c.CompanyName).Title("Company").Sortable(true);
            columns.Bound(c => c.ContactType).Title("Type").Width(150).Sortable(true);
            columns.Bound(c => c.PhonePrimaryFormatted).Title("Phone").Width(120).Sortable(false);
            columns.Bound(c => c.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(150).Sortable(true);
            columns.Bound(c => c.Id).ClientTemplate("<a onclick=\"UserInterface.ShowEditContact('<#=Id#>');return false\">Edit</a> | <a onclick=\"Contact.Delete('<#=Id#>');return false\" class=\"deleteContact\">Delete</a>").Title("Action").Sortable(false).Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Contact")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<% } %>
<script type="text/javascript">
$("#List_Contact .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageContact)) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Contact", Click: UserInterface.ShowNewContact } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Excel Export", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>