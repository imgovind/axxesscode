﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<% var data = Model != null ? Model.ToCostRateDictionary() : new Dictionary<string, CostRate>(); %>
<div class="column">
    <div class="row">
        <div class="float-left strong">Disciplines</div>
        <div class="ac float-right">
            Cost Per Visit
            <br />
            <em>(Employee Pay Rates)</em>
        </div>
    </div>
    <div class="row">
        <label for="New_Insurance_SkilledNursePerUnit" class="float-left">Skilled Nurse</label>
        <%= Html.Hidden("RateDiscipline", "SkilledNurse") %>
        <div class="float-right"><%= Html.TextBox("SkilledNurse_PerUnit", data.ContainsKey("SkilledNurse") ? data["SkilledNurse"].PerUnit : string.Empty, new { @id = "New_Insurance_SkilledNursePerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_SkilledNurseTeachingPerUnit" class="float-left">Skilled Nurse (Teaching)</label>
        <%= Html.Hidden("RateDiscipline", "SkilledNurseTeaching") %>
        <div class="float-right"><%= Html.TextBox("SkilledNurseTeaching_PerUnit", data.ContainsKey("SkilledNurseTeaching") ? data["SkilledNurseTeaching"].PerUnit : string.Empty, new { @id = "New_Insurance_SkilledNurseTeachingPerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_SkilledNurseObservationPerUnit" class="float-left">Skilled Nurse (Observation)</label>
        <%= Html.Hidden("RateDiscipline", "SkilledNurseObservation") %>
        <div class="float-right"><%= Html.TextBox("SkilledNurseObservation_PerUnit", data.ContainsKey("SkilledNurseObservation") ? data["SkilledNurseObservation"].PerUnit : string.Empty, new { @id = "New_Insurance_SkilledNurseObservationPerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_SkilledNurseManagementPerUnit" class="float-left">Skilled Nurse (Management)</label>
        <%= Html.Hidden("RateDiscipline", "SkilledNurseManagement") %>
        <div class="float-right"><%= Html.TextBox("SkilledNurseManagement_PerUnit", data.ContainsKey("SkilledNurseManagement") ? data["SkilledNurseManagement"].PerUnit : string.Empty, new { @id = "New_Insurance_SkilledNurseManagementPerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_PhysicalTherapyPerUnit" class="float-left">Physical Therapy</label>
        <%= Html.Hidden("RateDiscipline", "PhysicalTherapy")%>
        <div class="float-right"><%= Html.TextBox("PhysicalTherapy_PerUnit", data.ContainsKey("PhysicalTherapy") ? data["PhysicalTherapy"].PerUnit : string.Empty, new { @id = "New_Insurance_PhysicalTherapyPerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_PhysicalTherapyAssistancePerUnit" class="float-left">Physical Therapy Assistant</label>
        <%= Html.Hidden("RateDiscipline", "PhysicalTherapyAssistance")%>
        <div class="float-right"><%= Html.TextBox("PhysicalTherapyAssistance_PerUnit", data.ContainsKey("PhysicalTherapyAssistance") ? data["PhysicalTherapyAssistance"].PerUnit : string.Empty, new { @id = "New_Insurance_PhysicalTherapyAssistancePerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_PhysicalTherapyMaintenancePerUnit" class="float-left">Physical Therapy (Maintenance)</label>
        <%= Html.Hidden("RateDiscipline", "PhysicalTherapyMaintenance")%>
        <div class="float-right"><%= Html.TextBox("PhysicalTherapyMaintenance_PerUnit", data.ContainsKey("PhysicalTherapyMaintenance") ? data["PhysicalTherapyMaintenance"].PerUnit : string.Empty, new { @id = "New_Insurance_PhysicalTherapyMaintenancePerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_OccupationalTherapyPerUnit" class="float-left">Occupational Therapy</label>
        <%= Html.Hidden("RateDiscipline", "OccupationalTherapy")%>
        <div class="float-right"><%= Html.TextBox("OccupationalTherapy_PerUnit", data.ContainsKey("OccupationalTherapy") ? data["OccupationalTherapy"].PerUnit : string.Empty, new { @id = "New_Insurance_OccupationalTherapyPerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_OccupationalTherapyAssistancePerUnit" class="float-left">Occupational Therapy Assistant</label>
        <%= Html.Hidden("RateDiscipline", "OccupationalTherapyAssistance")%>
        <div class="float-right"><%= Html.TextBox("OccupationalTherapyAssistance_PerUnit", data.ContainsKey("OccupationalTherapyAssistance") ? data["OccupationalTherapyAssistance"].PerUnit : string.Empty, new { @id = "New_Insurance_OccupationalTherapyAssistancePerUnit", @class = "rates currency shorter" })%></div>
    </div>
</div>
<div class="column">
    <div class="row">
        <div class="float-left strong">Disciplines</div>
        <div class="ac float-right">
            Cost Per Visit
            <br />
            <em>(Employee Pay Rates)</em>
        </div>
    </div>
    <div class="row">
        <label for="New_Insurance_OccupationalTherapyMaintenancePerUnit" class="float-left">Occupational Therapy (Maintenance)</label>
        <%= Html.Hidden("RateDiscipline", "OccupationalTherapyMaintenance")%>
        <div class="float-right"><%= Html.TextBox("OccupationalTherapyMaintenance_PerUnit", data.ContainsKey("OccupationalTherapyMaintenance") ? data["OccupationalTherapyMaintenance"].PerUnit : string.Empty, new { @id = "New_Insurance_OccupationalTherapyMaintenancePerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_SpeechTherapyPerUnit" class="float-left">Speech Therapy</label>
        <%= Html.Hidden("RateDiscipline", "SpeechTherapy")%>
        <div class="float-right"><%= Html.TextBox("SpeechTherapy_PerUnit", data.ContainsKey("SpeechTherapy") ? data["SpeechTherapy"].PerUnit : string.Empty, new { @id = "New_Insurance_SpeechTherapyPerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_SpeechTherapyMaintenancePerUnit" class="float-left">Speech Therapy (Maintenance)</label>
        <%= Html.Hidden("RateDiscipline", "SpeechTherapyMaintenance")%>
        <div class="float-right"><%= Html.TextBox("SpeechTherapyMaintenance_PerUnit", data.ContainsKey("SpeechTherapyMaintenance") ? data["SpeechTherapyMaintenance"].PerUnit : string.Empty, new { @id = "New_Insurance_SpeechTherapyMaintenancePerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_MedicareSocialWorkerPerUnit" class="float-left">Medicare Social Worker</label>
        <%= Html.Hidden("RateDiscipline", "MedicareSocialWorker")%>
        <div class="float-right"><%= Html.TextBox("MedicareSocialWorker_PerUnit", data.ContainsKey("MedicareSocialWorker") ? data["MedicareSocialWorker"].PerUnit : string.Empty, new { @id = "New_Insurance_MedicareSocialWorkerPerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_HomeHealthAidePerUnit" class="float-left">Home Health Aide</label>
        <%= Html.Hidden("RateDiscipline", "HomeHealthAide")%>
        <div class="float-right"><%= Html.TextBox("HomeHealthAide_PerUnit", data.ContainsKey("HomeHealthAide") ? data["HomeHealthAide"].PerUnit : string.Empty, new { @id = "New_Insurance_HomeHealthAidePerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_AttendantPerUnit" class="float-left">Attendant</label>
        <%= Html.Hidden("RateDiscipline", "Attendant")%>
        <div class="float-right"><%= Html.TextBox("Attendant_PerUnit", data.ContainsKey("Attendant") ? data["Attendant"].PerUnit : string.Empty, new { @id = "New_Insurance_AttendantPerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_CompanionCarePerUnit" class="float-left">Companion Care</label>
        <%= Html.Hidden("RateDiscipline", "CompanionCare")%>
        <div class="float-right"><%= Html.TextBox("CompanionCare_PerUnit", data.ContainsKey("CompanionCare") ? data["CompanionCare"].PerUnit : string.Empty, new { @id = "New_Insurance_CompanionCarePerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_HomemakerServicesPerUnit" class="float-left">Homemaker Services</label>
        <%= Html.Hidden("RateDiscipline", "HomemakerServices")%>
        <div class="float-right"><%= Html.TextBox("HomemakerServices_PerUnit", data.ContainsKey("HomemakerServices") ? data["HomemakerServices"].PerUnit : string.Empty, new { @id = "New_Insurance_HomemakerServicesPerUnit", @class = "rates currency shorter" })%></div>
    </div>
    <div class="row">
        <label for="New_Insurance_PrivateDutySitterPerUnit" class="float-left">Private Duty Sitter</label>
        <%= Html.Hidden("RateDiscipline", "PrivateDutySitter")%>
        <div class="float-right"><%= Html.TextBox("PrivateDutySitter_PerUnit", data.ContainsKey("PrivateDutySitter") ? data["PrivateDutySitter"].PerUnit : string.Empty, new { @id = "New_Insurance_PrivateDutySitterPerUnit", @class = "rates currency shorter" })%></div>
    </div>
</div>