﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<span class="wintitle">Quality Assurance (QA) Center | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
<%  using (Html.BeginForm("BulkUpdate", "Schedule", FormMethod.Post, new { @id = "caseManagementForm" })) { %>
    <div class="float-right">
        <div class="buttons">
            <ul><li><a onclick="Agency.loadCaseManagement('<%=ViewData["GroupName"]%>');return false">Refresh</a></li></ul>
            <br />
            <ul><li><%= Html.ActionLink("Export to Excel", "QAScheduleList", "Export", new { BranchId = ViewData["BranchId"], Status = 1 }, new { id = "CaseManagement_ExportLink" })%></li></ul>
        </div>
    </div>
    <fieldset class="grid-controls">
        <label class="float-left">Branch:</label>
        <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = "CaseManagement_BranchCode" })%></div>
        <label class="float-left">Status:</label>
        <div class="float-left">
            <select id="CaseManagement_Status" name="StatusId" class="PatientStatusDropDown">
                <option value="0">All</option>
                <option value="1" selected>Active</option>
                <option value="2">Discharged</option>
            </select>
        </div>
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Agency.loadCaseManagement('<%=ViewData["GroupName"]%>');return false">Generate</a></li>
            </ul>
        </div>
        <div class="buttons">
            <ul>
                <li><a onclick="Agency.loadCaseManagement('PatientName');return false">Group By Patient</a></li>
                <li><a onclick="Agency.loadCaseManagement('EventDate');return false">Group By Date</a></li>
                <li><a onclick="Agency.loadCaseManagement('DisciplineTaskName');return false">Group By Task</a></li>
                <li><a onclick="Agency.loadCaseManagement('UserName');return false">Group By Clinician</a></li>
            </ul>
        </div>
    </fieldset>
    <div id="caseManagementContentId"><% Html.RenderPartial("~/Views/Agency/QA/CaseManagementContent.ascx", Model); %></div>
    <div class="buttons abs-bottom wrapper">
        <%= Html.Hidden("CommandType", "", new {@id="BulkUpdate_Type" })%>
        <ul>
            <li><a onclick="BulkUpdate('Approve');return false">Approve Selected</a></li>
            <li><a onclick="BulkUpdate('Return');return false">Return Selected</a></li>
        </ul>
    </div>
<% } %>
</div>
<script type="text/javascript">
    function BulkUpdate(type) {
        if ($("input[name=CustomValue]:checked").length > 0) {
            $("#BulkUpdate_Type").val(type);
            Schedule.BulkUpdate('#caseManagementForm');
        } else U.Growl("Select at least one item to approve or return.", "error");
    }
</script>