﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Orders To Be Sent | <%= Current.AgencyName.Clean() %></span>
<div id="List_OrdersToBeSent_Container" class="wrapper main blue">
    <div class="float-right">
        <div class="buttons">
            <ul><li><a id="List_OrdersToBeSent_SendButton" onclick="Agency.MarkOrdersAsSent('#List_OrdersToBeSent_Container');return false">Send Electronically</a></li></ul>
            <br />
            <ul><li><%= Html.ActionLink("Excel Export", "OrdersToBeSent", "Export", new { BranchId = Guid.Empty, sendAutomatically = true, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "OrdersToBeSent_ExportLink", @class = "excel" })%></li></ul>
        </div>
    </div>
    <fieldset class="grid-controls">
        <label class="float-left">Branch:</label><div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "OrdersToBeSent_BranchId" })%></div>
        <label class="strong" for="OrdersToBeSent_StartDate">Date Range:</label><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="OrdersToBeSent_StartDate" class="short" />
        <label class="strong" for="OrdersToBeSent_EndDate">To</label><input type="date" name="EndtDate" value="<%= DateTime.Now.ToShortDateString() %>" id="OrdersToBeSent_EndDate" class="short" />
        <div class="buttons float-right"><ul><li><a onclick="Agency.RebindOrdersToBeSent();return false">Generate</a></li></ul></div><div class="clear"></div>
        <label class="strong" for="List_OrdersToBeSent_SendType">Filter by:</label><select id="List_OrdersToBeSent_SendType" class="SendAutomatically" name="SendAutomatically"><option value="true">Electronic Orders</option><option value="false">Manual Orders (Fax, Mail, etc)</option></select>
        <div id="OrdersToBeSent_Search" class="fl"></div>
    </fieldset>
    <%= Html.Telerik().Grid<Order>().Name("List_OrdersToBeSent").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
        columns.Bound(o => o.Number).Title("Order").Width(80).Sortable(true);
	    columns.Bound(o => o.Type).Title(" ").ClientTemplate("<input type='checkbox' class='OrdersToBeSent' name='<#=Type#>' value='<#=Id#>' />").Width(35).Sortable(false);
    	columns.Bound(o => o.Number).Title("Order").Width(80).Sortable(true);
	    columns.Bound(o => o.PatientName).Title("Patient").Sortable(true);
    	columns.Bound(o => o.Text).Title("Type").Sortable(true);
	    columns.Bound(o => o.PhysicianName).Title("Physician").Sortable(true);
    	columns.Bound(o => o.CreatedDate).Title("Order Date").Width(100).Sortable(true);
	    columns.Bound(o => o.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35).Sortable(false);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersToBeSent", "Agency", new { BranchId = Guid.Empty, sendAutomatically = true, StartDate = DateTime.Now.AddDays(-59).ToShortDateString(), EndDate = DateTime.Now.ToShortDateString() })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>
<script type="text/javascript">
    $("#OrdersToBeSent_Search").append(
        $("<div/>").GridSearchById("#List_OrdersToBeSent")
    );
    $("#List_OrdersToBeSent_SendType").change(function() {
        Agency.RebindOrdersToBeSent();
        if ($(this).val() == 'true') $("#List_OrdersToBeSent_SendButton").text("Send Electronically");
        else $("#List_OrdersToBeSent_SendButton").text("Mark as Sent to Physician");
    });
</script>