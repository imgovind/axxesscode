﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Order>" %>
<% using (Html.BeginForm("EditOrders", "Agency", FormMethod.Post, new { @id = "updateOrderHistry" })) { %>
<div class="form_wrapper">
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("Type", Model.Type) %>
    <fieldset>
        <legend>Update <%= Model.Text %></legend>
        <div class="wide-column">
            <div class="row">
                <label class="float-left">Sent Date:</label>
                <div class="float-right"><input type="date" name="SendDate" value="<%= Model.SendDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.SendDate.ToShortDateString() %>" /></div>
            </div><div class="row">
                <label class="float-left">Received Date:</label>
                <div class="float-right"><input type="date" name="ReceivedDate" value="<%= Model.ReceivedDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.ReceivedDate.ToShortDateString() %>" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Update</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<% } %>