﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Order History | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    <div class="float-right">
        <div class="buttons">
            <ul>
                <li><%= Html.ActionLink("Excel Export", "OrdersHistory", "Export", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "OrdersHistory_ExportLink", @class = "excel" })%></li>
            </ul>
        </div>
    </div>
<%  } %>
    <fieldset class="grid-controls">
        <label class="float-left" for="OrdersHistory_BranchId">Branch:</label>
        <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "OrdersHistory_BranchId" })%></div>
        <label class="strong" for="OrdersHistory_StartDate">Date Range:</label>
        <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="OrdersHistory_StartDate" class="short" />
        <label class="strong" for="OrdersHistory_EndDate">To</label>
        <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="OrdersHistory_EndDate" class="short" />
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Agency.RebindOrdersHistory();return false">Generate</a></li>
            </ul>
        </div>
        <div class="clear"></div>
        <label class="float-left">Filter by:</label>
        <div id="OrdersHistory_Search" class="fl"></div>
    </fieldset>
    <%= Html.Telerik().Grid<Order>().Name("List_OrdersHistory").HtmlAttributes(new { @class = "bottom-bar" }).DataKeys(keys => {
            keys.Add(o => o.Id).RouteKey("id");
            keys.Add(o => o.Type).RouteKey("type");
            keys.Add(o => o.ReceivedDate).RouteKey("receivedDate");
            keys.Add(o => o.SendDate).RouteKey("sendDate");
        }).Columns(columns => {
            columns.Bound(o => o.Number).Title("Order").Width(70).Sortable(true).ReadOnly();
            columns.Bound(o => o.PatientName).Title("Patient").Width(180).Sortable(true).ReadOnly();
            columns.Bound(o => o.Text).Title("Type").Sortable(true).ReadOnly();
            columns.Bound(o => o.PhysicianName).Title("Physician").Width(180).Sortable(true).ReadOnly();
            columns.Bound(o => o.OrderDate).Title("Order Date").Width(100).Sortable(true).ReadOnly();
            columns.Bound(o => o.SendDate).Format("{0:MM/dd/yyyy}").Title("Sent Date").Width(100).Sortable(true);
            columns.Bound(o => o.ReceivedDate).Format("{0:MM/dd/yyyy}").Title("Received Date").Width(120).Sortable(true);
            columns.Bound(o => o.Id).Sortable(false).ClientTemplate("<a onclick=\"UserInterface.ShowOrdersHistoryModal('<#=Id#>','<#=Type#>');return false\">Edit</a>").Title("Action").Width(90);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersHistoryList", "Agency", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#OrdersHistory_Search").append($("<div/>").GridSearchById("#List_OrdersHistory"));
</script>