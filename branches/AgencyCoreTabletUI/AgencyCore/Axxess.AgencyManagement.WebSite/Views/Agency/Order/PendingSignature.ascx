﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Orders Pending Signature | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    <div class="float-right">
        <div class="buttons">
            <ul>
                <li><%= Html.ActionLink("Excel Export", "OrdersPendingSignature", "Export", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "OrdersPendingSignature_ExportLink", @class = "excel" })%></li>
            </ul>
        </div>
    </div>
<%  } %>
    <fieldset class="grid-controls">
        <label class="float-left" for="OrdersPendingSignature_BranchId">Branch:</label>
        <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "OrdersPendingSignature_BranchId" })%></div>
        <label class="float-left" for="OrdersPendingSignature_StartDate">Date Range:</label>
        <div class="float-left"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="OrdersPendingSignature_StartDate" class="short" />
        <label class="strong" for="OrdersPendingSignature_EndDate">To</label>
        <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="OrdersPendingSignature_EndDate" class="short" /></div>
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Agency.RebindPendingOrders();return false">Generate</a></li>
            </ul>
        </div>
        <div class="clear"></div>
        <label class="float-left">Filter by:</label>
        <div id="OrdersPendingSignature_Search" class="fl"></div>
    </fieldset>
    <%= Html.Telerik().Grid<Order>().Name("List_OrdersPendingSignature").HtmlAttributes(new { @class = "bottom-bar" }).DataKeys(keys => {
            keys.Add(o => o.Id).RouteKey("id");
            keys.Add(o => o.Type).RouteKey("type");
            keys.Add(o => o.ReceivedDate).RouteKey("receivedDate");
            keys.Add(o => o.StartDate).RouteKey("StartDate");
            keys.Add(o => o.EndDate).RouteKey("EndDate");
            keys.Add(o => o.BranchId).RouteKey("BranchId");
        }).Columns(columns => {
            columns.Bound(o => o.Number).Title("Order").Width(70).Sortable(true).ReadOnly();
            columns.Bound(o => o.PatientName).Title("Patient").Width(200).Sortable(true).ReadOnly();
            columns.Bound(o => o.Text).Title("Type").Sortable(true).ReadOnly();
            columns.Bound(o => o.PhysicianName).Title("Physician").Sortable(true).ReadOnly();
            columns.Bound(o => o.CreatedDate).Title("Order Date").Width(100).Sortable(true).ReadOnly();
            columns.Bound(o => o.SentDate).Title("Sent Date").Width(100).Sortable(true).ReadOnly();
            columns.Bound(o => o.ReceivedDate).ClientTemplate("<#=''#>").Title("Received Date").Width(120).Sortable(true);
            columns.Command(commands => { commands.Edit(); }).Width(160).Title("Action");
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersPendingSignature", "Agency", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }).Update("MarkOrderAsReturned", "Agency")).Editable(editing => editing.Mode(GridEditMode.InLine)).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).ClientEvents(events => events
            .OnRowDataBound("Agency.OrderCenterOnload")
            .OnEdit("Agency.PendingSignatureOrdersOnEdit")) %>
</div>
<script type="text/javascript">
    $("#OrdersPendingSignature_Search").append($("<div/>").GridSearchById("#List_OrdersPendingSignature"));
</script>