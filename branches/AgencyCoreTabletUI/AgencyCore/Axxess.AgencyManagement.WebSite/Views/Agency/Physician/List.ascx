﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyPhysician>" %>
<span class="wintitle">Physician List | <%= Current.AgencyName.Clean() %></span>
<% using (Html.BeginForm("Physicians", "Export", FormMethod.Post)) { %>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<AgencyPhysician>()
        .Name("List_Physician")
        .HtmlAttributes(new { @class = "top-gap bottom-bar" })
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(p => p.NPI).Title("NPI").Sortable(true).Width(95);
            columns.Bound(p => p.DisplayName).Title("Name").Sortable(true);
            columns.Bound(p => p.EmailAddress).Title("Email").Sortable(true);
            columns.Bound(p => p.AddressFull).Title("Address").Sortable(true);
            columns.Bound(p => p.PhoneWorkFormatted).Title("Phone Number").Sortable(false).Width(110);
            columns.Bound(p => p.PhoneAlternateFormatted).Title("Alternate Number").Sortable(false).Width(115);
            columns.Bound(p => p.FaxNumberFormatted).Title("Fax Number").Sortable(false).Width(110);
            columns.Bound(p => p.PhysicianAccess).Title("Phy. Access").Sortable(false).Width(90);
            columns.Bound(p => p.Id).ClientTemplate("<a onclick=\"UserInterface.ShowEditPhysician('<#=Id#>');return false\">Edit</a> | <a onclick=\"Physician.Delete('<#=Id#>');return false\" class=\"\">Delete</a>").Sortable(false).Title("Action").Width(100);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Physician"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
$("#List_Physician .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManagePhysicians)) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Physician", Click: UserInterface.ShowNewPhysician } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Excel Export", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
<% } %>