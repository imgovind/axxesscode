﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<fieldset class="medication">
    <legend>License</legend>
<%  using (Html.BeginForm("AddLicense", "Physician", FormMethod.Post, new { @id = "editPhysicainLicenseForm" })) { %>
<%= Html.Hidden("PhysicainId", Model, new { @id = "Edit_PhysicainLicense_Id" })%>
    <div class="column">
        <div class="row">
            <label class="float-left" for="Edit_PhysicainLicense_LicenseNumber">License Number</label>
            <div class="float-right"><%= Html.TextBox("LicenseNumber", "", new { @id = "Edit_PhysicainLicense_LicenseNumber", @class = "valid", @maxlength = "25" })%></div>
        </div>
        <div class="row">
            <label class="float-left" for="Edit_UserLicense_Type">State</label>
            <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "State", "", new { @id = "Edit_UserLicense_Type", @class = "valid" })%></div>
        </div>
    </div>
    <div class="column">
        <div class="row">
            <label class="float-left" for="Edit_PhysicainLicense_InitDate">Issue Date</label>
            <div class="float-right"><input type="date" name="InitiationDate" id="Edit_PhysicainLicense_InitDate" class="required" /></div>
        </div>
        <div class="row">
            <label class="float-left" for="Edit_PhysicainLicense_ExpDate">Expiration Date</label>
            <div class="float-right"><input type="date" name="ExpirationDate" id="Edit_PhysicainLicense_ExpDate" class="required" /></div>
        </div>
    </div>
    <div class="buttons">
        <ul>
            <li><a class="save">Add License</a></li>
        </ul>
    </div>
    <div class="wide-column">
        <div class="row currentlicenses">
            <%= Html.Telerik().Grid<PhysicainLicense>().Name("List_Physicain_Licenses").DataKeys(keys => { keys.Add(M => M.Id); }).Columns(columns => {
                columns.Bound(l => l.LicenseNumber).ReadOnly().Sortable(false);
                columns.Bound(l => l.State).ReadOnly().Sortable(false);
                columns.Bound(l => l.InitiationDateFormatted).ReadOnly().Title("Issue Date").Sortable(false);
                columns.Bound(l => l.ExpirationDate).Title("Expiration Date").Sortable(false);
                columns.Command(commands => {
                    commands.Edit();
                    commands.Delete();
                }).Width(135).Title("Action");
                }).DataBinding(dataBinding => dataBinding.Ajax()
                    .Select("LicenseList", "Physician", new { physicianId = Model })
                    .Update("UpdateLicense", "Physician", new { physicianId = Model })
                    .Delete("DeleteLicense", "Physician", new { physicianId = Model })
                ).Sortable().Footer(false).Scrollable(scrolling => scrolling.Enabled(true)) %>
        </div>
    </div>
<% } %>
</fieldset>