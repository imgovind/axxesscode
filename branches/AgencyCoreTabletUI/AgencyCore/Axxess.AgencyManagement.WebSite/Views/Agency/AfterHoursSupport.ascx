﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper main">
    <fieldset>
        <legend>After-Hours Support Contacts</legend>
        <div class="column">
            <div class="row">
                <label class="fl strong">Diana Cahill</label>
                <label class="fr">(214) 586-4330</label>
            </div>
            <div class="row">
                <label class="fl strong">Betty Singleton</label>
                <label class="fr">(972) 975-0439</label>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="fl strong">Joni Wilson</label>
                <label class="fr">(972) 975-0417</label>
            </div>
            <div class="row">
                <label class="fl strong">Pam Thomas</label>
                <label class="fr">(214) 949-9348</label>
            </div>
        </div>
    </fieldset>
</div>