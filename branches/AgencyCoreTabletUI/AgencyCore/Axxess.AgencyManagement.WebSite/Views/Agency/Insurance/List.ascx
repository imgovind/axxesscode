﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Insurance | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper">
    <%= Html.Telerik().Grid<InsuranceLean>().Name("List_AgencyInsurance").HtmlAttributes(new { @class = "top-gap bottom-bar" }).ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(c => c.Name).Title("Insurance Name").Width(150).Sortable(false);
    columns.Bound(c => c.PayerTypeName).Title("Payer Type").Sortable(false);
    columns.Bound(c => c.PayorId).Title("Payor Id").Sortable(false);
    columns.Bound(c => c.InvoiceTypeName).Title("Invoice Type").Sortable(false);
    columns.Bound(c => c.PhoneNumber).Title("Phone").Width(120);
    columns.Bound(c => c.ContactPerson).Title("Contact Person").Sortable(false);
    columns.Bound(c => c.Action).Title("Action").Width(100);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Insurance")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
$("#List_AgencyInsurance .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageInsurance)) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Insurance", Click: UserInterface.ShowNewInsurance } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
