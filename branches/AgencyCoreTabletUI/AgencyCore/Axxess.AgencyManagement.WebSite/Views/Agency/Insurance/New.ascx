﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Insurance/Payor | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main">
<%  using (Html.BeginForm("Add", "Insurance", FormMethod.Post, new { @id = "newInsuranceForm" })) { %>

    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row"><label for="New_Insurance_Name" class="float-left">Insurance/Payor Name:</label><div class="float-right"><%= Html.TextBox("Name", "", new { @id = "New_Insurance_Name", @class = "text input_wrapper required", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_Insurance_PayorType" class="float-left">Payor Type:</label><div class="float-right"><%= Html.PaymentSourceWithOutMedicareTradition( "PayorType", "", new { @id = "New_Insurance_PayorType", @class = "required notzero valid" })%></div></div>
            <div class="row"><label for="New_Insurance_InvoiceType" class="float-left">Invoice Type:</label><div class="float-right"><% var invoiceType = new SelectList(new[] { new SelectListItem { Text = "UB-04", Value = "1" }, new SelectListItem { Text = "HCFA 1500", Value = "2" }, new SelectListItem { Text = "Invoice", Value = "3" } }, "Value", "Text", "1"); %><%= Html.DropDownList("InvoiceType", invoiceType, new  { @id = "New_Insurance_InvoiceType" })%></div></div>
            <div class="row"><label for="New_Insurance_BillType" class="float-left">Bill Type:</label><div class="float-right"><% var billType = new SelectList(new[] { new SelectListItem { Text = "Institutional", Value = "institutional" }, new SelectListItem { Text = "Professional", Value = "professional" } }, "Value", "Text", "institutional"); %><%= Html.DropDownList("BillType", billType, new { @id = "New_Insurance_BillType" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Insurance_PayorId" class="float-left">Payor ID:</label><div class="float-right"><%= Html.TextBox("PayorId", "", new { @id = "New_Insurance_PayorId", @class = "text input_wrapper required", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_ProviderId" class="float-left">Provider ID/Code:</label><div class="float-right"><%= Html.TextBox("ProviderId", "", new { @id = "New_Insurance_ProviderId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_OtherProviderId" class="float-left">Other Provider ID:</label><div class="float-right"><%= Html.TextBox("OtherProviderId", "", new { @id = "New_Insurance_OtherProviderId", @class = "text input_wrapper ", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_ProviderSubscriberId" class="float-left">Provider Subscriber ID:</label><div class="float-right"><%= Html.TextBox("ProviderSubscriberId", "", new { @id = "New_Insurance_ProviderSubscriberId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Insurance_SubmitterId" class="float-left">Submitter ID:</label><div class="float-right"><%= Html.TextBox("SubmitterId", "", new { @id = "New_Insurance_SubmitterId", @class = "text input_wrapper", @maxlength = "40" })%></div></div>
        </div><div class="clear"></div>
        <div class="wide-column">
            <div class="row">
                <label for="New_Insurance_Ub04Locator81cca" class="float-left">UB04 Locator 81CCa:</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator1")%>
                <div class="float-left margin">
                    <%= Html.TextBox("Locator1_Code1", "", new { @id = "New_Insurance_Locator1_Code1", @class = "text short", @maxlength = "40" })%>
                    <%= Html.TextBox("Locator1_Code2", "", new { @id = "New_Insurance_Locator1_Code2", @class = "text short", @maxlength = "40" })%>
                    <%= Html.TextBox("Locator1_Code3", "", new { @id = "New_Insurance_Locator1_Code3", @class = "text short", @maxlength = "40" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>EDI Information</legend>
        <div class="wide-column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsAxxessTheBiller",false, new { @id = "New_Insurance_IsAxxessTheBiller", @class = "radio float-left" })%>
                        <label for="New_Insurance_IsAxxessTheBiller" class="radio">Check here if claims are electronically submitted to your clearing house through Axxess&#8482;.</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Insurance_ClearingHouse" class="float-left">Clearing House:</label>
                <div class="fr">
                    <%  var clearingHouse = new SelectList(new[] {
                            new SelectListItem { Text = string.Empty, Value = "0" },
                            new SelectListItem { Text = "ZirMed", Value = "ZirMed" },
                            new SelectListItem { Text = "Availity", Value = "Availity"}
                        }, "Value", "Text", "0"); %>
                    <%= Html.DropDownList("ClearingHouse", clearingHouse, new { @id = "New_Insurance_ClearingHouse" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div id="New_Insurance_EdiInformation">
            <div class="column">
                <div class="row">
                    <label for="New_Insurance_InterchangeReceiverId" class="float-left">Interchange Receiver ID:</label>
                    <div class="float-right">
                        <%  var interchangeReceiverId = new SelectList(new[] {
                                new SelectListItem { Text = "Mutually Defined (ZZ)", Value = "ZZ" },
                                new SelectListItem { Text = "Carrier Identification Number as assigned by Health Care Financing Administration (HCFA) (27)", Value = "27" },
                                new SelectListItem { Text = "Duns (Dun &amp; Bradstreet) (01)", Value = "01"},
                                new SelectListItem { Text = "Duns Plus Suffix (14)", Value = "14"},
                                new SelectListItem { Text = "Fiscal Intermediary Identification Number as assigned by Health Care Financing Administration (HCFA) (28)", Value = "28"},
                                new SelectListItem { Text = "Health Industry Number (HIN) (20)", Value = "20"},
                                new SelectListItem { Text = "Medicare Provider and Supplier Identification Number as assigned by Health Care Financing Administration (HCFA) (29)", Value = "29"},
                                new SelectListItem { Text = "Association of Insurance Commisioners Company Code (NAIC) (33)", Value = "33"},
                                new SelectListItem { Text = "U.S. Federal Tax Identification Number (30)", Value = "30"}
                            }, "Value", "Text","28"); %>
                        <%= Html.DropDownList("InterchangeReceiverId", interchangeReceiverId, new { @id = "New_Insurance_InterchangeReceiverId", @class = "valid" })%>
                    </div>
                </div>
                <div class="row">
                    <label for="New_Insurance_ClearingHouseSubmitterId" class="float-left">Clearing House Submitter ID:</label>
                    <div class="float-right"><%= Html.TextBox("ClearingHouseSubmitterId", "", new { @id = "New_Insurance_ClearingHouseSubmitterId", @class = "text input_wrapper", @maxlength = "40" })%></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="New_Insurance_SubmitterName" class="float-left">Submitter Name:</label>
                    <div class="float-right"><%= Html.TextBox("SubmitterName", "", new { @id = "New_Insurance_SubmitterName", @class = "text input_wrapper", @maxlength = "100" })%></div>
                </div>
                <div class="row">
                    <label for="New_Insurance_SubmitterPhone1" class="float-left">Submitter Phone:</label>
                    <div class="float-right">
                        <input type="text" class="input_wrappermultible autotext digits phone-short"name="SubmitterPhoneArray" id="New_Insurance_SubmitterPhone1" maxlength="3" />
                        -
                        <input type="text" class="input_wrappermultible autotext digits phone-short" name="SubmitterPhoneArray" id="New_Insurance_SubmitterPhone2" maxlength="3" />
                        -
                        <input type="text" class="input_wrappermultible autotext digits phone-long" name="SubmitterPhoneArray" id="New_Insurance_SubmitterPhone3" maxlength="4" />
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Rates, Unit Type &#38; HCPCS Codes</legend>
        <div class="column">
            <div class="row">
                <label for="New_Insurance_ChargeType" class="float-left">Bill Type (Unit):</label>
                <div class="fr">
                    <%  var unitType = new SelectList(new[] {
                            new SelectListItem { Text = "Per Visit", Value = "1" },
                            new SelectListItem { Text = "Hourly", Value = "2" },
                            new SelectListItem { Text = "Per 15 Min", Value = "3" }
                        }, "Value", "Text", 1); %>
                    <%= Html.DropDownList("ChargeType", unitType, new { @id = "New_Insurance_ChargeType" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="column">
            <div class="row">
                <div class="ins_disc strong">Disciplines</div>
                <div class="ins_rate strong ac">Bill Rate</div>
                <div class="ins_code strong ac">HCPCS Code</div>
            </div>
            <div class="row">
                <div class="ins_disc">Skilled Nurse</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SkilledNurse")%>
                    <%= Html.TextBox("SkilledNurse_Charge", "", new { @id = "New_Insurance_SkilledNurseCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SkilledNurse_Code", "", new { @id = "New_Insurance_SkilledNurseCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Skilled Nurse (Teaching)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SkilledNurseTeaching")%>
                    <%= Html.TextBox("SkilledNurseTeaching_Charge", "", new { @id = "New_Insurance_SkilledNurseTeachingCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SkilledNurseTeaching_Code", "", new { @id = "New_Insurance_SkilledNurseTeachingCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Skilled Nurse (Observation)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SkilledNurseObservation")%>
                    <%= Html.TextBox("SkilledNurseObservation_Charge", "", new { @id = "New_Insurance_SkilledNurseObservationCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SkilledNurseObservation_Code", "", new { @id = "New_Insurance_SkilledNurseObservationCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Skilled Nurse (Management)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SkilledNurseManagement")%>
                    <%= Html.TextBox("SkilledNurseManagement_Charge", "", new { @id = "New_Insurance_SkilledNurseManagementCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SkilledNurseManagement_Code", "", new { @id = "New_Insurance_SkilledNurseManagementCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Physical Therapy</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "PhysicalTherapy")%>
                    <%= Html.TextBox("PhysicalTherapy_Charge", "", new { @id = "New_Insurance_PhysicalTherapyCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("PhysicalTherapy_Code", "", new { @id = "New_Insurance_PhysicalTherapyCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Physical Therapy Assistant </div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "PhysicalTherapyAssistance")%>
                    <%= Html.TextBox("PhysicalTherapyAssistance_Charge", "", new { @id = "New_Insurance_PhysicalTherapyAssistanceCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("PhysicalTherapyAssistance_Code", "", new { @id = "New_Insurance_PhysicalTherapyAssistanceCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Physical Therapy (Maintenance)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "PhysicalTherapyMaintenance")%>
                    <%= Html.TextBox("PhysicalTherapyMaintenance_Charge", "", new { @id = "New_Insurance_PhysicalTherapyMaintenanceCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("PhysicalTherapyMaintenance_Code", "", new { @id = "New_Insurance_PhysicalTherapyMaintenanceCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Speech Therapy</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SpeechTherapy")%>
                    <%= Html.TextBox("SpeechTherapy_Charge", "", new { @id = "New_Insurance_SpeechTherapyCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SpeechTherapy_Code", "", new { @id = "New_Insurance_SpeechTherapyCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Speech Therapy(Maintenance)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SpeechTherapyMaintenance")%>
                    <%= Html.TextBox("SpeechTherapyMaintenance_Charge", "", new { @id = "New_Insurance_SpeechTherapyMaintenanceCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SpeechTherapyMaintenance_Code", "", new { @id = "New_Insurance_SpeechTherapyMaintenanceCode", @class = "fill" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="ins_disc strong">Disciplines</div>
                <div class="ins_rate strong ac">Bill Rate</div>
                <div class="ins_code strong ac">HCPCS Code</div>
            </div>
            <div class="row">
                <div class="ins_disc">Occupational Therapy</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "OccupationalTherapy")%>
                    <%= Html.TextBox("OccupationalTherapy_Charge", "", new { @id = "New_Insurance_OccupationalTherapyCharge ", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("OccupationalTherapy_Code", "", new { @id = "New_Insurance_OccupationalTherapyCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Occupational Therapy Assistant</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "OccupationalTherapyAssistance")%>
                    <%= Html.TextBox("OccupationalTherapyAssistance_Charge", "", new { @id = "New_Insurance_OccupationalTherapyAssistanceCharge ", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("OccupationalTherapyAssistance_Code", "", new { @id = "New_Insurance_OccupationalTherapyAssistanceCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Occupational Therapy(Maintenance)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "OccupationalTherapyMaintenance")%>
                    <%= Html.TextBox("OccupationalTherapyMaintenance_Charge", "", new { @id = "New_Insurance_OccupationalTherapyMaintenanceCharge ", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("OccupationalTherapyMaintenance_Code", "", new { @id = "New_Insurance_OccupationalTherapyMaintenanceCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Medical Social Worker</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "MedicareSocialWorker")%>
                    <%= Html.TextBox("MedicareSocialWorker_Charge", "", new { @id = "New_Insurance_MedicareSocialWorkerCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("MedicareSocialWorker_Code", "", new { @id = "New_Insurance_MedicareSocialWorkerCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Home Health Aide</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "HomeHealthAide")%>
                    <%= Html.TextBox("HomeHealthAide_Charge", "", new { @id = "New_Insurance_HomeHealthAideCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("HomeHealthAide_Code", "", new { @id = "New_Insurance_HomeHealthAideCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Attendant</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "Attendant")%>
                    <%= Html.TextBox("Attendant_Charge", "", new { @id = "New_Insurance_AttendantCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("Attendant_Code", "", new { @id = "New_Insurance_AttendantCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Companion Care</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "CompanionCare")%>
                    <%= Html.TextBox("CompanionCare_Charge", "", new { @id = "New_Insurance_CompanionCareCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("CompanionCare_Code", "", new { @id = "New_Insurance_CompanionCareCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Homemaker Services</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "HomemakerServices")%>
                    <%= Html.TextBox("HomemakerServices_Charge", "", new { @id = "New_Insurance_HomemakerServicesCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("HomemakerServices_Code", "", new { @id = "New_Insurance_HomemakerServicesCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Private Duty Sitter</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "PrivateDutySitter")%>
                    <%= Html.TextBox("PrivateDutySitter_Charge", "", new { @id = "New_Insurance_PrivateDutySitterCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("PrivateDutySitter_Code", "", new { @id = "New_Insurance_PrivateDutySitterCode", @class = "fill" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Address</legend>
        <div class="column">
            <div class="row"><label for="New_Insurance_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("AddressLine1", "", new { @id = "New_Insurance_AddressLine1", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Insurance_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("AddressLine2", "", new { @id = "New_Insurance_AddressLine2", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Insurance_AddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("AddressCity", "", new { @id = "New_Insurance_AddressCity", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Insurance_AddressStateCode" class="float-left">State, Zip:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Insurance_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Insurance_AddressZipCode", @class = "text digits isValidUSZip zip", @maxlength = "5" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Contact Person</legend>
        <div class="column">
            <div class="row"><label for="New_Insurance_ContactPersonFirstName" class="float-left">First Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonFirstName", "", new { @id = "New_Insurance_ContactPersonFirstName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Insurance_ContactPersonLastName" class="float-left">Last Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonLastName", "", new { @id = "New_Insurance_ContactPersonLastName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Insurance_ContactEmailAddress" class="float-left">Email:</label><div class="float-right"><%= Html.TextBox("ContactEmailAddress", "", new { @id = "New_Insurance_ContactEmailAddress", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Insurance_PhoneNumberArray1" class="float-left">Phone:</label><div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone-short"name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray1" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone-short" name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray2" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone-long" name="PhoneNumberArray" id="New_Insurance_PhoneNumberArray3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Insurance_FaxNumberArray1" class="float-left">Fax Number:</label><div class="float-right"><input type="text" class="input_wrappermultible autotext digits phone-short" name="FaxNumberArray" id="New_Insurance_FaxNumberArray1" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone-short" name="FaxNumberArray" id="New_Insurance_FaxNumberArray2" maxlength="3" />&#160;-&#160;<input type="text" class="input_wrappermultible autotext digits phone-long" name="FaxNumberArray" id="New_Insurance_FaxNumberArray3" maxlength="4" /></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Insurance_CurrentBalance" class="float-left">Current Balance:</label><div class="float-right"><%= Html.TextBox("CurrentBalance", "", new { @id = "New_Insurance_CurrentBalance", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Insurance_WorkWeekStartDay" class="float-left">Work Week Begins:</label><div class="float-right"><% var workWeekStartDay = new SelectList(new[]{ new SelectListItem { Text = "Sunday", Value = "1" },new SelectListItem { Text = "Monday", Value = "2" } }, "Value", "Text");%><%= Html.DropDownList("WorkWeekStartDay", workWeekStartDay, new { @id = "New_Insurance_WorkWeekStartDay" })%></div></div><br />
            <div class="row"><label for="New_Insurance_VisitAuthReq" class="float-left">Visit Authorization Required:</label><div class="float-right"><%= Html.RadioButton("IsVisitAuthorizationRequired", "1", new {  @class = "radio" })%><label class="inline-radio">Yes</label><%= Html.RadioButton("IsVisitAuthorizationRequired", "0", true, new { @id = "New_Insurance_VisitAuthReq", @class = "radio" })%><label class="inline-radio">No</label></div></div><br />
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    Forms.HideIfChecked($("#New_Insurance_IsAxxessTheBiller"), $("#New_Insurance_EdiInformation"));
</script>