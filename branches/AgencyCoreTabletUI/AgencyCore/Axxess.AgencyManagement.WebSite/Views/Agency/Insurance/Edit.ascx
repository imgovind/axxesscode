﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyInsurance>" %>
<span class="wintitle">Edit Insurance | <%= Model != null ? Model.Name.ToTitleCase().Clean() : "" %></span>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Insurance_Id" })%>
<%  var data = Model != null ? Model.ToChargeRateDictionary() : new Dictionary<string, ChargeRate>(); %>
<%  var locator = Model != null ? Model.ToLocatorDictionary() : new Dictionary<string, Locator>(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Update", "Insurance", FormMethod.Post, new { @id = "editInsuranceForm" })) { %>

    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Insurance_Name" class="float-left">Insurance/Payor Name:</label>
                <div class="float-right"><%= Html.TextBox("Name", Model.Name, new { @id = "Edit_Insurance_Name", @class = "text input_wrapper required", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Insurance_PayorType" class="float-left">Payor Type:</label>
                <div class="float-right"><%= Html.PaymentSourceWithOutMedicareTradition( "PayorType", Convert.ToString(Model.PayorType), new { @id = "Edit_Insurance_PayorType", @class = "required notzero valid" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Insurance_InvoiceType" class="float-left">Invoice Type:</label>
                <div class="float-right">
                    <%  var invoiceType = new SelectList(new[] {
                            new SelectListItem { Text = "UB-04", Value = "1" },
                            new SelectListItem { Text = "HCFA 1500", Value = "2" },
                            new SelectListItem { Text = "Invoice", Value = "3" }
                        }, "Value", "Text", Convert.ToString(Model.InvoiceType)); %>
                    <%= Html.DropDownList("InvoiceType", invoiceType, new  { @id = "Edit_Insurance_InvoiceType" })%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Insurance_BillType" class="float-left">Bill Type:</label>
                <div class="float-right">
                    <%  var billType = new SelectList(new[] {
                            new SelectListItem { Text = "Institutional", Value = "institutional" },
                            new SelectListItem { Text = "Professional", Value = "professional" }
                        }, "Value", "Text", Model.BillType); %>
                    <%= Html.DropDownList("BillType", billType, new { @id = "Edit_Insurance_BillType" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Insurance_PayorId" class="float-left">Payor Id:</label>
                <div class="float-right"><%= Html.TextBox("PayorId", Convert.ToString(Model.PayorId), new { @id = "Edit_Insurance_PayorId", @class = "text input_wrapper required" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Insurance_ProviderId" class="float-left">Provider ID/Code:</label>
                <div class="float-right"><%= Html.TextBox("ProviderId", Model.ProviderId, new { @id = "Edit_Insurance_ProviderId", @class = "text input_wrapper", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Insurance_OtherProviderId" class="float-left">Other Provider ID:</label>
                <div class="float-right"><%= Html.TextBox("OtherProviderId", Model.OtherProviderId, new { @id = "Edit_Insurance_OtherProviderId", @class = "text input_wrapper", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Insurance_ProviderSubscriberId" class="float-left">Provider Subscriber ID:</label>
                <div class="float-right"><%= Html.TextBox("ProviderSubscriberId", Model.ProviderSubscriberId, new { @id = "Edit_Insurance_ProviderSubscriberId", @class = "text input_wrapper ", @maxlength = "40" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Insurance_SubmitterId" class="float-left">Submitter ID:</label>
                <div class="float-right"><%= Html.TextBox("SubmitterId", Model.SubmitterId, new { @id = "Edit_Insurance_SubmitterId", @class = "text input_wrapper", @maxlength = "40" })%></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide-column">
            <div class="row">
                <label for="Edit_Insurance_Ub04Locator81cca" class="float-left">UB04 Locator 81CCa:</label>
                <%= Html.Hidden("Ub04Locator81cca", "Locator1")%>
                <div class="float-left"><%= Html.TextBox("Locator1_Code1", locator.ContainsKey("Locator1") ? locator["Locator1"].Code1 : string.Empty, new { @id = "Edit_Insurance_Locator1_Code1", @class = "text short", @maxlength = "40" })%>
                <%= Html.TextBox("Locator1_Code2", locator.ContainsKey("Locator1") ? locator["Locator1"].Code2 : string.Empty, new { @id = "Edit_Insurance_Locator1_Code2", @class = "text short", @maxlength = "40" })%>
                <%= Html.TextBox("Locator1_Code3", locator.ContainsKey("Locator1") ? locator["Locator1"].Code3 : string.Empty, new { @id = "Edit_Insurance_Locator1_Code3", @class = "text short", @maxlength = "40" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>EDI Information</legend>
        <div class="wide-column">
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.CheckBox("IsAxxessTheBiller", Model.IsAxxessTheBiller, new { @id = "Edit_Insurance_IsAxxessTheBiller", @class = "radio float-left" })%>
                        <label for="Edit_Insurance_IsAxxessTheBiller" class="radio">Check here if claims are electronically submitted to your clearing house through Axxess&#8482;.</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Insurance_ClearingHouse" class="float-left">Clearing House:</label>
                <div class="fr">
                    <%  var clearingHouse = new SelectList(new[] {
                            new SelectListItem { Text = string.Empty, Value = "0" },
                            new SelectListItem { Text = "ZirMed", Value = "ZirMed" },
                            new SelectListItem { Text = "Availity", Value = "Availity"}
                        }, "Value", "Text", Model.ClearingHouse); %>
                    <%= Html.DropDownList("ClearingHouse", clearingHouse, new { @id = "Edit_Insurance_ClearingHouse" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div id="Edit_Insurance_EdiInformation">
            <div class="column">
                <div class="row">
                    <label for="Edit_Insurance_InterchangeReceiverId" class="float-left">Interchange Receiver ID:</label>
                    <div class="float-right">
                        <%  var interchangeReceiverId = new SelectList(new[] {
                                new SelectListItem { Text = "Mutually Defined (ZZ)", Value = "ZZ" },
                                new SelectListItem { Text = "Carrier Identification Number as assigned by Health Care Financing Administration (HCFA) (27)", Value = "27" },
                                new SelectListItem { Text = "Duns (Dun &amp; Bradstreet) (01)", Value = "01"},
                                new SelectListItem { Text = "Duns Plus Suffix (14)", Value = "14"},
                                new SelectListItem { Text = "Fiscal Intermediary Identification Number as assigned by Health Care Financing Administration (HCFA) (28)", Value = "28"},
                                new SelectListItem { Text = "Health Industry Number (HIN) (20)", Value = "20"},
                                new SelectListItem { Text = "Medicare Provider and Supplier Identification Number as assigned by Health Care Financing Administration (HCFA) (29)", Value = "29"},
                                new SelectListItem { Text = "Association of Insurance Commisioners Company Code (NAIC) (33)", Value = "33"},
                                new SelectListItem { Text = "U.S. Federal Tax Identification Number (30)", Value = "30"}
                            }, "Value", "Text", Model.InterchangeReceiverId); %>
                        <%= Html.DropDownList("InterchangeReceiverId", interchangeReceiverId, new { @id = "Edit_Insurance_InterchangeReceiverId", @class = "valid" })%>
                    </div>
                </div>
                <div class="row">
                    <label for="Edit_Insurance_ClearingHouseSubmitterId" class="float-left">Clearing House Submitter ID:</label>
                    <div class="float-right"><%= Html.TextBox("ClearingHouseSubmitterId", Model.ClearingHouseSubmitterId, new { @id = "Edit_Insurance_ClearingHouseSubmitterId", @class = "text input_wrapper", @maxlength = "40" })%></div>
                </div>
            </div>
            <div class="column">
                <div class="row">
                    <label for="Edit_Insurance_SubmitterName" class="float-left">Submitter Name:</label>
                    <div class="float-right"><%= Html.TextBox("SubmitterName", Model.SubmitterName, new { @id = "Edit_Insurance_SubmitterName", @class = "text input_wrapper", @maxlength = "100" })%></div>
                </div>
                <div class="row">
                    <label for="Edit_Insurance_SubmitterPhone1" class="float-left">Submitter Phone:</label>
                    <div class="float-right">
                        <%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() ? Model.SubmitterPhone.Substring(0, 3) : "", new { @id = "Edit_Insurance_SubmitterPhoneArray1", @class = "input_wrappermultible autotext  digits phone-short", @maxlength = "3", @size = "3" })%>
                        -
                        <%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() ? Model.SubmitterPhone.Substring(3, 3) : "", new { @id = "Edit_Insurance_SubmitterPhoneArray2", @class = "input_wrappermultible autotext  digits phone-short", @maxlength = "3", @size = "3" })%>
                        -
                        <%= Html.TextBox("SubmitterPhoneArray", Model.SubmitterPhone.IsNotNullOrEmpty() ? Model.SubmitterPhone.Substring(6, 4) : "", new { @id = "Edit_Insurance_SubmitterPhoneArray3", @class = "input_wrappermultible autotext  digits phone-long", @maxlength = "4", @size = "4" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Rates, Unit Type &#38; HCPCS Codes</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Insurance_ChargeType" class="float-left">Bill Type (Unit):</label>
                <div class="fr">
                    <%  var unitType = new SelectList(new[] {
                            new SelectListItem { Text = "Per Visit", Value = "1" },
                            new SelectListItem { Text = "Hourly", Value = "2" },
                            new SelectListItem { Text = "Per 15 Min", Value = "3" }
                        }, "Value", "Text", Model.ChargeType); %>
                    <%= Html.DropDownList("ChargeType", unitType, new { @id = "Edit_Insurance_ChargeType" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="column">
            <div class="row">
                <div class="ins_disc strong">Disciplines</div>
                <div class="ins_rate strong ac">Bill Rate</div>
                <div class="ins_code strong ac">HCPCS Code</div>
            </div>
            <div class="row">
                <div class="ins_disc">Skilled Nurse</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SkilledNurse")%>
                    <%= Html.TextBox("SkilledNurse_Charge", data.ContainsKey("SkilledNurse") ? data["SkilledNurse"].Charge : string.Empty, new { @id = "Edit_Insurance_SkilledNurseCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SkilledNurse_Code", data.ContainsKey("SkilledNurse") ? data["SkilledNurse"].Code : string.Empty, new { @id = "Edit_Insurance_SkilledNurseCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Skilled Nurse (Teaching)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SkilledNurseTeaching")%>
                    <%= Html.TextBox("SkilledNurseTeaching_Charge", data.ContainsKey("SkilledNurseTeaching") ? data["SkilledNurseTeaching"].Charge : string.Empty, new { @id = "Edit_Insurance_SkilledNurseTeachingCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SkilledNurseTeaching_Code", data.ContainsKey("SkilledNurseTeaching") ? data["SkilledNurseTeaching"].Code : string.Empty, new { @id = "Edit_Insurance_SkilledNurseTeachingCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Skilled Nurse (Observation)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SkilledNurseObservation")%>
                    <%= Html.TextBox("SkilledNurseObservation_Charge", data.ContainsKey("SkilledNurseObservation") ? data["SkilledNurseObservation"].Charge : string.Empty, new { @id = "Edit_Insurance_SkilledNurseObservationCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SkilledNurseObservation_Code", data.ContainsKey("SkilledNurseObservation") ? data["SkilledNurseObservation"].Code : string.Empty, new { @id = "Edit_Insurance_SkilledNurseObservationCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Skilled Nurse (Management)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SkilledNurseManagement")%>
                    <%= Html.TextBox("SkilledNurseManagement_Charge", data.ContainsKey("SkilledNurseManagement") ? data["SkilledNurseManagement"].Charge : string.Empty, new { @id = "Edit_Insurance_SkilledNurseManagementCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SkilledNurseManagement_Code", data.ContainsKey("SkilledNurseManagement") ? data["SkilledNurseManagement"].Code : string.Empty, new { @id = "Edit_Insurance_SkilledNurseManagementCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Physical Therapy</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "PhysicalTherapy")%>
                    <%= Html.TextBox("PhysicalTherapy_Charge", data.ContainsKey("PhysicalTherapy") ? data["PhysicalTherapy"].Charge : string.Empty, new { @id = "Edit_Insurance_PhysicalTherapyCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("PhysicalTherapy_Code", data.ContainsKey("PhysicalTherapy") ? data["PhysicalTherapy"].Code : string.Empty, new { @id = "Edit_Insurance_PhysicalTherapyCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Physical Therapy Assistant </div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "PhysicalTherapyAssistance")%>
                    <%= Html.TextBox("PhysicalTherapyAssistance_Charge", data.ContainsKey("PhysicalTherapyAssistance") ? data["PhysicalTherapyAssistance"].Charge : string.Empty, new { @id = "Edit_Insurance_PhysicalTherapyAssistanceCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("PhysicalTherapyAssistance_Code", data.ContainsKey("PhysicalTherapyAssistance") ? data["PhysicalTherapyAssistance"].Code : string.Empty, new { @id = "Edit_Insurance_PhysicalTherapyAssistanceCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Physical Therapy (Maintenance)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "PhysicalTherapyMaintenance")%>
                    <%= Html.TextBox("PhysicalTherapyMaintenance_Charge", data.ContainsKey("PhysicalTherapyMaintenance") ? data["PhysicalTherapyMaintenance"].Charge : string.Empty, new { @id = "Edit_Insurance_PhysicalTherapyMaintenanceCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("PhysicalTherapyMaintenance_Code", data.ContainsKey("PhysicalTherapyMaintenance") ? data["PhysicalTherapyMaintenance"].Code : string.Empty, new { @id = "Edit_Insurance_PhysicalTherapyMaintenanceCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Speech Therapy</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SpeechTherapy")%>
                    <%= Html.TextBox("SpeechTherapy_Charge", data.ContainsKey("SpeechTherapy") ? data["SpeechTherapy"].Charge : string.Empty, new { @id = "Edit_Insurance_SpeechTherapyCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SpeechTherapy_Code", data.ContainsKey("SpeechTherapy") ? data["SpeechTherapy"].Code : string.Empty, new { @id = "Edit_Insurance_SpeechTherapyCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Speech Therapy(Maintenance)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "SpeechTherapyMaintenance")%>
                    <%= Html.TextBox("SpeechTherapyMaintenance_Charge", data.ContainsKey("SpeechTherapyMaintenance") ? data["SpeechTherapyMaintenance"].Charge : string.Empty, new { @id = "Edit_Insurance_SpeechTherapyMaintenanceCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("SpeechTherapyMaintenance_Code", data.ContainsKey("SpeechTherapyMaintenance") ? data["SpeechTherapyMaintenance"].Code : string.Empty, new { @id = "Edit_Insurance_SpeechTherapyMaintenanceCode", @class = "fill" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="ins_disc strong">Disciplines</div>
                <div class="ins_rate strong ac">Bill Rate</div>
                <div class="ins_code strong ac">HCPCS Code</div>
            </div>
            <div class="row">
                <div class="ins_disc">Occupational Therapy</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "OccupationalTherapy")%>
                    <%= Html.TextBox("OccupationalTherapy_Charge", data.ContainsKey("OccupationalTherapy") ? data["OccupationalTherapy"].Charge : string.Empty, new { @id = "Edit_Insurance_OccupationalTherapyCharge ", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("OccupationalTherapy_Code", data.ContainsKey("OccupationalTherapy") ? data["OccupationalTherapy"].Code : string.Empty, new { @id = "Edit_Insurance_OccupationalTherapyCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Occupational Therapy Assistant</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "OccupationalTherapyAssistance")%>
                    <%= Html.TextBox("OccupationalTherapyAssistance_Charge", data.ContainsKey("OccupationalTherapyAssistance") ? data["OccupationalTherapyAssistance"].Charge : string.Empty, new { @id = "Edit_Insurance_OccupationalTherapyAssistanceCharge ", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("OccupationalTherapyAssistance_Code", data.ContainsKey("OccupationalTherapyAssistance") ? data["OccupationalTherapyAssistance"].Code : string.Empty, new { @id = "Edit_Insurance_OccupationalTherapyAssistanceCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Occupational Therapy(Maintenance)</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "OccupationalTherapyMaintenance")%>
                    <%= Html.TextBox("OccupationalTherapyMaintenance_Charge", data.ContainsKey("OccupationalTherapyMaintenance") ? data["OccupationalTherapyMaintenance"].Charge : string.Empty, new { @id = "Edit_Insurance_OccupationalTherapyMaintenanceCharge ", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("OccupationalTherapyMaintenance_Code", data.ContainsKey("OccupationalTherapyMaintenance") ? data["OccupationalTherapyMaintenance"].Code : string.Empty, new { @id = "Edit_Insurance_OccupationalTherapyMaintenanceCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Medical Social Worker</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "MedicareSocialWorker")%>
                    <%= Html.TextBox("MedicareSocialWorker_Charge", data.ContainsKey("MedicareSocialWorker") ? data["MedicareSocialWorker"].Charge : string.Empty, new { @id = "Edit_Insurance_MedicareSocialWorkerCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("MedicareSocialWorker_Code", data.ContainsKey("MedicareSocialWorker") ? data["MedicareSocialWorker"].Code : string.Empty, new { @id = "Edit_Insurance_MedicareSocialWorkerCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Home Health Aide</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "HomeHealthAide")%>
                    <%= Html.TextBox("HomeHealthAide_Charge", data.ContainsKey("HomeHealthAide") ? data["HomeHealthAide"].Charge : string.Empty, new { @id = "Edit_Insurance_HomeHealthAideCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("HomeHealthAide_Code", data.ContainsKey("HomeHealthAide") ? data["HomeHealthAide"].Code : string.Empty, new { @id = "Edit_Insurance_HomeHealthAideCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Attendant</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "Attendant")%>
                    <%= Html.TextBox("Attendant_Charge", data.ContainsKey("Attendant") ? data["Attendant"].Charge : string.Empty, new { @id = "Edit_Insurance_AttendantCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("Attendant_Code", data.ContainsKey("Attendant") ? data["Attendant"].Code : string.Empty, new { @id = "Edit_Insurance_AttendantCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Companion Care</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "CompanionCare")%>
                    <%= Html.TextBox("CompanionCare_Charge", data.ContainsKey("CompanionCare") ? data["CompanionCare"].Charge : string.Empty, new { @id = "Edit_Insurance_CompanionCareCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("CompanionCare_Code", data.ContainsKey("CompanionCare") ? data["CompanionCare"].Code : string.Empty, new { @id = "Edit_Insurance_CompanionCareCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Homemaker Services</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "HomemakerServices")%>
                    <%= Html.TextBox("HomemakerServices_Charge", data.ContainsKey("HomemakerServices") ? data["HomemakerServices"].Charge : string.Empty, new { @id = "Edit_Insurance_HomemakerServicesCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("HomemakerServices_Code", data.ContainsKey("HomemakerServices") ? data["HomemakerServices"].Code : string.Empty, new { @id = "Edit_Insurance_HomemakerServicesCode", @class = "fill" })%></div>
            </div>
            <div class="row">
                <div class="ins_disc">Private Duty Sitter</div>
                <div class="ins_rate">
                    <%= Html.Hidden("RateDiscipline", "PrivateDutySitter")%>
                    <%= Html.TextBox("PrivateDutySitter_Charge", data.ContainsKey("PrivateDutySitter") ? data["PrivateDutySitter"].Charge : string.Empty, new { @id = "Edit_Insurance_PrivateDutySitterCharge", @class = "rates currency fill" })%>
                </div>
                <div class="ins_code"><%= Html.TextBox("PrivateDutySitter_Code", data.ContainsKey("PrivateDutySitter") ? data["PrivateDutySitter"].Code : string.Empty, new { @id = "Edit_Insurance_PrivateDutySitterCode", @class = "fill" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Insurance_AddressLine1", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Insurance_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Insurance_AddressLine2", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_AddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Insurance_AddressCity", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Insurance_AddressStateCode" class="float-left">State, Zip:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Insurance_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Insurance_AddressZipCode", @class = "text digits isValidUSZip zip", @maxlength = "5" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Insurance Contact Person</legend>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_ContactPersonFirstName" class="float-left">First Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "Edit_Insurance_ContactPersonFirstName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Insurance_ContactPersonLastName" class="float-left">Last Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "Edit_Insurance_ContactPersonLastName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Insurance_ContactEmailAddress" class="float-left">Email:</label><div class="float-right"><%= Html.TextBox("ContactEmailAddress", Model.ContactEmailAddress, new { @id = "Edit_Insurance_ContactEmailAddress", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Insurance_PhoneNumberArray1" class="float-left">Phone:</label><div class="float-right"><%= Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() ? Model.PhoneNumber.Substring(0, 3) : "", new { @id = "Edit_Insurance_PhoneNumberArray1", @class = "input_wrappermultible autotext  digits phone-short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() ? Model.PhoneNumber.Substring(3, 3) : "", new { @id = "Edit_Insurance_PhoneNumberArray2", @class = "input_wrappermultible autotext required digits phone-short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("PhoneNumberArray", Model.PhoneNumber.IsNotNullOrEmpty() ? Model.PhoneNumber.Substring(6, 4) : "", new { @id = "Edit_Insurance_PhoneNumberArray3", @class = "input_wrappermultible autotext required digits phone-long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Insurance_FaxNumberArray1" class="float-left">Fax Number:</label><div class="float-right"><%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Insurance_FaxNumberArray1", @class = "input_wrappermultible autotext digits phone-short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Insurance_FaxNumberArray2", @class = "input_wrappermultible autotext required digits phone-short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Insurance_FaxNumberArray3", @class = "input_wrappermultible autotext required digits phone-long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Insurance_CurrentBalance" class="float-left">Current Balance:</label><div class="float-right"><%= Html.TextBox("CurrentBalance", Model.CurrentBalance, new { @id = "Edit_Insurance_CurrentBalance", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Insurance_WorkWeekStartDay" class="float-left">Work Week Begins:</label><div class="float-right"><%var workWeekStartDay = new SelectList(new[]{ new SelectListItem { Text = "Sunday", Value = "1" },new SelectListItem { Text = "Monday", Value = "2" }}, "Value", "Text", Convert.ToString(Model.WorkWeekStartDay));%><%= Html.DropDownList("WorkWeekStartDay", workWeekStartDay, new { @id = "Edit_Insurance_WorkWeekStartDay" })%></div></div>
            <br />
            <div class="row"><label for="Edit_Insurance_IsAuthReq" class="float-left">Visit Authorization Required:</label><div class="float-right"><%= Html.RadioButton("IsVisitAuthorizationRequired", true, Model.IsVisitAuthorizationRequired, new { @id = "Edit_Insurance_IsAuthReq", @class = "required radio" })%><label class="inline-radio">Yes</label><%= Html.RadioButton("IsVisitAuthorizationRequired",false ,!Model.IsVisitAuthorizationRequired, new { @class = "required radio" })%><label class="inline-radio">No</label></div></div>
         </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<span class=\"img icon activity\" onclick=\"Log.LoadInsuranceLog('{0}')\" title=\"Activity Logs\"></span>", Model.Id)%></div>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
<% } %>
</div>
<script type="text/javascript">
    Forms.HideIfChecked($("#Edit_Insurance_IsAxxessTheBiller"), $("#Edit_Insurance_EdiInformation"));
 </script>
