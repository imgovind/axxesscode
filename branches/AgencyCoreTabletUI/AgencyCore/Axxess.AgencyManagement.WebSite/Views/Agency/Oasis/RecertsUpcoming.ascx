﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Upcoming Recerts | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
<%  if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    <div class="float-right">
        <div class="buttons">
            <ul>
                <li><%= Html.ActionLink("Export to Excel", "RecertsUpcoming", "Export", new { BranchId = Guid.Empty, InsuranceId = Model }, new { id = "AgencyUpcomingRecet_ExportLink" })%></li>
            </ul>
        </div>
    </div>
<%  } %>
    <fieldset class="grid-controls">
        <label class="float-left" for="AgencyUpcomingRecet_BranchCode">Branch:</label>
        <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "AgencyUpcomingRecet_BranchCode" })%></div>
        <label class="float-left" for="AgencyUpcomingRecet_InsuranceId">Insurance:</label>
        <%= Html.Insurances("InsuranceId", Model, new { @id = "AgencyUpcomingRecet_InsuranceId", @class = "Insurances fl" })%>
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Agency.RebindAgencyUpcomingRecet();return false">Generate</a></li>
            </ul>
        </div>
        <div class="clear"></div>
        <label class="float-left">Filter by:</label>
        <div id="AgencyUpcomingRecet_Search" class="fl"></div>
    </fieldset>
    <%= Html.Telerik().Grid<RecertEvent>().Name("List_UpcomingRecerts").Columns(columns => {
            columns.Bound(r => r.PatientName).Sortable(true);
            columns.Bound(r => r.PatientIdNumber).Title("MR#").Sortable(true).Width(120);
            columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true);
            columns.Bound(r => r.Status).Title("Status").Sortable(true);
            columns.Bound(r => r.TargetDate).Format("{0:MM/dd/yyyy}").Title("Due Date").Width(120).Sortable(true);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("RecertsUpcoming", "Agency", new { BranchId = Guid.Empty, InsuranceId = Model })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#AgencyUpcomingRecet_Search").append( $("<div/>").GridSearchById("#List_UpcomingRecerts"));
</script>
