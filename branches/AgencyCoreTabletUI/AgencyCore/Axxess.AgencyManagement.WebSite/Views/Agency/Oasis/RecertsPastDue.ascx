﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<span class="wintitle">Past Due Recerts | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    <div class="float-right">
        <div class="buttons">
            <ul>
                <li><%= Html.ActionLink("Export to Excel", "RecertsPastDue", "Export", new { BranchId = Guid.Empty, InsuranceId = Model, StartDate = DateTime.Now.AddDays(-60) }, new { id = "AgencyPastDueRecet_ExportLink" })%></li>
            </ul>
        </div>
    </div>
<%  } %>
    <fieldset class="grid-controls">
        <label class="float-left" for="AgencyPastDueRecet_BranchCode">Branch:</label>
        <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "AgencyPastDueRecet_BranchCode" })%></div>
        <label class="strong" for="AgencyPastDueRecet_StartDate">Date Range:</label>
        <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="AgencyPastDueRecet_StartDate" class="short" />
        <label class="strong" for="AgencyPastDueRecet_EndDate">To</label>
        <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="AgencyPastDueRecet_EndDate" class="short" />
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Agency.RebindAgencyPastDueRecet();return false">Generate</a></li>
            </ul>
        </div>
        <div class="clear"></div>
        <label class="float-left" for="AgencyPastDueRecet_InsuranceId">Insurance:</label>
        <div class="float-left"><%= Html.Insurances("InsuranceId", Model, new { @id = "AgencyPastDueRecet_InsuranceId", @class = "Insurances" })%></div>
        <div id="AgencyPastDueRecerts_Search" class="fl"></div>
    </fieldset>
    <%= Html.Telerik().Grid<RecertEvent>().Name("List_PastDueRecerts").Columns(columns => {
            columns.Bound(r => r.PatientName).Sortable(true);
            columns.Bound(r => r.PatientIdNumber).Title("MR#").Sortable(true).Width(120);
            columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true);
            columns.Bound(r => r.Status).Title("Status").Sortable(true);
            columns.Bound(r => r.TargetDate).Format("{0:MM/dd/yyyy}").Title("Due Date").Width(120).Sortable(true);
            columns.Bound(r => r.DateDifference).Title("Past Dates").Width(60);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("RecertsPastDue", "Agency", new { BranchId = Guid.Empty, InsuranceId=Model , StartDate = DateTime.Now.AddDays(-60) })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
$("#AgencyPastDueRecerts_Search").append( $("<div/>").GridSearchById("#List_PastDueRecerts"));
</script>
