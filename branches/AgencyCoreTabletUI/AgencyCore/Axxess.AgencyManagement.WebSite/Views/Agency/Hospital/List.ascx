﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List Hospital | <%= Current.AgencyName.Clean() %></span>
<% using (Html.BeginForm("Hospitals", "Export", FormMethod.Post)) { %>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyHospital>().Name("List_AgencyHospital").HtmlAttributes(new { @class = "top-gap bottom-bar" }).ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(c => c.Name).Title("Hospital Name").Width(150).Sortable(true);
    columns.Bound(c => c.ContactPerson).Title("Contact Person").Sortable(true);
    columns.Bound(c => c.AddressFull).Title("Address").Sortable(false);
    columns.Bound(c => c.PhoneFormatted).Title("Phone").Width(120).Sortable(false);
    columns.Bound(c => c.FaxFormatted).Title("Fax Number").Width(120).Sortable(false);
    columns.Bound(c => c.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(150).Sortable(true);
    columns.Bound(c => c.Id).ClientTemplate("<a onclick=\"UserInterface.ShowEditHospital('<#=Id#>');return false\">Edit</a> | <a onclick=\"Hospital.Delete('<#=Id#>');return false\" class=\"deleteContact\">Delete</a>").Title("Action").Sortable(false).Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Hospital")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
$("#List_AgencyHospital .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageHospital)) { %>.append(
    $("<div/>").addClass("float-left").Buttons([ { Text: "New Hospital", Click: UserInterface.ShowNewHospital } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float-right").Buttons([ { Text: "Excel Export", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
