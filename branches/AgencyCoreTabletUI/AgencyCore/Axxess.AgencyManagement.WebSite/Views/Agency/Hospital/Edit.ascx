﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyHospital>" %>
<span class="wintitle">Edit Hospital<%= Model != null ? " | " + Model.Name.ToTitleCase().Clean() : string.Empty %></span>
<%  using (Html.BeginForm("Update", "Hospital", FormMethod.Post, new { @id = "editHospitalForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Hospital_Id" }) %>
<div class="wrapper main">
    <fieldset>
        <legend>Hospital Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Hospital_Name" class="float-left">Hospital Name:</label><div class="float-right"> <%= Html.TextBox("Name", Model.Name, new { @id = "Edit_Hospital_Name", @class = "text input_wrapper required", @maxlength = "100" })%></div></div>
            <div class="row"><label for="Edit_Hospital_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"> <%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Hospital_AddressLine1", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Hospital_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"> <%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Hospital_AddressLine2", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Hospital_AddressCity" class="float-left">City:</label><div class="float-right"> <%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Hospital_AddressCity", @class = "text input_wrapper required", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Hospital_AddressStateCode" class="float-left"> State, Zip:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Hospital_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Hospital_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div></div>
         </div>   
        <div class="column">   
            <div class="row"><label for="Edit_Hospital_ContactPersonFirstName" class="float-left">Contact First Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "Edit_Hospital_ContactPersonFirstName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Hospital_ContactPersonLastName" class="float-left">Contact Last Name:</label><div class="float-right"><%= Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "Edit_Hospital_ContactPersonLastName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="Edit_Hospital_EmailAddress" class="float-left">Email :</label><div class="float-right"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_Hospital_EmailAddress", @class = "text email input_wrapper", @maxlength = "50" })%></div></div>
            <div class="row"><label for="Edit_Hospital_PhoneArray1" class="float-left">Primary Phone:</label><div class="float-right"> <%= Html.TextBox("PhoneArray", Model.Phone.IsNotNullOrEmpty() ? Model.Phone.Substring(0, 3) : "", new { @id = "Edit_Hospital_PhoneArray1", @class = "input_wrappermultible autotext required digits phone-short", @maxlength = "3", @size = "4" })%> - <%= Html.TextBox("PhoneArray", Model.Phone.IsNotNullOrEmpty() ? Model.Phone.Substring(3, 3) : "", new { @id = "Edit_Hospital_PhoneArray2", @class = "input_wrappermultible autotext required digits phone-short", @maxlength = "3", @size = "3" })%> - <%= Html.TextBox("PhoneArray", Model.Phone.IsNotNullOrEmpty() ? Model.Phone.Substring(6, 4) : "", new { @id = "Edit_Hospital_PhoneArray3", @class = "input_wrappermultible autotext required digits phone-long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Hospital_FaxNumberArray1" class="float-left">Fax Number:</label><div class="float-right"> <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Hospital_FaxNumberArray1", @class = "input_wrappermultible autotext digits phone-short", @maxlength = "3", @size = "4" })%> - <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Hospital_FaxNumberArray2", @class = "input_wrappermultible autotext digits phone-short", @maxlength = "3", @size = "3" })%> - <%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Hospital_FaxNumberArray3", @class = "input_wrappermultible autotext digits phone-long", @maxlength = "4", @size = "5" })%></div></div> 
        </div>
        <table class="form"><tbody><tr class="linesep vert"><td><label for="Edit_Hospital_Comments">Comment:</label><div><%= Html.TextArea("Comment", Model.Comment, new { @id = "Edit_Hospital_Comments" })%></div></td></tr></tbody></table>
    </fieldset>
    <div class="activity-log"><% = string.Format("<span class=\"img icon activity\" onclick=\"Log.LoadHospitalLog('{0}')\" title=\"Activity Logs\"></span>", Model.Id)%></div>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<%} %>

