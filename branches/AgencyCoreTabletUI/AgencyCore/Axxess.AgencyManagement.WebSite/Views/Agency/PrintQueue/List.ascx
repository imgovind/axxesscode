﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Print Queue | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
    <div class="float-right buttons">
        <ul>
            <li><a onclick="User.RebindPrintQueue();return false">Refresh</a></li>
            <li><a href="Export/PrintQueueList">Excel Export</a></li>
        </ul>
    </div>
    <fieldset class="grid-controls">
        <div class="buttons">
            <ul>
                <li><a onclick="User.loadPrintQueue('PatientName');return false">Group By Patient</a></li>
                <li><a onclick="User.loadPrintQueue('VisitDate');return false">Group By Date</a></li>
                <li><a onclick="User.loadPrintQueue('DisciplineTaskName');return false">Group By Task</a></li>
                <li><a onclick="Agency.loadPrintQueue('UserName');return false">Group By Clinician</a></li>
            </ul>
        </div>
    </fieldset>
    <div id="printQueueContentId"><% Html.RenderPartial("~/Views/Agency/PrintQueue/Content.ascx", Model); %></div>
    <div class="buttons abs-bottom">
<%  using (Html.BeginForm("BulkUpdate", "Schedule", FormMethod.Post, new { @id = "printQueueForm" })) { %>
        <%= Html.Hidden("CommandType", "Print", new {@id="PrintQueueUpdate_Type" })%>
        <ul>
            <li><a onclick="MarkAsPrinted('Print');return false">Mark As Printed</a></li>
        </ul>
<%  } %>
    </div>
</div>
<script type="text/javascript">
    $("#printQueueContentId .t-group-indicator").hide();
    $("#printQueueContentId .t-grouping-header").remove();
    function MarkAsPrinted(type) {
        if ($("input[name=CustomValue]:checked").length > 0) {
            Schedule.BulkUpdate('#printQueueForm');
        } else { U.Growl("Select at least one item to mark as printed.", "error"); }
    }
</script>