﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<span class="wintitle">List Location | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyLocation>().Name("List_Location").HtmlAttributes(new { @class = "top-gap bottom-bar" }).ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(l => l.Name).Title("Company Name").Sortable(false);
    columns.Bound(l => l.MedicareProviderNumber).Title("Provider Number").Sortable(false).Width(150);
    columns.Bound(l => l.AddressFull).Title("Address").Sortable(true);
    columns.Bound(l => l.PhoneWork).Title("Phone Number").Width(120);
    columns.Bound(l => l.FaxNumber).Title("Fax Number").Width(120);
    columns.Bound(l => l.Id).ClientTemplate("<a onclick=\"UserInterface.ShowEditLocation('<#=Id#>');return false\">Edit</a>").Title("Action").Width(100);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Location")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $("#List_Location .t-grid-toolbar").html("");
    $(".t-grid-content").css({ 'height': 'auto' });
</script>
