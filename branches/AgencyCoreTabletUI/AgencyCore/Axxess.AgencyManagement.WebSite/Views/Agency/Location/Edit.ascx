﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<span class="wintitle">Edit Location | <%= Model != null ? Model.Name.ToTitleCase().Clean() : "" %></span>
<%  using (Html.BeginForm("Update", "Location", FormMethod.Post, new { @id = "editLocationForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Location_Id" })%>
<div class="wrapper main">

    <fieldset>
        <legend>Location Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Location_Name" class="float-left">Location Name:</label><div class="float-right"><%= Html.TextBox("Name", Model.Name, new { @id = "Edit_Location_Name", @class = "text input_wrapper required", @maxlength = "20" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Location_CustomId" class="float-left">Custom ID:</label><div class="float-right"><%= Html.TextBox("CustomId", Model.CustomId, new { @id = "Edit_Location_CustomId", @class = "text input_wrapper" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Location Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Location_AddressLine1" class="float-left">Address Line 1:</label><div class="float-right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Location_AddressLine1", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressLine2" class="float-left">Address Line 2:</label><div class="float-right"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Location_AddressLine2", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressCity" class="float-left">City:</label><div class="float-right"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Location_AddressCity", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressStateCode" class="float-left">State, Zip:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Location_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Location_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Location_PhoneArray1" class="float-left">Primary Phone:</label><div class="float-right"><%= Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(0, 3) : "", new { @id = "Edit_Location_PhoneArray1", @class = "input_wrappermultible autotext required digits phone-short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(3, 3) : "", new { @id = "Edit_Location_PhoneArray2", @class = "input_wrappermultible autotext required digits phone-short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(6, 4) : "", new { @id = "Edit_Location_PhoneArray3", @class = "input_wrappermultible autotext required digits phone-long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Location_FaxNumberArray1" class="float-left">Fax Number:</label><div class="float-right"><%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Location_FaxNumberArray1", @class = "input_wrappermultible autotext  digits phone-short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Location_FaxNumberArray2", @class = "input_wrappermultible autotext  digits phone-short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Location_FaxNumberArray3", @class = "input_wrappermultible autotext  digits phone-long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
        <table class="form"><tbody>
            <tr class="linesep vert">
                <td><label for="Edit_Location_Comments">Comments:</label><div><%= Html.TextArea("Comments", Model.Comments, new { @id = "Edit_Location_Comments" })%></div></td>
            </tr>
        </tbody></table>
    </fieldset>
    <div class="activity-log"><% = string.Format("<span class=\"img icon activity\" onclick=\"Log.LoadLocationLog('{0}')\" title=\"Activity Logs\"></span>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a class="save">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul></div>
</div>
<% } %>