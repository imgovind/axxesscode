﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="winmenu">
    <ul>
        <li><a>Inbox</a></li>
        <li><a>Compose Mail</a></li>
        <li><a>Delete</a></li>
    </ul>
</div>
<ul class="widget-message-list"></ul>
<div class="widget-more"><a>More &#187;</a></div>