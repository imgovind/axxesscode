﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th>Patient Name</th>
				<th>MR#</th>
				<th>Due Date</th>
			</tr>
        </thead>
	    <tbody id="recertPastDueWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div onclick="UserInterface.ShowPastDueRecerts();" id="pastDueRecertsMore" class="widget-more"><a href="javascript:void(0);">More &#187;</a></div>
