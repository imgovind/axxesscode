﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Existing Referral List | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main">
    <div id="List_Referral"></div>
</div>
<script type="text/javascript">
    $("#List_Referral").AcoreGrid({
        Id: "List_Referral",
        Layout: [
            { Title: "Referral Date", Width: "11%", Data: "ReferralDate" },
            { Title: "Name", Width: "14%", Data: "DisplayName" },
            { Title: "Referral Source", Width: "16%", Data: "AdmissionSource" },
            { Title: "Date of Birth", Width: "10%", Data: "DateOfBirth" },
            { Title: "Gender", Width: "5%", Data: "Gender" },
            { Title: "Status", Width: "8%", Data: "Status" },
            { Title: "Created By", Width: "17%", Data: "CreatedBy" },
            { Title: "Action", Width: "19%", Format: "<a class='link' onclick=\"UserInterface.ShowEditReferral('<#=Id#>');return false\">Edit</a> | <a class='link' onclick=\"Referral.Delete('<#=Id#>');return false\">Delete</a> | <a class='link' onclick=\"Referral.Admit('<#=Id#>');return false\">Admit</a> | <a class='link' onclick=\"UserInterface.ShowNonAdmitReferralModal('<#=Id#>');return false\">Non-Admit</a>" }
        ],
        Url: "Referral/Grid"
    });
</script>


