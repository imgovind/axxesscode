﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<% using (Html.BeginForm("AddNonAdmit", "Referral", FormMethod.Post, new { @id = "newNonAdmitReferralForm" }))   { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "NonAdmit_Referral_Id" })%>
<%= Html.Hidden("IsAdmit", "false", new { @id = "NonAdmit_Referral_IsAdmit" })%>
<div class="wrapper main">

    <fieldset>
        <legend>Non-Admission Details</legend>
        <div class="column">
            <div class="row">
                <span class="bigtext align-center"><%= Model.DisplayName %></span>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="NonAdmit_Referral_Date" class="float-left">Date:</label>
                <div class="float-right"><input type="date" name="NonAdmitDate" id="NonAdmit_Referral_Date" class="required" /></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide-column">
            <div class="row">
                <label for="Comment" class="strong">Reason Not Admitted:</label>
                <div class="checkgroup">
                    <div class="option">
                        <input id="NonAdmit_Referral_ReasonInAppropriate" type="checkbox" value="Inappropriate For Home Care" name="Reason" class="required radio float-left" />
                        <label for="NonAdmit_Referral_ReasonInAppropriate" class="radio">Inappropriate For Home Care</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmit_Referral_ReasonRefused" type="checkbox" value="Referral Refused Service" name="Reason" class="required radio float-left" />
                        <label for="NonAdmit_Referral_ReasonRefused" class="radio">Referral Refused Service</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmit_Referral_ReasonOutofService" type="checkbox" value="Out of Service Area" name="Reason" class="required radio float-left" />
                        <label for="NonAdmit_Referral_ReasonOutofService" class="radio">Out of Service Area</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmit_Referral_ReasonOnService" type="checkbox" value="On Service with another agency" name="Reason" class="required radio float-left" />
                        <label for="NonAdmit_Referral_ReasonOnService" class="radio">On Service with another agency</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmit_Referral_ReasonNotaProvider" type="checkbox" value="Not a Provider" name="Reason" class="required radio float-left" />
                        <label for="NonAdmit_Referral_ReasonNotaProvider" class="radio">Not a Provider</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmit_Referral_ReasonNotHomebound" type="checkbox" value="Not Homebound" name="Reason" class="required radio float-left" />
                        <label for="NonAdmit_Referral_ReasonNotHomebound" class="radio">Not Homebound</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmit_Referral_ReasonRedirected" type="checkbox" value="Redirected to alternate care facility" name="Reason" class="required radio float-left" />
                        <label for="NonAdmit_Referral_ReasonRedirected" class="radio">Redirected to alternate care facility</label>
                    </div>
                    <div class="option">
                        <input id="NonAdmit_Referral_ReasonOther" type="checkbox" value="Other" name="Reason" class="required radio float-left" />
                        <label for="NonAdmit_Referral_ReasonOther" class="radio">Other (specify in Comments)</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="NonAdmit_Referral_Comments" class="strong">Comments:</label>
                <%= Html.TextArea("Comments", "", new { @id = "NonAdmit_Referral_Comments" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a class="save">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul></div>
</div>
<%} %>

