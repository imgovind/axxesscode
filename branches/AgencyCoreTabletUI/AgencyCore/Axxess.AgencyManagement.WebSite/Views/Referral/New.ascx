﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">New Referral | <%= Current.AgencyName.Clean() %></span>
<% using (Html.BeginForm("Add", "Referral", FormMethod.Post, new { @id = "newReferralForm" })) { %>
<div class="wrapper main">

    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="New_Referral_Physician" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("ReferrerPhysician", "", new { @id = "New_Referral_Physician", @class = "physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
            </div>
            <div class="row">
                <label for="New_Referral_AdmissionSource" class="float-left">Admission Source:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", "", new { @id = "New_Referral_AdmissionSource", @class = "AdmissionSource" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_Referral_OtherReferralSource" class="float-left">Other Referral Source:</label>
                <div class="float-right"><%= Html.TextBox("OtherReferralSource", "", new { @id = "New_Referral_OtherReferralSource", @class = "text input_wrapper", @maxlength = "50" })%></div>
            </div><div class="row">
                <label for="New_Referral_Date" class="float-left">Referral Date:</label>
                <div class="float-right"><input type="date" name="ReferralDate" id="New_Referral_Date" /></div>
            </div><div class="row">
                <label for="New_Referral_InternalReferral" class="float-left">Internal Referral:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", "", new { @id = "New_Referral_InternalReferral", @class = "Users valid" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="New_Referral_FirstName" class="float-left">First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", "", new { @id = "New_Referral_FirstName", @maxlength = "50", @class = "required" }) %></div>
            </div><div class="row">
                <label for="New_Referral_LastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", "", new { @id = "New_Referral_LastName", @maxlength = "50", @class = "required" }) %></div>
            </div><div class="row">
                <label for="New_Referral_MedicareNo" class="float-left">Medicare No:</label>
                <div class="float-right"><%= Html.TextBox("MedicareNumber", " ", new { @id = "New_Referral_MedicareNo", @maxlength = "20", @class = "text MedicareNo" })%></div>
            </div><div class="row">
                <label for="New_Referral_MedicaidNo" class="float-left">Medicaid No:</label>
                <div class="float-right"><%= Html.TextBox("MedicaidNumber", " ", new { @id = "New_Referral_MedicaidNo", @maxlength = "20", @class = "text MedicaidNo" })%></div>
            </div><div class="row">
                <label for="New_Referral_SSN" class="float-left">SSN:</label>
                <div class="float-right"><%= Html.TextBox("SSN", "", new { @id = "New_Referral_SSN", @maxlength = "9" }) %></div>
            </div><div class="row">
                <label for="New_Referral_DateOfBirth" class="float-left">Date of Birth:</label>
                <div class="float-right"><input type="date" name="DOB" id="New_Referral_DateOfBirth" class="required" /></div>
            </div><div class="row">
                <label class="float-left">Gender:</label>
                <div class="float-right">
                    <%= Html.RadioButton("Gender", "Female", new { @id = "New_Referral_Gender_F", @class = "radio required" }) %>
                    <label for="New_Referral_Gender_F" class="inline-radio">Female</label>
                    <%= Html.RadioButton("Gender", "Male", new { @id = "New_Referral_Gender_M", @class = "radio required" })%>
                    <label for="New_Referral_Gender_M" class="inline-radio">Male</label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_Referral_HomePhone1" class="float-left">Home Phone:</label>
                <div class="float-right">
                    <input type="text" class="autotext numeric required phone-short" name="PhoneHomeArray" id="New_Referral_HomePhone1" maxlength="3" />
                    -
                    <input type="text" class="autotext numeric required phone-short" name="PhoneHomeArray" id="New_Referral_HomePhone2" maxlength="3" />
                    -
                    <input type="text" class="autotext numeric required phone-long" name="PhoneHomeArray" id="New_Referral_HomePhone3" maxlength="4" />
                </div>
            </div><div class="row">
                <label for="New_Referral_Email" class="float-left">Email Address:</label>
                <div class="float-right"><%= Html.TextBox("EmailAddress", "", new { @id = "New_Referral_Email", @class = "text email input_wrapper", @maxlength = "50" })%></div>
            </div><div class="row">
                <label for="New_Referral_AddressLine1" class="float-left">Address Line 1:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine1", "", new { @id = "New_Referral_AddressLine1", @maxlength = "50", @class = "text required input_wrapper" }) %></div>
            </div><div class="row">
                <label for="New_Referral_AddressLine2" class="float-left">Address Line 2:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine2", "", new { @id = "New_Referral_AddressLine2", @maxlength = "50", @class = "text input_wrapper" }) %></div>
            </div><div class="row">
                <label for="New_Referral_AddressCity" class="float-left">City:</label>
                <div class="float-right"><%= Html.TextBox("AddressCity", "", new { @id = "New_Referral_AddressCity", @maxlength = "50", @class = "text required input_wrapper" }) %></div>
            </div><div class="row">
                <label for="New_Referral_AddressStateCode" class="float-left">State, Zip:</label>
                <div class="float-right">
                    <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Referral_AddressStateCode", @class = "AddressStateCode required notzero valid" })%>
                    <%= Html.TextBox("AddressZipCode", "", new { @id = "New_Referral_AddressZipCode", @class = "text numeric required input_wrapper zip", @size = "5", @maxlength = "5" })%>
                </div>
            </div><div class="row">
                <label for="New_Referral_Assign" class="float-left">Assign to Clinician:</label>
                <div class="float-right"><%= Html.Clinicians("UserId", "", new { @id = "New_Referral_Assign", @class = "Users required valid" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Services Required</legend>
        <div class="wide-column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <input type="checkbox" value="0" class="radio fl" id="ServicesRequiredCollection0" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection0" class="radio">SNV</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="1" class="radio fl" id="ServicesRequiredCollection1" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection1" class="radio">HHA</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="2" class="radio fl" id="ServicesRequiredCollection2" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection2" class="radio">PT</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="3" class="radio fl" id="ServicesRequiredCollection3" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection3" class="radio">OT</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="4" class="radio fl" id="ServicesRequiredCollection4" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection4" class="radio">SP</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="5" class="radio fl" id="ServicesRequiredCollection5" name="ServicesRequiredCollection" />
                        <label for="ServicesRequiredCollection5" class="radio">MSW</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>DME Needed</legend>
        <div class="wide-column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <input type="checkbox" value="0" class="radio float-left" id="DMECollection0" name="DMECollection" />
                        <label for="DMECollection0" class="radio">Bedside Commode</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="1" class="radio float-left" id="DMECollection1" name="DMECollection" />
                        <label for="DMECollection1" class="radio">Cane</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="2" class="radio float-left" id="DMECollection2" name="DMECollection" />
                        <label for="DMECollection2" class="radio">Elevated Toilet Seat</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="3" class="radio float-left" id="DMECollection3" name="DMECollection" />
                        <label for="DMECollection3" class="radio">Grab Bars</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="4" class="radio float-left" id="DMECollection4" name="DMECollection" />
                        <label for="DMECollection4" class="radio">Hospital Bed</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="5" class="radio float-left" id="DMECollection5" name="DMECollection" />
                        <label for="DMECollection5" class="radio">Nebulizer</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="6" class="radio float-left" id="DMECollection6" name="DMECollection" />
                        <label for="DMECollection6" class="radio">Oxygen</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="7" class="radio float-left" id="DMECollection7" name="DMECollection" />
                        <label for="DMECollection7" class="radio">Tub/Shower Bench</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="8" class="radio float-left" id="DMECollection8" name="DMECollection" />
                        <label for="DMECollection8" class="radio">Walker</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="9" class="radio float-left" id="DMECollection9" name="DMECollection" />
                        <label for="DMECollection9" class="radio">Wheelchair</label>
                    </div>
                    <div class="option">
                        <input type="checkbox" value="10" id="New_Referral_DMEOther" class="radio" name="DMECollection" />
                        <label for="New_Referral_DMEOther" class="radio more">Other</label>
                        <%= Html.TextBox("OtherDME", "", new { @id = "New_Referral_OtherDME", @class = "text", @style="display:none;", @maxlength="50"}) %>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Referral_PhysicianDropDown1" class="float-left">Primary Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Referral_PhysicianDropDown1", @class = "physicians" })%><a class="abs addrem" onclick="Patient.addPhysician(this);return false" status="Add Row">+</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown2" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Referral_PhysicianDropDown2", @class = "physicians" })%><a class="abs addrem" onclick="Patient.removePhysician(this);return false" status="Remove Row">-</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown3" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Referral_PhysicianDropDown3", @class = "physicians" })%><a class="abs addrem" onclick="Patient.removePhysician(this);return false" status="Remove Row">-</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown4" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Referral_PhysicianDropDown4", @class = "physicians" })%><a class="abs addrem" onclick="Patient.removePhysician(this);return false" status="Remove Row">-</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Referral_PhysicianDropDown5" class="float-left">Additional Physician:</label>
                <div class="float-right"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "New_Referral_PhysicianDropDown5", @class = "physicians" })%><a class="abs addrem" onclick="Patient.removePhysician(this);return false" status="Remove Row">-</a></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac">
                <textarea id="New_Referral_Comments" name="Comments" cols="5" rows="6" maxcharacters="500" class="tall"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Add Referral</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<%  } %>
