﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<span class="wintitle">Edit Referral | <%= Model != null ? (Model.LastName + ", " + Model.FirstName).ToTitleCase().Clean() : "" %></span>
<%  using (Html.BeginForm("Update", "Referral", FormMethod.Post, new { @id = "editReferralForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Referral_Id" }) %>
<div class="wrapper main">
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Referral_Physician" class="float-left">Physician:</label>
                <div class="float-right"><%= Html.TextBox("ReferrerPhysician",Model.ReferrerPhysician.ToString(), new { @id = "Edit_Referral_Physician", @class = "physicians" })%></div>
                <div class="clear"></div>
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
            </div>
            <div class="row"><label for="Edit_Referral_AdmissionSource" class="float-left">Admission Source:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", Model.AdmissionSource.ToString(), new { @id = "Edit_Referral_AdmissionSource", @class = "AdmissionSource" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="Edit_Referral_OtherReferralSource" class="float-left">Other Referral Source:</label><div class="float-right"><%= Html.TextBox("OtherReferralSource", Model.OtherReferralSource, new { @id = "Edit_Referral_OtherReferralSource", @class = "text input_wrapper", @maxlength = "50" })%></div></div>
            <div class="row"><label for="Edit_Referral_Date" class="float-left">Referral Date:</label><div class="float-right"><input type="date" name="ReferralDate" value="<%= Model.ReferralDate.ToShortDateString() %>" id="Edit_Referral_Date" /></div></div>
            <div class="row"><label for="Edit_Referral_InternalReferral" class="float-left">Internal Referral:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", Model.InternalReferral.ToString(), new { @id = "Edit_Referral_InternalReferral", @class = "Users valid" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="Edit_Referral_FirstName" class="float-left">First Name:</label>
                <div class="float-right"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_Referral_FirstName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_LastName" class="float-left">Last Name:</label>
                <div class="float-right"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_Referral_LastName", @maxlength = "50", @class = "required" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_MedicareNo" class="float-left">Medicare No:</label>
                <div class="float-right"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "Edit_Referral_MedicareNo", @maxlength = "11", @class = "text MedicareNo" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_MedicaidNo" class="float-left">Medicaid No:</label>
                <div class="float-right"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "Edit_Referral_MedicaidNo", @maxlength = "20", @class = "text MedicaidNo" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_SSN" class="float-left">SSN:</label>
                <div class="float-right"><%= Html.TextBox("SSN", Model.SSN, new { @id = "Edit_Referral_SSN", @maxlength = "9" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_DateOfBirth" class="float-left">Date of Birth:</label>
                <div class="float-right"><input type="date" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="Edit_Referral_DateOfBirth" class="required" /></div>
            </div>
            <div class="row">
                <label class="float-left">Gender:</label>
                <div class="float-right">
                    <%= Html.RadioButton("Gender", "Female", new { @id = "Edit_Referral_Gender_F", @class = "radio required" }) %>
                    <label for="Edit_Referral_Gender_F" class="inline-radio">Female</label>
                    <%= Html.RadioButton("Gender", "Male", new { @id = "Edit_Referral_Gender_M", @class = "radio required" }) %>
                    <label for="Edit_Referral_Gender_M" class="inline-radio">Male</label>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Edit_Referral_HomePhone1" class="float-left">Home Phone:</label>
                <div class="float-right"><span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(0, 3) : "", new { @id = "Edit_Referral_HomePhone1", @class = "autotext required digits phone-short", @maxlength = "3", @size = "3" })%> </span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(3, 3) : "", new { @id = "Edit_Referral_HomePhone2", @class = "autotext required digits phone-short", @maxlength = "3", @size = "3" })%> </span>- <span class="input_wrappermultible"> <%= Html.TextBox("PhoneHomeArray", Model.PhoneHome != null && Model.PhoneHome != "" ? Model.PhoneHome.Substring(6, 4) : "", new { @id = "Edit_Referral_HomePhone3", @class = "autotext required digits phone-long", @maxlength = "4", @size = "5" })%></span></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_Email" class="float-left">Email Address:</label>
                <div class="float-right"><%= Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_Referral_Email", @class = "text email input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_AddressLine1" class="float-left">Address Line 1:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Referral_AddressLine1", @maxlength = "50", @class = "text required input_wrapper" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_AddressLine2" class="float-left">Address Line 2:</label>
                <div class="float-right"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Referral_AddressLine2", @maxlength = "50", @class = "text input_wrapper" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_AddressCity" class="float-left">City:</label>
                <div class="float-right"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Referral_AddressCity", @maxlength = "50", @class = "text required input_wrapper" }) %></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_AddressStateCode" class="float-left">State, Zip:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Referral_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Referral_AddressZipCode", @class = "text numeric required input_wrapper zip", @size = "5", @maxlength = "5" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Referral_Assign" class="float-left">Assign to Clinician:</label>
                <div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "Edit_Referral_Assign", @class = "Users required valid" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Services Required</legend>
        <div class="wide-column">
            <div class="row">
                <%  string[] servicesRequired = Model.ServicesRequired != null && Model.ServicesRequired != "" ? Model.ServicesRequired.Split(';') : null; %>
                <input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection0' type='checkbox' value='0' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("0") ? "checked='checked'" : "" )%>
                        <label for="ServicesRequiredCollection0" class="radio">SNV</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection1' type='checkbox' value='1' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("1") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection1" class="radio">HHA</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection2' type='checkbox' value='2' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("2") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection2" class="radio">PT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection3' type='checkbox' value='3' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("3") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection3" class="radio">OT</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection4' type='checkbox' value='4' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("4") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection4" class="radio">SP</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id ='ServicesRequiredCollection5' type='checkbox' value='5' class='radio float-left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("5") ? "checked='checked'" : "")%>
                        <label for="ServicesRequiredCollection5" class="radio">MSW</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>DME Needed</legend>
        <div class="wide-column">
            <div class="row">
                <%  string[] DME = Model.DME != null && Model.DME != "" ? Model.DME.Split(';') : null; %>
                <input type="hidden" value=" " class="radio" name="DMECollection" />
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= string.Format("<input id='DMECollection0' type='checkbox' value='0' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("0") ? "checked='checked'" : "")%>
                        <label for="DMECollection0" class="radio">Bedside Commode</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection1' type='checkbox' value='1' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("1") ? "checked='checked'" : "")%>
                        <label for="DMECollection1" class="radio">Cane</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection2' type='checkbox' value='2' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("2") ? "checked='checked'" : "")%>
                        <label for="DMECollection2" class="radio">Elevated Toilet Seat</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection3' type='checkbox' value='3' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("3") ? "checked='checked'" : "")%>
                        <label for="DMECollection3" class="radio">Grab Bars</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection4' type='checkbox' value='4' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("4") ? "checked='checked'" : "")%>
                        <label for="DMECollection4" class="radio">Hospital Bed</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection5' type='checkbox' value='5' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("5") ? "checked='checked'" : "")%>
                        <label for="DMECollection5" class="radio">Nebulizer</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection6' type='checkbox' value='6' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("6") ? "checked='checked'" : "")%>
                        <label for="DMECollection6" class="radio">Oxygen</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection7' type='checkbox' value='7' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("7") ? "checked='checked'" : "")%>
                        <label for="DMECollection7" class="radio">Tub/Shower Bench</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection8' type='checkbox' value='8' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("8") ? "checked='checked'" : "")%>
                        <label for="DMECollection8" class="radio">Walker</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection9' type='checkbox' value='9' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("9") ? "checked='checked'" : "")%>
                        <label for="DMECollection9" class="radio">Wheelchair</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input id='DMECollection10' type='checkbox' value='10' class='radio float-left' name='DMECollection' {0} />", DME != null && DME.Contains("10") ? "checked='checked'" : "")%>
                        <label for="DMECollection10" class="radio">Other</label>
                        <%= Html.TextBox("OtherDME", Model.OtherDME, new { @id = "Edit_Referral_DMEOther", @class = "text", @style = "display:none;" })%>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="medication">
        <legend>Physicians</legend>
        <div class="wide-column">
            <div class="row">
                <div class="float-left">
                    <%= Html.TextBox("AgencyPhysicians", "", new { @id = "EditReferral_PhysicianSelector", @class = "physicians" }) %>
                </div>
                <div class="buttons float-left">
                    <ul>
                        <li>
                            <a id="EditReferral_NewPhysician">Add Selected Physician</a>
                        </li>
                    </ul>
                </div>
                <div class="float-right button-with-arrow"><a onclick="UserInterface.ShowNewPhysicianModal();return false">New Physician</a></div>
                <div class="clear"></div>
                <%= Html.Telerik().Grid<AgencyPhysician>().Name("EditReferral_PhysicianGrid").Columns(columns => {
                        columns.Bound(c => c.FirstName);
                        columns.Bound(c => c.LastName);
                        columns.Bound(c => c.PhoneWorkFormatted).Title("Work Phone");
                        columns.Bound(c => c.FaxNumberFormatted);
                        columns.Bound(c => c.EmailAddress);
                        columns.Bound(c => c.Id).ClientTemplate("<a onclick=\"Referral.DeletePhysician('<#=Id#>','" + Model.Id + "');return false\" class=\"deleteReferral\">Delete</a> | <a onclick=\"Referral.SetPrimaryPhysician('<#=Id #>','" + Model.Id + "');return false\" class=<#= !Primary ? \"\" : \"hidden\" #>><#=Primary ? \"\" : \"Make Primary\" #></a>").Title("Action").Width(135);
                    }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetPhysicians", "Referral", new { ReferralId = Model.Id })).Sortable().Footer(false) %>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide-column">
            <div class="row ac">
                <textarea id="Edit_Referral_Comments" name="Comments" cols="5" rows="6" maxcharacters="500" class="tall"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<span class=\"img icon activity\" onclick=\"Log.LoadReferralLog('{0}')\" title=\"Activity Logs\"></span>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a class="save">Save</a></li>
        <li><a class="close">Cancel</a></li>
    </ul></div>
</div>
<%= string.Format("<script type='text/javascript'> if({0}==1) {{ $(\"#Edit_Referral_DMEOther\").show();}} else {{ $(\"#Edit_Referral_DMEOther\").hide(); }}</script>", DME != null && DME.Contains("9") ? 1 : 0) %>
<% } %>
<!--[if !IE]>end forms<![endif]-->
<script type="text/javascript">
    $("#DMECollection10").click(function() {
        var otherDme = $('#DMECollection10:checked').is(':checked');
        if (!otherDme) $("#Edit_Referral_DMEOther").hide();
        else $("#Edit_Referral_DMEOther").show();
    })
</script>