﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PayrollDetail>" %>
<div class="payroll">
    <ul>
        <li class="align-center"><span id="payrollUserId" class="very-hidden"><%= Model.Id %></span><%= Model.Name.IsNotNullOrEmpty() ? Model.Name : "" %> [<a onclick="$('#payrollSearchResultDetails').hide();$('#payrollSearchResultDetail').hide();$('#payrollMarkAsPaidButton').hide();$('#payrollSearchResult').show();return false">Back to Search Results</a>] <%= Html.ActionLink("[Export to Excel]", "PayrollDetail", "Export", new { UserId = Model.Id, StartDate = Model.StartDate, EndDate = Model.EndDate, payrollStatus= Model.PayrollStatus }, new { id = "ExportPayrollDetail_ExportLink" })%>  <a onclick="U.GetAttachment('Payroll/SummaryDetailPdf', { 'userId': '<%=Model.Id %>', 'payrollStartDate': '<%=Model.StartDate %>', 'payrollEndDate': '<%=Model.EndDate %>', 'payrollStatus': '<%=Model.PayrollStatus %>' });return false"> [ Print PDF ]</a></li>
        <li>
            <span class="payrollcheckbox"></span>
            <span class="payrolldate strong">Schedule Date</span>
            <span class="payrolldate strong">Visit Date</span>
            <span class="payrollname strong">Patient Name</span>
            <span class="payrolltask strong">Task</span>
            <span class="payrolltime strong">Time</span>
            <span class="payrolltotaltime strong">Total Min.</span>
            <span class="payrollmileage strong">Mileage</span>
            <span class="payrollstatus strong">Status</span>
            <span class="payrollicon strong">Paid</span>
        </li>
    </ul>
    <ol><%
    if (Model.Visits != null && Model.Visits.Count > 0) {
        int i = 1;
        var totalTime = 0;
        foreach (var visit in Model.Visits) { %>
        <%= i % 2 != 0 ? "<li class='odd'>" : "<li class='even'>" %>
            <span class="payrollcheckbox"><%= Html.CheckBox("visitSelected", false, new { @id = "visitSelected" + i, @class = "radio", @value = string.Format("{0}_{1}_{2}", visit.EpisodeId, visit.PatientId, visit.Id) })%></span>
            <span class="payrolldate"><%= visit.ScheduleDate %></span>
            <span class="payrolldate"><%= visit.VisitDate %></span>
            <span class="payrollname"><%= visit.PatientName %></span>
            <span class="payrolltask"><%= visit.TaskName %></span>
            <span class="payrolltime"><%= visit.TimeIn.IsNotNullOrEmpty() && visit.TimeOut.IsNotNullOrEmpty() ? string.Format("{0} - {1}", visit.TimeIn, visit.TimeOut) : string.Empty %></span>
            <span class="payrolltotaltime"><%= visit.MinSpent %></span>
            <span class="payrollmileage"><%= visit.AssociatedMileage %></span>
            <span class="payrollstatus"><%= visit.StatusName %></span>
            <span class="payrollicon"><span class='img icon <%= visit.IsVisitPaid ? "success-small" : "error-small" %>'></span></span>
        </li><%
                 i++; totalTime += visit.MinSpent;
        } 
        
        %>
        <%= i % 2 != 0 ? "<li class='odd'>" : "<li class='even'>" %>
            <span class="payrolltotaltimetitle">Total Time :</span>
            <span class="payrolltotalhour"><%=string.Format(" {0} min =  {1:#0.00} hour(s)", totalTime, (double)totalTime / 60)%></span>
        </li>
    <% } else { %>
        <li class="align-center">No Results found</li><%
    } %>
    </ol>
</div>
<script type="text/javascript">
    $(".payroll ol").each(function() { $("li:first", this).addClass("first"); $("li:last", this).addClass("last"); });
</script>