﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DateTime>" %>
<span class="wintitle">Payroll Summary | <%= Current.AgencyName.Clean() %></span>
<%  using (Html.BeginForm("Search", "Payroll", FormMethod.Post, new { @id = "searchPayrollForm" })) { %>
<div id="Payroll_SummaryContent" class="main wrapper align-center">
    <fieldset class="grid-controls">
        <label class="strong" for="payrollStartDate">Date Range:</label>
        <input type="date" id="Date1" name="payrollStartDate" value="<%= Model.ToShortDateString() %>" class="required short" />
        <label class="strong" for="payrollEndDate">To</label>
        <input type="date" id="Date2" name="payrollEndDate" value="<%= DateTime.Today.ToShortDateString() %>" maxdate="<%= DateTime.Today.ToShortDateString() %>" class="required short" />
        <label for="payrollStatus">Staus: </label>
        <select name="payrollStatus" id="payrollStatus"><option value="All">All</option><option value="true">Paid</option><option value="false" selected>Unpaid</option></select>
        <div class="buttons float-right">
            <ul>
                <li><a class="save">Generate</a></li>
            </ul>
        </div>
    </fieldset>
    <div id="payrollSearchResult" class="acore-grid"></div>
    <div id="payrollSearchResultDetails" class="acore-grid"></div>
    <div id="payrollSearchResultDetail" class="acore-grid"></div>
    <div id="payrollMarkAsPaidButton" class="buttons hidden">
        <ul>
            <li><a onclick="Payroll.MarkAsPaid('#payrollSearchResultDetails');return false">Mark As Paid</a></li>
            <li><a onclick="Payroll.MarkAsUnpaid('#payrollSearchResultDetails');return false">Mark As Unpaid</a></li>
        </ul>
    </div>
</div>
<%  } %>