﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PayrollDetailsViewData>" %>
<%if (Model != null && Model.Details != null && Model.Details.Count > 0)
  {%>
    <div class="payroll">
       <ul>
         <li class="align-center">[<a onclick="$('#payrollSearchResultDetails').hide();$('#payrollSearchResultDetail').hide();$('#payrollMarkAsPaidButton').hide();$('#payrollSearchResult').show();return false">Back to Search Results</a>] <%= Html.ActionLink("Export All To Excel", "PayrollDetails", "Export", new { StartDate = Model.StartDate, EndDate = Model.EndDate }, new { id = "ExportPayrollDetail_ExportLink" })%><a onclick="U.GetAttachment('Payroll/SummaryDetailsPdf', { 'payrollStartDate': '<%=Model.StartDate %>', 'payrollEndDate': '<%=Model.EndDate %>', 'payrollStatus': '<%=Model.PayrollStatus %>' });return false"> [ Print PDF ]</a></li>
       </ul>
    </div>                                                                                      
  <% foreach (var detail in Model.Details) {
       Html.RenderPartial("Detail", detail); 
   }%>
<%} %>