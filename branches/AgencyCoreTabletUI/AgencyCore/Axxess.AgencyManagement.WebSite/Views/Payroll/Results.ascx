﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PayrollSummaryViewData>" %>
<% if (Model != null && Model.VisitSummary != null && Model.VisitSummary.Count > 0){ %>
<ul>
    <li class="align-center">Summary [<a onclick="Payroll.LoadDetails();return false">View Details</a>] <%= Html.ActionLink("Export to Excel", "PayrollSummary", "Export", new { StartDate = Model.StartDate, EndDate = Model.EndDate, payrollStatus = Model.PayrollStatus }, new { id = "ExportPayrollSummary_ExportLink" })%><a onclick="U.GetAttachment('Payroll/SummaryPdf', { 'payrollStartDate': '<%=Model.StartDate %>', 'payrollEndDate': '<%=Model.EndDate %>', 'payrollStatus': '<%=Model.PayrollStatus %>' });return false"> [ Print PDF ]</a></li>
    <li><span class="payrolluser strong">User</span><span class="payrollcount strong">Count</span><span class="payrollaction strong">Action</span></li>
</ul>

<ol><%
    int i = 1;
    foreach (var summary in Model.VisitSummary)
    { %>
    <%= i % 2 != 0 ? "<li class='odd'>" : "<li class='even'>" %>
        <span class="payrolluser"><%= summary.UserName %></span>
        <span class="payrollcount"><%= summary.VisitCount.ToString() %></span>
        <span class="payrollaction"><%= summary.VisitCount > 0 ? "<a onclick=\"Payroll.LoadDetail('" + summary.UserId.ToString() + "');return false\">Detail</a>" : ""%></span>
    </li><% i++; } %>
</ol>
<script type="text/javascript">$(".payroll ol").each(function() { $("li:first", this).addClass("first"); $("li:last", this).addClass("last"); });</script>
<%} else { %><div class="strong align-center"><span class="img warning"></span>Your search yielded 0 results.</div><%} %>
<%= Html.Hidden("PayrollSearchType", "", new { @id="payrollSearchType" }) %>