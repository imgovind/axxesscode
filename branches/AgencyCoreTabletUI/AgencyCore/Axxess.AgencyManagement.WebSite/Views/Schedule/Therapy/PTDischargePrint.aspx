﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Physical Therapy Discharge<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body></body><%
string[] functionLimitations = data != null && data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null;
string[] patientCondition = data != null && data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null;
string[] serviceProvided = data != null && data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
    .Add("jquery-1.7.1.min.js")
    .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
    .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
).OnDocumentReady(() => { %>
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3EPhysical Therapy Discharge%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3EPhysical Therapy Discharge%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "";
    printview.addsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("ROM",true) +
            printview.span("Strength",true)) +
        printview.col(6,
            printview.span("Part",true) +
            printview.span("Action",true) +
            printview.span("Right",true) +
            printview.span("Left",true) +
            printview.span("Right",true) +
            printview.span("Left",true) +
            printview.span("Shoulder",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionROMRight") && data["GenericShoulderFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionROMLeft") && data["GenericShoulderFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionStrengthRight") && data["GenericShoulderFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionStrengthLeft") && data["GenericShoulderFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionROMRight") && data["GenericShoulderExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionROMLeft") && data["GenericShoulderExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionStrengthRight") && data["GenericShoulderExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionStrengthLeft") && data["GenericShoulderExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Abduction") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionROMRight") && data["GenericShoulderAbductionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionROMLeft") && data["GenericShoulderAbductionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionStrengthRight") && data["GenericShoulderAbductionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionStrengthLeft") && data["GenericShoulderAbductionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Int Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotROMRight") && data["GenericShoulderIntRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotROMLeft") && data["GenericShoulderIntRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotStrengthRight") && data["GenericShoulderIntRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotStrengthLeft") && data["GenericShoulderIntRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Ext Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotROMRight") && data["GenericShoulderExtRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotROMLeft") && data["GenericShoulderExtRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotStrengthRight") && data["GenericShoulderExtRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotStrengthLeft") && data["GenericShoulderExtRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Elbow",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionROMRight") && data["GenericElbowFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionROMLeft") && data["GenericElbowFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionStrengthRight") && data["GenericElbowFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionStrengthLeft") && data["GenericElbowFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionROMRight") && data["GenericElbowExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionROMLeft") && data["GenericElbowExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionStrengthRight") && data["GenericElbowExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionStrengthLeft") && data["GenericElbowExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Finger",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionROMRight") && data["GenericFingerFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionROMLeft") && data["GenericFingerFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionStrengthRight") && data["GenericFingerFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionStrengthLeft") && data["GenericFingerFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionROMRight") && data["GenericFingerExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionROMLeft") && data["GenericFingerExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionStrengthRight") && data["GenericFingerExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionStrengthLeft") && data["GenericFingerExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Wrist",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionROMRight") && data["GenericWristFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionROMLeft") && data["GenericWristFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionStrengthRight") && data["GenericWristFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionStrengthLeft") && data["GenericWristFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionROMRight") && data["GenericWristExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionROMLeft") && data["GenericWristExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionStrengthRight") && data["GenericWristExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionStrengthLeft") && data["GenericWristExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Hip",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionROMRight") && data["GenericHipFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionROMLeft") && data["GenericHipFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionStrengthRight") && data["GenericHipFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionStrengthLeft") && data["GenericHipFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionROMRight") && data["GenericHipExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionROMLeft") && data["GenericHipExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionStrengthRight") && data["GenericHipExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionStrengthLeft") && data["GenericHipExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Abduction") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionROMRight") && data["GenericHipAbductionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionROMLeft") && data["GenericHipAbductionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionStrengthRight") && data["GenericHipAbductionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionStrengthLeft") && data["GenericHipAbductionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Int Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotROMRight") && data["GenericHipIntRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotROMLeft") && data["GenericHipIntRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotStrengthRight") && data["GenericHipIntRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotStrengthLeft") && data["GenericHipIntRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Ext Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotROMRight") && data["GenericHipExtRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotROMLeft") && data["GenericHipExtRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotStrengthRight") && data["GenericHipExtRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotStrengthLeft") && data["GenericHipExtRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Knee",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionROMRight") && data["GenericKneeFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionROMLeft") && data["GenericKneeFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionStrengthRight") && data["GenericKneeFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionStrengthLeft") && data["GenericKneeFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionROMRight") && data["GenericKneeExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionROMLeft") && data["GenericKneeExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionStrengthRight") && data["GenericKneeExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionStrengthLeft") && data["GenericKneeExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Ankle",true) +
            printview.span("Plantarflexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionROMRight") && data["GenericAnklePlantFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionROMLeft") && data["GenericAnklePlantFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionStrengthRight") && data["GenericAnklePlantFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionStrengthLeft") && data["GenericAnklePlantFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Dorsiflexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionROMRight") && data["GenericAnkleFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionROMLeft") && data["GenericAnkleFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionStrengthRight") && data["GenericAnkleFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionStrengthLeft") && data["GenericAnkleFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Trunk",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionROMRight") && data["GenericTrunkFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionROMLeft") && data["GenericTrunkFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionStrengthRight") && data["GenericTrunkFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionStrengthLeft") && data["GenericTrunkFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Rotation") +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationROMRight") && data["GenericTrunkRotationROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationROMLeft") && data["GenericTrunkRotationROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationStrengthRight") && data["GenericTrunkRotationStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationStrengthLeft") && data["GenericTrunkRotationStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionROMRight") && data["GenericTrunkExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionROMLeft") && data["GenericTrunkExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionStrengthRight") && data["GenericTrunkExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionStrengthLeft") && data["GenericTrunkExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Neck",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionROMRight") && data["GenericNeckFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionROMLeft") && data["GenericNeckFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionStrengthRight") && data["GenericNeckFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionStrengthLeft") && data["GenericNeckFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionROMRight") && data["GenericNeckExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionROMLeft") && data["GenericNeckExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionStrengthRight") && data["GenericNeckExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionStrengthLeft") && data["GenericNeckExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Lat Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionROMRight") && data["GenericNeckLatFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionROMLeft") && data["GenericNeckLatFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionStrengthRight") && data["GenericNeckLatFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionStrengthLeft") && data["GenericNeckLatFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Long  Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionROMRight") && data["GenericNeckLongFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionROMLeft") && data["GenericNeckLongFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionStrengthRight") && data["GenericNeckLongFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionStrengthLeft") && data["GenericNeckLongFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionStrengthLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("&#160;",true) +
            printview.span("Rotation") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationROMRight") && data["GenericNeckRotationROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationROMRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationROMLeft") && data["GenericNeckRotationROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationROMLeft"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationStrengthRight") && data["GenericNeckRotationStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationStrengthRight"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationStrengthLeft") && data["GenericNeckRotationStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationStrengthLeft"].Answer.Clean() : string.Empty%>",0,1)),
        "Physical Assessment");
    printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistive Device",true) +
            printview.span("% Assist",true) +
            printview.span("Rolling",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityRollingAssistiveDevice") && data["GenericBedMobilityRollingAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilityRollingAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityRollingAssist") && data["GenericBedMobilityRollingAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilityRollingAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Sit Stand Sit",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilitySitStandSitAssistiveDevice") && data["GenericBedMobilitySitStandSitAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilitySitStandSitAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilitySitStandSitAssist") && data["GenericBedMobilitySitStandSitAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilitySitStandSitAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Sup to Sit",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilitySupToSitAssistiveDevice") && data["GenericBedMobilitySupToSitAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilitySupToSitAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilitySupToSitAssist") && data["GenericBedMobilitySupToSitAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilitySupToSitAssist"].Answer.Clean() + "%" : string.Empty%>",0,1)) +
        "%3Ch3%3EGait%3C/h3%3E" +
        printview.col(2,
            printview.span("Level",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericGaitLevelAssist") && data["GenericGaitLevelAssist"].Answer.IsNotNullOrEmpty() ? data["GenericGaitLevelAssist"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%><%= data != null && data.ContainsKey("GenericGaitLevelFeet") && data["GenericGaitLevelFeet"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericGaitLevelFeet"].Answer.Clean() : string.Empty %> Feet",0,1) +
            printview.span("Unlevel",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericGaitUnLevelAssist") && data["GenericGaitUnLevelAssist"].Answer.IsNotNullOrEmpty() ? data["GenericGaitUnLevelAssist"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%><%= data != null && data.ContainsKey("GenericGaitUnLevelFeet") && data["GenericGaitUnLevelFeet"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericGaitUnLevelFeet"].Answer.Clean() : string.Empty %> Feet",0,1) +
            printview.span("Step/ Stair",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericGaitStepStairAssist") && data["GenericGaitStepStairAssist"].Answer.IsNotNullOrEmpty() ? data["GenericGaitStepStairAssist"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%><%= data != null && data.ContainsKey("GenericGaitStepStairFeet") && data["GenericGaitStepStairFeet"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericGaitStepStairFeet"].Answer.Clean() : string.Empty %> Feet",0,1)),
        "Bed Mobility",2);
    printview.addsubsection(
        printview.col(3,
            printview.span("&#160;") +
            printview.span("Assistive Device",true) +
            printview.span("% Assist",true) +
            printview.span("Bed-Chair",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferBedChairAssistiveDevice") && data["GenericTransferBedChairAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferBedChairAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferBedChairAssist") && data["GenericTransferBedChairAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferBedChairAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Chair-Bed",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferChairBedAssistiveDevice") && data["GenericTransferChairBedAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferChairBedAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferChairBedAssist") && data["GenericTransferChairBedAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferChairBedAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Chair to W/C",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferChairToWCAssistiveDevice") && data["GenericTransferChairToWCAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferChairToWCAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferChairToWCAssist") && data["GenericTransferChairToWCAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferChairToWCAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Toilet or BSC",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferToiletOrBSCAssistiveDevice") && data["GenericTransferToiletOrBSCAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferToiletOrBSCAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferToiletOrBSCAssist") && data["GenericTransferToiletOrBSCAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferToiletOrBSCAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Car/Van",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferCanVanAssistiveDevice") && data["GenericTransferCanVanAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferCanVanAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferCanVanAssist") && data["GenericTransferCanVanAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferCanVanAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Tub/Shower",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferTubShowerAssistiveDevice") && data["GenericTransferTubShowerAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericTransferTubShowerAssistiveDevice"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferTubShowerAssist") && data["GenericTransferTubShowerAssist"].Answer.IsNotNullOrEmpty() ? data["GenericTransferTubShowerAssist"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("&#160;") +
            printview.span("Static",true) +
            printview.span("Dynamic",true) +
            printview.span("Sitting Balance",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferSittingBalanceStatic") && data["GenericTransferSittingBalanceStatic"].Answer.IsNotNullOrEmpty() ? data["GenericTransferSittingBalanceStatic"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferSittingBalanceDynamic") && data["GenericTransferSittingBalanceDynamic"].Answer.IsNotNullOrEmpty() ? data["GenericTransferSittingBalanceDynamic"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Stand Balance",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferStandBalanceStatic") && data["GenericTransferStandBalanceStatic"].Answer.IsNotNullOrEmpty() ? data["GenericTransferStandBalanceStatic"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferStandBalanceDynamic") && data["GenericTransferStandBalanceDynamic"].Answer.IsNotNullOrEmpty() ? data["GenericTransferStandBalanceDynamic"].Answer.Clean() + "%" : string.Empty%>",0,1)),
        "Transfer");
    printview.addsubsection(
        printview.col(3,
            printview.span("Level",true) +
            printview.span("Ramp",true) +
            printview.span("Maneuver",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericWCMobilityLevel") && data["GenericWCMobilityLevel"].Answer.IsNotNullOrEmpty() ? data["GenericWCMobilityLevel"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWCMobilityRamp") && data["GenericWCMobilityRamp"].Answer.IsNotNullOrEmpty() ? data["GenericWCMobilityRamp"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWCMobilityManeuver") && data["GenericWCMobilityManeuver"].Answer.IsNotNullOrEmpty() ? data["GenericWCMobilityManeuver"].Answer.Clean() + "%" : string.Empty%>",0,1)),
        "W/C Mobility");
    printview.addsubsection(
        printview.col(2,
            printview.span("Pain Level",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPainLevel") && data["GenericPainLevel"].Answer.IsNotNullOrEmpty() ? data["GenericPainLevel"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("ADL",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADL") && data["GenericADL"].Answer.IsNotNullOrEmpty() ? data["GenericADL"].Answer.Clean() + "%" : string.Empty%>",0,1) +
            printview.span("Endurance",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericEndurance") && data["GenericEndurance"].Answer.IsNotNullOrEmpty() ? data["GenericEndurance"].Answer.Clean() : string.Empty%>",0,1)));
    printview.addsubsection(
        printview.col(2,
            printview.checkbox("Reached Max Potential",<%= data != null && data.ContainsKey("GenericReasonForDischarge") && data["GenericReasonForDischarge"].Answer.Split(',').Contains("1") ? "true" : "false" %>) +
            printview.checkbox("No Longer Homebound",<%= data != null && data.ContainsKey("GenericReasonForDischarge") && data["GenericReasonForDischarge"].Answer.Split(',').Contains("2") ? "true" : "false" %>) +
            printview.checkbox("Per Patient/Family Request",<%= data != null && data.ContainsKey("GenericReasonForDischarge") && data["GenericReasonForDischarge"].Answer.Split(',').Contains("3") ? "true" : "false" %>) +
            printview.checkbox("Prolonged On-Hold Status",<%= data != null && data.ContainsKey("GenericReasonForDischarge") && data["GenericReasonForDischarge"].Answer.Split(',').Contains("4") ? "true" : "false" %>) +
            printview.checkbox("Prolonged On-Hold Status",<%= data != null && data.ContainsKey("GenericReasonForDischarge") && data["GenericReasonForDischarge"].Answer.Split(',').Contains("5") ? "true" : "false" %>) +
            printview.checkbox("Hospitalized",<%= data != null && data.ContainsKey("GenericReasonForDischarge") && data["GenericReasonForDischarge"].Answer.Split(',').Contains("6") ? "true" : "false" %>) +
            printview.checkbox("Expired",<%= data != null && data.ContainsKey("GenericReasonForDischarge") && data["GenericReasonForDischarge"].Answer.Split(',').Contains("7") ? "true" : "false" %>) +
            printview.span("%3Cstrong%3EOther%3C/strong%3E <%= data != null && data.ContainsKey("GenericReasonForDischargeOther") && data["GenericReasonForDischargeOther"].Answer.IsNotNullOrEmpty() ? data["GenericReasonForDischargeOther"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>",0,1)),
        "Reason for Discharge");
    printview.addsubsection(
        printview.col(2,
            printview.checkbox("Thera Ex",<%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("1") ? "true" : "false" %>) +
            printview.checkbox("Bed Mobility Training",<%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("2") ? "true" : "false" %>) +
            printview.checkbox("Transfer Training",<%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("3") ? "true" : "false" %>) +
            printview.checkbox("Balance Training",<%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("4") ? "true" : "false" %>) +
            printview.checkbox("Gait Training",<%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("5") ? "true" : "false" %>) +
            printview.checkbox("HEP",<%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("6") ? "true" : "false" %>) +
            printview.checkbox("Electrotherapy",<%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("7") ? "true" : "false" %>) +
            printview.checkbox("Ultrasound",<%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("8") ? "true" : "false" %>) +
            printview.checkbox("Prosthetic Training",<%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("9") ? "true" : "false" %>) +
            printview.checkbox("Manual Therapy",<%= data != null && data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer.Split(',').Contains("10") ? "true" : "false" %>) +
            printview.span("%3Cstrong%3EOther%3C/strong%3E <%= data != null && data.ContainsKey("GenericTreatmentCodesOther") && data["GenericTreatmentCodesOther"].Answer.IsNotNullOrEmpty() ? data["GenericTreatmentCodesOther"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>",0,1)),
        "Treatment Plan");
    printview.addsection(
        printview.col(4,
            printview.span("Frequency",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericFrequency") && data["GenericFrequency"].Answer.IsNotNullOrEmpty() ? data["GenericFrequency"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Duration",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericDuration") && data["GenericDuration"].Answer.IsNotNullOrEmpty() ? data["GenericDuration"].Answer.Clean() : string.Empty%>",0,1)));
    printview.addsection(
        printview.span("<%= data != null && data.ContainsKey("GenericNarrativeComment") ? data["GenericNarrativeComment"].Answer.Clean() : string.Empty%>",0,2),
        "Narrative");
    printview.addsection(
        printview.col(2,
            printview.span("Clinician Signature:",true) +
            printview.span("Date:",true) +
            printview.span("<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : string.Empty %>",0,1) +
            printview.span("<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : string.Empty %>",0,1))); <%
}).Render(); %>
</html>
