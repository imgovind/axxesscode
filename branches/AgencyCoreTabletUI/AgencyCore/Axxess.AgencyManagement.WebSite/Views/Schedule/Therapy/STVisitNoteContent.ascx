﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>();
    string[] genericSpeechTherapyDone = data.ContainsKey("GenericSpeechTherapyDone") && data["GenericSpeechTherapyDone"].Answer != "" ? data["GenericSpeechTherapyDone"].Answer.Split(',') : null;
    var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate; %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="2">Treatment Diagnosis/Problem</th>
            <th colspan="2">Short Term Goals</th>
        </tr><tr>
            <td colspan="2"><%= Html.TextArea(Model.Type + "_GenericTreatmentDiagnosis", data.ContainsKey("GenericTreatmentDiagnosis") ? data["GenericTreatmentDiagnosis"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_GenericTreatmentDiagnosis", @class = "fill" })%></td>
            <td colspan="2"><%= Html.TextArea(Model.Type + "_GenericShortTermGoals", data.ContainsKey("GenericShortTermGoals") ? data["GenericShortTermGoals"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_GenericShortTermGoals", @class = "fill" })%></td>
        </tr><tr>
            <td colspan="4">
                <input type="hidden" name="<%= Model.Type %>_GenericSpeechTherapyDone" value="" />
                <div class="third align-left">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone1' class='radio' name='{1}_GenericSpeechTherapyDone' value='1' type='checkbox' {0} />", genericSpeechTherapyDone != null && genericSpeechTherapyDone.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone1">Speech (C2)</label>
                </div><div class="third align-left">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone2' class='radio' name='{1}_GenericSpeechTherapyDone' value='2' type='checkbox' {0} />", genericSpeechTherapyDone != null && genericSpeechTherapyDone.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone2">Lip, tongue, facial, exercises to improve swallowing/vocal skills</label>
                </div><div class="third align-left">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone3' class='radio' name='{1}_GenericSpeechTherapyDone' value='3' type='checkbox' {0} />", genericSpeechTherapyDone != null && genericSpeechTherapyDone.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone3">Voice</label>
                </div><div class="third align-left">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone4' class='radio' name='{1}_GenericSpeechTherapyDone' value='4' type='checkbox' {0} />", genericSpeechTherapyDone != null && genericSpeechTherapyDone.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone4">Dysphagia Treatments (C4)</label>
                </div><div class="third align-left">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone5' class='radio' name='{1}_GenericSpeechTherapyDone' value='5' type='checkbox' {0} />", genericSpeechTherapyDone != null && genericSpeechTherapyDone.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone5">Fluency</label>
                </div><div class="third align-left">
                    <%= string.Format("<input id='{1}_GenericSpeechTherapyDone6' class='radio' name='{1}_GenericSpeechTherapyDone' value='6' type='checkbox' {0} />", genericSpeechTherapyDone != null && genericSpeechTherapyDone.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                    <label for="<%= Model.Type %>_GenericSpeechTherapyDone6">Language Disorder (C5)</label>
                </div>
            </td>
        </tr><tr>
            <th colspan="4">Results Of Therapy Session</th>
        </tr><tr>
            <td colspan="4"><%= Html.TextArea(Model.Type + "_GenericResultsOfTherapySession", data.ContainsKey("GenericResultsOfTherapySession") ? data["GenericResultsOfTherapySession"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_GenericResultsOfTherapySession", @class = "fill" })%></td>
        </tr>
    </tbody>
</table>

