﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Physical Therapist Visit<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body></body><%
string[] genericHomeBoundStatus = data.ContainsKey("GenericHomeBoundStatus") && data["GenericHomeBoundStatus"].Answer != "" ? data["GenericHomeBoundStatus"].Answer.Split(',') : null;
string[] genericFunctionalLimitations = data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer != "" ? data["GenericFunctionalLimitations"].Answer.Split(',') : null;
string[] genericSupervisoryVisit = data.ContainsKey("GenericSupervisoryVisit") && data["GenericSupervisoryVisit"].Answer != "" ? data["GenericSupervisoryVisit"].Answer.Split(',') : null;
string[] genericTherapeuticExercises = data.ContainsKey("GenericTherapeuticExercises") && data["GenericTherapeuticExercises"].Answer != "" ? data["GenericTherapeuticExercises"].Answer.Split(',') : null;
string[] genericTherapyTraning = data.ContainsKey("GenericTherapyTraning") && data["GenericTherapyTraning"].Answer != "" ? data["GenericTherapyTraning"].Answer.Split(',') : null;
string[] genericTeaching = data.ContainsKey("GenericTeaching") && data["GenericTeaching"].Answer != "" ? data["GenericTeaching"].Answer.Split(',') : null;
string[] genericWalkDirection = data.ContainsKey("GenericWalkDirection") && data["GenericWalkDirection"].Answer != "" ? data["GenericWalkDirection"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
    .Add("jquery-1.7.1.min.js")
    .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
    .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
).OnDocumentReady(() => { %>
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3EPhysical Therapist Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3EPhysical Therapist Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "";
    printview.addsection(
        printview.col(4,
            printview.checkbox("Needs assist with transfer.",<%= genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("1") ? "true" : "false" %>) +
            printview.checkbox("Needs assist with gait.",<%= genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("2") ? "true" : "false" %>) +
            printview.checkbox("Needs assist leaving the home.",<%= genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("3") ? "true" : "false" %>) +
            printview.checkbox("Unable to be up for long period.",<%= genericHomeBoundStatus != null && genericHomeBoundStatus.Contains("4") ? "true" : "false" %>)),
        "Homebound Status");
    printview.addsection(
        printview.col(4,
            printview.checkbox("ROM/Strength.",<%= genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Pain.",<%= genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Safety Techniques.",<%= genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("3") ? "true" : "false"%>) +
            printview.checkbox("W/C Mobility.",<%= genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Balance / Gait.",<%= genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Bed Mobility.",<%= genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Transfer.",<%= genericFunctionalLimitations != null && genericFunctionalLimitations.Contains("7") ? "true" : "false"%>)),
        "Functional Limitations");
    printview.addsection(
        printview.col(4,
            printview.span("%3Cstrong%3EPrior BP:%3C/strong%3E <%= data != null && data.ContainsKey("GenericPriorBP") ? data["GenericPriorBP"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>") +
            printview.span("%3Cstrong%3EPost BP:%3C/strong%3E <%= data != null && data.ContainsKey("GenericPostBP") ? data["GenericPostBP"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>") +
            printview.span("%3Cstrong%3EPrior Pulse:%3C/strong%3E <%= data != null && data.ContainsKey("GenericPriorPulse") ? data["GenericPriorPulse"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>") +
            printview.span("%3Cstrong%3EPost Pulse:%3C/strong%3E <%= data != null && data.ContainsKey("GenericPostPulse") ? data["GenericPostPulse"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E"%>")),
        "Vital Signs");
    printview.addsection(
        printview.col(2,
            printview.checkbox("Supervisory Visit",<%= genericSupervisoryVisit != null && genericSupervisoryVisit.Contains("1") ? "true" : "false" %>,true) +
            printview.checkbox("LPTA Present",<%= genericSupervisoryVisit != null && genericSupervisoryVisit.Contains("2") ? "true" : "false" %>,true) +
            printview.span("Comments:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericSupervisoryVisitComment") ? data["GenericSupervisoryVisitComment"].Answer.Clean() : string.Empty%>",0,2)),
        "Supervisory Visit");
    printview.addsection(
        printview.span("<%= data != null && data.ContainsKey("GenericSubjective") ? data["GenericSubjective"].Answer.Clean() : string.Empty%>",0,2),
        "Subjective");
    printview.addsection(
        printview.checkbox("Therapeutic Exercises", <%= genericTherapeuticExercises != null && genericTherapeuticExercises.Contains("1") ? "true" : "false" %>, true) +
        printview.col(4,
            printview.span("ROM To:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericROMTo") ? data["GenericROMTo"].Answer.Clean() : string.Empty %><%= data != null && data.ContainsKey("GenericROMToReps") && data["GenericROMToReps"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericROMToReps"].Answer.Clean() + "reps" : string.Empty%>", 0, 1) +
            printview.span("Active To:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericActiveTo") ? data["GenericActiveTo"].Answer.Clean() : string.Empty%><%= data != null && data.ContainsKey("GenericActiveToReps") && data["GenericActiveToReps"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericActiveToReps"].Answer.Clean() + "reps" : string.Empty%>", 0, 1) +
            printview.span("Active/Assistive To:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericAssistive") ? data["GenericAssistive"].Answer.Clean() : string.Empty%><%= data != null && data.ContainsKey("GenericAssistiveReps") && data["GenericAssistiveReps"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericAssistiveReps"].Answer.Clean() + "reps" : string.Empty%>", 0, 1) +
            printview.span("Resistive, Manual, To:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericManual") ? data["GenericManual"].Answer.Clean() : string.Empty%><%= data != null && data.ContainsKey("GenericManualReps") && data["GenericManualReps"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericManualReps"].Answer.Clean() + "reps" : string.Empty%>", 0, 1) +
            printview.span("Resistive, w/Weights, To:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericResistiveWWeights") ? data["GenericResistiveWWeights"].Answer.Clean() : string.Empty%><%= data != null && data.ContainsKey("GenericResistiveWWeightsReps") && data["GenericResistiveWWeightsReps"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericResistiveWWeightsReps"].Answer.Clean() + "reps" : string.Empty%>", 0, 1) +
            printview.span("Stretching To:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericStretchingTo") ? data["GenericStretchingTo"].Answer.Clean() : string.Empty%><%= data != null && data.ContainsKey("GenericStretchingToReps") && data["GenericStretchingToReps"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericStretchingToReps"].Answer.Clean() + "reps" : string.Empty%>", 0, 1)),
        "Objective");
    printview.addsection(
        printview.col(4,
            printview.span("Rolling",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericRolling") ? data["GenericRolling"].Answer.Clean() : string.Empty%><%= data != null && data.ContainsKey("GenericRollingReps") && data["GenericRollingReps"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericRollingReps"].Answer.Clean() + "reps" : string.Empty%>", 0, 1) +
            printview.span("Sup-Sit",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericSupSit") ? data["GenericSupSit"].Answer.Clean() : string.Empty%><%= data != null && data.ContainsKey("GenericSupSitReps") && data["GenericSupSitReps"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericSupSitReps"].Answer.Clean() + "reps" : string.Empty%>", 0, 1) +
            printview.span("Scooting Toward",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericScootingToward") ? data["GenericScootingToward"].Answer.Clean() : string.Empty%><%= data != null && data.ContainsKey("PTVisit_GenericScootingTowardReps") && data["PTVisit_GenericScootingTowardReps"].Answer.IsNotNullOrEmpty() ? "X" + data["PTVisit_GenericScootingTowardReps"].Answer.Clean() + "reps" : string.Empty%>", 0, 1) +
            printview.span("Sit to Stand",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericSitToStand") ? data["GenericSitToStand"].Answer.Clean() : string.Empty%><%= data != null && data.ContainsKey("PTVisit_GenericSitToStandReps") && data["PTVisit_GenericSitToStandReps"].Answer.IsNotNullOrEmpty() ? "X" + data["PTVisit_GenericSitToStandReps"].Answer.Clean() + "reps" : string.Empty%>", 0, 1) +
            printview.span("Proper Foot Placement",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericProperFootPlacement") && data["GenericProperFootPlacement"].Answer == "1" ? "true" : "false" %>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericProperFootPlacement") && data["GenericProperFootPlacement"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Proper Unfolding",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericProperUnfolding") && data["GenericProperUnfolding"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericProperUnfolding") && data["GenericProperUnfolding"].Answer == "0" ? "true" : "false"%>))),
        "Bed Mobility Training");
    printview.addsection(
        printview.col(2,
            printview.span("Transfer Training", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransferTraining") && data["GenericTransferTraining"].Answer.IsNotNullOrEmpty() ? "X" + data["GenericTransferTraining"].Answer.Clean() + "reps" : string.Empty%>",0,1) +
            printview.span("Assistive Device", true) +
            printview.col(4,
                printview.checkbox("Yes", <%= data != null && data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer == "1" ? "true" : "false" %>) +
                printview.checkbox("No", <%= data != null && data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer == "0" ? "true" : "false" %>) +
                printview.span("Specify:") +
                printview.span("<%= data != null && data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer.IsNotNullOrEmpty() ? data["GenericAssistiveDevice"].Answer.Clean() + "reps" : string.Empty%>"))) +
        printview.col(6,
                printview.span("Bed - Chair", true) +
                printview.span("<%= data != null && data.ContainsKey("GenericBedChairAssist") && data["GenericBedChairAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedChairAssist"].Answer.Clean() + "% Assist" : string.Empty%>") +
                printview.span("Chair - Toilet", true) +
                printview.span("<%= data != null && data.ContainsKey("GenericChairToiletAssist") && data["GenericChairToiletAssist"].Answer.IsNotNullOrEmpty() ? data["GenericChairToiletAssist"].Answer.Clean() + "% Assist" : string.Empty%>") +
                printview.span("Chair - Car", true) +
                printview.span("<%= data != null && data.ContainsKey("GenericChairCarAssist") && data["GenericChairCarAssist"].Answer.IsNotNullOrEmpty() ? data["GenericChairCarAssist"].Answer.Clean() + "% Assist" : string.Empty%>")) +
        printview.col(2,
            printview.col(2,
                printview.span("Sitting Balance Activities WB UE", true) +
                printview.col(2,
                    printview.checkbox("Yes", <%= data != null && data.ContainsKey("GenericSBAWE") && data["GenericSBAWE"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No", <%= data != null && data.ContainsKey("GenericSBAWE") && data["GenericSBAWE"].Answer == "0" ? "true" : "false"%>))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Static:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericSBAStaticAssist") && data["GenericSBAStaticAssist"].Answer.IsNotNullOrEmpty() ? data["GenericSBAStaticAssist"].Answer.Clean() + "% Assist" : string.Empty%>")) +
                printview.col(2,
                    printview.span("Dynamic:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericSBADynamicAssist") && data["GenericSBADynamicAssist"].Answer.IsNotNullOrEmpty() ? data["GenericSBADynamicAssist"].Answer.Clean() + "% Assist" : string.Empty%>"))) +
            printview.span("Standing Balance Activities", true) +
            printview.col(2,
                printview.col(2,
                    printview.span("Static:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericSBAStaticAssist") && data["GenericSBAStaticAssist"].Answer.IsNotNullOrEmpty() ? data["GenericSBAStaticAssist"].Answer.Clean() + "% Assist" : string.Empty%>")) +
                printview.col(2,
                    printview.span("Dynamic:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericSBADynamicAssist") && data["GenericSBADynamicAssist"].Answer.IsNotNullOrEmpty() ? data["GenericSBADynamicAssist"].Answer.Clean() + "% Assist" : string.Empty%>")))),
        "Transfer Training");
    printview.addsection(
        printview.col(4,
            printview.span("Gait Training", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericGaitTrainingFt") && data["GenericGaitTrainingFt"].Answer.IsNotNullOrEmpty() ? data["GenericGaitTrainingFt"].Answer.Clean() + "ft." : string.Empty %><%= data != null && data.ContainsKey("GenericGaitTrainingFtX") && data["GenericGaitTrainingFtX"].Answer.IsNotNullOrEmpty() ? " X" + data["GenericGaitTrainingFtX"].Answer.Clean() : string.Empty %>",0,1) +
            printview.checkbox("Walking Sideways.",<%= genericWalkDirection != null && genericWalkDirection.Contains("1") ? "true" : "false" %>) +
            printview.checkbox("Walking Backwards.",<%= genericWalkDirection != null && genericWalkDirection.Contains("2") ? "true" : "false"%>) +
            printview.span("With Device", true) +
            printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericGaitTrainingWithDevice") && data["GenericGaitTrainingWithDevice"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("No",<%= data != null && data.ContainsKey("GenericGaitTrainingWithDevice") && data["GenericGaitTrainingWithDevice"].Answer == "0" ? "true" : "false"%>) +
            printview.span("<%= data != null && data.ContainsKey("GenericGaitTrainingWithDeviceAssist") && data["GenericGaitTrainingWithDeviceAssist"].Answer.IsNotNullOrEmpty() ? data["GenericGaitTrainingWithDeviceAssist"].Answer.Clean() + "% Assist" : string.Empty%>",0,1)) +
        printview.col(2,
            printview.col(2,
                printview.span("Turning", true) +
                printview.span("<%= data != null && data.ContainsKey("GenericTurningDistance") && data["GenericTurningDistance"].Answer.IsNotNullOrEmpty() ? data["GenericTurningDistance"].Answer.Clean() + " Distance" : string.Empty%>",0,1)) +
            printview.col(3,
                printview.span("Uneven", true) +
                printview.span("<%= data != null && data.ContainsKey("GenericUnevenAssist") && data["GenericUnevenAssist"].Answer.IsNotNullOrEmpty() ? data["GenericUnevenAssist"].Answer.Clean() + "% Assist" : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericUnevenDevice") && data["GenericUnevenDevice"].Answer.IsNotNullOrEmpty() ? data["GenericUnevenDevice"].Answer.Clean() + " Device" : string.Empty%>",0,1)) +
            printview.col(3,
                printview.span("Stairs/Steps", true) +
                printview.span("<%= data != null && data.ContainsKey("GenericStairsStepsAssist") && data["GenericStairsStepsAssist"].Answer.IsNotNullOrEmpty() ? data["GenericStairsStepsAssist"].Answer.Clean() + "% Assist" : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericStairsStepsDevice") && data["GenericStairsStepsDevice"].Answer.IsNotNullOrEmpty() ? data["GenericStairsStepsDevice"].Answer.Clean() + " Device" : string.Empty%>",0,1)) +
            printview.col(2,
                printview.span("W/C Training", true) +
                printview.span("<%= data != null && data.ContainsKey("GenericWCTrainingAssist") && data["GenericWCTrainingAssist"].Answer.IsNotNullOrEmpty() ? data["GenericWCTrainingAssist"].Answer.Clean() + "% Assist" : string.Empty%>",0,1))) +
        printview.col(5,
            printview.checkbox("WB (L)",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("1") ? "true" : "false"%>) +
            printview.checkbox("WB (R)",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Hip/Knee Extension",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Stance Phase (L)",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Stance Phase (R)",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Heel Strike",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Step Length",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Proper Posture",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("8") ? "true" : "false"%>) +
            printview.checkbox("Speed",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("9") ? "true" : "false"%>)) +
        printview.col(2,
            printview.span("Other", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericGaitTrainingOther") && data["GenericGaitTrainingOther"].Answer.IsNotNullOrEmpty() ? data["GenericGaitTrainingOther"].Answer.Clean() : string.Empty%>")),
        "Gait Training");
    printview.addsection(
        printview.col(6,
                printview.checkbox("Patient",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("1") ? "true" : "false" %>) +
                printview.checkbox("Caregiver",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("2") ? "true" : "false" %>) +
                printview.checkbox("HEP",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("3") ? "true" : "false" %>) +
                printview.checkbox("Safe Transfer",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("4") ? "true" : "false" %>) +
                printview.checkbox("Safe Gait",<%= genericTherapyTraning != null && genericTherapyTraning.Contains("5") ? "true" : "false" %>) +
                printview.span("Other: <%= data != null && data.ContainsKey("GenericGaitTrainingOther") ? data["GenericGaitTrainingOther"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E" %>",0,1)),
        "Teaching");
    printview.addsection(
        printview.col(4,
            printview.span("%3Cstrong%3EPain level prior to therapy:%3C/strong%3E <%= data != null && data.ContainsKey("GenericPriorIntensityOfPain") ? data["GenericPriorIntensityOfPain"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E" %>",0,1) +
            printview.span("%3Cstrong%3EPain level after therapy:%3C/strong%3E <%= data != null && data.ContainsKey("GenericPostIntensityOfPain") ? data["GenericPostIntensityOfPain"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E" %>",0,1) +
            printview.span("%3Cstrong%3ELocation:%3C/strong%3E <%= data != null && data.ContainsKey("GenericPainLocation") ? data["GenericPainLocation"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E" %>",0,1) +
            printview.span("%3Cstrong%3ERelieved by:%3C/strong%3E <%= data != null && data.ContainsKey("GenericPainRelievedBy") ? data["GenericPainRelievedBy"].Answer.Clean() : "%3Cspan class=%22blank%22%3E%3C/span%3E" %>",0,1)) +
        printview.span("Other Comment:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericPainProfileComment") ? data["GenericPainProfileComment"].Answer.Clean() : string.Empty%>",0,2),
        "Pain");
    printview.addsection(
        printview.span("<%= data != null && data.ContainsKey("GenericAssessment") ? data["GenericAssessment"].Answer.Clean() : string.Empty%>",0,2),
        "Assessment");
    printview.addsection(
        printview.col(6,
            printview.span("Continue Plan:", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericContinuePrescribedPlan") ? data["GenericContinuePrescribedPlan"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Change Plan:", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericChangePrescribedPlan") ? data["GenericChangePrescribedPlan"].Answer.Clean() : string.Empty%>",0,1) +
            printview.span("Plan Discharge:", true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPlanDischarge") ? data["GenericPlanDischarge"].Answer.Clean() : string.Empty%>",0,1)),
        "Plan");
    printview.addsection(
        printview.span("<%= data != null && data.ContainsKey("GenericNarrativeComment") ? data["GenericNarrativeComment"].Answer.Clean() : string.Empty%>",0,2),
        "Narrative");
    printview.addsection(
        printview.col(2,
            printview.span("Clinician Signature:",true) +
            printview.span("Date:",true) +
            printview.span("<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : string.Empty %>",0,1) +
            printview.span("<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : string.Empty %>",0,1))); <%
}).Render(); %>
</html>
