﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>();
    string[] homeboundStatusAssist = data.ContainsKey("GenericHomeboundStatusAssist") && data["GenericHomeboundStatusAssist"].Answer != "" ? data["GenericHomeboundStatusAssist"].Answer.Split(',') : null;
    string[] functionalLimitations = data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer != "" ? data["GenericFunctionalLimitations"].Answer.Split(',') : null;
    string[] objectiveLevelOfAssist = data.ContainsKey("GenericObjectiveLevelOfAssist") && data["GenericObjectiveLevelOfAssist"].Answer != "" ? data["GenericObjectiveLevelOfAssist"].Answer.Split(',') : null;
    string[] genericPropulsionWith = data.ContainsKey("GenericPropulsionWith") && data["GenericPropulsionWith"].Answer != "" ? data["GenericPropulsionWith"].Answer.Split(',') : null;
    string[] genericUEWeightBearing = data.ContainsKey("GenericUEWeightBearing") && data["GenericUEWeightBearing"].Answer != "" ? data["GenericUEWeightBearing"].Answer.Split(',') : null;
    string[] genericTeaching = data.ContainsKey("GenericTeaching") && data["GenericTeaching"].Answer != "" ? data["GenericTeaching"].Answer.Split(',') : null;
    string[] genericPlan = data.ContainsKey("GenericPlan") && data["GenericPlan"].Answer != "" ? data["GenericPlan"].Answer.Split(',') : null; %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <td colspan="4">
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <label class="float-left">Homebound Status: requires assist with:</label>
                                <input type="hidden" name="<%= Model.Type %>_GenericHomeboundStatusAssist" value="" />
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericHomeboundStatusAssist1' name='{1}_GenericHomeboundStatusAssist' value='1' type='checkbox' {0} />", homeboundStatusAssist != null && homeboundStatusAssist.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist1" class="radio">Gait</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericHomeboundStatusAssist2' name='{1}_GenericHomeboundStatusAssist' value='2' type='checkbox' {0} />", homeboundStatusAssist != null && homeboundStatusAssist.Contains("2") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist2" class="radio">Leaving the Home </label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericHomeboundStatusAssist3' name='{1}_GenericHomeboundStatusAssist' value='3' type='checkbox' {0} />", homeboundStatusAssist != null && homeboundStatusAssist.Contains("3") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist3" class="radio">Transfers </label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericHomeboundStatusAssist4' name='{1}_GenericHomeboundStatusAssist' value='4' type='checkbox' {0} />", homeboundStatusAssist != null && homeboundStatusAssist.Contains("4") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist4" class="radio">SOB/Endurance</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericHomeboundStatusAssist5' name='{1}_GenericHomeboundStatusAssist' value='5' type='checkbox' {0} />", homeboundStatusAssist != null && homeboundStatusAssist.Contains("5") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist5" class="radio">Orientation</label>
                            </td>
                        </tr><tr>
                            <td>
                                <label class="float-left">Functional Limitations:</label>
                                <input type="hidden" name="<%= Model.Type %>_GenericFunctionalLimitations" value="" />
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations1' name='{1}_GenericFunctionalLimitations' value='1' type='checkbox' {0} />", functionalLimitations != null && functionalLimitations.Contains("1") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFunctionalLimitations1" class="radio">Transfer</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations2' name='{1}_GenericFunctionalLimitations' value='2' type='checkbox' {0} />", functionalLimitations != null && functionalLimitations.Contains("2") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFunctionalLimitations2" class="radio">Gait</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations3' name='{1}_GenericFunctionalLimitations' value='3' type='checkbox' {0} />", functionalLimitations != null && functionalLimitations.Contains("3") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFunctionalLimitations3" class="radio">Strength</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations4' name='{1}_GenericFunctionalLimitations' value='4' type='checkbox' {0} />", functionalLimitations != null && functionalLimitations.Contains("4") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFunctionalLimitations4" class="radio">Bed Mobility</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations5' name='{1}_GenericFunctionalLimitations' value='5' type='checkbox' {0} />", functionalLimitations != null && functionalLimitations.Contains("5") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFunctionalLimitations5" class="radio">Safety Techniques</label>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations6' name='{1}_GenericFunctionalLimitations' value='6' type='checkbox' {0} />", functionalLimitations != null && functionalLimitations.Contains("6") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFunctionalLimitations6" class="radio">ADL&#8217;s</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations7' name='{1}_GenericFunctionalLimitations' value='7' type='checkbox' {0} />", functionalLimitations != null && functionalLimitations.Contains("7") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFunctionalLimitations7" class="radio">ROM</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericFunctionalLimitations8' name='{1}_GenericFunctionalLimitations' value='8' type='checkbox' {0} />", functionalLimitations != null && functionalLimitations.Contains("8") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericFunctionalLimitations8" class="radio">W/C Mobility</label>
                            </td><td>
                                <label for="<%= Model.Type %>_GenericFunctionalLimitationsOther" class="float-left">Other</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFunctionalLimitationsOther", data.ContainsKey("GenericFunctionalLimitationsOther") ? data["GenericFunctionalLimitationsOther"].Answer : "", new { @id = Model.Type + "_GenericFunctionalLimitationsOther", @class = "fill" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="third">
                    <label for="<%= Model.Type %>_GenericSubjective" class="float-left">Subjective:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericSubjective", data.ContainsKey("GenericSubjective") ? data["GenericSubjective"].Answer : string.Empty, new { @class = "loc", @id = Model.Type + "_GenericSubjective" })%>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericPainAssessmentLocation" class="float-left">Pain Location:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericPainAssessmentLocation", data.ContainsKey("GenericPainAssessmentLocation") ? data["GenericPainAssessmentLocation"].Answer : string.Empty, new { @class = "loc", @id = Model.Type + "_GenericPainAssessmentLocation" })%>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericIntensityOfPain" class="float-left">Pain Level:</label>
                    <div class="float-right"><%
                        var GenericIntensityOfPain = new SelectList(new[] {
                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "Moderate Pain", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" }
                        }, "Value", "Text", data.ContainsKey("GenericIntensityOfPain") ? data["GenericIntensityOfPain"].Answer : "0"); %>
                        <%= Html.DropDownList(Model.Type + "_GenericIntensityOfPain", GenericIntensityOfPain, new { @id = Model.Type + "_GenericIntensityOfPain", @class = "oe" })%>
                    </div>
                </div><table class="fixed">
                    <tbody>
                        <tr>
                            <td colspan="8">
                                <label class="float-left">Objective: Skilled Intervention consisted of the following: Level of Assist:</label>
                                <input type="hidden" name="<%= Model.Type %>_GenericObjectiveLevelOfAssist" value="" /> 
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist1' name='{1}_GenericObjectiveLevelOfAssist' value='1' type='checkbox' {0} />", objectiveLevelOfAssist != null && objectiveLevelOfAssist.Contains("1") ? "checked='checked'" : "",Model.Type) %>
                                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist1" class="radio">IND</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist2' name='{1}_GenericObjectiveLevelOfAssist' value='2' type='checkbox' {0} />", objectiveLevelOfAssist != null && objectiveLevelOfAssist.Contains("2") ? "checked='checked'" : "",Model.Type) %>
                                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist2" class="radio">SUP</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist3' name='{1}_GenericObjectiveLevelOfAssist' value='3' type='checkbox' {0} />", objectiveLevelOfAssist != null && objectiveLevelOfAssist.Contains("3") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist3" class="radio">SBA</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist4' name='{1}_GenericObjectiveLevelOfAssist' value='4' type='checkbox' {0} />", objectiveLevelOfAssist != null && objectiveLevelOfAssist.Contains("4") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist4" class="radio">CGA</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist5' name='{1}_GenericObjectiveLevelOfAssist' value='5' type='checkbox' {0} />", objectiveLevelOfAssist != null && objectiveLevelOfAssist.Contains("5") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist5" class="radio">MIN</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist6' name='{1}_GenericObjectiveLevelOfAssist' value='6' type='checkbox' {0} />", objectiveLevelOfAssist != null && objectiveLevelOfAssist.Contains("6") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist6" class="radio">MOD</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist7' name='{1}_GenericObjectiveLevelOfAssist' value='7' type='checkbox' {0} />", objectiveLevelOfAssist != null && objectiveLevelOfAssist.Contains("7") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist7" class="radio">MAX</label>
                            </td><td>
                                <%= string.Format("<input class='radio float-left' id='{1}_GenericObjectiveLevelOfAssist8' name='{1}_GenericObjectiveLevelOfAssist' value='8' type='checkbox' {0} />", objectiveLevelOfAssist != null && objectiveLevelOfAssist.Contains("8") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericObjectiveLevelOfAssist8" class="radio">DEP</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th colspan="2">ADL Training</th>
            <th colspan="2">Neuromuscular Reeducation</th>
        </tr><tr>
            <td colspan="2" rowspan="3">
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLBathing" class="float-left">Bathing:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericADLBathing", data.ContainsKey("GenericADLBathing") ? data["GenericADLBathing"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericADLBathing" })%>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLUEDressing" class="float-left">UE Dressing:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericADLUEDressing", data.ContainsKey("GenericADLUEDressing") ? data["GenericADLUEDressing"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericADLUEDressing" })%>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLLEDressing" class="float-left">LE Dressing:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericADLLEDressing", data.ContainsKey("GenericADLLEDressing") ? data["GenericADLLEDressing"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericADLLEDressing" })%>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLGrooming" class="float-left">Grooming:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericADLGrooming", data.ContainsKey("GenericADLGrooming") ? data["GenericADLGrooming"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericADLGrooming" })%>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLToileting" class="float-left">Toileting:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericADLToileting", data.ContainsKey("GenericADLToileting") ? data["GenericADLToileting"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericADLToileting" })%>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLFeeding" class="float-left">Feeding:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericADLFeeding", data.ContainsKey("GenericADLFeeding") ? data["GenericADLFeeding"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericADLFeeding" })%>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLMealPrep" class="float-left">Meal Prep:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericADLMealPrep", data.ContainsKey("GenericADLMealPrep") ? data["GenericADLMealPrep"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericADLMealPrep" })%>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLHouseCleaning" class="float-left">House Cleaning:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericADLHouseCleaning", data.ContainsKey("GenericADLHouseCleaning") ? data["GenericADLHouseCleaning"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericADLHouseCleaning" })%>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLHouseSafety" class="float-left">House Safety:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericADLHouseSafety", data.ContainsKey("GenericADLHouseSafety") ? data["GenericADLHouseSafety"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericADLHouseSafety" })%>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLAdaptiveEquipment" class="float-left">Adaptive Equipment/Assistive Device Use or Needs:</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericADLAdaptiveEquipment", data.ContainsKey("GenericADLAdaptiveEquipment") ? data["GenericADLAdaptiveEquipment"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericADLAdaptiveEquipment" })%>
                    </div>
                </div>
            </td><td colspan="2">
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist" class="float-left">Sitting Balance Activities</label>
                    <div class="float-right align-right">
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist">Static:</label>
                        <%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesStaticAssist", data.ContainsKey("GenericSittingBalanceActivitiesStaticAssist") ? data["GenericSittingBalanceActivitiesStaticAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesStaticAssist" })%>
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesStaticAssist">% assist</label><br />
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist">Dynamic</label>
                        <%= Html.TextBox(Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist", data.ContainsKey("GenericSittingBalanceActivitiesDynamicAssist") ? data["GenericSittingBalanceActivitiesDynamicAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist" })%>
                        <label for="<%= Model.Type %>_GenericSittingBalanceActivitiesDynamicAssist">% assist</label>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist" class="float-left">Standing Balance Activities</label>
                    <div class="float-right align-right">
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist">Static:</label>
                        <%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesStaticAssist", data.ContainsKey("GenericSittingBalanceActivitiesStaticAssist") ? data["GenericSittingBalanceActivitiesStaticAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesStaticAssist" })%>
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesStaticAssist">% assist</label><br />
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist">Dynamic</label>
                        <%= Html.TextBox(Model.Type + "_GenericStandingBalanceActivitiesDynamicAssist", data.ContainsKey("GenericSittingBalanceActivitiesDynamicAssist") ? data["GenericSittingBalanceActivitiesDynamicAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericSittingBalanceActivitiesDynamicAssist" })%>
                        <label for="<%= Model.Type %>_GenericStandingBalanceActivitiesDynamicAssist">% assist</label>
                    </div>
                </div><div class="padnoterow">
                    <input type="hidden" name="<%= Model.Type %>_GenericUEWeightBearing" value="" />
                    <%= string.Format("<input id='{1}_GenericUEWeightBearing1' class='radio' name='{1}_GenericUEWeightBearing' value='1' type='checkbox' {0} />", genericUEWeightBearing != null && genericUEWeightBearing.Contains("1") ? "checked='checked'" : "",Model.Type)%>
                    <label for="<%= Model.Type %>_GenericUEWeightBearing1">UE Weight-Bearing Activities</label>
                </div>
            </td>
        </tr><tr>
            <th colspan="2">Therapeutic Exercise</th>
        </tr><tr>
            <td colspan="2">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROM" class="float-left">ROM</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROM", data.ContainsKey("GenericTherapeuticExerciseROM") ? data["GenericTherapeuticExerciseROM"].Answer : string.Empty, new { @id = Model.Type + "_GenericTherapeuticExerciseROM" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROMSet", data.ContainsKey("GenericTherapeuticExerciseROMSet") ? data["GenericTherapeuticExerciseROMSet"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseROMSet" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseROMReps", data.ContainsKey("GenericTherapeuticExerciseROMReps") ? data["GenericTherapeuticExerciseROMReps"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseROMReps" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMReps">reps</label>
                            </td>
                        </tr><tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseAROMAAROM" class="float-left">AROM/AAROM</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROM", data.ContainsKey("GenericTherapeuticExerciseAROMAAROM") ? data["GenericTherapeuticExerciseAROMAAROM"].Answer : string.Empty, new { @id = Model.Type + "_GenericTherapeuticExerciseAROMAAROM" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROMSet", data.ContainsKey("GenericTherapeuticExerciseAROMAAROMSet") ? data["GenericTherapeuticExerciseAROMAAROMSet"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseAROMAAROMSet" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseAROMAAROMReps", data.ContainsKey("GenericTherapeuticExerciseAROMAAROMReps") ? data["GenericTherapeuticExerciseAROMAAROMReps"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseAROMAAROMReps" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseAROMAAROMReps">reps</label>
                            </td>
                        </tr><tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseResistive" class="float-left">Resistive (Type)</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistive", data.ContainsKey("GenericTherapeuticExerciseResistive") ? data["GenericTherapeuticExerciseResistive"].Answer : string.Empty, new { @id = Model.Type + "_GenericTherapeuticExerciseResistive" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistiveSet", data.ContainsKey("GenericTherapeuticExerciseResistiveSet") ? data["GenericTherapeuticExerciseResistiveSet"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseResistiveSet" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseResistiveReps", data.ContainsKey("GenericTherapeuticExerciseResistiveReps") ? data["GenericTherapeuticExerciseResistiveReps"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseResistiveReps" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseResistiveReps">reps</label>
                            </td>
                        </tr><tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseStretching" class="float-left">Stretching</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretching", data.ContainsKey("GenericTherapeuticExerciseStretching") ? data["GenericTherapeuticExerciseStretching"].Answer : string.Empty, new { @id = Model.Type + "_GenericTherapeuticExerciseStretching" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretchingSet", data.ContainsKey("GenericTherapeuticExerciseStretchingSet") ? data["GenericTherapeuticExerciseStretchingSet"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseStretchingSet" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseROMSet">set(s)</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseStretchingReps", data.ContainsKey("GenericTherapeuticExerciseStretchingReps") ? data["GenericTherapeuticExerciseStretchingReps"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTherapeuticExerciseStretchingReps" })%>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseStretchingReps">reps</label>
                            </td>
                        </tr><tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericTherapeuticExerciseOther" class="float-left">Other:</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTherapeuticExerciseOther", data.ContainsKey("GenericTherapeuticExerciseOther") ? data["GenericTherapeuticExerciseOther"].Answer : string.Empty, new { @id = Model.Type + "_GenericTherapeuticExerciseOther" })%>
                            </td><td colspan="2">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th colspan="2">Therapeutic/Dynamic Activities</th>
            <th colspan="2">Teaching</th>
        </tr><tr>
            <td colspan="2" rowspan="3">
                <div class="padnoterow">
                    <label class="float-left">Bed Mobility:</label>
                    <div class="margin">
                        <label for="<%= Model.Type %>_GenericBedMobilityRollingReps" class="float-left">Rolling:</label>
                        <div class="float-right">
                            <label for="<%= Model.Type %>_GenericBedMobilityRollingReps">X</label>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingReps", data.ContainsKey("GenericBedMobilityRollingReps") ? data["GenericBedMobilityRollingReps"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingReps" })%>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingAssist", data.ContainsKey("GenericBedMobilityRollingAssist") ? data["GenericBedMobilityRollingAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingAssist" })%>
                            <label for="<%= Model.Type %>_GenericBedMobilityRollingAssist">% assist</label>
                        </div>
                        <div class="clear"></div>
                        <label for="<%= Model.Type %>_GenericBedMobilitySupineToSitReps" class="float-left">Supine to Sit:</label>
                        <div class="float-right">
                            <label for="<%= Model.Type %>_GenericBedMobilitySupineToSitReps">X</label>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilitySupineToSitReps", data.ContainsKey("GenericBedMobilityRollingReps") ? data["GenericBedMobilityRollingReps"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingReps" })%>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilitySupineToSitAssist", data.ContainsKey("GenericBedMobilityRollingAssist") ? data["GenericBedMobilityRollingAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingAssist" })%>
                            <label for="<%= Model.Type %>_GenericBedMobilitySupineToSitAssist">% assist</label>
                        </div>
                        <div class="clear"></div>
                        <label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingReps" class="float-left">Dynamic Reaching:</label>
                        <div class="float-right">
                            <label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingReps">X</label>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityDynamicReachingReps", data.ContainsKey("GenericBedMobilityRollingReps") ? data["GenericBedMobilityRollingReps"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingReps" })%>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityDynamicReachingAssist", data.ContainsKey("GenericBedMobilityRollingAssist") ? data["GenericBedMobilityRollingAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingAssist" })%>
                            <label for="<%= Model.Type %>_GenericBedMobilityDynamicReachingAssist">% assist</label>
                        </div>
                        <div class="clear"></div>
                        <label for="<%= Model.Type %>_GenericBedMobilityGrossFineMotorCoordReps" class="float-left">Gross/Fine Motor Coord:</label>
                        <div class="float-right">
                            <label for="<%= Model.Type %>_GenericBedMobilityGrossFineMotorCoordReps">X</label>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityGrossFineMotorCoordReps", data.ContainsKey("GenericBedMobilityRollingReps") ? data["GenericBedMobilityRollingReps"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingReps" })%>
                            <%= Html.TextBox(Model.Type + "_GenericBedMobilityGrossFineMotorCoordAssist", data.ContainsKey("GenericBedMobilityRollingAssist") ? data["GenericBedMobilityRollingAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingAssist" })%>
                            <label for="<%= Model.Type %>_GenericBedMobilityGrossFineMotorCoordAssist">% assist</label>
                        </div>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericTransfersType" class="float-left">Transfers:</label>
                    <div class="float-right">
                        <label for="<%= Model.Type %>_GenericTransfersType">Type:</label>
                        <%= Html.TextBox(Model.Type + "_GenericTransfersType", data.ContainsKey("GenericTransfersType") ? data["GenericTransfersType"].Answer : string.Empty, new { @id = Model.Type + "_GenericTransfersType" }) %>
                        <label for="<%= Model.Type %>_GenericBedMobilityGrossFineMotorCoordReps">X</label>
                        <%= Html.TextBox(Model.Type + "_GenericBedMobilityGrossFineMotorCoordReps", data.ContainsKey("GenericBedMobilityGrossFineMotorCoordReps") ? data["GenericBedMobilityGrossFineMotorCoordReps"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilityGrossFineMotorCoordReps" })%>
                        <%= Html.TextBox(Model.Type + "_GenericBedMobilityGrossFineMotorCoordAssist", data.ContainsKey("GenericBedMobilityGrossFineMotorCoordAssist") ? data["GenericBedMobilityGrossFineMotorCoordAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilityGrossFineMotorCoordAssist" })%>
                        <label for="<%= Model.Type %>_GenericBedMobilityGrossFineMotorCoordAssist">% assist</label>
                    </div>
                    <div class="clear"></div>
                    <div class="margin">
                        <label class="float-left">Correct Unfolding:</label>
                        <div class="float-right">
                            <%= Html.RadioButton(Model.Type + "_GenericCorrectUnfolding", "1", data.ContainsKey("GenericCorrectUnfolding") && data["GenericCorrectUnfolding"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericCorrectUnfolding1", @class = "radio" }) %>
                            <label for="<%= Model.Type %>_GenericCorrectUnfolding1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton(Model.Type + "_GenericCorrectUnfolding", "0", data.ContainsKey("GenericCorrectUnfolding") && data["GenericCorrectUnfolding"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericCorrectUnfolding0", @class = "radio" }) %>
                            <label for="<%= Model.Type %>_GenericCorrectUnfolding0" class="inline-radio">No</label>
                        </div>
                        <div class="clear"></div>
                        <label class="float-left">Correct Foot Placement:</label> 
                        <div class="float-right">
                            <%= Html.RadioButton(Model.Type + "_GenericCorrectFootPlacement", "1", data.ContainsKey("GenericCorrectFootPlacement") && data["GenericCorrectFootPlacement"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericCorrectFootPlacement1", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericCorrectFootPlacement1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton(Model.Type + "_GenericCorrectFootPlacement", "0", data.ContainsKey("GenericCorrectFootPlacement") && data["GenericCorrectFootPlacement"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericCorrectFootPlacement0", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericCorrectFootPlacement0" class="inline-radio">No</label>
                        </div>
                        <div class="clear"></div>
                        <label class="float-left">Assistive Device:</label>
                        <div class="float-right">
                            <%= Html.RadioButton(Model.Type + "_GenericAssistiveDevice", "1", data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericAssistiveDevice1", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericAssistiveDevice1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton(Model.Type + "_GenericAssistiveDevice", "0", data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericAssistiveDevice0", @class = "radio" })%>
                            <label for="<%= Model.Type %>_GenericAssistiveDevice0" class="inline-radio">No</label>
                        </div>
                    </div>
                </div>
            </td><td colspan="2">
                <input type="hidden" name=Model.Type + "_GenericTeaching" value="" />
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericAssessment1' class='float-left radio' name='{1}_GenericAssessment' value='1' type='checkbox' {0} />", genericTeaching != null && genericTeaching.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericAssessment1" class="radio">Patient/Family</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericAssessment2' class='float-left radio' name={1}_GenericAssessment' value='2' type='checkbox' {0} />", genericTeaching != null && genericTeaching.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericAssessment2" class="radio">Caregiver</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericAssessment3' class='float-left radio' name='{1}_GenericAssessment' value='3' type='checkbox' {0} />", genericTeaching != null && genericTeaching.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericAssessment3" class="radio">Correct Use of Adaptive Equipment</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericAssessment4' class='float-left radio' name='{1}_GenericAssessment' value='4' type='checkbox' {0} />", genericTeaching != null && genericTeaching.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericAssessment4" class="radio">Safety Technique</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericAssessment5' class='float-left radio' name='{1}_GenericAssessment' value='5' type='checkbox' {0} />", genericTeaching != null && genericTeaching.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericAssessment5" class="radio">ADLs</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericAssessment6' class='float-left radio' name='{1}_GenericAssessment' value='6' type='checkbox' {0} />", genericTeaching != null && genericTeaching.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericAssessment6" class="radio">HEP</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericAssessment7' class='float-left radio' name='{1}_GenericAssessment' value='7' type='checkbox' {0} />", genericTeaching != null && genericTeaching.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericAssessment7" class="radio">Correct Use of Assistive Device</label>
                            </td><td colspan="2">
                                <label for="<%= Model.Type %>_GenericTeachingOther" class="float-left">Other: (modalities, DME/AE need, consults, etc)</label>
                                <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericTeachingOther", data.ContainsKey("GenericTeachingOther") ? data["GenericTeachingOther"].Answer : string.Empty, new { @id = Model.Type + "_GenericTeachingOther" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th colspan="2">Assessment</th>
        </tr><tr>
            <td colspan="2">
                <%= Html.TextArea(Model.Type + "_GenericAssessment", data.ContainsKey("GenericAssessment") ? data["GenericAssessment"].Answer : string.Empty, new { @id = Model.Type + "_GenericAssessment", @class = "fill" })%>
            </td>
        </tr><tr>
            <th colspan="2">W/C Training</th>
            <th colspan="2">Plan</th>
        </tr><tr>
            <td colspan="2">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <label class="float-left">Propulsion with</label>
                                <input type="hidden" name="<%= Model.Type %>_GenericPropulsionWith" value="" />
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith1' class='float-left radio' name='{1}_GenericPropulsionWith' value='1' type='checkbox' {0} />", genericPropulsionWith != null && genericPropulsionWith.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith1" class="radio">RUE</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith2' class='float-left radio' name='{1}_GenericPropulsionWith' value='2' type='checkbox' {0} />", genericPropulsionWith != null && genericPropulsionWith.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith2" class="radio">LUE</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith3' class='float-left radio' name='{1}_GenericPropulsionWith' value='3' type='checkbox' {0} />", genericPropulsionWith != null && genericPropulsionWith.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith3" class="radio">BUE</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith4' class='float-left radio' name='{1}_GenericPropulsionWith' value='4' type='checkbox' {0} />", genericPropulsionWith != null && genericPropulsionWith.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith4" class="radio">RLE</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith5' class='float-left radio' name='{1}_GenericPropulsionWith' value='5' type='checkbox' {0} />", genericPropulsionWith != null && genericPropulsionWith.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith5" class="radio">LLE</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPropulsionWith6' class='float-left radio' name='{1}_GenericPropulsionWith' value='6' type='checkbox' {0} />", genericPropulsionWith != null && genericPropulsionWith.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPropulsionWith6" class="radio">BLE</label>
                            </td>
                        </tr>
                    </tbody>
                </table><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericDistanceFT" class="float-left">Distance</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericDistanceFT", data.ContainsKey("GenericDistanceFT") ? data["GenericDistanceFT"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericDistanceFT" })%>
                        <label for="<%= Model.Type %>_GenericDistanceFT">ft</label>
                        <label for="<%= Model.Type %>_GenericDistanceFTReps">x</label>
                        <%= Html.TextBox(Model.Type + "_GenericDistanceFTReps", data.ContainsKey("GenericDistanceFTReps") ? data["GenericDistanceFTReps"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericDistanceFTReps" })%>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericManagement" class="float-left">Management</label>
                    <div class="float-right">
                        <%= Html.TextBox(Model.Type + "_GenericManagement", data.ContainsKey("GenericManagement") ? data["GenericManagement"].Answer : string.Empty, new { @id = Model.Type + "_GenericManagement" })%>
                        <%= Html.TextBox(Model.Type + "_GenericManagementAssist", data.ContainsKey("GenericManagementAssist") ? data["GenericManagementAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericManagementAssist" })%>
                        <label for="<%= Model.Type %>_GenericManagementAssist">% assist</label>
                    </div>
                </div>
            </td><td colspan="2">
                <input type="hidden" name=Model.Type + "_GenericPlan" value="" />
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPlan1' class='float-left radio' name='{1}_GenericPlan' value='1' type='checkbox' {0} />", genericPlan != null && genericPlan.Contains("1") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlan1" class="radio">Continue Prescribed Plan</label>
                            </td><td colspan="2">
                                <%= string.Format("<input id='{1}_GenericPlan6' class='float-left radio' name='{1}_GenericPlan' value='6' type='checkbox' {0} />", genericPlan != null && genericPlan.Contains("6") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlan6" class="radio">Change Prescribed Plan</label>
                                <div class="float-right">
                                    <%= Html.TextBox(Model.Type + "_GenericPlanChangePrescribed", data.ContainsKey("GenericPlanChangePrescribed") ? data["GenericPlanChangePrescribed"].Answer : string.Empty, new { @id = Model.Type + "_GenericPlanChangePrescribed" })%>
                                </div>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericPlan2' class='float-left radio' name='{1}_GenericPlan' value='2' type='checkbox' {0} />", genericPlan != null && genericPlan.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlan2" class="radio">Plan Discharge</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPlan3' class='float-left radio' name='{1}_GenericPlan' value='3' type='checkbox' {0} />", genericPlan != null && genericPlan.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlan3" class="radio">In Progress</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericPlan4' class='float-left radio' name='{1}_GenericPlan' value='4' type='checkbox' {0} />", genericPlan != null && genericPlan.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPlan4" class="radio">As of Today</label>
                            </td>
                        </tr><tr>
                            <td colspan="3">
                                <%= string.Format("<input id='{1}_GenericPlan5' class='float-left radio' name='{1}_GenericPlan' value='5' type='checkbox' {0} />", genericPlan != null && genericPlan.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                <span class="float-left">
                                    <label for="<%= Model.Type %>_GenericPlan5">Patient/Family Notified</label>
                                    <%= Html.TextBox(Model.Type + "_GenericPlanPtDaysNotice", data.ContainsKey("GenericPlanPtDaysNotice") ? data["GenericPlanPtDaysNotice"].Answer : "5", new { @id = Model.Type + "_GenericPlanPtDaysNotice", @class = "small-digit" })%>
                                    <label for="<%= Model.Type %>_GenericPlan5">Days Prior to Discharge</label>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table><div class="padnoterow">
                    <div class="float-left">
                        <strong>Agency Notification</strong>
                        <%= Html.TextBox(Model.Type + "_GenericPlanAgencyDaysNotice", data.ContainsKey("GenericPlanAgencyDaysNotice") ? data["GenericPlanAgencyDaysNotice"].Answer : "5", new { @id = Model.Type + "_GenericPlanAgencyDaysNotice", @class = "small-digit" })%>
                        <strong>Days Prior?</strong>
                    </div><div class="float-right">
                        <%= Html.RadioButton(Model.Type + "_GenericPlanIsAgencyNotification", "1", data.ContainsKey("GenericPlanIsAgencyNotification") && data["GenericPlanIsAgencyNotification"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericPlanIsAgencyNotification1", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericPlanIsAgencyNotification1" class="inline-radio">Yes</label>
                        <%= Html.RadioButton(Model.Type + "_GenericPlanIsAgencyNotification", "0", data.ContainsKey("GenericPlanIsAgencyNotification") && data["GenericPlanIsAgencyNotification"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericPlanIsAgencyNotification0", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericPlanIsAgencyNotification0" class="inline-radio">No</label>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>