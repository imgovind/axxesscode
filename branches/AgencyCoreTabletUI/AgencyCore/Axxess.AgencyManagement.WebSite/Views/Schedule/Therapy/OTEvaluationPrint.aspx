﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%var dictonary = new Dictionary<string, string>() {
    { DisciplineTasks.OTEvaluation.ToString(), "Occupational Therapy Evaluation" },
    { DisciplineTasks.OTReEvaluation.ToString(), "Occupational Therapy ReEvaluation" },
    { DisciplineTasks.OTDischarge.ToString(), "Occupational Therapy Discharge" },
    { DisciplineTasks.OTMaintenance.ToString(), "Occupational Therapy Maintenance Visit" }
     }; %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %> <%= dictonary.ContainsKey(Model.Type) ? dictonary[Model.Type] : string.Empty %><%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body></body><%
string[] functionLimitations = data != null && data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null;
string[] patientCondition = data != null && data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null;
string[] serviceProvided = data != null && data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
    .Add("jquery-1.7.1.min.js")
    .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
    .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
).OnDocumentReady(() => { %>
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3E<%= dictonary.ContainsKey(Model.Type) ? dictonary[Model.Type] : ""%>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        '<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>' +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3EOccupational Therapy <%= Model.Type == "OTDischarge" ? "Discharge" : (Model.Type == "OTReEvaluation" ? "Re-" : "") + "Evaluation" %>%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EPhysician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E%3C/span%3E%3Cspan%3E%3C/span%3E%3C/span%3E%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
    printview.addsection(
        printview.col(4,
            printview.span("Medical Diagnosis",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericMedicalDiagnosis") && data["GenericMedicalDiagnosis"].Answer.IsNotNullOrEmpty() ? data["GenericMedicalDiagnosis"].Answer.Clean() : "" %>",0,1) +
            printview.checkbox("Onset",<%= data != null && data.ContainsKey("GenericMedicalDiagnosisOnset") && data["GenericMedicalDiagnosisOnset"].Answer == "1" ? "true" : "false" %>) +
            printview.span("<%= data != null && data.ContainsKey("MedicalDiagnosisDate") && data["MedicalDiagnosisDate"].Answer.IsNotNullOrEmpty() ? data["MedicalDiagnosisDate"].Answer.Clean() : "" %>",0,1) +
            printview.span("OT Diagnosis",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericOTDiagnosis") && data["GenericOTDiagnosis"].Answer.IsNotNullOrEmpty() ? data["GenericOTDiagnosis"].Answer.Clean() : "" %>",0,1) +
            printview.checkbox("Onset",<%= data != null && data.ContainsKey("GenericOTDiagnosisOnset") && data["GenericOTDiagnosisOnset"].Answer == "1" ? "true" : "false"%>) +
            printview.span("<%= data != null && data.ContainsKey("OTDiagnosisDate") && data["OTDiagnosisDate"].Answer.IsNotNullOrEmpty() ? data["OTDiagnosisDate"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Past Medical/Surgical History",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericGenericPastMedicalHistory") && data["GenericGenericPastMedicalHistory"].Answer.IsNotNullOrEmpty() ? data["GenericGenericPastMedicalHistory"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Prior Functional Status",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPriorFunctionalStatus") && data["GenericPriorFunctionalStatus"].Answer.IsNotNullOrEmpty() ? data["GenericPriorFunctionalStatus"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Pain Location",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPainAssessmentLocation") && data["GenericPainAssessmentLocation"].Answer.IsNotNullOrEmpty() ? data["GenericPainAssessmentLocation"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Pain Level",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericIntensityOfPain") && data["GenericIntensityOfPain"].Answer.IsNotNullOrEmpty() ? data["GenericIntensityOfPain"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Mental Status",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericMentalStatus") && data["GenericMentalStatus"].Answer.IsNotNullOrEmpty() ? data["GenericMentalStatus"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Precautions",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPrecautions") && data["GenericPrecautions"].Answer.IsNotNullOrEmpty() ? data["GenericPrecautions"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Sensation",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericSensation") && data["GenericSensation"].Answer.IsNotNullOrEmpty() ? data["GenericSensation"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Muscle Tone",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericMuscleTone") && data["GenericMuscleTone"].Answer.IsNotNullOrEmpty() ? data["GenericMuscleTone"].Answer.Clean() : string.Empty %>",0,1)) +
        printview.col(6,
            printview.span("Endurance",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericEndurance") && data["GenericEndurance"].Answer.IsNotNullOrEmpty() ? data["GenericEndurance"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Edema",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericEdema") && data["GenericEdema"].Answer.IsNotNullOrEmpty() ? data["GenericEdema"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Coordination",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericCoordination") && data["GenericCoordination"].Answer.IsNotNullOrEmpty() ? data["GenericCoordination"].Answer.Clean() : string.Empty %>",0,1)) +
        printview.col(5,
            printview.span("Req. Assistance With",true) +
            printview.checkbox("Gait",<%= data != null && data.ContainsKey("GenericHomeboundStatusAssist") && data["GenericHomeboundStatusAssist"].Answer.Split(',').Contains("1") ? "true" : "false" %>) +
            printview.checkbox("Leaving the Home",<%= data != null && data.ContainsKey("GenericHomeboundStatusAssist") && data["GenericHomeboundStatusAssist"].Answer.Split(',').Contains("2") ? "true" : "false" %>) +
            printview.checkbox("Transfers",<%= data != null && data.ContainsKey("GenericHomeboundStatusAssist") && data["GenericHomeboundStatusAssist"].Answer.Split(',').Contains("3") ? "true" : "false" %>) +
            printview.checkbox("SOB/Endurance",<%= data != null && data.ContainsKey("GenericHomeboundStatusAssist") && data["GenericHomeboundStatusAssist"].Answer.Split(',').Contains("4") ? "true" : "false" %>)) +
        printview.span("Medical History",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericMedicalHistory") && data["GenericMedicalHistory"].Answer.IsNotNullOrEmpty() ? data["GenericMedicalHistory"].Answer.Clean() : string.Empty %>"),
        "Diagnosis");
    printview.addsection(
        printview.col(3,
            printview.span("") +
            printview.span("%3Cspan class=%22align-center fill%22%3EROM%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3EStrength%3C/span%3E",true)) +
        printview.col(6,
            printview.span("Part",true) +
            printview.span("Action",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ERight%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ELeft%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ERight%3C/span%3E",true) +
            printview.span("%3Cspan class=%22align-center fill%22%3ELeft%3C/span%3E",true) +
            printview.span("Shoulder",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionROMRight") && data["GenericShoulderFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionROMLeft") && data["GenericShoulderFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionStrengthRight") && data["GenericShoulderFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderFlexionStrengthLeft") && data["GenericShoulderFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionROMRight") && data["GenericShoulderExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionROMLeft") && data["GenericShoulderExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionStrengthRight") && data["GenericShoulderExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtensionStrengthLeft") && data["GenericShoulderExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtensionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Abduction") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionROMRight") && data["GenericShoulderAbductionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionROMLeft") && data["GenericShoulderAbductionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionStrengthRight") && data["GenericShoulderAbductionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderAbductionStrengthLeft") && data["GenericShoulderAbductionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderAbductionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Int Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotROMRight") && data["GenericShoulderIntRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotROMLeft") && data["GenericShoulderIntRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotStrengthRight") && data["GenericShoulderIntRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderIntRotStrengthLeft") && data["GenericShoulderIntRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderIntRotStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Ext Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotROMRight") && data["GenericShoulderExtRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotROMLeft") && data["GenericShoulderExtRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotStrengthRight") && data["GenericShoulderExtRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericShoulderExtRotStrengthLeft") && data["GenericShoulderExtRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericShoulderExtRotStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Elbow",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionROMRight") && data["GenericElbowFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionROMLeft") && data["GenericElbowFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionStrengthRight") && data["GenericElbowFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowFlexionStrengthLeft") && data["GenericElbowFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionROMRight") && data["GenericElbowExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionROMLeft") && data["GenericElbowExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionStrengthRight") && data["GenericElbowExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericElbowExtensionStrengthLeft") && data["GenericElbowExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericElbowExtensionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Finger",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionROMRight") && data["GenericFingerFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionROMLeft") && data["GenericFingerFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionStrengthRight") && data["GenericFingerFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerFlexionStrengthLeft") && data["GenericFingerFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionROMRight") && data["GenericFingerExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionROMLeft") && data["GenericFingerExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionStrengthRight") && data["GenericFingerExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericFingerExtensionStrengthLeft") && data["GenericFingerExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericFingerExtensionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Wrist",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionROMRight") && data["GenericWristFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionROMLeft") && data["GenericWristFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionStrengthRight") && data["GenericWristFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristFlexionStrengthLeft") && data["GenericWristFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionROMRight") && data["GenericWristExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionROMLeft") && data["GenericWristExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionStrengthRight") && data["GenericWristExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericWristExtensionStrengthLeft") && data["GenericWristExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericWristExtensionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Hip",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionROMRight") && data["GenericHipFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionROMLeft") && data["GenericHipFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionStrengthRight") && data["GenericHipFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipFlexionStrengthLeft") && data["GenericHipFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionROMRight") && data["GenericHipExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionROMLeft") && data["GenericHipExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionStrengthRight") && data["GenericHipExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtensionStrengthLeft") && data["GenericHipExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtensionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Abduction") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionROMRight") && data["GenericHipAbductionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionROMLeft") && data["GenericHipAbductionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionStrengthRight") && data["GenericHipAbductionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipAbductionStrengthLeft") && data["GenericHipAbductionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipAbductionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Int Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotROMRight") && data["GenericHipIntRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotROMLeft") && data["GenericHipIntRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotStrengthRight") && data["GenericHipIntRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipIntRotStrengthLeft") && data["GenericHipIntRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipIntRotStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Ext Rot") +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotROMRight") && data["GenericHipExtRotROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotROMLeft") && data["GenericHipExtRotROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotStrengthRight") && data["GenericHipExtRotStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericHipExtRotStrengthLeft") && data["GenericHipExtRotStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericHipExtRotStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Knee",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionROMRight") && data["GenericKneeFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionROMLeft") && data["GenericKneeFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionStrengthRight") && data["GenericKneeFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeFlexionStrengthLeft") && data["GenericKneeFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionROMRight") && data["GenericKneeExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionROMLeft") && data["GenericKneeExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionStrengthRight") && data["GenericKneeExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericKneeExtensionStrengthLeft") && data["GenericKneeExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericKneeExtensionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Ankle",true) +
            printview.span("Plantarflexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionROMRight") && data["GenericAnklePlantFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionROMLeft") && data["GenericAnklePlantFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionStrengthRight") && data["GenericAnklePlantFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnklePlantFlexionStrengthLeft") && data["GenericAnklePlantFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnklePlantFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Dorsiflexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionROMRight") && data["GenericAnkleFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionROMLeft") && data["GenericAnkleFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionStrengthRight") && data["GenericAnkleFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericAnkleFlexionStrengthLeft") && data["GenericAnkleFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericAnkleFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Trunk",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionROMRight") && data["GenericTrunkFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionROMLeft") && data["GenericTrunkFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionStrengthRight") && data["GenericTrunkFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkFlexionStrengthLeft") && data["GenericTrunkFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Rotation") +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationROMRight") && data["GenericTrunkRotationROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationROMLeft") && data["GenericTrunkRotationROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationStrengthRight") && data["GenericTrunkRotationStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkRotationStrengthLeft") && data["GenericTrunkRotationStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkRotationStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionROMRight") && data["GenericTrunkExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionROMLeft") && data["GenericTrunkExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionStrengthRight") && data["GenericTrunkExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTrunkExtensionStrengthLeft") && data["GenericTrunkExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericTrunkExtensionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Neck",true) +
            printview.span("Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionROMRight") && data["GenericNeckFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionROMLeft") && data["GenericNeckFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionStrengthRight") && data["GenericNeckFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckFlexionStrengthLeft") && data["GenericNeckFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Extension") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionROMRight") && data["GenericNeckExtensionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionROMLeft") && data["GenericNeckExtensionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionStrengthRight") && data["GenericNeckExtensionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckExtensionStrengthLeft") && data["GenericNeckExtensionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckExtensionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Lat Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionROMRight") && data["GenericNeckLatFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionROMLeft") && data["GenericNeckLatFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionStrengthRight") && data["GenericNeckLatFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLatFlexionStrengthLeft") && data["GenericNeckLatFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLatFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Long Flexion") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionROMRight") && data["GenericNeckLongFlexionROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionROMLeft") && data["GenericNeckLongFlexionROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionStrengthRight") && data["GenericNeckLongFlexionStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckLongFlexionStrengthLeft") && data["GenericNeckLongFlexionStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckLongFlexionStrengthLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Rotation") +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationROMRight") && data["GenericNeckRotationROMRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationROMRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationROMLeft") && data["GenericNeckRotationROMLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationROMLeft"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationStrengthRight") && data["GenericNeckRotationStrengthRight"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationStrengthRight"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeckRotationStrengthLeft") && data["GenericNeckRotationStrengthLeft"].Answer.IsNotNullOrEmpty() ? data["GenericNeckRotationStrengthLeft"].Answer.Clean() : string.Empty %>",0,1)),
        "Physical Assessment");
    printview.addsection(
        printview.col(6,
            printview.span("Bathing",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLBathing") && data["GenericADLBathing"].Answer.IsNotNullOrEmpty() ? data["GenericADLBathing"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("UE Dressing",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLUEDressing") && data["GenericADLUEDressing"].Answer.IsNotNullOrEmpty() ? data["GenericADLUEDressing"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("LE Dressing",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLLEDressing") && data["GenericADLLEDressing"].Answer.IsNotNullOrEmpty() ? data["GenericADLLEDressing"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Grooming",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLGrooming") && data["GenericADLGrooming"].Answer.IsNotNullOrEmpty() ? data["GenericADLGrooming"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Toileting",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLToileting") && data["GenericADLToileting"].Answer.IsNotNullOrEmpty() ? data["GenericADLToileting"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Feeding",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLFeeding") && data["GenericADLFeeding"].Answer.IsNotNullOrEmpty() ? data["GenericADLFeeding"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Meal Prep",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLMealPrep") && data["GenericADLMealPrep"].Answer.IsNotNullOrEmpty() ? data["GenericADLMealPrep"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("House Cleaning",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLHouseCleaning") && data["GenericADLHouseCleaning"].Answer.IsNotNullOrEmpty() ? data["GenericADLHouseCleaning"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Adapt. Equip.",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLAdaptiveEquipment") && data["GenericADLAdaptiveEquipment"].Answer.IsNotNullOrEmpty() ? data["GenericADLAdaptiveEquipment"].Answer.Clean() : string.Empty %>",0,1)
        ) +
        printview.col(2,
            printview.col(2,
                printview.span("Bed Mobility",true) +
                printview.col(2,
                    printview.span("Rolling:",true) +
                    printview.span("<%= data != null && data.ContainsKey("GenericADLBedMobilityRolling") && data["GenericADLBedMobilityRolling"].Answer.IsNotNullOrEmpty() ? data["GenericADLBedMobilityRolling"].Answer.Clean() : string.Empty %>",0,1))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Supine&#8594;Sit:",true) +
                    printview.span("<%= data != null && data.ContainsKey("GenericADLBedMobilitySupineToSit") && data["GenericADLBedMobilitySupineToSit"].Answer.IsNotNullOrEmpty() ? data["GenericADLBedMobilitySupineToSit"].Answer.Clean() : string.Empty %>",0,1)) +
                printview.col(2,
                    printview.span("Sit&#8594;Stand:",true) +
                    printview.span("<%= data != null && data.ContainsKey("GenericADLBedMobilitySitToStand") && data["GenericADLBedMobilitySitToStand"].Answer.IsNotNullOrEmpty() ? data["GenericADLBedMobilitySitToStand"].Answer.Clean() : string.Empty %>",0,1)))) +
        printview.col(5,
            printview.span("Transfers",true) +
            printview.span("Tub/Shower/Toilet:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLTransfersTubShowerToilet") && data["GenericADLTransfersTubShowerToilet"].Answer.IsNotNullOrEmpty() ? data["GenericADLTransfersTubShowerToilet"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Bed&#8594;Chair:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLTransfersBedToChairWheelchair") && data["GenericADLTransfersBedToChairWheelchair"].Answer.IsNotNullOrEmpty() ? data["GenericADLTransfersBedToChairWheelchair"].Answer.Clean() : string.Empty %>",0,1)) +
        printview.col(2,
            printview.span("W/C Mobility:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLWCMobility") && data["GenericADLWCMobility"].Answer.IsNotNullOrEmpty() ? data["GenericADLWCMobility"].Answer.Clean() : string.Empty %>",0,1) +
            printview.col(2,
                printview.span("Sitting Balance",true) +
                printview.span("Static:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data != null && data.ContainsKey("GenericADLSittingBalanceStatic") && data["GenericADLSittingBalanceStatic"].Answer == "2" ? "true" : "false"%>) +
                printview.checkbox("Fair",<%= data != null && data.ContainsKey("GenericADLSittingBalanceStatic") && data["GenericADLSittingBalanceStatic"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("Poor",<%= data != null && data.ContainsKey("GenericADLSittingBalanceStatic") && data["GenericADLSittingBalanceStatic"].Answer == "0" ? "true" : "false"%>)) +
            printview.col(2,
                printview.span("") +
                printview.span("Dynamic:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data != null && data.ContainsKey("GenericADLSittingBalanceDynamic") && data["GenericADLSittingBalanceDynamic"].Answer == "2" ? "true" : "false"%>) +
                printview.checkbox("Fair",<%= data != null && data.ContainsKey("GenericADLSittingBalanceDynamic") && data["GenericADLSittingBalanceDynamic"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("Poor",<%= data != null && data.ContainsKey("GenericADLSittingBalanceDynamic") && data["GenericADLSittingBalanceDynamic"].Answer == "0" ? "true" : "false"%>)) +
            printview.col(2,
                printview.span("Standing Balance",true) +
                printview.span("Static:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data != null && data.ContainsKey("GenericADLStandingBalanceStatic") && data["GenericADLStandingBalanceStatic"].Answer == "2" ? "true" : "false"%>) +
                printview.checkbox("Fair",<%= data != null && data.ContainsKey("GenericADLStandingBalanceStatic") && data["GenericADLStandingBalanceStatic"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("Poor",<%= data != null && data.ContainsKey("GenericADLStandingBalanceStatic") && data["GenericADLStandingBalanceStatic"].Answer == "0" ? "true" : "false"%>)) +
            printview.col(2,
                printview.span("") +
                printview.span("Dynamic:",true)) +
            printview.col(3,
                printview.checkbox("Good",<%= data != null && data.ContainsKey("GenericADLStandingBalanceDynamic") && data["GenericADLStandingBalanceDynamic"].Answer == "2" ? "true" : "false"%>) +
                printview.checkbox("Fair",<%= data != null && data.ContainsKey("GenericADLStandingBalanceDynamic") && data["GenericADLStandingBalanceDynamic"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("Poor",<%= data != null && data.ContainsKey("GenericADLStandingBalanceDynamic") && data["GenericADLStandingBalanceDynamic"].Answer == "0" ? "true" : "false"%>))),
        "ADLs / Functional Mobility Level / Level of Assist");
    printview.addsection(
        printview.col(4,
            printview.checkbox("B1 Evaluation",<%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("B2 Thera Ex",<%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("B3 Transfer Training",<%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("B4 Home Program",<%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("B5 Gait Training",<%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("B6 Chest PT",<%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("B7 Ultrasound",<%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("B8 Electrother",<%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
            printview.checkbox("B9 Prosthetic Training",<%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
            printview.checkbox("B10 Muscle Re-ed",<%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
            printview.checkbox("B11 Muscle Re-ed",<%= data != null && data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer.Split(',').Contains("11") ? "true" : "false"%>) +
            printview.span("Other: <%= data != null && data.ContainsKey("GenericTreatmentCodesOther") && data["GenericTreatmentCodesOther"].Answer.IsNotNullOrEmpty() ? data["GenericTreatmentCodesOther"].Answer.Clean() : string.Empty %>")),
        "Treatment Codes");
    printview.addsection(
        printview.col(4,
            printview.checkbox("Decreased Strength",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Decreased ROM",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Decreased ADL",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Ind",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Decreased Mobility",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Pain",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Decreased Endurance",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.span("") +
            printview.span("Rehab Potential",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericRehabPotential") && data["GenericRehabPotential"].Answer.IsNotNullOrEmpty() ? data["GenericRehabPotential"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Prognosis",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPrognosis") && data["GenericPrognosis"].Answer.IsNotNullOrEmpty() ? data["GenericPrognosis"].Answer.Clean() : string.Empty %>",0,1)) +
        printview.span("Comments",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericAssessmentMore") && data["GenericAssessmentMore"].Answer.IsNotNullOrEmpty() ? data["GenericAssessmentMore"].Answer.Clean() : string.Empty %>",0,2),
        "Assessment");
    printview.addsubsection(
        "%3Cspan class=%22float-left%22%3E&#160;%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("&#160;") +
                printview.span("Time Frame", true)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E1.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal1") && data["GenericShortTermGoal1"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal1"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal1TimeFrame") && data["GenericShortTermGoal1TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal1TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E2.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal2") && data["GenericShortTermGoal2"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal2"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal2TimeFrame") && data["GenericShortTermGoal2TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal2TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E3.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal3") && data["GenericShortTermGoal3"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal3"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal3TimeFrame") && data["GenericShortTermGoal3TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal3TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E4.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal4") && data["GenericShortTermGoal4"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal4"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal4TimeFrame") && data["GenericShortTermGoal4TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal4TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E5.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal5") && data["GenericShortTermGoal5"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal5"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericShortTermGoal5TimeFrame") && data["GenericShortTermGoal5TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermGoal5TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E" +
        printview.col(4,
            printview.span("Frequency",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericShortTermFrequency") && data["GenericShortTermFrequency"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermFrequency"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Duration",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericShortTermDuration") && data["GenericShortTermDuration"].Answer.IsNotNullOrEmpty() ? data["GenericShortTermDuration"].Answer.Clean() : string.Empty %>",0,1)),
        "Short Term Goals");
    printview.addsubsection(
        "%3Cspan class=%22float-left%22%3E&#160;%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("&#160;") +
                printview.span("Time Frame", true)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E1.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal1") && data["GenericLongTermGoal1"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal1"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal1TimeFrame") && data["GenericLongTermGoal1TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal1TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E2.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal2") && data["GenericLongTermGoal2"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal2"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal2TimeFrame") && data["GenericLongTermGoal2TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal2TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E3.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal3") && data["GenericLongTermGoal3"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal3"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal3TimeFrame") && data["GenericLongTermGoal3TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal3TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E4.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal4") && data["GenericLongTermGoal4"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal4"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal4TimeFrame") && data["GenericLongTermGoal4TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal4TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E%3Cspan class=%22float-left%22%3E5.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal5") && data["GenericLongTermGoal5"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal5"].Answer.Clean() : string.Empty%>",0,1) +
                printview.span("<%= data != null && data.ContainsKey("GenericLongTermGoal5TimeFrame") && data["GenericLongTermGoal5TimeFrame"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermGoal5TimeFrame"].Answer.Clean() : string.Empty%>",0,1)) +
        "%3C/span%3E" +
        printview.col(4,
            printview.span("Frequency",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericLongTermFrequency") && data["GenericLongTermFrequency"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermFrequency"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Duration",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericLongTermDuration") && data["GenericLongTermDuration"].Answer.IsNotNullOrEmpty() ? data["GenericLongTermDuration"].Answer.Clean() : string.Empty %>",0,1)),
        "Long Term Goals"); <%
}).Render(); %>
</html>
