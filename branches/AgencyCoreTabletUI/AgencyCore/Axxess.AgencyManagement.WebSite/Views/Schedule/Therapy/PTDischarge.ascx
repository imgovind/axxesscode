﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase().Clean() : "" %></span>
<%  var dictonary = new Dictionary<string, string>() {
        { DisciplineTasks.PTDischarge.ToString(), "ptDischarge" }
    };
    using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) {
        var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
    <%= Html.Hidden(Model.Type+"_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type+"_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type+"_EventId", Model.EventId)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
    <div class="wrapper main">
        <table class="fixed nursing">
            <tbody>
                <tr>
                    <th colspan="10"><%= Model.TypeName %></th>
                </tr><tr>
                    <td colspan="7">
                        <span class="bigtext"><%= Model.Patient.DisplayName %></span>
                    </td><td colspan="3">
                        <% if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                        <div class="buttons">
                            <ul>
                                <li><%= Model.CarePlanOrEvalUrl%></li>
                            </ul>
                        </div>
                        <% } %>
                    </td>
                </tr><tr>
                    <td colspan="10">
                        <div class="half">
                            <label for="<%= Model.Type %>_MR" class="float-left">MR#</label>
                            <div class="float-right">
                                <%= Html.TextBox(Model.Type+"_MR",Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty, new { @id = Model.Type+"_MR", @readonly = "readonly" }) %>
                            </div>
                        </div><div class="half">
                            <label for="<%= Model.Type %>_VisitDate" class="float-left">Visit Date:</label>
                            <div class="float-right">
                                <input type="date" name="<%= Model.Type %>_VisitDate" value="<%= Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate : "" %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%= Model.Type %>_VisitDate" class="required" />
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="half">
                            <label for="<%= Model.Type %>_TimeIn" class="float-left">Time In:</label>
                            <div class="float-right">
                                <%= Html.TextBox(Model.Type+"_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : "", new { @id = Model.Type+"_TimeIn", @class = "loc" }) %>
                            </div>
                        </div><div class="half">
                            <label for="<%= Model.Type %>_TimeOut" class="float-left">Time Out:</label>
                            <div class="float-right">
                                <%= Html.TextBox(Model.Type+"_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : "", new { @id = Model.Type+"_TimeOut", @class = "loc" }) %>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="half">
                            <label for="<%= Model.Type %>_GenericMedicalDiagnosis" class="float-left">Medical Diagnosis:</label>
                            <div class="float-right">
                                <%= Html.TextBox(Model.Type+"_GenericMedicalDiagnosis", data.ContainsKey("GenericMedicalDiagnosis") ? data["GenericMedicalDiagnosis"].Answer : string.Empty, new { @id = Model.Type+"_GenericMedicalDiagnosis" }) %>
                            </div>
                        </div><div class="half">
                            &#160;
                        </div>
                        <div class="clear"></div>
                        <% if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
                        <div class="half">
                            <label for="<%= Model.Type %>_PreviousNotes" class="float-left">Previous Notes:</label>
                            <div class="float-right">
                                <%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" }) %>
                            </div>
                        </div><div class="half">
                        </div>
                        <% } %>
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="ptDischargeContentId"><% Html.RenderPartial("~/Views/Schedule/Therapy/PTDischargeContent.ascx", Model); %></div>
        <table class="fixed nursing">
            <tbody>                
                <tr>
                    <th colspan="10">Electronic Signature</th>
                </tr><tr>
                    <td colspan="10">
                        <div class="third">
                            <label for="<%= Model.Type %>_Clinician" class="float-left">Clinician:</label>
                            <div class="float-right">
                                <%= Html.Password(Model.Type+"_Clinician", "", new { @id = Model.Type+"_Clinician" })%>
                            </div>
                        </div><div class="third"></div><div class="third">
                            <label for="<%= Model.Type %>_SignatureDate" class="float-left">Date:</label>
                            <div class="float-right"><%
                                var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate;
                                var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer.ToDateTime() : (Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate.ToDateTime() : Model.EndDate); %>
                                <input type="date" name="<%= Model.Type %>_SignatureDate" value="<%= date.ToShortDateString() %>" id="<%= Model.Type %>_SignatureDate" />
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" id="<%= Model.Type %>_Save">Save</a></li>
                <li><a href="javascript:void(0);" id="<%= Model.Type %>_Submit">Complete</a></li>
                <li><a href="javascript:void(0);" id="<%= Model.Type %>_Cancel">Exit</a></li>
            </ul>
        </div>
    </div>
<% } %>
<script type="text/javascript">
    $("#<%= Model.Type %>_MR").attr("readonly", true);
    $("#<%= Model.Type %>_Save").click(function() {
        $("#<%= Model.Type %>_TimeIn,#<%= Model.Type %>_TimeOut,#<%= Model.Type %>_Clinician,#<%= Model.Type %>_SignatureDate").removeClass("required");
        $('#<%= Model.Type %>_Button').val($(this).html()).closest('form').submit();
    });
    $("#<%= Model.Type %>_Submit").click(function() {
        $("#<%= Model.Type %>_TimeIn,#<%= Model.Type %>_TimeOut,#<%= Model.Type %>_Clinician,#<%= Model.Type %>_SignatureDate").removeClass("required").addClass("required");
        $('#<%= Model.Type %>_Button').val($(this).html()).closest('form').submit();
    });
    $("#<%= Model.Type %>_Cancel").click(function() {
        $(this).closest(".window").Close();
    });
    $("#<%= Model.Type %>_PreviousNotes").change(function() {
        $("#ptDischargeContentId").load("/Schedule/PTDischargeContent", { patientId: $("#<%= Model.Type %>_PatientId").val(), noteId: $("#<%= Model.Type %>_PreviousNotes").val() }, function(responseText, textStatus, XMLHttpRequest) { });
    });
</script>