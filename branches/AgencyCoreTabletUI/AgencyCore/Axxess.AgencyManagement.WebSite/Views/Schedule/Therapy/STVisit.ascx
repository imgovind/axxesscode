﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle"><%= Model.TypeName %> | <%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase().Clean() : "" %></span>
<%
using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "STVisitForm" })) {
    var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>();
    string[] genericSpeechTherapyDone = data.ContainsKey("GenericSpeechTherapyDone") && data["GenericSpeechTherapyDone"].Answer != "" ? data["GenericSpeechTherapyDone"].Answer.Split(',') : null;
    var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate;
    var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer.ToDateTime() : (Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate.ToDateTime() : Model.EndDate); %>
<%= Html.Hidden(Model.Type+"_PatientId", Model.PatientId)%>
<%= Html.Hidden(Model.Type+"_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden(Model.Type+"_EventId", Model.EventId)%>
<%= Html.Hidden("Type", Model.Type)%>
<%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4"><%= string.Format("{0}", Model.TypeName) %></th></tr>
            <tr><td colspan="3"><span class="bigtext"><%= Model.Patient.DisplayName %></span></td><td><% if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %><div class="buttons"><ul><li><%= Model.CarePlanOrEvalUrl%></li></ul></div><% } %></td></tr>
            <tr>
                <td colspan="4">
                    <div class="half">
                        <label for="<%= Model.Type %>_MR" class="float-left">MR#</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type+"_MR",Model!=null && Model.Patient!=null? Model.Patient.PatientIdNumber: string.Empty, new { @id = Model.Type+"_MR", @readonly = "readonly" })%></div>
                    </div><div class="half">
                        <label for="<%= Model.Type %>_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="date" name="<%= Model.Type %>_VisitDate" value="<%= Model != null && Model.VisitDate.IsNotNullOrEmpty() && Model.VisitDate.IsValidDate() ? Model.VisitDate : "" %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%= Model.Type %>_VisitDate" class="required" /></div>
                    </div><div class="clear"></div><div class="half">
                        <label for="<%= Model.Type %>_TimeIn" class="float-left">Time In:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type+"_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : "", new { @id = Model.Type+"_TimeIn", @class = "loc" })%></div>
                    </div><div class="half">
                        <label for="<%= Model.Type %>_TimeOut" class="float-left">Time Out:</label>
                        <div class="float-right"><%= Html.TextBox(Model.Type+"_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : "", new { @id = Model.Type+"_TimeOut", @class = "loc" })%></div>
                    </div>
                    <div class="half">
                        <% if (Current.HasRight(Permissions.ViewPreviousNotes)) { %><label for="<%= Model.Type %>_PreviousNotes" class="float-left">Previous Notes:</label>
                        <div class="float-right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" })%></div><% } %>
                    </div><div class="half"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="stVisitContentId"><% Html.RenderPartial("~/Views/Schedule/Therapy/STVisitNoteContent.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr><tr>
                <td colspan="4">
                    <div class="third">
                        <label for="<%= Model.Type %>_Clinician" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password(Model.Type+"_Clinician", "", new { @id = Model.Type+"_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="<%= Model.Type %>_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="date" name="<%= Model.Type %>_SignatureDate" value="<%= date.ToShortDateString() %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
           </tr>
    </tbody>
    </table>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li>
                <a href="javascript:void(0);" onclick="STVisitRemove(); $('#<%= Model.Type %>_Button').val($(this).html()).closest('form').submit();">Save</a>
            </li><li>
                <a href="javascript:void(0);" onclick="STVisitSubmit();">Complete</a>
            </li><li>
                <a href="javascript:void(0);" onclick="STVisitRemove(); UserInterface.CloseWindow('stVisit');">Exit</a>
            </li>
        </ul>
    </div>
</div><%
} %>
<script type="text/javascript">
    $("#OTEvaluation_MR").attr('readonly', true);
    function STVisitAdd() {
        $("#<%= Model.Type %>_TimeIn").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_TimeOut").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_Clinician").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required').addClass('required');
    }
    function STVisitRemove() {
        $("#<%= Model.Type %>_TimeIn").removeClass('required');
        $("#<%= Model.Type %>_TimeOut").removeClass('required');
        $("#<%= Model.Type %>_Clinician").removeClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required');
    }
    function STVisitSubmit() {
        STVisitAdd();
        $('#<%= Model.Type %>_Button').val("Complete").closest('form').submit();
    }
    $("#<%= Model.Type %>_PreviousNotes").change(function() {
        $("#stVisitContentId").load("/Schedule/STVisitContent", { patientId: $("#<%= Model.Type %>_PatientId").val(), noteId: $("#<%= Model.Type %>_PreviousNotes").val() }, function(responseText, textStatus, XMLHttpRequest) { })
    });
</script>