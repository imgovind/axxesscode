﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewEpisodeData>" %>
<% using (Html.BeginForm("AddEpisode", "Schedule", FormMethod.Post, new { @id = "newEpisodeForm" })){%>
<div class="wrapper main">
<span class="bigtext align-center">New Episode<%= Model.PatientId != Guid.Empty ? ": " + Model.DisplayName : "" %></span>
    <fieldset>
        <legend>Patient</legend>
        <div class="column"><div class="row"><label for="New_Episode_PatientId" class="float-left">Patient:</label><div class="float-right"><%= Html.Hidden("PatientId", Model.PatientId.ToString(), new { @id = "New_Episode_PatientId"})%><%=Model.DisplayName %></div></div></div>
        <div class="column"><div class="row"><label for="New_Episode_AdmissionId" class="float-left">Start Of Care Date:</label><div class="float-right"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @class = "New_Episode_AdmissionId" })%> </div></div></div>
    </fieldset>
    <span id="newEpisodeTip"><% if (Model != null && Model.EndDate != DateTime.MinValue){  %><label class="bold" >Tip:</label><em> Last Episode end date is :- <%= Model.EndDateFormatted %></em><% } %></span>
    <fieldset>
        <legend>Details</legend>
        <div class="column">
            <div class="row"><label for="New_Episode_TargetDate" class="float-left">Episode Start Date:</label><div class="float-right"><input type="date" name="StartDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(1).ToShortDateString() : DateTime.Today.ToShortDateString() %>" onchange="Schedule.newEpisodeStartDateOnChange()" id="New_Episode_StartDate" class="required" /></div></div>
            <div class="row"><label for="New_Episode_EndDate" class="float-left">Episode End Date:</label><div class="float-right"><input type="date" name="EndDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(60).ToShortDateString() : DateTime.Today.AddDays(59).ToShortDateString() %>" id="New_Episode_EndDate" class="required" /></div></div>
            <div class="row"><label for="New_Episode_CaseManager" class="float-left">Case Manager:</label><div class="float-right"><%= Html.LookupSelectList(SelectListTypes.Users, "Detail.CaseManager", Model.CaseManager.IsNotNullOrEmpty() ? Model.CaseManager : Guid.Empty.ToString(), new { @id = "New_Episode_CaseManager", @class = "Users required valid" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Episode_PrimaryPhysician" class="float-left">Primary Physician:</label><div class="float-right"><%= Html.TextBox("Detail.PrimaryPhysician", Model.PrimaryPhysician, new { @id = "New_Episode_PrimaryPhysician", @class = "physicians" })%></div></div>
            <div class="row"><label for="New_Episode_PrimaryInsurance" class="float-left">Primary Insurance:</label><div class="float-right"><%= Html.Insurances("Detail.PrimaryInsurance", Model.PrimaryInsurance, false, new { @id = "New_Episode_PrimaryInsurance", @class = "Insurances" })%></div></div>
            <div class="row"><label for="New_Episode_SecondaryInsurance" class="float-left">Secondary Insurance:</label><div class="float-right"><%= Html.Insurances("Detail.SecondaryInsurance", Model.SecondaryInsurance, false, new { @id = "New_Episode_SecondaryInsurance", @class = "Insurances" })%></div></div>
        </div>
    </fieldset>
    <fieldset><legend>Comments <em><span class="img icon note-blue"></span> (Blue Sticky Note)</em></legend><div class="wide-column"><div class="row"><textarea id="New_Episode_Comments" name="Detail.Comments" rows="10"  cols=""></textarea></div></div></fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="close">Cancel</a></li>
        </ul>
    </div>
</div>
<%  } %>