﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">Schedule Deviation | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
    <div class="float-right">
        <div class="buttons">
            <ul>
                <li><%= Html.ActionLink("Excel Export", "ExportScheduleDeviation", "Report", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "AgencyScheduleDeviation_ExportLink", @class = "excel" })%></li>
            </ul>
        </div>
    </div>
    <fieldset class="grid-controls">
        <label class="float-left" for="AgencyScheduleDeviation_BranchCode">Branch:</label>
        <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "AgencyScheduleDeviation_BranchCode", "", new { @id = "AgencyScheduleDeviation_BranchCode", @class = "AddressBranchCode report_input valid" })%></div>
        <label class="strong" for="AgencyScheduleDeviation_StartDate">Date Range:</label>
        <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="AgencyScheduleDeviation_StartDate" class="short" />
        <label class="strong" for="AgencyScheduleDeviation_EndDate">To</label>
        <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="AgencyScheduleDeviation_EndDate" class="short" />
        <div class="buttons fr">
            <ul>
                <li><a href="javascript:void(0);" onclick="RebindAgencyScheduleDeviation();">Generate</a></li>
            </ul>
        </div>
    </fieldset>
    <%= Html.Telerik().Grid<ScheduleEvent>().Name("AgencyScheduleDeviationGrid").HtmlAttributes(new { @class = "bottom-bar" }).Columns(columns => {
            columns.Bound(m => m.DisciplineTaskName).Title("Task");
            columns.Bound(m => m.PatientName).Title("Patient Name");
            columns.Bound(m => m.PatientIdNumber).Title("MR#").Width(70);
            columns.Bound(m => m.StatusName).Title("Status");
            columns.Bound(p => p.UserName).Title("Employee").Width(155);
            columns.Bound(m => m.EventDate).Title("Schedule Date").Format("{0:MM/dd/yyyy}").Width(90);
            columns.Bound(m => m.VisitDate).Title("Visit Date").Format("{0:MM/dd/yyyy}").Width(80);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("ScheduleDeviation", "Report", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Selectable().Scrollable() %>
</div>
<script type="text/javascript">
    $('#AgencyScheduleDeviationGrid .t-grid-content').css({ 'height': 'auto' });
    $('#AgencyScheduleDeviationGrid').css({ 'top': '90px', 'bottom': '' });
    function RebindAgencyScheduleDeviation() {
        var grid = $('#AgencyScheduleDeviationGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AgencyScheduleDeviation_BranchCode").val(), StartDate: $('#AgencyScheduleDeviation_StartDate').val(), EndDate: $('#AgencyScheduleDeviation_EndDate').val() }); }
        var $exportLink = $('#AgencyScheduleDeviation_ExportLink');
        if ($exportLink != null) {
            var href = $exportLink.attr('href');
            if (href != null) {
                href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyScheduleDeviation_BranchCode").val());
                href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#AgencyScheduleDeviation_StartDate").val());
                href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#AgencyScheduleDeviation_EndDate").val());
                $exportLink.attr('href', href);
            }
        }
    }
</script>