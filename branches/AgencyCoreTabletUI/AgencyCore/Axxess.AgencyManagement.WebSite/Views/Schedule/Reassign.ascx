﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ReassignViewData>" %>
<% using (Html.BeginForm("Reassign"+Model.Type+"Schedules" , "Schedule", FormMethod.Post, new { @id = "reassign"+Model.Type+"Form" })){ %>
<% if (!Model.Type.IsEqual("All")){ %><%= Html.Hidden("PatientId", Model.PatientId, new { @id = Model.Type + "_PatientId" })%><%} %>
<div class="wrapper main">
    <fieldset>
    <legend>Reassign Tasks</legend>
        <div class="wide-column">
           <% if (!Model.Type.IsEqual("All")){ %><div class="row"><label class="float-left">Patient Name:</label><div class="float-right"><label><%= Model.PatientDisplayName%></label> </div> </div><%} %>
           <div class="row"><label  class="float-left">Employee From:</label><div class="float-right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "EmployeeOldId", "", Guid.Empty, 0, new { @id = Model.Type + "_EmployeeOldId", @class = "required notzero valid" })%></div></div>
           <div class="row"><label  class="float-left">Employee To:</label><div class="float-right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "EmployeeId", "", Guid.Empty, 1, new { @id = Model.Type + "_EmployeeId", @class = "required notzero valid" })%></div></div>
           <div class="row"><span>Note:<em>&nbsp;Only tasks that are not started and not yet due will be reassigned.</em></span></div>
        </div>
        <div class="clear"></div>
    </fieldset>
    <div class="buttons"><ul><li><a class="save">Reassign</a></li><li><a class="close">Exit</a></li></ul></div>
</div>
<%} %>
