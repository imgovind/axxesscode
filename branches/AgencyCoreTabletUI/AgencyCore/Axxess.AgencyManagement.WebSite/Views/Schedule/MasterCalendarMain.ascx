﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<span class="wintitle">Master Calendar | <%= Model.DisplayName.IsNotNullOrEmpty() ? Model.DisplayName.ToTitleCase().Clean() : "" %></span>
<div id="masterCalendarResult"><% Html.RenderPartial("~/Views/Schedule/MasterCalendar.ascx", Model); %></div>



