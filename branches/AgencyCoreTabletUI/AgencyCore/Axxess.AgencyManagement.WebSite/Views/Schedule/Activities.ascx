﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleActivityArgument>" %>
<% Html.Telerik().Grid<ScheduleEvent>().Name("ScheduleActivityGrid").Columns(columns => {
       columns.Bound(s => s.EventId).Visible(false);
       columns.Bound(s => s.Url).ClientTemplate("<#=Url#>").Title("Task");
       columns.Bound(s => s.EventDateSortable).Title("Scheduled Date").Width(105);
       columns.Bound(s => s.UserName).Title("Assigned To").Width(180);
       columns.Bound(s => s.StatusName).Title("Status");
       columns.Bound(s => s.OasisProfileUrl).Title(" ").ClientTemplate("<#=OasisProfileUrl#>").Width(30);
       columns.Bound(s => s.StatusComment).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip red-note\" tooltip=\"<#=StatusComment#>\"></a>");
       columns.Bound(s => s.Comments).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip\" tooltip=\"<#=Comments#>\"></a>");
       columns.Bound(s => s.EpisodeNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip blue-note\" tooltip=\"<#=EpisodeNotes#>\"></a>");
       columns.Bound(s => s.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(30);
       columns.Bound(s => s.AttachmentUrl).Title(" ").ClientTemplate("<#=AttachmentUrl#>").Width(30);
       columns.Bound(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Title("Action").Width(200);
       columns.Bound(s => s.IsComplete).Visible(false);
   }).ClientEvents(c => c.OnRowDataBound("Schedule.activityRowDataBound")).DataBinding(dataBinding => dataBinding.Ajax().Select("Activity", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, discipline = Model.Discpline })).Sortable().Scrollable().Footer(false).Render();
%>
<script type="text/javascript">
    $('#ScheduleActivityGrid .t-grid-content').css({ 'height': 'auto' });
    if ($('#schedulecenter_showall').length) $('#schedulecenter_showall').remove();
    Schedule.SetEpisodeId('<%=Model.EpisodeId%>');
    Schedule.SetId('<%=Model.PatientId%>');
    $('#ScheduleTabStrip').prepend(unescape("%3Cdiv id=%22schedulecenter_showall%22 class=%22abs buttons%22 style=%22margin-top:-.3em;left:550px;line-height:25px;%22%3E%3Cul%3E%3Cli%3E%3Ca onclick=%22Schedule.showAll();return false%22%3EShow all%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
</script>