﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CalendarViewData>" %>
<%  if (Model != null) { %>
    <%  if (Model.Episode != null) { %>
        <%  var scheduleEvents = Model.Episode.Schedule.IsNotNullOrEmpty() ? Model.Episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.ToDateTime() >= Model.Episode.StartDate && s.EventDate.ToDateTime() <= Model.Episode.EndDate && s.DisciplineTask > 0 && s.EventId!=Guid.Empty).OrderBy(o => o.EventDate.ToZeroFilled().ToDateTime()).ToList() : new List<ScheduleEvent>(); %>
        <%  DateTime[] startdate = new DateTime[3], enddate = new DateTime[3], currentdate = new DateTime[3]; %>
        <%  startdate[0] = DateUtilities.GetStartOfMonth(Model.Episode.StartDate.Month, Model.Episode.StartDate.Year); %>
        <%  enddate[0] = DateUtilities.GetEndOfMonth(Model.Episode.StartDate.Month, Model.Episode.StartDate.Year); %>
        <%  currentdate[0] = startdate[0].AddDays(-(int)startdate[0].DayOfWeek); %>
        <%  startdate[1] = enddate[0].AddDays(1); %>
        <%  enddate[1] = DateUtilities.GetEndOfMonth(startdate[1].Month, startdate[1].Year); %>
        <%  currentdate[1] = startdate[1].AddDays(-(int)startdate[1].DayOfWeek); %>
        <%  startdate[2] = enddate[1].AddDays(1); %>
        <%  enddate[2] = DateUtilities.GetEndOfMonth(startdate[2].Month, startdate[2].Year); %>
        <%  currentdate[2] = startdate[2].AddDays(-(int)startdate[2].DayOfWeek); %>
        <%= Html.Hidden("SchedulePatientID", Model.Episode.PatientId, new { @id = "SchedulePatientID" })%>
        <%= Html.Hidden("ScheduleEpisodeID", Model.Episode.Id, new { @id = "ScheduleEpisodeID" })%>
<div class="ac">
        <%  for (int c = 0; c < 3; c++) { %>
    <div class="cal">
        <table>
            <thead>
                <tr>
                    <td colspan="7" class="caltitle"><%= string.Format("{0:MMMM} {0:yyyy}", startdate[c])%></td>
                </tr>
                <tr>
                    <th>Su</th>
                    <th>Mo</th>
                    <th>Tu</th>
                    <th>We</th>
                    <th>Th</th>
                    <th>Fr</th>
                    <th>Sa</th>
                </tr>
            </thead>
            <tbody>
            <%  var maxWeek=DateUtilities.Weeks(startdate[c].Month, startdate[c].Year); %>
            <%  for (int i = 0; i <= maxWeek; i++) { %>
                   <tr>
                <%  string tooltip = ""; %>
                <%  int addedDate = (i) * 7; %>
                <%  for (int j = 0; j <= 6; j++) { %>
                    <%  var specificDate = currentdate[c].AddDays(j + addedDate); %>
                    <%  if (specificDate < Model.Episode.StartDate || specificDate < startdate[c] || specificDate > enddate[c] || specificDate.Date > Model.Episode.EndDate.Date) { %>
                        <td class="inactive"></td>
                    <%  } else { %>
                        <%  var events = scheduleEvents.FindAll(e => e.EventDate.ToZeroFilled() == specificDate.ToShortDateString().ToZeroFilled()); %>
                        <%  var count = events.Count; %>
                        <%  if (count > 1) { %>
                            <%  var allevents = "<br />"; %>
                            <%  events.ForEach(e => { allevents += string.Format("{0} - <em>{1}</em><br />", e.DisciplineTaskName, UserEngine.GetName(e.UserId, Current.AgencyId)); }); %>
                        <td class="multi" date="<%= specificDate %>">
                            <%  tooltip = specificDate.ToShortDateString() + allevents; %>
                        <%  } else if (count == 1) { %>
                            <%  var evnt = events.First(); %>
                            <%  var missed = (evnt.IsMissedVisit) ? "missed" : string.Empty; %>
                            <%  var status = evnt.Status != null ? int.Parse(evnt.Status) : 1000; %>
                        <td class="status<%= status %> scheduled <%= missed %>" date="<%= specificDate %>">
                            <%  tooltip = specificDate.ToShortDateString() + string.Format("<br />{0} - <em>{1}</em>", evnt.DisciplineTaskName, evnt.UserName.IsNotNullOrEmpty() || evnt.UserId.IsEmpty() ? evnt.UserName : UserEngine.GetName(evnt.UserId, Current.AgencyId)); %>
                        <%  } else { %>
                        <td date="<%= specificDate %>">
                            <%  tooltip = ""; %>
                        <%  } %>
                        <%  var dayOnClick = Current.HasRight(Permissions.ScheduleVisits) ? string.Format("Schedule.Add('{0}');", specificDate.ToShortDateString()) : ""; %>
                            <%= string.Format("<div class=\"datelabel\" onclick=\"{0}\"{1}><a>{2}</a></div></td>", dayOnClick, tooltip.IsNotNullOrEmpty() ? " tooltip=\"" + tooltip + "\"" : "", specificDate.Day) %>
                    <%  } %>
                <%  } %>
                </tr>
            <%  } %>
            </tbody>
        </table>
    </div>
    <%  } %>
    <div class="clear"></div>
    <fieldset class="legend">
        <ul>
            <li><div class="scheduled">&#160;</div> Scheduled</li>
            <li><div class="completed">&#160;</div> Completed</li>
            <li><div class="missed">&#160;</div> Missed</li>
            <li><div class="multi">&#160;</div> Multiple</li>
        </ul>
    </fieldset>
</div>
<script type="text/javascript">
    $(".datelabel").each(function() {
        if ($(this).attr("tooltip") != undefined) {
            $(this).tooltip({
                track: true,
                showURL: false,
                top: 10,
                left: 10,
                extraClass: "calday",
                bodyHandler: function() { return $(this).attr("tooltip") }
            })
        }
    });
    $("#Calendar_EpisodeList").change(function() {
        Schedule.RefreshCurrentEpisode("<%= Model.Episode.PatientId %>", $(this).val())
    });
</script>
    <%  } else { %>
        <%  if (!Model.IsDischarged) { %>
<script type="text/javascript">
    $("#ScheduleMainResult").html(
        U.MessageWarn("No Episodes", "There is currently no episodes created for this patient.").after(
            $("<div/>").addClass("error").Buttons([
                { Text: "Add New Episode", Click: function() { UserInterface.ShowNewEpisodeModal("<%= Model.PatientId %>") } }
            ])
        )
    )
</script>
        <%  } else { %>
<script type="text/javascript">
    $("#ScheduleMainResult").empty()
</script>
        <%  } %>
    <%  } %>
<%  } %>