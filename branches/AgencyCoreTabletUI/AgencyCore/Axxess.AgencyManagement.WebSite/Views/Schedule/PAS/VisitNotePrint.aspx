﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Home Health Aide Progress Note<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body></body><%
string[] isVitalSignParameter = data != null && data.ContainsKey("IsVitalSignParameter") && data["IsVitalSignParameter"].Answer != "" ? data["IsVitalSignParameter"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
    .Add("jquery-1.7.1.min.js")
    .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
    .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
).OnDocumentReady(() => {  %>
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3EPersonal Assistance Services%3Cbr /%3EProgress Note%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPAS Frequency:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("PASFrequency") && data["PASFrequency"].Answer.IsNotNullOrEmpty() ? data["PASFrequency"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.StartDate.IsValid() && Model.EndDate.IsValid()? string.Format(" {0} &#8211; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETertiary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("PrimaryDiagnosis2") ? data["PrimaryDiagnosis2"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3EPersonal Assistance Services%3Cbr /%3EProgress Note%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "";
    printview.addsection(<%
        if (isVitalSignParameter != null && isVitalSignParameter.Contains("1")) { %>
            printview.checkbox("N/A",true),<%
        } else { %>
            "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth%3E%3C/th%3E%3Cth%3ESBP%3C/th%3E%3Cth%3EDBP%3C/th%3E%3Cth%3EHR%3C/th%3E%3Cth%3EResp%3C/th%3E%3Cth%3ETemp%3C/th%3E%3Cth%3EWeight%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3Egreater than (&#62;)%3C/th%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("SystolicBPGreaterThan") ? data["SystolicBPGreaterThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("DiastolicBPGreaterThan") ? data["DiastolicBPGreaterThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("PulseGreaterThan") ? data["PulseGreaterThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("RespirationGreaterThan") ? data["RespirationGreaterThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("TempGreaterThan") ? data["TempGreaterThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("WeightGreaterThan") ? data["WeightGreaterThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth%3Eor less than (&#60;)%3C/th%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("SystolicBPLessThan") ? data["SystolicBPLessThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("DiastolicBPLessThan") ? data["DiastolicBPLessThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("PulseLessThan") ? data["PulseLessThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("RespirationLessThan") ? data["RespirationLessThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("TempLessThan") ? data["TempLessThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3Ctd%3E" +
            printview.span("<%= data != null && data.ContainsKey("WeightLessThan") ? data["WeightLessThan"].Answer.Clean() : string.Empty %>",0,1) +
            "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E",<%
        } %>
        "Vital Sign Parameters");
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth%3EBP%3C/th%3E%3Cth%3EHR%3C/th%3E%3Cth%3ETemp%3C/th%3E%3Cth%3EResp%3C/th%3E%3Cth%3EWeight%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
        printview.span("<%= data.ContainsKey("VitalSignBPVal") ? data["VitalSignBPVal"].Answer.Clean() : string.Empty %>",0,1) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.ContainsKey("VitalSignHRVal") ? data["VitalSignHRVal"].Answer.Clean() : string.Empty %>",0,1) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.ContainsKey("VitalSignTempVal") ? data["VitalSignTempVal"].Answer.Clean() : string.Empty %>",0,1) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.ContainsKey("VitalSignRespVal") ? data["VitalSignRespVal"].Answer.Clean() : string.Empty %>",0,1) +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data.ContainsKey("VitalSignWeightVal") ? data["VitalSignWeightVal"].Answer.Clean() : string.Empty %>",0,1) +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E",
        "Vital Signs");
    printview.addsection(
        "%3Ctable class=%22align-center%22%3E%3Ctbody%3E%3Ctr%3E%3Cth colspan=%223%22%3EAssignment%3C/th%3E%3Cth colspan=%223%22%3EStatus%3C/th%3E%3Cth colspan=%223%22%3EAssignment%3C/th%3E%3Cth%3EStatus%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth colspan=%223%22%3EPersonal Care%3C/th%3E%3Cth%3ECompleted%3C/th%3E%3Cth%3ERefuse%3C/th%3E%3Cth%3EN/A%3C/th%3E%3Cth colspan=%223%22%3EElimination%3C/th%3E%3Cth%3ECompleted%3C/th%3E%3Cth%3ERefuse%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EBed Bath%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EAssist with Bed Pan/Urinal%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EAssist with Chair Bath%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EAssist with BSC%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3ETub Bath%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EIncontinence Care%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EShower%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EEmpty Drainage Bag%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EShower w/Chair%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3ERecord Bowel Movement%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EShampoo Hair%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3ECatheter Care%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EHair Care/Comb Hair%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Cth colspan=%223%22%3EActivity%3C/th%3E%3Cth%3ECompleted%3C/th%3E%3Cth%3ERefuse%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EOral Care%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EDangle on Side of Bed%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3ESkin Care%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3ETurn &#38; Position%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EPericare%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EAssist with Transfer%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3ENail Care%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EAssist with Ambulation%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EShave%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3ERange of Motion%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EAssist with Dressing%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Cth colspan=%223%22%3EHousehold Task%3C/th%3E%3Cth%3ECompleted%3C/th%3E%3Cth%3ERefuse%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth colspan=%223%22%3ENutrition%3C/th%3E%3Cth%3ECompleted%3C/th%3E%3Cth%3ERefuse%3C/th%3E%3Cth%3EN/A%3C/th%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EMake Bed%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EMeal Set-up%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EChange Linen%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%223%22%3EAssist with Feeding%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd class=%22align-left%22 colspan=%223%22%3ELight Housekeeping%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "2" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "1" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3Ctd%3E" +
        "<%= data != null && data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "0" ? "&#x2713;" : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align-left%22 colspan=%2212%22%3EOther (Describe):" +
        printview.span("<%= data != null && data.ContainsKey("HouseholdTaskOther") ? data["HouseholdTaskOther"].Answer : "" %>",0,10) +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
    printview.addsection(
        printview.span("<%= data != null && data.ContainsKey("Comment") && data["Comment"].Answer.IsNotNullOrEmpty() ? data["Comment"].Answer.Clean() : "" %>",0,10),
        "Comments");
    printview.addsection(
        printview.col(2,
            printview.span("Clinician Signature:",true) +
            printview.span("Date:",true) +
            printview.span("<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : string.Empty %>",0,1) +
            printview.span("<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : string.Empty %>",0,1))); <%
}).Render(); %>
</html>