﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Personal Assistance Services Progress Note | <%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase().Clean() : "" %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "PASVisitForm" })) {%>
<%= Html.Hidden("PASVisit_PatientId", Model.PatientId)%>
<%= Html.Hidden("PASVisit_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("PASVisit_EventId", Model.EventId)%>
<%= Html.Hidden("Type", "PASVisit")%>
<%= Html.Hidden("DisciplineTask", "99")%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="2">Personal Assistance Services Progress Note</th></tr>
            <tr><td colspan="2" class="bigtext"><%= Model.Patient.DisplayName %></td></tr>
            <tr>
                <td>
                    <div><label for="PASVisit_MR" class="float-left">MR#:</label><div class="float-right"><%= Html.TextBox("PASVisit_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty, new { @id = "PASVisit_MR", @readonly = "readonly" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="PASVisit_EpsPeriod" class="float-left">Episode/Period:</label><div class="float-right"><%= Html.TextBox("PASVisit_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = "PASVisit_EpsPeriod", @readonly = "readonly" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="PASVisit_VisitDate" class="float-left">Visit Date:</label><div class="float-right"><input type="date" name="PASVisit_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="PASVisit_VisitDate" class="required" /></div></div>
                    <div class="clear"></div>
                    <div><label for="PASVisit_TimeIn" class="float-left">Time In:</label><div class="float-right"><%= Html.TextBox("PASVisit_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : string.Empty, new { @id = "PASVisit_TimeIn", @class = "loc" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="PASVisit_TimeOut" class="float-left">Time Out:</label><div class="float-right"><%= Html.TextBox("PASVisit_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : string.Empty, new { @id = "PASVisit_TimeOut", @class = "loc" })%></div></div>
                </td>
                <td>
                    <% if (Current.HasRight(Permissions.ViewPreviousNotes)) { %><div><label for="PASVisit_PreviousNotes" class="float-left">Previous Notes:</label><div class="float-right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = "PASVisit_PreviousNotes" })%></div></div>
                    <div class="clear"></div><% } %>
                    <div><label for="PASVisit_HHAFrequency" class="float-left">HHA Frequency:</label><div class="float-right"><%= Html.TextBox("PASVisit_HHAFrequency", data.ContainsKey("HHAFrequency") ? data["HHAFrequency"].Answer : string.Empty, new { @id = "PASVisit_HHAFrequency", @readonly = "readonly" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="PASVisit_PrimaryDiagnosis" class="float-left">Primary Diagnosis:</label><div class="float-right"><%= Html.TextBox("PASVisit_PrimaryDiagnosis", data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : string.Empty, new { @id = "PASVisit_PrimaryDiagnosis", @readonly = "readonly" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="PASVisit_PrimaryDiagnosis1" class="float-left">Secondary Diagnosis:</label><div class="float-right"><%= Html.TextBox("PASVisit_PrimaryDiagnosis1", data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : string.Empty, new { @id = "PASVisit_PrimaryDiagnosis1", @readonly = "readonly" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="PASVisit_DNR" class="float-left">DNR:</label><div class="float-right"><%= Html.RadioButton("PASVisit_DNR", "1", data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? true : false, new { @id = "PASVisit_DNR1", @class = "radio" })%><label for="PASVisit_DNR1" class="inline-radio">Yes</label><%= Html.RadioButton("PASVisit_DNR", "0", data.ContainsKey("DNR") && data["DNR"].Answer == "0" ? true : false, new { @id = "PASVisit_DNR2", @class = "radio" })%><label for="PASVisit_DNR2" class="inline-radio">No</label></div></div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="pasVisitContentId"><% Html.RenderPartial("~/Views/Schedule/PAS/VisitNoteContent.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>       
            <tr><th colspan="2">Electronic Signature</th></tr>
            <tr>
                <td colspan="2">
                    <div class="third">
                        <label for="PASVisit_ClinicianSignature" class="float-left">Clinician Signature:</label>
                        <div class="float-right"><%= Html.Password("PASVisit_Clinician", "", new { @id = "PASVisit_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="PASVisit_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="date" name="PASVisit_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : string.Empty %>" id="PASVisit_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="PASVisit_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="PASVisitRemove(); pasVisit.Submit($(this));">Save</a></li>
            <li><a href="javascript:void(0);" onclick="PASVisitAdd(); pasVisit.Submit($(this));">Complete</a></li>
            <li><a href="javascript:void(0);" onclick="PASVisitRemove(); UserInterface.CloseWindow('pasVisit');">Exit</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $("#PASVisit_IsVitalSignParameter").click(function() {
    if ($('#PASVisit_IsVitalSignParameter').is(':checked')) $("#window_pasVisit .vitalsigns").each(function() { $(this).hide(); });
        else $("#window_pasVisit .vitalsigns").each(function() { $(this).show(); });
    });
    if ($('#PASVisit_IsVitalSignParameter').is(':checked')) $("#window_pasVisit .vitalsigns").each(function() { $(this).hide(); });
    else $("#window_pasVisit .vitalsigns").each(function() { $(this).show(); });

    $("#PASVisit_IsVitalSigns").click(function() {
    if ($('#PASVisit_IsVitalSigns').is(':checked')) $("#window_pasVisit .vitalsignparameter").each(function() { $(this).hide(); });
    else $("#window_pasVisit .vitalsignparameter").each(function() { $(this).show(); });
    });
    if ($('#PASVisit_IsVitalSigns').is(':checked')) $("#window_pasVisit .vitalsignparameter").each(function() { $(this).hide(); });
    else $("#window_pasVisit .vitalsignparameter").each(function() { $(this).show(); });
    $("#PASVisit_MR").attr('readonly', true);
    $("#PASVisit_EpsPeriod").attr('readonly', true);
    function PASVisitAdd() {
        $("#PASVisit_TimeIn").removeClass('required').addClass('required');
        $("#PASVisit_TimeOut").removeClass('required').addClass('required');
        $("#PASVisit_Clinician").removeClass('required').addClass('required');
        $("#PASVisit_SignatureDate").removeClass('required').addClass('required');
    }
    function PASVisitRemove() {
        $("#PASVisit_TimeIn").removeClass('required');
        $("#PASVisit_TimeOut").removeClass('required');
        $("#PASVisit_Clinician").removeClass('required');
        $("#PASVisit_SignatureDate").removeClass('required');
    }
    $("#PASVisit_PreviousNotes").change(function() {
        $("#pasVisitContentId").Load("/Schedule/PASVisitNoteContent", { patientId: $("#PASVisit_PatientId").val(), noteId: $("#PASVisit_PreviousNotes").val() })
    })
</script>
