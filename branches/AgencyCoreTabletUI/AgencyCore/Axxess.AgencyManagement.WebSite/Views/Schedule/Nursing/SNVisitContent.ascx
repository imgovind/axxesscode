﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<ul>
    <li class="quarter">
        <div class="wrapper">
            <h3>Vital Signs</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>Pain Profile</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>Skin</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>Respiratory</h3>
        </div>
    </li>
</ul>
<ol>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/VitalSigns.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/PainProfile.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Skin.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Respiratory.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
</ol>
<ul>
    <li class="quarter">
        <div class="wrapper">
            <h3>Cardiovascular</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>Neurological</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>Musculoskeletal</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>Gastrointestinal</h3>
        </div>
    </li>
</ul>
<ol>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Cardiovascular.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Neurological.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Musculoskeletal.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Gastrointestinal.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
</ol>
<ul>
    <li class="quarter">
        <div class="wrapper">
            <h3>Nutrition</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>Genitourinary</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>
                Diabetic Care &#8212;
                <%  string[] isDiabeticCareApplied = data.AnswerArray("GenericIsDiabeticCareApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsDiabeticCareApplied", string.Empty, new { @id = Model.Type + "_GenericIsDiabeticCareAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsDiabeticCareApplied1' name='{0}_GenericIsDiabeticCareApplied' value='1' type='checkbox' {1} />", Model.Type, isDiabeticCareApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsDiabeticCareApplied1">N/A</label>
            </h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>
                IV &#8212;
                <%  string[] isIVApplied = data.AnswerArray("GenericIsIVApplied"); %>
                <%= Html.Hidden(Model.Type + "isIVApplied", string.Empty, new { @id = Model.Type + "_GenericIsIVAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsIVApplied1' name='{0}_GenericIsIVApplied' value='1' type='checkbox' {1} />", Model.Type, isIVApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>isIVApplied1">N/A</label>
            </h3>
        </div>
    </li>
</ul>
<ol>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Nutrition.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Genitourinary.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/DiabeticCare.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/IV.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
</ol>
<ul>
    <li class="quarter">
        <div class="wrapper">
            <h3>Infection Control</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>Care Coordination</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>Care Plan</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>Discharge Planning</h3>
        </div>
    </li>
</ul>
<ol>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/InfectionControl.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/CareCoordination.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/CarePlan.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/DischargePlanning.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
</ol>
<ul>
    <li>
        <div class="wrapper">
            <h3>Interventions</h3>
        </div>
    </li>
</ul>
<ol>
    <li>
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Interventions.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
</ol>
<ul>
    <li>
        <div class="wrapper">
            <h3>Narrative &#38; Teaching</h3>
        </div>
    </li>
</ul>
<ol>
    <li>
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Narrative.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
</ol>
<ul>
    <li class="quarter">
        <div class="wrapper">
            <h3>Response to Teaching/Interventions</h3>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <h3>Homebound Status</h3>
        </div>
    </li>
    <li class="half">
        <div class="wrapper">
            <h3>
                Phlebotomy &#8212;
                <%  string[] isPhlebotomyApplied = data.AnswerArray("GenericIsPhlebotomyApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsPhlebotomyApplied", string.Empty, new { @id = Model.Type + "_GenericIsPhlebotomyAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsPhlebotomyApplied1' name='{0}_GenericIsPhlebotomyApplied' value='1' type='checkbox' {1} />", Model.Type, isPhlebotomyApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsPhlebotomyApplied1">N/A</label>
            </h3>
        </div>
    </li>
</ul>
<ol>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Response.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="quarter">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/HomeBoundStatus.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
    <li class="half">
        <div class="wrapper">
            <div class="wide-column"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Phlebotomy.ascx", Model); %></div>
            <div class="clear"></div>
        </div>
    </li>
</ol>