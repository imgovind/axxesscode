﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericDigestive = data.AnswerArray("GenericDigestive"); %>
<%= Html.Hidden(Model.Type + "_GenericDigestive", string.Empty, new { @id = Model.Type + "_GenericDigestiveHidden" })%>
<div class="row">
    <div class="wide checkgroup">
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive1' name='{0}_GenericDigestive' value='1' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive1">WNL (Within Normal Limits)</label>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive2' name='{0}_GenericDigestive' value='2' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("2").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive2">Bowel Sounds:</label>
            <div class="extra">
                <%  var bowelSounds = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "0" },
                    new SelectListItem { Text = "Present/WNL x4 quadrants", Value = "1" },
                    new SelectListItem { Text = "Hyperactive", Value = "2" },
                    new SelectListItem { Text = "Hypoactive", Value = "3" },
                    new SelectListItem { Text = "Absent", Value = "4" }
                }, "Value", "Text", data.AnswerOrDefault("GenericDigestiveBowelSoundsType", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericDigestiveBowelSoundsType", bowelSounds, new { @id = Model.Type + "_GenericDigestiveBowelSoundsType", @class = "oe" }) %>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive3' name='{0}_GenericDigestive' value='3' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("3").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive3">Abdominal Palpation:</label>
            <div class="extra">
                <%  var abdominalPalpation = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "0" },
                    new SelectListItem { Text = "Soft/WNL", Value = "1" },
                    new SelectListItem { Text = "Firm", Value = "2" },
                    new SelectListItem { Text = "Tender", Value = "3" },
                    new SelectListItem { Text = "Other", Value = "4" }
                }, "Value", "Text", data.AnswerOrDefault("GenericAbdominalPalpation", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericAbdominalPalpation", abdominalPalpation, new { @id = Model.Type + "_GenericAbdominalPalpation", @class = "oe" }) %>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive4' name='{0}_GenericDigestive' value='4' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("4").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive4">Bowel Incontinence</label>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive5' name='{0}_GenericDigestive' value='5' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("5").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive5">Nausea</label>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive6' name='{0}_GenericDigestive' value='6' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("6").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive6">Vomiting</label>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive7' name='{0}_GenericDigestive' value='7' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("7").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive7">GERD</label>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive8' name='{0}_GenericDigestive' value='8' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("8").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive8">Abd Girth:</label>
            <div class="extra">
                <%= Html.TextBox(Model.Type + "_GenericDigestiveAbdGirthLength", data.AnswerOrEmptyString("GenericDigestiveAbdGirthLength"), new { @id = Model.Type + "_GenericDigestiveAbdGirthLength", @class = "oe", @maxlength = "5" }) %>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="strong">Elimination:</div>
    <div class="wide checkgroup">
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestive11' name='{0}_GenericDigestive' value='11' type='checkbox' {1} />", Model.Type, genericDigestive.Contains("11").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestive11">Last BM:</label>
            <div class="extra">
                <label class="float-left" for="<%= Model.Type %>_GenericDigestiveLastBMDate">Date:</label>
                <div class="fr"><input type="date" name ="<%= Model.Type %>_GenericDigestiveLastBMDate" value="<%= data.AnswerOrEmptyString("GenericDigestiveLastBMDate") %>" id="<%= Model.Type %>_GenericDigestiveLastBMDate" class="short" /></div>
                <%  string[] genericDigestiveLastBM = data.AnswerArray("GenericDigestiveLastBM"); %>
                <%= Html.Hidden(Model.Type + "_GenericDigestiveLastBM", string.Empty, new { @id = Model.Type + "_GenericDigestiveLastBMHidden" })%>
                <div class="clear"></div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericDigestiveLastBM1' name='{0}_GenericDigestiveLastBM' value='1' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBM.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDigestiveLastBM1">WNL (Within Normal Limits)</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericDigestiveLastBM2' name='{0}_GenericDigestiveLastBM' value='2' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBM.Contains("2").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDigestiveLastBM2">Abnormal Stool:</label>
                </div>
                <div id="<%= Model.Type %>_GenericDigestiveLastBM2More">
                    <%  string[] genericDigestiveLastBMAbnormalStool = data.AnswerArray("GenericDigestiveLastBMAbnormalStool"); %>
                    <%= Html.Hidden(Model.Type + "_GenericDigestiveLastBMAbnormalStool", string.Empty, new { @id = Model.Type + "_GenericDigestiveLastBMAbnormalStoolHidden" })%>
                    <div class="fl">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBMAbnormalStool1' name='{0}_GenericDigestiveLastBMAbnormalStool' value='1' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBMAbnormalStool.Contains("1").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBMAbnormalStool1">Gray</label>
                    </div>
                    <div class="fl">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBMAbnormalStool2' name='{0}_GenericDigestiveLastBMAbnormalStool' value='2' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBMAbnormalStool.Contains("2").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBMAbnormalStool2">Tarry</label>
                    </div>
                    <div class="fl">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBMAbnormalStool4' name='{0}_GenericDigestiveLastBMAbnormalStool' value='4' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBMAbnormalStool.Contains("4").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBMAbnormalStool4">Black</label>
                    </div>
                    <div class="fl">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBMAbnormalStool3' name='{0}_GenericDigestiveLastBMAbnormalStool' value='3' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBMAbnormalStool.Contains("3").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBMAbnormalStool3">Fresh Blood</label>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericDigestiveLastBM3' name='{0}_GenericDigestiveLastBM' value='3' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBM.Contains("3").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDigestiveLastBM3">Constipation:</label>
                </div>
                <div id="<%= Model.Type %>_GenericDigestiveLastBM3More">
                    <div class="fl">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBMConstipationTypeChronic' name='{0}_GenericDigestiveLastBMConstipationType' value='Chronic' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Chronic").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBMConstipationTypeChronic">Chronic</label>
                    </div>
                    <div class="fl">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBMConstipationTypeAcute' name='{0}_GenericDigestiveLastBMConstipationType' value='Acute' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Acute").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBMConstipationTypeAcute">Acute</label>
                    </div>
                    <div class="fl">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBMConstipationTypeOccasional' name='{0}_GenericDigestiveLastBMConstipationType' value='Occasional' type='checkbox' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMConstipationType").Equals("Occasional").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBMConstipationTypeOccasional">Occasional</label>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericDigestiveLastBM4' name='{0}_GenericDigestiveLastBM' value='4' type='checkbox' {1} />", Model.Type,  genericDigestiveLastBM.Contains("4").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericDigestiveLastBM4">Diarrhea:</label>
                </div>
                <div id="<%= Model.Type %>_GenericDigestiveLastBM4More">
                    <div class="fl">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBMDiarrheaTypeChronic' name='{0}_GenericDigestiveLastBMDiarrheaType' value='Chronic' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Chronic").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBMDiarrheaTypeChronic" class="inline-radio">Chronic</label>
                    </div>
                    <div class="fl">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBMDiarrheaTypeAcute' name='{0}_GenericDigestiveLastBMDiarrheaType' value='Acute' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Acute").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBMDiarrheaTypeAcute" class="inline-radio">Acute</label>
                    </div>
                    <div class="fl">
                        <%= string.Format("<input id='{0}_GenericDigestiveLastBMDiarrheaTypeOccasional' name='{0}_GenericDigestiveLastBMDiarrheaType' value='Occasional' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericDigestiveLastBMDiarrheaType").Equals("Occasional").ToChecked()) %>
                        <label for="<%= Model.Type %>_GenericDigestiveLastBMDiarrheaTypeOccasional" class="inline-radio">Occasional</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="strong">Ostomy:</div>
    <%  string[] genericDigestiveOstomy = data.AnswerArray("GenericDigestiveOstomy"); %>
    <%= Html.Hidden(Model.Type + "_GenericDigestiveOstomy", string.Empty, new { @id = Model.Type + "_GenericDigestiveOstomyHidden" })%>
    <div class="wide checkgroup">
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestiveOstomy1' name='{0}_GenericDigestiveOstomy' value='1' type='checkbox' {1} />", Model.Type, genericDigestiveOstomy.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestiveOstomy1">Ostomy Type:</label>
            <div class="extra">
                <%  var ostomy = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "0" },
                    new SelectListItem { Text = "N/A", Value = "1" },
                    new SelectListItem { Text = "Ileostomy ", Value = "2" },
                    new SelectListItem { Text = "Colostomy", Value = "3" },
                    new SelectListItem { Text = "Other", Value = "4" }
                }, "Value", "Text", data.AnswerOrDefault("GenericDigestiveOstomyType", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericDigestiveOstomyType", ostomy, new { @id = Model.Type + "_GenericDigestiveOstomyType", @class = "oe" }) %>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestiveOstomy2' name='{0}_GenericDigestiveOstomy' value='2' type='checkbox' {1} />", Model.Type, genericDigestiveOstomy.Contains("2").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestiveOstomy2">Stoma Appearance:</label>
            <div class="extra">
                <%= Html.TextBox(Model.Type + "_GenericDigestiveStomaAppearance", data.AnswerOrEmptyString("GenericDigestiveStomaAppearance"), new { @id = Model.Type + "_GenericDigestiveStomaAppearance", @class = "oe" }) %>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericDigestiveOstomy3' name='{0}_GenericDigestiveOstomy' value='3' type='checkbox' {1} />", Model.Type, genericDigestiveOstomy.Contains("3").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericDigestiveOstomy3">Surrounding Skin:</label>
            <div class="extra">
                <%= Html.TextBox(Model.Type + "_GenericDigestiveSurSkinType", data.AnswerOrEmptyString("GenericDigestiveSurSkinType"), new { @id = Model.Type + "_GenericDigestiveSurSkinType", @class = "oe" }) %>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <label for="<%= Model.Type %>_GenericGastrointestinalComment" class="strong">Comments:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericGastrointestinalComment", data.AnswerOrEmptyString("GenericGastrointestinalComment"), new { @class = "fill", @id = Model.Type + "_GenericGastrointestinalComment" })%></div>
</div>