﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] homeBoundReason = data.AnswerArray("HomeBoundReason"); %>
<%= Html.Hidden(Model.Type + "_HomeBoundReason", string.Empty, new { @id = Model.Type + "_HomeBoundReasonHidden" })%>
<div class="align-left">
    <label class="strong">Homebound Status</label>
</div>
<div class="align-left">
    <%= string.Format("<input id='{0}_HomeBoundReason2' class='float-left radio' name='{0}_HomeBoundReason' value='2' type='checkbox' {1} />", Model.Type, homeBoundReason.Contains("2").ToChecked()) %>
    <label for="<%= Model.Type %>_HomeBoundReason2" class="radio">Exhibits considerable &#38; taxing effort to leave home</label>
</div><div class="align-left">
    <%= string.Format("<input id='{0}_HomeBoundReason3' class='float-left radio' name='{0}_HomeBoundReason' value='3' type='checkbox' {1} />", Model.Type, homeBoundReason.Contains("3").ToChecked()) %>
    <label for="<%= Model.Type %>_HomeBoundReason3" class="radio">Requires the assistance of another to get up and moving safely</label>
</div><div class="align-left">
    <%= string.Format("<input id='{0}_HomeBoundReason4' class='float-left radio' name='{0}_HomeBoundReason' value='4' type='checkbox' {1} />", Model.Type, homeBoundReason.Contains("4").ToChecked()) %>
    <label for="<%= Model.Type %>_HomeBoundReason4" class="radio">Severe Dyspnea</label>
</div><div class="align-left">
    <%= string.Format("<input id='{0}_HomeBoundReason5' class='float-left radio' name='{0}_HomeBoundReason' value='5' type='checkbox' {1} />", Model.Type, homeBoundReason.Contains("5").ToChecked()) %>
    <label for="<%= Model.Type %>_HomeBoundReason5" class="radio">Unable to safely leave home unassisted</label>
</div><div class="align-left">
    <%= string.Format("<input id='{0}_HomeBoundReason6' class='float-left radio' name='{0}_HomeBoundReason' value='6' type='checkbox' {1} />", Model.Type, homeBoundReason.Contains("6").ToChecked()) %>
    <label for="<%= Model.Type %>_HomeBoundReason6" class="radio">Unsafe to leave home due to cognitive or psychiatric impairments</label>
</div><div class="align-left">
    <%= string.Format("<input id='{0}_HomeBoundReason7' class='float-left radio' name='{0}_HomeBoundReason' value='7' type='checkbox' {1} />", Model.Type, homeBoundReason.Contains("7").ToChecked()) %>
    <label for="<%= Model.Type %>_HomeBoundReason7" class="radio">Unable to leave home due to medical restriction(s)</label>
</div><div class="align-left">
    <%= string.Format("<input id='{0}_HomeBoundReason8' class='float-left radio' name='{0}_HomeBoundReason' value='8' type='checkbox' {1} />", Model.Type, homeBoundReason.Contains("8").ToChecked()) %>
    <span class="radio">
        <label for="<%= Model.Type %>_HomeBoundReason8">Other</label>
        <%= Html.TextBox("OtherHomeBoundDetails", data.AnswerOrEmptyString("OtherHomeBoundDetails"), new { @id = Model.Type + "_HomeBoundReasonOther", @maxlength = "60", @class = "oe" })%>
    </span>
</div>
<div class="align-left">
    <label for="<%= Model.Type %>_HomeEnvironment" class="float-left">Home Environment</label><div class="clear">&#160;</div>
    <div class="float-right">
        <%  var homeEnvironment = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "0" },
            new SelectListItem { Text = "No issues identified", Value = "1" },
            new SelectListItem { Text = "Lack of Finances", Value = "2" },
            new SelectListItem { Text = "Lack of CG/Family Support", Value = "3" },
            new SelectListItem { Text = "Poor Home Environment", Value = "4" },
            new SelectListItem { Text = "Cluttered/Soiled Living Conditions", Value = "5" },
            new SelectListItem { Text = "Other", Value = "6" }
        }, "Value", "Text", data.AnswerOrDefault("HomeEnvironment", "0")); %>
        <%= Html.DropDownList(Model.Type + "_HomeEnvironment", homeEnvironment, new { @id = Model.Type + "_HomeEnvironment" }) %>
    </div>
</div>