﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericCardioVascular = data.AnswerArray("GenericCardioVascular"); %>
<%= Html.Hidden(Model.Type + "_GenericCardioVascular", string.Empty, new { @id = Model.Type + "_GenericCardioVascularHidden" })%>
<div class="row">
    <div class="wide checkgroup">
        <div class="option">
            <%= string.Format("<input id='{0}_GenericCardioVascular1' name='{0}_GenericCardioVascular' value='1' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericCardioVascular1">WNL (Within Normal Limits)</label>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericCardioVascular2' name='{0}_GenericCardioVascular' value='2' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("2").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericCardioVascular2">Heart Rhythm:</label>
            <div class="extra">
                <%  var heartRhythm = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "Regular/WNL", Value = "1" },
                        new SelectListItem { Text = "Tachycardia", Value = "2" },
                        new SelectListItem { Text = "Bradycardia", Value = "3" },
                        new SelectListItem { Text = "Arrhythmia/ Dysrhythmia", Value = "4" },
                        new SelectListItem { Text = "Other", Value = "5" }
                    }, "Value", "Text", data.AnswerOrDefault("GenericHeartSoundsType", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericHeartSoundsType", heartRhythm, new { @id = Model.Type + "_GenericHeartSoundsType", @class = "short" }) %>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericCardioVascular3' name='{0}_GenericCardioVascular' value='3' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("3").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericCardioVascular3">Cap. Refill:</label>
            <div class="extra">
                <% var CapRefill = new SelectList(new[] {
                    new SelectListItem { Text = "", Value = "" },
                    new SelectListItem { Text = "<3 sec", Value = "1" },
                    new SelectListItem { Text = ">3 sec", Value = "0" }
                }, "Value", "Text", data.AnswerOrEmptyString("GenericCapRefillLessThan3")); %>
                <%= Html.DropDownList(Model.Type + "_GenericCapRefillLessThan3", CapRefill, new { @id = Model.Type + "_GenericCapRefillLessThan3", @class = "oe" }) %>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericCardioVascular4' name='{0}_GenericCardioVascular' value='4' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("4").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericCardioVascular4">Pulses:</label>
            <div class="extra">
                <label class="float-left" for="GenericCardiovascularRadial">Radial:</label>
                <div class="fr">
                    <%  var radialPulse = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "1+ Weak", Value = "1" },
                            new SelectListItem { Text = "2+ Normal", Value = "2" },
                            new SelectListItem { Text = "3+ Strong", Value = "3" },
                            new SelectListItem { Text = "4+ Bounding", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericCardiovascularRadial", "0")); %>
                    <%= Html.DropDownList(Model.Type + "_GenericCardiovascularRadial", radialPulse, new { @id = Model.Type + "_GenericCardiovascularRadial", @class = "short" })%>
                </div>
                <div class="clear"></div>
                <div class="fr">
                    <%= string.Format("<input id='{0}_GenericCardiovascularRadialPosition0' name='{0}_GenericCardiovascularRadialPosition' class='radio' value='0' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("0").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericCardiovascularRadialPosition0" class="inline-radio">Bilateral</label>
                    <%= string.Format("<input id='{0}_GenericCardiovascularRadialPosition1' name='{0}_GenericCardiovascularRadialPosition' class='radio' value='1' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("1").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericCardiovascularRadialPosition1" class="inline-radio">Left</label>
                    <%= string.Format("<input id='{0}_GenericCardiovascularRadialPosition2' name='{0}_GenericCardiovascularRadialPosition' class='radio' value='2' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("2").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericCardiovascularRadialPosition2" class="inline-radio">Right</label>
                </div>
                <div class="clear"></div>
                <label class="float-left" for="GenericCardiovascularPedal">Pedal:</label>
                <div class="fr">
                    <% var pedalPulse = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "1+ Weak", Value = "1" },
                        new SelectListItem { Text = "2+ Normal", Value = "2" },
                        new SelectListItem { Text = "3+ Strong", Value = "3" },
                        new SelectListItem { Text = "4+ Bounding", Value = "4" }
                    }, "Value", "Text", data.AnswerOrDefault("GenericCardiovascularPedal", "0")); %>
                    <%= Html.DropDownList(Model.Type + "_GenericCardiovascularPedal", pedalPulse, new { @id = Model.Type + "_GenericCardiovascularPedal", @class = "oe" }) %>
                </div>
                <div class="clear"></div>
                <div class="fr">
                    <%= string.Format("<input id='{0}_GenericCardiovascularPedalPosition0' name='{0}_GenericCardiovascularPedalPosition' class='radio' value='0' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("0").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericCardiovascularPedalPosition0" class="inline-radio">Bilateral</label>
                    <%= string.Format("<input id='{0}_GenericCardiovascularPedalPosition1' name='{0}_GenericCardiovascularPedalPosition' class='radio' value='1' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("1").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericCardiovascularPedalPosition1" class="inline-radio">Left</label>
                    <%= string.Format("<input id='{0}_GenericCardiovascularPedalPosition2' name='{0}_GenericCardiovascularPedalPosition' class='radio' value='2' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("2").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericCardiovascularPedalPosition2" class="inline-radio">Right</label>
                </div>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericCardioVascular5' name='{0}_GenericCardioVascular' value='5' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("5").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericCardioVascular5">Edema:</label>
            <div class="extra">
                <label class="float-left" for="GenericEdemaLocation">Location:</label>
                <div class="fr"><%= Html.TextBox(Model.Type + "_GenericEdemaLocation", data.AnswerOrEmptyString("GenericEdemaLocation"), new { @id = Model.Type + "_GenericEdemaLocation", @class = "short", @maxlength = "50" })%></div>
                <%  string[] genericPittingEdemaType = data.AnswerArray("GenericPittingEdemaType"); %>
                <%= Html.Hidden(Model.Type + "_GenericPittingEdemaType", string.Empty, new { @id = Model.Type + "_GenericPittingEdemaTypeHidden" })%>
                <div class="clear"></div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericPittingEdemaType1' name='{0}_GenericPittingEdemaType' value='1' type='checkbox' {1} />", Model.Type, genericPittingEdemaType.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPittingEdemaType1">Non-Pitting:</label>
                </div>
                <div id="<%= Model.Type %>__GenericPittingEdemaType1More">
                    <div class="fr">
                        <%  var nonPitting = new SelectList( new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "N/A", Value = "1" },
                                new SelectListItem { Text = "Mild", Value = "2" },
                                new SelectListItem { Text = "Moderate", Value = "3" },
                                new SelectListItem { Text = "Severe", Value = "4" }
                            }, "Value", "Text", data.AnswerOrDefault("GenericEdemaNonPitting", "0")); %>
                        <%= Html.DropDownList(Model.Type + "_GenericEdemaNonPitting", nonPitting, new { @id = Model.Type + "_GenericEdemaNonPitting", @class = "short" }) %>
                    </div>
                    <div class="clear"></div>
                    <div class="fr">
                        <%= string.Format("<input id='{0}_GenericEdemaNonPittingPosition0' name='{0}_GenericEdemaNonPittingPosition' class='radio' value='0' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("0").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericEdemaNonPittingPosition0" class="inline-radio">Bilateral</label>
                        <%= string.Format("<input id='{0}_GenericEdemaNonPittingPosition1' name='{0}_GenericEdemaNonPittingPosition' class='radio' value='1' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("1").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericEdemaNonPittingPosition1" class="inline-radio">Left</label>
                        <%= string.Format("<input id='{0}_GenericEdemaNonPittingPosition2' name='{0}_GenericEdemaNonPittingPosition' class='radio' value='2' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("2").ToChecked())%>
                        <label for="<%= Model.Type %>_GenericEdemaNonPittingPosition2" class="inline-radio">Right</label>
                    </div>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericPittingEdemaType2' name='{0}_GenericPittingEdemaType' value='2' type='checkbox' {1} />", Model.Type, genericPittingEdemaType.Contains("2").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericPittingEdemaType2">Pitting:</label>
                </div>
                <div id="<%= Model.Type %>__GenericPittingEdemaType2More" class="fr">
                    <%  var pitting = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "1+", Value = "1" },
                            new SelectListItem { Text = "2+", Value = "2" },
                            new SelectListItem { Text = "3+", Value = "3" },
                            new SelectListItem { Text = "4+", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericEdemaPitting", "0")); %>
                    <%= Html.DropDownList(Model.Type + "_GenericEdemaPitting", pitting, new { @id = Model.Type + "_GenericEdemaPitting", @class = "oe" }) %>
                </div>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericCardioVascular6' name='{0}_GenericCardioVascular' value='6' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("6").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericCardioVascular6">Heart Sound</label>
            <div class="extra">
                <%  var heartSound = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "Distant", Value = "1" },
                        new SelectListItem { Text = "Faint", Value = "2" },
                        new SelectListItem { Text = "Reg", Value = "3" },
                        new SelectListItem { Text = "Murmur", Value = "4" }
                    }, "Value", "Text", data.AnswerOrDefault("GenericHeartSound", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericHeartSound", heartSound, new { @id = Model.Type + "_GenericHeartSound", @class = "short" })%>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericCardioVascular7' name='{0}_GenericCardioVascular' value='7' type='checkbox' {1} />", Model.Type, genericCardioVascular.Contains("7").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericCardioVascular7">Neck Veins</label>
            <div class="extra">
                <%  var neckVeins = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "Flat", Value = "1" },
                        new SelectListItem { Text = "Distended", Value = "2" },
                        new SelectListItem { Text = "Cyanosis", Value = "3" },
                        new SelectListItem { Text = "Other", Value = "4" }
                    }, "Value", "Text", data.AnswerOrDefault("GenericNeckVeins", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericNeckVeins", neckVeins, new { @id = Model.Type + "_GenericNeckVeins", @class = "short" })%>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <label for="<%= Model.Type %>_GenericCardiovascularComment" class="strong">Comment:</label>
    <div class="ac"><%= Html.TextArea(Model.Type + "_GenericCardiovascularComment", data.AnswerOrEmptyString("GenericCardiovascularComment"), new { @id = Model.Type + "_GenericCardiovascularComment", @class = "fill" })%></div>
</div>
