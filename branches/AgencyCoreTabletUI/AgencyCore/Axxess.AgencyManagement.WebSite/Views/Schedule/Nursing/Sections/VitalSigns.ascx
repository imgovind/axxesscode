﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="row">
    <label class="float-left" for="<%= Model.Type %>_GenericTemp">Temp</label>
    <div class="fr">
        <%= Html.TextBox(Model.Type + "_GenericTemp", data.AnswerOrEmptyString("GenericTemp"), new { @class = "vitals", @id = Model.Type + "_GenericTemp" }) %>
        <% var temp = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Axillary ", Value = "Axillary" },
            new SelectListItem { Text = "Oral", Value = "Oral" },
            new SelectListItem { Text = "Tympanic", Value = "Tympanic" },
            new SelectListItem { Text = "Temporal", Value = "Temporal" }
        }, "Value", "Text", data.AnswerOrDefault("GenericTempType", "0")); %>
        <%= Html.DropDownList(Model.Type + "_GenericTempType", temp, new { @id = Model.Type + "_GenericTempType", @class = "shorter" })%>
    </div>
</div>
<div class="row">
    <label class="float-left" for="<%= Model.Type %>_GenericResp">Resp</label>
    <div class="fr">
        <%= Html.TextBox(Model.Type + "_GenericResp", data.AnswerOrEmptyString("GenericResp"), new { @class = "vitals", @id = Model.Type + "_GenericResp" })%>
        <% var resp = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Regular ", Value = "Regular" },
            new SelectListItem { Text = "Irregular", Value = "Irregular" }
        }, "Value", "Text", data.AnswerOrDefault("GenericRespType", "0")); %>
        <%= Html.DropDownList(Model.Type + "_GenericRespType", resp, new { @id = Model.Type + "_GenericRespType", @class = "shorter" })%>
    </div>
</div>
<div class="row">
    <label class="float-left" for="<%= Model.Type %>_GenericPulseApical">Apical Pulse</label>
    <div class="fr">
        <%= Html.TextBox(Model.Type + "_GenericPulseApical", data.AnswerOrEmptyString("GenericPulseApical"), new { @class = "vitals", @id = Model.Type + "_GenericPulseApical" })%>
        <% var pulseApical = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Regular ", Value = "Regular" },
            new SelectListItem { Text = "Irregular", Value = "Irregular" }
        }, "Value", "Text", data.AnswerOrDefault("GenericPulseApicalRegular", "0")); %>
        <%= Html.DropDownList(Model.Type + "_GenericPulseApicalRegular", pulseApical, new { @id = Model.Type + "_GenericPulseApicalRegular", @class = "shorter" })%>
    </div>
</div>
<div class="row">
    <label class="float-left" for="<%= Model.Type %>_GenericPulseRadial">Radial Pulse</label>
    <div class="fr">
        <%= Html.TextBox(Model.Type + "_GenericPulseRadial", data.AnswerOrEmptyString("GenericPulseRadial"), new { @class = "vitals", @id = Model.Type + "_GenericPulseRadial" })%>
        <% var pulseRadial = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Regular ", Value = "Regular" },
            new SelectListItem { Text = "Irregular", Value = "Irregular" }
        }, "Value", "Text", data.AnswerOrDefault("GenericPulseRadialRegular", "0")); %>
        <%= Html.DropDownList(Model.Type + "_GenericPulseRadialRegular", pulseRadial, new { @id = Model.Type + "_GenericPulseRadialRegular", @class = "shorter" })%>
    </div>
</div>
<div class="row">
    <table class="fixed">
        <thead>
            <tr>
                <th class="al_left strong">BP</th>
                <th class="strong">Lying</th>
                <th class="strong">Sitting</th>
                <th class="strong">Standing</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="al strong">Left</th>
                <td class="ac"><%= Html.TextBox(Model.Type + "_GenericBPLeftLying", data.AnswerOrEmptyString("GenericBPLeftLying"), new { @class = "vitals", @id = Model.Type + "_GenericBPLeftLying" })%></td>
                <td class="ac"><%= Html.TextBox(Model.Type + "_GenericBPLeftSitting", data.AnswerOrEmptyString("GenericBPLeftSitting"), new { @class = "vitals", @id = Model.Type + "_GenericBPLeftSitting" })%></td>
                <td class="ac"><%= Html.TextBox(Model.Type + "_GenericBPLeftStanding", data.AnswerOrEmptyString("GenericBPLeftStanding"), new { @class = "vitals", @id = Model.Type + "_GenericBPLeftStanding" })%></td>
            </tr><tr>
                <th class="al strong">Right</th>
                <td class="ac"><%= Html.TextBox(Model.Type + "_GenericBPRightLying", data.AnswerOrEmptyString("GenericBPRightLying"), new { @class = "vitals", @id = Model.Type + "_GenericBPRightLying" })%></td>
                <td class="ac"><%= Html.TextBox(Model.Type + "_GenericBPRightSitting", data.AnswerOrEmptyString("GenericBPRightSitting"), new { @class = "vitals", @id = Model.Type + "_GenericBPRightSitting" })%></td>
                <td class="ac"><%= Html.TextBox(Model.Type + "_GenericBPRightStanding", data.AnswerOrEmptyString("GenericBPRightStanding"), new { @class = "vitals", @id = Model.Type + "_GenericBPRightStanding" })%></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="row">
    <label class="float-left" for="<%= Model.Type %>_GenericWeight">Weight</label>
    <div class="fr">
        <%= Html.TextBox(Model.Type + "_GenericWeight", data.AnswerOrEmptyString("GenericWeight"), new { @class = "vitals", @id = Model.Type + "_GenericWeight" })%>
        <% var weight = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "N/A ", Value = "N/A" },
            new SelectListItem { Text = "kg", Value = "kg" },
            new SelectListItem { Text = "lbs", Value = "lbs" }
        }, "Value", "Text", data.AnswerOrDefault("GenericWeightUnit", "0")); %>
        <%= Html.DropDownList(Model.Type + "_GenericWeightUnit", weight, new { @id = Model.Type + "_GenericWeightUnit", @class = "shorter" })%>
    </div>
</div>
<div class="row">
    <label class="float-left" for="<%= Model.Type %>_GenericPulseOximetry">Pulse Oximetry</label>
    <div class="fr">
        <%= Html.TextBox(Model.Type + "_GenericPulseOximetry", data.AnswerOrEmptyString("GenericPulseOximetry"), new { @class = "vitals", @id = Model.Type + "_GenericPulseOximetry" })%>
        <% var pulseOximetry = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "N/A ", Value = "N/A" },
            new SelectListItem { Text = "On O2", Value = "On O2" },
            new SelectListItem { Text = "on RA", Value = "on RA" }
        }, "Value", "Text", data.AnswerOrDefault("GenericPulseOximetryUnit", "0")); %>
        <%= Html.DropDownList(Model.Type + "_GenericPulseOximetryUnit", pulseOximetry, new { @id = Model.Type + "_GenericPulseOximetryUnit", @class = "shorter" })%>
    </div>
</div>
<div class="row">
    <label class="strong" for="<%= Model.Type %>_GenericVitlaSignComment">Comment</label>
    <div class="ac"><%= Html.TextArea(Model.Type + "_GenericVitlaSignComment", data.AnswerOrEmptyString("GenericVitlaSignComment"), new { @id = Model.Type + "_GenericVitlaSignComment", @class = "fill" })%></div>
</div>