﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
printview.span("<%= data.AnswerOrEmptyString("GenericNarrativeComment").Clean() %>")
<%  if (Model.Type == "SNDiabeticDailyVisit") { %> +
printview.col(3,
    printview.span("Tolerated Cares",true) +
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("ToleratedCares").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("ToleratedCares").Equals("0").ToString().ToLower() %>)) +
printview.span("Describe",true) +
printview.span("<%= data.AnswerOrEmptyString("ToleratedCaresDescribe").Clean() %>") +
printview.col(3,
    printview.span("Patient Goals Met this Visit",true) +
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GoalsMet").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("GoalsMet").Equals("0").ToString().ToLower() %>)) +
printview.span("Specify",true) +
printview.span("<%= data.AnswerOrEmptyString("GoalsMetSpecify").Clean() %>") +
printview.col(3,
    printview.span("Care Plan Revised",true) +
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("RevisedCarePlan").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("RevisedCarePlan").Equals("0").ToString().ToLower() %>)) +
printview.span("Specify",true) +
printview.span("<%= data.AnswerOrEmptyString("RevisedCarePlanSpecify").Clean() %>") +
printview.span("Patient Response",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericPatientResponse").Clean() %>") +
printview.col(3,
    printview.span("HHA Present",true) +
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("HhaPresent").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("HhaPresent").Equals("0").ToString().ToLower() %>)) +
printview.col(3,
    printview.span("Aide following Care Plan",true) +
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("AideFollowCarePlan").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("AideFollowCarePlan").Equals("0").ToString().ToLower() %>)) +
printview.span("Comments",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericComments").Clean() %>")
<%  } %>,
"Narrative &#38; Teaching"