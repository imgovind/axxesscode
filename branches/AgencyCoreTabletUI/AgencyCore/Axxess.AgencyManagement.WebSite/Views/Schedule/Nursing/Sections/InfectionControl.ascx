﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  if (Current.HasRight(Permissions.ManageIncidentAccidentInfectionReport)) { %>
<div class="buttons">
    <ul>
        <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Patient.LoadNewInfectionReport('{0}');\" title=\"New Infection Report\">Add Infection Report</a>", Model.PatientId)%></li>
    </ul>
</div>
<% } %>
<%  string[] infectionControl = data.AnswerArray("InfectionControl"); %>
<%= Html.Hidden(Model.Type + "_InfectionControl", string.Empty, new { @id = Model.Type + "_InfectionControlHidden" })%>
<ul class="checkgroup">
    <li>
        <div class="option">
            <%= string.Format("<input id='{0}_InfectionControl1' name='{0}_InfectionControl' value='1' type='checkbox' {1} />", Model.Type, infectionControl.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_InfectionControl1">Universal Precautions Observed</label>
        </div>
    </li><li>
        <div class="option">
            <%= string.Format("<input id='{0}_InfectionControl2' name='{0}_InfectionControl' value='2' type='checkbox' {1} />", Model.Type, infectionControl.Contains("2").ToChecked()) %>
            <label for="<%= Model.Type %>_InfectionControl2">Sharps/Waste Disposal</label>
        </div>
    </li>
</ul>
<div class="clear"></div>
<label class="float-left">New Infection:</label>
<div class="float-right">
    <ul class="checkgroup inline">
        <li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericIsNewInfection", "1", data.AnswerOrEmptyString("GenericIsNewInfection").Equals("1"), new { @id = Model.Type + "_GenericIsNewInfection1" }) %>
                <label for="<%= Model.Type %>_GenericIsNewInfection1">Yes</label>
            </div>
        </li><li>
            <div class="option">
                <%= Html.RadioButton(Model.Type + "_GenericIsNewInfection", "0", data.AnswerOrEmptyString("GenericIsNewInfection").Equals("0"), new { @id = Model.Type + "_GenericIsNewInfection0" }) %>
                <label for="<%= Model.Type %>_GenericIsNewInfection0">No</label>
            </div>
        </li>
    </ul>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericInfectionControlComment" class="strong">Comments:</label>
<div class="align-center"><%= Html.TextArea(Model.Type + "_GenericInfectionControlComment", data.AnswerOrEmptyString("GenericInfectionControlComment"), new { @class = "fill", @id = Model.Type + "_GenericInfectionControlComment" })%></div>