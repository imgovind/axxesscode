﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] interventions = data.AnswerArray("GenericInterventions"); %>
printview.col(2,
    printview.checkbox("Skilled Observation/Assessment",<%= interventions.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Instructed on Safety Precautions",<%= interventions.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Medication Adminstration",<%= interventions.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Diabetic Monitoring/Care",<%= interventions.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("Foley Change",<%= interventions.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("IV Tubing Change",<%= interventions.Contains("6").ToString().ToLower() %>) +
    printview.checkbox("Patient/CG teaching",<%= interventions.Contains("7").ToString().ToLower() %>) +
    printview.checkbox("Inst. on Emergency Preparedness",<%= interventions.Contains("8").ToString().ToLower() %>) +
    printview.checkbox("Prep./Admin. Insulin",<%= interventions.Contains("9").ToString().ToLower() %>) +
    printview.checkbox("Administer Enteral nutrition",<%= interventions.Contains("10").ToString().ToLower() %>) +
    printview.checkbox("Glucometer calibration",<%= interventions.Contains("11").ToString().ToLower() %>) +
    printview.checkbox("Trachea care",<%= interventions.Contains("12").ToString().ToLower() %>) +
    printview.checkbox("IV Site Dressing Change",<%= interventions.Contains("13").ToString().ToLower() %>) +
    printview.checkbox("IM Injection/SQ Injection",<%= interventions.Contains("14").ToString().ToLower() %>) +
    printview.checkbox("Peg/GT Tube Site care",<%= interventions.Contains("15").ToString().ToLower() %>) +
    printview.checkbox("Foot care performed",<%= interventions.Contains("16").ToString().ToLower() %>) +
    printview.checkbox("Wound Care/Dressing Change",<%= interventions.Contains("17").ToString().ToLower() %>) +
    printview.checkbox("IV Site Change",<%= interventions.Contains("18").ToString().ToLower() %>) +
    printview.checkbox("Foley Irrigation",<%= interventions.Contains("19").ToString().ToLower() %>) +
    printview.checkbox("Diet Teaching: <%= data.AnswerOrEmptyString("GenericDietTeaching").Clean() %>",<%= interventions.Contains("20").ToString().ToLower() %>) +
    printview.checkbox("Venipuncture/Lab",<%= interventions.Contains("21").ToString().ToLower() %>) +
    printview.span("<%= data.AnswerOrEmptyString("GenericVenipuncture").Clean() %>") +
    printview.checkbox("Instructed on Medication",<%= interventions.Contains("22").ToString().ToLower() %>) +
    printview.span("<%= data.AnswerOrEmptyString("GenericInstructedOnMedication").Clean() %>") +
    printview.checkbox("Instructed on Disease Process to inc.",<%= interventions.Contains("23").ToString().ToLower() %>) +
    printview.span("<%= data.AnswerOrEmptyString("GenericInstructedOnDiseaseProcess").Clean() %>")) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericInterventionsComment").Clean() %>"),
"Interventions"