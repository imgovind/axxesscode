﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericNeurologicalStatus = data.AnswerArray("GenericNeurologicalStatus"); %>
<%= Html.Hidden(Model.Type + "_GenericNeurologicalStatus", string.Empty, new { @id = Model.Type + "_GenericNeurologicalStatusHidden" })%>
<div class="row">
    <div class="wide checkgroup">
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus1' name='{0}_GenericNeurologicalStatus' value='1' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("1").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus1">LOC</label>
            <div class="extra">
                <div clasc="fr">
                    <% var LOC = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "" },
                        new SelectListItem { Text = "Alert", Value = "Alert" },
                        new SelectListItem { Text = "Lethargic", Value = "Lethargic" },
                        new SelectListItem { Text = "Comatose", Value = "Comatose" },
                        new SelectListItem { Text = "Disoriented", Value = "Disoriented" },
                        new SelectListItem { Text = "Other", Value = "Other" }
                    }, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalLOC", "0")); %>
                    <%= Html.DropDownList(Model.Type + "_GenericNeurologicalLOC", LOC, new { @id = Model.Type + "_GenericNeurologicalLOC", @class = "short" })%>
                </div>
                <div class="clear"></div>
                <div class="fl">Orientation:</div>
                <%  string[] genericNeurologicalOriented = data.AnswerArray("GenericNeurologicalOriented"); %>
                <%= Html.Hidden(Model.Type + "_GenericNeurologicalOriented", string.Empty, new { @id = Model.Type + "_GenericNeurologicalOrientedHidden" })%>
                <div class="fr">
                    <%= string.Format("<input id='{0}_GenericNeurologicalOriented1' class='radio' name='{0}_GenericNeurologicalOriented' value='1' type='radio' {1} />", Model.Type, genericNeurologicalOriented.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericNeurologicalOriented1" class="inline-radio">Person</label>
                    <%= string.Format("<input id='{0}_GenericNeurologicalOriented2' class='radio' name='{0}_GenericNeurologicalOriented' value='2' type='radio' {1} />", Model.Type, genericNeurologicalOriented.Contains("2").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericNeurologicalOriented2" class="inline-radio">Place</label>
                    <%= string.Format("<input id='{0}_GenericNeurologicalOriented3' class='radio' name='{0}_GenericNeurologicalOriented' value='3' type='radio' {1} />", Model.Type, genericNeurologicalOriented.Contains("3").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericNeurologicalOriented3" class="inline-radio">Time</label>
                </div>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus12' name='{0}_GenericNeurologicalStatus' value='12' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("12").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus12">Behavior Status</label>
            <div class="extra">
                <%  string[] genericBehaviorStatus = data.AnswerArray("GenericBehaviorStatus"); %>
                <%= Html.Hidden(Model.Type + "_GenericBehaviorStatus", string.Empty, new { @id = Model.Type + "_GenericBehaviorStatusHidden" })%>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericBehaviorStatus1' name='{0}_GenericBehaviorStatus' value='1' type='checkbox' {1} />", Model.Type, genericBehaviorStatus.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericBehaviorStatus1">WNL</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericBehaviorStatus2' name='{0}_GenericBehaviorStatus' value='2' type='checkbox' {1} />", Model.Type, genericBehaviorStatus.Contains("2").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericBehaviorStatus2">Difficulty coping w/ Altered Health Status</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericBehaviorStatus3' name='{0}_GenericBehaviorStatus' value='3' type='checkbox' {1} />", Model.Type, genericBehaviorStatus.Contains("3").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericBehaviorStatus3">Withdrawn</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericBehaviorStatus4' name='{0}_GenericBehaviorStatus' value='4' type='checkbox' {1} />", Model.Type, genericBehaviorStatus.Contains("4").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericBehaviorStatus4">Combative</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericBehaviorStatus5' name='{0}_GenericBehaviorStatus' value='5' type='checkbox' {1} />", Model.Type, genericBehaviorStatus.Contains("5").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericBehaviorStatus5">Expresses Depression</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericBehaviorStatus6' name='{0}_GenericBehaviorStatus' value='6' type='checkbox' {1} />", Model.Type, genericBehaviorStatus.Contains("6").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericBehaviorStatus6">Irritability</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericBehaviorStatus7' name='{0}_GenericBehaviorStatus' value='7' type='checkbox' {1} />", Model.Type, genericBehaviorStatus.Contains("7").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericBehaviorStatus7">Impaired Decision Making</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericBehaviorStatus8' name='{0}_GenericBehaviorStatus' value='8' type='checkbox' {1} />", Model.Type, genericBehaviorStatus.Contains("8").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericBehaviorStatus8">Other</label>
                </div>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus2' name='{0}_GenericNeurologicalStatus' value='2' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("2").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus2">Pupils:</label>
            <div class="extra">
                <div class="fr">
                    <%  var pupils = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "PERRLA/WNL", Value = "1" },
                            new SelectListItem { Text = "Sluggish", Value = "2" },
                            new SelectListItem { Text = "Non-Reactive", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalPupils", "0")); %>
                    <%= Html.DropDownList(Model.Type + "_GenericNeurologicalPupils", pupils, new { @id = Model.Type + "_GenericNeurologicalPupils" }) %>
                </div>
                <div class="clear"></div>
                <div class="fr">
                    <%= string.Format("<input id='{0}_GenericNeurologicalPupilsPosition0' name='{0}_GenericNeurologicalPupilsPosition' value='0' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("0").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericNeurologicalPupilsPosition0" class="inline-radio">Bilateral</label>
                    <%= string.Format("<input id='{0}_GenericNeurologicalPupilsPosition1' name='{0}_GenericNeurologicalPupilsPosition' value='1' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("1").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericNeurologicalPupilsPosition1" class="inline-radio">Left</label>
                    <%= string.Format("<input id='{0}_GenericNeurologicalPupilsPosition2' name='{0}_GenericNeurologicalPupilsPosition' value='2' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("GenericNeurologicalPupilsPosition").Equals("2").ToChecked())%>
                    <label for="<%= Model.Type %>_GenericNeurologicalPupilsPosition2" class="inline-radio">Right</label>
                </div>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus3' name='{0}_GenericNeurologicalStatus' value='3' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("3").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus3">Vision:</label>
            <div class="extra">
                <%  string[] genericNeurologicalVisionStatus = data.AnswerArray("GenericNeurologicalVisionStatus"); %>
                <%= Html.Hidden(Model.Type + "_GenericNeurologicalVisionStatus", string.Empty, new { @id = Model.Type + "_GenericNeurologicalVisionStatusHidden" })%>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus1' name='{0}_GenericNeurologicalVisionStatus' value='1' type='checkbox' {1} />", Model.Type, genericNeurologicalVisionStatus.Contains("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericNeurologicalVisionStatus1" class="inline-radio">WNL</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus2' name='{0}_GenericNeurologicalVisionStatus' value='2' type='checkbox' {1} />", Model.Type, genericNeurologicalVisionStatus.Contains("2").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericNeurologicalVisionStatus2" class="inline-radio">Blurred Vision</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus3' name='{0}_GenericNeurologicalVisionStatus' value='3' type='checkbox' {1} />", Model.Type, genericNeurologicalVisionStatus.Contains("3").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericNeurologicalVisionStatus3" class="inline-radio">Cataracts</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus4' name='{0}_GenericNeurologicalVisionStatus' value='4' type='checkbox' {1} />", Model.Type, genericNeurologicalVisionStatus.Contains("4").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericNeurologicalVisionStatus4" class="inline-radio">Wears Corrective Lenses</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus5' name='{0}_GenericNeurologicalVisionStatus' value='5' type='checkbox' {1} />", Model.Type, genericNeurologicalVisionStatus.Contains("5").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericNeurologicalVisionStatus5" class="inline-radio">Glaucoma</label>
                </div>
                <div class="fl">
                    <%= string.Format("<input id='{0}_GenericNeurologicalVisionStatus6' name='{0}_GenericNeurologicalVisionStatus' value='6' type='checkbox' {1} />", Model.Type, genericNeurologicalVisionStatus.Contains("6").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericNeurologicalVisionStatus6" class="inline-radio">Legally Blind</label>
                </div>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus4' name='{0}_GenericNeurologicalStatus' value='4' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("4").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus4">Speech:</label>
            <div class="extra fr">
                <%  var speech = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "Clear", Value = "1" },
                        new SelectListItem { Text = "Slurred", Value = "2" },
                        new SelectListItem { Text = "Aphasic", Value = "3" },
                        new SelectListItem { Text = "Other", Value = "4" }
                    }, "Value", "Text", data.AnswerOrDefault("GenericNeurologicalSpeech", "0")); %>
                <%= Html.DropDownList(Model.Type + "_GenericNeurologicalSpeech", speech, new { @id = Model.Type + "_GenericNeurologicalSpeech", @class = "oe" }) %>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus5' name='{0}_GenericNeurologicalStatus' value='5' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("5").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus5">Paralysis:</label>
            <div class="extra fr">
                <%= Html.TextBox(Model.Type + "_GenericNeurologicalParalysisLocation", data.AnswerOrEmptyString("GenericNeurologicalParalysisLocation"), new { @id = Model.Type + "_GenericNeurologicalParalysisLocation", @class = "oe", @maxlength = "30" }) %>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus6' name='{0}_GenericNeurologicalStatus' value='6' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("6").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus6">Quadriplegia</label>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus7' name='{0}_GenericNeurologicalStatus' value='7' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("7").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus7">Paraplegia</label>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus8' name='{0}_GenericNeurologicalStatus' value='8' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("8").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus8">Seizures</label>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus9' name='{0}_GenericNeurologicalStatus' value='9' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("9").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus9">Tremors</label>
            <div class="extra">
                <label for="<%= Model.Type %>_GenericNeurologicalTremorsLocation">Location:</label>
                <%= Html.TextBox(Model.Type + "_GenericNeurologicalTremorsLocation", data.AnswerOrEmptyString("GenericNeurologicalTremorsLocation"), new { @id = Model.Type + "_GenericNeurologicalTremorsLocation", @class = "oe" }) %>
            </div>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus10' name='{0}_GenericNeurologicalStatus' value='10' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("10").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus10">Dizziness</label>
        </div>
        <div class="option">
            <%= string.Format("<input id='{0}_GenericNeurologicalStatus11' name='{0}_GenericNeurologicalStatus' value='11' type='checkbox' {1} />", Model.Type, genericNeurologicalStatus.Contains("11").ToChecked()) %>
            <label for="<%= Model.Type %>_GenericNeurologicalStatus11">Headache</label>
        </div>
    </div>
</div>
<div class="row">
    <label class="fl strong">HOH:</label>
    <div class="fr">
        <%= string.Format("<input id='{0}_NeurologicalHOHPosition0' name='{0}_NeurologicalHOHPosition' value='0' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("NeurologicalHOHPosition").Equals("0").ToChecked()) %>
        <label for="<%= Model.Type %>_NeurologicalHOHPosition0" class="inline-radio">Bilateral</label>
        <%= string.Format("<input id='{0}_NeurologicalHOHPosition1' name='{0}_NeurologicalHOHPosition' value='1' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("NeurologicalHOHPosition").Equals("1").ToChecked())%>
        <label for="<%= Model.Type %>_NeurologicalHOHPosition1" class="inline-radio">Left</label>
        <%= string.Format("<input id='{0}_NeurologicalHOHPosition2' name='{0}_NeurologicalHOHPosition' value='2' type='radio' {1} />", Model.Type, data.AnswerOrEmptyString("NeurologicalHOHPosition").Equals("2").ToChecked())%>
        <label for="<%= Model.Type %>_NeurologicalHOHPosition2" class="inline-radio">Right</label></div>
    </div>
<div class="row">
    <label for="<%= Model.Type %>_GenericNeurologicalComment" class="strong">Comments:</label>
    <div class="ac"><%= Html.TextArea(Model.Type + "_GenericNeurologicalComment", data.AnswerOrEmptyString("GenericNeurologicalComment"), new { @class = "fill", @id = Model.Type + "_GenericNeurologicalComment" })%></div>
</div>