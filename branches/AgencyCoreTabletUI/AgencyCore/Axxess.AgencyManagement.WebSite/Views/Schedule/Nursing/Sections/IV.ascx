﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div id="<%= Model.Type %>_IVContainer">
    <label for="<%= Model.Type %>_GenericIVAccess" class="float-left">IV Access:</label>
    <div class="float-right">
        <%  var IVAccess = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Saline Lock", Value = "Saline Lock" },
            new SelectListItem { Text = "PICC Line", Value = "PICC Line" },
            new SelectListItem { Text = "Central Line", Value = "Central Line" },
            new SelectListItem { Text = "Port-A-Cath", Value = "Port-A-Cath" },
            new SelectListItem { Text = "Med-A-Port", Value = "Med-A-Port" },
            new SelectListItem { Text = "Other", Value = "Other" }
        }, "Value", "Text", data.AnswerOrDefault("GenericIVAccess", "0")); %>
        <%= Html.DropDownList(Model.Type + "_GenericIVAccess", IVAccess, new { @id = Model.Type + "_GenericIVAccess", @class = "oe" }) %>
    </div>
    <div class="clear"></div>
    <label for="<%= Model.Type %>_GenericIVLocation" class="float-left">IV Location:</label>
    <div class="float-right"><%= Html.TextBox(Model.Type + "_GenericIVLocation", data.AnswerOrEmptyString("GenericIVLocation"), new { @id = Model.Type + "_GenericIVLocation", @class = "oe" }) %></div>
    <div class="clear"></div>
    <label for="<%= Model.Type %>_GenericIVConditionOfIVSite" class="float-left">Condition of IV Site:</label>
    <div class="float-right">
        <%  var IVConditionOfIVSite = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "WNL", Value = "WNL" },
            new SelectListItem { Text = "Phlebitis", Value = "Phlebitis" },
            new SelectListItem { Text = "Redness", Value = "Redness" },
            new SelectListItem { Text = "Swelling", Value = "Swelling" },
            new SelectListItem { Text = "Pallor ", Value = "Pallor" },
            new SelectListItem { Text = "Warmth", Value = "Warmth" },
            new SelectListItem { Text = "Bleeding", Value = "Bleeding" }
        }, "Value", "Text", data.AnswerOrDefault("GenericIVConditionOfIVSite", "0")); %>
        <%= Html.DropDownList(Model.Type + "_GenericIVConditionOfIVSite", IVConditionOfIVSite, new { @id = Model.Type + "_GenericIVConditionOfIVSite", @class = "oe" }) %>
    </div>
    <div class="clear"></div>
    <label for="<%= Model.Type %>_GenericIVConditionOfDressing" class="float-left">Condition of Dressing:</label>
    <div class="float-right">
        <%  var IVConditionOfDressing = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Dry & Intact/WNL", Value = "Dry & Intact/WNL" },
            new SelectListItem { Text = "Bloody", Value = "Bloody" },
            new SelectListItem { Text = "Soiled", Value = "Soiled" },
            new SelectListItem { Text = "Other", Value = "Other" }
        }, "Value", "Text", data.AnswerOrDefault("GenericIVConditionOfDressing", "0")); %>
        <%= Html.DropDownList(Model.Type + "_GenericIVConditionOfDressing", IVConditionOfDressing, new { @id = Model.Type + "_GenericIVConditionOfDressing", @class = "oe" }) %>
    </div>
    <div class="clear"></div>
    <label for="<%= Model.Type %>_GenericIVSiteDressing" class="float-left">IV site Dressing Changed performed on this visit:</label>
    <div class="float-right">
        <%  var IVSiteDressing = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "N/A", Value = "N/A" },
            new SelectListItem { Text = "Yes", Value = "Yes" },
            new SelectListItem { Text = "No", Value = "No" }
        }, "Value", "Text", data.AnswerOrDefault("GenericIVSiteDressing", "0")); %>
        <%= Html.DropDownList(Model.Type + "_GenericIVSiteDressing", IVSiteDressing, new { @id = Model.Type + "_GenericIVSiteDressing", @class = "oe" }) %>
    </div>
    <div class="clear"></div>
    <label for="<%= Model.Type %>_GenericFlush" class="float-left">Flush:</label>
    <div class="float-right">
        <ul class="checkgroup inline">
            <li>
                <div class="option">
                    <%= string.Format("<input id ='{0}_GenericFlush1' type='checkbox' value='1' name='{0}_GenericFlush' {1} />", Model.Type, data.AnswerOrEmptyString("GenericFlush").Equals("1").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericFlush1">Yes</label>
                </div>
            </li><li>
                <div class="option">
                    <%= string.Format("<input id ='{0}_GenericFlush0' type='checkbox' value='0' name='{0}_GenericFlush' {1} />", Model.Type, data.AnswerOrEmptyString("GenericFlush").Equals("0").ToChecked()) %>
                    <label for="<%= Model.Type %>_GenericFlush0">No</label>
                </div>
            </li>
        </ul>
    </div>
    <div class="float-right">
        <label for="<%= Model.Type %>_GenericIVAccessFlushed">Flushed with</label>
        <%= Html.TextBox(Model.Type + "_GenericIVAccessFlushed", data.AnswerOrEmptyString("GenericIVAccessFlushed"), new { @class = "vitals", @id = Model.Type + "_GenericIVAccessFlushed" })%>
        <label for="<%= Model.Type %>_GenericIVSiteDressingUnit">/ml of</label>
        <%  var IVSiteDressingUnit = new SelectList(new[] {
            new SelectListItem { Text = "", Value = "" },
            new SelectListItem { Text = "Normal Saline", Value = "Normal Saline" },
            new SelectListItem { Text = "Heparin", Value = "Heparin" },
            new SelectListItem { Text = "Other", Value = "Other" }
        }, "Value", "Text", data.AnswerOrDefault("GenericIVSiteDressingUnit", "0")); %>
        <%= Html.DropDownList(Model.Type + "_GenericIVSiteDressingUnit", IVSiteDressingUnit, new { @id = Model.Type + "_GenericIVSiteDressingUnit", @class = "oe" }) %>
    </div>
    <div class="clear"></div>
    <label for="<%= Model.Type %>_GenericIVComment" class="strong">Comments:</label>
    <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericIVComment", data.AnswerOrEmptyString("GenericIVComment"), new { @class = "fill", @id = Model.Type + "_GenericIVComment" }) %></div>
</div>