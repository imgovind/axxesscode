﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">LVN Supervisory Visit | <%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase().Clean() : "" %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "LVNSVisitForm" })) { %>
    <%= Html.Hidden("LVNSVisit_PatientId", Model.PatientId)%>
    <%= Html.Hidden("LVNSVisit_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("LVNSVisit_EventId", Model.EventId)%>
    <%= Html.Hidden("Type", "LVNSVisit", new { @id = "LVNSVisit_Type" })%>
    <%= Html.Hidden("DisciplineTask", Model.DisciplineTask, new { @id = "LVNSVisit_DisciplineTask" })%>
    <%= Html.Hidden("button", string.Empty, new { @id = "LVNSVisit_button" })%>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Licensed Vocational Nurse Supervisory Visit</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label class="fl strong">Patient Name</label>
                        <div class="fr"><%= Model.Patient.DisplayName %></div>
                    </div>
                    <div class="row">
                        <label class="fl strong">MR#</label>
                        <div class="fr"><%= Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty %></div>
                    </div>
                    <div class="row">
                        <label for="LVNSVisit_VisitDate" class="float-left">Visit Date:</label>
                        <div class="float-right"><input type="date" name="LVNSVisit_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="LVNSVisit_VisitDate" class="required" /></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label for="LVNSVisit_VisitDate" class="float-left">LVN:</label>
                        <div class="float-right"><%= Html.LVNs("LVN", data.ContainsKey("LVN") ? data["LVN"].Answer : "", new { @id = "LVNSVisit_HealthAide" })%></div>
                    </div>
                    <div class="row">
                        <label for="LVNSVisit_LVNPresentY" class="float-left">LVN Present:</label>
                        <div class="float-right">
                            <%= Html.RadioButton("LVNSVisit_LVNPresent", "1", data.ContainsKey("LVNPresent") && data["LVNPresent"].Answer == "1" ? true : false, new { @id = "LVNSVisit_LVNPresentY", @class = "radio" })%><label class="inline-radio" for="LVNSVisit_LVNPresentY">Yes</label>
                            <%= Html.RadioButton("LVNSVisit_LVNPresent", "0", data.ContainsKey("LVNPresent") && data["LVNPresent"].Answer == "0" ? true : false, new { @id = "LVNSVisit_LVNPresentN", @class = "radio" })%><label class="inline-radio" for="LVNSVisit_LVNPresentN">No</label>
                        </div>
                    </div>
                    <div class="row">
                        <label for="LVNSVisit_AssociatedMileage" class="float-left">Associated Mileage:</label>
                        <div class="float-right"><%= Html.TextBox("LVNSVisit_AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : "", new { @class = "st digits", @maxlength = 6, @id = "LVNSVisit_AssociatedMileage" })%></div>
                    </div>
                </div>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                <div class="buttons">
                    <ul>
                        <li><%= Model.CarePlanOrEvalUrl %></li>
                    </ul>
                </div>
    <%  } %>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Evaluation</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <label class="fl">1. Arrives for assigned visits as scheduled</label>
                        <div class="fr">
                            <%= Html.RadioButton("LVNSVisit_ArriveOnTime", "1", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "1" ? true : false, new { @id = "LVNSVisit_ArriveOnTimeY", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_ArriveOnTimeY">Yes</label>
                            <%= Html.RadioButton("LVNSVisit_ArriveOnTime", "0", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "0" ? true : false, new { @id = "LVNSVisit_ArriveOnTimeN", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_ArriveOnTimeN">No</label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="fl">2. Follows client&#8217;s plan of care</label>
                        <div class="fr">
                            <%= Html.RadioButton("LVNSVisit_FollowPOC", "1", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "1" ? true : false, new { @id = "LVNSVisit_FollowPOCY", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_FollowPOCY">Yes</label>
                            <%= Html.RadioButton("LVNSVisit_FollowPOC", "0", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "0" ? true : false, new { @id = "LVNSVisit_FollowPOCN", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_FollowPOCN">No</label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="fl">3. Demonstrates positive and helpful attitude towards the client and others</label>
                        <div class="fr">
                            <%= Html.RadioButton("LVNSVisit_HasPositiveAttitude", "1", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "1" ? true : false, new { @id = "LVNSVisit_HasPositiveAttitudeY", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_HasPositiveAttitudeY">Yes</label>
                            <%= Html.RadioButton("LVNSVisit_HasPositiveAttitude", "0", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "0" ? true : false, new { @id = "LVNSVisit_HasPositiveAttitudeN", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_HasPositiveAttitudeN">No</label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="fl">4. Informs Nurse Supervisor/Case Manager of client needs and changes in condition</label>
                        <div class="fr">
                            <%= Html.RadioButton("LVNSVisit_InformChanges", "1", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "1" ? true : false, new { @id = "LVNSVisit_InformChangesY", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_InformChangesY">Yes</label>
                            <%= Html.RadioButton("LVNSVisit_InformChanges", "0", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "0" ? true : false, new { @id = "LVNSVisit_InformChangesN", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_InformChangesN">No</label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="fl">5. Implements Universal Precautions per agency policy</label>
                        <div class="fr">
                            <%= Html.RadioButton("LVNSVisit_IsUniversalPrecautions", "1", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "1" ? true : false, new { @id = "LVNSVisit_IsUniversalPrecautionsY", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_IsUniversalPrecautionsY">Yes</label>
                            <%= Html.RadioButton("LVNSVisit_IsUniversalPrecautions", "0", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "0" ? true : false, new { @id = "LVNSVisit_IsUniversalPrecautionsN", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_IsUniversalPrecautionsN">No</label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="fl">6. Any changes made to client plan of care at this time</label>
                        <div class="fr">
                            <%= Html.RadioButton("LVNSVisit_POCChanges", "1", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "1" ? true : false, new { @id = "LVNSVisit_POCChangesY", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_POCChangesY">Yes</label>
                            <%= Html.RadioButton("LVNSVisit_POCChanges", "0", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "0" ? true : false, new { @id = "LVNSVisit_POCChangesN", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_POCChangesN">No</label>
                        </div>
                    </div>
                    <div class="row">
                        <label class="fl">7. Patient/CG satisfied with care and services provided by LVN/LPN</label>
                        <div class="fr">
                            <%= Html.RadioButton("LVNSVisit_IsServicesSatisfactory", "1", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "1" ? true : false, new { @id = "LVNSVisit_IsServicesSatisfactoryY", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_IsServicesSatisfactoryY">Yes</label>
                            <%= Html.RadioButton("LVNSVisit_IsServicesSatisfactory", "0", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "0" ? true : false, new { @id = "LVNSVisit_IsServicesSatisfactoryN", @class = "radio" })%>
                            <label class="inline-radio" for="LVNSVisit_IsServicesSatisfactoryN">No</label>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <div>8. Additional Comments/Findings</div>
                        <div><%= Html.TextArea("LVNSVisit_AdditionalComments", data.ContainsKey("AdditionalComments") ? data["AdditionalComments"].Answer : "", new { @class = "tallest" })%></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Electronic Signature</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label for="LVNSVisit_Signature" class="float-left">Signature</label>
                        <div class="float-right"><%= Html.Password("LVNSVisit_Clinician", "", new { @class = "complete-required", @id = "LVNSVisit_Clinician" })%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label for="LVNSVisit_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="date" class="complete-required" name="LVNSVisit_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : string.Empty %>" id="LVNSVisit_SignatureDate" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="complete">Complete</a></li>
            <li><a class="close">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>