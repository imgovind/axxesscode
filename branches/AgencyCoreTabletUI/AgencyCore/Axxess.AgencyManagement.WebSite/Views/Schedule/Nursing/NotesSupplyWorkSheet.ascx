﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientVisitNote>" %>
<% var noteType = Model.NoteType; %>
<div class="medication medicationbmargin">
    <span class="bigtext">New Supplies</span>
    <% var actionCommand = string.Format("<div class=\"buttons\"><ul><li><a href=\"javascript:void(0);\" onclick=\"Schedule.AddSupply($(this),'{0}','{1}','{2}','{3}','<#=Id#>');\">Add</a></li></ul></div>", Model.EpisodeId, Model.PatientId, Model.Id, noteType); %>
    <% var input = string.Format("<input type=\"text\" class=\"quantity fixedinputWidth\">",string.Empty); %>
    <% var date = string.Format("<input type=\"text\" class=\"date\" value='{0}'/>",string.Empty); %>
    <% var suppliesGrid = string.Format("{0}_SupplyFilterGrid", noteType); %>
    <%= Html.Telerik().Grid<Supply>()
        .Name(suppliesGrid)
        .ToolBar(commands => commands.Insert().ButtonType(GridButtonType.ImageAndText).HtmlAttributes(new { id = "", style = "margin-left:0" }))
        .Columns(columns => {
            columns.Bound(s => s.Description);
            columns.Bound(s => s.Code).Title("Code").Width(55);
            columns.Bound(e => e.Quantity).ClientTemplate(input).Title("Quantity").Width(85);
            columns.Bound(e => e.Date).ClientTemplate(date).Title("Date").Width(85);
            columns.Bound(e => e.Id).ClientTemplate(actionCommand).Title("Action").Width(100);
        }).DataBinding(dataBinding => {
            dataBinding.Ajax().Select("SuppliesGrid", "LookUp", new { q = string.Empty, limit = 0, type = string.Empty });
        }).Footer(false)
    %>
</div>
<div class="clear">
</div>
<div class="medication">
    <% var currentSuppliesGrid = string.Format("{0}_SupplyGrid", noteType); %>
    <%= Html.Telerik().Grid<Supply>().Name(currentSuppliesGrid).DataKeys(keys => { keys.Add(M => M.UniqueIdentifier).RouteKey("UniqueIdentifier"); })
        .ToolBar(commnds => commnds.Custom()).DataBinding(dataBinding => {
            dataBinding.Ajax()
                 .Select("GetNoteSupply", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id })
                 .Update("EditNoteSupply", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id })
                 .Delete("DeleteNoteSupply", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, eventId = Model.Id });
        }).Columns(columns => {
            columns.Bound(s => s.Description).ReadOnly();
            columns.Bound(s => s.Code).Title("Code").Width(55).ReadOnly();
            columns.Bound(s => s.Quantity).Title("Quantity").Width(85);
            columns.Bound(e => e.Date).Title("Date").Format("{0:MM/dd/yyyy}").ReadOnly().Width(85);
            columns.Command(commands => {
                commands.Edit();
                commands.Delete();
            }).Width(180).Title("Commands");
        }).Editable(editing => editing.Mode(GridEditMode.InLine)).ClientEvents(events => events.OnLoad("Schedule.SuppliesLoad")).Sortable().Footer(false)
    %>
</div>
<% var con = string.Format("<label class='float-left' >Enter the Supply Name: </label><div class='float-left'><input id='{0}_GenericSupplyDescription' onfocus=Oasis.SupplyDescription('{0}'); /> </div> <label class='float-left'>  or  Enter the Supply Code: </label> <div class='float-left'> <input id='{0}_GenericSupplyCode' onfocus=Oasis.SupplyCode('{0}'); /> </div>", noteType); %>
<% = string.Format("<script type='text/javascript'>  $(\"#{0}_SupplyFilterGrid .t-grid-toolbar\").html(\"{1}\"); $(\" .t-grid\").css({2});</script>", noteType, con, "{'min-height': '5px'}")%>
<script type="text/javascript">
    $(".t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '60px !important', 'bottom': '0' });
</script>

