﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Discharge Summary<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body></body><%
string[] patientCondition = data != null && data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null;
string[] serviceProvided = data != null && data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null;
string[] dischargeInstructionsGivenTo = data != null && data.ContainsKey("DischargeInstructionsGivenTo") && data["DischargeInstructionsGivenTo"].Answer != "" ? data["DischargeInstructionsGivenTo"].Answer.Split(',') : null;
string[] differentTasks = data != null && data.ContainsKey("DifferentTasks") && data["DifferentTasks"].Answer != "" ? data["DifferentTasks"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
    .Add("jquery-1.7.1.min.js")
    .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
    .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
).OnDocumentReady(() => { %>
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3EDischarge Summary%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3ECompletion Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("CompletedDate") && data["CompletedDate"].Answer.IsNotNullOrEmpty() ? data["CompletedDate"].Answer.ToDateTime().ToString("MM/dd/yyy").Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EDischarge Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("DischargeDate") && data["DischargeDate"].Answer.IsNotNullOrEmpty() ? data["DischargeDate"].Answer.ToDateTime().ToString("MM/dd/yyy").Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.StartDate.IsValid() && Model.EndDate.IsValid()? string.Format(" {0} &#8211; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan class=%22dual%22%3E" +
        "<%= Model.PhysicianDisplayName.Clean().ToTitleCase()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPatient Notified of Discharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null ? (data.ContainsKey("IsNotificationDC") && data["IsNotificationDC"].Answer == "1" ? "Yes" + (data.ContainsKey("NotificationDate") ? " " + data["NotificationDate"].Answer : "") + (data.ContainsKey("NotificationDateOther") ? " " + data["NotificationDateOther"].Answer : "") : "No") : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EReason for D/C:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("ReasonForDC") ? data["ReasonForDC"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3EDischarge Summary%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "";
    printview.addsection(
        printview.col(4,
            printview.checkbox("Stable",<%= patientCondition != null && patientCondition.Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Improved",<%= patientCondition != null && patientCondition.Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Unchanged",<%= patientCondition != null && patientCondition.Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Unstable",<%= patientCondition != null && patientCondition.Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Declined",<%= patientCondition != null && patientCondition.Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Goals Met",<%= patientCondition != null && patientCondition.Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Goals Not Met",<%= patientCondition != null && patientCondition.Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Goals Partially Met",<%= patientCondition != null && patientCondition.Contains("8") ? "true" : "false"%>)),
        "Patient Condition and Outcomes");
    printview.addsection(
        printview.col(4,
            printview.checkbox("SN",<%= serviceProvided != null && serviceProvided.Contains("1") ? "true" : "false"%>) +
            printview.checkbox("PT",<%= serviceProvided != null && serviceProvided.Contains("2") ? "true" : "false"%>) +
            printview.checkbox("OT",<%= serviceProvided != null && serviceProvided.Contains("3") ? "true" : "false"%>) +
            printview.checkbox("ST",<%= serviceProvided != null && serviceProvided.Contains("4") ? "true" : "false"%>) +
            printview.checkbox("MSW",<%= serviceProvided != null && serviceProvided.Contains("5") ? "true" : "false"%>) +
            printview.checkbox("HHA",<%= serviceProvided != null && serviceProvided.Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Other",<%= serviceProvided != null && serviceProvided.Contains("7") ? "true" : "false"%>) +
            printview.span("<%= data != null && data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer.Clean() : string.Empty %>",0,1) ),
        "Service(s) Provided");
    printview.addsection(
        printview.span("<%= data != null && data.ContainsKey("CareSummary") ? data["CareSummary"].Answer.Clean() : string.Empty %>",false,10),
        "Care Summary: (Care Given, Progress, Regress Including Therapies)");
    printview.addsection(
        printview.span("<%= data != null && data.ContainsKey("ConditionOfDischarge") ? data["ConditionOfDischarge"].Answer.Clean() : string.Empty %>",0,10),
        "Condition of discharge (Include VS, BS, Functional and Overall Status)");
    printview.addsection(
        printview.span("Discharge Disposition: Where is the patient after discharge from your agency?",true) +
        printview.checkbox("1 &#8211; Patient remained in the community (without formal assistive services)",<%= data != null && data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "01" ? "true" : "false" %>) +
        printview.checkbox("2 &#8211; Patient remained in the community (with formal assistive services)",<%= data != null && data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "02" ? "true" : "false" %>) +
        printview.checkbox("3 &#8211; Patient transferred to a non-institutional hospice",<%= data != null && data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "03" ? "true" : "false" %>) +
        printview.checkbox("4 &#8211; Unknown because patient moved to a geographic location not served by this agency",<%= data != null && data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "04" ? "true" : "false" %>) +
        printview.checkbox("UK &#8211; Other unknown",<%= data != null && data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "UK" ? "true" : "false" %>));
    printview.addsection(
        printview.span("Discharge Instructions Given To:",true) +
        printview.col(5,
            printview.checkbox("Patient",<%= dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("1") ? "true" : "false" %>) +
            printview.checkbox("Caregiver",<%= dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("2") ? "true" : "false" %>) +
            printview.checkbox("N/A",<%= dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("3") ? "true" : "false" %>) +
            printview.checkbox("Other",<%= dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("4") ? "true" : "false" %>) +
            printview.span("<%= data != null && data.ContainsKey("DischargeInstructionsGivenToOther") ? ": " + data["DischargeInstructionsGivenToOther"].Answer.Clean() : "" %>")));
    printview.addsection(
        printview.col(2,
            printview.span("Verbalized understanding",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "0" ? "true" : "false"%>))) +
        printview.checkbox("All services notified and discontinued",<%= differentTasks != null && differentTasks.Contains("1") ? "true" : "false" %>) +
        printview.checkbox("Order and summary completed",<%= differentTasks != null && differentTasks.Contains("2") ? "true" : "false" %>) +
        printview.checkbox("Information provided to patient for continuing needs",<%= differentTasks != null && differentTasks.Contains("3") ? "true" : "false" %>) +
        printview.checkbox("Physician notified",<%= differentTasks != null && differentTasks.Contains("4") ? "true" : "false" %>));
    printview.addsection(
        printview.col(2,
            printview.span("Clinician Signature:",true) +
            printview.span("Date:",true) +
            printview.span("<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : string.Empty %>",0,1) +
            printview.span("<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : string.Empty %>",0,1))); <%
}).Render(); %>
</html>