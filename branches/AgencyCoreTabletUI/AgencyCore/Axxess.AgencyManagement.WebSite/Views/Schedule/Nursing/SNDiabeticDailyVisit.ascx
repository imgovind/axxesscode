﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Diabetic Daily Visit Nursing Note | <%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase().Clean() : "" %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId)%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId)%>
    <%= Html.Hidden("Type", Model.Type)%>
    <%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Diabetic Daily Visit Nursing Note</th>
            </tr>
            <tr>
                <td colspan="2"><span class="bigtext">Patient Name: <%= Model.Patient.DisplayName %></span></td>
                <td><span class="bigtext">MR: <%= Model.Patient.PatientIdNumber %></span></td>
                <td>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl %></li>
                        </ul>
                    </div>
    <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label for="<%= Model.Type %>_VisitDate" class="float-left">Visit Date:</label>
                    <%  var scheduledPrn = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "" },
                        new SelectListItem { Text = "Scheduled", Value = "Scheduled" },
                        new SelectListItem { Text = "PRN", Value = "PRN" }
                    }, "Value", "Text", data.AnswerOrDefault("ScheduledPrn", "0")); %>
                    <div class="float-right">
                        <input type="date" name="<%= Model.Type %>_VisitDate" id="<%= Model.Type %>_VisitDate" value="<%= Model.VisitDate %>" />
                        <%= Html.DropDownList(Model.Type + "_ScheduledPrn", scheduledPrn, new { @id = Model.Type + "_ScheduledPrn", @class = "oe" })%>
                    </div>
                </td>
                <td colspan="2">
                    <div class="half">
                        <label for="" class="float-left">Time In:</label>
                        <div class="float-right"><input type="time" name="<%= Model.Type %>_TimeIn" id="<%= Model.Type %>_TimeIn" value="<%= data.AnswerOrEmptyString("TimeIn") %>" class="oe" /></div>
                    </div>
                    <div class="half">
                        <label for="" class="float-left">Time Out:</label>
                        <div class="float-right"><input type="time" name="<%= Model.Type %>_TimeOut" id="<%= Model.Type %>_TimeOut" value="<%= data.AnswerOrEmptyString("TimeOut") %>" class="oe" /></div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>Vital Signs</th>
                <th>Cardiovascular</th>
                <th>Respiratory</th>
                <th>Diabetes</th>
            </tr>
            <tr>
                <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/VitalSigns.ascx", Model); %></td>
                <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Cardiovascular.ascx", Model); %></td>
                <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Respiratory.ascx", Model); %></td>
                <td rowspan="3"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/DiabeticCare.ascx", Model); %></td>
            </tr>
            <tr>
                <th colspan="3">Comments/Teaching</th>
            </tr>
            <tr>
                <td colspan="3"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Narrative.ascx", Model); %></td>
            </tr>
            <tr>
                <th colspan="4">HHA PRN Supervisory Visit</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label class="float-left">HHA Present:</label>
                        <div class="float-right">
                            <ul class="checkgroup inline">
                                <li>
                                    <div class="option">
                                        <%= Html.RadioButton(Model.Type + "_HhaPresent", "1", data.AnswerOrEmptyString("HhaPresent").Equals("1"), new { @id = Model.Type + "_HhaPresent1" })%>
                                        <label for="<%= Model.Type %>_HhaPresent1">Yes</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="option">
                                        <%= Html.RadioButton(Model.Type + "_HhaPresent", "0", data.AnswerOrEmptyString("HhaPresent").Equals("0"), new { @id = Model.Type + "_HhaPresent0" })%>
                                        <label for="<%= Model.Type %>_HhaPresent0">No</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="third">
                        <label class="float-left">Aide following Care Plan:</label>
                        <div class="float-right">
                            <ul class="checkgroup inline">
                                <li>
                                    <div class="option">
                                        <%= Html.RadioButton(Model.Type + "_AideFollowCarePlan", "1", data.AnswerOrEmptyString("AideFollowCarePlan").Equals("1"), new { @id = Model.Type + "_AideFollowCarePlan1" })%>
                                        <label for="<%= Model.Type %>_AideFollowCarePlan1">Yes</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="option">
                                        <%= Html.RadioButton(Model.Type + "_AideFollowCarePlan", "0", data.AnswerOrEmptyString("AideFollowCarePlan").Equals("0"), new { @id = Model.Type + "_AideFollowCarePlan0" })%>
                                        <label for="<%= Model.Type %>_AideFollowCarePlan0">No</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="align-left">
                        <label class="strong">Comments:</label>
                        <div class="align-center"><%= Html.TextArea(Model.Type + "_GenericComments", data.AnswerOrEmptyString("GenericComments"), 3, 20, new { @class = "fill", @id = Model.Type + "_GenericComments" })%></div>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="DDVisit_Signature" class="float-left">Signature</label>
                        <div class="float-right"><%= Html.Password(Model.Type + "_Clinician", string.Empty, new { @id = Model.Type + "_Clinician" })%></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="<%= Model.Type %>_SignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input type="date" name="<%= Model.Type %>_SignatureDate" value="<%= data.AnswerOrEmptyString("SignatureDate") %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="<%= Model.Type %>_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="DDVisitRemove(); snDiabeticDailyVisit.Submit($(this));">Save</a></li>
            <li><a href="javascript:void(0);" onclick="DDVisitAdd(); snDiabeticDailyVisit.Submit($(this));">Complete</a></li>
            <li><a class="close">Exit</a></li>
        </ul>
    </div>
<%  } %>  
</div>