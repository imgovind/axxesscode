﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Skilled Nursing Visit Note | <%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase().Clean() : "" %></span>
<div class="wrapper main note">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId, new { @id = Model.Type + "_PatientId" })%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId, new { @id = Model.Type + "_EpisodeId" })%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId, new { @id = Model.Type + "_Id" })%>
    <%= Html.Hidden("Type", Model.Type)%>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Skilled Nurse Visit Note</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label class="fl strong">Patient Name</label>
                        <div class="fr"><%= Model.Patient.DisplayName %></div>
                    </div>
                    <div class="row">
                        <label class="fl strong">MR#</label>
                        <div class="fr"><%= Model.Patient.PatientIdNumber %></div>
                    </div>
                    <div class="row">
                        <label class="float-left">Visit Date</label>
                        <div class="fr"><input type="date" name="<%= Model.Type %>_VisitDate" value="<%= Model.VisitDate.IsValidDate() ? Model.VisitDate : string.Empty %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%= Model.Type %>_VisitDate" /></div>
                    </div>
                    <div class="row">
                        <label class="float-left">Time In</label>
                        <div class="fr"><input type="time" name ="<%= Model.Type %>_TimeIn" value="<%= data.AnswerOrEmptyString("TimeIn") %>" id="<%= Model.Type %>_TimeIn" /></div>
                    </div>
                    <div class="row">
                        <label class="float-left">Time Out</label>
                        <div class="fr"><input type="time" name="<%= Model.Type %>_TimeOut" value="<%= data.AnswerOrEmptyString("TimeOut") %>" id="<%= Model.Type %>_TimeOut" /%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label class="float-left">Discipline Task</label>
                        <div class="fr"><%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, new { @id = Model.Type + "_DisciplineTask", @class = "required notzero" })%></div>
                    </div>
                    <div class="row">
                        <label class="fl strong">Primary Diagnosis</label>
                        <div class="fr">
                            <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                            <%= data.AnswerOrEmptyString("PrimaryDiagnosis") %>
                            <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
    <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                                <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
    <%  } %>
                        </div>
                    </div>
                    <div class="row">
                        <label class="fl strong">Secondary Diagnosis</label>
                        <div class="fr">
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <%= data.AnswerOrEmptyString("PrimaryDiagnosis1")%>
                        <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
    <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                        <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
    <%  } %>
                        </div>
                    </div>
    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
                    <div class="row">
                        <label class="float-left">Previous Notes:</label>
                        <div class="fr"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" })%></div>
                    </div>
    <%  } %>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="row">
                        <div class="buttons">
                            <ul>
                                <li><%-- Model.CarePlanOrEvalUrl --%></li>
                            </ul>
                        </div>
                    </div>
    <%  } %>
                        <%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
                        <li style="float: right;"><a href="javascript:void(0);" onclick="UserInterface.ShowMultipleDayScheduleModal('<%= Model.EpisodeId %>', '<%= Model.PatientId %>');" title="Schedule Supervisory Visit">Schedule Supervisory Visit</a></li>
                        <%  } %>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("~/Views/Schedule/Nursing/SNVisitContent.ascx", Model); %></div>
    <ol>
        <li>
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <div class="buttons">
                            <ul>
                        <%  if (Model.IsSupplyExist) { %>
                                <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadNoteSupplyWorkSheet('{0}','{1}','{2}');\" >Edit Supply Worksheet</a>", Model.EpisodeId, Model.PatientId, Model.EventId)%></li>
                        <%  } else { %>
                                <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadNoteSupplyWorkSheet('{0}','{1}','{2}');\" >Add Supply Worksheet</a>", Model.EpisodeId, Model.PatientId, Model.EventId)%></li>
                        <%  } %>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="SNVisit_Button" />
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Electronic Signature</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label class="float-left">Clinician Signature</label>
                        <div class="fr"><%= Html.Password(Model.Type + "_Clinician", string.Empty, new { @id = Model.Type + "_Clinician" }) %></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label class="float-left">Date</label>
                        <div class="fr"><input type="date" name="<%= Model.Type %>_SignatureDate" value="<%= data.AnswerOrEmptyString("SignatureDate") %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SNVisitRemove();snVisit.Submit($(this));">Save</a></li>
            <li><a href="javascript:void(0);" onclick="SNVisitAdd(); snVisit.Submit($(this));">Complete</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="SNVisitRemove(); snVisit.Submit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="SNVisitRemove(); snVisit.Submit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0);" onclick="SNVisitRemove(); UserInterface.CloseWindow('snVisit');">Exit</a></li>
            <li><a class="save">Save</a></li>
            <li><a class="complete">Complete</a></li>
            <li><a class="close">Exit</a></li>
        </ul>
    </div>
</div>
<%  } %>