﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">60 Day Summary/Case Conference | <%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase().Clean() : "" %></span>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "SixtyDaySummaryForm" })) { %>
    <%= Html.Hidden("SixtyDaySummary_PatientId", Model.PatientId)%>
    <%= Html.Hidden("SixtyDaySummary_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("SixtyDaySummary_EventId", Model.EventId)%>
    <%= Html.Hidden("DisciplineTask", "85", new { @id = "SixtyDaySummary_DisciplineTask" })%>
    <%= Html.Hidden("Type", "SixtyDaySummary", new { @id = "SixtyDaySummary_Type" })%>
    <%= Html.Hidden("button", string.Empty, new { @id = "SixtyDaySummary_button" })%>
    <ul>
        <li>
            <div class="wrapper">
                <h3>60 Day Summary/Case Conference</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label class="fl strong">Patient Name</label>
                        <div class="fr"><%= Model.Patient.DisplayName %></div>
                    </div>
                    <div class="row">
                        <label class="fl strong">MR#</label>
                        <div class="fr"><%= Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty %></div>
                    </div>
                    <div class="row">
                        <label class="fl strong">Episode/Period</label>
                        <div class="fr"><%= Model != null ? Model.StartDate.ToShortDateString() + "&#8211;" + Model.EndDate.ToShortDateString() : string.Empty %></div>
                    </div>
                    <div class="row">
                        <label for="SixtyDaySummary_Physician" class="float-left">Physician</label>
                        <div class="fr"><%= Html.TextBox("SixtyDaySummary_Physician", data.ContainsKey("Physician") ? data["Physician"].Answer : string.Empty, new { @id = "SixtyDaySummary_Physician", @class = "physicians" })%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label class="fl strong">Primary Diagnosis</label>
                        <div class="fr"><%= data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : string.Empty %></div>
                    </div>
                    <div class="row">
                        <label class="fl strong">Secondary Diagnosis</label>
                        <div class="fr"><%= data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : string.Empty%></div>
                    </div>
                    <div class="row">
                        <label class="float-left">DNR:</label>
                        <div class="fr">
                            <%= Html.RadioButton("SixtyDaySummary_DNR", "1", data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? true : false, new { @id = "SixtyDaySummary_DNR1", @class = "radio" })%>
                            <label for="SixtyDaySummary_DNR1" class="inline-radio">Yes</label>
                            <%= Html.RadioButton("SixtyDaySummary_DNR", "0", data.ContainsKey("DNR") && data["DNR"].Answer == "0" ? true : false, new { @id = "SixtyDaySummary_DNR2", @class = "radio" })%>
                            <label for="SixtyDaySummary_DNR2" class="inline-radio">No</label>
                        </div>
                        <div class="clear"></div>
                        <div class="fr">
                            <%  var patientReceived = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "5 day", Value = "1" },
                                    new SelectListItem { Text = "2 day", Value = "2" },
                                    new SelectListItem { Text = "Other", Value = "3" }
                                }, "Value", "Text", data.ContainsKey("NotificationDate") ? data["NotificationDate"].Answer : "0"); %>
                            <%= Html.DropDownList("SixtyDaySummary_NotificationDate", patientReceived, new { @id = "SixtyDaySummary_NotificationDate" }) %>
                        </div>
                        <div class="clear"></div>
                        <div class="fr">
                            <%= Html.TextBox("SixtyDaySummary_NotificationDateOther", data.ContainsKey("NotificationDateOther") ? data["NotificationDateOther"].Answer : "", new { @id = "SixtyDaySummary_NotificationDateOther" })%>
                        </div>
                    </div>
               </div>
                <div class="clear"></div>                
            </div>
        </li>
    </ol>
    <ul>
        <li class="half">
            <div class="wrapper">
                <h3>Homebound Status</h3>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <h3>Vital Sign Ranges</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <%  string[] homeboundStatus = data.ContainsKey("HomeboundStatus") && data["HomeboundStatus"].Answer != "" ? data["HomeboundStatus"].Answer.Split(',') : null; %>
                        <input name="SixtyDaySummary_HomeboundStatus" value=" " type="hidden" />
                        <div class="wide checkgroup">
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusLeave' name='SixtyDaySummary_HomeboundStatus' value='2' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("2") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_HomeboundStatusLeave" class="radio normal">Exhibits considerable &#38; taxing effort to leave home</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusAssistRequired' name='SixtyDaySummary_HomeboundStatus' value='3' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("3") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_HomeboundStatusAssistRequired" class="radio normal">Requires the assistance of another to get up and move safely</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusDyspnea' name='SixtyDaySummary_HomeboundStatus' value='4' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("4") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_HomeboundStatusDyspnea" class="radio normal">Severe Dyspnea</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusUnableToLeave' name='SixtyDaySummary_HomeboundStatus' value='5' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("5") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_HomeboundStatusUnableToLeave" class="radio normal">Unable to safely leave home unassisted</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusUnsafeToLeave' name='SixtyDaySummary_HomeboundStatus' value='6' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("6") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_HomeboundStatusUnsafeToLeave" class="radio normal">Unsafe to leave home due to cognitive or psychiatric impairments</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusMedReason' name='SixtyDaySummary_HomeboundStatus' value='7' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("7") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_HomeboundStatusMedReason" class="radio normal">Unable to leave home due to medical restriction(s)</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_HomeboundStatusOtherCheck' name='SixtyDaySummary_HomeboundStatus' value='8' class='radio float-left' type='checkbox' {0} />", homeboundStatus != null && homeboundStatus.Contains("8") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_HomeboundStatusOtherCheck" class="radio normal">Other</label>
                                <%= Html.TextBox("SixtyDaySummary_HomeboundStatusOther", data.ContainsKey("HomeboundStatusOther") ? data["HomeboundStatusOther"].Answer : "", new { @id = "SixtyDaySummary_HomeboundStatusOther", @class = "float-left" })%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <label class="float-left">Temperature</label>
                        <div class="fr">
                            <label for="SixtyDaySummary_VitalSignTempMin">greater than (&#62;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignTempMin", data.ContainsKey("VitalSignTempMin") ? data["VitalSignTempMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignTempMin", @class = "vitals" })%>
                            <label for="SixtyDaySummary_VitalSignTempMax">or less than (&#60;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignTempMax", data.ContainsKey("VitalSignTempMax") ? data["VitalSignTempMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignTempMax", @class = "vitals" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label class="float-left">Pulse</label>
                        <div class="fr">
                            <label for="<%= Model.TypeName %>_GenericTempGreaterThan">greater than (&#62;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignHRMin", data.ContainsKey("VitalSignHRMin") ? data["VitalSignHRMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignHRMin", @class = "vitals" })%>
                            <label for="<%= Model.TypeName %>_GenericTempLessThan">or less than (&#60;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignHRMax", data.ContainsKey("VitalSignHRMax") ? data["VitalSignHRMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignHRMax", @class = "vitals" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label class="float-left">Respirations</label>
                        <div class="fr">
                            <label for="<%= Model.TypeName %>_GenericTempGreaterThan">greater than (&#62;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignRespMin", data.ContainsKey("VitalSignRespMin") ? data["VitalSignRespMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignRespMin", @class = "vitals" })%>
                            <label for="<%= Model.TypeName %>_GenericTempLessThan">or less than (&#60;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignRespMax", data.ContainsKey("VitalSignRespMax") ? data["VitalSignRespMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignRespMax", @class = "vitals" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label class="float-left">Systolic BP</label>
                        <div class="fr">
                            <label for="<%= Model.TypeName %>_GenericTempGreaterThan">greater than (&#62;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignBPMin", data.ContainsKey("VitalSignBPMin") ? data["VitalSignBPMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBPMin", @class = "vitals" })%>
                            <label for="<%= Model.TypeName %>_GenericTempLessThan">or less than (&#60;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignBPMax", data.ContainsKey("VitalSignBPMax") ? data["VitalSignBPMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBPMax", @class = "vitals" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label class="float-left">Diastolic BP</label>
                        <div class="fr">
                            <label for="<%= Model.TypeName %>_GenericTempGreaterThan">greater than (&#62;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignBPDiaMin", data.ContainsKey("VitalSignBPDiaMin") ? data["VitalSignBPDiaMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBPDiaMin", @class = "vitals" })%>
                            <label for="<%= Model.TypeName %>_GenericTempLessThan">or less than (&#60;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignBPDiaMax", data.ContainsKey("VitalSignBPDiaMax") ? data["VitalSignBPDiaMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBPDiaMax", @class = "vitals" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label class="float-left">Blood Glucose</label>
                        <div class="fr">
                            <label for="<%= Model.TypeName %>_GenericTempGreaterThan">greater than (&#62;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignBGMin", data.ContainsKey("VitalSignBGMin") ? data["VitalSignBGMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBGMin", @class = "vitals" })%>
                            <label for="<%= Model.TypeName %>_GenericTempLessThan">or less than (&#60;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignBGMax", data.ContainsKey("VitalSignBGMax") ? data["VitalSignBGMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignBGMax", @class = "vitals" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label class="float-left">Weight</label>
                        <div class="fr">
                            <label for="<%= Model.TypeName %>_GenericTempGreaterThan">greater than (&#62;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignWeightMin", data.ContainsKey("VitalSignWeightMin") ? data["VitalSignWeightMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignWeightMin", @class = "vitals" })%>
                            <label for="<%= Model.TypeName %>_GenericTempLessThan">or less than (&#60;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignWeightMax", data.ContainsKey("VitalSignWeightMax") ? data["VitalSignWeightMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignWeightMax", @class = "vitals" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label class="float-left">Pain</label>
                        <div class="fr">
                            <label for="<%= Model.TypeName %>_GenericTempGreaterThan">greater than (&#62;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignPainMin", data.ContainsKey("VitalSignPainMin") ? data["VitalSignPainMin"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignPainMin", @class = "vitals" })%>
                            <label for="<%= Model.TypeName %>_GenericTempLessThan">or less than (&#60;)</label>
                            <%= Html.TextBox("SixtyDaySummary_VitalSignPainMax", data.ContainsKey("VitalSignPainMax") ? data["VitalSignPainMax"].Answer : string.Empty, new { @id = "SixtyDaySummary_VitalSignPainMax", @class = "vitals" })%>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ul>
        <li class="half">
            <div class="wrapper">
                <h3>Patient Condition</h3>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <h3>Service(s) Provided</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <% string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %>
                        <input name="SixtyDaySummary_PatientCondition" value=" " type="hidden" />
                        <div class="narrow checkgroup">
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_PatientConditionStable' name='SixtyDaySummary_PatientCondition' value='1' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("1") ? "checked='checked'" : "" )%>
                                <label for="SixtyDaySummary_PatientConditionStable" class="radio">Stable</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_PatientConditionImproved' name='SixtyDaySummary_PatientCondition' value='2' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("2") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_PatientConditionImproved" class="radio">Improved</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_PatientConditionUnchanged' name='SixtyDaySummary_PatientCondition' value='3' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("3") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_PatientConditionUnchanged" class="radio">Unchanged</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_PatientConditionUnstable' name='SixtyDaySummary_PatientCondition' value='4' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("4") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_PatientConditionUnstable" class="radio">Unstable</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_PatientConditionDeclined' name='SixtyDaySummary_PatientCondition' value='5' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("5") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_PatientConditionDeclined" class="radio">Declined</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <% string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %>
                        <input name="SixtyDaySummary_ServiceProvided" value=" " type="hidden" />
                        <div class="narrow checkgroup">
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedSN' name='SixtyDaySummary_ServiceProvided' value='1' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("1") ? "checked='checked'" : "" )%>
                                <label for="SixtyDaySummary_ServiceProvidedSN" class="radio">SN</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedPT' name='SixtyDaySummary_ServiceProvided' value='2' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("2") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_ServiceProvidedPT" class="radio">PT</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedOT' name='SixtyDaySummary_ServiceProvided' value='3' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("3") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_ServiceProvidedOT" class="radio">OT</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedST' name='SixtyDaySummary_ServiceProvided' value='4' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("4") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_ServiceProvidedST" class="radio">ST</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedMSW' name='SixtyDaySummary_ServiceProvided' value='5' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("5") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_ServiceProvidedMSW" class="radio">MSW</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedHHA' name='SixtyDaySummary_ServiceProvided' value='6' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("6") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_ServiceProvidedHHA" class="radio">HHA</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_ServiceProvidedOther' name='SixtyDaySummary_ServiceProvided' value='7' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("7") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_ServiceProvidedOther" class="radio">Other</label>
                                <%= Html.TextBox("SixtyDaySummary_ServiceProvidedOtherValue", data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : string.Empty, new { @id = "SixtyDaySummary_ServiceProvidedOtherValue", @class = "fill" })%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ul>
        <li class="third">
            <div class="wrapper">
                <h3>Summary of Care Provided</h3>
            </div>
        </li>
        <li class="third">
            <div class="wrapper">
                <h3>Patient&#8217;s Current Condition</h3>
            </div>
        </li>
        <li class="third">
            <div class="wrapper">
                <h3>Goals</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li class="third">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <%= Html.Templates("SixtyDaySummary_SummaryOfCareProvidedTemplates", new { @class = "templates", @template = "#SixtyDaySummary_SummaryOfCareProvided" })%>
                        <%= Html.TextArea("SixtyDaySummary_SummaryOfCareProvided", data.ContainsKey("SummaryOfCareProvided") ? data["SummaryOfCareProvided"].Answer : string.Empty, new { @class = "tallest", @id = "SixtyDaySummary_SummaryOfCareProvided" })%>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li class="third">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <%= Html.Templates("SixtyDaySummary_PatientCurrentConditionTemplates", new { @class = "templates", @template = "#SixtyDaySummary_PatientCurrentCondition" })%>
                        <%= Html.TextArea("SixtyDaySummary_PatientCurrentCondition", data.ContainsKey("PatientCurrentCondition") ? data["PatientCurrentCondition"].Answer : string.Empty, new { @class = "tallest", @id = "SixtyDaySummary_PatientCurrentCondition" })%>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li class="third">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <%= Html.Templates("SixtyDaySummary_GoalsTemplates", new { @class = "templates", @template = "#SixtyDaySummary_Goals" })%>
                        <%= Html.TextArea("SixtyDaySummary_Goals", data.ContainsKey("Goals") ? data["Goals"].Answer : string.Empty, new { @class = "tallest", @id = "SixtyDaySummary_Goals" })%>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ul>
        <li class="half">
            <div class="wrapper">
                <h3>Recomended Services</h3>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <h3>Notifications</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <%  string[] recommendedService = data.ContainsKey("RecommendedService") && data["RecommendedService"].Answer != "" ? data["RecommendedService"].Answer.Split(',') : null; %>
                        <input name="SixtyDaySummary_RecommendedService" value="" type="hidden" />
                        <div class="narrow checkgroup">
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_RecommendedServiceSN' name='SixtyDaySummary_RecommendedService' value='1' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("1") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_RecommendedServiceSN" class="radio">SN</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_RecommendedServicePT' name='SixtyDaySummary_RecommendedService' value='2' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("2") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_RecommendedServicePT" class="radio">PT</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_RecommendedServiceOT' name='SixtyDaySummary_RecommendedService' value='3' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("3") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_RecommendedServiceOT" class="radio">OT</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_RecommendedServiceST' name='SixtyDaySummary_RecommendedService' value='4' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("4") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_RecommendedServiceST" class="radio">ST</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_RecommendedServiceMSW' name='SixtyDaySummary_RecommendedService' value='5' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("5") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_RecommendedServiceMSW" class="radio">MSW</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_RecommendedServiceHHA' name='SixtyDaySummary_RecommendedService' value='6' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("6") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_RecommendedServiceHHA" class="radio">HHA</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_RecommendedServiceOther' name='SixtyDaySummary_RecommendedService' value='7' class='radio float-left' type='checkbox' {0} />", recommendedService != null && recommendedService.Contains("7") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_RecommendedServiceOther" class="radio">Other</label>
                                <%= Html.TextBox("SixtyDaySummary_RecommendedServiceOtherValue", data.ContainsKey("RecommendedServiceOtherValue") ? data["RecommendedServiceOtherValue"].Answer:string.Empty, new { @id = "SixtyDaySummary_RecommendedServiceOtherValue", @class = "fill" })%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <div class="wide checkgroup">
                            <div class="option">
                                <%= string.Format("<input id='SixtyDaySummary_SummarySentToPhysician' name='SixtyDaySummary_SummarySentToPhysician' value='Yes' class='radio float-left' type='checkbox' {0} />", data.ContainsKey("SummarySentToPhysician") && data["SummarySentToPhysician"] != null && data["SummarySentToPhysician"].Answer.IsNotNullOrEmpty() && data["SummarySentToPhysician"].Answer.IsEqual("Yes") ? "checked='checked'" : "")%>
                                <label for="SixtyDaySummary_SummarySentToPhysician" class="radio">Summary Sent To Physician</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="float-left" for="SixtyDaySummary_SummarySentBy">Sent By:</label>
                        <div class="float-right"><%= Html.Users("SixtyDaySummary_SummarySentBy", data.ContainsKey("SummarySentBy") ? data["SummarySentBy"].Answer : string.Empty, new { @id = "SixtyDaySummary_SummarySentBy", @class = "fill" })%></div>
                    </div>
                    <div class="row">
                        <label for="SixtyDaySummary_SummarySentDate" for="SixtyDaySummary_SummarySentDate">Date Sent:</label>
                        <div class="float-right"><input type="date" name="SixtyDaySummary_SummarySentDate" value="<%= data.ContainsKey("SummarySentDate") && data["SummarySentDate"] != null && data["SummarySentDate"].Answer.IsNotNullOrEmpty() && data["SummarySentDate"].Answer.IsValidDate() ? data["SummarySentDate"].Answer : string.Empty %>" id="SixtyDaySummary_SummarySentDate" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Electronic Signature</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label for="SixtyDaySummary_ClinicianSignature" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password("SixtyDaySummary_Clinician", "", new { @id = "SixtyDaySummary_Clinician", @class = "complete-required" })%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label for="SixtyDaySummary_ClinicianSignatureDate" class="float-left">Date:</label>
                        <div class="float-right"><input class="complete-required" type="date" name="SixtyDaySummary_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() ? data["SignatureDate"].Answer : string.Empty %>" id="SixtyDaySummary_SignatureDate" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label for="SixtyDaySummary_ClinicianSignature2" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password("SixtyDaySummary_Clinician2", "", new { @id = "SixtyDaySummary_Clinician2" })%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label for="SixtyDaySummary_ClinicianSignatureDate2" class="float-left">Date:</label>
                        <div class="float-right"><input type="date" name="SixtyDaySummary_SignatureDate2" value="<%= data.ContainsKey("SignatureDate2") && data["SignatureDate2"].Answer.IsNotNullOrEmpty() && data["SignatureDate2"].Answer.IsValidDate() ? data["SignatureDate2"].Answer : string.Empty %>" id="SixtyDaySummary_SignatureDate2" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label for="SixtyDaySummary_ClinicianSignature3" class="float-left">Clinician:</label>
                        <div class="float-right"><%= Html.Password("SixtyDaySummary_Clinician3", "", new { @id = "SixtyDaySummary_Clinician3" })%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label for="SixtyDaySummary_ClinicianSignatureDate3" class="float-left">Date:</label>
                        <div class="float-right"><input type="date" name="SixtyDaySummary_SignatureDate3" value="<%= data.ContainsKey("SignatureDate3") && data["SignatureDate3"].Answer.IsNotNullOrEmpty() && data["SignatureDate3"].Answer.IsValidDate() ? data["SignatureDate3"].Answer : string.Empty %>" id="SixtyDaySummary_SignatureDate3" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="complete">Complete</a></li>
            <li><a class="close">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>