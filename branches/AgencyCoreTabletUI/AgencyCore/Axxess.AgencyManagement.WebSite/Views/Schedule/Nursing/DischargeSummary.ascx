﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<span class="wintitle">Discharge Summary | <%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase().Clean() : "" %></span>
<%  var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<div class="wrapper main note">
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "dischargeSummaryForm" })) { %>
    <%= Html.Hidden("DischargeSummary_PatientId", Model.PatientId)%>
    <%= Html.Hidden("DischargeSummary_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("DischargeSummary_EventId", Model.EventId)%>
    <%= Html.Hidden("DisciplineTask", "18")%>
    <%= Html.Hidden("Type", "DischargeSummary")%>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Discharge Summary</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label class="fl strong">Patient Name</label>
                        <div class="fr"><%= Model.Patient.DisplayName %></div>
                    </div>
                    <div class="row">
                        <label class="fl strong">MR#</label>
                        <div class="fr"><%= Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty %></div>
                    </div>
                    <div class="row">
                        <label class="fl strong">Visit Date</label>
                        <div class="fr"><input type="date" name="DischargeSummary_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="DischargeSummary_VisitDate" class="required" /></div>
                    </div>
                    <div class="row">
                        <label class="fl strong">Episode/Period</label>
                        <div class="fr"><%= Model != null ? Model.StartDate.ToShortDateString() + "&#8211;" + Model.EndDate.ToShortDateString() : string.Empty %></div>
                    </div>
                    <div class="row">
                        <label for="SixtyDaySummary_Physician" class="float-left">Physician</label>
                        <div class="fr"><%= Html.TextBox("DischargeSummary_Physician", Model.PhysicianId.ToString(), new { @id = "DischargeSummary_Physician", @class = "physicians" })%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label class="fl strong">Discharge Date</label>
                        <div class="fr"><input type="date" name="DischargeSummary_DischargeDate" value="<%= data.ContainsKey("DischargeDate") && data["DischargeDate"].Answer.IsNotNullOrEmpty() && data["DischargeDate"].Answer.IsValidDate() ? data["DischargeDate"].Answer : "" %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="DischargeSummary_DischargeDate" /></div>
                    </div>
                    <div class="row">
                        <label class="fl strong">Patient Notified of Discharge</label>
                        <div class="fr">
                            <%= Html.Hidden("DischargeSummary_IsNotificationDC", " ", new { @id = "" })%>
                            <%= Html.RadioButton("DischargeSummary_IsNotificationDC", "1", data.ContainsKey("IsNotificationDC") && data["IsNotificationDC"].Answer == "1" ? true : false, new { @id = "DischargeSummary_IsNotificationDCY", @class = "radio" })%>
                            <label for="DischargeSummary_IsNotificationDCY" class="inline-radio">Yes</label>
                            <%= Html.RadioButton("DischargeSummary_IsNotificationDC", "0", data.ContainsKey("IsNotificationDC") && data["IsNotificationDC"].Answer == "0" ? true : false, new { @id = "DischargeSummary_IsNotificationDCN", @class = "radio" })%>
                            <label for="DischargeSummary_IsNotificationDCN" class="inline-radio">No</label>
                        </div>
                        <div class="clear"></div>
                        <div class="fr">
                            <%  var patientReceived = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "5 day", Value = "1" },
                                    new SelectListItem { Text = "2 day", Value = "2" },
                                    new SelectListItem { Text = "Other", Value = "3" }
                                }, "Value", "Text", data.ContainsKey("NotificationDate") ? data["NotificationDate"].Answer : "0"); %>
                            <%= Html.DropDownList("DischargeSummary_NotificationDate", patientReceived, new { @id = "DischargeSummary_NotificationDate" }) %>
                            <%= Html.TextBox("DischargeSummary_NotificationDateOther", data.ContainsKey("NotificationDateOther") ? data["NotificationDateOther"].Answer : "", new { @id = "DischargeSummary_NotificationDateOther", @style = "display:none;"}) %>
                        </div>
                    </div>
                    <div class="row">
                        <label class="float-left">Discharge Reason</label>
                        <div class="fr">
                            <%  var reasonForDC = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "Goals Met", Value = "1" },
                                    new SelectListItem { Text = "To Nursing Home", Value = "2" },
                                    new SelectListItem { Text = "Deceased", Value = "3" },
                                    new SelectListItem { Text = "Noncompliant", Value = "4" },
                                    new SelectListItem { Text = "To Hospital", Value = "5" },
                                    new SelectListItem { Text = "Moved from Service Area", Value = "6" },
                                    new SelectListItem { Text = "Refused Care", Value = "7" },
                                    new SelectListItem { Text = "No Longer Homebound", Value = "8" },
                                    new SelectListItem { Text = "Other", Value = "9" }
                                }, "Value", "Text", data.ContainsKey("ReasonForDC") ? data["ReasonForDC"].Answer : "0"); %>
                            <%= Html.DropDownList("DischargeSummary_ReasonForDC", reasonForDC, new { @id = "DischargeSummary_ReasonForDC", @class = "float-right" })%>
                            <%= Html.TextBox("DischargeSummary_ReasonForDCOther",data.ContainsKey("ReasonForDCOther") ? data["ReasonForDCOther"].Answer : "", new { @id = "DischargeSummary_ReasonForDCOther", @style = "display:none;", @class = "float-right" })%>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>                
            </div>
        </li>
    </ol>
    <ul>
        <li class="half">
            <div class="wrapper">
                <h3>Patient Condition and Outcomes</h3>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <h3>Service(s) Provided</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <%  string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %>
                        <input name="DischargeSummary_PatientCondition" value=" " type="hidden" />
                        <div class="narrow checkgroup">
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_PatientConditionStable' name='DischargeSummary_PatientCondition' value='1' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("1") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_PatientConditionStable" class="radio">Stable</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_PatientConditionImproved' name='DischargeSummary_PatientCondition' value='2' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("2") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_PatientConditionImproved" class="radio">Improved</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_PatientConditionUnchanged' name='DischargeSummary_PatientCondition' value='3' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("3") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_PatientConditionUnchanged" class="radio">Unchanged</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_PatientConditionUnstable' name='DischargeSummary_PatientCondition' value='4' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("4") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_PatientConditionUnstable" class="radio">Unstable</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_PatientConditionDeclined' name='DischargeSummary_PatientCondition' value='5' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("5") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_PatientConditionDeclined" class="radio">Declined</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_PatientConditionGoalsMet' name='DischargeSummary_PatientCondition' value='6' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("6") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_PatientConditionGoalsMet" class="radio">Goals Met</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_PatientConditionGoalsPartiallyMet' name='DischargeSummary_PatientCondition' value='7' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("7") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_PatientConditionGoalsPartiallyMet" class="radio">GoalsNot Met</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_PatientConditionGoalsNotMet' name='DischargeSummary_PatientCondition' value='8' class='radio float-left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("8") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_PatientConditionGoalsNotMet" class="radio">Goals Partially Met</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <%  string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %>
                        <input name="DischargeSummary_ServiceProvided" value=" " type="hidden" />
                        <div class="narrow checkgroup">
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_ServiceProvidedSN' name='DischargeSummary_ServiceProvided' value='1' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("1") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_ServiceProvidedSN" class="radio">SN</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_ServiceProvidedPT' name='DischargeSummary_ServiceProvided' value='2' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("2") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_ServiceProvidedPT" class="radio">PT</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_ServiceProvidedOT' name='DischargeSummary_ServiceProvided' value='3' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("3") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_ServiceProvidedOT" class="radio">OT</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_ServiceProvidedST' name='DischargeSummary_ServiceProvided' value='4' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("4") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_ServiceProvidedST" class="radio">ST</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_ServiceProvidedMSW' name='DischargeSummary_ServiceProvided' value='5' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("5") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_ServiceProvidedMSW" class="radio">MSW</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_ServiceProvidedHHA' name='DischargeSummary_ServiceProvided' value='6' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("6") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_ServiceProvidedHHA" class="radio">HHA</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_ServiceProvidedOther' name='DischargeSummary_ServiceProvided' value='7' class='radio float-left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("7") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_ServiceProvidedOther" class="radio">Other</label>
                                <%= Html.TextBox("DischargeSummary_ServiceProvidedOtherValue", data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : "", new { @id = "DischargeSummary_ServiceProvidedOtherValue", @class = "fill" })%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ul>
        <li class="half">
            <div class="wrapper">
                <h3>Care Summary</h3>
            </div>
        </li>
        <li class="half">
            <div class="wrapper">
                <h3>Condition of Discharge</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <em>(Care Given, Progress, Regress including Therapies)</em>
                        <div class="fr">
                            <label for="DischargeSummary_CareSummaryTemplates">Templates:</label>
                            <%= Html.Templates("DischargeSummary_CareSummaryTemplates", new { @class = "templates", @template = "#DischargeSummary_CareSummary" })%>
                        </div>
                        <%= Html.TextArea("DischargeSummary_CareSummary", data.ContainsKey("CareSummary") ? data["CareSummary"].Answer : "", new { @class = "fill taller", @id = "DischargeSummary_CareSummary" })%>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </li>
        <li class="half">
            <div class="wrapper">
                <div class="wide-column">
                    <div class="row">
                        <em>(Include VS, BS, Functional and Overall Status)</em>
                        <div class="fr">
                            <label for="DischargeSummary_CareSummaryTemplates">Templates:</label>
                            <%= Html.Templates("DischargeSummary_ConditionOfDischargeTemplates", new { @class = "templates", @template = "#DischargeSummary_ConditionOfDischarge" })%>
                        </div>
                        <%= Html.TextArea("DischargeSummary_ConditionOfDischarge", data.ContainsKey("ConditionOfDischarge") ? data["ConditionOfDischarge"].Answer : "", new { @class = "fill taller", @id = "DischargeSummary_ConditionOfDischarge" })%>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </li>
    </ol>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Discharge Details</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label class="strong">Discharge Disposition: Where is the Patient after Discharge from your Agency?</label>
                        <%= Html.Hidden("DischargeSummary_DischargeDisposition", " ", new { @id = "" })%>
                        <div class="wide checkgroup">
                            <div class="option">
                                <%= Html.RadioButton("DischargeSummary_DischargeDisposition", "01", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "01" ? true : false, new { @id = "DischargeSummary_DischargeDisposition1", @class = "radio float-left" })%>
                                <label for="DischargeSummary_DischargeDisposition1">
                                    <span class="float-left">1 &#8211;</span>
                                    <span class="normal margin">Patient remained in the community (without formal assistive services)</span>
                                </label>
                            </div>
                            <div class="option">
                                <%= Html.RadioButton("DischargeSummary_DischargeDisposition", "02", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "02" ? true : false, new { @id = "DischargeSummary_DischargeDisposition2", @class = "radio float-left" })%>
                                <label for="DischargeSummary_DischargeDisposition2">
                                    <span class="float-left">2 &#8211;</span>
                                    <span class="normal margin">Patient remained in the community (with formal assistive services)</span>
                                </label>
                            </div>
                            <div class="option">
                                <%= Html.RadioButton("DischargeSummary_DischargeDisposition", "03", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "03" ? true : false, new { @id = "DischargeSummary_DischargeDisposition3", @class = "radio float-left" })%>
                                <label for="DischargeSummary_DischargeDisposition3">
                                    <span class="float-left">3 &#8211;</span>
                                    <span class="normal margin">Patient transferred to a non-institutional hospice)</span>
                                </label>
                            </div>
                            <div class="option">
                                <%= Html.RadioButton("DischargeSummary_DischargeDisposition", "04", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "04" ? true : false, new { @id = "DischargeSummary_DischargeDisposition4", @class = "radio float-left" })%>
                                <label for="DischargeSummary_DischargeDisposition4">
                                    <span class="float-left">4 &#8211;</span>
                                    <span class="normal margin">Unknown because patient moved to a geographic location not served by this agency</span>
                                </label>
                            </div>
                            <div class="option">
                                <%= Html.RadioButton("DischargeSummary_DischargeDisposition", "UK", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "UK" ? true : false, new { @id = "DischargeSummary_DischargeDispositionUK", @class = "radio float-left" })%>
                                <label for="DischargeSummary_DischargeDispositionUK">
                                    <span class="float-left">UK &#8211;</span>
                                    <span class="normal margin">Other unknown</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <%  string[] dischargeInstructionsGivenTo = data.ContainsKey("DischargeInstructionsGivenTo") && data["DischargeInstructionsGivenTo"].Answer != "" ? data["DischargeInstructionsGivenTo"].Answer.Split(',') : null; %>
                        <input name="DischargeSummary_DischargeInstructionsGivenTo" value=" " type="hidden" />
                        <div class="strong">Discharge Instructions Given To:</div>
                        <div class="narrow checkgroup">
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_DischargeInstructionsGivenTo1' name='DischargeSummary_DischargeInstructionsGivenTo' value='1' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("1") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_DischargeInstructionsGivenTo1" class="radio">Patient</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_DischargeInstructionsGivenTo2' name='DischargeSummary_DischargeInstructionsGivenTo' value='2' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("2") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_DischargeInstructionsGivenTo2" class="radio">Caregiver</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_DischargeInstructionsGivenTo3' name='DischargeSummary_DischargeInstructionsGivenTo' value='3' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("3") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_DischargeInstructionsGivenTo3" class="radio">N/A</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_DischargeInstructionsGivenTo4' name='DischargeSummary_DischargeInstructionsGivenTo' value='4' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("4") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_DischargeInstructionsGivenTo4" class="radio">Other:</label>
                                <%= Html.TextBox("DischargeSummary_DischargeInstructionsGivenToOther", data.ContainsKey("DischargeInstructionsGivenToOther") ? data["DischargeInstructionsGivenToOther"].Answer : "", new { @id = "DischargeSummary_DischargeInstructionsGivenToOther", @class="fill" })%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <%= Html.Hidden("DischargeSummary_IsVerbalizedUnderstanding", " ", new { @id = "" })%>
                        <label class="fl strong">Verbalized Understanding</label>
                        <div class="fr">
                            <%= Html.RadioButton("DischargeSummary_IsVerbalizedUnderstanding", "1", data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "1" ? true : false, new { @id = "DischargeSummary_IsVerbalizedUnderstandingY", @class = "radio" })%>
                            <label for="DischargeSummary_IsVerbalizedUnderstandingY" class="inline-radio">Yes</label>
                            <%= Html.RadioButton("DischargeSummary_IsVerbalizedUnderstanding", "0", data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "0" ? true : false, new { @id = "DischargeSummary_IsVerbalizedUnderstandingN", @class = "radio" })%>
                            <label for="DischargeSummary_IsVerbalizedUnderstandingN" class="inline-radio">No</label>
                        </div>
                    </div>
                    <div class="row">
                        <%  string[] differentTasks = data.ContainsKey("DifferentTasks") && data["DifferentTasks"].Answer != "" ? data["DifferentTasks"].Answer.Split(',') : null; %>
                        <input name="DischargeSummary_DifferentTasks" value=" " type="hidden" />
                        <div class="wide checkgroup">
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_DifferentTasks1' name='DischargeSummary_DifferentTasks' value='1' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("1") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_DifferentTasks1" class="radio">All services notified and discontinued</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_DifferentTasks2' name='DischargeSummary_DifferentTasks' value='2' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("2") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_DifferentTasks2" class="radio">Order and summary completed</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_DifferentTasks3' name='DischargeSummary_DifferentTasks' value='3' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("3") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_DifferentTasks3" class="radio">Information provided to patient for continuing needs</label>
                            </div>
                            <div class="option">
                                <%= string.Format("<input id='DischargeSummary_DifferentTasks4' name='DischargeSummary_DifferentTasks' value='4' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("4") ? "checked='checked'" : "")%>
                                <label for="DischargeSummary_DifferentTasks4" class="radio">Physician notified</label>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="clear"></div>
            </div>
        </li>
    </ol>
    <ul>
        <li>
            <div class="wrapper">
                <h3>Electronic Signature</h3>
            </div>
        </li>
    </ul>
    <ol>
        <li>
            <div class="wrapper">
                <div class="column">
                    <div class="row">
                        <label for="DischargeSummary_ClinicianSignature" class="float-left">Clinician Signature</label>
                        <div class="fr"><%= Html.Password("DischargeSummary_Clinician", "", new { @id = "DischargeSummary_Clinician" })%></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <label for="DischargeSummary_ClinicianSignatureDate" class="float-left">Date</label>
                        <div class="fr"><input type="date" name="DischargeSummary_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : "" %>" id="DischargeSummary_SignatureDate" /></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    </ol>
    <input type="hidden" name="button" value="" id="DischargeSummary_Button" />
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="complete">Complete</a></li>
            <li><a class="close">Exit</a></li>
        </ul>
    </div>
<%  } %>
</div>