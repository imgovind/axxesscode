﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<span class="wintitle">Task Details | <%= Model.DisciplineTaskName.ToTitleCase().Clean() + " | " + Model.PatientName.ToTitleCase().Clean() %></span>
<% using (Html.BeginForm("UpdateDetails", "Schedule", FormMethod.Post, new { @id = "scheduleDetailsForm" })) { %>
    <%= Html.Hidden("EventId", Model.EventId) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("EpisodeId", Model.EpisodeId) %>
    <%= Html.Hidden("Discipline", Model.Discipline)%>
<div class="wrapper main">
    <div class="align-center">
        <span class="bigtext"><%= Model.PatientName + (Model.PatientIdNumber.IsNotNullOrEmpty() ? " (" + Model.PatientIdNumber + ") | " : "") %></span><span class="bigtext"><%= Model.StartDate.ToShortDateString().ToZeroFilled() %> - <%= Model.EndDate.ToShortDateString().ToZeroFilled() %></span>
    </div>
    <%  var visible = true;//Model.Discipline.IsEqual(Disciplines.Orders.ToString()) ? false : true; %>
    <%  if (visible) Html.RenderPartial("~/Views/Schedule/Detail/Note.ascx", Model); %>
    <%  else Html.RenderPartial("~/Views/Schedule/Detail/Order.ascx", Model); %>
    <fieldset>
        <legend>Reassign Episode</legend>
        <div class="wide-column">
            <div class="row">
                <label>Check here to reassign this event to another Episode</label>
                <%= Html.CheckBox("IsEpisodeReassiged", false, new { @id = "Schedule_Detail_IsEpisodeReassiged", @class = "radio float-left" })%>
            </div>
            <div class="row" id = "Schedule_Detail_EpisodeReassigedContainer">
                <label>Select Episode:</label>
                <%= Html.PatientEpisodes("NewEpisodeId", Model.EpisodeId.ToString(), Model.PatientId, new { @id = "Schedule_Detail_NewEpisodeId" })%>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>
            Comments
            <span class="img icon note"></span>
            <em>(Yellow Sticky Note)</em>
        </legend>
        <div class="wide-column">
            <div class="row">
                <textarea id="Schedule_Detail_Comments" name="Comments" cols="5" rows="6"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Attachments</legend>
        <div class="wide-column">
            <div class="row">
                <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
                <span>There are <span id="scheduleEvent_Assest_Count"><%= Model.Assets.Count.ToString() %></span> attachment(s).</span>
    <%  foreach (Guid assetId in Model.Assets) { %>
                <span><%= Html.Asset(assetId) + " | " + string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.DeleteScheduleEventAsset($(this),'{0}','{1}','{2}','{3}');\">Delete</a>&nbsp;", Model.PatientId, Model.EpisodeId, Model.EventId, assetId) %></span>
    <%  } %>
                <br />
                <br />
                <span>Use the upload fields below to upload files associated with this scheduled task.</span>
                <br />
                <table class="form">
                    <tbody>
                        <tr>
                            <td><input id="Schedule_Detail_File1" type="file" name="Attachment1" /></td>
                        </tr><tr>
                            <td><input id="Schedule_Detail_File2" type="file" name="Attachment2" /></td>
                        </tr><tr>
                            <td><input id="Schedule_Detail_File3" type="file" name="Attachment3" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('scheduledetails');">Cancel</a></li>
        </ul>
    </div>
    <div class="activity-log"><% = string.Format("<span class=\"img icon activity\" onclick=\"Schedule.LoadLog('{0}','{1}','{2}')\" title=\"Activity Logs\"></span>", Model.PatientId, Model.EventId, Model.DisciplineTask)%></div>
</div>
<%  } %>
<script type="text/javascript">
    Forms.ShowIfChecked($("#Schedule_Detail_IsEpisodeReassiged"), $("#Schedule_Detail_EpisodeReassigedContainer"));
</script>