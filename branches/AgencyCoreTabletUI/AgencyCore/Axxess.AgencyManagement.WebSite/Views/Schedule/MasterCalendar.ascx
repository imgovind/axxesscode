﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %><%
var scheduleEvents = Model.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId != Guid.Empty && e.DisciplineTask > 0 && e.IsDeprecated != true && e.EventDate.IsNotNullOrEmpty() && e.EventDate.IsValidDate() && e.EventDate.ToDateTime() >= Model.StartDate && e.EventDate.ToDateTime() <= Model.EndDate).OrderBy(o => o.EventDate.ToDateTime()).ToList();
var startWeekDay = (int)Model.StartDate.DayOfWeek;
var startDate = Model.StartDate;
var endDate = Model.EndDate;
var currentDate = Model.StartDate.AddDays(-startWeekDay); %>
<%= Model.HasPrevious ? "<span class='trical'><span class='abs-left'><a onclick=\"Schedule.loadMasterCalendarNavigation('" + Model.PreviousEpisode.Id + "','" + Model.PatientId + "');\"><span class='largefont'>&#171;</span> Previous Episode</a></span></span>" : string.Empty %>
<%= Model.HasNext ? "<span class='trical'><span class='abs-right'><a onclick=\"Schedule.loadMasterCalendarNavigation('" + Model.NextEpisode.Id + "','" + Model.PatientId + "');\">Next Episode <span class='largefont'>&#187;</span></a></span></span>" : string.Empty %>
<%= String.Format("<div class=\"buttons\"><ul><li><a href='javascript:void(0);' onclick=\"U.GetAttachment('Schedule/MasterCalendarPdf', {{ 'patientId': '{0}', 'episodeId': '{1}' }});\">Print</span></a></li></ul></div>", Model.PatientId, Model.Id)%>
<label>Patient Name:</label> <span class="largefont strong"><%= Model.DisplayName %></span>
<%= Model.Detail != null && Model.Detail.FrequencyList.IsNotNullOrEmpty() ? "<br /><label>Frequencies:</label> <span class='largefont strong'>" + Model.Detail.FrequencyList + "</span>" : string.Empty %>
<table id="masterCalendarTable" class="masterCalendar">
    <thead><tr><th></th><th><div>Sun</div></th><th><div>Mon</div></th><th><div>Tue</div></th><th><div>Wed</div></th><th><div>Thu</div></th><th><div>Fri</div></th><th><div>Sat</div></th></tr></thead>
    <tbody><%for (int i = 1; i <= 10; i++) { %>
            <%= i == 10 ? "<tr class='lastTr'>" : "<tr>" %>
            <th><div>Week <%= i %></div></th><%
            int addedDate = (i - 1) * 7;
            for (int j = 0; j <= 6; j++) 
            {
                var specificDate = currentDate.AddDays(j + addedDate);
                if (specificDate < startDate || specificDate > endDate) { %><td><div></div></td><%}
                else{
                        var currentSchedules = scheduleEvents.FindAll(e => e.EventDate.ToDateTime() == specificDate); %>
                        <%= currentSchedules.Count != 0 ? "<td " + (j == 6 ? " class=\"lastTd\"" : "") + ">" : "<td>" %>
                            <div><span><%= string.Format("{0:MM/dd}", specificDate) %></span><br /><%
                                if (currentSchedules.Count != 0) 
                                {
                                    foreach (var evnt in currentSchedules) {if (evnt.Discipline != Disciplines.Orders.ToString()) { %><span class="eventTitle"><%= EnumExtensions.GetCustomShortDescription((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), evnt.DisciplineTask)).ToUpper() %><br /></span><span class="desc">Employee: <%= evnt.UserName %></span><span class="desc"><br />Status: <%= evnt.StatusName %><br /></span><%}}
                                } 
                                else { %><br /><%} %></div></td><%
                    }
            } %></tr><%} %>
    </tbody>
</table>