﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MissedVisit>" %>
<div class="wrapper main">
    <fieldset>
        <div class="wide-column">
            <div class="row">
                <label for="Missed_Visit_Info_Name" class="fl strong">Patient Name:</label>
                <div class="float-right"><%= Model != null && Model.PatientName.IsNotNullOrEmpty() ? Model.PatientName : "Unknown" %></div>
            </div>
            <div class="row">
                <label for="Missed_Visit_Info_TypeofVisit" class="fl strong">Type of Visit:</label>
                <div class="float-right"><%= Model != null && Model.VisitType.IsNotNullOrEmpty() ? Model.VisitType  : "" %></div>
            </div>
            <div class="row">
                <label for="Missed_Visit_Info_DateofVisit" class="fl strong">Date of Visit:</label>
                <div class="float-right"><%= Model != null ? Model.Date.ToShortDateString():string.Empty %></div>
            </div>
            <div class="row">
                <label for="Missed_Visit_Info_UserName" class="fl strong">Assigned To:</label>
                <div class="float-right"><%= Model != null && Model.UserName.IsNotNullOrEmpty() ? Model.UserName : "" %></div>
            </div>
            <div class="row">
                <label for="Missed_Visit_Info_OrderGenerated" class="fl strong">Order Generated:</label>
                <div class="float-right"><%= (Model != null && Model.IsOrderGenerated) ? "Yes" : "No"%></div>
            </div>
            <div class="row">
                <label for="Missed_Visit_Info_PhysicianOfficeNotified" class="fl strong">Physician Office Notified:</label>
                <div class="float-right"><%= (Model != null && Model.IsPhysicianOfficeNotified) ? "Yes" : "No"%></div>
            </div>
            <div class="row">
                <label for="Missed_Visit_Info_Reason" class="fl strong">Reason:</label>
                <div class="float-right"><%= Model != null && Model.Reason.IsNotNullOrEmpty() ? Model.Reason : "" %></div>
            </div>
            <div class="row">
                <label for="Missed_Visit_Info_Comments" class="fl strong">Comments:</label>
                <div class="float-right"><%= Model != null && Model.Comments.IsNotNullOrEmpty() ? Model.Comments : "" %></div>
            </div>
        </div>
    </fieldset>
</div>