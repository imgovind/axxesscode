﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<% using (Html.BeginForm("AddMissedVisit", "Schedule", FormMethod.Post, new { @id = "newMissedVisitForm" })) { %>
<%= Html.Hidden("Id", Model.EventId, new { @id = "New_MissedVisit_EventId" })%>
<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "New_MissedVisit_EpisodeId" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "New_MissedVisit_PatientId" })%>
<div class="wrapper main">
    <div><span id="New_MissedVisit_Patient" class="bigtext"><%=Model.PatientName %></span></div>
    <fieldset>
        <legend>Missed Visit Details</legend>
        <div class="column">
            <div class="row">
                <label for="New_MissedVisit_TaskName" class="float-left">Type of Visit:</label>
                <div class="float-right"><span id="New_MissedVisit_TaskName"><%=Model.DisciplineTaskName %></span></div>
            </div><div class="row">
                <label for="New_MissedVisit_Date" class="float-left">Date of Visit:</label>
                <div class="float-right"><span id="New_MissedVisit_Date"><%=Model.EventDate %></span></div>
            </div><div class="row">
                <label for="New_MissedVisit_Order_Yes" class="float-left">Order Generated:</label>
                <div class="float-right">
                    <input id="New_MissedVisit_Order_Yes" type="radio" class="radio" name="IsOrderGenerated" value="true" />
                    <label for="New_MissedVisit_Order_Yes">Yes</label>
                    <input id="New_MissedVisit_Order_No" type="radio" class="radio" name="IsOrderGenerated" value="false" />
                    <label for="New_MissedVisit_Order_No">No</label>
                </div>
            </div><div class="row">
                <label for="New_MissedVisit_PhysicianNotified_Yes" class="float-left">Physician Office Notified:</label>
                <div class="float-right">
                    <input id="New_MissedVisit_PhysicianNotified_Yes" type="radio" checked="checked" class="radio" name="IsPhysicianOfficeNotified" value="true" />
                    <label for="New_MissedVisit_PhysicianNotified_Yes">Yes</label>
                    <input id="New_MissedVisit_PhysicianNotified_No" type="radio" class="radio" name="IsPhysicianOfficeNotified" value="false" />
                    <label for="New_MissedVisit_PhysicianNotified_No">No</label>
                </div>
            </div><div class="row">
                <label for="New_MissedVisit_Reason" class="float-left">Reason:</label>
                <div class="float-right">
                    <select id="New_MissedVisit_Reason" name="Reason" class="selectDropDown">
                        <option value="">** Select **</option>
                        <option value="Cancellation of Care">Cancellation of Care</option>
                        <option value="Doctor - Clinic Appointment">Doctor - Clinic Appointment</option>
                        <option value="Family - Caregiver Able to Assist Patient">Family - Caregiver Able to Assist Patient</option>
                        <option value="No Answer to Locked Door">No Answer to Locked Door</option>
                        <option value="No Answer to Phone Call">No Answer to Phone Call</option>
                        <option value="Patient - Family Uncooperative">Patient - Family Uncooperative</option>
                        <option value="Patient Hospitalized">Patient Hospitalized</option>
                        <option value="Patient Unable to Answer Door">Patient Unable to Answer Door</option>
                        <option value="Therapy on Hold">Therapy on Hold</option>
                        <option value="Other">Other (Specify)</option>
                    </select>
                </div>
            </div>
        </div>
        <table class="form">
            <tbody>
                <tr class="linesep vert">
                    <td>
                        <label for="Comment">Comments:</label>
                        <div><%= Html.TextArea("Comments", "", new { @id = "New_MissedVisit_Comments" })%></div>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset><fieldset>
        <div class="column">
            <div class="row">
                <label for="New_MissedVisit_ClinicianSignature" class="bigtext float-left">Staff Signature:</label>
                <div class="float-right"><%= Html.Password("Signature", "", new { @id = "New_MissedVisit_ClinicianSignature", @class = "required" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_MissedVisit_ClinicianSignatureDate" class="bigtext float-left">Signature Date:</label>
                <div class="float-right"><input type="date" name="SignatureDate" value="<%= Model.EventDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="New_MissedVisit_ClinicianSignatureDate" class="required" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="complete">Submit</a></li>
            <li><a class="close">Close</a></li>
        </ul>
    </div>
</div>
<% } %>