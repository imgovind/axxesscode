﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<%  string[] stabs = new string[] { "Nursing", "HHA","MSW", "Therapy" }; %>
<%  if (Model.Episode != null) { %>
<div class="top">
    <div class="winmenu">
        <ul>
            <%  if (!Model.IsDischarged && Current.HasRight(Permissions.EditEpisode)) { %>
            <li><a href="javascript:void(0);" onclick="UserInterface.ShowNewEpisodeModal('<%= Model.PatientId %>');" status="Add New Episode">New Episode</a></li>
            <%  } %>
            <%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
            <li><a href="javascript:void(0);" onclick="UserInterface.ShowMultipleDayScheduleModal('<%= Model.Episode.Id %>', '<%= Model.PatientId %>');" status="Multiple Employee">Schedule Employee</a></li>
            <%  } %>
            <%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
            <li><a href="javascript:void(0);" onclick="UserInterface.ShowMultipleReassignModal('<%= Model.Episode.Id %>', '<%= Model.PatientId %>','Episode');" status="Reassign Schedules">Reassign Schedules</a></li>
            <%  } %>
            <li><a href="javascript:void(0);" onclick="Schedule.loadMasterCalendar('<%= Model.Episode.PatientId %>','<%= Model.Episode.Id %>');">Master Calendar</a></li>
            <%  if (Current.HasRight(Permissions.EditEpisode)) { %>
            <li><a href="javascript:void(0);" onclick="Schedule.loadInactiveEpisodes('<%= Model.Episode.PatientId %>');">Inactive Episodes</a></li>
            <%  } %>
        </ul>
    </div>
    <div class="wrapper">
        <%  if (Model.Episode.HasPrevious) { %>
        <span class="button top left">
            <a onclick="Schedule.NavigateEpisode('<%= Model.Episode.PreviousEpisode.Id%>','<%=Model.Episode.PatientId %>');">
                <span class="largefont">&#171;</span>
                Previous Episode
            </a>
        </span>
        <%  } %>
        <%  if (Model.Episode.HasNext) { %>
        <span class="button top right">
            <a onclick="Schedule.NavigateEpisode('<%= Model.Episode.NextEpisode.Id%>','<%=Model.Episode.PatientId %>');">
                Next Episode
                <span class="largefont">&#187;</span>
            </a>
        </span>
        <%  } %>
        <fieldset class="patient-summary ma">
            <div class="abs-left">
                <h1 class="fl"><%= Model.Episode.DisplayName %></h1>
                <div class="fl"><%= Html.PatientEpisodes("EpisodeList", Model.Episode.Id.ToString(), Model.PatientId, new { @id = "Calendar_EpisodeList" }) %></div>
            </div>
            <div class="buttons abs-right">
                <ul>
                    <%  if (Current.HasRight(Permissions.EditEpisode)) { %>
                    <li><a href="javascript:void(0);" onclick="UserInterface.ShowEditEpisodeModal('<%= Model.Episode.Id %>','<%= Model.Episode.PatientId %>');">Manage Episode</a></li>
                    <%  } %>
                    <li><a href="javascript:void(0);" onclick="Schedule.RefreshCurrentEpisode('<%= Model.Episode.PatientId %>','<%= Model.Episode.Id %>');">Refresh</a></li>
                </ul>
            </div>
        </fieldset>
        <%  Html.RenderPartial("Calendar", new CalendarViewData { Episode = Model.Episode, PatientId = Model.Episode.PatientId, IsDischarged = Model.IsDischarged }); %>
        <%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
        <div id="schedule_collapsed">
            <a href="javascript:void(0);" onclick="Schedule.ShowScheduler()" class="show-scheduler">Show Scheduler</a>
        </div>
            <%  Html.Telerik().TabStrip().Name("ScheduleTabStrip").ClientEvents(events => events.OnSelect("Schedule.OnSelect")).Items(tabstrip => { %>
                <%  for (int sindex = 0; sindex < stabs.Length; sindex++) { %>
                    <%  string stitle = stabs[sindex]; %>
                    <%  string tabname = stitle == "MSW" ? "MSW / Other" : stitle; %>
                    <%  tabstrip.Add().Text(tabname).HtmlAttributes(new { id = stitle + "_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
                        <%  using (Html.BeginForm("Add", "Schedule", FormMethod.Post)) { %>
        <%= Html.Hidden("patientId") %>
        <div class="tabcontents">
            <table id="<%= stitle %>ScheduleTable" data="<%= stitle %>" class="scheduleTables purgable">
                <thead>
                    <tr>
                        <th>Task</th>
                        <th>User</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" />
            <input type="hidden" name="episodeId" value="" class="scheduleValue" />
            <div class="buttons">
                <ul>
                    <li><%= String.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ScheduleInputFix($(this),'Patient','#{0}ScheduleTable');\">Save</a>", stitle) %></li>
                    <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
                </ul>
            </div>
        </div>
                        <%  } %>
                    <%  }); %>
                <%  } %>
                <%  tabstrip.Add().Text("Orders/Care Plans").HtmlAttributes(new { id = "Orders_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
                    <%  using (Html.BeginForm("Add", "Schedule", FormMethod.Post)) { %>
        <%= Html.Hidden("patientId") %>
        <div class="tabcontents">
            <table id="OrdersScheduleTable" data="Orders" class="scheduleTables purgable">
                <thead>
                    <tr>
                        <th>Task</th>
                        <th>User</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" /><input type="hidden" name="episodeId" value="" />
            <div class="buttons float-right">
                <ul>
                    <li><a href="javascript:void(0);" onclick="Schedule.ScheduleInputFix($(this),'Patient','#OrdersScheduleTable');">Save</a></li>
                    <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
                </ul>
            </div>
        </div>
                    <%  } %>
                <%  }); %>
                <%  tabstrip.Add().Text("Daily/Outlier").ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
                    <%  using (Html.BeginForm("AddMultiple", "Schedule", FormMethod.Post, new { @id = " ", })) { %>
        <%= Html.Hidden("patientId", "", new { @id = "" })%>
        <div class="tabcontents">
            <table id="multipleScheduleTable" data="Multiple" class="scheduleTables">
                <thead>
                    <tr>
                        <th>Task</th>
                        <th>User</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <%  var htmlAttributes = new Dictionary<string, string>(); %>
                    <%  htmlAttributes.Add("id", "multipleDisciplineTask"); %>
                    <%  htmlAttributes.Add("class", "MultipleDisciplineTask"); %>
                        <td><%= Html.MultipleDisciplineTasks("DisciplineTask", "", htmlAttributes)%></td>
                        <td><%= Html.Users("userId", "", new { @class = "suppliesCode Users" }) %></td>
                        <td class="daterange">
                            <input type="text" name="StartDate" value="<%= Model.Episode.StartDate.ToShortDateString() %>" mindate="<%= Model.Episode.StartDate.ToShortDateString() %>" maxdate="<%= Model.Episode.EndDate.ToShortDateString() %>" id="outlierStartDate" />
                            <span>to</span>
                            <input type="text" name="EndDate" value="<%= Model.Episode.StartDate.ToShortDateString() %>" mindate="<%= Model.Episode.StartDate.ToShortDateString() %>" maxdate="<%= Model.Episode.EndDate.ToShortDateString() %>" id="outlierEndDate" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" name="episodeId" value="" class="scheduleValue" />
            <input type="hidden" name="Discipline" value="" />
            <input type="hidden" name="IsBillable" value="" />
            <div class="buttons float-right">
                <ul>
                    <li><a href="javascript:void(0);" onclick="Schedule.FormSubmitMultiple($(this));">Save</a></li>
                    <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
                </ul>
            </div>
        </div>
                    <%  } %>
                <%  }); %>
            <%  }).SelectedIndex(0).Render(); %>
        <%  } %>
    </div>
</div>
<div id="scheduleBottomPanel" class="bottom"><% Html.RenderPartial("Activities", new ScheduleActivityArgument { EpisodeId = Model.Episode.Id, PatientId = Model.Episode.PatientId, Discpline = "all" }); %></div>
<script type="text/javascript">
    Schedule.positionBottom();
    $("#outlierStartDate,#outlierEndDate").DatePicker();
</script>
<%  } else { %>
    <%  if (!Model.IsDischarged) { %>
<script type="text/javascript">
    $("#ScheduleMainResult").html(U.MessageWarn("No Episode Found", "No episode found for this patient.  Please add a new episode.").append(
        $("<div/>", { "class": "heading" }).Buttons([{
            Text: "Add New Episode",
            Click: function() {
                UserInterface.ShowNewEpisodeModal("<%= Model.PatientId.ToString() %>")
            }
        },{
            Text: "Inactive Episodes",
            Click: function() {
                Schedule.loadInactiveEpisodes("<%= Model.PatientId %>")
            }
        }])
    ));
</script>
    <%  } else { %>
<script type="text/javascript">
    $("#ScheduleMainResult").html(U.MessageWarn("No Episode Found", "No episodes found for this discharged patient. Re-admit the patient to create new episodes."));
</script>
    <%  } %>
<%  } %>