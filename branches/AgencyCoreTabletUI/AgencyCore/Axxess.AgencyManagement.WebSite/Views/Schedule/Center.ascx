﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<span class="wintitle">Schedule Center | <%= Current.AgencyName.Clean() %></span>
<%  string[] stabs = new string[] { "Nursing", "HHA","Therapy"}; %>
<div class="wrapper layout">
    <div class="ui-layout-west">
        <div class="top">
            <div class="buttons heading">
                <ul>
<%  if (Current.HasRight(Permissions.ManagePatients)) { %>
                    <li><a href="javascript:void(0)" onclick="javascript:Acore.Open('newpatient')" status="Add New Patient">Add New Patient</a></li>
<%  } %>
                </ul>
            </div>
            <div class="row">
                <label for="ScheduleBranchCode">Branch:</label>
                <div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new { @id = "ScheduleBranchCode", @class = "ScheduleBranchCode" })%></div>
            </div>
            <div class="row">
                <label for="ScheduleStatusDropDown">View:</label>
                <div>
                    <select name="list" class="ScheduleStatusDropDown" id="ScheduleStatusDropDown">
                        <option value="1">Active Patients</option>
                        <option value="2">Discharged Patients</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label for="SchedulePaymentDropDown">Filter:</label>
                <div>
                    <select name="list" class="SchedulePaymentDropDown" id="SchedulePaymentDropDown">
                        <option value="0">All</option>
                        <option value="1">Medicare (traditional)</option>
                        <option value="2">Medicare (HMO/managed care)</option>
                        <option value="3">Medicaid (traditional)</option>
                        <option value="4">Medicaid (HMO/managed care)</option>
                        <option value="5">Workers' compensation</option>
                        <option value="6">Title programs</option>
                        <option value="7">Other government</option>
                        <option value="8">Private</option>
                        <option value="9">Private HMO/managed care</option>
                        <option value="10">Self Pay</option>
                        <option value="11">Unknown</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label for="txtSearch_Schedule_Selection">Find:</label>
                <div><input id="txtSearch_Schedule_Selection" class="text" name="" value="" type="text" /></div>
            </div>
        </div>
        <div class="bottom new-patient"><%  Html.RenderPartial("Patients"); %></div>
    </div>
    <div id="ScheduleMainResult" class="ui-layout-center"></div>
</div>
<%  if (Model == null || Model.Count == 0) { %>
<script type="text/javascript">
    $("#ScheduleMainResult").html(undefined.Warning("No Patient Found", "There are no patients to be gound which match your search criteria.  Please try a different search."));
</script>
<%  } %>