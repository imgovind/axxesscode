<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Forum.Master" Inherits="System.Web.Mvc.ViewPage<OpenForum.Core.ViewModels.PostViewModel>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%= (Model.IncludeDefaultStyles) ? ForumViewHelper.GetDefaultStyles() : "" %>
<% if (Model.IncludeValidationSummary) Html.ValidationSummary(); %>
<% Html.BeginForm(); %>
    <label>Title</label>
    <div><%= Html.TextBox("title", Model.Post.Title, new { @class = "open-forum-textbox" }) %></div>
    <label>Question</label>
    <div id="wysiwyg"><%= Html.TextArea("body", Model.Post.Body, new { @class = "open-forum-textarea" }) %></div>
    <input id="submit" type="submit" value="Submit" class="hidden" />
    <%= Html.Hidden("submit", "Submit") %>
    <%= Html.Hidden("id", Model.Post.Id) %>
    <ul class="post-controls">
        <li class="first"><a onclick="$('#body').val(CKEDITOR.instances['body'].getData());$('#submit').click();return false">Submit</a></li>
        <li><%= Html.ActionLink("Cancel", "Index") %></li>
    </ul>
<% Html.EndForm(); %>
<script type="text/javascript">
    var editor = CKEDITOR.replace('body', {
        skin: 'office2003',
        resize_enabled: false,
        height: '500px',
        removePlugins: 'elementspath',
        toolbarCanCollapse: false,
        toolbar: [['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                'JustifyBlock', '-', 'Link', 'Unlink', '-', 'Font', 'FontSize', 'TextColor', '-', 'SpellChecker', 'Scayt']]
    });
</script>
</asp:Content>