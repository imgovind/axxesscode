﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<ValidationInfoViewData>" %><%
var dictonary = new Dictionary<string, string>() {
    { AssessmentType.StartOfCare.ToString(), "StartOfCare" },
    { AssessmentType.ResumptionOfCare.ToString(), "ResumptionOfCare" },
    { AssessmentType.Recertification.ToString(), "Recertification" },
    { AssessmentType.FollowUp.ToString(), "FollowUp" },
    { AssessmentType.TransferInPatientNotDischarged.ToString(), "TransferInPatientNotDischarged" },
    { AssessmentType.TransferInPatientDischarged.ToString(), "TransferInPatientDischarged" },
    { AssessmentType.DischargeFromAgencyDeath.ToString(), "DischargeFromAgencyDeath" },
    { AssessmentType.DischargeFromAgency.ToString(), "DischargeFromAgency" }
}; %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>OASIS-C Validation</title>
        <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
                .Add("globals.css")
                .Add("sprite.css")
                .Add("forms.css")
                .Add("validation.css")
                .Add("telerik.common.css")
                .Add("telerik.office2007.css")
                .Combined(true)
                .Compress(true)
                .CacheDurationInDays(1)
                .Version(Current.AssemblyVersion)) %>
        <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group

            // jQuery
            .Add("jquery-1.7.1.min.js")

            // jQuery UI Plugins
            .Add("Plugins/jQueryUI/jquery.ui.core.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.widget.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.mouse.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.position.min.js")
            .Add("Plugins/jQueryUI/jquery.ui.datepicker.min.js")
                                
            // Other Plugins
            .Add("Plugins/Other/form.min.js")
            .Add("Plugins/Other/validate.min.js")

            // Custom Plugins
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "DatePicker.js")
            .Add("Plugins/Custom/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "TimePicker.js")
            
            // Modules            
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Oasis.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "OasisValidation.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")

            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
    </head>
    <body>
        <div class="wrapper main">
<%  if (Model.ValidationErrors != null) { %>
            <div class="title">
                You have
                <strong><%= Model.ValidationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count()%> error<%= Model.ValidationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count() != 1 ? "s" : string.Empty %></strong>
                and
                <strong><%= Model.ValidationErrors.Where(e => e.ErrorType == "WARNING").Count() %> warning<%= Model.ValidationErrors.Where(e => e.ErrorType == "WARNING").Count() != 1 ? "s" : string.Empty %></strong>
            </div>
    <%  foreach (var data in Model.ValidationErrors) { %>
            <div class="error<%= data.ErrorType == "FATAL" ? " fatal" : string.Empty %>">
                <a onclick="window.parent.Oasis.gotoQuestion('<%= data.ErrorDup.Substring(0, 5) %>','<%= dictonary[Model.AssessmentType] %>}');window.parent.UserInterface.CloseModal()">
                    <%= data.ErrorType == "ERROR" || data.ErrorType == "FATAL" ? "<span class='img icon error'></span>" : "<span class='img icon warning'></span>" %>
                    <span class="error-name"><%= data.ErrorDup %></span>
                    <span class="description"><%= data.Description %></span>
                </a>
            </div>
    <%  } %>
<%  } %>
<%  if (Model.ValidationErrors != null && Model.ValidationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count() == 0) { %>
    <%  if (Current.HasRight(Permissions.ViewHHRGCalculations)) { %>
        
            <fieldset>
                <legend>OASIS Validation</legend>
                <div class="column">
                    <div class="row">
                        <div class="fl strong">HIPPS Code:</div>
                        <div class="fr"><%= Model.HIPPSCODE %></div>
                    </div>
                    <div class="row">
                        <div class="fl strong">OASIS Claim Matching Key:</div>
                        <div class="fr"><%= Model.HIPPSKEY %></div>
                    </div>
                </div>
                <div class="column">
                    <div class="row">
                        <div class="fl strong">HHRG Code:</div>
                        <div class="fr"><%= Model.HHRG %></div>
                    </div>
                    <div class="row">
                        <div class="fl strong">Episode Payment Rate:</div>
                        <div class="fr"><%= Model.StandardPaymentRate != 0 ? string.Format("${0}", Math.Round(Model.StandardPaymentRate,2).ToString()) : string.Empty %></div>
                    </div>
                </div>
            </fieldset>
    <%  } %>
    <%  using (Html.BeginForm("SubmitOnly", "Oasis", FormMethod.Post, new { @id = "oasisValidationForm" })) { %>
        <%= Html.Hidden(Model.AssessmentType + "_PatientId", Model.PatientId)%>
        <%= Html.Hidden(Model.AssessmentType + "_EpisodeId", Model.EpisodeId)%>
        <%= Html.Hidden(Model.AssessmentType + "_Id", Model.AssessmentId)%>
        <%= Html.Hidden("OasisValidationType", Model.AssessmentType)%>
        <%= Html.Hidden("oasisPageName", dictonary[Model.AssessmentType])%>
            <input type="hidden" id="oasisval_submit" onclick="$(this).closest('form').submit()" />
            <fieldset>
                <legend>Signature</legend>
                <div class="column">
        <%  if (Model.AssessmentType == AssessmentType.StartOfCare.ToString() || Model.AssessmentType == AssessmentType.Recertification.ToString() || Model.AssessmentType == AssessmentType.ResumptionOfCare.ToString() || Model.AssessmentType == AssessmentType.FollowUp.ToString()){ %>
                    <div class="row">
                        <label for="<%= Model.AssessmentType%>_TimeIn" class="float-left">Time In</label>
                        <div class="fr"><%= Html.TextBox(Model.AssessmentType + "_TimeIn", Model.TimeIn, new { @id = Model.AssessmentType + "_TimeIn", @class = "loc required" }) %></div>
                    </div>
        <%  } %>
                    <div class="row">
                        <label for="<%= Model.AssessmentType%>_ValidationClinician" class="float-left">Clinician Signature</label>
                        <div class="fr"><%= Html.Password(Model.AssessmentType + "_ValidationClinician", "", new { @id = Model.AssessmentType + "_ValidationClinician", @class = "required" }) %></div>
                    </div>
                </div>
                <div class="column">
        <%  if (Model.AssessmentType == AssessmentType.StartOfCare.ToString() || Model.AssessmentType == AssessmentType.Recertification.ToString() || Model.AssessmentType == AssessmentType.ResumptionOfCare.ToString() || Model.AssessmentType == AssessmentType.FollowUp.ToString()){ %>
                    <div class="row">
                        <label for="<%= Model.AssessmentType%>_TimeOut" class="float-left">Time Out</label>
                        <div class="fr"><%= Html.TextBox(Model.AssessmentType + "_TimeOut", Model.TimeOut, new { @id = Model.AssessmentType + "_TimeOut", @class = "loc required" }) %></div>
                    </div>
        <%  } %>
                    <div class="row">
                        <label for="<%= Model.AssessmentType%>_ValidationSignatureDate" class="float-left">Date</label>
                        <div class="fr"><%= String.Format("<input type='text' id='{0}_ValidationSignatureDate' name='{0}_ValidationSignatureDate' class='date required' />", Model.AssessmentType) %></div>
                    </div>
                </div>
            </fieldset>
    <%  } %>
<%  } %>
        </div>
        <script type="text/javascript">
            Oasis.oasisSignatureSubmit($("#oasisValidationForm"));
<%  if (Model.ValidationErrors != null && Model.ValidationErrors.Where(e => e.ErrorType == "ERROR" || e.ErrorType == "FATAL").Count() == 0) { %>
            $("#printbutton", window.parent.document).parent().html(
                $("<a/>", { "text": "Finish", "onclick": "$('#printview').contents().find('#oasisval_submit').click();return false" })
            ).next().find("a").text("Cancel");
<%  } %>
        </script>
    </body>
</html>