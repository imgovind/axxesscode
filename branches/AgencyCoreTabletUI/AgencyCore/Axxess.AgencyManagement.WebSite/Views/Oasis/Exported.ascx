﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">List of Exported OASIS Assessments | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
<%  var visible = Current.HasRight(Permissions.ReopenDocuments); %>
<%  using (Html.BeginForm("ExportedOasis", "Export", FormMethod.Post)) { %>
    <div class="float-right">
        <div class="buttons">
            <ul>
                <li><%= Html.ActionLink("Export to Excel", "ExportedOasis", "Export", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = "ExportedOasis_ExportLink" })%></li>
            </ul>
        </div>
    </div>
    <fieldset class="grid-controls">
        <label class="float-left" for="ExportedOasis_BranchCode">Branch:</label>
        <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = "ExportedOasis_BranchCode" })%></div>
        <label class="strong" for="ExportedOasis_StartDate">Date Range:</label>
        <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="ExportedOasis_StartDate" class="short" />
        <label class="strong" for="ExportedOasis_EndDate">To</label>
        <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="ExportedOasis_EndDate" class="short" />
        <div class="buttons float-right">
            <ul>
                <li><a onclick="Agency.RebindExportedOasis();return false">Generate</a></li>
            </ul>
        </div>
        <div class="clear"></div>
        <label class="float-left" for="ExportedOasis_Status">Filter by:</label>
        <select id="ExportedOasis_Status" name="StatusId" class="PatientStatusDropDown">
            <option value="0">All</option>
            <option value="1" selected>Active</option>
            <option value="2">Discharged</option>
        </select>
        <div id="ExportedOasis_Search" class="fl"></div>
    </fieldset>
    <%  Html.Telerik().Grid<AssessmentExport>().Name("exportedOasisGrid").Columns(columns => {
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(true);
            columns.Bound(o => o.AssessmentName).Title("Assessment").Sortable(true);
            columns.Bound(o => o.AssessmentDate).Format("{0:MM/dd/yyyy}").Width(120).Title("Assessment Date").Sortable(true);
            columns.Bound(o => o.EpisodeRange).Format("{0:MM/dd/yyyy}").Width(150).Title("Episode").Sortable(true);
            columns.Bound(o => o.ExportedDate).Format("{0:MM/dd/yyyy}").Width(120).Title("Exported Date").Sortable(true);
            columns.Bound(o => o.Insurance).Sortable(true);
            columns.Bound(o => o.AssessmentId).Title("Cancel").Width(110).Template(o =>{%><%= string.Format("<a onclick=\"Oasis.LoadCancel('{0}','{1}');return false\" >Generate Cancel</a>", o.AssessmentId, o.AssessmentType)%><%}).ClientTemplate("<a onclick=\"Oasis.LoadCancel('<#= AssessmentId#>','<#= AssessmentType#>');return false\" >Generate Cancel</a>").Sortable(false).Visible(visible).Sortable(false);
            columns.Bound(o => o.AssessmentId).Title("Action").Width(60).Template(o =>{%><%= string.Format("<a onclick=\"Oasis.Reopen('{0}','{1}','{2}','{3}','{4}');return false\" >Reopen</a>", o.AssessmentId, o.PatientId, o.EpisodeId, o.AssessmentType, "ReOpen")%><%}).ClientTemplate("<a onclick=\"Oasis.Reopen('<#= AssessmentId#>','<#= PatientId#>','<#= EpisodeId#>','<#= AssessmentType#>','ReOpen');return false\" >Reopen</a>").Sortable(false).Visible(visible);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Exported", "Oasis", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Scrollable().Sortable().Footer(false).Render(); %>
<%  } %>
</div>
<script type="text/javascript">
    $("#ExportedOasis_Search").append(
        $("<div/>").GridSearchById("#exportedOasisGrid")
    );
</script>

