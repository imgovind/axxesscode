﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Assessment>" %><%
var data = Model.ToDictionary();
var agency = Model.AgencyData != null ? Model.AgencyData.ToObject<Agency>() : new Agency(); 
var patient = Model.PatientData != null ? Model.PatientData.ToObject<Patient>() : new Patient(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + " | " : string.Empty %>OASIS-C Start of Care<%= data != null && data.ContainsKey("M0040LastName") && data["M0040LastName"].Answer.IsNotNullOrEmpty() ? " | " + data["M0040LastName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040Suffix") && data["M0040Suffix"].Answer.IsNotNullOrEmpty() ? " " + data["M0040Suffix"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040FirstName") && data["M0040FirstName"].Answer.IsNotNullOrEmpty() ? ", " + data["M0040FirstName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040MI") && data["M0040MI"].Answer.IsNotNullOrEmpty() ? " " + data["M0040MI"].Answer : string.Empty %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
    <% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<% var location = agency.GetBranch(patient != null ? patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = agency.GetMainOffice(); %>
<body>
<script type="text/javascript">
    printview.cssclass = "oasis";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : string.Empty %><%= location != null && location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : string.Empty %><%= location != null && location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : string.Empty %>%3Cbr /%3E<%= location != null && location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : string.Empty %><%= location != null && location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : string.Empty %><%= location != null && location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty %>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EOASIS-C%3Cbr /%3EStart of Care%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= data != null && data.ContainsKey("M0040LastName") && data["M0040LastName"].Answer.IsNotNullOrEmpty() ? data["M0040LastName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040Suffix") && data["M0040Suffix"].Answer.IsNotNullOrEmpty() ? " " + data["M0040Suffix"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040FirstName") && data["M0040FirstName"].Answer.IsNotNullOrEmpty() ? ", " + data["M0040FirstName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040MI") && data["M0040MI"].Answer.IsNotNullOrEmpty() ? " " + data["M0040MI"].Answer : string.Empty %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22hexcol%22%3E%3Cspan%3E%3Cstrong%3EAssessment Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.AssessmentDate != null && Model.AssessmentDate.IsValid() ?  Model.AssessmentDate.ToShortDateString().Clean() : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.TimeIn.Clean()%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.TimeOut.Clean()%>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            "<%= agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : string.Empty %><%= location != null && location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : string.Empty %><%= location != null && location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : string.Empty %>%3Cbr /%3E<%= location != null && location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : string.Empty %><%= location != null && location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : string.Empty %><%= location != null && location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : string.Empty %>" +
            "%3C/td%3E%3Cth class=%22h1%22%3EOASIS-C%3Cbr /%3EStart of Care%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= data != null && data.ContainsKey("M0040LastName") && data["M0040LastName"].Answer.IsNotNullOrEmpty() ? data["M0040LastName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040Suffix") && data["M0040Suffix"].Answer.IsNotNullOrEmpty() ? " " + data["M0040Suffix"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040FirstName") && data["M0040FirstName"].Answer.IsNotNullOrEmpty() ? ", " + data["M0040FirstName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040MI") && data["M0040MI"].Answer.IsNotNullOrEmpty() ? " " + data["M0040MI"].Answer : string.Empty %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "";
</script>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Demographics.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/PatientHistory.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/RiskAssessment.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Prognosis.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/SupportiveAssistance.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Sensory.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Pain.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Integumentary.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Respiratory.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Endocrine.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Cardiac.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Elimination.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Nutrition.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/NeuroBehavioral.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/AdlIadl.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/SuppliesDme.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/Medications.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/CareManagement.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/TherapyNeed.ascx", Model); %>
<% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Print/OrdersDiscipline.ascx", Model); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.span("Signature:", true) +
            printview.span("Date", true) +
            printview.span("<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : string.Empty %>", false, 1) +
            printview.span("<%= Model != null && Model.SignatureDate != null && Model.SignatureDate.IsValid() ? Model.SignatureDate.ToShortDateString() : string.Empty %>", false, 1)),
        "Signature");
</script>
</body>
</html>