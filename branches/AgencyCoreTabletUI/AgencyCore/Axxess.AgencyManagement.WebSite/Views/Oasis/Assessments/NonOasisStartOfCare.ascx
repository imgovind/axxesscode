﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<span class="wintitle">Non-OASIS Start of Care | <%= (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "") + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "") %></span>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="vertical-tabs strong">
        <li><a href="#<%= Model.TypeName %>_Demographics">Demographics</a></li>
        <li><a href="#<%= Model.TypeName %>_PatientHistory">Patient History &#38; Diagnoses</a></li>
        <li><a href="#<%= Model.TypeName %>_Prognosis">Prognosis</a></li>
        <li><a href="#<%= Model.TypeName %>_SupportiveAssistance">Supportive Assistance</a></li>
        <li><a href="#<%= Model.TypeName %>_Sensory">Sensory</a></li>
        <li><a href="#<%= Model.TypeName %>_Pain">Pain</a></li>
        <li><a href="#<%= Model.TypeName %>_Integumentary">Integumentary</a></li>
        <li><a href="#<%= Model.TypeName %>_Respiratory">Respiratory</a></li>
        <li><a href="#<%= Model.TypeName %>_Endocrine">Endocrine</a></li>
        <li><a href="#<%= Model.TypeName %>_Cardiac">Cardiac</a></li>
        <li><a href="#<%= Model.TypeName %>_Elimination">Elimination</a></li>
        <li><a href="#<%= Model.TypeName %>_Nutrition">Nutrition</a></li>
        <li><a href="#<%= Model.TypeName %>_NeuroBehavioral">Neuro/Behavioral</a></li>
        <li><a href="#<%= Model.TypeName %>_AdlIadl">ADL/IADLs</a></li>
        <li><a href="#<%= Model.TypeName %>_SuppliesDme">Supplies and DME</a></li>
        <li><a href="#<%= Model.TypeName %>_Medications">Medications</a></li>
        <li><a href="#<%= Model.TypeName %>_TherapyNeed">Therapy Need &#38; Plan of Care</a></li>
        <li><a href="#<%= Model.TypeName %>_OrdersDiscipline">Orders for Discipline &#38; Treatment</a></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="general">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
    <div id="<%= Model.TypeName %>_PatientHistory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Prognosis" class="general loading"></div>
    <div id="<%= Model.TypeName %>_SupportiveAssistance" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Sensory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Pain" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Integumentary" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Respiratory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Endocrine" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Cardiac" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Elimination" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Nutrition" class="general loading"></div>
    <div id="<%= Model.TypeName %>_NeuroBehavioral" class="general loading"></div>
    <div id="<%= Model.TypeName %>_AdlIadl" class="general loading"></div>
    <div id="<%= Model.TypeName %>_SuppliesDme" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Medications" class="general loading"></div>
    <div id="<%= Model.TypeName %>_TherapyNeed" class="general loading"></div>
    <div id="<%= Model.TypeName %>_OrdersDiscipline" class="general loading"></div>
</div>