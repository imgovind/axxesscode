<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var NursingInterventions = data.AnswerArray("485NursingInterventions"); %>
<%  var NursingGoals = data.AnswerArray("485NursingGoals"); %>
<%  if (NursingInterventions.Length > 0 || (data.ContainsKey("485ADLComments") && data["485ADLComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (NursingInterventions.Contains("1")) { %>
    printview.span("Physical therapy to evaluate.") +
    <%  } %>
    <%  if (NursingInterventions.Contains("2")) { %>
    printview.span("Occupational therapy to evaluate.") +
    <%  } %>
    <%  if (NursingInterventions.Contains("3")) { %>
    printview.span("SN to assess/instruct on pain management, proper body mechanics and safety measures.") +
    <%  } %>
    <%  if (NursingInterventions.Contains("4")) { %>
    printview.span("SN to assess for patient adherence to appropriate activity levels.") +
    <%  } %>
    <%  if (NursingInterventions.Contains("5")) { %>
    printview.span("SN to assess patient&#8217;s compliance with home exercise program.") +
    <%  } %>
    <%  if (NursingInterventions.Contains("6")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructRomExcercisePerson","Patient/Caregiver").Clean() %> on proper ROM exercises and body alignment techniques.") +
    <%  } %>
    <%  if (NursingInterventions.Contains("7")) { %>
    printview.span("HHA (freq) <%= data.AnswerOrDefault("485InstructHhaFreq","<span class='blank'></span>").Clean() %> assistance with ADLs/IADLs.") +
    <%  } %>
    <%  if (data.ContainsKey("485ADLComments") && data["485ADLComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485ADLComments").Clean() %>") +
    <%  } %>
    "","ADL Interventions");
<%  } %>
<%  if (NursingGoals.Length > 0 || (data.ContainsKey("485ADLGoalComments") && data["485ADLGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (NursingGoals.Contains("1")) { %>
    printview.span("Patient will have increased mobility, self care, endurance, ROM and decreased pain by the end of the episode.") +
    <%  } %>
    <%  if (NursingGoals.Contains("2")) { %>
    printview.span("Patient will maintain optimal joint function, increased mobility and independence in ADL&#8217;s by the end of the episode.") +
    <%  } %>
    <%  if (NursingGoals.Contains("3")) { %>
    printview.span("Patient&#8217;s strength, endurance and mobility will be improved.") +
    <%  } %>
    <%  if (NursingGoals.Contains("4")) { %>
    printview.span("The <%= data.AnswerOrDefault("485DemonstrateROMExcercisePerson","Patient/Caregiver").Clean() %> will demonstrate proper ROM exercise and body alignment techniques.") +
    <%  } %>
    <%  if (NursingGoals.Contains("5")) { %>
    printview.span("Patient&#8217;s ADL/IADL needs will be met with assistance of HHA.") +
    <%  } %>
    <%  if (data.ContainsKey("485ADLGoalComments") && data["485ADLGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485ADLGoalComments").Clean() %>") +
    <%  } %>
    "","ADL Goals");
<%  } %>