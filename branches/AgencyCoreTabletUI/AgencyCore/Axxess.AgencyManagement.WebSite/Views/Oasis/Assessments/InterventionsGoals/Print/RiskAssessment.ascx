<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var RiskAssessmentInterventions = data.AnswerArray("485RiskAssessmentInterventions"); %>
<%  var RiskAssessmentGoals = data.AnswerArray("485RiskAssessmentGoals"); %>
<%  if (RiskAssessmentInterventions.Length > 0 || (data.ContainsKey("485RiskInterventionComments") && data["485RiskInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (RiskAssessmentInterventions.Contains("1")) { %>
    printview.span("SN to assist patient to obtain ERS button.") +
    <%  } %>
    <%  if (RiskAssessmentInterventions.Contains("2")) { %>
    printview.span("SN to develop individualized emergency plan with patient.") +
    <%  } %>
    <%  if (RiskAssessmentInterventions.Contains("3")) { %>
    printview.span("SN to instruct patient on importance of receiving influenza and pneumococcal vaccines.") +
    <%  } %>
    <%  if (data.ContainsKey("485RiskInterventionComments") && data["485RiskInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485RiskInterventionComments").Clean()%>")
    <%  } %>
    "","Risk Assessment Interventions");
<%  } %>
<%  if (RiskAssessmentGoals.Length > 0 || (data.ContainsKey("485RiskGoalComments") && data["485RiskGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (RiskAssessmentGoals.Contains("1")) { %>
    printview.span("The patient will have no hospitalizations during the episode.") +
    <%  } %>
    <%  if (RiskAssessmentGoals.Contains("2")) { %>
    printview.span("The <%= data.AnswerOrDefault("485VerbalizeEmergencyPlanPerson", "Patient/Caregiver")%> will verbalize understanding of individualized emergency plan by the end of the episode.") +
    <%  } %>
    <%  if (data.ContainsKey("485RiskGoalComments") && data["485RiskGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485RiskGoalComments").Clean()%>") +
    <%  } %>
    "","Risk Assessment Goals");
<%  } %>