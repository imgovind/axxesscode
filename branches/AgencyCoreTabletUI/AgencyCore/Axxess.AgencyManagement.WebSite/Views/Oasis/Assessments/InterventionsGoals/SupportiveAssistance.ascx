<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  if (Model.Discipline == "Nursing") { %>
<%  var data = Model.ToDictionary(); %>
<%  string[] supportiveAssistanceInterventions = data.AnswerArray("485SupportiveAssistanceInterventions"); %>
<fieldset class="loc485">
    <legend>Interventions</legend>
    <%= Html.Hidden(Model.TypeName + "_485SupportiveAssistanceInterventions", " ", new { @id = Model.TypeName + "_485SupportiveAssistanceInterventionsHidden" })%>
    <div class="wide-column">
        <div class="row">
            <div class="wide checkgroup">
                <div class="option">
                    <%= string.Format("<input status='(485 Locator 21) Orders' id='{0}_485SupportiveAssistanceInterventions1' name='{0}_485SupportiveAssistanceInterventions' value='1' type='checkbox' {1} />", Model.TypeName, supportiveAssistanceInterventions.Contains("1").ToChecked())%>
                    <label for="<%= Model.TypeName %>_485SupportiveAssistanceInterventions1" class="radio">MSW to assess psychosocial needs, environment and assist with community referrals and resources.</label>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="<%= Model.TypeName %>_485SupportiveAssistanceComments">Additional Orders:</label>
            <%= Html.Templates(Model.TypeName + "_485SupportiveAssistanceOrderTemplates", new { @class = "templates", @template = "#" + Model.TypeName + "_485SupportiveAssistanceComments" })%>
            <%= Html.TextArea(Model.TypeName + "_485SupportiveAssistanceComments", data.AnswerOrEmptyString("485SupportiveAssistanceComments"), 5, 70, new { @id = Model.TypeName + "_485SupportiveAssistanceComments", @status = "(485 Locator 21) Orders" })%>
        </div>
    </div>
</fieldset>
<%  } %>