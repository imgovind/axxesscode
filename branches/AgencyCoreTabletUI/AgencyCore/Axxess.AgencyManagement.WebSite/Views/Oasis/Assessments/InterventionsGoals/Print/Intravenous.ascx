<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var IntegumentaryIVInterventions = data.AnswerArray("485IntegumentaryIVInterventions"); %>
<%  var IntegumentaryIVGoals = data.AnswerArray("485IntegumentaryIVGoals"); %>
<%  if (IntegumentaryIVInterventions.Length > 0 || (data.ContainsKey("485IntegumentaryIVInterventionComments") && data["485IntegumentaryIVInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (IntegumentaryIVInterventions.Contains("1")) { %>
    printview.span("SN to instruct <%= data.AnswerOrDefault("485InstructInfectionSignsSymptomsPerson", "Patient/Caregiver")%> on signs and symptoms of infection and infiltration.") +
    <%  } %>
    <%  if (IntegumentaryIVInterventions.Contains("2")) { %>
    printview.span("SN to change central line dressing every <%= data.AnswerOrDefault("485ChangeCentralLineEvery", "<span class='blank'></span>")%> using sterile technique.") +
    <%  } %>
    <%  if (IntegumentaryIVInterventions.Contains("3")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructChangeCentralLinePerson", "Patient/Caregiver")%> to change central line dressing every <%= data.AnswerOrDefault("485InstructChangeCentralLineEvery", "<span class='blank'></span>")%> using sterile technique.") +
    <%  } %>
    <%  if (IntegumentaryIVInterventions.Contains("4")) { %>
    printview.span("SN to change <%= data.AnswerOrDefault("485ChangePortDressingType", "<span class='blank'></span>")%> port dressing using sterile technique every <%= data.AnswerOrDefault("485ChangePortDressingEvery", "<span class='blank'></span>")%>.") +
    <%  } %>
    <%  if (data.ContainsKey("485IntegumentaryInterventionComments") && data["485IntegumentaryInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485IntegumentaryIVInterventionComments").Clean()%>")+
    <%  } %>
    "","Intravenous Interventions");
<%  } %>
<%  if (IntegumentaryIVGoals.Length > 0 || (data.ContainsKey("485IntegumentaryIVInterventionComments") && data["485IntegumentaryIVInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (IntegumentaryIVGoals.Contains("1")) { %>
    printview.span("IV will remain patent and free from signs and symptoms of infection.") +
    <%  } %>
    <%  if (IntegumentaryIVGoals.Contains("2")) { %>
    printview.span("The <%= data.AnswerOrDefault("485InstructChangeDress", "Patient/Caregiver")%> will demonstrate understanding of changing <%= data.AnswerOrDefault("485InstructChangeDressSterile", "<span class='blank'></span>")%> dressing using sterile technique.") +
    <%  } %>
    <%  if (data.ContainsKey("485IntegumentaryIVGoalComments") && data["485IntegumentaryIVGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485IntegumentaryIVGoalComments").Clean()%>") +
    <%  } %>
    "","Intravenous Goals");
<%  } %>