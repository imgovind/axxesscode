<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5 && Model.AssessmentTypeNum.ToInteger() != 8) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "FromAgencyPainForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName)%>
    <%= Html.Hidden("categoryType", "EmergentCare")%>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="save next">Save &#38; Continue</a></li>
            <li><a class="save close">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float-right">
            <li><a class="oasis-validate">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <fieldset class="oasis">
        <legend>Emergent Care</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M2300">
                <label class="strong">
                    <a status="More Information about M2300" class="green" onclick="Oasis.Tip('M2300');return false">(M2300)</a>
                    Emergent Care: Since the last time OASIS data were collected, has the patient utilized a hospital emergency department (includes holding/observation)?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2300EmergentCare", "", new { @id = Model.TypeName + "_M2300EmergentCareHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2300EmergentCare", "00", data.AnswerOrEmptyString("M2300EmergentCare").Equals("00"), new { @id = Model.TypeName + "_M2300EmergentCare0" })%>
                        <label for="<%= Model.TypeName %>_M2300EmergentCare0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2300EmergentCare", "01", data.AnswerOrEmptyString("M2300EmergentCare").Equals("01"), new { @id = Model.TypeName + "_M2300EmergentCare1" })%>
                        <label for="<%= Model.TypeName %>_M2300EmergentCare1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes, used hospital emergency department WITHOUT hospital admission</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2300EmergentCare", "02", data.AnswerOrEmptyString("M2300EmergentCare").Equals("02"), new { @id = Model.TypeName + "_M2300EmergentCare2" })%>
                        <label for="<%= Model.TypeName %>_M2300EmergentCare2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Yes, used hospital emergency department WITH hospital admission</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2300EmergentCare", "UK", data.AnswerOrEmptyString("M2300EmergentCare").Equals("UK"), new { @id = Model.TypeName + "_M2300EmergentCareUK" })%>
                        <label for="<%= Model.TypeName %>_M2300EmergentCareUK">
                            <span class="float-left">UK &#8211;</span>
                            <span class="normal margin">Unknown</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M2300')">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M2310">
                <label class="strong">
                    <a status="More Information about M2310" class="green" onclick="Oasis.Tip('M2310');return false">(M2310)</a>
                    Reason for Emergent Care: For what reason(s) did the patient receive emergent care (with or without hospitalization)?
                    <em>(Mark all that apply)</em>
                </label>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareMed", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareMedHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareMed' class='M2310' name='{0}_M2310ReasonForEmergentCareMed' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareMed").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareMed">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Improper medication administration, medication side effects, toxicity, anaphylaxis</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareFall", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareFallHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareFall' class='M2310' name='{0}_M2310ReasonForEmergentCareFall' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareFall").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareFall">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Injury caused by fall</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareResInf", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareResInfHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareResInf' class='M2310' name='{0}_M2310ReasonForEmergentCareResInf' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareResInf").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareResInf">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Respiratory infection (e.g., pneumonia, bronchitis)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareOtherResInf", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareOtherResInfHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareOtherResInf' class='M2310' name='{0}_M2310ReasonForEmergentCareOtherResInf' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareOtherResInf").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareOtherResInf">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Other respiratory problem</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareHeartFail", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareHeartFailHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareHeartFail' class='M2310' name='{0}_M2310ReasonForEmergentCareHeartFail' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareHeartFail").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareHeartFail">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">Heart failure (e.g., fluid overload)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareCardiac", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareCardiacHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareCardiac' class='M2310' name='{0}_M2310ReasonForEmergentCareCardiac' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareCardiac").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareCardiac">
                            <span class="float-left">6 &#8211;</span>
                            <span class="normal margin">Cardiac dysrhythmia (irregular heartbeat)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareMyocardial", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareMyocardialHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareMyocardial' class='M2310' name='{0}_M2310ReasonForEmergentCareMyocardial' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareMyocardial").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareMyocardial">
                            <span class="float-left">7 &#8211;</span>
                            <span class="normal margin">Myocardial infarction or chest pain</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareHeartDisease", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareHeartDiseaseHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareHeartDisease' class='M2310' name='{0}_M2310ReasonForEmergentCareHeartDisease' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareHeartDisease").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareHeartDisease">
                            <span class="float-left">8 &#8211;</span>
                            <span class="normal margin">Other heart disease</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareStroke", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareStrokeHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareStroke' class='M2310' name='{0}_M2310ReasonForEmergentCareStroke' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareStroke").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareStroke">
                            <span class="float-left">9 &#8211;</span>
                            <span class="normal margin">Stroke (CVA) or TIA</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareHypo", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareHypoHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareHypo' class='M2310' name='{0}_M2310ReasonForEmergentCareHypo' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareHypo").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareHypo">
                            <span class="float-left">10 &#8211;</span>
                            <span class="normal margin">Hypo/Hyperglycemia, diabetes out of control</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareGI", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareGIHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareGI' class='M2310' name='{0}_M2310ReasonForEmergentCareGI' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareGI").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareGI">
                            <span class="float-left">11 &#8211;</span>
                            <span class="normal margin">GI bleeding, obstruction, constipation, impaction</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareDehMal", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareDehMalHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareDehMal' class='M2310' name='{0}_M2310ReasonForEmergentCareDehMal' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareDehMal").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareDehMal">
                            <span class="float-left">12 &#8211;</span>
                            <span class="normal margin">Dehydration, malnutrition</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareUrinaryInf", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareUrinaryInfHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareUrinaryInf' class='M2310' name='{0}_M2310ReasonForEmergentCareUrinaryInf' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareUrinaryInf").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareUrinaryInf">
                            <span class="float-left">13 &#8211;</span>
                            <span class="normal margin">Urinary tract infection</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareIV", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareIVHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareIV' class='M2310' name='{0}_M2310ReasonForEmergentCareIV' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareIV").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareIV">
                            <span class="float-left">14 &#8211;</span>
                            <span class="normal margin">IV catheter-related infection or complication</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareWoundInf", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareWoundInfHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareWoundInf' class='M2310' name='{0}_M2310ReasonForEmergentCareWoundInf' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareWoundInf").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareWoundInf">
                            <span class="float-left">15 &#8211;</span>
                            <span class="normal margin">Wound infection or deterioration</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareUncontrolledPain", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareUncontrolledPainHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareUncontrolledPain' class='M2310' name='{0}_M2310ReasonForEmergentCareUncontrolledPain' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareUncontrolledPain").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareUncontrolledPain">
                            <span class="float-left">16 &#8211;</span>
                            <span class="normal margin">Uncontrolled pain</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareMental", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareMentalHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareMental' class='M2310' name='{0}_M2310ReasonForEmergentCareMental' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareMental").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareMental">
                            <span class="float-left">17 &#8211;</span>
                            <span class="normal margin">Acute mental/behavioral health problem</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareDVT", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareDVTHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareDVT' class='M2310' name='{0}_M2310ReasonForEmergentCareDVT' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareDVT").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareDVT">
                            <span class="float-left">18 &#8211;</span>
                            <span class="normal margin">Deep vein thrombosis, pulmonary embolus</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareOther", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareOtherHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareOther' class='M2310' name='{0}_M2310ReasonForEmergentCareOther' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareOther").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareOther">
                            <span class="float-left">19 &#8211;</span>
                            <span class="normal margin">Other than above reasons</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M2310ReasonForEmergentCareUK", "", new { @id = Model.TypeName + "_M2310ReasonForEmergentCareUKHidden" })%>
                        <%= string.Format("<input id='{0}_M2310ReasonForEmergentCareUK' class='radio float-left' name='{0}_M2310ReasonForEmergentCareUK' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M2310ReasonForEmergentCareUK").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M2310ReasonForEmergentCareUK">
                            <span class="float-left">UK &#8211;</span>
                            <span class="normal margin">Reason unknown</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M2310')">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="save next">Save &#38; Continue</a></li>
            <li><a class="save close">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float-right">
            <li><a class="oasis-validate">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>
