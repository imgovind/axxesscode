<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<div class="wrapper main">
<%  if ((Model.AssessmentTypeNum.ToInteger() < 5 || Model.AssessmentTypeNum.ToInteger() == 11 || Model.AssessmentTypeNum.ToInteger() == 14) && Current.HasRight(Permissions.ViewPreviousNotes)) { %>
    <fieldset>
        <legend>Previous Assessments</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Select Assessment:</label>
                <div class="float-right">
                    <%= Html.PreviousAssessments(Model.PatientId, Model.Id, Model.AssessmentTypeNum.ToInteger(), new { @id = Model.TypeName + "_PreviousAssessments" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="buttons" style="text-align: left;">
                    <ul>
                        <li><a class="load-previous">Load Assessment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </fieldset>
<%  } %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "DemographicsForm" })) { %>
    <%  var data = Model.ToDictionary(); %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id, new { @id = Model.TypeName + "_Id" })%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit", new { @id = Model.TypeName + "_Action" })%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId, new { @id = Model.TypeName + "_PatientGuid" })%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId, new { @id = Model.TypeName + "_EpisodeId" })%>
    <%= Html.Hidden("assessment", Model.TypeName, new { @id = Model.TypeName + "_AssessmentType" })%>
    <%= Html.Hidden("categoryType", "Demographics")%> 
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="save next">Save &#38; Continue</a></li>
            <li><a class="save close">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float-right">
            <li><a class="oasis-validate">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <fieldset class="oasis loc485">
        <legend>Patient Information</legend>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0020">
                <label for="<%= Model.TypeName %>_M0020PatientIdNumber" class="float-left">
                    <a status="More Information about M0020" class="green" onclick="Oasis.Tip('M0020');return false">(M0020)</a>
                    ID Number:
                </label>
                <div class="float-right oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0020PatientIdNumber", data.AnswerOrEmptyString("M0020PatientIdNumber"), new { @id = Model.TypeName + "_M0020PatientIdNumber", @status = "(OASIS M0020) ID Number", @maxlength = "15" }) %>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0020')" status="More Information about M0020">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0040">
                <label for="<%= Model.TypeName %>_M0040FirstName" class="float-left">
                    <a status="More Information about M0040" class="green" onclick="Oasis.Tip('M0040');return false">(M0040)</a>
                    First Name:
                </label>
                <div class="float-right oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0040FirstName", data.AnswerOrEmptyString("M0040FirstName"), new { @id = Model.TypeName + "_M0040FirstName", @status = "(OASIS M0040) First Name" }) %>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0040')" status="More Information about M0040">?</div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_M0040MI" class="float-left">
                    <a status="More Information about M0040" class="green" onclick="Oasis.Tip('M0040');return false">(M0040)</a>
                    MI:
                </label>
                <div class="float-right oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0040MI", data.AnswerOrEmptyString("M0040MI"), new { @id = Model.TypeName + "_M0040MI", @status = "(OASIS M0040) Middle Initial", @class = "mi", @maxlength = "1" })%>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0040')" status="More Information about M0040">?</div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_M0040LastName" class="float-left">
                    <a status="More Information about M0040" class="green" onclick="Oasis.Tip('M0040');return false">(M0040)</a>
                    Last Name:
                </label>
                <div class="float-right oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0040LastName", data.AnswerOrEmptyString("M0040LastName"), new { @id = Model.TypeName + "_M0040LastName", @status = "(OASIS M0040) Last Name" })%>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0040')" status="More Information about M0040">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0050">
                <label for="<%= Model.TypeName %>_M0040Suffix" class="float-left">
                    <a status="More Information about M0040" class="green" onclick="Oasis.Tip('M0040');return false">(M0040)</a>
                    Suffix:
                </label>
                <div class="float-right oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0040Suffix", data.AnswerOrEmptyString("M0040Suffix"), new { @id = Model.TypeName + "_M0040Suffix", @status = "(OASIS M0040) Suffix", @class = "mi", @maxlength = "4" })%>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0040')" status="More Information about M0040">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0060">
                <label for="<%= Model.TypeName %>_M0050PatientState" class="float-left">
                    <a status="More Information about M0050" class="green" onclick="Oasis.Tip('M0050');return false">(M0050)</a>
                    State,
                    <a status="More Information about M0060" class="green" onclick="Oasis.Tip('M0060');return false">(M0060)</a>
                    Zip:
                </label>
                <div class="float-right oasis">
                    <%= Html.LookupSelectList(SelectListTypes.States, Model.TypeName + "_M0050PatientState", data.AnswerOrEmptyString("M0050PatientState"), new { @id = Model.TypeName + "_M0050PatientState", @status = "(OASIS M0050) State", @class = "AddressStateCode" })%>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0050')" status="More Information about M0050">?</div>
                    <%= Html.TextBox(Model.TypeName + "_M0060PatientZipCode", data.AnswerOrEmptyString("M0060PatientZipCode"), new { @id = Model.TypeName + "_M0060PatientZipCode", @status = "(OASIS M0060) Zip Code", @class = "zip numeric", @maxlength = "5" })%>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0060')" status="More Information about M0060">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0069">
                <label class="float-left">
                    <a status="More Information about M0069" class="green" onclick="Oasis.Tip('M0069');return false">(M0069)</a>
                    Gender:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M0069Gender", " ", new { }) %>
                <div class="float-right oasis">
                    <%= Html.RadioButton(Model.TypeName + "_M0069Gender", "1", data.AnswerOrEmptyString("M0069Gender").Equals("1"), new { @id = Model.TypeName + "_M0069GenderMale", @status = "(OASIS M0069) Gender, Male", @class = "radio" })%>
                    <label for="<%= Model.TypeName %>_M0069GenderMale" class="inline-radio">Male</label>
                    <%= Html.RadioButton(Model.TypeName + "_M0069Gender", "2", data.AnswerOrEmptyString("M0069Gender").Equals("2"), new { @id = Model.TypeName + "_M0069GenderFemale", @status = "(OASIS M0069) Gender, Female", @class = "radio" })%>
                    <label for="<%= Model.TypeName %>_M0069GenderFemale" class="inline-radio">Female</label>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0069')" status="More Information about M0069">?</div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0064">
                <label for="<%= Model.TypeName %>_M0064PatientSSN" class="float-left">
                    <a status="More Information about M0064" class="green" onclick="Oasis.Tip('M0064');return false">(M0064)</a>
                    Social Security Number:
                </label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_M0064PatientSSN", data.AnswerOrEmptyString("M0064PatientSSN"), new { @id = Model.TypeName + "_M0064PatientSSN", @status = "(OASIS M0064) Social Security Number", @maxlength = "9" })%>
                </div>
                <div class="clear"></div>
                <div class="align-right">
                    <%= Html.Hidden(Model.TypeName + "_M0064PatientSSNUnknown", " ", new { @id = Model.TypeName + "_M0064PatientSSNUnknownHidden" })%>
                    <%= string.Format("<input status='(OASIS M0064) Social Security Number, Unknown' id='{0}_M0064PatientSSNUnknown' name='{0}_M0064PatientSSNUnknown' class='radio' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0064PatientSSNUnknown").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0064PatientSSNUnknown">UK &#8211; Unknown or Not Available</label>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0064')" status="More Information about M0064">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0063">
                <label for="<%= Model.TypeName %>_M0063PatientMedicareNumber" class="float-left">
                    <a status="More Information about M0063" class="green" onclick="Oasis.Tip('M0063');return false">(M0063)</a>
                    Medicare Number:
                </label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_M0063PatientMedicareNumber", data.AnswerOrEmptyString("M0063PatientMedicareNumber"), new { @id = Model.TypeName + "_M0063PatientMedicareNumber", @status = "(OASIS M0063) Medicare Number", @maxlength = "11" })%>
                </div>
                <div class="clear"></div>
                <div class="align-right">
                    <%= Html.Hidden(Model.TypeName + "_M0063PatientMedicareNumberUnknown", " ", new { @id = Model.TypeName + "_M0063PatientMedicareNumberUnknownHidden" })%>
                    <%= string.Format("<input status='(OASIS M0063) Medicare Number, Not Applicable' id='{0}_M0063PatientMedicareNumberUnknown' name='{0}_M0063PatientMedicareNumberUnknown' class='radio' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0063PatientMedicareNumberUnknown").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0063PatientMedicareNumberUnknown" >NA &#8211; No Medicare</label>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0063')" status="More Information about M0063">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0065">
                <label for="<%= Model.TypeName %>_M0065PatientMedicaidNumber" class="float-left">
                    <a status="More Information about M0065" class="green" onclick="Oasis.Tip('M0065');return false">(M0065)</a>
                    Medicaid Number:
                </label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_M0065PatientMedicaidNumber", data.AnswerOrEmptyString("M0065PatientMedicaidNumber"), new { @id = Model.TypeName + "_M0065PatientMedicaidNumber", @status = "(OASIS M0065) Medicaid Number", @maxlength = "14" })%>
                </div>
                <div class="clear"></div>
                <div class="align-right">
                    <%= Html.Hidden(Model.TypeName + "_M0065PatientMedicaidNumberUnknown", " ", new { @id = Model.TypeName + "_M0065PatientMedicaidNumberUnknownHidden" })%>
                    <%= string.Format("<input status='(OASIS M0063) Medicaid Number, Not Applicable' id='{0}_M0065PatientMedicaidNumberUnknown' name='{0}_M0065PatientMedicaidNumberUnknown' class='radio' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0065PatientMedicaidNumberUnknown").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0065PatientMedicaidNumberUnknown">NA &#8211; No Medicaid</label>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0065')" status="More Information about M0065">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0066">
                <label for="<%= Model.TypeName %>_M0066PatientDoB" class="float-left">
                    <a status="More Information about M0066" class="green" onclick="Oasis.Tip('M0066');return false">(M0066)</a>
                    Birth Date:
                </label>
                <div class="float-right oasis">
                    <input type="date" name="<%= Model.TypeName %>_M0066PatientDoB" value="<%= data.AnswerOrEmptyString("M0066PatientDoB") %>" id="<%= Model.TypeName %>_M0066PatientDoB" status="(OASIS M0066) Birth Date" />
                    <div class="oasis-tip" onclick="Oasis.Tip('M0066')" status="More Information about M0066">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis loc485">
        <legend>Episode Information</legend>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0030">
                <label for="<%= Model.TypeName %>_M0030SocDate" class="float-left">
                    <a status="More Information about M0030" class="green" onclick="Oasis.Tip('M0030');return false">(M0030)</a>
                    Start of Care Date:
                </label>
                <div class="float-right oasis">
                    <input type="date" name="<%= Model.TypeName %>_M0030SocDate" value="<%= data.AnswerOrEmptyString("M0030SocDate") %>" id="<%= Model.TypeName %>_M0030SocDate" status="(OASIS M0030) Start of Care Date" />
                    <div class="oasis-tip" onclick="Oasis.Tip('M0030')" status="More Information about M0030">?</div>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericEpisodeStartDate" class="float-left">Episode Start Date:</label>
                <div class="float-right">
                    <input type="date" name="<%= Model.TypeName %>_GenericEpisodeStartDate" value="<%= data.AnswerOrEmptyString("GenericEpisodeStartDate") %>" id="<%= Model.TypeName %>_GenericEpisodeStartDate" status="(Optional) Episode Start Date" />
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0032">
                <label for="<%= Model.TypeName %>_M0032ROCDate" class="float-left">
                    <a status="More Information about M0032" class="green" onclick="Oasis.Tip('M0032');return false">(M0032)</a>
                    Resumption of Care Date:
                </label>
                <div class="float-right">
                    <input type="date" name="<%= Model.TypeName %>_M0032ROCDate" value="<%= data.AnswerOrEmptyString("M0032ROCDate") %>" id="<%= Model.TypeName %>_M0032ROCDate" status="(OASIS M0032) Resumption of Care Date" />
                </div>
                <div class="clear"></div>
                <div class="align-right">
                    <%= Html.Hidden(Model.TypeName + "_M0032ROCDateNotApplicable", " ", new { @id = Model.TypeName + "_M0032ROCDateNotApplicableHidden" })%>
                    <%= string.Format("<input status='(M0032) Resumption of Care Date, Not Applicable' id='{0}_M0032ROCDateNotApplicable' name='{0}_M0032ROCDateNotApplicable' class='radio' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0032ROCDateNotApplicable").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0032ROCDateNotApplicable">NA &#8211; Not Applicable</label>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0032')" status="More Information about M0032">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0090">
                <label for="<%= Model.TypeName %>_M0090AssessmentCompleted" class="float-left">
                    <a status="More Information about M0090" class="green" onclick="Oasis.Tip('M0090');return false">(M0090)</a>
                    Date Assessment Completed:
                </label>
                <div class="float-right oasis">
                    <input type="date" name="<%= Model.TypeName %>_M0090AssessmentCompleted" value="<%= data.AnswerOrEmptyString("M0090AssessmentCompleted") %>" id="<%= Model.TypeName %>_M0090AssessmentCompleted" status="(OASIS M0090) Date Assessment Completed" />
                    <div class="oasis-tip" onclick="Oasis.Tip('M0090')" status="More Information about M0090">?</div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row" id="<%= Model.TypeName %>_M0080">
                <label for="<%= Model.TypeName %>_M0080DisciplinePerson" class="float-left">
                    <a status="More Information about M0080" class="green" onclick="Oasis.Tip('M0080');return false">(M0080)</a>
                    Discipline of Person Completing Assessment:
                </label>
                <div class="float-right oasis">
                    <%= Html.Hidden(Model.TypeName + "_M0080DisciplinePerson", "", new { @id = Model.TypeName + "_M0080DisciplinePersonTitle" })%>
                    <%  var DisciplinePersonCompletingAssessment = new SelectList(new[] {
                            new SelectListItem { Text = "1 - RN", Value = "01" },
                            new SelectListItem { Text = "2 - PT", Value = "02" },
                            new SelectListItem { Text = "3 - SLP/ST", Value = "03"},
                            new SelectListItem { Text = "4 - OT", Value = "04" }
                        }, "Value", "Text", data.AnswerOrDefault("M0080DisciplinePerson", "01")); %>
                    <%= Html.DropDownList(Model.TypeName + "_M0080DisciplinePerson", DisciplinePersonCompletingAssessment)%>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0080')" status="More Information about M0080">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0010">
                <label for="<%= Model.TypeName %>_M0010CertificationNumber" class="float-left">
                    <a status="More Information about M0010" class="green" onclick="Oasis.Tip('M0010');return false">(M0010)</a>
                    CMS Certification Number:
                </label>
                <div class="float-right oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0010CertificationNumber", data.AnswerOrEmptyString("M0010CertificationNumber"), new { @id = Model.TypeName + "_M0010CertificationNumber", @status = "(OASIS M0010) CMS Certification Number" })%>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0010')" status="More Information about M0010">?</div>
                </div>
            </div>
            <div class="row hidden" id="<%= Model.TypeName %>_M0014">
                <label for="<%= Model.TypeName %>_M0014BranchState" class="float-left">
                    <a status="More Information about M0014" class="green" onclick="Oasis.Tip('M0014');return false">(M0014)</a>
                    Branch State:
                </label>
                <div class="float-right oasis">
                    <%= Html.LookupSelectList(SelectListTypes.States, Model.TypeName + "_M0014BranchState", data.AnswerOrEmptyString("M0014BranchState"), new { @id = Model.TypeName + "_M0014BranchState", @status = "(OASIS M0014) Branch State", @class = "AddressStateCode" })%>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0014')" status="More Information about M0014">?</div>
                </div>
            </div>
            <div class="row hidden" id="<%= Model.TypeName %>_M0016">
                <label for="<%= Model.TypeName %>_M0016BranchId" class="float-left">
                    <a status="More Information about M0016" class="green" onclick="Oasis.Tip('M0016');return false">(M0016)</a>
                    Branch ID Number:
                </label>
                <div class="float-right oasis">
                    <%= Html.TextBox(Model.TypeName + "_M0016BranchId", data.AnswerOrEmptyString("M0016BranchId"), new { @id = Model.TypeName + "_M0016BranchId", @status = "(OASIS M0016) Branch ID Number" })%>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0016')" status="More Information about M0016">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0018">
                <label for="<%= Model.TypeName %>_M0018NationalProviderId" class="float-left">
                    <a status="More Information about M0018" class="green" onclick="Oasis.Tip('M0018');return false">(M0018)</a>
                    Physician NPI Number:
                </label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_M0018NationalProviderId", data.AnswerOrEmptyString("M0018NationalProviderId"), new { @id = Model.TypeName + "_M0018NationalProviderId", @status = "(OASIS M0018) National Provider Identifier" })%>
                </div>
                <div class="clear"></div>
                <div class="align-right">
                    <%= Html.Hidden(Model.TypeName + "_M0018NationalProviderIdUnknown", " ", new { @id = Model.TypeName + "_M0018NationalProviderIdUnknownHidden" })%>
                    <%= string.Format("<input status='(M0018) National Provider Identifier, Unknown' id='{0}_M0018NationalProviderIdUnknown' name='{0}_M0018NationalProviderIdUnknown' type='checkbox' class='radio' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0018NationalProviderIdUnknown").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0018NationalProviderIdUnknown">UK &#8211; Unknown or Not Available</label>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0018')" status="More Information about M0018">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    <fieldset class="oasis<%= Model.AssessmentTypeNum.ToInteger() < 6 ? " half float-left" : string.Empty %>">
        <legend>Assessment Information</legend>
        <div class="<%= Model.AssessmentTypeNum.ToInteger() > 5 ? "wide_" : string.Empty %>column" id="<%= Model.TypeName %>_M0100">
            <%= Html.Hidden(Model.TypeName + "_M0100AssessmentType", Model.AssessmentTypeNum, new { @id = Model.TypeName + "_M0100AssessmentTypeHidden" })%>
            <div class="row strong">
                <a status="More Information about M0100" class="green" onclick="Oasis.Tip('M0100');return false">(M0100)</a>
                Reason for this Assessment:
            </div>
            <div class="row">
                <div>Start/Resumption of Care:</div>
                <div>
                    <%= string.Format("<input status='(OASIS M0100) Reason for this Assessment, Start of Care' id='{0}_M0100AssessmentType1' name='{0}_M0100AssessmentType' type='radio' class='radio float-left' {1} />", Model.TypeName, Model.AssessmentTypeNum.Equals("01").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0100AssessmentType1">
                        <span class="float-left">1 &#8211;</span>
                        <span class="normal margin">Start of care&#8212;further visits planned</span>
                    </label>
                </div>
                <div>
                    <%= string.Format("<input status='(OASIS M0100) Reason for this Assessment, Resumption of Care' id='{0}_M0100AssessmentType3' name='{0}_M0100AssessmentType' type='radio' class='radio float-left' {1} />", Model.TypeName, Model.AssessmentTypeNum.Equals("03").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0100AssessmentType3">
                        <span class="float-left">3 &#8211;</span>
                        <span class="normal margin">Resumption of care (after inpatient stay)</span>
                    </label>
                </div>
            </div>
            <div class="row">
                <div>Follow-Up:</div>
                <div>
                    <%= string.Format("<input status='(OASIS M0100) Reason for this Assessment, Recertification' id='{0}_M0100AssessmentType4' name='{0}_M0100AssessmentType' type='radio' class='radio float-left' {1} />", Model.TypeName, Model.AssessmentTypeNum.Equals("04").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0100AssessmentType4">
                        <span class="float-left">4 &#8211;</span>
                        <span class="normal margin">Recertification (follow-up) reassessment</span>
                    </label>
                </div>
                <div>
                    <%= string.Format("<input status='(OASIS M0100) Reason for this Assessment, Other Follow-Up' id='{0}_M0100AssessmentType5' name='{0}_M0100AssessmentType' type='radio' class='radio float-left' {1} />", Model.TypeName, Model.AssessmentTypeNum.Equals("05").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0100AssessmentType5">
                        <span class="float-left">5 &#8211;</span>
                        <span class="normal margin">Other follow-up</span>
                    </label>
                </div>
            </div>
            <div class="row">
                <div>Transfer to an Inpatient Facility:</div>
                <div>
                    <%= string.Format("<input status='(OASIS M0100) Reason for this Assessment, Transferred, but not Discharged from Agency' id='{0}_M0100AssessmentType6' name='{0}_M0100AssessmentType' type='radio' class='radio float-left' {1} />", Model.TypeName, Model.AssessmentTypeNum.Equals("06").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0100AssessmentType6">
                        <span class="float-left">6 &#8211;</span>
                        <span class="normal margin">Transferred to an inpatient facility&#8212;patient not discharged from agency</span>
                    </label>
                </div>
                <div>
                    <%= string.Format("<input status='(OASIS M0100) Reason for this Assessment, Transferred and Discharged from Agency' id='{0}_M0100AssessmentType7' name='{0}_M0100AssessmentType' type='radio' class='radio float-left' {1} />", Model.TypeName, Model.AssessmentTypeNum.Equals("07").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0100AssessmentType7">
                        <span class="float-left">7 &#8211;</span>
                        <span class="normal margin">Transferred to an inpatient facility&#8212;patient discharged from agency</span>
                    </label>
                </div>
            </div>
            <div class="row">
                <div>Discharge from Agency &#8212; Not to an Inpatient Facility:</div>
                <div>
                    <%= string.Format("<input status='(OASIS M0100) Reason for this Assessment, Death at Home' id='{0}_M0100AssessmentType8' name='{0}_M0100AssessmentType' type='radio' class='radio float-left' {1} />", Model.TypeName, Model.AssessmentTypeNum.Equals("08").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0100AssessmentType8">
                        <span class="float-left">8 &#8211;</span>
                        <span class="normal margin">Death at home</span>
                    </label>
                </div>
                <div>
                    <%= string.Format("<input status='(OASIS M0100) Reason for this Assessment, Discharge from Agency' id='{0}_M0100AssessmentType9' name='{0}_M0100AssessmentType' type='radio' class='radio float-left' {1} />", Model.TypeName, Model.AssessmentTypeNum.Equals("09").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0100AssessmentType9">
                        <span class="float-left">9 &#8211;</span>
                        <span class="normal margin">Discharge from agency</span>
                    </label>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M0100');" status="More Information about M0100">?</div>
                </div>
            </div>
        </div>
    </fieldset>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 6) { %>
    <fieldset class="oasis half float-right">
        <legend>Dates/Timing</legend>
        <div class="column">
        <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
            <div class="row" id="<%= Model.TypeName %>_M0102">
                <label for="<%= Model.TypeName %>_M0102PhysicianOrderedDate" class="strong">
                    <a status="More Information about M0102" class="green" onclick="Oasis.Tip('M0102');return false">(M0102)</a>
                    Date of Physician-ordered Start/Resumption of Care:
                </label>
                <p>
                    If the physician indicated a specific start of care (resumption of care) date when the patient was referred for home
                    health services, record the date specified.
                </p>
                <div class="float-right" id="<%= Model.TypeName %>_M0102PhysicianOrderedDateDiv">
                    <input type="date" name="<%= Model.TypeName %>_M0102PhysicianOrderedDate" value="<%= data.AnswerOrEmptyString("M0102PhysicianOrderedDate") %>" id="<%= Model.TypeName %>_M0102PhysicianOrderedDate" status="(OASIS M0102) Date of Physician-ordered Start/Resumption of Care" />
                </div>
                <div class="clear"></div>
                <div class="align-right">
                    <%= Html.Hidden(Model.TypeName + "_M0102PhysicianOrderedDateNotApplicable", " ", new { @id = Model.TypeName + "_M0102PhysicianOrderedDateNotApplicableHidden" })%>
                    <%= string.Format("<input status='(OASIS M0102) Date of Physician-ordered Start/Resumption of Care, Not Applicable' id='{0}_M0102PhysicianOrderedDateNotApplicable' name='{0}_M0102PhysicianOrderedDateNotApplicable' class='radio' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0102PhysicianOrderedDateNotApplicable").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M0102PhysicianOrderedDateNotApplicable">NA &#8211; No specific SOC date ordered by physician</label>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0102')" status="More Information about M0102">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M0104">
                <label for="<%= Model.TypeName %>_M0104ReferralDate" class="strong">
                    <a status="More Information about M0104" class="green" onclick="Oasis.Tip('M0104');return false">(M0104)</a>
                    Date of Referral:
                </label>
                <p>Indicate the date that the written or verbal referral for initiation or resumption of care was received by the HHA.</p>
                <div class="float-right oasis">
                    <input type="date" name="<%= Model.TypeName %>_M0104ReferralDate" value="<%= data.AnswerOrEmptyString("M0104ReferralDate") %>" id="<%= Model.TypeName %>_M0104ReferralDate" status="(OASIS M0104) Date of Referral" />
                    <div class="oasis-tip" onclick="Oasis.Tip('M0104')" status="More Information about M0104">?</div>
                </div>
            </div>
        <%  } %>
            <div class="row" id="<%= Model.TypeName %>_M0110">
                <label for="<%= Model.TypeName %>_M0110EpisodeTiming" class="strong">
                    <a status="More Information about M0110" class="green" onclick="Oasis.Tip('M0110');return false">(M0110)</a>
                    Episode Timing:
                </label>
                <p>
                    Is the Medicare home health payment episode for which this assessment will define a case mix group an &#8220;early&#8221; episode
                    or a &#8220;later&#8221; episode in the patient&#8217;s current sequence of adjacent Medicare home health payment episodes?
                </p>
                <div class="float-right oasis">
                    <%= Html.Hidden(Model.TypeName + "_M0110EpisodeTiming", "", new { @id = Model.TypeName + "_M0110EpisodeTimingHidden" })%>
                    <%  var EpisodeTiming = new SelectList(new[] {
                            new SelectListItem { Text = "Early", Value = "01" },
                            new SelectListItem { Text = "Later", Value = "02" },
                            new SelectListItem { Text = "Unknown", Value = "UK"},
                            new SelectListItem { Text = "Not Applicable/No Medicare case mix group", Value = "NA" }
                        }, "Value", "Text", data.AnswerOrDefault("M0110EpisodeTiming", "01")); %>
                    <%= Html.DropDownList(Model.TypeName + "_M0110EpisodeTiming", EpisodeTiming)%>
                    <div class="oasis-tip" onclick="Oasis.Tip('M0110')" status="More Information about M0110">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
        <%  } %>
    <%  } %>
    <%  if (Model.AssessmentTypeNum != "08") { %>
    <fieldset class="oasis">
        <legend>Race/Ethnicity</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>"_M0140">
                <label class="strong">
                    <a status="More Information about M0140" class="green" onclick="Oasis.Tip('M0140');return false">(M0140)</a>
                    Race/Ethnicity (Mark all that apply)
                </label>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0140RaceAMorAN", " ", new { @id = Model.TypeName + "_M0140RaceAMorANHidden" })%>
                        <%= string.Format("<input status='(M0140) Race/Ethnicity, American Indian or Alaska Native' id='{0}_M0140RaceAMorAN' name='{0}_M0140RaceAMorAN' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0140RaceAMorAN").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0140RaceAMorAN">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">American Indian or Alaska Native</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0140RaceAsia", " ", new { @id = Model.TypeName + "_M0140RaceAsiaHidden" })%>
                        <%= string.Format("<input status='(M0140) Race/Ethnicity, Asian' id='{0}_M0140RaceAsia' name='{0}_M0140RaceAsia' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0140RaceAsia").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0140RaceAsia">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Asian</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0140RaceBalck", " ", new { @id = Model.TypeName + "_M0140RaceBalckHidden" })%>
                        <%= string.Format("<input status='(M0140) Race/Ethnicity, Black or African-American' id='{0}_M0140RaceBalck' name='{0}_M0140RaceBalck' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0140RaceBalck").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0140RaceBalck">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Black or African-American</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0140RaceHispanicOrLatino", " ", new { @id = Model.TypeName + "_M0140RaceHispanicOrLatinoHidden" })%>
                        <%= string.Format("<input status='(M0140) Race/Ethnicity, Hispanic or Latino' id='{0}_M0140RaceHispanicOrLatino' name='{0}_M0140RaceHispanicOrLatino' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0140RaceHispanicOrLatino").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0140RaceHispanicOrLatino">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Hispanic or Latino</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0140RaceNHOrPI", " ", new { @id = Model.TypeName + "_M0140RaceNHOrPIHidden" })%>
                        <%= string.Format("<input status='(M0140) Race/Ethnicity, Native Hawaiian or Pacific Islander' id='{0}_M0140RaceNHOrPI' name='{0}_M0140RaceNHOrPI' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0140RaceNHOrPI").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0140RaceNHOrPI">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">Native Hawaiian or Pacific Islander</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0140RaceWhite", " ", new { @id = Model.TypeName + "_M0140RaceWhiteHidden" })%>
                        <%= string.Format("<input status='(M0140) Race/Ethnicity, White' id='{0}_M0140RaceWhite' name='{0}_M0140RaceWhite' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0140RaceWhite").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0140RaceWhite">
                            <span class="float-left">6 &#8211;</span>
                            <span class="normal margin">White</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M0140')" status="More Information about M0140">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Payment Source</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_M0150">
                <label class="strong"><a status="More Information about M0150" class="green" onclick="Oasis.Tip('M0150');return false">(M0150)</a> Payment Source (Mark all that apply)</label>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceNone", " ", new { @id = Model.TypeName + "_M0150PaymentSourceNoneHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, None' id='{0}_M0150PaymentSourceNone' name='{0}_M0150PaymentSourceNone' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceNone").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourceNone">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">None; no charge for current services</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceMCREFFS", " ", new { @id = Model.TypeName + "_M0150PaymentSourceMCREFFSHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Traditional Medicare' id='{0}_M0150PaymentSourceMCREFFS' name='{0}_M0150PaymentSourceMCREFFS' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourceMCREFFS">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Medicare (traditional fee-for-service)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceMCREHMO", " ", new { @id = Model.TypeName + "_M0150PaymentSourceMCREHMOHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Medicare' id='{0}_M0150PaymentSourceMCREHMO' name='{0}_M0150PaymentSourceMCREHMO' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourceMCREHMO">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Medicare (HMO/managed care/Advantage plan)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceMCAIDFFS", " ", new { @id = Model.TypeName + "_M0150PaymentSourceMCAIDFFSHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Traditional Medicaid' id='{0}_M0150PaymentSourceMCAIDFFS' name='{0}_M0150PaymentSourceMCAIDFFS' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourceMCAIDFFS">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Medicaid (traditional fee-for-service)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceMACIDHMO", " ", new { @id = Model.TypeName + "_M0150PaymentSourceMACIDHMOHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Medicaid' id='{0}_M0150PaymentSourceMACIDHMO' name='{0}_M0150PaymentSourceMACIDHMO' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourceMACIDHMO">
                            <span class="float-left">4 &#8211;</span>
                            <span class="normal margin">Medicaid (HMO/managed care)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceWRKCOMP", " ", new { @id = Model.TypeName + "_M0150PaymentSourceWRKCOMPHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Workers&#8217; compensation' id='{0}_M0150PaymentSourceWRKCOMP' name='{0}_M0150PaymentSourceWRKCOMP' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceWRKCOMP").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourceWRKCOMP">
                            <span class="float-left">5 &#8211;</span>
                            <span class="normal margin">Workers&#8217; compensation</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceTITLPRO", " ", new { @id = Model.TypeName + "_M0150PaymentSourceTITLPROHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Title Programs' id='{0}_M0150PaymentSourceTITLPRO' name='{0}_M0150PaymentSourceTITLPRO' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceTITLPRO").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourceTITLPRO">
                            <span class="float-left">6 &#8211;</span>
                            <span class="normal margin">Title programs (e.g., Title III, V, or XX)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceOTHGOVT", " ", new { @id = Model.TypeName + "_M0150PaymentSourceOTHGOVTHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Other Government' id='{0}_M0150PaymentSourceOTHGOVT' name='{0}_M0150PaymentSourceOTHGOVT' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceOTHGOVT").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourceOTHGOVT">
                            <span class="float-left">7 &#8211;</span>
                            <span class="normal margin">Other government (e.g., TriCare, VA, etc.)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourcePRVINS", " ", new { @id = Model.TypeName + "_M0150PaymentSourcePRVINSHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Private Insurance' id='{0}_M0150PaymentSourcePRVINS' name='{0}_M0150PaymentSourcePRVINS' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourcePRVINS").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourcePRVINS">
                            <span class="float-left">8 &#8211;</span>
                            <span class="normal margin">Private insurance</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourcePRVHMO", " ", new { @id = Model.TypeName + "_M0150PaymentSourcePRVHMOHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Private HMO' id='{0}_M0150PaymentSourcePRVHMO' name='{0}_M0150PaymentSourcePRVHMO' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourcePRVHMO">
                            <span class="float-left">9 &#8211;</span>
                            <span class="normal margin">Private HMO/managed care</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceSelfPay", " ", new { @id = Model.TypeName + "_M0150PaymentSourceSelfPayHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Self-pay' id='{0}_M0150PaymentSourceSelfPay' name='{0}_M0150PaymentSourceSelfPay' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceSelfPay").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourceSelfPay">
                            <span class="float-left">10 &#8211;</span>
                            <span class="normal margin">Self-pay</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceUnknown", " ", new { @id = Model.TypeName + "_M0150PaymentSourceUnknownHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Unknown' id='{0}_M0150PaymentSourceUnknown' name='{0}_M0150PaymentSourceUnknown' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceUnknown").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourceUnknown">
                            <span class="float-left">UK &#8211;</span>
                            <span class="normal margin">Unknown</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M0150PaymentSourceOtherSRS", " ", new { @id = Model.TypeName + "_M0150PaymentSourceOtherSRSHidden" })%>
                        <%= string.Format("<input status='(M0150) Payment Source, Other' id='{0}_M0150PaymentSourceOtherSRS' name='{0}_M0150PaymentSourceOtherSRS' type='checkbox' class='M0150' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M0150PaymentSourceOtherSRS">
                            <span class="float-left">11 &#8211;</span>
                            <span class="normal margin">Other</span>
                        </label>
                        <div id="<%= Model.TypeName %>_M0150PaymentSourceOtherSRSMore" class="normal margin">
                            <label for="<%= Model.TypeName %>_M0150PaymentSourceOther"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_M0150PaymentSourceOther", data.AnswerOrEmptyString("M0150PaymentSourceOther"), new { @id = Model.TypeName + "_M0150PaymentSourceOther" })%>
                        </div>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M0150')" status="More Information about M0150">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="save next">Save &#38; Continue</a></li>
            <li><a class="save close">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float-right">
            <li><a class="oasis-validate">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>