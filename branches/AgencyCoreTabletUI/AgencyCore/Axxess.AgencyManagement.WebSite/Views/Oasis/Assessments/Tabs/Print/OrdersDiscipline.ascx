﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var isOasis = !Model.TypeName.Contains("NonOasis"); %>
<%  var Conclusions = data.AnswerArray("485Conclusions"); %>
<%  var RehabilitationPotential = data.AnswerArray("485RehabilitationPotential"); %>
<%  var DischargePlans = data.AnswerArray("485DischargePlans"); %>
<%  var DischargePlansReason = data.AnswerArray("485DischargePlansReason"); %>
<%  var SIResponse = data.AnswerArray("485SIResponse"); %>
<%  var ConferencedWith = data.AnswerArray("485ConferencedWith"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    printview.addsection(
        printview.col(7,
            printview.span("Frequencies:",true) +
            printview.span("%3Cstrong%3ESN%3C/strong%3E <%= data.AnswerOrEmptyString("485SNFrequency").Clean() %>") +
            printview.span("%3Cstrong%3EPT%3C/strong%3E <%= data.AnswerOrEmptyString("485PTFrequency").Clean() %>") +
            printview.span("%3Cstrong%3EOT%3C/strong%3E <%= data.AnswerOrEmptyString("485OTFrequency").Clean() %>") +
            printview.span("%3Cstrong%3EST%3C/strong%3E <%= data.AnswerOrEmptyString("485STFrequency").Clean() %>") +
            printview.span("%3Cstrong%3EMSW%3C/strong%3E <%= data.AnswerOrEmptyString("485MSWFrequency").Clean() %>") +
            printview.span("%3Cstrong%3EHHA%3C/strong%3E <%= data.AnswerOrEmptyString("485HHAFrequency").Clean() %>")) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data.AnswerOrEmptyString("485OrdersDisciplineInterventionComments").Clean() %>",false,2),
        "Orders for Discipline and Treatments");
    printview.addsection(
        printview.col(4,
            printview.checkbox("Skilled Intervention Needed",<%= Conclusions.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Skilled Instruction Needed",<%= Conclusions.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("No Skilled Service Needed",<%= Conclusions.Contains("3").ToString().ToLower() %>) +
            printview.span("Other: <%= data.AnswerOrEmptyString("485ConclusionOther").Clean() %>")),
        "Conclusions");
    printview.addsection(
        printview.col(2,
            printview.span("Rehabilitation potential for stated goals:",true) +
            printview.col(3,
                printview.checkbox("Good",<%= RehabilitationPotential.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Fair",<%= RehabilitationPotential.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Poor",<%= RehabilitationPotential.Contains("3").ToString().ToLower() %>))) +
        printview.span("Other rehabilitation potential:",true) +
        printview.span("<%= data.AnswerOrEmptyString("485AchieveGoalsComments").Clean() %>",false,2),
        "Rehabilitation Potential");
    printview.addsection(
        printview.col(2,
            printview.span("Patient to be discharged to the care of:",true) +
            printview.col(3,
                printview.checkbox("Physician",<%= DischargePlans.Contains("1").ToString().ToLower() %>) +
                printview.checkbox("Caregiver",<%= DischargePlans.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("Self care",<%= DischargePlans.Contains("3").ToString().ToLower() %>)) +
            printview.col(3,
                printview.span("Discharge Plans",true) +
                printview.checkbox("When goals met.",<%= DischargePlansReason.Contains("2").ToString().ToLower() %>) +
                printview.checkbox("When wounds heal.",<%= DischargePlansReason.Contains("3").ToString().ToLower() %>)) +
            printview.checkbox("When caregiver willing &#38; able to manage all aspects of patient&#8217;s care.",<%= DischargePlansReason.Contains("1").ToString().ToLower() %>)) +
        printview.span("Additional discharge plans:",true) +
        printview.span("<%= data.AnswerOrEmptyString("485DischargePlanComments").Clean() %>",false,2),
        "Discharge Plans");
    printview.addsection(
        printview.span("Skilled Intervention/Teaching:",true) +
        printview.span("<%= data.AnswerOrEmptyString("485SkilledInterventionComments").Clean() %>",false,2) +
        printview.col(2,
            printview.checkbox("Verbalizes <%= SIResponse.Contains("1") ? data.AnswerOrEmptyString("485SIVerbalizedUnderstandingPercent") : string.Empty %> understanding of teaching.",<%= SIResponse.Contains("1").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.checkbox("PT",<%= (SIResponse.Contains("1") && data.AnswerOrEmptyString("485SIVerbalizedUnderstandingPT").Equals("1")).ToString().ToLower() %>) +
                printview.checkbox("CG",<%= (SIResponse.Contains("1") && data.AnswerOrEmptyString("485SIVerbalizedUnderstandingCG").Equals("1")).ToString().ToLower() %>)) +
            printview.checkbox("Needs further teaching.",<%= SIResponse.Contains("2").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.checkbox("PT",<%= (SIResponse.Contains("2") && data.AnswerOrEmptyString("485NEEDSFURTHERTEACHINGPT").Equals("1")).ToString().ToLower() %>) +
                printview.checkbox("CG",<%= (SIResponse.Contains("2") && data.AnswerOrEmptyString("485NEEDSFURTHERTEACHINGCG").Equals("1")).ToString().ToLower() %>)) +
            printview.checkbox("Able to return correct demonstration of procedure.",<%= SIResponse.Contains("3").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.checkbox("PT",<%= (SIResponse.Contains("3") && data.AnswerOrEmptyString("485DEMONSTRATIONOFPROCEDUREPT").Equals("1")).ToString().ToLower() %>) +
                printview.checkbox("CG",<%= (SIResponse.Contains("3") && data.AnswerOrEmptyString("485DEMONSTRATIONOFPROCEDURECG").Equals("1")).ToString().ToLower() %>)) +
            printview.checkbox("Unable to return correct demonstration of procedure.",<%= SIResponse.Contains("4").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.checkbox("PT",<%= (SIResponse.Contains("4") && data.AnswerOrEmptyString("485UNDEMONSTRATIONOFPROCEDUREPT").Equals("1")).ToString().ToLower() %>) +
                printview.checkbox("CG",<%= (SIResponse.Contains("4") && data.AnswerOrEmptyString("485UNDEMONSTRATIONOFPROCEDURECG").Equals("1")).ToString().ToLower() %>))),
        "Narrative");
    printview.addsection(
        printview.col(8,
            printview.span("Conference w/",true) +
            printview.checkbox("MD",<%= ConferencedWith.Contains("MD").ToString().ToLower() %>) +
            printview.checkbox("SN",<%= ConferencedWith.Contains("SN").ToString().ToLower() %>) +
            printview.checkbox("PT",<%= ConferencedWith.Contains("PT").ToString().ToLower() %>) +
            printview.checkbox("OT",<%= ConferencedWith.Contains("OT").ToString().ToLower() %>) +
            printview.checkbox("ST",<%= ConferencedWith.Contains("ST").ToString().ToLower() %>) +
            printview.checkbox("MSW",<%= ConferencedWith.Contains("MSW").ToString().ToLower() %>) +
            printview.checkbox("HHA",<%= ConferencedWith.Contains("HHA").ToString().ToLower() %>)) +
        printview.col(4,
            printview.span("Name:",true) +
            printview.span("<%= data.AnswerOrEmptyString("485ConferencedWithName").Clean() %>",false,1) +
            printview.span("Regarding:",true) +
            printview.span("<%= data.AnswerOrEmptyString("485SkilledInterventionRegarding").Clean() %>",false,1)),
        "Cordination of Care");
<%  } %>
</script>