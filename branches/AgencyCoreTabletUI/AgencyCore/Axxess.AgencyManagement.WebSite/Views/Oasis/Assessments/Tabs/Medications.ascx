<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum != "08") { %>
<div class="wrapper main">
    <div class="buttons">
        <ul>
            <li><a onclick="$('#<%= Model.TypeName %>_SupplySave').click();return false">Save</a></li>
            <li><a onclick="$('#<%= Model.TypeName %>_SupplySaveCont').click();return false">Save &#38; Continue</a></li>
            <li><a onclick="$('#<%= Model.TypeName %>_SupplySaveExit').click();return false">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float-right">
            <li><a onclick="$('#<%= Model.TypeName %>_SupplyCheckErr').click();return false">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset class="loc485">
        <legend>Medications (Locator #10)</legend>
        <div class="wide-column">
            <div class="row" id="<%= Model.TypeName %>_medications">
                <%  var medicationProfile = (Model != null && Model.MedicationProfile.IsNotNullOrEmpty()) ? Model.MedicationProfile.ToObject<MedicationProfile>() : new MedicationProfile(); %>
                <%  Html.RenderPartial("/Views/Patient/MedicationProfile/ProfileGrid.ascx", new OasisMedicationProfileViewData { Id = medicationProfile.Id, AssessmentType = Model.TypeName, Profile = medicationProfile }); %>
            </div>
        </div>
    </fieldset>
    <%  } %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "MedicationForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %> 
    <%= Html.Hidden("categoryType", "Medications")%> 
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>Medication Administration Record</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecTime" class="float-left">Time:</label>
                <div class="float-right">
                    <input type="time" name="<%= Model.TypeName %>_GenericMedRecTime" value="<%= data.AnswerOrEmptyString("GenericMedRecTime") %>" id="<%= Model.TypeName %>_GenericMedRecTime" class="time" status="(Optional) Medication Administration Record, Time" />
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecMedication" class="float-left">Medication</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_GenericMedRecMedication", data.AnswerOrEmptyString("GenericMedRecMedication"), new { @id = Model.TypeName + "_GenericMedRecMedication", @maxlength = "30", @status = "(Optional) Medication Administration Record, Medication" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecDose" class="float-left">Dose</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_GenericMedRecDose", data.AnswerOrEmptyString("GenericMedRecDose"), new { @id = Model.TypeName + "_GenericMedRecDose", @maxlength = "30", @status = "(Optional) Medication Administration Record, Dose" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecRoute" class="float-left">Route</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_GenericMedRecRoute", data.AnswerOrEmptyString("GenericMedRecRoute"), new { @id = Model.TypeName + "_GenericMedRecRoute", @maxlength = "30", @status = "(Optional) Medication Administration Record, Route" })%>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecFrequency" class="float-left">Frequency</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_GenericMedRecFrequency", data.AnswerOrEmptyString("GenericMedRecFrequency"), new { @id = Model.TypeName + "_GenericMedRecFrequency", @maxlength = "30", @status = "(Optional) Medication Administration Record, Frequency" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecPRN" class="float-left">PRN Reason</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_GenericMedRecPRN", data.AnswerOrEmptyString("GenericMedRecPRN"), new { @id = Model.TypeName + "_GenericMedRecPRN", @maxlength = "30", @status = "(Optional) Medication Administration Record, PRN" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecLocation" class="float-left">Location</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_GenericMedRecLocation", data.AnswerOrEmptyString("GenericMedRecLocation"), new { @id = Model.TypeName + "_GenericMedRecLocation", @maxlength = "30", @status = "(Optional) Medication Administration Record, Location" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecResponse" class="float-left">Patient Response</label>
                <div class="float-right">
                    <%= Html.TextBox(Model.TypeName + "_GenericMedRecResponse", data.AnswerOrEmptyString("GenericMedRecResponse"), new { @id = Model.TypeName + "_GenericMedRecResponse", @maxlength = "30", @status = "(Optional) Medication Administration Record, Response" })%>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericMedRecComments" class="strong">Comment</label>
                <%= Html.TextArea(Model.TypeName + "_GenericMedRecComments", data.AnswerOrEmptyString("GenericMedRecComments"), 5, 70, new { @id = Model.TypeName + "_GenericMedRecComments", @status = "(Optional) Medication Administration Record, Comments" })%>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset class="oasis">
        <legend>Medications</legend>
        <div class="wide-column">
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
            <div class="row" id="<%= Model.TypeName %>_M2000">
                <label class="strong">
                    <a status="More Information about M2000" class="green" onclick="Oasis.Tip('M2000');return false">(M2000)</a>
                    Drug Regimen Review: Does a complete drug regimen review indicate potential clinically significant medication issues, e.g., drug reactions, ineffective drug therapy, side effects, drug interactions, duplicate therapy, omissions, dosage errors, or noncompliance?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2000DrugRegimenReview", "", new { @id = Model.TypeName + "_M2000DrugRegimenReviewHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2000DrugRegimenReview", "00", data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("00"), new { @id = Model.TypeName + "_M2000DrugRegimenReview0", @status = "(OASIS M2000) Drug Regimen Review, Not Assessed" })%>
                        <label for="<%= Model.TypeName %>_M2000DrugRegimenReview0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Not assessed/reviewed</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2000DrugRegimenReview", "01", data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("01"), new { @id = Model.TypeName + "_M2000DrugRegimenReview1", @status = "(OASIS M2000) Drug Regimen Review, No Problems" })%>
                        <label for="<%= Model.TypeName %>_M2000DrugRegimenReview1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">No problems found during review</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2000DrugRegimenReview", "02", data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("02"), new { @id = Model.TypeName + "_M2000DrugRegimenReview2", @status = "(OASIS M2000) Drug Regimen Review, Problems" })%>
                        <label for="<%= Model.TypeName %>_M2000DrugRegimenReview2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Problems found during review</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2000DrugRegimenReview", "NA", data.AnswerOrEmptyString("M2000DrugRegimenReview").Equals("NA"), new { @id = Model.TypeName + "_M2000DrugRegimenReviewNA", @status = "(OASIS M2000) Drug Regimen Review, No Medication" })%>
                        <label for="<%= Model.TypeName %>_M2000DrugRegimenReviewNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">Patient is not taking any medications</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M2000')" status="More Information about M2000">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M2002">
                <label class="strong">
                    <a status="More Information about M2000" class="green" onclick="Oasis.Tip('M2002');return false">(M2002)</a>
                    Medication Follow-up: Was a physician or the physician&#8212;designee contacted within one calendar day to resolve clinically significant medication issues, including reconciliation?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2002MedicationFollowup", "", new { @id = Model.TypeName + "_M2002MedicationFollowupHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2002MedicationFollowup", "0", data.AnswerOrEmptyString("M2002MedicationFollowup").Equals("0"), new { @id = Model.TypeName + "_M2002MedicationFollowup0", @status = "(OASIS M2002) Medication Follow-up, No" })%>
                        <label for="<%= Model.TypeName %>_M2002MedicationFollowup0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2002MedicationFollowup", "1", data.AnswerOrEmptyString("M2002MedicationFollowup").Equals("1"), new { @id = Model.TypeName + "_M2002MedicationFollowup1", @status = "(OASIS M2002) Medication Follow-up, Yes" })%>
                        <label for="<%= Model.TypeName %>_M2002MedicationFollowup1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M2002')" status="More Information about M2000">?</div>
                </div>
            </div>
            <%  } %>
            <%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5) { %>
            <div class="row" id="<%= Model.TypeName %>_M2004">
                <label class="strong">
                    <a class="green" onclick="Oasis.Tip('M2004');return false">(M2004)</a>
                    Medication Intervention: If there were any clinically significant medication issues since the previous OASIS assessment, was a physician or the physician-designee contacted within one calendar day of the assessment to resolve clinically significant medication issues, including reconciliation?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2004MedicationIntervention", "", new { @id = Model.TypeName + "_M2004MedicationInterventionHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2004MedicationIntervention", "00", data.AnswerOrEmptyString("M2004MedicationIntervention").Equals("00"), new { @id = Model.TypeName + "_M2004MedicationIntervention0" })%>
                        <label for="<%= Model.TypeName %>_M2004MedicationIntervention0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2004MedicationIntervention", "01", data.AnswerOrEmptyString("M2004MedicationIntervention").Equals("01"), new { @id = Model.TypeName + "_M2004MedicationIntervention1" })%>
                        <label for="<%= Model.TypeName %>_M2004MedicationIntervention1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2004MedicationIntervention", "NA", data.AnswerOrEmptyString("M2004MedicationIntervention").Equals("NA"), new { @id = Model.TypeName + "_M2004MedicationInterventionNA" })%>
                        <label for="<%= Model.TypeName %>_M2004MedicationInterventionNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">No clinically significant medication issues identified since the previous OASIS assessment</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M2004')">?</div>
                </div>
            </div>
            <%  } %>
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
            <div class="row" id="<%= Model.TypeName %>_M2010">
                <label class="strong">
                    <a status="More Information about M2000" class="green" onclick="Oasis.Tip('M2010');return false">(M2010)</a>
                    Patient/Caregiver High Risk Drug Education: Has the patient/caregiver received instruction on special precautions for all high-risk medications (such as hypoglycemics, anticoagulants, etc.) and how and when to report problems that may occur?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducation", "", new { @id = Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducationHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducation", "00", data.AnswerOrEmptyString("M2010PatientOrCaregiverHighRiskDrugEducation").Equals("00"), new { @id = Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducation0", @status = "(OASIS M2010) High Risk Drug Education, No" })%>
                        <label for="<%= Model.TypeName %>_M2010PatientOrCaregiverHighRiskDrugEducation0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducation", "01", data.AnswerOrEmptyString("M2010PatientOrCaregiverHighRiskDrugEducation").Equals("01"), new { @id = Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducation1", @status = "(OASIS M2010) High Risk Drug Education, Yes" })%>
                        <label for="<%= Model.TypeName %>_M2010PatientOrCaregiverHighRiskDrugEducation1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducation", "NA", data.AnswerOrEmptyString("M2010PatientOrCaregiverHighRiskDrugEducation").Equals("NA"), new { @id = Model.TypeName + "_M2010PatientOrCaregiverHighRiskDrugEducation2", @status = "(OASIS M2010) High Risk Drug Education, None Prescribed" })%>
                        <label for="<%= Model.TypeName %>_M2010PatientOrCaregiverHighRiskDrugEducation2">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">Patient not taking any high risk drugs OR patient/caregiver fully knowledgeable about special precautions associated with all high-risk medications</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M2010')" status="More Information about M2010">?</div>
                </div>
            </div>
            <%  } %>
            <%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5) { %>
            <div class="row" id="<%= Model.TypeName %>_M2015">
                <label class="strong">
                    <a class="green" onclick="Oasis.Tip('M2015');return false">(M2015)</a>
                    Patient/Caregiver Drug Education Intervention: Since the previous OASIS assessment, was the patient/caregiver instructed by agency staff or other health care provider to monitor the effectiveness of drug therapy, drug reactions, and side effects, and how and when to report problems that may occur?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2015PatientOrCaregiverDrugEducationIntervention", "", new { @id = Model.TypeName + "_M2015PatientOrCaregiverDrugEducationInterventionHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2015PatientOrCaregiverDrugEducationIntervention", "00", data.AnswerOrEmptyString("M2015PatientOrCaregiverDrugEducationIntervention").Equals("00"), new { @id = Model.TypeName + "_M2015PatientOrCaregiverDrugEducationIntervention0" })%>
                        <label for="<%= Model.TypeName %>_M2015PatientOrCaregiverDrugEducationIntervention0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2015PatientOrCaregiverDrugEducationIntervention", "01", data.AnswerOrEmptyString("M2015PatientOrCaregiverDrugEducationIntervention").Equals("01"), new { @id = Model.TypeName + "_M2015PatientOrCaregiverDrugEducationIntervention1" })%>
                        <label for="<%= Model.TypeName %>_M2015PatientOrCaregiverDrugEducationIntervention1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2015PatientOrCaregiverDrugEducationIntervention", "NA", data.AnswerOrEmptyString("M2015PatientOrCaregiverDrugEducationIntervention").Equals("NA"), new { @id = Model.TypeName + "_M2015PatientOrCaregiverDrugEducationInterventionNA" })%>
                        <label for="<%= Model.TypeName %>_M2015PatientOrCaregiverDrugEducationInterventionNA">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">Patient not taking any drugs</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M2015');">?</div>
                </div>
            </div>
            <%  } %>
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row" id="<%= Model.TypeName %>_M2020">
                <label class="strong">
                    <a status="More Information about M2020" class="green" onclick="Oasis.Tip('M2020');return false">(M2020)</a>
                    Management of Oral Medications: Patient&#8217;s current ability to prepare and take all oral medications reliably and safely, including administration of the correct dosage at the appropriate times/intervals. Excludes injectable and IV medications.
                    <em>(NOTE: This refers to ability, not compliance or willingness)</em>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2020ManagementOfOralMedications", "", new { @id = Model.TypeName + "_M2020ManagementOfOralMedicationsHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2020ManagementOfOralMedications", "00", data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("00"), new { @id = Model.TypeName + "_M2020ManagementOfOralMedications0", @status = "(OASIS M2020) Oral Medications, Independent" })%>
                        <label for="<%= Model.TypeName %>_M2020ManagementOfOralMedications0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to independently take the correct oral medication(s) and proper dosage(s) at the correct times.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2020ManagementOfOralMedications", "01", data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("01"), new { @id = Model.TypeName + "_M2020ManagementOfOralMedications1", @status = "(OASIS M2020) Oral Medications, Doses Prepared by Another Person or Another Person Develops Drug Diary/Chary" })%>
                        <label for="<%= Model.TypeName %>_M2020ManagementOfOralMedications1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Able to take medication(s) at the correct times if:
                                <ul>
                                    <li>
                                        <span class="float-left">(a)</span>
                                        <span class="radio">individual dosages are prepared in advance by another person; OR</span>
                                    </li>
                                    <li>
                                        <span class="float-left">(b)</span>
                                        <span class="radio">another person develops a drug diary or chart.</span>
                                    </li>
                                </ul>
                            </span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2020ManagementOfOralMedications", "02", data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("02"), new { @id = Model.TypeName + "_M2020ManagementOfOralMedications2", @status = "(OASIS M2020) Oral Medications, Requires Reminders" })%>
                        <label for="<%= Model.TypeName %>_M2020ManagementOfOralMedications2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Able to take medication(s) at the correct times if given reminders by another person at the appropriate times</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2020ManagementOfOralMedications", "03", data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("03"), new { @id = Model.TypeName + "_M2020ManagementOfOralMedications3", @status = "(OASIS M2020) Oral Medications, Must be Administered by Another Person" })%>
                        <label for="<%= Model.TypeName %>_M2020ManagementOfOralMedications3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Unable to take medication unless administered by another person.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2020ManagementOfOralMedications", "NA", data.AnswerOrEmptyString("M2020ManagementOfOralMedications").Equals("NA"), new { @id = Model.TypeName + "_M2020ManagementOfOralMedications4", @status = "(OASIS M2020) Oral Medications, None Prescribed" })%>
                        <label for="<%= Model.TypeName %>_M2020ManagementOfOralMedications4">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">No oral medications prescribed.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M2020')" status="More Information about M2020">?</div>
                </div>
            </div>
            <%  } %>
            <%  if (Model.AssessmentTypeNum.ToInteger() < 6 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
            <div class="row" id="<%= Model.TypeName %>_M2030">
                <label class="strong">
                    <a status="More Information about M2030" class="green" onclick="Oasis.Tip('M2030');return false">(M2030)</a>
                    Management of Injectable Medications: Patient&#8217;s current ability to prepare and take all prescribed injectable medications reliably and safely, including administration of correct dosage at the appropriate times/intervals. Excludes IV medications.
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2030ManagementOfInjectableMedications", "", new { @id = Model.TypeName + "_M2030ManagementOfInjectableMedicationsHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2030ManagementOfInjectableMedications", "00", data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("00"), new { @id = Model.TypeName + "_M2030ManagementOfInjectableMedications0", @status = "(OASIS M2030) Injectable Medications, Independent" })%>
                        <label for="<%= Model.TypeName %>_M2030ManagementOfInjectableMedications0">
                            <span class="float-left">0 &#8211;</span>
                            <span class="normal margin">Able to independently take the correct medication(s) and proper dosage(s) at the correct times.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2030ManagementOfInjectableMedications", "01", data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("01"), new { @id = Model.TypeName + "_M2030ManagementOfInjectableMedications1", @status = "(OASIS M2030) Injectable Medications, Syringes Prepared by Another Person or Another Person Develops Drug Diary/Chart" })%>
                        <label for="<%= Model.TypeName %>_M2030ManagementOfInjectableMedications1">
                            <span class="float-left">1 &#8211;</span>
                            <span class="normal margin">Able to take injectable medication(s) at the correct times if:
                                <ul>
                                    <li>
                                        <span class="float-left">(a)</span>
                                        <span class="radio">individual syringes are prepared in advance by another person; OR</span>
                                    </li>
                                    <li>
                                        <span class="float-left">(b)</span>
                                        <span class="radio">another person develops a drug diary or chart.</span>
                                    </li>
                                </ul>
                            </span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2030ManagementOfInjectableMedications", "02", data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("02"), new { @id = Model.TypeName + "_M2030ManagementOfInjectableMedications2", @status = "(OASIS M2030) Injectable Medications, Requires Reminders" })%>
                        <label for="<%= Model.TypeName %>_M2030ManagementOfInjectableMedications2">
                            <span class="float-left">2 &#8211;</span>
                            <span class="normal margin">Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2030ManagementOfInjectableMedications", "03", data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("03"), new { @id = Model.TypeName + "_M2030ManagementOfInjectableMedications3", @status = "(OASIS M2030) Injectable Medications, Must be Administered by Another Person" })%>
                        <label for="<%= Model.TypeName %>_M2030ManagementOfInjectableMedications3">
                            <span class="float-left">3 &#8211;</span>
                            <span class="normal margin">Unable to take injectable medication unless administered by another person.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2030ManagementOfInjectableMedications", "NA", data.AnswerOrEmptyString("M2030ManagementOfInjectableMedications").Equals("NA"), new { @id = Model.TypeName + "_M2030ManagementOfInjectableMedications4", @status = "(OASIS M2030) Injectable Medications, None Prescribed" })%>
                        <label for="<%= Model.TypeName %>_M2030ManagementOfInjectableMedications4">
                            <span class="float-left">NA &#8211;</span>
                            <span class="normal margin">No injectable medications prescribed.</span>
                        </label>
                    </div>
                </div>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M2030')" status="More Information about M2030">?</div>
                </div>
            </div>
            <%  } %>
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
            <div class="row" id="<%= Model.TypeName %>_M2040">
                <label class="strong">
                    <a status="More Information about M2040" class="green" onclick="Oasis.Tip('M2040');return false">(M2040)</a>
                    Prior Medication Management: Indicate the patient’s usual ability with managing oral and injectable medications prior to this current illness, exacerbation, or injury. Check only one box in each row.
                </label>
                <table class="form">
                    <thead>
                        <tr>
                            <th>Functional Area</th>
                            <th>Independent</th>
                            <th>Needed Some Help</th>
                            <th>Dependent</th>
                            <th>Not Applicable</th>
                        </tr>
                    </thead>
                    <tbody class="checkgroup padfix">
                        <tr>
                            <td>
                                <div class="option">
                                    <label>a. Oral medications</label>
                                    <%= Html.Hidden(Model.TypeName + "_M2040PriorMedicationOral", "", new { @id = Model.TypeName + "_M2040PriorMedicationOralHidden" })%>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2040PriorMedicationOral", "00", data.AnswerOrEmptyString("M2040PriorMedicationOral").Equals("00"), new { @id = Model.TypeName + "_M2040PriorMedicationOral0", @status = "(OASIS M2040) Prior Medication Management, Oral medications, Independent" })%>
                                    <label for="<%= Model.TypeName %>_M2040PriorMedicationOral0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2040PriorMedicationOral", "01", data.AnswerOrEmptyString("M2040PriorMedicationOral").Equals("01"), new { @id = Model.TypeName + "_M2040PriorMedicationOral1", @status = "(OASIS M2040) Prior Medication Management, Oral medications, Needed Some Help" })%>
                                    <label for="<%= Model.TypeName %>_M2040PriorMedicationOral1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2040PriorMedicationOral", "02", data.AnswerOrEmptyString("M2040PriorMedicationOral").Equals("02"), new { @id = Model.TypeName + "_M2040PriorMedicationOral2", @status = "(OASIS M2040) Prior Medication Management, Oral medications, Dependent" })%>
                                    <label for="<%= Model.TypeName %>_M2040PriorMedicationOral2" class="radio">2</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2040PriorMedicationOral", "NA", data.AnswerOrEmptyString("M2040PriorMedicationOral").Equals("NA"), new { @id = Model.TypeName + "_M2040PriorMedicationOral3", @status = "(OASIS M2040) Prior Medication Management, Oral medications, Not Applicable" })%>
                                    <label for="<%= Model.TypeName %>_M2040PriorMedicationOral3" class="radio">NA</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="option">
                                    <label>b. Injectable medications</label>
                                    <%= Html.Hidden(Model.TypeName + "_M2040PriorMedicationInject", "", new { @id = Model.TypeName + "_M2040PriorMedicationInjectHidden" })%>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2040PriorMedicationInject", "00", data.AnswerOrEmptyString("M2040PriorMedicationInject").Equals("00"), new { @id = Model.TypeName + "_M2040PriorMedicationInject0", @status = "(OASIS M2040) Prior Medication Management, Injectable medications, Independent" })%>
                                    <label for="<%= Model.TypeName %>_M2040PriorMedicationInject0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2040PriorMedicationInject", "01", data.AnswerOrEmptyString("M2040PriorMedicationInject").Equals("01"), new { @id = Model.TypeName + "_M2040PriorMedicationInject1", @status = "(OASIS M2040) Prior Medication Management, Injectable medications, Needed Some Help" })%>
                                    <label for="<%= Model.TypeName %>_M2040PriorMedicationInject1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2040PriorMedicationInject", "02", data.AnswerOrEmptyString("M2040PriorMedicationInject").Equals("02"), new { @id = Model.TypeName + "_M2040PriorMedicationInject2", @status = "(OASIS M2040) Prior Medication Management, Injectable medications, Dependent" })%>
                                    <label for="<%= Model.TypeName %>_M2040PriorMedicationInject2" class="radio">2</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2040PriorMedicationInject", "NA", data.AnswerOrEmptyString("M2040PriorMedicationInject").Equals("NA"), new { @id = Model.TypeName + "_M2040PriorMedicationInject3", @status = "(OASIS M2040) Prior Medication Management, Injectable medications, Not Applicable" })%>
                                    <label for="<%= Model.TypeName %>_M2040PriorMedicationInject3" class="radio">NA</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float-right oasis">
                    <div class="oasis-tip" onclick="Oasis.Tip('M2040')" status="More Information about M2040">?</div>
                </div>
            </div>
            <%  } %>
        </div>
    </fieldset>
    <% if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Medication.ascx", Model); %>
    <div class="buttons">
        <ul>
            <li><a id="<%= Model.TypeName %>_SupplySave" class="save">Save</a></li>
            <li><a id="<%= Model.TypeName %>_SupplySaveCont" class="save next">Save &#38; Continue</a></li>
            <li><a id="<%= Model.TypeName %>_SupplySaveExit" class="save close">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float-right">
            <li><a id="<%= Model.TypeName %>_SupplyCheckErr" class="oasis-validate">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>