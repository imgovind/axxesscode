<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "PrognosisForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", "Prognosis")%> 
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="save next">Save &#38; Continue</a></li>
            <li><a class="save close">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float-right">
            <li><a class="oasis-validate">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <fieldset class="loc485">
        <legend>Prognosis (Locator #20)</legend>
        <div class="wide-column">
            <div class="row">
                <div class="narrow checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_485Prognosis", "Guarded", data.AnswerOrEmptyString("485Prognosis").Equals("Guarded"), new { @id = Model.TypeName + "_485PrognosisGuarded", @status = "(485 Locator 20) Prognosis, Guarded" })%>
                        <label for="<%= Model.TypeName %>_485PrognosisGuarded" class="radio">Guarded</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_485Prognosis", "Poor", data.AnswerOrEmptyString("485Prognosis").Equals("Poor"), new { @id = Model.TypeName + "_485PrognosisPoor", @status = "(485 Locator 20) Prognosis, Poor" })%>
                        <label for="<%= Model.TypeName %>_485PrognosisPoor" class="radio">Poor</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_485Prognosis", "Fair", data.AnswerOrEmptyString("485Prognosis").Equals("Fair"), new { @id = Model.TypeName + "_485PrognosisFair", @status = "(485 Locator 20) Prognosis, Fair" })%>
                        <label for="<%= Model.TypeName %>_485PrognosisFair" class="radio">Fair</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_485Prognosis", "Good", data.AnswerOrEmptyString("485Prognosis").Equals("Good"), new { @id = Model.TypeName + "_485PrognosisGood", @status = "(485 Locator 20) Prognosis, Good" })%>
                        <label for="<%= Model.TypeName %>_485PrognosisGood" class="radio">Good</label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_485Prognosis", "Excellent", data.AnswerOrEmptyString("485Prognosis").Equals("Excellent"), new { @id = Model.TypeName + "_485PrognosisExcellent", @status = "(485 Locator 20) Prognosis, Excellent" })%>
                        <label for="<%= Model.TypeName %>_485PrognosisExcellent" class="radio">Excellent</label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="loc485">
        <legend>Functional Limitations (Locator #18.A)</legend>
        <%  string[] functionLimitations = data.AnswerArray("485FunctionLimitations"); %>
        <%= Html.Hidden(Model.TypeName + "_485FunctionLimitations", "", new { @id = Model.TypeName + "_485FunctionLimitationsHidden" })%>
        <div class="wide-column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input status='(485 Locator 18A) Functional Limitations, Amputation' id='{0}_485FunctionLimitations1' name='{0}_485FunctionLimitations' value='1' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485FunctionLimitations1" class="radio">Amputation</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(485 Locator 18A) Functional Limitations, Incontinence' id='{0}_485FunctionLimitations2' name='{0}_485FunctionLimitations' value='2' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("2").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485FunctionLimitations2" class="radio">Bowel/Bladder Incontinence</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(485 Locator 18A) Functional Limitations, Contrature' id='{0}_485FunctionLimitations3' name='{0}_485FunctionLimitations' value='3' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("3").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485FunctionLimitations3" class="radio">Contracture</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(485 Locator 18A) Functional Limitations, Hearing' id='{0}_485FunctionLimitations4' name='{0}_485FunctionLimitations' value='4' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("4").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485FunctionLimitations4" class="radio">Hearing</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(485 Locator 18A) Functional Limitations, Paralysis' id='{0}_485FunctionLimitations5' name='{0}_485FunctionLimitations' value='5' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("5").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485FunctionLimitations5" class="radio">Paralysis</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(485 Locator 18A) Functional Limitations, Endurance' id='{0}_485FunctionLimitations6' name='{0}_485FunctionLimitations' value='6' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("6").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485FunctionLimitations6" class="radio">Endurance</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(485 Locator 18A) Functional Limitations, Ambulation' id='{0}_485FunctionLimitations7' name='{0}_485FunctionLimitations' value='7' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("7").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485FunctionLimitations7" class="radio">Ambulation</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(485 Locator 18A) Functional Limitations, Speech' id='{0}_485FunctionLimitations8' name='{0}_485FunctionLimitations' value='8' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("8").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485FunctionLimitations8" class="radio">Speech</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(485 Locator 18A) Functional Limitations, Legally Blind' id='{0}_485FunctionLimitations9' name='{0}_485FunctionLimitations' value='9' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("9").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485FunctionLimitations9" class="radio">Legally Blind</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(485 Locator 18A) Functional Limitations, Dyspnea' id='{0}_485FunctionLimitationsA' name='{0}_485FunctionLimitations' value='A' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("A").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485FunctionLimitationsA" class="radio">Dyspnea with Minimal Exertion</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(485 Locator 18A) Functional Limitations, Other' id='{0}_485FunctionLimitationsB' name='{0}_485FunctionLimitations' value='B' type='checkbox' {1} />", Model.TypeName, functionLimitations.Contains("B").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485FunctionLimitationsB" class="radio">Other</label>
                        <div id="<%= Model.TypeName %>_485FunctionLimitationsBMore">
                            <label for="<%= Model.TypeName %>_485FunctionLimitationsOther"><em>(Specify)</em></label>
                            <%= Html.TextArea(Model.TypeName + "_485FunctionLimitationsOther", data.AnswerOrEmptyString("485FunctionLimitationsOther"), 5, 70, new { @id = Model.TypeName + "_485FunctionLimitationsOther", @status = "(485 Locator 18A) Functional Limitations, Specify Other" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Advanced Directives</legend>
        <div class="column">
            <div class="row">
                <label class="float-left">Are there any Advanced Directives?</label>
                <div class="float-right">
                    <%= Html.RadioButton(Model.TypeName + "_485AdvancedDirectives", "Yes", data.AnswerOrEmptyString("485AdvancedDirectives").Equals("Yes"), new { @id = Model.TypeName + "_485AdvancedDirectivesYes", @status = "(Optional) Advanced Directives, Yes", @class = "radio" })%>
                    <label for="<%= Model.TypeName %>_485AdvancedDirectivesYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_485AdvancedDirectives", "No", data.AnswerOrEmptyString("485AdvancedDirectives").Equals("No"), new { @id = Model.TypeName + "_485AdvancedDirectivesNo", @status = "(Optional) Advanced Directives, No", @class = "radio" })%>
                    <label for="<%= Model.TypeName %>_485AdvancedDirectivesNo" class="inline-radio">No</label>
                </div>
            </div>
            <div class="row">
                <div class="strong">Intent</div>
                <%  string[] advancedDirectivesIntent = data.AnswerArray("485AdvancedDirectivesIntent"); %>
                <%= Html.Hidden(Model.TypeName + "_485AdvancedDirectivesIntent", "", new { @id = Model.TypeName + "_485AdvancedDirectivesIntentHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input status='(Optional) Advanced Directives Intent, Do Not Resuscitate' id='{0}_485AdvancedDirectivesIntentDNR' name='{0}_485AdvancedDirectivesIntent' value='DNR' class='485AdvancedDirectivesIntent' type='checkbox' {1} />", Model.TypeName, advancedDirectivesIntent.Contains("DNR").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485AdvancedDirectivesIntentDNR" class="radio">DNR</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(Optional) Advanced Directives Intent, Living Will' id='{0}_485AdvancedDirectivesIntentLivingWill' name='{0}_485AdvancedDirectivesIntent' value='Living Will' class='485AdvancedDirectivesIntent' type='checkbox' {1} />", Model.TypeName, advancedDirectivesIntent.Contains("Living Will").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485AdvancedDirectivesIntentLivingWill" class="radio">Living Will</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(Optional) Advanced Directives Intent, Medical Power of Attorney' id='{0}_485AdvancedDirectivesIntentMPOA' name='{0}_485AdvancedDirectivesIntent' value='Medical Power of Attorney' class='485AdvancedDirectivesIntent' type='checkbox' {1} />", Model.TypeName, advancedDirectivesIntent.Contains("Medical Power of Attorney").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485AdvancedDirectivesIntentMPOA" class="radio">Medical Power of Attorney</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input status='(Optional) Advanced Directives Intent, Other' id='{0}_485AdvancedDirectivesIntentOther' name='{0}_485AdvancedDirectivesIntent' value='Other' class='485AdvancedDirectivesIntent' type='checkbox' {1} />", Model.TypeName, advancedDirectivesIntent.Contains("Other").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_485AdvancedDirectivesIntentOther" class="radio">Other</label>
                        <div id="<%= Model.TypeName %>_485AdvancedDirectivesIntentOtherMore">
                            <label for="<%= Model.TypeName %>_485AdvancedDirectivesIntentOther"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_485AdvancedDirectivesIntentOther", data.AnswerOrEmptyString("485AdvancedDirectivesIntentOther"), new { @id = Model.TypeName + "_485AdvancedDirectivesIntentOther", @class = "fill", @maxlength="50", @status = "(Optional) Advanced Directives Intent, Specify Other" }) %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label class="float-left">Copy on file at agency?</label>
                <div class="float-right">
                    <%= Html.Hidden(Model.TypeName + "_485AdvancedDirectivesCopyOnFile", "", new { @id = Model.TypeName + "_485AdvancedDirectivesCopyOnFileHidden" })%>
                    <%= Html.RadioButton(Model.TypeName + "_485AdvancedDirectivesCopyOnFile", "Yes", data.AnswerOrEmptyString("485AdvancedDirectivesCopyOnFile").Equals("Yes"), new { @id = Model.TypeName + "_485AdvancedDirectivesCopyOnFileYes", @class = "radio", @status = "(Optional) Advanced Directives Copy on File, Yes" })%>
                    <label for="<%= Model.TypeName %>_485AdvancedDirectivesCopyOnFileYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_485AdvancedDirectivesCopyOnFile", "No", data.AnswerOrEmptyString("485AdvancedDirectivesCopyOnFile").Equals("No"), new { @id = Model.TypeName + "_485AdvancedDirectivesCopyOnFileNo", @class = "radio", @status = "(Optional) Advanced Directives Copy on File, No" })%>
                    <label for="<%= Model.TypeName %>_485AdvancedDirectivesCopyOnFileNo" class="inline-radio">No</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Patient was provided written and verbal information on Advance Directives</label>
                <div class="float-right">
                    <%= Html.Hidden(Model.TypeName + "_485AdvancedDirectivesWrittenAndVerbal", "", new { @id = Model.TypeName + "_485AdvancedDirectivesWrittenAndVerbalHidden" })%>
                    <%= Html.RadioButton(Model.TypeName + "_485AdvancedDirectivesWrittenAndVerbal", "Yes", data.AnswerOrEmptyString("485AdvancedDirectivesWrittenAndVerbal").Equals("Yes"), new { @id = Model.TypeName + "_485AdvancedDirectivesWrittenAndVerbalYes", @class = "radio", @status = "(Optional) Patient Provided with Information on Advanced Directives, Yes" })%>
                    <label for="<%= Model.TypeName %>_485AdvancedDirectivesWrittenAndVerbalYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_485AdvancedDirectivesWrittenAndVerbal", "No", data.AnswerOrEmptyString("485AdvancedDirectivesWrittenAndVerbal").Equals("No"), new { @id = Model.TypeName + "_485AdvancedDirectivesWrittenAndVerbalNo", @class = "radio", @status = "(Optional) Patient Provided with Information on Advanced Directives, No" })%>
                    <label for="<%= Model.TypeName %>_485AdvancedDirectivesWrittenAndVerbalNo" class="inline-radio">No</label>
                </div>
            </div>
            <div class="row">
                <label class="float-left">Is the Patient DNR (Do Not Resuscitate)?</label>
                <div class="float-right">
                    <%= Html.Hidden(Model.TypeName + "_GenericPatientDNR") %>
                    <%= Html.RadioButton(Model.TypeName + "_GenericPatientDNR", "Yes", data.AnswerOrEmptyString("GenericPatientDNR").Equals("Yes"), new { @id = Model.TypeName + "_GenericPatientDNRYes", @class = "radio", @status = "(Optional) Do Not Resuscitate, Yes" })%>
                    <label for="<%= Model.TypeName %>_GenericPatientDNRYes" class="inline-radio">Yes</label>
                    <%= Html.RadioButton(Model.TypeName + "_GenericPatientDNR", "No", data.AnswerOrEmptyString("GenericPatientDNR").Equals("No"), new { @id = Model.TypeName + "_GenericPatientDNRNo", @class = "radio", @status = "(Optional) Do Not Resuscitate, No" })%>
                    <label for="<%= Model.TypeName %>_GenericPatientDNRNo" class="inline-radio">No</label>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wide-column">
            <div class="row">
                <label for="<%= Model.TypeName %>_485AdvancedDirectivesComment" class="strong">Comments</label>
                <%= Html.TextArea(Model.TypeName + "_485AdvancedDirectivesComment", data.AnswerOrEmptyString("485AdvancedDirectivesComment"), 5, 70, new { @id = Model.TypeName + "_485AdvancedDirectivesComment", @status = "(Optional) Advanced Directives Comments" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a class="save">Save</a></li>
            <li><a class="save next">Save &#38; Continue</a></li>
            <li><a class="save close">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float-right">
            <li><a class="oasis-validate">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>

