﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<span class="wintitle">OASIS Export | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main blue">
<%  using (Html.BeginForm("Generate", "Oasis", FormMethod.Post, new { @id = "generateOasis" })) { %>
    <div class="float-right">
        <div class="buttons">
            <ul>
                <li><%= Html.ActionLink("Export to Excel", "OasisExport", "Export", new { BranchId = ViewData["BranchId"], InsuranceId = ViewData["InsuranceId"] != null && ViewData["InsuranceId"] != "" ? ViewData["InsuranceId"].ToString() : "0" }, new { id = "OasisExport_ExportLink" })%></li>
            </ul>
        </div>
    </div>
    <fieldset class="grid-controls">
        <label class="float-left" for="OasisExport_BranchCode">Branch:</label>
        <div class="float-left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = "OasisExport_BranchCode" })%></div>
        <label class="float-left" for="OasisExport_InsuranceId">Primary Insurance:</label>
        <div class="float-left"><%= Html.Insurances("InsuranceId", ViewData["InsuranceId"] != null && ViewData["InsuranceId"] != ""?ViewData["InsuranceId"].ToString():"0", new { @id = "OasisExport_InsuranceId" })%></div>
        <div class="buttons fr">
            <ul>
                <li><a onclick="Agency.RebindOasisToExport();return false">Generate</a></li>
            </ul>
        </div>
        <br />
        <div class="checkgroup">
            <div class="option">
                <input id="selectAllOasisExport" type="checkbox" class="float-left radio" />
                <label for="selectAllOasisExport" class="radio">Check/Uncheck All</label>
            </div>
        </div>
    </fieldset>
    <%  Html.Telerik().Grid<AssessmentExport>().Name("generateOasisGrid").HtmlAttributes(new { @class = "bottom-gap" }).Columns(columns => {
            columns.Bound(o => o.AssessmentId).Template(t => { %><%= string.Format("<input name=\"OasisSelected\" type=\"checkbox\" value=\"{0}\" />", t.Identifier)%><% }).ClientTemplate("<input name='OasisSelected' type='checkbox' value='<#= Identifier #>'/>").Title("").Width(50).HtmlAttributes(new { style = "text-align:center" }).Sortable(false);
            columns.Bound(o => o.PatientName).Title("Patient Name");
            columns.Bound(o => o.AssessmentName).Title("Assessment Type").Sortable(true);
            columns.Bound(o => o.AssessmentDateFormatted).Title("Assessment Date").Width(120).Sortable(true);
            columns.Bound(o => o.EpisodeRange).Width(150).Title("Episode").Sortable(true);
            columns.Bound(o => o.Insurance).Sortable(true);
            columns.Bound(o => o.CorrectionNumberFormat).Title("Correction #").Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Export", "Oasis", new { BranchId = ViewData["BranchId"], InsuranceId = ViewData["InsuranceId"] != null && ViewData["InsuranceId"] != "" ? ViewData["InsuranceId"].ToString() : "0" })).Footer(false).Scrollable().Sortable().Render(); %>
    <div class="buttons abs-bottom wrapper">
        <%= Html.Hidden("CommandType", "", new {@id="BulkUpdate_Type" })%>
        <ul>
            <li><a onclick="GenerateSubmit($(this));return false">Generate OASIS File</a></li>
            <li><a onclick="MarkAsSubmited();return false">Mark Selected As Exported</a></li>
        </ul>
    </div>
<%  } %>
</div>
<script type="text/javascript">
    $("#window_oasisExport .t-group-indicator").hide();
    $("#window_oasisExport .t-grouping-header").remove();
    $("#window_oasisExport .t-grid-content").css({ "height": "auto", "position": "absolute",  "top": "25px"});
    function formatJSONDate(jsonDate) { var date = new Date(parseInt(jsonDate.substr(6))); return date; }
    function GenerateSubmit(control) { if ($("input[name=OasisSelected]:checked").length > 0) $(control).closest('form').submit(); else U.Growl("Select at least one OASIS assessment to generate.", "error"); }
    function MarkAsSubmited() {
        if ($("input[name=OasisSelected]:checked").length > 0) {
            Acore.Modal({
                Name: "OASIS Mark as Exported",
                Content: $("<div/>", { "id": "oasisMarkAsExportDialog", "class": "wrapper main" }).append(
                    $("<fieldset/>").append(
                        $("<div/>").html("Please verify that your OASIS submission was accepted by CMS (Center for Medicare &#38; Medicaid Services) before you complete this step. If this OASIS file was not accepted, do not mark as exported.")).append(
                        $("<div/>").addClass("strong align-center").html("Are you sure that this OASIS file has been accepted by CMS?"))).append(
                    $("<div/>").Buttons([
                        { Text: "Yes, Mark as exported", Click: function() { Oasis.MarkAsExported("#generateOasis"); $(this).closest(".window").Close() } },
                        { Text: "No, Cancel", Click: function() { $(this).closest(".window").Close() } }
                    ])),
                Width: "700px",
                WindowFrame: false
            })
        } else U.Growl("Select at least one OASIS assessment to mark as exported.", "error");
    }
    $("#selectAllOasisExport").change(function() { $("#generateOasisGrid input[name='OasisSelected']").each(function() { $(this).prop("checked", $("#selectAllOasisExport").prop("checked")); }) });
</script>