﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="firstVisit" class="firstvisit hidden">
    <form id="firstVisitForm" action="/Home/FirstVisit" method="post">
        <fieldset>
            <legend>Change Temporary Password</legend>
            <div class="wide_column">Your new password must be a minimum of 8 characters in length.</div>
            <div class="wide_column">
                <div class="row"><label for="First_Visit_CurrentPassword">Temporary Password:</label><div class="float_right"> <%=Html.Password("CurrentPassword", "", new { @id = "First_Visit_CurrentPassword", @class = "input_wrapper required", @maxlength = "15" })%></div></div>
                <div class="row"><label for="First_Visit_NewPassword">New Password:</label><div class="float_right"><%=Html.Password("NewPassword", "", new { @id = "First_Visit_NewPassword", @class = "input_wrapper required", @maxlength = "15" })%></div></div>
                <div class="row"><label for="First_Visit_NewPasswordConfirm">Confirm New Password:</label><div class="float_right"> <%=Html.Password("NewPasswordConfirm", "", new { @id = "First_Visit_NewPasswordConfirm", @class = "input_wrapper required", @maxlength = "15" })%></div></div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Add Electronic Signature</legend>
            <div class="wide_column"><input type="checkbox" class="radio" name="First_Visit_SameAsPassword" id="First_Visit_SameAsPassword" /><label for="First_Visit_SameAsPassword"> Same as password</label></div>
            <div class="wide_column">
                <div class="row"><label for="First_Visit_NewSignature">New Signature:</label><div class="float_right"><%=Html.Password("NewSignature", "", new { @id = "First_Visit_NewSignature", @class = "input_wrapper required", @maxlength = "15" })%></div></div>
                <div class="row"><label for="First_Visit_NewSignatureConfirm">Confirm New Signature:</label><div class="float_right"> <%=Html.Password("NewSignatureConfirm", "", new { @id = "First_Visit_NewSignatureConfirm", @class = "input_wrapper required", @maxlength = "15" })%></div></div>
            </div>
        </fieldset>
        <div class="buttons"><ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        </ul></div>
    </form>
</div>
