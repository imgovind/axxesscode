﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Axxess.Core.Infrastructure" %>
<span class="wintitle"><%= Current.DisplayName.Clean() %>&#8217;s Dashboard | <%= Current.AgencyName.Clean() %></span>
<div class="wrapper main">
    <ul id="widget_col1" class="widgets">
        <li class="widget stationary" id="intro_widget">
            <div class="widget-head"><h5>Hello,&#160;<%= Current.DisplayName %>!</h5></div>
            <% Html.RenderPartial("~/Views/Widget/CustomMessage.ascx"); %>
        </li><li class="widget stationary" id="news_widget">
            <div class="widget-head"><h5>News/Updates</h5></div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/NewsFeed.ascx"); %>    
            </div>
        </li><% if (Current.IsQA || Current.IsOfficeManager || Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsClerk || Current.IsScheduler || Current.IsBiller) { %><li class="widget" id="recertsdue_widget">
            <div class="widget-head"><h5>Past Due Recerts</h5></div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/RecertsPastDue.ascx"); %>
            </div>
        </li><% } %>
    </ul><ul id="widget_col2" class="widgets">
        <li class="widget" id="local_widget">
            <div class="widget-head"><h5>Local</h5></div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Local.ascx"); %>
            </div>
        </li><% if (!Current.IsCommunityLiason) { %><li class="widget" id="schedule_widget">
            <div class="widget-head"><h5>My Scheduled Tasks</h5></div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Schedule.ascx"); %>
            </div>
        </li><% } %><% if (Current.IsQA || Current.IsOfficeManager || Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsClerk || Current.IsScheduler || Current.IsBiller) { %><li class="widget" id="upcomingrecert_widget">
            <div class="widget-head"><h5>Upcoming Recerts</h5></div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/RecertsUpcoming.ascx"); %>
            </div>
        </li><% } %>
    </ul><ul id="widget_col3" class="widgets">
        <li class="widget" id="messages-widget">
            <div class="widget-head"><h5>Messages</h5></div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Messages.ascx"); %>
            </div>
        </li><% if (!Current.IsCommunityLiason) { %><li class="widget" id="birthday_widget">
            <div class="widget-head"><h5>Patient Birthdays</h5></div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Birthday.ascx"); %>
            </div>
        </li><% } %><% if (Current.HasRight(Permissions.AccessBillingCenter)) { %><li class="widget" id="claims_widget">
            <div class="widget-head"><h5>Outstanding Claims</h5></div>
            <div class="widget-content">
                <% Html.RenderPartial("~/Views/Widget/Claims.ascx"); %>
            </div>
        </li><% } %>
    </ul>
</div>
<script type="text/javascript">
    Home.Init();
</script>