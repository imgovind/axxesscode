﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AgencyCore Link Accounts</title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
        .Add("globals.css")
        .Add("growl.css")
        .Add("sprite.css")
        .Add("forms.css")
        .Add("login.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion)) %>
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="shade"></div>
    <div class="login-promt">
    <%  if (Model.UserId != Guid.Empty) { %>
        <h3>Link Accounts</h3>
        <div class="row"><%= string.Format("{0} has been linked to your account.", Model.AgencyName) %></div>
        <div class="row">
            <%= Html.LabelFor(a => a.Name) %>
            <%= Model.Name %>
        </div>
        <div class="row">
            <%= Html.LabelFor(a => a.EmailAddress) %>
            <%= Model.EmailAddress %>
        </div>
        <div class="row">
            <%= Html.LabelFor(a => a.AgencyName)%>
            <%= Model.AgencyName %>
        </div>
        <div class="row">Note: Your Password and Electronic Signature will remain the same.</div>
        <div class="buttons">
            <ul>
                <li><a class="login">Login</a></li>
            </ul>
        </div>
    <%  } else { %>
        <h3>Page Not Found</h3>
        <p>You may have mistyped the address or clicked on an expired link. Click <a class="link" href="/Login">here</a> to Login.</p>
    <%  } %>
    </div>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Plugins/Other/blockui.min.js")
            .Add("Plugins/Other/form.min.js")
            .Add("Plugins/Other/validate.min.js")
            .Add("Plugins/Other/jgrowl.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
        ).OnDocumentReady(() => { %>
            Logon.LinkInit();
    <%  }).Render(); %>
</body>
</html>