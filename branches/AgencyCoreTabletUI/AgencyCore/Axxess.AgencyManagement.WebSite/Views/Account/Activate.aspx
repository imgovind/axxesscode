﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AgencyCore Activate Account</title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
        .Add("globals.css")
        .Add("growl.css")
        .Add("sprite.css")
        .Add("forms.css")
        .Add("login.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion)) %>
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="shade"></div>
    <div class="login-promt">
        <%  if (Model.UserId != Guid.Empty) { %>
        <%  using (Html.BeginForm("Activate", "Account", FormMethod.Post, new { @id = "activate-accountForm", @class = "activate-account" })) { %>
        <h3>Welcome to Axxess AgencyCore</h3>
        <p>To activate your account, follow the steps below.</p>
        <div class="row">
            <label><span class="strong">Step 1</span> &#8211; Verify your information below</label>
            <%= Html.Hidden("UserId", Model.UserId, new { @id = "Activate_User_Id" })%>
            <%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "Activate_User_AgencyId" })%>
            <%= Html.Hidden("EmailAddress", Model.EmailAddress, new { @id = "Activate_User_EmailAddress" })%>
        </div>
        <div class="row">
            <%= Html.LabelFor(a => a.Name) %>
            <%= Model.Name %>
        </div>
        <div class="row">
            <%= Html.LabelFor(a => a.EmailAddress) %>
            <%= Model.EmailAddress %>
        </div>
        <div class="row">
            <%= Html.LabelFor(a => a.AgencyName)%>
            <%= Model.AgencyName %>
        </div>
        <div class="row">
            <label><span class="strong">Step 2</span> &#8211; Enter a new password</label>
            <%= Html.Hidden("UserId", Model.UserId, new { @id = "Activate_User_Id" })%>
            <%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "Activate_User_AgencyId" })%>
            <%= Html.Hidden("EmailAddress", Model.EmailAddress, new { @id = "Activate_User_EmailAddress" })%>
        </div>
        <div class="row">
            <%= Html.LabelFor(a => a.Password)%>
            <%= Html.PasswordFor(a => a.Password, new { @class = "required", @maxlength="20" })%>
            <em>Note: Your Electronic Signature will be the same as your new password.</em>
        </div>
        <div class="buttons">
            <ul>
                <li><a class="send">Activate Account</a></li>
            </ul>
        </div>
        <%  } %>
        <%  } else { %>
        <h3>Page Not Found</h3>
        <p>You may have mistyped the address or clicked on an expired link. Click <a class="link" href="/Login">here</a> to Login.</p>
        <%  } %>
    </div>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Plugins/Other/form.min.js")
            .Add("Plugins/Other/validate.min.js")
            .Add("Plugins/Other/jgrowl.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
        ).OnDocumentReady(() => { %>
            Logon.ActivateInit();
    <%  }).Render(); %>
</body>
</html>