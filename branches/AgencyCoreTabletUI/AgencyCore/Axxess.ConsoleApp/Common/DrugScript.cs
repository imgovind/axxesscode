﻿namespace Axxess.ConsoleApp
{
    public class DrugScript
    {
        private DrugData drugData;
        private const string DRUG_INSERT = "INSERT INTO `drugs`(`SequenceNumber`,`LabelCode`,`ProductCode`,`Strength`,`Unit`,`RxOrOTC`,`TradeName`," +
            "`FullName`) VALUES ('{0}', '{1}', '{2}','{3}','{4}', '{5}', '{6}', '{7}');";

        public DrugScript(DrugData drugData)
        {
            this.drugData = drugData;
        }

        public override string ToString()
        {
            return string.Format(DRUG_INSERT, drugData.SequenceNumber, drugData.LabelCode, drugData.ProductCode, drugData.Strength, drugData.Unit, drugData.RxOrOTC, drugData.TradeName, drugData.FullName);
        }
    }
}
