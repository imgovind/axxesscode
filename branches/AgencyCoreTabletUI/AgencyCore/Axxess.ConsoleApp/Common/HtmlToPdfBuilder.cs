﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;

namespace Axxess.ConsoleApp.Common
{
    #region HtmlToPdfBuilder Class

    public class HtmlToPdfBuilder
    {
        #region Constants

        private const string STYLE_DEFAULT_TYPE = "style";
        private const string DOCUMENT_HTML_START = "<html><head></head><body>";
        private const string DOCUMENT_HTML_END = "</body></html>";
        private const string REGEX_GROUP_SELECTOR = "selector";
        private const string REGEX_GROUP_STYLE = "style";

        //amazing regular expression magic
        private const string REGEX_GET_STYLES = @"(?<selector>[^\{\s]+\w+(\s\[^\{\s]+)?)\s?\{(?<style>[^\}]*)\}";

        #endregion

        #region Constructors

        public HtmlToPdfBuilder(Rectangle size)
        {
            this.PageSize = size;
            this._Styles = new StyleSheet();
            this._Pages = new List<HtmlPdfPage>();
        }

        #endregion

        #region Delegates

        public event RenderEvent BeforeRender;

        public event RenderEvent AfterRender;

        #endregion

        #region Properties

        public Rectangle PageSize { get; set; }

        public HtmlPdfPage this[int index]
        {
            get
            {
                return this._Pages[index];
            }
        }

        public HtmlPdfPage[] Pages
        {
            get
            {
                return this._Pages.ToArray();
            }
        }

        #endregion

        #region Members

        private StyleSheet _Styles;
        private List<HtmlPdfPage> _Pages;

        #endregion

        #region Working With The Document

        public HtmlPdfPage AddPage()
        {
            HtmlPdfPage page = new HtmlPdfPage();
            this._Pages.Add(page);
            return page;
        }

        public void RemovePage(HtmlPdfPage page)
        {
            this._Pages.Remove(page);
        }

        public void AddStyle(string selector, string styles)
        {
            this._Styles.LoadTagStyle(selector, HtmlToPdfBuilder.STYLE_DEFAULT_TYPE, styles);
        }

        public void ImportStylesheet(string path)
        {

            //load the file
            string content = File.ReadAllText(path);

            //use a little regular expression magic
            foreach (Match match in Regex.Matches(content, HtmlToPdfBuilder.REGEX_GET_STYLES))
            {
                string selector = match.Groups[HtmlToPdfBuilder.REGEX_GROUP_SELECTOR].Value;
                string style = match.Groups[HtmlToPdfBuilder.REGEX_GROUP_STYLE].Value;
                this.AddStyle(selector, style);
            }

        }


        #endregion

        #region Document Navigation

        public void InsertBefore(HtmlPdfPage page, HtmlPdfPage before)
        {
            this._Pages.Remove(page);
            this._Pages.Insert(
                Math.Max(this._Pages.IndexOf(before), 0),
                page);
        }

        public void InsertAfter(HtmlPdfPage page, HtmlPdfPage after)
        {
            this._Pages.Remove(page);
            this._Pages.Insert(
                Math.Min(this._Pages.IndexOf(after) + 1, this._Pages.Count),
                page);
        }

        #endregion

        #region Rendering The Document

        public byte[] RenderPdf()
        {
            //Document is inbuilt class, available in iTextSharp
            MemoryStream file = new MemoryStream();
            Document document = new Document(this.PageSize);
            PdfWriter writer = PdfWriter.GetInstance(document, file);

            //allow modifications of the document
            if (this.BeforeRender is RenderEvent)
            {
                this.BeforeRender(writer, document);
            }

            //header
            document.Add(new Header(Markup.HTML_ATTR_STYLESHEET, string.Empty));
            document.Open();

            //render each page that has been added
            foreach (HtmlPdfPage page in this._Pages)
            {
                document.NewPage();

                //generate this page of text
                MemoryStream output = new MemoryStream();
                StreamWriter html = new StreamWriter(output, Encoding.UTF8);

                //get the page output
                html.Write(string.Concat(HtmlToPdfBuilder.DOCUMENT_HTML_START, page._Html.ToString(), HtmlToPdfBuilder.DOCUMENT_HTML_END));
                html.Close();
                html.Dispose();

                //read the created stream
                MemoryStream generate = new MemoryStream(output.ToArray());
                StreamReader reader = new StreamReader(generate);
                foreach (object item in HTMLWorker.ParseToList(reader, this._Styles))
                {
                    document.Add((IElement)item);
                }

                //cleanup these streams
                html.Dispose();
                reader.Dispose();
                output.Dispose();
                generate.Dispose();

            }

            //after rendering
            if (this.AfterRender is RenderEvent)
            {
                this.AfterRender(writer, document);
            }

            //return the rendered PDF
            document.Close();
            return file.ToArray();

        }

        #endregion
    }

    #endregion

    #region HtmlPdfPage Class

    public class HtmlPdfPage
    {
        #region Constructors

        public HtmlPdfPage()
        {
            this._Html = new StringBuilder();
        }

        #endregion

        #region Fields

        //parts for generating the page
        internal StringBuilder _Html;

        #endregion

        #region Working With The Html

        public virtual void AppendHtml(string content, params object[] values)
        {
            this._Html.AppendFormat(content, values);
        }

        #endregion
    }

    #endregion

    #region Rendering Delegate

    public delegate void RenderEvent(PdfWriter writer, Document document);

    #endregion

}
