﻿namespace Axxess.ConsoleApp
{
    using System;
    public class EpisodeData
    {
        public Guid Id { get; set; }
        public DateTime EndDate { get; set; }
        public string Schedule { get; set; }
        public DateTime StartDate { get; set; }
        public Guid PatientId { get; set; }
        public Guid AgencyId { get; set; }
    }
}
