﻿namespace Axxess.ConsoleApp
{
    public class DrugListingScript
    {
        private DrugListingData drugData;
        private const string DRUG_INSERT = "INSERT INTO `druglistings`(`SequenceNumber`,`LabelCode`,`ProductCode`,`Strength`,`Unit`,`RxOrOTC`,`TradeName`," +
            "`FullName`) VALUES ('{0}', '{1}', '{2}','{3}','{4}', '{5}', '{6}', '{7}');";

        public DrugListingScript(DrugListingData drugData)
        {
            this.drugData = drugData;
        }

        public override string ToString()
        {
            return string.Format(DRUG_INSERT, drugData.SequenceNumber, drugData.LabelCode, drugData.ProductCode, drugData.Strength, drugData.Unit, drugData.RxOrOTC, drugData.TradeName, drugData.FullName);
        }
    }
}
