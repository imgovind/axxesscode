﻿using System;

using Axxess.Core.Extension;
using Axxess.ConsoleApp.Tests;

using Axxess.Api;
using Axxess.Api.Contracts;

using System.Net;
using System.Linq;
using System.Xml.Linq;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;

using Kent.Boogaart.KBCsv;
using Axxess.AgencyManagement.App;

using HomeHealthGold.Audits;
using Axxess.AgencyManagement.Domain;

using Enyim.Caching;
using Enyim.Caching.Memcached;
using Enyim.Caching.Configuration;

namespace Axxess.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //AllegenyIntake.Run();
            //FluentAdoTest.Run();
            //HHRGInsertScript.Run();
            //ValidationAndGrouperTest.Run();
            //MembaseTest.Run();
            //AISPatientIntake.Run();
            //KinnserPatientIntake.Run();
            //VisiTrackPatientIntake.Run();
            //VisiTrackPhysicianIntake.Run();
            //VisiTrackPatientIntakeTwo.Run();
            //Ub04Document.Create();
            //HomeCareBizPatientIntake.Run(DataFormat.CSV);
            //HtmlToPdfTest.Run();
            //NPIUpdate.Run();
            //PecosUpdate.Run();
            //DrugUpdate.Run();
            //CryptoTest.Run();
            //CradlePatientEpisodeIntake.Run();
            //CradlePatientEpisodeIntakeTwo.Run();
            //SynergyIntake.Run();
            //SynergyIntakeTwo.Run();
            //SynergyIntakeThree.Run();
            //ManualExcelIntake.Run();
            //ManualExcelIntakeTwo.Run();
            //Console.WriteLine(Axxess.Core.Infrastructure.OperatingSystem.Is64Bit);
            //AddPatientPhysicians.Run();
            //HomeSolutionsNetIntake.Run();
            //LoadPhysicians.Run();
            //MjsOneIntake.Run();
            //AloraHealthIntake.Run();
            //UpdatePhysicianInfo.Run();
            //LoadAllergyProfiles.Run();
            //UpdatePermissions.Run();
            //ManualExcelIntakeThree.Run();
            //ManualCsvIntake.Run();
            //IgeaHomeHealthIntake.Run();

            //AxxessIntake.Run();
            //AxxessIntakeTwo.Run();

            //OhioAgencyScript.Run();
            //CaliAgencyScript.Run();
            //IllinoisAgencyScript.Run();

            //HavenPatientIntake.Run();
             //1 GenerateAdmission.Run();
             //2 CommunicatonNotePhysicianLoad.Run();
             //3 FaceToFaceEncounterPhysicianLoad.Run();
             //4 MedicationProfileHistoryPhysician.Run();
             //5 PhysicianOrderPhysicianLoad.Run();
             //6 PlanOfCarePhysicainLoad.Run();
             //7 StandAlonePlanOfCarePhysicainLoad.Run();
            //HealthMedixIntake.Run();
           // StandAlonePlanOfCarePhysicainLoad.Run();
           // PlanOfCarePhysicainLoad.Run();
            //var oasisAudit = new OasisAudits();
            //var xDocument = oasisAudit.CheckForAuditsXml("B1          00        C-072009    02.003646497171.0  1011352                                                               148227                 N         2011-0071           20110929        1TING FU      GER                  IL60565      320605902M  03206059020              119141217 111846680480012011100101010000 0100000000000                                    707.05 707.22              000001000 707.0503 707.2203 707.0202 707.2102 294.8 03 285.9 020001      000010                                                       02  04     02       02              0300010001  0500030400             0000001000  03  03  03                  05  02                  05                                                                                     0  1              147788174601                                                                                    0000         120110927000000                                                                        0100000002       06020302030211          02  00  00  00  00  00              0300    00  1        04010000060403050600000     00000020202020201   00  03NANANA010101010101010101000100010101                                                                                                                                                                                                                                                                                                     %");
            //var xDocument = oasisAudit.CheckForAuditsXml("A1677937    181466701      7003246         EMINENT HOME HEALTHCARE LLC   8035 E.R.L THORNTON FWY #107                                DALLAS              TX75228      KOCHUKUNJU, KOSHY             2146604404                                                                                                                                                                                      364649717AXXESS AGENCYCORE             9535 FOREST LANE              SUITE 235                     DALLAS              TX75243      NIYI OLAJIDE                  2145757711     2011100611841492683                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             %");
            //var xDocument = oasisAudit.CheckForAuditsXml("Z1000005                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             %");
            //foreach (XElement element in xDocument.Descendants(XName.Get("edit")))
            //{
            //    var error = new LogicalError
            //    {
            //        Header = element.Element("header") != null && element.Element("header").Value.IsNotNullOrEmpty() ? element.Element("header").Value : string.Empty,
            //        Reason = element.Element("reason") != null && element.Element("reason").Value.IsNotNullOrEmpty() ? element.Element("reason").Value : string.Empty,
            //        Explanation = element.Element("explanation") != null && element.Element("explanation").Value.IsNotNullOrEmpty() ? element.Element("explanation").Value : string.Empty,
            //        Action = element.Element("action") != null && element.Element("action").Value.IsNotNullOrEmpty() ? element.Element("action").Value : string.Empty,
            //    };
            //MyHomeCareBizPatientIntake.Run();

            //    var oasisItems = from e in element.Elements(XName.Get("oasis"))
            //                     select e.Value;

            //    if (oasisItems != null && oasisItems.Count<string>() > 0)
            //    {
            //        oasisItems.ForEach(o =>
            //        {
            //            error.OasisItems.Add(o);
            //        });
            //    }

            //    Console.WriteLine(error.ToString());
            //}

            //var cacheAgent = new CacheAgent();
            //var list = cacheAgent.GetPhysicians(new Guid("d0307ff6-9d69-435a-a411-f737b1980bfb"));

            //if (list != null)
            //{
            //    list.ForEach(p =>
            //    {
            //        if (p.IsNotNullOrEmpty())
            //        {
            //            var physician = p.ToObject<AgencyPhysician>();
            //            if (physician != null)
            //            {
            //                Console.WriteLine(physician.DisplayName);
            //            }
            //        }
            //    });
            //}

           

            Console.WriteLine("Script Complete");
            Console.ReadLine();
        }
    }
}
