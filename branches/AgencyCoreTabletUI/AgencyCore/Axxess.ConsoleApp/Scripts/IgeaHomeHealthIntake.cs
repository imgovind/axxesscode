﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class IgeaHomeHealthIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\Best.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Best_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                int rowCounter = 1;
                                PatientData patientData = null;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        if (rowCounter % 3 == 1)
                                        {
                                            patientData = new PatientData();
                                            patientData.AgencyId = "a4849ceb-b86b-47a5-9f91-26c8ad995fdd";
                                            patientData.AgencyLocationId = "6c346afb-907b-44c2-9f4a-3d9e84540622";
                                            patientData.PatientNumber = dataRow.GetValue(0);
                                            var nameDataArray = dataRow.GetValue(1).Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameDataArray != null && nameDataArray.Length > 1)
                                            {
                                                var nameArray = nameDataArray[0].Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (nameArray != null && nameArray.Length > 1)
                                                {
                                                    var firstNameArray = nameArray[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (firstNameArray != null && firstNameArray.Length > 0)
                                                    {
                                                        if (firstNameArray.Length == 2)
                                                        {
                                                            patientData.FirstName = firstNameArray[0].Trim();
                                                            patientData.MiddleInitial = firstNameArray[1].Trim();
                                                        }
                                                        else
                                                        {
                                                            patientData.FirstName = firstNameArray[0].Trim();
                                                        }
                                                    }
                                                    patientData.LastName = nameArray[0];
                                                }

                                                patientData.MedicareNumber = nameDataArray[1].Trim();
                                            }
                                            patientData.AddressCounty = dataRow.GetValue(5);
                                            patientData.Phone = dataRow.GetValue(7).ToPhoneDB();
                                            if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Cert. Start Date: {0}. ", dataRow.GetValue(12));
                                            }

                                            if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Principal Diagnosis: {0}. ", dataRow.GetValue(14));
                                            }
                                        }
                                        else
                                        {
                                            if (rowCounter % 3 == 0)
                                            {
                                                if (dataRow.GetValue(0).IsNotNullOrEmpty())
                                                {
                                                    patientData.BirthDate = dataRow.GetValue(0).ToMySqlDate(0);
                                                }
                                                if (dataRow.GetValue(1).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Address: {0}. ", dataRow.GetValue(1));
                                                }
                                                if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Case Manager: {0}. ", dataRow.GetValue(5));
                                                }
                                                if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Physician Fax: {0}. ", dataRow.GetValue(7));
                                                }
                                                if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                                {
                                                    patientData.StartofCareDate = dataRow.GetValue(12).ToMySqlDate(0);
                                                }
                                                if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Eligibility Status: {0}. ", dataRow.GetValue(14));
                                                }

                                                textWriter.WriteLine(new PatientScript(patientData).ToString());
                                                textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                                textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());

                                                textWriter.Write(textWriter.NewLine);
                                            }
                                            else
                                            {
                                                patientData.SSN = dataRow.GetValue(0).Replace("-", "");
                                                if (dataRow.GetValue(1).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Address: {0}. ", dataRow.GetValue(1));
                                                }
                                                if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Physician: {0}. ", dataRow.GetValue(5));
                                                }
                                                if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Physician Phone: {0}. ", dataRow.GetValue(7));
                                                }
                                                if (dataRow.GetValue(12).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Cert. End Date: {0}. ", dataRow.GetValue(12));
                                                }
                                                if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Disciplines: {0}. ", dataRow.GetValue(14));
                                                }
                                            }
                                        }
                                    }
                                    rowCounter++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
