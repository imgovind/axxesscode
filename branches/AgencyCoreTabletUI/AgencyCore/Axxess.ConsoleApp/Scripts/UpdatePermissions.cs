﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

using Axxess.Core;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using Axxess.OasisC.Domain;
using Axxess.OasisC.Repositories;

using Axxess.AgencyManagement.Enums;
using Axxess.AgencyManagement.Domain;
using Axxess.AgencyManagement.Repositories;

using Axxess.LookUp.Domain;
using Axxess.LookUp.Repositories;

using Excel;
using Kent.Boogaart.KBCsv;

namespace Axxess.ConsoleApp.Tests
{
    public static class UpdatePermissions
    {
        private static readonly IAgencyManagementDataProvider agencyDataProvider = new AgencyManagementDataProvider();

        public static void Run()
        {
            try
            {
                var users = agencyDataProvider.UserRepository.All();
                var i = 1;
                users.ForEach(u =>
                {
                    if (u.Roles.IsInRole(AgencyRoles.Administrator)
                        || u.Roles.IsInRole(AgencyRoles.DoN)
                        || u.Roles.IsInRole(AgencyRoles.CaseManager)
                        || u.Roles.IsInRole(AgencyRoles.Clerk)
                        || u.Roles.IsInRole(AgencyRoles.Scheduler)
                        || u.Roles.IsInRole(AgencyRoles.Biller)
                        || u.Roles.IsInRole(AgencyRoles.OfficeManager)
                        || u.Roles.IsInRole(AgencyRoles.QA))
                    {
                        Console.WriteLine(u.DisplayName);
                        var permissions = u.Permissions.ToObject<List<string>>();

                        if (permissions != null && permissions.Count > 0)
                        {
                            var hasPrintPermission = false;
                            permissions.ForEach(p =>
                            {
                                Permissions permission = p.ToEnum<Permissions>(Permissions.None);
                                if ((ulong)permission == (ulong)Permissions.PrintClinicalDocuments)
                                {
                                    hasPrintPermission = true;
                                    Console.WriteLine(Enum.GetName(typeof(Permissions), permission));
                                }
                            });

                            if (hasPrintPermission == false)
                            {
                                permissions.Add(((ulong)Permissions.PrintClinicalDocuments).ToString());
                                u.PermissionsArray = permissions;
                                if (u.PermissionsArray != null && u.PermissionsArray.Count > 0)
                                {
                                    u.Permissions = u.PermissionsArray.ToXml();
                                }
                                if (agencyDataProvider.UserRepository.UpdateModel(u))
                                {
                                    Console.WriteLine("{0}) {1} Permission updated", i, u.DisplayName);
                                }
                            }
                        }
                        Console.WriteLine();
                        i++;
                    }
                });
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
        }

        public static bool IsInRole(this string roles, AgencyRoles roleId)
        {
            var result = false;
            if (roleId > 0)
            {
                var role = ((int)roleId).ToString();
                if (roles.IsNotNullOrEmpty())
                {
                    var agencyRoles = roles.Split(';');
                    if (agencyRoles.Length > 0)
                    {
                        foreach (string agencyRole in agencyRoles)
                        {
                            if (role.IsEqual(agencyRole))
                            {
                                result = true;
                                break;
                            }
                        }
                    }
                }
            }
            return result;
        }
    }
}
