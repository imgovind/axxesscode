﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class HomeSolutionsNetIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\Integrity.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Integrity_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            int counter = 0;
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                PatientData patientData = null;
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (counter % 2 == 0)
                                    {
                                        patientData = new PatientData();
                                        patientData.AgencyId = "dc38c3ce-9335-4f2a-9499-249f2cdf29f2";
                                        patientData.AgencyLocationId = "459f7c5a-15d6-4bb3-a39e-286a88150940";
                                        patientData.MiddleInitial = "";
                                        patientData.AddressLine1 = "";
                                        patientData.AddressLine2 = "";
                                        patientData.AddressCity = "";
                                        patientData.AddressState = "";
                                        patientData.AddressZipCode = "";
                                        patientData.MaritalStatus = "Unknown";

                                        if (dataRow.GetValue(0).IsNotNullOrEmpty())
                                        {
                                            var nameIdArray = dataRow.GetValue(0).Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameIdArray != null && nameIdArray.Length > 1)
                                            {
                                                patientData.PatientNumber = nameIdArray[1].Trim();

                                                var nameArray = nameIdArray[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (nameArray != null && nameArray.Length > 1)
                                                {
                                                    patientData.FirstName = nameArray[1].Trim();
                                                    patientData.LastName = nameArray[0].Trim();
                                                }
                                            }
                                        }

                                        if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                        {
                                            patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(4))).ToString("yyyy-M-d");
                                        }

                                        if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                        {
                                            patientData.PatientStatusId = "2";
                                            patientData.DischargeDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(5))).ToString("yyyy-M-d");
                                        }
                                        else
                                        {
                                            patientData.PatientStatusId = "1";
                                        }

                                        if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                        {
                                            patientData.SSN = dataRow.GetValue(8).Replace("-", "");
                                        }

                                        if (dataRow.GetValue(11).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("Primary Diagnosis: {0}. ", dataRow.GetValue(11));
                                        }

                                        patientData.Gender = dataRow.GetValue(16).IsEqual("F") ? "Female" : "Male";

                                        if (dataRow.GetValue(21).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("Visits: {0}. ", dataRow.GetValue(21));
                                        }
                                    }
                                    else
                                    {
                                        if (dataRow.GetValue(1).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("{0}. ", dataRow.GetValue(1));
                                        }
                                        patientData.MedicareNumber = dataRow.GetValue(7);
                                        textWriter.WriteLine(new PatientScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                        textWriter.Write(textWriter.NewLine);
                                    }
                                    counter++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
