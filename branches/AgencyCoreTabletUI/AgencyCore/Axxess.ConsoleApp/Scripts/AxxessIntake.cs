﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;

    using Axxess.Core.Extension;

    public static class AxxessIntake
    {
        private static Sheet sheet;
        private static HSSFWorkbook workBook;
        private static string input = Path.Combine(App.Root, "Files\\Ropheka3.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Ropheka3_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string npioutput = Path.Combine(App.Root, string.Format("Files\\Ropheka_Physician3_{0}.xls", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            Initialize();
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                int i = 1;
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        var patientData = new PatientData();
                                        patientData.AgencyId = "b19d9289-8c18-467a-abfa-4bb6763324c5";
                                        patientData.AgencyLocationId = "1e58c2db-ebfa-438a-850a-2840ab8cc829";

                                        patientData.PatientStatusId = "1";
                                        patientData.FirstName = dataRow.GetValue(0);
                                        patientData.LastName = dataRow.GetValue(1);
                                        patientData.Gender = dataRow.GetValue(2).ToLower().ToTitleCase();
                                        patientData.PatientNumber = dataRow.GetValue(3);
                                        patientData.MedicareNumber = dataRow.GetValue(4);
                                        if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                        {
                                            patientData.BirthDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(5))).ToString("yyyy-M-d");
                                        }
                                        patientData.SSN = dataRow.GetValue(6).Replace(" ", "");
                                        if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                        {
                                            patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(7))).ToString("yyyy-M-d");
                                        }

                                        if (dataRow.GetValue(9).ToLower().Contains("married"))
                                        {
                                            patientData.MaritalStatus = "Married";
                                        } 
                                        else if (dataRow.GetValue(9).ToLower().Contains("single"))
                                        {
                                            patientData.MaritalStatus = "Single";
                                        }
                                        else if (dataRow.GetValue(9).ToLower().Contains("widowed"))
                                        {
                                            patientData.MaritalStatus = "Widowed";
                                        }
                                        else if (dataRow.GetValue(9).ToLower().Contains("divorced"))
                                        {
                                            patientData.MaritalStatus = "Divorced";
                                        }
                                        else
                                        {
                                            patientData.MaritalStatus = "Unknown";
                                        }

                                        patientData.AddressLine1 = dataRow.GetValue(10);
                                        patientData.AddressLine2 = "";
                                        patientData.AddressCity = dataRow.GetValue(11);
                                        patientData.AddressState = dataRow.GetValue(12).ToUpper();
                                        patientData.AddressZipCode = dataRow.GetValue(13);
                                        patientData.Phone = dataRow.GetValue(14).ToPhoneDB();
                                        
                                        textWriter.WriteLine(new PatientScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());
                                        textWriter.Write(textWriter.NewLine);

                                        Row excelRow = sheet.CreateRow(i);
                                        excelRow.CreateCell(0).SetCellValue(patientData.PatientId.ToString());
                                        excelRow.CreateCell(1).SetCellValue(patientData.PatientNumber);
                                        excelRow.CreateCell(2).SetCellValue(patientData.LastName);
                                        excelRow.CreateCell(3).SetCellValue(patientData.FirstName);
                                        excelRow.CreateCell(4).SetCellValue(dataRow.GetValue(22));
                                        excelRow.CreateCell(5).SetCellValue("");
                                        excelRow.CreateCell(6).SetCellValue(dataRow.GetValue(15).IsNotNullOrEmpty() && dataRow.GetValue(15).Split(' ').Length > 1 ? dataRow.GetValue(15).Split(' ')[1] : string.Empty);
                                        excelRow.CreateCell(7).SetCellValue(dataRow.GetValue(15).IsNotNullOrEmpty() && dataRow.GetValue(15).Split(' ').Length > 1 ? dataRow.GetValue(15).Split(' ')[0] : string.Empty);
                                        excelRow.CreateCell(8).SetCellValue(dataRow.GetValue(15));
                                        excelRow.CreateCell(9).SetCellValue(dataRow.GetValue(16));
                                        excelRow.CreateCell(10).SetCellValue(dataRow.GetValue(17));
                                        excelRow.CreateCell(11).SetCellValue(dataRow.GetValue(18));
                                        excelRow.CreateCell(12).SetCellValue(dataRow.GetValue(19));
                                        excelRow.CreateCell(13).SetCellValue(dataRow.GetValue(20).ToPhoneDB());
                                        excelRow.CreateCell(14).SetCellValue(dataRow.GetValue(21).ToPhoneDB());

                                        i++;
                                    }
                                }
                                Write();
                            }
                        }
                    }
                }
            }
        }

        private static void Initialize()
        {
            workBook = new HSSFWorkbook();

            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = dsi;

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export";
            workBook.SummaryInformation = si;

            sheet = workBook.CreateSheet("Physicians");

            Font headerFont = workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            CellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Row headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("PatientGUID");
            headerRow.CreateCell(1).SetCellValue("PatientHIC");
            headerRow.CreateCell(2).SetCellValue("PatientLastName");
            headerRow.CreateCell(3).SetCellValue("PatientFirstName");
            headerRow.CreateCell(4).SetCellValue("PhysicianNPI");
            headerRow.CreateCell(5).SetCellValue("Physiciantitle");
            headerRow.CreateCell(6).SetCellValue("Physicianlname");
            headerRow.CreateCell(7).SetCellValue("Physicianfname");
            headerRow.CreateCell(8).SetCellValue("Physicianname");
            headerRow.CreateCell(9).SetCellValue("Physicianaddr");
            headerRow.CreateCell(10).SetCellValue("Physiciancity");
            headerRow.CreateCell(11).SetCellValue("Physicianstate");
            headerRow.CreateCell(12).SetCellValue("Physicianzip");
            headerRow.CreateCell(13).SetCellValue("Physicianphone");
            headerRow.CreateCell(14).SetCellValue("Physicianfax");
            headerRow.RowStyle = headerStyle;
        }

        private static void Write()
        {
            int columnCounter = 0;
            do
            {
                sheet.AutoSizeColumn(columnCounter);
                columnCounter++;
            }
            while (columnCounter < 15);

            using (FileStream fileStream = new FileStream(npioutput, FileMode.OpenOrCreate, FileAccess.Write))
            {
                workBook.Write(fileStream);
            }
        }
    }
}
