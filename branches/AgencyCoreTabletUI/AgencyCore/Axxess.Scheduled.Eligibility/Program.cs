﻿using System;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using Axxess.Scheduled.Eligibility.Data;
using Axxess.Scheduled.Eligibility.Model;
using Axxess.Scheduled.Eligibility.Service;

namespace Axxess.Scheduled.Eligibility
{
    class Program
    {
        const int MAX_THREADS = 10;
        static Queue<PatientEligibilityData> itemList = Database.GetPatients();

        static void Main(string[] args)
        {
            Run();

            Console.Read();
        }

        private static void Run()
        {
            int counter = 0;
            var threadList = new List<Thread>();
            int takeAmount = (int)Math.Ceiling(itemList.Count / (double)MAX_THREADS);

            do
            {
                var workList = itemList.Dequeue(takeAmount).ToList();
                var thread = new Thread(() => Work(workList));
                thread.Name = string.Format("Thread{0}", counter);
                threadList.Add(thread);

                counter++;
            } while (counter < MAX_THREADS);

            threadList.ForEach(thread =>
            {
                thread.Start();
            });

            threadList.ForEach(thread =>
            {
                thread.Join();
            });
        }

        private static void Work(List<PatientEligibilityData> list)
        {
            list.ForEach(patient =>
            {
                Console.WriteLine("Patient: {0} {1} working on {2}", patient.FirstName, patient.LastName, Thread.CurrentThread.Name);
                var jsonResult = MedicareService.Verify(patient);
                if (jsonResult.IsNotNullOrEmpty())
                {
                    var result = jsonResult.ToEligibilityResult();

                    if (result != null)
                    {
                        if (result.Request_Validation != null && result.Request_Validation.success.IsNotNullOrEmpty() && result.Request_Validation.success.IsEqual("yes"))
                        {
                            Console.WriteLine("Rejection Reason: {0}", result.Request_Validation.reject_reason_code.IsNotNullOrEmpty() ? result.Request_Validation.reject_reason_code : string.Empty);
                            Console.WriteLine("Follow up Action Code: {0}", result.Request_Validation.follow_up_action_code.IsNotNullOrEmpty() ? result.Request_Validation.follow_up_action_code : string.Empty);
                        }
                        else
                        {
                            if (result.Subscriber != null && result.Subscriber.success.IsNotNullOrEmpty() && result.Subscriber.success.IsEqual("yes"))
                            {
                                Console.WriteLine("Identification Code: {0}", result.Subscriber.identification_code.IsNotNullOrEmpty() ? result.Subscriber.identification_code : string.Empty);
                                Console.WriteLine("Date of Birth: {0}", result.Subscriber.date.IsNotNullOrEmpty() ? result.Subscriber.date.ToDateTimeString() : string.Empty);
                                Console.WriteLine("Subscriber Name: {0}, {1} {2}", result.Subscriber.last_name.IsNotNullOrEmpty() ? result.Subscriber.last_name : string.Empty, result.Subscriber.first_name.IsNotNullOrEmpty() ? result.Subscriber.first_name : string.Empty, result.Subscriber.middle_name.IsNotNullOrEmpty() ? result.Subscriber.middle_name : string.Empty);
                                Console.WriteLine("Gender: {0}", result.Subscriber.gender.IsNotNullOrEmpty() ? result.Subscriber.gender : string.Empty);
                            }
                            if (result.Medicare_Part_A != null && result.Medicare_Part_A.success.IsNotNullOrEmpty() && result.Medicare_Part_A.success.IsEqual("yes"))
                            {
                                Console.WriteLine("Medicare Part A: {0}", result.Medicare_Part_A.date.IsNotNullOrEmpty() ? result.Medicare_Part_A.date.ToDateTimeString() + " - Current" : string.Empty);
                            }
                            if (result.Medicare_Part_B != null && result.Medicare_Part_B.success.IsNotNullOrEmpty() && result.Medicare_Part_B.success.IsEqual("yes"))
                            {
                                Console.WriteLine("Medicare Part B: {0}", result.Medicare_Part_B.date.IsNotNullOrEmpty() ? result.Medicare_Part_B.date.ToDateTimeString() + " - Current" : string.Empty);
                            }
                            if (result.Health_Benefit_Plan_Coverage != null && !result.Health_Benefit_Plan_Coverage.success.IsEqual("yes"))
                            {
                                Console.WriteLine("Primary Payer: Medicare");
                            }
                            if (result.Episode != null && result.Episode.name.IsNotNullOrEmpty())
                            {
                                Console.WriteLine("Last Home Care Episode Payer: {0}", result.Episode.name.IsNotNullOrEmpty() ? result.Episode.name : string.Empty);
                                Console.WriteLine("Last Home Care Episode NPI#: {0}", result.Episode.reference_id.IsNotNullOrEmpty() ? result.Episode.reference_id : string.Empty);
                                Console.WriteLine("Last Home Care Episode Provider Code: {0}", result.Episode.provider_code.IsNotNullOrEmpty() ? result.Episode.provider_code : string.Empty);
                                Console.WriteLine("Last Home Care Episode Episode Date Range: {0} - {1}", result.Episode.period_start.IsNotNullOrEmpty() ? result.Episode.period_start.ToDateTimeString() : string.Empty,
                                    result.Episode.period_end.IsNotNullOrEmpty() ? result.Episode.period_end.ToDateTimeString() : string.Empty);
                            }
                            if (result.Health_Benefit_Plan_Coverage != null && result.Health_Benefit_Plan_Coverage.success.IsNotNullOrEmpty() && result.Health_Benefit_Plan_Coverage.success.IsEqual("yes"))
                            {
                                Console.WriteLine("Health Benefit Plan Coverage: {0}", result.Health_Benefit_Plan_Coverage.name.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.name : string.Empty);
                                Console.WriteLine("Insurance Type: {0}", result.Health_Benefit_Plan_Coverage.insurance_type.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.insurance_type : string.Empty);
                                Console.WriteLine("Reference Id Qualifier: {0}", result.Health_Benefit_Plan_Coverage.reference_id_qualifier.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.reference_id_qualifier : string.Empty);
                                Console.WriteLine("Address: {0}", result.Health_Benefit_Plan_Coverage.address1.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.address1 : string.Empty);
                                Console.WriteLine("Phone Number: {0}", result.Health_Benefit_Plan_Coverage.phone.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.phone : string.Empty);
                                Console.WriteLine("Health Benefit Plan Payer: {0}", result.Health_Benefit_Plan_Coverage.payer.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.payer : string.Empty);
                                Console.WriteLine("Reference Id: {0}", result.Health_Benefit_Plan_Coverage.reference_id.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.reference_id : string.Empty);
                                Console.WriteLine("Date: {0}", result.Health_Benefit_Plan_Coverage.date.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.date.ToDateTimeString() : string.Empty);
                                Console.WriteLine("City: {0}", result.Health_Benefit_Plan_Coverage.city.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.city : string.Empty);
                                Console.WriteLine("State: {0}", result.Health_Benefit_Plan_Coverage.state.IsNotNullOrEmpty() ? result.Health_Benefit_Plan_Coverage.state : string.Empty);
                                Console.WriteLine("Health Benefit Plan Payer: {0}");
                            }
                        }

                        if (MedicareService.Insert(patient, jsonResult))
                        {
                            Console.WriteLine("Result Saved into the Database.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No Result.");
                    }
                }
                else
                {
                    Console.WriteLine("No Result.");
                }
                Console.WriteLine("========================================================================");
            });
        }
    }
}
