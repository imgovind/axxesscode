﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Scheduled.Eligibility.Model
{
    public class PatientEligibilityData
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid EligibilityId { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public DateTime DateofBirth { get; set; }
        public string MedicareNumber { get; set; }
        public string Gender { get; set; }
        public DateTime LastEligibilityCheckDate { get; set; }
    }
}
