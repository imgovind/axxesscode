﻿var Agency = {
    Edit: function(Id) { acore.open("editagency", 'Agency/Edit', function() { Agency.InitEdit(); }, { id: Id }); },
    Delete: function(Id) { U.DeleteTemplate("Agency", Id); },
    InitEdit: function() {
        U.PhoneAutoTab('Edit_Agency_SubmitterPhone');
        U.PhoneAutoTab('Edit_Agency_SubmitterFax');
        $(".names").alpha({ nocaps: false });

        if ($("#Edit_Agency_AxxessBiller").is(':checked')) {
            if ($("#Edit_Agency_Payor").val() == "1") {
                $("#Edit_Agency_SubmitterId").val("SW23071");
                $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult");
            } else if ($("#Edit_Agency_Payor").val() == "2") {
                $("#Edit_Agency_SubmitterId").val("CH0001841");
                $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult");
            } else if ($("#Edit_Agency_Payor").val() == "3") {
                $("#Edit_Agency_SubmitterId").val("IA003133");
                $("#Edit_Agency_SubmitterName").val("Axxess Consult Inc");
            } else { $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult"); }

            $("#Edit_Agency_SubmitterPhone1").val("214");
            $("#Edit_Agency_SubmitterPhone2").val("575");
            $("#Edit_Agency_SubmitterPhone3").val("7711");
            $("#Edit_Agency_SubmitterFax1").val("214");
            $("#Edit_Agency_SubmitterFax2").val("575");
            $("#Edit_Agency_SubmitterFax3").val("7722");
        }

        $("#Edit_Agency_AxxessBiller").click(function() {
            if ($(this).is(':checked')) {
                if ($("#Edit_Agency_Payor").val() == "1") {
                    $("#Edit_Agency_SubmitterId").val("SW23071");
                    $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult");
                } else if ($("#Edit_Agency_Payor").val() == "2") {
                    $("#Edit_Agency_SubmitterId").val("CH0001841");
                    $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult");
                } else if ($("#Edit_Agency_Payor").val() == "3") {
                    $("#Edit_Agency_SubmitterId").val("IA003133");
                    $("#Edit_Agency_SubmitterName").val("Axxess Consult Inc");
                } else { $("#Edit_Agency_SubmitterName").val("Axxess Healthcare Consult"); }

                $("#Edit_Agency_SubmitterPhone1").val("214");
                $("#Edit_Agency_SubmitterPhone2").val("575");
                $("#Edit_Agency_SubmitterPhone3").val("7711");
                $("#Edit_Agency_SubmitterFax1").val("214");
                $("#Edit_Agency_SubmitterFax2").val("575");
                $("#Edit_Agency_SubmitterFax3").val("7722");
            }
            else {
                $("#Edit_Agency_SubmitterId").add("#Edit_Agency_SubmitterName").add("input[name=SubmitterPhoneArray]").add("input[name=SubmitterFaxArray]").val("");
            }
        });

        $("input[name=IsAgreementSigned]").change(function() {
            if ($(this).val() == "true") $("#Edit_Agency_TrialPeriod").val("").attr("disabled", "disabled");
            else $("#Edit_Agency_TrialPeriod").removeAttr("disabled");
        });

        U.InitEditTemplate("Agency");
    },
    Users: function(Id) { acore.open("listagencyusers", 'Agency/UserGrid', function() { }, { agencyId: Id }); },
    Locations: function(Id) { acore.open("listagencylocations", 'Agency/LocationGrid', function() { }, { agencyId: Id }); },
    InitNew: function() {
        Lookup.loadStates();
        U.PhoneAutoTab('New_Agency_Phone');
        U.PhoneAutoTab('New_Agency_Fax');
        U.PhoneAutoTab('New_Agency_SubmitterPhone');
        U.PhoneAutoTab('New_Agency_SubmitterFax');
        U.PhoneAutoTab('New_Agency_ContactPhone');
        $(".names").alpha({ nocaps: false });

        $("input[name=New_Agency_SameAsAdmin]").click(function() {
            if ($(this).is(':checked')) {
                $("#New_Agency_ContactPersonEmail").val($("#New_Agency_AdminUsername").val());
                $("#New_Agency_ContactPersonFirstName").val($("#New_Agency_AdminFirstName").val());
                $("#New_Agency_ContactPersonLastName").val($("#New_Agency_AdminLastName").val());
            } else $("#New_Agency_ContactPersonEmail").add("#New_Agency_ContactPersonFirstName").add("#New_Agency_ContactPersonLastName").val('');
        });
        $("#New_Agency_AxxessBiller").click(function() {
            if ($(this).is(':checked')) {
                if ($("#New_Agency_Payor").val() == "1") {
                    $("#New_Agency_SubmitterId").val("SW23071");
                    $("#New_Agency_SubmitterName").val("Axxess Healthcare Consult");
                } else if ($("#New_Agency_Payor").val() == "2") {
                    $("#New_Agency_SubmitterId").val("CH0001841");
                    $("#New_Agency_SubmitterName").val("Axxess Healthcare Consult");
                } else if ($("#New_Agency_Payor").val() == "3") {
                    $("#New_Agency_SubmitterId").val("IA003133");
                    $("#New_Agency_SubmitterName").val("Axxess Consult Inc");
                } else {
                    $("#New_Agency_SubmitterName").val("Axxess Healthcare Consult");
                }

                $("#New_Agency_SubmitterPhone1").val("214");
                $("#New_Agency_SubmitterPhone2").val("575");
                $("#New_Agency_SubmitterPhone3").val("7711");
                $("#New_Agency_SubmitterFax1").val("214");
                $("#New_Agency_SubmitterFax2").val("575");
                $("#New_Agency_SubmitterFax3").val("7722");
            }
            else {
                $("#New_Agency_SubmitterId").add("#New_Agency_SubmitterName").add("input[name=SubmitterPhoneArray]").add("input[name=SubmitterFaxArray]").val("");
            }
        });
        $("input[name=IsAgreementSigned]").change(function() {
            if ($(this).val() == "true") $("#New_Agency_TrialPeriod").val("").attr("disabled", "disabled");
            else $("#New_Agency_TrialPeriod").removeAttr("disabled");
        });
        U.InitNewTemplate("Agency");
    },
    RebindList: function() { U.rebindTGrid($('#List_Agencies')); },
    FilterTGrid: function(text) {
        search = text.split(" ");
        $("tr", "#List_Agencies .t-grid-content").removeClass("match").hide();
        for (var i = 0; i < search.length; i++) {
            $("td", "#List_Agencies .t-grid-content").each(function() {
                if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
            });
        }
        $("tr.match", "#List_Agencies .t-grid-content").removeClass("t-alt").show();
        $("tr.match:even", "#List_Agencies .t-grid-content").addClass("t-alt");
    }
}