﻿var SystemMessage = {
    _ckeloaded: false,
    InitNew: function() {
        U.InitTemplate($('#newSystemMessageForm'), function() { UserInterface.CloseWindow("newsystemmessage"); }, "Message has been sent successfully.", function() {
            $("textarea[name=Body]").val(CKEDITOR.instances['New_SystemMessage_Body'].getData());
        });
    
        if (!this._ckeloaded) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() { SystemMessage._ckeloaded = true; SystemMessage.InitCKEditor() });
        else SystemMessage.InitCKEditor();
    },
    InitCKEditor: function() {
        CKEDITOR.replace('New_SystemMessage_Body', {
            skin: 'office2003',
            resize_enabled: false,
            height: '100%',
            removePlugins: 'elementspath',
            toolbarCanCollapse: false,
            toolbar: [['Source', 'Preview', '-', 'Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                'JustifyBlock', '-', 'Link', 'Unlink', '-', 'Font', 'FontSize', 'TextColor', '-', 'SpellChecker', 'Scayt']]
        });
    }
}

var DashboardMessage = {
    _ckeloaded: false,
    InitNew: function() {
        U.InitTemplate($('#newDashboardMessageForm'), function() { UserInterface.CloseWindow("newdashboardmessage"); }, "Dashboard Content has been sent successfully.", function() {
            $("textarea[name=Text]").val(CKEDITOR.instances['New_DashboardMessage_Body'].getData());
        });

        if (!this._ckeloaded) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() { DashboardMessage._ckeloaded = true; DashboardMessage.InitCKEditor() });
        else DashboardMessage.InitCKEditor();
    },
    InitCKEditor: function() {
        CKEDITOR.replace('New_DashboardMessage_Body', {
            skin: 'office2003',
            resize_enabled: false,
            height: '100%',
            removePlugins: 'elementspath',
            toolbarCanCollapse: false,
            toolbar: [['Source', 'Preview', '-', 'Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                'JustifyBlock', '-', 'Link', 'Unlink', '-', 'Font', 'FontSize', 'TextColor', '-', 'SpellChecker', 'Scayt']]
        });
    },
    RebindList: function() { U.rebindTGrid($('#List_Dashboard_Messages')); }
}