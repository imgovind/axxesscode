﻿var U =
{
    ajaxError: "%3Cdiv class=%22ajaxerror%22%3E%3Cimg src=%22/Images/icons/error.png%22 /%3E%3Ch1%3EThere was an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for further assistance.%3C/div%3E",
    tooltip: function(o, c) { o.each(function() { if ($(this).attr("tooltip") != undefined) { $(this).tooltip({ track: true, showURL: false, top: 10, left: 10, extraClass: c, bodyHandler: function() { return $(this).attr("tooltip"); } }); } }); },
    rebindTGrid: function(g, a) { if (g.data('tGrid') != null) g.data('tGrid').rebind(a); },
    growl: function(m, t) { $.jGrowl($.trim(m), { theme: t, life: 5000 }); },
    PhoneAutoTab: function(name) {
        $('#' + name + '1').autotab({ target: name + '2', format: 'numeric' });
        $('#' + name + '2').autotab({ target: name + '3', format: 'numeric', previous: name + '1' });
        $('#' + name + '3').autotab({ format: 'numeric', previous: name + '2' });
    },
    FilterResults: function(type) {
        $('#' + type + 'MainResult').empty().addClass("loading");
        U.rebindTGrid($('#' + type + 'SelectionGrid'), { statusId: $("select." + type + "StatusDropDown").val(), paymentSourceId: $("select." + type + "PaymentDropDown").val(), name: $("#txtSearch_" + type + "_Selection").val() });
        /*setTimeout(function() {
        $('#' + type + 'MainResult').removeClass("loading");
        if ($("#" + type + "SelectionGrid .t-grid-content tr").length) $("#" + type + "SelectionGrid .t-grid-content tr:first").click();
        else $('#' + type + 'MainResult').html('<p>No patients found matching your search criteria!</p>');
        }, 5000);*/
    },
    HandlerHelperTemplate: function(t, w, form, control, action) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
                form.find('.form-omitted :input').val("").end().find('.form-omitted:input').val("");
            },
            success: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    if (control.html() == "Save &amp; Exit") {
                        UserInterface.CloseWindow(w);
                        Schedule.Rebind();
                        User.RebindScheduleList();
                    }
                    else if (control.html() == "Save &amp; Continue") Oasis.NextTab("#" + t + "Tabs.tabs");
                    else if (control.html() == "Save &amp; Check for Errors") action();
                    else if (control.html() == "Check for Errors") action();
                    else if (control.html() == "Save &amp; Complete") action();
                } else U.growl(result.responseText, "error");
            },
            error: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    if (control.html() == "Save &amp; Exit") {
                        UserInterface.CloseWindow(w);
                        Schedule.Rebind();
                        User.RebindScheduleList();
                    }
                    else if (control.html() == "Save &amp; Continue") Oasis.NextTab("#" + t + "Tabs.tabs");
                    else if (control.html() == "Save &amp; Check for Errors") action();
                    else if (control.html() == "Check for Errors") action();
                    else if (control.html() == "Save &amp; Complete") action();
                } else U.growl(result.responseText, "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    Delete: function(name, url, data, callback, bypassAlerts) {
        if (bypassAlerts || confirm("Are you sure you want to update this " + name.toLowerCase() + "?")) {
            U.postUrl(url, data, function(result) {
                if (result.isSuccessful) {
                    if (callback != undefined && typeof (callback) == 'function') callback();
                    if (!bypassAlerts) U.growl(name + " has been successfully updated.", "success");
                } else U.growl(result.errorMessage, "error");
            });
        }
    },
    AutoComplete: function(field, url, param, callback) {
        field.autocomplete(url, { max: 50, minChars: 2, dataType: 'json', width: 400, extraParams: param,
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                return rows;
            },
            formatItem: function(row, i, n) { return row.Code + '-' + row.ShortDescription; }
        }).result(function(event, row, formatted) {
            if (callback != undefined && typeof (callback) == 'function') callback();
        });
    },
    showIfOtherSelected: function(selectList, field) {
        if (field.val().length == 0) field.attr("disabled", "disabled");
        selectList.change(function() {
            if (selectList.val() == "Other") field.attr("disabled", "").addClass("required").focus();
            else field.attr("disabled", "disabled").val("").removeClass("required");
        });
    },
    showIfOtherChecked: function(checkbox, field) {
        if (checkbox.is(":checked")) field.show();
        else field.hide();
        checkbox.bind('click', function() {
            if ($(this).is(":checked")) field.show();
            else field.hide().val("");
        });
    },
    showIfChecked: function(checkbox, field) {
        if (checkbox.is(":checked")) field.show().removeClass('form-omitted');
        else field.hide().addClass('form-omitted');
        checkbox.bind('click', function() { field.toggleClass('form-omitted').toggle(); });
    },
    hideIfChecked: function(checkbox, field) {
        if (checkbox.is(":checked")) field.hide().addClass('form-omitted');
        else field.show().removeClass('form-omitted');
        checkbox.bind('click', function() { field.toggleClass('form-omitted').toggle(); });
    },
    showIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.show().removeClass('form-omitted');
        else field.hide().addClass('form-omitted');
        $(':radio[name=' + group + ']').bind('click', function() { if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.show().removeClass('form-omitted'); else field.hide().addClass('form-omitted'); });
    },
    hideIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.hide().addClass('form-omitted');
        else field.show().removeClass('form-omitted');
        $(':radio[name=' + group + ']').bind('click', function() { if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.hide().addClass('form-omitted'); else field.show().removeClass('form-omitted'); });
    },
    showIfSelectEquals: function(select, value, field) {
        if (select.val() == value) field.show().removeClass('form-omitted');
        else field.hide().addClass('form-omitted');
        select.bind('change', function() {
            if (select.val() == value) field.show().removeClass('form-omitted');
            else field.hide().addClass('form-omitted');
        });
    },
    noneOfTheAbove: function(checkbox, group) {
        if (checkbox.is(":checked")) group.each(function() { if ($(this).attr('id') != checkbox.attr('id') && $(this).is(":checked")) $(this).click(); });
        group.bind('change', function() { if ($(this).attr('id') != checkbox.attr('id') && $(this).is(":checked") && checkbox.is(":checked")) checkbox.click(); });
        checkbox.bind('change', function() { group.each(function() { if ($(this).attr('id') != checkbox.attr('id') && $(this).is(":checked")) $(this).click(); }); });
    },
    DeleteTemplate: function(name, id) { U.Delete(name, name + "/Delete", { Id: id }, function() { eval(name + ".RebindList()"); }); },
    InitTemplate: function(formobj, callback, message, before) {
        $(".numeric").numeric();
        formobj.validate({
            submitHandler: function(form) {
                if (before != undefined && typeof (before) == 'function') before();
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            if (callback != undefined && typeof (callback) == 'function') callback();
                            U.growl(message, "success");
                        } else U.growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditTemplate: function(type) {
        U.InitTemplate($("#edit" + type + "Form"), function() {
            eval(type + ".RebindList()");
            UserInterface.CloseWindow("edit" + type.toLowerCase());
        }, type + " successfully updated");
    },
    InitNewTemplate: function(type) {
        U.InitTemplate($("#new" + type + "Form"), function() {
            eval(type + ".RebindList()");
            UserInterface.CloseWindow("new" + type.toLowerCase());
        }, "New " + type + " successfully added");
    },
    showDialog: function(popupBox, onReady) {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: $(popupBox),
            css: {
                top: ($(window).height() - $(popupBox).height()) / 2 + 'px',
                left: ($(window).width() - $(popupBox).width()) / 2 + 'px'
            },
            onBlock: function() {
                if (typeof (onReady) == 'function') {
                    onReady();
                }
            }
        });
    },
    closeDialog: function() {
        $.unblockUI();
    },
    getUrl: function(url, input, onSuccess) {
        $.ajax({
            url: url,
            data: input,
            dataType: 'json',
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            }
        });
    },
    postUrl: function(url, input, onSuccess, onFailure) {
        $.ajax({
            url: url,
            data: input,
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
            },
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            },
            error: function(data) {
                if (typeof (onFailure) == 'function') {
                    onFailure(data);
                }
            }
        });
    },
    block: function() {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: 'Loading...',
            css: {
                color: '#fff',
                opacity: .6,
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                top: ($(window).height() - 40) / 2 + 'px',
                left: ($(window).width() - 100) / 2 + 'px'
            }
        });
    },
    unBlock: function(selector) {
        $.unblockUI();
    },
    toTitleCase: function(text) {
        var txt = '';
        var txtArray = text.toLowerCase().split(' ');
        if (txtArray.length > 1) {
            var i = 0;
            for (i = 0; i < txtArray.length; i++) {
                txt += txtArray[i].substr(0, 1).toUpperCase() + txtArray[i].substr(1) + ' ';
            }
        }
        else {
            txt = text.toLowerCase().substr(0, 1).toUpperCase() + text.toLowerCase().substr(1);
        }
        return txt;
    }
};


jQuery.fn.contains = function(txt) { return jQuery(this).indexOf(txt) >= 0; }

