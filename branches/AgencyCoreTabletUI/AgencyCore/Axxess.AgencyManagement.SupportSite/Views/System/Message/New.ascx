﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("AddMessage", "System", FormMethod.Post, new { @id = "newSystemMessageForm" })) { %>

<div id="messagingContainer" class="wrapper">
    <div class="newMessageContent" id="newMessageContent">
        <div class="inboxSubHeader"><span>New System Message</span>
            <div class="buttons float_right"><ul><li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newsystemmessage');">Cancel</a></li></ul></div>
        </div>
        <div class="buttons float_left"><ul><li class="send"><a href="javascript:void(0);" onclick="$(this).closest('form').submit();"><span class="img icon send"></span><br />Send</a></li></ul></div>
        <div class="newMessageContentPanel clear">
            <div class="newMessageRow"><label for="New_SystemMessage_Subject">Subject:</label><input type="text" id="New_SystemMessage_Subject" name="Subject" maxlength="150" /></div>
            <div class="newMessageRow"><label for="New_SystemMessage_CreatedBy">Created By:</label><span id="New_SystemMessage_CreatedBy"><%= Current.DisplayName %></span></div>
            <div class="newMessageRow"><label for="New_SystemMessage_CreatedDate">Created By:</label><span id="New_SystemMessage_CreatedDate"><%= DateTime.Now.ToShortDateString() %></span></div>
        </div>
        <div class="newMessageRow" id="newMessageBodyDiv">
            <div>
                <textarea id="New_SystemMessage_Body" name="Body"></textarea>
            </div>
        </div>
    </div>
</div>
<%} %>
<script type="text/javascript">
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>