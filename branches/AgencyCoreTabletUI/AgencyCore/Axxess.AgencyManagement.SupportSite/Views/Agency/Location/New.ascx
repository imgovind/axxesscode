﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<%= string.Format("{0}{1}{2}", "<script type='text/javascript'>acore.renamewindow('New Location | ", Model.Name, "','newagencylocation');</script>")%>
<% using (Html.BeginForm("Add", "Location", FormMethod.Post, new { @id = "newLocationForm" })) { %>
<%= Html.Hidden("AgencyId", Model.Id, new { @id = "New_Location_AgencyId" })%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row"><label for="New_Location_Name" class="float_left">Location Name:</label><div class="float_right"> <%=Html.TextBox("Name", "", new { @id = "New_Location_Name", @class = "text input_wrapper required", @maxlength = "20" })%></div></div>
            <div class="row"><label for="New_Location_CustomId" class="float_left">Custom Id:</label><div class="float_right"> <%=Html.TextBox("CustomId", "", new { @id = "New_Location_CustomId", @class = "text input_wrapper required" })%></div></div>
         </div>   
        <div class="column">               
            <div class="row"><label for="New_Location_MedicareProviderNumber" class="float_left">Medicare Provider Number:</label><div class="float_right"><%=Html.TextBox("MedicareProviderNumber", "", new { @id = "New_Location_MedicareProviderNumber", @class = "text input_wrapper required", @maxlength = "10" })%></div></div>
        </div>
    </fieldset> 
      <fieldset>
        <legend>Submitter Information (Medicare)</legend>
        <div class="float_left"><%=Html.CheckBox("IsSubmitterInfoTheSame", true, new { @id = "New_Location_IsSubmitterInfoTheSame", @class = "radio" })%>&nbsp;<label for="New_Location_IsSubmitterInfoTheSame">Check here if this branch office uses the same submitter info with the main branch.</label></div>
        <div class="clear"></div>
        <div id="New_Location_SubmiterInfoContent">
            <div class="column">
                <div class="row"><label for="New_Agency_SubmitterId">Submitter Id:</label><div class="float_right"><%=Html.TextBox("SubmitterId", "", new { @id = "New_Location_SubmitterId", @maxlength = "15", @class = "text input_wrapper required" })%></div></div>
                <div class="row"><label for="New_Agency_SubmitterName">Submitter Name:</label><div class="float_right"><%=Html.TextBox("SubmitterName", "", new { @id = "New_Location_SubmitterName", @maxlength = "50", @class = "text input_wrapper required" })%></div></div>
            </div>
            <div class="column">
                <div class="row"><label for="New_Agency_SubmitterPhone1">Submitter Phone Number:</label><div class="float_right"><input type="text" class="autotext numeric phone_short required" name="SubmitterPhoneArray" id="New_Location_SubmitterPhone1" maxlength="3" /> - <input type="text" class="autotext numeric phone_short required" name="SubmitterPhoneArray" id="New_Location_SubmitterPhone2" maxlength="3" /> - <input type="text" class="autotext numeric phone_long required" name="SubmitterPhoneArray" id="New_Location_SubmitterPhone3" maxlength="4" /></div></div>
                <div class="row"><label for="New_Agency_SubmitterFax1">Submitter Fax Number:</label><div class="float_right"><input type="text" class="autotext numeric phone_short required" name="SubmitterFaxArray" id="New_Location_SubmitterFax1" maxlength="3" /> - <input type="text" class="autotext numeric phone_short required" name="SubmitterFaxArray" id="New_Location_SubmitterFax2" maxlength="3" /> - <input type="text" class="autotext numeric phone_long required" name="SubmitterFaxArray" id="New_Location_SubmitterFax3" maxlength="4" /></div></div>
            </div>
        </div>
         <div class="wide_column"><div class="row"><label for="New_Location_BranchID" class="float_left">(M0016)Branch ID Number(For OASIS Submission):</label><div class="float_left"><%var branchId = new SelectList(new[] { new SelectListItem { Text = "-- Select Branch Id --", Value = "0" }, new SelectListItem { Text = "N", Value = "N" }, new SelectListItem { Text = "P", Value = "P" }, new SelectListItem { Text = "Other", Value = "Other" }}, "Value", "Text", "0"); %><%= Html.DropDownList("BranchId", branchId, new { @id = "New_Location_BranchId", @class = "requireddropdown" })%></div><%=Html.TextBox("BranchIdOther", "", new { @id = "New_Location_BranchIdOther", @class = string.Format("text input_wrapper hidden") })%> </div></div>
    </fieldset> 
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row"><label for="New_Location_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"> <%=Html.TextBox("AddressLine1", "", new { @id = "New_Location_AddressLine1", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="New_Location_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"> <%=Html.TextBox("AddressLine2", "", new { @id = "New_Location_AddressLine2", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Location_AddressCity" class="float_left">City:</label><div class="float_right"> <%=Html.TextBox("AddressCity", "", new { @id = "New_Location_AddressCity", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="New_Location_AddressStateCode" class="float_left"> State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Location_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Location_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div></div>
         </div>   
        <div class="column">   
            <div class="row"><label for="New_Location_PhoneArray1" class="float_left">Primary Phone:</label><div class="float_right"><input type="text" class="input_wrappermultible autotext required digits phone_short" name="PhoneArray" id="New_Location_PhoneArray1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext required digits phone_short" name="PhoneArray" id="New_Location_PhoneArray2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext required digits phone_long" name="PhoneArray" id="New_Location_PhoneArray3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Location_FaxNumberArray1" class="float_left">Fax Number:</label><div class="float_right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Location_FaxNumberArray1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Location_FaxNumberArray2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_long" name="FaxNumberArray" id="New_Location_FaxNumberArray3" maxlength="4" /></div></div> 
            <div class="row"><label for="New_Location_IsMainOffice">Is Main Office?</label><div class="float_right"><input type="checkbox" name="IsMainOffice" class="radio" id="New_Location_IsMainOffice" /></div></div>
        </div>
        <table class="form"><tbody><tr class="linesep vert"><td><label for="New_Location_Comments">Comments:</label><div><%= Html.TextArea("Comments", "", new { @id = "New_Location_Comments" })%></div></td></tr></tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newlocation');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
<script type="text/javascript">
    $("#New_Location_IsSubmitterInfoTheSame").click(function() { if ($(this).is(':checked')) { $("#New_Location_SubmiterInfoContent").hide(); $("#New_Location_SubmiterInfoContent input[type=text]").removeClass("required"); $("#New_Location_SubmiterInfoContent select").removeClass("requireddropdown"); } else { $("#New_Location_SubmiterInfoContent").show(); $("#New_Location_SubmiterInfoContent input[type=text]").addClass("required"); $("#New_Location_SubmiterInfoContent select").addClass("requireddropdown"); } });
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $("#New_Location_BranchId").change(function() { if ($(this).val() == 'Other') { $("#New_Location_BranchIdOther").removeClass("hidden") } else { $("#New_Location_BranchIdOther").removeClass("hidden").addClass("hidden"); ; } });
</script>