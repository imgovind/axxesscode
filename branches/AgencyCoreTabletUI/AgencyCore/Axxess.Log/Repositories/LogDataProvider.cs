﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.Repository;

namespace Axxess.Log.Repositories
{
   public class LogDataProvider : ILogDataProvider
    {
       #region Members and Properties

       private readonly SimpleRepository database;

        public LogDataProvider()
        {
            this.database = new SimpleRepository("LogsConnectionString", SimpleRepositoryOptions.None);
        }

        #endregion

        #region AgencyCoreDataProvider Members

        private ILogRepository logRepository;
        public ILogRepository LogRepository
        {
            get
            {
                if (logRepository == null)
                {
                    logRepository = new LogRepository(this.database);
                }
                return logRepository;
            }
        }
#endregion 
    }
}
