﻿namespace Axxess.AgencyManagement.SupportApp.Enums
{
    public enum SelectListTypes
    {
        States,
        TitleTypes,
        CredentialTypes,
        LicenseTypes,
        CahpsVendors
    }
}
