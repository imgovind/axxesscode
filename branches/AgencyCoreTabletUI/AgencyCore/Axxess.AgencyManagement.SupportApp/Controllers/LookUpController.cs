﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Telerik.Web.Mvc;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class LookUpController : BaseController
    {
        #region Constructor
        private ILookupRepository lookupRepository;

        public LookUpController(ILookUpDataProvider lookupDataProvider)
        {
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");

            lookupRepository = lookupDataProvider.LookUpRepository;
        }

        #endregion

        #region Actions

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult States()
        {
            return Json(lookupRepository.States());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult Races()
        {
            return Json(lookupRepository.Races());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult PaymentSources()
        {
            return Json(lookupRepository.PaymentSources());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult DrugClassifications()
        {
            return Json(lookupRepository.DrugClassifications());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult ReferralSources()
        {
            return Json(lookupRepository.ReferralSources());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult SupplyCategories()
        {
            return Json(lookupRepository.SupplyCategories());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult AdmissionSources()
        {
            return Json(lookupRepository.AdmissionSources());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DisciplineTasks(string Discipline)
        {
            return Json(lookupRepository.DisciplineTasks(Discipline));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult MultipleDisciplineTasks()
        {
            return Json(lookupRepository.DisciplineTasks().Where(d => d.IsMultiple).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DiagnosisCodes()
        {
            var gridModel = new GridModel();
            var pageNumber = this.HttpContext.Request.Params["page"];
            var diagnosisCodes = lookupRepository.DiagnosisCodes();
            if (diagnosisCodes != null && pageNumber.HasValue())
            {
                int page = int.Parse(pageNumber);
                gridModel.Data = diagnosisCodes.Skip((page - 1) * 13).Take(13);
                gridModel.Total = diagnosisCodes.Count;
            }
            return Json(gridModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DiagnosisCode(string q, int limit, string type)
        {
            var diagnosisCodes = new List<DiagnosisCode>();
            if (type.ToUpper() == "ICD")
            {
                diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.Code.StartsWith(q.ToUpper().Trim())).ToList();
            }
            else if (type.ToUpper() == "DIAGNOSIS")
            {
                diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.ShortDescription.ToLower().Contains(q.ToLower())).ToList();
            }
            return Json(diagnosisCodes);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DiagnosisCodeNoVE(string q, int limit, string type)
        {
            var diagnosisCodes = new List<DiagnosisCode>();
            if (type.ToUpper() == "ICD")
            {
                diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.Code.StartsWith(q.ToUpper().Trim())).Where(s => !s.Code.StartsWith("V") && !s.Code.StartsWith("E")).ToList();
            }
            else if (type.ToUpper() == "DIAGNOSIS")
            {
                diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.ShortDescription.ToLower().Contains(q.ToLower())).Where(s => !s.Code.StartsWith("V") && !s.Code.StartsWith("E")).ToList();
            }
            return Json(diagnosisCodes);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Supplies(string q, int limit, string type)
        {
            if (type == "Code")
            {
                var supplies = lookupRepository.Supplies().Where(p => p.Code.Contains
                    (q.ToUpper())).Select(p => new { p.Description, p.Code }).Take(limit).ToList();
                return Json(supplies);
            }
            else if (type == "Description")
            {
                var supplies = lookupRepository.Supplies().Where(p => p.Description.ToLower().Contains(q.ToLower())).Select(p => new { p.Description, p.Code }).Take(limit).ToList();
                return Json(supplies);
            }

            return Json(null);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SuppliesGrid(string q, int limit, string type)
        {
            if (q != string.Empty)
            {
                if (type == "Code")
                {
                    var supplies = lookupRepository.Supplies().Where(p => p.Code.StartsWith
                        (q.ToUpper())).Take(limit).ToList();
                    return View(new GridModel(supplies));
                }
                else if (type == "Description")
                {
                    var supplies = lookupRepository.Supplies().Where(p => p.Description.ToLower().Contains(q.ToLower())).Take(limit).ToList();
                    return View(new GridModel(supplies));
                }
            }
            var list = new List<Supply>();
            return View(new GridModel(list));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult ProcedureCodes()
        {
            return Json(lookupRepository.ProcedureCodes());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ProcedureCode(string q, int limit, string type)
        {
            var procedureCodes = new List<ProcedureCode>();
            if (type.ToUpper() == "PROCEDUREICD")
            {
                procedureCodes = lookupRepository.ProcedureCodes().Where(p => p.Code.StartsWith(q)).ToList();
            }
            else if (type.ToUpper() == "PROCEDURE")
            {
                procedureCodes = lookupRepository.ProcedureCodes().Where(p => p.ShortDescription.ToLower().Contains(q.ToLower())).ToList();
            }
            return Json(procedureCodes);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Npi(string npiNumber)
        {
            return Json(lookupRepository.GetNpiData(npiNumber));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Npis(string q, int limit)
        {
            return Json(lookupRepository.GetNpis(q, limit));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NewShortGuid()
        {
            return Json(new { text = ShortGuid.NewId().ToString() });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationRoute(string q, int limit)
        {
            return Json(lookupRepository.MedicationRoute(q, limit));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationClassfication(string q, int limit)
        {
            return Json(lookupRepository.MedicationClassfication(q, limit));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationDosage(string q, int limit)
        {
            return Json(lookupRepository.MedicationDosage(q, limit));
        }

        #endregion

    }
}
