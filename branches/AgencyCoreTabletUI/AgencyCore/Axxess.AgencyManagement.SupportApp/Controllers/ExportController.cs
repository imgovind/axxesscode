﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System.Linq;
    using System.Web.Mvc;

    using Exports;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Repositories;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ExportController : BaseController
    {
        #region Private Members/Constructor

        private readonly IAgencyRepository agencyRepository;

        public ExportController(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region ExportController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Agencies()
        {
            var agencies = agencyRepository.AllAgencies();
            var export = new AgencyExporter(agencies.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Agencies.xls");
        }

        #endregion
    }
}
