﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class SystemController : BaseController
    {
         #region Private Members/Constructor

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IMessageRepository messageRepository;

        public SystemController(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.messageRepository = agencyManagementDataProvider.MessageRepository;
        }

        #endregion

        #region SystemController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewMessage()
        {
            return PartialView("Message/New");
        }

        [ValidateInput(false)] 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddMessage([Bind] SystemMessage message)
        {
            Check.Argument.IsNotNull(message, "message");

            var viewData = new JsonViewData();

            if (message.IsValid)
            {
                message.CreatedBy = Current.DisplayName;
                if (!messageRepository.AddSystemMessage(message))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the system message.";
                }
                else
                {
                    var users = userRepository.GetAll();
                    users.ForEach(user =>
                    {
                        var login = loginRepository.Find(user.LoginId);
                        if (login != null)
                        {
                            if (user.Messages.IsNullOrEmpty())
                            {
                                user.SystemMessages = new List<MessageState>();
                            }
                            else
                            {
                                user.SystemMessages = user.Messages.ToObject<List<MessageState>>();
                            }
                            user.SystemMessages.Add(
                                new MessageState { 
                                    Id = message.Id, 
                                    IsRead = false 
                                });

                            user.Messages = user.SystemMessages.ToXml();
                            if (userRepository.Refresh(user))
                            {
                                var parameters = new string[4];
                                parameters[0] = "recipientfirstname";
                                parameters[1] = user.FirstName;
                                parameters[2] = "senderfullname";
                                parameters[3] = "Axxess Technology Solutions";
                                var bodyText = MessageBuilder.PrepareTextFrom("NewMessageNotification", parameters);
                                Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, "Axxess Technology Solutions sent you a message.", bodyText);
                            }
                        }
                    });

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "System message was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = message.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewDashboardMessage()
        {
            return PartialView("Dashboard/New");
        }

        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddDashboardMessage([Bind] DashboardMessage message)
        {
            Check.Argument.IsNotNull(message, "message");

            var viewData = new JsonViewData();

            if (message.IsValid)
            {
                message.CreatedBy = Current.DisplayName;
                if (!messageRepository.AddDashboardMessage(message))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Dashboard message.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Dashboard message was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = message.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DashboardPreview()
        {
            return PartialView("Dashboard/Preview");
        }

        #endregion
    }
}
