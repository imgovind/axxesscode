﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;

    using Axxess.AgencyManagement.Domain;
    
    public interface IAgencyService
    {
        Agency GetAgency(Guid Id);
        bool CreateAgency(Agency agency);
        bool UpdateAgency(Agency agency);
        bool CreateLocation(AgencyLocation location);
    }
}
