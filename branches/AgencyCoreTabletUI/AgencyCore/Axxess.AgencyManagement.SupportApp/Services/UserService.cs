﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    public class UserService : IUserService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;

        public UserService(IAgencyManagementDataProvider agencyManagmentDataProvider, IMembershipDataProvider membershipDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagmentDataProvider.UserRepository;
        }
        
        #endregion

        #region IUserService Members

        public bool CreateUser(User user)
        {
            try
            {
                var isNewLogin = false;

                var login = loginRepository.Find(user.EmailAddress);
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = user.FirstName;
                    login.EmailAddress = user.EmailAddress;
                    login.Role = Roles.ApplicationUser.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login))
                    {
                        isNewLogin = true;
                    }
                }

                user.LoginId = login.Id;
                user.Profile = new UserProfile();
                user.Profile.EmailWork = user.EmailAddress;

                if (userRepository.Add(user))
                {
                    string bodyText = string.Empty;
                    string subject = string.Format("{0} - Invitation to use Axxess Home Health Software", user.AgencyName);
                    
                    if (isNewLogin)
                    {
                        var parameters = string.Format("id={0}&agencyid={1}", user.Id, user.AgencyId);
                        var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                        bodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName,
                            "agencyname", user.AgencyName, "encryptedQueryString", encryptedParameters);
                    }
                    else
                    {
                        bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", user.FirstName,
                            "agencyname", user.AgencyName);
                    }

                    Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, subject, bodyText);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }

            return false;
        }

        #endregion
    }
}
