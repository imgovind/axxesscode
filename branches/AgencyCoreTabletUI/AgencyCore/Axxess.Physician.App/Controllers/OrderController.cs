﻿namespace Axxess.Physician.App.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Linq;

    using Enums;
    using Services;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;
    using System.Collections.Generic;
    using Axxess.AgencyManagement;
    using Axxess.OasisC.Domain;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OrderController : BaseController
    {
        #region Constructor

        private readonly IPhysicianService physicianService;
        private readonly IAssessmentService assessmentService;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;

        public OrderController(IAgencyManagementDataProvider agencyManagementDataProvider, IPhysicianService physicianService, IAssessmentService assessmentService)
        {
            Check.Argument.IsNotNull(physicianService, "physicianService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.physicianService = physicianService;
            this.assessmentService = assessmentService;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region Order Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ListHeader()
        {
            ViewData["GroupName"] = "EventDate";
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListView(string groupName)
        {
            ViewData["GroupName"] = groupName;
            return PartialView();
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListGrid()
        {
            return View(new GridModel(physicianService.GetOrders()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianOrderPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var order = patientRepository.GetOrder(orderId, patientId, agencyId);
            if (order != null)
            {
                order.Agency = AgencyEngine.Get(order.AgencyId);
                order.Patient = patientRepository.Get(order.PatientId, order.AgencyId);
                order.Physician = physicianRepository.Get(order.PhysicianId, order.AgencyId);

                var episode = patientRepository.GetEpisodeById(order.AgencyId, order.EpisodeId, order.PatientId);
                if (episode != null)
                {
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                    var allergies = assessmentService.GetAllergies(episode.AssessmentId, episode.AssessmentType, order.AgencyId);
                    if (allergies != null && allergies.Count > 0 && allergies.ContainsKey("485Allergies") && allergies["485Allergies"].Answer.IsNotNullOrEmpty() && allergies["485Allergies"].Answer == "Yes" && allergies.ContainsKey("485AllergiesDescription") && allergies["485AllergiesDescription"].Answer.IsNotNullOrEmpty()) order.Allergies = allergies["485AllergiesDescription"].Answer;
                    else order.Allergies = "NKA (Food/Drugs/Latex)";
                    var diagnosis = assessmentService.GetDiagnoses(episode.AssessmentId, episode.AssessmentType, order.AgencyId);
                    if (diagnosis != null && diagnosis.Count > 0)
                    {
                        if (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            order.PrimaryDiagnosisText = diagnosis["M1020PrimaryDiagnosis"].Answer;
                        }
                        if (diagnosis.ContainsKey("M1020ICD9M") && diagnosis["M1020ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            order.PrimaryDiagnosisCode = diagnosis["M1020ICD9M"].Answer;
                        }
                        if (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            order.SecondaryDiagnosisText = diagnosis["M1022PrimaryDiagnosis1"].Answer;
                        }
                        if (diagnosis.ContainsKey("M1022ICD9M1") && diagnosis["M1022ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            order.SecondaryDiagnosisCode = diagnosis["M1022ICD9M1"].Answer;
                        }
                    }
                }
                return View("Print/Physician", order);
            }
            return View("Print/Physician", new PhysicianOrder());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PlanofCareOrderPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var planofCare = assessmentService.GetPlanofCare(agencyId, episodeId, patientId, orderId);
            if (planofCare != null && planofCare.Data.IsNotNullOrEmpty())
            {
                var agency = agencyRepository.Get(planofCare.AgencyId);
                var patient = patientRepository.GetPatientOnly(planofCare.PatientId, planofCare.AgencyId);
                planofCare.AgencyData = agency != null ? agency.ToXml() : string.Empty;
                planofCare.PatientData = patient != null ? patient.ToXml() : string.Empty;
                planofCare.Questions = planofCare.Data.ToObject<List<Question>>();

                var episode = patientRepository.GetEpisode(planofCare.AgencyId, planofCare.EpisodeId, planofCare.PatientId);
                if (episode != null)
                {
                    if (planofCare.AssessmentType.IsNotNullOrEmpty() && planofCare.AssessmentType.ToLower().Contains("recert"))
                    {
                        planofCare.EpisodeEnd = episode != null && episode.EndDate != DateTime.MinValue ? episode.EndDate.AddDays(60).ToShortDateString().ToZeroFilled() : string.Empty;
                        planofCare.EpisodeStart = episode != null && episode.EndDate != DateTime.MinValue ? episode.EndDate.AddDays(1).ToShortDateString().ToZeroFilled() : string.Empty;
                    }
                    else
                    {
                        planofCare.EpisodeEnd = episode.EndDateFormatted;
                        planofCare.EpisodeStart = episode.StartDateFormatted;
                    }
                }

                if (!planofCare.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(planofCare.PhysicianId, planofCare.AgencyId);
                    planofCare.PhysicianData = physician != null ? physician.ToXml() : string.Empty;
                }
                else
                {
                    if (planofCare.PhysicianData.IsNotNullOrEmpty())
                    {
                        var oldPhysician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                        {
                            var physician = physicianRepository.Get(oldPhysician.Id, planofCare.AgencyId);
                            if (physician != null)
                            {
                                planofCare.PhysicianData = physician.ToXml();
                            }
                        }
                    }
                }
                return View("Print/485", planofCare);
            }
            return View("Print/485", new PlanofCare());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FaceToFaceEncounter(Guid agencyId, Guid episodeId, Guid patientId, Guid id)
        {
            var order = patientRepository.GetFaceToFaceEncounter(id, patientId, agencyId);
            if (order != null)
            {
                order.Agency = agencyRepository.Get(agencyId);
                order.Patient = patientRepository.Get(order.PatientId, agencyId);
                order.Physician = physicianRepository.Get(order.PhysicianId, agencyId);
                var episode = patientRepository.GetEpisodeById(agencyId, order.EpisodeId, order.PatientId);
                order.EpisodeEndDate = episode.EndDateFormatted;
                order.EpisodeStartDate = episode.StartDateFormatted;
            }
            else
            {
                order = new FaceToFaceEncounter();
                order.Agency = agencyRepository.Get(agencyId);
                order.Patient = patientRepository.Get(patientId, agencyId);
            }
            return View("FaceToFace/Edit", order);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateFaceToFace([Bind] FaceToFaceEncounter faceToFace)
        {
            Check.Argument.IsNotNull(faceToFace, "faceToFace");

            var viewData = new JsonViewData { errorMessage = "Face-To-Face Encounter could not be updated. Please try again.", isSuccessful = false };

            if (faceToFace.IsValid)
            {
                if (physicianService.UpdateFaceToFaceEncounter(faceToFace))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Face-To-Face Encounter has been updated successfully.";
                }
            }
            else
            {
                viewData.errorMessage = faceToFace.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FaceToFaceList()
        {
            return PartialView("FaceToFace/List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FaceToFaceGrid()
        {
            return View(new GridModel(physicianService.GetFaceToFaceEncounters()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FaceToFaceEncounterPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId)
        {
            var order = patientRepository.GetFaceToFaceEncounter(orderId, patientId, agencyId);
            if (order != null)
            {
                order.Agency = agencyRepository.Get(agencyId);
                order.Patient = patientRepository.Get(order.PatientId, agencyId);
                order.Physician = physicianRepository.Get(order.PhysicianId, agencyId);
                var episode = patientRepository.GetEpisodeById(agencyId, order.EpisodeId, order.PatientId);
                order.EpisodeEndDate = episode.EndDateFormatted;
                order.EpisodeStartDate = episode.StartDateFormatted;
            }
            else
            {
                order = new FaceToFaceEncounter();
                order.Agency = agencyRepository.Get(agencyId);
                order.Patient = patientRepository.Get(patientId, agencyId);
            }

            return View("Print/FaceToFace", order);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update(string action, Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, int orderType, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "This order could not been updated." };
            if (orderType == (int)OrderType.PhysicianOrder)
            {
                if (physicianService.UpdatePhysicianOrder(agencyId, episodeId, patientId, orderId, action, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The order has been updated successfully.";
                }
            }
            else if (orderType == (int)OrderType.HCFA485 || orderType == (int)OrderType.HCFA485StandAlone)
            {
                if (assessmentService.UpdatePlanofCare(agencyId, episodeId, patientId, orderId, action, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The plan of care has been updated successfully.";
                }
            }
            else if (orderType == (int)OrderType.FaceToFaceEncounter)
            {
                if (physicianService.UpdateFaceToFaceEncounter(agencyId, episodeId, patientId, orderId, action, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Face-to-Face encounter has been updated successfully.";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CompleteList()
        {
            return PartialView("List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CompleteGrid()
        {
            return View(new GridModel(physicianService.GetOrdersCompleted()));
        }

        #endregion
    }
}
