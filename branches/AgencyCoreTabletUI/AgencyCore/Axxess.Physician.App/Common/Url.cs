﻿namespace Axxess.Physician.App
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    using Axxess.AgencyManagement.Extensions;

    public static class Url
    {
        public static string Print(Guid orderId, Guid patientId, Guid episodeId, Guid agencyId, OrderType type, bool useIcon)
        {
            string printUrl = string.Empty;
            string linkText = type.GetDescription();
            if (useIcon) linkText = "<span class=\"img icon print\"></span>";

            switch (type)
            {
                case OrderType.PhysicianOrder:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Order/Physician/{0}/{1}/{2}/{3}',function(){{ Order.Update('Approve','{0}','{1}','{2}','{3}','{4}'); }},function(){{ Order.Update('Return','{0}','{1}','{2}','{3}','{4}'); }});\">{5}</a>", agencyId, episodeId, patientId, orderId, (int)type, linkText);
                    break;
                case OrderType.FaceToFaceEncounter:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Order/FaceToFace/{0}/{1}/{2}/{3}',function(){{ Order.Update('Approve','{0}','{1}','{2}','{3}','{4}'); }},function(){{ Order.Update('Return','{0}','{1}','{2}','{3}','{4}'); }});\">{5}</a>", agencyId, episodeId, patientId, orderId, (int)type, linkText);
                    break;
                case OrderType.HCFA485:
                case OrderType.HCFA485StandAlone:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Order/PlanofCare/{0}/{1}/{2}/{3}',function(){{ Order.Update('Approve','{0}','{1}','{2}','{3}','{4}'); }},function(){{ Order.Update('Return','{0}','{1}','{2}','{3}','{4}'); }});\">{5}</a>", agencyId, episodeId, patientId, orderId, (int)type, linkText);
                    break;

            }
            return printUrl;
        }

        public static string View(Guid orderId, Guid patientId, Guid episodeId, Guid agencyId, OrderType type, bool useIcon)
        {
            string printUrl = string.Empty;
            string linkText = type.GetDescription();
            if (useIcon) linkText = "<span class=\"img icon print\"></span>";

            switch (type)
            {
                case OrderType.PhysicianOrder:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Order/Physician/{0}/{1}/{2}/{3}');\">{4}</a>", agencyId, episodeId, patientId, orderId, linkText);
                    break;
                case OrderType.FaceToFaceEncounter:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Order/FaceToFace/{0}/{1}/{2}/{3}');\">{4}</a>", agencyId, episodeId, patientId, orderId, linkText);
                    break;
                case OrderType.HCFA485:
                case OrderType.HCFA485StandAlone:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Order/PlanofCare/{0}/{1}/{2}/{3}');\">{4}</a>", agencyId, episodeId, patientId, orderId, linkText);
                    break;

            }
            return printUrl;
        }
    }
}
