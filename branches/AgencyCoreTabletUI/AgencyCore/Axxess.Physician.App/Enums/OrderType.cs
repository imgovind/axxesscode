﻿namespace Axxess.Physician.App.Enums
{
    using Axxess.Core.Infrastructure;
    using System.ComponentModel;

    public enum OrderType : byte
    {
        [Description("Physician Order")]
        PhysicianOrder = 1,
        [Description("Plan of Treatment/Care")]
        HCFA485 = 2,
        [Description("486 Plan Of Care")]
        HCFA486 = 3,
        [Description("Plan of Treatment/Care")]
        HCFA485StandAlone = 4,
        [Description("Physician Face-to-face Encounter")]
        FaceToFaceEncounter = 5
    }
}
