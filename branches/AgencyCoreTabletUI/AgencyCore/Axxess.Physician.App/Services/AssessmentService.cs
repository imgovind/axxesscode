﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    public class AssessmentService : IAssessmentService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IOasisCDataProvider oasisDataProvider;

        public AssessmentService(IOasisCDataProvider oasisDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(oasisDataProvider, "dataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.oasisDataProvider = oasisDataProvider;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
        }

        #endregion

        #region IAssessmentService Members

        private Assessment GetAssessment(Guid assessmentId, string assessmentType, Guid agencyId)
        {
            return oasisDataProvider.OasisAssessmentRepository.Get(assessmentId, assessmentType, agencyId);
        }

        public PlanofCare GetPlanofCare(Guid agencyId, Guid episodeId, Guid patientId, Guid planofcareId)
        {
            var planofCare = oasisDataProvider.PlanofCareRepository.Get(agencyId, planofcareId);

            if (planofCare == null)
            {
                var standAlone = oasisDataProvider.PlanofCareRepository.GetStandAlone(agencyId, episodeId, patientId, planofcareId);
                if (standAlone != null)
                {
                    planofCare = standAlone.ToPlanofCare();
                }
            }

            return planofCare;
        }

        public IDictionary<string, Question> GetAllergies(Guid assessmentId, string AssessmentType, Guid agencyId)
        {
            var allergies = new Dictionary<string, Question>();
            var assessment = this.GetAssessment(assessmentId, AssessmentType, agencyId);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();
                if (questions.ContainsKey("485Allergies") && questions["485Allergies"] != null)
                {
                    allergies.Add("485Allergies", questions["485Allergies"]);
                }
                if (questions.ContainsKey("485AllergiesDescription") && questions["485AllergiesDescription"] != null)
                {
                    allergies.Add("485AllergiesDescription", questions["485AllergiesDescription"]);
                }
            }
            return allergies;
        }

        public IDictionary<string, Question> GetDiagnoses(Guid assessmentId, string AssessmentType, Guid agencyId)
        {
            var diagnosis = new Dictionary<string, Question>();
            var assessment = this.GetAssessment(assessmentId, AssessmentType, agencyId);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();

                if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                {
                    diagnosis.Add("M1020PrimaryDiagnosis", questions["M1020PrimaryDiagnosis"]);
                }
                if (questions.ContainsKey("M1020ICD9M") && questions["M1020ICD9M"] != null)
                {
                    diagnosis.Add("M1020ICD9M", questions["M1020ICD9M"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis1", questions["M1022PrimaryDiagnosis1"]);
                }

                if (questions.ContainsKey("M1022ICD9M1") && questions["M1022ICD9M1"] != null)
                {
                    diagnosis.Add("M1022ICD9M1", questions["M1022ICD9M1"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis2") && questions["M1022PrimaryDiagnosis2"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis2", questions["M1022PrimaryDiagnosis2"]);
                }

                if (questions.ContainsKey("M1022ICD9M2") && questions["M1022ICD9M2"] != null)
                {
                    diagnosis.Add("M1022ICD9M2", questions["M1022ICD9M2"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis3") && questions["M1022PrimaryDiagnosis3"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis3", questions["M1022PrimaryDiagnosis3"]);
                }

                if (questions.ContainsKey("M1022ICD9M3") && questions["M1022ICD9M3"] != null)
                {
                    diagnosis.Add("M1022ICD9M3", questions["M1022ICD9M3"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis4") && questions["M1022PrimaryDiagnosis4"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis4", questions["M1022PrimaryDiagnosis4"]);
                }

                if (questions.ContainsKey("M1022ICD9M4") && questions["M1022ICD9M4"] != null)
                {
                    diagnosis.Add("M1022ICD9M4", questions["M1022ICD9M4"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis5") && questions["M1022PrimaryDiagnosis5"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis5", questions["M1022PrimaryDiagnosis5"]);
                }

                if (questions.ContainsKey("M1022ICD9M5") && questions["M1022ICD9M5"] != null)
                {
                    diagnosis.Add("M1022ICD9M5", questions["M1022ICD9M5"]);
                }

            }
            return diagnosis;
        }

        public bool UpdatePlanofCare(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;

            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            PlanofCare planofCare = null;

            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(agencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(agencyId, scheduleEvent.UserId, patientId, eventId);
                }

                var planofCareStandAlone = oasisDataProvider.PlanofCareRepository.GetStandAlone(agencyId, episodeId, patientId, eventId);
                if (planofCareStandAlone != null)
                {
                    if (actionType == "Approve")
                    {
                        planofCareStandAlone.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                        planofCareStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", Current.DisplayName);
                        planofCareStandAlone.ReceivedDate = DateTime.Now;
                        if (oasisDataProvider.PlanofCareRepository.UpdateStandAlone(planofCareStandAlone))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (actionType == "Return")
                    {
                        planofCareStandAlone.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                        if (oasisDataProvider.PlanofCareRepository.UpdateStandAlone(planofCareStandAlone))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                }
                else
                {
                    planofCare = oasisDataProvider.PlanofCareRepository.Get(agencyId, episodeId, patientId, eventId);
                    if (planofCare != null)
                    {
                        if (actionType == "Approve")
                        {
                            planofCare.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                            planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", Current.DisplayName);
                            planofCare.ReceivedDate = DateTime.Now;
                            if (oasisDataProvider.PlanofCareRepository.Update(planofCare))
                            {
                                if (scheduleEvent != null)
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                    scheduleEvent.ReturnReason = string.Empty;
                                    if (userEvent != null)
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                        userEvent.ReturnReason = string.Empty;
                                    }
                                    shouldUpdateEpisode = true;
                                }
                            }
                        }
                        else if (actionType == "Return")
                        {
                            planofCare.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                            if (oasisDataProvider.PlanofCareRepository.Update(planofCare))
                            {
                                if (scheduleEvent != null)
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    scheduleEvent.ReturnReason = reason;
                                    if (userEvent != null)
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                        userEvent.ReturnReason = reason;
                                    }
                                    shouldUpdateEpisode = true;
                                }
                            }
                        }
                    }
                }

                if (shouldUpdateEpisode)
                {
                    if (patientRepository.UpdateEpisode(agencyId, scheduleEvent))
                    {
                        if (userEvent != null)
                        {
                            if (userRepository.UpdateEvent(agencyId, userEvent))
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            userRepository.AddUserEvent(agencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                            result = true;
                        }
                    }
                }
            }

            return result;
        }

        #endregion
    }
}
