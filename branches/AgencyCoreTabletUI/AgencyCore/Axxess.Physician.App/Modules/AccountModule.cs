﻿namespace Axxess.Physician.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class AccountModule : Module
    {
        public override string Name
        {
            get { return "Account"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               "Activate",
               "Activate",
               new { controller = this.Name, action = "Activate", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "Login",
              "Login",
              new { controller = this.Name, action = "LogOn", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "Forgot",
               "Forgot",
               new { controller = this.Name, action = "ForgotPassword", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Logout",
                "Logout",
                new { controller = this.Name, action = "LogOff", id = UrlParameter.Optional }
            );
        }
    }
}
