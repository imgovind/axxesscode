﻿namespace Axxess.Physician.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class OrderModule : Module
    {
        public override string Name
        {
            get { return "Order"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "OrderList",
                "Order/List",
                new { controller = this.Name, action = "ListHeader", id = UrlParameter.Optional });

            routes.MapRoute(
                "PhysicianOrderPrint",
                "Order/Physician/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "PhysicianOrderPrint", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "FaceToFaceOrderPrint",
                "Order/FaceToFace/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "FaceToFaceEncounterPrint", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "PlanofCareOrderPrint",
                "Order/PlanofCare/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "PlanofCareOrderPrint", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });
        }
    }
}
