﻿namespace Axxess.Physician.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class DefaultModule : Module
    {
        public override string Name
        {
            get { return "Home"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.RouteExistingFiles = true;
            routes.IgnoreRoute("{file}.txt");
            routes.IgnoreRoute("{file}.htm");
            routes.IgnoreRoute("{file}.html");
            routes.IgnoreRoute("{file}.jpeg");
            routes.IgnoreRoute("{handler}.axd");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("Scripts/{*pathInfo}");
            routes.IgnoreRoute("Images/{*pathInfo}");
            routes.IgnoreRoute("css/{*pathInfo}");
            routes.IgnoreRoute("Content/{*pathInfo}");

            routes.MapRoute(
                this.Name, // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = this.Name, action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }
    }
}
