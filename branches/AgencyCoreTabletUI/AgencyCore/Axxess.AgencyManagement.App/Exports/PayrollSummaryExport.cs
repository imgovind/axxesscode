﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

   public class PayrollSummaryExport : BaseExporter
    {
        private IList<VisitSummary> visitSummaries;
        private DateTime StartDate;
        private DateTime EndDate;
        public PayrollSummaryExport(IList<VisitSummary> visitSummaries, DateTime StartDate, DateTime EndDate)
            : base()
        {
            this.visitSummaries = visitSummaries;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Payroll Visit Summary";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("PayrollVisitSummary");
            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Payroll Visit Summary");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("User");
            headerRow.CreateCell(1).SetCellValue("Count");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.visitSummaries.Count > 0)
            {
                int i = 2;
                this.visitSummaries.ForEach(order =>
                {
                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(order.UserName);
                    row.CreateCell(1).SetCellValue(order.VisitCount);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Payroll Visit Summary: {0}", visitSummaries.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
        }

    }
}
