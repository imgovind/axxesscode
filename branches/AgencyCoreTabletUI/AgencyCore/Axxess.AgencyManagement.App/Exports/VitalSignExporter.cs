﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Domain;
using NPOI.SS.UserModel;
using NPOI.HPSF;

namespace Axxess.AgencyManagement.App.Exports
{
    public class VitalSignExporter : BaseExporter
    {
        private IList<VitalSign> vitalSigns;
        private DateTime StartDate;
        private DateTime EndDate;
        public VitalSignExporter(IList<VitalSign> vitalSigns, DateTime StartDate, DateTime EndDate)
            : base()
        {
            this.vitalSigns = vitalSigns;
            this.StartDate=StartDate;
            this.EndDate=EndDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Vital Signs";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("VitalSigns");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Vital Signs");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}",string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"),EndDate.ToString("MM/dd/yyyy"))));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Visit Date");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Employee Name");
            headerRow.CreateCell(3).SetCellValue("BP Lying");
            headerRow.CreateCell(4).SetCellValue("BP Sitting");
            headerRow.CreateCell(5).SetCellValue("BP Standing");
            headerRow.CreateCell(6).SetCellValue("Temp");
            headerRow.CreateCell(7).SetCellValue("Resp");
            headerRow.CreateCell(8).SetCellValue("Apical Pulse");
            headerRow.CreateCell(9).SetCellValue("Radial Pulse");
            headerRow.CreateCell(10).SetCellValue("BS");
            headerRow.CreateCell(11).SetCellValue("Weight");
            headerRow.CreateCell(12).SetCellValue("Pain Level");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.vitalSigns.Count > 0)
            {
                int i = 2;
                this.vitalSigns.ForEach(vitalSign =>
                {
                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(vitalSign.VisitDate);
                    row.CreateCell(1).SetCellValue(vitalSign.DisciplineTask);
                    row.CreateCell(2).SetCellValue(vitalSign.UserDisplayName);
                    row.CreateCell(3).SetCellValue(vitalSign.BPLying);
                    row.CreateCell(4).SetCellValue(vitalSign.BPSitting);
                    row.CreateCell(5).SetCellValue(vitalSign.BPStanding);
                    row.CreateCell(6).SetCellValue(vitalSign.Temp);
                    row.CreateCell(7).SetCellValue(vitalSign.Resp);
                    row.CreateCell(8).SetCellValue(vitalSign.ApicalPulse);
                    row.CreateCell(9).SetCellValue(vitalSign.RadialPulse);
                    row.CreateCell(10).SetCellValue(vitalSign.BS);
                    row.CreateCell(11).SetCellValue(vitalSign.Weight);
                    row.CreateCell(12).SetCellValue(vitalSign.PainLevel);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Vital Signs: {0}", vitalSigns.Count));
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
            sheet.AutoSizeColumn(12);
        }
    }
}
