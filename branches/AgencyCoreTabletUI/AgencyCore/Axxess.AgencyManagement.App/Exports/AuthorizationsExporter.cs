﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class AuthorizationsExporter : BaseExporter
    {
        private IList<Authorization> authorizations;
        public AuthorizationsExporter(IList<Authorization> authorizations) : base() {
            this.authorizations = authorizations;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Authorizations";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("Authorizations");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Authorizations");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Number");
            headerRow.CreateCell(1).SetCellValue("Start Date");
            headerRow.CreateCell(2).SetCellValue("End Date");
            headerRow.CreateCell(3).SetCellValue("Insurance");
            headerRow.CreateCell(4).SetCellValue("SN Visit");
            headerRow.CreateCell(5).SetCellValue("SN Visit Count Type");
            headerRow.CreateCell(6).SetCellValue("PT Visit");
            headerRow.CreateCell(7).SetCellValue("PT Visit Count Type");
            headerRow.CreateCell(8).SetCellValue("OT Visit");
            headerRow.CreateCell(9).SetCellValue("OT Visit Count Type");
            headerRow.CreateCell(10).SetCellValue("ST Visit");
            headerRow.CreateCell(11).SetCellValue("ST Visit Count Type");
            headerRow.CreateCell(12).SetCellValue("MSW Visit");
            headerRow.CreateCell(13).SetCellValue("MSW Visit Count Type");
            headerRow.CreateCell(14).SetCellValue("HHA Visit");
            headerRow.CreateCell(15).SetCellValue("HHA Visit Count Type");
            headerRow.CreateCell(16).SetCellValue("Dietician Visit");
            headerRow.CreateCell(17).SetCellValue("Dietician Visit Count Type");
            headerRow.CreateCell(18).SetCellValue("RN Visit");
            headerRow.CreateCell(19).SetCellValue("RN Visit Count Type");
            headerRow.CreateCell(20).SetCellValue("LVN Visit");
            headerRow.CreateCell(21).SetCellValue("LVN Visit Count Type");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.authorizations.Count > 0) {
                int i = 2;
                this.authorizations.ForEach(a =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.Number);
                    dataRow.CreateCell(1).SetCellValue(a.StartDate.ToShortDateString());
                    dataRow.CreateCell(2).SetCellValue(a.EndDate.ToShortDateString());
                    dataRow.CreateCell(3).SetCellValue(a.Insurance);
                    dataRow.CreateCell(4).SetCellValue(a.SNVisit);
                    dataRow.CreateCell(5).SetCellValue(a.SNVisitCountType == "1" ? "Visits" : (a.SNVisitCountType == "2" ? "Hours" : string.Empty));
                    dataRow.CreateCell(6).SetCellValue(a.PTVisit);
                    dataRow.CreateCell(7).SetCellValue(a.PTVisitCountType == "1" ? "Visits" : (a.PTVisitCountType == "2" ? "Hours" : string.Empty));
                    dataRow.CreateCell(8).SetCellValue(a.OTVisit);
                    dataRow.CreateCell(9).SetCellValue(a.OTVisitCountType == "1" ? "Visits" : (a.OTVisitCountType == "2" ? "Hours" : string.Empty));
                    dataRow.CreateCell(10).SetCellValue(a.STVisit);
                    dataRow.CreateCell(11).SetCellValue(a.STVisitCountType == "1" ? "Visits" : (a.STVisitCountType == "2" ? "Hours" : string.Empty));
                    dataRow.CreateCell(12).SetCellValue(a.MSWVisit);
                    dataRow.CreateCell(13).SetCellValue(a.MSWVisitCountType == "1" ? "Visits" : (a.MSWVisitCountType == "2" ? "Hours" : string.Empty));
                    dataRow.CreateCell(14).SetCellValue(a.HHAVisit);
                    dataRow.CreateCell(15).SetCellValue(a.HHAVisitCountType == "1" ? "Visits" : (a.HHAVisitCountType == "2" ? "Hours" : string.Empty));
                    dataRow.CreateCell(16).SetCellValue(a.DieticianVisit);
                    dataRow.CreateCell(17).SetCellValue(a.DieticianVisitCountType == "1" ? "Visits" : (a.DieticianVisitCountType == "2" ? "Hours" : string.Empty));
                    dataRow.CreateCell(18).SetCellValue(a.RNVisit);
                    dataRow.CreateCell(19).SetCellValue(a.RNVisitCountType == "1" ? "Visits" : (a.RNVisitCountType == "2" ? "Hours" : string.Empty));
                    dataRow.CreateCell(20).SetCellValue(a.LVNVisit);
                    dataRow.CreateCell(21).SetCellValue(a.LVNVisitCountType == "1" ? "Visits" : (a.LVNVisitCountType == "2" ? "Hours" : string.Empty));
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Authorizations: {0}", authorizations.Count));
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
            sheet.AutoSizeColumn(12);
            sheet.AutoSizeColumn(13);
            sheet.AutoSizeColumn(14);
            sheet.AutoSizeColumn(15);
            sheet.AutoSizeColumn(16);
            sheet.AutoSizeColumn(17);
            sheet.AutoSizeColumn(18);
            sheet.AutoSizeColumn(19);
            sheet.AutoSizeColumn(20);
            sheet.AutoSizeColumn(21);
        }
    }
}
