﻿namespace Axxess.AgencyManagement.App.Exports
{
     using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.Enums;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using Axxess.AgencyManagement.Domain;
   public class SubmittedBatchClaimsExport :BaseExporter
    {
        private IList<ClaimDataLean> batchClaims;
         private DateTime StartDate;
         private DateTime EndDate;
         private string ClaimType;
         public SubmittedBatchClaimsExport(IList<ClaimDataLean> batchClaims, DateTime StartDate, DateTime EndDate, string ClaimType)
            : base()
        {
            this.batchClaims = batchClaims;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.ClaimType = ClaimType;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Submitted Batch Claims";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("SubmittedBatchClaims");
            
            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Submitted Batch Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Claim Type: {0}", ClaimType.ToUpperCase() == "ALL" ? "ALL" : Enum.IsDefined(typeof(ClaimType), ClaimType) ? ((ClaimType)Enum.Parse(typeof(ClaimType), ClaimType)).GetDescription() : string.Empty));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Batch Id");
            headerRow.CreateCell(1).SetCellValue("Submission Date");
            headerRow.CreateCell(2).SetCellValue("# of claims");
            headerRow.CreateCell(3).SetCellValue("# of RAPs");
            headerRow.CreateCell(4).SetCellValue("# of Finals");
           
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.batchClaims.Count > 0)
            {
                int i = 2;
                this.batchClaims.ForEach(claim =>
                {
                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(claim.Id);
                    row.CreateCell(1).SetCellValue(claim.Created.ToString("MM/dd/yyyy"));
                    row.CreateCell(2).SetCellValue(claim.Count);
                    row.CreateCell(3).SetCellValue(claim.RAPCount);
                    row.CreateCell(4).SetCellValue(claim.FinalCount);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Submitted Batch Claims: {0}", batchClaims.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);

        }
    }
}
