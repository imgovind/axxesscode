﻿namespace Axxess.AgencyManagement.App.Exports
{

    using System.Collections.Generic;


    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;


    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using System;

    public class UserScheduleExporter : BaseExporter
    {
        private IList<UserVisit> userVisit;
        public UserScheduleExporter(IList<UserVisit> userVisit)
            : base()
        {
            this.userVisit = userVisit;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - User Schedule Tasks";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("UserScheduleTasks");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("User Schedule Tasks");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Date");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.userVisit.Count > 0)
            {
                int i = 2;
                this.userVisit.ForEach(u =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(u.PatientName);
                    dataRow.CreateCell(1).SetCellValue(u.TaskName.ToTitleCase());
                    dataRow.CreateCell(2).SetCellValue(u.VisitDate.ToZeroFilled());
                    dataRow.CreateCell(3).SetCellValue(u.StatusName);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of User Schedule Tasks: {0}", userVisit.Count));
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
        }
    }
}
