﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;

    public interface IMembershipService
    {
        void LogOff(string userName);

        bool Deactivate(Guid userId);
        bool Activate(Account account);

        AxxessPrincipal Get(string userName);
        AxxessPrincipal Get(Guid userId, Guid agencyId);

        bool Login(Account account);
        LoginAttemptType Validate(Account account);
        ResetAttemptType Validate(string userName);

        bool Impersonate(Guid linkId);

        bool ResetPassword(string userName);
        bool UpdatePassword(Account account);

        bool ResetSignature(Guid loginId);
        bool UpdateSignature(Account account);

        bool InitializeWith(Account account);

        bool Switch(Guid loginId);
    }
}
