﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Collections.Generic;

    using Axxess.LookUp.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Xsl;


    public static class HtmlHelperExtensions
    {
        #region Data Repositories

        private static IUserRepository userRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.UserRepository;
            }
        }

        private static IAssetRepository assetRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AssetRepository;
            }
        }

        private static IPatientRepository patientRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.PatientRepository;
            }
        }

        private static IAgencyRepository agencyRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AgencyRepository;
            }
        }

        private static IPhysicianRepository physicianRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.PhysicianRepository;
            }
        }

        private static ILookupRepository lookupRepository
        {
            get
            {
                ILookUpDataProvider dataProvider = Container.Resolve<ILookUpDataProvider>();
                return dataProvider.LookUpRepository;
            }
        }

        private static IAssessmentRepository assessmentRepository
        {
            get
            {
                IOasisCDataProvider dataProvider = Container.Resolve<IOasisCDataProvider>();
                return dataProvider.OasisAssessmentRepository;
            }
        }

        private static ICachedDataRepository cachedDataRepository
        {
            get
            {
                IOasisCDataProvider dataProvider = Container.Resolve<IOasisCDataProvider>();
                return dataProvider.CachedDataRepository;
            }
        }

        #endregion

        #region HtmlHelper Extensions

        public static MvcHtmlString AssemblyVersion(this HtmlHelper html)
        {
            System.Version version = Assembly.GetExecutingAssembly().GetName().Version;
            var versionText = string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
            return MvcHtmlString.Create(versionText);
        }

        public static MvcHtmlString ZipCode(this HtmlHelper html)
        {
            //var ipAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            //if (ipAddress.IsNullOrEmpty())
            //{
            //    ipAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            //}

            //int intAddress = BitConverter.ToInt32(System.Net.IPAddress.Parse(ipAddress).GetAddressBytes(), 0);

            //var zipcode = lookupRepository.GetZipCodeFromIpAddress(intAddress);

            //if (zipcode.IsNullOrEmpty())
            //{
            //    zipcode = "75243";
            //}
            var zipCode = "75243";
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null && agency.MainLocation != null)
            {
                zipCode = agency.MainLocation.AddressZipCode;
            }
            return MvcHtmlString.Create(zipCode);
        }

        public static MvcHtmlString Asset(this HtmlHelper html, Guid assetId)
        {
            var sb = new StringBuilder();
            if (!assetId.IsEmpty())
            {
                var asset = assetRepository.Get(assetId, Current.AgencyId);
                if (asset != null)
                {
                    sb.AppendFormat("<a href=\"/Asset/{0}\">{1}</a>&#160;", asset.Id.ToString(), asset.FileName);
                }
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Recipients(this HtmlHelper html)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div class=\"recipient-list\" id=\"recipient-list\">");
            sb.AppendLine("<div class=\"compose-header\">Recipient List</div>");
            sb.AppendLine("<div class=\"recipient-panel\"><div class=\"recipient\"><input type=\"checkbox\" class=\"contact\" id=\"New_Message_CheckallRecipients\" />&#160;<label for=\"New_Message_CheckallRecipients\" class=\"bold\">Select All</label></div>");

            var recipients = userRepository.GetAgencyUsers(Current.AgencyId);
            if (recipients.Count > 0)
            {
                int counter = 1;
                recipients.ForEach(r =>
                {
                    if (r.Id != Current.UserId)
                    {
                        sb.AppendFormat("<div class=\"recipient{3}\"><input name=\"Recipients\" type=\"checkbox\" id=\"New_Message_Recipient_{0}\" value=\"{1}\" title=\"{2}\" onclick=\"Message.AddRemoveRecipient('New_Message_Recipient_{0}');\" />&#160;<label for=\"New_Message_Recipient_{0}\">{2}</label></div>", counter.ToString(), r.Id.ToString(), r.DisplayName, counter % 2 == 1 ? " t-alt" : "");
                        sb.AppendLine();
                        counter++;
                    }
                });
            }
            sb.AppendLine("</div>");
            sb.AppendLine("</div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Recipients(this HtmlHelper html, string prefix, List<Guid> selectedRecipients)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<table class=\"fixed\"><tbody>");

            var recipients = userRepository.GetAgencyUsers(Current.AgencyId);
            if (recipients.Count > 0)
            {
                int totalItems = recipients.Count;
                for (int i = 0; i < totalItems; i++)
                {
                    if (i % 3 == 0)
                    {
                        sb.Append("<tr>");
                    }

                    sb.AppendFormat("<td><input name=\"RecipientArray\" type=\"checkbox\" id=\"{0}_Recipient{1}\" value=\"{2}\" title=\"{3}\" style=\"float: left; width: 20px;\"{4} />&#160;<label for=\"{0}_Recipient{1}\" class=\"float-left\">{3}</label></td>", prefix, i.ToString(), recipients[i].Id.ToString(), recipients[i].DisplayName, selectedRecipients.Contains(recipients[i].Id) ? " checked" : string.Empty);

                    if (i % 3 == 2)
                    {
                        sb.Append("</tr>");
                    }
                }

                if (totalItems % 3 != 0)
                {
                    if (totalItems % 3 == 1)
                    {
                        sb.Append("<td colspan=\"2\"></td></tr>");
                    }
                    else
                    {
                        sb.Append("<td></td></tr>");
                    }
                }
            }
            sb.AppendLine("</tbody></table>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Templates(this HtmlHelper html, string name, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Template --",
                Value = ""
            });

            var templates = agencyRepository.GetTemplates(Current.AgencyId);
            if (templates != null && templates.Count > 0)
            {
                templates.ForEach(t =>
                {
                    items.Add(new SelectListItem
                    {
                        Text = t.Title,
                        Value = t.Id.ToString()
                    });
                });
            }

            items.Insert(items.Count, new SelectListItem
            {
                Text = "----------",
                Value = "spacer"
            });

            items.Insert(items.Count, new SelectListItem
            {
                Text = "Erase",
                Value = "empty"
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Status(this HtmlHelper html, string name, string status, int disciplineTask, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (disciplineTask > 0)
            {
                DisciplineTasks task = (DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), disciplineTask.ToString());
                var taskGroup = task.GetCustomGroup();

                Array scheduleStatusValues = Enum.GetValues(typeof(ScheduleStatus));
                foreach (ScheduleStatus scheduleStatus in scheduleStatusValues)
                {
                    if (scheduleStatus.GetGroup().IsEqual(taskGroup))
                    {
                        int statusId = (int)scheduleStatus;
                        if (status.IsEqual(statusId.ToString()))
                        {
                            items.Add(new SelectListItem
                            {
                                Text = scheduleStatus.GetDescription(),
                                Value = statusId.ToString(),
                                Selected = true
                            });
                        }
                    }
                }

                string discipline = task.GetCustomCategory();
                switch (discipline.ToLower())
                {
                    case "sn":
                    case "pt":
                    case "ot":
                    case "st":
                    case "hha":
                    case "notes":
                    case "msw":
                        items.Add(new SelectListItem
                        {
                            Text = "Completed",
                            Value = ((int)ScheduleStatus.NoteCompleted).ToString(),
                            Selected = false
                        });
                        break;
                    case "485":
                    case "486":
                    case "order":
                    case "nonoasis485":
                        items.Add(new SelectListItem
                        {
                            Text = "Completed",
                            Value = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString(),
                            Selected = false
                        });
                        break;
                    case "reportsandnotes":
                        items.Add(new SelectListItem
                        {
                            Text = "Completed",
                            Value = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString(),
                            Selected = false
                        });
                        break;
                    case "oasis":
                    case "nonoasis":
                        items.Add(new SelectListItem
                        {
                            Text = "Completed",
                            Value = ((int)ScheduleStatus.OasisExported).ToString(),
                            Selected = false
                        });
                        break;
                }
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Agencies(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var agencies = agencyRepository.All();
            tempItems = from agency in agencies
                        select new SelectListItem
                        {
                            Text = agency.Name,
                            Value = agency.Id.ToString(),
                            Selected = (agency.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Agency --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PreviousNotes(this HtmlHelper html, IDictionary<Guid, string> previousNotes, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            tempItems = from note in previousNotes
                        select new SelectListItem
                        {
                            Text = note.Value,
                            Value = note.Key.ToString()
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Previous Notes --",
                Value = string.Empty.ToString()
            });

            return html.DropDownList("PreviousNotes", items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PreviousAssessments(this HtmlHelper html, Guid patientId, Guid assessmentId, int assessmentType, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            int count = 0;
            var assessments = new List<Assessment>();
            var previousAssessments = new Dictionary<string, string>();
            if (assessmentType < 5)
            {
                var socAssessments = assessmentRepository.GetPreviousAssessments(Current.AgencyId, patientId, AssessmentType.StartOfCare);
                if (socAssessments != null && socAssessments.Count > 0)
                {
                    assessments.AddRange(socAssessments);
                }

                var rocAssessments = assessmentRepository.GetPreviousAssessments(Current.AgencyId, patientId, AssessmentType.ResumptionOfCare);
                if (rocAssessments != null && rocAssessments.Count > 0)
                {
                    assessments.AddRange(rocAssessments);
                }

                var recertAssessments = assessmentRepository.GetPreviousAssessments(Current.AgencyId, patientId, AssessmentType.Recertification);
                if (recertAssessments != null && recertAssessments.Count > 0)
                {
                    assessments.AddRange(recertAssessments);
                }
            }
            else if (assessmentType > 10)
            {
                var nonOasisSocAssessments = assessmentRepository.GetPreviousAssessments(Current.AgencyId, patientId, AssessmentType.NonOasisStartOfCare);
                if (nonOasisSocAssessments != null && nonOasisSocAssessments.Count > 0)
                {
                    assessments.AddRange(nonOasisSocAssessments);
                }

                var nonOasisRecertAssessments = assessmentRepository.GetPreviousAssessments(Current.AgencyId, patientId, AssessmentType.NonOasisRecertification);
                if (nonOasisRecertAssessments != null && nonOasisRecertAssessments.Count > 0)
                {
                    assessments.AddRange(nonOasisRecertAssessments);
                }
            }

            if (assessments.Count > 0)
            {
                assessments = assessments.OrderByDescending(a => a.AssessmentDate.ToShortDateString().ToOrderedDate()).ToList();
                assessments.ForEach(assessment =>
                {
                    if (assessment.Id != assessmentId)
                    {
                        var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, assessment.EpisodeId, assessment.PatientId, assessment.Id);
                        if (scheduleEvent != null)
                        {
                            var status = scheduleEvent.Status != null && Enum.IsDefined(typeof(ScheduleStatus), int.Parse(scheduleEvent.Status)) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status) : ScheduleStatus.NoStatus;
                            if (count < 5 && (status == ScheduleStatus.OasisCompletedPendingReview
                                || status == ScheduleStatus.OasisCompletedExportReady
                                || status == ScheduleStatus.OasisExported))
                            {
                                previousAssessments.Add(string.Format("{0}_{1}", assessment.Id, assessment.Type.ToString()), string.Format("{0} {1} by {2}", assessment.TypeDescription, scheduleEvent.EventDate.ToZeroFilled(), UserEngine.GetName(scheduleEvent.UserId, Current.AgencyId)));
                                count++;
                            }
                            
                            if (count == 5)
                            {
                                return;
                            }
                        }
                    }
                });
            }
            tempItems = from note in previousAssessments
                        select new SelectListItem
                        {
                            Text = note.Value,
                            Value = note.Key.ToString()
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select --",
                Value = string.Empty
            });
            return html.DropDownList("PreviousAssessments", items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Insurances(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        items.Add(new SelectListItem
                         {
                             Text = standardInsurance.Name,
                             Value = standardInsurance.Id.ToString(),
                             Selected = (standardInsurance.Id.ToString().IsEqual(value))
                         });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = "All",
                Value = "0"
            });
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesFilter(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        items.Add(new SelectListItem
                        {
                            Text = standardInsurance.Name,
                            Value = standardInsurance.Id.ToString(),
                            Selected = (standardInsurance.Id.ToString().IsEqual(value))
                        });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = "All",
                Value = "0"
            });
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            items.Add(new SelectListItem()
                {
                    Text = "No Insurance assigned",
                    Value = "-1"
                });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesMedicare(this HtmlHelper html, string name, string value, bool isSelectAll, string title, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = (standardInsurance.Id.ToString().IsEqual(value)) });
                    }
                }
            }

            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                if (i.PayorType == 2 || i.PayorType == 3)
                {
                    items.Add(new SelectListItem() { Text = i.Name, Value = i.Id.ToString(), Selected = (i.Id.ToString().IsEqual(value)) });
                }
            });
            if (isSelectAll)
            {
                items.Insert(0, new SelectListItem { Text = title, Value = "0" });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesNoneMedicare(this HtmlHelper html, string name, string value, bool isSelectAll, string title, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            if (isSelectAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = title,
                    Value = "0"
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Insurances(this HtmlHelper html, string name, string value, bool addNew, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        items.Add(new SelectListItem
                        {
                            Text = standardInsurance.Name,
                            Value = standardInsurance.Id.ToString(),
                            Selected = (standardInsurance.Id.ToString().IsEqual(value))
                        });

                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Insurance --",
                Value = "0"
            });

            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            if (addNew)
            {
                items.Insert(1, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
                items.Insert(2, new SelectListItem
                {
                    Text = "** Add New Insurance **",
                    Value = "new"
                });
                items.Insert(3, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Insurances(this HtmlHelper html, string name, string value, bool addNew, IDictionary<string, string> htmlAttributes)
        {
            var attributes = new StringBuilder();
            var selectList = new StringBuilder();
            if (htmlAttributes != null && htmlAttributes.Count > 0)
            {
                htmlAttributes.ForEach(kvp => attributes.AppendFormat("{0}='{1}' ", kvp.Key, kvp.Value));
            }
            selectList.AppendFormat("<select name={0} {1}><option value='{2}' IsHmo='{3}'>-- Select Insurance --</option>", name, attributes.ToString(), "0", 0);
            if (addNew)
            {
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "new", 0, "** Add New Insurance **");
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
            }
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", standardInsurance.Id.ToString(), 0, ((standardInsurance.Id.ToString().IsEqual(value)) ? " selected" : ""), standardInsurance.Name);
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(insurance =>
            {
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", insurance.Id.ToString(), 1, ((insurance.Id.ToString().IsEqual(value)) ? " selected" : ""), insurance.Name);
            });
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString ReportBranchList(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var branches = agencyRepository.GetBranches(Current.AgencyId);
            tempItems = from branch in branches
                        select new SelectListItem
                        {
                            Text = branch.Name,
                            Value = branch.Id.ToString(),
                            Selected = (branch.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- All Branches --",
                Value = Guid.Empty.ToString(),
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString MedicationTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();

            items.Add(new SelectListItem { Text = "New", Value = "N", Selected = value.IsEqual("N") });
            items.Add(new SelectListItem { Text = "Changed", Value = "C", Selected = value.IsEqual("C") });
            items.Add(new SelectListItem { Text = "Unchanged", Value = "U", Selected = value.IsEqual("U") });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DisciplineTaskList(this HtmlHelper html, string discipline, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var tasks = lookupRepository.DisciplineTasks(discipline);
            tempItems = from task in tasks
                        select new SelectListItem
                        {
                            Text = task.Task,
                            Value = task.Id.ToString(),
                            Selected = (task.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Task --",
                Value = ""
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString MultipleDisciplineTasks(this HtmlHelper html, string name, string value, IDictionary<string, string> htmlAttributes)
        {
            var tasks = lookupRepository.DisciplineTasks()
                .Where(d => d.Discipline == "Nursing" || d.Discipline == "PT" || d.Discipline == "ST" || d.Discipline == "OT" || d.Discipline == "HHA" || d.Discipline == "MSW" || d.Discipline == "Orders" || d.Discipline == "ReportsAndNotes")
                .OrderBy(d => d.Task)
                .ToList();

            var attributes = new StringBuilder();
            var selectList = new StringBuilder();
            if (htmlAttributes != null && htmlAttributes.Count > 0)
            {
                htmlAttributes.ForEach(kvp => attributes.AppendFormat("{0}='{1}' ", kvp.Key, kvp.Value));
            }
            selectList.AppendFormat("<select name={0} {1}><option value=''>-- Select Discipline --</option>", name, attributes.ToString());
            tasks.ForEach(task =>
            {
                selectList.AppendFormat("<option value='{0}' IsBillable='{1}' data='{2}'{3}>{4}</option>", task.Id.ToString(), task.IsBillable, task.Discipline, ((task.Id.ToString().IsEqual(value)) ? " selected" : ""), task.Task);
            });
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString StatusPatients(this HtmlHelper html, string name, string excludedValue, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            Array patientStatusValues = Enum.GetValues(typeof(PatientStatus));
            foreach (PatientStatus patientStatus in patientStatusValues)
            {
                var statusId = (int)patientStatus;
                if (statusId.ToString() != excludedValue && statusId != (int)PatientStatus.Deprecated)
                {
                    items.Add(new SelectListItem
                    {
                        Text = patientStatus.GetDescription(),
                        Value = statusId.ToString()
                    });
                }
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LookupSelectList(this HtmlHelper html, SelectListTypes listType, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            switch (listType)
            {
                case SelectListTypes.States:
                    var states = lookupRepository.States();
                    tempItems = from state in states
                                select new SelectListItem
                                {
                                    Text = state.Name,
                                    Value = state.Code,
                                    Selected = (state.Code.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select State --",
                        Value = " "
                    });
                    break;
                case SelectListTypes.Insurance:
                    var insurances = agencyRepository.GetInsurances(Current.AgencyId);
                    tempItems = from insurance in insurances
                                select new SelectListItem
                                {
                                    Text = insurance.Name,
                                    Value = insurance.Id.ToString(),
                                    Selected = (insurance.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Insurance --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.AuthorizationStatus:
                    var authStatusTypes = new List<string>();
                    Array authStatusValues = Enum.GetValues(typeof(AuthorizationStatusTypes));
                    foreach (AuthorizationStatusTypes authStatusType in authStatusValues)
                    {
                        authStatusTypes.Add(authStatusType.GetDescription());
                    }
                    tempItems = from type in authStatusTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    break;
                case SelectListTypes.CahpsVendors:
                    var cahpsVendors = new Dictionary<string, int>();
                    Array cahpsVendorValues = Enum.GetValues(typeof(CahpsVendors));
                    foreach (CahpsVendors cahpsVendor in cahpsVendorValues)
                    {
                        cahpsVendors.Add(cahpsVendor.GetDescription(), (int)cahpsVendor);
                    }

                    tempItems = from type in cahpsVendors
                                select new SelectListItem
                                {
                                    Text = type.Key,
                                    Value = type.Value.ToString(),
                                    Selected = (type.Value.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    break;
                case SelectListTypes.ContactTypes:
                    var contactTypes = new List<string>();
                    Array contactTypeValues = Enum.GetValues(typeof(ContactTypes));
                    foreach (ContactTypes contactType in contactTypeValues)
                    {
                        contactTypes.Add(contactType.GetDescription());
                    }

                    tempItems = from type in contactTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Contact Type --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.LicenseTypes:
                    var licenseTypes = new List<string>();
                    Array licenseTypeValues = Enum.GetValues(typeof(LicenseTypes));
                    foreach (LicenseTypes licenseType in licenseTypeValues)
                    {
                        licenseTypes.Add(licenseType.GetDescription());
                    }

                    tempItems = from type in licenseTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select License Type --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.CredentialTypes:
                    var credentialTypes = new List<string>();
                    Array credentialValues = Enum.GetValues(typeof(CredentialTypes));
                    foreach (CredentialTypes credentialType in credentialValues)
                    {
                        credentialTypes.Add(credentialType.GetDescription());
                    }

                    tempItems = from type in credentialTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Unknown --",
                        Value = ""
                    });
                    break;
                case SelectListTypes.TitleTypes:
                    var titleTypes = new List<string>();
                    Array titleValues = Enum.GetValues(typeof(TitleTypes));
                    foreach (TitleTypes titleType in titleValues)
                    {
                        titleTypes.Add(titleType.GetDescription());
                    }

                    tempItems = from type in titleTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Title Type --",
                        Value = "",
                    });
                    break;
                case SelectListTypes.AdmissionSources:
                    var adminSources = lookupRepository.AdmissionSources();
                    tempItems = from source in adminSources
                                select new SelectListItem
                                {
                                    Text = string.Format("({0}) {1}", source.Code, source.Description),
                                    Value = source.Id.ToString(),
                                    Selected = (source.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Admission Source --",
                        Value = "0",
                    });
                    break;
                case SelectListTypes.Users:
                    var users = userRepository.GetUsersOnly(Current.AgencyId, (int)UserStatus.Active);
                    users = users.OrderBy(u => u.DisplayName).ToList();
                    tempItems = from user in users
                                select new SelectListItem
                                {
                                    Text = user.DisplayName,
                                    Value = user.Id.ToString(),
                                    Selected = (user.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select User --",
                        Value = Guid.Empty.ToString(),
                    });
                    break;
                case SelectListTypes.Branches:
                    var branches = agencyRepository.GetBranches(Current.AgencyId);
                    tempItems = from branch in branches
                                select new SelectListItem
                                {
                                    Text = branch.Name,
                                    Value = branch.Id.ToString(),
                                    Selected = (branch.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    break;

                case SelectListTypes.BranchesReport:
                    var branchesReport = agencyRepository.GetBranches(Current.AgencyId);
                    tempItems = from branch in branchesReport
                                select new SelectListItem
                                {
                                    Text = branch.Name,
                                    Value = branch.Id.ToString(),
                                    Selected = (branch.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "All",
                        Value = Guid.Empty.ToString()
                    });
                    break;
                case SelectListTypes.Patients:
                    if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
                    {
                        var patients = patientRepository.FindPatientOnly((int)PatientStatus.Active, Current.AgencyId).ToList();

                        tempItems = from patient in patients
                                    select new SelectListItem
                                    {
                                        Text = patient.DisplayName.Trim().ToTitleCase(),
                                        Value = patient.Id.ToString(),
                                        Selected = (patient.Id.ToString().IsEqual(value))
                                    };
                    }
                    else if (Current.IsClinicianOrHHA)
                    {
                        var patients = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active);

                        tempItems = from patient in patients
                                    select new SelectListItem
                                    {
                                        Text = patient.DisplayName.Trim().ToTitleCase(),
                                        Value = patient.Id.ToString(),
                                        Selected = (patient.Id.ToString().IsEqual(value))
                                    };
                    }
                    items = tempItems.OrderBy(i => i.Text).ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Patient --",
                        Value = Guid.Empty.ToString()
                    });
                    break;
                case SelectListTypes.Physicians:
                    var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId).OrderBy(p => p.DisplayName);
                    tempItems = from physician in physicians
                                select new SelectListItem
                                {
                                    Text = string.Format("{0} ({1})", physician.DisplayName, physician.NPI),
                                    Value = physician.Id.ToString(),
                                    Selected = (physician.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Physician --",
                        Value = Guid.Empty.ToString()
                    });

                    break;
                case SelectListTypes.PaymentSource:
                    var paymentSources = lookupRepository.PaymentSources();
                    tempItems = from paymentSource in paymentSources
                                select new SelectListItem
                                {
                                    Text = paymentSource.Name,
                                    Value = paymentSource.Id.ToString(),
                                    Selected = (paymentSource.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                  {
                      Text = "-- Select Payment Source --",
                      Value = "0"
                  });
                    items.Insert(11, new SelectListItem
                    {
                        Text = "Contract",
                        Value = "13"
                    });
                    break;
                default:
                    break;

                case SelectListTypes.MapAddress:
                    var addresses = agencyRepository.GetBranches(Current.AgencyId);
                    tempItems = from address in addresses
                                select new SelectListItem
                                {
                                    Text = address.Name,
                                    Value = string.Format("{0} {1}, {2}, {3} {4}", address.AddressLine1, address.AddressLine2, address.AddressCity, address.AddressStateCode, address.AddressZipCode)
                                };
                    items = tempItems.ToList();
                    if (Current.UserAddress.IsNotNullOrEmpty())
                    {
                        items.Insert(0, new SelectListItem
                        {
                            Text = "Your Address",
                            Value = Current.UserAddress
                        });
                    }
                    items.Add(new SelectListItem
                    {
                        Text = "Specify Address",
                        Value = "specify"
                    });
                    break;
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PaymentSourceWithOutMedicareTradition(this HtmlHelper html,  string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var paymentSources = lookupRepository.PaymentSources();
            tempItems = from paymentSource in paymentSources where paymentSource.Id!=3
                        select new SelectListItem
                        {
                            Text = paymentSource.Name,
                            Value = paymentSource.Id.ToString(),
                            Selected = (paymentSource.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Payment Source --",
                Value = "0"
            });
            items.Insert(11, new SelectListItem
            {
                Text = "Contract",
                Value = "13"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LookupSelectListWithBranchAndStatus(this HtmlHelper html, SelectListTypes listType, string name, string value, Guid branchId, int status, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            switch (listType)
            {
                case SelectListTypes.Users:
                    var users = new List<User>();
                    if (status == 0)
                    {
                        if (branchId.IsEmpty())
                        {
                            users = userRepository.GetUsersOnly(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
                        }
                        else
                        {
                            users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
                        }
                    }
                    else
                    {
                        if (branchId.IsEmpty())
                        {
                            users = userRepository.GetUsersByStatus(Current.AgencyId, status).OrderBy(u => u.DisplayName).ToList();
                        }
                        else
                        {
                            users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId, status).OrderBy(u => u.DisplayName).ToList();
                        }

                    }
                    tempItems = from user in users
                                select new SelectListItem
                                {
                                    Text = user.DisplayName,
                                    Value = user.Id.ToString(),
                                    Selected = (user.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select User --",
                        Value = Guid.Empty.ToString(),
                    });
                    break;



                case SelectListTypes.Patients:
                    IList<Patient> patients = patientRepository.Find(status, branchId, Current.AgencyId);
                    
                    tempItems = from patient in patients
                                select new SelectListItem
                                {
                                    Text = patient.DisplayName.Trim().ToUpperCase(),
                                    Value = patient.Id.ToString(),
                                    Selected = (patient.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.OrderBy(i => i.Text).ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Patient --",
                        Value = Guid.Empty.ToString()
                    });
                    break;

            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Physicians(this HtmlHelper html, string name, string value, bool addNew, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId).OrderBy(p => p.DisplayName);
            tempItems = from physician in physicians
                        select new SelectListItem
                        {
                            Text = string.Format("{0} ({1})", physician.DisplayName, physician.NPI),
                            Value = physician.Id.ToString(),
                            Selected = (physician.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Physician --",
                Value = Guid.Empty.ToString()
            });

            if (addNew)
            {
                items.Insert(1, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
                items.Insert(2, new SelectListItem
                {
                    Text = "** Add New Physician **",
                    Value = "new"
                });
                items.Insert(3, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PermissionList(this HtmlHelper html, string category, List<string> userPermissions)
        {
            var sb = new StringBuilder();
            sb.Append("<div class=\"row\">");
            sb.AppendFormat("<label class=\"float-left tall-line\">{0} &#8211; </label>", category);
            sb.AppendFormat("<div class=\"checkgroup fr\"><div class=\"option\"><input id=\"New_User_{0}Permissions\" type=\"checkbox\" onclick=\"User.SelectPermissions($(this), 'input.{0}:checkbox');\" class=\"radio float-left\" value=\"\" />", category.Replace(" ", ""));
            sb.AppendFormat("<label for=\"New_User_{0}Permissions\" class=\"radio\">Select all {1}</label></div></div><div class=\"wide checkgroup\">", category.Replace(" ", ""), category);

            Permissions[] permissions = (Permissions[])Enum.GetValues(typeof(Permissions));
            foreach (Permissions permission in permissions)
            {
                Permission p = permission.GetPermission();
                if (p.Category == category)
                {
                    sb.AppendLine("<div class=\"option\">");
                    ulong id = (ulong)permission;
                    string controlChecked = string.Empty;
                    string controlId = string.Format("New_User_Permission_{0}", id.ToString());
                    if (userPermissions != null && userPermissions.Count > 0)
                    {
                        controlId = string.Format("Edit_User_Permission_{0}", id.ToString());
                        if (userPermissions.Contains(id.ToString()))
                        {
                            controlChecked = " Checked=\"Checked\"";
                        }
                    }

                    sb.AppendFormat("<input id=\"{0}\" type=\"checkbox\" value=\"{1}\" name=\"PermissionsArray\" class=\"{2} required radio float-left\"{3} />", controlId.ToString(), id.ToString(), category.Replace(" ", ""), controlChecked);
                    sb.AppendFormat("<label for=\"{0}\" class=\"radio\"><strong>{1}</strong> &#8211; {2}</label>", controlId.ToString(), p.Description, p.LongDescription);
                    sb.AppendLine("</div>");
                }
            }
            sb.AppendLine("</div></div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString CaseManagers(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetCaseManagerUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Case Manager --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString HHAides(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetHHAUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Home Health Aide --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LVNs(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetLVNUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select LVN --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Clinicians(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetClinicalUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Clinician --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Users(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetUsersOnly(Current.AgencyId, (int)UserStatus.Active);
            users = users.OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Users --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Patients(this HtmlHelper html, string name, string value, int status, string title, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                var patients = patientRepository.Find(status, Guid.Empty, Current.AgencyId);
                tempItems = from patient in patients
                            select new SelectListItem
                            {
                                Text = patient.DisplayName.Trim().ToTitleCase(),
                                Value = patient.Id.ToString(),
                                Selected = (patient.Id.ToString().IsEqual(value))
                            };
            }
            else if (Current.IsClinicianOrHHA)
            {
                var patients = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)status);
                tempItems = from patient in patients
                            select new SelectListItem
                            {
                                Text = patient.DisplayName.Trim().ToTitleCase(),
                                Value = patient.Id.ToString(),
                                Selected = (patient.Id.ToString().IsEqual(value))
                            };
            }
           
            items = tempItems.OrderBy(i => i.Text).ToList();
            items.Insert(0, new SelectListItem
            {
                Text = title,
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PatientEpisodes(this HtmlHelper html, string name, string value, Guid patientId, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var episodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patientId).OrderBy(e => e.StartDate).ToList();
            tempItems = from episode in episodes
                        select new SelectListItem
                        {
                            Text = string.Format("{0} - {1}", episode.StartDate.ToString("MM/dd/yyyy"), episode.EndDate.ToString("MM/dd/yyyy")),
                            Value = episode.Id.ToString(),
                            Selected = (episode.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PatientEpisodes(this HtmlHelper html, string name, string value, Guid patientId, string title, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IEnumerable<SelectListItem> tempItems = null;

            if (!patientId.IsEmpty())
            {
                var episodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patientId).OrderBy(e => e.StartDate).ToList();
                tempItems = from episode in episodes
                            select new SelectListItem
                            {
                                Text = string.Format("{0} - {1}", episode.StartDate.ToString("MM/dd/yyyy"), episode.EndDate.ToString("MM/dd/yyyy")),
                                Value = episode.Id.ToString(),
                                Selected = (episode.Id.ToString().IsEqual(value))
                            };

                items = tempItems.ToList();
            }
            items.Insert(0, new SelectListItem
            {
                Text = title,
                Value = Guid.Empty.ToString()
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Months(this HtmlHelper html, string name, string value, int start, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            List<SelectListItem> tempItems = new List<SelectListItem>();

            int currentYear = DateTime.Now.Year;
            if (start < currentYear)
            {
                for (int val = start; val <= currentYear; val++)
                {
                    tempItems.Add(new SelectListItem
                    {
                        Text = val.ToString(),
                        Value = val.ToString(),
                        Selected = (val.ToString().IsEqual(value))
                    });
                }
            }
            tempItems.Reverse();
            items = tempItems.ToList();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DisciplineTypes(this HtmlHelper html, string name, int disciplineTask, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            if (Enum.IsDefined(typeof(DisciplineTasks), disciplineTask))
            {
                var task = (DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), disciplineTask.ToString());
                var formGroup = task.GetFormGroup();
                if (formGroup.IsNotNullOrEmpty())
                {
                    Array disciplineTaskArray = Enum.GetValues(typeof(DisciplineTasks));
                    foreach (DisciplineTasks disciplineTaskItem in disciplineTaskArray)
                    {
                        if (formGroup.IsEqual(disciplineTaskItem.GetFormGroup()))
                        {
                            var data = lookupRepository.GetDisciplineTask((int)disciplineTaskItem);
                            if (data != null)
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = data.Task,
                                    Value = ((int)data.Id).ToString(),
                                    Selected = ((int)data.Id == disciplineTask ? true : false)
                                });
                            }
                        }
                    }
                }

            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static void RenderXML(this HtmlHelper html, XmlDocument xmlDocument, string XSLTPath, Dictionary<string, string> xslArgParams)
        {
            ViewContext context = html.ViewContext;
            XsltArgumentList xslArgs = new XsltArgumentList();
            if (xslArgParams != null)
            {
                foreach (string key in xslArgParams.Keys)
                {
                    xslArgs.AddParam(key, null, xslArgParams[key]);
                }
            }
            XslCompiledTransform t = new XslCompiledTransform();
            t.Load(XSLTPath);
            t.Transform(xmlDocument, xslArgs, context.HttpContext.Response.Output);
        }

        public static MvcHtmlString ClaimTypes(this HtmlHelper html, string name, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array claimTypes = Enum.GetValues(typeof(ClaimType));
            foreach (ClaimType claimType in claimTypes)
            {
                items.Add(new SelectListItem
                {
                    Text = claimType.GetDescription(),
                    Value = claimType.ToString(),
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "All",
                Value = "All ",
                Selected=true
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString UB4PatientStatus(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var ub4PatientStatusValues = Enum.GetValues(typeof(UB4PatientStatus));
            if (ub4PatientStatusValues != null && ub4PatientStatusValues.Length > 0)
            {
                foreach (UB4PatientStatus status in ub4PatientStatusValues)
                {
                        items.Add(new SelectListItem
                        {
                            Text = status.GetDescription(),
                            Value = ((int)status).ToString(),
                            Selected= value==((int)status).ToString()
                        });
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Patient Status --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }


        #endregion
    }
}
