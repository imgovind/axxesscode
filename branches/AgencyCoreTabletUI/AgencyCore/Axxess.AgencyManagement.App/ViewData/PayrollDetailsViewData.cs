﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.App.Domain;

   public class PayrollDetailsViewData
    {
       public DateTime StartDate { get; set; }
       public DateTime EndDate { get; set; }
       public List<PayrollDetail> Details { get; set; }
       public string PayrollStatus { get; set; }

    }
}
