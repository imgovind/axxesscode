﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Domain;

    public class OasisAudit
    {
        public Guid Id { get; set; }
        public Patient Patient { get; set; }
        public Agency Agency { get; set; }
        public Assessment Assessment { get; set; }
        public List<LogicalError> LogicalErrors { get; set; }
    }
}
