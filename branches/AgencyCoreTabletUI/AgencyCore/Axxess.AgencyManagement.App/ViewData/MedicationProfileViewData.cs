﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Domain;

    public class MedicationProfileViewData
    {
        public Guid EpisodeId { get; set; }
        public Patient Patient { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public MedicationProfile MedicationProfile { get; set; }
        public IDictionary<string, Question> Questions { get; set; }
        public Guid PhysicianId { get; set; }
        public string PhysicianDisplayName { get; set; }
        public AssessmentType AssessmentType { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        public Agency Agency { get; set; }
        public string Allergies { get; set; }
        public string SignatureName { get; set; }
        public DateTime SignatureDate { get; set; }
    }
}