﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;

    using Axxess.Core;
    using Axxess.AgencyManagement.Repositories;

    public class DateService : IDateService
    {
        private readonly IPatientRepository patientRepository;

        public DateService(IAgencyManagementDataProvider dataProvider)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            this.patientRepository = dataProvider.PatientRepository;
        }

        public DateRange GetDateRange(string rangeIdentifier, Guid patientId)
        {
            DateRange dateRange = new DateRange() { Id = rangeIdentifier };
            DateTime currentStartDate = DateUtilities.GetStartOfDay(DateTime.Now);
            DateTime currentEndDate = DateUtilities.GetEndOfDay(DateTime.Now);
            switch (rangeIdentifier)
            {
                case "All":
                    dateRange.StartDate = DateTime.MinValue;
                    dateRange.EndDate = DateTime.MaxValue;
                    break;
                case "Today":
                    dateRange.StartDate = currentStartDate;
                    dateRange.EndDate = currentEndDate;
                    break;
                case "Yesterday":
                    dateRange.StartDate = currentStartDate.AddDays(-1);
                    dateRange.EndDate = currentStartDate.AddDays(-1);
                    break;
                case "ThisWeek":
                    dateRange.StartDate = DateUtilities.GetStartOfCurrentWeek();
                    dateRange.EndDate = DateUtilities.GetEndOfCurrentWeek();
                    break;
                case "ThisWeekToDate":
                    dateRange.StartDate = DateUtilities.GetStartOfCurrentWeek();
                    dateRange.EndDate = currentEndDate;
                    break;
                case "LastWeek":
                    dateRange.StartDate = DateUtilities.GetStartOfLastWeek();
                    dateRange.EndDate = DateUtilities.GetEndOfLastWeek();
                    break;
                case "LastWeekToDate":
                    dateRange.StartDate = DateUtilities.GetStartOfLastWeek();
                    dateRange.EndDate = currentEndDate;
                    break;
                case "NextWeek":
                    dateRange.StartDate = DateUtilities.GetStartOfNextWeek();
                    dateRange.EndDate = DateUtilities.GetEndOfNextWeek();
                    break;
                case "Next4Weeks":
                    dateRange.StartDate = DateUtilities.GetStartOfNextWeek();
                    dateRange.EndDate = DateUtilities.GetStartOfNextWeek().AddDays(27);
                    break;
                case "ThisMonth":
                    dateRange.StartDate = DateUtilities.GetStartOfCurrentMonth();
                    dateRange.EndDate = DateUtilities.GetEndOfCurrentMonth();
                    break;
                case "ThisMonthToDate":
                    dateRange.StartDate = DateUtilities.GetStartOfCurrentMonth();
                    dateRange.EndDate = currentEndDate;
                    break;
                case "LastMonth":
                    dateRange.StartDate = DateUtilities.GetStartOfLastMonth();
                    dateRange.EndDate = DateUtilities.GetEndOfLastMonth();
                    break;
                case "LastMonthToDate":
                    dateRange.StartDate = DateUtilities.GetStartOfLastMonth();
                    dateRange.EndDate = currentEndDate;
                    break;
                case "NextMonth":
                    dateRange.StartDate = DateUtilities.GetStartOfNextMonth();
                    dateRange.EndDate = DateUtilities.GetEndOfNextMonth();
                    break;
                case "ThisFiscalQuarter":
                    dateRange.StartDate = DateUtilities.GetStartOfCurrentQuarter();
                    dateRange.EndDate = DateUtilities.GetEndOfCurrentQuarter();
                    break;
                case "ThisFiscalQuarterToDate":
                    dateRange.StartDate = DateUtilities.GetStartOfCurrentQuarter();
                    dateRange.EndDate = currentEndDate;
                    break;
                case "ThisFiscalYear":
                    dateRange.StartDate = DateUtilities.GetStartOfCurrentYear();
                    dateRange.EndDate = DateUtilities.GetEndOfCurrentYear();
                    break;
                case "ThisFiscalYearToDate":
                    dateRange.StartDate = DateUtilities.GetStartOfCurrentYear();
                    dateRange.EndDate = currentEndDate;
                    break;
                case "LastFiscalQuarter":
                    dateRange.StartDate = DateUtilities.GetStartOfLastQuarter();
                    dateRange.EndDate = DateUtilities.GetEndOfLastQuarter();
                    break;
                case "LastFiscalQuarterToDate":
                    dateRange.StartDate = DateUtilities.GetStartOfLastQuarter();
                    dateRange.EndDate = currentEndDate;
                    break;
                case "LastFiscalYear":
                    dateRange.StartDate = DateUtilities.GetStartOfLastYear();
                    dateRange.EndDate = DateUtilities.GetEndOfLastYear();
                    break;
                case "LastFiscalYearToDate":
                    dateRange.StartDate = DateUtilities.GetStartOfLastYear();
                    dateRange.EndDate = currentEndDate;
                    break;
                case "NextFiscalQuarter":
                    dateRange.StartDate = DateUtilities.GetStartOfNextQuarter();
                    dateRange.EndDate = DateUtilities.GetEndOfNextQuarter();
                    break;
                case "NextFiscalYear":
                    dateRange.StartDate = DateUtilities.GetStartOfNextYear();
                    dateRange.EndDate = DateUtilities.GetEndOfNextYear();
                    break;
                case "ThisEpisode":
                    var episodeRange = patientRepository.GetCurrentEpisodeDate(Current.AgencyId, patientId);
                    dateRange.StartDate = episodeRange.StartDate;
                    dateRange.EndDate = episodeRange.EndDate;
                    break;
                case "NextEpisode":
                    var nextEpisodeRange = patientRepository.GetNextEpisode(Current.AgencyId, patientId);
                    dateRange.StartDate = nextEpisodeRange.StartDate;
                    dateRange.EndDate = nextEpisodeRange.EndDate;
                    break;
                case "LastEpisode":
                    var prevEpisodeRange = patientRepository.GetPreviousEpisode(Current.AgencyId, patientId);
                    dateRange.StartDate = prevEpisodeRange.StartDate;
                    dateRange.EndDate = prevEpisodeRange.EndDate;
                    break;
            }
            return dateRange;
        }
    }
}
