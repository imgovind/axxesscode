﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Text;
    using System.Web.Mvc;
    using System.Linq;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Drawing.Drawing2D;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    using Enums;
    using Common;
    using Domain;
    using ViewData;
    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Log.Enums;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.Log.Repositories;

    using Axxess.Membership.Logging;
    using Axxess.Log.Domain;

    public class PatientService : IPatientService
    {
        #region Private Members

        private readonly IDrugService drugService;
        private readonly ILogRepository logRepository;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IReferralRepository referralRepository;
        #endregion

        #region Constructor

        public PatientService(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, ILogDataProvider logDataProvider, IOasisCDataProvider oasisDataProvider, IAssessmentService assessmentService, IDrugService drugService)
        {
            Check.Argument.IsNotNull(drugService, "drugService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.drugService = drugService;
            this.assessmentService = assessmentService;
            this.logRepository = logDataProvider.LogRepository;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.assetRepository = agencyManagementDataProvider.AssetRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
        }

        #endregion

        #region IPatientService Members 

        public bool AddPatient(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            patient.AdmissionId = admissionDateId;
            if (patientRepository.Add(patient , out patientOut))
            {
                if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, PatientData = patientOut != null ? patientOut.ToXml() : string.Empty, StartOfCareDate = patient.StartofCareDate, IsActive = true, IsDeprecated = false, Status = patientOut.Status }))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdded, (patient.ShouldCreateEpisode && !patient.UserId.IsEmpty()) && patient.Status == (int)PatientStatus.Active ? string.Empty : "Pending for admission");
                    result = true;
                }
            }
            return result;
        }

        public bool EditPatient(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            var patientToEdit = patientRepository.GetPatientOnly(patient.Id, Current.AgencyId);
            if (patientToEdit != null)
            {
                var oldAdmissionDateId = patientToEdit.AdmissionId;
                patient.AdmissionId = patientToEdit.AdmissionId.IsEmpty() ? admissionDateId : patientToEdit.AdmissionId;
                if (patientRepository.Edit(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                            result = true;
                        }
                        else
                        {
                            patientToEdit.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patientToEdit);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientToEdit.Id, oldAdmissionDateId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.StartOfCareDate = patientOut.StartofCareDate;
                            admissionData.PatientData = patientOut.ToXml();
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientToEdit.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patientToEdit);
                            }
                        }
                        else
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = oldAdmissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patientOut.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientToEdit.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patientToEdit);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdatePatientForPhotoRemove(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var photoId = patient.PhotoId;
            patient.PhotoId = Guid.Empty;
            if (patientRepository.Update(patient, out patientOut))
            {
                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientOut.Id, patientOut.AdmissionId);
                if (admissionData != null && patientOut != null)
                {
                    admissionData.PatientData = patientOut.ToXml();
                    if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
                        result = true;
                    }
                    else
                    {
                        patient.PhotoId = photoId;
                        patientRepository.Update(patient);
                    }
                }
                else
                {
                    patient.PhotoId = photoId;
                    patientRepository.Update(patient);
                }
            }
            return result;
        }

        public bool DeletePatient(Guid Id)
        {
            var result = false;
            if (patientRepository.DeprecatedPatient(Current.AgencyId, Id))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, Id, Id.ToString(), LogType.Patient, LogAction.PatientDeleted, string.Empty);
                result = true;
            }
            return result;
        }

        public bool AdmitPatient(PendingPatient pending)
        {
            var result = false;
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            var patient = patientRepository.GetPatientOnly(pending.Id, Current.AgencyId);
            if (patient != null)
            {
                patient.IsFaceToFaceEncounterCreated = pending.IsFaceToFaceEncounterCreated;
                var oldAdmissionDateId = patient.AdmissionId;
                pending.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.AdmitPatient(pending, out patientOut))
                {
                    if (patientOut != null)
                    {
                        if (oldAdmissionDateId.IsEmpty())
                        {
                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
                            {
                                var medId = Guid.NewGuid();
                                patient.EpisodeStartDate = pending.EpisodeStartDate;
                                if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
                                    result = true;
                                }
                                else
                                {
                                    patientRepository.DeletePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
                                    patientRepository.Update(patient);
                                }
                            }
                            else
                            {
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                            if (admissionData != null)
                            {
                                var oldPatientData = admissionData.PatientData;
                                var oldSoc = admissionData.StartOfCareDate;
                                admissionData.PatientData = patientOut.ToXml();
                                admissionData.StartOfCareDate = pending.StartofCareDate;
                                if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                {
                                    var medId = Guid.NewGuid();
                                    patient.EpisodeStartDate = pending.EpisodeStartDate;
                                    if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
                                        result = true;
                                    }
                                    else
                                    {
                                        admissionData.PatientData = oldPatientData;
                                        admissionData.StartOfCareDate = oldSoc;
                                        patientRepository.UpdatePatientAdmissionDate(admissionData);
                                        patientRepository.Update(patient);
                                    }
                                }
                                else
                                {
                                    patientRepository.Update(patient);
                                }
                            }
                            else
                            {
                                if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
                                {
                                    var medId = Guid.NewGuid();
                                    patient.EpisodeStartDate = pending.EpisodeStartDate;
                                    if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
                                        result = true;
                                    }
                                    else
                                    {
                                        patientRepository.DeletePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
                                        patientRepository.Update(patient);
                                    }
                                }
                                else
                                {
                                    patientRepository.Update(patient);
                                }
                            }
                        }
                    }
                    else
                    {
                        patientRepository.Update(patient);
                    }
                }
            }
            return result;
        }

        public bool NonAdmitPatient(PendingPatient pending)
        {
            var result = false;
            Patient patientOut = null;
            var patient = patientRepository.GetPatientOnly(pending.Id, Current.AgencyId);
            if (patient != null)
            {
                var admissionDateId = Guid.NewGuid();
                pending.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.NonAdmitPatient(pending, out patientOut))
                {
                    if (patient.AdmissionId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                            result = true;
                        }
                        else
                        {
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patientOut.Status;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {

                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientRepository.Update(patient);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool SetPatientPending(Guid patientId)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                Patient patientOut = null;
                var oldStatus = patient.Status;
                patient.Status = (int)PatientStatus.Pending;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Set Pending.", IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                            result = true;
                        }
                        else
                        {
                            patient.Status = oldStatus;
                            patient.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patient.Status;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = oldAdmissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patientOut.StartofCareDate, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool DischargePatient(Guid patientId, DateTime dischargeDate, string dischargeReason)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var admissionDateId = Guid.NewGuid();
                var oldStatus = patient.Status;
                var oldDischargeDate = patient.DischargeDate;
                var oldDischargeReason = patient.DischargeReason;
                patient.DischargeDate = dischargeDate;
                patient.Status = (int)PatientStatus.Discharged;
                patient.DischargeReason = dischargeReason;
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                Patient patientOut = null;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                        {
                            var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, dischargeDate);
                            if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                            {
                                if (episode != null)
                                {
                                    var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                    if (final != null && (final.Status == (int)ScheduleStatus.ClaimCreated || final.Status == (int)ScheduleStatus.ClaimReOpen))
                                    {
                                        final.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateFinal(final);
                                    }
                                    var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                    if (rap != null && (rap.Status == (int)ScheduleStatus.ClaimCreated || rap.Status == (int)ScheduleStatus.ClaimReOpen))
                                    {
                                        rap.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateRap(rap);
                                    }
                                }
                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                            }
                            result = true;
                        }
                        else
                        {
                            patient.DischargeDate = oldDischargeDate;
                            patient.Status = oldStatus;
                            patient.DischargeReason = oldDischargeReason;
                            patient.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patient.Status;
                            admissionData.StartOfCareDate = patient.StartofCareDate;
                            admissionData.DischargedDate = patient.DischargeDate;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, dischargeDate);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && (final.Status == (int)ScheduleStatus.ClaimCreated || final.Status == (int)ScheduleStatus.ClaimReOpen))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && (rap.Status == (int)ScheduleStatus.ClaimCreated || rap.Status == (int)ScheduleStatus.ClaimReOpen))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                result = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                            {
                                var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, dischargeDate);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && (final.Status == (int)ScheduleStatus.ClaimCreated || final.Status == (int)ScheduleStatus.ClaimReOpen))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && (rap.Status == (int)ScheduleStatus.ClaimCreated || rap.Status == (int)ScheduleStatus.ClaimReOpen))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                result = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }

                        }
                    }
                }
            }
            return result;
        }

        public bool ActivatePatient(Guid patientId)
        {
            var result = false;
            Patient patientOut = null;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var oldStatus = patient.Status;
                patient.Status = (int)PatientStatus.Active;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (patientOut != null)
                    {
                        if (oldAdmissionDateId.IsEmpty())
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                            if (admissionData != null)
                            {
                                admissionData.PatientData = patientOut.ToXml();
                                admissionData.Status = patient.Status;
                                admissionData.StartOfCareDate = patient.StartofCareDate;
                                if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                    result = true;
                                }
                                else
                                {
                                    patient.Status = oldStatus;
                                    patient.AdmissionId = oldAdmissionDateId;
                                    patientRepository.Update(patient);
                                }
                            }
                            else
                            {
                                if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                    result = true;
                                }
                                else
                                {
                                    patient.Status = oldStatus;
                                    patient.AdmissionId = oldAdmissionDateId;
                                    patientRepository.Update(patient);
                                }
                            }
                        }
                    }
                    else
                    {
                        patient.Status = oldStatus;
                        patient.AdmissionId = oldAdmissionDateId;
                        patientRepository.Update(patient);
                    }
                }
            }
            return result;
        }

        public bool ActivatePatient(Guid patientId, DateTime startOfCareDate)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            Patient patientOut = null;
            if (patient != null)
            {
                var oldStatus = patient.Status;
                var oldStartOfCareDate = patient.StartofCareDate;
                patient.Status = (int)PatientStatus.Active;
                patient.StartofCareDate = startOfCareDate;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = admissionDateId;
                if (patientRepository.Update(patient, out patientOut))
                {
                   
                    if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
                        result = true;
                    }
                    else
                    {
                        patient.Status = oldStatus;
                        patient.StartofCareDate = oldStartOfCareDate;
                        patient.AdmissionId = oldAdmissionDateId;
                        patientRepository.Update(patient);
                    }
                   
                    //else
                    //{
                    //    var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                    //    if (admissionData != null && patientOut != null)
                    //    {
                    //        admissionData.PatientData = patientOut.ToXml();
                    //        admissionData.Status = patient.Status;
                    //        admissionData.StartOfCareDate = patient.StartofCareDate;
                    //        if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                    //        {
                    //            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
                    //            result = true;
                    //        }
                    //        else
                    //        {
                    //            patient.Status = oldStatus;
                    //            patient.StartofCareDate = oldStartOfCareDate;
                    //            patient.AdmissionId = oldAdmissionDateId;
                    //            patientRepository.Update(patient);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        patient.Status = oldStatus;
                    //        patient.StartofCareDate = oldStartOfCareDate;
                    //        patient.AdmissionId = oldAdmissionDateId;
                    //        patientRepository.Update(patient);
                    //    }
                    //}
                }
            }
            return result;
        }

        public bool AddPhoto(Guid patientId, HttpFileCollectionBase httpFiles)
        {
            var result = false;

             var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                if (httpFiles.Count > 0)
                {
                    HttpPostedFileBase file = httpFiles.Get("Photo1");
                    if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        var photo = Image.FromStream(file.InputStream);
                        var resizedImageStream = ResizeAndEncodePhoto(photo, file.ContentType, false);
                        if (resizedImageStream != null)
                        {
                            var binaryReader = new BinaryReader(resizedImageStream);
                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset))
                            {
                                Patient patientOut = null;
                                var oldPhotoId = patient.PhotoId;
                                var admissionDateId = Guid.NewGuid();
                                patient.PhotoId = asset.Id;
                                var oldAdmissionDateId = patient.AdmissionId;
                                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                                if (patientRepository.Update(patient, out patientOut))
                                {
                                    if (oldAdmissionDateId.IsEmpty())
                                    {
                                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason =string.Empty, IsDeprecated = false, IsActive = true }))
                                        {
                                            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Added/Updated");
                                            result = true;
                                        }
                                        else
                                        {
                                            patient.PhotoId = oldPhotoId;
                                            patient.AdmissionId = oldAdmissionDateId;
                                            patientRepository.Update(patient);
                                        }
                                    }
                                    else
                                    {
                                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                                        if (admissionData != null && patientOut != null)
                                        {
                                            admissionData.PatientData = patientOut.ToXml();
                                            admissionData.Status = patient.Status;
                                            admissionData.StartOfCareDate = patient.StartofCareDate;
                                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                            {
                                                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Added/Updated");
                                                result = true;
                                            }
                                            else
                                            {
                                                patient.PhotoId = oldPhotoId;
                                                patient.AdmissionId = oldAdmissionDateId;
                                                patientRepository.Update(patient);
                                            }
                                        }
                                        else
                                        {
                                            patient.PhotoId = oldPhotoId;
                                            patient.AdmissionId = oldAdmissionDateId;
                                            patientRepository.Update(patient);
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool AddPrimaryEmergencyContact(Patient patient)
        {
            var result = false;
            if (patientRepository.AddEmergencyContact(patient.EmergencyContact) && patientRepository.SetPrimaryEmergencyContact(Current.AgencyId, patient.Id, patient.EmergencyContact.Id))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEmergencyContactAdded, "The Contact is set as primary.");
                result = true;
            }
            return result;
        }

        public PatientAdmissionDate GetIfExitOrCreate(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            var admission = new PatientAdmissionDate();
            if (patient != null)
            {
                var admissionId=Guid.NewGuid();
                var oldAdmissionId = patient.AdmissionId;
                patient.AdmissionId = admissionId;
                 admission = new PatientAdmissionDate
                {
                    Id = admissionId,
                    AgencyId=Current.AgencyId,
                    PatientId=patient.Id,
                    StartOfCareDate=patient.StartofCareDate,
                    DischargedDate=patient.DischargeDate,
                    Status=patient.Status,
                    IsActive=true,
                    IsDeprecated=false,
                    Created=DateTime.Now,
                    Modified=DateTime.Now,
                    PatientData=patient.ToXml()
                };
                 if (patientRepository.Update(patient))
                 {
                     if (patientRepository.AddPatientAdmissionDate(admission))
                     {
                         return admission;
                     }
                     else
                     {
                         patient.AdmissionId = oldAdmissionId;
                         patientRepository.Update(patient);
                     }
                 }
            }
            return admission;
        }

        public bool NewEmergencyContact(PatientEmergencyContact emergencyContact, Guid patientId)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patient = patientRepository.GetPatient<Patient>(patientId, Current.AgencyId);
            bool result = false;
            if (patient != null)
            {
                emergencyContact.PatientId = patientId;
                emergencyContact.AgencyId = Current.AgencyId;
                if (patientRepository.AddEmergencyContact(emergencyContact))
                {
                    if (emergencyContact.IsPrimary)
                    {
                        patientRepository.SetPrimaryEmergencyContact(Current.AgencyId, patient.Id, emergencyContact.Id);
                    }
                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEmergencyContactAdded, emergencyContact.IsPrimary ? "The Contact is set as primary." : string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool EditEmergencyContact(PatientEmergencyContact emergencyContact)
        {
            bool result = false;
            if (patientRepository.EditEmergencyContact(Current.AgencyId, emergencyContact))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, emergencyContact.PatientId, emergencyContact.PatientId.ToString(), LogType.Patient, LogAction.PatientEmergencyContactEdited, emergencyContact.IsPrimary ? "The Contact is set as primary." : string.Empty);
                result = true;
            }
            return result;
        }

        public bool DeleteEmergencyContact(Guid Id, Guid patientId)
        {
            bool result = false;
            if (patientRepository.DeleteEmergencyContact(Id, patientId))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEmergencyContactDeleted, string.Empty);
                result = true;
            }
            return result;
        }

        public PatientProfile GetProfile(Guid patientId)
        {
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            if (patient != null )
            {
                var patientProfile = new PatientProfile();
                patientProfile.Patient = patient;
                patientProfile.Agency = agencyRepository.GetWithBranches(Current.AgencyId);

                patientProfile.Allergies = GetAllergies(patientId);

                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {
                    patientProfile.Physician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                }
                if (patient.EmergencyContacts != null && patient.EmergencyContacts.Count > 0)
                {
                    patientProfile.EmergencyContact = patient.EmergencyContacts.FirstOrDefault(p => p.IsPrimary);
                }
                if (!patient.CaseManagerId.IsEmpty())
                {
                     patient.CaseManagerName = UserEngine.GetName(patient.CaseManagerId, Current.AgencyId);
                }
                if (!patient.UserId.IsEmpty())
                {
                     patientProfile.Clinician = UserEngine.GetName(patient.UserId, Current.AgencyId);
                }

                SetInsurance(patient);

                var episode = patientRepository.GetCurrentEpisode(Current.AgencyId, patientId);
                if (episode != null)
                {
                    var freq = this.GetPatientEpisodeWithFrequency(episode.Id, patient.Id);
                    patientProfile.Frequencies = freq.Detail.FrequencyList;
                    patientProfile.CurrentEpisode = episode;
                    patientProfile.CurrentAssessment = assessmentService.GetEpisodeAssessment(episode.Id, patient.Id);
                }
                return patientProfile;
            }

            return null;
        }

        public string GetAllergies(Guid patientId)
        {
            var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
            if (allergyProfile != null)
            {
                return allergyProfile.ToString();
            }
            return string.Empty;
        }

        public bool CreateMedicationProfile(Patient patient, Guid medId)
        {
            var medicationProfile = new MedicationProfile { AgencyId = Current.AgencyId, Id = medId, Medication = new List<Medication>().ToXml(), PharmacyName = patient.PharmacyName, PharmacyPhone = patient.PharmacyPhone, PatientId = patient.Id, Created = DateTime.Now, Modified = DateTime.Now };
            if (patientRepository.AddNewMedicationProfile(medicationProfile))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, medicationProfile.Id.ToString(), LogType.MedicationProfile, LogAction.MedicationProfileAdded, string.Empty);
                return true;
            }
            return false;
        }

        public bool AddMedication(Guid medicationProfileId, Medication medication, string medicationType)
        {
            bool result = false;
            if (medication != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
                if (medicationProfile != null)
                {
                    medication.Id = Guid.NewGuid();
                    medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                    medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                    if (medicationProfile.Medication.IsNullOrEmpty())
                    {
                        var newList = new List<Medication>() { medication };
                        medicationProfile.Medication = newList.ToXml();
                    }
                    else
                    {
                        var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                        existingList.Add(medication);
                        medicationProfile.Medication = existingList.ToXml();
                    }
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, LogAction.MedicationAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateMedication(Guid medicationProfileId, Medication medication, string medicationType)
        {
            bool result = false;
            if (medication != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
                if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
                {
                    medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    if (existingList.Exists(m => m.Id == medication.Id))
                    {
                        var exisitingMedication = existingList.Single(m => m.Id == medication.Id);
                        if (exisitingMedication != null)
                        {
                            var logAction = LogAction.MedicationUpdated;
                            if (exisitingMedication.StartDate != medication.StartDate || exisitingMedication.Route != medication.Route || exisitingMedication.Frequency != medication.Frequency || exisitingMedication.MedicationDosage != medication.MedicationDosage)
                            {
                                medication.Id = Guid.NewGuid();
                                medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                                medication.MedicationType = new MedicationType { Value = "C", Text = MedicationTypeEnum.C.GetDescription() };
                                medication.MedicationCategory = exisitingMedication.MedicationCategory;
                                existingList.Add(medication);

                                exisitingMedication.MedicationCategory = "DC";
                                exisitingMedication.DCDate = DateTime.Now;
                                medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                                logAction = LogAction.MedicationUpdatedWithDischarge;
                            }
                            else
                            {
                                exisitingMedication.IsLongStanding = medication.IsLongStanding;
                                exisitingMedication.MedicationType = medication.MedicationType;
                                exisitingMedication.Classification = medication.Classification;
                                if (exisitingMedication.MedicationCategory == "DC")
                                {
                                    exisitingMedication.DCDate = medication.DCDate;
                                    logAction = LogAction.MedicationDischarged;
                                }
                                medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                            }

                            if (patientRepository.UpdateMedication(medicationProfile))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateMedicationStatus(Guid medicationProfileId, Guid medicationId, string medicationCategory, DateTime dischargeDate)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingMedications = medicationProfile.Medication.ToObject<List<Medication>>();
                var medication = existingMedications != null ? existingMedications.SingleOrDefault(m => m.Id == medicationId) : null;
                if (medication != null)
                {
                    var logAction = new LogAction();
                    if (medicationCategory.IsEqual(MedicationCategoryEnum.DC.ToString()))
                    {
                        medication.DCDate = dischargeDate;
                        medication.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                        logAction = LogAction.MedicationDischarged;
                    }
                    else
                    {
                        medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                        logAction = LogAction.MedicationActivated;
                    }
                    medication.LastChangedDate = DateTime.Now;
                    medicationProfile.Medication = existingMedications.ToXml<List<Medication>>();
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool DeleteMedication(Guid medicationProfileId, Guid medicationId)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == medicationId))
                {
                    existingList.RemoveAll(m => m.Id == medicationId);
                    medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                }
                if (patientRepository.UpdateMedication(medicationProfile))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, LogAction.MedicationDeleted, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool SignMedicationHistory(MedicationProfileHistory medicationProfileHistory)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileHistory.ProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                medicationProfileHistory.Id = Guid.NewGuid();
                medicationProfileHistory.UserId = Current.UserId;
                medicationProfileHistory.AgencyId = Current.AgencyId;
                medicationProfileHistory.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                medicationProfileHistory.Created = DateTime.Now;
                medicationProfileHistory.Modified = DateTime.Now;
                medicationProfileHistory.Medication = medicationProfile.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == "Active").ToList().ToXml();
                if (medicationProfileHistory.PharmacyPhoneArray != null && medicationProfileHistory.PharmacyPhoneArray.Count == 3)
                {
                    medicationProfileHistory.PharmacyPhone = medicationProfileHistory.PharmacyPhoneArray.ToArray().PhoneEncode();
                }
                var physician = physicianRepository.Get(medicationProfileHistory.PhysicianId, Current.AgencyId);
                if (physician != null)
                {
                    medicationProfileHistory.PhysicianData = physician.ToXml();
                }
                if (patientRepository.AddNewMedicationHistory(medicationProfileHistory))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, medicationProfile.PatientId, medicationProfile.Id.ToString(), LogType.MedicationProfileHistory, LogAction.MedicationProfileSigned, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId)
        {
            var medProfile = patientRepository.GetMedicationHistoryForPatient(patientId, Current.AgencyId);
            if (medProfile != null)
            {
                medProfile.ForEach(m =>
                {
                    if (m.UserId != Guid.Empty)
                    {
                        m.UserName = UserEngine.GetName(m.UserId, Current.AgencyId);
                    }
                }
                );
            }
            return medProfile.OrderByDescending(m => m.SignedDate.ToShortDateString().ToOrderedDate()).ToList();
        }

        public List<Medication> GetCurrentMedicationsByCategory(Guid patientId, string medicationCategory)
        {
            var medicationHistory = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            var medicationList = new List<Medication>();
            if (medicationHistory != null)
            {
                medicationList = medicationHistory.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == medicationCategory).ToList();
            }
            return medicationList;
        }

        public AllergyProfileViewData GetAllergyProfilePrint(Guid Id) {
            var profile = new AllergyProfileViewData();
            var allergies = patientRepository.GetAllergyProfileByPatient(Id, Current.AgencyId);
            if (allergies != null) profile.AllergyProfile = allergies;
            var patient = patientRepository.Get(Id, Current.AgencyId);
            if (patient != null) profile.Patient = patient;
            var agency = agencyRepository.GetWithBranches(Current.AgencyId);
            if (agency != null) profile.Agency = agency;
            return profile;
        }

        public MedicationProfileSnapshotViewData GetMedicationProfilePrint(Guid patientId)
        {
            var med = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            var profile = new MedicationProfileSnapshotViewData();
            if (med != null)
            {
                profile.MedicationProfile = med;
                profile.Patient = patientRepository.Get(patientId, Current.AgencyId);
                profile.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                profile.Allergies = GetAllergies(patientId);
                if (profile.Patient != null)
                {
                    if (profile.Patient.PhysicianContacts != null && profile.Patient.PhysicianContacts.Count > 0)
                    {
                        var physician = profile.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physician != null)
                        {
                            profile.PhysicianName = physician.DisplayName;
                        }
                    }
                    if (med.PharmacyName.IsNotNullOrEmpty()) profile.PharmacyName = med.PharmacyName;
                    else profile.PharmacyName = profile.Patient.PharmacyName;
                    if (med.PharmacyPhone.IsNotNullOrEmpty()) profile.PharmacyPhone = med.PharmacyPhone;
                    else profile.PharmacyPhone = profile.Patient.PharmacyPhone;
                    var currentEpisode = patientRepository.GetCurrentEpisode(Current.AgencyId, patientId);
                    if (currentEpisode != null)
                    {
                        profile.EpisodeId = currentEpisode.Id;
                        profile.StartDate = currentEpisode.StartDate;
                        profile.EndDate = currentEpisode.EndDate;
                        if (!currentEpisode.Id.IsEmpty() && !patientId.IsEmpty())
                        {
                            var assessment = assessmentService.GetEpisodeAssessment(currentEpisode.Id, patientId);
                            if (assessment != null)
                            {
                                var questions = assessment.ToDictionary();
                                if (questions != null)
                                {
                                    if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                                    {
                                        profile.PrimaryDiagnosis = questions["M1020PrimaryDiagnosis"].Answer;
                                    }
                                    if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                                    {
                                        profile.SecondaryDiagnosis = questions["M1022PrimaryDiagnosis1"].Answer;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return profile;
        }

        public DrugDrugInteractionsViewData GetDrugDrugInteractionsPrint(Guid patientId, List<string> drugsSelected)
        {
            var viewData = new DrugDrugInteractionsViewData();
            if (drugsSelected != null && drugsSelected.Count > 0)
            {
                viewData.DrugDrugInteractions = drugService.GetDrugDrugInteractions(drugsSelected);
            }
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);

            return viewData;
        }

        public MedicationProfileSnapshotViewData GetMedicationSnapshotPrint(Guid Id)
        {
            var viewData = new MedicationProfileSnapshotViewData();
            var snapShot = patientRepository.GetMedicationProfileHistory(Id, Current.AgencyId);
            if (snapShot != null)
            {
                viewData.MedicationProfile = snapShot.ToProfile();
                viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                viewData.Patient = patientRepository.Get(snapShot.PatientId, Current.AgencyId);
                viewData.Allergies = snapShot.Allergies;
                viewData.PrimaryDiagnosis = snapShot.PrimaryDiagnosis;
                viewData.SecondaryDiagnosis = snapShot.SecondaryDiagnosis;
                AgencyPhysician physician = null;
                if (snapShot.PhysicianData.IsNotNullOrEmpty())
                {
                    physician = snapShot.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        viewData.PhysicianName = physician.DisplayName;
                    }
                    else
                    {
                        if (!snapShot.PhysicianId.IsEmpty())
                        {
                            physician = PhysicianEngine.Get(snapShot.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                viewData.PhysicianName = physician.DisplayName;
                            }
                        }
                    }
                }
                else
                {
                    if (!snapShot.PhysicianId.IsEmpty())
                    {
                        physician = PhysicianEngine.Get(snapShot.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            viewData.PhysicianName = physician.DisplayName;
                        }
                    }
                }

                if (snapShot.SignatureText.IsNotNullOrEmpty())
                {
                    viewData.SignatureText = snapShot.SignatureText;
                    viewData.SignatureDate = snapShot.SignedDate;
                }
                else
                {
                    if (!snapShot.UserId.IsEmpty())
                    {
                        var displayName = UserEngine.GetName(snapShot.UserId, Current.AgencyId);
                        if (displayName.IsNotNullOrEmpty())
                        {
                            viewData.SignatureText = string.Format("Electronically Signed by: {0}", displayName);
                            viewData.SignatureDate = snapShot.SignedDate;
                        }
                    }
                }
                if (viewData.Patient != null)
                {
                    if (snapShot.PharmacyName.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyName = snapShot.PharmacyName;
                    }
                    else
                    {
                        viewData.PharmacyName = viewData.Patient.PharmacyName;
                    }

                    if (snapShot.PharmacyPhone.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyPhone = snapShot.PharmacyPhone;
                    }
                    else
                    {
                        viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                    }

                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, snapShot.EpisodeId, snapShot.PatientId);
                    if (episode != null)
                    {
                        viewData.EpisodeId = episode.Id;
                        viewData.StartDate = episode.StartDate;
                        viewData.EndDate = episode.EndDate;
                    }
                }
            }
            return viewData;
        }

        public bool LinkPhysicians(Patient patient)
        {
            var result = true;
            if (patient != null && patient.AgencyPhysicians.Any())
            {
                int i = 0;
                bool isPrimary = false;
                foreach (var agencyPhysicianId in patient.AgencyPhysicians)
                {
                    if (i == 0)
                    {
                        isPrimary = true;
                    }
                    else
                    {
                        isPrimary = false;
                    }
                    if (!agencyPhysicianId.IsEmpty() && !physicianRepository.Link(patient.Id, agencyPhysicianId, isPrimary))
                    {
                        result = false;
                        break;
                    }
                    i++;
                }
                if (i > 0 && result)
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PhysicianLinked, "One of the physician is set as primary.");
                }
            }
            return result;
        }

        public bool LinkPhysician(Guid patientId, Guid physicianId, bool isPrimary)
        {
            if (physicianRepository.Link(patientId, physicianId, isPrimary))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PhysicianLinked, isPrimary ? "Primary" : string.Empty);
                return true;
            }
            return false;
        }

        public bool UnlinkPhysician(Guid patientId, Guid physicianId)
        {
            if (patientRepository.DeletePhysicianContact(physicianId, patientId))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PhysicianUnLinked, string.Empty);
                return true;
            }
            return false;
        }

        public void AddPhysicianOrderUserAndScheduleEvent(PhysicianOrder physicianOrder, out PhysicianOrder physicianOrderOut)
        {
            var newScheduleEvent = new ScheduleEvent
            {
                EventId = physicianOrder.Id,
                UserId = physicianOrder.UserId,
                PatientId = physicianOrder.PatientId,
                EpisodeId = physicianOrder.EpisodeId,
                Status = physicianOrder.Status.ToString(),
                Discipline = Disciplines.Nursing.ToString(),
                EventDate = physicianOrder.OrderDate.ToShortDateString(),
                VisitDate = physicianOrder.OrderDate.ToShortDateString(),
                DisciplineTask = (int)DisciplineTasks.PhysicianOrder,
                IsOrderForNextEpisode = physicianOrder.IsOrderForNextEpisode
            };
            if (patientRepository.UpdateEpisode(Current.AgencyId, physicianOrder.EpisodeId, physicianOrder.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
            {
                Auditor.Log(newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.PhysicianOrder);
            }

            physicianOrder.EpisodeId = newScheduleEvent.EpisodeId;
            physicianOrderOut = physicianOrder;
        }

        public bool ProcessPhysicianOrder(Guid episodeId, Guid patientId, Guid eventId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;
            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            PhysicianOrder physicianOrder = null;

            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                }
                var description = string.Empty;
                physicianOrder = patientRepository.GetOrder(eventId, patientId, Current.AgencyId);
                if (physicianOrder != null)
                {
                    if (actionType == "Approve")
                    {
                        physicianOrder.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician);
                        physicianOrder.Modified = DateTime.Now;
                        if (patientRepository.UpdateOrder(physicianOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician).ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician).ToString();
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                                description = "Approved By:" + Current.UserFullName;
                            }
                        }
                    }
                    else if (actionType == "Return")
                    {
                        physicianOrder.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                        physicianOrder.Modified = DateTime.Now;
                        if (patientRepository.UpdateOrder(physicianOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                                description = "Returned By: " + Current.UserFullName;
                            }
                        }
                    }
                    else if (actionType == "Print")
                    {
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                    }

                    if (shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(Current.AgencyId, userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)physicianOrder.Status, DisciplineTasks.PhysicianOrder, description);
                        }
                    }
                }
            }

            return result;
        }

        public List<Order> GetOrdersFromSchduleEvents( Guid patientId , DateTime startDate, DateTime endDate ,List<ScheduleEvent> schedules )
        {
            var orders = new List<Order>();
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPatientPhysicianOrders(Current.AgencyId, patientId, physicianOrdersIds, startDate, endDate);
                    physicianOrders.ForEach(po =>
                    {
                        orders.Add(new Order
                        {
                            Id = po.Id,
                            PatientId = po.PatientId,
                            EpisodeId = po.EpisodeId,
                            Type = OrderType.PhysicianOrder,
                            Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                            Number = po.OrderNumber,
                            PhysicianName = po.PhysicianName,
                            PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true) + string.Format(" | <a href=\"javaScript:void(0);\" onclick=\"Patient.DeleteOrder('{0}','{1}','{2}');\">Delete</a>", po.Id, po.PatientId,po.EpisodeId),
                            CreatedDate = po.OrderDateFormatted.ToZeroFilled(),
                            ReceivedDate = po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate,
                            SendDate = po.SentDate,
                            Status = po.Status
                        });
                    });
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPatientPlanofCares(Current.AgencyId, patientId, planofCareOrdersIds);
                    planofCareOrders.ForEach(poc =>
                    {
                        var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                        if (evnt != null)
                        {
                            var physician = poc.PhysicianData.IsNotNullOrEmpty() ? poc.PhysicianData.ToObject<AgencyPhysician>() : PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = poc.Id,
                                PatientId = poc.PatientId,
                                EpisodeId = poc.EpisodeId,
                                Type = OrderType.HCFA485,
                                Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
                                Number = poc.OrderNumber,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                SendDate = poc.SentDate,
                                Status = poc.Status
                            });
                        }
                    });
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPatientPlanofCaresStandAlones(Current.AgencyId, patientId, planofCareStandAloneOrdersIds);
                    planofCareStandAloneOrders.ForEach(poc =>
                    {
                        var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                        if (evnt != null)
                        {
                            var physician = poc.PhysicianData.IsNotNullOrEmpty() ? poc.PhysicianData.ToObject<AgencyPhysician>() : PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = poc.Id,
                                PatientId = poc.PatientId,
                                EpisodeId = poc.EpisodeId,
                                Type = OrderType.HCFA485StandAlone,
                                Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                Number = poc.OrderNumber,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
                                CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                                Status = poc.Status
                            });
                        }
                    });
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetPatientFaceToFaceEncounterOrders(Current.AgencyId, patientId, faceToFaceEncounterOrdersIds);
                    faceToFaceEncounters.ForEach(ffe =>
                    {
                        var evnt = faceToFaceEncounterSchedules.SingleOrDefault(s => s.EventId == ffe.Id);
                        if (evnt != null)
                        {
                            var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = ffe.Id,
                                PatientId = ffe.PatientId,
                                EpisodeId = ffe.EpisodeId,
                                Type = OrderType.FaceToFaceEncounter,
                                Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                Number = ffe.OrderNumber,
                                PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate,
                                SendDate = ffe.RequestDate,
                                Status = ffe.Status
                            });
                        }
                    });
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public List<Order> GetPatientOrders(Guid patientId, DateTime startDate, DateTime endDate)
        {
            return GetOrdersFromSchduleEvents(patientId,startDate,endDate, patientRepository.GetPatientOrderScheduleEvents(Current.AgencyId, patientId, startDate, endDate));
        }

        public List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId)
        {
            var orders = new List<Order>();
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var schedules = new List<ScheduleEvent>();

                if (episode.Schedule.IsNotNullOrEmpty())
                {
                    var currentSchedules = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && (s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone || s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485 || s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder) && !s.IsOrderForNextEpisode).ToList();
                    if (currentSchedules != null && currentSchedules.Count > 0)
                    {
                        schedules.AddRange(currentSchedules);
                    }
                    var previousEpisode = patientRepository.GetPreviousEpisodFluent(Current.AgencyId, patientId, episode.StartDate);
                    if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var previousSchedules = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= previousEpisode.StartDate.Date && s.EventDate.ToDateTime().Date <= previousEpisode.EndDate.Date && (s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter || s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone || s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485 || s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder) && s.IsOrderForNextEpisode).ToList();
                        if (previousSchedules != null && previousSchedules.Count > 0)
                        {
                            schedules.AddRange(previousSchedules);
                        }
                    }
                }
                orders = GetOrdersFromSchduleEvents(episode.PatientId,episode.StartDate,episode.EndDate, schedules);
            }
            return orders;
        }

        public bool DeletePhysicianOrder(Guid orderId, Guid patientId ,Guid episodeId)
        {
            bool result = false;
            try
            {
                var physicianOrder = patientRepository.GetOrderOnly(orderId,Current.AgencyId);
                if (physicianOrder != null && physicianOrder.EpisodeId==episodeId && physicianOrder.PatientId==patientId)
                {
                    if (patientRepository.MarkOrderAsDeleted(orderId, patientId, Current.AgencyId, true))
                    {
                        if (!physicianOrder.EpisodeId.IsEmpty())
                        {
                            var scheduleEvent = patientRepository.GetScheduleOnly(Current.AgencyId, physicianOrder.EpisodeId, patientId, orderId);
                            if (scheduleEvent != null)
                            {
                                if (patientRepository.DeleteScheduleEvent(Current.AgencyId, physicianOrder.EpisodeId, patientId, orderId, (int)DisciplineTasks.PhysicianOrder))
                                {
                                    if (!scheduleEvent.UserId.IsEmpty())
                                    {
                                        userRepository.RemoveScheduleEvent(Current.AgencyId, patientId, orderId, scheduleEvent.UserId);
                                    }
                                    Auditor.Log(physicianOrder.EpisodeId, physicianOrder.PatientId, physicianOrder.Id, Actions.Deleted, DisciplineTasks.PhysicianOrder);
                                }
                            }
                        }
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
            return result;
        }

        public PhysicianOrder GetOrderPrint()
        {
            var order = new PhysicianOrder();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return order;
        }

        public PhysicianOrder GetOrderPrint(Guid patientId, Guid orderId)
        {
            var order = patientRepository.GetOrder(orderId, patientId, Current.AgencyId);
            if (order == null) order = new PhysicianOrder();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            order.Patient = patientRepository.Get(order.PatientId, Current.AgencyId);
            if ((order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || order.Status == (int)ScheduleStatus.OrderSentToPhysician || order.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !order.PhysicianId.IsEmpty() && order.PhysicianData.IsNotNullOrEmpty())
            {
                var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                if (physician != null)
                {
                    order.Physician = physician;
                }
                else
                {
                    order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                }
            }
            else
            {
                order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
            }

            order.Allergies = GetAllergies(patientId);
            var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, order.EpisodeId, order.PatientId);
            if (episode != null)
            {
                if (order.IsOrderForNextEpisode)
                {
                    order.EpisodeStartDate = episode.EndDate.AddDays(1).ToShortDateString();
                    order.EpisodeEndDate = episode.EndDate.AddDays(60).ToShortDateString();
                }
                else
                {
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                }
                if (order.Patient != null)
                {
                    order.Patient.StartofCareDate = episode.StartOfCareDate;
                }
            }
            return order;
        }

        public void AddCommunicationNoteUserAndScheduleEvent(CommunicationNote communicationNote, out CommunicationNote communicationNoteOut)
        {
            var newScheduleEvent = new ScheduleEvent
            {
                EventId = communicationNote.Id,
                UserId = communicationNote.UserId,
                EpisodeId = communicationNote.EpisodeId,
                PatientId = communicationNote.PatientId,
                Status = communicationNote.Status.ToString(),
                Discipline = Disciplines.ReportsAndNotes.ToString(),
                EventDate = communicationNote.Created.ToShortDateString(),
                VisitDate = communicationNote.Created.ToShortDateString(),
                DisciplineTask = (int)DisciplineTasks.CommunicationNote
            };
            if (patientRepository.UpdateEpisode(Current.AgencyId, communicationNote.EpisodeId, communicationNote.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
            {
                Auditor.Log(newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.CommunicationNote);
            }
            communicationNote.EpisodeId = newScheduleEvent.EpisodeId;
            communicationNoteOut = communicationNote;
        }

        public bool ProcessCommunicationNotes(string button, Guid patientId, Guid eventId, string reason)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;
            UserEvent userEvent = null;
            ScheduleEvent scheduleEvent = null;

            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var communicationNote = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                if (communicationNote != null)
                {
                    if (!communicationNote.EpisodeId.IsEmpty())
                    {
                        scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, communicationNote.EpisodeId, patientId, eventId);
                        if (scheduleEvent != null)
                        {
                            userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                        }
                    }
                    var description = string.Empty;
                    var action = new Actions();
                    if (button == "Approve")
                    {
                        communicationNote.Status = ((int)ScheduleStatus.NoteCompleted);
                        communicationNote.Modified = DateTime.Now;

                        if (patientRepository.UpdateCommunicationNoteModal(communicationNote))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = ((int)ScheduleStatus.NoteCompleted).ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.NoteCompleted).ToString();
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                                description = "Approved By:" + Current.UserFullName;
                                action = Actions.Approved;
                            }
                        }
                    }
                    else if (button == "Return")
                    {
                        communicationNote.Status = ((int)ScheduleStatus.NoteReturned);
                        communicationNote.Modified = DateTime.Now;
                        communicationNote.SignatureText = string.Empty;
                        communicationNote.SignatureDate = DateTime.MinValue;
                        if (patientRepository.UpdateCommunicationNoteModal(communicationNote))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                                description = "Returned By:" + Current.UserFullName;
                                action = Actions.Returned;
                            }
                        }
                    }
                    else if (button == "Print")
                    {
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                    }

                    if (scheduleEvent != null && shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(Current.AgencyId, userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, DisciplineTasks.CommunicationNote, description);
                        }
                    }
                }
            }
            return result;
        }

        public List<CommunicationNote> GetCommunicationNotes(Guid branchId, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var commNotes = new List<CommunicationNote>();
            var schedules = patientRepository.GetCommunicationNoteScheduleEvents(Current.AgencyId,branchId,patientStatus, startDate, endDate);
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersIds = schedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                commNotes = patientRepository.GetCommunicationNoteByIds(Current.AgencyId, physicianOrdersIds);
            }
            return commNotes;
        }

        public List<CommunicationNote> GetCommunicationNotes(Guid patientId)
        {
            var commNotes = patientRepository.GetCommunicationNotes(Current.AgencyId, patientId);
            if (commNotes != null)
            {
                commNotes.ForEach(c =>
                {
                    if (!c.UserId.IsEmpty())
                    {
                        var userName = UserEngine.GetName(c.UserId, Current.AgencyId);
                        if (userName.IsNotNullOrEmpty())
                        {
                            c.UserDisplayName = userName;
                        }
                    }
                    var physician = PhysicianEngine.Get(c.PhysicianId, Current.AgencyId);
                    c.PhysicianName = physician != null ? physician.DisplayName : string.Empty;
                    var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, c.EpisodeId, c.PatientId, c.Id);
                    if (scheduleEvent != null)
                    {
                        c.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return commNotes;
        }

        public bool DeleteCommunicationNote(Guid Id, Guid patientId)
        {
            var communicationNote = patientRepository.GetCommunicationNote(Id, patientId, Current.AgencyId);
            bool result = false;
            if (communicationNote != null)
            {
                if (!communicationNote.EpisodeId.IsEmpty())
                {
                    DeleteSchedule(communicationNote.EpisodeId, communicationNote.PatientId, communicationNote.Id, communicationNote.UserId, (int)DisciplineTasks.CommunicationNote);
                    result = patientRepository.DeleteCommunicationNote(Current.AgencyId, Id, patientId, true);
                }
                else
                {
                    result = patientRepository.DeleteCommunicationNote(Current.AgencyId, Id, patientId, true);
                }
            }
            return result;
        }

        public CommunicationNote GetCommunicationNotePrint(Guid eventId, Guid patientId)
        {
            var note = new CommunicationNote();
            if (!patientId.IsEmpty() && !eventId.IsEmpty())
            {
                note = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                if (note != null)
                {
                    if ((note.Status == (int)ScheduleStatus.NoteCompleted || note.Status == (int)ScheduleStatus.NoteSubmittedWithSignature) && !note.PhysicianId.IsEmpty() && note.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = note.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            note.Physician = physician;
                        }
                        else
                        {
                            note.Physician = PhysicianEngine.Get(note.PhysicianId, Current.AgencyId);
                        }
                    }
                    else
                    {
                        note.Physician = PhysicianEngine.Get(note.PhysicianId, Current.AgencyId);
                    }

                    note.Patient = patientRepository.Get(patientId, Current.AgencyId);
                    note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                }
            }
            return note;
        }

        public bool CreateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter)
        {
            var result = false;
            try
            {
                faceToFaceEncounter.UserId = Current.UserId;
                faceToFaceEncounter.AgencyId = Current.AgencyId;
                faceToFaceEncounter.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                faceToFaceEncounter.OrderNumber = patientRepository.GetNextOrderNumber();
                faceToFaceEncounter.Modified = DateTime.Now;
                faceToFaceEncounter.Created = DateTime.Now;
                if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        faceToFaceEncounter.PhysicianData = physician.ToXml();
                    }
                }
                var newScheduleEvent = new ScheduleEvent
                {
                    EventId = faceToFaceEncounter.Id,
                    UserId = faceToFaceEncounter.UserId,
                    PatientId = faceToFaceEncounter.PatientId,
                    EpisodeId = faceToFaceEncounter.EpisodeId,
                    Status = faceToFaceEncounter.Status.ToString(),
                    Discipline = Disciplines.Orders.ToString(),
                    EventDate = faceToFaceEncounter.RequestDate.ToShortDateString(),
                    VisitDate = faceToFaceEncounter.RequestDate.ToShortDateString(),
                    DisciplineTask = (int)DisciplineTasks.FaceToFaceEncounter
                };
                if (patientRepository.UpdateEpisode(Current.AgencyId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
                {
                    Auditor.Log(newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.FaceToFaceEncounter);
                    return patientRepository.AddFaceToFaceEncounter(faceToFaceEncounter);
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
            return result;
        }

        public FaceToFaceEncounter GetFaceToFacePrint()
        {
            var order = new FaceToFaceEncounter();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return order;
        }

        public FaceToFaceEncounter GetFaceToFacePrint(Guid patientId, Guid orderId)
        {
            var order = patientRepository.GetFaceToFaceEncounter(orderId, patientId, Current.AgencyId);
            if (order != null)
            {
                order.Patient = patientRepository.Get(order.PatientId, Current.AgencyId);
                if ((order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || order.Status == (int)ScheduleStatus.OrderSentToPhysician || order.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !order.PhysicianId.IsEmpty() && order.PhysicianData.IsNotNullOrEmpty())
                {
                    var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        order.Physician = physician;
                    }
                    else
                    {
                        order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                    }
                }
                else
                {
                    order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                }
                var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, order.EpisodeId, order.PatientId);
                if (episode != null)
                {
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                    if (order.Patient != null)
                    {
                        order.Patient.StartofCareDate = episode.StartOfCareDate;
                    }
                }
            }
            else order = new FaceToFaceEncounter();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return order;
        }

        public PatientEpisode CreateEpisode(Guid patientId, DateTime startDate, ScheduleEvent scheduleEvent)
        {
            var patientEpisode = new PatientEpisode();
            patientEpisode.Id = Guid.NewGuid();
            patientEpisode.AgencyId = Current.AgencyId;
            patientEpisode.PatientId = patientId;
            patientEpisode.StartDate = startDate;
            patientEpisode.EndDate = startDate.AddDays(59);
            patientEpisode.IsActive = true;
            var events = new List<ScheduleEvent>();
            if (scheduleEvent != null && !scheduleEvent.EventId.IsEmpty())
            {
                scheduleEvent.EpisodeId = patientEpisode.Id;
                events.Add(scheduleEvent);
            }
            patientEpisode.Schedule = events.ToXml();
            return patientEpisode;
        }

        public PatientEpisode CreateEpisode(Guid patientId, PatientEpisode episode, ScheduleEvent scheduleEvent)
        {
            var patientEpisode = new PatientEpisode();
            patientEpisode.Id = Guid.NewGuid();
            patientEpisode.AgencyId = Current.AgencyId;
            patientEpisode.PatientId = patientId;
            patientEpisode.StartDate = episode.StartDate;
            patientEpisode.EndDate = episode.EndDate;
            patientEpisode.IsActive = true;
            patientEpisode.Detail = episode.Detail;
            patientEpisode.StartOfCareDate = episode.StartOfCareDate;
            patientEpisode.Details = patientEpisode.Detail.ToXml();
            var events = new List<ScheduleEvent>();
            if (scheduleEvent != null && !scheduleEvent.EventId.IsEmpty())
            {
                scheduleEvent.EpisodeId = patientEpisode.Id;
                events.Add(scheduleEvent);
            }
            patientEpisode.Schedule = events.ToXml();
            return patientEpisode;
        }

        public bool AddEpisode(PatientEpisode patientEpisode)
        {
            bool result = false;
            if (patientRepository.AddEpisode(patientEpisode))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientEpisode.PatientId, patientEpisode.Id.ToString(), LogType.Episode, LogAction.EpisodeAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public bool UpdateEpisode(PatientEpisode episode)
        {
            var result = false;
            episode.Details = episode.Detail.ToXml();
            var episodeToEdit = patientRepository.GetEpisodeById(Current.AgencyId, episode.Id, episode.PatientId);
            if (episodeToEdit != null)
            {
                var logAction = episodeToEdit.IsActive == episode.IsActive ? LogAction.EpisodeEdited : (!episode.IsActive ? LogAction.EpisodeDeactivated : LogAction.EpisodeActivated);
                episodeToEdit.IsActive = episode.IsActive;
                episodeToEdit.StartDate = episode.StartDate;
                episodeToEdit.EndDate = episode.EndDate;
                episodeToEdit.Detail = episodeToEdit.Details.ToObject<EpisodeDetail>();
                episode.Detail.Assets.AddRange(episodeToEdit.Detail.Assets);
                episodeToEdit.StartOfCareDate = episode.StartOfCareDate;
                episodeToEdit.Details = episode.Detail.ToXml();
                episodeToEdit.AdmissionId = episode.AdmissionId;
                if (patientRepository.UpdateEpisode(Current.AgencyId, episodeToEdit))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, episodeToEdit.PatientId, episodeToEdit.Id.ToString(), LogType.Episode, logAction, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool IsValidEpisode(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var episodes = patientRepository.GetPatientAllEpisodes(Current.AgencyId, patientId);
            bool result = true;
            if (episodes != null)
            {
                foreach (var e in episodes)
                {
                    if ((startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date) || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date))
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        public bool IsValidEpisode(Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var episodes = patientRepository.GetPatientAllEpisodes(Current.AgencyId, patientId);
            bool result = true;
            if (episodes != null)
            {
                foreach (var e in episodes)
                {
                    if (e.Id == episodeId)
                    {
                        continue;
                    }
                    else
                    {
                        if ((startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date) || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date))
                        {
                            result = false;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateEpisodeForDischarge(Guid patientId, DateTime dischargeDate, PatientEpisode episode)
        {
            var result = false;
            try
            {
                var patientEpisodes = new List<PatientEpisode>();

                if (episode != null)
                {
                    episode.EndDate = dischargeDate;
                    episode.IsLinkedToDischarge = true;
                    episode.Modified = DateTime.Now;
                    patientEpisodes.Add(episode);
                }
                var episodes = patientRepository.EpisodesToDischarge(Current.AgencyId, patientId, dischargeDate);//  database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.StartDate.Date >= dischargeDate.Date);
                if (episodes != null && episodes.Count > 0)
                {
                    episodes.ForEach(es =>
                    {
                        es.Modified = DateTime.Now;
                        es.IsDischarged = true;
                    });
                    patientEpisodes.AddRange(episodes);
                }
                if (patientEpisodes.Count > 0)
                {
                    if (patientRepository.UpdateEpisodeForDischarge(patientEpisodes))
                    {
                        if (episode != null)
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, episode.PatientId, episode.Id.ToString(), LogType.Episode, LogAction.EpisodeEdited, "Updated for patient discharge");
                        }
                        if (episodes != null && episodes.Count > 0)
                        {
                            episodes.ForEach(e =>
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, episode.PatientId, episode.Id.ToString(), LogType.Episode, LogAction.EpisodeDeactivatedForDischarge, string.Empty);
                            });
                        }
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool CreateEpisodeAndClaims(Patient patient)
        {
            bool result = false;
            var patientToEdit = patientRepository.Get(patient.Id, Current.AgencyId);
            if (patientToEdit != null)
            {
                if (patient.StartofCareDate.Date == patient.EpisodeStartDate.Date)
                {
                    var newEvent = new ScheduleEvent
                    {
                        EventId = Guid.NewGuid(),
                        PatientId = patient.Id,
                        UserId = patient.UserId,
                        Discipline = Disciplines.Nursing.ToString(),
                        Status = ((int)ScheduleStatus.OasisNotYetDue).ToString(),
                        DisciplineTask = (int)DisciplineTasks.OASISCStartofCare,
                        EventDate = patient.EpisodeStartDate.ToShortDateString(),
                        VisitDate = patient.EpisodeStartDate.ToShortDateString(),
                        IsBillable = true
                    };
                    var episode = this.CreateEpisode(patient.Id, patient.EpisodeStartDate, newEvent);
                    episode.StartOfCareDate = patient.StartofCareDate;
                    episode.AdmissionId = patientToEdit.AdmissionId;
                    if (this.AddEpisode(episode))
                    {
                        newEvent.EpisodeId = episode.Id;
                        this.ProcessSchedule(newEvent, patientToEdit, episode);

                        if (patient.UserId != Guid.Empty)
                        {
                            userRepository.AddUserEvent(Current.AgencyId, patient.Id, patient.UserId,
                                new UserEvent
                                {
                                    EventId = newEvent.EventId,
                                    PatientId = patient.Id,
                                    UserId = patient.UserId,
                                    EpisodeId = episode.Id,
                                    DisciplineTask = (int)DisciplineTasks.OASISCStartofCare,
                                    Status = ((int)ScheduleStatus.OasisNotYetDue).ToString(),
                                    EventDate = patient.EpisodeStartDate.ToShortDateString()
                                });
                        }
                        if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() && patient.PrimaryInsurance.ToInteger() > 0 && patient.PrimaryInsurance.ToInteger() < 1000)
                        {
                            var rap = this.CreateRap(patientToEdit, episode);
                            var final = this.CreateFinal(patientToEdit, episode);
                            if (rap != null)
                            {
                                if (billingRepository.AddRap(rap))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                                }
                            }
                            if (final != null)
                            {
                                if (billingRepository.AddFinal(final))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                                }
                            }
                        }

                        if (patient.IsFaceToFaceEncounterCreated)
                        {
                            if (!patientToEdit.Id.IsEmpty() && patientToEdit.PhysicianContacts.Count > 0 )
                            {
                                var physician = patientToEdit.PhysicianContacts.Where(p => p.Primary = true).FirstOrDefault();
                                if (physician != null && !physician.Id.IsEmpty())
                                {
                                    var faceToFaceEncounter = new FaceToFaceEncounter { PatientId = patient.Id, EpisodeId = episode.Id, UserId = patient.UserId, PhysicianId = physician.Id, Id = Guid.NewGuid(), RequestDate = DateTime.Now };
                                    this.CreateFaceToFaceEncounter(faceToFaceEncounter);
                                }
                            }
                        }
                        if (Enum.IsDefined(typeof(DisciplineTasks), newEvent.DisciplineTask))
                        {
                            Auditor.Log(newEvent.EpisodeId, newEvent.PatientId, newEvent.EventId, Actions.Add, (DisciplineTasks)newEvent.DisciplineTask);
                        }
                        result = true;
                    }
                }
                else if (patient.EpisodeStartDate.Date > patient.StartofCareDate.Date)
                {
                    var episode = this.CreateEpisode(patient.Id, patient.EpisodeStartDate, null);
                    episode.StartOfCareDate = patient.StartofCareDate;
                    episode.AdmissionId = patientToEdit.AdmissionId;
                    if (this.AddEpisode(episode))
                    {
                        if (patient.IsFaceToFaceEncounterCreated)
                        {
                            if (!patientToEdit.Id.IsEmpty() && patientToEdit.PhysicianContacts.Count > 0)
                            {
                                var physician = patientToEdit.PhysicianContacts.Where(p => p.Primary = true).FirstOrDefault();
                                if (physician != null && !physician.Id.IsEmpty())
                                {
                                    var faceToFaceEncounter = new FaceToFaceEncounter { PatientId = patient.Id, EpisodeId = episode.Id, UserId = patient.UserId, PhysicianId = physician.Id, Id = Guid.NewGuid(), RequestDate = patient.EpisodeStartDate };
                                    this.CreateFaceToFaceEncounter(faceToFaceEncounter);
                                }
                            }
                        }
                        result = true;
                    }
                }
            }

            return result;
        }

        public void DeleteEpisodeAndClaims(Patient patient)
        {
            PatientEpisode episodeDeleted = null;
            patientRepository.DeleteEpisode(Current.AgencyId, patient, out episodeDeleted);
            if (episodeDeleted != null)
            {
                billingRepository.DeleteRap(Current.AgencyId, patient.Id, episodeDeleted.Id);
                billingRepository.DeleteFinal(Current.AgencyId, patient.Id, episodeDeleted.Id);
            }
        }

        public bool AddMultiDaySchedule(Guid episodeId, Guid patientId, Guid userId, int disciplineTaskId, string visitDates)
        {
            bool result = false;
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if ( episode != null)
            {
                 var patient = patientRepository.Get(patientId, Current.AgencyId);
                if (!episode.AdmissionId.IsEmpty())
                {
                    var admissinData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                    if (admissinData != null && admissinData.PatientData.IsNotNullOrEmpty())
                    {
                        patient = admissinData.PatientData.ToObject<Patient>();
                    }
                }
                if (patient != null)
                {
                    episode.StartOfCareDate = patient.StartofCareDate;
                    var scheduledEvents = new List<ScheduleEvent>();
                    var disciplineTask = lookupRepository.GetDisciplineTask(disciplineTaskId);
                    var visitDateArray = visitDates.Split(',');
                    if (visitDateArray != null && visitDateArray.Length>0)
                    {
                        visitDateArray.ForEach(date =>
                        {
                            if (date.IsDate())
                            {
                                var newScheduledEvent = new ScheduleEvent
                                {
                                    UserId = userId,
                                    IsDeprecated = false,
                                    EventId = Guid.NewGuid(),
                                    PatientId = patientId,
                                    EpisodeId = episodeId,
                                    EventDate = date.ToZeroFilled(),
                                    VisitDate = date.ToZeroFilled(),
                                    StartDate = episode.StartDate,
                                    EndDate = episode.EndDate,
                                    DisciplineTask = disciplineTaskId,
                                    PatientName = patient.DisplayName.ToUpperCase(),
                                    IsBillable = disciplineTask.IsBillable,
                                    Discipline = disciplineTask != null ? disciplineTask.Discipline : ""
                                };
                                scheduledEvents.Add(newScheduledEvent);
                                ProcessSchedule(newScheduledEvent, patient, episode);
                                if (Enum.IsDefined(typeof(DisciplineTasks), newScheduledEvent.DisciplineTask))
                                {
                                    Auditor.Log(newScheduledEvent.EpisodeId, newScheduledEvent.PatientId, newScheduledEvent.EventId, Actions.Add, (DisciplineTasks)newScheduledEvent.DisciplineTask);
                                }
                            }
                        });
                    }

                    if (patientRepository.UpdateEpisode(Current.AgencyId, episodeId, patientId, scheduledEvents))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateEpisode(Guid episodeId, Guid patientId, string jsonString)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(jsonString, "jsonString");

            bool result = false;
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var patient = patientRepository.Get(patientId, Current.AgencyId);
                if (!episode.AdmissionId.IsEmpty())
                {
                    var admissinData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                    if (admissinData != null && admissinData.PatientData.IsNotNullOrEmpty())
                    {
                        patient = admissinData.PatientData.ToObject<Patient>();
                    }
                }
                if (patient != null)
                {
                    episode.StartOfCareDate = patient.StartofCareDate;
                    var newEvents = JsonExtensions.FromJson<List<ScheduleEvent>>(jsonString);
                    newEvents.ForEach(ev =>
                    {
                        ev.EventId = Guid.NewGuid();
                        ev.PatientId = patientId;
                        ev.EpisodeId = episodeId;
                        ev.EventDate = ev.EventDate.ToZeroFilled();
                        ev.VisitDate = ev.EventDate.ToZeroFilled();
                        ev.StartDate = episode.StartDate;
                        ev.EndDate = episode.EndDate;
                        ev.IsDeprecated = false;
                        ProcessSchedule(ev, patient, episode);
                        if (Enum.IsDefined(typeof(DisciplineTasks), ev.DisciplineTask))
                        {
                            Auditor.Log(ev.EpisodeId, ev.PatientId, ev.EventId, Actions.Add, (DisciplineTasks)ev.DisciplineTask);
                        }
                    });
                    if (patientRepository.UpdateEpisode(Current.AgencyId, episodeId, patientId, newEvents))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateEpisode(Guid episodeId, Guid patientId, string disciplineTask, string discipline, Guid userId, bool isBillable, DateTime startDate, DateTime endDate)
        {
            bool result = false;
            int dateDifference = endDate.Subtract(startDate).Days + 1;
            var newEvents = new List<ScheduleEvent>();
            for (int i = 0; i < dateDifference; i++)
            {
                newEvents.Add(new ScheduleEvent { EventId = Guid.NewGuid(), PatientId = patientId, EpisodeId = episodeId, UserId = userId, DisciplineTask = int.Parse(disciplineTask), Discipline = discipline, EventDate = String.Format("{0:MM/dd/yyyy}", startDate.AddDays(i)), VisitDate = String.Format("{0:MM/dd/yyyy}", startDate.AddDays(i)), IsBillable = isBillable, IsDeprecated = false });
            }
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var patient = patientRepository.Get(patientId, Current.AgencyId);
                if (!episode.AdmissionId.IsEmpty())
                {
                    var admissinData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                    if (admissinData != null && admissinData.PatientData.IsNotNullOrEmpty())
                    {
                        patient = admissinData.PatientData.ToObject<Patient>();
                    }
                }
                if (patient != null)
                {
                    episode.StartOfCareDate = patient.StartofCareDate;
                    if (newEvents != null && newEvents.Count > 0)
                    {
                        newEvents.ForEach(ev =>
                        {
                            ProcessSchedule(ev, patient, episode);
                            if (Enum.IsDefined(typeof(DisciplineTasks), ev.DisciplineTask))
                            {
                                Auditor.Log(ev.EpisodeId, ev.PatientId, ev.EventId, Actions.Add, (DisciplineTasks)ev.DisciplineTask);
                            }
                        });
                    }
                    if (patientRepository.UpdateEpisode(Current.AgencyId, episodeId, patientId, newEvents))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateScheduleEvent(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;
            var schedule = patientRepository.GetSchedule(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
            if (schedule != null)
            {
                if (httpFiles.Count > 0)
                {
                    foreach (string key in httpFiles.AllKeys)
                    {
                        HttpPostedFileBase file = httpFiles.Get(key);
                        if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                        {
                            var binaryReader = new BinaryReader(file.InputStream);
                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset))
                            {
                                schedule.Assets.Add(asset.Id);
                            }
                            else
                            {
                                assetsSaved = false;
                                break;
                            }
                        }
                    }
                }
                Guid userId = schedule.UserId;
                schedule.UserId = scheduleEvent.UserId;
                schedule.Comments = scheduleEvent.Comments;
                schedule.Status = scheduleEvent.Status;
                schedule.EventDate = scheduleEvent.EventDate;

                schedule.VisitDate = scheduleEvent.VisitDate;
                schedule.IsBillable = scheduleEvent.IsBillable;
                schedule.IsDeprecated = scheduleEvent.IsDeprecated;
                schedule.Surcharge = scheduleEvent.Surcharge;
                schedule.AssociatedMileage = scheduleEvent.AssociatedMileage;
                schedule.TimeIn = scheduleEvent.TimeIn;
                schedule.TimeOut = scheduleEvent.TimeOut;
                schedule.IsMissedVisit = scheduleEvent.IsMissedVisit;
                schedule.DisciplineTask = scheduleEvent.DisciplineTask;
                schedule.PhysicianId = scheduleEvent.PhysicianId;
                if (ProcessEditDetail(schedule))
                {
                    if (assetsSaved && patientRepository.UpdateEpisode(Current.AgencyId, schedule))
                    {
                        var userEvent = userRepository.GetEvent(Current.AgencyId, schedule.UserId, schedule.PatientId, schedule.EventId);
                        if (userEvent != null)
                        {
                            userEvent.IsDeprecated = schedule.IsDeprecated;
                            userEvent.EventDate = schedule.EventDate;
                            userEvent.VisitDate = schedule.VisitDate;
                            userEvent.TimeIn = schedule.TimeIn;
                            userEvent.TimeOut = schedule.TimeOut;
                            userEvent.IsMissedVisit = schedule.IsMissedVisit;
                            userEvent.Status = schedule.Status;
                            if (userId == schedule.UserId)
                            {
                                userRepository.UpdateEvent(Current.AgencyId, userEvent);
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, schedule.PatientId, schedule.UserId, userEvent);
                                userRepository.RemoveScheduleEvent(Current.AgencyId, schedule.PatientId, schedule.EventId, schedule.UserId);
                            }
                        }
                        else
                        {
                            userRepository.AddUserEvent(Current.AgencyId, schedule.PatientId, schedule.UserId, new UserEvent { DisciplineTask = schedule.DisciplineTask, EventDate = schedule.EventDate, VisitDate = schedule.VisitDate, EventId = schedule.EventId, UserId = schedule.UserId, PatientId = schedule.PatientId, Status = schedule.Status, EpisodeId = schedule.EpisodeId, Discipline = schedule.Discipline, IsDeprecated = schedule.IsDeprecated });
                        }
                        if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask))
                        {
                            Auditor.Log(schedule.EpisodeId, schedule.PatientId, schedule.EventId, Actions.EditDetail, (DisciplineTasks)schedule.DisciplineTask);
                        }
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateScheduleEventDetail(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId);
            var newPatientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.NewEpisodeId, scheduleEvent.PatientId);
            if (newPatientEpisode != null && episode != null)
            {
                var oldScheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                var newScheduleEvents = newPatientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                var schedule = oldScheduleEvents.FirstOrDefault(e => e.EventId == scheduleEvent.EventId && e.EpisodeId == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId);
                if (schedule != null)
                {
                    oldScheduleEvents.RemoveAll(e => e.EventId == schedule.EventId && e.EpisodeId == schedule.EpisodeId && e.PatientId == schedule.PatientId);
                    episode.Schedule = oldScheduleEvents.ToXml();
                    if (httpFiles.Count > 0)
                    {
                        foreach (string key in httpFiles.AllKeys)
                        {
                            HttpPostedFileBase file = httpFiles.Get(key);
                            if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                            {
                                var binaryReader = new BinaryReader(file.InputStream);
                                var asset = new Asset
                                {
                                    FileName = file.FileName,
                                    AgencyId = Current.AgencyId,
                                    ContentType = file.ContentType,
                                    FileSize = file.ContentLength.ToString(),
                                    Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                                };
                                if (assetRepository.Add(asset))
                                {
                                    schedule.Assets.Add(asset.Id);
                                }
                                else
                                {
                                    assetsSaved = false;
                                    break;
                                }
                            }
                        }
                    }
                    Guid userId = schedule.UserId;
                    schedule.EpisodeId = newPatientEpisode.Id;
                    schedule.StartDate = newPatientEpisode.StartDate;
                    schedule.EndDate = newPatientEpisode.EndDate;
                    schedule.UserId = scheduleEvent.UserId;
                    schedule.Comments = scheduleEvent.Comments;
                    schedule.Status = scheduleEvent.Status;
                    schedule.EventDate = scheduleEvent.EventDate;

                    schedule.VisitDate = scheduleEvent.VisitDate;
                    schedule.IsBillable = scheduleEvent.IsBillable;
                    schedule.IsDeprecated = scheduleEvent.IsDeprecated;
                    schedule.Surcharge = scheduleEvent.Surcharge;
                    schedule.AssociatedMileage = scheduleEvent.AssociatedMileage;
                    schedule.TimeIn = scheduleEvent.TimeIn;
                    schedule.TimeOut = scheduleEvent.TimeOut;
                    schedule.IsMissedVisit = scheduleEvent.IsMissedVisit;
                    schedule.DisciplineTask = scheduleEvent.DisciplineTask;
                    schedule.PhysicianId = scheduleEvent.PhysicianId;
                    
                    if (ProcessEditDetailForReassign(schedule))
                    {
                        newScheduleEvents.Add(schedule);
                        newPatientEpisode.Schedule = newScheduleEvents.ToXml();

                        if (assetsSaved && patientRepository.UpdateEpisode(newPatientEpisode) && patientRepository.UpdateEpisode(episode))
                        {
                            var userEvent = userRepository.GetEvent(Current.AgencyId, schedule.UserId, schedule.PatientId, schedule.EventId);
                            if (userEvent != null)
                            {
                                userEvent.EpisodeId = schedule.EpisodeId;
                                userEvent.IsDeprecated = schedule.IsDeprecated;
                                userEvent.EventDate = schedule.EventDate;
                                userEvent.VisitDate = schedule.VisitDate;
                                userEvent.TimeIn = schedule.TimeIn;
                                userEvent.TimeOut = schedule.TimeOut;
                                userEvent.IsMissedVisit = schedule.IsMissedVisit;
                                userEvent.Status = schedule.Status;
                                if (userId == schedule.UserId)
                                {
                                    userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                }
                                else
                                {
                                    userRepository.AddUserEvent(Current.AgencyId, schedule.PatientId, schedule.UserId, userEvent);
                                    userRepository.RemoveScheduleEvent(Current.AgencyId, schedule.PatientId, schedule.EventId, schedule.UserId);
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, schedule.PatientId, schedule.UserId, new UserEvent { DisciplineTask = schedule.DisciplineTask, EventDate = schedule.EventDate, VisitDate = schedule.VisitDate, EventId = schedule.EventId, UserId = schedule.UserId, PatientId = schedule.PatientId, Status = schedule.Status, EpisodeId = schedule.EpisodeId, Discipline = schedule.Discipline, IsDeprecated = schedule.IsDeprecated });
                            }
                            if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask))
                            {
                                Auditor.Log(schedule.EpisodeId, schedule.PatientId, schedule.EventId, Actions.EditDetail, (DisciplineTasks)schedule.DisciplineTask, "Reassigned from other episode.");
                            }
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        public List<ScheduleEvent> GetScheduledEvents(Guid patientId, string discipline, DateRange range)
        {
            var patientEvents = new List<ScheduleEvent>();
            if (range.Id.IsEqual("ThisEpisode"))
            {
                var patientEpisode = patientRepository.GetCurrentEpisodeLean(Current.AgencyId, patientId);
                if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var detail = new EpisodeDetail();
                    var schedule = new List<ScheduleEvent>();

                    if (patientEpisode.Details.IsNotNullOrEmpty())
                    {
                        detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                    }

                    if (patientEpisode.Status == (int)PatientStatus.Discharged)
                    {
                        schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.PatientDischargeDate).ToList();
                    }
                    else
                    {
                        schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true).ToList();
                    }
                    if (schedule.Count > 0)
                    {
                        if (discipline == "All")
                        {
                            schedule = schedule.Where(s => s.DisciplineTask > 0).ToList();
                        }
                        else
                        {
                            schedule = schedule.Where(s => s.DisciplineTask > 0 && s.Discipline.IsEqual(discipline)).ToList();
                        }
                        schedule.ForEach(s =>
                        {
                            if (!s.UserId.IsEmpty())
                            {
                                s.UserName = UserEngine.GetName(s.UserId, Current.AgencyId);
                            }
                            s.EpisodeNotes = detail.Comments.Clean();
                            if (s.IsMissedVisit)
                            {
                                var missedVisit = patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);

                                if (missedVisit != null)
                                {
                                    s.MissedVisitComments = missedVisit.ToString();
                                }
                            }
                            s.EventDate = s.EventDate.ToZeroFilled();
                            s.PatientId = patientId;
                            s.EndDate = DateTime.Parse(patientEpisode.EndDate);
                            s.StartDate = DateTime.Parse(patientEpisode.StartDate);
                            Common.Url.Set(s, true, true);
                            patientEvents.Add(s);
                        });
                    }
                }
            }
            else
            {
                var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
                patientEpisodes.ForEach(patientEpisode =>
                {
                    if (patientEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var detail = new EpisodeDetail();
                        var schedule = new List<ScheduleEvent>();

                        if (patientEpisode.Details.IsNotNullOrEmpty())
                        {
                            detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                        }

                        if (patientEpisode.Status == (int)PatientStatus.Discharged)
                        {
                            schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.PatientDischargeDate).ToList();
                        }
                        else
                        {
                            schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventId != Guid.Empty && s.IsDeprecated != true).ToList();
                        }
                        if (schedule.Count > 0)
                        {
                            if (discipline == "All")
                            {
                                schedule = schedule.Where(s => s.DisciplineTask > 0 && DateTime.Parse(s.EventDate) >= range.StartDate && DateTime.Parse(s.EventDate) <= range.EndDate).ToList();
                            }
                            else
                            {
                                schedule = schedule.Where(s => s.DisciplineTask > 0 && s.Discipline.IsEqual(discipline) && (DateTime.Parse(s.EventDate) >= range.StartDate && DateTime.Parse(s.EventDate) <= range.EndDate)).ToList();
                            }
                            schedule.ForEach(s =>
                            {
                                if (!s.UserId.IsEmpty())
                                {
                                    s.UserName = UserEngine.GetName(s.UserId, Current.AgencyId);
                                }
                                s.EpisodeNotes = detail.Comments;
                                if (s.IsMissedVisit)
                                {
                                    var missedVisit = patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);

                                    if (missedVisit != null)
                                    {
                                        s.MissedVisitComments = missedVisit.ToString();
                                    }
                                }
                                s.EventDate = s.EventDate.ToZeroFilled();
                                s.PatientId = patientId;
                                s.EndDate = DateTime.Parse(patientEpisode.EndDate);
                                s.StartDate = DateTime.Parse(patientEpisode.StartDate);
                                Common.Url.Set(s, true, true);
                                patientEvents.Add(s);
                            });
                        }
                    }
                });
            }

            return patientEvents.OrderByDescending(e => e.EventDateSortable).ToList();
        }

        public List<ScheduleEvent> GetScheduledEvents(Guid episodeId, Guid patientId, string discipline)
        {
            var patientEvents = new List<ScheduleEvent>();
            var patientEpisode = patientRepository.GetPatientScheduledEvents(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
            {
                var detail = new EpisodeDetail();
                var schedule = new List<ScheduleEvent>();

                if (patientEpisode.Details.IsNotNullOrEmpty())
                {
                    detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                }
                if (discipline.IsEqual("all"))
                {
                    schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId != Guid.Empty && e.DisciplineTask > 0 && e.IsDeprecated != true).ToList();
                }
                else if (discipline.IsEqual("Therapy"))
                {
                    schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId != Guid.Empty && e.DisciplineTask > 0 && e.IsDeprecated != true && (e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST")).ToList();
                }
                else
                {
                    schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId != Guid.Empty && e.DisciplineTask > 0 && e.IsDeprecated != true && e.Discipline == discipline).ToList();
                }

                if (patientEpisode.Status == (int)PatientStatus.Discharged)
                {
                    schedule = schedule.Where(s => s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date).ToList();
                }
                if (schedule.Count > 0)
                {
                    schedule.ForEach(s =>
                    {
                        if (!s.UserId.IsEmpty())
                        {
                            s.UserName = UserEngine.GetName(s.UserId, Current.AgencyId);
                        }
                        s.EpisodeNotes = detail.Comments;
                        if (s.IsMissedVisit)
                        {
                            var missedVisit = patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);

                            if (missedVisit != null)
                            {
                                s.MissedVisitComments = missedVisit.ToString();
                            }
                        }
                        s.EventDate = s.EventDate.ToZeroFilled();
                        s.EndDate = DateTime.Parse(patientEpisode.EndDate);
                        s.StartDate = DateTime.Parse(patientEpisode.StartDate);
                        s.PatientId = patientId;
                        Common.Url.Set(s, true, true);
                        patientEvents.Add(s);
                    });
                }
            }
            return patientEvents.OrderByDescending(e => e.EventDateSortable).ToList();
        }

        public List<ScheduleEvent> GetDeletedTasks(Guid patientId)
        {
            var scheduledEvents = new List<ScheduleEvent>();
            var deletedItems = patientRepository.GetDeletedItems(Current.AgencyId, patientId);
            if (deletedItems != null)
            {
                deletedItems.ForEach(item =>
                {
                    if (item != null && item.Schedule.IsNotNullOrEmpty())
                    {
                        var deletedEvents = item.Schedule.ToObject<List<ScheduleEvent>>();
                        if (deletedEvents != null)
                        {
                            deletedEvents.ForEach(e =>
                            {
                                e.EventDate = e.EventDate.ToZeroFilled();
                                e.UserName = UserEngine.GetName(e.UserId, Current.AgencyId);
                                scheduledEvents.Add(e);
                            });
                        }
                    }
                });
            }
            return scheduledEvents;
        }

        public ScheduleEvent GetScheduledEvent(Guid episodeId, Guid patientId, Guid eventId)
        {
            var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);

            if (scheduleEvent != null && scheduleEvent.Discipline.IsEqual(Disciplines.Orders.ToString()))
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PhysicianOrder)
                {
                    var order = patientRepository.GetOrder(eventId, patientId, Current.AgencyId);
                    if (order != null)
                    {
                        scheduleEvent.PhysicianId = order.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HCFA485
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485)
                {
                    var planofcare = assessmentService.GetPlanofCare(episodeId, patientId, eventId);
                    if (planofcare != null)
                    {
                        scheduleEvent.PhysicianId = planofcare.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone)
                {
                    var planofcareStandalone = assessmentService.GetPlanofCareStandAlone(episodeId, patientId, eventId);
                    if (planofcareStandalone != null)
                    {
                        scheduleEvent.PhysicianId = planofcareStandalone.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter)
                {
                    var faceToFace = patientRepository.GetFaceToFaceEncounter(eventId, patientId, Current.AgencyId);
                    if (faceToFace != null)
                    {
                        scheduleEvent.PhysicianId = faceToFace.PhysicianId;
                    }
                }
            }

            return scheduleEvent;
        }

        public bool Reopen(Guid episodeId, Guid patientId, Guid eventId)
        {
            var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
            if (scheduleEvent != null)
            {
                switch ((DisciplineTasks)scheduleEvent.DisciplineTask)
                {
                    case DisciplineTasks.NonOASISDischarge:
                    case DisciplineTasks.NonOASISStartofCare:
                    case DisciplineTasks.NonOASISRecertification:
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathOT:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.OASISCTransferOT:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNAssessmentRecert:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent) && userRepository.Reassign(Current.AgencyId, scheduleEvent, Current.UserId))
                        {
                            var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), scheduleEvent.DisciplineTask)).ToString();
                            assessmentService.UpdateAssessmentStatus(eventId, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisReopened).ToString(), string.Empty);
                            return true;
                        }
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:

                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVObservationAndAssessment:

                    case DisciplineTasks.HHAideVisit:
                    case DisciplineTasks.HHAideCarePlan:
                    case DisciplineTasks.HHAideSupervisoryVisit:

                    case DisciplineTasks.PASVisit:
                    case DisciplineTasks.PASCarePlan:

                    case DisciplineTasks.PTAVisit:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTVisit:

                    case DisciplineTasks.STDischarge:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STVisit:

                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTVisit:

                    case DisciplineTasks.SixtyDaySummary:
                    case DisciplineTasks.TransferSummary:
                    case DisciplineTasks.CoordinationOfCare:
                    case DisciplineTasks.DischargeSummary:

                    case DisciplineTasks.SNDiabeticDailyVisit:

                    case DisciplineTasks.LVNSupervisoryVisit:

                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.MSWProgressNote:
                    case DisciplineTasks.MSWVisit:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.NoteReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent) && userRepository.Reassign(Current.AgencyId, scheduleEvent, Current.UserId))
                        {
                            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                            if (patientVisitNote != null)
                            {
                                patientVisitNote.UserId = scheduleEvent.UserId;
                                patientVisitNote.Status = (int)ScheduleStatus.NoteReopened;
                                patientVisitNote.SignatureText = string.Empty;
                                patientVisitNote.SignatureDate = DateTime.MinValue;
                                patientRepository.UpdateVisitNote(patientVisitNote);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }
                        break;

                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent) && userRepository.Reassign(Current.AgencyId, scheduleEvent, Current.UserId))
                        {
                            var planOfCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
                            if (planOfCare != null)
                            {
                                planOfCare.UserId = scheduleEvent.UserId;
                                planOfCare.Status = (int)ScheduleStatus.OrderReopened;
                                planOfCare.SignatureText = string.Empty;
                                planOfCare.SignatureDate = DateTime.MinValue;
                                planofCareRepository.Update(planOfCare);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent) && userRepository.Reassign(Current.AgencyId, scheduleEvent, Current.UserId))
                        {
                            var planOfCareStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, eventId);
                            if (planOfCareStandAlone != null)
                            {
                                planOfCareStandAlone.UserId = scheduleEvent.UserId;
                                planOfCareStandAlone.Status = (int)ScheduleStatus.OrderReopened;
                                planOfCareStandAlone.SignatureText = string.Empty;
                                planOfCareStandAlone.SignatureDate = DateTime.MinValue;
                                planofCareRepository.UpdateStandAlone(planOfCareStandAlone);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.OrderReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent) && userRepository.Reassign(Current.AgencyId, scheduleEvent, Current.UserId))
                        {
                            var physicianOrder = patientRepository.GetOrder(eventId, patientId, Current.AgencyId);
                            if (physicianOrder != null)
                            {
                                physicianOrder.UserId = scheduleEvent.UserId;
                                physicianOrder.Status = (int)ScheduleStatus.OrderReopened;
                                physicianOrder.SignatureText = string.Empty;
                                physicianOrder.SignatureDate = DateTime.MinValue;
                                patientRepository.UpdateOrderModel(physicianOrder);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }
                        break;
                    case DisciplineTasks.CommunicationNote:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.NoteReopened).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent) && userRepository.Reassign(Current.AgencyId, scheduleEvent, Current.UserId))
                        {
                            var communicationNote = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                            if (communicationNote != null)
                            {
                                communicationNote.UserId = scheduleEvent.UserId;
                                communicationNote.Status = (int)ScheduleStatus.NoteReopened;
                                communicationNote.SignatureText = string.Empty;
                                communicationNote.SignatureDate = DateTime.MinValue;
                                communicationNote.Modified = DateTime.Now;
                                patientRepository.UpdateCommunicationNoteModal(communicationNote);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }

                        break;

                    case DisciplineTasks.IncidentAccidentReport:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReopen).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent) && userRepository.Reassign(Current.AgencyId, scheduleEvent, Current.UserId))
                        {
                            var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
                            if (incident != null)
                            {
                                incident.UserId = scheduleEvent.UserId;
                                incident.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                                incident.SignatureText = string.Empty;
                                incident.SignatureDate = DateTime.MinValue;
                                incident.Modified = DateTime.Now;
                                agencyRepository.UpdateIncidentModal(incident);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }

                        break;

                    case DisciplineTasks.InfectionReport:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReopen).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent) && userRepository.Reassign(Current.AgencyId, scheduleEvent, Current.UserId))
                        {
                            var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
                            if (infection != null)
                            {
                                infection.UserId = scheduleEvent.UserId;
                                infection.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                                infection.SignatureText = string.Empty;
                                infection.SignatureDate = DateTime.MinValue;
                                infection.Modified = DateTime.Now;
                                agencyRepository.UpdateInfectionModal(infection);
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                            return true;
                        }

                        break;
                }
            }
            return false;
        }

        public bool RestoreTask(Guid episodeId, Guid patientId, Guid eventId)
        {
            bool result = false;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var deletedItem = patientRepository.GetDeletedItem(Current.AgencyId, episodeId, patientId);
                if (deletedItem != null && deletedItem.Schedule.IsNotNullOrEmpty())
                {
                    var deletedEvents = deletedItem.Schedule.ToObject<List<ScheduleEvent>>();
                    if (deletedEvents != null && deletedEvents.Count>0)
                    {
                        var deletedEvent = deletedEvents.Find(e => e.EventId == eventId);
                      
                        if (deletedEvent != null )
                        {
                            deletedEvents.RemoveAll(e => e.EventId == eventId);
                        }
                        deletedItem.Schedule = deletedEvents.ToXml();
                        if (patientRepository.UpdateDeletedItem(deletedItem))
                        {
                            if (UpdateScheduleEntity(eventId, episodeId, patientId, ((DisciplineTasks)deletedEvent.DisciplineTask).ToString(), false))
                            {
                                if (patientRepository.AddNewScheduleEvent(Current.AgencyId, patientId, episodeId, deletedEvent))
                                {
                                    if (!deletedEvent.UserId.IsEmpty())
                                    {
                                        patientRepository.AddNewUserEvent(Current.AgencyId, patientId, deletedEvent.ToUserEvent());
                                    }
                                    Auditor.Log(episodeId, patientId, eventId, Actions.Restored, (DisciplineTasks)deletedEvent.DisciplineTask);
                                    result = true;
                                }
                                else
                                {
                                    UpdateScheduleEntity(eventId, episodeId, patientId, ((DisciplineTasks)deletedEvent.DisciplineTask).ToString(), true);
                                    if (deletedEvent != null)
                                    {
                                        deletedEvents.Add(deletedEvent);
                                        deletedItem.Schedule = deletedEvents.ToXml();
                                        patientRepository.UpdateDeletedItem(deletedItem);
                                    }
                                }
                            }
                            else
                            {
                                if (deletedEvent != null)
                                {
                                    deletedEvents.Add(deletedEvent);
                                    deletedItem.Schedule = deletedEvents.ToXml();
                                    patientRepository.UpdateDeletedItem(deletedItem);
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool DeleteSchedule(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId, int task)
        {
            bool result = false;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty() && task >= 0 && Enum.IsDefined(typeof(DisciplineTasks), task))
            {
                if (patientRepository.DeleteScheduleEvent(Current.AgencyId, episodeId, patientId, eventId, task))
                {
                    result = true;

                    UpdateScheduleEntity(eventId, episodeId, patientId, ((DisciplineTasks)task).ToString(), true);

                    if (!employeeId.IsEmpty())
                    {
                        if (userRepository.DeleteScheduleEvent(patientId, eventId, employeeId))
                        {
                            result = true;
                        }
                    }
                    Auditor.Log(episodeId, patientId, eventId, Actions.Deleted, (DisciplineTasks)task);
                }
            }

            return result;
        }

        public bool DeleteScheduleEventAsset(Guid episodeId, Guid patientId, Guid eventId, Guid assetId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            var result = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null && scheduleEvent.Assets != null && scheduleEvent.Assets.Count > 0)
                {
                    scheduleEvent.Assets.Remove(assetId);
                    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                    {
                        if (!assetId.IsEmpty())
                        {
                            if (assetRepository.Delete(assetId))
                            {
                                result = true;
                            }
                        }
                        Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.EditDetail, (DisciplineTasks)scheduleEvent.DisciplineTask, "Uploaded asset is deleted.");
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool Reassign(Guid episodeId, Guid patientId, Guid eventId, Guid oldEmployeeId, Guid employeeId)
        {
            bool result = false;
            try
            {
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    if (patientRepository.Reassign(Current.AgencyId, episodeId, patientId, eventId, employeeId) && ReassignScheduleEntity(episodeId, patientId, eventId, employeeId, ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString()))
                    {
                        userRepository.Reassign(Current.AgencyId, scheduleEvent, employeeId);
                        Auditor.Log(scheduleEvent.EpisodeId, patientId, scheduleEvent.EventId, Actions.Reassigned, (DisciplineTasks)scheduleEvent.DisciplineTask, "( From " + UserEngine.GetName(oldEmployeeId, Current.AgencyId) + " To " + UserEngine.GetName(employeeId, Current.AgencyId) + " )");
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        public bool ReassignSchedules(Guid employeeOldId, Guid employeeId)
        {
            bool result = false;
            var userEvents = userRepository.GetSchedule(Current.AgencyId, employeeOldId);
            if (userEvents != null && userEvents.Count > 0)
            {
                userEvents.ForEach(evnt =>
                {
                    var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, evnt.EpisodeId, evnt.PatientId, evnt.EventId);
                    if (scheduleEvent != null)
                    {
                        if (scheduleEvent.Status == ((int)ScheduleStatus.NoteNotStarted).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.NoteNotYetDue).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisNotStarted).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OrderNotYetDue).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OrderNotYetStarted).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.ReportAndNotesCreated).ToString())
                        {
                            if (patientRepository.Reassign(Current.AgencyId, evnt.EpisodeId, evnt.PatientId, evnt.EventId, employeeId) && ReassignScheduleEntity(evnt.EpisodeId, evnt.PatientId, evnt.EventId, employeeId, ((DisciplineTasks)evnt.DisciplineTask).ToString()))
                            {
                                userRepository.Reassign(Current.AgencyId, scheduleEvent, employeeId);
                                Auditor.Log(evnt.EpisodeId, evnt.PatientId, evnt.EventId, Actions.Reassigned, (DisciplineTasks)evnt.DisciplineTask, "( From " + UserEngine.GetName(employeeOldId, Current.AgencyId) + " To " + UserEngine.GetName(employeeId, Current.AgencyId) + " )");
                            }
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool ReassignSchedules(Guid patientId, Guid employeeOldId, Guid employeeId)
        {
            bool result = false;
            var scheduleEvents = patientRepository.GetScheduledEventsByEmployeeAssigned(Current.AgencyId, patientId, employeeOldId);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                scheduleEvents.ForEach(evnt =>
                {
                    if (evnt.Status == ((int)ScheduleStatus.NoteNotStarted).ToString() || evnt.Status == ((int)ScheduleStatus.NoteNotYetDue).ToString() || evnt.Status == ((int)ScheduleStatus.OasisNotStarted).ToString() || evnt.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() || evnt.Status == ((int)ScheduleStatus.OrderNotYetDue).ToString() || evnt.Status == ((int)ScheduleStatus.OrderNotYetStarted).ToString() || evnt.Status == ((int)ScheduleStatus.ReportAndNotesCreated).ToString())
                    {
                        if (patientRepository.Reassign(Current.AgencyId, evnt.EpisodeId, patientId, evnt.EventId, employeeId) && ReassignScheduleEntity(evnt.EpisodeId, patientId, evnt.EventId, employeeId, ((DisciplineTasks)evnt.DisciplineTask).ToString()))
                        {
                            userRepository.Reassign(Current.AgencyId, evnt, employeeId);
                            Auditor.Log(evnt.EpisodeId, patientId, evnt.EventId, Actions.Reassigned, (DisciplineTasks)evnt.DisciplineTask, "( From " + UserEngine.GetName(employeeOldId, Current.AgencyId) + " To " + UserEngine.GetName(employeeId, Current.AgencyId) + " )");
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public PatientSchedule GetPatientWithSchedule(Guid patientId, string discipline)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");

            var patientSchedule = patientRepository.GetPatient<PatientSchedule>(patientId, Current.AgencyId);
            if (patientSchedule != null)
            {
                patientSchedule.Episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline);
                return patientSchedule;
            }
            return null;
        }

        public PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patientEpisode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null) {
                var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId);
                if (assessment != null) {
                    var assessmentQuestions = assessment.ToDictionary();
                    var tmpString = new List<string>();
                    if (assessmentQuestions.ContainsKey("485SNFrequency") && assessmentQuestions["485SNFrequency"].Answer != "") tmpString.Add("SN Frequency:" + assessmentQuestions["485SNFrequency"].Answer);
                    if (assessmentQuestions.ContainsKey("485PTFrequency") && assessmentQuestions["485PTFrequency"].Answer != "") tmpString.Add("PT Frequency:" + assessmentQuestions["485PTFrequency"].Answer);
                    if (assessmentQuestions.ContainsKey("485OTFrequency") && assessmentQuestions["485OTFrequency"].Answer != "") tmpString.Add("OT Frequency:" + assessmentQuestions["485OTFrequency"].Answer);
                    if (assessmentQuestions.ContainsKey("485STFrequency") && assessmentQuestions["485STFrequency"].Answer != "") tmpString.Add("ST Frequency:" + assessmentQuestions["485STFrequency"].Answer);
                    if (assessmentQuestions.ContainsKey("485MSWFrequency") && assessmentQuestions["485MSWFrequency"].Answer != "") tmpString.Add("MSW Frequency:" + assessmentQuestions["485MSWFrequency"].Answer);
                    if (assessmentQuestions.ContainsKey("485HHAFrequency") && assessmentQuestions["485HHAFrequency"].Answer != "") tmpString.Add("HHA Frequency:" + assessmentQuestions["485HHAFrequency"].Answer);
                    if (tmpString.Count > 0) patientEpisode.Detail.FrequencyList = tmpString.ToArray().Join(", ") + ".";
                    else patientEpisode.Detail.FrequencyList = "";
                }
                var patient = patientRepository.Get(patientId, Current.AgencyId);
                if (patient != null) patientEpisode.PatientIdNumber = patient.PatientIdNumber;
            }
            return patientEpisode;
        }

        public bool SaveNotes(string button, FormCollection formCollection)
        {
            var result = false;

            var keys = formCollection.AllKeys;
            string type = keys.Contains("Type") ? formCollection["Type"] : string.Empty;
            if (type.IsNotNullOrEmpty())
            {
                Guid eventId = keys.Contains(string.Format("{0}_EventId", type)) ? formCollection.Get(string.Format("{0}_EventId", type)).ToGuid() : Guid.Empty;
                Guid episodeId = keys.Contains(string.Format("{0}_EpisodeId", type)) ? formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid() : Guid.Empty;
                Guid patientId = keys.Contains(string.Format("{0}_PatientId", type)) ? formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid() : Guid.Empty;
                var timeIn = keys.Contains(string.Format("{0}_TimeIn", type)) && formCollection[string.Format("{0}_TimeIn", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeIn", type)] : string.Empty;
                var timeOut = keys.Contains(string.Format("{0}_TimeOut", type)) && formCollection[string.Format("{0}_TimeOut", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeOut", type)] : string.Empty;
                string date = keys.Contains(string.Format("{0}_VisitDate", type)) ? formCollection.Get(string.Format("{0}_VisitDate", type)).ToDateTime().ToShortDateString() : string.Empty;
                int disciplineTask = keys.Contains("DisciplineTask") ? formCollection.Get("DisciplineTask").ToInteger() : 0;
                var shouldUpdateEpisode = false;

                var scheduleEvent = new ScheduleEvent();
                var userEvent = new UserEvent();
                var patientVisitNote = new PatientVisitNote();

                if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                {
                    scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                    if (scheduleEvent != null)
                    {
                        userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);

                        patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                        if (patientVisitNote != null)
                        {
                            patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                            patientVisitNote.Note = patientVisitNote.Questions.ToXml();

                            if (button == "Save")
                            {
                                var status = patientVisitNote.Status;
                                if (status != (int)ScheduleStatus.NoteReturned && !(Current.HasRight(Permissions.AccessCaseManagement) && status == (int)ScheduleStatus.NoteSubmittedWithSignature))
                                {
                                    patientVisitNote.Status = (int)ScheduleStatus.NoteSaved;
                                }
                                patientVisitNote.Modified = DateTime.Now;
                                patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                if (patientRepository.UpdateVisitNote(patientVisitNote))
                                {
                                    if (scheduleEvent != null)
                                    {
                                        scheduleEvent.Status = patientVisitNote.Status.ToString();
                                        scheduleEvent.TimeIn = timeIn;
                                        scheduleEvent.TimeOut = timeOut;
                                        scheduleEvent.DisciplineTask = disciplineTask;
                                        if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary))
                                        {
                                            scheduleEvent.VisitDate = date;
                                        }
                                        if (userEvent != null)
                                        {
                                            userEvent.Status = patientVisitNote.Status.ToString();
                                            userEvent.TimeIn = timeIn;
                                            userEvent.TimeOut = timeOut;
                                            userEvent.DisciplineTask = disciplineTask;
                                            if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary))
                                            {
                                                userEvent.VisitDate = date;
                                            }
                                        }
                                        shouldUpdateEpisode = true;
                                    }
                                }
                            }
                            else if (button == "Complete")
                            {

                                patientVisitNote.Status = (int)ScheduleStatus.NoteSubmittedWithSignature;
                                if (Current.HasRight(Permissions.BypassCaseManagement))
                                {
                                    patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                                    if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation)
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                    }
                                }

                                if (type == DisciplineTasks.PTEvaluation.ToString() || type == DisciplineTasks.PTReEvaluation.ToString()
                                    || type == DisciplineTasks.OTEvaluation.ToString() || type == DisciplineTasks.OTReEvaluation.ToString()
                                    || type == DisciplineTasks.STEvaluation.ToString() || type == DisciplineTasks.STReEvaluation.ToString())
                                {
                                    patientVisitNote.PhysicianId = formCollection[type + "_PhysicianId"].ToGuid();
                                    var physician = PhysicianEngine.Get(patientVisitNote.PhysicianId, Current.AgencyId);
                                    if (physician != null)
                                    {
                                        patientVisitNote.PhysicianData = physician.ToXml();
                                    }
                                }

                                patientVisitNote.UserId = Current.UserId;
                                patientVisitNote.Modified = DateTime.Now;
                                patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                patientVisitNote.SignatureDate = formCollection[type + "_SignatureDate"].ToDateTime();
                                patientVisitNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                                if (patientRepository.UpdateVisitNote(patientVisitNote))
                                {
                                    if (scheduleEvent != null)
                                    {
                                        scheduleEvent.Status = patientVisitNote.Status.ToString();
                                        scheduleEvent.TimeIn = timeIn;
                                        scheduleEvent.TimeOut = timeOut;
                                        scheduleEvent.DisciplineTask = disciplineTask;
                                        if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary))
                                        {
                                            scheduleEvent.VisitDate = date;
                                        }
                                        if (userEvent != null)
                                        {
                                            userEvent.Status = scheduleEvent.Status.ToString();
                                            userEvent.TimeIn = timeIn;
                                            userEvent.TimeOut = timeOut;
                                            userEvent.DisciplineTask = disciplineTask;
                                            if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary))
                                            {
                                                userEvent.VisitDate = date;
                                            }
                                        }
                                        shouldUpdateEpisode = true;
                                    }
                                }
                            }
                            else if (button == "Approve")
                            {
                                patientVisitNote.Status = ((int)ScheduleStatus.NoteCompleted);
                                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation)
                                {
                                    patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                }
                                patientVisitNote.Modified = DateTime.Now;
                                patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                if (patientRepository.UpdateVisitNote(patientVisitNote))
                                {
                                    if (scheduleEvent != null)
                                    {
                                        scheduleEvent.InPrintQueue = true;
                                        scheduleEvent.Status = ((int)ScheduleStatus.NoteCompleted).ToString();
                                        scheduleEvent.ReturnReason = string.Empty;
                                        if (userEvent != null)
                                        {
                                            userEvent.Status = ((int)ScheduleStatus.NoteCompleted).ToString();
                                            userEvent.ReturnReason = string.Empty;
                                        }
                                        shouldUpdateEpisode = true;
                                    }
                                }
                            }
                            else if (button == "Return")
                            {
                                patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                                patientVisitNote.Modified = DateTime.Now;
                                patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                if (patientRepository.UpdateVisitNote(patientVisitNote))
                                {
                                    if (scheduleEvent != null)
                                    {
                                        scheduleEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                        scheduleEvent.ReturnReason = string.Empty;
                                        if (userEvent != null)
                                        {
                                            userEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                            userEvent.ReturnReason = string.Empty;
                                        }
                                        shouldUpdateEpisode = true;
                                    }
                                }
                            }
                            if (shouldUpdateEpisode)
                            {
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                                {
                                    if (userEvent != null)
                                    {
                                        if (userRepository.UpdateEvent(Current.AgencyId, userEvent))
                                        {
                                            result = true;
                                        }
                                    }
                                    else
                                    {
                                        userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit });
                                        result = true;
                                    }
                                    if (scheduleEvent.Status.IsInteger())
                                    {
                                        Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId, string reason)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;
            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            var patientVisitNote = new PatientVisitNote();

            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                }
                patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null)
                {
                    if (button == "Approve")
                    {
                        patientVisitNote.Status = ((int)ScheduleStatus.NoteCompleted);
                        if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation
                            || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                            || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation
                            || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation
                            || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                            || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation)
                        {
                            patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                        }
                        patientVisitNote.Modified = DateTime.Now;
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = patientVisitNote.Status.ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    userEvent.Status = patientVisitNote.Status.ToString();
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (button == "Return")
                    {
                        patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                        patientVisitNote.Modified = DateTime.Now;
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (button == "Print")
                    {
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                    }
                    if (shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(Current.AgencyId, userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                            if (scheduleEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool SaveWoundCare(FormCollection formCollection, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;
            string type = formCollection["Type"];
            if (type.IsNotNullOrEmpty())
            {
                Guid eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
                Guid episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
                Guid patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();
                var patientVisitNote = new PatientVisitNote();
                if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                {
                    patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                    if (patientVisitNote != null)
                    {
                        patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                        if (patientVisitNote.Questions != null)
                        {
                            if (httpFiles.Count > 0)
                            {
                                foreach (string key in httpFiles.AllKeys)
                                {
                                    var keyArray = key.Split('_');
                                    HttpPostedFileBase file = httpFiles.Get(key);
                                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                                    {
                                        BinaryReader binaryReader = new BinaryReader(file.InputStream);
                                        var asset = new Asset
                                        {
                                            FileName = file.FileName,
                                            AgencyId = Current.AgencyId,
                                            ContentType = file.ContentType,
                                            FileSize = file.ContentLength.ToString(),
                                            Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                                        };

                                        if (assetRepository.Add(asset))
                                        {
                                            if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                            {
                                                patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = asset.Id.ToString();
                                            }
                                            else
                                            {
                                                patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = asset.Id.ToString(), Type = keyArray[0] });
                                            }
                                        }
                                        else
                                        {
                                            if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                            {
                                                patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                                            }
                                            else
                                            {
                                                patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                                            }
                                            assetsSaved = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                        {
                                            patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                                        }
                                        else
                                        {
                                            patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                                        }
                                    }
                                }
                            }
                            patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                            patientVisitNote.IsWoundCare = true;
                            if (assetsSaved && patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                                Auditor.Log(patientVisitNote.EpisodeId, patientVisitNote.PatientId, patientVisitNote.Id, Actions.Edit, DisciplineTasks.SkilledNurseVisit, "Wound Care is added/ updated.");
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(name, "name");
            var result = false;
            var patientVisitNote = new PatientVisitNote();
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.WoundNote.IsNotNullOrEmpty())
                {
                    patientVisitNote.Questions = patientVisitNote.WoundNote.ToObject<List<NotesQuestion>>();
                    if (patientVisitNote.Questions.Exists(q => q.Name == name))
                    {
                        patientVisitNote.Questions.SingleOrDefault(q => q.Name == name).Answer = Guid.Empty.ToString();
                        patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (assetRepository.Delete(assetId))
                            {
                                result = true;
                            }
                            Auditor.Log(patientVisitNote.EpisodeId, patientVisitNote.PatientId, patientVisitNote.Id, Actions.Edit, DisciplineTasks.SkilledNurseVisit, "Wound Care Asset is deleted.");
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public Rap CreateRap(Patient patient, PatientEpisode episode)
        {
            var rap = new Rap
             {
                 Id = episode.Id,
                 AgencyId = patient.AgencyId,
                 PatientId = patient.Id,
                 EpisodeId = episode.Id,
                 EpisodeStartDate = episode.StartDate,
                 EpisodeEndDate = episode.EndDate,
                 IsFirstBillableVisit = false,
                 IsOasisComplete = false,
                 PatientIdNumber = patient.PatientIdNumber,
                 IsGenerated = false,
                 MedicareNumber = patient.MedicareNumber,
                 FirstName = patient.FirstName,
                 LastName = patient.LastName,
                 DOB = patient.DOB,
                 Gender = patient.Gender,
                 AddressLine1 = patient.AddressLine1,
                 AddressLine2 = patient.AddressLine2,
                 AddressCity = patient.AddressCity,
                 AddressStateCode = patient.AddressStateCode,
                 AddressZipCode = patient.AddressZipCode,
                 StartofCareDate = episode.StartOfCareDate,
                 AreOrdersComplete = false,
                 AdmissionSource = patient.AdmissionSource,
                 PatientStatus = patient.Status,
                 UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
                 Status = (int)ScheduleStatus.ClaimCreated,
                 HealthPlanId = patient.PrimaryHealthPlanId,
                 DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>",
                 ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>",
                 Created = DateTime.Now
             };
            int primaryInsurance;
            if (int.TryParse(patient.PrimaryInsurance, out primaryInsurance))
            {
                rap.PrimaryInsuranceId = primaryInsurance;
            }
            if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
            {
                if (!patient.AgencyPhysicians[0].IsEmpty())
                {
                    var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
                    if (physician != null)
                    {
                        rap.PhysicianNPI = physician.NPI;
                        rap.PhysicianFirstName = physician.FirstName;
                        rap.PhysicianLastName = physician.LastName;
                    }
                }
            }
            return rap;
        }

        public Final CreateFinal(Patient patient, PatientEpisode episode)
        {
            var final = new Final
            {
                Id = episode.Id,
                AgencyId = patient.AgencyId,
                PatientId = patient.Id,
                EpisodeId = episode.Id,
                EpisodeStartDate = episode.StartDate,
                EpisodeEndDate = episode.EndDate,
                IsFirstBillableVisit = false,
                IsOasisComplete = false,
                PatientIdNumber = patient.PatientIdNumber,
                IsGenerated = false,
                MedicareNumber = patient.MedicareNumber,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                DOB = patient.DOB,
                Gender = patient.Gender,
                AddressLine1 = patient.AddressLine1,
                AddressLine2 = patient.AddressLine2,
                AddressCity = patient.AddressCity,
                AddressStateCode = patient.AddressStateCode,
                AddressZipCode = patient.AddressZipCode,
                StartofCareDate = episode.StartOfCareDate,
                AdmissionSource = patient.AdmissionSource,
                PatientStatus = patient.Status,
                UB4PatientStatus=patient.Status==1? "30": (patient.Status==2?"01": string.Empty),
                AreOrdersComplete = false,
                AreVisitsComplete = false,
                Status = (int)ScheduleStatus.ClaimCreated,
                IsSupplyVerified = false,
                IsFinalInfoVerified = false,
                IsVisitVerified = false,
                IsRapGenerated = false,
                HealthPlanId = patient.PrimaryHealthPlanId,
                Created = DateTime.Now,
                DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>",
                ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>",
                Modified = DateTime.Now
            };
            int primaryInsurance;
            if (int.TryParse(patient.PrimaryInsurance, out primaryInsurance))
            {
                final.PrimaryInsuranceId = primaryInsurance;
            }
            if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
            {
                if (!patient.AgencyPhysicians[0].IsEmpty())
                {
                    var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
                    if (physician != null)
                    {
                        final.PhysicianNPI = physician.NPI;
                        final.PhysicianFirstName = physician.FirstName;
                        final.PhysicianLastName = physician.LastName;
                    }
                }
            }
            return final;
        }

        public bool AddClaim(Guid patientId, Guid episodeId, string type)
        {
            bool result = false;
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            if (episode != null && patient != null)
            {
                if (type == "RAP" || type == "Rap")
                {
                    var rap = CreateRap(patient, episode);
                    if (billingRepository.AddRap(rap))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                        result = true;
                    }
                }
                else if (type == "Final" || type == "FINAL")
                {
                    var final = CreateFinal(patient, episode);
                    if (billingRepository.AddFinal(final))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool DeleteClaim(Guid patientId, Guid Id, string type)
        {
            bool result = false;
            if (type == "RAP" || type == "Rap")
            {
                if (billingRepository.DeleteRap(Current.AgencyId, patientId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Rap, LogAction.RAPDeleted, string.Empty);
                    result = true;
                }
            }
            else if (type == "Final" || type == "FINAL")
            {
                if (billingRepository.DeleteFinal(Current.AgencyId, patientId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalDeleted, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public ManagedClaim CreateManagedClaim(Patient patient, DateTime startDate, DateTime endDate, int insuranceId)
        {
            var managedClaim = new ManagedClaim
            {
                Id = Guid.NewGuid(),
                AgencyId = patient.AgencyId,
                PatientId = patient.Id,
                EpisodeStartDate = startDate,
                EpisodeEndDate = endDate,
                IsFirstBillableVisit = false,
                IsOasisComplete = false,
                PatientIdNumber = patient.PatientIdNumber,
                IsGenerated = false,
                IsuranceIdNumber = patient.MedicareNumber,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                DOB = patient.DOB,
                Gender = patient.Gender,
                AddressLine1 = patient.AddressLine1,
                AddressLine2 = patient.AddressLine2,
                AddressCity = patient.AddressCity,
                AddressStateCode = patient.AddressStateCode,
                AddressZipCode = patient.AddressZipCode,
                StartofCareDate = patient.StartofCareDate,
                AreOrdersComplete = false,
                AdmissionSource = patient.AdmissionSource,
                PatientStatus = patient.Status,
                UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
                Status = (int)ManagedClaimStatus.ClaimCreated,
                HealthPlanId = patient.PrimaryHealthPlanId,
                PrimaryInsuranceId = insuranceId,
                DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>",
                ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>",
                Created = DateTime.Now
            };

            if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
            {
                if (!patient.AgencyPhysicians[0].IsEmpty())
                {
                    var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
                    if (physician != null)
                    {
                        managedClaim.PhysicianNPI = physician.NPI;
                        managedClaim.PhysicianFirstName = physician.FirstName;
                        managedClaim.PhysicianLastName = physician.LastName;
                    }
                }
            }
            return managedClaim;
        }

        public bool AddManagedClaim(Guid patientId, DateTime startDate, DateTime endDate, int insuranceId)
        {
            bool result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var managedClaim = CreateManagedClaim(patient, startDate, endDate, insuranceId);
                if (billingRepository.AddManagedClaim(managedClaim))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedClaimAdded, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool AddMissedVisit(MissedVisit missedVisit)
        {
            bool result = false;
            var episode = patientRepository.GetEpisode(Current.AgencyId, missedVisit.EpisodeId, missedVisit.PatientId);
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                missedVisit.AgencyId = Current.AgencyId;
                var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                var scheduledEvent = events.Find(e => e.EventId == missedVisit.Id);
                if (scheduledEvent != null)
                {
                    scheduledEvent.IsMissedVisit = true;
                    var userEvent = userRepository.GetEvent(Current.AgencyId, scheduledEvent.UserId, scheduledEvent.PatientId, scheduledEvent.EventId);
                    if (userEvent != null)
                    {
                        userEvent.IsMissedVisit = true;
                        userRepository.UpdateEvent(Current.AgencyId, userEvent);
                    }
                    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent) 
                       && patientRepository.AddMissedVisit(missedVisit))
                    {
                        result = true;
                        if (Enum.IsDefined(typeof(DisciplineTasks), scheduledEvent.DisciplineTask))
                        {
                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), scheduledEvent.DisciplineTask), "Set as a missed visit by " + Current.UserFullName);
                        }
                    }
                }
            }
            return result;
        }

        public bool IsValidImage(HttpFileCollectionBase httpFiles)
        {
            var result = false;
            if (httpFiles.Count > 0)
            {
                HttpPostedFileBase file = httpFiles.Get("Photo1");
                if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                {
                    var fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
                    if (AppSettings.AllowedImageExtensions.IsNotNullOrEmpty())
                    {
                        var allowedExtensions = AppSettings.AllowedImageExtensions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        if (allowedExtensions != null && allowedExtensions.Length > 0)
                        {
                            allowedExtensions.ForEach(extension =>
                            {
                                if (fileExtension.IsEqual(extension))
                                {
                                    result = true;
                                    return;
                                }
                            });
                        }
                    }
                }
            }
            return result;
        }

        public bool AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, int supplyId, string quantity, string date)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.Id == supplyId && s.Date == date))
                    {
                        var supply = supplies.SingleOrDefault(s => s.Id == supplyId && s.Date == date);
                        if (supply != null)
                        {
                            supply.Quantity = quantity;
                            patientVisitNote.Supply = supplies.ToXml();
                            patientVisitNote.IsSupplyExist = true;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        var supply = lookupRepository.GetSupply(supplyId);
                        if (supply != null)
                        {
                            supply.Date = date;
                            supply.Quantity = quantity;
                            supply.UniqueIdentifier = Guid.NewGuid();
                            supplies.Add(supply);
                            patientVisitNote.Supply = supplies.ToXml();
                            patientVisitNote.IsSupplyExist = true;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                            }
                        }
                    }
                }
                else
                {
                    var supply = lookupRepository.GetSupply(supplyId);
                    if (supply != null)
                    {
                        var newSupplies = new List<Supply>();
                        supply.Date = date;
                        supply.Quantity = quantity;
                        supply.UniqueIdentifier = Guid.NewGuid();
                        newSupplies.Add(supply);
                        patientVisitNote.Supply = newSupplies.ToXml();
                        patientVisitNote.IsSupplyExist = true;
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        var editSupply = supplies.SingleOrDefault(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (editSupply != null)
                        {
                            editSupply.Quantity = supply.Quantity;
                            patientVisitNote.Supply = supplies.ToXml();
                            patientVisitNote.IsSupplyExist = true;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public bool DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        supplies.ForEach(s =>
                        {
                            if (s.UniqueIdentifier == supply.UniqueIdentifier)
                            {
                                supplies.Remove(s);
                            }
                        });
                        patientVisitNote.Supply = supplies.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public List<Supply> GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId)
        {
            var list = new List<Supply>();
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    list = patientVisitNote.Supply.ToObject<List<Supply>>();
                }
            }
            return list;
        }

        public PatientEligibility VerifyEligibility(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        {
            PatientEligibility patientEligibility = null;
            try
            {
                var jsonData = new
                {
                    input_medicare_number = medicareNumber,
                    input_last_name = lastName,
                    input_first_name = firstName.Substring(0, 1),
                    input_date_of_birth = dob.ToString("MM/dd/yyyy"),
                    input_gender_id = gender.Substring(0, 1)
                };

                var javaScriptSerializer = new JavaScriptSerializer();
                var jsonRequest = javaScriptSerializer.Serialize(jsonData);

                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = ("request=" + jsonRequest);
                byte[] requestData = encoding.GetBytes(postData);

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(AppSettings.PatientEligibilityUrl);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.ContentLength = requestData.Length;

                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {
                    requestStream.Write(requestData, 0, requestData.Length);
                    requestStream.Close();
                }

                var jsonResult = string.Empty;
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        jsonResult = streamReader.ReadToEnd();
                    }

                    if (jsonResult.IsNotNullOrEmpty())
                    {
                        patientEligibility = javaScriptSerializer.Deserialize<PatientEligibility>(jsonResult);
                        if (patientEligibility != null && patientEligibility.Episode != null && patientEligibility.Episode.reference_id.IsNotNullOrEmpty())
                        {
                            var npiData = Container.Resolve<ILookUpDataProvider>().LookUpRepository.GetNpiData(patientEligibility.Episode.reference_id.Trim());
                            if (npiData != null)
                            {
                                patientEligibility.Other_Agency_Data = new OtherAgencyData() {
                                    name = npiData.ProviderOrganizationName,
                                    address1 = npiData.ProviderFirstLineBusinessMailingAddress,
                                    address2 = npiData.ProviderSecondLineBusinessMailingAddress,
                                    city = npiData.ProviderBusinessMailingAddressCityName,
                                    state = npiData.ProviderBusinessMailingAddressStateName,
                                    zip = npiData.ProviderBusinessMailingAddressPostalCode,
                                    phone = npiData.ProviderBusinessMailingAddressTelephoneNumber,
                                    fax = npiData.ProviderBusinessMailingAddressFaxNumber
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }
            return patientEligibility;
        }

        public List<MedicareEligibility> GetMedicareEligibilityLists(Guid patientId)
        {
            var eligibilities = patientRepository.GetMedicareEligibilities(Current.AgencyId, patientId);
            if (eligibilities != null)
            {
                eligibilities.ForEach(e =>
                {
                    if (!e.EpisodeId.IsEmpty())
                    {
                        var episode = patientRepository.GetEpisode(Current.AgencyId, e.EpisodeId, e.PatientId);
                        if (episode != null)
                        {
                            e.EpisodeRange = string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted);
                        }
                        else
                        {
                            e.EpisodeRange = "Not in an episode";
                        }
                    }
                    e.AssignedTo = "Axxess";
                    e.TaskName = "Medicare Eligibility Report";
                    e.PrintUrl = "<a onclick=\"U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + e.PatientId + "', 'mcareEligibilityId': '" + e.Id + "' });return false\"><span class='img icon print'></span></a>";
                });
            }
            return eligibilities;
        }

        public string GetScheduledEventUrl(PatientEpisode episode, DisciplineTasks task)
        {
            var url = string.Empty;
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                if (scheduledEvents != null && scheduledEvents.Count > 0)
                {
                    scheduledEvents = scheduledEvents.OrderBy(e => e.EventDate).ToList();
                    scheduledEvents.ForEach(scheduleEvent =>
                    {
                        if (scheduleEvent != null && (int)task == scheduleEvent.DisciplineTask)
                        {
                            switch (task)
                            {
                                case DisciplineTasks.HHAideCarePlan:
                                    url = new StringBuilder("<a onclick=\"Acore.OpenPrintView({")
                                    .AppendFormat("Url: '/HHACarePlan/View/{0}/{1}/{2}'", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId)
                                    .Append("});return false\">View Care Plan</a>").ToString();
                                    break;
                                case DisciplineTasks.PTEvaluation:
                                    url = new StringBuilder("<a onclick=\"Acore.OpenPrintView({")
                                    .AppendFormat("Url: '/PTEvaluation/View/{0}/{1}/{2}'", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId)
                                    .Append("});return false\">View Eval</a>").ToString();
                                    break;
                                case DisciplineTasks.STEvaluation:
                                    url = new StringBuilder("<a onclick=\"Acore.OpenPrintView({")
                                    .AppendFormat("Url: '/STEvaluation/View/{0}/{1}/{2}'", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId)
                                    .Append("});return false\">View Eval</a>").ToString();
                                    break;
                                case DisciplineTasks.OTEvaluation:
                                    url = new StringBuilder("<a onclick=\"Acore.OpenPrintView({")
                                    .AppendFormat("Url: '/OTEvaluation/View/{0}/{1}/{2}'", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId)
                                    .Append("});return false\">View Eval</a>").ToString();
                                    break;
                                default:
                                    break;
                            }
                            if (url.IsNotNullOrEmpty())
                            {
                                return;
                            }
                        }
                    });
                }
            }
            return url;
        }

        public PlanofCareViewData GetPatientAndAgencyInfo(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new PlanofCareViewData();
            var planofcare = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, eventId);
            if (planofcare != null)
            {
                if (planofcare.Data.IsNotNullOrEmpty())
                {
                    planofcare.Questions = planofcare.Data.ToObject<List<Question>>();
                }
                viewData.EditData = planofcare.ToDictionary();

                var dictionary = new Dictionary<string, string>();
                dictionary.Add("Id", eventId.ToString());

                var patient = patientRepository.Get(patientId, Current.AgencyId);
                if (patient != null)
                {
                    dictionary.Add("PatientId", patient.Id.ToString());
                    dictionary.Add("PatientName", patient.DisplayName);
                    dictionary.Add("PatientIdNumber", patient.PatientIdNumber);
                    dictionary.Add("PatientMedicareNumber", patient.MedicareNumber);
                    dictionary.Add("PatientAddressFirstRow", patient.AddressFirstRow);
                    dictionary.Add("PatientAddressSecondRow", patient.AddressSecondRow);
                    dictionary.Add("PatientPhone", patient.PhoneHomeFormatted);
                    dictionary.Add("PatientDoB", patient.DOBFormatted);
                    dictionary.Add("PatientGender", patient.Gender);
                    

                    if (!patient.AgencyId.IsEmpty())
                    {
                        var agency = agencyRepository.Get(patient.AgencyId);
                        if (agency != null)
                        {
                            dictionary.Add("AgencyId", agency.Id.ToString());
                            dictionary.Add("AgencyMedicareProviderNumber", agency.MedicareProviderNumber);
                            dictionary.Add("AgencyName", agency.Name);
                            dictionary.Add("AgencyAddressFirstRow", agency.MainLocation.AddressFirstRow);
                            dictionary.Add("AgencyAddressSecondRow", agency.MainLocation.AddressSecondRow);
                            dictionary.Add("AgencyPhone", agency.MainLocation.PhoneWorkFormatted);
                            dictionary.Add("AgencyFax", agency.MainLocation.FaxNumberFormatted);
                        }
                    }

                    if (planofcare.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = planofcare.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            dictionary.Add("PhysicianId", physician.Id.ToString());
                            dictionary.Add("PhysicianName", physician.DisplayName);
                            dictionary.Add("PhysicianNpi", physician.NPI);
                        }
                    }
                    else
                    {
                        if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                        {
                            var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
                            if (primaryPhysician == null)
                            {
                                primaryPhysician = patient.PhysicianContacts.First();
                            }

                            if (primaryPhysician != null)
                            {
                                dictionary.Add("PhysicianId", primaryPhysician.Id.ToString());
                                dictionary.Add("PhysicianName", primaryPhysician.DisplayName);
                                dictionary.Add("PhysicianNpi", primaryPhysician.NPI);
                            }
                        }
                    }
                }

                var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, episodeId, patientId);//change when SOC implemented
                if (episode != null)
                {
                    dictionary.Add("PatientSoC", episode.StartOfCareDate.ToString("MM/dd/yyyy"));
                    dictionary.Add("EpisodeId", episode.Id.ToString());
                    dictionary.Add("EpisodeCertPeriod", string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted));
                }
                viewData.DisplayData = dictionary;
            }

            return viewData;
        }

        public void AddInfectionUserAndScheduleEvent(Infection infection, out Infection infectionOut)
        {
            var newScheduleEvent = new ScheduleEvent
            {
                EventId = infection.Id,
                UserId = infection.UserId,
                PatientId = infection.PatientId,
                EpisodeId = infection.EpisodeId,
                Status = infection.Status.ToString(),
                Discipline = Disciplines.ReportsAndNotes.ToString(),
                EventDate = infection.InfectionDate.ToShortDateString(),
                VisitDate = infection.InfectionDate.ToShortDateString(),
                DisciplineTask = (int)DisciplineTasks.InfectionReport
            };
            if (patientRepository.UpdateEpisode(Current.AgencyId, infection.EpisodeId, infection.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
            {
                Auditor.Log(newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.InfectionReport);
            }
            infectionOut = infection;
        }

        public void AddIncidentUserAndScheduleEvent(Incident incident, out Incident incidentOut)
        {
            var newScheduleEvent = new ScheduleEvent
            {
                EventId = incident.Id,
                UserId = incident.UserId,
                PatientId = incident.PatientId,
                EpisodeId = incident.EpisodeId,
                Status = incident.Status.ToString(),
                Discipline = Disciplines.ReportsAndNotes.ToString(),
                EventDate = incident.IncidentDate.ToShortDateString(),
                VisitDate = incident.IncidentDate.ToShortDateString(),
                DisciplineTask = (int)DisciplineTasks.IncidentAccidentReport
            };
            if (patientRepository.UpdateEpisode(Current.AgencyId, incident.EpisodeId, incident.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
            {
                Auditor.Log(newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.IncidentAccidentReport);
            }
            incident.EpisodeId = newScheduleEvent.EpisodeId;
            incidentOut = incident;
        }

        public List<VisitNoteViewData> GetSixtyDaySummary(Guid patientId)
        {
            var visitNoteviewDatas = new List<VisitNoteViewData>();
            var visits = patientRepository.GetVisitNotesByDisciplineTaskWithStatus(Current.AgencyId, patientId, DisciplineTasks.SixtyDaySummary, (int)ScheduleStatus.NoteCompleted);
            if (visits != null && visits.Count > 0)
            {
                visits.ForEach(v =>
                {
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, v.EpisodeId, patientId);
                    if (episode != null && episode.IsDischarged == false && episode.IsActive == true)
                    {
                        var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                        if (scheduleEvents != null && scheduleEvents.Count > 0)
                        {
                            var scheduleEvent = scheduleEvents.SingleOrDefault(s => s.EventId == v.Id);
                            if (scheduleEvent != null)
                            {
                                var visitNoteviewData = new VisitNoteViewData();
                                var user = userRepository.GetUserOnly(v.UserId, Current.AgencyId);
                                if (user != null)
                                {
                                    visitNoteviewData.UserDisplayName = user.DisplayName;
                                }
                                var questions = v.ToDictionary();
                                if (questions != null && questions.Count > 0)
                                {
                                    var physicianId = questions.ContainsKey("Physician") && questions["Physician"].Answer.IsNotNullOrEmpty() && questions["Physician"].Answer.IsGuid() ? questions["Physician"].Answer.ToGuid() : Guid.Empty;
                                    visitNoteviewData.VisitDate = scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? scheduleEvent.VisitDate.ToZeroFilled() : scheduleEvent.EventDate.ToZeroFilled();
                                    if (!physicianId.IsEmpty())
                                    {
                                        var physician = PhysicianEngine.Get(physicianId, Current.AgencyId);
                                        if (physician != null)
                                        {
                                            visitNoteviewData.PhysicianDisplayName = physician.DisplayName;
                                        }
                                    }
                                }
                                visitNoteviewData.StartDate = episode.StartDate;
                                visitNoteviewData.EndDate = episode.EndDate;
                                visitNoteviewData.SignatureDate = questions.ContainsKey("SignatureDate") && questions["SignatureDate"].Answer.IsNotNullOrEmpty() && questions["SignatureDate"].Answer.IsValidDate() ? questions["SignatureDate"].Answer : string.Empty;
                                visitNoteviewData.PrintUrl = Url.Print(scheduleEvent, true);
                                visitNoteviewDatas.Add(visitNoteviewData);
                            }
                        }
                    }
                });
            }
            return visitNoteviewDatas;
        }

        public List<VitalSign> GetPatientVitalSigns(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var vitalSigns = new List<VitalSign>();
            var scheduleEvents = patientRepository.GetScheduledEventsOnly(Current.AgencyId, patientId, startDate, endDate);
            if (scheduleEvents != null && scheduleEvents.Count>0)
            {
                scheduleEvents.ForEach(s =>
                {
                    if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
                    {
                        if (s.IsStartofCareAssessment() || s.IsRecertificationAssessment() || s.IsResumptionofCareAssessment())
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var assessment = assessmentService.GetAssessment(s.EventId, Enum.GetName(typeof(DisciplineTasks), s.DisciplineTask));
                            if (assessment != null && assessment.OasisData.IsNotNullOrEmpty())
                            {
                                var questions = assessment.ToDictionary();
                                vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                if (questions != null)
                                {
                                    vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                    vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


                                    vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                    vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                    vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                    vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                    vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                    vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

                                    vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPLyingRight : "");
                                    vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
                                    vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");

                                    var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                    var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                    var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                    var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                    var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
                                    var maxBs = bs.Max();

                                    vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                    vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;

                                    vitalSign.BS = maxBs > 0 ? maxBs.ToString() : string.Empty; //questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                    vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                    vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                }
                                var user = UserEngine.GetName(s.UserId, Current.AgencyId);
                                if (user.IsNotNullOrEmpty())
                                {
                                    vitalSign.UserDisplayName = user;
                                }
                                vitalSigns.Add(vitalSign);
                            }
                        }

                        if (((DisciplineTasks)s.DisciplineTask).GetCustomShortDescription() == "SNV")
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var vitalSign = new VitalSign();
                                vitalSign.DisciplineTask = s.DisciplineTaskName;
                                var questions = visitNote.ToDictionary();
                                vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                if (questions != null)
                                {
                                    vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                    vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;

                                 
                                    vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                    vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                    vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                    vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                    vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                    vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

                     
                                    vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty()? " R: "+ vitalSign.BPLyingRight:"");
                                    vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
                                    vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");


                                   
                                    var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                    var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                    var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                    var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                    var bs = new double[] { BSAM,BSNoon,BSPM,BSHS };
                                    var maxBs = bs.Max();
                                    vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                    vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;
                                    vitalSign.BS = maxBs > 0 ? maxBs.ToString() : string.Empty;// questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                   
                                    vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                    vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                }
                                var user = UserEngine.GetName(s.UserId, Current.AgencyId);
                                if (user.IsNotNullOrEmpty())
                                {
                                    vitalSign.UserDisplayName = user;
                                }
                                vitalSigns.Add(vitalSign);
                            }
                        }
                    }
                });
            }
            return vitalSigns;
        }

        public List<VitalSign> GetVitalSignsForSixtyDaySummary(Guid patientId, Guid episodeId, DateTime date)
        {
            var vitalSigns = new List<VitalSign>();
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => Enum.IsDefined(typeof(DisciplineTasks), e.DisciplineTask) && e.EventDate.IsValidDate()  &&  e.EventDate.ToDateTime() >= episode.StartDate && e.EventDate.ToDateTime() <= episode.EndDate && e.EventDate.ToDateTime().Date<=date.Date && !e.IsMissedVisit && e.IsDeprecated == false).ToList();
                if (scheduleEvents != null && scheduleEvents.Count>0 )
                {
                    scheduleEvents.ForEach(s =>
                    {
                        if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
                        {
                            var vitalSign = new VitalSign();
                            if (((DisciplineTasks)s.DisciplineTask).GetCustomShortDescription() == "SNV")
                            {

                                var visitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, s.EventId);
                                if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                                {
                                    var questions = visitNote.ToDictionary();
                                   
                                    vitalSign.VisitDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                    if (questions != null)
                                    {
                                        vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                        vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;

                                        vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                        vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                        vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                        vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                        vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                        vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

                                        vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                        vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;

                                        var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                        var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                        var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                        var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                        var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
                                        var maxBs = bs.Max();
                                        vitalSign.BS = maxBs > 0 ? maxBs.ToString() : string.Empty;//questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;

                                        vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                        vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                    }
                                    vitalSigns.Add(vitalSign);
                                }
                            }
                           else if (s.IsStartofCareAssessment() || s.IsRecertificationAssessment() || s.IsResumptionofCareAssessment())
                            {
                                vitalSign.DisciplineTask = s.DisciplineTaskName;
                                var assessment = assessmentService.GetAssessment(s.EventId, Enum.GetName(typeof(DisciplineTasks), s.DisciplineTask));
                                if (assessment != null && assessment.OasisData.IsNotNullOrEmpty())
                                {
                                    var questions = assessment.ToDictionary();
                                    vitalSign.VisitDate = s.VisitDate.IsNotNullOrEmpty() && s.VisitDate.IsValidDate() ? s.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty;
                                    if (questions != null)
                                    {
                                        vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                        vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


                                        vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                        vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                        vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                        vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                        vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                        vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

                                      
                                        var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                        var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                        var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                        var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                        var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
                                        var maxBs = bs.Max();

                                        vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                        vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;

                                        vitalSign.BS = maxBs > 0 ? maxBs.ToString() : string.Empty; //questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                        vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                        vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                    }
                                    vitalSigns.Add(vitalSign);
                                }
                            }
                        }
                    });
                }
            }
            return vitalSigns;
        }

        public DisciplineTask GetDisciplineTask(int disciplineTaskId)
        {
            return lookupRepository.GetDisciplineTask(disciplineTaskId);
        }

        public List<PatientEpisodeTherapyException> GetTherapyException(Guid branchId)
        {
            var episodes = patientRepository.GetAllEpisodeAfterApril(Current.AgencyId, branchId);
            var therapyEpisodes = new List<PatientEpisodeTherapyException>();
            if (episodes != null && episodes.Count > 0)
            {
                episodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty())
                    {
                        var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventDate.IsValidDate() && (e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date && (e.IsDeprecated == false)) && (e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST")).OrderBy(e => e.EventDate.ToDateTime().Date).ToList();
                        if (scheduleEvents != null && scheduleEvents.Count > 0)
                        {
                            var disciplines = scheduleEvents.Where(e => e.Discipline.IsNotNullOrEmpty()).Select(e => e.Discipline).Distinct().ToArray();
                            if (disciplines.Length == 1)
                            {
                                var evnt19check = true;
                                var evnt13check = true;
                                if (scheduleEvents.Count >= 13)
                                {
                                    var evnt13 = scheduleEvents[12];
                                    if (evnt13.EventDate.IsValidDate())
                                    {
                                        var date13 = evnt13.EventDate.ToDateTime();
                                        var scheduleEvents13 = scheduleEvents.Where(e => (e.EventDate.ToDateTime().Date == date13.Date)).ToList();
                                        if (scheduleEvents13 != null)
                                        {
                                            if (evnt13.Discipline == "PT")
                                            {
                                                evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                            }
                                            else if (evnt13.Discipline == "OT")
                                            {
                                                evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                            }
                                            else if (evnt13.Discipline == "ST")
                                            {
                                                evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                            }
                                        }
                                    }
                                    if (evnt13 != null)
                                    {
                                        episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate.IsValidDate() ? evnt13.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
                                    }
                                    if (scheduleEvents.Count >= 19)
                                    {
                                        var evnt19 = scheduleEvents[18];
                                        if (evnt19.EventDate.IsValidDate())
                                        {
                                            var date19 = evnt19.EventDate.ToDateTime();
                                            var scheduleEvents19 = scheduleEvents.Where(e => (e.EventDate.ToDateTime().Date == date19.Date)).ToList();
                                            if (scheduleEvents19 != null)
                                            {
                                                if (evnt19.Discipline == "PT")
                                                {
                                                    evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                                }
                                                else if (evnt19.Discipline == "OT")
                                                {
                                                    evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                }
                                                else if (evnt19.Discipline == "ST")
                                                {
                                                    evnt13check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                }
                                            }
                                        }
                                        if (evnt19 != null)
                                        {
                                            episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate.IsValidDate() ? evnt19.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
                                        }

                                    }
                                    else
                                    {
                                        episode.NineteenVisit = "NA";
                                    }
                                    if (!evnt19check || !evnt13check)
                                    {
                                        episode.ScheduledTherapy = scheduleEvents.Count;
                                        var completedSchedules = scheduleEvents.Where(s => s.Status.IsInteger() && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisExported).ToString())).ToList();
                                        if (completedSchedules != null)
                                        {
                                            episode.CompletedTherapy = completedSchedules.Count;
                                        }
                                        episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                        therapyEpisodes.Add(episode);
                                    }
                                }
                            }
                            else if (disciplines.Length > 1)
                            {
                                var discipline = string.Join(";", disciplines);
                                var evnt19check = true;
                                var evnt13check = true;
                                var first = true;
                                if (scheduleEvents.Count >= 13 && discipline.IsNotNullOrEmpty())
                                {
                                    var scheduleEvents13 = scheduleEvents.Take(13).Reverse().ToList();
                                    var scheduleEvents13End = scheduleEvents13.Take(3).ToList();
                                    if (scheduleEvents13End != null)
                                    {
                                        if (discipline.Contains("PT"))
                                        {
                                            first = false;
                                            evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                        }
                                        if (discipline.Contains("OT"))
                                        {
                                            if (first)
                                            {
                                                first = false;
                                                evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                            }
                                            else
                                            {
                                                evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                            }
                                        }
                                        if (discipline.Contains("ST"))
                                        {
                                            if (first)
                                            {
                                                first = false;
                                                evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                            }
                                            else
                                            {
                                                evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                            }
                                        }
                                    }
                                    if (!evnt13check)
                                    {
                                        var evnt13 = scheduleEvents[12];
                                        if (evnt13 != null)
                                        {
                                            episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate.IsValidDate() ? evnt13.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
                                        }
                                    }
                                    first = true;
                                    if (scheduleEvents.Count >= 19)
                                    {
                                        var evnt19 = scheduleEvents[18];
                                        var scheduleEvents19 = scheduleEvents.Take(19).Reverse().ToList();
                                        var scheduleEvents19End = scheduleEvents19.Take(3).ToList();
                                        if (scheduleEvents19End != null)
                                        {
                                            if (discipline.Contains("PT"))
                                            {
                                                first = false;
                                                evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);

                                            }

                                            if (discipline.Contains("OT"))
                                            {
                                                if (first)
                                                {
                                                    first = false;
                                                    evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                }
                                                else
                                                {
                                                    evnt19check = evnt19check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                }
                                            }
                                            if (discipline.Contains("ST"))
                                            {
                                                if (first)
                                                {
                                                    first = false;
                                                    evnt13check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                }
                                                else
                                                {
                                                    evnt13check = evnt13check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                }
                                            }
                                        }
                                        if (evnt19 != null)
                                        {
                                            episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate.IsValidDate() ? evnt19.EventDate.ToDateTime().ToString("MM/dd") : string.Empty);
                                        }
                                    }
                                    if (!evnt19check || !evnt13check)
                                    {
                                        episode.ScheduledTherapy = scheduleEvents.Count;
                                        var completedSchedules = scheduleEvents.Where(s => s.Status.IsInteger() && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisExported).ToString())).ToList();
                                        if (completedSchedules != null)
                                        {
                                            episode.CompletedTherapy = completedSchedules.Count;
                                        }
                                        episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                        therapyEpisodes.Add(episode);
                                    }
                                }
                            }
                        }
                    }
                });
            }
            return therapyEpisodes.OrderBy(o => o.PatientName).ToList();
        }

        public List<PatientEpisodeTherapyException> GetTherapyReevaluationException(Guid branchId, int count)
        {
            var episodes = patientRepository.GetAllEpisodeAfterApril(Current.AgencyId, branchId);
            var therapyEpisodes = new List<PatientEpisodeTherapyException>();
            if (episodes != null && episodes.Count > 0)
            {
                episodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty())
                    {
                        var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventDate.IsValidDate() && (e.EventDate.ToDateTime().Date >= episode.StartDate.Date && e.EventDate.ToDateTime().Date <= episode.EndDate.Date && (e.IsDeprecated == false)) && (e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST")).OrderBy(e => e.EventDate.ToDateTime().Date).ToList();
                        if (scheduleEvents != null && scheduleEvents.Count > 0)
                        {
                            var disciplines = scheduleEvents.Where(e => e.Discipline.IsNotNullOrEmpty()).Select(e => e.Discipline).Distinct().ToArray();
                            if (disciplines.Length > 0)
                            {
                                var discipline = string.Join(";", disciplines);
                                if (scheduleEvents.Count >= count && discipline.IsNotNullOrEmpty())
                                {
                                    var evntCommon = scheduleEvents[count - 1];
                                    var scheduleEventsCommon = scheduleEvents.Take(count).Reverse().ToList();
                                    var scheduleEventsCommonEnd = scheduleEventsCommon.Take(3).ToList();
                                    if (scheduleEventsCommonEnd != null)
                                    {
                                        if (discipline.Contains("PT"))
                                        {
                                            var ptEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation);
                                            if (ptEval != null && ptEval.EventDate.IsValidDate())
                                            {
                                                episode.PTEval = ptEval.EventDate.ToDateTime().ToString("MM/dd");
                                            }
                                        }
                                        else
                                        {
                                            episode.PTEval = "NA";
                                        }
                                        if (discipline.Contains("OT"))
                                        {
                                            var otEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation);
                                            if (otEval != null && otEval.EventDate.IsValidDate())
                                            {
                                                episode.OTEval = otEval.EventDate.ToDateTime().ToString("MM/dd");
                                            }
                                        }
                                        else
                                        {
                                            episode.OTEval = "NA";
                                        }
                                        if (discipline.Contains("ST"))
                                        {
                                            var stEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation);
                                            if (stEval != null && stEval.EventDate.IsValidDate())
                                            {
                                                episode.STEval = stEval.EventDate.ToDateTime().ToString("MM/dd");
                                            }
                                        }
                                        else
                                        {
                                            episode.STEval = "NA";
                                        }
                                    }
                                    episode.ScheduledTherapy = scheduleEvents.Count;
                                    var completedSchedules = scheduleEvents.Where(s => s.Status.IsInteger() && (s.Status == ((int)ScheduleStatus.NoteCompleted).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisExported).ToString())).ToList();
                                    if (completedSchedules != null)
                                    {
                                        episode.CompletedTherapy = completedSchedules.Count;
                                    }
                                    episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                    therapyEpisodes.Add(episode);
                                }
                            }
                        }
                    }
                });
            }
            return therapyEpisodes.OrderBy(o => o.PatientName).ToList();
        }

        public List<TaskLog> GetTaskLogs(Guid patientId, Guid eventId, int task)
        {
            var taskLogs = new List<TaskLog>();
            var taskAudit = logRepository.GetTaskAudit(Current.AgencyId, patientId, eventId, task);
            if (taskAudit != null && taskAudit.Log.IsNotNullOrEmpty())
            {
                var logs = taskAudit.Log.ToObject<List<TaskLog>>();
                if (logs != null && logs.Count > 0)
                {
                    logs.ForEach(log =>
                    {
                        log.UserName = UserEngine.GetName(log.UserId, Current.AgencyId);
                        taskLogs.Add(log);
                    });
                }
            }
            return taskLogs;
        }

        public List<AppAudit> GetGeneralLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId)
        {
            var generalLogs = new List<AppAudit>();
            var logs = logRepository.GetGeneralAudits(Current.AgencyId, logDomain.ToString(), logType.ToString(), domainId, entityId.ToString());
            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(log =>
                {
                    log.UserName = UserEngine.GetName(log.UserId, Current.AgencyId);
                    generalLogs.Add(log);
                });
            }
            return generalLogs;
        }

        public List<AppAudit> GetMedicationLogs(LogDomain logDomain, LogType logType, Guid domainId)
        {
            var generalLogs = new List<AppAudit>();
            var logs = logRepository.GetMedicationAudits(Current.AgencyId, logDomain.ToString(), domainId);

            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(log =>
                {
                    log.UserName = UserEngine.GetName(log.UserId, Current.AgencyId);
                    generalLogs.Add(log);
                });
            }
            return generalLogs;
        }

        public IDictionary<Guid, string> GetPreviousSkilledNurseNotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                        .Where(s => s.EventId != Guid.Empty && s.EventId != scheduledEvent.EventId
                           && s.IsDeprecated == false && s.IsSkilledNurseNote()
                           && (int.Parse(s.Status) == (int)ScheduleStatus.NoteCompleted
                               || int.Parse(s.Status) == (int)ScheduleStatus.NoteSubmittedWithSignature)
                           && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date);

                    if (scheduleEvents != null)
                    {
                        previousNoteEvents.AddRange(scheduleEvents);
                    }
                }
            });

            previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
            previousNoteEvents.ForEach(s =>
            {
                previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
            });

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousPTNotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                        .Where(s => s.EventId != Guid.Empty && s.EventId != scheduledEvent.EventId
                           && s.IsDeprecated == false && s.IsPTNote()
                           && (int.Parse(s.Status) == (int)ScheduleStatus.NoteCompleted
                               || int.Parse(s.Status) == (int)ScheduleStatus.NoteSubmittedWithSignature)
                           && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date < scheduledEvent.EventDate.ToDateTime().Date);

                    if (scheduleEvents != null)
                    {
                        previousNoteEvents.AddRange(scheduleEvents);
                    }
                }
            });

            previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
            previousNoteEvents.ForEach(s =>
            {
                previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
            });

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousPTEvals(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                        .Where(s => s.EventId != Guid.Empty && s.EventId != scheduledEvent.EventId
                           && s.IsDeprecated == false && s.IsPTEval()
                           && (int.Parse(s.Status) == (int)ScheduleStatus.NoteCompleted
                               || int.Parse(s.Status) == (int)ScheduleStatus.NoteSubmittedWithSignature)
                           && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date < scheduledEvent.EventDate.ToDateTime().Date);

                    if (scheduleEvents != null)
                    {
                        previousNoteEvents.AddRange(scheduleEvents);
                    }
                }
            });

            previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
            previousNoteEvents.ForEach(s =>
            {
                previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
            });

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousPTDischarges(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                        .Where(s => s.EventId != Guid.Empty && s.EventId != scheduledEvent.EventId
                           && s.IsDeprecated == false && s.IsPTDischarge()
                           && (int.Parse(s.Status) == (int)ScheduleStatus.NoteCompleted
                               || int.Parse(s.Status) == (int)ScheduleStatus.NoteSubmittedWithSignature)
                           && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date < scheduledEvent.EventDate.ToDateTime().Date);

                    if (scheduleEvents != null)
                    {
                        previousNoteEvents.AddRange(scheduleEvents);
                    }
                }
            });

            previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
            previousNoteEvents.ForEach(s =>
            {
                previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
            });

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousOTNotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                        .Where(s => s.EventId != Guid.Empty && s.EventId != scheduledEvent.EventId
                           && s.IsDeprecated == false && s.IsOTNote()
                           && (int.Parse(s.Status) == (int)ScheduleStatus.NoteCompleted
                               || int.Parse(s.Status) == (int)ScheduleStatus.NoteSubmittedWithSignature)
                           && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date);

                    if (scheduleEvents != null)
                    {
                        previousNoteEvents.AddRange(scheduleEvents);
                    }
                }
            });

            previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
            previousNoteEvents.ForEach(s =>
            {
                previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
            });

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousOTEvals(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                        .Where(s => s.EventId != Guid.Empty && s.EventId != scheduledEvent.EventId
                           && s.IsDeprecated == false && s.IsOTEval()
                           && (int.Parse(s.Status) == (int)ScheduleStatus.NoteCompleted
                               || int.Parse(s.Status) == (int)ScheduleStatus.NoteSubmittedWithSignature)
                           && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date);

                    if (scheduleEvents != null)
                    {
                        previousNoteEvents.AddRange(scheduleEvents);
                    }
                }
            });

            previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
            previousNoteEvents.ForEach(s =>
            {
                previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
            });

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousSTNotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                        .Where(s => s.EventId != Guid.Empty && s.EventId != scheduledEvent.EventId
                           && s.IsDeprecated == false && s.IsSTNote()
                           && (int.Parse(s.Status) == (int)ScheduleStatus.NoteCompleted
                               || int.Parse(s.Status) == (int)ScheduleStatus.NoteSubmittedWithSignature)
                           && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date);

                    if (scheduleEvents != null)
                    {
                        previousNoteEvents.AddRange(scheduleEvents);
                    }
                }
            });

            previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
            previousNoteEvents.ForEach(s =>
            {
                previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
            });

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousSTEvals(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                        .Where(s => s.EventId != Guid.Empty && s.EventId != scheduledEvent.EventId
                           && s.IsDeprecated == false && s.IsSTEval()
                           && (int.Parse(s.Status) == (int)ScheduleStatus.NoteCompleted
                               || int.Parse(s.Status) == (int)ScheduleStatus.NoteSubmittedWithSignature)
                           && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date);

                    if (scheduleEvents != null)
                    {
                        previousNoteEvents.AddRange(scheduleEvents);
                    }
                }
            });

            previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
            previousNoteEvents.ForEach(s =>
            {
                previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
            });

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousMSWProgressNotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                        .Where(s => s.EventId != Guid.Empty && s.EventId != scheduledEvent.EventId
                           && s.IsDeprecated == false && s.IsMSWProgressNote()
                           && (int.Parse(s.Status) == (int)ScheduleStatus.NoteCompleted
                               || int.Parse(s.Status) == (int)ScheduleStatus.NoteSubmittedWithSignature)
                           && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date);

                    if (scheduleEvents != null)
                    {
                        previousNoteEvents.AddRange(scheduleEvents);
                    }
                }
            });

            previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
            previousNoteEvents.ForEach(s =>
            {
                previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
            });

            return previousNotesList;
        }

        public IDictionary<Guid, string> GetPreviousHHANotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var previousNoteEvents = new List<ScheduleEvent>();
            var previousNotesList = new Dictionary<Guid, string>();
            var patientEpisodes = patientRepository.GetPatientScheduledEvents(Current.AgencyId, patientId);
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                        .Where(s => s.EventId != Guid.Empty && s.EventId != scheduledEvent.EventId
                           && s.IsDeprecated == false && s.IsHhaNote()
                           && (int.Parse(s.Status) == (int)ScheduleStatus.NoteCompleted
                               || int.Parse(s.Status) == (int)ScheduleStatus.NoteSubmittedWithSignature)
                           && s.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date <= scheduledEvent.EventDate.ToDateTime().Date);

                    if (scheduleEvents != null)
                    {
                        previousNoteEvents.AddRange(scheduleEvents);
                    }
                }
            });

            previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToOrderedDate()).Take(5).ToList();
            previousNoteEvents.ForEach(s =>
            {
                previousNotesList.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
            });

            return previousNotesList;
        }

        public bool UpdateAllergy(Allergy allergy)
        {
            var result = false;
            if (allergy != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfile(allergy.ProfileId, Current.AgencyId);
                if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
                {
                    var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                    if (existingList.Exists(a => a.Id == allergy.Id))
                    {
                        var exisitingAllergy = existingList.Single(m => m.Id == allergy.Id);
                        if (exisitingAllergy != null)
                        {
                            exisitingAllergy.Name = allergy.Name;
                            exisitingAllergy.Type = allergy.Type;
                            allergyProfile.Allergies = existingList.ToXml<List<Allergy>>();

                            if (patientRepository.UpdateAllergyProfile(allergyProfile))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfile.Id.ToString(), LogType.AllergyProfile, LogAction.AllergyUpdated, string.Empty);
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateAllergy(Guid allergyProfileId, Guid allergyId, bool isDeleted)
        {
            var result = false;
            var allergyProfile = patientRepository.GetAllergyProfile(allergyProfileId, Current.AgencyId);
               if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
                {
                    var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                    if (existingList.Exists(a => a.Id == allergyId))
                    {
                        var allergy = existingList.Single(m => m.Id == allergyId);
                        if (allergy != null)
                        {
                            allergy.IsDeprecated = isDeleted;
                            allergyProfile.Allergies = existingList.ToXml<List<Allergy>>();

                            if (patientRepository.UpdateAllergyProfile(allergyProfile))
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfileId.ToString(), LogType.AllergyProfile, LogAction.AllergyDeleted, string.Empty);
                                result = true;
                            }
                        }
                   }
            }
            return result;
        }

        public bool CreateAllergyProfile(Guid patientId)
        {
            var result = false;

            return result;
        }

        public bool AddAllergy(Allergy allergy)
        {
            var result = false;
            if (allergy != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfile(allergy.ProfileId, Current.AgencyId);
                if (allergyProfile != null)
                {
                    allergy.Id = Guid.NewGuid();
                    if (allergyProfile.Allergies.IsNullOrEmpty())
                    {
                        var newList = new List<Allergy>() { allergy };
                        allergyProfile.Allergies = newList.ToXml();
                    }
                    else
                    {
                        var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                        existingList.Add(allergy);
                        allergyProfile.Allergies = existingList.ToXml();
                    }
                    if (patientRepository.UpdateAllergyProfile(allergyProfile))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, allergyProfile.PatientId, allergyProfile.Id.ToString(), LogType.AllergyProfile, LogAction.AllergyAdded, string.Empty);
                        result = true;
                    }
                }
            }

            return result;
        }

        public bool AddHospitalizationLog(FormCollection formCollection)
        {
            var result = false;

            var hospitalizationLog = new HospitalizationLog
            {
                Id = Guid.NewGuid(),
                UserId = formCollection.GetGuid("UserId"),
                PatientId = formCollection.GetGuid("PatientId"),
                EpisodeId = formCollection.GetGuid("EpisodeId"),
                HospitalizationDate = formCollection.GetString("M0906DischargeDate").IsNotNullOrEmpty() && formCollection.GetString("M0906DischargeDate").IsDate() ? formCollection.GetString("M0906DischargeDate").ToDateTime() : DateTime.MinValue,
                LastHomeVisitDate = formCollection.GetString("M0903LastHomeVisitDate").IsNotNullOrEmpty() && formCollection.GetString("M0903LastHomeVisitDate").IsDate() ? formCollection.GetString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue,
                Data = ProcessHospitalizationData(formCollection),
                Created = DateTime.Now,
                AgencyId = Current.AgencyId,
                SourceId = (int)TransferSourceTypes.User,
                Modified = DateTime.Now
            };

            if (patientRepository.AddHospitalizationLog(hospitalizationLog))
            {
                var patient = patientRepository.Get(hospitalizationLog.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    patient.IsHospitalized = true;
                    patient.HospitalizationId = hospitalizationLog.Id;
                    if (patientRepository.Update(patient))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateHospitalizationLog(FormCollection formCollection)
        {
            var result = false;

            var logId = formCollection.GetGuid("Id");
            var patientId = formCollection.GetGuid("PatientId");
            var hospitalizationLog = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, logId);
            if (hospitalizationLog != null)
            {
                hospitalizationLog.UserId = formCollection.GetGuid("UserId");
                hospitalizationLog.EpisodeId = formCollection.GetGuid("EpisodeId");
                hospitalizationLog.Modified = DateTime.Now;
                hospitalizationLog.HospitalizationDate = formCollection.GetString("M0906DischargeDate").IsNotNullOrEmpty() && formCollection.GetString("M0906DischargeDate").IsDate() ? formCollection.GetString("M0906DischargeDate").ToDateTime() : DateTime.MinValue;
                hospitalizationLog.LastHomeVisitDate = formCollection.GetString("M0903LastHomeVisitDate").IsNotNullOrEmpty() && formCollection.GetString("M0903LastHomeVisitDate").IsDate() ? formCollection.GetString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue;
                hospitalizationLog.Data = ProcessHospitalizationData(formCollection);

                if (patientRepository.UpdateHospitalizationLog(hospitalizationLog))
                {
                    result = true;
                }
            }

            return result;
        }

        public List<HospitalizationLog> GetHospitalizationLogs(Guid agencyId, Guid patientId)
        {
            var logs = patientRepository.GetHospitalizationLogs(patientId, Current.AgencyId);
            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(l =>
                {
                    l.PrintUrl = "<a onclick=\"U.GetAttachment('Patient/HospitalizationLogPdf', { 'patientId': '" + l.PatientId + "', 'hospitalizationLogId': '" + l.Id + "' });return false\"><span class=\"img icon print\"></span></a>";
                });
            }
            return logs;
        }

        public HospitalizationLog GetHospitalizationLog(Guid patientId, Guid hospitalizationLogId)
        {
            var log = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, hospitalizationLogId);
            if (log != null)
            {
                log.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                log.Patient = patientRepository.GetPatientOnly(log.PatientId, Current.AgencyId);

                if (!log.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, log.EpisodeId, log.PatientId);
                    if (episode != null)
                    {
                        log.EpisodeRange = string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted);
                    }
                }
            }
            return log;
        }

        public List<NonAdmit> GetNonAdmits()
        {
            var list = new List<NonAdmit>();
            var patients = patientRepository.FindPatientOnly((int)PatientStatus.NonAdmission, Current.AgencyId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    list.Add(new NonAdmit
                    {
                        Id = p.Id,
                        LastName = p.LastName,
                        FirstName = p.FirstName,
                        DateOfBirth = p.DOBFormatted,
                        Phone = p.PhoneHomeFormatted,
                        Type = NonAdmitTypes.Patient,
                        MiddleInitial = p.MiddleInitial,
                        PatientIdNumber = p.PatientIdNumber,
                        NonAdmissionReason = p.NonAdmissionReason,
                        NonAdmitDate = p.NonAdmissionDateFormatted
                    });
                });
            }

            var referrals = referralRepository.GetAll(Current.AgencyId, ReferralStatus.NonAdmission).ToList();
            if (referrals != null && referrals.Count > 0)
            {
                referrals.ForEach(r =>
                {
                    list.Add(new NonAdmit
                    {
                        Id = r.Id,
                        LastName = r.LastName,
                        FirstName = r.FirstName,
                        DateOfBirth = r.DOBFormatted,
                        Phone = r.PhoneHomeFormatted,
                        Type = NonAdmitTypes.Referral,
                        PatientIdNumber = string.Empty,
                        NonAdmissionReason = r.NonAdmissionReason,
                        NonAdmitDate = r.NonAdmissionDateFormatted
                    });
                });
            }

            return list.OrderByDescending(l => l.NonAdmitDate).ToList();
        }

        public List<PendingPatient> GetPendingPatients()
        {
            var patients = patientRepository.GetPendingByAgencyId(Current.AgencyId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    if (p.PrimaryInsurance.IsNotNullOrEmpty())
                    {
                        var insurance = InsuranceEngine.Instance.Get(p.PrimaryInsurance.ToInteger(), Current.AgencyId);
                        if (insurance != null && insurance.Name.IsNotNullOrEmpty())
                        {
                            p.PrimaryInsuranceName = insurance.Name;
                        }
                    }
                });
            }

            return patients;
        }

        #endregion

        #region Private Methods

        private void SetInsurance(Patient patient)
        {
            if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
            {
                if (patient.PrimaryInsurance.ToInteger() < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.PrimaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.PrimaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId,patient.PrimaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.PrimaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
            if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger())
            {
                if (patient.SecondaryInsurance.ToInteger() < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.SecondaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.SecondaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.SecondaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.SecondaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
            if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger())
            {
                if (patient.TertiaryInsurance.ToInteger() < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.TertiaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.TertiaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.TertiaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.TertiaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
        }

        private void ProcessSchedule(ScheduleEvent scheduleEvent, Patient patient, PatientEpisode episode)
        {
            switch (((DisciplineTasks)scheduleEvent.DisciplineTask).ToString())
            {
                case "OASISCDeath":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;

                case "OASISCDeathOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;

                case "OASISCDeathPT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;

                case "OASISCDischarge":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;

                case "OASISCDischargeOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;

                case "OASISCDischargePT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;

                case "NonOASISDischarge":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.NonOasisDischarge, episode);
                    break;

                case "OASISCFollowUp":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.FollowUp, episode);
                    break;

                case "OASISCFollowupPT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.FollowUp, episode);
                    break;

                case "OASISCFollowupOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.FollowUp, episode);
                    break;

                case "OASISCRecertification":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedRecertification = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    var assessmentRecertification = assessmentService.AddAssessment(scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertification);
                    break;

                case "OASISCRecertificationPT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedRecertificationPT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    var assessmentRecertificationPT = assessmentService.AddAssessment(scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertificationPT);
                    break;

                case "OASISCRecertificationOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedRecertificationOT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    var assessmentRecertificationOT = assessmentService.AddAssessment(scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertificationOT);
                    break;

                case "SNAssessmentRecert":
                case "NonOASISRecertification":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    var currentMedNonOasisRecertification = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    var assessmentNonOasisRecertification = assessmentService.AddAssessment(scheduleEvent, AssessmentType.NonOasisRecertification, episode, currentMedNonOasisRecertification);
                    break;

                case "OASISCResumptionofCare":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedResumptionofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCare);
                    }
                    break;

                case "OASISCResumptionofCarePT":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.PT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedResumptionofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCare);
                    }
                    break;

                case "OASISCResumptionofCareOT":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.OT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedResumptionofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCare);
                    }
                    break;

                case "OASISCStartofCare":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        var assessment = assessmentService.AddAssessment(scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCare);
                    }
                    break;

                case "OASISCStartofCarePT":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.PT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        var assessment = assessmentService.AddAssessment(scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCare);
                    }
                    break;

                case "OASISCStartofCareOT":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.OT.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        var assessment = assessmentService.AddAssessment(scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCare);
                    }
                    break;  

                case "SNAssessment":
                case "NonOASISStartofCare":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                        scheduleEvent.IsBillable = true;
                        var currentMedNonOasisStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        var assessment = assessmentService.AddAssessment(scheduleEvent, AssessmentType.NonOasisStartOfCare, episode, currentMedNonOasisStartofCare);
                    }
                    break;

                case "OASISCTransferDischarge":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientDischarged, episode);
                    break;
                case "OASISCTransfer":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;
                case "OASISCTransferPT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;

                case "OASISCTransferOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;

                case "SkilledNurseVisit":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":
                case "SNDiabeticDailyVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNote);
                    break;
                case "LVNSupervisoryVisit":
                case "HHAideSupervisoryVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var snNoteNonebillableNursing = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableNursing);
                    break;
                case "PTEvaluation":
                case "PTReEvaluation":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    var ptEval = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(ptEval);
                    break;
                case "PTVisit":
                case "PTDischarge":
                case "PTAVisit":
                case "PTMaintenance":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillablePT = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillablePT);
                    break;
                case "OTEvaluation":
                case "OTReEvaluation":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    var otEval = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(otEval);
                    break;
                case "OTDischarge":
                case "OTVisit":
                case "COTAVisit":
                case "OTMaintenance":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillableOT = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableOT);
                    break;
                case "STEvaluation":
                case "STReEvaluation":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    var stEval = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(stEval);
                    break;
                case "STVisit":
                case "STDischarge":
                case "STMaintenance":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillableST = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableST);
                    break;
                case "MSWEvaluationAssessment":
                case "MSWVisit":
                case "MSWDischarge":
                case "MSWAssessment":
                case "MSWProgressNote":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    var snNoteNonebillableMSW = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableMSW);
                    break;
                case "DriverOrTransportationNote":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    var driverOrTransportationNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(driverOrTransportationNote);
                    break;
                case "DieticianVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Dietician.ToString();
                    scheduleEvent.IsBillable = false;
                    var snNoteNonebillableDietician = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNoteNonebillableDietician);
                    break;
                case "HHAideVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    var hhAideVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(hhAideVisit);
                    break;
                case "HHAideCarePlan":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var hhAideCarePlan = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(hhAideCarePlan);
                    break;
                case "PASVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    var pasVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(pasVisit);
                    break;
                case "PASCarePlan":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = false;
                    var pasCarePlan = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(pasCarePlan);
                    break;
                case "DischargeSummary":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var physician = physicianRepository.GetPatientPhysicians(patient.Id, Current.AgencyId).SingleOrDefault(p => p.Primary);
                    var dischargeSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    if (physician != null)
                    {
                        var questions = new List<NotesQuestion>();
                        questions.Add(new NotesQuestion { Name = "Physician", Answer = Convert.ToString(physician.Id), Type = "DischargeSummary" });
                        scheduleEvent.Questions = questions;
                        dischargeSummary.Note = questions.ToXml();
                        patientRepository.AddVisitNote(dischargeSummary);
                    }
                    else
                    {
                        patientRepository.AddVisitNote(dischargeSummary);
                    }
                    break;

                case "PhysicianOrder":
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    var order = new PhysicianOrder { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, OrderDate = scheduleEvent.EventDate.ToDateTime(), Created = DateTime.Now, Text = "", Summary = "", Status = int.Parse(scheduleEvent.Status), OrderNumber = patientRepository.GetNextOrderNumber() };
                    if (patient.PhysicianContacts!=null && patient.PhysicianContacts.Count > 0)
                    {
                        order.PhysicianId = patient.PhysicianContacts[0].Id;
                    }
                    patientRepository.AddOrder(order);
                    break;

                case "HCFA485StandAlone":
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderSaved).ToString();
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    var planofCare = new PlanofCareStandAlone { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, Status = int.Parse(scheduleEvent.Status), OrderNumber = patientRepository.GetNextOrderNumber() };
                    planofCare.Questions = new List<Question>();
                    planofCare.Data = planofCare.Questions.ToXml();
                    planofCareRepository.AddStandAlone(planofCare);
                    break;

                case "NonOasisHCFA485":
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderSaved).ToString();
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;

                case "HCFA485":
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderSaved).ToString();
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;

                case "HCFA486":
                case "PostHospitalizationOrder":
                case "MedicaidPOC":
                    scheduleEvent.IsBillable = false;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderNotYetDue).ToString();
                    break;
                case "IncidentAccidentReport":
                    scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCreated).ToString();
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    var incidentReport = new Incident { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, IncidentDate = scheduleEvent.EventDate.ToDateTime(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.ReportAndNotesCreated), UserId = scheduleEvent.UserId };
                    agencyRepository.AddIncident(incidentReport);
                    break;
                case "InfectionReport":
                    scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCreated).ToString();
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    var infectionReport = new Infection { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, InfectionDate = scheduleEvent.EventDate.ToDateTime(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.ReportAndNotesCreated), UserId = scheduleEvent.UserId };
                    agencyRepository.AddInfection(infectionReport);
                    break;
                case "Rap":
                    {
                        var pocAssessment = assessmentService.GetEpisodeAssessment(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventDate.ToDateTime());

                        var newRap = new Rap
                        {
                            Id = episode.Id,
                            AgencyId = patient.AgencyId,
                            PatientId = patient.Id,
                            EpisodeId = episode.Id,
                            EpisodeStartDate = episode.StartDate,
                            EpisodeEndDate = episode.EndDate,
                            IsFirstBillableVisit = false,
                            IsOasisComplete = false,
                            PatientIdNumber = patient.PatientIdNumber,
                            IsGenerated = false,
                            MedicareNumber = patient.MedicareNumber,
                            FirstName = patient.FirstName,
                            LastName = patient.LastName,
                            DOB = patient.DOB,
                            Gender = patient.Gender,
                            AddressLine1 = patient.AddressLine1,
                            AddressLine2 = patient.AddressLine2,
                            AddressCity = patient.AddressCity,
                            AddressStateCode = patient.AddressStateCode,
                            AddressZipCode = patient.AddressZipCode,
                            StartofCareDate = episode.StartOfCareDate,
                            AdmissionSource = patient.AdmissionSource,
                            PatientStatus = patient.Status,
                            UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
                            AreOrdersComplete = false,
                            Status = (int)ScheduleStatus.ClaimCreated,
                            Created = DateTime.Now,
                            ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>"
                        };
                        if (patient.PhysicianContacts != null)
                        {
                            var contact = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                            if (contact != null)
                            {
                                newRap.PhysicianNPI = contact.NPI;
                                newRap.PhysicianFirstName = contact.FirstName;
                                newRap.PhysicianLastName = contact.LastName;
                            }
                        }
                        if (pocAssessment != null)
                        {
                            var status = pocAssessment.Status;
                            if (status == 9 && status == 10)
                            {
                                var assessment = assessmentService.GetEpisodeAssessment(episode.Id, patient.Id);
                                if (assessment != null)
                                {
                                    var assessmentQuestions = assessment.ToDictionary();
                                    newRap.IsOasisComplete = true;
                                    newRap.IsFirstBillableVisit = true;
                                    if (assessmentQuestions["M0030SocDate"] != null && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty())
                                    {
                                        newRap.StartofCareDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                                        newRap.FirstBillableVisitDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                                    }
                                    string diagnosis = "<DiagonasisCodes>";
                                    if (assessmentQuestions["M1020ICD9M"] != null)
                                    {
                                        diagnosis += "<code1>" + assessmentQuestions["M1020ICD9M"].Answer + "</code1>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code1></code1>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M1"] != null)
                                    {
                                        diagnosis += "<code2>" + assessmentQuestions["M1022ICD9M1"].Answer + "</code2>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code2></code2>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M2"] != null)
                                    {
                                        diagnosis += "<code3>" + assessmentQuestions["M1022ICD9M2"].Answer + "</code3>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code3></code3>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M3"] != null)
                                    {
                                        diagnosis += "<code4>" + assessmentQuestions["M1022ICD9M3"].Answer + "</code4>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code4></code4>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M4"] != null)
                                    {
                                        diagnosis += "<code5>" + assessmentQuestions["M1022ICD9M4"].Answer + "</code5>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code5></code5>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M5"] != null)
                                    {
                                        diagnosis += "<code6>" + assessmentQuestions["M1022ICD9M5"].Answer + "</code6>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code6></code6>";
                                    }
                                    diagnosis += "</DiagonasisCodes>";
                                    newRap.DiagnosisCode = diagnosis;
                                    newRap.HippsCode = pocAssessment.HippsCode;
                                    newRap.ClaimKey = pocAssessment.ClaimKey;
                                }
                                else
                                {
                                    newRap.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                                }
                            }
                        }
                        else
                        {
                            newRap.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                        }
                        newRap.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";

                        billingRepository.AddRap(newRap);
                    }
                    break;

                case "Final":
                    {
                        var pocAssessment = assessmentService.GetEpisodeAssessment(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventDate.ToDateTime());
                        var newFinal = new Final
                        {
                            AgencyId = patient.AgencyId,
                            PatientId = patient.Id,
                            EpisodeId = episode.Id,
                            EpisodeStartDate = episode.StartDate,
                            EpisodeEndDate = episode.EndDate,
                            IsFirstBillableVisit = false,
                            IsOasisComplete = false,
                            PatientIdNumber = patient.PatientIdNumber,
                            IsGenerated = false,
                            MedicareNumber = patient.MedicareNumber,
                            FirstName = patient.FirstName,
                            LastName = patient.LastName,
                            DOB = patient.DOB,
                            Gender = patient.Gender,
                            AddressLine1 = patient.AddressLine1,
                            AddressLine2 = patient.AddressLine2,
                            AddressCity = patient.AddressCity,
                            AddressStateCode = patient.AddressStateCode,
                            AddressZipCode = patient.AddressZipCode,
                            StartofCareDate = episode.StartOfCareDate,
                            AdmissionSource = patient.AdmissionSource,
                            PatientStatus = patient.Status,
                            UB4PatientStatus=patient.Status==1? "30": (patient.Status==2?"01": string.Empty),
                            AreOrdersComplete = false,
                            Status = (int)ScheduleStatus.ClaimCreated,
                            Created = DateTime.Now,
                            ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>"
                        };
                        if (patient.PhysicianContacts != null)
                        {
                            var contact = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                            if (contact != null)
                            {
                                newFinal.PhysicianNPI = contact.NPI;
                                newFinal.PhysicianFirstName = contact.FirstName;
                                newFinal.PhysicianLastName = contact.LastName;
                            }
                        }
                        if (pocAssessment != null)
                        {
                            var status = pocAssessment.Status;
                            if (status == 9 && status == 10)
                            {
                                var assessment = assessmentService.GetEpisodeAssessment(episode.Id, patient.Id);
                                if (assessment != null)
                                {
                                    var assessmentQuestions = assessment.ToDictionary();
                                    newFinal.IsOasisComplete = true;
                                    newFinal.IsFirstBillableVisit = true;
                                    if (assessmentQuestions["M0030SocDate"] != null && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty())
                                    {
                                        newFinal.StartofCareDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                                        newFinal.FirstBillableVisitDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                                    }
                                    string diagnosis = "<DiagonasisCodes>";
                                    if (assessmentQuestions["M1020ICD9M"] != null)
                                    {
                                        diagnosis += "<code1>" + assessmentQuestions["M1020ICD9M"].Answer + "</code1>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code1></code1>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M1"] != null)
                                    {
                                        diagnosis += "<code2>" + assessmentQuestions["M1022ICD9M1"].Answer + "</code2>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code2></code2>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M2"] != null)
                                    {
                                        diagnosis += "<code3>" + assessmentQuestions["M1022ICD9M2"].Answer + "</code3>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code3></code3>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M3"] != null)
                                    {
                                        diagnosis += "<code4>" + assessmentQuestions["M1022ICD9M3"].Answer + "</code4>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code4></code4>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M4"] != null)
                                    {
                                        diagnosis += "<code5>" + assessmentQuestions["M1022ICD9M4"].Answer + "</code5>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code5></code5>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M5"] != null)
                                    {
                                        diagnosis += "<code6>" + assessmentQuestions["M1022ICD9M5"].Answer + "</code6>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code6></code6>";
                                    }
                                    diagnosis += "</DiagonasisCodes>";
                                    newFinal.DiagnosisCode = diagnosis;
                                    newFinal.HippsCode = pocAssessment.HippsCode;
                                    newFinal.ClaimKey = pocAssessment.ClaimKey;
                                }
                                else
                                {
                                    newFinal.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                                }
                            }
                        }
                        else
                        {
                            newFinal.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                        }
                        newFinal.DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";

                        billingRepository.AddFinal(newFinal);
                    }
                    break;

                case "SixtyDaySummary":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var sixtyDaySummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(sixtyDaySummary);
                    break;

                case "TransferSummary":
                case "CoordinationOfCare":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    var transferSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(transferSummary);
                    break;


                case "CommunicationNote":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    var comNote = new CommunicationNote { Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, AgencyId = Current.AgencyId, UserId = scheduleEvent.UserId, Status = ((int)ScheduleStatus.NoteNotYetDue), Created = scheduleEvent.EventDate.ToDateTime(), Modified = DateTime.Now };
                    patientRepository.AddCommunicationNote(comNote);
                    break;
                case "FaceToFaceEncounter":
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderNotYetDue).ToString();
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    var faceToFaceEncounter = new FaceToFaceEncounter { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, RequestDate = scheduleEvent.EventDate.ToDateTime(), Created = DateTime.Now, Modified = DateTime.Now,  Status = int.Parse(scheduleEvent.Status), OrderNumber = patientRepository.GetNextOrderNumber() };
                    if (patient.PhysicianContacts.Count > 0)
                    {
                        faceToFaceEncounter.PhysicianId = patient.PhysicianContacts[0].Id;
                    }
                    patientRepository.AddFaceToFaceEncounter(faceToFaceEncounter);
                    break;
            }
        }

        private bool UpdateScheduleEntity(Guid eventId, Guid episodeId, Guid patientId, string taskName, bool isDeprecated)
        {
            bool result = false;
            switch (taskName)
            {
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                case "NonOASISDischarge":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                case "NonOASISRecertification":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                case "NonOASISStartofCare":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "OASISCTransferDischarge":
                case "SNAssessment":
                case "SNAssessmentRecert":
                    result = assessmentService.MarkAsDeleted(eventId, episodeId, patientId, taskName, isDeprecated);
                    break;
                case "SkilledNurseVisit":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "LVNSupervisoryVisit":
                case "PTEvaluation":
                case "PTVisit":
                case "PTDischarge":
                case "OTEvaluation":
                case "OTReEvaluation":
                case "OTDischarge":
                case "OTVisit":
                case "STVisit":
                case "STEvaluation":
                case "STReEvaluation":
                case "STDischarge":
                case "MSWEvaluationAssessment":
                case "HHAideSupervisoryVisit":
                case "MSWVisit":
                case "MSWDischarge":
                case "DieticianVisit":
                case "PTAVisit":
                case "PTReEvaluation":
                case "COTAVisit":
                case "MSWAssessment":
                case "MSWProgressNote":
                case "HHAideVisit":
                case "HHAideCarePlan":
                case "PASVisit":
                case "PASCarePlan":
                case "DischargeSummary":
                case "SixtyDaySummary":
                case "TransferSummary":
                case "CoordinationOfCare":
                case "PTMaintenance":
                case "OTMaintenance":
                case "STMaintenance":
                case "DriverOrTransportationNote":
                case "SNDiabeticDailyVisit":
                    result = patientRepository.MarkVisitNoteAsDeleted(Current.AgencyId, episodeId, patientId, eventId, isDeprecated);
                    break;
                case "PhysicianOrder":
                    result = patientRepository.MarkOrderAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case "HCFA485":
                case "NonOasisHCFA485":
                    result = assessmentService.MarkPlanOfCareAsDeleted(eventId, episodeId, patientId, isDeprecated);
                    break;
                case "HCFA485StandAlone":
                    result = assessmentService.MarkPlanOfCareStandAloneAsDeleted(eventId, episodeId, patientId, isDeprecated);
                    break;
                case "IncidentAccidentReport":
                    result = agencyRepository.MarkIncidentAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case "InfectionReport":
                    result = agencyRepository.MarkInfectionsAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case "CommunicationNote":
                    result = patientRepository.DeleteCommunicationNote(Current.AgencyId, eventId, patientId, isDeprecated);
                    break;
                case "FaceToFaceEncounter":
                    result = patientRepository.DeleteFaceToFaceEncounter(Current.AgencyId, patientId, eventId, isDeprecated);
                    break;
            }
            return result;
        }

        private bool ReassignScheduleEntity(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId, string taskName)
        {
            bool result = false;
            switch (taskName)
            {
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                case "NonOASISDischarge":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                case "NonOASISRecertification":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                case "NonOASISStartofCare":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "OASISCTransferDischarge":
                case "SNAssessment":
                case "SNAssessmentRecert":
                    result = assessmentService.ReassignUser(episodeId, patientId, eventId, employeeId, taskName);
                    break;
                case "SkilledNurseVisit":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":

                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":

                case "LVNSupervisoryVisit":
                case "PTEvaluation":
                case "PTVisit":
                case "PTDischarge":
                case "OTEvaluation":
                case "OTReEvaluation":
                case "OTDischarge":
                case "OTVisit":
                case "STVisit":
                case "STEvaluation":
                case "STReEvaluation":
                case "STDischarge":
                case "MSWEvaluationAssessment":
                case "HHAideSupervisoryVisit":
                case "MSWVisit":
                case "MSWDischarge":
                case "DieticianVisit":
                case "PTAVisit":
                case "PTReEvaluation":
                case "COTAVisit":
                case "MSWAssessment":
                case "MSWProgressNote":
                case "HHAideVisit":
                case "HHAideCarePlan":
                case "DischargeSummary":
                case "SixtyDaySummary":
                case "TransferSummary":
                case "CoordinationOfCare":
                case "PTMaintenance":
                case "OTMaintenance":
                case "STMaintenance":
                case "DriverOrTransportationNote":
                case "SNDiabeticDailyVisit":
                    result = patientRepository.ReassignNotesUser(Current.AgencyId, episodeId, patientId, eventId, employeeId);
                    break;
                case "PhysicianOrder":
                    result = patientRepository.ReassignOrdersUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case "HCFA485":
                case "NonOasisHCFA485":
                    result = assessmentService.ReassignPlanOfCaresUser(Current.AgencyId, episodeId, patientId, eventId, employeeId);
                    break;
                case "IncidentAccidentReport":
                    result = agencyRepository.ReassignIncidentUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case "InfectionReport":
                    result = agencyRepository.ReassignInfectionsUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case "CommunicationNote":
                    result = patientRepository.ReassignCommunicationNoteUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case "FaceToFaceEncounter":
                    result = patientRepository.ReassignFaceToFaceEncounterUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
            }
            return result;
        }

        private bool ProcessEditDetail(ScheduleEvent schedule)
        {
            bool result = false;
            var type = ((DisciplineTasks)schedule.DisciplineTask).ToString();
            switch (type)
            {
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                case "NonOASISDischarge":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                case "NonOASISRecertification":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                case "NonOASISStartofCare":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "OASISCTransferDischarge":
                case "SNAssessment":
                case "SNAssessmentRecert":
                    if (schedule.DisciplineTask == (int)DisciplineTasks.OASISCRecertification
                        || schedule.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification)
                    {
                        if (int.Parse(schedule.Status) == (int)ScheduleStatus.OasisCompletedPendingReview
                            || int.Parse(schedule.Status) == (int)ScheduleStatus.OasisCompletedExportReady
                            || int.Parse(schedule.Status) == (int)ScheduleStatus.OasisExported)
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, true);
                        }
                        else
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, false);
                        }
                    }
                    result = assessmentService.UpdateAssessmentForDetail(schedule);
                    break;
                case "SkilledNurseVisit":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":
                case "SNDiabeticDailyVisit":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.NoteType = type;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                    
                case "PTEvaluation":
                case "PTVisit":
                case "PTDischarge":
                case "OTEvaluation":
                case "OTReEvaluation":
                case "OTDischarge":
                case "OTVisit":
                case "STVisit":
                case "STEvaluation":
                case "STReEvaluation":
                case "STDischarge":
                case "PTAVisit":
                case "PTReEvaluation":
                case "COTAVisit":
                case "PTMaintenance":
                case "OTMaintenance":
                case "STMaintenance":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                notesQuestions.Values.ForEach(q => { q.Type = type; });
                                visitNote.NoteType = type;
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;


                case "LVNSupervisoryVisit":
                case "MSWEvaluationAssessment":
                case "HHAideSupervisoryVisit":
                case "MSWVisit":
                case "MSWDischarge":
                case "DieticianVisit":
                case "MSWAssessment":
                case "MSWProgressNote":
                case "HHAideVisit":
                case "DriverOrTransportationNote":
                case "PASVisit":
                case "PASCarePlan":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;

                case "HHAideCarePlan":
                case "DischargeSummary":
                case "SixtyDaySummary":
                case "TransferSummary":
                case "CoordinationOfCare":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "PhysicianOrder":
                    {
                        var physicianOrder = patientRepository.GetOrderOnly(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (physicianOrder != null)
                        {
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                physicianOrder.OrderDate = schedule.VisitDate.ToDateTime();
                            }
                            if (physicianOrder.Status.ToString() != schedule.Status && physicianOrder.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    physicianOrder.PhysicianData = physician.ToXml();
                                }
                            }
                            physicianOrder.PhysicianId = schedule.PhysicianId;
                            physicianOrder.IsDeprecated = schedule.IsDeprecated;
                            result = patientRepository.UpdateOrderModel(physicianOrder);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "HCFA485":
                case "NonOasisHCFA485":
                    result = assessmentService.UpdatePlanOfCareForDetail(schedule);
                    break;
                case "HCFA485StandAlone":
                    result = assessmentService.UpdatePlanOfCareStandAloneForDetail(schedule);
                    break;
                case "FaceToFaceEncounter":
                    var faceToFace = patientRepository.GetFaceToFaceEncounter(schedule.EventId, Current.AgencyId);
                    if (faceToFace != null)
                    {
                        if (faceToFace.Status.ToString() != schedule.Status && faceToFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                faceToFace.PhysicianData = physician.ToXml();
                            }
                        }
                        faceToFace.PhysicianId = schedule.PhysicianId;
                        faceToFace.IsDeprecated = schedule.IsDeprecated;
                        faceToFace.RequestDate = schedule.VisitDate.ToDateTime();
                        if (patientRepository.UpdateFaceToFaceEncounter(faceToFace))
                        {
                            result = true;
                        }
                    }
                    break;
                case "IncidentAccidentReport":
                    {
                        var accidentReport = agencyRepository.GetIncidentReport(Current.AgencyId, schedule.EventId);
                        if (accidentReport != null)
                        {
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                accidentReport.IncidentDate = schedule.VisitDate.ToDateTime();
                            }
                            accidentReport.IsDeprecated = schedule.IsDeprecated;
                            result = agencyRepository.UpdateIncidentModal(accidentReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "InfectionReport":
                    {
                        var infectionReport = agencyRepository.GetInfectionReport(Current.AgencyId, schedule.EventId);
                        if (infectionReport != null)
                        {
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                infectionReport.InfectionDate = schedule.VisitDate.ToDateTime();
                            }
                            infectionReport.IsDeprecated = schedule.IsDeprecated;
                            result = agencyRepository.UpdateInfectionModal(infectionReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "CommunicationNote":
                    {
                        var comNote = patientRepository.GetCommunicationNote(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (comNote != null)
                        {
                            comNote.IsDeprecated = schedule.IsDeprecated;
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                comNote.Created = schedule.VisitDate.ToDateTime();
                            }
                            if (comNote.Status.ToString() != schedule.Status && comNote.Status == (int)ScheduleStatus.NoteCompleted && !comNote.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(comNote.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    comNote.PhysicianData = physician.ToXml();
                                }
                            }
                            result = patientRepository.UpdateCommunicationNoteModal(comNote);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
            }
            return result;
        }

        private bool ProcessEditDetailForReassign(ScheduleEvent schedule)
        {
            bool result = false;
            var type = ((DisciplineTasks)schedule.DisciplineTask).ToString();
            switch (type)
            {
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                case "NonOASISDischarge":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                case "NonOASISRecertification":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                case "NonOASISStartofCare":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "OASISCTransferDischarge":
                case "SNAssessment":
                case "SNAssessmentRecert":
                    if (schedule.DisciplineTask == (int)DisciplineTasks.OASISCRecertification
                        || schedule.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification)
                    {
                        if (int.Parse(schedule.Status) == (int)ScheduleStatus.OasisCompletedPendingReview
                            || int.Parse(schedule.Status) == (int)ScheduleStatus.OasisCompletedExportReady
                            || int.Parse(schedule.Status) == (int)ScheduleStatus.OasisExported)
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, true);
                        }
                        else
                        {
                            patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, false);
                        }
                    }
                    result = assessmentService.UpdateAssessmentForDetail(schedule);
                    break;
                case "SkilledNurseVisit":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":
                case "SNDiabeticDailyVisit":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.NoteType = type;
                                visitNote.EpisodeId = schedule.EpisodeId;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;


                case "PTEvaluation":
                case "PTVisit":
                case "PTDischarge":
                case "OTEvaluation":
                case "OTReEvaluation":
                case "OTDischarge":
                case "OTVisit":
                case "STVisit":
                case "STEvaluation":
                case "STReEvaluation":
                case "STDischarge":
                case "PTAVisit":
                case "PTReEvaluation":
                case "COTAVisit":
                case "PTMaintenance":
                case "OTMaintenance":
                case "STMaintenance":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                notesQuestions.Values.ForEach(q => { q.Type = type; });
                                visitNote.NoteType = type;
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.EpisodeId = schedule.EpisodeId;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "LVNSupervisoryVisit":
                case "MSWEvaluationAssessment":
                case "HHAideSupervisoryVisit":
                case "MSWVisit":
                case "MSWDischarge":
                case "DieticianVisit":
                case "MSWAssessment":
                case "MSWProgressNote":
                case "HHAideVisit":
                case "DriverOrTransportationNote":
                case "PASVisit":
                case "PASCarePlan":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.EpisodeId = schedule.EpisodeId;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;

                case "HHAideCarePlan":
                case "DischargeSummary":
                case "SixtyDaySummary":
                case "TransferSummary":
                case "CoordinationOfCare":
                    {
                        var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                        if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                        {
                            var notesQuestions = visitNote.ToDictionary();
                            if (notesQuestions != null)
                            {
                                if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate; } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate, Name = "VisitDate", Type = type }); }
                                visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                visitNote.IsDeprecated = schedule.IsDeprecated;
                                visitNote.EpisodeId = schedule.EpisodeId;
                                result = patientRepository.UpdateVisitNote(visitNote);
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "PhysicianOrder":
                    {
                        var physicianOrder = patientRepository.GetOrderOnly(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (physicianOrder != null)
                        {
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                physicianOrder.OrderDate = schedule.VisitDate.ToDateTime();
                            }
                            if (physicianOrder.Status.ToString() != schedule.Status && physicianOrder.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    physicianOrder.PhysicianData = physician.ToXml();
                                }
                            }
                            physicianOrder.PhysicianId = schedule.PhysicianId;
                            physicianOrder.IsDeprecated = schedule.IsDeprecated;
                            physicianOrder.EpisodeId = schedule.EpisodeId;
                            result = patientRepository.UpdateOrderModel(physicianOrder);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "HCFA485":
                case "NonOasisHCFA485":
                    result = assessmentService.UpdatePlanOfCareForDetail(schedule);
                    break;
                case "HCFA485StandAlone":
                    result = assessmentService.UpdatePlanOfCareStandAloneForDetail(schedule);
                    break;
                case "FaceToFaceEncounter":
                    var faceToFace = patientRepository.GetFaceToFaceEncounter(schedule.EventId, Current.AgencyId);
                    if (faceToFace != null)
                    {
                        if (faceToFace.Status.ToString() != schedule.Status && faceToFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                faceToFace.PhysicianData = physician.ToXml();
                            }
                        }
                        faceToFace.PhysicianId = schedule.PhysicianId;
                        faceToFace.IsDeprecated = schedule.IsDeprecated;
                        faceToFace.RequestDate = schedule.VisitDate.ToDateTime();
                        faceToFace.EpisodeId = schedule.EpisodeId;
                        if (patientRepository.UpdateFaceToFaceEncounter(faceToFace))
                        {
                            result = true;
                        }
                    }
                    break;
                case "IncidentAccidentReport":
                    {
                        var accidentReport = agencyRepository.GetIncidentReport(Current.AgencyId, schedule.EventId);
                        if (accidentReport != null)
                        {
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                accidentReport.IncidentDate = schedule.VisitDate.ToDateTime();
                            }
                            accidentReport.IsDeprecated = schedule.IsDeprecated;
                            accidentReport.EpisodeId = schedule.EpisodeId;
                            result = agencyRepository.UpdateIncidentModal(accidentReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "InfectionReport":
                    {
                        var infectionReport = agencyRepository.GetInfectionReport(Current.AgencyId, schedule.EventId);
                        if (infectionReport != null)
                        {
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                infectionReport.InfectionDate = schedule.VisitDate.ToDateTime();
                            }
                            infectionReport.IsDeprecated = schedule.IsDeprecated;
                            infectionReport.EpisodeId = schedule.EpisodeId;
                            result = agencyRepository.UpdateInfectionModal(infectionReport);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case "CommunicationNote":
                    {
                        var comNote = patientRepository.GetCommunicationNote(schedule.EventId, schedule.PatientId, Current.AgencyId);
                        if (comNote != null)
                        {
                            comNote.IsDeprecated = schedule.IsDeprecated;
                            if (schedule.VisitDate.IsNotNullOrEmpty() && schedule.VisitDate.IsValidDate())
                            {
                                comNote.Created = schedule.VisitDate.ToDateTime();
                            }
                            if (comNote.Status.ToString() != schedule.Status && comNote.Status == (int)ScheduleStatus.NoteCompleted && !comNote.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(comNote.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    comNote.PhysicianData = physician.ToXml();
                                }
                            }
                            comNote.EpisodeId = schedule.EpisodeId;
                            result = patientRepository.UpdateCommunicationNoteModal(comNote);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
            }
            return result;
        }

        private List<NotesQuestion> ProcessNoteQuestions(FormCollection formCollection)
        {
            string type = formCollection["Type"];
            formCollection.Remove("Type");

            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();

            foreach (var key in formCollection.AllKeys)
            {
                string[] nameArray = key.Split('_');
                if (nameArray.Length > 0)
                {
                    nameArray.Reverse();
                    string name = nameArray[0];
                    if (!questions.ContainsKey(name))
                    {
                        questions.Add(name, new NotesQuestion { Name = name, Answer = formCollection.GetValues(key).Join(","), Type = type });
                    }
                }
            }
            return questions.Values.ToList();
        }

        public static MemoryStream ResizeAndEncodePhoto(Image original, string contentType, bool resizeIfWider)
        {
            int imageWidth = 140;
            int imageHeight = 140;
            MemoryStream memoryStream = null;

            if (original != null)
            {
                int destX = 0;
                int destY = 0;
                int sourceX = 0;
                int sourceY = 0;
                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;
                int sourceWidth = original.Width;
                int sourceHeight = original.Height;

                nPercentW = ((float)imageWidth / (float)sourceWidth);
                nPercentH = ((float)imageHeight / (float)sourceHeight);

                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                }
                else
                {
                    nPercent = nPercentW;
                }

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);

                var newImage = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);

                newImage.SetResolution(original.HorizontalResolution, original.VerticalResolution);

                using (Graphics graphics = Graphics.FromImage(newImage))
                {
                    graphics.Clear(Color.White);
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.DrawImage(original,
                        new Rectangle(destX, destY, destWidth, destHeight),
                        new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                        GraphicsUnit.Pixel);
                }
                memoryStream = new MemoryStream();

                ImageCodecInfo imageCodec = null;
                ImageCodecInfo[] imageCodecs = ImageCodecInfo.GetImageEncoders();

                foreach (ImageCodecInfo imageCodeInfo in imageCodecs)
                {
                    if (imageCodeInfo.MimeType == contentType)
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }

                    if (imageCodeInfo.MimeType == "image/jpeg" && contentType == "image/pjpeg")
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }
                }
                if (imageCodec != null)
                {
                    EncoderParameters encoderParameters = new EncoderParameters();
                    encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
                    newImage.Save(memoryStream, imageCodec, encoderParameters);
                    newImage.Dispose();
                    original.Dispose();
                }
            }

            memoryStream.Position = 0;
            return memoryStream;
        }

        private string ProcessHospitalizationData(FormCollection formCollection)
        {
            formCollection.Remove("Id");
            formCollection.Remove("PatientId");
            formCollection.Remove("EpisodeId");
            formCollection.Remove("UserId");
            formCollection.Remove("Templates");
            formCollection.Remove("M0903LastHomeVisitDate");
            formCollection.Remove("M0906DischargeDate");

            var questions = new List<Question>();
            foreach (var key in formCollection.AllKeys)
            {
                questions.Add(Question.Create(key, formCollection.GetValues(key).Join(",")));
            }
            return questions.ToXml();
        }


        #endregion
    }
}
