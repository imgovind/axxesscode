﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;


    using Extensions;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Log.Enums;

    public class ReferralService : IReferralService
    {
        private readonly IReferralRepository referralRepository;

        public ReferralService(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
        }

        public List<ReferralData> GetPending(Guid agencyId)
        {
            var list = new List<ReferralData>();
            if (Current.IsCommunityLiason || Current.IsInRole(AgencyRoles.ExternalReferralSource))
            {
                list = referralRepository.AllByUser(agencyId, Current.UserId, ReferralStatus.Pending).ToList();
            }
            else
            {
                list = referralRepository.All(agencyId, ReferralStatus.Pending);
            }

            list.ForEach(r =>
            {
                r.AdmissionSource = r.AdmissionSourceId.ToSourceName();
            });

            return list;
        }

        public bool AddReferral(Referral referral)
        {
            var result = false;
            referral.Id = Guid.NewGuid();
            referral.CreatedById = Current.UserId;
            referral.AgencyId = Current.AgencyId;
            if (referralRepository.Add(referral))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, referral.Id.ToString(), LogType.Referral, LogAction.ReferralAdded, string.Empty);
                result = true;
            }
            return result;
        }

    }
}
