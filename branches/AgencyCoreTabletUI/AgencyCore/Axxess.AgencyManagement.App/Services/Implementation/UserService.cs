﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;


    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;
    using Axxess.Log.Enums;

    public class UserService : IUserService
    {
        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IPatientRepository patientRepository;

        public UserService(IAgencyManagementDataProvider agencyManagmentDataProvider, IMembershipDataProvider membershipDataProvider, IPatientService patientService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");

            this.patientService = patientService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.assetRepository = agencyManagmentDataProvider.AssetRepository;
            this.patientRepository = agencyManagmentDataProvider.PatientRepository;
        }

        public bool IsEmailAddressUnique(string emailAddress)
        {
            return loginRepository.Find(emailAddress) == null;
        }

        public bool IsPasswordCorrect(Guid userId, string password)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(password, login.PasswordHash, login.PasswordSalt))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsSignatureCorrect(Guid userId, string signature)
        {
            if (!userId.IsEmpty() && signature.IsNotNullOrEmpty())
            {
                var user = userRepository.Get(userId, Current.AgencyId);
                if (user != null)
                {
                    var login = loginRepository.Find(user.LoginId);
                    if (login != null)
                    {
                        var saltedHash = new SaltedHash();
                        if (saltedHash.VerifyHashAndSalt(signature, login.SignatureHash, login.SignatureSalt))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool CreateUser(User user)
        {
            try
            {
                var isNewLogin = false;
                var login = loginRepository.Find(user.EmailAddress);
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = user.FirstName;
                    login.EmailAddress = user.EmailAddress;
                    login.Role = Roles.ApplicationUser.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login))
                    {
                        isNewLogin = true;
                    }
                }

                user.LoginId = login.Id;
                user.Profile = new UserProfile();
                user.Profile.EmailWork = user.EmailAddress;

                if (userRepository.Add(user))
                {
                    string bodyText = string.Empty;
                    string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);

                    if (isNewLogin)
                    {
                        var parameters = string.Format("id={0}&agencyid={1}", user.Id, user.AgencyId);
                        var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                        bodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName,
                            "agencyname", user.AgencyName, "encryptedQueryString", encryptedParameters);
                    }
                    else
                    {
                        bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", user.FirstName,
                            "agencyname", user.AgencyName);
                    }
                    UserEngine.Refresh(Current.AgencyId);
                    Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, subject, bodyText);
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserAdded, string.Empty);
                    return true;
                }

            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }

            return false;
        }

        public bool DeleteUser(Guid userId)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            var result = false;
            if (user != null)
            {
                var accounts = userRepository.GetUsersByLoginId(user.LoginId);
                if (accounts != null)
                {
                    if (userRepository.Delete(Current.AgencyId, userId))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserDeleted, string.Empty);
                        result = true;
                    }
                }
            }

            return result;
        }

        public List<UserVisitWidget> GetScheduleWidget(Guid userId)
        {
            var to = DateTime.Today.AddDays(14);
            var from = DateTime.Now.AddDays(-89);
            var userVisits = new List<UserVisitWidget>();
            var patientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, from, to);
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                patientEpisodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty() && episode.EndDate.IsValidDate() && episode.StartDate.IsValidDate())
                    {
                        var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
                            s.EventId != Guid.Empty && s.UserId == userId && s.IsDeprecated == false && s.IsMissedVisit == false
                           && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date >= from.Date && s.EventDate.ToDateTime().Date <= to.Date
                           && !s.IsCompleted() && s.DisciplineTask != (int)DisciplineTasks.Rap && s.DisciplineTask != (int)DisciplineTasks.Final
                            ).ToList();

                        if (scheduledEvents.Count > 0)
                        {
                            scheduledEvents.ForEach(scheduledEvent =>
                            {
                                if (scheduledEvent != null)
                                {
                                    userVisits.Add(new UserVisitWidget
                                    {
                                        PatientName = episode.PatientName,
                                        TaskName = scheduledEvent.DisciplineTaskName,
                                        EventDate = scheduledEvent.EventDate.ToZeroFilled()
                                    });
                                }
                            });
                        }
                    }
                });
            }

            return userVisits.OrderBy(v => v.EventDate.ToOrderedDate()).Take(5).ToList();
        }

        public List<UserVisit> GetScheduleLean(Guid userId, DateTime from, DateTime to)
        {
            var userVisits = new List<UserVisit>();
            var patientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, from, to);
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                patientEpisodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty() && episode.EndDate.IsValidDate() && episode.StartDate.IsValidDate())
                    {
                        var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
                            s.EventId != Guid.Empty && s.UserId == userId && s.IsDeprecated == false && s.IsMissedVisit == false
                           && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date >= from.Date && s.EventDate.ToDateTime().Date <= to.Date
                           && !s.IsCompleted() && s.DisciplineTask != (int)DisciplineTasks.Rap && s.DisciplineTask != (int)DisciplineTasks.Final
                            ).ToList();

                        if (scheduledEvents.Count > 0)
                        {
                            scheduledEvents.ForEach(scheduledEvent =>
                            {
                                var visitNote = string.Empty;
                                var statusComments = string.Empty;
                                if (scheduledEvent != null)
                                {
                                    scheduledEvent.EndDate = episode.EndDate.ToDateTime();
                                    scheduledEvent.StartDate = episode.StartDate.ToDateTime();
                                    var episodeDetail = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
                                    if (scheduledEvent.Comments.IsNotNullOrEmpty())
                                    {
                                        visitNote = scheduledEvent.Comments.Clean();
                                    }
                                    if (scheduledEvent.StatusComment.IsNotNullOrEmpty())
                                    {
                                        statusComments = scheduledEvent.StatusComment.Clean();
                                    }

                                    Common.Url.Set(scheduledEvent, false, false);
                                    userVisits.Add(new UserVisit
                                    {
                                        Id = scheduledEvent.EventId,
                                        VisitNotes = visitNote,
                                        Url = scheduledEvent.Url,
                                        Status = scheduledEvent.Status,
                                        StatusName = scheduledEvent.StatusName,
                                        PatientName = episode.PatientName,
                                        StatusComment = statusComments,
                                        TaskName = scheduledEvent.DisciplineTaskName,
                                        EpisodeId = scheduledEvent.EpisodeId,
                                        PatientId = scheduledEvent.PatientId,
                                        EpisodeNotes = episodeDetail.Comments.Clean(),
                                        VisitDate = scheduledEvent.EventDate.ToZeroFilled(),
                                        IsMissedVisit = scheduledEvent.IsMissedVisit
                                    });
                                }
                            });
                        }
                    }
                });
            }

            return userVisits.OrderBy(v => v.VisitDate.ToOrderedDate()).ToList();
        }

        public List<UserVisit> GetScheduleLeanAll(Guid userId, DateTime from, DateTime to)
        {
            var userEvents = new List<UserEvent>();
            var userVisits = new List<UserVisit>();
            var userSchedules = userRepository.GetScheduleLean(Current.AgencyId, userId);
            userSchedules.ForEach(s =>
            {
                if (s.Visits.IsNotNullOrEmpty())
                {
                    var visits = s.Visits.ToObject<List<UserEvent>>();
                    visits.ForEach(v =>
                    {
                        if (v.EventDate.IsValidDate())
                        {
                            var date = v.EventDate.ToDateTime();
                            if (date >= from && date <= to
                               && v.IsDeprecated == false
                               && v.IsMissedVisit == false
                               && v.DisciplineTask != (int)DisciplineTasks.Rap
                               && v.DisciplineTask != (int)DisciplineTasks.Final)
                            {
                                v.PatientName = s.PatientName;
                                v.EpisodeDetails = s.EpisodeDetails;
                                v.EpisodeSchedule = s.EpisodeSchedule;
                                v.EpisodeEndDate = s.EpisodeEndDate;
                                v.EpisodeStartDate = s.EpisodeStartDate;
                                userEvents.Add(v);
                            }
                        }
                    });
                }
            });

            var orderedList = userEvents.OrderBy(e => e.EventDate.ToOrderedDate());
            foreach (UserEvent e in orderedList)
            {
                var visitNote = string.Empty;
                var statusComments = string.Empty;
                if (!e.EventId.IsEmpty() && e.EpisodeSchedule.IsNotNullOrEmpty())
                {
                    if (!userVisits.Exists(v => v.Id == e.EventId))
                    {
                        var episodeEvents = e.EpisodeSchedule.ToObject<List<ScheduleEvent>>();
                        if (episodeEvents != null && episodeEvents.Count > 0)
                        {
                            var scheduledEvent = episodeEvents.FirstOrDefault(se => se.EventId == e.EventId);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.EndDate = e.EpisodeEndDate;
                                scheduledEvent.StartDate = e.EpisodeStartDate;
                                if ( !scheduledEvent.IsDeprecated
                                   && scheduledEvent.EventDate.IsValidDate() && (scheduledEvent.EventDate.ToDateTime() >= scheduledEvent.StartDate) && (scheduledEvent.EventDate.ToDateTime() <= scheduledEvent.EndDate))
                                {
                                    var episodeDetail = e.EpisodeDetails.IsNotNullOrEmpty() ? e.EpisodeDetails.ToObject<EpisodeDetail>() : new EpisodeDetail();
                                    if (scheduledEvent.Comments.IsNotNullOrEmpty())
                                    {
                                        visitNote = scheduledEvent.Comments.Clean();
                                    }
                                    if (scheduledEvent.StatusComment.IsNotNullOrEmpty())
                                    {
                                        statusComments = scheduledEvent.StatusComment.Clean();
                                    }

                                    Common.Url.Set(scheduledEvent, false, false);
                                    userVisits.Add(new UserVisit
                                    {
                                        Id = scheduledEvent.EventId,
                                        VisitNotes = visitNote,
                                        Url = scheduledEvent.Url,
                                        Status = scheduledEvent.Status,
                                        StatusName = scheduledEvent.StatusName,
                                        PatientName = e.PatientName,
                                        StatusComment = statusComments,
                                        TaskName = scheduledEvent.DisciplineTaskName,
                                        EpisodeId = scheduledEvent.EpisodeId,
                                        PatientId = scheduledEvent.PatientId,
                                        EpisodeNotes = episodeDetail.Comments.Clean(),
                                        VisitDate = scheduledEvent.EventDate.ToZeroFilled(),
                                        IsMissedVisit = scheduledEvent.IsMissedVisit

                                    });
                                }
                            }
                        }
                    }
                }
            }
            return userVisits;
        }

        public List<UserVisit> GetCompletedVisits(Guid userId, DateTime start, DateTime end)
        {
            var userEvents = new List<UserEvent>();
            var userVisits = new List<UserVisit>();
            var userSchedules = userRepository.GetScheduleLean(Current.AgencyId, userId, start, end);
            userSchedules.ForEach(s =>
            {
                if (s.Visits.IsNotNullOrEmpty())
                {
                    var visits = s.Visits.ToObject<List<UserEvent>>();
                    visits.ForEach(v =>
                    {
                        var date = v.EventDate.IsNotNullOrEmpty() ? v.EventDate.ToDateTime() : DateTime.MinValue;
                        if (date != DateTime.MinValue)
                        {
                            if (date > start && date < end
                               && v.IsDeprecated == false
                               && v.IsMissedVisit == false
                               && v.DisciplineTask != (int)DisciplineTasks.Rap
                               && v.DisciplineTask != (int)DisciplineTasks.Final)
                            {
                                v.PatientName = s.PatientName;
                                v.EpisodeDetails = s.EpisodeDetails;
                                v.EpisodeSchedule = s.EpisodeSchedule;
                                v.EpisodeEndDate = s.EpisodeEndDate;
                                v.EpisodeStartDate = s.EpisodeStartDate;
                                userEvents.Add(v);
                            }
                        }
                    });
                }
            });

            var orderedList = userEvents.OrderBy(e => e.EventDate.ToOrderedDate());
            foreach (UserEvent e in orderedList)
            {
                var visitNote = string.Empty;
                var statusComments = string.Empty;
                if (!e.EventId.IsEmpty() && e.EpisodeSchedule.IsNotNullOrEmpty())
                {
                    if (!userVisits.Exists(v => v.Id == e.EventId))
                    {
                        var episodeEvents = e.EpisodeSchedule.ToObject<List<ScheduleEvent>>();
                        if (episodeEvents != null && episodeEvents.Count > 0)
                        {
                            var scheduledEvent = episodeEvents.FirstOrDefault(se => se.EventId == e.EventId);
                            if (scheduledEvent != null && scheduledEvent.IsCompleted() && !scheduledEvent.IsDeprecated && !scheduledEvent.IsMissedVisit)
                            {
                                scheduledEvent.EndDate = e.EpisodeEndDate;
                                scheduledEvent.StartDate = e.EpisodeStartDate;
                                if (DateTime.Parse(scheduledEvent.EventDate) >= scheduledEvent.StartDate && DateTime.Parse(scheduledEvent.EventDate) <= scheduledEvent.EndDate)
                                {
                                    if (scheduledEvent.Comments.IsNotNullOrEmpty())
                                    {
                                        visitNote = scheduledEvent.Comments.Clean();
                                    }
                                    if (scheduledEvent.StatusComment.IsNotNullOrEmpty())
                                    {
                                        statusComments = scheduledEvent.StatusComment.Clean();
                                    }

                                    Common.Url.Set(scheduledEvent, false, false);
                                    userVisits.Add(new UserVisit
                                    {
                                        Id = scheduledEvent.EventId,
                                        UserId = userId,
                                        VisitRate = "$0.00",
                                        Url = scheduledEvent.Url,
                                        UserDisplayName = UserEngine.GetName(userId, Current.AgencyId),
                                        Status = scheduledEvent.Status,
                                        Surcharge = scheduledEvent.Surcharge,
                                        StatusName = scheduledEvent.StatusName,
                                        PatientName = e.PatientName,
                                        StatusComment = statusComments,
                                        TaskName = scheduledEvent.DisciplineTaskName,
                                        EpisodeId = scheduledEvent.EpisodeId,
                                        PatientId = scheduledEvent.PatientId,
                                        VisitDate = scheduledEvent.EventDate.ToZeroFilled(),
                                        IsMissedVisit = scheduledEvent.IsMissedVisit

                                    });
                                }
                            }
                        }
                    }
                }
            }
            return userVisits;
        }

        public bool UpdateProfile(User user)
        {
            var result = false;

            if (userRepository.UpdateProfile(user))
            {
                result = true;
                if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty() || user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                {
                    User userInfo = userRepository.Get(user.Id, Current.AgencyId);
                    Login login = loginRepository.Find(userInfo.LoginId);
                    if (userInfo != null && login != null)
                    {
                        if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty())
                        {
                            string passwordsalt = string.Empty;
                            string passwordHash = string.Empty;

                            var saltedHash = new SaltedHash();
                            saltedHash.GetHashAndSalt(user.PasswordChanger.NewPassword, out passwordHash, out passwordsalt);
                            login.PasswordSalt = passwordsalt;
                            login.PasswordHash = passwordHash;
                        }

                        if (user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                        {
                            string signaturesalt = string.Empty;
                            string signatureHash = string.Empty;

                            var saltedHash = new SaltedHash();
                            saltedHash.GetHashAndSalt(user.SignatureChanger.NewSignature, out signatureHash, out signaturesalt);
                            login.SignatureSalt = signaturesalt;
                            login.SignatureHash = signatureHash;
                        }

                        if (!loginRepository.Update(login))
                        {
                            result = false;
                        }
                        else
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserProfileUpdated, string.Empty);
                        }
                    }
                }
                
            }

            return result;
        }

        public bool AddLicense(License license, System.Web.HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var isAssetSaved = true;
            var user = userRepository.Get(license.UserId, Current.AgencyId);
            if (user != null)
            {
                if (httpFiles.Count > 0)
                {
                    foreach (string key in httpFiles.AllKeys)
                    {
                        HttpPostedFileBase file = httpFiles.Get(key);
                        if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                        {
                            var binaryReader = new BinaryReader(file.InputStream);

                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset))
                            {
                                license.AssetId = asset.Id;
                            }
                            else
                            {
                                isAssetSaved = false;
                                break;
                            }
                        }
                    }
                }
                if (isAssetSaved)
                {
                    license.Id = Guid.NewGuid();
                    license.Created = DateTime.Now;
                    if (license.OtherLicenseType.IsNotNullOrEmpty())
                    {
                        license.LicenseType = license.OtherLicenseType;
                    }
                    if (user.LicensesArray != null )
                    {
                        user.LicensesArray.Add(license);
                        user.Licenses = user.LicensesArray.ToXml();
                    }
                    else
                    {
                        user.LicensesArray = new List<License>();
                        user.LicensesArray.Add(license);
                        user.Licenses = user.LicensesArray.ToXml();
                    }
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateLicense(Guid id, Guid userId, DateTime ExpirationDate)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                var license = user.LicensesArray.Find(l => l.Id == id);
                if (license != null)
                {
                    license.ExpirationDate = ExpirationDate;
                    user.Licenses = user.LicensesArray.ToXml();
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool DeleteLicense(Guid id, Guid userId)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                var license = user.LicensesArray.Find(l => l.Id == id);
                if (license != null)
                {
                    if (!license.AssetId.IsEmpty())
                    {
                        assetRepository.Delete(license.AssetId);
                    }
                    user.LicensesArray.RemoveAll(l => l.Id == id);
                    user.Licenses = user.LicensesArray.ToXml();
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseDeleted, string.Empty);
                        return true;
                    }

                }
            }
            return false;
        }

        public bool UpdatePermissions(FormCollection formCollection)
        {
            var result = false;
            var userId = formCollection["UserId"] != null ? formCollection["UserId"].ToGuid() : Guid.Empty;
            var permissionArray = formCollection["PermissionsArray"] != null ? formCollection["PermissionsArray"].ToArray().ToList() : null;
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null && permissionArray != null && permissionArray.Count > 0)
            {
                user.PermissionsArray = permissionArray;
                if (user.PermissionsArray != null && user.PermissionsArray.Count > 0)
                {
                    user.Permissions = user.PermissionsArray.ToXml();
                }
                if (userRepository.UpdateModel(user))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserPermissionsUpdated, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public List<User> GetUserByBranchAndStatus(Guid branchId, int status)
        {
            var users = new List<User>();
            if (status == 0)
            {
                if (branchId.IsEmpty())
                {
                    users = userRepository.GetUsersOnly(Current.AgencyId).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId).ToList();
                }
            }
            else
            {
                if (branchId.IsEmpty())
                {
                    users = userRepository.GetUsersByStatus(Current.AgencyId,status).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId,status).ToList();
                }

            }
            return users;
        }
    }
}
