﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using ViewData;

    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using System.Web;

    public interface IBillingService
    {
        bool AddRap(Rap rap);
        bool AddFinal(Final final);
        string GenerateJson(List<Guid> rapToGenerate, List<Guid> finalToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, Agency agency, AxxessSubmitterInfo payerInfo, out List<Rap> raps, out List<Final> finlas, bool isHMO, int insuranceId, AgencyLocation branch);
        bool GenerateDirect(List<Guid> rapToGenerate, List<Guid> finalToGenerate, ClaimCommandType commandType, out ClaimData claimDataOut, out BillExchange billExchange, Guid branchId, int insuranceId);
        bool VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> visit);
        bool VisitSupply(Guid Id, Guid episodeId, Guid patientId, FormCollection formCollection, List<Guid> supplyId);
        Bill AllUnProcessedBill(Guid branchId, int insuranceId);
        IList<ClaimBill> AllUnProcessedRaps(Guid branchId, int insuranceId);
        IList<ClaimBill> AllUnProcessedFinals(Guid branchId, int insuranceId);

        BillLean ClaimToGenerate(List<Guid> rapToGenerate, List<Guid> finalToGenerate, Guid branchId, int primaryInsurance);

        IList<ClaimHistoryLean> Activity(Guid patientId, int insuranceId);
        IList<PendingClaimLean> PendingClaimRaps(Guid branchId, string primaryInsurance);
        IList<PendingClaimLean> PendingClaimFinals(Guid branchId, string primaryInsurance);
        IList<ClaimLean> AccountsReceivables(Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        IList<ClaimLean> AccountsReceivableRaps(Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        IList<ClaimLean> AccountsReceivableFinals(Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        IList<TypeOfBill> GetAllUnProcessedBill();
        List<Supply> GetSupply(ScheduleEvent scheduleEvent);
        double GetSupplyReimbursement(char type, int year, out PPSStandard ppsStandardOut);
        string GetCbsaFromZip(string zip);
        Rap GetRap(Guid patientId, Guid episodeId);
        Final GetFinalInfo(Guid patientId, Guid episodeId);
        Dictionary<string, BillInfo> GetInsuranceUnit(int insurnaceId, out int unitType);
        bool UpdateRapStatus(List<Guid> rapToGenerate, Guid agencyId, string statusType);
        bool UpdateFinalStatus(List<Guid> finalToGenerate, Guid agencyId, string statusType);
        bool FinalComplete(Guid Id);
        UBOFourViewData GetUBOFourInfo(Guid patientId, Guid claimId, string type);
        ClaimInfoSnapShotViewData GetClaimSnapShotInfo(Guid patientId, Guid claimId, string type);
        ClaimViewData GetClaimViewData(Guid patientId, Guid Id, string type);
        bool UpdateProccesedClaimStatus(Guid patientId, Guid Id, string type, DateTime claimDate, double paymentAmount, DateTime paymentDate, int status, int primaryInsuranceId, string comment);
        bool UpdatePendingClaimStatus(FormCollection formCollection, string[] rapIds, string[] finalIds);
        bool UpdateRapClaimStatus(Guid Id, DateTime paymentDate, double paymentAmount, int status);
        bool UpdateFinalClaimStatus(Guid Id, DateTime paymentDate, double paymentAmount, int status);
        bool IsEpisodeHasClaim(Guid episodeId, Guid patientId, string type);
        IList<ClaimSnapShotViewData> ClaimSnapShots(Guid Id, string type);
        bool UpdateSnapShot(Guid Id, long batchId, double paymentAmount, DateTime paymentDate, int status, string type);
        ManagedClaim GetManagedClaimInfo(Guid patientId, Guid Id);
        List<SelectListItem> GetManagedClaimEpisodes(Guid patientId, Guid managedClaimId);
        bool ManagedVisitVerify(Guid Id, Guid patientId, List<Guid> visit);
        bool ManagedVisitSupply(Guid Id, Guid patientId, FormCollection formCollection, List<Guid> supplyId);
        bool UpdateProccesedManagedClaimStatus(Guid patientId, Guid Id, DateTime claimDate, double paymentAmount, DateTime paymentDate, int status, string comment);
        ManagedClaimSnapShotViewData GetManagedClaimSnapShotInfo(Guid patientId, Guid claimId);
        bool ManagedComplete(Guid Id, Guid patientId);
        Bill GetClaimsPrint(Guid branchId, int insuranceId);
        Final GetFinalPrint(Guid episodeId, Guid patientId);
        Rap GetRapPrint(Guid episodeId, Guid patientId);
        UBOFourViewData GetManagedUBOFourInfo(Guid patientId, Guid claimId);
        string GenerateJsonForManaged(List<Guid> managedClaimToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, Agency agency, AgencyInsurance payerInfo, bool isHMO, int insuranceId, out List<ManagedClaim> managedClaims ,AgencyLocation branch);
        bool GenerateManaged(List<Guid> managedClaimToGenerate, ClaimCommandType commandType, out ClaimData claimDataOut, out BillExchange billExchange, Guid branchId, int insuranceId);
        ManagedBillViewData ManagedBill(Guid branchId, int insuranceId, int status);
        ManagedBillViewData ManagedClaimToGenerate(List<Guid> managedClaimToGenerate, Guid branchId, int primaryInsurance);
        bool UpdateManagedClaimStatus(List<Guid> managedClaimToGenerate, Guid agencyId, string statusType);
        bool AddRemittanceUpload(HttpPostedFileBase file);
        List<ClaimInfoDetail> GetSubmittedBatchClaims(int batchId);
        ManagedClaimEpisodeData GetEpisodeAssessmentData(Guid episodeId, Guid patientId);

        IList<ClaimInfoDetail> BillingBatch(string claimType, DateTime batchDate);
    }
}
