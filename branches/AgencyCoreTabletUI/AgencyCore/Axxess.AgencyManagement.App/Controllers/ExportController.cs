﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Services;
    using Exports;
    using Enums;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.AgencyManagement.Enums;

    using Axxess.OasisC.Repositories;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.App.Domain;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ExportController : BaseController
    {
        #region Private Members/Constructor

        private readonly IAssessmentService assessmentService;
        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IReferralRepository referralRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly IReportService reportService;
        private readonly IPayrollService payrollService;

        public ExportController(IAgencyManagementDataProvider agencyManagementDataProvider, IOasisCDataProvider oasisCDataProvider, IAssessmentService assessmentService, IUserService userService, IAgencyService agencyService, IPatientService patientService, IReportService reportService, IPayrollService payrollService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.assessmentService = assessmentService;
            this.userService = userService;
            this.agencyService = agencyService;
            this.patientService = patientService;
            this.reportService = reportService;
            this.payrollService = payrollService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.assessmentRepository = oasisCDataProvider.OasisAssessmentRepository;
        }

        #endregion

        #region ExportController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Contacts()
        {
            var contacts = agencyRepository.GetContacts(Current.AgencyId);
            var export = new ContactExporter(contacts);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Contacts.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Hospitals()
        {
            var hospitals = agencyRepository.GetHospitals(Current.AgencyId);
            var export = new HosptialExporter(hospitals);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Hospitals.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Physicians()
        {
            var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId);
            var export = new PhysicianExporter(physicians);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Physicians.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Referrals()
        {
            IEnumerable<Referral> referrals = null;
            if (Current.IsCommunityLiason || Current.IsInRole(AgencyRoles.ExternalReferralSource))
            {
                referrals = referralRepository.GetAllByCreatedUser(Current.AgencyId, Current.UserId, ReferralStatus.Pending);
            }
            else
            {
                referrals = referralRepository.GetAll(Current.AgencyId, ReferralStatus.Pending);
            }
            var export = new ReferralExporter(referrals.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Referrals.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Patients()
        {
            var patients = patientRepository.GetAllByAgencyId(Current.AgencyId);
            var export = new PatientExporter(patients.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Patients.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ActiveUsers()
        {
            var users = userRepository.GetUsersByStatus(Current.AgencyId, (int)UserStatus.Active);
            var export = new UserExporter(users.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Users.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult InactiveUsers()
        {
            var users = userRepository.GetUsersByStatus(Current.AgencyId, (int)UserStatus.Inactive);
            var export = new UserExporter(users.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Users.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult CahpsReport(int sampleMonth, int sampleYear, List<int> paymentSources)
        {
            var export = new CahpsExporter(sampleMonth, sampleYear, paymentSources);
            return File(export.Process().GetBuffer(), export.MimeType, export.FileName);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleList()
        {
            var export = new UserScheduleExporter(userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "ScheduleList.xls");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult QAScheduleList(Guid BranchId, int Status)
        {
            var export = new QAScheduleExporter(agencyService.GetCaseManagerSchedule(BranchId, Status).OrderBy(p => p.PatientName).ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "QAScheduleList.xls");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintQueueList()
        {
            var export = new PrintQueueExporter(agencyService.GetPrintQueue().OrderBy(p => p.PatientName).ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "PrintQueue.xls");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult ExportedOasis(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            var assessments = assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisExported ,Status,  StartDate,  EndDate);
            var export = new ExportedOasis(assessments.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Exported Oasis.xls");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult OasisExport(Guid BranchId, string insuranceId)
        {
            var assessments = assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisCompletedExportReady, insuranceId);
            var export = new OasisExporter(assessments.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "OASIS To Export.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Authorizations(Guid patientId)
        {
            IList<Authorization> authorizations = patientRepository.GetAuthorizations(Current.AgencyId, patientId);
            var export = new AuthorizationsExporter(authorizations);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Authorizations.xls");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult VitalSigns(Guid patientId, DateTime StartDate, DateTime EndDate)
        {
            var vitalSigns = patientService.GetPatientVitalSigns(patientId, StartDate, EndDate);
            var export = new VitalSignExporter(vitalSigns.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Patient Vital Signs_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult OrdersHistory(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var orders = agencyService.GetProcessedOrders(BranchId, StartDate, EndDate, new List<int> { (int)ScheduleStatus.OrderReturnedWPhysicianSignature });
            var export = new OrderExport(orders.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Orders_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult PatientOrdersHistory(Guid patientId, DateTime StartDate, DateTime EndDate)
        {
            var orders = patientService.GetPatientOrders(patientId ,StartDate,EndDate );
            var export = new PatientOrderExport(orders.ToList(),StartDate, EndDate );
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("Patient Orders_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult OrdersPendingSignature(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var orders = agencyService.GetOrdersPendingSignature(BranchId, StartDate, EndDate);
            var export = new PendingSignatureOrderExport(orders.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("OrdersPendingPhysicianSignature_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult OrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime StartDate, DateTime EndDate)
        {
            var orders = agencyService.GetOrdersToBeSent(BranchId,sendAutomatically, StartDate, EndDate);
            var export = new OrdersToBeSentExporter(orders.ToList(), sendAutomatically,StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("OrdersToBeSent{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult RecertsPastDue(Guid BranchId, int insuranceId , DateTime StartDate)
        {
            var recertEvents = agencyService.GetRecertsPastDue(BranchId,insuranceId, StartDate,DateTime.Now);
            var export = new PastDueRecertExporter(recertEvents.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Past Due Recerts.xls");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult RecertsUpcoming(Guid BranchId, int InsuranceId)
        {
            var recertEvents = agencyService.GetRecertsUpcoming(BranchId, InsuranceId, DateTime.Now, DateTime.Now.AddDays(24));
            var export = new UpcomingRecertExporter(recertEvents.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Upcoming Recerts.xls");
        }

         [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Templates()
        {
            var templates = agencyRepository.GetTemplates(Current.AgencyId);
            var export = new TemplateExporter(templates.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Templates.xls");
        }

         [AcceptVerbs(HttpVerbs.Post)]
         public FileResult Incidents()
         {
             var incidents = agencyService.GetIncidents(Current.AgencyId);
             var export = new IncidentExporter(incidents.ToList());
             return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Incident.xls");
         }

         [AcceptVerbs(HttpVerbs.Post)]
         public FileResult Infections()
         {
             var infections = agencyService.GetInfections(Current.AgencyId);
             var export = new InfectionExporter(infections.ToList());
             return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Infections.xls");
         }

         [AcceptVerbs(HttpVerbs.Get)]
         public FileResult CommunicationNotes(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
         {
             var communicationNotes = patientService.GetCommunicationNotes(BranchId,Status, StartDate,EndDate);
             var export = new CommunicationNoteExporter(communicationNotes.ToList(),StartDate,EndDate);
             return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "CommunicationNotes.xls");
         }

        [AcceptVerbs(HttpVerbs.Get)]
         public FileResult ScheduleDeviations(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var scheduleEvents = reportService.GetScheduleDeviation(BranchId,StartDate,EndDate);
            var export = new ScheduleDeviationExporter(scheduleEvents.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "ScheduleDeviations.xls");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult PayrollSummary(DateTime StartDate, DateTime EndDate, string payrollStatus)
        {
            var payroll = payrollService.GetSummary(StartDate, EndDate,payrollStatus);
            var export = new PayrollSummaryExport(payroll.ToList(), StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollSummary_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult PayrollDetail(Guid UserId, DateTime StartDate, DateTime EndDate, string payrollStatus)
        {
            var detail = new PayrollDetail();
            detail.StartDate = StartDate;
            detail.EndDate = EndDate;
            detail.PayrollStatus = payrollStatus;
            if (!UserId.IsEmpty())
            {
                detail.Id = UserId;
                detail.Name = UserEngine.GetName(UserId, Current.AgencyId);
                detail.Visits = payrollService.GetVisits(UserId, StartDate, EndDate ,payrollStatus);
            }
            var export = new PayrollDetailExport(detail);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollDetail_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult PayrollDetails(DateTime StartDate, DateTime EndDate, string payrollStatus)
        {
            var details = payrollService.GetDetails(StartDate, EndDate, payrollStatus);
            var export = new PayrollDetailsExport(details, StartDate, EndDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PayrollDetail_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult SubmittedBatchClaims(DateTime StartDate, DateTime EndDate ,string ClaimType)
        {
            var claims = billingRepository.ClaimDatas(Current.AgencyId,StartDate, EndDate,ClaimType );
            var export = new SubmittedBatchClaimsExport(claims.ToList(), StartDate, EndDate, ClaimType);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("SubmittedBatchClaims_{0}.xls", DateTime.Now.Ticks));
        }
        

        #endregion
    }
}
