﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;
    
    using Services;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Repositories;

    [Compress]
    [Authorize]
    [HandleError]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class MapController : BaseController
    {
        #region Constructor
        private readonly IPatientRepository patientRepository;
        public MapController(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
        }
        #endregion

        #region MapController Actions
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GoogleMap(Guid patientId)
        {
            return View("~/Views/Patient/GoogleMap.aspx", patientRepository.Get(patientId, Current.AgencyId));
        }
        #endregion
    }
}
