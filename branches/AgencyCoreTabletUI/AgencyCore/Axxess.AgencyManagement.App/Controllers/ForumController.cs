﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;

    using OpenForum.Core;
    using OpenForum.Core.DataAccess;
    using OpenForum.Core.Models;
    using OpenForum.Core.Properties;
    using OpenForum.Core.ViewModels;

    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;


    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ForumController : BaseController
    {
        private static readonly byte[] IMAGE_BYTES;

        private readonly IUserRepository userRepository;
        private readonly IPostRepository postRepository;
        private readonly IViewModelFactory viewModelFactory;

        public bool IncludeDefaultStyles { get; set; }
        public bool IncludeValidationSummary  { get; set; }
        public bool IncludeWysiwygEditor { get; set; }

        static ForumController()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Resources.nicEditorIcons.Save(stream, Resources.nicEditorIcons.RawFormat);
                stream.Position = 0;
                IMAGE_BYTES = new byte[stream.Length];
                stream.Read(IMAGE_BYTES, 0, (int)stream.Length);
            }
        }

        public ForumController(IViewModelFactory viewModelFactory, IPostRepository postRepository, IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.postRepository = postRepository;
            this.viewModelFactory = viewModelFactory;

            IncludeDefaultStyles = Configurations.Current.IncludeDefaultStyles;
            IncludeValidationSummary = Configurations.Current.IncludeValidationSummary;
            IncludeWysiwygEditor = Configurations.Current.IncludeWysiwygEditor;

            ItemsPerPage = 20;
        }

        public int ItemsPerPage { get; set; }

        public ActionResult Index(string searchQuery, int? page)
        {
            IQueryable<Post> posts;
            string message = "";

            if (string.IsNullOrEmpty(searchQuery))
            {
                posts = postRepository.Find();
            }
            else
            {
                posts = postRepository.Search(searchQuery);
                message = string.Format("Search results for \"{0}\".", searchQuery);
            }

            int currentPage = page ?? 0;
            int postCount = posts.Count();
            int totalPages = (int)Math.Ceiling((double)postCount / (double)ItemsPerPage);

            if (postCount == 0)
            {
                if (string.IsNullOrEmpty(searchQuery))
                {
                    message = "There are currently no posts in this forum.";
                }
                else
                {
                    message = string.Format("Sorry, there are no results for \"{0}\".", searchQuery);
                }
            }
            else
            {
                foreach (Post post in posts)
                {
                    post.Replies = postRepository.FindReplies(post.Id);
                }
            }
            
            IndexViewModel viewModel = new IndexViewModel()
            {
                PageTitle = "Forum",
                SearchQuery = searchQuery,
                Message = message,
                Posts = Page(posts, page, ItemsPerPage).ToList(),
                CurrentPage = currentPage,
                TotalPages = totalPages,
                IncludeDefaultStyles = IncludeDefaultStyles,
                IncludeValidationSummary = IncludeValidationSummary,
            };

            return View(viewModelFactory.GetIndexViewModel(viewModel));
        }

        public ActionResult View(int id, string title)
        {
            Post post = FindPostHelper(id);
            post.IncrementViewCount();
            postRepository.SubmitPost(post);

            post.Replies = postRepository.FindReplies(post.Id);

            post.User = new OpenForum.Core.Models.ForumUser(post.CreatedById, post.CreatedBy, null, null);

            post.Replies.ForEach(r =>
            {
                r.User = new OpenForum.Core.Models.ForumUser(r.CreatedById, r.CreatedBy, null, null);
            });

            ViewViewModel viewModel = new ViewViewModel()
            {
                PageTitle = post.Title,
                Post = post,
                CurrentUser = Current.ForumUser,
                IncludeDefaultStyles = IncludeDefaultStyles,
                IncludeValidationSummary = IncludeValidationSummary,
            };

            return View(viewModelFactory.GetViewViewModel(viewModel));
        }

        public ActionResult Post(int? id)
        {
            Post post = FindPostHelper(id);

            PostViewModel viewModel = new PostViewModel()
            {
                PageTitle = post.Title ?? "New Post",
                Post = post,
                IncludeDefaultStyles = IncludeDefaultStyles,
                IncludeValidationSummary = IncludeValidationSummary,
                IncludeWysiwygEditor = IncludeWysiwygEditor,
            };

            return View(viewModelFactory.GetPostViewModel(viewModel));
        }

        public ActionResult Reply(int? id, int postId)
        {
            Post post = FindPostHelper(postId);
            Reply reply = FindReplyHelper(id, post);

            ReplyViewModel viewModel = new ReplyViewModel()
            {
                PageTitle = post.Title,
                Post = post,
                Reply = reply,
                IncludeDefaultStyles = IncludeDefaultStyles,
                IncludeValidationSummary = IncludeValidationSummary,
                IncludeWysiwygEditor = IncludeWysiwygEditor,
            };

            return View(viewModelFactory.GetReplyViewModel(viewModel));
        }

        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Post(int? id, string title, string body)
        {
            Post post = FindPostHelper(id);

            if (id != null && post.CreatedById != Current.UserId)
            {
                throw new OpenForumException("This post cannont be editted by the current user. (Post Id = {0}; Author = {1}; Current User = {2})", post.Id, post.CreatedById.ToString(), Current.UserId.ToString());
            }

            post.Title = title;
            post.Body = body;

            try
            {
                post.Validate();
            }
            catch (OpenForumException exception)
            {
                ModelState.AddModelError("_FORM", exception.Message);

                PostViewModel viewModel = new PostViewModel()
                {
                    PageTitle = post.Title ?? "New Post",
                    Post = post,
                    IncludeDefaultStyles = IncludeDefaultStyles,
                    IncludeValidationSummary = IncludeValidationSummary,
                    IncludeWysiwygEditor = IncludeWysiwygEditor,
                };

                return View(viewModelFactory.GetPostViewModel(viewModel));
            }

            postRepository.SubmitPost(post);
            TempData["Post"] = post;

            return RedirectToAction("View", new { id = post.Id });
        }

        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Reply(int? id, int postId, string body)
        {
            Post post = FindPostHelper(postId);
            Reply reply = FindReplyHelper(id, post);

            if (id != null && reply.CreatedById != Current.UserId)
            {
                throw new OpenForumException("This reply cannont be editted by the current user. (Reply Id = {0}; Author = {1}; Current User = {2})", reply.Id, reply.CreatedById.ToString(), Current.UserId.ToString());
            }

            reply.Body = body;
            post.LastPostDate = DateTime.Now;
            post.LastPostBy = Current.UserFullName;
            post.LastPostById = Current.UserId;

            try
            {
                reply.Validate();
            }
            catch (OpenForumException exception)
            {
                ModelState.AddModelError("_FORM", exception.Message);

                ReplyViewModel viewModel = new ReplyViewModel()
                {
                    PageTitle = post.Title,
                    Post = post,
                    Reply = reply,
                    IncludeDefaultStyles = IncludeDefaultStyles,
                    IncludeValidationSummary = IncludeValidationSummary,
                    IncludeWysiwygEditor = IncludeWysiwygEditor,
                };

                return View(viewModelFactory.GetReplyViewModel(viewModel));
            }

            postRepository.SubmitPost(post);
            postRepository.SubmitReply(reply);

            return RedirectToAction("View", new { id = reply.PostId });
        }

        public ActionResult Script()
        {            
            return File(Encoding.Default.GetBytes(Resources.nicEdit) ,"application/javascript");
        }

        public ActionResult Image()
        {
            return File(IMAGE_BYTES, "image/gif");
        }

        private Post FindPostHelper(int? id)
        {
            if (id == null || id == 0)
            {
                var newPost = new Post();
                newPost.CreatedBy = Current.UserFullName;
                newPost.CreatedById = Current.UserId;
                newPost.LastPostBy = Current.UserFullName;
                newPost.LastPostById = Current.UserId;
                return newPost;
            }

            Post post = postRepository.FindById(id.Value);

            if (post == null)
            {
                throw new OpenForumException("The request forum post could not be found. (Post id = {0})", id);
            }

            return post;
        }

        private Reply FindReplyHelper(int? id, Post post)
        {
            if (id == null || id == 0)
            {
                Reply newReply = new Reply(post);
                newReply.CreatedById = Current.UserId;
                newReply.CreatedBy = Current.UserFullName;
                return newReply;
            }

            Reply reply = postRepository.FindReplyById(id.Value);

            if (reply == null)
            {
                throw new OpenForumException("The request forum reply could not be found. (Reply id = {0})", id);
            }

            return reply;
        }

        private IQueryable<T> Page<T>(IQueryable<T> queryable, int? page, int itemsPerPage)
        {
            return queryable.Skip(itemsPerPage * page ?? 0).Take(itemsPerPage);
        }
    }
}
