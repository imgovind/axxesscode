﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;

    using ViewData;
    using Services;
    using Extensions;
    using iTextExtension;
    using iTextExtension.XmlParsing;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.AgencyManagement.App.Workflows;

    using Telerik.Web.Mvc;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.LookUp.Domain;

    using Axxess.Log.Enums;

    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ScheduleController : BaseController
    {
        #region Constructor

        private readonly IDateService dateService;
        private readonly IPatientService patientService;
        private readonly IAssessmentService assessmentService;
        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;

        public ScheduleController(IAgencyManagementDataProvider dataProvider, IPatientService patientService, IAssessmentService assessmentService, IUserService userService, IAgencyService agencyService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            Check.Argument.IsNotNull(patientService, "patientService");

            this.patientService = patientService;
            this.dateService = Container.Resolve<IDateService>();
            this.userRepository = dataProvider.UserRepository;
            this.assetRepository = dataProvider.AssetRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.assessmentService = assessmentService;
            this.userService = userService;
            this.agencyService = agencyService;
        }

        #endregion

        #region ScheduleController Actions

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult All(byte statusId, Guid branchId, byte paymentSourceId, string name)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                patientList = patientRepository.GetPatientSelection(Current.AgencyId, branchId, statusId, paymentSourceId, name);
            }
            else if (Current.IsClinicianOrHHA)
            {
                patientList = patientRepository.GetUserPatients(Current.AgencyId, branchId, Current.UserId, statusId);
            }
            else { }

            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Center()
        {
            var viewData = new ScheduleViewData();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                viewData.Count = patientRepository.GetPatientStatusCount(Current.AgencyId, (int)PatientStatus.Active);
            }
            else if (Current.IsClinicianOrHHA)
            {
                viewData.Count = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active).Count;
            }
            else { viewData.Count = 0; }

            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Data(Guid patientId)
        {
            var viewData = new ScheduleViewData();
            viewData.PatientId = patientId;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                viewData.Episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, "all");
                viewData.IsDischarged = patient.IsDischarged;
            }
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshData(Guid patientId, Guid episodeId)
        {
            var viewData = new ScheduleViewData();
            viewData.PatientId = patientId;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                viewData.Episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId, "all");
                viewData.IsDischarged = patient.IsDischarged;
            }
            return PartialView("Data", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activities(Guid patientId, string discipline)
        {
            return PartialView(patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Calendar(Guid patientId, string discipline)
        {
            CalendarViewData calendarViewData = null;
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    calendarViewData = new CalendarViewData();
                    var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline);
                    if (episode != null)
                    {
                        calendarViewData.Episode = episode;
                        calendarViewData.PatientId = patientId;
                    }
                    else
                    {
                        calendarViewData.PatientId = patientId;
                    }
                    calendarViewData.IsDischarged = patient.IsDischarged;
                }
            }
            return PartialView(calendarViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MultiDay(Guid episodeId, Guid patientId)
        {
            CalendarViewData calendarViewData = null;
            if (!episodeId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    calendarViewData = new CalendarViewData();
                    var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        calendarViewData.Episode = episode;
                        calendarViewData.PatientId = patientId;
                    }
                    else
                    {
                        calendarViewData.PatientId = patientId;
                    }
                    calendarViewData.IsDischarged = patient.IsDischarged;
                }
            }
            return PartialView("MultiDayScheduler", calendarViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditDetails(Guid episodeId, Guid patientId, Guid eventId)
        {
            if (patientId.IsEmpty() || episodeId.IsEmpty() || eventId.IsEmpty())
            {
                return PartialView("Detail/Edit", new ScheduleEvent());
            }
            return PartialView("Detail/Edit", patientService.GetScheduledEvent(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateDetails([Bind] ScheduleEvent scheduleEvent)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task details could not be saved." };
            var validationRules = new List<Validation>();
            if (scheduleEvent.IsEpisodeReassiged && scheduleEvent.EpisodeId != scheduleEvent.NewEpisodeId)
            {
                var patientEpisode = patientRepository.GetEpisode(Current.AgencyId, scheduleEvent.NewEpisodeId, scheduleEvent.PatientId);
                if (patientEpisode != null)
                {
                    var oldEvents = (patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()).Where(s => s.EventDate.IsValidDate() && !s.IsDeprecated).OrderByDescending(o => o.EventDate.ToDateTime().Date).ToList();
                    var evnt = scheduleEvent;
                    if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT))
                    {
                        var transfer = oldEvents.FirstOrDefault(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT) && (oe.EventDate.ToDateTime().Date < evnt.EventDate.ToDateTime().Date));
                        ScheduleEvent roc = null;
                        if (transfer != null)
                        {
                            roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date));
                        }
                        if (transfer != null && roc == null)
                        {
                            viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                            return PartialView("JsonResult", viewData);
                        }
                        else if (transfer != null && roc != null && roc.EventDate.ToDateTime().Date <= transfer.EventDate.ToDateTime().Date)
                        {
                            viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                            return PartialView("JsonResult", viewData);
                        }
                        else if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT))
                        {
                            viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete that one before creating a new one.";
                            return PartialView("JsonResult", viewData);
                        }
                        else if (evnt.EventDate.ToDateTime().Date < patientEpisode.EndDate.AddDays(-5).Date || evnt.EventDate.ToDateTime().Date > patientEpisode.EndDate.Date)
                        {
                            viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
                            return PartialView("JsonResult", viewData);
                        }
                    }
                    else if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || evnt.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT|| evnt.DisciplineTask == (int)DisciplineTasks.OASISCStartofCareOT))
                    {
                        if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT || oe.DisciplineTask==(int) DisciplineTasks.OASISCStartofCareOT))
                        {
                            viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete that one before creating a new one.";
                            return PartialView("JsonResult", viewData);
                        }
                        else if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT))
                        {
                        }
                    }
                    else if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT))
                    {
                        var roc = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT);
                        var transfer = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
                        if (roc == null)
                        {
                            if (transfer == null)
                            {
                                viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                return PartialView("JsonResult", viewData);
                            }
                            else if (transfer != null && (transfer.EventDate.ToDateTime() > evnt.EventDate.ToDateTime()))
                            {
                                viewData.errorMessage = "Resumption of Care date should be later that the Transfer date.";
                                return PartialView("JsonResult", viewData);
                            }
                        }
                        else if (roc != null)
                        {
                            if (transfer != null && (roc.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date))
                            {
                                viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                return PartialView("JsonResult", viewData);
                            }
                        }
                    }
                    else if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT))
                    {
                        var transfer = oldEvents.FirstOrDefault(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
                        ScheduleEvent roc = null;
                        if (transfer != null)
                        {
                            roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date));
                        }
                        if (transfer != null && roc == null)
                        {
                            viewData.errorMessage = "Please Create a Resumption of Care before creating another Transfer.";
                            return PartialView("JsonResult", viewData);
                        }
                    }

                    validationRules.Add(new Validation(() => scheduleEvent.EventDate.IsNullOrEmpty(), "Schedule date is required."));
                    validationRules.Add(new Validation(() => !scheduleEvent.EventDate.IsValidDate(), "Schedule date is not valid."));
                    validationRules.Add(new Validation(() => scheduleEvent.EventDate.IsNotNullOrEmpty() && scheduleEvent.EventDate.IsValidDate() ? !(scheduleEvent.EventDate.ToDateTime() >= patientEpisode.StartDate.Date && scheduleEvent.EventDate.ToDateTime().Date <= patientEpisode.EndDate.Date) : true, "Schedule date is not in the episode range."));
                    validationRules.Add(new Validation(() => scheduleEvent.VisitDate.IsNullOrEmpty(), "Visit date is required."));
                    validationRules.Add(new Validation(() => !scheduleEvent.VisitDate.IsValidDate(), "Visit date is not valid."));
                    validationRules.Add(new Validation(() => scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? !(scheduleEvent.VisitDate.ToDateTime() >= patientEpisode.StartDate.Date && scheduleEvent.VisitDate.ToDateTime().Date <= patientEpisode.EndDate.Date) : true, "Visit date is not in the episode range."));

                    var entityValidator = new EntityValidator(validationRules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.UpdateScheduleEventDetail(scheduleEvent, Request.Files))
                        {
                            viewData.isSuccessful = true;
                        }
                    }
                    else
                    {
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
            }
            else
            {
                var episode = patientRepository.GetEpisode(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId);
                if (episode != null)
                {
                    validationRules.Add(new Validation(() => scheduleEvent.EventDate.IsNullOrEmpty(), "Schedule date is required."));
                    validationRules.Add(new Validation(() => !scheduleEvent.EventDate.IsValidDate(), "Schedule date is not valid."));
                    validationRules.Add(new Validation(() => scheduleEvent.EventDate.IsNotNullOrEmpty() && scheduleEvent.EventDate.IsValidDate() ? !(scheduleEvent.EventDate.ToDateTime() >= episode.StartDate && scheduleEvent.EventDate.ToDateTime() <= episode.EndDate) : true, "Schedule date is not in the episode range."));
                    validationRules.Add(new Validation(() => scheduleEvent.VisitDate.IsNullOrEmpty(), "Visit date is required."));
                    validationRules.Add(new Validation(() => !scheduleEvent.VisitDate.IsValidDate(), "Visit date is not valid."));
                    validationRules.Add(new Validation(() => scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? !(scheduleEvent.VisitDate.ToDateTime() >= episode.StartDate && scheduleEvent.VisitDate.ToDateTime() <= episode.EndDate) : true, "Visit date is not in the episode range."));

                    var entityValidator = new EntityValidator(validationRules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.UpdateScheduleEvent(scheduleEvent, Request.Files))
                        {
                            viewData.isSuccessful = true;
                        }
                    }
                    else
                    {
                        viewData.errorMessage = entityValidator.Message;
                    }

                }
            }

            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Missed/New", patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddMissedVisit([Bind] MissedVisit missedVisit)
        {
            Check.Argument.IsNotNull(missedVisit, "missedVisit");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Missed Visit could not be saved." };
            if (!missedVisit.Id.IsEmpty() && !missedVisit.PatientId.IsEmpty() && !missedVisit.EpisodeId.IsEmpty())
            {
                var episode = patientRepository.GetEpisode(Current.AgencyId, missedVisit.EpisodeId, missedVisit.PatientId);
                if (episode != null)
                {
                    var validationRules = new List<Validation>();
                    validationRules.Add(new Validation(() => !missedVisit.SignatureDate.IsValid(), "Signature date is not valid date."));
                    validationRules.Add(new Validation(() => string.IsNullOrEmpty(missedVisit.Signature), "User Signature can't be empty."));
                    validationRules.Add(new Validation(() => missedVisit.Signature.IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, missedVisit.Signature) : false, "User Signature is not correct."));

                    if (missedVisit.SignatureDate.IsValid())
                    {
                        validationRules.Add(new Validation(() => (missedVisit.SignatureDate < episode.StartDate), "Missed visit date must be greater or equal to the episode start date."));
                        validationRules.Add(new Validation(() => (missedVisit.SignatureDate > episode.EndDate), "Missed visit date must be must be  less than or equalt to the episode end date."));
                    }

                    var entityValidator = new EntityValidator(validationRules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        missedVisit.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (patientService.AddMissedVisit(missedVisit))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Missed visit Successfully saved.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MissedVisitPrint(Guid patientId, Guid eventId)
        {
            return View("Missed/Print", assessmentService.GetMissedVisitPrint(patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MissedVisitPdf(Guid patientId, Guid eventId)
        {
            var doc = new MissedVisitPdf(assessmentService.GetMissedVisitPrint(patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MissedVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MissedVisitBlank()
        {
            return View("Missed/Print", assessmentService.GetMissedVisitPrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MissedVisitPdfBlank()
        {
            var doc = new MissedVisitPdf(assessmentService.GetMissedVisitPrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MissedVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisitInfo(Guid id)
        {
            return PartialView("Missed/Popup", patientRepository.GetMissedVisit(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LVNSVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToDictionary();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsNotNullOrEmpty() && scheduledEvent.VisitDate.IsValidDate() ? scheduledEvent.VisitDate : scheduledEvent.EventDate;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("Nursing/LVNSVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult LVNSVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new LVNSVisitPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=LVNSupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LVNSVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Nursing/LVNSVisitPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult LVNSVisitPdfBlank()
        {
            var doc = new LVNSVisitPdf(assessmentService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=LVNSupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LVNSVisitBlank()
        {
            return PartialView("Nursing/LVNSVisitPrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HHASVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToDictionary();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsNotNullOrEmpty() && scheduledEvent.VisitDate.IsValidDate() ? scheduledEvent.VisitDate : scheduledEvent.EventDate;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(episode, DisciplineTasks.HHAideCarePlan);
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            return PartialView("HHA/SupervisorVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHASVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("HHA/SupervisorVisitPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult HHASVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new HHASVisitPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHASupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHASVisitBlank()
        {
            return PartialView("HHA/SupervisorVisitPrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult HHASVisitPdfBlank()
        {
            var doc = new HHASVisitPdf(assessmentService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHASupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.UserId = patientvisitNote.UserId;
                viewData.TypeName = patientvisitNote.NoteType;
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsNotNullOrEmpty() && scheduledEvent.VisitDate.IsValidDate() ? scheduledEvent.VisitDate : scheduledEvent.EventDate;
                    viewData.IsWoundCareExist = patientvisitNote.IsWoundCare;
                    viewData.IsSupplyExist = patientvisitNote.IsSupplyExist;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    viewData.PreviousNotes = patientService.GetPreviousSkilledNurseNotes(patientId, scheduledEvent);
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {
                    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                        viewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                    }
                }
            }
            return PartialView("Nursing/SNVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNVisitContent(Guid patientId, Guid noteId, Guid previousNoteId)
        {
            var viewData = new VisitNoteViewData();
            var previousNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, previousNoteId);
            if (previousNote != null)
            {
                viewData.TypeName = previousNote.NoteType;
                viewData.IsWoundCareExist = previousNote.IsWoundCare;
                viewData.IsSupplyExist = previousNote.IsSupplyExist;
                viewData.PatientId = previousNote.PatientId;
                viewData.EpisodeId = previousNote.EpisodeId;
                viewData.EventId = previousNote.Id;

                var noteItems = previousNote.ToDictionary();

                var nameArray = new string[] { "PatientId", "EpisodeId", "EventId", "DisciplineTask", "VisitDate", "TimeIn", "TimeOut", "PreviousNotes", "Clinician", "SignatureDate", "button", "PrimaryDiagnosis", "ICD9M", "PrimaryDiagnosis1", "ICD9M1" };
                nameArray.ForEach(name =>
                {
                    noteItems.Remove(name);
                });
                viewData.Questions = noteItems;

                var currentNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
                if (currentNote != null)
                {
                    currentNote.Questions = noteItems.Values.ToList();
                    currentNote.Note = currentNote.Questions.ToXml();
                    currentNote.Modified = DateTime.Now;
                    if (patientRepository.UpdateVisitNote(currentNote))
                    {
                        var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, currentNote.EpisodeId, currentNote.PatientId, currentNote.Id);
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.Status = ((int)ScheduleStatus.NoteSaved).ToString();
                            var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, currentNote.Id);
                            if (userEvent != null)
                            {
                                userEvent.Status = scheduleEvent.Status;
                            }
                            if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            {
                                if (userEvent != null)
                                {
                                    userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                }
                                else
                                {
                                    userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit });
                                }
                                if (scheduleEvent.Status.IsInteger())
                                {
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, "Loaded Previous Note");
                                }
                            }
                        }
                    }
                }
            }
            return PartialView("Nursing/SNVisitContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Nursing/SNVisitPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SNVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new SNVisitPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SNVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNVisitBlank()
        {
            return View("Nursing/SNVisitPrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult SNVisitPdfBlank()
        {
            var doc = new SNVisitPdf(assessmentService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SNVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }

                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                        viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(episode, DisciplineTasks.PTEvaluation);
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";

                    viewData.PreviousNotes = patientService.GetPreviousPTNotes(patientId, scheduledEvent);
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("Therapy/PTVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTVisitContent(Guid patientId, Guid noteId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("Therapy/PTVisitContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTVisitBlank()
        {
            return View("Therapy/PTVisitPrint", assessmentService.GetVisitNotePrint("PTVisit"));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTVisitPdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("PTVisit"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTAVisitPdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("PTAVisit"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTAVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Therapy/PTVisitPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTDischarge(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);

                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                        viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(episode, DisciplineTasks.PTEvaluation);
                    }
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    viewData.PreviousNotes = patientService.GetPreviousPTDischarges(patientId, scheduledEvent);
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("Therapy/PTDischarge", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTDischargeContent(Guid patientId, Guid noteId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("Therapy/PTDischargeContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTDischargeBlank()
        {
            return View("Therapy/PTDischargePrint", assessmentService.GetVisitNotePrint("PTDischarge"));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTDischargePdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("PTDischarge"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTDischargePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Therapy/PTDischargePrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTDischargePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTEvaluation(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);

                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    viewData.PreviousNotes = patientService.GetPreviousPTEvals(patientId, scheduledEvent);
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("Therapy/PTEvaluation", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTEvalContent(Guid patientId, Guid noteId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("Therapy/PTEvalContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTEvaluationBlank()
        {
            return View("Therapy/PTEvaluationPrint", assessmentService.GetVisitNotePrint("PTEvaluation"));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTEvaluationPdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("PTEvaluation"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTReEvaluationPdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("PTReEvaluation"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTReEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTMaintenancePdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("PTMaintenance"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTMaintenance_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTEvaluationPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Therapy/PTEvaluationPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTEvaluationPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTEvaluation(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);

                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    viewData.PreviousNotes = patientService.GetPreviousOTEvals(patientId, scheduledEvent);
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("Therapy/OTEvaluation", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTEvalContent(Guid patientId, Guid noteId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("Therapy/OTEvalContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTEvaluationBlank()
        {
            return View("Therapy/OTEvaluationPrint", assessmentService.GetVisitNotePrint("OTEvaluation"));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTEvaluationPdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("OTEvaluation"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTReEvaluationPdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("OTReEvaluation"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTReEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTDischargePdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("OTDischarge"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTMaintenancePdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("OTMaintenance"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTMaintenance_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult COTAVisitPdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("COTAVisit"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=COTAVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTEvaluationPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Therapy/OTEvaluationPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTEvaluationPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);

                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                        viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(episode, DisciplineTasks.OTEvaluation);
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";

                    viewData.PreviousNotes = patientService.GetPreviousOTNotes(patientId, scheduledEvent);
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("Therapy/OTVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTVisitContent(Guid patientId, Guid noteId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("Therapy/OTVisitNoteContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTVisitBlank()
        {
            return View("Therapy/OTVisitPrint", assessmentService.GetVisitNotePrint("OTVisit"));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTVisitPdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("OTVisit"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Therapy/OTVisitPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STEvaluation(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);

                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    viewData.PreviousNotes = patientService.GetPreviousSTEvals(patientId, scheduledEvent);
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("Therapy/STEvaluation", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STEvalContent(Guid patientId, Guid noteId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("Therapy/STEvalContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STEvaluationBlank()
        {
            return View("Therapy/STEvaluationPrint", assessmentService.GetVisitNotePrint("STEvaluation"));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STEvaluationPdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("STEvaluation"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STReEvaluationPdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("STReEvaluation"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STReEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STMaintenancePdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("STMaintenance"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STMaintenance_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STEvaluationPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Therapy/STEvaluationPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STEvaluationPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STDischarge(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);

                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                        viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(episode, DisciplineTasks.STEvaluation);
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    viewData.PreviousNotes = patientService.GetPreviousSTEvals(patientId, scheduledEvent);
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("Therapy/STEvaluation", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STDischargeBlank()
        {
            return View("Therapy/STEvaluationPrint", assessmentService.GetVisitNotePrint("STDischarge"));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STDischargePdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("STDischarge"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STDischargePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Therapy/STEvaluationPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STDischargePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);

                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                        viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(episode, DisciplineTasks.STEvaluation);
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";

                    viewData.PreviousNotes = patientService.GetPreviousSTNotes(patientId, scheduledEvent);
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("Therapy/STVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STVisitContent(Guid patientId, Guid noteId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("Therapy/STVisitNoteContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STVisitBlank()
        {
            return View("Therapy/STVisitPrint", assessmentService.GetVisitNotePrint("STVisit"));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STVisitPdfBlank()
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("STVisit"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Therapy/STVisitPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MSWEvaluation(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);

                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("MSW/MSWEvaluation", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWEvaluationPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("MSW/MSWEvaluationPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MSWEvaluationPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new MSWPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWEvaluationBlank()
        {
            return View("MSW/MSWEvaluationPrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MSWEvaluationPdfBlank()
        {
            var doc = new MSWPdf(assessmentService.GetVisitNotePrint("MSWEvaluationAssessment"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MSWProgressNote(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);

                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    viewData.PreviousNotes = patientService.GetPreviousMSWProgressNotes(patientId, scheduledEvent);
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("MSW/MSWProgressNote", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MSWProgressNoteContent(Guid patientId, Guid noteId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("MSW/MSWProgressNoteContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWProgressNotePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("MSW/MSWProgressNotePrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MSWProgressNotePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new MSWPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWProgNote_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWProgressNoteBlank()
        {
            return View("MSW/MSWProgressNotePrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MSWProgressNotePdfBlank()
        {
            var doc = new MSWPdf(assessmentService.GetVisitNotePrint("MSWProgressNote"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWProgNote_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MSWVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                            {
                                noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                            {
                                noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                            }
                        }

                        if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                            {
                                noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                            }
                        }
                        viewData.Questions = noteQuestions;
                    }
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);

                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("MSW/MSWVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("MSW/MSWVisitPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MSWVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new MSWPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWVisitBlank()
        {
            return View("MSW/MSWVisitPrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MSWVisitPdfBlank()
        {
            var doc = new MSWPdf(assessmentService.GetVisitNotePrint("MSWVisit"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MSWAssessmentPdfBlank()
        {
            var doc = new MSWPdf(assessmentService.GetVisitNotePrint("MSWAssessment"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWAssessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MSWDischargePdfBlank()
        {
            var doc = new MSWPdf(assessmentService.GetVisitNotePrint("MSWDischarge"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TransportationNote(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("MSW/TransportationNote", assessmentService.GetTransportationNote(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TransportationNotePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = assessmentService.GetTransportationNote(episodeId, patientId, eventId);
            var xml = new VisitNoteXml(note, PdfDocs.TransportationLog);
            note.PrintViewJson = xml.GetJson();
            return View("MSW/TransportationNotePrint", note);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult TransportationNotePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TransportationPdf(assessmentService.GetTransportationNote(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=TransportationLog_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TransportationNoteBlank()
        {
            return View("MSW/TransportationNotePrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult TransportationNotePdfBlank()
        {
            var doc = new TransportationPdf(assessmentService.GetVisitNotePrint("DriverOrTransportationNote"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=TransportationLog_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundCare(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();

            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToWoundCareDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.PatientId = patientId;
                viewData.EpisodeId = episodeId;
                viewData.EventId = eventId;
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("Nursing/WoundCare", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundCareSave(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Wound care could not be saved" };
            if (patientService.SaveWoundCare(formCollection, Request.Files))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your wound care flowsheet has been saved successfully";
            }
            return View("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SixtyDaySummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null && scheduledEvent.EventDate.IsValidDate())
                {
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                        if (assessment != null)
                        {
                            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                            oasisQuestions = assessment.ToNotesQuestionDictionary();
                            viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                        }

                        viewData.Questions = oasisQuestions;
                        var vitalSigns = patientService.GetVitalSignsForSixtyDaySummary(patientId, episodeId , scheduledEvent.EventDate.ToDateTime());
                        if (vitalSigns != null && vitalSigns.Count > 0)
                        {
                            if (viewData.Questions != null)
                            {
                                #region BP

                                var bp = new List<string>();
                                var bpSysLow = int.MinValue;
                                var bpSysHigh = int.MaxValue;
                                var bpDiaLow = int.MinValue;
                                var bpDiaHigh = int.MaxValue;

                                var bpSitLeft = vitalSigns.Where(v => v.BPSittingLeft.IsNotNullOrEmpty()).Select(v => v.BPSittingLeft).ToList();
                                var bpSitRight = vitalSigns.Where(v => v.BPSittingRight.IsNotNullOrEmpty()).Select(v => v.BPSittingRight).ToList();
                                var bpStandLeft = vitalSigns.Where(v => v.BPStandingLeft.IsNotNullOrEmpty()).Select(v => v.BPStandingLeft).ToList();
                                var bpStandRight = vitalSigns.Where(v => v.BPStandingRight.IsNotNullOrEmpty()).Select(v => v.BPStandingRight).ToList();
                                var bpLyLeft = vitalSigns.Where(v => v.BPLyingLeft.IsNotNullOrEmpty()).Select(v => v.BPLyingLeft).ToList();
                                var bpLyRight = vitalSigns.Where(v => v.BPLyingRight.IsNotNullOrEmpty()).Select(v => v.BPLyingRight).ToList();

                                if (bpSitLeft != null && bpSitLeft.Count > 0) bp.AddRange(bpSitLeft.AsEnumerable());
                                if (bpSitRight != null && bpSitRight.Count > 0) bp.AddRange(bpSitRight.AsEnumerable());
                                if (bpStandLeft != null && bpStandLeft.Count > 0) bp.AddRange(bpStandLeft.AsEnumerable());
                                if (bpStandRight != null && bpStandRight.Count > 0) bp.AddRange(bpStandRight.AsEnumerable());
                                if (bpLyLeft != null && bpLyLeft.Count > 0) bp.AddRange(bpLyLeft.AsEnumerable());
                                if (bpLyRight != null && bpLyRight.Count > 0) bp.AddRange(bpLyRight.AsEnumerable());

                                if (bp != null && bp.Count > 0)
                                {
                                    bpSysLow = bp.Select(v =>
                                    {
                                        var min = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                        if (min != null && min.Length > 1)
                                        {
                                            if (min[0].IsInteger())
                                            {
                                                return min[0].ToInteger();
                                            }
                                        }
                                        return int.MinValue;
                                    }).Min();

                                    bpSysHigh = bp.Select(v =>
                                    {
                                        var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                        if (max != null && max.Length > 1)
                                        {
                                            if (max[0].IsInteger())
                                            {
                                                return max[0].ToInteger();
                                            }
                                        }
                                        return int.MaxValue;
                                    }).Max();

                                    bpDiaLow = bp.Select(v =>
                                    {
                                        var min = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                        if (min != null && min.Length > 1)
                                        {
                                            if (min[1].IsInteger())
                                            {
                                                return min[1].ToInteger();
                                            }
                                        }
                                        return int.MinValue;
                                    }).Min();

                                    bpDiaHigh = bp.Select(v =>
                                    {
                                        var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                        if (max != null && max.Length > 1)
                                        {
                                            if (max[1].IsInteger())
                                            {
                                                return max[1].ToInteger();
                                            }
                                        }
                                        return int.MaxValue;
                                    }).Max();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBPMax"))
                                {
                                    viewData.Questions["VitalSignBPMax"].Answer = bpSysHigh != int.MaxValue ? bpSysHigh.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBPMax", new NotesQuestion { Name = "VitalSignBPMax", Answer = bpSysHigh != int.MaxValue ? bpSysHigh.ToString() : string.Empty });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBPMin"))
                                {
                                    viewData.Questions["VitalSignBPMin"].Answer = bpSysLow != int.MinValue ? bpSysLow.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBPMin", new NotesQuestion { Name = "VitalSignBPMin", Answer = bpSysLow != int.MinValue ? bpSysLow.ToString() : string.Empty });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBPDiaMax"))
                                {
                                    viewData.Questions["VitalSignBPDiaMax"].Answer = bpDiaHigh != int.MaxValue ? bpDiaHigh.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBPDiaMax", new NotesQuestion { Name = "VitalSignBPDiaMax", Answer = bpDiaHigh != int.MaxValue ? bpDiaHigh.ToString() : string.Empty });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBPDiaMin"))
                                {
                                    viewData.Questions["VitalSignBPDiaMin"].Answer = bpDiaLow != int.MinValue ? bpDiaLow.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBPDiaMin", new NotesQuestion { Name = "VitalSignBPDiaMin", Answer = bpDiaLow != int.MinValue ? bpDiaLow.ToString() : string.Empty });
                                }

                                #endregion

                                #region HR

                                var apicalPulseMax = int.MinValue;
                                var apicalPulseMin = int.MaxValue;
                                var apicalPulse = vitalSigns.Where(v => v.ApicalPulse.IsNotNullOrEmpty() && v.ApicalPulse.IsInteger()).Select(v => v.ApicalPulse.ToInteger()).ToList();
                                if (apicalPulse != null && apicalPulse.Count > 0)
                                {
                                    apicalPulseMax = apicalPulse.Max();
                                    apicalPulseMin = apicalPulse.Min();
                                }

                                var radialPulseMax = int.MinValue;
                                var radialPulseMin = int.MaxValue;
                                var radialPulse = vitalSigns.Where(v => v.RadialPulse.IsNotNullOrEmpty() && v.RadialPulse.IsInteger()).Select(v => v.RadialPulse.ToInteger()).ToList();
                                if (radialPulse != null && radialPulse.Count > 0)
                                {
                                    radialPulseMax = radialPulse.Max();
                                    radialPulseMin = radialPulse.Min();
                                }

                                var maxHR = Math.Max(apicalPulseMax, radialPulseMax);
                                if (viewData.Questions.ContainsKey("VitalSignHRMax"))
                                {
                                    viewData.Questions["VitalSignHRMax"].Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignHRMax", new NotesQuestion { Name = "VitalSignHRMax", Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty });
                                }

                                var minHR = Math.Min(apicalPulseMin, radialPulseMin);

                                if (viewData.Questions.ContainsKey("VitalSignHRMin"))
                                {
                                    viewData.Questions["VitalSignHRMin"].Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignHRMin", new NotesQuestion { Name = "VitalSignHRMin", Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty });
                                }
                                #endregion

                                #region Resp

                                var respMax = int.MaxValue;
                                var respMin = int.MinValue;
                                var resp = vitalSigns.Where(v => v.Resp.IsNotNullOrEmpty() && v.Resp.IsInteger()).Select(v => v.Resp.ToInteger()).ToList();
                                if (resp != null && resp.Count > 0)
                                {
                                    respMin = resp.Min();
                                    respMax = resp.Max();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignRespMax"))
                                {
                                    viewData.Questions["VitalSignRespMax"].Answer = respMax != int.MaxValue ? respMax.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignRespMax", new NotesQuestion { Name = "VitalSignRespMax", Answer = respMax != int.MaxValue ? respMax.ToString() : "" });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignRespMin"))
                                {
                                    viewData.Questions["VitalSignRespMin"].Answer = respMin != int.MinValue ? respMin.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignRespMin", new NotesQuestion { Name = "VitalSignRespMin", Answer = respMin != int.MinValue ? respMin.ToString() : "" });
                                }

                                #endregion

                                #region Temp

                                var tempMax = double.MaxValue;
                                var tempMin = double.MinValue;
                                var temp = vitalSigns.Where(v => v.Temp.IsNotNullOrEmpty() && v.Temp.IsDouble()).Select(v => v.Temp.ToDouble()).ToList();
                                if (temp != null && temp.Count > 0)
                                {
                                    tempMax = temp.Max();
                                    tempMin = temp.Min();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignTempMax"))
                                {
                                    viewData.Questions["VitalSignTempMax"].Answer = tempMax != double.MaxValue ? tempMax.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignTempMax", new NotesQuestion { Name = "VitalSignTempMax", Answer = tempMax != double.MaxValue ? tempMax.ToString() : "" });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignTempMin"))
                                {
                                    viewData.Questions["VitalSignTempMin"].Answer = tempMin != double.MinValue ? tempMin.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignTempMin", new NotesQuestion { Name = "VitalSignTempMin", Answer = tempMin != double.MinValue ? tempMin.ToString() : "" });
                                }

                                #endregion

                                #region BS

                                var bsMax = int.MaxValue;
                                var bsMin = int.MinValue;
                                var bs = vitalSigns.Where(v => v.BS.IsNotNullOrEmpty() && v.BS.IsInteger()).Select(v => v.BS.Replace("/", "").ToDigitsOnly().ToInteger()).ToList();
                                if (bs != null && bs.Count > 0)
                                {
                                    bsMax = bs.Max();
                                    bsMin = bs.Min();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBGMax"))
                                {
                                    viewData.Questions["VitalSignBGMax"].Answer = bsMax != int.MaxValue ? bsMax.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBGMax", new NotesQuestion { Name = "VitalSignBGMax", Answer = bsMax != int.MaxValue ? bsMax.ToString() : "" });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBGMin"))
                                {
                                    viewData.Questions["VitalSignBGMin"].Answer = bsMin != int.MinValue ? bsMin.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBGMin", new NotesQuestion { Name = "VitalSignBGMin", Answer = bsMin != int.MinValue ? bsMin.ToString() : "" });
                                }
                                #endregion

                                #region Weight

                                var weightMax = double.MaxValue;
                                var weightMin = double.MinValue;
                                var weight = vitalSigns.Where(v => v.Weight.IsNotNullOrEmpty() && v.Weight.IsDouble()).Select(v => v.Weight.ToDigitsOnly().ToDouble()).ToList();
                                if (weight != null && weight.Count > 0)
                                {
                                    weightMin = weight.Min();
                                    weightMax = weight.Max();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignWeightMax"))
                                {
                                    viewData.Questions["VitalSignWeightMax"].Answer = weightMax != double.MaxValue ? weightMax.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignWeightMax", new NotesQuestion { Name = "VitalSignWeightMax", Answer = weightMax != double.MaxValue ? weightMax.ToString() : "" });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignWeightMin"))
                                {
                                    viewData.Questions["VitalSignWeightMin"].Answer = weightMin != double.MinValue ? weightMin.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignWeightMin", new NotesQuestion { Name = "VitalSignWeightMin", Answer = weightMin != double.MinValue ? weightMin.ToString() : "" });
                                }

                                #endregion 

                                #region Pain

                                var painMax = int.MaxValue;
                                var painMin = int.MinValue;
                                var pain = vitalSigns.Where(v => v.PainLevel.IsNotNullOrEmpty() && v.PainLevel.IsInteger()).Select(v => v.PainLevel.ToInteger()).ToList();
                                if (pain != null && pain.Count > 0)
                                {
                                    painMin = pain.Min();
                                    painMax = pain.Max();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignPainMax"))
                                {
                                    viewData.Questions["VitalSignPainMax"].Answer = painMax != int.MaxValue ? painMax.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignPainMax", new NotesQuestion { Name = "VitalSignPainMax", Answer = painMax != int.MaxValue ? painMax.ToString() : "" });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignPainMin"))
                                {
                                    viewData.Questions["VitalSignPainMin"].Answer = painMin != int.MinValue ? painMin.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignPainMin", new NotesQuestion { Name = "VitalSignPainMin", Answer = painMin != int.MinValue ? painMin.ToString() : "" });
                                }

                                #endregion
                            }

                        }
                        if (viewData.Questions != null)
                        {
                            if (patient != null)
                            {
                                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                                {
                                    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                                    if (physician != null)
                                    {
                                        if (viewData.Questions.ContainsKey("Physician"))
                                        {
                                            viewData.Questions["Physician"].Answer = physician.Id.ToString();
                                        }
                                        else
                                        {
                                            viewData.Questions.Add("Physician", new NotesQuestion { Name = "Physician", Answer = physician.Id.ToString() });
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        viewData.Questions = noteQuestions;
                    }
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                        if (evnt != null)
                        {
                            viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                        }
                    }
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }

            return PartialView("Nursing/60DaySummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SixtyDaySummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = assessmentService.GetVisitNotePrint(episodeId, patientId, eventId);
            var xml = new VisitNoteXml(note, PdfDocs.SixtyDaySummary);
            note.PrintViewJson = xml.GetJson();
            return View("Nursing/60DaySummaryPrint", note);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SixtyDaySummaryBlank()
        {
            return View("Nursing/60DaySummaryPrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SixtyDaySummaryPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new SixtyDaySummaryPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SixtyDaySummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult SixtyDaySummaryPdfBlank()
        {
            var doc = new SixtyDaySummaryPdf(assessmentService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SixtyDaySummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TransferSummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                    }
                }
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    viewData.Questions = oasisQuestions;
                }
                else
                {
                    var noteQuestions = patientvisitNote.ToDictionary();
                    if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                    {
                        if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                        {
                            noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                        }
                    }

                    if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                    {
                        if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                        {
                            noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                        }
                    }

                    if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                    {
                        if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                        {
                            noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                        }
                    }

                    if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                    {
                        if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                        {
                            noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                        }
                    }
                    viewData.Questions = noteQuestions;
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                    }
                }
                viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";

            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {

                    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                        viewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                    }
                }
            }
            return PartialView("Nursing/TransferSummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult TransferSummaryPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TransferSummaryPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=TransferSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TransferSummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Nursing/TransferSummaryPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult TransferSummaryPdfBlank()
        {
            var visit = assessmentService.GetVisitNotePrint();
            visit.Type = DisciplineTasks.TransferSummary.ToString();
            var doc = new TransferSummaryPdf(visit);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=TransferSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult CoordinationOfCarePdfBlank()
        {
            var visit = assessmentService.GetVisitNotePrint();
            visit.Type = DisciplineTasks.CoordinationOfCare.ToString();
            var doc = new TransferSummaryPdf(visit);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=TransferSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TransferSummaryBlank()
        {
            return PartialView("Nursing/TransferSummaryPrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HHAVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    var pocEvent = patientRepository.GetHHAPlanOfCareVisitNote(episodeId, patientId);
                    viewData.Questions = pocEvent.ToHHADefaults();
                }
                else
                {
                    viewData.Questions = patientvisitNote.ToDictionary();
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var scheduledEvent = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (scheduledEvent != null)
                    {
                        viewData.VisitDate = scheduledEvent.VisitDate.IsNotNullOrEmpty() && scheduledEvent.VisitDate.IsValidDate() ? scheduledEvent.VisitDate : scheduledEvent.EventDate;
                        viewData.PreviousNotes = patientService.GetPreviousHHANotes(patientId, scheduledEvent);
                    }
                    viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(episode, DisciplineTasks.HHAideCarePlan);
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            return PartialView("HHA/VisitNote", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HHAVisitContent(Guid patientId, Guid noteId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("HHA/VisitNoteContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHAVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("HHA/VisitNotePrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult HHAVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new HHAVisitPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHAVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHAVisitBlank()
        {
            return PartialView("HHA/VisitNotePrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult HHAVisitPdfBlank()
        {
            var doc = new HHAVisitPdf(assessmentService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHAVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HHACarePlan(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Allergies = patientService.GetAllergies(patientId);
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                    }
                }
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    viewData.Questions = oasisQuestions;
                }
                else
                {
                    var noteQuestions = patientvisitNote.ToDictionary();
                    if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                    {
                        if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                        {
                            noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                        }
                    }

                    if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
                    {
                        if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                        {
                            noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                        }
                    }

                    if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                    {
                        if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                        {
                            noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                        }
                    }

                    if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
                    {
                        if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                        {
                            noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                        }
                    }
                    viewData.Questions = noteQuestions;
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            return PartialView("HHA/CarePlan", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHACarePlanPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("HHA/CarePlanPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult HHACarePlanPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new HHACarePlanPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHACarePlan_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHACarePlanBlank()
        {
            return PartialView("HHA/CarePlanPrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult HHACarePlanPdfBlank()
        {
            var doc = new HHACarePlanPdf(assessmentService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHACarePlan_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DischargeSummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;

                        var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, evnt.EventDate.ToDateTime());
                        if (assessment != null)
                        {
                            viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                        }
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {
                    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                    }
                }
            }
            return PartialView("Nursing/DischargeSummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult DischargeSummaryPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new DischargeSummaryPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=DischargeSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargeSummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Nursing/DischargeSummaryPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult DischargeSummaryPdfBlank()
        {
            var doc = new DischargeSummaryPdf(assessmentService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=DischargeSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargeSummaryBlank()
        {
            return View("Nursing/DischargeSummaryPrint", assessmentService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditEpisode(Guid episodeId, Guid patientId)
        {
            PatientEpisode patientEpisode;
            if (patientId.IsEmpty() || episodeId.IsEmpty())
            {
                var episodeViewData = new PatientEpisode();
                var selection = new List<SelectListItem>();
                episodeViewData.AdmissionDates = selection;
                return PartialView("Episode/Edit", episodeViewData);
            }
            else
            {
                patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                if (patientEpisode != null)
                {
                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        patientEpisode.DisplayName = patient.DisplayName;
                    }
                    patientEpisode.Detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                }
                var selection = new List<SelectListItem>();
                var admissiondates = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
                if (admissiondates != null && admissiondates.Count > 0)
                {
                    selection = admissiondates.Select(a => new SelectListItem { Text = a.StartOfCareDate.ToString("MM/dd/yyy"), Value = a.Id.ToString(), Selected = patientEpisode.AdmissionId.IsEmpty() ? patientEpisode.StartOfCareDate.Date == a.StartOfCareDate.Date : a.Id == patientEpisode.AdmissionId }).ToList();
                }
                patientEpisode.AdmissionDates = selection;
            }
            return PartialView("Episode/Edit", patientEpisode);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateEpisode([Bind] PatientEpisode patientEpisode)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Episode could not be saved." };
            var patient = patientRepository.Get(patientEpisode.PatientId, Current.AgencyId);
            var validationRules = new List<Validation>();
            validationRules.Add(new Validation(() => !(patient != null), "Patient data is not available."));
            if (patient != null)
            {
                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientEpisode.AdmissionId);
                validationRules.Add(new Validation(() => !(admissionData != null), "Admission data is not available."));
            }
            validationRules.Add(new Validation(() => !patientEpisode.StartDate.IsValid(), "Episode start date is not valid date."));
            validationRules.Add(new Validation(() => !patientEpisode.EndDate.IsValid(), "Episode end date is not valid date."));
            if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
            {
                validationRules.Add(new Validation(() => !(patientEpisode.StartDate < patientEpisode.EndDate), "Episode start date must be less than episode end date."));
                validationRules.Add(new Validation(() => !(patientEpisode.EndDate.Subtract(patientEpisode.StartDate).Days <= 60), "Episode period can't be more than 60 days."));
            }
            if (!patientEpisode.IsActive)
            {
                validationRules.Add(new Validation(() => !patientService.IsValidEpisode(patientEpisode.Id, patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate), "Episode date is not in the valid date range."));
            }
            if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
            {
                validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate <= patientEpisode.StartDate), "Episode start date must be greater than start of care date."));
                validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate < patientEpisode.EndDate), "Episode end date must be   greater than start of care date."));
            }
            var entityValidator = new EntityValidator(validationRules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                patientEpisode.IsActive = !patientEpisode.IsActive;
                if (patientService.UpdateEpisode(patientEpisode))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Episode has been successfully updated.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = entityValidator.Message;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewPatientEpisode()
        {
            var episodeViewData = new NewEpisodeData();
            var selection = new List<SelectListItem>();
            episodeViewData.AdmissionDates = selection;
            return PartialView("NewEpisode", episodeViewData);
        }

        public ActionResult NewPatientEpisodeContent(Guid patientId)
        {
            var episodeViewData = new NewEpisodeData();
            if (!patientId.IsEmpty())
            {
                var episode = patientRepository.GetLastEpisode(Current.AgencyId, patientId);
                if (episode != null)
                {
                    episodeViewData = episode;
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        episodeViewData.PrimaryPhysician = physician.Id.ToString();
                    }
                }
                else
                {
                    var patient = patientRepository.Get(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        episodeViewData.PatientId = patientId;
                        episodeViewData.DisplayName = patient.DisplayName;
                        episodeViewData.StartOfCareDate = patient.StartofCareDate;
                        episodeViewData.CaseManager = patient.CaseManagerId.ToString();
                        episodeViewData.PrimaryInsurance = patient.PrimaryInsurance;
                        episodeViewData.SecondaryInsurance = patient.SecondaryInsurance;
                        episodeViewData.AdmissionId = patient.AdmissionId;
                        if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                        {
                            var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
                            if (primaryPhysician == null)
                            {
                                primaryPhysician = patient.PhysicianContacts.First();
                            }
                            episodeViewData.PrimaryPhysician = primaryPhysician.Id.ToString();
                        }
                    }
                }
                var selection = new List<SelectListItem>();
                var admissiondates = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
                if (admissiondates != null && admissiondates.Count > 0)
                {
                    selection = admissiondates.Select(a => new SelectListItem { Text = a.StartOfCareDate.ToString("MM/dd/yyy"), Value = a.Id.ToString(), Selected = a.Id == episodeViewData.AdmissionId }).ToList();
                }
                episodeViewData.AdmissionDates = selection;
            }
            return PartialView("NewEpisodeContent", episodeViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewEpisode(Guid patientId)
        {
            var episodeViewData = new NewEpisodeData();
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.Get(patientId, Current.AgencyId);
                if (patient != null)
                {
                    episodeViewData.PatientId = patientId;
                    episodeViewData.DisplayName = patient.DisplayName;
                    episodeViewData.StartOfCareDate = patient.StartofCareDate;
                    episodeViewData.CaseManager = patient.CaseManagerId.ToString();
                    episodeViewData.PrimaryInsurance = patient.PrimaryInsurance;
                    episodeViewData.SecondaryInsurance = patient.SecondaryInsurance;
                    episodeViewData.AdmissionId = patient.AdmissionId;
                    if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                    {
                        var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
                        if (primaryPhysician == null)
                        {
                            primaryPhysician = patient.PhysicianContacts.First();
                        }
                        episodeViewData.PrimaryPhysician = primaryPhysician.Id.ToString();
                    }

                    var episode = patientRepository.GetLastEpisode(Current.AgencyId, patientId);
                    if (episode != null)
                    {
                        episodeViewData.EndDate = episode.EndDate;
                        episodeViewData.StartDate = episode.StartDate;
                    }
                    else
                    {
                        episodeViewData.EndDate = DateTime.MinValue.Date;
                        episodeViewData.StartDate = DateTime.MinValue.Date;
                    }
                    var selection = new List<SelectListItem>();
                    var admissiondates = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
                    if (admissiondates != null && admissiondates.Count > 0)
                    {
                        selection = admissiondates.Select(a => new SelectListItem { Text = a.StartOfCareDate.ToString("MM/dd/yyy"), Value = a.Id.ToString(), Selected = a.Id == episodeViewData.AdmissionId }).ToList();
                    }
                    else
                    {
                        var admission = patientService.GetIfExitOrCreate(patientId);
                        if (admission != null)
                        {
                            selection.Add(new SelectListItem { Text = admission.StartOfCareDate.ToString("MM/dd/yyy"), Value = admission.Id.ToString(), Selected = true });
                        }
                    }
                    episodeViewData.AdmissionDates = selection;
                }
            }
            return PartialView("Episode/New", episodeViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddEpisode(PatientEpisode patientEpisode, EpisodeDetail detail)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Episode could not be saved" };
            if (patientEpisode != null)
            {
                patientEpisode.Detail = detail;
                var patient = patientRepository.Get(patientEpisode.PatientId, Current.AgencyId);
                var validationRules = new List<Validation>();
                validationRules.Add(new Validation(() => !(patient != null), "Patient data is not available."));
                if (patient != null)
                {
                    var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientEpisode.AdmissionId);
                    validationRules.Add(new Validation(() => !(admissionData != null), "Admission data is not available."));
                }
                validationRules.Add(new Validation(() => !patientEpisode.StartDate.IsValid(), "Episode start date is not valid date."));
                validationRules.Add(new Validation(() => !patientEpisode.EndDate.IsValid(), "Episode end date is not valid date."));
                if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
                {
                    validationRules.Add(new Validation(() => !(patientEpisode.StartDate < patientEpisode.EndDate), "Episode start date must be less than episode end date."));
                    validationRules.Add(new Validation(() => !(patientEpisode.EndDate.Subtract(patientEpisode.StartDate).Days <= 60), "Episode period can't be more than 60 days."));
                }
                validationRules.Add(new Validation(() => !patientService.IsValidEpisode(patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate), "Episode date is not in the valid date range."));
                if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
                {
                    validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate <= patientEpisode.StartDate), "Episode start date must be greater than start of care date."));
                    validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate < patientEpisode.EndDate), "Episode end date must be   greater than start of care date."));
                }
                var entityValidator = new EntityValidator(validationRules.ToArray());
                entityValidator.Validate();

                if (entityValidator.IsValid && patient != null)
                {
                    var workflow = new CreateEpisodeWorkflow(patient, patientEpisode);
                    if (workflow.IsCommitted)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Episode was created successfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = workflow.Message;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyWorksheet(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Nursing/NotesSupplyWorkSheet", patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View(new GridModel(patientService.GetNoteSupply(episodeId, patientId, eventId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, int supplyId, string quantity, string date)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNegativeOrZero(supplyId, "supplyId");
            Check.Argument.IsNotNull(quantity, "quantity");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The new supply could not added." };
            if (patientService.AddNoteSupply(episodeId, patientId, eventId, supplyId, quantity, date))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The new supply was successfully added.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            patientService.UpdateNoteSupply(episodeId, patientId, eventId, supply);
            return View(new GridModel(patientService.GetNoteSupply(episodeId, patientId, eventId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            patientService.DeleteNoteSupply(episodeId, patientId, eventId, supply);
            return View(new GridModel(patientService.GetNoteSupply(episodeId, patientId, eventId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be saved. Please try again." };
            var patientEpisode = patientRepository.GetEpisode(Current.AgencyId, formCollection["episodeId"].ToGuid(), formCollection["patientId"].ToGuid());
            var oldEvents = (patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()).Where(s => !s.IsDeprecated).OrderByDescending(o => o.EventDate.ToDateTime()).ToList();
            var events = oldEvents.OrderByDescending(o => o.EventDate.ToDateTime().Date);
            var newEvents = JsonExtensions.FromJson<List<ScheduleEvent>>(formCollection["Patient_Schedule"]).OrderBy(e => e.EventDate.ToDateTime().Date);
            foreach (var evnt in newEvents)
            {
                if (evnt.DisciplineTask == (int)DisciplineTasks.Rap)
                {
                    var rap = billingRepository.GetRap(Current.AgencyId, formCollection["patientId"].ToGuid(), formCollection["episodeId"].ToGuid());
                    if (rap != null)
                    {
                        viewData.errorMessage = "Rap already created. To create a new one, delete the existing first.";
                        return Json(viewData);
                    }
                }
                if (evnt.DisciplineTask == (int)DisciplineTasks.Final)
                {
                    var final = billingRepository.GetFinal(Current.AgencyId, formCollection["PatientId"].ToGuid(), formCollection["episodeId"].ToGuid());
                    if (final != null)
                    {
                        viewData.errorMessage = "Final already created. To create a new one, delete the existing first.";
                        return Json(viewData);
                    }
                }
                if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT))
                {
                    var transfer = oldEvents.FirstOrDefault(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT) && (oe.EventDate.ToDateTime().Date < evnt.EventDate.ToDateTime().Date));
                    ScheduleEvent roc = null;
                    if (transfer != null)
                    {
                        roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date));
                    }
                    if (transfer != null && roc == null)
                    {
                        viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                        return Json(viewData);
                    }
                    else if (transfer != null && roc != null && roc.EventDate.ToDateTime().Date <= transfer.EventDate.ToDateTime().Date)
                    {
                        viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                        return Json(viewData);
                    }
                    else if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT))
                    {
                        viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete that one before creating a new one.";
                        return Json(viewData);
                    }
                    else if (evnt.EventDate.ToDateTime().Date < patientEpisode.EndDate.AddDays(-5).Date || evnt.EventDate.ToDateTime().Date > patientEpisode.EndDate.Date)
                    {
                        viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
                        return Json(viewData);
                    }
                }
                else if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || evnt.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT || evnt.DisciplineTask==(int)DisciplineTasks.OASISCStartofCareOT))
                {
                    if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT || oe.DisciplineTask==(int) DisciplineTasks.OASISCStartofCareOT))
                    {
                        viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete that one before creating a new one.";
                        return Json(viewData);
                    }
                    else if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT))
                    {
                    }
                }
                else if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT))
                {
                    var roc = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT);
                    var transfer = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
                    if (roc == null)
                    {
                        if (transfer == null)
                        {
                            viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                            return Json(viewData);
                        }
                        else if (transfer != null && (transfer.EventDate.ToDateTime() > evnt.EventDate.ToDateTime()))
                        {
                            viewData.errorMessage = "Resumption of Care date should be later that the Transfer date.";
                            return Json(viewData);
                        }
                    }
                    else if (roc != null)
                    {
                        if (transfer != null && (roc.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date))
                        {
                            viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                            return Json(viewData);
                        }
                    }
                }
                else if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT))
                {
                    var transfer = oldEvents.FirstOrDefault(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
                    ScheduleEvent roc = null;
                    if (transfer != null)
                    {
                        roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date));
                    }
                    if (transfer != null && roc == null)
                    {
                        viewData.errorMessage = "Please create a Resumption of Care before creating another Transfer.";
                        return Json(viewData);
                    }
                }
            }
            if (patientService.UpdateEpisode(formCollection["episodeId"].ToGuid(), formCollection["patientId"].ToGuid(), formCollection["Patient_Schedule"]))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your task has been successfully scheduled.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in editing the data.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddMultiple(Guid episodeId, Guid patientId, string DisciplineTask, string Discipline, Guid userId, bool IsBillable, string StartDate, string EndDate)
        {
            Check.Argument.IsNotNull(userId, "userId");
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotEmpty(DisciplineTask, "DisciplineTask");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Event is not Saved" };
            viewData = Validate<JsonViewData>(
                          new Validation(() => string.IsNullOrEmpty(StartDate.ToString()), ". Patient date of birth is required."),
                          new Validation(() => !StartDate.ToString().IsValidDate(), ". Date Of birth  for the patient is not in the valid range."),
                          new Validation(() => string.IsNullOrEmpty(EndDate.ToString()), ". Patient date of birth is required."),
                          new Validation(() => !EndDate.ToString().IsValidDate(), ". Date Of birth  for the patient is not in the valid range.")
                          );
            if (viewData.isSuccessful)
            {
                var patientEpisode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                var oldEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                if (StartDate.ToDateTime().Date >= EndDate.ToDateTime().Date)
                {
                    viewData.errorMessage = "The start date must be greater than end date.";
                    viewData.isSuccessful = false;
                    return Json(viewData);
                }
                else if (StartDate.ToDateTime().Date < patientEpisode.StartDate.Date || StartDate.ToDateTime().Date > patientEpisode.EndDate.Date || EndDate.ToDateTime().Date < patientEpisode.StartDate.Date || EndDate.ToDateTime().Date > patientEpisode.EndDate.Date)
                {
                    viewData.errorMessage = "The start date and end date has to be in the current episode date range.";
                    viewData.isSuccessful = false;
                    return Json(viewData);
                }
                else
                {
                    if (patientService.UpdateEpisode(episodeId, patientId, DisciplineTask, Discipline, userId, IsBillable, StartDate.ToDateTime(), EndDate.ToDateTime()))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Events are successfully Saved.";
                        return Json(viewData);
                    }
                    return Json(viewData);
                }
            }
            else
            {
                return Json(viewData);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddMultiDaySchedule(Guid episodeId, Guid patientId, Guid userId, int disciplineTaskId, string visitDates)
        {
            Check.Argument.IsNotNull(userId, "userId");
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Pick the proper discipline task." };
            var discipline = patientService.GetDisciplineTask(disciplineTaskId);
            if (discipline != null)
            {
                var visitDateArray = visitDates.Split(',').Where(s => s.IsNotNullOrEmpty() && s.IsDate()).ToArray();
                if (!(discipline.IsMultiple == false && visitDateArray.Length > 1))
                {
                    viewData = Validate<JsonViewData>(new Validation(() => string.IsNullOrEmpty(visitDates.ToString()), "Select at least one date from the calendar."));

                    if (viewData.isSuccessful)
                    {
                        var patientEpisode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                        var oldEvents = (patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()).Where(s => !s.IsDeprecated).OrderByDescending(o => o.EventDate.ToDateTime()).ToList();
                        var events = oldEvents.OrderByDescending(o => o.EventDate.ToDateTime());

                        foreach (var date in visitDateArray)
                        {
                            if (date.IsDate())
                            {
                                if (disciplineTaskId == (int)DisciplineTasks.Rap)
                                {
                                    var rap = billingRepository.GetRap(Current.AgencyId, patientId, episodeId);
                                    if (rap != null)
                                    {
                                        viewData.isSuccessful = false;
                                        viewData.errorMessage = "Rap already created. To create a new one, delete the existing first.";
                                        return Json(viewData);
                                    }
                                }
                                if (disciplineTaskId == (int)DisciplineTasks.Final)
                                {
                                    var final = billingRepository.GetFinal(Current.AgencyId, patientId, episodeId);
                                    if (final != null)
                                    {
                                        viewData.isSuccessful = false;
                                        viewData.errorMessage = "Final already created. To create a new one, delete the existing first.";
                                        return Json(viewData);
                                    }
                                }
                                if ((disciplineTaskId == (int)DisciplineTasks.OASISCRecertification || disciplineTaskId == (int)DisciplineTasks.OASISCRecertificationOT || disciplineTaskId == (int)DisciplineTasks.OASISCRecertificationPT))
                                {
                                    var transfer = oldEvents.FirstOrDefault(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT) && (oe.EventDate.ToDateTime() < date.ToDateTime()));
                                    ScheduleEvent roc = null;
                                    if (transfer != null)
                                    {
                                        roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime() > transfer.EventDate.ToDateTime()));
                                    }
                                    if (transfer != null && roc == null)
                                    {
                                        viewData.isSuccessful = false;
                                        viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                        return Json(viewData);
                                    }
                                    else if (transfer != null && roc != null && roc.EventDate.ToDateTime() <= transfer.EventDate.ToDateTime())
                                    {
                                        viewData.isSuccessful = false;
                                        viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                        return Json(viewData);
                                    }
                                    else if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT))
                                    {
                                        viewData.isSuccessful = false;
                                        viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete that one before creating a new one.";
                                        return Json(viewData);
                                    }
                                    else if (date.ToDateTime() < patientEpisode.EndDate.AddDays(-5) || date.ToDateTime() > patientEpisode.EndDate)
                                    {
                                        viewData.isSuccessful = false;
                                        viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
                                        return Json(viewData);
                                    }
                                }
                                else if ((disciplineTaskId == (int)DisciplineTasks.OASISCStartofCare || disciplineTaskId == (int)DisciplineTasks.OASISCStartofCarePT || disciplineTaskId == (int)DisciplineTasks.OASISCStartofCareOT))
                                {
                                    if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT || oe.DisciplineTask==(int)DisciplineTasks.OASISCStartofCareOT))
                                    {
                                        viewData.isSuccessful = false;
                                        viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete that one before creating a new one.";
                                        return Json(viewData);
                                    }
                                    else if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT))
                                    {
                                    }
                                }
                                else if ((disciplineTaskId == (int)DisciplineTasks.OASISCResumptionofCare || disciplineTaskId == (int)DisciplineTasks.OASISCResumptionofCareOT || disciplineTaskId == (int)DisciplineTasks.OASISCResumptionofCarePT))
                                {
                                    var roc = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT);
                                    var transfer = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
                                    if (roc == null)
                                    {
                                        if (transfer == null)
                                        {
                                            viewData.isSuccessful = false;
                                            viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                            return Json(viewData);
                                        }
                                        else if (transfer != null && (transfer.EventDate.ToDateTime() > date.ToDateTime()))
                                        {
                                            viewData.isSuccessful = false;
                                            viewData.errorMessage = "Resumption of Care date should be later that the Transfer date.";
                                            return Json(viewData);
                                        }
                                    }
                                    else if (roc != null)
                                    {
                                        if (transfer != null && (roc.EventDate.ToDateTime() > transfer.EventDate.ToDateTime()))
                                        {
                                            viewData.isSuccessful = false;
                                            viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                            return Json(viewData);
                                        }
                                    }
                                }
                                else if ((disciplineTaskId == (int)DisciplineTasks.OASISCTransfer || disciplineTaskId == (int)DisciplineTasks.OASISCTransferOT || disciplineTaskId == (int)DisciplineTasks.OASISCTransferPT))
                                {
                                    var transfer = oldEvents.FirstOrDefault(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
                                    ScheduleEvent roc = null;
                                    if (transfer != null)
                                    {
                                        roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime() > transfer.EventDate.ToDateTime()));
                                    }
                                    if (transfer != null && roc == null)
                                    {
                                        viewData.isSuccessful = false;
                                        viewData.errorMessage = "Please create a Resumption of Care before creating another Transfer.";
                                        return Json(viewData);
                                    }
                                }
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Date is not in the right format.";
                                return Json(viewData);
                            }
                        }

                        if (!patientService.AddMultiDaySchedule(episodeId, patientId, userId, disciplineTaskId, visitDates))
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Unable to save this page. Please try again.";
                        }
                        else
                        {
                            viewData.errorMessage = "Task(s) scheduled for user succesfully.";
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Mutiple task is not allowed for this discipline.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Get(Guid id, string discipline)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotNull(discipline, "discipline");
            return Json(patientService.GetPatientWithSchedule(id, discipline));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetEpisode(Guid patientId, Guid episodeId, string discipline)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotNull(discipline, "discipline");
            return Json(patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId, discipline));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Notes(string button, FormCollection formCollection) {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The note could not be saved." };
            string type = formCollection["Type"];
            if (type.IsNotNullOrEmpty()) {
                var eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
                var episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
                var patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();
                if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty()) {
                    var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                    if (keys.Contains(type + "_VisitDate")) {
                        rules.Add(new Validation(() => !formCollection[type + "_VisitDate"].IsNotNullOrEmpty(), "Visit date can't be empty."));
                        rules.Add(new Validation(() => !formCollection[type + "_VisitDate"].IsValidDate(), "Visit date is not valid."));
                        rules.Add(new Validation(() => formCollection[type + "_VisitDate"].IsNotNullOrEmpty() && formCollection[type + "_VisitDate"].IsValidDate() ? !(formCollection[type + "_VisitDate"].ToDateTime().Date >= episode.StartDate.Date && formCollection[type + "_VisitDate"].ToDateTime().Date <= episode.EndDate.Date) : true, "Visit date is not in the episode range."));
                    }
                    if (button == "Save") {
                        rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsNotNullOrEmpty(), "Task can't be empty."));
                        if (formCollection["DisciplineTask"].IsNotNullOrEmpty()) rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsInteger(), "Select the right task."));
                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid) {
                            if (patientService.SaveNotes(button, formCollection)) {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "The note was successfully saved.";
                            } else {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "The note could not be saved.";
                            }
                        } else {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message;
                        }
                    } else if (button == "Complete") {
                        if (keys.Contains(type + "_Clinician")) {
                            rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_Clinician"]), "Clinician Signature is required."));
                            rules.Add(new Validation(() => formCollection[type + "_Clinician"].IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, formCollection[type + "_Clinician"]) : false, "User Signature is not correct."));
                        }
                        if (keys.Contains(type + "_TimeIn")) {
                            rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_TimeIn"]), "Time-In is required. "));
                        }
                        if (keys.Contains(type + "_TimeOut")) {
                            rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_TimeOut"]), "Time-Out is required. "));
                        }
                        if (keys.Contains(type + "_SignatureDate")) {
                            rules.Add(new Validation(() => formCollection[type + "_SignatureDate"].IsNullOrEmpty(), "Signature date can't be empty."));
                            rules.Add(new Validation(() => !formCollection[type + "_SignatureDate"].IsValidDate(), "Signature date is not valid."));
                            rules.Add(new Validation(() => formCollection[type + "_SignatureDate"].IsNotNullOrEmpty() && formCollection[type + "_SignatureDate"].IsValidDate() ? !(formCollection[type + "_SignatureDate"].ToDateTime() >= episode.StartDate && formCollection[type + "_SignatureDate"].ToDateTime() <= DateTime.Now) : true, "Signature date is not the in valid range."));
                        }
                        rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsNotNullOrEmpty(), "Task can't be empty."));
                        if (formCollection["DisciplineTask"].IsNotNullOrEmpty()) {
                            rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsInteger() && !Enum.IsDefined(typeof(DisciplineTasks), formCollection["DisciplineTask"].ToInteger()), "Select the right task."));
                        }

                        if (type == DisciplineTasks.PTEvaluation.ToString() || type == DisciplineTasks.PTReEvaluation.ToString()
                            || type == DisciplineTasks.OTEvaluation.ToString() || type == DisciplineTasks.OTReEvaluation.ToString()
                            || type == DisciplineTasks.STEvaluation.ToString() || type == DisciplineTasks.STReEvaluation.ToString())
                        {
                            rules.Add(new Validation(() => formCollection[type + "_PhysicianId"].IsNullOrEmpty(), "Physician is required. "));
                        }

                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid) {
                            if (patientService.SaveNotes(button, formCollection)) {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "The note was successfully Submited.";
                            }
                        } else {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message;
                        }
                    } else if (button == "Approve") {
                        if (patientService.SaveNotes(button, formCollection))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The note was successfully Approved.";
                        } else {
                            return Json(viewData);
                        }
                    } else if (button == "Return") {
                        if (patientService.SaveNotes(button, formCollection)) {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The note was successfully returned.";
                        } else {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "The note could not be returned.";
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId, string reason)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your note could not be saved." };
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                if (button == "Approve")
                {
                    if (patientService.ProcessNotes(button, episodeId, patientId, eventId, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your note has been successfully approved.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your note could not be approved.";
                    }
                }
                else if (button == "Return")
                {
                    if (patientService.ProcessNotes(button, episodeId, patientId, eventId, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your note has been successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your note could not be returned.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BulkUpdate(List<string> CustomValue, string CommandType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The item(s) you selected could not be updated." };
            if (CustomValue != null && CustomValue.Count > 0 && CommandType.IsNotNullOrEmpty())
            {
                int total = CustomValue.Count;
                int count = 0;
                CustomValue.ForEach(v =>
                {
                    var infos = v.Split('|');
                    if (infos.Length == 4 && infos[0].IsGuid() && infos[1].IsGuid() && infos[2].IsGuid() && infos[3].IsInteger())
                    {
                        var scheduleEvent = patientRepository.GetScheduleOnly(Current.AgencyId, infos[0].ToGuid(), infos[1].ToGuid(), infos[2].ToGuid());
                        if (scheduleEvent != null && scheduleEvent.DisciplineTask == infos[3].ToInteger())
                        {
                            var eventType = scheduleEvent.TypeOfEvent();
                            if (eventType.IsNotNullOrEmpty())
                            {
                                switch (eventType)
                                {
                                    case "Notes":
                                        if (patientService.ProcessNotes(CommandType, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, string.Empty))
                                        {
                                            count++;
                                        }
                                        break;
                                    case "OASIS":
                                        if (Enum.IsDefined(typeof(DisciplineTasks), infos[3].ToInteger()))
                                        {
                                            var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), infos[3].ToInteger())).ToString();
                                            if (CommandType == "Approve")
                                            {
                                                if (assessmentService.UpdateAssessmentStatus(scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.EpisodeId, assessmentType, ((int)ScheduleStatus.OasisCompletedExportReady).ToString(), string.Empty))
                                                {
                                                    if ((assessmentType == DisciplineTasks.OASISCDischargeOT.ToString() || assessmentType == DisciplineTasks.OASISCDischarge.ToString() || assessmentType == DisciplineTasks.OASISCDischargePT.ToString() || assessmentType == "DischargeFromAgency") || (assessmentType == DisciplineTasks.OASISCTransferDischarge.ToString() || assessmentType == "OASISCTransferDischarge") || (assessmentType == DisciplineTasks.OASISCDeath.ToString() || assessmentType == DisciplineTasks.OASISCDeathOT.ToString() || assessmentType == DisciplineTasks.OASISCDeathPT.ToString() || assessmentType == "OASISCDeath"))
                                                    {
                                                        var assessment = assessmentService.GetAssessment(scheduleEvent.EventId, assessmentType);
                                                        if (assessment != null)
                                                        {
                                                            var assessmentData = assessment.ToDictionary();
                                                            var schedule = patientRepository.GetSchedule(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                                                            if (schedule != null)
                                                            {
                                                                var date = DateTime.MinValue;
                                                                var dateAssessment = assessmentData["M0906DischargeDate"].Answer;
                                                                if (dateAssessment.IsNotNullOrEmpty() && dateAssessment.IsValidDate())
                                                                {
                                                                    date = dateAssessment.ToDateTime();
                                                                }
                                                                var eventDateSchedule = schedule.EventDate;
                                                                if (eventDateSchedule.IsNotNullOrEmpty() && eventDateSchedule.IsValidDate())
                                                                {
                                                                    date = date.Date > eventDateSchedule.ToDateTime().Date ? date : eventDateSchedule.ToDateTime();
                                                                }
                                                                var visitDateSchedule = schedule.VisitDate;
                                                                if (visitDateSchedule.IsNotNullOrEmpty() && visitDateSchedule.IsValidDate())
                                                                {
                                                                    date = date.Date > visitDateSchedule.ToDateTime().Date ? date : visitDateSchedule.ToDateTime();
                                                                }
                                                                if (date > DateTime.MinValue)
                                                                {
                                                                    patientService.DischargePatient(scheduleEvent.PatientId, date, "Patient dischrge due to discharge oasis.");
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            var schedule = patientRepository.GetSchedule(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                                                            if (schedule != null)
                                                            {
                                                                var date = DateTime.MinValue;
                                                                var eventDateSchedule = schedule.EventDate;
                                                                if (eventDateSchedule.IsNotNullOrEmpty() && eventDateSchedule.IsValidDate())
                                                                {
                                                                    date = date.Date > eventDateSchedule.ToDateTime().Date ? date : eventDateSchedule.ToDateTime();
                                                                }
                                                                var visitDateSchedule = schedule.VisitDate;
                                                                if (visitDateSchedule.IsNotNullOrEmpty() && visitDateSchedule.IsValidDate())
                                                                {
                                                                    date = date.Date > visitDateSchedule.ToDateTime().Date ? date : visitDateSchedule.ToDateTime();
                                                                }
                                                                if (date > DateTime.MinValue)
                                                                {
                                                                    patientService.DischargePatient(scheduleEvent.PatientId, date, "Patient dischrge due to discharge oasis.");
                                                                }
                                                            }
                                                        }
                                                    }
                                                    count++;
                                                }
                                            }
                                            else if (CommandType == "Return")
                                            {
                                                if (assessmentService.UpdateAssessmentStatus(scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.EpisodeId, assessmentType, ((int)ScheduleStatus.OasisReturnedForClinicianReview).ToString(), string.Empty))
                                                {
                                                    count++;
                                                }
                                            }
                                            else if (CommandType == "Print")
                                            {
                                                if (scheduleEvent != null)
                                                {
                                                    scheduleEvent.InPrintQueue = false;
                                                    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                                                    {
                                                        count++;
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case "PhysicianOrder":
                                        if (patientService.ProcessPhysicianOrder(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, CommandType, string.Empty))
                                        {
                                            count++;
                                        }
                                        break;
                                    case "PlanOfCare":
                                        if (assessmentService.UpdatePlanofCareStatus(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, CommandType, string.Empty))
                                        {
                                            count++;
                                        }
                                        break;

                                    case "IncidentAccident":
                                        if (agencyService.ProcessIncidents(CommandType, scheduleEvent.PatientId, scheduleEvent.EventId, string.Empty))
                                        {
                                            count++;
                                        }
                                        break;
                                    case "Infection":
                                        if (agencyService.ProcessInfections(CommandType, scheduleEvent.PatientId, scheduleEvent.EventId, string.Empty))
                                        {
                                            count++;
                                        }
                                        break;
                                    case "CommunicationNote":
                                        if (patientService.ProcessCommunicationNotes(CommandType, scheduleEvent.PatientId, scheduleEvent.EventId, string.Empty))
                                        {
                                            count++;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                });
                viewData.isSuccessful = true;
                if (count == total)
                {
                    viewData.errorMessage = string.Format("All ({0}) items were updated successfully.", count);
                }
                else if (count < total)
                {
                    viewData.errorMessage = string.Format("{0} out of {1} items were updated successfully. Try the again.", count, total);
                }

            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Wound care asset could not be deleted." };
            if (patientService.DeleteWoundCareAsset(episodeId, patientId, eventId, name, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Wound care asset successfully deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteScheduleEventAsset(Guid patientId, Guid episodeId, Guid eventId, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Asset Not Deleted." };
            if (patientService.DeleteScheduleEventAsset(episodeId, patientId, eventId, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Asset Successfully Deleted.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activity(Guid episodeId, Guid patientId, string discipline)
        {
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");

            var patientActivities = patientService.GetScheduledEvents(episodeId, patientId, discipline);
            return View(new GridModel(patientActivities));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReAssign(Guid episodeId, Guid patientId, Guid eventId, Guid oldUserId, Guid userId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(userId, "userId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Event is not Saved" };
            if (patientService.Reassign(episodeId, patientId, eventId, oldUserId, userId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Event is reassigned sucessfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReAssignSchedules()
        {
            var viewData = new ReassignViewData { EpisodeId = Guid.Empty, PatientId = Guid.Empty, Type = "All" };
            return PartialView("Reassign", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReAssignSchedulesContent(Guid episodeId, Guid patientId, string type)
        {
            var viewData = new ReassignViewData { EpisodeId = episodeId, PatientId = patientId, Type = type };
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    viewData.PatientDisplayName = patient.DisplayName;
                }
            }
            return PartialView("Reassign", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReAssignPatientSchedules(Guid PatientId, Guid EmployeeOldId, Guid EmployeeId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Events is not reassigned." };
            if (!PatientId.IsEmpty() && !EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    if (patientService.ReassignSchedules(PatientId, EmployeeOldId, EmployeeId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Events are reassigned sucessfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Events are not reassigned. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee  has to be different from the previous one.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignEpisodeSchedules(Guid PatientId, Guid EmployeeOldId, Guid EmployeeId)
        {
            JsonViewData viewData = new JsonViewData { isSuccessful = false, errorMessage = "Events is not reassigned." };
            if (!PatientId.IsEmpty() && !EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    if (patientService.ReassignSchedules(PatientId, EmployeeOldId, EmployeeId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Events are reassigned sucessfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Events are not reassigned. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee  has to be different from the previous one.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignAllSchedules(Guid EmployeeOldId, Guid EmployeeId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Events is not reassigned." };
            if (!EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    if (patientService.ReassignSchedules(EmployeeOldId, EmployeeId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Events are reassigned sucessfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Events are not reassigned. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee  has to be different from the previous one.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Reopen(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task cannot be reopened." };
            if (patientService.Reopen(episodeId, patientId, eventId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Task has been reopened sucessfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId, int task)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error trying to delete this task. Please try again." };
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty() && task >= 0)
            {
                if (patientService.DeleteSchedule(episodeId, patientId, eventId, employeeId, task))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Task has been successfully deleted.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Restore(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error trying to restore this task. Please try again." };
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                if (patientService.RestoreTask(episodeId, patientId, eventId))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Task has been successfully restored.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MasterCalendarMain(Guid patientId, Guid episodeId)
        {
            var patientEpisode = patientService.GetPatientEpisodeWithFrequency(episodeId, patientId);
            return PartialView("MasterCalendar", patientEpisode);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MasterCalendar(Guid patientId, Guid episodeId)
        {
            var patientEpisode = patientService.GetPatientEpisodeWithFrequency(episodeId, patientId);
            return PartialView("MasterCalendar", patientEpisode);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MasterCalendarPdf(Guid patientId, Guid episodeId)
        {
            var patientEpisode = patientService.GetPatientEpisodeWithFrequency(episodeId, patientId);
            var doc = new MasterCalendarPdf(patientEpisode);
            var PdfStream = doc.GetStream();
            PdfStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MasterCalendar_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(PdfStream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CalendarNav(Guid patientId, Guid episodeId, string discipline)
        {
            CalendarViewData calendarViewData = null;
            if (!patientId.IsEmpty() && !episodeId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    calendarViewData = new CalendarViewData();
                    var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId, discipline);
                    if (episode != null)
                    {
                        calendarViewData.Episode = episode;
                        calendarViewData.PatientId = patientId;
                    }
                    else
                    {
                        calendarViewData.PatientId = patientId;
                    }
                    calendarViewData.IsDischarged = patient.IsDischarged;
                }
            }
            return PartialView("Calendar", calendarViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ActivityFirstTime(Guid patientId, string discipline)
        {
            var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline);
            if (patientId.IsEmpty() || episode == null)
            {
                return PartialView("Activities", new ScheduleActivityArgument { EpisodeId = Guid.Empty, PatientId = Guid.Empty, Discpline = "" });
            }
            return PartialView("Activities", new ScheduleActivityArgument { EpisodeId = episode.Id, PatientId = patientId, Discpline = discipline });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PASVisitNote(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    var pocEvent = patientRepository.GetVisitNoteByType(episodeId, patientId, DisciplineTasks.PASCarePlan);
                    if (pocEvent != null)
                    {
                        viewData.Questions = pocEvent.ToDictionary();
                    }
                }
                else
                {
                    viewData.Questions = patientvisitNote.ToDictionary();
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;

                var previousNoteEvents = new List<ScheduleEvent>();
                var previousNotes = patientRepository.GetPreviousNotes(patientId, Current.AgencyId);
                if (previousNotes != null && previousNotes.Count > 0)
                {
                    foreach (var note in previousNotes)
                    {
                        if (note.Id != eventId && note.IsPASNote())
                        {
                            var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, note.EpisodeId, note.PatientId, note.Id);
                            if (scheduleEvent != null)
                            {
                                var status = scheduleEvent.Status != null && Enum.IsDefined(typeof(ScheduleStatus), int.Parse(scheduleEvent.Status)) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status) : ScheduleStatus.NoStatus;
                                if (status == ScheduleStatus.NoteCompleted || status == ScheduleStatus.NoteSubmittedWithSignature)
                                {
                                    previousNoteEvents.Add(scheduleEvent);
                                }
                            }
                        }
                    }
                }
                previousNoteEvents = previousNoteEvents.OrderByDescending(p => p.EventDate.ToZeroFilled()).Take(5).ToList();
                if (previousNoteEvents != null && previousNoteEvents.Count > 0)
                {
                    previousNoteEvents.ForEach(s =>
                    {
                        viewData.PreviousNotes.Add(s.EventId, string.Format("{0} {1}", s.DisciplineTaskName, s.EventDate.ToZeroFilled()));
                    });
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            return PartialView("PAS/VisitNote", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PASVisitNoteContent(Guid patientId, Guid noteId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("PAS/VisitNoteContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PASVisitNotePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.SignatureText = patientvisitNote.SignatureText;
                viewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                viewData.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            return PartialView("PAS/VisitNotePrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PASVisitNoteBlank()
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("PAS/VisitNotePrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PASCarePlan(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Allergies = patientService.GetAllergies(patientId);
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                    if (scheduledEvent != null)
                    {
                        var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                        if (assessment != null)
                        {
                            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                            viewData.Questions = assessment.ToNotesQuestionDictionary();
                        }
                    }
                }
                else
                {
                    viewData.Questions = patientvisitNote.ToDictionary();
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            return PartialView("PAS/CarePlan", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PASCarePlanPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
                if (allergyProfile != null)
                {
                    viewData.Allergies = allergyProfile.ToString();
                }
                viewData.SignatureText = patientvisitNote.SignatureText;
                viewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                viewData.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {
                    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                    }
                }
            }
            return View("PAS/CarePlanPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PASCarePlanBlank()
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("PAS/CarePlanPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Deviation()
        {
            return PartialView("Deviation");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeRangeList(Guid patientId)
        {
            var viewData = new List<EpisodeDateViewData>();
            if (!patientId.IsEmpty())
            {
                var episodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patientId).OrderBy(e => e.StartDate).ToList();
                if (episodes != null && episodes.Count > 0)
                {
                    viewData = episodes.Select(e => new EpisodeDateViewData { Id = e.Id, Range = string.Format("{0}-{1}", e.StartDate.ToString("MM/dd/yyyy"), e.EndDate.ToString("MM/dd/yyyy")) }).ToList();
                }
            }
            return Json(viewData);
        }

        public ActionResult ScheduleLogs(Guid eventId, Guid patientId, int task)
        {
            return PartialView("Logs", patientService.GetTaskLogs(patientId, eventId, task));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Inactive(Guid patientId)
        {
            return PartialView("Episode/Inactive", patientRepository.GetPatientDeactivatedAndDischargedEpisodes(Current.AgencyId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InactiveGrid(Guid patientId)
        {
            return PartialView("Episode/InactiveGrid", patientRepository.GetPatientDeactivatedAndDischargedEpisodes(Current.AgencyId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ActivateEpisode(Guid episodeId, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Episode could not be activated. Try again." };
            var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null)
            {
                var validationRules = new List<Validation>();
                validationRules.Add(new Validation(() => !patientService.IsValidEpisode(patientEpisode.Id, patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate), "Episode date is not in the valid date range."));
                var entityValidator = new EntityValidator(validationRules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {

                    patientEpisode.IsActive = true;
                    patientEpisode.IsDischarged = false;
                    patientEpisode.Modified = DateTime.Now;
                    if (patientRepository.UpdateEpisode(patientEpisode))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientEpisode.PatientId, patientEpisode.Id.ToString(), LogType.Episode, LogAction.EpisodeActivated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Episode has been successfully activated.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeLogs(Guid episodeId, Guid patientId)
        {
            return PartialView("Episode/Logs", patientService.GetGeneralLogs(LogDomain.Patient, LogType.Episode, patientId, episodeId.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNDiabeticDailyVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToDictionary();
                var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsNotNullOrEmpty() && scheduledEvent.VisitDate.IsValidDate() ? scheduledEvent.VisitDate : scheduledEvent.EventDate;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduledEvent.EventDate.ToDateTime());
                    if (assessment != null)
                    {
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;

            return PartialView("Nursing/SNDiabeticDailyVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SNDiabeticDailyVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new DiabeticDailyPdf(assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=SNDiabeticDailyVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNDiabeticDailyVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Nursing/SNDiabeticDailyVisitPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult SNDiabeticDailyVisitPdfBlank()
        {
            var doc = new LVNSVisitPdf(assessmentService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=SNDiabeticDailyVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }
        #endregion
    }
}
