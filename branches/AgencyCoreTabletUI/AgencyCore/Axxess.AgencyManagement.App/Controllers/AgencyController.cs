﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Linq;

    using Enums;
    using Services;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Enums;

    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.App.iTextExtension;

    using Axxess.LookUp.Repositories;
    using Common;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AgencyController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IPatientService patientService;
        private readonly IPhysicianService physicianService;
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly ILookupRepository lookupRepository;

        public AgencyController(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider coreDataProvider, ILookUpDataProvider lookUpDataProvider, IAgencyService agencyService, IUserService userService, IPatientService patientService, IPhysicianService physicianService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyService, "agencyService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "dataProvider");

            this.userService = userService;
            this.agencyService = agencyService;
            this.patientService = patientService;
            this.physicianService = physicianService;
            this.loginRepository = coreDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.lookupRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Agency Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Info()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Schedule()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult ScheduleList()
        {
            return View(new GridModel(agencyService.GetSchedule()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RecertsPastDueGrid()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("Oasis/RecertsPastDue", agency != null ? agency.Payor : "0");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RecertsUpcomingGrid()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("Oasis/RecertsUpcoming", agency != null ? agency.Payor : "0");
        }

        [GridAction]
        public ActionResult RecertsPastDue(Guid BranchId, int InsuranceId, DateTime StartDate)
        {
            return View(new GridModel(agencyService.GetRecertsPastDue(BranchId, InsuranceId, StartDate, DateTime.Now)));
        }

        public JsonResult RecertsPastDueWidget()
        {
            return Json(agencyService.GetRecertsPastDueWidget());
        }

        [GridAction]
        public ActionResult RecertsUpcoming(Guid BranchId, int InsuranceId)
        {
            return View(new GridModel(agencyService.GetRecertsUpcoming(BranchId,  InsuranceId, DateTime.Now,  DateTime.Now.AddDays(24))));
        }

        public JsonResult RecertsUpcomingWidget()
        {
            return Json(agencyService.GetRecertsUpcomingWidget());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CaseManagement()
        {
            ViewData["GroupName"] = "EventDate";
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] =  location != null ? location.Id : Guid.Empty;
            return PartialView("QA/CaseManagement", agencyService.GetCaseManagerSchedule(location != null ? location.Id : Guid.Empty, 1));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CaseManagementContent(string groupName,Guid BranchId, int Status)
        {
            ViewData["GroupName"] = groupName;
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
           // ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("QA/CaseManagementContent", agencyService.GetCaseManagerSchedule(BranchId, Status));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public LargeJsonResult CaseManagementGrid(Guid BranchId, int Status)
        {
            return new LargeJsonResult
            {
                MaxJsonLength=int.MaxValue,
                Data = new GridModel(agencyService.GetCaseManagerSchedule(BranchId, Status))
            };
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintQueue()
        {
            ViewData["GroupName"] = "EventDate";
            return PartialView("PrintQueue/List");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AfterHoursSupport()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PrintQueueContent(string groupName)
        {
            ViewData["GroupName"] = groupName;
            return PartialView("PrintQueue/Content");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PrintQueueGrid()
        {
            return View(new GridModel(agencyService.GetPrintQueue()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OrdersToBeSentView()
        {
            return PartialView("Order/ToBeSent");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersToBeSent(Guid BranchId, bool sendAutomatically, string StartDate, string EndDate)
        {
            return View(new GridModel(agencyService.GetOrdersToBeSent(BranchId, sendAutomatically, StartDate.ToDateTime(), EndDate.ToDateTime())));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OrdersHistory()
        {
            return PartialView("Order/History");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersHistoryList(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var status = new List<int> { (int)ScheduleStatus.OrderReturnedWPhysicianSignature, (int)ScheduleStatus.EvalReturnedWPhysicianSignature };

            return View(new GridModel(agencyService.GetProcessedOrders(BranchId, StartDate,EndDate,status)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MarkOrdersAsSent(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Orders could not be marked as sent to Physician" };

            if (formCollection.Count > 0)
            {
                if (agencyService.MarkOrdersAsSent(formCollection))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Orders have been marked as sent to Physician";
                }
            }
            else
            {
                viewData.errorMessage = "No Orders were selected";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MarkOrderAsReturned(Guid branchId, Guid id, Guid patientId, OrderType type, DateTime receivedDate, DateTime startDate, DateTime endDate)
        {
            agencyService.MarkOrderAsReturned(id, patientId, type, receivedDate);
            return View(new GridModel(agencyService.GetOrdersPendingSignature(branchId, startDate, endDate)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrderHistoryEdit(Guid id, Guid patientId, string type)
        {
            return PartialView("Order/HistoryEdit", agencyService.GetOrder(id, patientId, type));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditOrders(Guid Id, Guid PatientId, OrderType Type, DateTime ReceivedDate, DateTime SendDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order isn't updated. Try again." };
            if (agencyService.UpdateOrderDates(Id, PatientId, Type, ReceivedDate, SendDate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Order successfully updated";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OrdersPendingSignatureView()
        {
            return PartialView("Order/PendingSignature");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersPendingSignature(Guid BranchId,DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(agencyService.GetOrdersPendingSignature(BranchId, StartDate,EndDate)));
        }

        [GridAction]
        public ActionResult Users()
        {
            return View(new GridModel(userRepository.GetAgencyUsers(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Blankforms()
        {
            return PartialView();
        }

        #endregion

        #region Contact Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Contacts()
        {
            return PartialView("Contact/List");
        }

        [GridAction]
        public ActionResult ContactList()
        {
            return View(new GridModel(agencyRepository.GetContacts(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewContact()
        {
            return PartialView("Contact/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddContact([Bind] AgencyContact contact)
        {
            Check.Argument.IsNotNull(contact, "contact");

            var viewData = new JsonViewData();

            if (contact.IsValid)
            {
                if (!agencyService.CreateContact(contact))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the contact.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Contact was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = contact.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditContact(Guid Id)
        {
            return PartialView("Contact/Edit", agencyRepository.FindContact(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateContact([Bind] AgencyContact contact)
        {
            Check.Argument.IsNotNull(contact, "contact");

            var viewData = new JsonViewData();

            if (contact.IsValid)
            {
                if (agencyRepository.FindContact(Current.AgencyId, contact.Id) != null)
                {
                    contact.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditContact(contact))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the contact.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, contact.Id.ToString(), LogType.AgencyContact, LogAction.AgencyContactUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Contact was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected contact don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = contact.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteContact(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Contact could not be deleted. Please try again." };
            if (agencyRepository.DeleteContact(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyContact, LogAction.AgencyContactDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Contact has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Contact could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContactLogs(Guid contactId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyContact, Current.AgencyId, contactId.ToString()));
        }

        #endregion

        #region Hospital Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewHospital()
        {
            return PartialView("Hospital/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddHospital([Bind] AgencyHospital hospital)
        {
            Check.Argument.IsNotNull(hospital, "hospital");

            var viewData = new JsonViewData();

            if (hospital.IsValid)
            {
                hospital.AgencyId = Current.AgencyId;
                hospital.Id = Guid.NewGuid();
                if (!agencyRepository.AddHospital(hospital))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the hospital.";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, hospital.Id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Hospital was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = hospital.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Hospitals()
        {
            return PartialView("Hospital/List");
        }

        [GridAction]
        public ActionResult HospitalList()
        {
            return View(new GridModel(agencyRepository.GetHospitals(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditHospital(Guid Id)
        {
            return PartialView("Hospital/Edit", agencyRepository.FindHospital(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateHospital([Bind] AgencyHospital hospital)
        {
            Check.Argument.IsNotNull(hospital, "hospital");

            var viewData = new JsonViewData();

            if (hospital.IsValid)
            {
                if (agencyRepository.FindHospital(Current.AgencyId, hospital.Id) != null)
                {
                    hospital.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditHospital(hospital))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the hospital.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, hospital.Id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Hospital was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected Hospial don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = hospital.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteHospital(Guid Id)
        {
            Check.Argument.IsNotEmpty(Id, "Id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this hospital. Please try again." };
            if (agencyRepository.FindHospital(Current.AgencyId, Id) != null)
            {
                if (agencyRepository.DeleteHospital(Current.AgencyId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Id.ToString(), LogType.AgencyHospital, LogAction.AgencyHospitalDeleted, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Hospital was successfully deleted.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the hospital.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected Hospital don't exist.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalLogs(Guid hospitalId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyHospital, Current.AgencyId, hospitalId.ToString()));
        }

        #endregion

        #region Physician Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetPhysicians()
        {
            return Json(physicianRepository.GetAgencyPhysicians(Current.AgencyId).ForSelection());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetPhysician(Guid physicianId)
        {
            Check.Argument.IsNotEmpty(physicianId, "physicianId");
            return Json(physicianRepository.Get(physicianId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Physicians()
        {
            return PartialView("Physician/List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianList()
        {
            return View(new GridModel(physicianRepository.GetAgencyPhysicians(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewPhysician()
        {
            return PartialView("Physician/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddPhysician([Bind] AgencyPhysician agencyPhysician)
        {
            Check.Argument.IsNotNull(agencyPhysician, "agencyPhysician");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be added" };

            if (agencyPhysician.PhysicianAccess && agencyPhysician.EmailAddress.IsNullOrEmpty())
            {
                viewData.errorMessage = "E-mail Address required for Physician Access";
                return Json(viewData);
            }

            if (agencyPhysician.IsValid)
            {
                agencyPhysician.AgencyId = Current.AgencyId;
                if (!physicianService.CreatePhysician(agencyPhysician))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in Saving the data.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Data successfully Saved";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agencyPhysician.ValidationMessage;
            }

            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditPhysician(Guid Id)
        {
            return PartialView("Physician/Edit", physicianRepository.Get(Id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePhysician([Bind] AgencyPhysician physician)
        {
            Check.Argument.IsNotNull(physician, "physician");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be updated." };

            if (physician.PhysicianAccess && physician.EmailAddress.IsNullOrEmpty())
            {
                viewData.errorMessage = "E-mail Address required for Physician Access";
                return Json(viewData);
            }

            if (physician.IsValid)
            {
                if (physicianService.UpdatePhysician(physician))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Physician has been successfully edited";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Physician data could not be saved.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = physician.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeletePhysician(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "Id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be deleted. Please try again." };
            if (physicianRepository.Delete(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Physician has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Physician could not be deleted.";
            }
            return Json(viewData);
        }

        #endregion

        #region Location Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewLocation()
        {
            return PartialView("Location/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid)
            {
                if (!agencyService.CreateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Location.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Locations()
        {
            return PartialView("Location/List");
        }

        [GridAction]
        public ActionResult LocationList()
        {
            return View(new GridModel(agencyRepository.GetBranches(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditLocation(Guid Id)
        {
            return PartialView("Location/Edit", agencyRepository.FindLocation(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid)
            {
                if (agencyRepository.FindLocation(Current.AgencyId, location.Id) != null)
                {
                    location.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditLocation(location))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the location.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Location was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected location don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationLogs(Guid locationId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyLocation, Current.AgencyId, locationId.ToString()));
        }

        #endregion

        #region Insurance Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewInsurance()
        {
            return PartialView("Insurance/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddInsurance([Bind] AgencyInsurance insurance, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(insurance, "insurance");
            List<Validation> rules = new List<Validation>();
            var viewData = new JsonViewData();
            var keys = formCollection.AllKeys;
            if (keys.Contains("RateDiscipline"))
            {
                var disciplineList = formCollection["RateDiscipline"].ToArray();
                var visitRatesList = new List<ChargeRate>();
                if (disciplineList != null && disciplineList.Length > 0)
                {
                    disciplineList.ForEach(l =>
                    {
                        if (keys.Contains(l + "_Charge"))
                        {
                            visitRatesList.Add(new ChargeRate { RateDiscipline = l, Charge = formCollection[l + "_Charge"], ChargeType = insurance.ChargeType.ToString(), Code = keys.Contains(l + "_Code") ? formCollection[l + "_Code"] : string.Empty });
                            rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_Charge"]) ? !formCollection[l + "_Charge"].IsDouble() : false, "Wrong entry"));
                        }
                    });
                }
              
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    insurance.Charge = visitRatesList.ToXml();
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "One of the charge rate is not in correct format";
                    return Json(viewData);
                }
            }
            if (keys.Contains("Ub04Locator81cca"))
            {
                var locatorList = formCollection["Ub04Locator81cca"].ToArray();
                var locators = new List<Locator>();
                if (locatorList != null && locatorList.Length > 0)
                {
                    locatorList.ForEach(l =>
                    {
                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3"))
                        {
                            locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                        }
                    });
                }
                insurance.Ub04Locator81cca = locators.ToXml();
            }
            
            if (insurance.IsValid)
            {
                insurance.AgencyId = Current.AgencyId;
                if (!agencyRepository.AddInsurance(insurance))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the insurance.";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, insurance.Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceAdded, string.Empty);
                    InsuranceEngine.Instance.Refresh(Current.AgencyId);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Insurance was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = insurance.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetInsurances()
        {
            return Json(agencyService.GetInsurances());
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Insurances()
        {
            return PartialView("Insurance/List");
        }

        [GridAction]
        public ActionResult InsuranceList()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var insurances = new List<InsuranceLean>();
            if (agency != null)
            {
               int payor;
                if(int.TryParse(agency.Payor, out payor)){
                    var insurance = lookupRepository.GetInsurance(payor);
                    if (insurance != null)
                    {
                        insurances.Add(new InsuranceLean {Name=insurance.Name, PayorType=3,PayorId=agency.SubmitterId,InvoiceType= 1 , IsTradtionalMedicare=true});
                    }
                }
            }
            var data = agencyRepository.GetLeanInsurances(Current.AgencyId);
            if (data != null && data.Count > 0)
            {
                insurances.AddRange(data);
            }
            return View(new GridModel(insurances));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditInsurance(int Id)
        {
            return PartialView("Insurance/Edit", agencyRepository.FindInsurance(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateInsurance([Bind] AgencyInsurance insurance, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(insurance, "insurance");
            List<Validation> rules = new List<Validation>();
            var viewData = new JsonViewData();
            var keys = formCollection.AllKeys;
            if (keys.Contains("RateDiscipline"))
            {
                var disciplineList = formCollection["RateDiscipline"].ToArray();
                var visitRatesList = new List<ChargeRate>();
                disciplineList.ForEach(l =>
                {
                    if (keys.Contains(l + "_Charge") )
                    {
                        visitRatesList.Add(new ChargeRate { RateDiscipline = l, Charge = formCollection[l + "_Charge"], ChargeType = insurance.ChargeType.ToString(), Code = keys.Contains(l + "_Code") ? formCollection[l + "_Code"] : string.Empty });
                        rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_Charge"]) ? !formCollection[l + "_Charge"].IsDouble() : false, "Wrong entry"));
                    }
                });
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    insurance.Charge = visitRatesList.ToXml();
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "One of the charge rate is not in correct format";
                    return Json(viewData);
                }
            }
            if (keys.Contains("Ub04Locator81cca"))
            {
                var locatorList = formCollection["Ub04Locator81cca"].ToArray();
                var locators = new List<Locator>();
                if (locatorList != null && locatorList.Length > 0)
                {
                    locatorList.ForEach(l =>
                    {
                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3"))
                        {
                            locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                        }
                    });
                }
                insurance.Ub04Locator81cca = locators.ToXml();
            }
            if (insurance.IsValid)
            {
                if (agencyRepository.FindInsurance(Current.AgencyId, insurance.Id) != null)
                {
                    insurance.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditInsurance(insurance))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the insurance.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, insurance.Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceUpdated, string.Empty);
                        InsuranceEngine.Instance.Refresh(Current.AgencyId);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Insurance was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected insurance don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = insurance.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteInsurance(int Id)
        {
            Check.Argument.IsNotNegativeOrZero(Id, "Id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance. Please try again." };
            if (agencyRepository.FindInsurance(Current.AgencyId, Id) != null)
            {
                if (agencyRepository.DeleteInsurance(Current.AgencyId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Id.ToString(), LogType.AgencyInsurance, LogAction.AgencyInsuranceDeleted, string.Empty);
                    InsuranceEngine.Instance.Refresh(Current.AgencyId);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The insurance was successfully deleted.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the insurance.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected insurance don't exist.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceLogs(int insuranceId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyInsurance, Current.AgencyId, insuranceId.ToString()));
        }

        #endregion

        #region Infection Reports Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewInfection()
        {
            return PartialView("Infection/New", Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewInfection(Guid patientId)
        {
            return PartialView("Infection/New", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddInfection([Bind] Infection infection)
        {
            Check.Argument.IsNotNull(infection, "infection");

            var viewData = new JsonViewData();

            if (infection.IsValid)
            {
                infection.Id = Guid.NewGuid();
                infection.UserId = Current.UserId;
                infection.AgencyId = Current.AgencyId;
                if (infection.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    if (infection.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, infection.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this infection report.";
                        return Json(viewData);
                    }
                    else
                    {
                        infection.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                    }
                }
                else
                {
                    infection.SignatureText = string.Empty;
                }
                var newScheduleEvent = new ScheduleEvent
                {
                    EventId = infection.Id,
                    UserId = infection.UserId,
                    PatientId = infection.PatientId,
                    EpisodeId = infection.EpisodeId,
                    Status = infection.Status.ToString(),
                    Discipline = Disciplines.ReportsAndNotes.ToString(),
                    EventDate = infection.InfectionDate.ToShortDateString(),
                    VisitDate = infection.InfectionDate.ToShortDateString(),
                    DisciplineTask = (int)DisciplineTasks.InfectionReport
                };
               
                if (agencyRepository.AddInfection(infection))
                {
                    if (patientRepository.UpdateEpisode(Current.AgencyId, infection.EpisodeId, infection.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
                    {
                        Auditor.Log(infection.EpisodeId,infection.PatientId, infection.Id, Actions.Add, DisciplineTasks.InfectionReport);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Infection was saved successfully";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in Saving the Infection.";
                    }
                }
                else
                {

                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the infection.";
                }

            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = infection.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult InfectionGrid()
        {
            return PartialView("Infection/List");
        }

        [GridAction]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult InfectionList()
        {
            return View(new GridModel(agencyService.GetInfections(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditInfection(Guid Id)
        {
            var infection = agencyRepository.GetInfectionReport(Current.AgencyId, Id);
            if (infection != null)
            {
                var patient = patientRepository.GetPatientOnly(infection.PatientId, infection.AgencyId);
                if (patient != null)
                {
                    infection.PatientName = patient.DisplayName;
                }
                if (!infection.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, infection.EpisodeId, infection.PatientId);
                    if (episode != null)
                    {
                        infection.EpisodeEndDate = episode.EndDateFormatted;
                        infection.EpisodeStartDate = episode.StartDateFormatted;
                    }
                }
            }
            else { infection = new Infection(); }
            return PartialView("Infection/Edit", infection);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateInfection([Bind] Infection infection) {
            Check.Argument.IsNotNull(infection, "infection");
            var viewData = new JsonViewData();
            if (infection.IsValid) {
                infection.AgencyId = Current.AgencyId;
                infection.UserId = infection.UserId.IsEmpty() ? Current.UserId : infection.UserId;
                if (infection.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature) {
                    if (infection.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, infection.SignatureText)) {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this infection report.";
                        return Json(viewData);
                    } else {
                        infection.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement)) infection.Status = (int)ScheduleStatus.ReportAndNotesCompleted;
                    }
                } else infection.SignatureText = string.Empty;
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, infection.EpisodeId, infection.PatientId);
                if (episode != null && episode.Schedule.IsNotNullOrEmpty()) {
                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (scheduleEvents != null && scheduleEvents.Exists(e => e.EventId == infection.Id && e.PatientId == infection.PatientId)) {
                        var evnt = scheduleEvents.Single(e => e.EventId == infection.Id && e.PatientId == infection.PatientId);
                        if (evnt != null) {
                            evnt.VisitDate = infection.InfectionDate.ToString("MM/dd/yyyy");
                            evnt.Status = infection.Status.ToString();
                            evnt.ReturnReason = string.Empty;
                            episode.Schedule = scheduleEvents.ToXml();
                            if (patientRepository.UpdateEpisode(episode)) {
                                var userEvent = userRepository.GetEvent(Current.AgencyId, evnt.UserId, evnt.PatientId, evnt.EventId);
                                if (userEvent != null) {
                                    userEvent.EventDate = evnt.EventDate;
                                    userEvent.VisitDate = evnt.VisitDate;
                                    userEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                                    userEvent.Status = evnt.Status;
                                    userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                } else {
                                    var newUserEvent = new UserEvent {
                                        EventId = infection.Id,
                                        UserId = infection.UserId,
                                        PatientId = infection.PatientId,
                                        EpisodeId = infection.EpisodeId,
                                        Status = infection.Status.ToString(),
                                        Discipline = Disciplines.ReportsAndNotes.ToString(),
                                        EventDate = evnt.EventDate,
                                        VisitDate = evnt.VisitDate,
                                        DisciplineTask = (int)DisciplineTasks.InfectionReport
                                    };
                                    patientRepository.AddNewUserEvent(Current.AgencyId, infection.PatientId, newUserEvent);
                                }
                                if (evnt.Status.IsInteger()) Auditor.Log(evnt.EpisodeId,evnt.PatientId, evnt.EventId, Actions.Add, (ScheduleStatus)evnt.Status.ToInteger(), DisciplineTasks.InfectionReport, string.Empty);
                            } else {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Error in updating the data. Try again.";
                                return Json(viewData);
                            }
                        } else patientService.AddInfectionUserAndScheduleEvent(infection, out infection);
                    } else patientService.AddInfectionUserAndScheduleEvent(infection, out infection);
                } else patientService.AddInfectionUserAndScheduleEvent(infection, out infection);
                if (!agencyRepository.UpdateInfection(infection)) {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Infection could not be updated.";
                } else {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Infection was updated successfully";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = infection.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult InfectionReportPrint(Guid episodeId, Guid patientId, Guid eventId) {
            return View("Infection/Print", agencyService.GetInfectionReportPrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult InfectionReportPdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new InfectionReportPdf(agencyService.GetInfectionReportPrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=InfectionReport_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ProcessInfection(string button, Guid patientId, Guid eventId, string reason)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Infection Report could not be saved." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                if (button == "Approve")
                {
                    if (agencyService.ProcessInfections(button, patientId, eventId, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Infection Report has been successfully approved.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Infection Report could not be approved.";
                    }
                }
                else if (button == "Return")
                {
                    if (agencyService.ProcessInfections(button, patientId, eventId, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Infection Report has been successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Infection Report could not be returned.";
                    }
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Incident Reports Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewIncident()
        {
            return PartialView("Incident/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewIncident(Guid patientId)
        {
            return PartialView("Incident/New", patientId);
        }

        public ActionResult AddIncident([Bind] Incident incident)
        {
            Check.Argument.IsNotNull(incident, "incident");

            var viewData = new JsonViewData();

            if (incident.IsValid)
            {
                incident.Id = Guid.NewGuid();
                incident.UserId = Current.UserId;
                incident.AgencyId = Current.AgencyId;
                if (incident.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    if (incident.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, incident.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this incident/accident report.";
                        return Json(viewData);
                    }
                    else
                    {
                        incident.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                    }
                }
                else
                {
                    incident.SignatureText = string.Empty;
                }
                var newScheduleEvent = new ScheduleEvent
                {
                    EventId = incident.Id,
                    UserId = incident.UserId,
                    PatientId = incident.PatientId,
                    EpisodeId=incident.EpisodeId,
                    Status = incident.Status.ToString(),
                    Discipline = Disciplines.ReportsAndNotes.ToString(),
                    EventDate = incident.IncidentDate.ToShortDateString(),
                    VisitDate = incident.IncidentDate.ToShortDateString(),
                    DisciplineTask = (int)DisciplineTasks.IncidentAccidentReport
                };

                if (patientRepository.UpdateEpisode(Current.AgencyId, incident.EpisodeId, incident.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
                {
                    Auditor.Log(incident.EpisodeId,incident.PatientId, incident.Id, Actions.Add, DisciplineTasks.IncidentAccidentReport);
                    if (!agencyRepository.AddIncident(incident))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in saving the incident / accident.";
                    }
                    else
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Incident / Accident was saved successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in Saving the Incident / Accident.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = incident.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IncidentGrid()
        {
            return PartialView("Incident/List");
        }

        [GridAction]
        public ActionResult IncidentList()
        {
            return View(new GridModel(agencyService.GetIncidents(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditIncident(Guid Id)
        {
            var incident = agencyRepository.GetIncidentReport(Current.AgencyId, Id);
            if (incident != null)
            {
                var patient = patientRepository.GetPatientOnly(incident.PatientId, incident.AgencyId);
                if (patient != null)
                {
                    incident.PatientName = patient.DisplayName;
                }
                if (!incident.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, incident.EpisodeId, incident.PatientId);
                    if (episode != null)
                    {
                        incident.EpisodeEndDate = episode.EndDateFormatted;
                        incident.EpisodeStartDate = episode.StartDateFormatted;

                    }
                }
            }
            else { incident = new Incident(); }
            return PartialView("Incident/Edit", incident);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateIncident([Bind] Incident incident)
        {
            Check.Argument.IsNotNull(incident, "incident");

            var viewData = new JsonViewData();

            if (incident.IsValid)
            {
                incident.AgencyId = Current.AgencyId;
                incident.UserId = incident.UserId.IsEmpty() ? Current.UserId : incident.UserId;
                if (incident.Status == (int)ScheduleStatus.ReportAndNotesSubmittedWithSignature)
                {
                    if (incident.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, incident.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this incident report.";
                        return Json(viewData);
                    }
                    else
                    {
                        incident.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            incident.Status = (int)ScheduleStatus.ReportAndNotesCompleted;
                        }
                    }
                }
                else
                {
                    incident.SignatureText = string.Empty;
                }


                var episode = patientRepository.GetEpisodeById(Current.AgencyId, incident.EpisodeId, incident.PatientId);
                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvets = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (scheduleEvets != null && scheduleEvets.Exists(e => e.EventId == incident.Id && e.PatientId == incident.PatientId))
                    {
                        var evnt = scheduleEvets.Single(e => e.EventId == incident.Id && e.PatientId == incident.PatientId);
                        if (evnt != null)
                        {
                          
                                evnt.VisitDate = incident.IncidentDate.ToString("MM/dd/yyyy");
                                evnt.Status = incident.Status.ToString();
                                evnt.ReturnReason = string.Empty;
                                episode.Schedule = scheduleEvets.ToXml();
                                if (patientRepository.UpdateEpisode(episode))
                                {
                                    var userEvent = userRepository.GetEvent(Current.AgencyId, evnt.UserId, evnt.PatientId, evnt.EventId);
                                    if (userEvent != null)
                                    {
                                        userEvent.EventDate = evnt.EventDate;
                                        userEvent.VisitDate = evnt.VisitDate;
                                        userEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                                        userEvent.Status = evnt.Status;
                                        userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                    }
                                    else
                                    {
                                        var newUserEvent = new UserEvent
                                        {
                                            EventId = incident.Id,
                                            UserId = incident.UserId,
                                            PatientId = incident.PatientId,
                                            EpisodeId = incident.EpisodeId,
                                            Status = incident.Status.ToString(),
                                            Discipline = Disciplines.ReportsAndNotes.ToString(),
                                            EventDate = evnt.EventDate,
                                            VisitDate = evnt.VisitDate,
                                            DisciplineTask = (int)DisciplineTasks.IncidentAccidentReport
                                        };
                                        patientRepository.AddNewUserEvent(Current.AgencyId, incident.PatientId, newUserEvent);
                                    }
                                    if (evnt.Status.IsInteger())
                                    {
                                        Auditor.Log(evnt.EpisodeId,evnt.PatientId, evnt.EventId, Actions.Add, (ScheduleStatus)evnt.Status.ToInteger(), DisciplineTasks.IncidentAccidentReport, string.Empty);
                                    }
                                }
                                else
                                {
                                    viewData.isSuccessful = false;
                                    viewData.errorMessage = "Error in updating the data. Try again.";
                                    return Json(viewData);
                                }
                        }
                        else
                        {
                            patientService.AddIncidentUserAndScheduleEvent(incident, out incident);
                        }
                    }
                    else
                    {
                        patientService.AddIncidentUserAndScheduleEvent(incident, out incident);
                    }
                }
                else
                {
                    patientService.AddIncidentUserAndScheduleEvent(incident, out incident);
                }

                if (!agencyRepository.UpdateIncident(incident))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Incident could not be updated.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Incident was updated successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = incident.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IncidentReportPrint(Guid episodeId, Guid patientId, Guid eventId) {
            return View("Incident/Print", agencyService.GetIncidentReportPrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult IncidentReportPdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new IncidentReportPdf(agencyService.GetIncidentReportPrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=IncidentReport_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ProcessIncident(string button, Guid patientId, Guid eventId, string reason)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Incident/Accident Report could not be saved." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                if (button == "Approve")
                {
                    if (agencyService.ProcessIncidents(button, patientId, eventId, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Incident/Accident Report has been successfully approved.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Incident/Accident Report could not be approved.";
                    }
                }
                else if (button == "Return")
                {
                    if (agencyService.ProcessIncidents(button, patientId, eventId, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Incident/Accident Report has been successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your Incident/Accident Report could not be returned.";
                    }
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Visit Rate Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitRates()
        {
            return PartialView("VisitRate", agencyRepository.GetMainLocation(Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitRateContent(Guid branchId)
        {
            return PartialView("VisitRateContent", agencyRepository.FindLocation(Current.AgencyId, branchId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EditCost(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData();
            var rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            if (keys.Contains("AgencyLocationId") && formCollection["AgencyLocationId"].IsNotNullOrEmpty())
            {
                var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, formCollection["AgencyLocationId"].ToGuid());
                if (agencyLocation != null)
                {
                    if (keys.Contains("RateDiscipline"))
                    {
                        var disciplineList = formCollection["RateDiscipline"].ToArray();
                        var visitRatesList = new List<CostRate>();
                        disciplineList.ForEach(l =>
                        {
                            if ( keys.Contains(l + "_PerUnit"))
                            {
                                visitRatesList.Add(new CostRate { RateDiscipline = l,  PerUnit = formCollection[l + "_PerUnit"] });
                                rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_PerUnit"]) ? !formCollection[l + "_PerUnit"].IsDouble() : false, "Wrong entry"));
                            }
                        });
                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            agencyLocation.Cost = visitRatesList.ToXml(); 
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "One of the cost rate is not in correct format";
                            return Json(viewData);
                        }

                        if (agencyRepository.EditBranchCost(agencyLocation))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Branch cost rates was successfully edited.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Branch cost rates could not be updated.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Could not update the branch cost rates.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected branch don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected branch don't exist.";
            }
            return Json(viewData);
        }

        #endregion

        #region Template Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Templates()
        {
            return PartialView("Template/List");
        }

        [GridAction]
        public ActionResult TemplateList()
        {
            return View(new GridModel(agencyRepository.GetTemplates(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewTemplate()
        {
            return PartialView("Template/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddTemplate([Bind] AgencyTemplate template)
        {
            Check.Argument.IsNotNull(template, "template");

            var viewData = new JsonViewData();

            if (template.IsValid)
            {
                template.AgencyId = Current.AgencyId;
                template.Id = Guid.NewGuid();
                if (!agencyRepository.AddTemplate(template))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the template.";
                }
                else
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, template.Id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Template was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = template.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditTemplate(Guid id)
        {
            return PartialView("Template/Edit", agencyRepository.GetTemplate(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateTemplate([Bind] AgencyTemplate template)
        {
            Check.Argument.IsNotNull(template, "template");

            var viewData = new JsonViewData();

            if (template.IsValid)
            {
                var existingTemplate = agencyRepository.GetTemplate(Current.AgencyId, template.Id);
                if (existingTemplate != null)
                {
                    existingTemplate.Text = template.Text;
                    existingTemplate.Title = template.Title;
                    if (!agencyRepository.UpdateTemplate(existingTemplate))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the template.";
                    }
                    else
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, template.Id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Template was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected template don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = template.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteTemplate(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Template could not be deleted. Please try again." };
            if (agencyRepository.DeleteTemplate(Current.AgencyId, id))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyTemplate, LogAction.AgencyTemplateDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Template has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Template could not be deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetTemplate(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var template = agencyRepository.GetTemplate(Current.AgencyId, id);
            if (template != null)
            {
                return Json(template);
            }
            return Json(new AgencyTemplate());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TemplateLogs(Guid templateId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.AgencyTemplate, Current.AgencyId, templateId.ToString()));
        }

        #endregion
    }
}
