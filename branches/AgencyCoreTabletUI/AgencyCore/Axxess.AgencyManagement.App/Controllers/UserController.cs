﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Enums;
    using Domain;
    using ViewData;
    using Security;
    using Services;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.iTextExtension;

    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.Log.Enums;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class UserController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly IMembershipService membershipService;
        private readonly IPatientService patientService;

        public UserController(IAgencyManagementDataProvider dataProvider, IUserService userService, IMembershipService membershipService, IPatientService patientService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            Check.Argument.IsNotNull(membershipService, "membershipService");

            this.userService = userService;
            this.membershipService = membershipService;
            this.userRepository = dataProvider.UserRepository;
            this.patientService = patientService;
        }

        #endregion

        #region UserController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
            if (userCount >= Current.MaxAgencyUserCount)
            {
                return PartialView(true);
            }
            return PartialView(false);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData();
             var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
             if (userCount >= Current.MaxAgencyUserCount)
             {
                 viewData.isSuccessful = false;
                 viewData.errorMessage = "User can't be added because you have reached your maximum number of user accounts. Please contact Axxess about upgrading your account.";
             }
             else
             {

                 if (user.IsValid)
                 {
                     user.AgencyId = Current.AgencyId;
                     user.AgencyName = Current.AgencyName;
                     if (!userService.CreateUser(user))
                     {
                         viewData.isSuccessful = false;
                         viewData.errorMessage = "Error in saving the new User.";
                     }
                     else
                     {
                         viewData.isSuccessful = true;
                         viewData.errorMessage = "User was saved successfully";
                     }
                 }
                 else
                 {
                     viewData.isSuccessful = false;
                     viewData.errorMessage = user.ValidationMessage;
                 }
             }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid Id)
        {
            return PartialView(userRepository.Get(Id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData();

            if (user.IsValid)
            {
                if (userRepository.Update(user))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserEdited, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "User was saved successfully";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the new User.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLicense([Bind] License license)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be saved." };
            if (userService.AddLicense(license, Request.Files))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "License saved successfully";
            }
            return PartialView("JsonResult", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLicense(Guid Id, Guid userId, DateTime ExpirationDate)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(userId, "userId");

            if (userService.UpdateLicense(Id, userId, ExpirationDate))
            {
                return View(new GridModel(userRepository.GetUserLicenses(Current.AgencyId, userId)));
            }
            return View(new GridModel(new List<License>()));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteLicense(Guid Id, Guid userId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(userId, "userId");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User license cannot be deleted. Try Again." };
            if (userService.DeleteLicense(Id, userId))
            {
                return View(new GridModel(userRepository.GetUserLicenses(Current.AgencyId, userId)));
            }
            return View(new GridModel(new List<License>()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdatePermissions(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Permissions could not be updated." };
            if (userService.UpdatePermissions(formCollection))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Permissions updated successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Deactivate(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deactivated. Try Again." };
            if (userRepository.SetUserStatus(Current.AgencyId, userId, (int)UserStatus.Inactive))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserDeactivated, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deactivated successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activate(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be activated. Try Again." };
            
             var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
             if (userCount >= Current.MaxAgencyUserCount)
             {
                 viewData.isSuccessful = false;
                 viewData.errorMessage = "User can't be activated because you have reached your maximum number of user accounts. Please contact Axxess about upgrading your account.";
             }
             else
             {
                 if (userRepository.SetUserStatus(Current.AgencyId, userId, (int)UserStatus.Active))
                 {
                     Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserActivated, string.Empty);
                     viewData.isSuccessful = true;
                     viewData.errorMessage = "User has been activated successfully.";
                 }
             }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deleted. Try Again." };
            if (userService.DeleteUser(userId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Schedule()
        {
            ViewData["UserScheduleGroupName"] = "VisitDate";
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleGrouped(string groupName)
        {
            ViewData["UserScheduleGroupName"] = groupName;
            return PartialView("ScheduleGrid");
        }
        
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleList()
        {
            var pageNumber = this.HttpContext.Request.Params["page"];
            return View(new GridModel(userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14))));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ScheduleWidget()
        {
            return Json(userService.GetScheduleWidget(Current.UserId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Profile()
        {
            return PartialView("Profile/Edit", userRepository.Get(Current.UserId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Profile([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your profile could not be saved." };

            if (user.IsValid)
            {
                if (user.PasswordChanger.NewPassword.IsNotNullOrEmpty() && !userService.IsPasswordCorrect(user.Id, user.PasswordChanger.CurrentPassword))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The password provided does not match the one on file.";
                }
                else
                {
                    if (user.SignatureChanger.NewSignature.IsNotNullOrEmpty() && !userService.IsSignatureCorrect(user.Id, user.SignatureChanger.CurrentSignature))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature provided does not match the one on file.";
                    }
                    else
                    {
                        if (userService.UpdateProfile(user))
                        {

                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Your profile has been updated successfully.";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult All()
        {
            return Json(userRepository.GetAgencyUsers(Current.AgencyId).ForSelection());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Grid()
        {
            return PartialView("List");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult BranchList( Guid branchId, int status)
        {
            return Json(userService.GetUserByBranchAndStatus(branchId, status).Select(u => new { Id = u.Id, Name = u.DisplayName }).ToList());
        }

        [GridAction]
        public ActionResult List(int status)
        {
            return View(new GridModel(userRepository.GetUsersByStatus(Current.AgencyId, status)));
        }

        [GridAction]
        public ActionResult LicenseList(Guid userId)
        {
            return View(new GridModel(userRepository.GetUserLicenses(Current.AgencyId, userId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ForgotSignature()
        {
            return PartialView("Signature/Forgot", Current.User.Name);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EmailSignature()
        {
            var viewData = new JsonViewData();

            if (membershipService.ResetSignature(Current.LoginId))
            {
                viewData.isSuccessful = true;
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Signature could not be reset. Please try again later.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UserCalendar()
        {
            var fromDate = DateUtilities.GetStartOfMonth(DateTime.Today.Month, DateTime.Now.Year);
            var toDate = DateUtilities.GetEndOfMonth(DateTime.Today.Month, DateTime.Now.Year);
            return PartialView("Calendar",new UserCalendarViewData{ UserEvents= userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate),FromDate=fromDate,ToDate=toDate});
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GoToMeeting()
        {
            return PartialView("Help/GoToMeeting", string.Empty);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Webinars()
        {
            return PartialView("Help/Webinars", userRepository.Get(Current.UserId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult UserCalendarPdf(DateTime month) {
            var fromDate = DateUtilities.GetStartOfMonth(month.Month, month.Year);
            var toDate = DateUtilities.GetEndOfMonth(month.Month, month.Year);
            MonthCalendarPdf doc = new MonthCalendarPdf(new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate });
            var PdfStream = doc.GetStream();
            PdfStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MonthlyCalendar_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(PdfStream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserCalendarNavigate(int month,int year)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month, year);
            var toDate = DateUtilities.GetEndOfMonth(month, year);
            return PartialView("Calendar",new UserCalendarViewData{ UserEvents= userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate),FromDate=fromDate,ToDate=toDate});
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserVisits(int month, int year)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month, year);
            var toDate = DateUtilities.GetEndOfMonth(month, year);
            return View(new GridModel(userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate))); 
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserLogs(Guid userId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.User, Current.AgencyId, userId.ToString()));
        }


        #endregion
    }
}
