﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Services;
    using ViewData;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.LookUp.Repositories;

    using Telerik.Web.Mvc;

    using Axxess.LookUp.Domain;

    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.App.iTextExtension;
    using System.Xml;
    using Axxess.Log.Enums;
    using Axxess.Log.Domain;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class BillingController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly IBillingService billingService;
        private readonly IAgencyService agencyService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IReferralRepository referrralRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;

        public BillingController(IAgencyManagementDataProvider dataProvider, IOasisCDataProvider oasisCDataProvider, ILookUpDataProvider lookUpDataProvider, IPatientService patientService, IBillingService billingService, IAgencyService agencyService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            this.referrralRepository = dataProvider.ReferralRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.patientService = patientService;
            this.billingService = billingService;
            this.agencyService = agencyService;
            this.assessmentRepository = oasisCDataProvider.OasisAssessmentRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.lookupRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EligibilityReport()
        {
            return PartialView("MedicareEligibilityReport");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EligibilityList(Guid patientId)
        {
            return PartialView("MedicareEligibilityGrid", patientService.GetMedicareEligibilityLists(patientId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Center()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
                {
                    int payorType;
                    if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agency.Payor, out payorType))
                    {
                        return PartialView("Center", billingService.AllUnProcessedBill(agencyMainBranch.Id, payorType));
                    }
                    else
                    {
                        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                        if (agencyMedicareInsurance != null)
                        {
                            return PartialView("Center", billingService.AllUnProcessedBill(agencyMainBranch.Id, agencyMedicareInsurance.Id));
                        }
                    }
                }
                else
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        return PartialView("Center", billingService.AllUnProcessedBill(agencyMainBranch.Id, agencyMedicareInsurance.Id));
                    }
                }
            }
            return PartialView("Center", new Bill());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult ClaimsPdf(Guid branchId, int insuranceId)
        {
            var doc = new BillingClaimsPdf(billingService.GetClaimsPrint(branchId, insuranceId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Claims_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalGrid(Guid branchId, int insuranceId)
        {
            return PartialView("FinalGrid", billingService.AllUnProcessedFinals(branchId, insuranceId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RapGrid(Guid branchId, int insuranceId)
        {
            return PartialView("RapGrid", billingService.AllUnProcessedRaps(branchId, insuranceId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Unprocessed()
        {
            return Json(billingService.GetAllUnProcessedBill());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Rap(Guid episodeId, Guid patientId)
        {
            return PartialView("Rap", billingService.GetRap(patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult RapPdf(Guid episodeId, Guid patientId)
        {
            var doc = new BillingRapPdf(billingService.GetRapPrint(episodeId, patientId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Rap_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Final(Guid episodeId, Guid patientId)
        {
            if (episodeId.IsEmpty() || patientId.IsEmpty())
            {
                return PartialView("Final", new Final());
            }
            return PartialView("Final", billingRepository.GetFinal(Current.AgencyId, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult FinalPdf(Guid episodeId, Guid patientId)
        {
            var doc = new BillingFinalPdf(billingService.GetFinalPrint(episodeId, patientId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Final_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Info(Guid episodeId, Guid patientId)
        {
            return PartialView("Info", billingService.GetFinalInfo(patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Visit(Guid episodeId, Guid patientId)
        {
            var claim = billingRepository.GetFinal(Current.AgencyId, patientId, episodeId);
            if (claim != null)
            {
                var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    claim.EpisodeStartDate = episode.StartDate;
                    claim.EpisodeEndDate = episode.EndDate;
                    claim.Visits = episode.Schedule;
                }
                var unitType = -1;
                claim.IsMedicareHMO = agencyRepository.IsMedicareHMO(Current.AgencyId, claim.PrimaryInsuranceId);
                claim.BillInformations = billingService.GetInsuranceUnit(claim.PrimaryInsuranceId, out unitType);
                claim.UnitType = unitType;
            }
            return PartialView("Visit", claim);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Supply(Guid episodeId, Guid patientId)
        {
            Final claim = null;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                claim = billingRepository.GetFinal(Current.AgencyId, patientId, episodeId);
                if (claim != null)
                {
                    var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        claim.EpisodeStartDate = episode.StartDate;
                        claim.EpisodeEndDate = episode.EndDate;
                    }
                    if (claim.VerifiedVisits.IsNotNullOrEmpty())
                    {
                        var visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>();
                        var supplyList = claim.Supply.IsNotNullOrEmpty() ? claim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                        if (visits != null && visits.Count > 0)
                        {
                            visits.ForEach(v =>
                            {
                                var episodeSupply = billingService.GetSupply(v);
                                if (episodeSupply != null && episodeSupply.Count > 0)
                                {
                                    episodeSupply.ForEach(s =>
                                    {
                                        if (!supplyList.Exists(l => l.UniqueIdentifier == s.UniqueIdentifier))
                                        {
                                            supplyList.Add(s);
                                        }
                                    });
                                }
                            });
                            claim.Supply = supplyList.ToXml();
                        }
                    }
                }
            }
            return PartialView("Supply", claim);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Summary(Guid episodeId, Guid patientId)
        {
            var claim = billingService.GetFinalInfo(patientId, episodeId);
            if (claim != null)
            {
                var unitType = -1;
                claim.BillInformations = billingService.GetInsuranceUnit(claim.PrimaryInsuranceId, out unitType);
                claim.UnitType = unitType;
                claim.IsMedicareHMO = agencyRepository.IsMedicareHMO(Current.AgencyId, claim.PrimaryInsuranceId);
                PPSStandard ppsStandard;
                if (claim.SupplyTotal <= 0 && claim.HippsCode.IsNotNullOrEmpty() && claim.HippsCode.Length == 5)
                {
                    claim.SupplyTotal = billingService.GetSupplyReimbursement(claim.HippsCode[4], claim.EpisodeStartDate.Year, out ppsStandard);
                }
            }
            return PartialView("Summary", claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RapVerify(Rap claim, string FirstBillableVisitDateFormatInput)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The RAP could not be verified." };
            if (claim != null)
            {
                claim.FirstBillableVisitDateFormat = FirstBillableVisitDateFormatInput;
                if (claim.IsValid)
                {
                    if (billingRepository.VerifyRap(Current.AgencyId, claim))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.Rap, LogAction.RAPVerified, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The RAP was verified successfully.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = claim.ValidationMessage;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimSummary(List<Guid> RapSelected, List<Guid> FinalSelected, Guid BranchId, int PrimaryInsurance)
        {
            return PartialView("ClaimSummary", billingService.ClaimToGenerate(RapSelected, FinalSelected, BranchId, PrimaryInsurance));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Generate(int ansiId)
        {
            var claimData = billingRepository.GetClaimData(Current.AgencyId, ansiId);
            string generateJsonClaim = claimData != null ? claimData.Data : string.Empty;
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] buffer = encoding.GetBytes(generateJsonClaim);
            Stream fileStream = new MemoryStream();
            fileStream.Write(buffer, 0, generateJsonClaim.Length);
            fileStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=billing{0}.txt", DateTime.Now.ToString("yyyyMMddHH:mm:ss:ff")));
            return new FileStreamResult(fileStream, "Text/Plain");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateANSI(List<Guid> RapSelected, List<Guid> FinalSelected, Guid BranchId, int PrimaryInsurance)
        {
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateDirect(RapSelected, FinalSelected, ClaimCommandType.download, out claimDataOut, out billExchage, BranchId, PrimaryInsurance))
            {
                if (claimDataOut != null)
                {
                    return Json(new { isSuccessful = true, Id = claimDataOut.Id });
                }
            }
            else
            {
                if (billExchage != null)
                {
                    return Json(new { isSuccessful = false, Id = -1, errorMessage = billExchage.Message });
                }
            }
            return Json(new { isSuccessful = false, Id = -1, errorMessage = "Error in processing of the claim(s)." });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitClaimDirectly(List<Guid> RapSelected, List<Guid> FinalSelected, Guid BranchId, int PrimaryInsurance)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim(s) are not processed. Try again." };
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateDirect(RapSelected, FinalSelected, ClaimCommandType.direct, out claimDataOut, out billExchage, BranchId, PrimaryInsurance))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The claim(s) are processed successfully.";
            }
            else
            {
                if (billExchage != null)
                {
                    viewData.errorMessage = billExchage.Message;
                }
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateSingleManagedANSI(Guid Id)
        {
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    ClaimData claimDataOut = null;
                    BillExchange billExchage;
                    var ids = (new List<Guid>());
                    ids.Add(Id);
                    if (billingService.GenerateManaged(ids, ClaimCommandType.download, out claimDataOut, out billExchage, patient.AgencyLocationId, claim.PrimaryInsuranceId))
                    {
                        if (claimDataOut != null)
                        {
                            return Json(new { isSuccessful = true, Id = claimDataOut.Id });
                        }
                    }
                    else
                    {
                        if (billExchage != null)
                        {
                            return Json(new { isSuccessful = false, Id = -1, errorMessage = billExchage.Message });
                        }
                    }

                }
            }
            return Json(new { isSuccessful = false, Id = -1, errorMessage = "Error in processing of the claim(s)." });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateManagedANSI(List<Guid> ManagedClaimSelected, Guid BranchId, int PrimaryInsurance)
        {
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateManaged(ManagedClaimSelected, ClaimCommandType.download, out claimDataOut, out billExchage,  BranchId, PrimaryInsurance))
            {
                if (claimDataOut != null)
                {
                    return Json(new { isSuccessful = true, Id = claimDataOut.Id });
                }
            }
            else
            {
                if (billExchage != null)
                {
                    return Json(new { isSuccessful = false, Id = -1, errorMessage = billExchage.Message });
                }
            }
            return Json(new { isSuccessful = false, Id = -1, errorMessage = "Error in processing of the claim(s)." });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitManagedClaimDirectly(List<Guid> ManagedClaimSelected, Guid BranchId, int PrimaryInsurance)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim(s) are not processed. Try again." };
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateManaged(ManagedClaimSelected, ClaimCommandType.direct, out claimDataOut, out billExchage, BranchId, PrimaryInsurance))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The claim(s) are processed successfully.";
            }
            else
            {
                if (billExchage != null)
                {
                    viewData.errorMessage = billExchage.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult UB04Pdf(Guid patientId, Guid Id, string type)
        {
            UB04Pdf doc = new UB04Pdf(billingService.GetUBOFourInfo(patientId, Id, type), this.billingService);
            MemoryStream stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=UB04_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateStatus(List<Guid> RapSelected, List<Guid> FinalSelected, string StatusType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your status update is not Successful." };
            bool rapStatus = billingService.UpdateRapStatus(RapSelected, Current.AgencyId, StatusType);
            bool finalStatus = billingService.UpdateFinalStatus(FinalSelected, Current.AgencyId, StatusType);
            if (rapStatus && finalStatus)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your status update is successful.";
            }
            else if (rapStatus && !finalStatus)
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Your Final (EOE) status update is not successful.";
            }
            else if (!rapStatus && finalStatus)
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Your RAP status update is not successful.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitManagedClaims(List<Guid> ManagedClaimSelected, string StatusType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your status update is not Successful." };
            if (billingService.UpdateManagedClaimStatus(ManagedClaimSelected, Current.AgencyId, StatusType))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your status update is successful.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalComplete(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Final (EOE) could not be completed." };
            if (!id.IsEmpty())
            {
                if (billingService.FinalComplete(id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Final(EOE) completed successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult History()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var selectionLists = new FilterViewData();
            if (agency != null)
            {
                int payorType;
                if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agency.Payor, out payorType))
                {
                    selectionLists.Insurances = agencyService.Insurances(agency.Payor.ToString(), false, true);
                    selectionLists.SelecetdInsurance = agency.Payor;
                }
                else
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        selectionLists.Insurances = agencyService.Insurances(agencyMedicareInsurance.Id.ToString(), false, true);
                        selectionLists.SelecetdInsurance = agencyMedicareInsurance.Id.ToString();
                    }
                }
            }
            return PartialView("History", selectionLists);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedHistory()
        {
            var selectionLists = new FilterViewData();
            selectionLists.Insurances = agencyService.Insurances("0", true, true);
            selectionLists.SelecetdInsurance = "0";
            return PartialView("Managed/History", selectionLists);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedCenter()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType != 3).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        return PartialView("Managed/Center", billingService.ManagedBill(agencyMainBranch.Id, agencyMedicareInsurance.Id, (int)ManagedClaimStatus.ClaimCreated));
                    }
                }
            }
            return PartialView("Managed/Center", new ManagedBillViewData { Bills= new List<ManagedBill>()});
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedGrid(Guid branchId, int insuranceId)
        {
            return PartialView("Managed/ManagedGrid", billingRepository.GetManagedClaims(Current.AgencyId, branchId, insuranceId, (int)ManagedClaimStatus.ClaimCreated));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingClaims()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var selectionLists = new FilterViewData();
            if (agency != null)
            {
                var agencyBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyBranch != null && !agencyBranch.Id.IsEmpty())
                {
                    selectionLists.Branches = agencyService.Branchs(agencyBranch.Id.ToString(), false);
                    selectionLists.SelecetdBranch = agencyBranch.Id;
                    int payorType;
                    if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agency.Payor, out payorType))
                    {
                        selectionLists.Insurances = agencyService.Insurances(agency.Payor.ToString(), false, true);
                        selectionLists.SelecetdInsurance = agency.Payor;
                    }
                    else
                    {
                        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                        if (agencyMedicareInsurance != null)
                        {
                            selectionLists.Insurances = agencyService.Insurances(agencyMedicareInsurance.Id.ToString(), false, true);
                            selectionLists.SelecetdInsurance = agencyMedicareInsurance.Id.ToString();
                        }
                    }
                }
                else
                {
                    var agencyBranches = agencyRepository.GetBranches(Current.AgencyId).FirstOrDefault();
                    if (agencyBranches != null)
                    {
                        selectionLists.Branches = agencyService.Branchs(agencyBranches.Id.ToString(), false);
                        selectionLists.SelecetdBranch = agencyBranches.Id;

                        int payorType;
                        if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agency.Payor, out payorType))
                        {
                            selectionLists.Insurances = agencyService.Insurances(agency.Payor.ToString(), false, true);
                            selectionLists.SelecetdInsurance = agency.Payor;
                        }
                        else
                        {
                            var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                            if (agencyMedicareInsurance != null)
                            {
                                selectionLists.Insurances = agencyService.Insurances(agencyMedicareInsurance.Id.ToString(), false, true);
                                selectionLists.SelecetdInsurance = agencyMedicareInsurance.Id.ToString();
                            }
                        }
                    }
                }
            }
            return PartialView("PendingClaims", selectionLists);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingClaimRap(Guid branchId, string insuranceId)
        {
            return PartialView("PendingClaimRap", billingService.PendingClaimRaps(branchId, insuranceId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingClaimRaps(Guid branchId, string insuranceId)
        {
            return View(new GridModel(billingService.PendingClaimRaps(branchId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateRapClaim(Guid Id, Guid branchId, string insuranceId, DateTime PaymentDate, double PaymentAmount, int Status)
        {
            billingService.UpdateRapClaimStatus(Id, PaymentDate, PaymentAmount, Status);
            return View(new GridModel(billingService.PendingClaimRaps(branchId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RapSnapShots(Guid Id)
        {
            var raps = billingRepository.GetRapSnapShots(Current.AgencyId, Id);
            return View(new GridModel(raps != null && raps.Count > 0 ? raps : new List<RapSnapShot>()));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateRapSnapShotsClaim(Guid Id, long BatchId, DateTime PaymentDate, double Payment, int Status)
        {
            billingService.UpdateSnapShot(Id, BatchId, Payment, PaymentDate, Status, "RAP");
            return View(new GridModel(billingRepository.GetRapSnapShots(Current.AgencyId, Id)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingClaimFinal(Guid branchId, string insuranceId)
        {
            return PartialView("PendingClaimFinal", billingService.PendingClaimFinals(branchId, insuranceId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateFinalClaim(Guid Id, Guid branchId, string insuranceId, DateTime PaymentDate, double PaymentAmount, int Status)
        {
            billingService.UpdateFinalClaimStatus(Id, PaymentDate, PaymentAmount, Status);
            return View(new GridModel(billingService.PendingClaimFinals(branchId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingClaimFinals(Guid branchId, string insuranceId)
        {
            return View(new GridModel(billingService.PendingClaimFinals(branchId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalSnapShots(Guid Id)
        {
            return View(new GridModel(billingRepository.GetFinalSnapShots(Current.AgencyId, Id)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateFinalSnapShotsClaim(Guid Id, long BatchId, DateTime PaymentDate, double Payment, int Status)
        {
            billingService.UpdateSnapShot(Id, BatchId, Payment, PaymentDate, Status, "Final");
            return View(new GridModel(billingRepository.GetFinalSnapShots(Current.AgencyId, Id)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SnapShotClaims(Guid Id, string Type)
        {
            return View(new GridModel(billingService.ClaimSnapShots(Id, Type)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSnapShotClaim(Guid Id, long BatchId, string Type, DateTime PaymentDate, double PaymentAmount, int Status)
        {
            billingService.UpdateSnapShot(Id, BatchId, PaymentAmount, PaymentDate, Status, Type);
            return View(new GridModel(billingService.ClaimSnapShots(Id, Type)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryActivity(Guid patientId ,int insuranceId)
        {
            return View(new GridModel(billingService.Activity(patientId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimsActivity(Guid patientId, int insuranceId)
        {
            if (patientId.IsEmpty())
            {
                return View(new GridModel(new List<ManagedClaimLean>()));
            }
            return View(new GridModel(billingRepository.GetManagedClaimsPerPatient(Current.AgencyId, patientId, insuranceId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfoVerify(Final claim, string FirstBillableVisitDateFormatInput)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final Basic Info is not verified." };
            if (claim != null)
            {
                claim.FirstBillableVisitDateFormat = FirstBillableVisitDateFormatInput;
                if (claim.IsValid)
                {
                    if (billingRepository.VerifyInfo( Current.AgencyId,claim))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.Final, LogAction.FinalDemographicsVerified, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Final Basic Info is successfully verified.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = claim.ValidationMessage;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> Visit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final is not verified." };
            if (billingService.VisitVerify(Id, episodeId, patientId, Visit))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalVisitVerified, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Final Visit is successfully verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyVerify(FormCollection formCollection, List<Guid> SupplyId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final supply is not verified." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var episodeId = keys.Contains("episodeId") && formCollection["episodeId"].IsNotNullOrEmpty() ? formCollection["episodeId"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("patientId") && formCollection["patientId"].IsNotNullOrEmpty() ? formCollection["patientId"].ToGuid() : Guid.Empty;
                if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                {
                    if (billingService.VisitSupply(Id, episodeId, patientId, formCollection, SupplyId))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalSupplyVerified, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Final supply is successfully verified.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(Guid patientId, Guid id, string type)
        {
            return PartialView("Update", billingService.GetClaimViewData(patientId, id, type));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveUpdate(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var type = keys.Contains("Type") && formCollection["Type"].IsNotNullOrEmpty() ? formCollection["Type"].ToString() : string.Empty;
                var claimStatus = keys.Contains("Status") && formCollection["Status"].IsNotNullOrEmpty() && formCollection["Status"].IsInteger() ? formCollection["Status"].ToInteger() : 0;
                var primaryInsuranceId = keys.Contains("PrimaryInsuranceId") && formCollection["PrimaryInsuranceId"].IsNotNullOrEmpty() && formCollection["PrimaryInsuranceId"].IsInteger() ? formCollection["PrimaryInsuranceId"].ToInteger() : 0;
                var comment = keys.Contains("Comment") && formCollection["Comment"].IsNotNullOrEmpty() ? formCollection["Comment"].ToString() : string.Empty;

                rules.Add(new Validation(() => keys.Contains("PaymentAmount") && formCollection["PaymentAmount"].IsNotNullOrEmpty() ? !formCollection["PaymentAmount"].IsDouble() : false, "Payment Value is not a right format."));
                rules.Add(new Validation(() => keys.Contains("PaymentDateValue") && formCollection["PaymentDateValue"].IsNotNullOrEmpty() ? !formCollection["PaymentDateValue"].IsValidDate() : false, "Payment date is not a right format."));

                if (!Id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty())
                {
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        var paymentAmount = formCollection["PaymentAmount"].IsNotNullOrEmpty() ? formCollection["PaymentAmount"].ToDouble() : 0;
                        var paymentDate = formCollection["PaymentDateValue"].IsDate() ? formCollection["PaymentDateValue"].ToDateTime() : DateTime.MinValue;
                        if (billingService.UpdateProccesedClaimStatus(patientId, Id, type, paymentDate, paymentAmount, paymentDate, claimStatus, primaryInsuranceId, comment))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim updated successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message; ;
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimInfo(Guid patientId, Guid claimId, string claimType)
        {
            return PartialView("ClaimInfo", billingService.GetClaimSnapShotInfo(patientId, claimId, claimType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimData(Guid patientId, Guid claimId, string claimType)
        {
            var viewData = new BillingHistoryViewData();
            viewData.ClaimInfo = billingService.GetClaimSnapShotInfo(patientId, claimId, claimType);
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateClaim(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var episodeId = keys.Contains("EpisodeId") && formCollection["EpisodeId"].IsNotNullOrEmpty() ? formCollection["EpisodeId"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var type = keys.Contains("Type") && formCollection["Type"].IsNotNullOrEmpty() ? formCollection["Type"].ToString() : string.Empty;
                if (!episodeId.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty())
                {
                    rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "Patient dosn't exist."));
                    rules.Add(new Validation(() => !patientRepository.IsEpisodeExist(Current.AgencyId, episodeId), "Episode dosn't exist for this claim."));
                    rules.Add(new Validation(() => billingService.IsEpisodeHasClaim(episodeId, patientId, type), "Episode already has claim."));
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.AddClaim(patientId, episodeId, type))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim updated successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message; ;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Please verify the information provided.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewClaim(Guid patientId, string type)
        {
            var newClaimData = new NewClaimViewData();
            newClaimData.EpisodeData = billingRepository.GetEpisodeNeedsClaim(Current.AgencyId, patientId, type);
            newClaimData.Type = type;
            newClaimData.PatientId = patientId;
            return PartialView("NewClaim", newClaimData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewManagedClaim(Guid patientId)
        {
            var newClaimData = new NewManagedClaimViewData();
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    newClaimData.Insurances = agencyService.Insurances(patient.PrimaryInsurance, false, false);
                    newClaimData.SelecetdInsurance = patient.PrimaryInsurance;
                    newClaimData.PatientId = patientId;
                }
            }
            return PartialView("Managed/NewManagedClaim", newClaimData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateManagedClaim(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var insuranceId = keys.Contains("InsuranceId") && formCollection["InsuranceId"].IsNotNullOrEmpty() && formCollection["InsuranceId"].IsInteger() ? formCollection["InsuranceId"].ToInteger() : -1;
                var startDate = keys.Contains("StartDate") && formCollection["StartDate"].IsNotNullOrEmpty() && formCollection["StartDate"].IsValidDate() ? formCollection["StartDate"].ToDateTime() : DateTime.MinValue;
                var endDate = keys.Contains("EndDate") && formCollection["EndDate"].IsNotNullOrEmpty() && formCollection["EndDate"].IsValidDate() ? formCollection["EndDate"].ToDateTime() : DateTime.MinValue;
                if (!patientId.IsEmpty())
                {
                    rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "Patient dosn't exist."));
                    rules.Add(new Validation(() => startDate.Date <= DateTime.MinValue.Date, "Claim start Date don't have a correct date value."));
                    rules.Add(new Validation(() => endDate.Date <= DateTime.MinValue.Date, "Claim end Date don't have a correct date value."));
                    rules.Add(new Validation(() => startDate.Date > endDate.Date, "Claim start date must be less than claim end date."));
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.AddManagedClaim(patientId, startDate, endDate, insuranceId))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim added successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message; ;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Please verify the information provided.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaim(Guid Id, Guid patientId)
        {
            if (Id.IsEmpty() || patientId.IsEmpty())
            {
                return PartialView("Managed/Edit", new Final());
            }
            return PartialView("Managed/Edit", billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedClaimInfo(Guid Id, Guid patientId)
        {
            return PartialView("Managed/Info", billingService.GetManagedClaimInfo(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimEpisodes(Guid managedClaimId, Guid patientId)
        {
            return PartialView("Managed/MultipleEpisodes", billingService.GetManagedClaimEpisodes(patientId, managedClaimId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedClaimVisit(Guid Id, Guid patientId)
        {
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                claim.Visits = patientRepository.GetScheduledEventsOnly(Current.AgencyId, patientId, claim.EpisodeStartDate, claim.EpisodeEndDate);
                var unitType = -1;
                claim.BillInformations = billingService.GetInsuranceUnit(claim.PrimaryInsuranceId, out unitType);
                claim.UnitType = unitType;
            }
            return PartialView("Managed/Visit", claim);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedClaimSupply(Guid Id, Guid patientId)
        {
            ManagedClaim claim = null;
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                if (claim != null && claim.VerifiedVisits.IsNotNullOrEmpty())
                {
                    var visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>();
                    var supplyList = claim.Supply.IsNotNullOrEmpty() ? claim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                    if (visits != null && visits.Count > 0)
                    {
                        visits.ForEach(v =>
                        {
                            var episodeSupply = billingService.GetSupply(v);
                            if (episodeSupply != null && episodeSupply.Count > 0)
                            {
                                episodeSupply.ForEach(s =>
                                {
                                    if (!supplyList.Exists(l => l.UniqueIdentifier == s.UniqueIdentifier))
                                    {
                                        supplyList.Add(s);
                                    }
                                });
                            }
                        });
                        claim.Supply = supplyList.ToXml();
                    }
                }
            }
            return PartialView("Managed/Supply", claim);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedSummary(Guid Id, Guid patientId)
        {
            var claim = billingService.GetManagedClaimInfo(patientId, Id);
            if (claim != null)
            {
                var unitType = -1;
                claim.BillInformations = billingService.GetInsuranceUnit(claim.PrimaryInsuranceId, out unitType);
                claim.UnitType = unitType;
               // PPSStandard ppsStandard;
               // if (claim.SupplyTotal <= 0 && claim.HippsCode.IsNotNullOrEmpty() && claim.HippsCode.Length == 5) claim.SupplyTotal = billingService.GetSupplyReimbursement(claim.HippsCode[4], claim.EpisodeStartDate.Year, out ppsStandard);

            }
            return PartialView("Managed/Summary", claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedInfoVerify(ManagedClaim claim)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Managed Claim Basic Info is  not verified." };
            if (claim.IsValid)
            {
                if (billingRepository.ManagedVerifyInfo(claim, Current.AgencyId))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedDemographicsVerified, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Managed Claim Basic Info is successfully verified.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = claim.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ManagedClaimAssessmentData(string input)
        {
            var managedClaimEpisodeData = new ManagedClaimEpisodeData();
            if (input.IsNotNullOrEmpty())
            {
                var inputArray = input.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                if (inputArray != null && inputArray.Length > 0 && input.Length > 0)
                {
                    managedClaimEpisodeData = billingService.GetEpisodeAssessmentData(inputArray[0].ToGuid(), inputArray[1].ToGuid());
                }
            }
            return Json(managedClaimEpisodeData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedVisitVerify(Guid Id, Guid patientId, List<Guid> Visit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Managed Claim visit is not verified." };
            if (billingService.ManagedVisitVerify(Id, patientId, Visit))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedClaim, LogAction.ManagedVisitVerified, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Managed Claim Visit is successfully verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedSupplyVerify(FormCollection formCollection, List<Guid> SupplyId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Managed Claim supply is not verified." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("patientId") && formCollection["patientId"].IsNotNullOrEmpty() ? formCollection["patientId"].ToGuid() : Guid.Empty;
                if (!Id.IsEmpty() && !patientId.IsEmpty())
                {
                    if (billingService.ManagedVisitSupply(Id, patientId, formCollection, SupplyId))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedClaim, LogAction.ManagedSupplyVerified, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Managed Claim supply is successfully verified.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimSummary(List<Guid> ManagedClaimSelected, Guid BranchId, int PrimaryInsurance)
        {
            return PartialView("Managed/ClaimSummary", billingService.ManagedClaimToGenerate(ManagedClaimSelected, BranchId, PrimaryInsurance));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult ManagedUB04Pdf(Guid patientId, Guid Id)
        {
            ManagedUB04Pdf doc = new ManagedUB04Pdf(billingService.GetManagedUBOFourInfo(patientId, Id), billingService);
            MemoryStream stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=UB04_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteManagedClaim(Guid patientId, Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be deleted. Please try again." };
            var rules = new List<Validation>();
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "The patient does not exist."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (billingRepository.DeleteManagedClaim(Current.AgencyId, patientId, id))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, id.ToString(), LogType.ManagedClaim, LogAction.ManagedDeleted, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The claim has been deleted successfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in deleting the claim. Please try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the claim. Please try again";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaim(Guid patientId, Guid id)
        {
            return PartialView("Managed/Update", billingService.GetManagedClaimInfo(patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimStatus(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var claimStatus = keys.Contains("Status") && formCollection["Status"].IsNotNullOrEmpty() && formCollection["Status"].IsInteger() ? formCollection["Status"].ToInteger() : 0;
                var comment = keys.Contains("Comment") && formCollection["Comment"].IsNotNullOrEmpty() ? formCollection["Comment"].ToString() : string.Empty;
                rules.Add(new Validation(() => keys.Contains("PaymentAmount") && formCollection["PaymentAmount"].IsNotNullOrEmpty() ? !formCollection["PaymentAmount"].IsDouble() : false, "Payment Value is not a right format."));
                rules.Add(new Validation(() => keys.Contains("PaymentDateValue") && formCollection["PaymentDateValue"].IsNotNullOrEmpty() ? !formCollection["PaymentDateValue"].IsValidDate() : false, "Payment date is not a right format."));
                if (!Id.IsEmpty() && !patientId.IsEmpty())
                {
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        var paymentAmount = formCollection["PaymentAmount"].IsNotNullOrEmpty() ? formCollection["PaymentAmount"].ToDouble() : 0;
                        var paymentDate = formCollection["PaymentDateValue"].ToDateTime();
                        if (billingService.UpdateProccesedManagedClaimStatus(patientId, Id, paymentDate, paymentAmount, paymentDate, claimStatus, comment))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim updated successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message; ;
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedSnapShotClaimInfo(Guid patientId, Guid claimId)
        {
            return PartialView("Managed/ClaimInfo", billingService.GetManagedClaimSnapShotInfo(patientId, claimId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedComplete(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Managed Claim could not be completed." };
            if (!id.IsEmpty())
            {
                if (billingService.ManagedComplete(id, patientId))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Managed Claim completed successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteClaim(Guid patientId, Guid id, string type)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be deleted. Please try again." };
            var rules = new List<Validation>();
            if (!id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty())
            {
                rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "The patient does not exist."));
                rules.Add(new Validation(() => !billingService.IsEpisodeHasClaim(id, patientId, type), "This claim does not exist."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (patientService.DeleteClaim(patientId, id, type))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The claim has been deleted successfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in deleting the claim. Please try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the claim. Please try again";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePending(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim update was not successful." };
            var rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length>0)
            {
                var rapIds = keys.Contains("RapId") && formCollection["RapId"].IsNotNullOrEmpty() ? formCollection["RapId"].ToArray() : null;
                var finalIds = keys.Contains("FinalId") && formCollection["FinalId"].IsNotNullOrEmpty() ? formCollection["FinalId"].ToArray() : null;
                if ((rapIds != null && rapIds.Length > 0) || (finalIds != null && finalIds.Length > 0))
                {
                    if (rapIds != null && rapIds.Length > 0)
                    {
                        foreach (var id in rapIds)
                        {
                            rules.Add(new Validation(() => keys.Contains(string.Format("RapPaymentDate_{0}", id)) && formCollection[string.Format("RapPaymentDate_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("RapPaymentDate_{0}", id)].IsValidDate() : false, "Payment date is not a right format."));
                            rules.Add(new Validation(() => keys.Contains(string.Format("RapPayment_{0}", id)) && formCollection[string.Format("RapPayment_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("RapPayment_{0}", id)].IsDouble() : false, "Payment amount is not a right format."));
                        }
                    }
                    if (finalIds != null && finalIds.Length > 0)
                    {
                        foreach (var id in finalIds)
                        {
                            rules.Add(new Validation(() => keys.Contains(string.Format("FinalPaymentDate_{0}", id)) && formCollection[string.Format("FinalPaymentDate_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("FinalPaymentDate_{0}", id)].IsValidDate() : false, "Payment date is not a right format."));
                            rules.Add(new Validation(() => keys.Contains(string.Format("FinalPayment_{0}", id)) && formCollection[string.Format("FinalPayment_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("FinalPayment_{0}", id)].IsDouble() : false, "Payment amount is not a right format."));
                        }
                    }
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (billingService.UpdatePendingClaimStatus(formCollection, rapIds, finalIds))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim updated successfully.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "The claim update was not successful.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Remittance()
        {
            return PartialView("Remittances", billingRepository.GetRemittances(Current.AgencyId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceContent(DateTime StartDate, DateTime EndDate)
        {
            return PartialView("RemittanceContent", billingRepository.GetRemittances(Current.AgencyId, StartDate, EndDate));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult RemittancesPdf(DateTime StartDate, DateTime EndDate)
        {
            var doc = new RemittancesPdf(billingRepository.GetRemittances(Current.AgencyId, StartDate, EndDate).ToList<RemittanceLean>(), agencyRepository.Get(Current.AgencyId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Remittances_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult RemittancePdf(Guid Id) {
            var remittance = billingRepository.GetRemittance(Current.AgencyId, Id);
            if (remittance != null && remittance.Data.IsNotNullOrEmpty()) remittance.Data = remittance.Data.Replace("&", "&amp;").Replace("\r", "").Replace("\n", "");
            var doc = new RemittancePdf(remittance, agencyRepository.Get(Current.AgencyId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=RemittanceDetail_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceDetail(Guid Id)
        {
            var doc = new XmlDocument();
            var remittance = new Remittance();
            try
            {
                remittance = billingRepository.GetRemittance(Current.AgencyId, Id);
                if (remittance != null && remittance.Data.IsNotNullOrEmpty())
                {
                    var data = new RemittanceData();
                    var filtteredData = remittance.Data.Replace("&", "&amp;").Replace("\r", "").Replace("\n", "");
                    remittance.Data = filtteredData;
                    data = filtteredData.ToObject<RemittanceData>();
                    // var data=remittance.Data.Replace("&", "&amp;");
                    // doc = new XmlDocument();
                    // doc.LoadXml(data);
                    return PartialView("RemittanceDetail", remittance);
                }
            }
            catch (Exception ex)
            {
                return PartialView("RemittanceDetail", remittance);
            }
            return PartialView("RemittanceDetail", remittance);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceUpload()
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The remittance upload is unsuccessful. Try again." };
            var file = Request.Files.Get(0);
            if (file != null)
            {
                if (file.ContentType != "Text/Plain")
                {
                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        if (billingService.AddRemittanceUpload(file))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The upload is successful.";
                        }
                        else
                        {
                            viewData.errorMessage = "The remittance upload is unsuccessful. Try again.";
                        }
                    }
                    else
                    {
                        viewData.errorMessage = "The upload file is empty.";
                    }
                }
                else
                {
                    viewData.errorMessage = "The upload is not on the right format.";
                }
            }
            else
            {
                viewData.errorMessage = "There is no file uploaded.";
            }
            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteRemittance(Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The remittance could not be deleted. Please try again." };
            if (billingRepository.DeleteRemittance(Current.AgencyId, Id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The remittance has been deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimLogs(string type, Guid claimId, Guid patientId)
        {
            if (type.ToUpperCase() == LogType.Rap.ToString().ToUpperCase() || type.ToUpperCase() == LogType.Final.ToString().ToUpperCase() || type.ToUpperCase() == LogType.ManagedClaim.ToString().ToUpperCase())
            {
                return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Patient, type.ToUpperCase() == LogType.Rap.ToString().ToUpperCase() ? LogType.Rap : (type.ToUpperCase() == LogType.Final.ToString().ToUpperCase() ? LogType.Final : LogType.ManagedClaim), patientId, claimId.ToString()));
            }
            return PartialView("ActivityLogs", new List<AppAudit>());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SubmittedList()
        {
            return PartialView("SubmittedList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimSubmittedList(DateTime StartDate, DateTime EndDate, string ClaimType)
        {
            return View(new GridModel(billingRepository.ClaimDatas(Current.AgencyId, StartDate, EndDate, ClaimType)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmittedClaimDetail(int Id)
        {
            return PartialView("SubmittedClaimsDetail", billingService.GetSubmittedBatchClaims(Id));
        }

        #endregion
    }
}
