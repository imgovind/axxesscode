﻿namespace Axxess.AgencyManagement.App.Workflows
{
    using System;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.AgencyManagement.App.Security;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Repositories;

    public class CreateUserWorkflow : IWorkflow
    {
        #region CreateUserWorkflow Members

        private User user { get; set; }
        private Guid loginId { get; set; }
        private bool wasUserCreated { get; set; }
        private bool wasLoginCreated { get; set; }

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;

        public CreateUserWorkflow(User user)
        {
            Check.Argument.IsNotNull(user, "user");

            this.wasUserCreated = false;
            this.wasLoginCreated = false;

            this.user = user;

            this.loginRepository = Container.Resolve<IMembershipDataProvider>().LoginRepository;
            this.userRepository = Container.Resolve<IAgencyManagementDataProvider>().UserRepository;
        }

        #endregion

        #region IWorkflow Members

        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        public void Process()
        {
            var work = new WorkSequence();

            work.Complete += (sequence) =>
            {
                this.isCommitted = wasUserCreated && wasLoginCreated;
            };

            work.Error += (sequence, item, index) =>
            {
                this.message = item.Description;
            };

            //work.Add(
            //    () =>
            //        {
            //            string salt = string.Empty;
            //            var saltedHash = new SaltedHash();
            //            string passwordHash = string.Empty;
            //            saltedHash.GetHashAndSalt(user.Password, out passwordHash, out salt);
            //            this.loginId = loginRepository.Add(user.EmailAddress, passwordHash, salt, Roles.ApplicationUser.ToString(), user.FirstName, true, user.AllowWeekendAccess, user.EarliestLoginTime, user.AutomaticLogoutTime);
            //            this.wasLoginCreated = this.loginId != Guid.Empty;
            //        }, 
            //    () =>  
            //        {
            //            this.loginRepository.Delete(this.loginId);
            //        }, 
            //    "System could not save the Login information.");

            //work.Add(
            //    () =>
            //        {
            //            if (wasLoginCreated)
            //            {
            //                user.LoginId = this.loginId;
            //                user.AgencyId = Current.AgencyId;
            //                user.Id = userRepository.Add(user);
            //                this.wasUserCreated = user.Id != Guid.Empty;
            //            }
            //        }, 
            //    () =>
            //        {
            //            this.userRepository.Delete(this.user.Id);
            //        }, 
            //    "System could not save the User information.");

            //work.Add(
            //    () =>
            //        {
            //            if (this.wasUserCreated)
            //            {
            //                var bodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName, "agencyname", Current.AgencyName, "emailaddress", user.EmailAddress, "password", user.Password);
            //                Notify.User(AppSettings.WebMasterEmail, user.EmailAddress, "Welcome to Axxess Home Health Management Software", bodyText);
            //            }
            //        });

            work.Perform();
        }

        #endregion
    }
}
