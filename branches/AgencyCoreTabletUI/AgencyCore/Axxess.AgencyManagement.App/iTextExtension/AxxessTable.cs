﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    class AxxessTable : PdfPTable {
        public AxxessTable(int cols) : base(cols) {
            this.WidthPercentage = 100;
            this.SplitLate = false;
        }
        public AxxessTable(int cols, bool split) : base(cols) {
            this.WidthPercentage = 100;
            this.SplitLate = !split;
            this.SplitRows = split;
        }
        public AxxessTable(float[] widths, bool split) : base(widths) {
            this.WidthPercentage = 100;
            this.SplitLate = !split;
            this.SplitRows = split;
        }
        public void CompleteRow(PdfPCell filler) {
            filler.AddElement(new Chunk(" "));
            while (!this.rowCompleted) this.AddCell(filler);
        }
        public void AddCell(String text, Font font, float[] padding, float[] borders) {
            AxxessCell cell = new AxxessCell(padding, borders);
            cell.AddElement(new Chunk(text, font));
            this.AddCell(cell);
        }
        public void AddCell(String text, Font font, String alignment, float[] padding, float[] borders) {
            Paragraph paragraph = new Paragraph(text, font);
            paragraph.SetAlignment(alignment);
            AxxessCell cell = new AxxessCell(padding, borders);
            cell.AddElement(paragraph);
            this.AddCell(cell);
        }
        public void AddCell(IElement content, float[] padding, float[] borders) {
            AxxessCell cell = new AxxessCell(padding, borders);
            cell.AddElement(content);
            this.AddCell(cell);
        }
    }
}