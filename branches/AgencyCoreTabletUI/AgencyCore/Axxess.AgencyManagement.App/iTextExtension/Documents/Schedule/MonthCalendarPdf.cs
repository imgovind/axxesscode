﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using Axxess.Core;
    using iTextSharp.text;
    class MonthCalendarPdf : AxxessPdf {
        public MonthCalendarPdf(UserCalendarViewData data) {
            this.SetPageSize(AxxessDoc.Landscape);
            this.SetType(PdfDocs.MonthCalendar);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 9F;
            this.SetFonts(fonts);
            AxxessTable[] calendar = new AxxessTable[] { new AxxessTable(7, false) };
            DateTime CurrentDate = data.FromDate.AddDays(-(int)data.FromDate.DayOfWeek);
            int weekNumber = DateUtilities.Weeks(data.FromDate.Month, data.FromDate.Year);
            for (int i = 0; i <= weekNumber; i++) {
                int addedDate = (i) * 7;
                for (int j = 0; j <= 6; j++) {
                    DateTime SpecificDate = CurrentDate.AddDays(j + addedDate);
                    AxxessCell day = new AxxessCell(new float[] { 1, 2, 8, 2 }, new float[] { .2F, .2F, .2F, .2F });
                    if (SpecificDate < data.FromDate || SpecificDate > data.ToDate) {
                        day.AddElement(new Chunk(""));
                        calendar[0].AddCell(day);
                    } else {
                        day.AddElement(new Chunk(SpecificDate.Day.ToString()));
                        var events = data.UserEvents.FindAll(e => e.VisitDate.ToZeroFilled() == SpecificDate.ToShortDateString().ToZeroFilled());
                        var count = events.Count;
                        if (count > 0) events.ForEach(e => {
                            day.AddElement(new Paragraph(String.Format("{0} - {1}", e.TaskName, e.PatientName), fonts[0]));
                        });
                        calendar[0].AddCell(day);
                    }
                }
            }
            this.SetContent(calendar);
            this.SetMargins(new float[] { 98, 28, 28, 28 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("employee", String.Format("{0} - {1}", Current.UserFullName, Current.AgencyName));
            fieldmap[0].Add("month", String.Format("{0:MMMM} {0:yyyy}", data.FromDate));
            this.SetFields(fieldmap);
        }
    }
}