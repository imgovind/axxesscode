﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;
    using iTextSharp.text;
    class PatientProfilePdf : AxxessPdf {
        public PatientProfilePdf(PatientProfile data) {
            this.SetType(PdfDocs.PatientProfile);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(data != null && data.Patient != null && data.Patient.Comments.IsNotNullOrEmpty() ? data.Patient.Comments : " ") };
            this.SetContent(content);
            this.SetMargins(new float[] { 555, 35, 35, 35 });
            var assessment = data.CurrentAssessment != null ? data.CurrentAssessment.ToDictionary() : new Dictionary<string, Question>();
            String[] ethnicities = data.Patient.Ethnicities != null && data.Patient.Ethnicities != string.Empty ? data.Patient.Ethnicities.Split(';') : null;
            String race = string.Empty;
            if (ethnicities != null) {
                foreach(String ethnic in ethnicities) {
                    int result;
                    if (Int32.TryParse(ethnic, out result)) race += ((Race)Enum.ToObject(typeof(Race), (result))).GetDescription() + " ";
                }
            }
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name + "\n" : string.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1 : string.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2 + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity + ", " : string.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : string.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : string.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty)
                    : string.Empty)
                : string.Empty));
            fieldmap[0].Add("patient", (
                data != null && data.Patient != null ?
                    (data.Patient.AddressLine1.IsNotNullOrEmpty() ? data.Patient.AddressLine1 : string.Empty) +
                    (data.Patient.AddressLine2.IsNotNullOrEmpty() ? data.Patient.AddressLine2 + "\n" : "\n") +
                    (data.Patient.AddressCity.IsNotNullOrEmpty() ? data.Patient.AddressCity + ", " : string.Empty) +
                    (data.Patient.AddressStateCode.IsNotNullOrEmpty() ? data.Patient.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                    (data.Patient.AddressZipCode.IsNotNullOrEmpty() ? data.Patient.AddressZipCode : string.Empty) +
                    (data.Patient.PhoneHome.IsNotNullOrEmpty() ? "\nPhone: " + data.Patient.PhoneHome.ToPhone() : string.Empty)
                : string.Empty));
            fieldmap[0].Add("soc", data != null && data.Patient != null && data.Patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? data.Patient.StartOfCareDateFormatted : string.Empty);
            fieldmap[0].Add("bday", data != null && data.Patient != null && data.Patient.DOBFormatted.IsNotNullOrEmpty() ? data.Patient.DOBFormatted : string.Empty);
            fieldmap[0].Add("sex", data != null && data.Patient != null && data.Patient.Gender.IsNotNullOrEmpty() ? data.Patient.Gender : string.Empty);
            fieldmap[0].Add("marital", data != null && data.Patient != null && data.Patient.MaritalStatus.IsNotNullOrEmpty() ? data.Patient.MaritalStatus : string.Empty);
            fieldmap[0].Add("office", data != null && data.Agency != null && data.Agency.MainLocation != null && data.Agency.MainLocation.Name.IsNotNullOrEmpty() ? data.Agency.MainLocation.Name : string.Empty);
            fieldmap[0].Add("race", race);
            fieldmap[0].Add("height", assessment != null && assessment.ContainsKey("GenericHeight") && assessment["GenericHeight"].Answer.IsNotNullOrEmpty() ? assessment["GenericHeight"].Answer : string.Empty);
            fieldmap[0].Add("weight", assessment != null && assessment.ContainsKey("GenericWeight") && assessment["GenericWeight"].Answer.IsNotNullOrEmpty() ? assessment["GenericWeight"].Answer : string.Empty);
            fieldmap[0].Add("id", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("mcare", data != null && data.Patient != null && data.Patient.MedicareNumber.IsNotNullOrEmpty() ? data.Patient.MedicareNumber : string.Empty);
            fieldmap[0].Add("cert", data != null && data.CurrentEpisode != null && data.CurrentEpisode.StartDateFormatted.IsNotNullOrEmpty() && data.CurrentEpisode.EndDateFormatted.IsNotNullOrEmpty() ? data.CurrentEpisode.StartDateFormatted + " - " + data.CurrentEpisode.EndDateFormatted : string.Empty);
            fieldmap[0].Add("triage", data != null && data.Patient != null ? (data.Patient.Triage == 1 ? "1. Life threatening (or potential) and requires ongoing medical treatment." : string.Empty) + (data.Patient.Triage == 2 ? "2. Not life threatening but would suffer severe adverse effects if visit postponed." : string.Empty) + (data.Patient.Triage == 3 ? "3. Visits could be postponed 24-48 hours without adverse effects." : string.Empty) + (data.Patient.Triage == 4 ? "4. Visits could be postponed 72-96 hours without adverse effects." : string.Empty) : string.Empty);
            fieldmap[0].Add("refdate", data != null && data.Patient != null && data.Patient.ReferralDate.IsValid() ? data.Patient.ReferralDate.ToShortDateString().Clean() : string.Empty);
            fieldmap[0].Add("priins", data != null && data.Patient != null && data.Patient.PrimaryInsuranceName.IsNotNullOrEmpty() ? data.Patient.PrimaryInsuranceName.Clean() : string.Empty);
            fieldmap[0].Add("priinsnum", data != null && data.Patient != null && data.Patient.PrimaryHealthPlanId.IsNotNullOrEmpty() ? data.Patient.PrimaryHealthPlanId.Clean() : string.Empty);
            fieldmap[0].Add("secins", data != null && data.Patient != null && data.Patient.SecondaryInsuranceName.IsNotNullOrEmpty() ? data.Patient.SecondaryInsuranceName.Clean() : string.Empty);
            fieldmap[0].Add("secinsnum", data != null && data.Patient != null && data.Patient.SecondaryHealthPlanId.IsNotNullOrEmpty() ? data.Patient.SecondaryHealthPlanId.Clean() : string.Empty);
            fieldmap[0].Add("terins", data != null && data.Patient != null && data.Patient.TertiaryInsuranceName.IsNotNullOrEmpty() ? data.Patient.TertiaryInsuranceName.Clean() : string.Empty);
            fieldmap[0].Add("terinsnum", data != null && data.Patient != null && data.Patient.TertiaryHealthPlanId.IsNotNullOrEmpty() ? data.Patient.TertiaryHealthPlanId.Clean() : string.Empty);
            fieldmap[0].Add("ecname", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 0 && data.Patient.EmergencyContacts[0].DisplayName.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[0].DisplayName.Clean() : string.Empty);
            fieldmap[0].Add("ecphone", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 0 && data.Patient.EmergencyContacts[0].PrimaryPhone.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[0].PrimaryPhone.ToPhone().Clean() : string.Empty);
            fieldmap[0].Add("ecralation", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 0 && data.Patient.EmergencyContacts[0].Relationship.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[0].Relationship.Clean() : string.Empty);
            fieldmap[0].Add("ecname2", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 1 && data.Patient.EmergencyContacts[1].DisplayName.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[1].DisplayName.Clean() : string.Empty);
            fieldmap[0].Add("ecphone2", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 1 && data.Patient.EmergencyContacts[1].PrimaryPhone.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[1].PrimaryPhone.ToPhone().Clean() : string.Empty);
            fieldmap[0].Add("ecralation2", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 1 && data.Patient.EmergencyContacts[1].Relationship.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[1].Relationship.Clean() : string.Empty);
            fieldmap[0].Add("ecname3", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 2 && data.Patient.EmergencyContacts[2].DisplayName.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[2].DisplayName.Clean() : string.Empty);
            fieldmap[0].Add("ecphone3", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 2 && data.Patient.EmergencyContacts[2].PrimaryPhone.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[2].PrimaryPhone.ToPhone().Clean() : string.Empty);
            fieldmap[0].Add("ecralation3", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 2 && data.Patient.EmergencyContacts[2].Relationship.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[2].Relationship.Clean() : string.Empty);
            fieldmap[0].Add("ecname4", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 3 && data.Patient.EmergencyContacts[3].DisplayName.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[3].DisplayName.Clean() : string.Empty);
            fieldmap[0].Add("ecphone4", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 3 && data.Patient.EmergencyContacts[3].PrimaryPhone.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[3].PrimaryPhone.ToPhone().Clean() : string.Empty);
            fieldmap[0].Add("ecralation4", data != null && data.Patient != null && data.Patient.EmergencyContacts.Count > 3 && data.Patient.EmergencyContacts[3].Relationship.IsNotNullOrEmpty() ? data.Patient.EmergencyContacts[3].Relationship.Clean() : string.Empty);
            fieldmap[0].Add("allergies", data != null && data.Allergies.IsNotNullOrEmpty() ? data.Allergies : string.Empty);
            fieldmap[0].Add("pharm", data != null && data.Patient != null && data.Patient.PharmacyName.IsNotNullOrEmpty() ? data.Patient.PharmacyName : string.Empty);
            fieldmap[0].Add("pharmphone", data != null && data.Patient != null && data.Patient.PharmacyPhone.IsNotNullOrEmpty() ? data.Patient.PharmacyPhone.ToPhone() : string.Empty);
            fieldmap[0].Add("pridiag", assessment != null && assessment.ContainsKey("M1020PrimaryDiagnosis") && assessment["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? assessment["M1020PrimaryDiagnosis"].Answer : string.Empty);
            fieldmap[0].Add("secdiag", assessment != null && assessment.ContainsKey("M1022PrimaryDiagnosis1") && assessment["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? assessment["M1022PrimaryDiagnosis1"].Answer : string.Empty);
            fieldmap[0].Add("clinician", data != null && data.Clinician.IsNotNullOrEmpty() ? data.Clinician : string.Empty);
            fieldmap[0].Add("caseman", data != null && data.Patient != null && data.Patient.CaseManagerName.IsNotNullOrEmpty() ? data.Patient.CaseManagerName : string.Empty);
            fieldmap[0].Add("startofcare", data != null && data.Patient != null && data.Patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? data.Patient.StartOfCareDateFormatted : string.Empty);
            fieldmap[0].Add("freq", data != null && data.Frequencies.IsNotNullOrEmpty() ? data.Frequencies : string.Empty);
            fieldmap[0].Add("phys", data != null && data.Physician != null && data.Physician.DisplayName.IsNotNullOrEmpty() ? data.Physician.DisplayName : string.Empty);
            fieldmap[0].Add("physaddress", (
                data != null && data.Physician != null ?
                    (data.Physician.AddressLine1.IsNotNullOrEmpty() ? data.Physician.AddressLine1 : string.Empty) +
                    (data.Physician.AddressLine2.IsNotNullOrEmpty() ? data.Physician.AddressLine2 + "\n" : "\n") +
                    (data.Physician.AddressCity.IsNotNullOrEmpty() ? data.Physician.AddressCity + ", " : string.Empty) +
                    (data.Physician.AddressStateCode.IsNotNullOrEmpty() ? data.Physician.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                    (data.Physician.AddressZipCode.IsNotNullOrEmpty() ? data.Physician.AddressZipCode : string.Empty)
                : string.Empty));
            fieldmap[0].Add("physphone", data != null && data.Physician != null && data.Physician.PhoneWork.IsNotNullOrEmpty() ? data.Physician.PhoneWork.ToPhone() : string.Empty);
            fieldmap[0].Add("physfax", data != null && data.Physician != null && data.Physician.FaxNumber.IsNotNullOrEmpty() ? data.Physician.FaxNumber.ToPhone() : string.Empty);
            fieldmap[0].Add("physnpi", data != null && data.Physician != null && data.Physician.NPI.IsNotNullOrEmpty() ? data.Physician.NPI : string.Empty);
            fieldmap[0].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower() + ", " : string.Empty) + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower() + " " : string.Empty) + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : string.Empty);
            var serviceText = string.Empty;
            string[] servicesRequired = data != null && data.Patient != null && data.Patient.ServicesRequired.IsNotNullOrEmpty() ? data.Patient.ServicesRequired.Split(';') : null;
            if (servicesRequired != null && servicesRequired.Length > 0) {
                foreach (var service in servicesRequired) {
                    switch (service) {
                        case "0": serviceText += "SNV, "; break;
                        case "1": serviceText += "HHA, "; break;
                        case "2": serviceText += "PT, "; break;
                        case "3": serviceText += "OT, "; break;
                        case "4": serviceText += "ST, "; break;
                        case "5": serviceText += "MSW, "; break;
                    }
                }
                if (serviceText.Length > 0) serviceText = serviceText.Substring(0, serviceText.Length - 2);
            }
            fieldmap[0].Add("services", serviceText);
            var dmeText = string.Empty;
            string[] DME = data != null && data.Patient != null && data.Patient.DME.IsNotNullOrEmpty() ? data.Patient.DME.Split(';') : null;
            if (DME != null && DME.Length > 0) {
                foreach (var dme in DME) {
                    switch (dme) {
                        case "0": dmeText += "Bedside Commode, "; break;
                        case "1": dmeText += "Cane, "; break;
                        case "2": dmeText += "Elevated Toilet Seat, "; break;
                        case "3": dmeText += "Grab Bars, "; break;
                        case "4": dmeText += "Hospital Bed, "; break;
                        case "5": dmeText += "Nebulizer, "; break;
                        case "6": dmeText += "Oxygen, "; break;
                        case "7": dmeText += "Tub/Shower Bench, "; break;
                        case "8": dmeText += "Walker, "; break;
                        case "9": dmeText += "Wheelchair, "; break;
                        case "10": dmeText += "Other, "; break;
                    }
                }
                if (dmeText.Length > 0) dmeText = dmeText.Substring(0, dmeText.Length - 2);
            }
            fieldmap[0].Add("dme", dmeText);
            this.SetFields(fieldmap);
        }
    }
}