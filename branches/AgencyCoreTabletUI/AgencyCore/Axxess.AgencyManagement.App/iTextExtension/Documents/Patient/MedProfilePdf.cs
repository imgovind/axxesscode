﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class MedProfilePdf : AxxessPdf
    {
        public MedProfilePdf(MedicationProfileViewData data)
        {
            this.SetType(PdfDocs.MedProfile);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(new float[] { 2.85F, 11.67F, 31.91F, 16.86F, 12.97F, 2.59F, 21.15F }, true) };
            var medications = data != null && data.MedicationProfile != null && data.MedicationProfile.Medication.IsNotNullOrEmpty() ? data.MedicationProfile.Medication.ToObject<List<Medication>>().OrderByDescending(m => m.StartDateSortable).ToList() : new List<Medication>();
            if (medications.Count > 0)
            {
                foreach (var medication in medications)
                {
                    if (medication.MedicationCategory == "Active")
                    {
                        AxxessCell ls = new AxxessCell(new float[] { 0, 3, 8, 3 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell date = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell med = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell route = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell freq = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell type = new AxxessCell(new float[] { 0, 4, 8, 4 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell clas = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        ls.AddElement(new AxxessCheckbox("", medication.IsLongStanding));
                        date.AddElement(new Chunk(medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty, fonts[0]));
                        med.AddElement(new Chunk(medication.MedicationDosage, fonts[0]));
                        route.AddElement(new Chunk(medication.Route, fonts[0]));
                        freq.AddElement(new Chunk(medication.Frequency, fonts[0]));
                        type.AddElement(new Chunk(medication.MedicationType.Value, fonts[0]));
                        clas.AddElement(new Chunk(medication.Classification, fonts[0]));
                        content[0].AddCell(ls);
                        content[0].AddCell(date);
                        content[0].AddCell(med);
                        content[0].AddCell(route);
                        content[0].AddCell(freq);
                        content[0].AddCell(type);
                        content[0].AddCell(clas);
                    }
                }
                this.SetContent(content);
            }
            else this.SetContent(new IElement[] { new Chunk("") });
            this.SetMargins(new float[] { 265, 28.3F, 90.5F, 28.3F });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1 : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2 + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap[0].Add("episode", (data != null && data.StartDate.IsValid() ? data.StartDate.ToShortDateString() : "") + " - " + (data != null && data.EndDate.IsValid() ? data.EndDate.ToShortDateString() : ""));
            fieldmap[0].Add("pridiagnosis", data != null && data.Questions != null && data.Questions.ContainsKey("M1020PrimaryDiagnosis") ? data.Questions["M1020PrimaryDiagnosis"].Answer : "");
            fieldmap[0].Add("secdiagnosis", data != null && data.Questions != null && data.Questions.ContainsKey("M1022PrimaryDiagnosis1") ? data.Questions["M1022PrimaryDiagnosis1"].Answer : "");
            fieldmap[0].Add("physician", data != null && data.PhysicianDisplayName.IsNotNullOrEmpty() ? data.PhysicianDisplayName : "");
            fieldmap[0].Add("pharmacy", data != null && data.PharmacyName.IsNotNullOrEmpty() ? data.PharmacyName : "");
            fieldmap[0].Add("pharmphone", data != null && data.PharmacyPhone.IsNotNullOrEmpty() ? data.PharmacyPhone.ToPhone() : "");
            fieldmap[0].Add("allergies", data != null && data.Allergies.IsNotNullOrEmpty() ? data.Allergies : "");
            fieldmap[0].Add("sign", data != null && data.SignatureName.IsNotNullOrEmpty() ? data.SignatureName : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : "");
            fieldmap[1].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            this.SetFields(fieldmap);
        }

        public MedProfilePdf(MedicationProfileSnapshotViewData data)
        {
            this.SetType(PdfDocs.MedProfile);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(new float[] { 2.85F, 11.67F, 31.91F, 16.86F, 12.97F, 2.59F, 21.15F }, true) };
            var medications = data != null && data.MedicationProfile != null && data.MedicationProfile.Medication.IsNotNullOrEmpty() ? data.MedicationProfile.Medication.ToObject<List<Medication>>().OrderByDescending(m => m.StartDateSortable).ToList() : new List<Medication>();
            if (medications.Count > 0)
            {
                foreach (var medication in medications)
                {
                    if (medication.MedicationCategory == "Active")
                    {
                        AxxessCell ls = new AxxessCell(new float[] { 0, 3, 8, 3 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell date = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell med = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell route = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell freq = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell type = new AxxessCell(new float[] { 0, 4, 8, 4 }, new float[] { 0, 0, .5F, 0 });
                        AxxessCell clas = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        ls.AddElement(new AxxessCheckbox("", medication.IsLongStanding));
                        date.AddElement(new Chunk(medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty, fonts[0]));
                        med.AddElement(new Chunk(medication.MedicationDosage, fonts[0]));
                        route.AddElement(new Chunk(medication.Route, fonts[0]));
                        freq.AddElement(new Chunk(medication.Frequency, fonts[0]));
                        type.AddElement(new Chunk(medication.MedicationType.Value, fonts[0]));
                        clas.AddElement(new Chunk(medication.Classification, fonts[0]));
                        content[0].AddCell(ls);
                        content[0].AddCell(date);
                        content[0].AddCell(med);
                        content[0].AddCell(route);
                        content[0].AddCell(freq);
                        content[0].AddCell(type);
                        content[0].AddCell(clas);
                    }
                }
                this.SetContent(content);
            }
            else this.SetContent(new IElement[] { new Chunk("") });
            this.SetMargins(new float[] { 170, 28.3F, 120, 28.3F });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1 : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2 + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap[0].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            fieldmap[0].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("physician", data != null && data.PhysicianName.IsNotNullOrEmpty() ? data.PhysicianName : "");
            fieldmap[0].Add("episode", (data != null && data.StartDate.IsValid() ? data.StartDate.ToShortDateString() : "") + " - " + (data != null && data.EndDate.IsValid() ? data.EndDate.ToShortDateString() : ""));
            fieldmap[0].Add("pharmacy", data != null && data.PharmacyName.IsNotNullOrEmpty() ? data.PharmacyName : "");
            fieldmap[0].Add("pharmphone", data != null && data.PharmacyPhone.IsNotNullOrEmpty() ? data.PharmacyPhone.ToPhone() : "");
            fieldmap[0].Add("pridiagnosis", data != null && data.PrimaryDiagnosis.IsNotNullOrEmpty() ? data.PrimaryDiagnosis : "");
            fieldmap[0].Add("secdiagnosis", data != null && data.SecondaryDiagnosis.IsNotNullOrEmpty() ? data.SecondaryDiagnosis : "");
            fieldmap[0].Add("allergies", data != null && data.Allergies.IsNotNullOrEmpty() ? data.Allergies : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : "");
            this.SetFields(fieldmap);
        }
    }
}