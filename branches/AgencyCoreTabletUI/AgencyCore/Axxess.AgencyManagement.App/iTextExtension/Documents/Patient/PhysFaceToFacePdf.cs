﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    class PhysFaceToFacePdf : AxxessPdf {
        private PhysFaceToFaceXml xml;
        public PhysFaceToFacePdf(FaceToFaceEncounter data) {
            this.xml = new PhysFaceToFaceXml(data);
            this.SetType(PdfDocs.PhysFaceToFace);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans); 
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            this.SetContent(new IElement[1] { new AxxessContentSection(this.xml.GetLayout()[0], fonts, true, 10, this.IsOasis) });
            float[] margins = new float[] { 205, 35, 70, 35 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            if (location == null) location = data.Agency.GetMainOffice();
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1 : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2 + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap[0].Add("patient", (data != null && data.Patient != null ? (data.Patient.AddressLine1.IsNotNullOrEmpty() ? data.Patient.AddressLine1 + "\n" : "") + (data.Patient.AddressLine2.IsNotNullOrEmpty() ? data.Patient.AddressLine2 + "\n" : "") + (data.Patient.AddressCity.IsNotNullOrEmpty() ? data.Patient.AddressCity + ", " : "") + (data.Patient.AddressStateCode.IsNotNullOrEmpty() ? data.Patient.AddressStateCode + "  " : "") + (data.Patient.AddressZipCode.IsNotNullOrEmpty() ? data.Patient.AddressZipCode + "\n" : "") + (data.Patient.PhoneHome.IsNotNullOrEmpty() ? data.Patient.PhoneHome.ToPhone() + "\n" : "") + (data.Patient.MedicareNumber.IsNotNullOrEmpty() ? "HIC: " + data.Patient.MedicareNumber : "") : ""));
            fieldmap[0].Add("physician", (data != null && data.Physician != null ? (data.Physician.AddressLine1.IsNotNullOrEmpty() ? data.Physician.AddressLine1 : "") + (data.Physician.AddressLine2.IsNotNullOrEmpty() ? ", " + data.Physician.AddressLine2 + "\n" : "\n") + (data.Physician.AddressCity.IsNotNullOrEmpty() ? data.Physician.AddressCity + ", " : "") + (data.Physician.AddressStateCode.IsNotNullOrEmpty() ? data.Physician.AddressStateCode + "  " : "") + (data.Physician.AddressZipCode.IsNotNullOrEmpty() ? data.Physician.AddressZipCode + "\n" : "") + (data.Physician.PhoneWork.IsNotNullOrEmpty() ? "Phone: " + data.Physician.PhoneWork.ToPhone() : "") + (data.Physician.FaxNumber.IsNotNullOrEmpty() ? " | Fax: " + data.Physician.FaxNumber.ToPhone() + "\n" : "\n") + (data.Physician.NPI.IsNotNullOrEmpty() ? "NPI: " + data.Physician.NPI : "") : ""));
            fieldmap[0].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("dob", data != null && data.Patient != null && data.Patient.DOBFormatted.IsNotNullOrEmpty() ? data.Patient.DOBFormatted : string.Empty);
            fieldmap[0].Add("socdate", data != null && data.Patient != null && data.Patient.StartofCareDate.IsValid() && data.Patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? data.Patient.StartOfCareDateFormatted : string.Empty);
            fieldmap[0].Add("episode", (data != null && data.EpisodeStartDate.IsValidDate() ? data.EpisodeStartDate + "—" : string.Empty) + (data != null && data.EpisodeEndDate.IsValidDate() ? data.EpisodeEndDate : string.Empty));
            fieldmap[0].Add("physsign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : string.Empty);
            fieldmap[0].Add("physsigndate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : string.Empty);
            fieldmap[1].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            fieldmap[1].Add("physicianname", data != null && data.Physician != null && data.Physician.DisplayName.IsNotNullOrEmpty() ? data.Physician.DisplayName : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}