﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class PayrollSummaryDetailsPdf : AxxessPdf {
        private List<PayrollDetail> data;
        private Agency agency;
        private DateTime startDate;
        private DateTime endDate;
        public PayrollSummaryDetailsPdf(PayrollDetail data, Agency agency, DateTime startDate, DateTime endDate) {
            this.data = new List<PayrollDetail>();
            this.data.Add(data);
            this.agency = agency;
            this.startDate = startDate;
            this.endDate = endDate;
            this.BuildLayout();
        }
        public PayrollSummaryDetailsPdf(List<PayrollDetail> data, Agency agency, DateTime startDate, DateTime endDate) {
            this.data = data;
            this.agency = agency;
            this.startDate = startDate;
            this.endDate = endDate;
            this.BuildLayout();
        }
        private void BuildLayout() {
            this.SetType(PdfDocs.PayrollSummary);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 8F;
            this.SetFonts(fonts);
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            AxxessTable[] content = new AxxessTable[this.data.Count];
            int counter = 0;
            foreach (PayrollDetail table in data) {
                AxxessTable usertable = new AxxessTable(new float[] { 1, 1, 2, 3, 2, .5F, 1, 2 }, false);
                AxxessCell user = new AxxessCell(padding, borders);
                user.Colspan = 8;
                user.AddElement(new AxxessTitle(table.Name, fonts[1]));
                usertable.AddCell(user);
                usertable.AddCell("Schedule Date", fonts[1], padding, borders);
                usertable.AddCell("Visit Date", fonts[1], padding, borders);
                usertable.AddCell("Patient Name", fonts[1], padding, borders);
                usertable.AddCell("Task", fonts[1], padding, borders);
                usertable.AddCell("Time", fonts[1], padding, borders);
                usertable.AddCell("Min.", fonts[1], padding, borders);
                usertable.AddCell("Mileage", fonts[1], padding, borders);
                usertable.AddCell("Status", fonts[1], padding, borders);
                usertable.HeaderRows = 2;
                var totalTime=0;
                foreach (UserVisit row in table.Visits) {
                    usertable.AddCell(row.ScheduleDate, fonts[0], padding, borders);
                    usertable.AddCell(row.VisitDate, fonts[0], padding, borders);
                    usertable.AddCell(row.PatientName, fonts[0], padding, borders);
                    usertable.AddCell(row.TaskName, fonts[0], padding, borders);
                    usertable.AddCell(row.TimeIn.IsNotNullOrEmpty() && row.TimeOut.IsNotNullOrEmpty() ? string.Format("{0} - {1}", row.TimeIn, row.TimeOut) : string.Empty, fonts[0], padding, borders);
                    usertable.AddCell(row.MinSpent.ToString(), fonts[0], padding, borders);
                    usertable.AddCell(row.AssociatedMileage, fonts[0], padding, borders);
                    usertable.AddCell(row.StatusName, fonts[0], padding, borders);
                    totalTime+=row.MinSpent;
                }
                content[counter++] = usertable;
                AxxessCell userFooter = new AxxessCell(padding, borders);
                userFooter.Colspan = 8;
                userFooter.AddElement(new AxxessTitle(string.Format("Total Number Of Payroll Detail: {0}, Total Time for this employee: {1}", table.Visits.Count, string.Format(" {0} min = {1:#0.00} hour(s) ", totalTime, (double)totalTime / 60)), fonts[1]));
                usertable.AddCell(userFooter);

            }
            this.SetContent(content);
            float[] margins = new float[] { 100, 28, 28, 28 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", (
                data != null && agency != null ?
                    (agency.Name.IsNotNullOrEmpty() ? agency.Name + "\n" : "") +
                    (agency.MainLocation != null ?
                        (agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? agency.MainLocation.AddressLine1 : "") +
                        (agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? agency.MainLocation.AddressLine2 + "\n" : "\n") +
                        (agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? agency.MainLocation.AddressCity + ", " : "") +
                        (agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? agency.MainLocation.AddressStateCode.ToString().ToUpper() + "  " : "") +
                        (agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? agency.MainLocation.AddressZipCode : "") +
                        (agency.MainLocation.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + agency.MainLocation.PhoneWorkFormatted : "") +
                        (agency.MainLocation.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + agency.MainLocation.FaxNumberFormatted : "")
                    : "")
                : ""));
            fieldmap[0].Add("startdate", this.startDate.ToShortDateString());
            fieldmap[0].Add("enddate", this.endDate.ToShortDateString());
            this.SetFields(fieldmap);
        }
    }
}