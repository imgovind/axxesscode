﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.App.Services;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    class UB04Pdf : AxxessPdf {
        protected readonly IBillingService billingService;

        public UB04Pdf(UBOFourViewData data, IBillingService billingService) {
            this.billingService = billingService;
            this.SetType(PdfDocs.UB04);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 8F;
            this.SetFonts(fonts);
            this.SetContent(this.BuildContent(data));
            float[] margins = new float[] { 213, 12, 312, 12.5F };
            this.SetMargins(margins);
            this.SetFields(this.BuildFieldMap(data));
        }

        protected virtual List<Dictionary<String, String>> BuildFieldMap(UBOFourViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<String, String>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("1-1", data.Agency != null && data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToUpper() : string.Empty);
            fieldmap[0].Add("1-2", data.AgencyLocation != null && data.AgencyLocation.AddressFirstRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressFirstRow.ToUpper() : string.Empty);
            fieldmap[0].Add("1-3", data.AgencyLocation != null && data.AgencyLocation.AddressSecondRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressSecondRow.ToUpper() : string.Empty);
            fieldmap[0].Add("3aPatientCntlNum", data.Claim != null && data.Claim.MedicareNumber.IsNotNullOrEmpty() ? data.Claim.MedicareNumber.ToUpper() : string.Empty);
            fieldmap[0].Add("3bMedRecNum", data.Claim != null && data.Claim.PatientIdNumber.IsNotNullOrEmpty() ? data.Claim.PatientIdNumber.ToUpper() : string.Empty);
            fieldmap[0].Add("4BillType", data.Claim != null && data.Claim.Type.IsNotNullOrEmpty() ? data.Claim.Type : string.Empty);
            fieldmap[0].Add("5FedTaxNum", data.Agency != null && data.Agency.TaxId.IsNotNullOrEmpty() ? data.Agency.TaxId : string.Empty);
            fieldmap[0].Add("6StatementStart", data.Claim != null && data.Claim.EpisodeStartDate.IsValid() ? data.Claim.EpisodeStartDate.ToShortDateString() : string.Empty);
            fieldmap[0].Add("6StatementEnd", data.Claim.IsHMO?data.Claim.EpisodeEndDate.ToString("MM/dd/yyyy"): data.Claim != null && data.Claim.Type == "322" && data.Claim.EpisodeStartDate.IsValid() ? data.Claim.EpisodeStartDate.ToShortDateString() : data.Claim != null && data.Claim.Type == "329" && data.Claim.EpisodeEndDate.IsValid() ? data.Claim.EpisodeEndDate.ToShortDateString() : string.Empty);
            fieldmap[0].Add("8aPatientName", data.Claim != null && data.Claim.LastName.IsNotNullOrEmpty() ? data.Claim.LastName.ToUpper() : string.Empty);
            fieldmap[0].Add("8bPatientName", data.Claim != null && data.Claim.FirstName.IsNotNullOrEmpty() ? data.Claim.FirstName.ToUpper() : string.Empty);
            fieldmap[0].Add("9aPatientAddress", data.Claim != null && data.Claim.AddressLine1.IsNotNullOrEmpty() ? data.Claim.AddressLine1.ToUpper() + (data.Claim.AddressLine2.IsNotNullOrEmpty() ? " " + data.Claim.AddressLine2.ToUpper() : string.Empty) : string.Empty);
            fieldmap[0].Add("9bPatientCity", data.Claim != null && data.Claim.AddressCity.IsNotNullOrEmpty() ? data.Claim.AddressCity.ToUpper() : string.Empty);
            fieldmap[0].Add("9cPatientState", data.Claim != null && data.Claim.AddressStateCode.IsNotNullOrEmpty() ? data.Claim.AddressStateCode.ToUpper() : string.Empty);
            fieldmap[0].Add("9dPatientZip", data.Claim != null && data.Claim.AddressZipCode.IsNotNullOrEmpty() ? data.Claim.AddressZipCode.ToUpper() : string.Empty);
            fieldmap[0].Add("10PatientBirthdate", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("MMddyyyy") : string.Empty);
            fieldmap[0].Add("11PatientGender", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() ? data.Claim.Gender.Substring(0, 1).ToUpper() : string.Empty);
            fieldmap[0].Add("12AdmissionDate", data.Claim != null && data.Claim.StartofCareDate.IsValid() ? data.Claim.StartofCareDate.ToShortDateString() : string.Empty);
            fieldmap[0].Add("15AdmissionSource", data.Claim != null && data.Claim.AdmissionSource.IsNotNullOrEmpty() ? data.Claim.AdmissionSource : string.Empty);
            fieldmap[0].Add("17STAT", data.Claim != null ? data.Claim.Status.ToString().PadLeft(2, '0') : string.Empty);
            if (data.Claim != null && data.Claim.ConditionCodes.IsNotNullOrEmpty()) {
                var conditionCodes = XElement.Parse(data.Claim.ConditionCodes);
                fieldmap[0].Add("18ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : string.Empty);
                fieldmap[0].Add("19ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : string.Empty);
                fieldmap[0].Add("20ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : string.Empty);
                fieldmap[0].Add("21ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : string.Empty);
                fieldmap[0].Add("22ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : string.Empty);
                fieldmap[0].Add("23ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : string.Empty);
                fieldmap[0].Add("24ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : string.Empty);
                fieldmap[0].Add("25ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : string.Empty);
                fieldmap[0].Add("26ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : string.Empty);
                fieldmap[0].Add("27ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : string.Empty);
                fieldmap[0].Add("28ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : string.Empty);
            }
            fieldmap[0].Add("38Payor", (data.Claim != null && data.Claim.PayorName.IsNotNullOrEmpty() ? data.Claim.PayorName : string.Empty) + (data.Claim.IsHMO ? ( "\n" + data.Claim.PayorAddressLine1 + "\n" + data.Claim.PayorAddressLine2) : string.Empty));
            fieldmap[0].Add("39aValueCode", "61");
            fieldmap[0].Add("39aValueCodeAmount", data.Claim.CBSA.IsNotNullOrEmpty() ? data.Claim.CBSA + ".00" : string.Empty);
            fieldmap[0].Add("CreationDate", data.Claim != null && data.Claim.Created.Date > DateTime.MinValue.Date ? data.Claim.Created.ToString("MMddyyyy") : DateTime.Now.ToString("MMddyyyy"));
            fieldmap[0].Add("Total", string.Format("{0:0.00}", this.CalculateTotal(data)));
            fieldmap[0].Add("50aPayerName", data.Claim != null && data.Claim.PayorName.IsNotNullOrEmpty() ? data.Claim.PayorName : string.Empty);
            if (data.Claim != null && data.Claim.IsHMO) fieldmap[0].Add("51aHealthPlanId", data.Claim != null && data.Claim.HealthPlanId.IsNotNullOrEmpty() ? data.Claim.HealthPlanId : string.Empty);
            fieldmap[0].Add("56Npi", data.Agency != null && data.Agency.NationalProviderNumber.IsNotNullOrEmpty() ? data.Agency.NationalProviderNumber : string.Empty);
            if (data.Claim != null && data.Claim.IsHMO) {
                fieldmap[0].Add("57a", data.Claim != null && data.Claim.ProviderId.IsNotNullOrEmpty() ? data.Claim.ProviderId : string.Empty);
                fieldmap[0].Add("57bOther", data.Claim != null && data.Claim.OtherProviderId.IsNotNullOrEmpty() ? data.Claim.OtherProviderId : string.Empty);
                fieldmap[0].Add("57cPrvId", data.Claim != null && data.Claim.ProviderSubscriberId.IsNotNullOrEmpty() ? data.Claim.ProviderSubscriberId : string.Empty);
            }
            fieldmap[0].Add("58aInsuredName", data.Claim != null && data.Claim.LastName.IsNotNullOrEmpty() ? data.Claim.LastName.ToUpper() + (data.Claim.FirstName.IsNotNullOrEmpty() ? ", " + data.Claim.FirstName.ToUpper() : string.Empty) : string.Empty);
            fieldmap[0].Add("60aInsuredUniqueId", data.Claim != null && data.Claim.MedicareNumber.IsNotNullOrEmpty() ? data.Claim.MedicareNumber : string.Empty);
            if (data.Claim != null && data.Claim.IsHMO) fieldmap[0].Add("63aTreatmentAuthCode", data.Claim != null && data.Claim.PayorAuthorizationCode.IsNotNullOrEmpty() ? data.Claim.PayorAuthorizationCode : string.Empty);
            else fieldmap[0].Add("63aTreatmentAuthCode", data.Claim != null && data.Claim.ClaimKey.IsNotNullOrEmpty() ? data.Claim.ClaimKey : string.Empty);
            if (data.Claim != null && data.Claim.DiagnosisCode.IsNotNullOrEmpty()) {
                var diagnosis = XElement.Parse(data.Claim.DiagnosisCode);
                fieldmap[0].Add("67", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : string.Empty);
                fieldmap[0].Add("67a", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : string.Empty);
                fieldmap[0].Add("67b", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : string.Empty);
                fieldmap[0].Add("67c", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : string.Empty);
                fieldmap[0].Add("67d", diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value : string.Empty);
                fieldmap[0].Add("67e", diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value : string.Empty);
                fieldmap[0].Add("67f", diagnosis != null && diagnosis.Element("code7") != null ? diagnosis.Element("code7").Value : string.Empty);
                fieldmap[0].Add("67g", diagnosis != null && diagnosis.Element("code8") != null ? diagnosis.Element("code8").Value : string.Empty);
                fieldmap[0].Add("67h", diagnosis != null && diagnosis.Element("code9") != null ? diagnosis.Element("code9").Value : string.Empty);
                fieldmap[0].Add("67i", diagnosis != null && diagnosis.Element("code10") != null ? diagnosis.Element("code10").Value : string.Empty);
                fieldmap[0].Add("67j", diagnosis != null && diagnosis.Element("code11") != null ? diagnosis.Element("code11").Value : string.Empty);
                fieldmap[0].Add("67k", diagnosis != null && diagnosis.Element("code12") != null ? diagnosis.Element("code12").Value : string.Empty);
                fieldmap[0].Add("67l", diagnosis != null && diagnosis.Element("code13") != null ? diagnosis.Element("code13").Value : string.Empty);
                fieldmap[0].Add("67m", diagnosis != null && diagnosis.Element("code14") != null ? diagnosis.Element("code14").Value : string.Empty);
                fieldmap[0].Add("67n", diagnosis != null && diagnosis.Element("code15") != null ? diagnosis.Element("code15").Value : string.Empty);
                fieldmap[0].Add("67o", diagnosis != null && diagnosis.Element("code16") != null ? diagnosis.Element("code16").Value : string.Empty);
                fieldmap[0].Add("67p", diagnosis != null && diagnosis.Element("code17") != null ? diagnosis.Element("code17").Value : string.Empty);
                fieldmap[0].Add("67q", diagnosis != null && diagnosis.Element("code18") != null ? diagnosis.Element("code18").Value : string.Empty);
            }
            fieldmap[0].Add("76AttendingNpi", data.Claim != null && data.Claim.PhysicianNPI.IsNotNullOrEmpty() ? data.Claim.PhysicianNPI : string.Empty);
            fieldmap[0].Add("76AttendingLastName", data.Claim != null && data.Claim.PhysicianLastName.IsNotNullOrEmpty() ? data.Claim.PhysicianLastName.ToUpper() : string.Empty);
            fieldmap[0].Add("76AttendingFirstName", data.Claim != null && data.Claim.PhysicianFirstName.IsNotNullOrEmpty() ? data.Claim.PhysicianFirstName.ToUpper() : string.Empty);
            fieldmap[0].Add("80Remarks", data.Claim != null && data.Claim.Remark.IsNotNullOrEmpty() ? data.Claim.Remark : string.Empty);
            return fieldmap;
        }

        protected virtual IElement[] BuildContent(UBOFourViewData data) {
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(new float[] { 2.4F, 12.3F, 7.3F, 3.5F, 3.9F, 4.9F, 4.9F, 1 }, false) };
            Font font = AxxessPdf.sans;
            font.Size = 8;
            float[] padding = new float[] { 0, 1, .05F, 1 }, borders = new float[] { 0, 0, 0, 0 }, moneypad = new float[] { 0, 9, .05F, 1 };
            content[0].AddCell("0023", font, padding, borders);
            content[0].AddCell("HOME HEALTH SERVICES", font, padding, borders);
            content[0].AddCell(data.Claim != null && data.Claim.HippsCode.IsNotNullOrEmpty() ? data.Claim.HippsCode : string.Empty, font, padding, borders);
            content[0].AddCell(data.Claim != null && data.Claim.FirstBillableVisitDate.IsValid() ? data.Claim.FirstBillableVisitDate.ToString("MMddyyyy") : string.Empty, font, padding, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            content[0].AddCell("0.00", font, "Right", moneypad, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            if (data.Claim.Type == "329") {
                PPSStandard ppsStandard;
                double supply = 0;
                if (data.Claim != null && data.Claim.SupplyTotal <= 0 && data.Claim.HippsCode.IsNotNullOrEmpty() && data.Claim.HippsCode.Length == 5) supply += billingService.GetSupplyReimbursement(data.Claim.HippsCode[4], data.Claim.EpisodeStartDate.Year, out ppsStandard);
                else if (data.Claim != null && data.Claim.SupplyTotal > 0) supply += data.Claim.SupplyTotal;
                content[0].AddCell("0272", font, padding, borders);
                content[0].AddCell("SERVICE SUPPLIES", font, padding, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(data.Claim != null && data.Claim.EpisodeStartDate.IsValid() ? data.Claim.EpisodeStartDate.ToString("MMddyyyy") : string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(String.Format("{0:0.00}", supply), font, "Right", moneypad, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                var unitType = -1;
                var BillInfo = this.billingService.GetInsuranceUnit(data.Claim != null ? data.Claim.PrimaryInsuranceId : 0,out unitType);
                var visits = data.Claim != null && data.Claim.VerifiedVisit.IsNotNullOrEmpty() ? data.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate()).OrderBy(s => s.EventDate.ToDateTime().Date).ToList() : new List<ScheduleEvent>();
                var isHMO = data.Claim != null ? data.Claim.IsHMO : false;
                foreach (var visit in visits)
                {
                    visit.Discipline = visit.Discipline == "Nursing" ? "SN" : visit.Discipline;
                    var code = visit.GIdentify();
                    var amount = BillInfo.ContainsKey(code) && BillInfo[code].Amount.IsNotNullOrEmpty() && BillInfo[code].Amount.IsDouble() ? BillInfo[code].Amount.ToDouble() : 0.00;
                    var unit = isHMO ? (unitType == 1 ? 1 : (unitType == 2 ? (int)Math.Ceiling((double)visit.MinSpent / 60) : (unitType == 3 ? (int)Math.Ceiling((double)visit.MinSpent / 15) : 0))) : visit.Unit;
                    content[0].AddCell(code.IsNotNullOrEmpty() && BillInfo.ContainsKey(code) && BillInfo[code] != null ? BillInfo[code].CodeOne : string.Empty, font, padding, borders);
                    content[0].AddCell(visit.DisciplineTaskName.IsNotNullOrEmpty() ? visit.DisciplineTaskName.ToUpper() : string.Empty, font, padding, borders);
                    content[0].AddCell(code.IsNotNullOrEmpty() && BillInfo.ContainsKey(code) && BillInfo[code] != null ? BillInfo[code].CodeTwo : string.Empty, font, padding, borders);
                    content[0].AddCell(visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToString("MMddyyyy") : string.Empty, font, padding, borders);
                    content[0].AddCell(unit.ToString(), font, padding, borders);
                    content[0].AddCell(string.Format("{0:#0.00}", isHMO ? unit * amount : amount), font, "Right", moneypad, borders);
                    content[0].AddCell(string.Empty, font, padding, borders);
                    content[0].AddCell(string.Empty, font, padding, borders);
                }
            }
            return content;
        }

        protected virtual double CalculateTotal(UBOFourViewData data)
        {
            double total = 0;
            if (data.Claim.Type == "329")
            {
                PPSStandard ppsStandard;
                if (data.Claim != null && data.Claim.SupplyTotal <= 0 && data.Claim.HippsCode.IsNotNullOrEmpty() && data.Claim.HippsCode.Length == 5) total += billingService.GetSupplyReimbursement(data.Claim.HippsCode[4], data.Claim.EpisodeStartDate.Year, out ppsStandard);
                else if (data.Claim != null && data.Claim.SupplyTotal > 0) total += data.Claim.SupplyTotal;
                var unitType = -1;
                var isHMO = data.Claim != null ? data.Claim.IsHMO : false;
                var billInfo = this.billingService.GetInsuranceUnit(data.Claim != null ? data.Claim.PrimaryInsuranceId : 0, out unitType);
                var visits = data.Claim != null && data.Claim.VerifiedVisit.IsNotNullOrEmpty() ? data.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate()).OrderBy(s => s.EventDate.ToDateTime().Date).ToList() : new List<ScheduleEvent>();
                foreach (var visit in visits)
                {
                    visit.Discipline = visit.Discipline == "Nursing" ? "SN" : visit.Discipline;
                    var code = visit.GIdentify();
                    var amount = code.IsNotNullOrEmpty() && billInfo.ContainsKey(code) && billInfo[code].Amount.IsNotNullOrEmpty() && billInfo[code].Amount.IsDouble() ? billInfo[code].Amount.ToDouble() : 0.00;
                    var unit = isHMO ? (unitType == 1 ? 1 : (unitType == 2 ? (int)Math.Ceiling((double)visit.MinSpent / 60) : (unitType == 3 ? (int)Math.Ceiling((double)visit.MinSpent / 15) : 0))) : visit.Unit;
                    total += isHMO ? unit * amount : amount;
                }
            }
            return total;
        }

        protected override void AddPageNumber(PdfContentByte swap, int pageNum, int numPages) {
            swap.BeginText();
            swap.SetFontAndSize(AxxessPdf.sans.BaseFont, 10);
            swap.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, pageNum.ToString(), 90, 302, 0);
            swap.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, numPages.ToString(), 135, 302, 0);
            swap.EndText();
        }
    }
}