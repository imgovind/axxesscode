﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    
    using iTextSharp.text;
    class BillingClaimsPdf : AxxessPdf {
        public BillingClaimsPdf(Bill data) {
            this.SetType(PdfDocs.BillingClaims);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            AxxessTable[] content = new AxxessTable[2];
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            content[0] = new AxxessTable(1, true);
            content[0].AddCell(new AxxessTitle("RAP(S)", fonts[1]), padding, borders);
            content[0].HeaderRows = 1;
            AxxessTable rapTable = new AxxessTable(new float[] { 3, 2, 3, 1, 1, 1 }, false);
            rapTable.AddCell("Patient Name", fonts[1], padding, borders);
            rapTable.AddCell("Patient ID", fonts[1], padding, borders);
            rapTable.AddCell("Episode Date", fonts[1], padding, borders);
            rapTable.AddCell("Oasis", fonts[1], padding, borders);
            rapTable.AddCell("Billable Visit", fonts[1], padding, borders);
            rapTable.AddCell("Verified", fonts[1], padding, borders);
            rapTable.HeaderRows = 1;
            foreach (var rap in data.RapClaims) {
                rapTable.AddCell(rap.LastName + ", " + rap.FirstName, fonts[0], padding, borders);
                rapTable.AddCell(rap.PatientIdNumber, fonts[0], padding, borders);
                rapTable.AddCell(rap.EpisodeStartDate.ToShortDateString() + " - " + rap.EpisodeEndDate.ToShortDateString(), fonts[0], padding, borders);
                rapTable.AddCell(new AxxessCheckbox(rap.IsOasisComplete, 14), none, borders);
                rapTable.AddCell(new AxxessCheckbox(rap.IsFirstBillableVisit, 14), none, borders);
                rapTable.AddCell(new AxxessCheckbox(rap.IsVerified, 14), none, borders);
            }
            content[0].AddCell(rapTable, none, none);
            content[1] = new AxxessTable(1);
            content[1].AddCell(new AxxessTitle("FINAL(S)", fonts[1]), padding, borders);
            content[1].HeaderRows = 1;
            AxxessTable finalTable = new AxxessTable(new float[] { 3, 2, 3, 1, 1, 1, 1.1F }, false);
            finalTable.AddCell("Patient Name", fonts[1], padding, borders);
            finalTable.AddCell("Patient ID", fonts[1], padding, borders);
            finalTable.AddCell("Episode Date", fonts[1], padding, borders);
            finalTable.AddCell("Rap", fonts[1], padding, borders);
            finalTable.AddCell("Visit", fonts[1], padding, borders);
            finalTable.AddCell("Order", fonts[1], padding, borders);
            finalTable.AddCell("Verified", fonts[1], padding, borders);
            finalTable.HeaderRows = 1;
            foreach (var final in data.FinalClaims) {
                finalTable.AddCell(final.LastName + ", " + final.FirstName, fonts[0], padding, borders);
                finalTable.AddCell(final.PatientIdNumber, fonts[0], padding, borders);
                finalTable.AddCell(final.EpisodeStartDate.ToShortDateString() + " - " + final.EpisodeEndDate.ToShortDateString(), fonts[0], padding, borders);
                finalTable.AddCell(new AxxessCheckbox(final.IsRapGenerated, 14), none, borders);
                finalTable.AddCell(new AxxessCheckbox(final.AreVisitsComplete, 14), none, borders);
                finalTable.AddCell(new AxxessCheckbox(final.AreOrdersComplete, 14), none, borders);
                finalTable.AddCell(new AxxessCheckbox(final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified, 14), none, borders);
            }
            content[1].AddCell(finalTable, none, none);
            this.SetContent(content);
            float[] margins = new float[] { 82, 28, 28, 28 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            if (data.Agency != null)
            {
                var location = data.Agency.GetBranch(data.BranchId);
             
                    fieldmap.Add(new Dictionary<String, String>() { });
                    fieldmap[0].Add("agency", (
                        data != null && data.Agency != null ?
                            (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name + "\n" : "") +
                            (location != null ?
                                (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1 : "") +
                                (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2 + "\n" : "\n") +
                                (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity + ", " : "") +
                                (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") +
                                (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : "") +
                                (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : "") +
                                (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "")
                            : "")
                        : ""));
            }
            this.SetFields(fieldmap);
        }
    }
}