﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing {
    using System;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using Axxess.Core.Extension;
    using System.Linq;
    using System.Text;
    abstract class XmlElement {
        protected XElement Node;
        protected BaseXml Xml;
        public XmlElement(BaseXml Xml, XElement Node) {
            this.Xml = Xml;
            this.Node = Node;
        }
        public virtual String GetJson() {
            return string.Empty;
        }
        protected String GetAttribute(String Attribute) {
            if (this.Node.Attribute("print" + Attribute) != null) return this.FormatString("print" + Attribute);
            else if (this.Node.Attribute(Attribute) != null) return this.FormatString(Attribute);
            return string.Empty;
        }
        protected int GetIntAttribute(String Attribute) {
            String Value = this.GetAttribute(Attribute);
            if (Value.IsNotNullOrEmpty() && Value.ToInteger() > 0) return Value.ToInteger();
            return 1;
        }
        protected int[] GetColWidths(int Cols) {
            String Value = this.GetAttribute("colwidths");
            if (Value.IsNotNullOrEmpty() && Value.IndexOf(",") > 0 && Value.Split(",".ToCharArray()).Count() == Cols) return Value.Split(",".ToCharArray()).Select(x => int.Parse(x.ToString())).ToArray();
            return new int[0];
        }
        protected List<XmlPrintSection> GetSections() {
            List<XmlPrintSection> Sections = new List<XmlPrintSection>();
            if (this.Node.HasElements && this.Node.Elements("section") != null) foreach (XElement section in this.Node.Elements("section")) if (this.ElementIsIncluded(section)) Sections.Add(new XmlPrintSection(this.Xml, section));
            return Sections;
        }
        protected List<XmlPrintQuestion> GetQuestions() {
            List<XmlPrintQuestion> Questions = new List<XmlPrintQuestion>();
            if (this.Node.HasElements && this.Node.Elements("question") != null) foreach (XElement question in this.Node.Elements("question")) if (this.ElementIsIncluded(question)) Questions.Add(new XmlPrintQuestion(this.Xml, question));
            return Questions;
        }
        protected List<XmlPrintOption> GetOptions() {
            List<XmlPrintOption> Options = new List<XmlPrintOption>();
            if (this.Node.HasElements && this.Node.Elements("option") != null) foreach (XElement option in this.Node.Elements("option")) if (this.ElementIsIncluded(option)) Options.Add(new XmlPrintOption(this.Xml, option));
            return Options;
        }
        protected String GetData() {
            if (this.Node.Attribute("printdata") != null && Regex.IsMatch(this.Node.Attribute("printdata").Value, "{[0-9]}")) return this.FormatString("printdata");
            else if (this.Node.Attribute("printdata") != null) return this.ImportData(this.Node.Attribute("data").Value);
            else if (this.Node.Attribute("data") != null && Regex.IsMatch(this.Node.Attribute("data").Value, "{[0-9]}")) return this.FormatString("data");
            else if (this.Node.Attribute("data") != null) return this.ImportData(this.Node.Attribute("data").Value);
            return string.Empty;
        }
        protected String CleanForJson(String inputString) {
            return inputString.ToString().Clean();
        }
        private bool ElementIsIncluded(XElement node) {
            return (node.HasAttributes || node.HasElements) && (this.Xml.GetType() == string.Empty || node.Attribute("membership") == null || node.Attribute("membership").Value.Split(',').Contains(this.Xml.GetType()));
        }
        private String ImportData(String Index) {
            return this.Xml.GetData(Index).Trim();
        }
        private String FormatString(String Attribute) {
            if (this.Node.Attribute(Attribute) != null && Regex.IsMatch(this.Node.Attribute(Attribute).Value, "{[0-9]}")) {
                String[] fieldArray = new String[this.Node.Elements("field").Count()];
                for (int j = 0; j < this.Node.Elements("field").Count(); j++) {
                    fieldArray[j] = this.ImportData(this.Node.Elements("field").ElementAt(j).Attribute("data").Value);
                    if (fieldArray[j].Length == 0 && this.Node.Elements("field").ElementAt(j).Attribute("length") != null && this.Node.Elements("field").ElementAt(j).Attribute("length").Value.ToInteger() > 0)
                        for (int k = 0; k < this.Node.Elements("field").ElementAt(j).Attribute("length").Value.ToInteger(); k++)
                            fieldArray[j] += BaseXml.BlankChar;
                }
                return String.Format(this.Node.Attribute(Attribute).Value, fieldArray);
            } else if (this.Node.Attribute(Attribute) != null && this.Node.Attribute(Attribute).Value.IsNotNullOrEmpty()) return this.Node.Attribute(Attribute).Value;
            else return string.Empty;
        }
    }
}