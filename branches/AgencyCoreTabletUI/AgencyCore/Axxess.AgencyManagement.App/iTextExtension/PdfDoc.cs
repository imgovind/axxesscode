﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.IO;
    public class PdfDoc {
        private static String PdfBasePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.PdfBasePath);
        private static String XmlBasePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.XmlBasePath);
        private String PdfPath;
        private String XmlPath;
        public PdfDoc(String Pdf) {
            this.PdfPath = Path.Combine(PdfDoc.PdfBasePath, Pdf);
        }
        public PdfDoc(String Pdf, String Xml) {
            this.PdfPath = Path.Combine(PdfDoc.PdfBasePath, Pdf);
            this.XmlPath = Path.Combine(PdfDoc.XmlBasePath, Xml);
        }
        public String GetPdfFile() {
            return this.PdfPath;
        }
        public String GetXmlFile() {
            return this.XmlPath;
        }
    }
}
