﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;

    public class PayrollDetail
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PayrollStatus { get; set; }
        public List<UserVisit> Visits { get; set; }
    }
}
