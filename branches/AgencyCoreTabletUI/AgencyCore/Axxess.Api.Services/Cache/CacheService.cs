﻿namespace Axxess.Api.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using Axxess.Core.Extension;

    public class CacheService : BaseService, ICacheService
    {
        #region ICacheService Members

        public void RefreshAgency(Guid agencyId)
        {
            AgencyEngine.Instance.Refresh(agencyId);
        }

        public string GetAgencyXml(Guid agencyId)
        {
            return AgencyEngine.Instance.Get(agencyId);
        }

        public void RefreshPhysicians(Guid agencyId)
        {
            PhysicianEngine.Instance.Refresh(agencyId);
        }

        public string GetPhysicianXml(Guid physicianId, Guid agencyId)
        {
            return PhysicianEngine.Instance.Get(physicianId, agencyId);
        }

        public List<string> GetPhysicians(Guid agencyId)
        {
            return PhysicianEngine.Instance.GetPhysicians(agencyId);
        }

        public void RefreshUsers(Guid agencyId)
        {
            UserEngine.Instance.Refresh(agencyId);
        }

        public string GetUserDisplayName(Guid userId, Guid agencyId)
        {
            return UserEngine.Instance.GetName(userId, agencyId);
        }

        #endregion

    }
}