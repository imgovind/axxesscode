﻿namespace Axxess.Api.Services
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Linq;
    using System.Xml.Linq;
    using System.Reflection;
    using System.Configuration;
    using System.Collections.Generic;

    using OASIS_Validation;

    using HomeHealthGold.Audits;

    using Axxess.Api.Contracts;
    using Axxess.Core.Extension;

    internal static class OasisValidator
    {
        private static string ICD9_PATH = ConfigurationSettings.AppSettings["FILE_PATH"].ToString();
        private static string DICT_PATH = ConfigurationSettings.AppSettings["FILE_PATH"].ToString();

        public static List<ValidationError> CheckDataString(string oasisDataString)
        {
            Windows.EventLog.WriteEntry(string.Format("Validator.CheckDataString Method with oasisDataString input: {0}", oasisDataString), System.Diagnostics.EventLogEntryType.Information);
            var errors = new List<ValidationError>();

            try
            {
                int errorCount = 0;
                string[] dupList = new string[] { };
                string[] errorTypeList = new string[] { };
                string[] descriptionList = new string[] { };

                clsEasyValidate easyValidator = new clsEasyValidate();
                easyValidator.ErrorCheck_Oasis(oasisDataString, ICD9_PATH, DICT_PATH, true, ref errorCount, ref descriptionList, ref dupList, ref errorTypeList);
                Windows.EventLog.WriteEntry(string.Format("ICD9_PATH: {0} DICT_PATH: {1}", ICD9_PATH, DICT_PATH), System.Diagnostics.EventLogEntryType.Information);

                Windows.EventLog.WriteEntry(string.Format("Error Count: {0}", errorCount), System.Diagnostics.EventLogEntryType.Information);
                
                if (descriptionList.Length == dupList.Length && errorTypeList.Length == dupList.Length)
                {
                    for (int i = 0; i < descriptionList.Length; i++)
                    {
                        var error = new ValidationError { ErrorType = errorTypeList[i], Description = descriptionList[i], ErrorDup = dupList[i] };
                        errors.Add(error);
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw ex;
            }
            
            return errors;
        }

        public static List<LogicalError> CheckLogicalInconsistencies(string oasisDataString)
        {
            Windows.EventLog.WriteEntry(string.Format("Validator.CheckLogicalInconsistencies Method with oasisDataString input: {0}", oasisDataString), System.Diagnostics.EventLogEntryType.Information);
            var errors = new List<LogicalError>();

            try
            {
                var audits = new OasisAudits();
                XDocument xDocument = audits.CheckForAuditsXml(oasisDataString);

                if (xDocument != null)
                {
                    foreach (XElement element in xDocument.Descendants(XName.Get("edit")))
                    {
                        var error = new LogicalError
                        {
                            Header = element.Element("header") != null && element.Element("header").Value.IsNotNullOrEmpty() ? element.Element("header").Value : string.Empty,
                            Reason = element.Element("reason") != null && element.Element("reason").Value.IsNotNullOrEmpty() ? element.Element("reason").Value : string.Empty,
                            Explanation = element.Element("explanation") != null && element.Element("explanation").Value.IsNotNullOrEmpty() ? element.Element("explanation").Value : string.Empty,
                            Action = element.Element("action") != null && element.Element("action").Value.IsNotNullOrEmpty() ? element.Element("action").Value : string.Empty,
                        };

                        var oasisItems = from e in element.Elements(XName.Get("oasis"))
                                         select e.Value;

                        if (oasisItems != null && oasisItems.Count<string>() > 0)
                        {
                            oasisItems.ForEach(o =>
                            {
                                error.OasisItems.Add(o);
                            });
                        }

                        errors.Add(error);

                        Windows.EventLog.WriteEntry(error.ToString(), System.Diagnostics.EventLogEntryType.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw ex;
            }

            return errors;
        }
    }
}
