﻿namespace Axxess.LookUp.Repositories
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Globalization;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;

    using SubSonic.Repository;

    public class LookupRepository : ILookupRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public LookupRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region ILookupRepository Members

        public IList<AmericanState> States()
        {
            IList<AmericanState> states = null;
            if (!Cacher.TryGet(CacheKey.States.ToString(), out states))
            {
                states = database.All<AmericanState>().ToList();
                Cacher.Set(CacheKey.States.ToString(), states);
            }
            return states.OrderBy(s => s.Name).ToList();
        }

        public IList<Insurance> Insurances()
        {
            IList<Insurance> insurances = null;
            if (!Cacher.TryGet(CacheKey.Insurances.ToString(), out insurances))
            {
                insurances = database.All<Insurance>().ToList();
                Cacher.Set(CacheKey.Insurances.ToString(), insurances);
            }
            return insurances;
        }

        public Insurance GetInsurance(int insuranceId)
        {
            return database.Single<Insurance>(i => i.Id == insuranceId);
        }

        public IList<EthnicRace> Races()
        {
            IList<EthnicRace> races = null;
            if (!Cacher.TryGet(CacheKey.Races.ToString(), out races))
            {
                races = database.All<EthnicRace>().ToList();
                Cacher.Set(CacheKey.Races.ToString(), races);
            }
            return races;
        }

        public IList<DrugClassification> DrugClassifications()
        {
            IList<DrugClassification> classifcations = null;
            if (!Cacher.TryGet(CacheKey.DrugClassifications.ToString(), out classifcations))
            {
                classifcations = database.All<DrugClassification>().ToList();
                Cacher.Set(CacheKey.DrugClassifications.ToString(), classifcations);
            }
            return classifcations;
        }

        public IList<PaymentSource> PaymentSources()
        {
            IList<PaymentSource> paymentSources = null;
            if (!Cacher.TryGet(CacheKey.PaymentSources.ToString(), out paymentSources))
            {
                paymentSources = database.All<PaymentSource>().ToList();
                Cacher.Set(CacheKey.PaymentSources.ToString(), paymentSources);
            }
            return paymentSources;
        }

        public IList<ReferralSource> ReferralSources()
        {
            IList<ReferralSource> referralSources = null;
            if (!Cacher.TryGet(CacheKey.ReferralSources.ToString(), out referralSources))
            {
                referralSources = database.All<ReferralSource>().ToList();
                Cacher.Set(CacheKey.ReferralSources.ToString(), referralSources);
            }
            return referralSources;
        }

        public IList<DiagnosisCode> DiagnosisCodes()
        {
            IList<DiagnosisCode> diagnosisCodes = null;
            if (!Cacher.TryGet(CacheKey.DiagnosisCodes.ToString(), out diagnosisCodes))
            {
                diagnosisCodes = database.All<DiagnosisCode>().ToList();
                Cacher.Set(CacheKey.DiagnosisCodes.ToString(), diagnosisCodes);
            }
            return diagnosisCodes;
        }

        public IList<ProcedureCode> ProcedureCodes()
        {
            IList<ProcedureCode> procedureCodes = null;
            if (!Cacher.TryGet(CacheKey.ProcedureCodes.ToString(), out procedureCodes))
            {
                procedureCodes = database.All<ProcedureCode>().ToList();
                Cacher.Set(CacheKey.ProcedureCodes.ToString(), procedureCodes);
            }
            return procedureCodes;
        }

        public IList<SupplyCategory> SupplyCategories()
        {
            IList<SupplyCategory> supplyCategories = null;
            if (!Cacher.TryGet(CacheKey.SupplyCategories.ToString(), out supplyCategories))
            {
                supplyCategories = database.All<SupplyCategory>().ToList();
                Cacher.Set(CacheKey.SupplyCategories.ToString(), supplyCategories);
            }
            return supplyCategories;
        }

        public IList<AdmissionSource> AdmissionSources()
        {
            IList<AdmissionSource> admissionSources = null;
            if (!Cacher.TryGet(CacheKey.AdmissionSources.ToString(), out admissionSources))
            {
                admissionSources = database.All<AdmissionSource>().ToList();
                Cacher.Set(CacheKey.AdmissionSources.ToString(), admissionSources);
            }
            return admissionSources;
        }

        public AdmissionSource GetAdmissionSource(int sourceId)
        {
            return AdmissionSources().SingleOrDefault(a => a.Id == sourceId);
        }

        public string GetAdmissionSourceCode(int sourceId)
        {
            var addmissionCode = AdmissionSources().SingleOrDefault(a => a.Id == sourceId);
            if (addmissionCode!=null)
            {
                return addmissionCode.Code;
            }
            return string.Empty;
        }

        public IList<Supply> Supplies()
        {
            IList<Supply> supplies = null;
            if (!Cacher.TryGet(CacheKey.Supplies.ToString(), out supplies))
            {
                supplies = database.All<Supply>().ToList();
                Cacher.Set(CacheKey.Supplies.ToString(), supplies);
            }
            return supplies;
        }

        public Supply GetSupply(int Id)
        {
            return database.Single<Supply>(s => s.Id == Id);
        }

        public Npi GetNpiData(string npiId)
        {
            return database.Single<Npi>(n => n.Id == npiId);
        }

        public ZipCode GetZipCode(string zipCode)
        {
            return database.Single<ZipCode>(z => z.Code == zipCode);
        }

        public Axxess.LookUp.Domain.IPAddress GetIPAddress(int ipAddress)
        {
            return database.Single<Axxess.LookUp.Domain.IPAddress>(i => i.Ip == ipAddress);
        }

        public string GetZipCodeFromIpAddress(int ipAddress)
        {
            var query = database.GetPaged<Axxess.LookUp.Domain.IPAddress>(i => i.Ip < ipAddress, "Ip desc", 0, 1);
            if (query.Count > 0)
            {
                return query[0].ZipCode;
            }
            return string.Empty;
        }

        public IList<Npi> GetNpis(string q, int limit)
        {
            return database.GetPaged<Npi>(n => n.Id.StartsWith(q) && n.EntityTypeCode == "1", 0, limit);
        }

        public bool VerifyPecos(string npi)
        {
            return database.Exists<PecosPhysician>(p => p.Id == npi);
        }

        public IList<DisciplineTask> DisciplineTasks(string Discipline)
        {
            IList<DisciplineTask> disciplineTasks = null;
            if (!Cacher.TryGet(CacheKey.DisciplineTasks.ToString(), out disciplineTasks))
            {
                disciplineTasks = database.All<DisciplineTask>().ToList();
                Cacher.Set(CacheKey.DisciplineTasks.ToString(), disciplineTasks);
            }
            if (Discipline.IsEqual("nursing"))
            {
                disciplineTasks = disciplineTasks.Cast<DisciplineTask>().Where(e => e.Discipline == "Nursing" || e.Discipline == "ReportsAndNotes" && e.Task != "Medicare Eligibility Report").ToList();
            }
            else if (Discipline.IsEqual("therapy"))
            {
                disciplineTasks = disciplineTasks.Cast<DisciplineTask>().Where(e => e.Discipline == "PT" || e.Discipline == "OT" || e.Discipline == "ST").ToList();
            }
            else
            {
                disciplineTasks = disciplineTasks.Cast<DisciplineTask>().Where(e => e.Discipline == Discipline).ToList();
            }
            return disciplineTasks;
        }

        public IList<DisciplineTask> DisciplineTasks()
        {
            IList<DisciplineTask> disciplineTasks = null;
            if (!Cacher.TryGet(CacheKey.DisciplineTasks.ToString(), out disciplineTasks))
            {
                disciplineTasks = database.All<DisciplineTask>().ToList();
               Cacher.Set(CacheKey.DisciplineTasks.ToString(), disciplineTasks);
            }
            return disciplineTasks.Cast<DisciplineTask>().ToList();
        }

        public DisciplineTask GetDisciplineTask(int disciplineTaskId)
        {
            return DisciplineTasks().SingleOrDefault(t => t.Id == disciplineTaskId);
        }

        public IList<MedicationRoute> MedicationRoute(string q, int limit)
        {
            return database.Find<MedicationRoute>(m => m.LongName.Contains(q));
        }

        public IList<MedicationClassfication> MedicationClassification(string q, int limit)
        {
            return database.Find<MedicationClassfication>(m => m.Name.Contains(q));
        }

        public IList<MedicationDosage> MedicationDosage(string q, int limit)
        {
            return database.Find<MedicationDosage>(m => m.CommonName.StartsWith(q) || m.CommonName.Contains(q));
        }

        public IList<Drug> DrugsContains(string query, int limit)
        {
            var script = @"SELECT `Id`, `Name`, `Code` FROM `axxesslookup`.`drugs` WHERE `Name` LIKE '%{0}%' ORDER BY `Name` ASC LIMIT 0, 20";

            return new FluentCommand<Drug>(string.Format(script, query))
                .SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new Drug
                {
                    Id = reader.GetInt("Id"),
                    Name = reader.GetString("Name"),
                    Code = reader.GetString("Code")
                })
                .AsList();
        }

        public IList<Drug> DrugsStartsWith(string query, int limit)
        {
            var script = @"SELECT `Id`, `Name`, `Code` FROM `axxesslookup`.`drugs` WHERE `Name` LIKE '{0}%' ORDER BY `Name` ASC LIMIT 0, 20";

            return new FluentCommand<Drug>(string.Format(script, query))
                .SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new Drug
                {
                    Id = reader.GetInt("Id"),
                    Name = reader.GetString("Name"),
                    Code = reader.GetString("Code")
                })
                .AsList();
        }

        public string CbsaCodeByZip(string zipCode)
        {
            var cbsaCode = database.Find<CBSACode>(c => c.Zip == zipCode).FirstOrDefault();
            if (cbsaCode != null)
            {
                return cbsaCode.CBSA;
            }
            return string.Empty;
        }

        public CBSACode CbsaCode(string zipCode)
        {
            if (zipCode.IsNullOrEmpty())
            {
                return null;
            }
            return database.Find<CBSACode>(c => c.Zip == zipCode).FirstOrDefault();
        }

        public HippsAndHhrg GetHHRGByHIPPSCODE(string hippsCode)
        {
           
            return database.Find<HippsAndHhrg>(h => h.HIPPS == hippsCode).FirstOrDefault();
        }

        public PPSStandard GetPPSStandardByYear(int year)
        {
            var ppsStandards = database.All<PPSStandard>().ToList();
            PPSStandard ppsStandard = null;
            if (ppsStandards != null)
            {
                foreach (var p in ppsStandards)
                {
                    if (p.Time.Year == year)
                    {
                        ppsStandard = p;
                        break;
                    }
                }
            }
            return ppsStandard;
        }

        public double ProspectivePayAmount(string hppisCode, DateTime time,string zipCode ,string agencyZipCode)
        {
            double amount = 0;
            CBSACode cbsaCode = null;
            if (hppisCode.IsNotNullOrEmpty()&& time.IsValid())
            {
                var hhrg = GetHHRGByHIPPSCODE(hppisCode);
                if (zipCode.IsNotNullOrEmpty())
                {
                    cbsaCode = CbsaCode(zipCode);
                }
                if (cbsaCode == null || (cbsaCode != null && cbsaCode.CBSA.IsNullOrEmpty()))
                {
                    cbsaCode = CbsaCode(agencyZipCode);
                }
                var ppsStandard = GetPPSStandardByYear(time.Year);
                if (hhrg != null && cbsaCode != null && ppsStandard != null)
                {
                    var ppsRate = ppsStandard.Rate;
                    var hhrgWeight = hhrg.HHRGWeight;
                    var labor = ppsStandard.Labor;
                    var nonLabor = ppsStandard.NonLabor;
                    double wageIndex = 0;
                    if (time.Year == 2011)
                    {
                        wageIndex = cbsaCode.WITwoEleven;
                    }
                    else if (time.Year == 2010)
                    {
                        wageIndex = cbsaCode.WITwoTen;
                    }
                    else if (time.Year == 2009)
                    {
                        wageIndex = cbsaCode.WITwoNine;
                    }
                    else if (time.Year == 2008)
                    {
                        wageIndex = cbsaCode.WITwoEight;
                    }
                    else if (time.Year == 2007)
                    {
                        wageIndex = cbsaCode.WITwoSeven;
                    }
                    amount = (ppsRate * hhrgWeight) * ((labor * wageIndex) + nonLabor);
                }
            }
            return amount;
        }

        public IList<MedicareRate> DefaultMedicareRates()
        {
            IList<MedicareRate> medicareRates = null;
            if (!Cacher.TryGet(CacheKey.MedicareRates.ToString(), out medicareRates))
            {
                medicareRates = database.All<MedicareRate>().ToList();
                Cacher.Set(CacheKey.MedicareRates.ToString(), medicareRates);
            }
            return medicareRates;
        }

        #endregion
    }
}
