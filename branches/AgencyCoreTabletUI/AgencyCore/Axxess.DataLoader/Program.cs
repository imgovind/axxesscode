﻿namespace Axxess.DataLoader
{
    using System;
    using StructureMap;

    //using Axxess.DataLoader.Domain;

    class Program
    {
        static void Main(string[] args)
        {
            //CradleMedPointScript.Run(new Guid("5d50ae56-5e62-46cf-9b7f-40f5ca8202b3"), new Guid("4bab3fe9-51dc-4e90-9262-32575e505349"));
            //KinnserScript.Run(new Guid("887cf2a6-089d-4040-bb7e-79d81dea973a"), new Guid("480ecccb-8e23-40d8-af19-1bab096f560a"));
            //KinnserCsvScript.Run(new Guid("37d54330-f914-451a-937d-ebe3b1d19bf4"), new Guid("12c407d0-b0e5-4c9c-aab0-386c36e4057d"));
            //AxxessScript.Run(new Guid("4741922e-1e27-421d-871a-c2e2e3834013"), new Guid("d29f4c94-4489-4037-bbdb-2cd4f7b9c8ce"));
            //MedicationProfileScript.Run(new Guid("6d80ae1f-410d-408f-b342-051e956b037f"));
            //GenericExcelScript.Run(new Guid("8bf20512-359a-4fe5-9438-966dfaa5a34c"), new Guid("8885e4f3-36ce-4a26-a08a-0b691e89ccbe"));
            //SynergyScript.Run(new Guid("855d3109-d693-4458-9ed7-ca44612502c2"), new Guid("cce3cbb8-2ddd-47a0-8828-5ee0852bfada"));
            //VisiTrakScript.Run(new Guid("98edb6d5-71ec-4ccb-8634-100f882236fb"), new Guid("4804f918-37f3-403b-a48b-de479d4813c6"));
            //VisiTrakPhysicianScript.Run(new Guid("98edb6d5-71ec-4ccb-8634-100f882236fb"));
            //SynergyTwoScript.Run(new Guid("42d3a029-b1cb-4608-b788-6255f6cea23d"), new Guid("19a82659-eaa7-4320-99b9-311eff3cde97"));
            //SynergyPhysicianScript.Run(new Guid("42d3a029-b1cb-4608-b788-6255f6cea23d"));
            //HealthMedXPhysicianScript.Run(new Guid("abc2fbb2-807e-4dd0-987f-608e5d75558a"));
            //HealthCareFirstScript.Run(new Guid("e279f258-4622-47ed-9b3d-fb10b7b2f150"), new Guid("db30ed8f-5d92-49db-8029-d1f537ed38c1"));
            //Icd9Script.Run(true);
            //Icd9Script.Run(false);
            //GenericExcelTwoScript.Run(new Guid("4a318889-50b7-4ba2-90d2-bb259a663411"), new Guid("e4556fd3-e9e9-42e3-a801-06ddd9da77ee"));
            //WageIndexScript.Run();
            //CradleCsvScript.Run(new Guid("458f0cc0-41d0-4ef2-9180-5be8f5c5750c"), new Guid("a722a4be-4926-40e3-b3a2-a665b6227772"));
            Console.WriteLine("Script Complete");
            Console.ReadLine();
        }
    }
}
