﻿namespace Axxess.DataLoader
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public static class Database
    {
        private static readonly SimpleRepository lookupDatabase = new SimpleRepository("AxxessLookupConnectionString", SimpleRepositoryOptions.None);
        private static readonly SimpleRepository agencyManagementDatabase = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);
        internal static Patient GetPatient(Guid patientId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<Patient>(p => p.Id == patientId && p.AgencyId == agencyId);
        }

        internal static List<Patient> GetPatients(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Patient>(p => p.AgencyId == agencyId).ToList();
        }

        internal static AgencyPhysician GetPhysician(string npi, Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyPhysician>(p => p.NPI == npi && p.AgencyId == agencyId);
        }

        internal static Npi GetNpiData(string npi)
        {
            return lookupDatabase.Single<Npi>(p => p.Id == npi);
        }

        internal static bool Add<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool AddForLookup<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                lookupDatabase.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool Update<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static bool UpdateForLookup<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                lookupDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static List<CBSACode> GetCbsaCodes()
        {
            return lookupDatabase.All<CBSACode>().ToList();
        }
    }
}
