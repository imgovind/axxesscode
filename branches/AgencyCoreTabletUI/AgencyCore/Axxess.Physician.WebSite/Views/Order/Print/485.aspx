﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PlanofCare>" %><%
var data = Model.ToDictionary();
var medicationList = Model.MedicationProfileData.IsNotNullOrEmpty() ? Model.MedicationProfileData.ToObject<List<Medication>>() : new List<Medication>();
var patient = Model.PatientData.IsNotNullOrEmpty() ? Model.PatientData.ToObject<Patient>() : new Patient();
var agency = Model.AgencyData.IsNotNullOrEmpty() ? Model.AgencyData.ToObject<Agency>() : new Agency();
var location = agency != null && agency.MainLocation != null ? agency.MainLocation : new AgencyLocation();
var physician = Model.PhysicianData.IsNotNullOrEmpty() ? Model.PhysicianData.ToObject<AgencyPhysician>() : new AgencyPhysician();
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + " | " : ""%>485 Plan of Care<%= patient != null ? (" | " + patient.LastName + ", " + patient.FirstName + " " + patient.MiddleInitial).ToTitleCase() : ""%></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(3).Version(Current.AssemblyVersion))%>
    <% Html.Telerik().ScriptRegistrar().Globalization(true).DefaultGroup(group => group.Add("/Modules/printview.min.js").Compress(true).Combined(true).CacheDurationInDays(3).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.cssclass = "print485";
    printview.firstheader = "%3Cdiv class=%22small float_left%22%3EDepartment of Health and Human Services Health Care Financing Administration%3C/div%3E%3Cdiv class=%22small float_right%22%3EForm Approved OMB No. 0938-0357%3C/div%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Ch1%3EHome Health Certification and Plan of Care%3C/h1%3E%3Cdiv%3E%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Ctd%3E1. Patient&#8217;s HI Claim No.%3Cspan id=%22loc1%22 class=%22mono%22%3E" +
        "<%= patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber : "" %>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E2. Start of Care Date%3Cspan id=%22loc2%22 class=%22mono%22%3E" +
        "<%= patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? patient.StartOfCareDateFormatted : "" %>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E3. Certification Period%3Cspan id=%22loc3%22 class=%22mono%22%3EFrom: " +
        "<%= Model.EpisodeStart.IsNotNullOrEmpty() ? Model.EpisodeStart : "%3Cspan class=%22noline blank%22%3E%3C/span%3E"%>" +
        " To: " +
        "<%= Model.EpisodeEnd.IsNotNullOrEmpty() ? Model.EpisodeEnd : "%3Cspan class=%22noline blank%22%3E%3C/span%3E"%>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E4. Medical Record No.%3Cspan id=%22loc4%22 class=%22mono%22%3E" +
        "<%= patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E5. Provider No.%3Cspan id=%22loc5%22 class=%22mono%22%3E" +
        "<%= location.MedicareProviderNumber.IsNotNullOrEmpty() ? location.MedicareProviderNumber : "" %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/div%3E%3Cdiv%3E%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd%3E6. Patient&#8217;s Name and Address%3Cspan id=%22loc6%22 class=%22pent%22%3E" +
        "<%= patient.LastName.IsNotNullOrEmpty() ? patient.LastName + ", " : "" %>" +
        "<%= patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName : "" %>" +
        "<%= patient.MiddleInitial.IsNotNullOrEmpty() ? " " + patient.MiddleInitial + "." : "" %>" +
        "%3Cbr /%3E" +
        "<%= patient.AddressLine1.IsNotNullOrEmpty() ? patient.AddressLine1.ToTitleCase() : "" %>" +
        "<%= patient.AddressLine2.IsNotNullOrEmpty() ? " " + patient.AddressLine2.ToTitleCase() : ""%>" +
        "<%= patient.AddressCity.IsNotNullOrEmpty() ? "%3Cbr /%3E" + patient.AddressCity.ToTitleCase() : ""%>" +
        "<%= patient.AddressStateCode.IsNotNullOrEmpty() ? ", " + patient.AddressStateCode.ToUpper() : "" %>" +
        "<%= patient.AddressZipCode.IsNotNullOrEmpty() ? " &#160;" + patient.AddressZipCode : "" %>" +
        "<%= patient.PhoneHomeFormatted.IsNotNullOrEmpty() ? "%3Cbr /%3E" + patient.PhoneHomeFormatted : "" %>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E7. Provider&#8217;s Name, Address and Telephone Number%3Cspan id=%22loc7%22 class=%22hex%22%3E" +
        "<%= agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() : "" %>" +
        "%3Cbr /%3E" +
        "<%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : ""%>" +
        "<%= location.AddressLine2.IsNotNullOrEmpty() ? " " + location.AddressLine2.ToTitleCase() : ""%>" +
        "<%= location.AddressCity.IsNotNullOrEmpty() ? "%3Cbr /%3E" + location.AddressCity.ToTitleCase() : ""%>" +
        "<%= location.AddressStateCode.IsNotNullOrEmpty() ? ", " + location.AddressStateCode : ""%>" +
        "<%= location.AddressZipCode.IsNotNullOrEmpty() ? " &#160;" + location.AddressZipCode : ""%>" +
        "<%= location.PhoneWork.IsNotNullOrEmpty() ? "%3Cbr /%3EPhone: (" + location.PhoneWork.Substring(0, 3) + ") " + location.PhoneWork.Substring(3, 3) + "-" + location.PhoneWork.Substring(6, 4) : ""%>" +
        "<%= location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted.Clean() : "" %>" +
        "<%= agency.ContactPersonEmail.IsNotNullOrEmpty() ? "%3Cbr /%3EEmail:" + agency.ContactPersonEmail : "" %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22nopad%22%3E%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Ctd%3E8. Date of Birth " +
        "<%= patient.DOBFormatted.IsNotNullOrEmpty() ? patient.DOBFormatted : "%3Cspan class=%22noline blank%22%3E%3C/span%3E" %>" +
        "%3C/td%3E%3Ctd%3E9. Sex " +
        "<%= patient.Gender.IsNotNullOrEmpty() ? patient.Gender : "%3Cspan class=%22noline blank%22%3E%3C/span%3E" %>" +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/td%3E%3Ctd rowspan=%222%22%3E10. Medications: Dose/Freq./Route (N)ew (C)hanged%3Cspan id=%22loc10%22 class=%22deca%22%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22nopad%22%3E%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Ctd%3E11. ICD-9-CM%3Cspan id=%22loc11a%22 class=%22mono%22%3E" +
        "<%= data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer : ""%>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3EPrincipal Diagnosis%3Cspan id=%22loc11b%22 class=%22mono%22%3E" +
        "<%= data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : ""%>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3EDate&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;%3Cspan id=%22loc11c%22 class=%22mono%22%3E" +
        "<%= data.ContainsKey("M1020PrimaryDiagnosisDate") ? data["M1020PrimaryDiagnosisDate"].Answer : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E12. ICD-9-CM%3Cspan id=%22loc12a%22 class=%22mono%22%3E" +
        "<%= data.ContainsKey("485SurgicalProcedureCode1") ? data["485SurgicalProcedureCode1"].Answer : ""%>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3ESurgical Procedure%3Cspan id=%22loc12b%22 class=%22mono%22%3E" +
        "<%= data.ContainsKey("485SurgicalProcedureDescription1") ? data["485SurgicalProcedureDescription1"].Answer : ""%>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3EDate%3Cspan id=%22loc12c%22 class=%22mono%22%3E" +
        "<%= data.ContainsKey("485SurgicalProcedureCode1Date") ? data["485SurgicalProcedureCode1Date"].Answer : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E13. ICD-9-CM%3Cspan id=%22loc13a%22 class=%22pent%22%3E" +
        <% for (int i = 1; i < 5; i++) { %>
            "<%= data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() ? (i > 1 ? "%3Cbr /%3E" : "") + data["M1022ICD9M" + i].Answer : ""%>" +
        <% } %>
        "%3C/span%3E%3C/td%3E%3Ctd%3EOther Pertinent Diagnoses%3Cspan id=%22loc13b%22 class=%22pent%22%3E" +
        <% for (int i = 1; i < 5; i++) { %>
            "<%= data.ContainsKey("M1022PrimaryDiagnosis" + i) && data["M1022PrimaryDiagnosis" + i].Answer.IsNotNullOrEmpty() ? (i > 1 ? "%3Cbr /%3E" : "") + data["M1022PrimaryDiagnosis" + i].Answer : ""%>" +
        <% } %>
        "%3C/span%3E%3C/td%3E%3Ctd%3EDate%3Cspan id=%22loc13c%22 class=%22pent%22%3E" +
        <% for (int i = 1; i < 5; i++) { %>
            "<%= data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() && data.ContainsKey("M1022PrimaryDiagnosis" + i + "Date") ? (i > 1 ? "%3Cbr /%3E" : "") + data["M1022PrimaryDiagnosis" + i + "Date"].Answer : ""%>" +
        <% } %>
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E14. DME and Supplies%3Cspan id=%22loc14%22 class=%22trip%22%3E%3C/span%3E%3C/td%3E%3Ctd%3E15. Safety measures%3Cspan id=%22loc15%22 class=%22trip%22%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E16. Nutritional Requirements%3Cspan id=%22loc16%22 class=%22trip%22%3E%3C/span%3E%3C/td%3E%3Ctd%3E17. Allergies%3Cspan id=%22loc17%22 class=%22trip%22%3E" +
        "<%= data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "No" ? "NKA (Food/Drugs/Latex)" : "Allergic to:" + (data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : "") %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E18.A. Functional Limitations%3Cbr /%3E%3Ctable class=%22noborders small fixed%22%3E%3Ctbody%3E%3Ctr%3E" +
        <% string[] functionLimitations = data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer != "" ? data["485FunctionLimitations"].Answer.Split(',') : null; %>
        "%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("1") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 1. Amputation%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("5") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 5. Paralysis%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("9") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 9. Legally Blind%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("2") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 2. Bowel/Bladder (Incontinance)%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("6") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 6. Endurance%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("A") ? "&#x2713;" : "" %>" +
        "%3C/span%3E A. Dyspnea With%3Cbr /%3EMinimal Exertion%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("3") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 3. Contracture%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("7") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 7. Ambulation%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("B") ? "&#x2713;" : "" %>" +
        "%3C/span%3E B. Other (Specify)%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("4") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 4. Hearing%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("8") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 8. Speech%3C/span%3E%3C/td%3E%3Ctd%3E" +
        "<%= functionLimitations != null && functionLimitations.Contains("B") && data.ContainsKey("485FunctionLimitationsOther") ? data["485FunctionLimitationsOther"].Answer : "" %>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E&#160;%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/td%3E%3Ctd%3E18.B. Activities Permitted%3Cbr /%3E%3Ctable class=%22noborders small fixed%22%3E%3Ctbody%3E%3Ctr%3E" +
        <% string[] activitiesPermitted = data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer != "" ? data["485ActivitiesPermitted"].Answer.Split(',') : null; %>
        "%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("1") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 1. Complete Bedrest%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("6") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 6. Partial Weight Bearing%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("A") ? "&#x2713;" : "" %>" +
        "%3C/span%3E A. Wheelchair%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("2") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 2. Bedrest BRP%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("7") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 7. Independent At Home%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("B") ? "&#x2713;" : "" %>" +
        "%3C/span%3E B. Walker%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("3") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 3. Up As Tolerated%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("8") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 8. Crutches%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("C") ? "&#x2713;" : "" %>" +
        "%3C/span%3E C. No Restrictions%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("4") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 4. Transfer Bed/Chair%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("9") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 9. Cane%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("D") ? "&#x2713;" : "" %>" +
        "%3C/span%3E D. Other (Specify)%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E%3Cspan class=%22checklabel auto%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("5") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 5. Exercises Prescribed%3C/span%3E%3C/td%3E%3Ctd colspan=%222%22%3E" +
        "<%= activitiesPermitted != null && activitiesPermitted.Contains("D") && data.ContainsKey("485ActivitiesPermittedOther") ? data["485ActivitiesPermittedOther"].Answer : ""%>" +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E&#160;%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/div%3E%3Cdiv class=%22hexcol%22%3E%3Cspan%3E19. Mental Status%3C/span%3E" +
        <%string[] mentalStatus = data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer != "" ? data["485MentalStatus"].Answer.Split(',') : null; %>
        "%3Cspan class=%22checklabel small%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= mentalStatus!=null && mentalStatus.Contains("1") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 1. Oriented%3C/span%3E%3Cspan class=%22checklabel small%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= mentalStatus!=null && mentalStatus.Contains("3") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 3. Forgetful%3C/span%3E%3Cspan class=%22checklabel small%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= mentalStatus!=null && mentalStatus.Contains("5") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 5. Disoriented%3C/span%3E%3Cspan class=%22checklabel small%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= mentalStatus!=null && mentalStatus.Contains("7") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 7. Agitated%3C/span%3E%3Cspan class=%22checklabel%22%3E%3C/span%3E%3Cspan%3E%3C/span%3E%3Cspan class=%22checklabel small%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= mentalStatus!=null && mentalStatus.Contains("2") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 2. Comatose%3C/span%3E%3Cspan class=%22checklabel small%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= mentalStatus!=null && mentalStatus.Contains("4") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 4. Depressed%3C/span%3E%3Cspan class=%22checklabel small%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= mentalStatus!=null && mentalStatus.Contains("6") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 6. Lethargic%3C/span%3E%3Cspan class=%22checklabel small%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= mentalStatus!=null && mentalStatus.Contains("8") ? "&#x2713;" : "" %>" +
        "%3C/span%3E 8. Other%3C/span%3E%3Cspan%3E" +
        "<%= mentalStatus != null && mentalStatus.Contains("8") && data.ContainsKey("485MentalStatusOther") ? data["485MentalStatusOther"].Answer : ""%>" +
        "%3C/span%3E%3C/div%3E%3Cdiv class=%22hexcol%22%3E%3Cspan%3E20. Prognosis%3C/span%3E%3Cspan class=%22checklabel%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Poor" ? "&#x2713;" : "" %>" +
        "%3C/span%3E 1. Poor%3C/span%3E%3Cspan class=%22checklabel%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Guarded" ? "&#x2713;" : "" %>" +
        "%3C/span%3E 2. Guarded%3C/span%3E%3Cspan class=%22checklabel%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Fair" ? "&#x2713;" : "" %>" +
        "%3C/span%3E 3. Fair%3C/span%3E%3Cspan class=%22checklabel%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Good" ? "&#x2713;" : "" %>" +
        "%3C/span%3E 4. Good%3C/span%3E%3Cspan class=%22checklabel%22%3E%3Cspan class=%22checkbox%22%3E" +
        "<%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Excellent" ? "&#x2713;" : "" %>" +
        "%3C/span%3E 5. Excellent%3C/span%3E%3C/div%3E%3Cdiv%3E%3Cspan%3E21. Orders for Discipline and Treatments (Specify Amount/Frequency/Duration)%3C/span%3E%3Cspan id=%22loc21%22 class=%22octo%22%3E%3C/span%3E%3C/div%3E%3Cdiv%3E%3Cspan%3E22. Goals/Rehabilitation Potential/Discharge Plans%3C/span%3E%3Cspan id=%22loc22%22 class=%22deca%22%3E%3C/span%3E%3C/div%3E%3Cdiv%3E%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Ctd%3E23. Nurse&#8217;s Signature and Date of Verbal SOC Where Applicable:%3Cspan id=%22loc23%22 class=%22dual%22%3E" +
        "<%= Model != null ? Model.SignatureText + " " : "" %>" +
        "<%= Model != null && Model.SignatureDate.ToShortDateString() != "1/1/0001" ? Model.SignatureDate.ToShortDateString() : ""%>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E25. Date of HHA Received Signed POT:%3Cspan id=%22loc25%22 class=%22dual%22%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/div%3E%3Cdiv%3E%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd%3E24. Physician&#8217;s Name and Address%3Cspan id=%22loc24%22 class=%22pent%22%3E" +
        "<%= physician.LastName.IsNotNullOrEmpty() ? physician.LastName + ", " : "" %>" +
        "<%= physician.FirstName.IsNotNullOrEmpty() ? physician.FirstName : ""%>" +
        "<%= physician.MiddleName.IsNotNullOrEmpty() ? " " + physician.MiddleName : "" %>" +
        "%3Cbr /%3E" +
        "<%= physician.AddressFirstRow.IsNotNullOrEmpty() ? physician.AddressFirstRow + "%3Cbr /%3E" : ""%>" +
        "<%= physician.AddressSecondRow.IsNotNullOrEmpty() ? physician.AddressSecondRow + "%3Cbr /%3E" : ""%>" +
        "<%= physician.PhoneWorkFormatted.IsNotNullOrEmpty() ? "Phone: " + physician.PhoneWorkFormatted : "" %>" +
        "<%= physician.FaxNumberFormatted.IsNotNullOrEmpty() ?" | Fax: " + physician.FaxNumberFormatted : "" %>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E26. I " +
        "<%= Model.AssessmentType == "StartOfCare" ? "%3Cu%3Ecertify%3C/u%3E" : "certify" %>/<%= Model.AssessmentType == "Recertification" ? "%3Cu%3Erecertify%3C/u%3E" : "recertify" %>" +
        " that this patient is confined to his/her home and needs intermittent skilled nursing care, physical therapy and/or speech therapy or continues to need occupational therapy. The patient is under my care, and I have authorized services on this plan of care and will periodically review the plan.%3Cspan id=%22loc26%22 class=%22dual%22%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E27. Attending Physician&#8217;s Signature and Date Signed%3Cspan id=%22loc27%22 class=%22dual%22%3E%3C/span%3E%3C/td%3E%3Ctd%3E28. Anyone who misrepresents, falsifies, or conceals essential information required for payment of Federal funds may be subject to fine, imprisonment, or civil penalty under applicable Federal laws.%3Cspan id=%22loc28%22 class=%22dual%22%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/div%3E%3Cdiv class=%22small float_left%22%3ECMS-485 (C-3) (02-94) (Formerly HCFA-485) (Print Aligned)%3C/div%3E%3Cdiv class=%22small float_right pagenum%22%3E%3C/div%3E";
    printview.header = "%3Cdiv class=%22small float_left%22%3EDepartment of Health and Human Services Health Care Financing Administration%3C/div%3E%3Cdiv class=%22small float_right%22%3EForm Approved OHB No. 0938-0357%3C/div%3E%3Cspan class=%22clear%22%3E%3C/span%3E%3Ch1%3EAddendum to Plan of Care%3C/h1%3E%3Cdiv%3E%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Ctd%3E1. Patient&#8217;s HI Claim No.%3Cspan id=%22loc1-487%22 class=%22mono%22%3E" +
        "<%= patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber : "" %>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E2. Start of Care Date%3Cspan id=%22loc2-487%22 class=%22mono%22%3E" +
        "<%= patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? patient.StartOfCareDateFormatted : "" %>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E3. Certification Period%3Cspan id=%22loc3-487%22 class=%22mono%22%3EFrom: " +
        "<%= Model.EpisodeStart.IsNotNullOrEmpty() ? Model.EpisodeStart : "%3Cspan class=%22noline blank%22%3E%3C/span%3E" %>" +
        " To: " +
        "<%= Model.EpisodeEnd.IsNotNullOrEmpty() ? Model.EpisodeEnd : "%3Cspan class=%22noline blank%22%3E%3C/span%3E"%>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E4. Medical Record No.%3Cspan id=%22loc4-487%22 class=%22mono%22%3E" +
        "<%= patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E5. Provider No.%3Cspan id=%22loc5-487%22 class=%22mono%22%3E" +
        "<%= agency.MedicareProviderNumber.IsNotNullOrEmpty() ? agency.MedicareProviderNumber : "" %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/div%3E%3Cdiv%3E%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd%3E6. Patient&#8217;s Name%3Cspan id=%22loc6-487%22 class=%22dual%22%3E" +
        "<%= patient.LastName.IsNotNullOrEmpty() ? patient.LastName + ", " : "" %>" +
        "<%= patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName : "" %>" +
        "<%= patient.MiddleInitial.IsNotNullOrEmpty() ? " " + patient.MiddleInitial + "." : "" %>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E7. Provider&#8217;s Name%3Cspan id=%22loc7-487%22 class=%22dual%22%3E" +
        "<%= agency.Name.IsNotNullOrEmpty() ? agency.Name : "" %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/div%3E%3Cdiv class=%22border487%22%3E%3C/div%3E";
    printview.firstfooter = "&#160;";
    printview.footer = "%3Cdiv%3E%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%225%22%3E9. Signature of Physician%3Cspan id=%22loc9-487%22 class=%22dual%22%3E" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E10. Date%3Cspan id=%22loc10b-487%22 class=%22dual%22%3E" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%225%22%3E11. Optional Name/Signature of Nurse/Therapist%3Cspan id=%22loc11-487%22 class=%22dual%22%3E" +
        "<%= Model != null ? Model.SignatureText + " " : "" %>" +
        "%3C/span%3E%3C/td%3E%3Ctd%3E12. Date%3Cspan id=%22loc12b-487%22 class=%22dual%22%3E" +
        "<%= Model != null && Model.SignatureDate.ToShortDateString() != "1/1/0001" ? Model.SignatureDate.ToShortDateString() : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/div%3E%3Cdiv class=%22small float_left%22%3EForm HCFA-487 (C4)(4-87)%3C/div%3E%3Cdiv class=%22small float_right pagenum%22%3E%3C/div%3E";
    printview.addsection485("<%= data.ContainsKey("485Medications") && data["485Medications"].Answer.IsNotNullOrEmpty() ? data["485Medications"].Answer.Clean().Replace(", ", "<br/>") : "" %>", "<br/>", 10, "Medications", 0);<%
    if (data.ContainsKey("485SurgicalProcedureCode2") && data["485SurgicalProcedureCode2"].Answer.IsNotNullOrEmpty()) { %>
    printview.addsection485("<%=
        data.ContainsKey("485SurgicalProcedureCode2") && data["485SurgicalProcedureCode2"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode2"].Answer : "" %><%=
        data.ContainsKey("485SurgicalProcedureDescription2") && data["485SurgicalProcedureDescription2"].Answer.IsNotNullOrEmpty() ? " / " + data["485SurgicalProcedureDescription2"].Answer : "" %><%=
        data.ContainsKey("485SurgicalProcedureCode2Date") && data["485SurgicalProcedureCode2Date"].Answer.IsNotNullOrEmpty() ? " / " + data["485SurgicalProcedureCode2Date"].Answer : "" %>", "<br/>", 12, "Surgical Procedure", 0);<%
    }
    if (data.ContainsKey("M1022ICD9M5") && data["M1022ICD9M5"].Answer.IsNotNullOrEmpty()) { %>
    printview.addsection485("<%
        for (int i = 5; i < 12; i++) { %><%=
            data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() ? (i == 5 ? "" : "<br/>") + data["M1022ICD9M" + i].Answer : "" %><%=
            data.ContainsKey("M1022PrimaryDiagnosis" + i) && data["M1022PrimaryDiagnosis" + i].Answer.IsNotNullOrEmpty() ? " / " + data["M1022PrimaryDiagnosis" + i].Answer : "" %><%=
            data.ContainsKey("M1022PrimaryDiagnosis" + i + "Date") && data["M1022PrimaryDiagnosis" + i + "Date"].Answer.IsNotNullOrEmpty() ? " / " + data["M1022PrimaryDiagnosis" + i + "Date"].Answer : "" %><%
        } %>", "<br/>", 13, "Diagnoses", 0);<%
    } %>
    printview.addsection485("<%=
        data.ContainsKey("485DME") && data["485DME"].Answer.IsNotNullOrEmpty() ? PlanofCareXml.LookupText("DME", data["485DME"].Answer) : "" %><%=
        data.ContainsKey("485DMEComments") && data["485DMEComments"].Answer.IsNotNullOrEmpty() ? ", " + data["485DMEComments"].Answer : "" %><%=
        data.ContainsKey("485Supplies") && data["485Supplies"].Answer.IsNotNullOrEmpty() ? ", " + PlanofCareXml.LookupText("Supplies", data["485Supplies"].Answer) : "" %><%=
        data.ContainsKey("485SuppliesComment") && data["485SuppliesComment"].Answer.IsNotNullOrEmpty() ? data["485SuppliesComment"].Answer : "" %>", ", ", 14, "DME", 0);
    printview.addsection485("<%=
        data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.IsNotNullOrEmpty() ? PlanofCareXml.LookupText("SafetyMeasures", data["485SafetyMeasures"].Answer) : "" %><%=
        data.ContainsKey("485OtherSafetyMeasures") && data["485OtherSafetyMeasures"].Answer.IsNotNullOrEmpty() ? ", " + data["485OtherSafetyMeasures"].Answer : "" %>", ", ", 15, "Safety Measures", 0);
    printview.addsection485("<%= data.ContainsKey("485NutritionalReqs") ? data["485NutritionalReqs"].Answer.Clean() : "" %>", ", ", 16, "Nutrition", 0);
    printview.addsection485("<%= data.ContainsKey("485Interventions") ? data["485Interventions"].Answer.Clean() : "" %>", " ", 21, "Orders", 0);
    printview.addsection485("<%= data.ContainsKey("485Goals") ? data["485Goals"].Answer.Clean() : "" %>", " ", 22, "Goals", 0);
    printview.setpagenumbers();
</script>
</body>
</html>