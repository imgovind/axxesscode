﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type='text/javascript'>acore.renamewindow('{0}`s Pending Orders','listorders');</script>", Current.DisplayName)%>
<%= Html.Telerik().Grid<Order>().Name("orderGrid").Columns(columns => {
            columns.Bound(o => o.AgencyName);
            columns.Bound(o => o.PatientName);
            columns.Bound(o => o.TypeDescription).ClientTemplate("<#=PrintUrl#>").Title("Order Type");
            columns.Bound(o => o.OrderDate).Width(100);
            columns.Bound(o => o.StatusName).Title("Status");
        })
        .Groupable(settings => settings.Groups(groups => {
            var data = ViewData["GroupName"].ToString();
            if (data == "AgencyName")
            {
                groups.Add(o => o.AgencyName);
            }
            else if (data == "PatientName")
            {
                groups.Add(o => o.PatientName);
            }
            else if (data == "TypeDescription")
            {
                groups.Add(o => o.TypeDescription);
            }
            else if (data == "OrderDate")
            {
                groups.Add(o => o.OrderDate);
            }
            else
            {
                groups.Add(o => o.OrderDate);
            }
        }))
            .DataBinding(dataBinding => dataBinding.Ajax().Select("ListGrid", "Order"))
        .Scrollable().Sortable()
%>
<script type="text/javascript">
    $(".t-group-indicator", "#window_listorders").hide();
    $(".t-grouping-header", "#window_listorders").remove();
    $(".t-grid-content", "#window_listorders").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px', 'bottom': '25px' });
</script>