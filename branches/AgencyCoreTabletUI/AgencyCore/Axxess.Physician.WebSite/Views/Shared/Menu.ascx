﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">
acore.init();
acore.addmenu("View", "view", "mainmenu", 43);
acore.addwindow("listorders", "Orders Pending Review", "Order/List", function() { }, "view");
acore.addwindow("listfacetoface", "Face-To-Face Encounters", "Order/FaceToFaceList", function() { }, "view");
acore.addwindow("listpastorders", "Orders Completed", "Order/CompleteList", function() { }, "view");
acore.addwindow("faceToFace", "", 'Order/FaceToFaceEncounter', function() { }, true, false);

$('ul#mainmenu').superfish();

acore.open("listorders");

</script>

