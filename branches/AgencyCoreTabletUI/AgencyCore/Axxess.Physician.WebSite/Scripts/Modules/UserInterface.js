﻿var UserInterface = {
    CloseWindow: function(window) {
        acore.close(window);
    },
    ShowEditFaceToFaceEncounter: function(agencyId, episodeId, patientId, orderId) {
    acore.open("faceToFace", 'Order/FaceToFaceEncounter', function() { Order.InitFaceToFaceEncounter(); }, { agencyId: agencyId, episodeId: episodeId, patientId: patientId, id: orderId });
    }
}
