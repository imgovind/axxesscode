﻿var Order = {
    Load: function(groupby) {
        $("#orderListContent").empty().addClass("loading").load('Order/ListView', { groupName: groupby }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') U.growl('Pending Orders List could not be grouped. Please close this window and try again.', 'error');
            else if (textStatus == "success") $("#orderListContent").removeClass("loading");
        });
    },
    Rebind: function() {
        U.rebindTGrid($('#orderGrid'));
    },
    RebindFaceToFace: function() {
        U.rebindTGrid($('#List_FaceToFace'));
    },
    Update: function(action, agencyId, episodeId, patientId, orderId, orderType) {
        var reason = "";
        if (action == "Return") {
            if ($("#printreturnreason").is(":hidden")) {
                $("#printcontrols li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very_hidden");
                $("#printreturnreason").slideDown('slow');
            } else {
                reason = $("#printreturnreason textarea").val();
                U.postUrl("/Order/Update", { action: action, agencyId: agencyId, episodeId: episodeId, patientId: patientId, orderId: orderId, orderType: orderType, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        acore.closeprintview();
                        $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                        Order.Rebind();
                    } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                });
            }
        } else {
            U.postUrl("/Order/Update", { action: action, agencyId: agencyId, episodeId: episodeId, patientId: patientId, orderId: orderId, orderType: orderType, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    acore.closeprintview();
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                    Order.Rebind();
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    InitFaceToFaceEncounter: function() {
        $("#editFaceToFaceForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            Order.RebindFaceToFace();
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('faceToFace');
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    }
}