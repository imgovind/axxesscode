﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2010/05/")]
    public interface IAuthenticationService : IService
    {
        [OperationContract]
        int Count();
        [OperationContract]
        List<SingleUser> ToList();
        [OperationContract]
        void Log(SingleUser user);
        [OperationContract]
        SingleUser Get(Guid loginId);
        [OperationContract]
        void SignIn(SingleUser user);
        [OperationContract]
        void SignOut(string emailAddress);
        [OperationContract]
        bool Verify(SingleUser user);
    }
}
