﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2011/06/")]
    public interface IReportService : IService
    {
        [OperationContract]
        List<Dictionary<string, string>> CahpsExport(Guid agencyId, int sampleMonth, int sampleYear);
        [OperationContract]
        List<Dictionary<string, string>> CahpsExportByPaymentSources(Guid agencyId, int sampleMonth, int sampleYear, List<int> paymentSources);
    }
}
