﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Authentication/2011/07/")]
    public class SingleUser
    {
        [DataMember]
        public Guid LoginId { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string IpAddress { get; set; }
        [DataMember]
        public string SessionId { get; set; }
        [DataMember]
        public string AgencyName { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public bool IsAuthenticated { get; set; }
        [DataMember]
        public DateTime LastSecureActivity { get; set; }
    }
}
