﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2011/06/")]
    public class EpisodeData
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid AgencyId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public string Schedule { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public Guid PatientId { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string MiddleInitial { get; set; }
        [DataMember]
        public string MedicareNumber { get; set; }
        [DataMember]
        public string MedicaidNumber { get; set; }
        [DataMember]
        public string AddressLine1 { get; set; }
        [DataMember]
        public string AddressLine2 { get; set; }
        [DataMember]
        public string AddressCity { get; set; }
        [DataMember]
        public string PhoneHome { get; set; }
        [DataMember]
        public string AddressStateCode { get; set; }
        [DataMember]
        public string AddressZipCode { get; set; }
        [DataMember]
        public DateTime DischargeDate { get; set; }
        [DataMember]
        public string PaymentSources { get; set; }
    }
}