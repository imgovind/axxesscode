﻿namespace Axxess.AgencyManagement.Enums
{
    public enum PatientStatus
    {
        Active = 1,
        Discharged = 2,
        Pending = 3,
        NonAdmission = 4,
        Deprecated = 5
    }
}
