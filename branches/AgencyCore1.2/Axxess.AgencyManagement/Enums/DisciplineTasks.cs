﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    using Axxess.Core.Infrastructure;

    public enum DisciplineTasks
    {
        [CustomDescription("No Discipline", "No Discipline")]
        NoDiscipline = 0,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SkilledNurseVisit = 1,
        [CustomDescription("OASIS-B Follow-up", "Follow-up")]
        OASISBFollowUp = 2,
        [CustomDescription("OASIS-B Recertification", "Recert.")]
        OASISBRecertification = 3,
        [CustomDescription("OASIS-B Resumption of Care", "ROC")]
        OASISBResumptionofCare = 4,
        [CustomDescription("OASIS-C Death", "Death")]
        OASISCDeath = 5,
        [CustomDescription("OASIS-C Discharge", "Discharge")]
        OASISCDischarge = 6,
        [CustomDescription("OASIS-C Follow-up", "Follow-up")]
        OASISCFollowUp = 7,
        [CustomDescription("OASIS-C Recertification", "Recert.")]
        OASISCRecertification = 8,
        [CustomDescription("OASIS-C Resumption of Care", "ROC")]
        OASISCResumptionofCare = 9,
        [CustomDescription("OASIS-B Discharge", "Discharge")]
        OASISBDischarge = 10,
        [CustomDescription("OASIS-B Start of Care", "SOC")]
        OASISBStartofCare = 11,
        [CustomDescription("OASIS-B Transfer", "Transfer")]
        OASISBTransfer = 12,
        [CustomDescription("OASIS-C Start of Care", "SOC")]
        OASISCStartofCare = 13,
        [CustomDescription("OASIS-C Transfer", "Transfer")]
        OASISCTransfer = 14,
        [CustomDescription("OASIS B Death at Home", "Death")]
        OASISBDeathatHome = 15,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNInsulinAM = 16,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNInsulinPM = 17,
        [CustomDescription("Discharge Summary", "DS")]
        DischargeSummary = 18,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        FoleyCathChange = 19,
        [CustomDescription("LVN Supervisory Visit", "SNV")]
        LVNSupervisoryVisit = 20,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNB12INJ = 21,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNBMP = 22,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNCBC = 23,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNHaldolInj = 24,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        PICCMidlinePlacement = 25,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        PRNFoleyChange = 26,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        PRNSNV = 27,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        PRNVPforCMP = 28,
        [CustomDescription("PT w/ INR", "PT")]
        PTWithINR = 29,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        PTWithINRPRNSNV = 30,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SkilledNurseHomeInfusionSD = 31,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SkilledNurseHomeInfusionSDAdditional = 32,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNAssessment = 33,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNDC = 34,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNEvaluation = 35,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNFoleyLabs = 36,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNFoleyChange = 37,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNInjection = 38,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNInjectionLabs = 39,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNLabsSN = 40,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNVPsychNurse = 41,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNVwithAideSupervision = 42,
        [CustomDescription("Skilled Nurse Visit", "SNV")]
        SNVDCPlanning = 43,
        [CustomDescription("PT Evaluation", "PT")]
        PTEvaluation = 44,
        [CustomDescription("PT Visit", "PT")]
        PTVisit = 45,
        [CustomDescription("PT Discharge", "PT")]
        PTDischarge = 46,
        [CustomDescription("OT Evaluation", "OT")]
        OTEvaluation = 47,
        [CustomDescription("OT Discharge", "OT")]
        OTDischarge = 48,
        [CustomDescription("OT Visit", "OT")]
        OTVisit = 49,
        [CustomDescription("ST Visit", "ST")]
        STVisit = 50,
        [CustomDescription("ST Evaluation", "ST")]
        STEvaluation = 51,
        [CustomDescription("ST Discharge", "ST")]
        STDischarge = 52,
        [CustomDescription("MSW Evaluation", "MSW")]
        MSWEvaluationAssessment = 53,
        [CustomDescription("HHA Visit", "HHA")]
        HHAideVisit = 54,
        [CustomDescription("HHA Supervisory Visit", "SNV")]
        HHAideSupervisoryVisit = 55,
        [CustomDescription("MSW Visit", "MSW")]
        MSWVisit = 56,
        [CustomDescription("MSW Discharge", "MSW")]
        MSWDischarge = 57,
        [CustomDescription("Dietician Visit", "DV")]
        DieticianVisit = 58,
        [CustomDescription("PT Visit", "PT")]
        PTAVisit = 59,
        [CustomDescription("PT Re-Evaluation", "PT")]
        PTReEvaluation = 60,
        [CustomDescription("OASIS-C Start of Care", "SOC")]
        OASISCStartofCarePT = 61,
        [CustomDescription("OASIS-C Resumption of Care", "ROC")]
        OASISCResumptionofCarePT = 62,
        [CustomDescription("OASIS-C Death", "Death")]
        OASISCDeathPT = 63,
        [CustomDescription("OASIS-C Discharge", "Discharge")]
        OASISCDischargePT = 64,
        [CustomDescription("OASIS-C Follow-up", "Follow-up")]
        OASISCFollowupPT = 65,
        [CustomDescription("OASIS-C Recertification", "Recert.")]
        OASISCRecertificationPT = 66,
        [CustomDescription("OASIS-C Transfer", "Transfer")]
        OASISCTransferPT = 67,
        [CustomDescription("OT Visit", "OT")]
        COTAVisit = 68,
        [CustomDescription("OASIS-C Resumption of Care", "ROC")]
        OASISCResumptionofCareOT = 69,
        [CustomDescription("OASIS-C Death", "Death")]
        OASISCDeathOT = 70,
        [CustomDescription("OASIS-C Discharge", "Discharge")]
        OASISCDischargeOT = 71,
        [CustomDescription("OASIS-C Follow-up", "Follow-up")]
        OASISCFollowupOT = 72,
        [CustomDescription("OASIS-C Recertification", "Recert.")]
        OASISCRecertificationOT = 73,
        [CustomDescription("OASIS-C Transfer", "Transfer")]
        OASISCTransferOT = 74,
        [CustomDescription("HHAide Care Plan", "HHA")]
        HHAideCarePlan = 75,
        [CustomDescription("MSW Assessment", "MSW")]
        MSWAssessment = 76,
        [CustomDescription("MSW Progress Note", "MSW")]
        MSWProgressNote = 77,
        [CustomDescription("485 Plan Of Care", "485")]
        HCFA485 = 78,
        [CustomDescription("486 Plan Of Care", "486")]
        HCFA486 = 79,
        [CustomDescription("Physician Order", "Order")]
        PhysicianOrder = 80,
        [CustomDescription("Physician Order", "Order")]
        PostHospitalizationOrder = 81,
        [CustomDescription("Medicaid POC", "POC")]
        MedicaidPOC = 82,
        [CustomDescription("Rap", "Rap")]
        Rap = 83,
        [CustomDescription("Final", "Final")]
        Final = 84,
        [CustomDescription("60 Day Summary/Case Conference", "SDS")]
        SixtyDaySummary = 85,
        [CustomDescription("Coordination of Care/ Transfer Summary", "TS")]
        TransferSummary = 86,
        [CustomDescription("Communication Note", "Communication Note")]
        CommunicationNote = 87,
        [CustomDescription("OASIS-C Transfer Discharge", "Transfer Discharge")]
        OASISCTransferDischarge = 88,
    }
}
