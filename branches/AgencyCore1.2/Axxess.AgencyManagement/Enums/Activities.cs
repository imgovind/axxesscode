﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;

    public enum Activities
    {
        [Description("Order")]
        Order,
        [Description("485")]
        Order485,
        [Description("Communication Note")]
        CommunicatioNote,
        [Description("Oasis-C Start Of Care")]
        StartOfCare,
        [Description("Oasis-C Resumption Of Care")]
        ResumptionOfCare,
        [Description("Oasis-C Discharge From Agency")]
        DischargeFromAgency,
        [Description("Oasis-C Recertification")]
        Recertification,
        [Description("Oasis-C Other Follow-up")]
        FollowUp,
        [Description("Oasis-C Death at Home")]
        DischargeFromAgencyDeath,
        [Description("Oasis-C Transfer to inpatient facility - Not Discharged")]
        TransferInPatientNotDischarged,
        [Description("Oasis-C Transfer to inpatient facility - Discharged")]
        TransferInPatientDischarged,
        [Description("Discharge Summary")]
        DischargeSummary,
        [Description("Skilled Nurse Visit")]
        SkilledNurseVisit,
        [Description("60 Day Summary")]
        SixtyDaySummary,
        [Description("LVN Supervisory Visit")]
        LVNSupervisoryVisit,
        MSW,
        HHA
    }
}
