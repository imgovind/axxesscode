﻿namespace Axxess.AgencyManagement.Enums
{
    public enum ReferralStatus : byte
    {
        Pending = 1,
        Admitted = 2,
        Rejected = 3
    }
}
