﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;
    using Axxess.Core.Extension;

    public static class PatientExtensions
    {
        public static List<PatientSelection> ForSelection(this IList<Patient> patients)
        {
            var result = new List<PatientSelection>();

            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    result.Add(new PatientSelection { Id = p.Id, FirstName = p.FirstName, LastName = p.LastName });
                });
            }

            return result;
        }
    }
}
