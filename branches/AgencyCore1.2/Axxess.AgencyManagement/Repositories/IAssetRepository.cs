﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IAssetRepository
    {
        bool Delete(Guid id);
        bool Add(Asset asset);
        Asset Get(Guid id, Guid agencyId);
    }
}
