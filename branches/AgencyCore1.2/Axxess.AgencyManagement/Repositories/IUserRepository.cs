﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IUserRepository
    {
        User Get(Guid id, Guid agencyId);
        User GetByLoginId(Guid loginId);
        bool Add(User user);
        bool Delete(Guid agencyId, Guid userId);
        bool Update(User user);
        bool UpdateProfile(User user);
        IList<User> GetAgencyUsers(Guid agencyId);
        IList<User> GetClinicalUsers(Guid agencyId);
        IList<User> GetCaseManagerUsers(Guid agencyId);
        IList<User> GetAgencyUsers(string query, Guid agencyId);
        void UpdateEvent(Guid agencyId, Guid patientId, Guid userId, UserEvent userEvent);
        bool Reassign(Guid agencyId, ScheduleEvent scheduleEvent, Guid userId);
        UserEvent GetEvent(Guid agencyId, Guid userId, Guid patientId, Guid eventId);
        bool DeleteScheduleEvent(Guid patientId, Guid eventId, Guid userId);
        bool RemoveScheduleEvent(Guid patientId, Guid eventId, Guid userId);
        bool UpdateEvent(UserEvent editEvent);
        IList<UserEvent> GetSchedules(Guid agencyId);
        IList<UserEvent> GetSchedule(Guid agencyId, Guid userId);
        IList<UserEvent> GetSchedule(Guid agencyId, Guid userId, DateTime start, DateTime end);
        List<Patient> GetUserPatients(Guid agencyId, Guid userId, byte statusId);
        bool Deactivate(Guid agencyId, Guid userId);
    }
}
