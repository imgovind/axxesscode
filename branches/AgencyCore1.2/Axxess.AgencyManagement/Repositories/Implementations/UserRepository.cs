﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Domain;
    using Extensions;

    using SubSonic.Repository;

    public class UserRepository : IUserRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        private readonly IWebConfiguration webConfig;

        public UserRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
            webConfig = Container.Resolve<IWebConfiguration>();
        }

        #endregion

        #region IUserRepository Members

        public bool Delete(Guid agencyId, Guid userId)
        {
            var user = database.Single<User>(u => u.AgencyId == agencyId && u.Id == userId);

            if (user != null)
            {
                user.IsDeprecated = true;
                user.Modified = DateTime.Now;
                database.Update<User>(user);
                return true;
            }

            return false;
        }

        public bool Deactivate(Guid agencyId, Guid userId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(userId, "userId");

            bool result = false;

            var user = database.Single<User>(u => u.AgencyId == agencyId && u.Id == userId);

            if (user != null)
            {
                user.Status = (int)UserStatus.Inactive;
                user.Modified = DateTime.Now;
                database.Update<User>(user);
                result = true;
            }

            return result;
        }

        public User Get(Guid id, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            var user = database.Single<User>(u => u.Id == id && u.AgencyId == agencyId && u.IsDeprecated == false);

            user.Profile = user.ProfileData.ToObject<UserProfile>();
            if (user.Permissions.IsNotNullOrEmpty())
            {
                user.PermissionsArray = user.Permissions.ToObject<List<string>>();
            }

            return user;
        }

        public User GetByLoginId(Guid loginId)
        {
            Check.Argument.IsNotEmpty(loginId, "loginId");

            var user = database.Single<User>(u => u.LoginId == loginId && u.IsDeprecated == false);

            if (user.Permissions.IsNotNullOrEmpty())
            {
                user.PermissionsArray = user.Permissions.ToObject<List<string>>();
            }

            return user;
        }

        public IList<User> GetAgencyUsers(Guid agencyId)
        {
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == false);
            users.ForEach(user => {
                if (user.ProfileData.IsNotNullOrEmpty())
                {
                    user.Profile = user.ProfileData.ToObject<UserProfile>();
                    user.EmailAddress = user.Profile.EmailWork;
                }
                if (user.Permissions.IsNotNullOrEmpty())
                {
                    user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                }
            });
            return users;
        }

        public List<Patient> GetUserPatients(Guid agencyId, Guid userId, byte statusId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");

            var patients = database.Find<Patient>(p => p.AgencyId == agencyId && p.UserId == userId && p.Status == statusId && p.IsDeprecated == false).ToList();
            if (patients == null)
            {
                patients = new List<Patient>();
            }

            var userSchedules = database.Find<UserSchedule>(us => us.UserId == userId && us.AgencyId == agencyId).ToList();
            userSchedules.ForEach(userSchedule =>
            {
                var patient = database.Single<Patient>(p => p.Id == userSchedule.PatientId && p.AgencyId == agencyId);
                if (patient != null && patient.Status == statusId)
                {
                    if (!patients.Exists(p => p.Id == patient.Id))
                    {
                        patients.Add(patient);
                    }
                }
            });
            return patients.OrderBy(p => p.LastName).ToList();
        }

        public IList<User> GetClinicalUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == false);
            users.ForEach(user =>
            {
                if (user.Roles.IsClinician() || user.Roles.IsDirectorOfNursing() || user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin())
                {
                    userList.Add(user);
                }
            });
            return userList;
        }

        public IList<User> GetCaseManagerUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == false);
            users.ForEach(user =>
            {
                if (user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin() || user.Roles.IsDirectorOfNursing())
                {
                    userList.Add(user);
                }
            });
            return userList;
        }

        public IList<User> GetAgencyUsers(string query, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(query, "query");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == false && u.FirstName.Contains(query)).Take(15).ToList();
        }

        public bool Add(User user)
        {
            if (user != null)
            {
                user.Id = Guid.NewGuid();
                if (user.AgencyRoleList.Count > 0)
                {
                    user.Roles = user.AgencyRoleList.ToArray().AddColons();
                }
                user.Status = (int)UserStatus.Active;
                user.ProfileData = user.Profile.ToXml();
                if (user.PermissionsArray.Count > 0)
                {
                    user.Permissions = user.PermissionsArray.ToXml();
                }
                user.Created = DateTime.Now;
                user.Modified = DateTime.Now;

                database.Add<User>(user);

                return true;
            }
            return false;
        }

        public bool Update(User user)
        {
            bool result = false;

            if (user != null)
            {
                var editUser = database.Single<User>(u => u.Id == user.Id && u.IsDeprecated == false);

                if (editUser != null)
                {
                    if (user.AgencyRoleList != null && user.AgencyRoleList.Count > 0)
                    {
                        editUser.Roles = user.AgencyRoleList.ToArray().AddColons();
                    }
                    editUser.FirstName = user.FirstName;
                    editUser.LastName = user.LastName;
                    if (user.TitleTypeOther.IsNotNullOrEmpty())
                    {
                        user.TitleType = user.TitleTypeOther;
                    }
                    editUser.TitleType = user.TitleType;
                    editUser.Profile = editUser.ProfileData.ToObject<UserProfile>();

                    editUser.Profile.AddressLine1 = user.Profile.AddressLine1;
                    editUser.Profile.AddressLine2 = user.Profile.AddressLine2;
                    editUser.Profile.AddressCity = user.Profile.AddressCity;
                    editUser.Profile.AddressZipCode = user.Profile.AddressZipCode;
                    editUser.Profile.AddressStateCode = user.Profile.AddressStateCode;

                    if (user.HomePhoneArray != null && user.HomePhoneArray.Count > 0)
                    {
                        editUser.Profile.PhoneHome = user.HomePhoneArray.ToArray().PhoneEncode();
                    }
                    if (user.MobilePhoneArray != null && user.MobilePhoneArray.Count > 0)
                    {
                        editUser.Profile.PhoneMobile = user.MobilePhoneArray.ToArray().PhoneEncode();
                    }

                    editUser.ProfileData = editUser.Profile.ToXml();
                    editUser.Status = user.Status;
                    editUser.Modified = DateTime.Now;

                    database.Update<User>(editUser);
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateProfile(User user)
        {
            bool result = false;

            if (user != null)
            {
                var userInfo = database.Single<User>(u => u.Id == user.Id && u.IsDeprecated == false);

                if (userInfo != null && userInfo.ProfileData.IsNotNullOrEmpty())
                {
                    userInfo.Profile = userInfo.ProfileData.ToObject<UserProfile>();

                    userInfo.Profile.AddressLine1 = user.Profile.AddressLine1;
                    userInfo.Profile.AddressLine2 = user.Profile.AddressLine2;
                    userInfo.Profile.AddressCity = user.Profile.AddressCity;
                    userInfo.Profile.AddressZipCode = user.Profile.AddressZipCode;
                    userInfo.Profile.AddressStateCode = user.Profile.AddressStateCode;

                    if (user.HomePhoneArray != null && user.HomePhoneArray.Count > 0)
                    {
                        userInfo.Profile.PhoneHome = user.HomePhoneArray.ToArray().PhoneEncode();
                    }
                    if (user.MobilePhoneArray != null && user.MobilePhoneArray.Count > 0)
                    {
                        userInfo.Profile.PhoneMobile = user.MobilePhoneArray.ToArray().PhoneEncode();
                    }

                    userInfo.ProfileData = userInfo.Profile.ToXml();
                    userInfo.Modified = DateTime.Now;

                    database.Update<User>(userInfo);
                    result = true;
                }
            }
            return result;
        }

        public void UpdateEvent(Guid agencyId,Guid patientId, Guid userId, UserEvent userEvent)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(userId, "userId");
            Check.Argument.IsNotNull(userEvent, "userEvent");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var userEpisode = database.Single<UserSchedule>(us => us.AgencyId==agencyId && us.PatientId == patientId && us.UserId == userId);

            try
            {
                if (userEpisode != null)
                {
                    List<UserEvent> events = userEpisode.Visits.ToString().ToObject<List<UserEvent>>();
                    events.Add(userEvent);
                    userEpisode.Visits = Convert.ToString(XElement.Parse(events.ToXml()));
                    userEpisode.Modified = DateTime.Now;
                    database.Update<UserSchedule>(userEpisode);
                }
                else if (userEpisode == null)
                {
                    var userSchedule = new UserSchedule();

                    try
                    {
                        userSchedule.Id = Guid.NewGuid();
                        userSchedule.PatientId = patientId;
                        userSchedule.UserId = userId;
                        userSchedule.AgencyId = agencyId;
                        List<UserEvent> events = new List<UserEvent>();
                        events.Add(userEvent);
                        userSchedule.Visits = Convert.ToString(XElement.Parse(events.ToXml()));
                        userSchedule.Created = DateTime.Now;
                        userSchedule.Modified = DateTime.Now;
                        database.Add<UserSchedule>(userSchedule);

                    }
                    catch (Exception e)
                    {
                        //TODO: Log Exception
                    }
                }
            }
            catch (Exception e)
            {
                //TODO Log Exception
            }
        }

        public bool Reassign(Guid agencyId,ScheduleEvent scheduleEvent, Guid userId)
        {
            bool result = false;
            Check.Argument.IsNotNull(scheduleEvent, "scheduleEvent");
            Check.Argument.IsNotEmpty(userId, "userId");

            var employeeEpisode = database.Single<UserSchedule>(e => e.PatientId == scheduleEvent.PatientId && e.UserId == scheduleEvent.UserId);
            UserEvent newEvent = null;
            try
            {
                if (employeeEpisode != null)
                {
                    List<UserEvent> events = employeeEpisode.Visits.ToString().ToObject<List<UserEvent>>();
                    events.ForEach(ev =>
                    {
                        if (ev.EventId ==scheduleEvent.EventId)
                        {
                            newEvent = ev;
                            events.Remove(ev);
                        }
                    });

                    employeeEpisode.Visits = Convert.ToString(XElement.Parse(events.ToXml()));
                    if (newEvent != null)
                    {
                        newEvent.UserId = userId;
                    }
                    else
                    {
                        newEvent = new UserEvent
                        {
                            EventId = scheduleEvent.EventId,
                            PatientId = scheduleEvent.PatientId,
                            EpisodeId = scheduleEvent.EpisodeId,
                            EventDate = scheduleEvent.EventDate,
                            Discipline = scheduleEvent.Discipline,
                            DisciplineTask = scheduleEvent.DisciplineTask,
                            Status = scheduleEvent.Status,
                            IsMissedVisit = scheduleEvent.IsMissedVisit,
                            UserId = userId,
                            TimeIn = scheduleEvent.TimeIn,
                            TimeOut = scheduleEvent.TimeOut,
                            IsDeprecated=scheduleEvent.IsDeprecated,

                        };
                    }
                    UpdateEvent(agencyId,scheduleEvent.PatientId, userId, newEvent);
                    database.Update<UserSchedule>(employeeEpisode);
                    result = true;
                }
                else
                {
                    newEvent = new UserEvent {
                        EventId=scheduleEvent.EventId,
                        PatientId=scheduleEvent.PatientId,
                        EpisodeId=scheduleEvent.EpisodeId,
                        EventDate=scheduleEvent.EventDate,
                        Discipline=scheduleEvent.Discipline,
                        DisciplineTask=scheduleEvent.DisciplineTask,
                        Status=scheduleEvent.Status,
                        IsMissedVisit=scheduleEvent.IsMissedVisit,
                        UserId=userId,
                        TimeIn=scheduleEvent.TimeIn,
                        TimeOut=scheduleEvent.TimeOut,
                        IsDeprecated = scheduleEvent.IsDeprecated,
                        
                    };
                    UpdateEvent(agencyId, scheduleEvent.PatientId, userId, newEvent);
                    result = true;
                }
            }
            catch (Exception e)
            {
                return result;
            }
            return result;
        }

        public bool DeleteScheduleEvent(Guid patientId, Guid eventId, Guid userId)
        {
            bool result = false;
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var userEpisode = database.Single<UserSchedule>(e => e.PatientId == patientId && e.UserId == userId);
            if (userEpisode != null)
            {
                List<UserEvent> events = userEpisode.Visits.ToString().ToObject<List<UserEvent>>();
                events.ForEach(ev =>
                {
                    if (ev.EventId == eventId)
                    {
                        ev.IsDeprecated = true;
                    }
                });
                userEpisode.Visits = Convert.ToString(XElement.Parse(events.ToXml()));
                database.Update<UserSchedule>(userEpisode);
                result = true;
            }

            return result;
        }

        public bool RemoveScheduleEvent(Guid patientId, Guid eventId, Guid userId)
        {
            bool result = false;
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var userEpisode = database.Single<UserSchedule>(e => e.PatientId == patientId && e.UserId == userId);
            if (userEpisode != null)
            {
                List<UserEvent> events = userEpisode.Visits.ToString().ToObject<List<UserEvent>>();
                events.ForEach(ev =>
                {
                    if (ev.EventId == eventId)
                    {
                        events.Remove(ev);
                    }
                });
                userEpisode.Visits = Convert.ToString(XElement.Parse(events.ToXml()));
                database.Update<UserSchedule>(userEpisode);
                result = true;
            }

            return result;
        }

        public UserEvent GetEvent(Guid agencyId, Guid userId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            Check.Argument.IsNotNull(eventId, "eventId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var episode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == userId);
            UserEvent evnt = null;
            if (episode != null)
            {
                evnt = episode.Visits.ToObject<List<UserEvent>>().Where(e => e.EventId == eventId).FirstOrDefault();
            }

            return evnt;
        }
       
        public IList<UserEvent> GetSchedules(Guid agencyId)
        {
            var userEvents = new List<UserEvent>();
            var userSchedules = database.Find<UserSchedule>(us => us.AgencyId == agencyId);
            userSchedules.ForEach(userSchedule =>
            {
                var userVisits = userSchedule.Visits.IsNotNullOrEmpty() ? userSchedule.Visits.ToObject<List<UserEvent>>() : new List<UserEvent>();
                if (userVisits.Count > 0)
                {
                    userVisits.ForEach(userVisit =>
                    {
                        var eventDate = userVisit.EventDate.IsNotNullOrEmpty() ? DateTime.Parse(userVisit.EventDate) : DateTime.MaxValue;
                        if (eventDate != DateTime.MaxValue && eventDate < DateTime.Now.AddDays(7))
                        {
                            userEvents.Add(userVisit);
                        }
                    });
                }
            });
            return userEvents;
        }

        public IList<UserEvent> GetSchedule(Guid agencyId, Guid userId)
        {
            var userEvents = new List<UserEvent>();
            var userSchedules = database.Find<UserSchedule>(us => us.UserId == userId && us.AgencyId == agencyId);
            userSchedules.ForEach(userSchedule =>
            {
                var userVisits = userSchedule.Visits.IsNotNullOrEmpty() ? userSchedule.Visits.ToObject<List<UserEvent>>() : new List<UserEvent>();
                if (userVisits.Count > 0)
                {
                    userVisits.ForEach(userVisit =>
                    {
                        if (userVisit.Discipline != Disciplines.Claim.ToString())
                        {
                            userEvents.Add(userVisit);
                        }
                    });
                }
            });
            return userEvents;
        }

        public IList<UserEvent> GetSchedule(Guid agencyId, Guid userId, DateTime start, DateTime end)
        {
            var userEvents = new List<UserEvent>();
            var userSchedules = database.Find<UserSchedule>(us => us.UserId == userId && us.AgencyId == agencyId);
            userSchedules.ForEach(userSchedule =>
            {
                var userVisits = userSchedule.Visits.IsNotNullOrEmpty() ? userSchedule.Visits.ToObject<List<UserEvent>>() : new List<UserEvent>();
                if (userVisits.Count > 0)
                {
                    userVisits.ForEach(userVisit =>
                    {
                        userEvents.Add(userVisit);
                    });
                }
            });
            return userEvents;
        }

        public bool UpdateEvent(UserEvent userEvent)
        {
            bool result = false;

            if (userEvent != null)
            {
                var userEpisode = database.Single<UserSchedule>(e => e.UserId == userEvent.UserId && e.PatientId == userEvent.PatientId);
                if (userEpisode != null && !string.IsNullOrEmpty(userEpisode.Visits))
                {
                    var events = userEpisode.Visits.ToObject<List<UserEvent>>();
                    events.ForEach(e =>
                    {
                        if (e.EventId == userEvent.EventId)
                        {
                            e.UserId = userEvent.UserId;
                            e.Discipline = userEvent.Discipline;
                            e.PatientId = userEvent.PatientId;
                            e.EventDate = userEvent.EventDate;
                            e.Status = userEvent.Status;
                            e.EpisodeId = userEvent.EpisodeId;
                            e.DisciplineTask = userEvent.DisciplineTask;
                            e.Discipline = userEvent.Discipline;
                            e.IsMissedVisit = userEvent.IsMissedVisit;
                            e.IsDeprecated = userEvent.IsDeprecated;
                        }

                    });
                    userEpisode.Modified = DateTime.Now;
                    userEpisode.Visits = events.ToXml();
                    database.Update<UserSchedule>(userEpisode);
                    result = true;
                }
            }
            return result;
        }

        #endregion

    }
}
