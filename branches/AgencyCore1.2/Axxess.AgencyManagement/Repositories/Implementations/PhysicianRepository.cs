﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Domain;

    public class PhysicianRepository : IPhysicianRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PhysicianRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }
        #endregion

        #region IPhysicianRepository Members
            
        public bool Add(AgencyPhysician physician)
        {
            var result = false;
            if (physician != null)
            {
                physician.Id = Guid.NewGuid();
                if (physician.PhoneWorkArray != null && physician.PhoneWorkArray.Count >= 2)
                {
                    physician.PhoneWork = physician.PhoneWorkArray.ToArray().PhoneEncode();
                }
                if (physician.FaxNumberArray != null && physician.FaxNumberArray.Count >= 2)
                {
                    physician.FaxNumber = physician.FaxNumberArray.ToArray().PhoneEncode();
                }
                physician.Created = DateTime.Now;
                physician.Modified = DateTime.Now;
                database.Add<AgencyPhysician>(physician);
                result = true;
            }
            return result;
        }

        public bool Edit(AgencyPhysician physician)
        {
            bool result = false;
            if (physician != null)
            {
                physician.PhoneWork = physician.PhoneWorkArray.ToArray().PhoneEncode();
                physician.FaxNumber = physician.FaxNumberArray.ToArray().PhoneEncode();
                var physicianInfo = database.Single<AgencyPhysician>(p => p.Id == physician.Id);
                if (physicianInfo != null)
                {
                    physicianInfo.FirstName = physician.FirstName;
                    physicianInfo.LastName = physician.LastName;
                    physicianInfo.PhoneWork = physician.PhoneWork;
                    physicianInfo.FaxNumber = physician.FaxNumber;
                    physicianInfo.EmailAddress = physician.EmailAddress;
                    physicianInfo.NPI = physician.NPI;
                    physicianInfo.AddressLine1 = physician.AddressLine1;
                    physicianInfo.AddressLine2 = physician.AddressLine2;
                    physicianInfo.AddressCity = physician.AddressCity;
                    physicianInfo.AddressStateCode = physician.AddressStateCode;
                    physicianInfo.AddressZipCode = physician.AddressZipCode;
                    database.Update<AgencyPhysician>(physicianInfo);
                    result = true;
                }
            }
            return result;
        }

        public bool Delete(Guid agencyId, Guid id)
        {
            var physician = database.Single<AgencyPhysician>(p => p.AgencyId == agencyId && p.Id == id);
            if (physician != null)
            {
                physician.IsDeprecated = true;
                physician.Modified = DateTime.Now;
                database.Update<AgencyPhysician>(physician);
                return true;
            }
            return false;
        }

        public bool Update(AgencyPhysician physician)
        {
            bool result = false;
            if (physician != null)
            {
                var physicianInfo = database.Single<AgencyPhysician>(p => p.Id == physician.Id);
                if (physicianInfo != null)
                {
                    physicianInfo.FirstName = physician.FirstName;
                    physicianInfo.LastName = physician.LastName;

                    if (physician.PhoneWorkArray != null && physician.PhoneWorkArray.Count >= 2)
                    {
                        physician.PhoneWork = physician.PhoneWorkArray.ToArray().PhoneEncode();
                    }
                    if (physician.FaxNumberArray != null && physician.FaxNumberArray.Count >= 2)
                    {
                        physician.FaxNumber = physician.FaxNumberArray.ToArray().PhoneEncode();
                    }
                    physicianInfo.NPI = physician.NPI;
                    physicianInfo.EmailAddress = physician.EmailAddress;
                    physicianInfo.AddressLine1 = physician.AddressLine1;
                    physicianInfo.AddressLine2 = physician.AddressLine2;
                    physicianInfo.AddressCity = physician.AddressCity;
                    physicianInfo.AddressStateCode = physician.AddressStateCode;
                    physicianInfo.AddressZipCode = physician.AddressZipCode;
                    physicianInfo.Modified = DateTime.Now;
                    database.Update<AgencyPhysician>(physicianInfo);
                    result = true;
                }
            }
            return result;
        }

        public bool Link(Guid patientId, Guid physicianId , bool isPrimary)
        {
            var result = false;
            if (!patientId.IsEmpty() && !physicianId.IsEmpty())
            {
                var patientPhysician = new PatientPhysician
                {
                    PatientId = patientId,
                    PhysicianId = physicianId,
                    IsPrimary=isPrimary
                };
                database.Add<PatientPhysician>(patientPhysician);
                result = true;
            }
            return result;
        }

        public bool Unlink(Guid patientId, Guid physicianId)
        {
            var result = false;
            if (!patientId.IsEmpty() && !physicianId.IsEmpty())
            {
                database.DeleteMany<PatientPhysician>(pp => pp.PatientId == patientId && pp.PhysicianId == physicianId);
                result = true;
            }
            return result;
        }

        public bool UnlinkAll(Guid patientId)
        {
            var result = false;
            if (!patientId.IsEmpty())
            {
                database.DeleteMany<PatientPhysician>(pp => pp.PatientId == patientId);
                result = true;
            }
            return result;
        }

        public AgencyPhysician Get(Guid physicianId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(physicianId, "physicianId");
            return database.Single<AgencyPhysician>(p => p.Id == physicianId && p.AgencyId == agencyId && p.IsDeprecated == false);
        }

        public IList<AgencyPhysician> GetAgencyPhysicians(Guid agencyId)
        {
            return database.Find<AgencyPhysician>(p => p.AgencyId == agencyId && p.IsDeprecated == false);
        }

        public IList<Patient> GetPhysicanPatients(Guid physicianId)
        {
            var patients = new List<Patient>();
            var patientPhysicians = database.Find<PatientPhysician>(p => p.PhysicianId == physicianId);
            if (patientPhysicians.Count > 0)
            {
                patientPhysicians.ForEach(pp =>
                {
                    var patient = database.Single<Patient>(p => p.Id == pp.PatientId);
                    if (patient != null)
                    {
                        patients.Add(patient);
                    }
                });
            }

            return patients;
        }

        public bool DoesPhysicianExist(Guid patientId, Guid physicianId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(physicianId, "physicianId");
            bool result = false;
            var patientPhysician = database.Find<PatientPhysician>(p => p.PatientId == patientId);
            foreach (PatientPhysician physician in patientPhysician)
            {
                if (physician.PhysicianId == physicianId) {
                    result = true;
                    break;
                }
            }

            return result;
        }

        public IList<AgencyPhysician> GetPatientPhysicians(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            IList<AgencyPhysician> physicians = new List<AgencyPhysician>();
            var patientPhysician = database.Find<PatientPhysician>(p => p.PatientId == patientId);
            foreach (PatientPhysician physician in patientPhysician)
            {
                var agencyPhysician = database.Single<AgencyPhysician>(p => p.Id == physician.PhysicianId);
                agencyPhysician.Primary = physician.IsPrimary;
                physicians.Add(agencyPhysician);
            }

            return physicians;
        }

        public bool SetPrimary(Guid patientId, Guid physicianId)
        {
            bool result = false;
            bool flag = false;
            var patientPhysicians = database.Find<PatientPhysician>(p => p.PatientId == patientId);
            if (patientPhysicians != null)
            {
                foreach (PatientPhysician contat in patientPhysicians)
                {
                    if (contat.PhysicianId == physicianId)
                    {
                        contat.IsPrimary = true;
                        flag = true;
                    }
                    else
                    {
                        contat.IsPrimary = false;
                    }
                }
                if (flag)
                {
                    database.UpdateMany<PatientPhysician>(patientPhysicians);
                    result = true;
                }
            }
            return result;
        }

        public AgencyPhysician GetByPatientId(Guid physicianId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(physicianId, "physicianId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var physicianContact = database.Single<AgencyPhysician>(p => p.Id == physicianId);
            var patientPhysician = database.Single<PatientPhysician>(p => p.PatientId == patientId && p.PhysicianId == physicianId);
            physicianContact.Primary = patientPhysician.IsPrimary;
            
            return physicianContact;
        }

        #endregion
    }
}
