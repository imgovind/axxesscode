﻿namespace Axxess.AgencyManagement.Domain
{
    using System.Xml.Serialization;

    [XmlRoot()]
    public class ChargeRate
    {
        [XmlElement]
        public string RateDiscipline { get; set; }
        [XmlElement]
        public string Charge { get; set; }
        [XmlElement]
        public string ChargeType { get; set; }
    }
}
