﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using System.Text;

    public class Message : EntityBase
    {
        #region Members
        public Guid Id { get; set; }
        public Guid FromId { get; set; }
        public string FromName { get; set; }
        public Guid RecipientId { get; set; }
        public string RecipientNames { get; set; }
        public Guid PatientId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool MarkAsRead { get; set; }
        public DateTime Created { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string Date { get { return string.Format("{0: MMM d, hh:mm tt}", this.Created); } }
        [SubSonicIgnore]
        public string MessageDate { get { return this.Created.ToString("F"); } }
        [SubSonicIgnore]
        public List<Guid> Recipients { get; set; }
        [SubSonicIgnore]
        public string ReplyForwardBody
        {
            get
            {
                var sb = new StringBuilder();
                sb.AppendLine("<br />");
                sb.AppendLine("<br />");
                sb.AppendLine("<br />");
                sb.Append("_______________________________________________________________________________________________________________");
                sb.AppendLine("<br />");
                sb.AppendFormat("From: {0}", this.FromName);
                sb.AppendLine("<br />");
                sb.AppendFormat("Sent: {0}", this.MessageDate);
                sb.AppendLine("<br />");
                sb.AppendFormat("To: {0}", this.RecipientNames);
                sb.AppendLine("<br />");
                sb.AppendFormat("Subject: {0}", this.Subject);
                sb.AppendLine("<br />");
                sb.AppendLine("<br />");
                sb.Append(this.Body);
                return sb.ToString();
            }
        }
        [SubSonicIgnore]
        public string PatientName { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.Recipients.Count == 0, "Please select at least one recipient"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Subject), "Subject is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Body), "Body is required. <br />"));
        }

        #endregion

    }
}
