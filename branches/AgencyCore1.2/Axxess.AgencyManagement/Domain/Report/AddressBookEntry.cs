﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core.Extension;

    public class AddressBookEntry : IAddress
    {
        #region Members

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneMobile { get; set; }

        #endregion

        #region IAddress members

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string EmailAddress { get; set; }

        #endregion

        #region Domain

        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }

        #endregion
    }
}
