﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Enums;
    using System.Xml.Serialization;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    [KnownType(typeof(Medication))]
    [XmlRoot()]
    [XmlInclude(typeof(MedicationType))]
    public class Medication
    {
        [XmlAttribute]
        [ScaffoldColumn(false)]
        public Guid Id { get; set; }

        [XmlAttribute]
        [DisplayName("Start Date")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [XmlAttribute]
        [DisplayName("LS")]
        public bool IsLongStanding { get; set; }

        [XmlAttribute]
        [UIHint("MedicationDosage")]
        [DataType(DataType.Text)]
        public string MedicationDosage { get; set; }

        [XmlAttribute]
        [UIHint("MedicationRoute")]
        [DataType(DataType.Text)]
        public string Route { get; set; }

        [XmlAttribute]
        [UIHint("MedicationFrequency")]
        public string Frequency { get; set; }


        [XmlElement]
        [UIHint("MedicationType"), Required]
        public MedicationType MedicationType { get; set; }

        [XmlAttribute]
        [UIHint("MedicationClassification")]
        public string Classification { get; set; }

        [XmlAttribute]
        [UIHint("MedicationCategory")]
        public string MedicationCategory { get; set; }

        [XmlAttribute]
        [UIHint("DCDate")]
        public DateTime DCDate { get; set; }


        [XmlAttribute]
        public DateTime LastChangedDate { get; set; }

        [XmlIgnore]
        [UIHint("DischargeUrl")]
        public string DischargeUrl
        {
            get;
            set;
        }
        [XmlIgnore]
        public string DCDateFormated
        {
            get 
            {
                if (this.DCDate != null && !(this.DCDate <= DateTime.MinValue))
                {
                    return this.DCDate.ToShortDateString();
                }
                else
                {
                    return string.Empty;
                }
            }
           
        }
    }
}
