﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class CommunicationNote : EntityBase
    {
        #region Members
        
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid UserId { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        public string Text { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string DisplayName { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.PatientId.IsEmpty(), "Patient is required."));
            AddValidationRule(new Validation(() => !this.Created.ToString().IsValidDate(), "Created Date is invalid."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Text), "Text is required."));
        }

        #endregion
    }
}
