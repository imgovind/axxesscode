﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;
    
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Extension;

    [XmlRoot()]
    [DataContract]
    public class ScheduleEvent
    {
        public ScheduleEvent()
        {
            this.UserId = Guid.Empty;
            this.EventId = Guid.Empty;
            this.EventDate = string.Empty;
            this.Assets = new List<Guid>();
        }

        [XmlElement]
        [DataMember]
        public Guid EventId { get; set; }

        [XmlElement]
        [DataMember]
        public int DisciplineTask { get; set; }

        [XmlElement]
        [DataMember]
        public Guid UserId { get; set; }

        [XmlElement]
        [DataMember]
        public string UserName { get; set; }

        [XmlElement]
        [DataMember]
        public string EventDate { get; set; }

        [XmlElement]
        [DataMember]
        public string TargetDate { get; set; }

        [XmlElement]
        [DataMember]
        public string Status { get; set; }

        [XmlElement]
        [DataMember]
        public string Discipline { get; set; }

        [XmlElement]
        [DataMember]
        public Guid EpisodeId { get; set; }

        [XmlElement]
        [DataMember]
        public Guid PatientId { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsBillable { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsMissedVisit { get; set; }

        [XmlElement]
        [DataMember]
        public string TimeIn { get; set; }

        [XmlElement]
        [DataMember]
        public string TimeOut { get; set; }
       
        [XmlElement]
        [DataMember]
        public string Surcharge { get; set; }

        [XmlElement]
        [DataMember]
        public string AssociatedMileage { get; set; }

        [XmlElement]
        [DataMember]
        public string ReturnReason { get; set; }

        [XmlElement]
        [DataMember]
        public string Comments { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsDeprecated { get; set; }

        [XmlIgnore]
        public List<NotesQuestion> Questions { get; set; }

        [XmlIgnore]
        public string StatusName{ get; set; }

        [XmlIgnore]
        public string DisciplineTaskName
        {
            get
            {
                if (Enum.IsDefined(typeof(DisciplineTasks), this.DisciplineTask)) { return ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), this.DisciplineTask)).GetDescription(); } else { return ""; };
            }
        }

        [XmlIgnore]
        public string PatientName { get; set; }

        [XmlIgnore]
        public string Url { get; set; }

        [XmlIgnore]
        public string PrintUrl { get; set; }

        [XmlIgnore]
        public string ActionUrl { get; set; }

        [XmlIgnore]
        public string AttachmentUrl
        {
            get
            {
                if (this.Assets != null && this.Assets.Count > 0)
                {
                    return "<img title=\"Attachments\" alt=\"Attachments\" src=\"/Images/icons/paperclip.gif\" />";
                }
                return string.Empty;
            }
        }

        [XmlIgnore]
        public string MissedVisitComments { get; set; }

        [XmlIgnore]
        public string EpisodeNotes { get; set; }

        public List<Guid> Assets { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [XmlIgnore]
        public string StatusComment
        {
            get
            {
                var result = "";
                if (this.IsMissedVisit && this.MissedVisitComments.IsNotNullOrEmpty())
                {
                    result += this.MissedVisitComments +"\r\n";
                }
                if (this.ReturnReason.IsNotNullOrEmpty())
                {
                    result += "Reason:\r\n" + this.ReturnReason;
                }
                return result;
            }

        }

    }
}
