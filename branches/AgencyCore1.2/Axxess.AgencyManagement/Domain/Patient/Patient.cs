﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;

    public class Patient : EntityBase
    {
        #region Members
        
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid UserId { get; set; }
        public Guid AgencyLocationId { get; set; }
        public string PatientIdNumber { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [SubSonicNullString()]
        [SubSonicStringLength(1)]
        public string MiddleInitial { get; set; }
        [XmlIgnore]
        [ScriptIgnore]
        public string SSN { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string Ethnicities { get; set; }
        public string MaritalStatus { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneMobile { get; set; }
        public string EmailAddress { get; set; }
        public DateTime StartofCareDate { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        public string PrimaryInsurance { get; set; }
        public string SecondaryInsurance { get; set; }
        public string TertiaryInsurance { get; set; }
        public string DME { get; set; }
        public string OtherDME { get; set; }
        public string Payer { get; set; }
        public string ServicesRequired { get; set; }
        public string PaymentSource { get; set; }
        public string OtherPaymentSource { get; set; }
        public string AdmissionSource { get; set; }
        public Guid ReferrerPhysician { get; set; }
        public DateTime ReferralDate { get; set; }
        public string OtherReferralSource { get; set; }
        public Guid InternalReferral { get; set; }
        public Guid CaseManagerId { get; set; }
        public int Triage { get; set; }
        public Guid PhotoId { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime NonAdmissionDate { get; set; }
        public string NonAdmissionReason { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }

        #endregion

        #region Domain
        [SubSonicIgnore]
        public Guid ReferralId { get; set; }
        [SubSonicIgnore]
        public string Branch { get; set; }
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                return EnumExtensions.GetDescription((PatientStatus)Enum.ToObject(typeof(PatientStatus), this.Status));
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string PhoneHomeFormatted { get { return this.PhoneHome.ToPhone(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public bool ShouldCreateEpisode { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public bool ShouldRequestElligibility { get; set; }
        [SubSonicIgnore]
        public string DOBFormatted { get { return this.DOB.ToShortDateString(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> EthnicRaces { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<Guid> AgencyPhysicians { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {

                return string.Format("{0} {1} {2} {3}", this.AddressLine1.Trim(), this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
            }
        }
        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}", this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
            }
        }
        [SubSonicIgnore]
        public string StartOfCareDateFormatted { get { return this.StartofCareDate.ToShortDateString(); } }
        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.Created.ToShortDateString(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PaymentSources { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> ServicesRequiredCollection { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> DMECollection { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public DateTime EpisodeStartDate { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public PatientEmergencyContact EmergencyContact { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public AgencyPhysician Physician { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public IList<PatientEmergencyContact> EmergencyContacts { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public IList<AgencyPhysician> PhysicianContacts { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PhoneHomeArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PhoneMobileArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PharmacyPhoneArray { get; set; }
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string PrimaryInsuranceName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string SecondaryInsuranceName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string TertiaryInsuranceName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string CaseManagerName { get; set; }
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Patient first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Patient last name is required."));
            AddValidationRule(new Validation(() => this.DOB == DateTime.MinValue, "Patient date of birth is required."));
            AddValidationRule(new Validation(() => this.StartofCareDate == DateTime.MinValue, "Start of care date is required."));
            AddValidationRule(new Validation(() => this.EpisodeStartDate == DateTime.MinValue, "Episode start date is required."));
            AddValidationRule(new Validation(() => this.EpisodeStartDate < this.StartofCareDate, "Episode start date is must be greater or equal to the start of care date."));
            AddValidationRule(new Validation(() => !this.DOB.ToString().IsValidDate(), "Date of birth is not a valid date."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Gender), "Patient gender has to be selected."));
            AddValidationRule(new Validation(() => (this.EmailAddress == null ? !string.IsNullOrEmpty(this.EmailAddress) : !this.EmailAddress.IsEmail()), "Patient e-mail is not in a valid format."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Patient address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Patient city is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Patient state is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Patient zip is required."));
            if (this.SSN.IsNotNullOrEmpty())
            {
                AddValidationRule(new Validation(() => !this.SSN.IsSSN(), "Patient SSN is not in valid format."));
            }
            AddValidationRule(new Validation(() => this.AgencyLocationId.IsEmpty(), "Branch/Location is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.StartofCareDate.ToString()), "Patient Start of care date is required."));

            if (Ethnicities.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.EthnicRaces.Count == 0, "Patient Race/Ethnicity is required."));
            }

            if (PaymentSource.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PaymentSources.Count == 0, "Patient Payment Source is required."));
            }

            if (this.PhoneHome.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PhoneHomeArray.Count == 0, "Patient phone is required."));
            }
            if (this.ReferralDate.IsValid())
            {
                AddValidationRule(new Validation(()=>this.StartofCareDate < this.ReferralDate,"Referral date must be less than or equal to the start of care."));
            }
            AddValidationRule(new Validation(() => this.EmergencyContact == null, "Emergency contact details are required."));

        }

        #endregion

    }
}
