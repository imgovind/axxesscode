﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/validationerror/2010/05/")]
    public class ValidationError
    {
        [DataMember]
        public string ErrorDup { get; set; }
        [DataMember]
        public string ErrorType { get; set; }
        [DataMember]
        public string Description { get; set; }

        public override string ToString()
        {
            return string.Format("Type: {0} Dup: {1} Desc: {2}", this.ErrorType, this.ErrorDup, this.Description);
        }
    }
}
