﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Management;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.ComponentModel;

using Axxess.Api;
using Axxess.Core;
using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using SubSonic.Repository;

using SubSonic.Query;
using SubSonic.SqlGeneration.Schema;

using AutoMapper;

using Axxess.AgencyManagement.App;
using Axxess.AgencyManagement.App.Security;

using Enyim.Caching;
using Enyim.Caching.Memcached;

using NorthScale.Store;
using NorthScale.Store.Configuration;

using log4net;
using Enyim.Caching.Configuration;
using System.Net;
using Axxess.AgencyManagement.Domain;

namespace Axxess.ConsoleApp
{
    class Program
    {
        private static NorthScaleClient client;
        private static NorthScaleClient Client
        {
            get
            {
                if (client == null)
                {
                    LoadConfig();
                }
                return client;
            }
        }
        private static void LoadConfig()
        {
            var config = new NorthScaleClientConfiguration();

            config.SocketPool.ReceiveTimeout = new TimeSpan(0, 0, 2);
            config.SocketPool.DeadTimeout = new TimeSpan(0, 0, 10);

            config.Urls.Add(new Uri("http://10.0.1.6:8091/pools/default"));
            //config.Urls.Add(new Uri("http://10.0.5.42:8091/pools/default"));
            config.Credentials = new NetworkCredential("Administrator", "@gencyC0re");

            client = new NorthScaleClient(config, "default");
        }

        static void Main(string[] args)
        {
            //log4net.Config.XmlConfigurator.Configure();

            //Client.Remove("Insurances");
            //if (result != null)
            //{
            //    //Console.WriteLine(principal.Identity.Name);
            //}
            //Client.Store(StoreMode.Set, "Day", DateTime.Now.ToString("d"), DateTime.Now.AddMinutes(50));
            //for (int i = 1; i <= 100000; i++)
            //{
            //var dateTime = Client.Get("Day");
            //    if (dateTime != null)
            //    {
            //        var text = dateTime.ToString();

            //        Console.WriteLine(text);
            //    }
            //    else
            //    {
            //        Console.WriteLine("Not found");
            //    }
            //}

            //var mcc = new MemcachedClientConfiguration();
            //mcc.AddServer("10.0.1.6:11211");
            //mcc.AddServer("10.0.1.10:11211");

            //mcc.SocketPool.ReceiveTimeout = new TimeSpan(0, 0, 4);
            //mcc.SocketPool.ConnectionTimeout = new TimeSpan(0, 0, 4);
            //mcc.SocketPool.DeadTimeout = new TimeSpan(0, 0, 10);

            //var mc = new MemcachedClient(mcc);
            //mc.Store(StoreMode.Set, "DateTime", DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss"));

            //if (mc.Get("DateTime") != null)
            //{
            //    var text = mc.Get("DateTime").ToString();

            //    Console.WriteLine(text);
            //}
            //return;

            //var nc = new NorthScaleClient(nscc, "content");

            //var stats1 = nc.Stats("slabs");
            //foreach (var kvp in stats1.GetRaw("curr_connections"))
            //    Console.WriteLine("{0} -> {1}", kvp.Key, kvp.Value);

            //var nc2 = new NorthScaleClient(nscc, "content");

            //var stats2 = nc2.Stats();
            //foreach (var kvp in stats2.GetRaw("curr_connections"))
            //    Console.WriteLine("{0} -> {1}", kvp.Key, kvp.Value);
            //Permissions[] permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            //foreach (Permissions permission in permissions)
            //{
            //    ulong id = (ulong)permission;
            //    Permission p = permission.GetPermission();
            //    Console.WriteLine("{0}: {1}", id, p.Description);
            //}
            //Console.WriteLine(DateTime.Parse("9/6/2010").ToString("MM/dd/yyyy"));
            //Console.WriteLine(string.Format("{0:MM/dd/yyyy}", DateTime.Parse("9/6/2010")));
            //User user = new User();
            //user.Profile = new UserProfile()
            //{
            //    AddressCity = "Dallas",
            //    AddressLine1 = "5645 Abrams Street",
            //    AddressLine2 = "Suite 1000",
            //    AddressStateCode = "TX",
            //    AddressZipCode = "75232",
            //    DOB = DateTime.Now.AddYears(-30),
            //    Gender = "Male",
            //    HasMilitaryService = false,
            //    MaritalStatus = "Married",
            //    PhoneHome = "2144345755511",
            //    PhoneMobile ="3234315251212",
            //    SSN="123456789"
            //};
            //var profile = user.Profile.ToXml();
            //Console.WriteLine(profile);

            //string xmlLocation = Path.GetFullPath("OasisC-Plan-of-care.xml");
            //XElement planofCareXml = XElement.Load(xmlLocation);

            //if (planofCareXml != null)
            //{
            //    var elements = planofCareXml.Descendants(XName.Get("Item"));
            //    if (elements != null && elements.Count() > 0)
            //    {
            //        elements.ForEach(e =>
            //        {
            //            Console.WriteLine("Id: {0} Value: {1}", e.Attribute(XName.Get("id")).Value, e.Attribute(XName.Get("value")).Value);
            //        });
            //    }
            //}
            //Console.WriteLine(Environment.CurrentDirectory);
            //Console.WriteLine("Current directory: \"{0}\"", Directory.GetCurrentDirectory());

            ValidationAndGrouper("B1          00        C-072009    02.00364649717  1.0      1234567789                                                      123456                 N                       WH123420101029        1     WHITNEY            HOUSTON   TX      77074  837636635509474646360              120101029 212345678910012010102901001000 0100000000000                   1 250.60         250.60                     100100000 250.6003                                             0001      000010                                                       01  01     01       01              0310000001  0100020202             0100000020  01  02  01                  01  01                  03                                                                                     0  0              123456789101                                                                                    0210         120101028000100                                                        10              0011000002       04010102040201          01  00  00  00  00  00     2   1   00300    00  0        01010102020202020100000     00000010001020201   01  01010101050500050101020401010101010101                                                                                                                                                                                                                                                                                                     %");
            //ValidationAndGrouper("B1          00        C-072009    02.001234567891.0  7002054                                                               679433                 N         AXX343              20100628201007010FRANCIS      JERMY                TX75243      53453534534 03534534540345435353     019550707 1          1012010070303000010 0000100000000           201006290 001.9  002.0   001.0  001.1  001.9  002.1 100000000 001.0 01 001.1 02 001.9 03 002.0 04 002.1 02 002.2 031000      010000                                                       00  03     01       01              0101000000  0000020401             0100000020  02  02  03                  02  02                  01                                                                                     0  0                        02 001.0  002.0  002.9  003.0  003.29 006.8  001.1  002.0  002.1  004.20 005.0  006.1 0000         120100701100000 002.1  002.2  002.3  002.9   00.01  00.02  00.03  00.0900 002.2  002.3 0100000003       07010201010000                                                00    00  0          0100020004000500               0001020101000  00  020001010001020303040501000100010001NA                                                                                                                                                                                                                                                                                                     %");
            //ValidationAndGrouper("B1          00        C-072009    02.001234567891.0  7002054                                                               679433                 N         AXX344              20100413        1ROGER        SIMON                TX75243      34234343242 03423423420423424234     019550707 1          1012010070704       001000000000                                                                          001.9 02 004.3 01 003.0 04 006.3 03 007.9 02 002.9 010100                                                                   00         01       01              00      00  0100                                   00  01                                                                                                                                                             01 007.8  008.00 009.3  005.1  006.1  004.2  010.01 004.2  001.9  008.00 009.2  002.1 0000                                                                                                                         00   0                                                00    00  1                0101  0202                                    00                                                                                                                                                                                                                                                                                                                                       %");
            //ValidationAndGrouper("B1          00        C-072009    02.001234567891.0  7002054                                                               679433                 N         AXX346              20100702        1EVE          RAUL                 TX75230      96565465643 03453534530345353534     019400702 210030001260012010070701000010 0000100000000                   1 001.9  003.0   002.2  009.2  008.00 001.9 100000000 002.1 02 003.0 03 001.0 04 007.9 03 007.5 01 007.1 030100      100000                                                       00  00     03       01              0101000001  0300020001             0100000000  01  03  00                  01  01                  NA                                                                                     0  0                        01 002.2  004.1  005.4  007.4  006.8  004.2  002.1  006.0  007.4  003.23 004.8  005.810150         120100701100000 002.2  006.2  003.23 002.9   00.10  00.12  00.11  00.1100 003.29 005.810001000001       01010102000000                                                00    00  0        010101020200010104               0001020101000  00  00030101000102030405040101NA010001NA01                                                                                                                                                                                                                                                                                                     %");
            //ValidationAndGrouper("B1          00        C-072009    02.001234567891.0  7002054                                                               679433                 N                       AXX34620100702        1EVE          RAUL                 TX75243      96565465643 03453534530345353534     019400702 210030001260012010070701000010 0000100000000                   1 001.9  003.0   002.2  009.2  008.00 001.9 100000000 002.1 02 003.0 03 001.0 04 007.9 03 007.5 01 007.1 030100      100000                                                       00  00     03       01              0101000001  0300020001             0100000000  01  03  00                  01  01                  NA                                                                                     0  0                        01 002.2  004.1  005.4  007.4  006.8  004.2  002.1  006.0  007.4  003.23 004.8  005.810150         120100701100000 002.2  006.2  003.23 002.9   00.10  00.12  00.11  00.1300 003.29 005.810001000001       01010102000000                                                00    00  0        010101020200010104               0001020101000  00  00030101000102030405040101NA010001NA01                                                                                                                                                                                                                                                                                                     %");
            //ValidationAndGrouper("B1          00        C-072009    02.005819268982.508                                                                      017013HOM7013A         01Q70130027914693             20081103        1                                  AL36317      161366436A  0         0              119450114 1          002201001010100000100010000000000           200810290 332.0  728.871 332.0  728.87 493.22       000000100 332.0 02 728.8702 493.2202 239.0 02                  000101010010000001                              010000010000101111110000000000311000           0     0     0100010001020000000000000001       00000010000202020202020303010201020102010102020101020204040303040402020002NANANA                                                                     001000              117455293901                                                                                    0150           Rec 000000001: Code 2BHKS--OASIS 08LV10AA11EHILCKGH--Version V3110 --Flag 1                                   04   0            00  00  00                          00    00  1                0302  0203                                    NA                                                                                                                                                                                                                                 0000000026245632009011320991231C2BHKS02.032009011301 AL0000000829001327468900000000262456320081103S   %");
            //ValidationAndGrouper("B1          00        C-072009    02.008416314362.0  7002054                                                               679433               TXN         RH040224            20100313        1RUBY         HESTER               TX75241      450420901A  04504209010526860597     019240402 212957052910012010051004001000 0100000000000                   0                                           000000000 428.0 03 401.9 03 V57.1    286.9 03 716.8903 280.9 020001      000000                                                       01         00       NA              030000  01  0000                   0000000         02  02                                                                 0     0  0                         0000000                      0  0              121596521501               781.2                                                                0160                  000000                                                        00              00000000                 03   0                                                00    00  0  000000        0301  01022BGLS     V3110                     NA                                    00000000000000000              0000000000000000                                                                                                                                         THOMAS,GEORGE|THOMAS,GEORGE                                                                                %");
           Console.ReadLine();
        }

        private static void StressTest(MemcachedClient client)
        {
            var i = 0;
            var last = true;

            var progress = @"-\|/".ToCharArray();
            Console.CursorVisible = false;
            Dictionary<bool, int> counters = new Dictionary<bool, int>() { { true, 0 }, { false, 0 } };

            while (true)
            {
                var key = "Test_Key_" + i;
                var state = client.Store(StoreMode.Set, key, i) & client.Get<int>(key) == i;

                Action updateTitle = () => Console.Title = "Success: " + counters[true] + " Fail: " + counters[false];

                if (state != last)
                {
                    Console.ForegroundColor = state ? ConsoleColor.White : ConsoleColor.Red;
                    Console.Write(".");

                    counters[state] = 0;
                    last = state;

                    updateTitle();
                }
                else if (i % 200 == 0)
                {
                    Console.ForegroundColor = state ? ConsoleColor.White : ConsoleColor.Red;

                    Console.Write(progress[(i / 200) % 4]);
                    if (Console.CursorLeft == 0)
                    {
                        Console.CursorLeft = Console.WindowWidth - 1;
                        Console.CursorTop -= 1;
                    }
                    else
                    {
                        Console.CursorLeft -= 1;
                    }

                    updateTitle();
                }

                i++;
                counters[state] = counters[state] + 1;
            }
        }

        private static void FluentAdoTest()
        {
            string sql = @"select * from users where id = @userid";

            var user = new FluentCommand<User>(sql)
                .SetConnection(DatabaseConnection.GetConnection("server=10.0.3.31;user=appdata;database=agencymanagement;password=@gencyC0re;"))
                .AddGuid("userid", new Guid("13AA1C55-CE6E-4C75-AA30-6387ADD2E85B"))
                .SetMap(reader => new User
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetString("FirstName"),
                    LastName = reader.GetString("LastName")
                })
                .AsSingle();


            if (user != null)
            {
                Console.WriteLine("User Nationality: {0}", user.DisplayName);
            }
        }

        private static void ValidationAndGrouper(string oasisDataString)
        {
            //string oasisDataString = "B1          00        C-072009    02.001234567891.0  7002054                                                                                      N                        1200420100531        1ZENA        NALEXANDER            TX75230      2313123A6   032131231201232131       019590105 2          1042010060304       000000000001                                                                          008.4904 371.5102 011.0203 018.8402 045.2103 101.  021100                                                                   01         01       01              01      01  0300                                   00  01                                                                                                                                                             01 123.1  345.40 678.10 678.03 985.6  654.61 461.0  345.10 345.11 350.8  350.1  353.0    1                                                                                                                         02   0                                                00    00  1                0201  0106                                    NA                                                                                                                                                                                                                                                                                                                                       %";
            //string oasisDataString = "B1          00        C-072009    02.005819268982.508                                                                      017013HOM7013A         01Q70130027914693             20081103        1                                  AL36317      161366436A  0         0              119450114 1          002201001010100000100010000000000           200810290 332.0  728.871 332.0  728.87 493.22       000000100 332.0 02 728.8702 493.2202 239.0 02                  000101010010000001                              010000010000101111110000000000311000           0     0     0100010001020000000000000001       00000010000202020202020303010201020102010102020101020204040303040402020002NANANA                                                                     001000              117455293901                                                                                    0150           Rec 000000001: Code 2BHKS--OASIS 08LV10AA11EHILCKGH--Version V3110 --Flag 1                                   04   0                                                00    00  1        02      0302  0203                                    NA                                                                                                                                                                                                                                 0000000026245632009011320991231C2BHKS02.032009011301 AL0000000829001327468900000000262456320081103S   %";
            //string oasisDataString = "B1          00        C-072009    02.001234567891.0  7002054                                                                743102                 N         KH487               20101104        0TERESA       BONDS                TX75452      465178256A  04651782560              019601229 213260659880012010110401000001 0100000000000                   0                340.                       100000000 340.  03 401.9 03 311.  03 272.4 03 719.7 03 294.10  1000      010000                                                       01  00     00       NA              0200010101  0000000100             0000001000  02  01  02                  01  01                  00                                                                                     0  1                        01                                                                                       0 20101104020101104000000                                                        00              0000100001       06000000000000                                                00    00  0        04010101030102010200000     000000101010101010  01  02NA01010101010003000001                                                                                                                                                                                                                                                                                                                   %";

            Console.WriteLine("OASIS Data String: {0}", oasisDataString);
            Console.WriteLine("OASIS Data String Length: {0}", oasisDataString.Length);

            try
            {
                Console.WriteLine("Calling Validation");
                var validationAgent = new ValidationAgent();
                var result = validationAgent.ValidateAssessment(oasisDataString);

                Console.WriteLine("found {0}", result.Count);
                Console.WriteLine("Calling Grouper Logic");
                var grouperAgent = new GrouperAgent();
                Axxess.Api.Contracts.Hipps hipps = grouperAgent.GetHippsCode(oasisDataString);
                Console.WriteLine("hipps code: {0}", hipps.Code);
                Console.WriteLine("hipps version: {0}", hipps.Version);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //public static string format()
        //{
        //    IAssessmentService asse = Container.Resolve<IAssessmentService>();
        //    IDictionary<string, Question> ques = asse.GetAssessment(new Guid("d10f666e-f807-42a6-a2ee-09b5df9fbfe8"), "ResumptionOfCare");
        //    return asse.GetOasis1448Format(asse.GetFormat(), ques);
        //}

        //public static string formatTest()
        //{
        //    IAssessmentService asse = Container.Resolve<IAssessmentService>();
        //    IDictionary<string, Question> assessmentQuestions = asse.GetAssessment(new Guid("965b5486-4405-4128-9df9-cf1c4469ed06"), "StartOfCare");
        //    var output = "";
        //    asse.GetFormat().ForEach(data =>
        //    {
        //        string questionKey = data.ElementName;
        //        if (questionKey != null)
        //        {
        //            if (assessmentQuestions.ContainsKey(questionKey) && assessmentQuestions[questionKey].Answer != null)
        //                output += questionKey + " " +
        //                    (assessmentQuestions[questionKey].Answer.PadLeft((int)(data.Length)));
        //            else
        //                output += questionKey + " " +String.Empty.PadLeft((int)(data.Length));
        //        }
        //        else
        //        {
        //            output += data.Item + " " + String.Empty.PadLeft((int)(data.Length));
        //        }

        //        output += '\n';
        //    }
        //   );
        //     return output;
        //}
    }
}
