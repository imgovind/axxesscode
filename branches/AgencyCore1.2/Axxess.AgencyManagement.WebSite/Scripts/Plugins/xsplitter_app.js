﻿// xSplitter Application

var s6;



/*
Note:
Calling xAddEventListener before window.onload means that you must include
the xAddEventListener source code "before" this application code.
*/

jQuery.event.add(window, "load", win_onload);
jQuery.event.add(window, "resize", win_onresize);

function win_onload() {
    var cw = xClientWidth();
    var w = $(".window").width() - 2;
    var h = $(".window").height() - 55.5;
    s6 = new xSplitter('patient_Splitter1', 0, 0, w, h, true, 6, 200, 200, h / 5, true, 2, null, 0);    
};

function win_onresize() {
    var cw = xClientWidth();
    var w = $(".window").width() - 2;
    var h = $(".window").height() - 55.5;
  s6.paint(w, h, 200, 200, w / 4);
    
}
