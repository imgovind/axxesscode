﻿var Billing = {
    InitCenter: function() {

    },
    InitRap: function() {
        $("#rapVerification").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {

                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Navigate: function(index, id) {
        $(id).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            var tabstrip = $("#FinalTabStrip").data("tTabStrip");
                            var item = $("li", tabstrip.element)[index];
                            tabstrip.select(item);
                        }
                        else {
                            alert("")
                        }
                    },
                    error: function() {

                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    NavigateBack: function(index) {
        var tabstrip = $("#FinalTabStrip").data("tTabStrip");
        var item = $("li", tabstrip.element)[index];
        tabstrip.select(item);
    },
    UpdateStatus: function(control) {
        var fields = $(":input", $(control)).serializeArray();
        U.postUrl('Billing/UpdateStatus', fields, null, null);
    },
    OnPatientRowSelected: function(e) {
        var patientId = e.row.cells[2].innerHTML;
        Billing.RebindActivity(patientId);
    },
    RebindActivity: function(id) {
        var ActivityGrid = $('#BillingHistoryActivityGrid').data('tGrid');
        ActivityGrid.rebind({ patientId: id });
    },
    NoPatientBind: function(id) {
        Billing.RebindActivity(id);
    },
    loadGenerate: function(control) {
        var fields = $(":input", $(control)).serializeArray();
        acore.open("claimSummary", 'Billing/ClaimSummary', function() { }, fields);
    },
    loadPendingClaims: function() {
        $("#pendingClaimsResult").load('Billing/PendingClaims', function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#pendingClaimsResult').html('<p>There was an error making the request</p>');
                JQD.open_window('#pendingClaims');
            }
            else if (textStatus == "success") {
                JQD.open_window('#pendingClaims');


            }
        });
    }
}