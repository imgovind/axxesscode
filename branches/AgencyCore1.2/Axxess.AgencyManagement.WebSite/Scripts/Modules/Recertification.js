﻿var Recertification = {
    _RecertificationId: "",
    _patientId: "",
    _EpisodeId: "",
    GetId: function() {
        return Recertification._patientId;
    },
    SetId: function(patientId) {
        Recertification._patientId = patientId;
    },
    GetRecertificationId: function() {
        return Recertification._RecertificationId;
    },
    SetRecertificationId: function(RecertificationId) {
        Recertification._RecertificationId = RecertificationId;
    },
    GetSOCEpisodeId: function() {
        return Recertification._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        Recertification._EpisodeId = EpisodeId;
    },
    InitNew: function() {
        Oasis.BradenScaleOnchange('Recertification', '#recetBradenScale');
        $("input[name=StartOfCare_GenericNutritionalHealth]").click(function() {
            Oasis.CalculateNutritionScore('Recertification');
        });
    },
    Last: function(control) {
        var formId = control.closest("form").attr("id");
        $("#" + formId).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("input[name='Recertification_Id']").val(resultObject.Assessment.Id);
                            $("input[name='Recertification_PatientGuid']").val(resultObject.Assessment.PatientId);
                            $("input[name='Recertification_Action']").val('Edit');
                        }
                        else {
                            alert(resultObject.errorMessage);
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    HandlerHelper: function(form, control, action) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#recertificationTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('recertification');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save &amp; Check for Errors") {
                        action();
                    }
                    else if (actionType == "Check for Errors") {
                        action();
                    }
                }
                else {
                    $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                }
            },
            error: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#recertificationTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('recertification');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save &amp; Check for Errors") {
                        action();
                    }
                    else if (actionType == "Check for Errors") {
                        action();
                    }
                }
                else {
                    $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    FormSubmit: function(control , action) {
        var form = control.closest("form");
        form.find('.form-omitted :input').val("").end().find('.form-omitted:input').val("");
        form.validate();
        Recertification.HandlerHelper(form, control, action);
    },
    Validate: function(id) {
        OasisValidation.Validate(id, "Recertification");
    },
    loadRecertification: function(id, patientId, assessmentType) {
        acore.open("recertification", 'Oasis/Recertification', function() {
            $("#recertificationTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
            $("#recertificationTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
            Recertification.InitNew();
            Oasis.Init();
            $("#recertificationTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                Recertification.loadRecertificationParts(event, ui);
            });
        }, { Id: id, PatientId: patientId, AssessmentType: assessmentType });
    },
    loadRecertificationParts: function(event, ui) {
        $($(ui.tab).attr('href')).empty();
        $($(ui.tab).attr('href')).addClass("loading");
        $($(ui.tab).attr('href')).load('Oasis/RecertificationCategory', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
                $($(ui.tab).attr('href')).removeClass("loading");
                Recertification.InitNew();
                Oasis.Init();
            }
        });
    }
}