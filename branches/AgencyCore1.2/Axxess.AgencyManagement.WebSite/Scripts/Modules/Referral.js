﻿var Referral = {
    InitNew: function() {
        $("#newReferralForm").clearForm();
        $("#New_Referral_Date").data("tDatePicker").value(new Date());

        $(".numeric").numeric();
        $(".names").alpha({ nocaps: false });

        $('#New_Referral_HomePhone1').autotab({ target: 'New_Referral_HomePhone2', format: 'numeric' });
        $('#New_Referral_HomePhone2').autotab({ target: 'New_Referral_HomePhone3', format: 'numeric', previous: 'New_Referral_HomePhone1' });
        $('#New_Referral_HomePhone3').autotab({ target: 'New_Referral_Email', format: 'numeric', previous: 'New_Referral_HomePhone2' });
        $('#New_Referral_PhysicianPhone1').autotab({ target: 'New_Referral_PhysicianPhone2', format: 'numeric' });
        $('#New_Referral_PhysicianPhone2').autotab({ target: 'New_Referral_PhysicianPhone3', format: 'numeric', previous: 'New_Referral_PhysicianPhone1' });
        $('#New_Referral_PhysicianPhone3').autotab({ target: 'New_Referral_PhysicianFax', format: 'numeric', previous: 'New_Referral_PhysicianPhone2' });

        $("#New_Referral_DMEOther").click(function() {
            var otherDme = $('#New_Referral_DMEOther:checked').is(':checked');
            if (!otherDme) {
                $("#New_Referral_OtherDME").hide();
            }
            else {
                $("#New_Referral_OtherDME").show();
            }
        });

        $("select.Physicians").change(function() {
            var selectList = this;
            if ($(this).val() == "new") {
                UserInterface.ShowNewPhysicianModal();
            } else if ($(this).val() == "spacer") {
                selectList.selectedIndex = 0;
            }
        });

        $("select.ReferrerPhysician").change(function() {
            var selectList = this;
            if ($(this).val() == "new") {
                UserInterface.ShowNewPhysicianModal();
            } else if ($(this).val() == "spacer") {
                selectList.selectedIndex = 0;
            }
        });

        $("#New_Referral_PhysicianDropDown").change(function() {
            var data = 'PhysicianContactId=' + $(this).val();
            $.ajax({
                url: '/Patient/GetPhysicianContact',
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result) {
                    $('tbody', $(this).closest('table')).clearForm();
                    $("#New_Referral_NpiNumber").val('');
                    var data = eval(result);
                    $("#New_Referral_PhysicianFirstName").val(data.FirstName != null ? data.FirstName : '');
                    $("#New_Referral_PhysicianLastName").val(data.LastName != null ? data.LastName : '');
                    $("#New_Referral_PhysicianNPI").val(data.NPI != null ? data.NPI : '');
                    if (data.PhoneWork !== null) {
                        $("#New_Referral_PhysicianPhone1").val(data.PhoneWork.substring(0, 3));
                        $("#New_Referral_PhysicianPhone2").val(data.PhoneWork.substring(3, 6));
                        $("#New_Referral_PhysicianPhone3").val(data.PhoneWork.substring(6, 10));
                    }
                    if (data.FaxNumber !== null) {
                        $("#New_Referral_PhysicianFax").val(data.FaxNumber);
                    }
                    $("#New_Referral_PhysicianEmail").val(data.EmailAddress != null ? data.EmailAddress : '');
                }
            });
        });

        $("#New_Referral_NpiNumber").autocomplete('/LookUp/Npis', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Id, result: data[i].Id };
                }
                return rows;
            },
            formatItem: function(row, i, n) {
                return row.Id + '-' + row.ProviderFirstName + '  ' + row.ProviderLastName;
            },
            width: 400
        }).result(function(event, row, formatted) {
            $('tbody', $(this).closest('table')).clearForm();
            if (row) {
                $("#New_Referral_PhysicianFirstName").val(row.ProviderFirstName != null ? row.ProviderFirstName : '');
                $("#New_Referral_PhysicianLastName").val(row.ProviderLastName != null ? row.ProviderLastName : '');
                $("#New_Referral_PhysicianNPI").val(row.Id != null ? row.Id : '');

                if (row.ProviderBusinessPracticeLocationAddressTelephoneNumber !== null) {
                    var primaryPhone = row.ProviderBusinessPracticeLocationAddressTelephoneNumber;
                    $("#New_Referral_PhysicianPhone1").val(primaryPhone.substring(0, 3));
                    $("#New_Referral_PhysicianPhone2").val(primaryPhone.substring(3, 6));
                    $("#New_Referral_PhysicianPhone3").val(primaryPhone.substring(6, 10));
                }
                if (row.ProviderBusinessPracticeLocationAddressFaxNumber !== null) {
                    var ProviderBusinessPracticeLocationAddressFaxNumber = row.ProviderBusinessPracticeLocationAddressFaxNumber;
                    $("#New_Referral_PhysicianFax").val(ProviderBusinessPracticeLocationAddressFaxNumber);
                    $("#txtNew_Patient_PhysicianContactFax1").val(primaryPhone.substring(0, 3));
                    $("#txtNew_Patient_PhysicianContactFax2").val(primaryPhone.substring(3, 6));
                    $("#txtNew_Patient_PhysicianContactFax3").val(primaryPhone.substring(6, 10));
                }
                $("#New_Referral_PhysicianDropDown").val("0");
            }
        });

        $("#newReferralForm").validate({
            messages: {
                FirstName: "",
                LastName: "",
                DateOfBirth: "",
                Gender: "",
                HomePhoneArray: "",
                AddressLine1: "",
                AddressCity: "",
                AddressStateCode: "",
                AddressZipCode: ""
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("New referral successfully added.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('newreferral');
                            Patient.Rebind();
                            Referral.RebindList();
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Delete: function(id) {
        if (confirm("Are you sure you want to delete this referral?")) {
            U.postUrl("/Referral/Delete", { id: id }, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                    Referral.RebindList();
                } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    RebindList: function() {
    var grid = $('#List_Referral').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    },
    Admit: function(id) {
        acore.open("newpatient", 'Patient/New', function() { Patient.InitNew(); }, { referralId: id });
    },
    BindGridButton: function() {
        var newButton = $('#List_Referral_NewButton');
        href = "javascript:void(0);";
        newButton.attr('href', href);
    },
    InitEdit: function() {
        $("#editReferralForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('editreferral');
                            Patient.Rebind();
                            Referral.RebindList();
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}