﻿var TransferNotDischarge = {
    _TransferNotDischargeId: "",
    _patientId: "",
    _EpisodeId: "",
    GetId: function() {
        return TransferNotDischarge._patientId;
    },
    SetId: function(patientId) {
        TransferNotDischarge._patientId = patientId;
    },
    GetTransferNotDischargeId: function() {
        return TransferNotDischarge._TransferNotDischargeId;
    },
    SetTransferNotDischargeId: function(TransferNotDischargeId) {
        TransferNotDischarge._TransferNotDischargeId = TransferNotDischargeId;
    },
    GetSOCEpisodeId: function() {
        return TransferNotDischarge._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        TransferNotDischarge._EpisodeId = EpisodeId;
    },
    InitNew: function() {

        $("input[name=TransferInPatientNotDischarged_M1040InfluenzaVaccine]").click(function() {
            if ($(this).val() == "01" || $(this).val() == "NA") {

                $("#transferNotDischarge_M1045").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientNotDischarged_M1045InfluenzaVaccineNotReceivedReason ]").attr('checked', false);
            }
            else if ($(this).val() == "00") {
                $("#transferNotDischarge_M1045").unblock();
            }
        });

        $("input[name=TransferInPatientNotDischarged_M1050PneumococcalVaccine]").click(function() {
            if ($(this).val() == 1) {

                $("#transferNotDischarge_M1055").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientNotDischarged_M1055PPVNotReceivedReason ]").attr('checked', false);
            }
            else if ($(this).val() == 0) {
                $("#transferNotDischarge_M1055").unblock();
            }
        });

        $("input[name=TransferInPatientNotDischarged_M1500HeartFailureSymptons]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02" || $(this).val() == "NA") {

                $("#transferNotDischarge_M1510").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientNotDischarged_M1510HeartFailureFollowup ]").attr('checked', false);
            }
            else if ($(this).val() == "01") {
                $("#transferNotDischarge_M1510").unblock();
            }
        });

        $("input[name=TransferInPatientNotDischarged_M2300EmergentCare]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "UK") {

                $("#transferNotDischarge_M2310").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientNotDischarged_M2310ReasonForEmergentCare ]").attr('checked', false);
            }
            else if ($(this).val() == "01" || $(this).val() == "02") {
                $("#transferNotDischarge_M2310").unblock();
            }
        });
        $("input[name=TransferInPatientNotDischarged_M2410TypeOfInpatientFacility]").click(function() {
            if ($(this).val() == "01") {
                $("#transferNotDischarge_M2440").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#transferNotDischarge_M2430").unblock();
                $("input[name=TransferInPatientNotDischarged_M2440ReasonPatientAdmitted]").attr('checked', false);
            }
            else if ($(this).val() == "02" || $(this).val() == "04") {
                $("#transferNotDischarge_M2440").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#transferNotDischarge_M2430").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientNotDischarged_M2430ReasonForHospitalization]").attr('checked', false);
                $("input[name=TransferInPatientNotDischarged_M2440ReasonPatientAdmitted]").attr('checked', false);
            }
            else if ($(this).val() == "03") {
                $("#transferNotDischarge_M2440").unblock();
                $("#transferNotDischarge_M2430").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientNotDischarged_M2430ReasonForHospitalization]").attr('checked', false);
            }
        });
    },
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var actionType = control.html();
                    if (actionType == "Save/Continue") {
                        $("input[name='TransferInPatientNotDischarged_Id']").val(resultObject.Assessment.Id);
                        $("input[name='TransferInPatientNotDischarged_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='TransferInPatientNotDischarged_Action']").val('Edit');
                        Oasis.NextTab("#editTransferInPatientNotDischargedTabs.tabs");
                    }
                    else if (actionType == "Save/Exit") {
                        Oasis.Close(control);
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save") {
                        $("input[name='TransferInPatientNotDischarged_Id']").val(resultObject.Assessment.Id);
                        $("input[name='TransferInPatientNotDischarged_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='TransferInPatientNotDischarged_Action']").val('Edit');
                    }
                }
                else {
                    alert(resultObject.errorMessage);
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control, formType) {

        var form = control.closest("form");
        form.validate();
        TransferNotDischarge.HandlerHelper(form, control);
    },
    Validate: function() {
        OasisValidation.Validate(TransferNotDischarge._TransferNotDischargeId, "TransferInPatientNotDischarged");
    },
    loadTransferNotDischarge: function(id, patientId, assessmentType) {
        acore.open("transfernotdischarge", 'Oasis/TransferNotDischarge', function() {
        $("#transferInPatientNotDischargedTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
        $("#transferInPatientNotDischargedTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
            TransferNotDischarge.InitNew();
            $("#transferInPatientNotDischargedTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                TransferNotDischarge.loadTransferNotDischargeParts(event, ui);
                TransferNotDischarge.InitNew();
            });
        }, { Id: id, PatientId: patientId, AssessmentType: assessmentType });
    },
    loadTransferNotDischargeParts: function(event, ui) {

        $($(ui.tab).attr('href')).empty();
        $($(ui.tab).attr('href')).addClass("loading");
        $($(ui.tab).attr('href')).load('Oasis/TransferNotDischargeCategory', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
                $($(ui.tab).attr('href')).removeClass("loading");
            }
        });
    }

}