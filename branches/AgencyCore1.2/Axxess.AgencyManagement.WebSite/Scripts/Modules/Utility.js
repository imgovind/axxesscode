﻿var U =
{
    showDialog: function(popupBox, onReady) {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: $(popupBox),
            css: {
                top: ($(window).height() - $(popupBox).height()) / 2 + 'px',
                left: ($(window).width() - $(popupBox).width()) / 2 + 'px'
            },
            onBlock: function() {
                if (typeof (onReady) == 'function') {
                    onReady();
                }
            } 
        });
    },
    closeDialog: function() {
        $.unblockUI();
    },
    getUrl: function(url, input, onSuccess) {
        $.ajax({
            url: url,
            data: input,
            dataType: 'json',
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            }
        });
    },
    postUrl: function(url, input, onSuccess, onFailure) {
        $.ajax({
            url: url,
            data: input,
            type: 'POST',
            dataType: 'json',
            beforeSend: function() {
            },
            success: function(data) {
                if (typeof (onSuccess) == 'function') {
                    onSuccess(data);
                }
            },
            error: function(data) {
                if (typeof (onFailure) == 'function') {
                    onFailure(data);
                }
            }
        });
    },
    block: function() {
        $.blockUI.defaults.css = {};
        $.blockUI({
            message: 'Loading...',
            css: {
                color: '#fff',
                opacity: .6,
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                top: ($(window).height() - 40) / 2 + 'px',
                left: ($(window).width() - 100) / 2 + 'px'
            }
        });
    },
    unBlock: function(selector) {
        $.unblockUI();
    },
    toTitleCase: function(text) {
        var txt = '';
        var txtArray = text.toLowerCase().split(' ');
        if (txtArray.length > 1) {
            var i = 0;
            for (i = 0; i < txtArray.length; i++) {
                txt += txtArray[i].substr(0, 1).toUpperCase() + txtArray[i].substr(1) + ' ';
            }
        }
        else {
            txt = text.toLowerCase().substr(0, 1).toUpperCase() + text.toLowerCase().substr(1);
        }
        return txt;
    }
};


jQuery.fn.contains = function(txt) { return jQuery(this).indexOf(txt) >= 0; }

