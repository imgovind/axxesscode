﻿var Discharge = {
    _DischargeId: "",
    _patientId: "",
    _EpisodeId: "",
    GetId: function() {
        return Discharge._patientId;
    },
    SetId: function(patientId) {
        Discharge._patientId = patientId;
    },
    GetDischargeId: function() {
        return Discharge._DischargeId;
    },
    SetDischargeId: function(DischargeId) {
        Discharge._DischargeId = DischargeId;
    },
    GetSOCEpisodeId: function() {
        return Discharge._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        Discharge._EpisodeId = EpisodeId;
    },
    InitNew: function() {

        $("input[name=DischargeFromAgency_M1040InfluenzaVaccine]").click(function() {
            if ($(this).val() == "01" || $(this).val() == "NA") {

                $("#discharge_M1045").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1045InfluenzaVaccineNotReceivedReason ]").attr('checked', false);

            }
            else if ($(this).val() == "00") {
                $("#discharge_M1045").unblock();
            }
        });


        $("input[name=DischargeFromAgency_M1050PneumococcalVaccine]").click(function() {
            if ($(this).val() == 1) {

                $("#discharge_M1055").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1055PPVNotReceivedReason ]").attr('checked', false);

            }
            else if ($(this).val() == 0) {
                $("#discharge_M1055").unblock();
            }
        });
        $("input[name=DischargeFromAgency_M1306UnhealedPressureUlcers]").click(function() {
            if ($(this).val() == 0) {

                $("#discharge_M1307").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });

                $("#discharge_M1308").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#discharge_M13010_12_14").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#discharge_M1320").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1307NonEpithelializedStageTwoUlcer ]").attr('checked', false);
                $("#DischargeFromAgency_M1307NonEpithelializedStageTwoUlcerDate").val('');
                $("#discharge_M1308").find(':input').each(function() { $(this).val(''); });
                $("#discharge_M13010_12_14").find(':input').each(function() { $(this).val(''); });
                $("input[name=DischargeFromAgency_M1320MostProblematicPressureUlcerStatus ]").attr('checked', false);
            }
            else if ($(this).val() == 1) {
                $("#discharge_M1307").unblock();
                $("#discharge_M1308").unblock();
                $("#discharge_M13010_12_14").unblock();
                $("#discharge_M1320").unblock();
            }
        });

        $("input[name=DischargeFromAgency_M1330StasisUlcer]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "03") {
                $("#discharge_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1332CurrentNumberStasisUlcer ]").attr('checked', false);
                $("input[name=DischargeFromAgency_M1334StasisUlcerStatus ]").attr('checked', false);
            }
            else {
                $("#discharge_M1332AndM1334").unblock();
            }
        });
        $("input[name=DischargeFromAgency_M1340SurgicalWound]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02") {
                $("#discharge_M1342").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });

                $("input[name=DischargeFromAgency_M1342SurgicalWoundStatus]").attr('checked', false);

            }
            else {
                $("#discharge_M1342").unblock();
            }
        });
        $("input[name=DischargeFromAgency_M1500HeartFailureSymptons]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02" || $(this).val() == "NA") {
                $("#discharge_M1510").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1510HeartFailureFollowup ]").attr('checked', false);
            }
            else if ($(this).val() == "01") {
                $("#discharge_M1510").unblock();
            }
        });

        $("input[name=DischargeFromAgency_M1610UrinaryIncontinence]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02") {
                $("#discharge_M1615").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1615UrinaryIncontinenceOccur]").attr('checked', false);
            }
            else {
                $("#discharge_M1615").unblock();
            }
        });

        $("input[name=DischargeFromAgency_M2300EmergentCare]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "UK") {
                $("#discharge_M2310").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M2310ReasonForEmergentCare ]").attr('checked', false);
            }
            else if ($(this).val() == "01" || $(this).val() == "02") {
                $("#discharge_M2310").unblock();
            }
        });

        $("input[name=DischargeFromAgency_M2410TypeOfInpatientFacility]").click(function() {
            if ($(this).val() == "02" || $(this).val() == "04") {
                $("#discharge_M2420").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M2420DischargeDisposition]").attr('checked', false);
            }
            else {
                $("#discharge_M2420").unblock();
            }
        });
    }
    ,
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    var category = $('[name=categoryType]').val();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#dischargeTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('discharge');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save") {
                    }
                }
                else {
                    $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                }
            },
            error: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    var category = $('[name=categoryType]').val();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#dischargeTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('discharge');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save") {
                    }
                }
                else {
                    $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control) {
        var form = control.closest("form");
        form.validate();
        Discharge.HandlerHelper(form, control);
    },
    Validate: function() {
        OasisValidation.Validate(Discharge._DischargeId, "DischargeFromAgency");
    }
    ,
    loadDischarge: function(id, patientId, assessmentType) {
        acore.open("discharge", 'Oasis/Discharge', function() {
            $("#dischargeTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
            $("#dischargeTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
            Discharge.InitNew();
            $("#dischargeTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                Discharge.loadDischargeParts(event, ui);
                Discharge.InitNew();
            });
        }, { Id: id, PatientId: patientId, AssessmentType: assessmentType });
    },
    loadDischargeParts: function(event, ui) {

        $($(ui.tab).attr('href')).empty();
        $($(ui.tab).attr('href')).addClass("loading");
        $($(ui.tab).attr('href')).load('Oasis/DischargeCategory', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
                $($(ui.tab).attr('href')).removeClass("loading");
            }
        });
    }
}