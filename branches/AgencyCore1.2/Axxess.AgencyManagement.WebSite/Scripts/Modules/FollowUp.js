﻿var FollowUp = {
    _FollowUpId: "",
    GetFollowUpId: function() {
        return FollowUp._FollowUpId;
    },
    SetFollowUpId: function(FollowUpId) {
        FollowUp._FollowUpId = FollowUpId;
    },
    InitNew: function() {


        $("input[name=FollowUp_M1306UnhealedPressureUlcers]").click(function() {
            if ($(this).val() == 0) {
                $("#followUp_M1308").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#followUp_M1308").find(':input').each(function() { $(this).val(''); });
            }
            else if ($(this).val() == 1) {
                $("#followUp_M1308").unblock();
            }
        });

        $("input[name=FollowUp_M1330StasisUlcer]").click(function() {
            if ($(this).val() == 0 || $(this).val() == 3) {
                $("#followUp_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=FollowUp_M1332CurrentNumberStasisUlcer ]").attr('checked', false);
                $("input[name=FollowUp_M1334StasisUlcerStatus ]").attr('checked', false);
            }
            else {
                $("#followUp_M1332AndM1334").unblock();
            }
        });

        $("input[name=FollowUp_M1340SurgicalWound]").click(function() {
            if ($(this).val() == 0 || $(this).val() == 2) {
                $("#followUp_M1342").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=FollowUp_M1342SurgicalWoundStatus]").attr('checked', false);
            }
            else {
                $("#followUp_M1342").unblock();
            }
        });

    }
    ,
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {

                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#followUpTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('followup');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save") {
                    }
                }
                else {
                    $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                }

            },
            error: function(result) {

                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#followUpTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('followup');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save") {
                    }
                }
                else {
                    $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                }

            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control) {
        var form = control.closest("form");
        form.validate();
        FollowUp.HandlerHelper(form, control);
    },
    Validate: function(id) {

        OasisValidation.Validate(id, "FollowUp");

    },
    loadFollowUp: function(id, patientId, assessmentType) {
        acore.open("followup", 'Oasis/FollowUp', function() {
            $("#followUpTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
            $("#followUpTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
            FollowUp.InitNew();
            $("#followUpTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                FollowUp.loadFollowUpParts(event, ui);
            });
        }, { Id: id, PatientId: patientId, AssessmentType: assessmentType });
    }
    ,
    loadFollowUpParts: function(event, ui) {

        $($(ui.tab).attr('href')).empty();
        $($(ui.tab).attr('href')).addClass("loading");
        $($(ui.tab).attr('href')).load('Oasis/FollowUpCategory', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
                $($(ui.tab).attr('href')).removeClass("loading");
                FollowUp.InitNew();
                Oasis.Init();
            }
        });
    }

}