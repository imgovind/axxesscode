﻿var Message = {
    defaultMessage: "",
    _tokenlist: null,
    Init: function(messageId) {
        if (messageId != undefined) Message.defaultMessage = messageId;
        $("#MessageReplyButton").click(function() {
            var messageId = $("#messageId").val();
            acore.open("newmessage", 'Message/New', function() {
                Message.InitNew(false);
                U.postUrl("/Message/Get", "id=" + messageId, function(message) {
                    var recipient = $('input[name="Recipients"][value=' + message.FromId + ']');
                    if (recipient != null) {
                        recipient.attr('checked', true);
                        Message.AddRemoveRecipient(recipient.attr('id'));
                    }
                    $("#New_Message_PatientId").val(message.PatientId);
                    $("#New_Message_Subject").val("RE: " + message.Subject);
                    $("#New_Message_Body").val(message.ReplyForwardBody);
                    Message.initCKE();
                });
            });
        });
        $("#MessageForwardButton").click(function() {
            var messageId = $("#messageId").val();
            acore.open("newmessage", 'Message/New', function() {
                Message.InitNew(false);
                U.postUrl("/Message/Get", "id=" + messageId, function(message) {
                    $("#New_Message_PatientId").val(message.PatientId);
                    $("#New_Message_Subject").val("FW: " + message.Subject);
                    $("#New_Message_Body").val(message.ReplyForwardBody);
                    Message.initCKE();
                });
            });
        });
        $("#MessageDeleteButton").click(function() {
            if (confirm("Are you sure you want to delete this Message?")) {
                var messageId = $("#messageId").val();
                U.postUrl("/Message/Delete", "messageId=" + messageId, function(viewData) {
                    if (viewData.isSuccessful) {
                        $.jGrowl("Your message has been deleted successfully.", { theme: 'success', life: 5000 });
                        var messageGrid = $('#List_Messages').data('tGrid');
                        messageGrid.rebind();
                        if ($("#List_Messages .t-grid-content tbody tr").length == 0) $("#scheduleMainResult").html("No Messages Found");
                        setTimeout('$("#List_Messages .t-grid-content tbody tr:has(td):first").click();', 50);
                    } else $.jGrowl(viewData.errorMessage, { theme: 'error', life: 5000 });
                });
            }
        });
        $.fn.selectRange = function(start, end) {
            return this.each(function() {
                if (this.setSelectionRange) {
                    this.focus();
                    this.setSelectionRange(start, end);
                } else if (this.createTextRange) {
                    var range = this.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', end);
                    range.moveStart('character', start);
                    range.select();
                }
            });
        };
    },
    InitNew: function(wysiwyg) {
        if (wysiwyg == undefined) wysiwyg = true;
        $('#New_Message_CheckallRecipients').change(function() {
            if ($('#New_Message_CheckallRecipients').attr('checked')) {
                $('input[name="Recipients"]').each(function() {
                    if (!$(this).attr('checked')) {
                        $(this).attr('checked', true);
                        Message.AddRemoveRecipient($(this).attr('id'));
                    }
                });
            }
            else {
                $('input[name="Recipients"]').each(function() {
                    if ($(this).attr('checked')) {
                        $(this).attr('checked', false);
                        Message.AddRemoveRecipient($(this).attr('id'));
                    }
                });
            }
        });

        $("#newMessageForm").validate({
            submitHandler: function(form) {
                $("textarea[name=Body]").val(CKEDITOR.instances['New_Message_Body'].getData());
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("Your message has been sent successfully.", { theme: 'success', life: 5000 });
                            Message.Cancel();
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        Message._tokenlist = $.fn.tokenInput("#New_Message_Recipents", "/Message/Recipients", {
            classes: {
                tokenList: "token-input-list-facebook",
                token: "token-input-token-facebook",
                tokenDelete: "token-input-delete-token-facebook",
                selectedToken: "token-input-selected-token-facebook",
                highlightedToken: "token-input-highlighted-token-facebook",
                dropdown: "token-input-dropdown-facebook",
                dropdownItem: "token-input-dropdown-item-facebook",
                dropdownItem2: "token-input-dropdown-item2-facebook",
                selectedDropdownItem: "token-input-selected-dropdown-item-facebook",
                inputToken: "token-input-input-token-facebook"
            }
        });
        if (wysiwyg) Message.initCKE();
    },
    Cancel: function() {
        acore.close('newmessage');
    },
    AddRemoveRecipient: function(input) {
        var recipient = $('#' + input);
        var id = recipient.attr('value');
        var data = recipient.attr('title');
        if (recipient.attr('checked')) {
            var tokenIn = Message._tokenlist.insertToken(id, data);
            $.data(recipient.get(0), "tokenbox", { "token": tokenIn });
        }
        else {
            var tokenOut = $.data(recipient.get(0), "tokenbox");
            Message._tokenlist.removeToken(tokenOut.token);
        }
        Message.positionBottom();
    },
    Compose: function() {
        acore.open("newmessage");
        Message.initCKE();
    },
    OnLoad: function() { },
    OnDataBound: function() {
        var newButton = $('#List_Messages_NewButton');
        href = "javascript:void(0);";
        newButton.attr('href', href);
        if (Message.defaultMessage != "" && $("#" + Message.defaultMessage).length) $("#" + Message.defaultMessage).closest("tr").click();
        else $("#List_Messages .t-grid-content tbody tr:has(td):first").click();
        Message.defaultMessage = "";
    },
    OnRowSelected: function(e) {
        if (e.row.cells[1] != undefined) {
            var messageId = e.row.cells[1].innerHTML;
            U.postUrl("/Message/Get", "id=" + messageId, function(message) {
                $("#messageBody").html(message.Body);
                $("#messageSender").html(message.FromName);
                $("#messageSubject").html(message.Subject);
                $("#messageDate").html(message.MessageDate);
                $("#messageId").val(message.Id);
            });

            $("#" + messageId).removeClass('false').addClass('true');
        }
    },
    positionBottom: function() {
        $('#newMessageBodyDiv').css({ top: (parseInt($('.token-input-list-facebook').attr('clientHeight') + 100).toString() + "px") });
    },
    initCKE: function() {
        CKEDITOR.replace('New_Message_Body', {
            skin: 'office2003',
            resize_enabled: false,
            height: '100%',
            toolbar: [
                    ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Link', 'Unlink', '-', 'Font', 'FontSize', 'TextColor', '-', 'SpellChecker', 'Scayt']
                ]
        });
    },
    initWidget: function() {
        U.postUrl("/Message/List", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data.Total > 0) {
                for (var i = 0; i < data.Data.length && i < 5; i++) {
                    if (data.Data[i].MarkAsRead) css = "";
                    else css = " class=%22strong%22";
                    $('#messagesWidgetContent').append(unescape("%3Cdiv" + css + "%3E%3Cinput type=%22checkbox%22 class=%22radio float_left%22 value=%22" +
                        data.Data[i].Id + "%22 /%3E%3Ca href=%22javascript:void(0);%22 onclick=%22$(this).parent().removeClass('strong'); if($('#window_" +
                        "messageinbox').length == 0) { acore.open('messageinbox', 'Message/Inbox', function() { Message.Init('" + data.Data[i].Id + "');" +
                        " }); } else { acore.winfocus('messageinbox'); $('#" + data.Data[i].Id + "').closest('tr').click(); }%22 onmouseover=%22$(this)." +
                        "addClass('t-state-hover');%22 tooltip=%22" + data.Data[i].Body.substring(0, 200) + "%22 onmouseout=%22$(this).removeClass('t-st" +
                        "ate-hover');%22 class=%22message true%22%3E%3Cspan class=%22float_left%22%3E" + data.Data[i].FromName + " &ndash; "              +
                        data.Data[i].Subject + "%3C/span%3E%3Cdiv class=%22clear%22%3E%3C/div%3E%3C/a%3E%3C/div%3E"));
                }
            } else {
                $('#messagesWidgetContent').append(unescape("%3Ch1 class=%22align_center%22%3ENo Messages found.%3C/h1%3E"));
                $('#messageWidgetMore').hide();
            }
        });
        $(".mailcontrols select").bind("change", function() {
            if ($(this).find("option:first").is(":not(:selected)")) {
                if ($("#messagesWidgetContent input:checked").length) {
                    if ($(this).find(":selected").html() == "Archive") { alert("Archive"); }
                    if ($(this).find(":selected").html() == "Delete") {
                        if (confirm("Are you sure you want to delete all selected messages?")) {
                            $("#messagesWidgetContent input:checked").each(function() {
                                var messageId = $(this).val();
                                U.postUrl("/Message/Delete", "messageId=" + messageId, function(viewData) {
                                    if (viewData.isSuccessful) $("input[value=" + messageId + "]").parent().remove();
                                    else $.jGrowl(viewData.errorMessage, { theme: 'error', life: 5000 });
                                });
                            });
                        }
                    }
                    if ($(this).find(":selected").html() == "Mark as Read") { alert("Mark as Read"); }
                    if ($(this).find(":selected").html() == "Mark as Unread") { alert("Mark as Unread"); }
                } else {
                    $.jGrowl("Error: No messages selected to " + $(this).find(":selected").html(), { theme: 'error', life: 5000 });
                    $(this).find("option:first").attr("selected", true);
                }
            }
        });
    }
}