﻿var Hospital = {
    InitNew: function() {
        $("#newHospitalForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Hospital.RebindList();
                            $.jGrowl("New Hospital successfully added.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('newhospital');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEdit: function() {
        $("#editHospitalForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Hospital.RebindList();
                            $.jGrowl("Hospital successfully updated.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('edithospital');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Delete: function(Id) {
        if (confirm("Are you sure you want to delete this hospital?")) {
            var input = "Id=" + Id;
            U.postUrl("/Hospital/Delete", { Id: Id }, function(result) {
                if (result.isSuccessful) {
                    Hospital.RebindList();
                    $.jGrowl("Hospital successfully deleted.", { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    RebindList: function() {
        var grid = $('#List_AgencyHospital').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    }
}