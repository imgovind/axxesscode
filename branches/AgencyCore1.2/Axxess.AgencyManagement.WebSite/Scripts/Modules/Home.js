﻿var Home = {
    Init: function() {
        if ($("#firstVisit").attr("id") != undefined) {
            U.showDialog("#firstVisit", function() {
                $("#firstVisitForm").validate({
                    submitHandler: function(form) {
                        var options = {
                            dataType: 'json',
                            beforeSubmit: function(values, form, options) {
                            },
                            success: function(result) {
                                var resultObject = eval(result);
                                if (resultObject.isSuccessful) {
                                    U.closeDialog();
                                    window.location.replace("/");
                                }
                                else {
                                    alert(resultObject.errorMessage);
                                }
                            }
                        };
                        $(form).ajaxSubmit(options);
                        return false;
                    }
                });
            });
        }

        if ($("#Change_Signature_Trigger").val() == "true") {
            UserInterface.ShowChangeSignatureModal();
        }
    },
    LoadWidgets: function() {
        U.postUrl("/User/ScheduleList", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data.Data != undefined) {
                for (var i = 0; i < data.Data.length && i < 5; i++) $('#scheduleWidgetContent').append("<tr><td>" + data.Data[i].PatientName + "</td><td>" + data.Data[i].TaskName + "</td><td>" + data.Data[i].StatusName + "</td><td>" + data.Data[i].VisitDate + "</td></tr>");
            }
            else {
                $('#scheduleWidgetContent').append("<tr><td colspan='5' class='align_center'><h1>No Scheduled Tasks found.</h1></td></tr>");
                $('#userScheduleWidgetMore').hide();
            }
        });
        U.postUrl("/Agency/RecertsPastDue", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data.Data != undefined) {
                for (var i = 0; i < data.Data.length && i < 5; i++) $('#recertPastDueWidgetContent').append("<tr><td>" + data.Data[i].PatientName + "</td><td>" + data.Data[i].PatientIdNumber + "</td><td>" + data.Data[i].TargetDate + "</td></tr>");
            } else {
                $('#recertPastDueWidgetContent').append("<tr><td colspan='3' class='align_center'><h1>No Past Due Recertifications found.</h1></td></tr>");
                $('#pastDueRecertsMore').hide();
            }
        });

        U.postUrl("/Agency/RecertsUpcoming", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data.Data != undefined) {
                for (var i = 0; i < data.Data.length && i < 5; i++) $('#recertUpcomingWidgetContent').append("<tr><td>" + data.Data[i].PatientName + "</td><td>" + data.Data[i].PatientIdNumber + "</td><td>" + data.Data[i].TargetDate + "</td></tr>");
            } else {
                $('#recertUpcomingWidgetContent').append("<tr><td colspan='3' class='align_center'><h1>No Upcoming Recertifications found.</h1></td></tr>");
                $('#upcomingRecertsMore').hide();
            }
        });

        U.postUrl("/Billing/Unprocessed", "", function(data) {
            if (data != undefined && data.length > 0) {
                for (var i = 0; i < data.length && i < 5; i++) $('#claimsWidgetContent').append("<tr><td>" + data[i].PatientName + "</td><td>" + data[i].Type + "</td><td>" + data[i].EpisodeRange + "</td></tr>");
            } else {
                $('#claimsWidgetContent').append("<tr><td colspan='3' class='align_center'><h1>No Bills found.</h1></td></tr>");
                $('#claimsWidgetMore').hide();
            }
        });
    }
}
