﻿var Contact = {
    InitNew: function() {
        $("#New_Contact_Other_Div").hide();
        $("#New_Contact_Type").change(function() {
            var otherType = $('#New_Contact_Type').val();
            if (otherType == "Other") {
                $("#New_Contact_Other_Div").show();
            }
            else {
                $("#New_Contact_Other_Div").hide();
            }
        });

        $("#newContactForm").validate({
            messages: {
                CompanyName: "",
                FirstName: "",
                LastName: "",
                ContactType: "",
                AddressLine1: "",
                AddressCity: "",
                AddressZipCode: "",
                AddressStateCode: "",
                PhonePrimaryArray: ""
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Contact.RebindList();
                            $.jGrowl("New contact successfully added.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('newcontact');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEdit: function() {
        $("#editContactForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Contact.RebindList();
                            $.jGrowl("Contact successfully updated.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('editcontact');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Delete: function(Id) {
        if (confirm("Are you sure you want to delete this contact?")) {
            var input = "id=" + Id;
            U.postUrl("/Contact/Delete", { Id: Id }, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    Contact.RebindList();
                    $.jGrowl("Contact successfully deleted.", { theme: 'success', life: 5000 });

                } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    RebindList: function() {
        var grid = $('#List_Contact').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    }
}