﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Change>" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Axxess&trade; Technology Solutions - Agency Management Desktop
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (Model == Change.Password) { %>
            <% Html.RenderPartial("FirstVisit"); %>
    <% } %>
    <%= Html.Hidden("ChangeSignature", Model == Change.Signature ? "true" : "false", new { @id = "Change_Signature_Trigger" }) %>
    <div id="missedVisitInfo" class="hidden"></div>
    <div id="Reset_Signature_Container" class="resetSignature hidden"></div>
    <div id="New_Photo_Container" class="newphotomodal hidden"></div>
    <div id="New_Physician_Container" class="newphysicianmodal hidden"></div>
    <div id="Missed_Visit_Container" class="missedvisitmodal hidden"></div>
    <div id="Admit_Patient_Container" class="newadmitpatientmodal hidden"></div>
    <div id="NonAdmit_Patient_Container" class="newnonadmitpatientmodal hidden"></div>
    <div id="Edit_PlanofCareMed_Container" class="planofcaremedicationmodal hidden"></div>
    <div id="New_Episode_Container" class="newepisodemodal hidden"></div>
    <div id="Edit_Episode_Container" class="editepisodemodal hidden"></div>
    <div id="New_Patient_NextSteps" class="modal-gray hidden">
        <div>Patient saved successfully.<br />What would you like to do next?</div>
        <ul>
            <li><a href="javascript:void(0);" onclick="U.closeDialog();UserInterface.CloseWindow('newpatient');UserInterface.ShowNewPatient();">Add Another Patient</a></li>
            <li><a href="javascript:void(0);" onclick="U.closeDialog();UserInterface.CloseWindow('newpatient');UserInterface.ShowPatientCenter();">Go to Patient Center</a></li>
            <li class="close"><a href="javascript:void(0);" onclick="U.closeDialog();UserInterface.CloseWindow('newpatient');" onmouseover="$(this).addClass('t-state-hover');">Close Window</a></li>
        </ul>
    </div>
    
</asp:Content>

