<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Forum.Master" Inherits="System.Web.Mvc.ViewPage<OpenForum.Core.ViewModels.PostViewModel>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%= (Model.IncludeDefaultStyles) ? ForumViewHelper.GetDefaultStyles() : "" %>
<% if (Model.IncludeValidationSummary) Html.ValidationSummary(); %>
<%= (Model.IncludeWysiwygEditor) ? ForumViewHelper.GetWysiwygEditorText(Url, "body") : ""%>
<% Html.BeginForm(); %>
    <label>Title</label>
    <div><%= Html.TextBox("title", Model.Post.Title, new { @class = "openforum_textbox" }) %></div>
    <label>Question</label>
    <div id="wysiwyg"><%= Html.TextArea("body", Model.Post.Body, new { @class = "openforum_textarea" }) %></div>
    <input id="submit" type="submit" value="Submit" class="hidden" />
    <%= Html.Hidden("submit", "Submit") %>
    <%= Html.Hidden("id", Model.Post.Id) %>
    <ul class="post_controls">
        <li class="first"><a href="javascript:void(0);" onclick="$('#submit').click()">Submit</a></li>
        <li><%= Html.ActionLink("Cancel", "Index") %></li>
    </ul>
<% Html.EndForm(); %>
</asp:Content>