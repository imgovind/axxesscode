<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Forum.Master" Inherits="System.Web.Mvc.ViewPage<OpenForum.Core.ViewModels.ReplyViewModel>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%= (Model.IncludeDefaultStyles) ? ForumViewHelper.GetDefaultStyles() : ""%>
<% if (Model.IncludeValidationSummary) Html.ValidationSummary(); %>
<%= (Model.IncludeWysiwygEditor) ? ForumViewHelper.GetWysiwygEditorText(Url, "body") : ""%>
<% Html.BeginForm(); %>
    <label for="body">Reply</label>
    <div id="wysiwyg"><%= Html.TextArea("body", Model.Reply.Body, new { @class = "openforum_textarea" }) %></div>
    <input id="submit" type="submit" value="Submit" class="hidden" />
    <%= Html.Hidden("id", Model.Reply.Id) %>
    <%= Html.Hidden("postId", Model.Reply.PostId) %>
    <ul class="post_controls">
        <li class="first"><a href="javascript:void(0);" onclick="$('#submit').click()">Submit</a></li>
        <li><%= Html.ActionLink("Cancel", "View", new { id = Model.Reply.PostId, title = ForumViewHelper.ToUrlFriendlyTitle(Model.Post.Title) })%></li>
        <li></li>
    </ul>
<% Html.EndForm(); %>
</asp:Content>
