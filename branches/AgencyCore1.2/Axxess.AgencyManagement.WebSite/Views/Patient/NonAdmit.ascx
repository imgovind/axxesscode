﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("AddNonAdmit", "Patient", FormMethod.Post, new { @id = "newNonAdmitPatientForm" }))   { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "NonAdmit_Patient_Id" })%>
<%= Html.Hidden("IsAdmit", "false", new { @id = "NonAdmit_Patient_IsAdmit" })%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Non-Admission Details</legend>
        <div><span class="bigtext align_center"><%= Model.DisplayName %></span></div>
        <div class="column">
            <div class="row"><label for="NonAdmit_Patient_Date" class="float_left">Date:</label><div class="float_right"><%= Html.Telerik().DatePicker().Name("NonAdmitDate").Value(DateTime.Today).HtmlAttributes(new { @id = "NonAdmit_Patient_Date", @class = "text date required" })%></div></div>
        </div>
        <table class="form"><tbody>
            <tr><td colspan="4"><label for="Comment"><strong>Reason Not Admitted:</strong></label></td></tr>
            <tr>
                <td><input id="NonAdmit_Patient_ReasonInAppropriate" type="checkbox" value="Inappropriate For Home Care" name="Reason" class="required radio float_left" /><label for="NonAdmit_Patient_ReasonInAppropriate" class="radio">Inappropriate For Home Care</label></td>
                <td><input id="NonAdmit_Patient_ReasonRefused" type="checkbox" value="Patient Refused Service" name="Reason" class="required radio float_left" /><label for="NonAdmit_Patient_ReasonRefused" class="radio">Patient Refused Service</label></td>
                <td><input id="NonAdmit_Patient_ReasonOutofService" type="checkbox" value="Out of Service Area" name="Reason" class="required radio float_left" /><label for="NonAdmit_Patient_ReasonOutofService" class="radio">Out of Service Area</label></td>
                <td><input id="NonAdmit_Patient_ReasonOnService" type="checkbox" value="On Service with another agency" name="Reason" class="required radio float_left" /><label for="NonAdmit_Patient_ReasonOnService" class="radio">On Service with another agency</label></td>
            </tr><tr>
                <td><input id="NonAdmit_Patient_ReasonNotaProvider" type="checkbox" value="Not a Provider" name="Reason" class="required radio float_left" /><label for="NonAdmit_Patient_ReasonNotaProvider" class="radio">Not a Provider</label></td>
                <td><input id="NonAdmit_Patient_ReasonNotHomebound" type="checkbox" value="Not Homebound" name="Reason" class="required radio float_left" /><label for="NonAdmit_Patient_ReasonNotHomebound" class="radio">Not Homebound</label></td>
                <td><input id="NonAdmit_Patient_ReasonRedirected" type="checkbox" value="Redirected to alternate care facility" name="Reason" class="required radio float_left" /><label for="NonAdmit_Patient_ReasonRedirected" class="radio">Redirected to alternate care facility</label></td>
                <td><input id="NonAdmit_Patient_ReasonOther" type="checkbox" value="Other" name="Reason" class="required radio float_left" /><label for="NonAdmit_Patient_ReasonRedirected" class="radio">Other (specify)</label></td>
            </tr>
        </tbody></table>
        <table class="form"><tbody>           
            <tr class="linesep vert">
               <td><label for="Comment"><strong>Comments:</strong></label>
                 <div ><%= Html.TextArea("Comments", "", new { @id = "NonAdmit_Patient_Comments" })%></div></td>
            </tr>
        </tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="U.closeDialog();UserInterface.CloseWindow('nonadmitpatient');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>
<%} %>

