﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientCenterViewData>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Patient Center | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','patientcenter');</script>")%>
<div class="wrapper layout">
    <div class="layout_left">
        <div class="top">
            <div class="buttons heading"><ul><li><a href="javascript:void(0);" onclick="javascript:acore.open('newpatient');" title="Add New Patient">Add New Patient</a></li></ul></div>
            <div class="row">Select Patient</div>
            <div class="row"><label>View:</label><div><select name="list" class="patientStatusDropDown"><option value="1">Active Patients</option><option value="2">Discharged Patients</option></select></div></div>
            <div class="row"><label>Filter:</label><div><select name="list" class="patientPaymentDropDown"><option value="0">All</option><option value="1">Medicare (traditional)</option><option value="2">Medicare (HMO/managed care)</option><option value="3">Medicaid (traditional)</option><option value="4">Medicaid (HMO/managed care)</option><option value="5">Workers' compensation</option><option value="6">Title programs </option><option value="7">Other government</option><option value="8">Private</option><option value="9">Private HMO/managed care</option><option value="10">Self Pay</option><option value="11">Unknown</option></select></div></div>
            <div class="row"><label>Find:</label><div><input id="txtSearch_Patient_Selection" class="text" name="" value="" type="text" /></div></div>
        </div>
        <div class="bottom"><% Html.RenderPartial("/Views/Patient/Patients.ascx", Model.Patients); %></div>
    </div>
    <div id="patientMainResult" class="layout_main">
        <% if (Model.Patients == null || Model.Patients.Count == 0) { %>
            <div class="abs center">No Patient Data Found</div>
        <% } %>
    </div>
</div>
<script type="text/javascript">
    $('#window_patientcenter .layout').layout({ west__paneSelector: '.layout_left' });
    $('#window_patientcenter .t-grid-content').css({ height: 'auto' });
</script>