﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("AddAdmit", "Patient", FormMethod.Post, new { @id = "newAdmitPatientForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Admit_Patient_Id" })%>
<%= Html.Hidden("IsAdmit", "true", new { @id = "NonAdmit_Patient_IsAdmit" })%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Admit Patient</legend>
        <div><span class="bigtext align_center"><%= Model.DisplayName %></span></div>
        <div class="column">
            <div class="row">
                <label for="Admit_Patient_PatientID" class="float_left"><span class="green">(M0020)</span> Patient ID:</label>
                <div class="float_right"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "Admit_Patient_PatientID", @class = "text input_wrapper", @maxlength = "50" }) %></div>
            </div>
            <div class="row">
                <label for="Admit_Patient_StartOfCareDate" class="float_left"><span class="green">(M0030)</span> Start of Care Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("StartOfCareDate").Value(Model.StartofCareDate).HtmlAttributes(new { @id = "Admit_Patient_StartOfCareDate", @class = "text required date", @onchange = "OnSocChange();" })%></div>
            </div>
            <div class="row">
                <label for="Admit_Patient_EpisodeStartDate" class="float_left">Episode Start Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("EpisodeStartDate").Value(DateTime.Today).HtmlAttributes(new { @id = "Admit_Patient_EpisodeStartDate", @class = "text required date" }) %></div>
            </div>
             <div class="row">
                <label for="Admit_Patient_PatientReferralDate" class="float_left">Referral Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("ReferralDate").Value((Model.ReferralDate > DateTime.MinValue) ? Model.ReferralDate.ToShortDateString() : "").HtmlAttributes(new { @id = "Admit_Patient_PatientReferralDate", @class = "text date" })%></div>
            </div>
            <div class="row">
                <label for="Admit_Patient_CaseManager" class="float_left">Case Manager:</label>
                <div class="float_right"><%= Html.CaseManagers("CaseManagerId", (Model != null && !Model.CaseManagerId.IsEmpty()) ? Model.CaseManagerId.ToString() : "", new { @id = "Admit_Patient_CaseManager", @class = "Users requireddropdown valid" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="Admit_Patient_PrimaryInsurance" class="float_left">Primary Insurance:</label>
                <div class="float_right"><%= Html.Insurances("PrimaryInsurance", Model.PrimaryInsurance, false, new { @id = "Admit_Patient_PrimaryInsurance" })%></div>
            </div><div class="row">
                <label for="Admit_Patient_SecondaryInsurance" class="float_left">Secondary Insurance:</label>
                <div class="float_right"><%= Html.Insurances("SecondaryInsurance", Model.SecondaryInsurance, false, new { @id = "Admit_Patient_SecondaryInsurance" })%></div>
            </div>
            <div class="row">
                <label for="Admit_Patient_TertiaryInsurance" class="float_left">Tertiary Insurance:</label>
                <div class="float_right"><%= Html.Insurances("TertiaryInsurance", Model.TertiaryInsurance, false, new { @id = "Admit_Patient_TertiaryInsurance" })%></div>
            </div>
            <div class="row">
                <label for="Admit_Patient_Payer" class="float_left">Payer:</label>
                <div class="float_right"><select name="Payer" id="Admit_Patient_Payer"><option value="0">-- Select Payer --</option><option value="00380">Palmetto GBA</option><option value="00010">Cahaba GBA</option><option value="00454">National Government Services</option><option value="00180">Anthem Health Plans of Maine</option></select></div>
            </div>
             <div class="row">
                <label for="Admit_Patient_Assign" class="float_left">Assign to Clinician:</label>
                <div class="float_right"><%= Html.Clinicians("UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : "", new { @id = "Admit_Patient_Assign", @class = "requireddropdown Users  valid" })%></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Admit</a></li>
            <li><a href="javascript:void(0);" onclick="U.closeDialog();UserInterface.CloseWindow('admitpatient');">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");

    function OnSocChange() {
        var e = $("#Admit_Patient_StartOfCareDate").data("tDatePicker").value();
        $("#Admit_Patient_EpisodeStartDate").data("tDatePicker").value(e);
        $("#Admit_Patient_EpisodeStartDate").data("tDatePicker").minDate.value = e;
    }
</script>
<% } %>