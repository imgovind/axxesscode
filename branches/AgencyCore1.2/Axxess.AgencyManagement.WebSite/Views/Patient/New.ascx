﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('New Patient | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','newpatient');</script>")%>
<% using (Html.BeginForm("Add", "Patient", FormMethod.Post, new { @id = "newPatientForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_FirstName" class="float_left"><span class="green">(M0040)</span> First Name:</label>
                <div class="float_right"><%= Html.TextBox("FirstName", (Model != null && Model.FirstName.IsNotNullOrEmpty()) ? Model.FirstName : "", new { @id = "New_Patient_FirstName", @class = "text input_wrapper required", @maxlength = "30" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_MiddleInitial" class="float_left"><span class="green">(M0040)</span> MI:</label>
                <div class="float_right"><%= Html.TextBox("MiddleInitial", "", new { @id = "New_Patient_MiddleInitial", @class = "text input_wrapper mi", @maxlength = "1" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_LastName" class="float_left"><span class="green">(M0040)</span> Last Name:</label>
                <div class="float_right"><%= Html.TextBox("LastName", (Model != null && Model.LastName.IsNotNullOrEmpty()) ? Model.LastName : "", new { @id = "New_Patient_LastName", @class = "text input_wrapper required", @maxlength = "30" }) %></div>
            </div>
            <div class="row">
                <label class="float_left"><span class="green">(M0069)</span> Gender:</label>
                <div class="float_right"><%= Html.RadioButton("Gender", "Female", new { @id = "New_Patient_Gender_F", @class = "required radio" }) %><label for="New_Patient_Gender_F" class="inlineradio">Female</label><%= Html.RadioButton("Gender", "Male", new { @id = "New_Patient_Gender_M", @class = "required radio" }) %><label for="New_Patient_Gender_F" class="inlineradio">Male</label></div>
            </div>
            <div class="row">
                <label for="New_Patient_DOB" class="float_left"><span class="green">(M0066)</span> Date of Birth:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("DOB").Value(string.Empty).HtmlAttributes(new { @id = "New_Patient_DOB", @class = "text required date" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_MaritalStatus" class="float_left">Marital Status:</label>
                <div class="float_right"><select id="New_Patient_MaritalStatus" class="valid" name="MaritalStatus"><option value="0" selected="selected">-- Select Marital Status --</option><option value="Married">Married</option><option value="Divorce" >Divorced</option><option value="Widowed">Widowed</option><option value="Single">Single</option><option value="Unknown">Unknown</option></select></div>
            </div>
            <div class="row">
                <label for="New_Patient_LocationId" class="float_left">Agency Branch:</label>
                <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "New_Patient_LocationId", @class = "BranchLocation required" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_CaseManager" class="float_left">Case Manager:</label>
                <div class="float_right"><%= Html.CaseManagers("CaseManagerId", "", new { @id = "New_Patient_CaseManager", @class = "Users requireddropdown valid" }) %></div>
            </div>
             <div class="row" style="display:none;">
                <div class="float_right ancillary_button"><a href="javascript:void(0);" onclick="alert('TODO: Link to Verify Elligibility Window');">Verify Elligibility</a></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_Patient_PatientID" class="float_left"><span class="green">(M0020)</span> Patient ID:</label>
                <div class="float_right"><%= Html.TextBox("PatientIdNumber", "", new { @id = "New_Patient_PatientID", @class = "text input_wrapper", @maxlength = "15" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_MedicareNumber" class="float_left"><span class="green">(M0063)</span> Medicare Number:</label>
                <div class="float_right"><%= Html.TextBox("MedicareNumber", (Model != null && Model.MedicareNumber.IsNotNullOrEmpty()) ? Model.MedicareNumber : "", new { @id = "New_Patient_MedicareNumber", @class = "text input_wrapper", @maxlength = "11" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_MedicaidNumber" class="float_left"><span class="green">(M0065)</span> Medicaid Number:</label>
                <div class="float_right"><%= Html.TextBox("MedicaidNumber", (Model != null && Model.MedicaidNumber.IsNotNullOrEmpty()) ? Model.MedicaidNumber : "", new { @id = "New_Patient_MedicaidNumber", @class = "text digits input_wrapper", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_SSN" class="float_left"><span class="green">(M0064)</span> SSN:</label>
                <div class="float_right"><%= Html.TextBox("SSN", (Model != null && Model.SSN.IsNotNullOrEmpty()) ? Model.SSN : "", new { @id = "New_Patient_SSN", @class = "text digits input_wrapper", @maxlength = "9" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_StartOfCareDate" class="float_left"><span class="green">(M0030)</span> Start of Care Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("StartOfCareDate").Value(DateTime.Today).ClientEvents(events=> events.OnChange("Patient.OnSocChange")).HtmlAttributes(new { @id = "New_Patient_StartOfCareDate", @class = "text required date" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_EpisodeStartDate" class="float_left">Episode Start Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("EpisodeStartDate").Value(DateTime.Now).MinDate(DateTime.Now).HtmlAttributes(new { @id = "New_Patient_EpisodeStartDate", @class = "text required date" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_Assign" class="float_left">Assign to Clinician/Case Manager:</label>
                <div class="float_right"><%= Html.Clinicians("UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : "", new { @id = "New_Patient_Assign", @class = "requireddropdown Users  valid" })%></div>
            </div>
            <div class="row"><input id="New_Patient_CreateEpisode" type="checkbox" checked="checked" value="true" name="ShouldCreateEpisode" class="radio float_left" /><label for="New_Patient_CreateEpisode" class="radio">Create Episode & Schedule Start of Care Visit after saving </label><em>(If this box is unchecked, patient will be added to list of pending admissions)</em></div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0140)</span> Race/Ethnicity</legend>
        <table class="form"><tbody>
            <tr>
                <td><input id="New_Patient_RaceAmericanIndian" type="checkbox" value="0" name="EthnicRaces" class="required radio float_left" /><label for="New_Patient_RaceAmericanIndian" class="radio">American Indian or Alaska Native</label></td>
                <td><input id="New_Patient_RaceAsian" type="checkbox" value="1" name="EthnicRaces" class="required radio float_left" /><label for="New_Patient_RaceAsian" class="radio">Asian</label></td>
                <td><input id="New_Patient_RaceBlack" type="checkbox" value="2" name="EthnicRaces" class="required radio float_left" /><label for="New_Patient_RaceBlack" class="radio">Black or African-American</label></td>
                <td><input id="New_Patient_RaceHispanic" type="checkbox" value="3" name="EthnicRaces" class="required radio float_left" /><label for="New_Patient_RaceHispanic" class="radio">Hispanic or Latino</label></td>
            </tr><tr>
                <td><input id="New_Patient_RaceHawaiian" type="checkbox" value="4" name="EthnicRaces" class="required radio float_left" /><label for="New_Patient_RaceHawaiian" class="radio">Native Hawaiian or Pacific Islander</label></td>
                <td><input id="New_Patient_RaceWhite" type="checkbox" value="5" name="EthnicRaces" class="required radio float_left" /><label for="New_Patient_RaceWhite" class="radio">White</label></td>
                <td colspan="2"><input id="New_Patient_RaceUnknown" type="checkbox" value="6" name="EthnicRaces" class="required radio float_left" /><label for="New_Patient_RaceUnknown" class="radio">Unknown</label></td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0150)</span> Payment Source <span class="light">(Mark all that apply)</span></legend>
        <table class="form"><tbody>
            <tr>
                <td><input id="New_Patient_PaymentSourceNone" type="checkbox" value="0" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourceNone" class="radio"> None; no charge for current services</label></td>
                <td><input id="New_Patient_PaymentSourceMedicare" type="checkbox" value="1" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourceMedicare" class="radio"> Medicare (traditional fee-for-service)</label></td>
                <td><input id="New_Patient_PaymentSourceMedicareHmo" type="checkbox" value="2" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourceMedicareHmo" class="radio"> Medicare (HMO/ managed care)</label></td>
                <td><input id="New_Patient_PaymentSourceMedicaid" type="checkbox" value="3" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourceMedicaid" class="radio"> Medicaid (traditional fee-for-service)</label></td>
            </tr><tr>
                <td><input id="New_Patient_PaymentSourceMedicaidHmo" type="checkbox" value="4" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourceMedicaidHmo" class="radio"> Medicaid (HMO/ managed care)</label></td>
                <td><input id="New_Patient_PaymentSourceWorkers" type="checkbox" value="5" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourceWorkers" class="radio"> Workers' compensation</label></td>
                <td><input id="New_Patient_PaymentSourceTitleProgram" type="checkbox" value="6" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourceTitleProgram" class="radio"> Title programs (e.g., Titile III,V, or XX)</label></td>
                <td><input id="New_Patient_PaymentSourceOtherGovernment" type="checkbox" value="7" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourceOtherGovernment" class="radio"> Other government (e.g.,CHAMPUS,VA,etc)</td>
            </tr><tr>
                <td><input id="New_Patient_PaymentSourcePrivate" type="checkbox" value="8" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourcePrivate" class="radio"> Private insurance</td>
                <td><input id="New_Patient_PaymentSourcePrivateHmo" type="checkbox" value="9" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourcePrivateHmo" class="radio"> Private HMO/ managed care</td>
                <td><input id="New_Patient_PaymentSourceSelf" type="checkbox" value="10" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourceSelf" class="radio"> Self-pay</td>
                <td><input id="New_Patient_PaymentSourceUnknown" type="checkbox" value="11" name="PaymentSources" class="required radio float_left" /><label for="New_Patient_PaymentSourceUnknown" class="radio"> Unknown</td>
            </tr><tr>
                <td colspan='4'><input type="checkbox" id="New_Patient_PaymentSource" value="12" name="PaymentSources" class="radio float_left" /> <label for="New_Patient_PaymentSource" class="radio more">Other (specify)</label><%= Html.TextBox("OtherPaymentSource", "", new { @id = "New_Patient_OtherPaymentSource", @class = "text", @style = "display:none;" }) %></td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend>Patient Address</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_AddressLine1" class="float_left">Address Line 1:</label>
                <div class="float_right"><%= Html.TextBox("AddressLine1", (Model != null && Model.AddressLine1.IsNotNullOrEmpty()) ? Model.AddressLine1.ToString() : "", new { @id = "New_Patient_AddressLine1", @class = "text required input_wrapper" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_AddressLine2" class="float_left">Address Line 2:</label>
                <div class="float_right"><%= Html.TextBox("AddressLine2", (Model != null && Model.AddressLine2.IsNotNullOrEmpty()) ? Model.AddressLine2.ToString() : "", new { @id = "New_Patient_AddressLine2", @class = "text input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_AddressCity" class="float_left">City:</label>
                <div class="float_right"><%= Html.TextBox("AddressCity", (Model != null && Model.AddressCity.IsNotNullOrEmpty()) ? Model.AddressCity.ToString() : "", new { @id = "New_Patient_AddressCity", @class = "text required input_wrapper" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_AddressStateCode" class="float_left"><span class="green">(M0050)</span> State, <span class="green">(M0060)</span> Zip:</label>
                <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", (Model != null && Model.AddressStateCode.IsNotNullOrEmpty()) ? Model.AddressStateCode.ToString() : "", new { @id = "New_Patient_AddressStateCode", @class = "AddressStateCode requireddropdown valid" })%><%= Html.TextBox("AddressZipCode", (Model != null && Model.AddressZipCode.IsNotNullOrEmpty()) ? Model.AddressZipCode.ToString() : "", new { @id = "New_Patient_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Patient_HomePhone1" class="float_left">Home Phone:</label>
                <div class="float_right"><%=Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() ? Model.PhoneHome.Substring(0, 3) : "", new { @id = "New_Patient_HomePhone1", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "3" })%> - <%=Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() ? Model.PhoneHome.Substring(3, 3) : "", new { @id = "New_Patient_HomePhone2", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3", @tabindex = "20" })%> - <%=Html.TextBox("PhoneHomeArray", Model != null && Model.PhoneHome.IsNotNullOrEmpty() ? Model.PhoneHome.Substring(6, 4) : "", new { @id = "New_Patient_HomePhone3", @class = "autotext required digits phone_long", @maxlength = "4", @size = "5" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_MobilePhone1" class="float_left">Mobile Phone:</label>
                <div class="float_right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="PhoneMobileArray" id="New_Patient_MobilePhone1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_short" name="PhoneMobileArray" id="New_Patient_MobilePhone2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_long" name="PhoneMobileArray" id="New_Patient_MobilePhone3" maxlength="4" /></div>
            </div>
            <div class="row">
                <label for="New_Patient_Email" class="float_left">Email:</label>
                <div class="float_right"><%= Html.TextBox("EmailAddress", (Model != null && Model.EmailAddress.IsNotNullOrEmpty()) ? Model.EmailAddress : "", new { @id = "New_Patient_Email", @class = "text input_wrapper" }) %></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float_left">
        <legend>Physician Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_PhysicianDropDown1" class="float_left">Primary Physician:</label>
                <div class="float_right"><%= Html.Physicians("AgencyPhysicians", "", true, new { @id = "New_Patient_PhysicianDropDown1", @class = "Physicians" })%><a href="javascript:void(0);" onclick="Patient.addPhysician(this)">add row</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Patient_PhysicianDropDown2" class="float_left">Secondary Physician:</label>
                <div class="float_right"><%= Html.Physicians("AgencyPhysicians", "", true, new { @id = "New_Patient_PhysicianDropDown2", @class = "Physicians" }) %><a href="javascript:void(0);" onclick="Patient.removePhysician(this)">remove</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Patient_PhysicianDropDown3" class="float_left">Secondary Physician:</label>
                <div class="float_right"><%= Html.Physicians("AgencyPhysicians", "", true, new { @id = "New_Patient_PhysicianDropDown3", @class = "Physicians" })%><a href="javascript:void(0);" onclick="Patient.removePhysician(this)">remove</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Patient_PhysicianDropDown4" class="float_left">Secondary Physician:</label>
                <div class="float_right"><%= Html.Physicians("AgencyPhysicians", "", true, new { @id = "New_Patient_PhysicianDropDown4", @class = "Physicians" })%><a href="javascript:void(0);" onclick="Patient.removePhysician(this)">remove</a></div>
            </div>
            <div class="row hidden">
                <label for="New_Patient_PhysicianDropDown5" class="float_left">Secondary Physician:</label>
                <div class="float_right"><%= Html.Physicians("AgencyPhysicians", "", true, new { @id = "New_Patient_PhysicianDropDown5", @class = "Physicians" })%><a href="javascript:void(0);" onclick="Patient.removePhysician(this)">remove</a></div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="float_right ancillary_button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float_right">
        <legend>Pharmacy Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_PharmacyName" class="float_left">Name:</label>
                <div class="float_right"><%= Html.TextBox("PharmacyName", "", new { @id = "New_Patient_PharmacyName", @class = "text input_wrapper", @maxlength = "20" }) %></div>
            </div>
            <div class="row">
                <label for="New_Patient_PharmacyPhone" class="float_left">Phone:</label>
                <div class="float_right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="PharmacyPhoneArray" id="New_Patient_PharmacyPhone1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_short" name="PharmacyPhoneArray" id="New_Patient_PharmacyPhone2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_long" name="PharmacyPhoneArray" id="New_Patient_PharmacyPhone3" maxlength="4" /></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="half float_left">
        <legend>Insurance / Payor Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_PrimaryInsurance" class="float_left">Primary:</label>
                <div class="float_right"><%= Html.Insurances("PrimaryInsurance", "", true, new { @id = "New_Patient_PrimaryInsurance", @class = "Insurances requireddropdown" })%></div>
            </div><div class="row">
                <label for="New_Patient_SecondaryInsurance" class="float_left">Secondary:</label>
                <div class="float_right"><%= Html.Insurances("SecondaryInsurance", "", true, new { @id = "New_Patient_SecondaryInsurance", @class = "Insurances" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_TertiaryInsurance" class="float_left">Tertiary:</label>
                <div class="float_right"><%= Html.Insurances("TertiaryInsurance", "",true, new { @id = "New_Patient_TertiaryInsurance" , @class = "Insurances"})%></div>
            </div>
        </div>
    </fieldset>
     <fieldset class="half float_right">
        <legend>Emergency Triage <span class="light">(Select one)</span></legend>
        <div class="column">
            <div class="row"><input id="New_Patient_Triage1" type="checkbox" value="1" name="Triage" class="radio Triage float_left" /><div class="float_left">&nbsp;1.&nbsp;</div><label class="normal margin" for="New_Patient_Triage1">Life threatening (or potential) and requires ongoing medical treatment. When necessary, appropriate arrangements for evacuation to an acute care facility will be made.</label></div>
            <div class="row"><input id="New_Patient_Triage2" type="checkbox" value="2" name="Triage" class="radio Triage float_left" /><div class="float_left">&nbsp;2.&nbsp;</div><label class="normal margin" for="New_Patient_Triage2">Not life threatening but would suffer severe adverse effects from interruption of services (i.e., daily insulin, IV medications, sterile wound care of a wound with a large amount of drainage.)</label></div>
            <div class="row"><input id="New_Patient_Triage3" type="checkbox" value="3" name="Triage" class="radio Triage float_left" /><div class="float_left">&nbsp;3.&nbsp;</div><label class="normal margin" for="New_Patient_Triage3">Visits could be postponed 24-48 hours without adverse effects (i.e., new insulin dependent diabetic able to self inject, sterile wound care with a minimal amount to no drainage)</label></div>
            <div class="row"><input id="New_Patient_Triage4" type="checkbox" value="4" name="Triage" class="radio Triage float_left" /><div class="float_left">&nbsp;4.&nbsp;</div><label class="normal margin" for="New_Patient_Triage4">Visits could be postponed 72-96 hours without adverse effects (i.e., post op with no open wound, anticipated discharge within the next 10-14 days, routine catheter changes)</label></div>
        </div>
    </fieldset>
    
       <fieldset class="half float_left">
        <legend>Primary Emergency Contact</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_EmergencyContactFirstName" class="float_left">First Name:</label>
                <div class="float_right"><%= Html.TextBox("EmergencyContact.FirstName", "", new { @id = "New_Patient_EmergencyContactFirstName", @class = "text input_wrapper", @maxlength = "100" }) %></div>
            </div><div class="row">
                <label for="New_Patient_EmergencyContactLastName" class="float_left">Last Name:</label>
                <div class="float_right"><%= Html.TextBox("EmergencyContact.LastName", "", new { @id = "New_Patient_EmergencyContactLastName", @class = "text input_wrapper", @maxlength = "100" }) %></div>
            </div><div class="row">
                <label for="New_Patient_EmergencyContactRelationship" class="float_left">Relationship:</label>
                <div class="float_right"><%= Html.TextBox("EmergencyContact.Relationship", "", new { @id = "New_Patient_EmergencyContactRelationship", @class = "text input_wrapper" }) %></div>
            </div><div class="row">
                <label for="New_Patient_EmergencyContactPhonePrimary1" class="float_left">Primary Phone:</label>
                <div class="float_right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhonePrimaryArray" id="New_Patient_EmergencyContactPhonePrimary1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_short" name="EmergencyContact.PhonePrimaryArray" id="New_Patient_EmergencyContactPhonePrimary2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_long" name="EmergencyContact.PhonePrimaryArray" id="New_Patient_EmergencyContactPhonePrimary3" maxlength="4" /></div>
            </div><div class="row">
                <label for="New_Patient_EmergencyContactEmail" class="float_left">Email:</label>
                <div class="float_right"><%= Html.TextBox("EmergencyContact.EmailAddress", "", new { @id = "New_Patient_EmergencyContactEmail", @class = "text email input_wrapper", @maxlength = "100" }) %></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_ReferralPhysician" class="float_left">Physician:</label>
                <div class="float_right"><%= Html.Physicians("ReferrerPhysician", (Model != null && !Model.ReferrerPhysician.IsEmpty()) ? Model.ReferrerPhysician.ToString() : "", true, new { @id = "New_Patient_ReferrerPhysician", @class = "ReferrerPhysician Physicians" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_AdmissionSource" class="float_left">Admission Source:</label>
                <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", (Model != null && Model.AdmissionSource > 0) ? Model.AdmissionSource.ToString() : "", new { @id = "New_Patient_AdmissionSource", @class = "AdmissionSource" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_OtherReferralSource" class="float_left">Other Referral Source:</label>
                <div class="float_right"><%= Html.TextBox("OtherReferralSource", (Model != null && Model.OtherReferralSource.IsNotNullOrEmpty()) ? Model.OtherReferralSource : "", new { @id = "New_Patient_OtherReferralSource", @class = "text input_wrapper", @maxlength = "30" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_Patient_PatientReferralDate" class="float_left">Referral Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("ReferralDate").Value((Model != null && Model.ReferralDate != DateTime.MinValue) ? Model.ReferralDate.ToString() : "").HtmlAttributes(new { @id = "New_Patient_PatientReferralDate", @class = "text date" })%></div>
            </div>
            <div class="row">
                <label for="New_Patient_InternalReferral" class="float_left">Internal Referral:</label>
                <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", (Model != null && !Model.InternalReferral.IsEmpty()) ? Model.InternalReferral.ToString() : "", new { @id = "New_Patient_InternalReferral", @class = "Users valid" })%></div>
            </div>
        </div>
    </fieldset>
 
<fieldset>
    <legend>Services Required <span class="light">(Optional)</span></legend>
    <table class="form">
        <tbody>
            <%string[] servicesRequired = Model != null && Model.ServicesRequired != null && Model.ServicesRequired.IsNotNullOrEmpty() ? Model.ServicesRequired.Split(';') : null;  %><input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
            <tr>
                <td><%= string.Format("<input id ='New_Patient_ServiceRequiredSNV' type='checkbox' value='0' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("0") ? "checked='checked'" : "")%>
                    <label for="New_Patient_ServiceRequiredSNV" class="radio">SNV</label></td>
                <td><%= string.Format("<input id ='New_Patient_ServiceRequiredHHA' type='checkbox' value='1' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("1") ? "checked='checked'" : "")%>
                    <label for="New_Patient_ServiceRequiredHHA" class="radio">HHA</label></td>
                <td><%= string.Format("<input id ='New_Patient_ServiceRequiredPT' type='checkbox' value='2' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("2") ? "checked='checked'" : "")%>
                    <label for="New_Patient_ServiceRequiredPT" class="radio">PT</label></td>
                <td><%= string.Format("<input id ='New_Patient_ServiceRequiredOT' type='checkbox' value='3' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("3") ? "checked='checked'" : "")%>
                    <label for="New_Patient_ServiceRequiredOT" class="radio">OT</label></td>
                <td><%= string.Format("<input id ='New_Patient_ServiceRequiredST' type='checkbox' value='4' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("4") ? "checked='checked'" : "")%>
                    <label for="New_Patient_ServiceRequiredST" class="radio">SP</label></td>
                <td><%= string.Format("<input id ='New_Patient_ServiceRequiredMSW' type='checkbox' value='5' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("5") ? "checked='checked'" : "")%>
                    <label for="New_Patient_ServiceRequiredMSW" class="radio">MSW</label></td>
            </tr>
        </tbody>
    </table>
</fieldset>
    <fieldset>
        <legend>DME Needed</legend>
        <table class="form">
        <%string[] DME = (Model != null && Model.DME.IsNotNullOrEmpty()) ? Model.DME.Split(';') : null;  %><input type="hidden" value=" " class="radio" name="DMECollection" />
         <tbody>
            <tr class="firstrow">
                <td><%= string.Format("<input id='New_Patient_DMECollectionBed' type='checkbox' value='0' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("0") ? "checked='checked'" : "")%>
                    <label for="New_Patient_DMECollectionBed" class="radio">Bedside Commode</label></td>
                <td><%= string.Format("<input id='New_Patient_DMECollectionCane' type='checkbox' value='1' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("1") ? "checked='checked'" : "")%>
                    <label for="New_Patient_DMECollectionCane" class="radio">Cane</label></td>
                <td><%= string.Format("<input id='New_Patient_DMECollectionToilet' type='checkbox' value='2' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("2") ? "checked='checked'" : "")%>
                    <label for="New_Patient_DMECollectionToilet" class="radio">Elevated Toilet Seat</label></td>
                <td><%= string.Format("<input id='New_Patient_DMECollectionBars' type='checkbox' value='3' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("3") ? "checked='checked'" : "")%>
                    <label for="New_Patient_DMECollectionBars" class="radio">Grab Bars</label></td>
                <td><%= string.Format("<input id='New_Patient_DMECollectionHospitalBed' type='checkbox' value='4' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("4") ? "checked='checked'" : "")%>
                    <label for="New_Patient_DMECollectionHospitalBed" class="radio">Hospital Bed</label></td>
            </tr><tr>
                <td><%= string.Format("<input id='New_Patient_DMECollectionNebulizer' type='checkbox' value='5' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("5") ? "checked='checked'" : "")%>
                    <label for="New_Patient_DMECollectionNebulizer" class="radio">Nebulizer</label></td>
                <td><%= string.Format("<input id='New_Patient_DMECollectionOxygen' type='checkbox' value='6' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("6") ? "checked='checked'" : "")%>
                    <label for="New_Patient_DMECollectionOxygen" class="radio">Oxygen</label></td>
                <td><%= string.Format("<input id='New_Patient_DMECollectionTub' type='checkbox' value='7' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("7") ? "checked='checked'" : "")%>
                    <label for="New_Patient_DMECollectionTub" class="radio">Tub/Shower Bench</label></td>
                <td><%= string.Format("<input id='New_Patient_DMECollectionWalker' type='checkbox' value='8' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("8") ? "checked='checked'" : "")%>
                    <label for="New_Patient_DMECollectionWalker" class="radio">Walker</label></td>
                <td><%= string.Format("<input id='New_Patient_DMECollectionWheelChair' type='checkbox' value='9' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("9") ? "checked='checked'" : "")%>
                    <label for="New_Patient_DMECollectionWheelChair" class="radio">Wheelchair</label></td>
            </tr><tr>
                <td colspan="5"><%= string.Format("<input id='New_Patient_DMEOther' type='checkbox' value='10' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("10") ? "checked='checked'" : "")%>
                    <label for="New_Patient_DMEOther" class="radio">Other</label><%=Html.TextBox("OtherDME", (Model != null && Model.OtherDME.IsNotNullOrEmpty()) ? Model.OtherDME : "", new { @id = "New_Patient_OtherDME", @class = "text", @style = "display:none;" })%></td>
            </tr>
        </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide_column">
            <div class="row"><textarea id="New_Patient_Comments" name="Comments" cols="5" rows="6"></textarea></div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_Patient_Status" })%>     
    <%= Html.Hidden("ReferralId", (Model != null && !Model.Id.IsEmpty()) ? Model.Id.ToString() : "false", new { @id = "New_Patient_ReferralId" })%>     
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$('#New_Patient_Status').val('3');$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$('#New_Patient_Status').val('1');$(this).closest('form').submit();">Complete</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newpatient');">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $("select.Physicians").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            UserInterface.ShowNewPhysicianModal();
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") {
            selectList.selectedIndex = 0;
        }
    });
    $("select.ReferrerPhysician").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            UserInterface.ShowNewPhysicianModal();
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") {
            selectList.selectedIndex = 0;
        }
    });


    $("select.Insurances").change(function() {
        var selectList = this;
        if ($(this).val() == "new") {
            acore.open('newinsurance');
            selectList.selectedIndex = 0;
        } else if ($(this).val() == "spacer") {
            selectList.selectedIndex = 0;
        }
    });
   
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row div.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
    $("#New_Patient_SSN").mask("999999999", { placeholder: "" });
</script>
<% } %>