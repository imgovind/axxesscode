﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PatientProfile>" %>
<% var data = Model.CurrentAssessment != null ? Model.CurrentAssessment.ToDictionary() : new Dictionary<string, Question>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Patient Profile<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body>
<% 
    if (Model != null) { %>
    <div class="page largerfont"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <div>
            <table class="fixed"><tbody>
                <tr>
                    <td colspan="2">
                        <%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + "<br />" : "" %>
                        <%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1 : ""%> <%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2 : ""%><br />
                        <%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>
                    </td><th class="h1">Patient Profile</th>
                </tr><tr>
                    <td colspan="2">
                        <span class="float_right auto">SOC: <%= Model.Patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? Model.Patient.StartOfCareDateFormatted : "" %><br />Referral Date:<%= Model.Patient.ReferralDate.IsValid()? Model.Patient.ReferralDate.ToString("MM/dd/yyyy"):"" %></span>
                        <span class="big">Patient Name: <%= Model.Patient.LastName %>, <%= Model.Patient.FirstName %> <%= Model.Patient.MiddleInitial %></span><br />
                        <%= Model.Patient.AddressLine1.IsNotNullOrEmpty() ? Model.Patient.AddressLine1.ToTitleCase() + "<br />" : ""%>
                        <%= Model.Patient.AddressLine2.IsNotNullOrEmpty() ? Model.Patient.AddressLine2.ToTitleCase() + "<br />" : ""%>
                        <%= Model.Patient.AddressCity.IsNotNullOrEmpty() ? Model.Patient.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Patient.AddressStateCode.IsNotNullOrEmpty() ? Model.Patient.AddressStateCode + "&nbsp; " : ""%><%= Model.Patient.AddressZipCode.IsNotNullOrEmpty() ? Model.Patient.AddressZipCode : ""%><br />
                        <%= Model.Patient.PatientIdNumber.IsNotNullOrEmpty() ? "ID: " + Model.Patient.PatientIdNumber : "" %><span class="blank"></span><%= Model.Patient.PhoneHome.IsNotNullOrEmpty() ? "Phone: " + Model.Patient.PhoneHome.ToPhone() : ""%>
                        <span>Cert. Period: <%= Model.CurrentEpisode.StartDateFormatted.IsNotNullOrEmpty() && Model.CurrentEpisode.EndDateFormatted.IsNotNullOrEmpty() ? Model.CurrentEpisode.StartDateFormatted + " &ndash; " + Model.CurrentEpisode.EndDateFormatted : ""%></span>
                    </td><td>
                        <span class="bicol">
                            <span><strong>Birthday:</strong><%= Model.Patient.DOBFormatted.IsNotNullOrEmpty() ? Model.Patient.DOBFormatted : "" %></span>
                            <span><strong>Age:</strong><%= Model.Patient.DOB.ToString("yyyy").IsNotNullOrEmpty() ? (DateTime.Now.Month < Model.Patient.DOB.Month || (DateTime.Now.Month == Model.Patient.DOB.Month && DateTime.Now.Day < Model.Patient.DOB.Day)) ? (DateTime.Today.ToString("yyyy").ToInteger() - Model.Patient.DOB.ToString("yyyy").ToInteger() - 1).ToString() : (DateTime.Today.ToString("yyyy").ToInteger() - 1 - Model.Patient.DOB.ToString("yyyy").ToInteger()).ToString() : "" %></span>
                            <span><strong>Sex:</strong><%= Model.Patient.Gender.IsNotNullOrEmpty() ? Model.Patient.Gender : "" %></span>
                            <span><strong>Marital:</strong><%= Model.Patient.MaritalStatus.IsNotNullOrEmpty()&& Model.Patient.MaritalStatus!="0"? Model.Patient.MaritalStatus : "" %></span>
                        </span>
                        <span><span><strong>County:</strong></span></span>
                        <span><span><strong>Office:</strong><%= Model.Agency.MainLocation.Name.IsNotNullOrEmpty() ? Model.Agency.MainLocation.Name : "" %></span></span>
                        <span class="bicol">
                            <span><strong>Team:</strong></span>
                            <span><strong>Retired:</strong></span>
                        </span>
                         <%string[] ethnicities = Model.Patient.Ethnicities != null && Model.Patient.Ethnicities != "" ? Model.Patient.Ethnicities.Split(';') : null;
                           var race = string.Empty;
                           if (ethnicities != null)
                           {
                               foreach(var ethnic in ethnicities){
                                   int result;
                                   if(Int32.TryParse(ethnic,out result)){
                                   race += ((Race)Enum.ToObject(typeof(Race), (result))).GetDescription();
                                   race += " ";
                                   }
                               }
                           }
                           %>
                        <span><span><strong>Race:</strong><%= race%></span></span>
                        <span class="bicol">
                            <span><strong>Height:</strong><%= data.ContainsKey("GenericHeight") ? data["GenericHeight"].Answer : "" %></span>
                            <span><strong>Weight:</strong><%= data.ContainsKey("GenericWeight") ? data["GenericWeight"].Answer : "" %></span>
                        </span>
                    </td>
                </tr>
            </tbody></table>
        </div>
        <h3>Insurance</h3>
        <div class="tricol">
            <span><strong>Primary Insurance</strong></span>
            <span><strong>Secondary Insurance</strong></span>
            <span><strong>Tertiary Insurance</strong></span>
            <span><%= Model.Patient.PrimaryInsuranceName.IsNotNullOrEmpty() ? Model.Patient.PrimaryInsuranceName : "" %></span>
            <span><%= Model.Patient.SecondaryInsuranceName.IsNotNullOrEmpty() ? Model.Patient.SecondaryInsuranceName : ""%></span>
            <span><%= Model.Patient.TertiaryInsuranceName.IsNotNullOrEmpty() ? Model.Patient.TertiaryInsuranceName : ""%></span>
        </div>
        <h3>Allergies</h3>
        <div><%= data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "No" ? "NKA (Food/Drugs/Latex)" : "Allergic to:" + (data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : "") %></div>
        <h3>Current Episode</h3>
        <div>
            <span class="bicol">
                <span><strong>Primary Diagnosis:</strong><%= data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : ""%></span>
                <span><strong>Secondary Diagnosis:</strong><%= data.ContainsKey("M1022PrimaryDiagnosis1") ? data["M1022PrimaryDiagnosis1"].Answer : ""%></span>
                <span><strong>Primary Clinician:</strong><%= data.ContainsKey("") ? data[""].Answer : "" %></span>
                <span><strong>Primary Aide:</strong><%= data.ContainsKey("") ? data[""].Answer : "" %></span>
                <span><strong>Case Manager:</strong><%= Model.Patient.CaseManagerName %></span>
                <span><strong>Start of Care:</strong><%= Model.Patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? Model.Patient.StartOfCareDateFormatted : "" %></span>
            </span><span><span><strong>Frequencies:</strong><%= data.ContainsKey("") ? data[""].Answer : "" %></span></span>
            <span><span></span></span>
        </div>
        <h3>Emergency Contact</h3>
<% if (Model.Patient.EmergencyContacts.Count > 0) { %>
        <div class="tricol">
            <span><strong>Name:</strong></span>
            <span><strong>Phone:</strong></span>
            <span><strong>Relationship:</strong></span>
            <span><%= Model.Patient.EmergencyContacts[0].DisplayName.ToTitleCase() %></span>
            <span><%= Model.Patient.EmergencyContacts[0].PrimaryPhone.ToPhone()%></span>
            <span><%= Model.Patient.EmergencyContacts[0].Relationship.ToTitleCase() %></span>
        </div>
<% } else { %><div>None</div><% } %>
        <h3>Physician</h3>
        <div class="pentcol">
            <span><strong>Address:</strong></span>
            <span><strong>Phone:</strong></span>
            <span><strong>Facsimile:</strong></span>
            <span><strong>UPIN:</strong></span>
            <span><strong>State ID:</strong></span>
            <span><%= (Model.Physician != null && Model.Physician.AddressFull.IsNotNullOrEmpty()) ? Model.Physician.AddressFull : "" %></span>
            <span><%= (Model.Physician != null && Model.Physician.PhoneWorkFormatted.IsNotNullOrEmpty()) ? Model.Physician.PhoneWorkFormatted : ""%></span>
            <span><%= (Model.Physician != null && Model.Physician.FaxNumberFormatted.IsNotNullOrEmpty()) ? Model.Physician.FaxNumberFormatted : ""%></span>
            <span><%= (Model.Physician != null && Model.Physician.UPIN.IsNotNullOrEmpty()) ? Model.Physician.UPIN : ""%></span>
            <span><%= (Model.Physician != null && Model.Physician.LicenseNumber.IsNotNullOrEmpty()) ? Model.Physician.LicenseNumber : ""%></span>
        </div>
        <h3>Pharmacy</h3>
        <div class="bicol">
            <span><strong>Name:</strong><%= Model.Patient.PharmacyName.IsNotNullOrEmpty() ? Model.Patient.PharmacyName : "" %></span>
            <span><strong>Phone:</strong><%= Model.Patient.PharmacyPhone.IsNotNullOrEmpty() ? Model.Patient.PharmacyPhone.ToPhone() : ""%></span>
        </div>
        <h3>Comments</h3>
        <div class="deca"><%= data.ContainsKey("") ? data[""].Answer : "" %></div>
    </div>
<% } %>
</body>
</html>