﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="mailcontrols">
    <span class="float_left"><a href='javascript:void(0);' onclick="UserInterface.ShowMessages();">Inbox</a> &ndash; <a href='javascript:void(0);' onclick="UserInterface.ShowNewMessage();">Compose Mail</a></span>
    <select class="float_right">
        <option>Actions...</option>
        <option>Archive</option>
        <option>Delete</option>
        <option>Mark as Read</option>
        <option>Mark as Unread</option>
    </select>
    <div class="clear"></div>
</div>
<div style="position: relative; height: 181px;" id="messagesWidgetContent"></div>
<div id="messageWidgetMore" class="widget-more"><a onclick="UserInterface.ShowMessages();" href="javascript:void(0);">More &raquo;</a></div>

<% if (Current.HasRight(Permissions.Messaging)) { %>
<script type="text/javascript">
    Message.initWidget();
</script>
<% } %>