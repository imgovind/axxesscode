﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.ServiceModel.Syndication.SyndicationFeed>" %>
<div id="newsfeed_widget">
    <ul><%
int i = 0;
foreach (var item in ViewData.Model.Items) {
    if (i < 2) { %> 
        <li class="post">
            <a href="<%= item.Links[0].Uri.AbsoluteUri %>" target="_blank">
                <span class="title" ><%= item.Title.Text.ToTitleCase() %></span>&nbsp;-&nbsp;
                <span class="date"><%= item.PublishDate.ToString("d") %></span>
                <br />
                <span class="summary"><%= item.Summary.Text %></span>
            </a>
        </li><%
    }
    i++;
} %>
    </ul>
</div>
<div class="widget-more"><a target="_blank" href="http://acore.axxessconsult.com/blog">More &raquo;</a></div>