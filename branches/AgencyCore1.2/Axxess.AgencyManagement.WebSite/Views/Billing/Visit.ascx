﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<% using (Html.BeginForm("VisitVerify", "Billing", FormMethod.Post, new { @id = "billingVisit" })) { %>
<%= Html.Hidden("Id",Model.Id) %>
<%= Html.Hidden("episodeId",Model.EpisodeId) %>
<%= Html.Hidden("patientId",Model.PatientId) %><%
    var notVerifiedVisits = Model.NotVerifiedVisits.ToObject<List<ScheduleEvent>>().Where(s=>s.IsDeprecated==false && s.EventDate.ToDateTime()>=Model.EpisodeStartDate && s.EventDate.ToDateTime()<=Model.EpisodeEndDate);
    List<ScheduleEvent>[] a = new List<ScheduleEvent>[] { notVerifiedVisits.Where(v => v.IsBillable && (v.Status == "420" || v.Status == "425" || v.Status == "435" || v.Status == "215" || v.Status == "220" || v.Status == "225" || v.Status == "235") && !v.IsMissedVisit).ToList(), notVerifiedVisits.Where(v => !v.IsBillable && (v.Status == "420" || v.Status == "425" || v.Status == "435" || v.Status == "215" || v.Status == "220" || v.Status == "225" || v.Status == "235") && !v.IsMissedVisit).ToList(), notVerifiedVisits.Where(v => v.IsMissedVisit).ToList() };
    String[] b = new String[] {"Billable Visits","Non Billable Visits","Missed Visit"};
    String[] c = new String[] {"PT", "Nursing", "ST", "OT", "MSW"};
    String[] d = new String[] {"Physical Therapy (0421)", "Skilled Nursing (0551)", "Speech Therapy (0440)", "Occupational Therapy (0431)", "Social Worker (0561)"};
    String[] e = new String[] {"G0151", "G0154", "G0153", "G0152", "G0155"};
    %>
<div class="wrapper main"><%
    for (int g = 0; g < 3; g++) { %>
    <table class="claim">
        <thead>
            <tr><th colspan="6">Episode: <%= Model.EpisodeStartDate.ToShortDateString() %> &ndash; <%= Model.EpisodeEndDate.ToShortDateString() %></th></tr>
            <tr><th colspan="6"><%= b[g] %></th></tr>
            <tr><th>Visit Type</th><th>Date</th><th>HCPCS</th><th>Status</th><th>Units</th><th>Charge</th></tr>
        </thead><tbody><%
        for (int h = 0; h < 5; h++) {
            if (a[g] != null) {
                var visists = a[g].Where(f => f.Discipline == c[h]).ToList();
                if (visists != null && visists.Count > 0) { %>
            <tr><td colspan="6"><%= d[h] %></td></tr><% 
                    int i = 1;
                    foreach (var visit in visists) { %>
            <tr>
            <tr>
                <td><span class="alphali"><%= i %>.</span><input class="radio" name="Visit" type="checkbox" value='<%= visit.EventId %>' checked="checked" /><label class="inlineradio"><%= visit.DisciplineTaskName %></label></td>
                <td><%= visit.EventDate %></td>
                <td><%= e[h] %></td>
                <td><%= visit.StatusName %></td>
                <td></td>
                <td></td>
            </tr><%
                        i++;
                    }
                }
            }
        } %>
        </tbody>
    </table><%
    } %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Billing.NavigateBack(0);">Back</a></li>
            <li><a href="javascript:void(0);" onclick="Billing.Navigate(2,'#billingVisit');">Verify and Next</a></li>
        </ul>
    </div>
</div>
<% } %>