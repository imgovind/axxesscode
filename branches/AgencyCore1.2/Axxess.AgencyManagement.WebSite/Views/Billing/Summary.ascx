﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<div class="wrapper main">
    <fieldset><%= Html.Hidden("Id", Model.Id)%>
        <div class="column">
            <div class="row">
                <div class="float_left">Patient First Name:</div>
                <div class="float_right light"><%= Model.FirstName%></div>
            </div><div class="row">
                <div class="float_left">Patient Last Name:</div>
                <div class="float_right light"><%= Model.LastName%></div>
            </div><div class="row">
                <div class="float_left">Medicare #:</div>
                <div class="float_right light"><%= Model.MedicareNumber%></div>
            </div><div class="row">
                <div class="float_left">Patient Record #:</div>
                <div class="float_right light"><%= Model.PatientIdNumber%></div>
            </div><div class="row">
                <div class="float_left">Gender:</div>
                <div class="float_right light"><%= Model.Gender%></div>
            </div><div class="row">
                <div class="float_left">Date of Birth:</div>
                <div class="float_right light"><%= Model.DOB != null ? Model.DOB.ToShortDateString() : ""%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="float_left">Episode Start Date:</div>
                <div class="float_right light"><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToShortDateString() : ""%></div>
            </div><div class="row">
                <div class="float_left">Admission Date:</div>
                <div class="float_right light"><%= Model.StartofCareDate != null ? Model.StartofCareDate.ToShortDateString() : ""%></div>
            </div><div class="row">
                <div class="float_left">Address Line 1:</div>
                <div class="float_right light"><%= Model.AddressLine1%></div>
            </div><div class="row">
                <div class="float_left">Address Line 2:</div>
                <div class="float_right light"><%= Model.AddressLine2%></div>
            </div><div class="row">
                <div class="float_left">City:</div>
                <div class="float_right light"><%= Model.AddressCity%></div>
            </div><div class="row">
                <div class="float_left">State, Zip Code:</div>
                <div class="float_right light"><%= Model.AddressStateCode + Model.AddressZipCode%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <div class="float_left">HIPPS Code:</div>
                <div class="float_right light"><%= Model.HippsCode%></div>
            </div><div class="row">
                <div class="float_left">Oasis Matching Key:</div>
                <div class="float_right light"><%= Model.ClaimKey%></div>
            </div><div class="row">
                <div class="float_left">Date Of First Billable Visit:</div>
                <div class="float_right light"><%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToShortDateString() : ""%></div>
            </div><div class="row">
                <div class="float_left">Physician Last Name, F.I.:</div>
                <div class="float_right light"><%= Model.PhysicianLastName + Model.PhysicianFirstName != null && Model.PhysicianFirstName != "" ? Model.PhysicianFirstName.Substring(0, 1) : ""%></div>
            </div><div class="row">
                <div class="float_left">Physician NPI #:</div>
                <div class="float_right light"><%= Model.PhysicianNPI%></div>
            </div><div class="row">
                <div>Remark:</div>
                <div class="margin light"><p><%= Model.Remark%></p></div>
            </div>
        </div><div class="column">
            <div class="row"><% var diganosis = XElement.Parse(Model.DiagonasisCode); var val = (diganosis != null && diganosis.Element("code1") != null ? diganosis.Element("code1").Value : ""); %>
                <div>Diagonasis Codes:</div>
                <div class="margin">
                    <div class="float_left">Primary</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code1") != null ? Regex.Replace(diganosis.Element("code1").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                    <div class="float_left">Second</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code2") != null ? Regex.Replace(diganosis.Element("code2").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                    <div class="float_left">Third</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code3") != null ? Regex.Replace(diganosis.Element("code3").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                    <div class="float_left">Fourth</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code4") != null ? Regex.Replace(diganosis.Element("code4").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                    <div class="float_left">Fifth</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code5") != null ? Regex.Replace(diganosis.Element("code5").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                    <div class="float_left">Sixth</div>
                    <div class="float_right light"><%= diganosis != null && diganosis.Element("code6") != null ? Regex.Replace(diganosis.Element("code6").Value, @"[.]", "") : ""%></div><div class="clear"></div>
                </div>
            </div>
        </div>
    </fieldset>
    <table class="claim">
        <thead><tr><th></th><th>Description</th><th>HCPCS/HIIPS Code</th><th>Service Date</th><th>Service Unit</th><th>Total Charges</th></tr></thead>
        <tbody class="light">
            <tr class="even">
                <td>0023</td>
                <td>Home Health Services</td>
                <td><%= Model.HippsCode%></td>
                <td><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToShortDateString() : ""%></td>
                <td></td>
                <td></td>
            </tr><tr class="odd">
                <td>0272</td>
                <td>Service Supplies</td>
                <td></td>
                <td><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToShortDateString() : ""%></td>
                <td></td>
                <td></td>
            </tr><%
    var visits = Model.VerifiedVisits.ToObject<List<ScheduleEvent>>().OrderBy(s => s.EventDate).ToList();
    var count = 0;
    foreach (var visit in visits)
    { %>
            <tr class='<%= count % 2 == 0 ? "even" : "odd" %>'>
                <td><%
    switch (visit.Discipline)
    {
        case "PT": Response.Write("0421"); break;
        case "OT": Response.Write("0431"); break;
        case "ST": Response.Write("0440"); break;
        case "Nursing": Response.Write("0551"); break;
        case "MSW": Response.Write("0561"); break;
        case "HHA": Response.Write("0571"); break;
    }       %></td>
                <td>Visit</td>
                <td><%
    switch (visit.Discipline)
    {
        case "PT": Response.Write("G0151"); break;
        case "OT": Response.Write("G0152"); break;
        case "ST": Response.Write("G0153"); break;
        case "Nursing": Response.Write("G0154"); break;
        case "MSW": Response.Write("G0155"); break;
        case "HHA": Response.Write("G0156"); break;
    }       %></td>
                <td><%= visit.EventDate%></td>
                <td></td>
                <td></td>
            </tr><%
    count++;
    } %>
        </tbody>
    </table>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Billing.NavigateBack(2);">Back</a></li>
            <li><a href="javascript:void(0);" onclick="Billing.Navigate(2,'#billingInfo');">Verify and Next</a></li>
        </ul>
    </div>
</div>