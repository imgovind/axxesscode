﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyHospital>" %>
<%  using (Html.BeginForm("Update", "Hospital", FormMethod.Post, new { @id = "editHospitalForm" })) { %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Edit Hospital | ",
        Model != null ? Model.Name.ToTitleCase() : "",
        "','edithospital');</script>")%>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Hospital_Id" })%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Hospital Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Hospital_Name" class="float_left">Hospital Name:</label><div class="float_right"> <%=Html.TextBox("Name", Model.Name, new { @id = "Edit_Hospital_Name", @class = "text input_wrapper required", @maxlength = "20" })%></div></div>
            <div class="row"><label for="Edit_Hospital_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"> <%=Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Hospital_AddressLine1", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Hospital_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"> <%=Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Hospital_AddressLine2", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Hospital_AddressCity" class="float_left">City:</label><div class="float_right"> <%=Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Hospital_AddressCity", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="Edit_Hospital_AddressStateCode" class="float_left"> State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Hospital_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Hospital_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div></div>
         </div>   
        <div class="column">   
            <div class="row"><label for="Edit_Hospital_ContactPersonFirstName" class="float_left">Contact First Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonFirstName", Model.ContactPersonFirstName, new { @id = "Edit_Hospital_ContactPersonFirstName", @class = "text input_wrapper", @maxlength = "40", @tabindex = "49" })%></div></div>
            <div class="row"><label for="Edit_Hospital_ContactPersonLastName" class="float_left">Contact Last Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonLastName", Model.ContactPersonLastName, new { @id = "Edit_Hospital_ContactPersonLastName", @class = "text input_wrapper", @maxlength = "40", @tabindex = "49" })%></div></div>
            <div class="row"><label for="Edit_Hospital_EmailAddress" class="float_left">Email :</label><div class="float_right"><%=Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "Edit_Hospital_EmailAddress", @class = "text email input_wrapper", @maxlength = "50", @tabindex = "49" })%></div></div>
            <div class="row"><label for="Edit_Hospital_PhoneArray1" class="float_left">Primary Phone:</label><div class="float_right"> <%=Html.TextBox("PhoneArray", Model.Phone.IsNotNullOrEmpty() ? Model.Phone.Substring(0, 3) : "", new { @id = "Edit_Hospital_PhoneArray1", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "4" })%> - <%=Html.TextBox("PhoneArray", Model.Phone.IsNotNullOrEmpty() ? Model.Phone.Substring(3, 3) : "", new { @id = "Edit_Hospital_PhoneArray2", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "3" })%> - <%=Html.TextBox("PhoneArray", Model.Phone.IsNotNullOrEmpty() ? Model.Phone.Substring(6, 4) : "", new { @id = "Edit_Hospital_PhoneArray3", @class = "input_wrappermultible autotext required digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Hospital_FaxNumberArray1" class="float_left">Fax Number:</label><div class="float_right"> <%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Hospital_FaxNumberArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%> - <%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Hospital_FaxNumberArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%> - <%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Hospital_FaxNumberArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div> 
        </div>
        <table class="form"><tbody>           
             <tr class="linesep vert">
               <td><label for="Edit_Hospital_Comments">Comment:</label><div><%= Html.TextArea("Comment", Model.Comment, new { @id = "Edit_Hospital_Comments" })%></div></td>
            </tr>
        </tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('edithospital');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>
<%} %>

