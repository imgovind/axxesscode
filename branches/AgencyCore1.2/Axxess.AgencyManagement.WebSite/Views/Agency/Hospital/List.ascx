﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Hospitals", "Export", FormMethod.Post)) { %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('List Hospitals | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','listhospitals');</script>")%>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyHospital>().Name("List_AgencyHospital").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(c => c.Name).Title("Hospital Name").Width(150).Sortable(false);
    columns.Bound(c => c.ContactPerson).Title("Contact Person").Sortable(false);
    columns.Bound(c => c.AddressFull).Title("Address").Sortable(false);
    columns.Bound(c => c.PhoneFormatted).Title("Phone").Width(120);
    columns.Bound(c => c.FaxFormatted).Title("Fax Number").Width(120);
    columns.Bound(c => c.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(150).Sortable(false);
    columns.Bound(c => c.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditHospital('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Hospital.Delete('<#=Id#>');\" class=\"deleteContact\">Delete</a>").Title("Action").Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Hospital")).Pageable(paging => paging.PageSize(10)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
    $("#List_AgencyHospital .t-grid-toolbar").html("");
<% if (Current.HasRight(Permissions.ManageHospital)) { %>
    $("#List_AgencyHospital .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewHospital(); return false;%22%3ENew Hospital%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
<% } %>
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    $("#List_AgencyHospital .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExcel Export%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
<% } %>

    $(".t-grid-content").css({ 'height': 'auto' });
</script>
