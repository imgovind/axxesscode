﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Orders Management Center | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','ordersmanagement');function renamebutton(){$('#List_OrdersPendingSignature .t-grid-edit').html('Receive Order');}</script>")%>
<div class="fixedGridHeight">
<%= Html.Telerik().Grid<Order>().Name("List_OrdersToBeSent").ToolBar(commnds => commnds.Custom()).Columns(columns => {
            columns.Bound(o => o.Type).Title(" ").ClientTemplate("<input type='checkbox' class='OrdersToBeSent' name='<#=Type#>' value='<#=Id#>' />").Width(35);
            columns.Bound(o => o.Number).Title("Order").Width(80).Sortable(false);
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(false);
            columns.Bound(o => o.Text).Title("Type").Width(180).Sortable(false);
            columns.Bound(o => o.PhysicianName).Title("Physician").Sortable(false);
            columns.Bound(o => o.CreatedDate).Title("Order Date").Width(100).Sortable(false);
            columns.Bound(o => o.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersToBeSent", "Agency")).Pageable(paging => paging.PageSize(15)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
%>
</div><div class="absoluteGrid">
<%= Html.Telerik().Grid<Order>().Name("List_OrdersPendingSignature").ToolBar(commnds => commnds.Custom()).DataKeys(keys => {
             keys.Add(o => o.Id).RouteKey("id");
             keys.Add(o => o.Type).RouteKey("type");
             keys.Add(o => o.ReceivedDate).RouteKey("receivedDate");
         }).Columns(columns => {
            columns.Bound(o => o.Number).Title("Order").Width(70).Sortable(false).ReadOnly();
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(false).ReadOnly();
            columns.Bound(o => o.Text).Title("Type").Width(180).Sortable(false).ReadOnly();
            columns.Bound(o => o.PhysicianName).Title("Physician").Sortable(false).ReadOnly();
            columns.Bound(o => o.CreatedDate).Title("Order Date").Width(100).Sortable(false).ReadOnly();
            columns.Bound(o => o.SentDate).Title("Sent Date").Width(100).Sortable(false).ReadOnly();
            columns.Bound(o => o.ReceivedDate).Title("Received Date").Width(120).Sortable(false);
            columns.Command(commands => { commands.Edit(); }).Width(160).Title("Action");
        }).DataBinding(dataBinding => dataBinding.Ajax()
            .Select("OrdersPendingSignature", "Agency")
            .Update("MarkOrderAsReturned", "Agency")
        ).Editable(editing => editing.Mode(GridEditMode.InLine)).Pageable(paging => paging.PageSize(20)).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).ClientEvents(events => events.OnRowDataBound("renamebutton"))
%>
</div>
<script type="text/javascript">
    $("#List_OrdersToBeSent .t-grid-toolbar").html("");
    $("#List_OrdersToBeSent .t-grid-toolbar").append(unescape("%3Cdiv class=%22align_center%22%3E%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3EOrders To Be Sent%3C/div%3E%3Cdiv class=%22float_right buttons%22%3E%3Cul class=%22float_right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22Agency.MarkOrdersAsSent();%22%3EMark As Sent To Physician%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E%3C/div%3E"));
    $("#List_OrdersPendingSignature .t-grid-toolbar").html("");
    $("#List_OrdersPendingSignature .t-grid-toolbar").append(unescape("%3Cdiv class=%22align_center%22%3E%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3EOrders Pending Physician Signature%3C/div%3E%3C/div%3E"));
    $("#List_OrdersToBeSent .t-grid-content").css({ 'height': '200px' });
    $("#List_OrdersPendingSignature .t-grid-content").css({ 'height': 'auto' });
    $("#window_ordersmanagement .t-grid-content").css({ 'top': '60px' });
</script>