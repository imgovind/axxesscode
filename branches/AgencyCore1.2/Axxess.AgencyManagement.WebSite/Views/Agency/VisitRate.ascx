﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Edit Visit Rates | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','visitrates');</script>")%>
<% using (Html.BeginForm("EditCost", "Agency", FormMethod.Post, new { @id = "editVisitCostForm" })){ %>
<fieldset>
 <div class="wide_column"><label for="Edit_VisitRate_LocationId" class="float_left">Agency Branch:</label><div class="float_left"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.Id.ToString(), new { @id = "Edit_VisitRate_LocationId", @class = "BranchLocation required" })%></div></div>
 <div id="Edit_VisitRate_Container" class="wide_column" style="margin:20px 0px 20px 0px;">
        <% Html.RenderPartial("~/Views/Agency/VisitRateContent.ascx", Model); %>
 </div>
 <label class="error" > Error</label>
 <div class="buttons"><ul>
                            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
                            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('visitrates');">Close</a></li>
                        </ul></div>
</fieldset>
<%} %>
<script type="text/javascript">
    $('#Edit_VisitRate_LocationId').change(function() {
        Agency.loadVisitRateContent($(this).val());
    });
    $("fieldset").css({'position':'absolute','bottom':'0px','top':'5px','right':'5px','left':'5px'});
</script>