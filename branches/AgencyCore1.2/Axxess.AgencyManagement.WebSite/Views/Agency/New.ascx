﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Add", "Agency", FormMethod.Post, new { @id = "newAgencyForm" }))%>
<%  { %>
<div class="form_wrapper">
    <fieldset>
        <legend>Admin Information</legend>
        <div class="column">
            <div class="row"><label for="New_Agency_AdminUsername">E-mail:</label><div class="float_right"><%=Html.TextBox("AgencyAdminUsername", "", new { @id = "New_Agency_AdminUsername", @class = "text required", @maxlength = "40" })%></div></div>
            <div class="row"><label for="New_Agency_AdminPassword">Password:</label><div class="float_right"><%=Html.TextBox("AgencyAdminPassword", "", new { @id = "New_Agency_AdminPassword", @class = "text required password", @maxlength = "20" })%><div class="buttons float_right"><ul><li><a href="javascript:void(0);" id="New_Agency_GeneratePassword">Generate</a></li></ul></div></div></div>
        </div><div class="column">
            <div class="row"><label for="New_Agency_AdminFirstName">First Name:</label><div class="float_right"><%=Html.TextBox("AgencyAdminFirstName", "", new { @id = "New_Agency_AdminFirstName", @class = "text required names input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="New_Agency_AdminLastName">Last Name:</label><div class="float_right"><%=Html.TextBox("AgencyAdminLastName", "", new { @id = "New_Agency_AdminLastName", @class = "text required names input_wrapper", @maxlength = "20" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Company Information</legend>
        <div class="column">
            <div class="row"><label for="New_Agency_CompanyName">Company Name:</label><div class="float_right"><%=Html.TextBox("Name", "", new { @id = "New_Agency_CompanyName", @maxlength = "50", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_TaxId">Tax Id:</label><div class="float_right"><%=Html.TextBox("TaxId", "", new { @id = "New_Agency_TaxId", @maxlength = "10", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_TaxIdType">Tax Id Type:</label><div class="float_right"><select id="New_Agency_TaxIdType" name="TaxIdType"><option value="0">Choose Tax ID Type</option><option value="1">EIN (Employer Identification Number)</option><option value="2">SSN (Social Security Number)</option></select></div></div>
            <div class="row"><label for="New_Agency_Payor">Payor:</label><div class="float_right"><select id="New_Agency_Payor" name="Payor"><option value="0">Choose Payor</option><option value="1">Palmetto GBA</option><option value="2">National Government Services (formerly known as United Government Services)</option><option value="3">Blue Cross Blue Shield of Alabama (AKA Chaba GBA)</option><option value="4">Anthem Health Plans of Maine</option></select></div></div>
            <div class="row">&nbsp;</div>
            <div class="row"><div class="float_right"><input type="checkbox" class="radio" name="New_Agency_SameAsAdmin" id="New_Agency_SameAsAdmin" />&nbsp;Check here if the Admin is same as the Contact Person</div></div>
            <div class="row"><label for="New_Agency_ContactPersonEmail">Contact Person E-mail:</label><div class="float_right"><%=Html.TextBox("ContactPersonEmail", "", new { @id = "New_Agency_ContactPersonEmail", @class = "text required input_wrapper", @maxlength = "40" })%></div></div>
            <div class="row"><label for="ContactPhoneArray">Contact Person Phone:</label><div class="float_right"><input type="text" class="autotext required numeric phone_short" name="ContactPhoneArray" id="New_Agency_ContactPhone1" maxlength="3" /> - <input type="text" class="autotext required numeric phone_short" name="ContactPhoneArray" id="New_Agency_ContactPhone2" maxlength="3" /> - <input type="text" class="autotext required numeric phone_long" name="ContactPhoneArray" id="New_Agency_ContactPhone3" maxlength="4"/></div></div>
        </div><div class="column">
            <div class="row"><label for="New_Agency_NationalProviderNo">National Provider Number:</label><div class="float_right"><%=Html.TextBox("NationalProviderNumber", " ", new { @id = "New_Agency_NationalProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_MedicareProviderNo">Medicare Provider Number:</label><div class="float_right"><%=Html.TextBox("MedicareProviderNumber", " ", new { @id = "New_Agency_MedicareProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_MedicaidProviderNo">Medicaid Provider Number:</label><div class="float_right"><%=Html.TextBox("MedicaidProviderNumber", " ", new { @id = "New_Agency_MedicaidProviderNo", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_HomeHealthAgencyId">Unique HHA Agency ID Code:</label><div class="float_right"><%=Html.TextBox("HomeHealthAgencyId", " ", new { @id = "New_Agency_HomeHealthAgencyId", @maxlength = "10", @class = "text input_wrapper" })%></div></div>
            <div class="row">&nbsp;</div><div class="row">&nbsp;</div>
            <div class="row"><label for="New_Agency_ContactPersonFirstName">Contact Person First Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonFirstName", "", new { @id = "New_Agency_ContactPersonFirstName", @maxlength = "50", @class = "text required names input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_ContactPersonLastName">Contact Person Last Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonLastName", "", new { @id = "New_Agency_ContactPersonLastName", @class = "text required names input_wrapper", @maxlength = "20" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Location Information</legend>
        <div class="column">
            <div class="row"><label for="New_Agency_LocationName">Location Name: &nbsp;(e.g. Headquarters)</label><div class="float_right"><%=Html.TextBox("LocationName", "", new { @id = "New_Agency_LocationName", @maxlength = "50", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_Phone1">Phone Number:</label><div class="float_right"><input type="text" class="autotext numeric required phone_short" name="PhoneArray" id="New_Agency_Phone1" maxlength="3" /> - <input type="text" class="autotext numeric required phone_short" name="PhoneArray" id="New_Agency_Phone2" maxlength="3" /> - <input type="text" class="autotext numeric required phone_long" name="PhoneArray" id="New_Agency_Phone3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Agency_Fax1">Fax Number:</label><div class="float_right"><input type="text" class="autotext numeric phone_short" name="FaxArray" id="New_Agency_Fax1" maxlength="3" /> - <input type="text" class="autotext numeric phone_short" name="FaxArray" id="New_Agency_Fax2" maxlength="3" /> - <input type="text" class="autotext numeric phone_long" name="FaxArray" id="New_Agency_Fax3" maxlength="4" /></div></div>
        </div><div class="column">
            <div class="row"><label for="New_Agency_AddressLine1">Address Line 1:</label><div class="float_right"><%=Html.TextBox("AddressLine1", "", new { @id = "New_Agency_AddressLine1", @maxlength = "20", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_AddressLine2">Address Line 2:</label><div class="float_right"><%=Html.TextBox("AddressLine2", "", new { @id = "New_Agency_AddressLine2", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_AddressCity">City:</label><div class="float_right"><%=Html.TextBox("AddressCity", "", new { @id = "New_Agency_AddressCity", @maxlength = "20", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Agency_AddressStateCode">State, Zip Code:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Agency_AddressStateCode", @class = "AddressStateCode required valid" }) %><%=Html.TextBox("AddressZipCode", "", new { @id = "New_Agency_AddressZipCode", @class = "text numeric required zip", @maxlength = "5" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newagency');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
