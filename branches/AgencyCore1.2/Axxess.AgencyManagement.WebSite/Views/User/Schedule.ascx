﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('",
        Axxess.AgencyManagement.App.Current.User.DisplayName,
        "&rsquo;s Schedule and Tasks','listuserschedule');</script>")%>
<div class="wrapper">
    <%= Html.Telerik().Grid<UserVisit>()
        .Name("List_User_Schedule")
        .Columns(columns =>
        {
            columns.Bound(v => v.PatientName);
            columns.Bound(v => v.TaskName).ClientTemplate("<#=Url#>").Title("Task").Width(250);
            columns.Bound(v => v.VisitDate).Title("Date").Width(100).Visible(true);
            columns.Bound(v => v.StatusName).Width(200).Sortable(false).Title("Status");
            columns.Bound(v => v.VisitNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=VisitNotes#>\"></a>");
            columns.Bound(v => v.EpisodeNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip blue_note\" href=\"javascript:void(0);\" tooltip=\"<#=EpisodeNotes#>\"></a>");
            columns.Bound(v => v.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowMissedVisitModal('<#=EpisodeId#>','<#=PatientId#>','<#=Id#>');\">Missed Visit Form</a>").Title(" ").Width(200);
        })
        .ClientEvents(c => c.OnRowDataBound("Schedule.tooltip"))
        .DataBinding(dataBinding => dataBinding.Ajax()
        .Select("ScheduleList", "User"))
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $(".t-group-indicator").hide();
    $(".t-grid-content").css({ 'height': 'auto','bottom':'25px' });
</script>