﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('List Referrals | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','listreferrals');</script>")%>
<% using (Html.BeginForm("Referrals", "Export", FormMethod.Post)) { %>
<% var action = ""; var visible = false; %>
<% if (Current.HasRight(Permissions.ManageReferrals)) {
       visible = true;
       action = "<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditReferral('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Referral.Delete('<#=Id#>');\" class=\"deleteReferral\">Delete</a> | <a href=\"javascript:void(0);\" onclick=\"Referral.Admit('<#=Id#>');\">Admit</a>";
   } %>
<div class="wrapper">
    <%= Html.Telerik().Grid<Referral>().Name("List_Referral").ToolBar(commnds => commnds.Custom()).Columns(columns => {
            columns.Bound(r => r.Created).Format("{0:MM/dd/yyyy}").Title("Referral Date").Width(100);
            columns.Bound(r => r.DisplayName).Title("Name").Width(180);
            columns.Bound(r => r.ReferralSourceName).Title("Referral Source");
            columns.Bound(r => r.DOBFormatted).Title("Date of Birth").Width(100);
            columns.Bound(r => r.Gender).Title("Gender").Width(80);
            columns.Bound(r => r.StatusName).Title("Status").Width(80).Sortable(false);
            columns.Bound(r => r.Id).ClientTemplate(action).Title("Action").Width(180).Visible(visible);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "Referral")).ClientEvents(events => events.OnDataBound("Referral.BindGridButton")).Pageable(paging => paging.PageSize(10)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
    $("#List_Referral .t-grid-toolbar").html("");
<% if (Current.HasRight(Permissions.ManageReferrals)) { %>
    $("#List_Referral .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewReferral(); return false;%22%3ENew Referral%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
<% } %>
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    $("#List_Referral .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExcel Export%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
<% } %>
    $(".t-grid-content").css({ 'height': 'auto' });
</script>