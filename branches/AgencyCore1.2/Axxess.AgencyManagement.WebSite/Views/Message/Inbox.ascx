﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Message>>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('",
        Axxess.AgencyManagement.App.Current.User.DisplayName,
        "&rsquo;s Messages','messageinbox');</script>")%>
<div id="messagingContainer" class="wrapper layout">
    <div class="layout_left">
        <% Html.RenderPartial("~/Views/Message/Grid.ascx", Model); %>
    </div>
    <div id="scheduleMainResult" class="layout_main"><%
        if (Model != null && Model.Count<Message>() > 0) Html.RenderPartial("~/Views/Message/Info.ascx", Model.First());
        else { %><div class="abs center">No Messages Found</div><% } %>
    </div>
</div>
<script type="text/javascript">
    $('#window_messageinbox .layout').layout({ west: { paneSelector: '.layout_left', size: 275 } });
</script>

