﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('New Message | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','newmessage');</script>")%>
<% using (Html.BeginForm("New", "Message", FormMethod.Post, new { @id = "newMessageForm" })) { %>
<div id="messagingContainer" class="wrapper layout">
    <div class="layout_left">
        <%= Html.Recipients() %>
    </div>
    <div class="newMessageContent layout_main" id="newMessageContent">
        <div class="inboxSubHeader"><span>New Message</span>
            <div class="buttons float_right"><ul><li><a href="javascript:void(0);" onclick="Message.Cancel();">Cancel</a></li></ul></div>
        </div>
        <div class="buttons float_left"><ul><li class="send"><a href="javascript:void(0);" onclick="$(this).closest('form').submit();"><img src="/Images/forms/send.png" alt="" /><br />Send</a></li></ul></div>
        <div class="newMessageContentPanel clear">
            <div class="newMessageRow"><label for="New_Message_Recipents">To:</label><input type="text" id="New_Message_Recipents" name="newMessageRecipents" /></div>
            <div class="newMessageRow"><label for="New_Message_Subject">Subject:</label><input type="text" id="New_Message_Subject" name="Subject" /></div>
            <div class="newMessageRow"><label for="New_Message_PatientId">Regarding:</label><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "New_Message_PatientId", @class = "", @tabindex = "3" })%></div>                
        </div>
        <div class="newMessageRow" id="newMessageBodyDiv">
            <div>
                <textarea id="New_Message_Body" name="Body"></textarea>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    if ($('#window_messageinbox .layout').length) $('#window_messageinbox .layout').layout({ west: { paneSelector: '.layout_left', size: 275} });
    if ($('#window_newmessage .layout').length) $('#window_newmessage .layout').layout({ west: { paneSelector: '.layout_left', size: 275} });
</script>
<% } %>