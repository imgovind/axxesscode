﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">
acore.init();
acore.addmenu("Home", "home", "mainmenu", "home.png");
acore.addmenu("My Account", "account", "home");
<% if (Current.HasRight(Permissions.AccessPersonalProfile)) { %>acore.addwindow("editprofile", "Edit Profile", "User/Profile", function() { User.InitProfile(); }, "account");<% } %>
acore.addwindow("forgotsignature", "Reset Signature", "Signature/Forgot", function() { User.InitForgotSignature(); }, "account");
<% if (Current.HasRight(Permissions.Dashboard)) { %>acore.addwindow("homepage", "Dashboard", "Home/Dashboard", function() { iNettuts.init(); Home.LoadWidgets(); }, "home", "My Dashboard");<% } %>
<% if (Current.HasRight(Permissions.Messaging)) { %>acore.addwindow("messageinbox", "My Messages", "Message/Inbox", function() { Message.Init(); }, "home", "My Messages");<% } %>
acore.addwindow("listuserschedule", "My Schedule/Tasks", "User/Schedule", function() { }, "home");
<% if (Current.HasRight(Permissions.AccessCaseManagement)) { %>acore.addwindow("caseManagement", "Case Management", "Agency/CaseManagement", function() { }, "home");<% } %>

    
acore.addmenu("Create", "create", "mainmenu", "edit.png");
acore.addmenu("New", "createnew", "create");
<% if (Current.HasRight(Permissions.ManagePatients)) { %>acore.addwindow("newpatient", "Patient", "Patient/New", function() { Patient.InitNew(); }, "createnew");<% } %>
<% if (Current.HasRight(Permissions.ManageReferrals)) { %>acore.addwindow("newreferral", "Referral", "Referral/New", function() { Referral.InitNew(); }, "createnew");<% } %>
<% if (Current.HasRight(Permissions.Messaging)) { %>acore.addwindow("newmessage", "Message", "Message/New", function() { Message.InitNew(); }, "createnew");<% } %>
<% if (Current.HasRight(Permissions.ManagePatients)) { %>acore.addwindow("newcommnote", "Communication Note", "CommunicationNote/New", function() { Patient.InitNewCommunicationNote(); }, "createnew");<% } %>
<% if (Current.HasRight(Permissions.AccessOrderManagementCenter)) { %>acore.addwindow("neworder", "Order", "Order/New", function() { Patient.InitNewOrder(); }, "createnew");<% } %>
<% if (Current.HasRight(Permissions.ManageHospital)) { %>acore.addwindow("newhospital", "Hospital", "Hospital/New", function() { Hospital.InitNew(); }, "createnew");<% } %>
<% if (Current.HasRight(Permissions.ManageInsurance)) { %>acore.addwindow("newinsurance", "Insurance/Payor", "Insurance/New", function() { Insurance.InitNew(); }, "createnew");<% } %>
<% if (Current.HasRight(Permissions.ManagePhysicians)) { %>acore.addwindow("newphysician", "Physician", "Physician/New", function() { Physician.InitNew(); }, "createnew");<% } %>
<% if (Current.HasRight(Permissions.ManageContact)) { %>acore.addwindow("newcontact", "Contact", "Contact/New", function() { Contact.InitNew(); }, "createnew");<% } %>
<% if (Current.HasRight(Permissions.ViewIncidentAccidentInfectionReport)) { %>acore.addwindow("newinfectionreport", "Infection Report", "Agency/NewInfectionReport", function() { }, "createnew");<% } %>
<% if (Current.HasRight(Permissions.ViewIncidentAccidentInfectionReport)) { %>acore.addwindow("newincidentreport", "Incident/Accident Report", "Agency/NewIncidentReport", function() { }, "createnew");<% } %>
<% if (Current.HasRight(Permissions.CreateOasisSubmitFile)) { %>acore.addwindow("oasisExport", "Oasis Export", "Oasis/ExportView", function() { }, "create");<% } %>


acore.addmenu("View", "view", "mainmenu", "view.png");
<% if (Current.HasRight(Permissions.ViewLists)) { %>
acore.addmenu("Lists", "viewlist", "view");
acore.addwindow("listpatients", "Patients", "Patient/Grid", function() { }, "viewlist");
acore.addwindow("listreferrals", "Referrals", "Referral/List", function() { }, "viewlist");
acore.addwindow("listcontacts", "Contacts", "Contact/Grid", function() { }, "viewlist");
acore.addwindow("listhospitals", "Hospitals", "Hospital/Grid", function() { }, "viewlist");
acore.addwindow("listinsurances", "Insurances / Payors", "Insurance/Grid", function() { }, "viewlist");
acore.addwindow("listlocations", "Locations", "Location/Grid", function() { }, "viewlist");
acore.addwindow("listphysicians", "Physicians", "Physician/Grid", function() { }, "viewlist");
acore.addwindow("listusers", "Users", "User/Grid", function() { }, "viewlist");
<% } %>
//acore.addwindow("blankforms", "Blank Forms", "Agency/Blankforms", function() { }, "view");
<% if (Current.HasRight(Permissions.ViewExportedOasis)) { %>acore.addwindow("oasisExported", "Exported Oasis", "Oasis/ExportedView", function() { }, "view");<% } %>
<% if (Current.HasRight(Permissions.ViewMedicationProfile)) { %>
acore.addwindow("medicationprofilesnapshot", "Medication Profile SnapShot", "Patient/MedicationProfileSnapShot", function() { Patient.InitMedicationProfile(); }, false);
acore.addwindow("medicationprofile", "Medication Profile", "Patient/MedicationHistoryView", function() { Patient.InitMedicationProfile(); }, false);
acore.addwindow("medicationprofilesnapshothistory", "Medication Profile Snapshot History", "Patient/MedicationHistorySnapshotView", function() { Patient.InitMedicationProfileSnapshot(); }, false);
<% } %>
<% if (Current.HasRight(Permissions.ViewScheduledTasks)) { %>
acore.addwindow("listpastduerecerts", "Past Due Recerts", "Agency/RecertsPastDueGrid", function() { }, "view");
acore.addwindow("listupcomingrecerts", "Upcoming Recerts", "Agency/RecertsUpcomingGrid", function() { }, "view");
<% } %>
<% if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %>
acore.addwindow("ordersmanagement", "Orders Management Center", "Agency/OrdersManagement", function() { }, "view");
<% } %>


acore.addmenu("Patients", "patients", "mainmenu", "patients.png");
<% if (Current.HasRight(Permissions.AccessPatientCenter)) { %>acore.addwindow("patientcenter", "Patient Center", "Patient/Center", function() { Patient.InitCenter(); }, "patients");<% } %>
<% if (Current.HasRight(Permissions.ViewExisitingReferrals)) { %>acore.addwindow("listreferrals", "Existing Referrals", "Referral/List", function() { }, "patients");<% } %>
<% if (Current.HasRight(Permissions.ViewLists)) { %>
acore.addwindow("listpendingpatients", "Pending Admissions", "Patient/PendingGrid", function() { }, "patients");
<% } %>

acore.addmenu("Schedule", "schedule", "mainmenu", "schedule.png");
<% if (Current.HasRight(Permissions.AccessScheduleCenter)) { %>acore.addwindow("schedulecenter", "Schedule Center", "Schedule/Center", function() { Schedule.InitCenter(); }, "schedule");<% } %>
<% if (Current.HasRight(Permissions.ScheduleVisits)) { %>
acore.addwindow("woundcare", "Wound Care Flowsheet", "Schedule/WoundCare", function() {Schedule.WoundCareInit(); }, false);
acore.addwindow("snVisit", "SN Visit", "Schedule/SNVisit", function() { Schedule.InitCenter(); }, false);
acore.addwindow("startofcare", "OASIS Start of Care", "Oasis/Soc", function() { SOC.InitNew(); Oasis.Init(); }, false);
acore.addwindow("recertification", "OASIS-C Recertification", "Oasis/Recertification", function() { Recertification.InitNew(); Oasis.Init(); }, false);
acore.addwindow("resumptionofcare", "OASIS Resumption Of Care", "Oasis/Roc", function() { ROC.InitNew(); Oasis.Init(); }, false);
acore.addwindow("followup", "OASIS Follow Up", "Oasis/FollowUp", function() { FollowUp.InitNew(); }, false);
acore.addwindow("deathathome", "OASIS Death At Home", "Oasis/Death", function() { DeathAtHome.InitNew(); }, false);
acore.addwindow("discharge", "OASIS Discharge From Agency", "Oasis/Discharge", function() { Discharge.InitNew(); Oasis.Init(); }, false);
acore.addwindow("transferfordischarge", "OASIS Transfer For Discharge", "Oasis/TransferForDischarge", function() { TransferForDischarge.InitNew(); }, false);
acore.addwindow("transfernotdischarge", "OASIS Transfer Not Discharge", "Oasis/TransferNotDischarge", function() { TransferNotDischarge.InitNew(); }, false);
acore.addwindow("lvnsVisit", "LVN Supervisory Visit", "Schedule/LVNSVisit", function() { Schedule.lvnInit(); }, false);
acore.addwindow("hhasVisit", "HHA Supervisory Visit", "Schedule/HHASVisit", function() { Schedule.hhaInit(); }, false);
acore.addwindow("hhAideVisit", "HHA Visit Note", "Schedule/HHAVisitNote", function() { Schedule.hhaVisitNoteInit(); }, false);
acore.addwindow("dischargeSummary", "Discharge Summary", "Schedule/DischargeSummary", function() { Schedule.dischargeSummaryInit(); }, false);
acore.addwindow("hhAideVisit", "Home Health Aide Progress Note", "Schedule/HHAVisitNote", function() { Schedule.hhaVisitNoteInit(); }, false);
acore.addwindow("hhaCarePlan", "Home Health Aide Care Plan", "Schedule/HHACarePlan", function() { Schedule.hhaCarePlanInit(); }, false);
acore.addwindow("sixtyDaySummary", "60 Day Summary/CASE CONFERENCE", "Schedule/SixtyDaySummary", function() { Schedule.sixtyDaySummaryInit(); }, false);
acore.addwindow("transferSummary", "Coordination of Care/Transfer Summary", "Schedule/TransferSummary", function() { Schedule.transferSummaryInit(); }, false);
<% } %>

acore.addmenu("Billing", "billing", "mainmenu", "billing.png");
<% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>acore.addwindow("billingcenter", "Billing Center", "Billing/Center", function() { Billing.InitCenter(); }, "billing");<% } %>
<% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>acore.addwindow("claimSummary", "Claim Summary", "Billing/ClaimSummary", function() {  }, false);<% } %>

acore.addmenu("Admin", "admin", "mainmenu", "admin.png");
acore.addmenu("New", "adminadd", "admin");
<% if (Current.HasRight(Permissions.ManagePatients)) { %>acore.addwindow("newpatient", "Patient", "Patient/New", function() { Patient.InitNew(); }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.ManageReferrals)) { %>acore.addwindow("newreferral", "Referral", "Referral/New", function() { Referral.InitNew(); }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.AccessCaseManagement)) { %>acore.addwindow("newmessage", "Message", "Message/New", function() { Message.InitNew(); }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.AccessOrderManagementCenter)) { %>acore.addwindow("neworder", "Order", "Order/New", function() { Patient.InitNewOrder(); }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.ManagePatients)) { %>acore.addwindow("newcommnote", "Communication Note", "CommunicationNote/New", function() { Patient.InitNewCommunicationNote(); }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.ManageHospital)) { %>acore.addwindow("newhospital", "Hospital", "Hospital/New", function() { Hospital.InitNew(); }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.ManageInsurance)) { %>acore.addwindow("newinsurance", "Insurance/Payor", "Insurance/New", function() { Insurance.InitNew(); }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.ManagePhysicians)) { %>acore.addwindow("newphysician", "Physician", "Physician/New", function() { Physician.InitNew(); }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.ManageContact)) { %>acore.addwindow("newcontact", "Contact", "Contact/New", function() { Contact.InitNew(); }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.ManageUsers)) { %>acore.addwindow("newuser", "User", "User/New", function() { User.InitNew(); }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.ViewIncidentAccidentInfectionReport)) { %>acore.addwindow("newinfectionreport", "Infection Report", "Agency/NewInfectionReport", function() {  }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.ViewIncidentAccidentInfectionReport)) { %>acore.addwindow("newincidentreport", "Incident/Accident Report", "Agency/NewIncidentReport", function() {  }, "adminadd");<% } %>
<% if (Current.HasRight(Permissions.ViewLists)) { %>
acore.addmenu("Lists", "adminlist", "admin");
acore.addwindow("listpatients", "Patients", "Patient/Grid", function() { }, "adminlist");
acore.addwindow("listreferrals", "Referrals", "Referral/List", function() { }, "adminlist");
acore.addwindow("listcontacts", "Contacts", "Contact/Grid", function() { }, "adminlist");
acore.addwindow("listhospitals", "Hospitals", "Hospital/Grid", function() { }, "adminlist");
acore.addwindow("listinsurances", "Insurances / Payors", "Insurance/Grid", function() { }, "adminlist");
acore.addwindow("listlocations", "Locations", "Location/Grid", function() { }, "adminlist");
acore.addwindow("listphysicians", "Physicians", "Physician/Grid", function() { }, "adminlist");
acore.addwindow("listusers", "Users", "User/Grid", function() { }, "adminlist");
acore.addmenu("Billing", "billings","admin");
acore.addwindow("visitrates", "Edit Visit Rates", "Agency/VisitRates", function() {Agency.InitEditCost(); }, "billings");

<% } %>

<% if (Current.HasRight(Permissions.ManagePayroll)) { %>acore.addwindow("payrollsummary", "Payroll Summary", "Payroll/Search", function() { Payroll.InitSearch(); }, "admin");<% } %>

//<% if (Current.HasRight(Permissions.AccessReports)) { %>
//acore.addmenu("Reports", "reports", "mainmenu", "reports.png");
//acore.addwindow("reportcenter", "Report Center", "Report/Center", function() { Report.Init(); }, "reports");
//acore.addmenuitem("reportcenter", "reports", "Patient Reports", "Report/Center", function() { Report.Patient(); });
//acore.addmenuitem("reportcenter", "reports", "Clinical Reports", "Report/Center", function() { Report.Patient(); });
//acore.addmenuitem("reportcenter", "reports", "Schedule Reports", "Report/Center", function() { Report.Patient(); });
//acore.addmenuitem("reportcenter", "reports", "Billing Reports", "Report/Center", function() { Report.Patient(); });
//acore.addmenuitem("reportcenter", "reports", "Employee Reports", "Report/Center", function() { Report.Patient(); });
//acore.addmenuitem("reportcenter", "reports", "Statistical Reports", "Report/Center", function() { Report.Patient(); });
//<% } %>

acore.addmenu("Help", "help", "mainmenu", "help.png");
acore.addmenulink("Discussion Forum", "/Forum", "help");
acore.addmenulink("Recent Updates", "/Updates", "help");
acore.addmenulink("Join GoTo Meeting", "/GoToMeeting", "help");
acore.addmenulink("Open Support Ticket", "/SupportTicket", "help");

acore.addwindow("rap", "Rap", null, null, false);
acore.addwindow("final", "Final", null, null, false);
acore.addwindow("scheduledetails", "Task Details", null, null, false);
acore.addwindow("editepisode", "Edit Episode", null, null, false);
acore.addwindow("editagency", "Edit Agency", null, null, false);
acore.addwindow("editorder", "Edit Order", null, null, false);
acore.addwindow("editcontact", "Edit Contact", null, null, false);
acore.addwindow("edithospital", "Edit Hospital", null, null, false);
acore.addwindow("editlocation", "Edit Location", null, null, false);
acore.addwindow("editphysician", "Edit Physician", null, null, false);
acore.addwindow("editinsurance", "Edit Insurance", null, null, false);
acore.addwindow("edituser", "Edit User", null, null, false);
acore.addwindow("editpatient", "Edit Patient", null, null, false);
acore.addwindow("admitpatient", "Admit Patient", null, null, false);
acore.addwindow("nonadmitpatient", "Patient Non-Admission", null, null, false);
acore.addwindow("editreferral", 'Edit Referral', null, null, false);
acore.addwindow("validation", "Validation Result", null, null, false);
acore.addwindow("newmissedvisit", "Missed Visit Report", null, null, false);
acore.addwindow("notessupplyworksheet", "Supply Worksheet", "Schedule/SupplyWorksheet", function() { }, false);
acore.addwindow("editemergencycontact", "Edit Emergency Contact", "Patient/EditEmergencyContactContent", function() { }, false);
acore.addwindow("newemergencycontact", "New Emergency Contact", "Patient/NewEmergencyContactContent", function() { }, false);
<% if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %>acore.addwindow("editplanofcare", "Edit 485 - Plan of Care", null, null, false);<% } %>

$('ul#mainmenu').superfish();
    
<% if (Current.HasRight(Permissions.Dashboard)) { %>acore.open("homepage");<% } %>
</script>

