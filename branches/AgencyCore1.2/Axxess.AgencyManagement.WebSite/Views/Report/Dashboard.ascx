﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<ul class="widgets">
    <li class="widget">
        <div class="widget-head"><h5>Patient Reports</h5></div>
        <div class="widget-content" style="height: 190px;">
            <div class="report_links">
                <ul> 
                    <li class="link"><a href="/Report/Patient/Roster" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Patient Roster</span></a></li> 
                    <li class="link"><a href="/Report/Patient/EmergencyList" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Emergency Contact Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/Birthdays" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Patient Birthday Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/AddressList" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Patient Address Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/Physician" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Patient By Physician Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/SocCertPeriod" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Patient SOC Cert Period Listing</span></a></li> 
                </ul>
            </div>
            <div class="widget-more"><a href="/Report/Patient" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;">More &raquo;</a></div>
        </div>
    </li>
    <li class="widget">
        <div class="widget-head"><h5>Billing Reports</h5></div>
        <div class="widget-content" style="height: 190px;">
            <div class="report_links">
                <ul> 
                    <li class="link"><a href="/Report/Billing/OutstandingClaims" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Outstanding Claims</span></a></li> 
                    <li class="link"><a href="/Report/Billing/SummaryReport" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Billing Summary Report</span></a></li> 
                    <li class="link"><a href="/Report/Billing/SubmittedHistory" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Submitted Claims History</span></a></li> 
                    <li class="link"><a href="/Report/Billing/PaidHistory" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Paid Claims History</span></a></li> 
                    <li class="link"><a href="/Report/Billing/PPSAnalysis" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">PPS Analysis</span></a></li> 
                    <li class="link"><a href="/Report/Billing/DetailReport" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Episode Detail Report</span></a></li> 
                </ul>
            </div>
            <div class="widget-more"><a href="/Report/Billing" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;">More &raquo;</a></div>
        </div>
    </li>
</ul>
<ul class="widgets">
    <li class="widget">
        <div class="widget-head"><h5>Clinical Reports</h5></div>
        <div class="widget-content" style="height: 190px;">
            <div class="report_links">
                <ul> 
                    <li class="link"><a href="/Report/Clinical/OpenOasis" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Open OASIS</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/CurrentHospitalized" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Current Hospitalized Patients</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/CnaEpisodes" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">CNA Episodes</span></a></li>
                    <li class="link"><a href="/Report/Clinical/Orders" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Orders Management</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/RecentHospitalized" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Recent Hospitalization</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/MissedVisits" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Missed Visit Report</span></a></li> 
                </ul>
            </div>
            <div class="widget-more"><a href="/Report/Clinical" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;">More &raquo;</a></div>
        </div>
    </li>
    <li class="widget">
        <div class="widget-head"><h5>Employee Reports</h5></div>
        <div class="widget-content" style="height: 190px;">
            <div class="report_links">
                <ul> 
                    <li class="link"><a href="/Report/Employee/Roster" onclick="Report.Show(5, '#employee_reports', $(this).attr('href')); return false;"><span class="title">Employee Roster</span></a></li> 
                    <li class="link"><a href="/Report/Employee/ContactList" onclick="Report.Show(5, '#employee_reports', $(this).attr('href')); return false;"><span class="title">Employee Contact Listing</span></a></li> 
                    <li class="link"><a href="/Report/Employee/Birthdays" onclick="Report.Show(5, '#employee_reports', $(this).attr('href')); return false;"><span class="title">Employee Birthday Listing</span></a></li> 
                    <li class="link"><a href="/Report/Employee/ServiceLength" onclick="Report.Show(5, '#employee_reports', $(this).attr('href')); return false;"><span class="title">Employee Length of Service Report</span></a></li> 
                    <li class="link"><a href="/Report/Employee/License" onclick="Report.Show(5, '#employee_reports', $(this).attr('href')); return false;"><span class="title">Employee License Listing</span></a></li> 
                </ul>
            </div>
            <div class="widget-more"><a href="/Report/Employee" onclick="Report.Show(5, '#employee_reports', $(this).attr('href')); return false;">More &raquo;</a></div>
        </div>
    </li>
</ul>
<ul class="widgets">
    <li class="widget">
        <div class="widget-head"><h5>Schedule Reports</h5></div>
        <div class="widget-content" style="height: 190px;">
            <div class="report_links">
                <ul> 
                    <li class="link"><a href="/Report/Schedule/PatientWeekly" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;"><span class="title">Patient Weekly Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/EmployeeWeekly" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;"><span class="title">Employee Weekly Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/ScheduleSummary" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;"><span class="title">Summary of Scheduled Visits</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/DailySchedule" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;"><span class="title">Daily Work Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/MonthlySchedule" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;"><span class="title">Monthly Work Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/MasterSchedule" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;"><span class="title">Master Schedule</span></a></li> 
                </ul>
            </div>
            <div class="widget-more"><a href="/Report/Schedule" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;">More &raquo;</a></div>
        </div>
    </li>
    <li class="widget">
        <div class="widget-head"><h5>Statistical Reports</h5></div>
        <div class="widget-content" style="height: 190px;">
            <div class="report_links">
                <ul> 
                    <li class="link"><a href="/Report/Statistical/ReferralSource" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Admission By Referral Source</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/DiagnosisCode" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Admission By Diagnosis Code</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/DischargeType" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Discharge by Discharge Type</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/AgeRaceSex" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Patient Statistics By Age, Race, Sex</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/InsuranceCompany" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;"><span class="title">Patients By Insurance Company</span></a></li> 
                </ul>
            </div>
            <div class="widget-more"><a href="/Report/Statistical" onclick="Report.Show(6, '#statistical_reports', $(this).attr('href')); return false;">More &raquo;</a></div>
        </div>
    </li>
</ul>