﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Roster</legend>
        <div class="column">
            <div class="row"><label for="Report_Patient_Roster_Status" class="float_left">Status:</label><div class="float_right"><select id="Report_Patient_Roster_Status" name="StatusId" class="patientStatusDropDown"><option value="0">All</option><option value="1">Active</option><option value="2">Discharged</option></select></div></div>
            <div class="row"><label for="Report_Patient_Roster_Branch" class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Branches, "PatientRoster_AddressBranchCode", "", new { @id = "Report_Patient_Roster_Branch", @class = "AddressBranchCode report_input", @tabindex = "3" })%></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientRoster();">Generate Report</a></li></ul></div>
    </fieldset>
    <div class="clear">&nbsp;</div>
    <div class="ReportGrid">
         <%= Html.Telerik().Grid<PatientRoster>().Name("ReportPatientRosterGrid").Columns(columns =>
            {
                columns.Bound(r => r.PatientDisplayName).Title("Patient");
                columns.Bound(r => r.PatientAddressLine1).Title("Address");
                columns.Bound(r => r.PatientAddressCity).Title("City");
                columns.Bound(r => r.PatientAddressStateCode).Title("State");
                columns.Bound(r => r.PatientAddressZipCode).Title("Zip Code");
                columns.Bound(r => r.PatientPhone).Title("Home Phone");
                columns.Bound(r => r.PatientGender);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("PatientRoster", "Report", new { StatusId = 1, AddressBranchCode = Guid.Empty }))
           .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $('#ReportPatientRosterGrid .t-grid-content').css({ 'height': 'auto' }); </script>