﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="wrapper">
    <fieldset>
        <legend>Current Hospitalized Patients</legend>
        <span><%= DateTime.Now.ToString() %></span>
    </fieldset>
    <div class="clear">&nbsp;</div>
    <div class="ReportGrid">
        <%= Html.Telerik().Grid<ClinicalOrder>().Name("Report_Patient_Orders_Grid").Columns(columns =>
           {
               columns.Bound(p => p.Number);
               columns.Bound(p => p.PatientName);
               columns.Bound(p => p.Type);
               columns.Bound(p => p.Physician);
               columns.Bound(p => p.CreatedDate);

           }).DataBinding(dataBinding => dataBinding.Ajax().Select("ClinicalOrders", "Report", new { StatusId = 5 }))
            .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $("#Report_Patient_Orders_Grid .t-grid-content").css({ 'height': 'auto' });</script>