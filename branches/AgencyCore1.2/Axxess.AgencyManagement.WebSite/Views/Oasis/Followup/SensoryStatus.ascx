﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpSensoryStatusForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("FollowUp_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<%= Html.Hidden("categoryType", "Sensory")%>
<div class="wrapper main">
<fieldset class="oasis">
    <legend>OASIS</legend>
    <div class="wide_column">
        <div class="row">
            <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1200');">(M1200)</a> Vision (with corrective lenses if the patient usually wears them)</label><%= Html.Hidden("FollowUp_M1200Vision", " ", new { @id = "" }) %>
            <div class="margin">
                <div><%= Html.RadioButton("FollowUp_M1200Vision", "00", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "00" ? true : false, new { @id = "FollowUp_M1200Vision00", @class = "radio float_left" }) %><label for="FollowUp_M1200Vision00"><span class="float_left">0 &ndash;</span><span class="normal margin">Normal vision: sees adequately in most situations; can see medication labels, newsprint.</span></label></div>
                <div><%= Html.RadioButton("FollowUp_M1200Vision", "01", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "01" ? true : false, new { @id = "FollowUp_M1200Vision01", @class = "radio float_left" })%><label for="FollowUp_M1200Vision01"><span class="float_left">1 &ndash;</span><span class="normal margin">Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm&rsquo;s length.</span></label></div>
                <div><div class="float_right oasis"><div class="tooltip_oasis" onclick="Oasis.ToolTip('M1200');">?</div></div><%= Html.RadioButton("FollowUp_M1200Vision", "02", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "02" ? true : false, new { @id = "FollowUp_M1200Vision02", @class = "radio float_left" })%><label for="FollowUp_M1200Vision02"><span class="float_left">2 &ndash;</span><span class="normal margin">Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.</span></label></div>
            </div>
        </div>
    </div>
</fieldset>
 <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.Validate('{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "FollowUp")%></li>
        </ul>
    </div>
</div>
<% } %>
