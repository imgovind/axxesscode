﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpTherapyNeedForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("FollowUp_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<%= Html.Hidden("categoryType", "TherapyNeed")%> 
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS M2200</legend>
        <div class="wide_column">
            <div class="row">
                <label for="FollowUp_M2200NumberOfTherapyNeed" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2200');">(M2200)</a> Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)? (Enter zero [&ldquo;000&rdquo;] if no therapy visits indicated.)</label>
                <div id="FollowUp_M2200NumberOfTherapyNeedDiv" class="float_right oasis">
                    <label>Number of therapy visits indicated (total of physical, occupational and speech-language pathology combined).</label>
                    <%=Html.TextBox("FollowUp_M2200NumberOfTherapyNeed", data.ContainsKey("M2200NumberOfTherapyNeed") ? data["M2200NumberOfTherapyNeed"].Answer : "", new { @id = "FollowUp_M2200NumberOfTherapyNeed", @class = "vitals numeric", @maxlength = "3" })%>
                    
                </div>
                <div class="clear"></div>
                <div class="float_right oasis">
                    <input name="FollowUp_M2200TherapyNeedNA" value="" type="hidden" />
                    <%= string.Format("<input id='FollowUp_M2200TherapyNeedNA' class='radio' name='FollowUp_M2200TherapyNeedNA' value='1' type='checkbox' {0} onclick=\"Oasis.blockText($(this),'#FollowUp_M2200NumberOfTherapyNeed');\"/>", data.ContainsKey("M2200TherapyNeedNA") && data["M2200TherapyNeedNA"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="FollowUp_M2200TherapyNeedNA">NA &ndash; Not Applicable: No case mix group defined by this assessment.</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2200');">?</div>
                </div>
            </div>
        </div>
    </fieldset>
      <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.Validate('{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "StartOfCare")%></li>
        </ul>
    </div>
</div> 
<% } %>
<script type="text/javascript">
    Oasis.hideIfChecked($("#FollowUp_M2200TherapyNeedNA"), $("#FollowUp_M2200NumberOfTherapyNeedDiv"));
</script>