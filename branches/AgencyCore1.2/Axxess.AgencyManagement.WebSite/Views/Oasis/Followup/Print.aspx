﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Assessment>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<% var data = Model.ToDictionary(); %>
<head>
    <%= string.Format("<title>Follow-Up | {0}{1}, {2}{3}</title>",data.ContainsKey("M0040LastName")?data["M0040LastName"].Answer:"",data.ContainsKey("M0040Suffix")?" "+data["M0040Suffix"].Answer:"",data.ContainsKey("M0040FirstName")?data["M0040FirstName"].Answer:"",data.ContainsKey("M0040MI")?" "+data["M0040MI"].Answer:"") %>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body>
    <div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Follow-Up</h1>
        <h2>Demographics</h2>
        <div class="tricol">
            <span><strong>Time In:</strong>00:00</span>
            <span><strong>Time Out:</strong>00:00</span>
            <span><strong>Visit Date:</strong>00/00/0000</span>
        </div><div class="bicol">
            <span><strong>(M0020) Patient ID Number:</strong><%= data.ContainsKey("M0020PatientIdNumber") ? data["M0020PatientIdNumber"].Answer : "" %></span>
            <span><strong>(M0030) Follow-Up Date:</strong><%= data.ContainsKey("M0030SocDate") ? data["M0030SocDate"].Answer : "" %></span>
            <span><strong>(M0032) Resuption of Care Date:</strong><%= data.ContainsKey("M0032ROCDate") ? data["M0032ROCDate"].Answer : "" %></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0032ROCDateNotApplicable") && data["M0032ROCDateNotApplicable"].Answer == "1" ? "&#x2713;" : "" %></span> NA &ndash; Not Applicable</span>
        </div><div>
            <span><strong>Episode Start Date:</strong><%= data.ContainsKey("GenericEpisodeStartDate") ? data["GenericEpisodeStartDate"].Answer : "" %></span>
        </div><div class="bicol">
            <span><strong>(M0040) Patient Name:</strong>(Last Suffix First MI)</span>
            <span><%= data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : ""%> <%= data.ContainsKey("M0040Suffix") ? data["M0040Suffix"].Answer : ""%> <%= data.ContainsKey("M0040FirstName") ? data["M0040FirstName"].Answer : ""%> <%= data.ContainsKey("M0040MI") ? data["M0040MI"].Answer : "" %></span>
            <span><strong>(M0064) Social Security Number:</strong><%= data.ContainsKey("M0064PatientSSN") ? data["M0064PatientSSN"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0064PatientSSNUnknown") && data["M0064PatientSSNUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Unknown or Not Available</span>
        </div><div class="bicol">
            <span><strong>(M0050) Patient State:</strong><%= data.ContainsKey("M0050PatientState") ? data["M0050PatientState"].Answer : ""%></span>
            <span><strong>(M0060) Patient Zip Code:</strong><%= data.ContainsKey("M0060PatientZipCode") ? data["M0060PatientZipCode"].Answer : ""%></span>
        </div><div class="bicol">
            <span><strong>(M0063) Medicare Number:</strong><%= data.ContainsKey("M0063PatientMedicareNumber") ? data["M0063PatientMedicareNumber"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0063PatientMedicareNumberUnknown") && data["M0063PatientMedicareNumberUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No Medicare</span>
            <span><strong>(M0065) Medicaid Number:</strong><%= data.ContainsKey("M0065PatientMedicaidNumber") ? data["M0065PatientMedicaidNumber"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0065PatientMedicaidNumberUnknown") && data["M0065PatientMedicaidNumberUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No Medicaid</span>
        </div><div class="bicol">
            <span><strong>(M0066) Birth Date:</strong><%= data.ContainsKey("M0066PatientDoB") ? data["M0066PatientDoB"].Answer : ""%></span>
            <span><strong>(M0069) Gender:</strong><%= data.ContainsKey("M0069Gender") ? data["M0069Gender"].Answer == "1" ? "Male" : "Female" : "" %></span>
        </div><div class="bicol">
            <span><strong>(M0080) Discipline of Person Completing Assessment:</strong><% if (data.ContainsKey("M0080DisciplinePerson")) { switch(data["M0080DisciplinePerson"].Answer) { case "01": %>1 &ndash; RN<% break; case "02": %>2 &ndash; PT<% break; case "03": %>3 &ndash; SLP/ST<% break; case "04": %>4 &ndash; OT<% break; } } %></span>
            <span><strong>(M0090) Date Assessment Completed:</strong><%= data.ContainsKey("M0090AssessmentCompleted") ? data["M0090AssessmentCompleted"].Answer : ""%></span>
        </div><div>
            <span><strong>(M0100) This Assessment is Currently Being Completed for the Following Reason:</strong></span>
            <span><strong>Start/Resuption of Care</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Follow-Up&mdash;further visits planned</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Resumption of care (after inpatient stay)</span>
            <span><strong>Follow-Up</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Follow-Up (follow-up) reassessment <em>[Go to M0110]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Other follow-up <em>[Go to M0110]</em></span>
            <span><strong>Transfer to an Inpatient Facility</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "06" ? "&#x2713;" : ""%></span> 6 &ndash; Transfer to inpatient facility&mdash;patient not discharged from agency <em>[Go to M1040]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "07" ? "&#x2713;" : ""%></span> 7 &ndash; Transfer to inpatient facility&mdash;patient discharged from agency <em>[Go to M1040]</em></span>
            <span><strong>Discharge from Agency – Not to an Inpatient Facility</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "08" ? "&#x2713;" : ""%></span> 8 &ndash; Death at home <em>[Go to M0903]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "09" ? "&#x2713;" : ""%></span> 9 &ndash; Discharge from agency <em>[Go to M1040]</em></span>
        </div><div>
            <span><strong>(M0102) Date of Physician-ordered Follow-Up (Resumption of Care):</strong><%= data.ContainsKey("M0102PhysicianOrderedDate") && data["M0102PhysicianOrderedDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer + " <em>[Go to M0110]</em>" : "" %></span>
            <span>If the physician indicated a specific Follow-Up (resumption of care) date when the patient was referred for home health services, record the date specified.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && data["M0102PhysicianOrderedDateNotApplicable"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No specific SOC date ordered by physician</span>
        </div><div>
            <span><strong>(M0104) Date of Referral:</strong><%= data.ContainsKey("M0104ReferralDate") && data["M0104ReferralDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer : "" %></span>
            <span>Indicate the date that the written or documented orders from the physician or physician designee for initiation or resumption of care were received by the HHA.</span>
        </div><div>
            <span><strong>(M0110) Episode Timing:</strong><%= data.ContainsKey("M0104ReferralDate") && data["M0104ReferralDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer : "" %></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Early</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Later</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "UK" ? "&#x2713;" : ""%></span> UK &ndash; Unknown</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Not Applicable/No Medicare case mix group to be defined by this assessment </span>
            <span>Is the Medicare home health payment episode for which this assessment will define a case mix group an &ldquo;early&rdquo; episode or a &ldquo;later&rdquo; episode in the patient&rsquo;s current sequence of adjacent Medicare home health payment episodes?</span>
        </div><div>
            <span><strong>(M0140) Race/Ethnicity <em>(Mark all that apply)</em>:</strong></span>
            <span class="bicol">
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceAMorAN") && data["M0140RaceAMorAN"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; American Indian or Alaska Native</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceAsia") && data["M0140RaceAsia"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Asian</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceBalck") && data["M0140RaceBalck"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Black or African American</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceHispanicOrLatino") && data["M0140RaceHispanicOrLatino"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Hispanic or Latino</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceNHOrPI") && data["M0140RaceNHOrPI"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Native Hawaiian or Pacific Islander</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceWhite") && data["M0140RaceWhite"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; White</span>        
            </span>
        </div><div>
            <span><strong>(M0150) Current Payment Sources for Home Care <em>(Mark all that apply)</em>:</strong></span>
            <span class="bicol">
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceNone") && data["M0150PaymentSourceNone"].Answer == "1" ? "&#x2713;" : ""%></span> 0 &ndash; None&mdash;No charge for current services</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCREFFS") && data["M0150PaymentSourceMCREFFS"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Medicare (traditional fee-for-service)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCREHMO") && data["M0150PaymentSourceMCREHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Medicare (HMO/Managed Care/Advantage plan)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCAIDFFS") && data["M0150PaymentSourceMCAIDFFS"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Medicaid (traditional fee-for-service)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMACIDHMO") && data["M0150PaymentSourceMACIDHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Medicaid (HMO/Managed Care)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceWRKCOMP") && data["M0150PaymentSourceWRKCOMP"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Worker&rsquo;s compensation</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceTITLPRO") && data["M0150PaymentSourceTITLPRO"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; Title progams (e.g. Title III, V, or XX)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceOTHGOVT") && data["M0150PaymentSourceOTHGOVT"].Answer == "1" ? "&#x2713;" : ""%></span> 7 &ndash; Other government (e.g. TriCare, VA etc)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourcePRVINS") && data["M0150PaymentSourcePRVINS"].Answer == "1" ? "&#x2713;" : ""%></span> 8 &ndash; Private Insurance</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourcePRVHMO") && data["M0150PaymentSourcePRVHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 9 &ndash; Private HMO/managed care</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceSelfPay") && data["M0150PaymentSourceSelfPay"].Answer == "1" ? "&#x2713;" : ""%></span> 10 &ndash; Self-pay</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceOtherSRS") && data["M0150PaymentSourceOtherSRS"].Answer == "1" ? "&#x2713;" : ""%></span> 11 &ndash; Other<%= data.ContainsKey("M0150PaymentSourceOther") && data["M0150PaymentSourceOther"].Answer.IsNotNullOrEmpty() ? ": " + data["M0150PaymentSourceOther"].Answer : "" %></span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceUnknown") && data["M0150PaymentSourceUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Unknown</span>        
            </span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Follow-Up</h1>
        <h2>Patient History and Diagnoses</h2>
        <div>
            <span><strong>(M1020/1022/1024) Diagnoses, Symptom Control, and Payment Diagnoses</strong></span>
            <span class="instructions">
                <p>List each diagnosis for which the patient is receiving home care (Column 1) and enter its ICD-9-C M code at the level of highest specificity (no surgical/procedure codes) (Column 2). Diagnoses are listed in the order that best reflect the seriousness of each condition and support the disciplines and services provided. Rate the degree of symptom control for each condition (Column 2). Choose one value that represents the degree of symptom control appropriate for each diagnosis: V-codes (for M1020 or M1022) or E-codes (for M1022 only) may be used. ICD-9-C M sequencing requirements must be followed if multiple coding is indicated for any diagnoses. If a V-code is reported in place of a case mix diagnosis, then optional item M1024 Payment Diagnoses (Columns 3 and 4) may be completed. A case mix diagnosis is a diagnosis that determines the Medicare P P S case mix group. Do not assign symptom control ratings for V- or E-codes.</p>
                <strong>Code each row according to the following directions for each column:</strong>
                <ul>
                    <li><span class="float_left">Column 1:</span><p>Enter the description of the diagnosis.</p></li>
                    <li><span class="float_left">Column 2:</span><p>Enter the ICD-9-C M code for the diagnosis described in Column 1;</p>
                        <ul>
                            <li>Rate the degree of symptom control for the condition listed in Column 1 using thefollowing scale:</li>
                            <li><span class="float_left">0.</span><p>Asymptomatic, no treatment needed at this time</p></li>
                            <li><span class="float_left">1.</span><p>Symptoms well controlled with current therapy</p></li>
                            <li><span class="float_left">2.</span><p>Symptoms controlled with difficulty, affecting daily functioning; patient needsongoing monitoring</p></li>
                            <li><span class="float_left">3.</span><p>Symptoms poorly controlled; patient needs frequent adjustment in treatment anddose monitoring</p></li>
                            <li><span class="float_left">4.</span><p>Symptoms poorly controlled; history of re-hospitalizations Note that in Column2 the rating for symptom control of each diagnosis should not be used to determinethe sequencing of the diagnoses listed in Column 1. These are separate items andsequencing may not coincide. Sequencing of diagnoses should reflect the seriousnessof each condition and support the disciplines and services provided.</p></li>
                        </ul>
                    </li>
                    <li><span class="float_left">Column 3:</span><p>(Optional) If a V-code is assigned to any row in Column 2, in place ofa case mix diagnosis, it may be necessary to complete optional item M1024 PaymentDiagnoses (Columns 3 and 4). See OASIS-C Guidance Manual.</p></li>
                    <li><span class="float_left">Column 4:</span><p>(Optional) If a V-code in Column 2 is reported in place of a case mixdiagnosis that requires multiple diagnosis codes under ICD-9-C M coding guidelines,enter the diagnosis descriptions and the ICD-9-C M codes in the same row in Columns 3 and 4. For example, if the case mix diagnosis is a manifestation code, record the diagnosis description and ICD-9-C M code for the underlying condition in Column 3 of that row and the diagnosis description and ICD-9-C M code for the manifestation in Column 4 of that row. Otherwise, leave Column 4 blank in that row.</p></li>
                </ul>
            </span>
        </div>
        <div><table>
            <tbody><tr><th colspan="2">(M1020) Primary Diagnosis & (M1022) Other Diagnoses</th><th colspan="2">(M1024) Payment Diagnoses <em>(Optional)</em></th></tr>
                <tr>
                    <td>Descriptions</td>
                    <td>ICD-9-CM/Symptom Control Rating</td>
                    <td>Description/ICD-9-CM</td>
                    <td>Description/ICD-9-CM</td>
                </tr><tr>
                    <td>a. <%= data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : "" %></td>
                    <td>a. <%= data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer : ""%></td>
                    <td>a. <%= data.ContainsKey("M1024PaymentDiagnosesA3") ? data["M1024PaymentDiagnosesA3"].Answer : ""%></td>
                    <td>a. <%= data.ContainsKey("M1024PaymentDiagnosesA4") ? data["M1024PaymentDiagnosesA4"].Answer : ""%></td>
                </tr>
                <% for (int i = 1; i < 6; i++) { %>
                    <%= string.Format(
               "<tr>" +
                   "<td>{0}. {1}</td>" + 
                   "<td>{0}. {2}</td>" +
                   "<td>{0}. {3}</td>" +
                   "<td>{0}. {4}</td>" +
               "</tr>",
                           Convert.ToChar(i + 96),
                           data.ContainsKey("M1022PrimaryDiagnosis" + i) ? data["M1022PrimaryDiagnosis" + i].Answer : "",
                           data.ContainsKey("M1022ICD9M" + i) ? data["M1022ICD9M" + i].Answer : "",
                           data.ContainsKey("M1024PaymentDiagnoses" + Convert.ToChar(i + 65) + "3") ? data["M1024PaymentDiagnoses" + Convert.ToChar(i + 65) + "3"].Answer : "",
                           data.ContainsKey("M1024PaymentDiagnoses" + Convert.ToChar(i + 65) + "4") ? data["M1024PaymentDiagnoses" + Convert.ToChar(i + 65) + "4"].Answer : "") %>
                <% } %>
            </tbody>
        </table></div>
        <div>
            <span><strong>(M1030) Therapies the Patient Receives at Home</strong></span>
            <span class="bicol">
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1030HomeTherapiesInfusion") && data["M1030HomeTherapiesInfusion"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Intravenous or Infusion Therapy (Excludes TPN)</span>
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1030HomeTherapiesParNutrition") && data["M1030HomeTherapiesParNutrition"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Parenteral Nutrition (TPN or Lipids)</span>
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1030HomeTherapiesEntNutrition") && data["M1030HomeTherapiesEntNutrition"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Enteral Nutrition (Nasogastric, Gastrostomy, Jejunostomy, or Any Other Artificial Entry into the Alimentary Canal)</span>
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1030HomeTherapiesNone") && data["M1030HomeTherapiesNone"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; None of the Above</span>
            </span>
        </div>
        <h2>Sensory Status</h2>
        <div>
            <span><strong>(M1200) Vision <em>(with corrective lenses if the patient usually wears them)</em></strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Normal vision: sees adequately in most situations; can see medication labels, newsprint.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm&rsquo;s length.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.</span>
        </div>
        <h2>Pain</h2>
        <div>
            <span><strong>(M1242) Frequency of Pain Interfering with patient&rsquo;s activity or movement</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Patient has no pain</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Patient has pain that does not interfere with activity or movement</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Less often than daily</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Daily, but not constantly</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; All of the time</span>
        </div>
        <h2>Integumentary Status</h2>
        <div>
            <span><strong>(M1306) Does this patient have at least one Unhealed Pressure Ulcer at Stage II or Higher or designated as &ldquo;unstageable&rdquo;?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "0" ? "&#x2713;" : ""%></span> 0 &ndash; No <em>[Go to M1322]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Follow-up</h1>
        <h2>Integumentary Status</h2>
        <div>
            <span><strong>(M1308) Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each Stage: <em>(Enter &ldquo;0&rdquo; if none; excludes Stage I pressure ulcers)</em></strong></span>
        </div>
        <div><table><tbody>
            <tr><td></td><th>Column 1<br /><em>Complete at SOC/ROC/FU &amp; D/C</em></th><th>Column 2<br /><em>Complete at FU &amp; D/C</em></th></tr>
            <tr><td>Stage description &mdash; unhealed pressure ulcers</td><td>Number Currently Present</td><td>Number of those listed in Column 1 that were present on admission (most recent SOC / ROC)</td></tr>
            <tr>
                <td>a. Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer with red pink wound bed, without slough. May also present as an intact or open/ruptured serum-filled blister.</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerCurrent") ? data["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerAdmission") ? data["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td>b. Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscles are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") ? data["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerAdmission") ? data["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td>c. Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") ? data["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageIVUlcerAdmission") ? data["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td>d.1 Unstageable: Known or likely but unstageable due to non-removable dressing or device</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td>d.2 Unstageable: Known or likely but unstageable due to coverage of wound bed by slough and/or eschar.</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td>d.3 Unstageable: Suspected deep tissue injury in evolution.</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td colspan="3">Directions for M1310, M1312, and M1314: If the patient has one or more unhealed (non-epithelialized) Stage III or IV pressure ulcers, identify the Stage III or IV pressure ulcer with the largest surface dimension (length x width) and record in centimeters. If no Stage III or Stage IV pressure ulcers, go to M1320.</td>
            </tr>
        </tbody></table></div>
        <div>
            <span><strong>(M1322) Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. The area may be painful, firm, soft, warmer or cooler as compared to adjacent tissue.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Zero</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; One</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Two</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Three</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Four or more</span>
        </div><div>
            <span><strong>(M1324) Stage of Most Problematic Unhealed (Observable) Pressure Ulcer</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Stage I</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Stage II</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Stage III</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Stage IV</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; No observable pressure ulcer or unhealed pressure ulcer</span>
        </div><div>
            <span><strong>(M1330) Does this patient have a Stasis Ulcer?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No <em>[Go to M1340]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes, patient has BOTH observable and unobservable stasis ulcers</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Yes, patient has observable stasis ulcers ONLY</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing) <em>[Go to M1340]</em></span>
        </div><div>
            <span><strong>(M1332) Current Number of (Observable) Stasis Ulcer(s)</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; One</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Two</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Three</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Four or more</span>
        </div><div>
            <span><strong>(M1334) Status of Most Problematic (Observable) Stasis Ulcer</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Newly epithelialized</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Fully granulating</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Early/partial granulation</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Not healing</span>
        </div><div>
            <span><strong>(M1340) Does this patient have a Surgical Wound?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No <em>[Go to M1350]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes, patient has at least one (observable) surgical wound</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Surgical wound known but not observable due to non-removable dressing <em>[Go to M1350]</em></span>
        </div><div>
            <span><strong>(M1342) Status of Most Problematic (Observable) Surgical Wound</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Newly epithelialized</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Fully granulating</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Early/partial granulation</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Not healing</span>
        </div><div>
            <span><strong>(M1350) Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy, other than those described above that is receiving intervention by the home health agency?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "0" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Follow-Up</h1>
        <h2>Respiratory Status</h2>
        <div>
            <span><strong>(M1400) When is the patient dyspneic or noticeably Short of Breath?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Patient is not short of breath</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; When walking more than 20 feet, climbing stairs</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; With moderate exertion (e.g., while dressing, using commode or bedpan, walking distances less than 20 feet)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; With minimal exertion (e.g., while eating, talking, or performing other ADLs) or with agitation</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; At rest (during day or night)</span>
        </div>
        <h2>Elimination Status</h2>
        <div>
            <span><strong>(M1610) Urinary Incontinence or Urinary Catheter Presence:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No incontinence or catheter (includes anuria or ostomy for urinary drainage) <em>[Go to M1620]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Patient is incontinent</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Patient requires a urinary catheter (i.e., external, indwelling, intermittent, suprapubic) <em>[Go to M1620]</em></span>
        </div><div>
            <span><strong>(M1620) Bowel Incontinence Frequency:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Very rarely or never has bowel incontinence</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Less than once weekly</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; One to three times weekly</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Four to six times weekly</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; On a daily basis</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; More often than once daily</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Patient has ostomy for bowel elimination</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "UK" ? "&#x2713;" : ""%></span> UK &ndash; Unknown <em>[Omit &ldquo;UK&rdquo; option on FU, DC]</em></span>
        </div><div>
            <span><strong>(M1630) Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that (within the last 14 days): a) was related to an inpatient facility stay, or b) necessitated a change in medical or treatment regimen?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Patient does not have an ostomy for bowel elimination.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Patient&rsquo;s ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.</span>
        </div>
	    <h2>ADL/IADLs</h2>
        <div>
            <span><strong>(M1810) Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts and blouses, managing zippers, buttons, and snaps:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to dress upper body without assistance if clothing is laid out or handed to the patient.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Someone must help the patient put on upper body clothing.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Patient depends entirely upon another person to dress the upper body.</span>
        </div><div>
            <span><strong>(M1820) Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to obtain, put on, and remove clothing and shoes without assistance.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Patient depends entirely upon another person to dress lower body.</span>
        </div><div>
            <span><strong>(M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to bathe self in shower or tub independently, including getting in and out of tub/shower.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Able to bathe in shower or tub with the intermittent assistance of another person:
                <ul>
                    <li>(a) for intermittent supervision or encouragement or reminders, OR</li>
                    <li>(b) to get in and out of the shower or tub, OR</li>
                    <li>(c) for washing difficult to reach areas.</li>
                </ul></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the assistance or supervision of another person throughout the bath.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "06" ? "&#x2713;" : ""%></span> 6 &ndash; Unable to participate effectively in bathing and is bathed totally by another person.</span>
        </div><div>
            <span><strong>(M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to get to and from the toilet and transfer independently with or without a device.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Is totally dependent in toileting.</span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Follow-Up</h1>
        <h2>ADL/IADLs</h2>
        <div>
            <span><strong>(M1850) Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to independently transfer.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to transfer with minimal human assistance or with use of an assistive device.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Able to bear weight and pivot during the transfer process but unable to transfer self.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Unable to transfer self and is unable to bear weight or pivot when transferred by another person.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Bedfast, unable to transfer but is able to turn and position self in bed.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Bedfast, unable to transfer and is unable to turn and position self.</span>
        </div><div>
            <span><strong>(M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (i.e., needs no human assistance or assistive device).</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Requires use of a two-handed device (e.g., walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Able to walk only with the supervision or assistance of another person at all times.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Chairfast, unable to ambulate but is able to wheel self independently.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Chairfast, unable to ambulate and is unable to wheel self.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "06" ? "&#x2713;" : ""%></span> 6 &ndash; Bedfast, unable to ambulate or be up in a chair.</span>
        </div>
        <h2>Medications</h2>
        <div>
            <span><strong>(M2030) Management of Injectable Medications: Patient&rsquo;s current ability to prepare and take all prescribed injectable medications reliably and safely, including administration of correct dosage at the appropriate times/intervals. Excludes IV medications.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to independently take the correct medication(s) and proper dosage(s) at the correct times.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to take injectable medication(s) at the correct times if:
                <ul>
                    <li><span class="float_left">(a) individual syringes are prepared in advance by another person; OR</span></li>
                    <li><span class="float_left">(b) another person develops a drug diary or chart.</span></li>
                </ul></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Unable to take injectable medication unless administered by another person.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; No injectable medications prescribed.</span>
        </div>
		<h2>Therapy Need &amp; Plan of Care</h2>
        <div>
            <span><strong>(M2200) Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)? (Enter zero [&ldquo;000&rdquo;] if no therapy visits indicated.)</strong><%=data.ContainsKey("M2200NumberOfTherapyNeed") ? data["M2200NumberOfTherapyNeed"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2200TherapyNeed") && data["M2200TherapyNeed"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; Not Applicable: No case mix group defined by this assessment.</span>
        </div>
    </div>
</body>
</html>