﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareSupportiveAssistanceForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id) %>
<%= Html.Hidden("StartOfCare_Action", "Edit") %>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("StartOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "StartOfCare") %>
<%= Html.Hidden("categoryType", "SupportiveAssistance")%>
<div class="wrapper main"> 
    <fieldset class="oasis">
        <legend>Patient Living Situation</legend>
        <%= Html.Hidden("StartOfCare_M1100LivingSituation") %>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1100');">(M1100)</a> Patient Living Situation: Which of the following best describes the patient&rsquo;s residential circumstance and availability of assistance? (Check one box only.)</label>
                <table class="form align_center bordergrid">
                    <thead>
                        <tr>
                            <th colspan="3">Living Arrangement</th>
                            <th colspan="5">Availability of Assistance</th>
                        </tr><tr>
                            <th colspan="3"></th>
                            <th>Around the Clock</th>
                            <th>Regular Daytime</th>
                            <th>Regular Nighttime</th>
                            <th>Occasional/ Short-term Assistance</th>
                            <th>No Assistance Available</th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td colspan="3" class="align_left">a. Patient lives alone</td>
                            <td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "01", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "01" ? true : false, new { @id = "M1100LivingSituation01", @class = "radio" }) %>
                                <label for="M1100LivingSituation01">01</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "02", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "02" ? true : false, new { @id = "M1100LivingSituation02", @class = "radio" }) %>
                                <label for="M1100LivingSituation02">02</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "03", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "03" ? true : false, new { @id = "M1100LivingSituation03", @class = "radio" }) %>
                                <label for="M1100LivingSituation03">03</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "04", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "04" ? true : false, new { @id = "M1100LivingSituation04", @class = "radio" }) %>
                                <label for="M1100LivingSituation04">04</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "05", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "05" ? true : false, new { @id = "M1100LivingSituation05", @class = "radio" }) %>
                                <label for="M1100LivingSituation05">05</label>
                            </td>
                        </tr><tr>
                            <td colspan="3" class="align_left">b. Patient lives with other person(s) in the home</td>
                            <td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "06", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "06" ? true : false, new { @id = "M1100LivingSituation06", @class = "radio" }) %>
                                <label for="M1100LivingSituation06">06</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "07", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "07" ? true : false, new { @id = "M1100LivingSituation07", @class = "radio" }) %>
                                <label for="M1100LivingSituation07">07</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "08", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "08" ? true : false, new { @id = "M1100LivingSituation08", @class = "radio" }) %>
                                <label for="M1100LivingSituation08">08</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "09", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "09" ? true : false, new { @id = "M1100LivingSituation09", @class = "radio" }) %>
                                <label for="M1100LivingSituation09">09</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "10", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "10" ? true : false, new { @id = "M1100LivingSituation10", @class = "radio" }) %>
                                <label for="M1100LivingSituation10">10</label>
                            </td>
                        </tr><tr>
                            <td colspan="3" class="align_left">c. Patient lives in congregate situation (e.g., assisted living)</td>
                            <td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "11", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "11" ? true : false, new { @id = "M1100LivingSituation11", @class = "radio" }) %>
                                <label for="M1100LivingSituation11">11</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "12", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "12" ? true : false, new { @id = "M1100LivingSituation12", @class = "radio" }) %>
                                <label for="M1100LivingSituation12">12</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "13", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "13" ? true : false, new { @id = "M1100LivingSituation13", @class = "radio" }) %>
                                <label for="M1100LivingSituation13">13</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "14", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "14" ? true : false, new { @id = "M1100LivingSituation14", @class = "radio" }) %>
                                <label for="M1100LivingSituation14">14</label>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_M1100LivingSituation", "15", data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "15" ? true : false, new { @id = "M1100LivingSituation15", @class = "radio" }) %>
                                <label for="M1100LivingSituation15">15</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1100');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="half float_left">
        <legend>Community Agencies/ Social Service Screening</legend>
        <div class="column">
            <div class="row">
                <table class="form align_center">
                    <thead>
                        <tr>
                            <th colspan="4" class="align_left">Social Service Screening</th>
                            <th>Yes</th>
                            <th>No</th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td colspan="4" class="align_left">
                                Community resource info needed to manage care
                                <%= Html.Hidden("StartOfCare_GenericCommunityResourceInfoNeeded") %>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_GenericCommunityResourceInfoNeeded", "1", data.ContainsKey("GenericCommunityResourceInfoNeeded") && data["GenericCommunityResourceInfoNeeded"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericCommunityResourceInfoNeededYes", @class = "radio" }) %>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_GenericCommunityResourceInfoNeeded", "0", data.ContainsKey("GenericCommunityResourceInfoNeeded") && data["GenericCommunityResourceInfoNeeded"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericCommunityResourceInfoNeededNo", @class = "radio" }) %>
                            </td>
                        </tr><tr>
                            <td colspan="4" class="align_left">
                                Altered affect, e.g., express sadness or anxiety, grief
                                <%= Html.Hidden("StartOfCare_GenericAlteredAffect") %>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_GenericAlteredAffect", "Yes", data.ContainsKey("GenericAlteredAffect") && data["GenericAlteredAffect"].Answer == "Yes" ? true : false, new { @id = "", @class = "radio" }) %>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_GenericAlteredAffect", "No", data.ContainsKey("GenericAlteredAffect") && data["GenericAlteredAffect"].Answer == "No" ? true : false, new { @id = "", @class = "radio" }) %>
                            </td>
                        </tr><tr>
                            <td colspan="4" class="align_left">
                                Suicidal ideation
                                <%= Html.Hidden("StartOfCare_GenericSuicidalIdeation") %>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_GenericSuicidalIdeation", "Yes", data.ContainsKey("GenericSuicidalIdeation") && data["GenericSuicidalIdeation"].Answer == "Yes" ? true : false, new { @id = "", @class = "radio" }) %>
                            </td><td>
                                <%= Html.RadioButton("StartOfCare_GenericSuicidalIdeation", "No", data.ContainsKey("GenericSuicidalIdeation") && data["GenericSuicidalIdeation"].Answer == "No" ? true : false, new { @id = "", @class = "radio" }) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><div class="row">
                <label class="strong">Suspected Abuse/ Neglect</label>
                <input type="hidden" name="StartOfCare_GenericSuspected" value=" " />
                <table class="form">
                    <tbody>
                        <tr>
                            <td><% string[] genericSuspected = data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer != "" ? data["GenericSuspected"].Answer.Split(',') : null; %>
                                <%= string.Format("<input class='radio float_left' id='StartOfCare_GenericSuspected1' type='checkbox' name='StartOfCare_GenericSuspected' value='1' {0} />", genericSuspected != null && genericSuspected.Contains("1") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericSuspected1" class="radio">Unexplained bruises</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' id='StartOfCare_GenericSuspected2' type='checkbox' name='StartOfCare_GenericSuspected' value='2' {0} />", genericSuspected != null && genericSuspected.Contains("2") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericSuspected2" class="radio">Inadequate food</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='StartOfCare_GenericSuspected3' type='checkbox' name='StartOfCare_GenericSuspected' value='3' {0} />", genericSuspected != null && genericSuspected.Contains("3") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericSuspected3" class="radio">Fearful of family member</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' id='StartOfCare_GenericSuspected4' type='checkbox' name='StartOfCare_GenericSuspected' value='4' {0} />", genericSuspected != null && genericSuspected.Contains("4") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericSuspected4" class="radio">Exploitation of funds</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='StartOfCare_GenericSuspected5' type='checkbox' name='StartOfCare_GenericSuspected' value='5' {0} />", genericSuspected != null && genericSuspected.Contains("5") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericSuspected5" class="radio">Sexual abuse</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' id='StartOfCare_GenericSuspected6' type='checkbox' name='StartOfCare_GenericSuspected' value='6' {0} />", genericSuspected != null && genericSuspected.Contains("6") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericSuspected6" class="radio">Neglect</label>
                            </td>
                        </tr><tr>
                            <td colspan="2"><%= string.Format("<input class='radio float_left' id='StartOfCare_GenericSuspected7' type='checkbox' name='StartOfCare_GenericSuspected' value='7' {0} />", genericSuspected != null && genericSuspected.Contains("7") ? "checked='checked'" : "")%><label for="StartOfCare_GenericSuspected7" class="radio">Left unattended if constant supervision is needed</label></td>
                        </tr>
                    </tbody>
                </table>
            </div><div class="row">
                <label for="StartOfCare_GenericMSWIndicatedForWhat" class="float_left">MSW referral indicated for</label>
                <div class="float_right"><%= Html.TextBox("StartOfCare_GenericMSWIndicatedForWhat", data.ContainsKey("GenericMSWIndicatedForWhat") ? data["GenericMSWIndicatedForWhat"].Answer : "", new { @id = "StartOfCare_GenericMSWIndicatedForWhat", @maxlength="150" }) %></div>
                <div class="clear"></div>
                <div class="float_right">
                    <%= Html.Hidden("StartOfCare_GenericMSWIndicatedFor") %>
                    <%= Html.RadioButton("StartOfCare_GenericMSWIndicatedFor", "Yes", data.ContainsKey("GenericMSWIndicatedFor") && data["GenericMSWIndicatedFor"].Answer == "Yes" ? true : false, new { @id = "GenericMSWIndicatedForYes", @class = "radio" }) %>
                    <label for="GenericMSWIndicatedForYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("StartOfCare_GenericMSWIndicatedFor", "No", data.ContainsKey("GenericMSWIndicatedFor") && data["GenericMSWIndicatedFor"].Answer == "No" ? true : false, new { @id = "GenericMSWIndicatedForNo", @class = "radio" }) %>
                    <label for="GenericMSWIndicatedForNo" class="inlineradio">No</label>
                </div>
            </div><div class="row">
                <label class="float_left">Coordinator notified</label>
                <div class="float_right">
                    <%= Html.Hidden("StartOfCare_GenericCoordinatorNotified") %>
                    <%= Html.RadioButton("StartOfCare_GenericCoordinatorNotified", "Yes", data.ContainsKey("GenericCoordinatorNotified") && data["GenericCoordinatorNotified"].Answer == "Yes" ? true : false, new { @id = "GenericCoordinatorNotifiedYes", @class = "radio" }) %>
                    <label for="GenericCoordinatorNotifiedYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("StartOfCare_GenericCoordinatorNotified", "No", data.ContainsKey("GenericCoordinatorNotified") && data["GenericCoordinatorNotified"].Answer == "No" ? true : false, new { @id = "GenericCoordinatorNotifiedNo", @class = "radio" }) %>
                    <label for="GenericCoordinatorNotifiedNo" class="inlineradio">No</label>
                </div>
            </div><div class="row">
                <label class="float_left">Ability of Patient to Handle Finances</label>
                <%= Html.Hidden("StartOfCare_GenericAbilityHandleFinance") %>
                <div class="float_right">
                    <%= Html.RadioButton("StartOfCare_GenericAbilityHandleFinance", "Independent", data.ContainsKey("GenericAbilityHandleFinance") && data["GenericAbilityHandleFinance"].Answer == "Independent" ? true : false, new { @id = "GenericAbilityHandleFinanceIndependent", @class = "radio" }) %>
                    <label for="GenericAbilityHandleFinanceIndependent" class="inlineradio">Independent</label><div class="clear"></div>
                    <%= Html.RadioButton("StartOfCare_GenericAbilityHandleFinance", "Dependent", data.ContainsKey("GenericAbilityHandleFinance") && data["GenericAbilityHandleFinance"].Answer == "Dependent" ? true : false, new { @id = "GenericAbilityHandleFinanceDependent", @class = "radio" }) %>
                    <label for="GenericAbilityHandleFinanceDependent" class="inlineradio">Dependent</label> <div class="clear"></div>
                    <%= Html.RadioButton("StartOfCare_GenericAbilityHandleFinance", "Needs assistance", data.ContainsKey("GenericAbilityHandleFinance") && data["GenericAbilityHandleFinance"].Answer == "Needs assistance" ? true : false, new { @id = "GenericAbilityHandleFinanceAssistance", @class = "radio" }) %>
                    <label for="GenericAbilityHandleFinanceAssistance" class="inlineradio">Needs assistance</label>
                </div>
            </div><div class="row">
                <label for="StartOfCare_GenericAlteredAffectComments" class="strong">Comments</label>
                <%= Html.TextArea("StartOfCare_GenericAlteredAffectComments", data.ContainsKey("GenericAlteredAffectComments") ? data["GenericAlteredAffectComments"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericAlteredAffectComments" }) %>
            </div><div class="row">
                <label for="StartOfCare_GenericOrgProvidingAssistanceNames" class="strong">Supportive Assistance: Names of organizations providing assistance</label>
                <%= Html.TextArea("StartOfCare_GenericOrgProvidingAssistanceNames", data.ContainsKey("GenericOrgProvidingAssistanceNames") ? data["GenericOrgProvidingAssistanceNames"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericOrgProvidingAssistanceNames" }) %>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Safety/ Sanitation Hazards</legend>
        <div class="column">
            <div class="row">
                <label class="strong">Safety/Sanitation Hazards affecting patient: <em>(Select all that apply)</em></label>
                <input type="hidden" name="StartOfCare_GenericNoHazardsIdentified" value="" />
                <table class="form">
                    <tbody>
                        <tr>
                            <td><% string[] genericHazardsIdentified = data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer != "" ? data["GenericHazardsIdentified"].Answer.Split(',') : null; %>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHazardsIdentified0' name='StartOfCare_GenericHazardsIdentified' value='0' {0} />", genericHazardsIdentified != null && genericHazardsIdentified.Contains("0") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericHazardsIdentified0" class="radio">No hazards identified</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHazardsIdentified1' name='StartOfCare_GenericHazardsIdentified' value='1' {0} />", genericHazardsIdentified != null && genericHazardsIdentified.Contains("1") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericHazardsIdentified1" class="radio">Stairs</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHazardsIdentified2' name='StartOfCare_GenericHazardsIdentified' value='2' {0} />", genericHazardsIdentified != null && genericHazardsIdentified.Contains("2") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericHazardsIdentified2" class="radio">Narrow or obstructed walkway</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHazardsIdentified3' name='StartOfCare_GenericHazardsIdentified' value='3' {0} />", genericHazardsIdentified != null && genericHazardsIdentified.Contains("3") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericHazardsIdentified3" class="radio">No gas/ electric appliance</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHazardsIdentified4' name='StartOfCare_GenericHazardsIdentified' value='4' {0} />", genericHazardsIdentified != null && genericHazardsIdentified.Contains("4") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericHazardsIdentified4" class="radio">No running water, plumbing</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHazardsIdentified5' name='StartOfCare_GenericHazardsIdentified' value='5' {0} />", genericHazardsIdentified != null && genericHazardsIdentified.Contains("5") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericHazardsIdentified5" class="radio">Insect/ rodent infestation</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHazardsIdentified6' name='StartOfCare_GenericHazardsIdentified' value='6' {0} />", genericHazardsIdentified != null && genericHazardsIdentified.Contains("6") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericHazardsIdentified6" class="radio">Cluttered/ soiled living area</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHazardsIdentified8' name='StartOfCare_GenericHazardsIdentified' value='8' {0} />", genericHazardsIdentified != null && genericHazardsIdentified.Contains("8") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericHazardsIdentified8" class="radio">Lack of fire safety devices</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHazardsIdentified7' name='StartOfCare_GenericHazardsIdentified' value='7' {0} />", genericHazardsIdentified != null && genericHazardsIdentified.Contains("7") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericHazardsIdentified7" class="radio">Inadequate lighting, heating and cooling</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHazardsIdentified9' name='StartOfCare_GenericHazardsIdentified' value='9' {0} />", genericHazardsIdentified != null && genericHazardsIdentified.Contains("9") ? "checked='checked'" : "")%>
                                <label for="StartOfCare_GenericHazardsIdentified9" class="radio more">Other</label>
                                <div id="StartOfCare_GenericHazardsIdentified9More" class="float_right"><label for="StartOfCare_GenericOtherHazards"><em>(Specify)</em></label><%= Html.TextBox("StartOfCare_GenericOtherHazards", data.ContainsKey("GenericOtherHazards") ? data["GenericOtherHazards"].Answer : "", new { @class = "oe", @id = "StartOfCare_GenericOtherHazards", @maxlength = "30" }) %></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><div class="row">
                <label for="StartOfCare_GenericHazardsComments" class="strong">Comments</label>
                <div><%= Html.TextArea("StartOfCare_GenericHazardsComments", data.ContainsKey("GenericHazardsComments") ? data["GenericHazardsComments"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericHazardsComments" }) %></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float_right">
        <legend>Cultural</legend>
        <div class="column">
            <div class="row">
                <label for="StartOfCare_GenericPrimaryLanguage" class="float_left">Primary language?</label>
                <div class="float_right"><%= Html.TextBox("StartOfCare_GenericPrimaryLanguage", data.ContainsKey("GenericPrimaryLanguage") ? data["GenericPrimaryLanguage"].Answer : "", new { @id = "StartOfCare_GenericPrimaryLanguage" }) %></div>
            </div><div class="row">
                <% string[] useOfInterpreter = data.ContainsKey("GenericUseOfInterpreter") && data["GenericUseOfInterpreter"].Answer != "" ? data["GenericUseOfInterpreter"].Answer.Split(',') : null; %>
                <input type="hidden" name="StartOfCare_GenericUseOfInterpreter" value="" />
                <label class="float_left">Use of interpreter <em>(Select Patient Preferences)</em></label>
                <div class="float_right">
                    <div class="float_left">
                        <%= string.Format("<input class='radio' type='checkbox' id='StartOfCare_GenericUseOfInterpreter1' name='StartOfCare_GenericUseOfInterpreter' value='1' {0} />", useOfInterpreter!=null && useOfInterpreter.Contains("1") ? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericUseOfInterpreter1" class="fixed inlineradio">Family</label>
                    </div><div class="float_left">
                        <%= string.Format("<input class='radio' type='checkbox' id='StartOfCare_GenericUseOfInterpreter2' name='StartOfCare_GenericUseOfInterpreter' value='2' {0} />", useOfInterpreter!=null && useOfInterpreter.Contains("2") ? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericUseOfInterpreter2" class="fixed inlineradio">Friend</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input class='radio' type='checkbox' id='StartOfCare_GenericUseOfInterpreter3' name='StartOfCare_GenericUseOfInterpreter' value='3' {0} />", useOfInterpreter!=null && useOfInterpreter.Contains("3") ? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericUseOfInterpreter3" class="fixed inlineradio">Professional</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input class='radio' type='checkbox' id='StartOfCare_GenericUseOfInterpreter4' name='StartOfCare_GenericUseOfInterpreter' value='4' {0} />", useOfInterpreter!=null && useOfInterpreter.Contains("4") ? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericUseOfInterpreter4" class="fixed inlineradio">Other</label>
                        <div id="StartOfCare_GenericUseOfInterpreter4More" class="float_right"><label for="StartOfCare_GenericUseOfInterpreterOtherDetails"><em>(Specify)</em></label><%= Html.TextBox("StartOfCare_GenericUseOfInterpreterOtherDetails", data.ContainsKey("GenericUseOfInterpreterOtherDetails") ? data["GenericUseOfInterpreterOtherDetails"].Answer : "", new { @class = "oe", @id = "StartOfCare_GenericUseOfInterpreterOtherDetails", @maxlength = "20" }) %></div>
                    </div>
                </div>
            </div><div class="row">
                <label class="float_left">Does patient have cultural practices that influence health care?</label>
                <div class="float_right">
                    <%= Html.Hidden("StartOfCare_GenericCulturalPractices") %>
                    <%= Html.RadioButton("StartOfCare_GenericCulturalPractices", "1", data.ContainsKey("GenericCulturalPractices") && data["GenericCulturalPractices"].Answer == "1" ? true : false, new { @id = "StartOfCare_GenericCulturalPracticesYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericCulturalPracticesYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("StartOfCare_GenericCulturalPractices", "0", data.ContainsKey("GenericCulturalPractices") && data["GenericCulturalPractices"].Answer == "0" ? true : false, new { @id = "StartOfCare_GenericCulturalPracticesNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericCulturalPracticesNo" class="inlineradio">No</label>
                </div>
                <div class="clear"></div>
                <div id="StartOfCare_GenericCulturalPracticesYesMore">
                    <label for="StartOfCare_GenericCulturalPracticesDetails">Please explain:</label>
                    <%= Html.TextArea("StartOfCare_GenericCulturalPracticesDetails", data.ContainsKey("GenericCulturalPracticesDetails") ? data["GenericCulturalPracticesDetails"].Answer : "", 5, 70, new { @id = "StartOfCare_GenericCulturalPracticesDetails" }) %>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Homebound</legend>
        <div class="wide_column">
            <div class="row">
                <label class="float_left">Is The Patient Homebound?</label>
                <div class="float_left margin">
                    <%= Html.Hidden("StartOfCare_GenericIsHomeBound")%>
                    <%= Html.RadioButton("StartOfCare_GenericIsHomeBound", "Yes", data.ContainsKey("GenericIsHomeBound") && data["GenericIsHomeBound"].Answer == "Yes" ? true : false, new { @id = "StartOfCare_GenericIsHomeBoundYes", @class = "radio" })%>
                    <label for="StartOfCare_GenericIsHomeBoundYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("StartOfCare_GenericIsHomeBound", "No", data.ContainsKey("GenericIsHomeBound") && data["GenericIsHomeBound"].Answer == "No" ? true : false, new { @id = "StartOfCare_GenericIsHomeBoundNo", @class = "radio" })%>
                    <label for="StartOfCare_GenericIsHomeBoundNo" class="inlineradio">No</label>
                </div>
                <% string[] homeBoundReason = data.ContainsKey("GenericHomeBoundReason") && data["GenericHomeBoundReason"].Answer != "" ? data["GenericHomeBoundReason"].Answer.Split(',') : null; %>
                <input type="hidden" name="StartOfCare_GenericHomeBoundReason" value="" />
            </div><div class="HomeBound row">
                <div class="column">
                    <div class="row">
                        <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHomeBoundReason1' name='StartOfCare_GenericHomeBoundReason' value='1' {0} />", homeBoundReason!=null && homeBoundReason.Contains("1") ? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericHomeBoundReason1" class="radio">Exhibits considerable &amp; taxing effort to leave home</label>
                        <div class="clear"></div>
                        <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHomeBoundReason5' name='StartOfCare_GenericHomeBoundReason' value='5' {0} />", homeBoundReason!=null && homeBoundReason.Contains("5") ? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericHomeBoundReason5" class="radio">Unsafe to leave home due to cognitive or psychiatric impairments</label>
                        <div class="clear"></div>
                        <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHomeBoundReason4' name='StartOfCare_GenericHomeBoundReason' value='4' {0} />", homeBoundReason!=null && homeBoundReason.Contains("4") ? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericHomeBoundReason4" class="radio">Unable to safely leave home unassisted</label>
                        <div class="clear"></div>
                        <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHomeBoundReason7' name='StartOfCare_GenericHomeBoundReason' value='7' {0} />", homeBoundReason!=null && homeBoundReason.Contains("7") ? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericHomeBoundReason7" class="inlineradio fixed">Other</label>
                        <div id="StartOfCare_GenericHomeBoundReason7More" class="float_right"><label for="StartOfCare_GenericOtherHomeBoundDetails"><em>(Specify)</em></label><%= Html.TextBox("StartOfCare_GenericOtherHomeBoundDetails", data.ContainsKey("GenericOtherHomeBoundDetails") ? data["GenericOtherHomeBoundDetails"].Answer : "", new { @id = "StartOfCare_GenericOtherHomeBoundDetails",@maxlength = "60" }) %></div>
                    </div>
                </div><div class="column">
                    <div class="row">
                        <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHomeBoundReason3' name='StartOfCare_GenericHomeBoundReason' value='3' {0} />", homeBoundReason!=null && homeBoundReason.Contains("3") ? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericHomeBoundReason3" class="radio">Severe Dyspnea</label>
                        <div class="clear"></div>
                        <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHomeBoundReason2' name='StartOfCare_GenericHomeBoundReason' value='2' {0} />", homeBoundReason!=null && homeBoundReason.Contains("2") ? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericHomeBoundReason2" class="radio">Requires the assistance of another to get up and move safely</label>
                        <div class="clear"></div>
                        <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_GenericHomeBoundReason6' name='StartOfCare_GenericHomeBoundReason' value='6' {0} />", homeBoundReason!=null && homeBoundReason.Contains("6") ? "checked='checked'" : "" ) %>
                        <label for="StartOfCare_GenericHomeBoundReason6" class="radio">Unable to leave home due to medical restriction(s)</label>
                    </div>
                </div>
            <div class="clear"></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="loc485">
        <legend>Safety Measures (Locator #15)</legend>
        <% string[] safetyMeasure = data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer != "" ? data["485SafetyMeasures"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485SafetyMeasures" value="" />
        <div class="wide_column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures1' name='StartOfCare_485SafetyMeasures' value='1' {0} />", safetyMeasure!=null && safetyMeasure.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures1" class="radio">Anticoagulant Precautions</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures2' name='StartOfCare_485SafetyMeasures' value='2' {0} />", safetyMeasure!=null && safetyMeasure.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures2" class="radio">Emergency Plan Developed</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures3' name='StartOfCare_485SafetyMeasures' value='3' {0} />", safetyMeasure!=null && safetyMeasure.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures3" class="radio">Fall Precautions</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures4' name='StartOfCare_485SafetyMeasures' value='4' {0} />", safetyMeasure!=null && safetyMeasure.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures4" class="radio">Keep Pathway Clear</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures5' name='StartOfCare_485SafetyMeasures' value='5' {0} />", safetyMeasure!=null && safetyMeasure.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures5" class="radio">Keep Side Rails Up</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures6' name='StartOfCare_485SafetyMeasures' value='6' {0} />", safetyMeasure!=null && safetyMeasure.Contains("6") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures6" class="radio">Neutropenic Precautions</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures7' name='StartOfCare_485SafetyMeasures' value='7' {0} />", safetyMeasure!=null && safetyMeasure.Contains("7") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures7" class="radio">O<sub>2</sub> Precautions</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures8' name='StartOfCare_485SafetyMeasures' value='8' {0} />", safetyMeasure!=null && safetyMeasure.Contains("8") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures8" class="radio">Proper Position During Meals</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures9' name='StartOfCare_485SafetyMeasures' value='9' {0} />", safetyMeasure!=null && safetyMeasure.Contains("9") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures9" class="radio">Safety in ADLs</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures10' name='StartOfCare_485SafetyMeasures' value='10' {0} />", safetyMeasure!=null && safetyMeasure.Contains("10") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures10" class="radio">Seizure Precautions</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures11' name='StartOfCare_485SafetyMeasures' value='11' {0} />", safetyMeasure!=null && safetyMeasure.Contains("11") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures11" class="radio">Sharps Safety</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures12' name='StartOfCare_485SafetyMeasures' value='12' {0} />", safetyMeasure!=null && safetyMeasure.Contains("12") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures12" class="radio">Slow Position Change</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures13' name='StartOfCare_485SafetyMeasures' value='13' {0} />", safetyMeasure!=null && safetyMeasure.Contains("13") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures13" class="radio">Standard Precautions/ Infection Control</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures14' name='StartOfCare_485SafetyMeasures' value='14' {0} />", safetyMeasure!=null && safetyMeasure.Contains("14") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures14" class="radio">Support During Transfer and Ambulation</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures15' name='StartOfCare_485SafetyMeasures' value='15' {0} />", safetyMeasure!=null && safetyMeasure.Contains("15") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures15" class="radio">Use of Assistive Devices</label></td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures16' name='StartOfCare_485SafetyMeasures' value='16' {0} />", safetyMeasure!=null && safetyMeasure.Contains("16") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures16" class="radio">Instructed on safe utilities management</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures17' name='StartOfCare_485SafetyMeasures' value='17' {0} />", safetyMeasure!=null && safetyMeasure.Contains("17") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures17" class="radio">Instructed on mobility safety</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures18' name='StartOfCare_485SafetyMeasures' value='18' {0} />", safetyMeasure!=null && safetyMeasure.Contains("18") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures18" class="radio">Instructed on DME &amp; electrical safety</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures19' name='StartOfCare_485SafetyMeasures' value='19' {0} />", safetyMeasure!=null && safetyMeasure.Contains("19") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures19" class="radio">Instructed on sharps container</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures20' name='StartOfCare_485SafetyMeasures' value='20' {0} />", safetyMeasure!=null && safetyMeasure.Contains("20") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures20" class="radio">Instructed on medical gas</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures21' name='StartOfCare_485SafetyMeasures' value='21' {0} />", safetyMeasure!=null && safetyMeasure.Contains("21") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures21" class="radio">Instructed on disaster/ emergency plan</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures22' name='StartOfCare_485SafetyMeasures' value='22' {0} />", safetyMeasure!=null && safetyMeasure.Contains("22") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures22" class="radio">Instructed on safety measures</label>
                            </td><td colspan="2">
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='StartOfCare_485SafetyMeasures23' name='StartOfCare_485SafetyMeasures' value='23' {0} />", safetyMeasure!=null && safetyMeasure.Contains("23") ? "checked='checked'" : "" ) %>
                                <label for="StartOfCare_485SafetyMeasures23" class="radio">Instructed on proper handling of biohazard waste</label>
                            </td>
                        </tr><tr>
                            <td colspan="3">
                                <label for="StartOfCare_485TriageEmergencyCode">Emergency Triage Code</label>
                                <%  var triageEmergencyCode = new SelectList(new[] {
                                        new SelectListItem { Text = "", Value = "0" },
                                        new SelectListItem { Text = "1", Value = "1" },
                                        new SelectListItem { Text = "2", Value = "2" },
                                        new SelectListItem { Text = "3", Value = "3" },
                                        new SelectListItem { Text = "4", Value = "4" }
                                    }, "Value", "Text", data.ContainsKey("485TriageEmergencyCode") && data["485TriageEmergencyCode"].Answer != "" ? data["485TriageEmergencyCode"].Answer : "0"); %>
                                <%= Html.DropDownList("StartOfCare_485TriageEmergencyCode", triageEmergencyCode)%>
                            </td>
                        </tr><tr>
                            <td colspan="3">
                                <label for="StartOfCare_485OtherSafetyMeasures">Other (Specify)</label>
                                <%= Html.TextArea("StartOfCare_485OtherSafetyMeasures", data.ContainsKey("485OtherSafetyMeasures") ? data["485OtherSafetyMeasures"].Answer : "", 5, 70, new { @id = "StartOfCare_485OtherSafetyMeasures" }) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Interventions</legend>
        <%string[] supportiveAssistanceInterventions = data.ContainsKey("485SupportiveAssistanceInterventions") && data["485SupportiveAssistanceInterventions"].Answer != "" ? data["485SupportiveAssistanceInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="StartOfCare_485SupportiveAssistanceInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='StartOfCare_485SupportiveAssistanceInterventions1' class='radio float_left' name='StartOfCare_485SupportiveAssistanceInterventions' value='1' type='checkbox' {0} />", supportiveAssistanceInterventions != null && supportiveAssistanceInterventions.Contains("1") ? "checked='checked'" : "")%>
                <label for="StartOfCare_485SupportiveAssistanceInterventions1" class="radio">MSW to assess psychosocial needs, environment and assist with community referrals and resources.</label>
            </div><div class="row">
                <label for="StartOfCare_485SupportiveAssistanceOrderTemplates">Additional Orders:</label>
                <%  var supportiveAssistanceOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485SupportiveAssistanceOrderTemplates") && data["485SupportiveAssistanceOrderTemplates"].Answer != "" ? data["485SupportiveAssistanceOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485SupportiveAssistanceOrderTemplates", supportiveAssistanceOrderTemplates)%>
                <%=Html.TextArea("StartOfCare_485SupportiveAssistanceComments", data.ContainsKey("485SupportiveAssistanceComments") ? data["485SupportiveAssistanceComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485SupportiveAssistanceComments" })%>
            </div>
        </div>
    </fieldset> 
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"SOC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('StartOfCare_ValidationContainer','{0}','{1}','{2}','StartOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfChecked($("#StartOfCare_GenericHazardsIdentified9"), $("#StartOfCare_GenericHazardsIdentified9More"));
    Oasis.showIfChecked($("#StartOfCare_GenericUseOfInterpreter4"), $("#StartOfCare_GenericUseOfInterpreter4More"));
    Oasis.showIfRadioEquals("StartOfCare_GenericCulturalPractices", "1", $("#StartOfCare_GenericCulturalPracticesYesMore"));
    Oasis.showIfRadioEquals("StartOfCare_GenericIsHomeBound", "Yes", $("#window_startofcare .HomeBound"));
    Oasis.showIfChecked($("#StartOfCare_GenericHomeBoundReason7"), $("#StartOfCare_GenericHomeBoundReason7More"));
</script>
