﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.span("Allergies:",true) +
            printview.span("<%= data != null && data.ContainsKey("485Allergies") && data["485Allergies"].Answer.IsNotNullOrEmpty() ? (data["485Allergies"].Answer == "No" ? "No Known Allergies" : (data.ContainsKey("485AllergiesDescription") && data["485AllergiesDescription"].Answer.IsNotNullOrEmpty() ? data["485AllergiesDescription"].Answer : "Not Specified")) : "<span class='blank'></span>"%>")));
    printview.addsection(
        printview.col(4,
            printview.span("Apical Pulse:",true) +
            printview.col(3,
                printview.span("<%= data != null && data.ContainsKey("GenericPulseApical") && data["GenericPulseApical"].Answer.IsNotNullOrEmpty() ? data["GenericPulseApical"].Answer : ""%>",false,1) +
                printview.checkbox("Reg",<%= data != null && data.ContainsKey("GenericPulseApicalRegular") && data["GenericPulseApicalRegular"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("Irreg",<%= data != null && data.ContainsKey("GenericPulseApicalRegular") && data["GenericPulseApicalRegular"].Answer == "2" ? "true" : "false"%>)) +
            printview.span("Radial Pulse:",true) +
            printview.col(3,
                printview.span("<%= data != null && data.ContainsKey("GenericPulseRadial") && data["GenericPulseRadial"].Answer.IsNotNullOrEmpty() ? data["GenericPulseRadial"].Answer : ""%>",false,1) +
                printview.checkbox("Reg",<%= data != null && data.ContainsKey("GenericPulseRadialRegular") && data["GenericPulseRadialRegular"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("Irreg",<%= data != null && data.ContainsKey("GenericPulseRadialRegular") && data["GenericPulseRadialRegular"].Answer == "2" ? "true" : "false"%>)) +
            printview.span("<strong>Height:</strong><%= data != null && data.ContainsKey("GenericHeight") && data["GenericHeight"].Answer.IsNotNullOrEmpty() ? data["GenericHeight"].Answer : "<span class='blank'></span>"%>") +
            printview.span("<strong>Weight:</strong><%= data != null && data.ContainsKey("GenericWeight") && data["GenericWeight"].Answer.IsNotNullOrEmpty() ? data["GenericWeight"].Answer : "<span class='blank'></span>"%> <%= data != null && data.ContainsKey("GenericWeightActualStated") && data["GenericWeightActualStated"].Answer == "1" ? "(Actual)" : ""%><%= data != null && data.ContainsKey("GenericWeightActualStated") && data["GenericWeightActualStated"].Answer == "2" ? "(Stated)" : ""%>") +
            printview.span("<strong>Temp</strong><%= data != null && data.ContainsKey("GenericTemp") && data["GenericTemp"].Answer.IsNotNullOrEmpty() ? data["GenericTemp"].Answer : "<span class='blank'></span>"%>") +
            printview.span("<strong>Resp</strong><%= data != null && data.ContainsKey("GenericResp") && data["GenericResp"].Answer.IsNotNullOrEmpty() ? data["GenericResp"].Answer : "<span class='blank'></span>"%>") +
            printview.span("BP",true) +
            printview.span("Lying",true) +
            printview.span("Sitting",true) +
            printview.span("Standing",true) +
            printview.span("Left",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericBPLeftLying") && data["GenericBPLeftLying"].Answer.IsNotNullOrEmpty() ? data["GenericBPLeftLying"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBPLeftSitting") && data["GenericBPLeftSitting"].Answer.IsNotNullOrEmpty() ? data["GenericBPLeftSitting"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBPLeftStanding") && data["GenericBPLeftStanding"].Answer.IsNotNullOrEmpty() ? data["GenericBPLeftStanding"].Answer : ""%>",false,1) +
            printview.span("Right",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericBPRightLying") && data["GenericBPRightLying"].Answer.IsNotNullOrEmpty() ? data["GenericBPRightLying"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBPRightSitting") && data["GenericBPRightSitting"].Answer.IsNotNullOrEmpty() ? data["GenericBPRightSitting"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBPRightStanding") && data["GenericBPRightStanding"].Answer.IsNotNullOrEmpty() ? data["GenericBPRightStanding"].Answer : ""%>",false,1)),
        "Vital Signs");
    printview.addsection(
        printview.col(6,
            printview.span("Temperature:",true) +
            printview.span(" &gt;<%= data != null && data.ContainsKey("GenericTempGreaterThan") && data["GenericTempGreaterThan"].Answer.IsNotNullOrEmpty() ? data["GenericTempGreaterThan"].Answer : "<span class='blank short'></span>" %> or &lt;<%= data != null && data.ContainsKey("GenericTempLessThan") && data["GenericTempLessThan"].Answer.IsNotNullOrEmpty() ? data["GenericTempLessThan"].Answer : "<span class='blank short'></span>" %>") +
            printview.span("Pulse:",true) +
            printview.span(" &gt;<%= data != null && data.ContainsKey("GenericPulseGreaterThan") && data["GenericPulseGreaterThan"].Answer.IsNotNullOrEmpty() ? data["GenericPulseGreaterThan"].Answer : "<span class='blank short'></span>" %> or &lt;<%= data != null && data.ContainsKey("GenericPulseLessThan") && data["GenericPulseLessThan"].Answer.IsNotNullOrEmpty() ? data["GenericPulseLessThan"].Answer : "<span class='blank short'></span>" %>") +
            printview.span("Respirations:",true) +
            printview.span(" &gt;<%= data != null && data.ContainsKey("GenericRespirationGreaterThan") && data["GenericRespirationGreaterThan"].Answer.IsNotNullOrEmpty() ? data["GenericRespirationGreaterThan"].Answer : "<span class='blank short'></span>" %> or &lt;<%= data != null && data.ContainsKey("GenericRespirationLessThan") && data["GenericRespirationLessThan"].Answer.IsNotNullOrEmpty() ? data["GenericRespirationLessThan"].Answer : "<span class='blank short'></span>" %>") +
            printview.span("Systolic BP:",true) +
            printview.span(" &gt;<%= data != null && data.ContainsKey("GenericSystolicBPGreaterThan") && data["GenericSystolicBPGreaterThan"].Answer.IsNotNullOrEmpty() ? data["GenericSystolicBPGreaterThan"].Answer : "<span class='blank short'></span>" %> or &lt;<%= data != null && data.ContainsKey("GenericSystolicBPLessThan") && data["GenericSystolicBPLessThan"].Answer.IsNotNullOrEmpty() ? data["GenericSystolicBPLessThan"].Answer : "<span class='blank short'></span>" %>") +
            printview.span("Diastolic BP:",true) +
            printview.span(" &gt;<%= data != null && data.ContainsKey("GenericDiastolicBPGreaterThan") && data["GenericDiastolicBPGreaterThan"].Answer.IsNotNullOrEmpty() ? data["GenericDiastolicBPGreaterThan"].Answer : "<span class='blank short'></span>" %> or &lt;<%= data != null && data.ContainsKey("GenericDiastolicBPLessThan") && data["GenericDiastolicBPLessThan"].Answer.IsNotNullOrEmpty() ? data["GenericDiastolicBPLessThan"].Answer : "<span class='blank short'></span>" %>") +
            printview.span("O<sub>2</sub> Sat (percent):",true) +
            printview.span(" &lt;<%= data != null && data.ContainsKey("Generic02SatLessThan") && data["Generic02SatLessThan"].Answer.IsNotNullOrEmpty() ? data["Generic02SatLessThan"].Answer : "<span class='blank short'></span>" %>") +
            printview.span("Fasting Blood Sugar:",true) +
            printview.span(" &gt;<%= data != null && data.ContainsKey("GenericFastingBloodSugarGreaterThan") && data["GenericFastingBloodSugarGreaterThan"].Answer.IsNotNullOrEmpty() ? data["GenericFastingBloodSugarGreaterThan"].Answer : "<span class='blank short'></span>" %> or &lt;<%= data != null && data.ContainsKey("GenericFastingBloodSugarLessThan") && data["GenericFastingBloodSugarLessThan"].Answer.IsNotNullOrEmpty() ? data["GenericFastingBloodSugarLessThan"].Answer : "<span class='blank short'></span>" %>") +
            printview.span("Random Blood Sugar",true) +
            printview.span(" &gt;<%= data != null && data.ContainsKey("GenericRandomBloddSugarGreaterThan") && data["GenericRandomBloddSugarGreaterThan"].Answer.IsNotNullOrEmpty() ? data["GenericRandomBloddSugarGreaterThan"].Answer : "<span class='blank short'></span>" %> or &lt;<%= data != null && data.ContainsKey("GenericRandomBloodSugarLessThan") && data["GenericRandomBloodSugarLessThan"].Answer.IsNotNullOrEmpty() ? data["GenericRandomBloodSugarLessThan"].Answer : "<span class='blank short'></span>" %>") +
            printview.span("Weight (lbs/week):",true) +
            printview.span(" &gt;<%= data != null && data.ContainsKey("GenericWeightGreaterThan") && data["GenericWeightGreaterThan"].Answer.IsNotNullOrEmpty() ? data["GenericWeightGreaterThan"].Answer : "<span class='blank short'></span>" %> or &lt;<%= data != null && data.ContainsKey("GenericWeightLessThan") && data["GenericWeightLessThan"].Answer.IsNotNullOrEmpty() ? data["GenericWeightLessThan"].Answer : "<span class='blank short'></span>" %>")),
        "Vital Sign Parameters");
    printview.addsection(
        printview.col(6,
            printview.span("Pneumonia",true) +
            printview.span("<%= data != null && data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer.IsNotNullOrEmpty() ? data["485Pnemonia"].Answer : "" %>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("485PnemoniaDate") && data["485PnemoniaDate"].Answer.IsNotNullOrEmpty() ? data["485PnemoniaDate"].Answer : "" %>",false,1) +
            printview.span("Flu",true) +
            printview.span("<%= data != null && data.ContainsKey("485Flu") && data["485Flu"].Answer.IsNotNullOrEmpty() ? data["485Flu"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("485FluDate") && data["485FluDate"].Answer.IsNotNullOrEmpty() ? data["485FluDate"].Answer : ""%>",false,1) +
            printview.span("TB",true) +
            printview.span("<%= data != null && data.ContainsKey("485TB") && data["485TB"].Answer.IsNotNullOrEmpty() ? data["485TB"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("485TBDate") && data["485TBDate"].Answer.IsNotNullOrEmpty() ? data["485TBDate"].Answer : ""%>",false,1) +
            printview.span("TB Exposure",true) +
            printview.span("<%= data != null && data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer.IsNotNullOrEmpty() ? data["485TBExposure"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("485TBExposureDate") && data["485TBExposureDate"].Answer.IsNotNullOrEmpty() ? data["485TBExposureDate"].Answer : ""%>",false,1) +
            <%= data != null && data.ContainsKey("485AdditionalImmunization1Name") && data["485AdditionalImmunization1Name"].Answer.IsNotNullOrEmpty() ?
                "printview.span(\"" + data["485AdditionalImmunization1Name"].Answer + "\",true,1) +" +
                "printview.span(\"" + (data.ContainsKey("485AdditionalImmunization1") ? data["485AdditionalImmunization1"].Answer : "") + "\",false,1) +" +
                "printview.span(\"" + (data.ContainsKey("485AdditionalImmunization1Date") ? data["485AdditionalImmunization1Date"].Answer : "") + "\",false,1) +" : "\"\" + " %>
            <%= data != null && data.ContainsKey("485AdditionalImmunization2Name") && data["485AdditionalImmunization2Name"].Answer.IsNotNullOrEmpty() ? 
                "printview.span(\"" + data["485AdditionalImmunization2Name"].Answer + "\",true,1) +" +
                "printview.span(\"" + (data.ContainsKey("485AdditionalImmunization2") ? data["485AdditionalImmunization2"].Answer : "") + "\",false,1) +" +
                "printview.span(\"" + (data.ContainsKey("485AdditionalImmunization2Date") ? data["485AdditionalImmunization2Date"].Answer : "") + "\",false,1)" : "\"\"" %>
        ) +
        printview.span("Comments:",true) +
        printview.span("<%= data != null && data.ContainsKey("485ImmunizationComments") && data["485ImmunizationComments"].Answer.IsNotNullOrEmpty() ? data["485ImmunizationComments"].Answer : ""%>",false,2)
        ,"Immunizations");
    printview.addsection(
        printview.span("(M1000) From which of the following Inpatient Facilities was the patient discharged during the past 14 days? (Mark all that apply)",true) +
        printview.col(2,
            printview.checkbox("1 &ndash; Long-term nursing facility (NF)",<%= data != null && data.ContainsKey("M1000InpatientFacilitiesLTC") && data["M1000InpatientFacilitiesLTC"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Skilled nursing facility (SNF/ TCU)",<%= data != null && data.ContainsKey("M1000InpatientFacilitiesSNF") && data["M1000InpatientFacilitiesSNF"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Short-stay acute hospital (IPPS)",<%= data != null && data.ContainsKey("M1000InpatientFacilitiesIPPS") && data["M1000InpatientFacilitiesIPPS"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Long-term care hospital (LTCH)",<%= data != null && data.ContainsKey("M1000InpatientFacilitiesLTCH") && data["M1000InpatientFacilitiesLTCH"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; Inpatient rehabilitation hospital or unit (IRF)",<%= data != null && data.ContainsKey("M1000InpatientFacilitiesIRF") && data["M1000InpatientFacilitiesIRF"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("6 &ndash; Psychiatric hospital or unit",<%= data != null && data.ContainsKey("M1000InpatientFacilitiesPhych") && data["M1000InpatientFacilitiesPhych"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("7 &ndash; Other (specify) <%= data != null && data.ContainsKey("M1000InpatientFacilitiesOther") && data["M1000InpatientFacilitiesOther"].Answer.IsNotNullOrEmpty() ? data["M1000InpatientFacilitiesOther"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("M1000InpatientFacilitiesOTHR") && data["M1000InpatientFacilitiesOTHR"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("NA &ndash; Patient was not discharged from an inpatient facility",<%= data != null && data.ContainsKey("M1000InpatientFacilitiesNone") && data["M1000InpatientFacilitiesNone"].Answer == "1" ? "true" : "false"%>)
        ));<%
    if (data == null || data.ContainsKey("M1000InpatientFacilitiesNone") == false || data["M1000InpatientFacilitiesNone"].Answer != "1") { %>
        printview.addsection(
            printview.span("(M1005) Inpatient Discharge Date (most recent)",true) +
            printview.span("<%= data != null && data.ContainsKey("M1005InpatientDischargeDateUnknown") && data["M1005InpatientDischargeDateUnknown"].Answer == "1" ? "UK &ndash; Unknown" : (data != null && data.ContainsKey("M1005InpatientDischargeDate") && data["M1005InpatientDischargeDate"].Answer.IsNotNullOrEmpty() ? data["M1005InpatientDischargeDate"].Answer : "" ) %>",false,1));
        printview.addsection(
            printview.span("(M1010) List each Inpatient Diagnosis and ICD-9-C M code at the level of highest specificity for only those conditions treated during an inpatient stay within the last 14 days",true) +
            "%3Cspan class=%22float_left%22%3Ea.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosis1") && data["M1010InpatientFacilityDiagnosis1"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosis1"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosisCode1") && data["M1010InpatientFacilityDiagnosisCode1"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosisCode1"].Answer : "" %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22float_left%22%3Eb.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosis2") && data["M1010InpatientFacilityDiagnosis2"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosis2"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosisCode2") && data["M1010InpatientFacilityDiagnosisCode2"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosisCode2"].Answer : "" %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22float_left%22%3Ec.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosis3") && data["M1010InpatientFacilityDiagnosis3"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosis3"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosisCode3") && data["M1010InpatientFacilityDiagnosisCode3"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosisCode3"].Answer : "" %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22float_left%22%3Ed.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosis4") && data["M1010InpatientFacilityDiagnosis4"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosis4"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosisCode4") && data["M1010InpatientFacilityDiagnosisCode4"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosisCode4"].Answer : "" %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22float_left%22%3Ee.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosis5") && data["M1010InpatientFacilityDiagnosis5"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosis5"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosisCode5") && data["M1010InpatientFacilityDiagnosisCode5"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosisCode5"].Answer : "" %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22float_left%22%3Ef.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosis6") && data["M1010InpatientFacilityDiagnosis6"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosis6"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1010InpatientFacilityDiagnosisCode6") && data["M1010InpatientFacilityDiagnosisCode6"].Answer.IsNotNullOrEmpty() ? data["M1010InpatientFacilityDiagnosisCode6"].Answer : "" %>",false,1)) +
            "%3C/span%3E");<%
        if (data == null || data.ContainsKey("M1012InpatientFacilityProcedureCodeNotApplicable") == false || data["M1012InpatientFacilityProcedureCodeNotApplicable"].Answer != "1") { %>
            printview.addsection(
                printview.span("(M1012) List each Inpatient Procedure and the associated ICD-9-C M procedure code relevant to the plan of care.",true) +<%
            if (data != null && data.ContainsKey("M1012InpatientFacilityProcedureCodeUnknown") && data["M1012InpatientFacilityProcedureCodeUnknown"].Answer == "1") { %>
                printview.span("UK &ndash; Unknown"));<%
            } else { %>
                "%3Cspan class=%22float_left%22%3Ea.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
                printview.col(2,
                    printview.span("<%= data != null && data.ContainsKey("M1012InpatientFacilityProcedure1") && data["M1012InpatientFacilityProcedure1"].Answer.IsNotNullOrEmpty() ? data["M1012InpatientFacilityProcedure1"].Answer : "" %>",false,1) +
                    printview.span("<%= data != null && data.ContainsKey("M1012InpatientFacilityProcedureCode1") && data["M1012InpatientFacilityProcedureCode1"].Answer.IsNotNullOrEmpty() ? data["M1012InpatientFacilityProcedureCode1"].Answer : "" %>",false,1)) +
                "%3C/span%3E%3Cspan class=%22float_left%22%3Eb.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
                printview.col(2,
                    printview.span("<%= data != null && data.ContainsKey("M1012InpatientFacilityProcedure2") && data["M1012InpatientFacilityProcedure2"].Answer.IsNotNullOrEmpty() ? data["M1012InpatientFacilityProcedure2"].Answer : "" %>",false,1) +
                    printview.span("<%= data != null && data.ContainsKey("M1012InpatientFacilityProcedureCode2") && data["M1012InpatientFacilityProcedureCode2"].Answer.IsNotNullOrEmpty() ? data["M1012InpatientFacilityProcedureCode2"].Answer : "" %>",false,1)) +
                "%3C/span%3E%3Cspan class=%22float_left%22%3Ec.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
                printview.col(2,
                    printview.span("<%= data != null && data.ContainsKey("M1012InpatientFacilityProcedure3") && data["M1012InpatientFacilityProcedure3"].Answer.IsNotNullOrEmpty() ? data["M1012InpatientFacilityProcedure3"].Answer : "" %>",false,1) +
                    printview.span("<%= data != null && data.ContainsKey("M1012InpatientFacilityProcedureCode3") && data["M1012InpatientFacilityProcedureCode3"].Answer.IsNotNullOrEmpty() ? data["M1012InpatientFacilityProcedureCode3"].Answer : "" %>",false,1)) +
                "%3C/span%3E%3Cspan class=%22float_left%22%3Ed.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
                printview.col(2,
                    printview.span("<%= data != null && data.ContainsKey("M1012InpatientFacilityProcedure4") && data["M1012InpatientFacilityProcedure4"].Answer.IsNotNullOrEmpty() ? data["M1012InpatientFacilityProcedure4"].Answer : "" %>",false,1) +
                    printview.span("<%= data != null && data.ContainsKey("M1012InpatientFacilityProcedureCode4") && data["M1012InpatientFacilityProcedureCode4"].Answer.IsNotNullOrEmpty() ? data["M1012InpatientFacilityProcedureCode4"].Answer : "" %>",false,1)) +
                "%3C/span%3E");<%
            }
        }
    }
    if (data == null || data.ContainsKey("M1016MedicalRegimenDiagnosisNotApplicable") == false || data["M1016MedicalRegimenDiagnosisNotApplicable"].Answer != "1") { %>
        printview.addsection(
            printview.span("(M1016) Diagnoses Requiring Medical or Treatment Regimen Change Within Past 14 Days: List the patient&rsquo;s Medical Diagnoses and ICD-9-C M codes at the level of highest specificity for those conditions requiring changed medical or treatment regimen within the past 14 days",true) +
            "%3Cspan class=%22float_left%22%3Ea.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosis1") && data["M1016MedicalRegimenDiagnosis1"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosis1"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosisCode1") && data["M1016MedicalRegimenDiagnosisCode1"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosisCode1"].Answer : "" %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22float_left%22%3Eb.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosis2") && data["M1016MedicalRegimenDiagnosis2"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosis2"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosisCode2") && data["M1016MedicalRegimenDiagnosisCode2"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosisCode2"].Answer : "" %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22float_left%22%3Ec.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosis3") && data["M1016MedicalRegimenDiagnosis3"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosis3"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosisCode3") && data["M1016MedicalRegimenDiagnosisCode3"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosisCode3"].Answer : "" %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22float_left%22%3Ed.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosis4") && data["M1016MedicalRegimenDiagnosis4"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosis4"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosisCode4") && data["M1016MedicalRegimenDiagnosisCode4"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosisCode4"].Answer : "" %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22float_left%22%3Ee.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosis5") && data["M1016MedicalRegimenDiagnosis5"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosis5"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosisCode5") && data["M1016MedicalRegimenDiagnosisCode5"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosisCode5"].Answer : "" %>",false,1)) +
            "%3C/span%3E%3Cspan class=%22float_left%22%3Ef.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosis6") && data["M1016MedicalRegimenDiagnosis6"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosis6"].Answer : "" %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("M1016MedicalRegimenDiagnosisCode6") && data["M1016MedicalRegimenDiagnosisCode6"].Answer.IsNotNullOrEmpty() ? data["M1016MedicalRegimenDiagnosisCode6"].Answer : "" %>",false,1)) +
            "%3C/span%3E");<%
    } %>
    printview.addsection(
        printview.span("(M1018) Conditions Prior to Medical or Treatment Regimen Change or Inpatient Stay Within Past 14 Days: If this patient experienced an inpatient facility discharge or change in medical or treatment regimen within the past 14 days, indicate any conditions which existed prior to the inpatient stay or change in medical or treatment regime.",true) +
        printview.col(2,
            printview.checkbox("1 &ndash; Urinary incontinence",<%= data != null && data.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI") && data["M1018ConditionsPriorToMedicalRegimenUI"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Indwelling/suprapubic catheter",<%= data != null && data.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH") && data["M1018ConditionsPriorToMedicalRegimenCATH"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Intractable pain",<%= data != null && data.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain") && data["M1018ConditionsPriorToMedicalRegimenPain"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Impaired decision-making",<%= data != null && data.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN") && data["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; Disruptive or socially inappropriate behavior",<%= data != null && data.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive") && data["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("6 &ndash; Memory loss to the extent that supervision required",<%= data != null && data.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss") && data["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("7 &ndash; None of the above",<%= data != null && data.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone") && data["M1018ConditionsPriorToMedicalRegimenNone"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("UK &ndash; Unknown",<%= data != null && data.ContainsKey("M1018ConditionsPriorToMedicalRegimenUK") && data["M1018ConditionsPriorToMedicalRegimenUK"].Answer == "1" ? "true" : "false"%>)
        ) +
        printview.checkbox("NA &ndash; No inpatient facility discharge and no change in medical or treatment regimen in past 14 days",<%= data != null && data.ContainsKey("M1018ConditionsPriorToMedicalRegimenNA") && data["M1018ConditionsPriorToMedicalRegimenNA"].Answer == "1" ? "true" : "false"%>));
    printview.addsection(
        printview.col(4,
            printview.span("Column 1",true) +
            printview.span("Column 2",true) +
            printview.span("Column 3",true) +
            printview.span("Column 4",true) +
            printview.span("(M1020) Primary Diagnosis",true) +
            printview.span("ICD-9-C M and Severity",true) +
            printview.span("(M1024) Description/ ICD-9-C M",true) +
            printview.span("(M1024) Description/ ICD-9-C M",true)) +
        "%3Cspan class=%22float_left%22%3Ea.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
        printview.col(4,
            printview.span("<%= data != null && data.ContainsKey("M1020PrimaryDiagnosis") && data["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data["M1020PrimaryDiagnosis"].Answer : ""%><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis") && data["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer == "2" ? " Onset " : "" %><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis") && data["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer == "1" ? " Exacerbation " : "" %><%= data != null && data.ContainsKey("M1020PrimaryDiagnosisDate") && data["M1020PrimaryDiagnosisDate"].Answer.IsNotNullOrEmpty() ? data["M1020PrimaryDiagnosisDate"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1020ICD9M") && data["M1020ICD9M"].Answer.IsNotNullOrEmpty() ? data["M1020ICD9M"].Answer : ""%><%= data != null && data.ContainsKey("M1020SymptomControlRating") && data["M1020SymptomControlRating"].Answer.IsNotNullOrEmpty() ? " Severity " + data["M1020SymptomControlRating"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesA3") && data["M1024PaymentDiagnosesA3"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesA3"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9MA3") && data["M1024ICD9MA3"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9MA3"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesA4") && data["M1024PaymentDiagnosesA4"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesA4"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9MA4") && data["M1024ICD9MA4"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9MA4"].Answer : ""%>",false,1)) +
        "%3C/span%3E" +
        printview.span("(M1022) Other Diagnoses",true) +
        "%3Cspan class=%22float_left%22%3Eb.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
        printview.col(4,
            printview.span("<%= data != null && data.ContainsKey("M1022PrimaryDiagnosis1") && data["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis1"].Answer : ""%><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis1") && data["485ExacerbationOrOnsetPrimaryDiagnosis1"].Answer == "2" ? " Onset " : "" %><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis1") && data["485ExacerbationOrOnsetPrimaryDiagnosis1"].Answer == "1" ? " Exacerbation " : "" %><%= data != null && data.ContainsKey("M1022PrimaryDiagnosisDate1") && data["M1022PrimaryDiagnosisDate1"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosisDate1"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1022ICD9M1") && data["M1022ICD9M1"].Answer.IsNotNullOrEmpty() ? data["M1022ICD9M1"].Answer : ""%><%= data != null && data.ContainsKey("M1022OtherDiagnose1Rating") && data["M1022OtherDiagnose1Rating"].Answer.IsNotNullOrEmpty() ? " Severity " + data["M1022OtherDiagnose1Rating"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesB3") && data["M1024PaymentDiagnosesB3"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesB3"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9MB3") && data["M1024ICD9MB3"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9MB3"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesB4") && data["M1024PaymentDiagnosesB4"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesB4"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9MB4") && data["M1024ICD9MB4"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9MB4"].Answer : ""%>",false,1)) +
        "%3C/span%3E%3Cspan class=%22float_left%22%3Ec.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
        printview.col(4,
            printview.span("<%= data != null && data.ContainsKey("M1022PrimaryDiagnosis2") && data["M1022PrimaryDiagnosis2"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis2"].Answer : ""%><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis2") && data["485ExacerbationOrOnsetPrimaryDiagnosis2"].Answer == "2" ? " Onset " : "" %><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis2") && data["485ExacerbationOrOnsetPrimaryDiagnosis2"].Answer == "1" ? " Exacerbation " : "" %><%= data != null && data.ContainsKey("M1022PrimaryDiagnosisDate2") && data["M1022PrimaryDiagnosisDate2"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosisDate2"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1022ICD9M2") && data["M1022ICD9M2"].Answer.IsNotNullOrEmpty() ? data["M1022ICD9M2"].Answer : ""%><%= data != null && data.ContainsKey("M1022OtherDiagnose2Rating") && data["M1022OtherDiagnose2Rating"].Answer.IsNotNullOrEmpty() ? " Severity " + data["M1022OtherDiagnose2Rating"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesC3") && data["M1024PaymentDiagnosesC3"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesC3"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9MC3") && data["M1024ICD9MC3"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9MC3"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesC4") && data["M1024PaymentDiagnosesC4"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesC4"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9MC4") && data["M1024ICD9MC4"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9MC4"].Answer : ""%>",false,1)) +
        "%3C/span%3E%3Cspan class=%22float_left%22%3Ed.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
        printview.col(4,
            printview.span("<%= data != null && data.ContainsKey("M1022PrimaryDiagnosis3") && data["M1022PrimaryDiagnosis3"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis3"].Answer : ""%><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis3") && data["485ExacerbationOrOnsetPrimaryDiagnosis3"].Answer == "2" ? " Onset " : "" %><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis3") && data["485ExacerbationOrOnsetPrimaryDiagnosis3"].Answer == "1" ? " Exacerbation " : "" %><%= data != null && data.ContainsKey("M1022PrimaryDiagnosisDate3") && data["M1022PrimaryDiagnosisDate3"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosisDate3"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1022ICD9M3") && data["M1022ICD9M3"].Answer.IsNotNullOrEmpty() ? data["M1022ICD9M3"].Answer : ""%><%= data != null && data.ContainsKey("M1022OtherDiagnose3Rating") && data["M1022OtherDiagnose3Rating"].Answer.IsNotNullOrEmpty() ? " Severity " + data["M1022OtherDiagnose3Rating"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesD3") && data["M1024PaymentDiagnosesD3"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesD3"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9MD3") && data["M1024ICD9MD3"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9MD3"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesD4") && data["M1024PaymentDiagnosesD4"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesD4"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9MD4") && data["M1024ICD9MD4"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9MD4"].Answer : ""%>",false,1)) +
        "%3C/span%3E%3Cspan class=%22float_left%22%3Ee.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
        printview.col(4,
            printview.span("<%= data != null && data.ContainsKey("M1022PrimaryDiagnosis4") && data["M1022PrimaryDiagnosis4"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis4"].Answer : ""%><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis4") && data["485ExacerbationOrOnsetPrimaryDiagnosis4"].Answer == "2" ? " Onset " : "" %><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis4") && data["485ExacerbationOrOnsetPrimaryDiagnosis4"].Answer == "1" ? " Exacerbation " : "" %><%= data != null && data.ContainsKey("M1022PrimaryDiagnosisDate4") && data["M1022PrimaryDiagnosisDate4"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosisDate4"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1022ICD9M4") && data["M1022ICD9M4"].Answer.IsNotNullOrEmpty() ? data["M1022ICD9M4"].Answer : ""%><%= data != null && data.ContainsKey("M1022OtherDiagnose4Rating") && data["M1022OtherDiagnose4Rating"].Answer.IsNotNullOrEmpty() ? " Severity " + data["M1022OtherDiagnose4Rating"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesE3") && data["M1024PaymentDiagnosesE3"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesE3"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9ME3") && data["M1024ICD9ME3"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9ME3"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesE4") && data["M1024PaymentDiagnosesE4"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesE4"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9ME4") && data["M1024ICD9ME4"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9ME4"].Answer : ""%>",false,1)) +
        "%3C/span%3E%3Cspan class=%22float_left%22%3Ef.%3C/span%3E%3Cspan class=%22labeledrow%22%3E" +
        printview.col(4,
            printview.span("<%= data != null && data.ContainsKey("M1022PrimaryDiagnosis5") && data["M1022PrimaryDiagnosis5"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis5"].Answer : ""%><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis5") && data["485ExacerbationOrOnsetPrimaryDiagnosis5"].Answer == "2" ? " Onset " : "" %><%= data != null && data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis5") && data["485ExacerbationOrOnsetPrimaryDiagnosis5"].Answer == "1" ? " Exacerbation " : "" %><%= data != null && data.ContainsKey("M1022PrimaryDiagnosisDate5") && data["M1022PrimaryDiagnosisDate5"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosisDate5"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1022ICD9M5") && data["M1022ICD9M5"].Answer.IsNotNullOrEmpty() ? data["M1022ICD9M5"].Answer : ""%><%= data != null && data.ContainsKey("M1022OtherDiagnose5Rating") && data["M1022OtherDiagnose5Rating"].Answer.IsNotNullOrEmpty() ? " Severity " + data["M1022OtherDiagnose5Rating"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesF3") && data["M1024PaymentDiagnosesF3"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesF3"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9MF3") && data["M1024ICD9MF3"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9MF3"].Answer : ""%>",false,1) +
            printview.span("<%= data != null && data.ContainsKey("M1024PaymentDiagnosesF4") && data["M1024PaymentDiagnosesF4"].Answer.IsNotNullOrEmpty() ? data["M1024PaymentDiagnosesF4"].Answer : ""%><%= data != null && data.ContainsKey("M1024ICD9MF4") && data["M1024ICD9MF4"].Answer.IsNotNullOrEmpty() ? " " + data["M1024ICD9MF4"].Answer : ""%>",false,1)) +
        "%3C/span%3E");
    printview.addsection(
        printview.col(3,
            printview.span("Surgical Procedure",true) +
            printview.span("Code",true) +
            printview.span("Date",true) +
            printview.span("<%= data != null && data.ContainsKey("485SurgicalProcedureDescription1") && data["485SurgicalProcedureDescription1"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureDescription1"].Answer : ""%>") +
            printview.span("<%= data != null && data.ContainsKey("485SurgicalProcedureCode1") && data["485SurgicalProcedureCode1"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode1"].Answer : ""%>") +
            printview.span("<%= data != null && data.ContainsKey("485SurgicalProcedureCode1Date") && data["485SurgicalProcedureCode1Date"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode1Date"].Answer : ""%>") +
            printview.span("<%= data != null && data.ContainsKey("485SurgicalProcedureDescription2") && data["485SurgicalProcedureDescription2"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureDescription2"].Answer : ""%>") +
            printview.span("<%= data != null && data.ContainsKey("485SurgicalProcedureCode2") && data["485SurgicalProcedureCode2"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode2"].Answer : ""%>") +
            printview.span("<%= data != null && data.ContainsKey("485SurgicalProcedureCode2Date") && data["485SurgicalProcedureCode2Date"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode2Date"].Answer : ""%>")
        ),"Surgical Procedure");
    printview.addsection(
        printview.span("(M1030) Therapies the patient receives at home",true) +
        printview.col(2,
            printview.checkbox("1 &ndash; Intravenous or infusion therapy (excludes TPN)",<%= data != null && data.ContainsKey("M1030HomeTherapiesInfusion") && data["M1030HomeTherapiesInfusion"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Parenteral nutrition (TPN or lipids)",<%= data != null && data.ContainsKey("M1030HomeTherapiesParNutrition") && data["M1030HomeTherapiesParNutrition"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Enteral nutrition",<%= data != null && data.ContainsKey("M1030HomeTherapiesEntNutrition") && data["M1030HomeTherapiesEntNutrition"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; None of the above",<%= data != null && data.ContainsKey("M1030HomeTherapiesNone") && data["M1030HomeTherapiesNone"].Answer == "1" ? "true" : "false"%>)));
</script>