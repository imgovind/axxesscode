﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%228%22%3E" +
        printview.span("(M1100) Patient Living Situation: Which of the following best describes the patient&rsquo;s residential circumstance and availability of assistance?",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth colspan=%223%22%3ELiving Arangement%3C/th%3E%3Cth colspan=%225%22%3EAvailability of Assistance%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth colspan=%223%22%3E%3C/th%3E%3Cth%3EAround the Clock%3C/th%3E%3Cth>Regular Daytime%3C/th%3E%3Cth%3ERegular Nighttime%3C/th%3E%3Cth%3EOccasional/ Short-term Assistance%3C/th%3E%3Cth%3ENo Assistance%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E" +
        printview.span("a. Patient lives alone") + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("01",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "01" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("02",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "02" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("03",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "03" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("04",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "04" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("05",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "05" ? "true" : "false"%>) + "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E" +
        printview.span("b. Patient lives with other person(s) in the home") + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("06",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "06" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("07",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "07" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("08",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "08" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("09",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "09" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("10",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "10" ? "true" : "false"%>) + "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E" +
        printview.span("c. Patient lives in congregate situation (e.g., assisted living)") + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("11",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "11" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("12",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "12" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("13",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "13" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("14",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "14" ? "true" : "false"%>) + "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("15",<%= data != null && data.ContainsKey("M1100LivingSituation") && data["M1100LivingSituation"].Answer == "15" ? "true" : "false"%>) + "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
    printview.addsection(
        printview.col(2,
            printview.span("Community resource info needed to manage care",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericCommunityResourceInfoNeeded") && data["GenericCommunityResourceInfoNeeded"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericCommunityResourceInfoNeeded") && data["GenericCommunityResourceInfoNeeded"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Altered affect, e.g., expressed sadness or anxiety, grief ",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericAlteredAffect") && data["GenericAlteredAffect"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericAlteredAffect") && data["GenericAlteredAffect"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Suicidal ideation",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericSuicidalIdeation") && data["GenericSuicidalIdeation"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericSuicidalIdeation") && data["GenericSuicidalIdeation"].Answer == "0" ? "true" : "false"%>))) +
        printview.span("Suspected Abuse/ Neglect",true) +
        printview.col(2,
            printview.col(2,
                printview.checkbox("Unexplained bruises",<%= data != null && data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("Inadequate food",<%= data != null && data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "1" ? "true" : "false"%>)) +
            printview.col(2,
                printview.checkbox("Fearful of family member",<%= data != null && data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("Exploitation of funds",<%= data != null && data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "1" ? "true" : "false"%>)) +
            printview.col(2,
                printview.checkbox("Sexual abuse",<%= data != null && data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("Neglect",<%= data != null && data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "1" ? "true" : "false"%>)) +
            printview.checkbox("Left unattended if constant supervision is needed",<%= data != null && data.ContainsKey("GenericSuspected") && data["GenericSuspected"].Answer == "1" ? "true" : "false"%>) +
            printview.span("MSW referral indicated for <%= data != null && data.ContainsKey("GenericMSWIndicatedForWhat") && data["GenericMSWIndicatedForWhat"].Answer.IsNotNullOrEmpty() ? data["GenericMSWIndicatedForWhat"].Answer : "<span class='blank'></span>"%>",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericMSWIndicatedFor") && data["GenericMSWIndicatedFor"].Answer == "Yes" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericMSWIndicatedFor") && data["GenericMSWIndicatedFor"].Answer == "No" ? "true" : "false"%>)) +
            printview.span("Coordinator notified",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericCoordinatorNotified") && data["GenericCoordinatorNotified"].Answer == "Yes" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericCoordinatorNotified") && data["GenericCoordinatorNotified"].Answer == "No" ? "true" : "false"%>)) +
            printview.span("Ability of Patient to Handle Finances",true) +
            printview.col(3,
                printview.checkbox("Independent",<%= data != null && data.ContainsKey("GenericAbilityHandleFinance") && data["GenericAbilityHandleFinance"].Answer == "Independent" ? "true" : "false"%>) +
                printview.checkbox("Dependent",<%= data != null && data.ContainsKey("GenericAbilityHandleFinance") && data["GenericAbilityHandleFinance"].Answer == "Dependent" ? "true" : "false"%>) +
                printview.checkbox("Needs assistance",<%= data != null && data.ContainsKey("GenericAbilityHandleFinance") && data["GenericAbilityHandleFinance"].Answer == "Needs assistance" ? "true" : "false"%>))) +
        printview.span("Comments:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericAlteredAffectComments") && data["GenericAlteredAffectComments"].Answer.IsNotNullOrEmpty() ? data["GenericAlteredAffectComments"].Answer : ""%>",false,2) +
        printview.span("Supportive Assistance: Names of organizations providing assistance:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericOrgProvidingAssistanceNames") && data["GenericOrgProvidingAssistanceNames"].Answer.IsNotNullOrEmpty() ? data["GenericOrgProvidingAssistanceNames"].Answer : ""%>",false,2),
        "Community Agencies/ Social Service Screening");
    printview.addsection(
        printview.col(3,
            printview.checkbox("No hazards identified",<%= data != null && data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer.Split(',').Contains("0") ? "true" : "false"%>) +
            printview.checkbox("Stairs",<%= data != null && data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Narrow or obstructed walkway",<%= data != null && data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("No gas/ electric appliance",<%= data != null && data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("No running water, plumbing",<%= data != null && data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Insect/ rodent infestation",<%= data != null && data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Cluttered/ soiled living area",<%= data != null && data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Lack of fire safety devices",<%= data != null && data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
            printview.checkbox("Inadequate lighting, heating and cooling",<%= data != null && data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Other (Specify) <%= data != null && data.ContainsKey("GenericOtherHazards") && data["GenericOtherHazards"].Answer.IsNotNullOrEmpty() ? data["GenericOtherHazards"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericHazardsIdentified") && data["GenericHazardsIdentified"].Answer.Split(',').Contains("9") ? "true" : "false"%>)) +
        printview.span("Comments:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericMSWIndicatedForWhat") && data["GenericMSWIndicatedForWhat"].Answer.IsNotNullOrEmpty() ? data["GenericMSWIndicatedForWhat"].Answer : ""%>",false,2),
        "Safety/ Sanitation Hazards");
    printview.addsection(
        printview.col(2,
            printview.span("Primary language?",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPrimaryLanguage") && data["GenericPrimaryLanguage"].Answer.IsNotNullOrEmpty() ? data["GenericPrimaryLanguage"].Answer : ""%>",false,1)) +
        printview.col(5,
            printview.span("Use of interpreter",true) +
            printview.checkbox("Family",<%= data != null && data.ContainsKey("GenericUseOfInterpreter") && data["GenericUseOfInterpreter"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Friend",<%= data != null && data.ContainsKey("GenericUseOfInterpreter") && data["GenericUseOfInterpreter"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Professional",<%= data != null && data.ContainsKey("GenericUseOfInterpreter") && data["GenericUseOfInterpreter"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericUseOfInterpreterOtherDetails") && data["GenericUseOfInterpreterOtherDetails"].Answer.IsNotNullOrEmpty() ? data["GenericUseOfInterpreterOtherDetails"].Answer : ""%>",<%= data != null && data.ContainsKey("GenericUseOfInterpreter") && data["GenericUseOfInterpreter"].Answer.Split(',').Contains("4") ? "true" : "false"%>)) +
        printview.col(2,
            printview.span("Does patient have cultural practices that influence health care?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericCulturalPractices") && data["GenericCulturalPractices"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericCulturalPractices") && data["GenericCulturalPractices"].Answer == "0" ? "true" : "false"%>))) +
        printview.span("If yes, please explain",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericCulturalPracticesDetails") && data["GenericCulturalPracticesDetails"].Answer.IsNotNullOrEmpty() ? data["GenericCulturalPracticesDetails"].Answer : ""%>",false,2),
        "Cultural");
    printview.addsection(
        printview.col(2,
            printview.span("Is the patient homebound?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericIsHomeBound") && data["GenericIsHomeBound"].Answer == "Yes" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericIsHomeBound") && data["GenericIsHomeBound"].Answer == "No" ? "true" : "false"%>))) +<%
    if (data == null || data.ContainsKey("GenericIsHomeBound") == false || data["GenericIsHomeBound"].Answer != "No") { %>
        printview.col(2,
            printview.checkbox("Exhibits considerable &amp; taxing effort to leave home",<%= data != null && data.ContainsKey("GenericHomeBoundReason") && data["GenericHomeBoundReason"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Severe Dyspnea",<%= data != null && data.ContainsKey("GenericHomeBoundReason") && data["GenericHomeBoundReason"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Unsafe to leave home due to cognitive or psychiatric impairments",<%= data != null && data.ContainsKey("GenericHomeBoundReason") && data["GenericHomeBoundReason"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Requires the assistance of another to get up and moving safely",<%= data != null && data.ContainsKey("GenericHomeBoundReason") && data["GenericHomeBoundReason"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Unable to safely leave home unassisted",<%= data != null && data.ContainsKey("GenericHomeBoundReason") && data["GenericHomeBoundReason"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Unable to leave home due to medical restriction(s)",<%= data != null && data.ContainsKey("GenericHomeBoundReason") && data["GenericHomeBoundReason"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericOtherHomeBoundDetails") && data["GenericOtherHomeBoundDetails"].Answer.IsNotNullOrEmpty() ? data["GenericOtherHomeBoundDetails"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericHomeBoundReason") && data["GenericHomeBoundReason"].Answer.Split(',').Contains("7") ? "true" : "false"%>)) +<%
    } %>"","Homebound");
    printview.addsection(
        printview.col(3,
            printview.checkbox("Anticoagulant Precautions",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Emergency Plan Developed",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Fall Precautions",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Keep Pathway Clear",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Keep Side Rails Up",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Neutropenic Precautions",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("O<sub>2</sub> Precautions",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Proper Position During Meals",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
            printview.checkbox("Safety in ADLs",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
            printview.checkbox("Seizure Precautions",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
            printview.checkbox("Sharps Safety",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("11") ? "true" : "false"%>) +
            printview.checkbox("Slow Position Change",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("12") ? "true" : "false"%>) +
            printview.checkbox("Standard Precautions/ Infection Control",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("13") ? "true" : "false"%>) +
            printview.checkbox("Support During Transfer and Ambulation",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("14") ? "true" : "false"%>) +
            printview.checkbox("Use of Assistive Devices",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("15") ? "true" : "false"%>) +
            printview.checkbox("Instructed on safe utilities management",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("16") ? "true" : "false"%>) +
            printview.checkbox("Instructed on mobility safety",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("17") ? "true" : "false"%>) +
            printview.checkbox("Instructed on DME &amp; electrical safety",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("18") ? "true" : "false"%>) +
            printview.checkbox("Instructed on sharps container",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("19") ? "true" : "false"%>) +
            printview.checkbox("Instructed on medical gas",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("20") ? "true" : "false"%>) +
            printview.checkbox("Instructed on disaster/ emergency plan",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("21") ? "true" : "false"%>) +
            printview.checkbox("Instructed on safety measures",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("22") ? "true" : "false"%>) +
            printview.checkbox("Instructed on proper handling of bio waste",<%= data != null && data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.Split(',').Contains("23") ? "true" : "false"%>)) +
        printview.col(2,
            printview.span("Emergency Triage Code:",true) +
            printview.span("<%= data != null && data.ContainsKey("485TriageEmergencyCode") && data["485TriageEmergencyCode"].Answer.IsNotNullOrEmpty() ? data["485TriageEmergencyCode"].Answer : ""%>",false,1)) +
        printview.span("Other:",true) +
        printview.span("<%= data != null && data.ContainsKey("485OtherSafetyMeasures") && data["485OtherSafetyMeasures"].Answer.IsNotNullOrEmpty() ? data["485OtherSafetyMeasures"].Answer : ""%>",false,2),
        "Safety Measures");
    printview.addsection(
        printview.checkbox("MSW to assess psychosocial needs, environment and assist with community referrals and resources.",<%= data != null && data.ContainsKey("485SupportiveAssistanceInterventions") && data["485SupportiveAssistanceInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485SupportiveAssistanceComments") && data["485SupportiveAssistanceComments"].Answer.IsNotNullOrEmpty() ? data["485SupportiveAssistanceComments"].Answer : ""%>",false,2),
        "Interventions");
</script>