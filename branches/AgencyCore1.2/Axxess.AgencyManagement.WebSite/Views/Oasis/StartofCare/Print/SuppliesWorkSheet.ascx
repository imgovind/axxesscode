﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
/*
    printview.addsection(
        "%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Cth%3EDescription%3C/th%3E%3Cth%3ECode%3C/th%3E%3Cth%3EQuantity%3C/th%3E%3Cth%3EDate%3C/th%3E%3C/tr%3E" +
// Start Loop, for each supply
        "%3Ctr%3E%3Ctd%3E" +
// Description
        "%3C/td%3E%3Ctd%3E" +
// Code
        "%3C/td%3E%3Ctd%3E" +
// Quantity
        "%3C/td%3E%3Ctd%3E" +
// Date
        "%3C/td%3E%3C/tr%3E" +
// End Loop
        "%3C/tbody%3E%3C/table%3E","Current Supplies Used");
*/
    printview.addsection(
        printview.col(6,
            printview.checkbox("ABDs",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Ace Wrap",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Alcohol Pads",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Chux/Underpads",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Diabetic Supplies",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Drainage Bag",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Dressing Supplies",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Duoderm",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
            printview.checkbox("Exam Gloves",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
            printview.checkbox("Foley Catheter",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("10") ? "true" : "false"%>) +
            printview.checkbox("Gauze Pads",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("11") ? "true" : "false"%>) +
            printview.checkbox("Insertion Kit",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("12") ? "true" : "false"%>) +
            printview.checkbox("Irrigation Set",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("13") ? "true" : "false"%>) +
            printview.checkbox("Irrigation Solution",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("14") ? "true" : "false"%>) +
            printview.checkbox("Kerlix Rolls",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("15") ? "true" : "false"%>) +
            printview.checkbox("Leg Bag",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("16") ? "true" : "false"%>) +
            printview.checkbox("Needles",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("17") ? "true" : "false"%>) +
            printview.checkbox("NG Tube",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("18") ? "true" : "false"%>) +
            printview.checkbox("Probe Covers",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("19") ? "true" : "false"%>) +
            printview.checkbox("Sharps Container",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("20") ? "true" : "false"%>) +
            printview.checkbox("Sterile Gloves",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("21") ? "true" : "false"%>) +
            printview.checkbox("Syringe",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("22") ? "true" : "false"%>) +
            printview.checkbox("Tape",<%= data != null && data.ContainsKey("485Supplies") && data["485Supplies"].Answer.Split(',').Contains("23") ? "true" : "false"%>)) +
        printview.span("Other:",true) +
        printview.span("<%= data != null && data.ContainsKey("485SuppliesComment") && data["485SuppliesComment"].Answer.IsNotNullOrEmpty() ? data["485SuppliesComment"].Answer : ""%>",false,2),
        "Supplies");
    printview.addsection(
        printview.col(5,
            printview.checkbox("Bedside Commode",<%= data != null && data.ContainsKey("485DME") && data["485DME"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Cane",<%= data != null && data.ContainsKey("485DME") && data["485DME"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Elevated Toilet Seat",<%= data != null && data.ContainsKey("485DME") && data["485DME"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Grab Bars",<%= data != null && data.ContainsKey("485DME") && data["485DME"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Hospital Bed",<%= data != null && data.ContainsKey("485DME") && data["485DME"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Nebulizer",<%= data != null && data.ContainsKey("485DME") && data["485DME"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Oxygen",<%= data != null && data.ContainsKey("485DME") && data["485DME"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Tub/Shower Bench",<%= data != null && data.ContainsKey("485DME") && data["485DME"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
            printview.checkbox("Walker",<%= data != null && data.ContainsKey("485DME") && data["485DME"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
            printview.checkbox("Wheelchair",<%= data != null && data.ContainsKey("485DME") && data["485DME"].Answer.Split(',').Contains("10") ? "true" : "false"%>)) +
        printview.span("Other:",true) +
        printview.span("<%= data != null && data.ContainsKey("485DMEComments") && data["485DMEComments"].Answer.IsNotNullOrEmpty() ? data["485DMEComments"].Answer : ""%>",false,2),
        "DME");
    printview.addsection(
        printview.col(4,
            printview.span("Name:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericDMEProviderName") && data["GenericDMEProviderName"].Answer.IsNotNullOrEmpty() ? data["GenericDMEProviderName"].Answer : ""%>",false,1) +
            printview.span("Phone Number:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericDMEProviderPhone") && data["GenericDMEProviderPhone"].Answer.IsNotNullOrEmpty() ? data["GenericDMEProviderPhone"].Answer : ""%>",false,1)) +
        printview.span("DME/Supplies Provided:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericDMESuppliesProvided") && data["GenericDMESuppliesProvided"].Answer.IsNotNullOrEmpty() ? data["GenericDMESuppliesProvided"].Answer : ""%>",false,2),
        "DME Provider");
</script>