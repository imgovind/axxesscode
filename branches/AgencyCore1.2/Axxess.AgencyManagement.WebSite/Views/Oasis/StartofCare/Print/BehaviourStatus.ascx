﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.checkbox("LOC:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.span("") +
            printview.span("Oriented to:") +
            printview.checkbox("Person",<%= data != null && data.ContainsKey("GenericNeurologicalOriented") && data["GenericNeurologicalOriented"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Place",<%= data != null && data.ContainsKey("GenericNeurologicalOriented") && data["GenericNeurologicalOriented"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Time",<%= data != null && data.ContainsKey("GenericNeurologicalOriented") && data["GenericNeurologicalOriented"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Pupils:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.span("") +
            printview.span("") +
            printview.checkbox("Bilateral",<%= data != null && data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "0" ? "true" : "false"%>) +
            printview.checkbox("Left",<%= data != null && data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("Right",<%= data != null && data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "2" ? "true" : "false"%>) +
            printview.checkbox("Vision:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true)) +
        printview.col(6,
            printview.checkbox("WNL",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Blurred Vision",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Cataracts",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Corrective Lenses",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Glaucoma",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Legally Blind",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Speech:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeurologicalSpeech") && data["GenericNeurologicalSpeech"].Answer.IsNotNullOrEmpty() ? (data["GenericNeurologicalSpeech"].Answer == "0" ? "" : "") + (data["GenericNeurologicalSpeech"].Answer == "1" ? "Clear" : "") + (data["GenericNeurologicalSpeech"].Answer == "2" ? "Slurred" : "") + (data["GenericNeurologicalSpeech"].Answer == "3" ? "Aphasic" : "") + (data["GenericNeurologicalSpeech"].Answer == "4" ? "Other" : "") : ""%>",false,1) +
            printview.checkbox("Paralysis:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("5") ? "true" : "false"%>,true) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeurologicalParalysisLocation") && data["GenericNeurologicalParalysisLocation"].Answer.IsNotNullOrEmpty() ? "Location: " + data["GenericNeurologicalParalysisLocation"].Answer : "" %>",false,1) +
            printview.checkbox("Tremors:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("9") ? "true" : "false"%>,true) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeurologicalTremorsLocation") && data["GenericNeurologicalTremorsLocation"].Answer.IsNotNullOrEmpty() ? "Location: " + data["GenericNeurologicalTremorsLocation"].Answer : ""%>",false,1) +
            printview.checkbox("Quadripiegia",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("6") ? "true" : "false"%>,true) +
            printview.checkbox("Parapiegia",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("7") ? "true" : "false"%>,true) +
            printview.checkbox("Seizures",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("8") ? "true" : "false"%>,true) +
            printview.checkbox("Dizziness",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("10") ? "true" : "false"%>,true) +
            printview.checkbox("Headache",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("11") ? "true" : "false"%>,true)) +
        printview.span("Comments:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericNeuroEmoBehaviorComments") && data["GenericNeuroEmoBehaviorComments"].Answer.IsNotNullOrEmpty() ? data["GenericNeuroEmoBehaviorComments"].Answer : ""%>",false,2),
        "Neurological");
    printview.addsection(
        printview.col(4,
            printview.checkbox("Oriented",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Comatose",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Forgetful",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Agitated",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Depressed",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Disoriented",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Lethargic",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("485MentalStatusOther") && data["485MentalStatusOther"].Answer.IsNotNullOrEmpty() ? data["485MentalStatusOther"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("8") ? "true" : "false"%>)) + 
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485MentalStatusComments") && data["485MentalStatusComments"].Answer.IsNotNullOrEmpty() ? data["485MentalStatusComments"].Answer : ""%>",false,2),
        "Mental Status");
    printview.addsection(
        printview.col(4,
            printview.checkbox("WNL (Within Normal Limits)",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Withdrawn",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Expresses Depression",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Impaired Decision Making",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Difficulty coping w/Health Status",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Combative",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Irritability",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericBehaviourOther") && data["GenericBehaviourOther"].Answer.IsNotNullOrEmpty() ? data["GenericBehaviourOther"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("8") ? "true" : "false"%>)) + 
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericBehaviourComments") && data["GenericBehaviourComments"].Answer.IsNotNullOrEmpty() ? data["GenericBehaviourComments"].Answer : ""%>",false,2),
        "Behaviour Status");
    printview.addsection(
        printview.col(3,
            printview.checkbox("WNL (No Issues Identified)",<%= data != null && data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Poor Home Environment",<%= data != null && data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("No emotional support from family/CG",<%= data != null && data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Verbalize desire for spiritual support",<%= data != null && data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericPsychosocialOther") && data["GenericPsychosocialOther"].Answer.IsNotNullOrEmpty() ? data["GenericPsychosocialOther"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer.Split(',').Contains("5") ? "true" : "false"%>)) + 
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericPsychosocialComments") && data["GenericPsychosocialComments"].Answer.IsNotNullOrEmpty() ? data["GenericPsychosocialComments"].Answer : ""%>",false,2),
        "Psychosocial");
    printview.addsection(
        printview.col(4,
            printview.checkbox("Motivated Learner",<%= data != null && data.ContainsKey("485PatientStrengths") && data["485PatientStrengths"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Strong Support System",<%= data != null && data.ContainsKey("485PatientStrengths") && data["485PatientStrengths"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Absence of Multiple Diagnosis",<%= data != null && data.ContainsKey("485PatientStrengths") && data["485PatientStrengths"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Enhanced Socioeconomic Status",<%= data != null && data.ContainsKey("485PatientStrengths") && data["485PatientStrengths"].Answer.Split(',').Contains("4") ? "true" : "false"%>)) +
        printview.span("Other:",true) +
        printview.span("<%= data != null && data.ContainsKey("485PatientStrengthOther") && data["485PatientStrengthOther"].Answer.IsNotNullOrEmpty() ? data["485PatientStrengthOther"].Answer : ""%>",false,2),
        "Patient Strengths");
    printview.addsection(
        printview.span("(M1700) Cognitive Functioning: Patient&rsquo;s current (day of assessment) level of alertness, orientation, comprehension, concentration, and immediate memory for simple commands.",true) +
        printview.checkbox("0 &ndash; Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.",<%= data != null && data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.",<%= data != null && data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Requires assistance and some direction in specific situations (e.g., on all tasks involving shifting of attention), or consistently requires low stimulus environment due to distractibility.",<%= data != null && data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift attention and recall directions more than half the time.",<%= data != null && data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "03" ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.",<%= data != null && data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "04" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1710) When Confused (Reported or Observed Within the Last 14 Days):",true) +
        printview.col(3,
            printview.checkbox("0 &ndash; Never",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; In new or complex situations only",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; On awakening or at night only",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; During day and evening, but not constant",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Constantly",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "04" ? "true" : "false"%>) +
            printview.checkbox("NA &ndash; Patient nonresponsive",<%= data != null && data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "NA" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1720) When Anxious (Reported or Observed Within the Last 14 Days):",true) +
        printview.col(3,
            printview.checkbox("0 &ndash; None of the time",<%= data != null && data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Less often than daily",<%= data != null && data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Daily, but not constantly",<%= data != null && data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; All of the time",<%= data != null && data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("NA &ndash; Patient nonresponsive",<%= data != null && data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "NA" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1730) Depression Screening: Has the patient been screened for depression, using a standardized depression screening tool?",true) +
        printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Yes, patient was screened using the PHQ-2&copy;* scale. (Instructions for this two-question tool: Ask patient: &ldquo;Over the last two weeks, how often have you been bothered by any of the following problems&rdquo;)",<%= data != null && data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Yes, with a different standardized assessment-and the patient meets criteria for further evaluation for depression",<%= data != null && data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Yes, patient was screened with a different standardized assessment-and the patient does not meet criteria for further evaluation for depression.",<%= data != null && data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "03" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1740) Cognitive, behavioral, and psychiatric symptoms that are demonstrated at least once a week (Reported or Observed): (Mark all that apply.)",true) +
        printview.checkbox("1 &ndash; Memory deficit: failure to recognize familiar persons/places, inability to recall events of past 24 hours, significant memory loss so that supervision is required",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Impaired decision-making: failure to perform usual ADLs or IADLs, inability to appropriately stop activities, jeopardizes safety through actions",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Verbal disruption: yelling, threatening, excessive profanity, sexual references, etc.",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Physical aggression: aggressive or combative to self and others (e.g., hits self, throws objects, punches, dangerous maneuvers with wheelchair or other objects)",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; Disruptive, infantile, or socially inappropriate behavior (excludes verbal actions)",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
        printview.checkbox("6 &ndash; Delusional, hallucinatory, or paranoid behavior",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.Split(',').Contains("06") ? "true" : "false"%>) +
        printview.checkbox("7 &ndash; None of the above behaviors demonstrated",<%= data != null && data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.Split(',').Contains("07") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1745) Frequency of Disruptive Behavior Symptoms (Reported or Observed) Any physical, verbal, or other disruptive/dangerous symptoms that are injurious to self or others or jeopardize personal safety.",true) +
        printview.col(3,
            printview.checkbox("0 &ndash; Never",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Less than once a month",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Once a month",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Several times each month",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Several times a week",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "04" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; At least daily",<%= data != null && data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "05" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1750) Is this patient receiving Psychiatric Nursing Services at home provided by a qualified psychiatric nurse?",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes",<%= data != null && data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "01" ? "true" : "false"%>)));
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth%3E%3C/th%3E%3Cth%3ENot at all%3Cbr /%3E(0 &ndash; 1 day)%3C/th%3E%3Cth%3EServeral Days%3Cbr /%3E(2 &ndash; 6 days)%3C/th%3E%3Cth%3EMore than half of the days%3Cbr /%3E(7 &ndash; 11 days)%3C/th%3E%3Cth%3ENearly every day%3Cbr /%3E(12 &ndash; 14 days)%3C/th%3E%3Cth%3EN/A Unable to respond%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3Ea) Little interest or pleasure in doing things%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "02" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "03" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA",<%= data != null && data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3Eb) Feeling down, depressed, or hopeless?%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "02" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "03" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA",<%= data != null && data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E","PHQ-2&copy;");
    printview.addsection(
        printview.checkbox("SN to notify physicain this pateint was screened for depression using PHQ-2&copy; scale and meets criteria for further evaluation for depression.",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("SN to perform a neurological assessment each visit.",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("SN to assess/instruct on seizure disorder signs &amp; symptoms and appropriate actions during seizure activity.",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct the <%= data != null && data.ContainsKey("485InstructSeizurePrecautionPerson") && data["485InstructSeizurePrecautionPerson"].Answer.IsNotNullOrEmpty() ? data["485InstructSeizurePrecautionPerson"].Answer : "<span class='blank'></span>"%> on seizure precautions.",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct caregiver on orientation techniques to use when patient becomes disoriented.",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.checkbox("MSW: <%= data != null && data.ContainsKey("485MSWCommunityAssistanceVisits") && data["485MSWCommunityAssistanceVisits"].Answer.IsNotNullOrEmpty() && data["485MSWCommunityAssistanceVisits"].Answer == "1" ? "1&ndash;2" : (data != null && data.ContainsKey("485MSWCommunityAssistanceVisitAmount") && data["485MSWCommunityAssistanceVisitAmount"].Answer.IsNotNullOrEmpty() ? data["485MSWCommunityAssistanceVisitAmount"].Answer : "<span class='blank'></span>")%> visits, every 60 days for community resource assistance. ",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485BehaviorComments") && data["485BehaviorComments"].Answer.IsNotNullOrEmpty() ? data["485BehaviorComments"].Answer : ""%>",false,2),
        "Interventions");
    printview.addsection(
        printview.checkbox("Neuro status will be within normal limits and free of S&amp;S of complications or further deterioration.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("Patient will exhibit stable neuro status evidenced by increased alertness, mobility and self-care by the end of the episode.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("Patient will exhibit stable neuro status evidence by absence of seizure activity by the end of the episode.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("Patient will have optimal cognitive functioning within parameters established for the stage of disease by the end of the episode.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.checkbox("Patient will remain free from increased confusion during the episode.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.checkbox("The <%= data != null && data.ContainsKey("485VerbalizeSeizurePrecautionsPerson") && data["485VerbalizeSeizurePrecautionsPerson"].Answer.IsNotNullOrEmpty() ? data["485VerbalizeSeizurePrecautionsPerson"].Answer : "<span class='blank'></span>"%> will verbalize understanding of seizure precautions.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
        printview.checkbox("Caregiver will verbalize understanding of proper orientation techniques to use when patient becomes disoriented.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
        printview.checkbox("Patient&rsquo;s community resource needs will be met with assistance of social worker.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
        printview.span("Additional Goals:",true) +
        printview.span("<%= data != null && data.ContainsKey("485BehaviorGoalComments") && data["485BehaviorGoalComments"].Answer.IsNotNullOrEmpty() ? data["485BehaviorGoalComments"].Answer : ""%>",false,2),
        "Goals");
</script>