﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
        "<script type='text/javascript'>acore.renamewindow('Oasis-C Follow-up | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','followup');</script>")
%>
<div id="followUpTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_followUp">Clinical Record Items</a></li>
        <li><a href="#patienthistory_followUp">Patient History & Diagnoses</a></li>
        <li><a href="#sensorystatus_followUp">Sensory Status</a></li>
        <li><a href="#pain_followUp">Pain</a></li>
        <li><a href="#integumentarystatus_followUp">Integumentary Status</a></li>
        <li><a href="#respiratorystatus_followUp">Respiratory Status</a></li>
        <li><a href="#eliminationstatus_followUp">Elimination Status</a></li>
        <li><a href="#adl_followUp">ADL/IADLs</a></li>
        <li><a href="#medications_followUp">Medications</a></li>
        <li><a href="#therapyneed_followUp">Therapy Need & Plan Of Care</a></li>
    </ul>
    <!-- Add validation back in later -->
    <div id="clinicalRecord_followUp" class="general"><% Html.RenderPartial("~/Views/Oasis/Followup/Demographics.ascx", Model); %></div>
    <div id="patienthistory_followUp" class="general loading"></div>
    <div id="sensorystatus_followUp" class="general loading"></div>
    <div id="pain_followUp" class="general loading"></div>
    <div id="integumentarystatus_followUp" class="general loading"></div>
    <div id="respiratorystatus_followUp" class="general loading"></div>
    <div id="eliminationstatus_followUp" class="general loading"></div>
    <div id="adl_followUp" class="general loading"></div>
    <div id="medications_followUp" class="general loading"></div>
    <div id="therapyneed_followUp" class="general loading"></div>
</div>
