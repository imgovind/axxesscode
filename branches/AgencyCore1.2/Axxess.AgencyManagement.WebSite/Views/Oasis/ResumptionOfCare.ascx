﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
            "<script type='text/javascript'>acore.renamewindow('Oasis-C Resumption Of Care | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','resumptionofcare');</script>")
%>
<div id="rocTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_roc">Clinical Record Items</a></li>
        <li><a href="#patienthistory_roc">Patient History & Diagnoses</a></li>
        <li><a href="#riskassessment_roc">Risk Assessment</a></li>
        <li><a href="#prognosis_roc">Prognosis</a></li>
        <li><a href="#supportiveassistance_roc">Supportive Assistance</a></li>
        <li><a href="#sensorystatus_roc">Sensory Status</a></li>
        <li><a href="#pain_roc">Pain</a></li>
        <li><a href="#integumentarystatus_roc">Integumentary Status</a></li>
        <li><a href="#respiratorystatus_roc">Respiratory Status</a></li>
        <li><a href="#endocrine_roc">Endocrine</a></li>
        <li><a href="#cardiacstatus_roc">Cardiac Status</a></li>
        <li><a href="#eliminationstatus_roc">Elimination Status</a></li>
        <li><a href="#nutrition_roc">Nutrition</a></li>
        <li><a href="#behaviourialstatus_roc">Neuro/Behaviourial Status</a></li>
        <li><a href="#adl_roc">ADL/IADLs</a></li>
        <li><a href="#suppliesworksheet_roc">Supplies Worksheet</a></li>
        <li><a href="#medications_roc">Medications</a></li>
        <li><a href="#caremanagement_roc">Care Management</a></li>
        <li><a href="#therapyneed_roc">Therapy Need & Plan Of Care</a></li>
        <li><a href="#ordersdisciplinetreatment_roc">Orders for Discipline and Treatment</a></li>
    </ul>
   <!-- Add validation back in later -->
    <div id="clinicalRecord_roc" class="general"><% Html.RenderPartial("~/Views/Oasis/ResumptionofCare/Demographics.ascx", Model); %></div>
    <div id="patienthistory_roc" class="general loading"></div>
    <div id="riskassessment_roc" class="general loading"></div>
    <div id="prognosis_roc" class="general loading"></div>
    <div id="supportiveassistance_roc" class="general loading"></div>
    <div id="sensorystatus_roc" class="general loading"></div>
    <div id="pain_roc" class="general loading"></div>
    <div id="integumentarystatus_roc" class="general loading"></div>
    <div id="respiratorystatus_roc" class="general loading"></div>
    <div id="endocrine_roc" class="general loading"></div>
    <div id="cardiacstatus_roc" class="general loading"></div>
    <div id="eliminationstatus_roc" class="general loading"></div>
    <div id="nutrition_roc" class="general loading"></div>
    <div id="behaviourialstatus_roc" class="general loading"></div>
    <div id="adl_roc" class="general loading"></div>
    <div id="suppliesworksheet_roc" class="general loading"></div>
    <div id="medications_roc" class="general loading"></div>
    <div id="caremanagement_roc" class="general loading"></div>
    <div id="therapyneed_roc" class="general loading"></div>
    <div id="ordersdisciplinetreatment_roc" class="general loading"></div>
</div>
