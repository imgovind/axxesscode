﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.span("SN Frequency:",true) +
            printview.span("<%= data != null && data.ContainsKey("485SNFrequency") && data["485SNFrequency"].Answer.IsNotNullOrEmpty() ? data["485SNFrequency"].Answer : ""%>",false,1) +
            printview.span("PT Frequency:",true) +
            printview.span("<%= data != null && data.ContainsKey("485PTFrequency") && data["485PTFrequency"].Answer.IsNotNullOrEmpty() ? data["485PTFrequency"].Answer : ""%>",false,1) +
            printview.span("OT Frequency:",true) +
            printview.span("<%= data != null && data.ContainsKey("485OTFrequency") && data["485OTFrequency"].Answer.IsNotNullOrEmpty() ? data["485OTFrequency"].Answer : ""%>",false,1) +
            printview.span("ST Frequency:",true) +
            printview.span("<%= data != null && data.ContainsKey("485STFrequency") && data["485STFrequency"].Answer.IsNotNullOrEmpty() ? data["485STFrequency"].Answer : ""%>",false,1) +
            printview.span("MSW Frequency:",true) +
            printview.span("<%= data != null && data.ContainsKey("485MSWFrequency") && data["485MSWFrequency"].Answer.IsNotNullOrEmpty() ? data["485MSWFrequency"].Answer : ""%>",false,1) +
            printview.span("HHA Frequency:",true) +
            printview.span("<%= data != null && data.ContainsKey("485HHAFrequency") && data["485HHAFrequency"].Answer.IsNotNullOrEmpty() ? data["485HHAFrequency"].Answer : ""%>",false,1)) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485OrdersDisciplineInterventionComments") && data["485OrdersDisciplineInterventionComments"].Answer.IsNotNullOrEmpty() ? data["485OrdersDisciplineInterventionComments"].Answer : ""%>",false,2),
        "Orders for Discipline and Treatments");
    printview.addsection(
        printview.col(4,
            printview.checkbox("Skilled Intervention Needed",<%= data != null && data.ContainsKey("485Conclusions") && data["485Conclusions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Skilled Instruction Needed",<%= data != null && data.ContainsKey("485Conclusions") && data["485Conclusions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("No Skilled Service Needed",<%= data != null && data.ContainsKey("485Conclusions") && data["485Conclusions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.span("Other: <%= data != null && data.ContainsKey("485ConclusionOther") && data["485ConclusionOther"].Answer.IsNotNullOrEmpty() ? data["485ConclusionOther"].Answer : "<span class='blank'></span>"%>")),
        "Conclusions");
    printview.addsection(
        printview.col(2,
            printview.span("Rehabilitation potential for stated goals:",true) +
            printview.col(3,
                printview.checkbox("Good",<%= data != null && data.ContainsKey("485RehabilitationPotential") && data["485RehabilitationPotential"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Fair",<%= data != null && data.ContainsKey("485RehabilitationPotential") && data["485RehabilitationPotential"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Poor",<%= data != null && data.ContainsKey("485RehabilitationPotential") && data["485RehabilitationPotential"].Answer.Split(',').Contains("3") ? "true" : "false"%>))) +
        printview.span("Other rehabilitation potential:",true) +
        printview.span("<%= data != null && data.ContainsKey("485AchieveGoalsComments") && data["485AchieveGoalsComments"].Answer.IsNotNullOrEmpty() ? data["485AchieveGoalsComments"].Answer : ""%>",false,2),
        "Rehabilitation Potential");
    printview.addsection(
        printview.col(2,
            printview.span("Patient to be discharged to the care of:",true) +
            printview.col(3,
                printview.checkbox("Physician",<%= data != null && data.ContainsKey("485DischargePlans") && data["485DischargePlans"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Caregiver",<%= data != null && data.ContainsKey("485DischargePlans") && data["485DischargePlans"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Self care",<%= data != null && data.ContainsKey("485DischargePlans") && data["485DischargePlans"].Answer.Split(',').Contains("3") ? "true" : "false"%>)) +
            printview.span("Discharge Plans",true) +
            printview.checkbox("When caregiver willing &amp; able to manage all aspects of patient&rsquo;s care.",<%= data != null && data.ContainsKey("485DischargePlansReason") && data["485DischargePlansReason"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("When goals met.",<%= data != null && data.ContainsKey("485DischargePlansReason") && data["485DischargePlansReason"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("When wound(s) healed.",<%= data != null && data.ContainsKey("485DischargePlansReason") && data["485DischargePlansReason"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("When reliable caregiver available to assist w/patient&rsquo;s medical needs.",<%= data != null && data.ContainsKey("485DischargePlansReason") && data["485DischargePlansReason"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("When patient is independent in management of medical needs.",<%= data != null && data.ContainsKey("485DischargePlansReason") && data["485DischargePlansReason"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("When patient is independent in management of medical needs.",<%= data != null && data.ContainsKey("485DischargePlansReason") && data["485DischargePlansReason"].Answer.Split(',').Contains("6") ? "true" : "false"%>)) +
        printview.span("Additional discharge plans:",true) +
        printview.span("<%= data != null && data.ContainsKey("485DischargePlanComments") && data["485DischargePlanComments"].Answer.IsNotNullOrEmpty() ? data["485DischargePlanComments"].Answer : ""%>",false,2),
        "Discharge Plans");
    printview.addsection(
        printview.span("Skilled Intervention/Teaching:",true) +
        printview.span("<%= data != null && data.ContainsKey("485SkilledInterventionComments") && data["485SkilledInterventionComments"].Answer.IsNotNullOrEmpty() ? data["485SkilledInterventionComments"].Answer : ""%>",false,2) +
        printview.col(4,
            printview.span("Verbalized Understanding",true) +
            printview.col(2,
                printview.checkbox("Pt",<%= data != null && data.ContainsKey("485SIVerbalizedUnderstandingPT") && data["485SIVerbalizedUnderstandingPT"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("485SIVerbalizedUnderstandingPTPercent") && data["485SIVerbalizedUnderstandingPTPercent"].Answer.IsNotNullOrEmpty() ? data["485SIVerbalizedUnderstandingPTPercent"].Answer + "%" : ""%>") +
                printview.checkbox("CG",<%= data != null && data.ContainsKey("485SIVerbalizedUnderstandingCG") && data["485SIVerbalizedUnderstandingCG"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("485SIVerbalizedUnderstandingCGPercent") && data["485SIVerbalizedUnderstandingCGPercent"].Answer.IsNotNullOrEmpty() ? data["485SIVerbalizedUnderstandingCGPercent"].Answer + "%" : ""%>")) +
            printview.span("Return Demonstration",true) +
            printview.col(2,
                printview.checkbox("Pt",<%= data != null && data.ContainsKey("485SIReturnDemonstrationPT") && data["485SIReturnDemonstrationPT"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("485SIReturnDemonstrationPTPercent") && data["485SIReturnDemonstrationPTPercent"].Answer.IsNotNullOrEmpty() ? data["485SIReturnDemonstrationPTPercent"].Answer + "%" : ""%>") +
                printview.checkbox("CG",<%= data != null && data.ContainsKey("485SIReturnDemonstrationCG") && data["485SIReturnDemonstrationCG"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("485SIReturnDemonstrationCGPercent") && data["485SIReturnDemonstrationCGPercent"].Answer.IsNotNullOrEmpty() ? data["485SIReturnDemonstrationCGPercent"].Answer + "%" : ""%>"))),
        "Narrative");
    printview.addsection(
        printview.col(4,
            printview.span("Conferenced With:",true) +
            printview.span("<%= data != null && data.ContainsKey("485ConferencedWith") && data["485ConferencedWith"].Answer.IsNotNullOrEmpty() && data["485ConferencedWith"].Answer != "0" ? data["485ConferencedWith"].Answer : "" %>",false,1) +
            printview.span("Name:",true) +
            printview.span("<%= data != null && data.ContainsKey("485ConferencedWithName") && data["485ConferencedWithName"].Answer.IsNotNullOrEmpty() ? data["485ConferencedWithName"].Answer : ""%>",false,1)) +
        printview.span("Regarding:",true) +
        printview.span("<%= data != null && data.ContainsKey("485SkilledInterventionRegarding") && data["485SkilledInterventionRegarding"].Answer.IsNotNullOrEmpty() ? data["485SkilledInterventionRegarding"].Answer : ""%>",false,2),
        "Cordination of Care");
</script>