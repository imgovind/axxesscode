﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.checkbox("Complete bed rest",<%= data != null && data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Bed rest with BRP",<%= data != null && data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Up as tolerated",<%= data != null && data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Transfer bed-chair",<%= data != null && data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Exercise prescribed",<%= data != null && data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Partial weight bearing",<%= data != null && data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Independent at home",<%= data != null && data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Crutches",<%= data != null && data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
            printview.checkbox("Cane",<%= data != null && data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
            printview.checkbox("Wheelchair",<%= data != null && data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer.Split(',').Contains("A") ? "true" : "false"%>) +
            printview.checkbox("Walker",<%= data != null && data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer.Split(',').Contains("B") ? "true" : "false"%>) +
            printview.span("Other: <%= data != null && data.ContainsKey("485ActivitiesPermittedOther") && data["485ActivitiesPermittedOther"].Answer.IsNotNullOrEmpty() ? data["485ActivitiesPermittedOther"].Answer : "<span class='blank'></span>"%>")),
        "Activities Permitted");
    printview.addsection(
        printview.col(2,
            printview.checkbox("WNL (Within Normal Limits)",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.col(2,
                printview.checkbox("Impaired Motor Skill:",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true) +
                printview.span("<%= data != null && data.ContainsKey("GenericMusculoskeletalImpairedMotorSkills") && data["GenericMusculoskeletalImpairedMotorSkills"].Answer.IsNotNullOrEmpty() ? (data["GenericMusculoskeletalImpairedMotorSkills"].Answer == "0" ? "" : "") + (data["GenericMusculoskeletalImpairedMotorSkills"].Answer == "1" ? "N/A" : "") + (data["GenericMusculoskeletalImpairedMotorSkills"].Answer == "2" ? "Fine" : "") + (data["GenericMusculoskeletalImpairedMotorSkills"].Answer == "3" ? "Gross" : "") : ""%>",false,1))) +
        printview.col(5,
            printview.checkbox("Grip Strength:",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.span("<%= data != null && data.ContainsKey("GenericMusculoskeletalHandGrips") && data["GenericMusculoskeletalHandGrips"].Answer.IsNotNullOrEmpty() ? (data["GenericMusculoskeletalHandGrips"].Answer == "0" ? "" : "") + (data["GenericMusculoskeletalHandGrips"].Answer == "1" ? "Strong" : "") + (data["GenericMusculoskeletalHandGrips"].Answer == "2" ? "Weak" : "") + (data["GenericMusculoskeletalHandGrips"].Answer == "3" ? "Other" : "") : ""%>",false,1) +
            printview.checkbox("Bilateral",<%= data != null && data.ContainsKey("GenericMusculoskeletalHandGripsPosition") && data["GenericMusculoskeletalHandGripsPosition"].Answer.Split(',').Contains("0") ? "true" : "false"%>) +
            printview.checkbox("Left",<%= data != null && data.ContainsKey("GenericMusculoskeletalHandGripsPosition") && data["GenericMusculoskeletalHandGripsPosition"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Right",<%= data != null && data.ContainsKey("GenericMusculoskeletalHandGripsPosition") && data["GenericMusculoskeletalHandGripsPosition"].Answer.Split(',').Contains("2") ? "true" : "false"%>)) +
        printview.col(2,
            printview.col(2,
                printview.checkbox("Limited ROM:",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true) +
                printview.span("Location: <%= data != null && data.ContainsKey("GenericLimitedROMLocation") && data["GenericLimitedROMLocation"].Answer.IsNotNullOrEmpty() ? data["GenericLimitedROMLocation"].Answer  : "<span class='blank'></span>"%>")) +
            printview.col(2,
                printview.checkbox("Mobility:",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("5") ? "true" : "false"%>,true) +
                printview.span("<%= data != null && data.ContainsKey("GenericMusculoskeletalMobility") && data["GenericMusculoskeletalMobility"].Answer.IsNotNullOrEmpty() ? (data["GenericMusculoskeletalMobility"].Answer == "0" ? "" : "") + (data["GenericMusculoskeletalMobility"].Answer == "1" ? "WNL" : "") + (data["GenericMusculoskeletalMobility"].Answer == "2" ? "Ambulatory" : "") + (data["GenericMusculoskeletalMobility"].Answer == "3" ? "Ambulatory w/assistance" : "") + (data["GenericMusculoskeletalMobility"].Answer == "4" ? "Chair fast" : "") + (data["GenericMusculoskeletalMobility"].Answer == "5" ? "Bedfast" : "") + (data["GenericMusculoskeletalMobility"].Answer == "6" ? "Non-ambulatory" : "") : ""%>",false,1))) +
        printview.col(6,
            printview.checkbox("Assistive Device:",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("6") ? "true" : "false"%>,true) +
            printview.checkbox("Cane",<%= data != null && data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Crutches",<%= data != null && data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Walker",<%= data != null && data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Wheelchair",<%= data != null && data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericAssistiveDeviceOther") && data["GenericAssistiveDeviceOther"].Answer.IsNotNullOrEmpty() ? data["GenericAssistiveDeviceOther"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer.Split(',').Contains("5") ? "true" : "false"%>)) +
        printview.col(2,
            printview.col(2,
                printview.checkbox("Contracture:",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("7") ? "true" : "false"%>,true) +
                printview.span("Location: <%= data != null && data.ContainsKey("GenericContractureLocation") && data["GenericContractureLocation"].Answer.IsNotNullOrEmpty() ? data["GenericContractureLocation"].Answer : ""%>")) +
            printview.checkbox("Weakness",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("8") ? "true" : "false"%>,true) +
            printview.col(2,
                printview.checkbox("Joint Pain:",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("9") ? "true" : "false"%>,true) +
                printview.span("Location: <%= data != null && data.ContainsKey("GenericJointPainLocation") && data["GenericJointPainLocation"].Answer.IsNotNullOrEmpty() ? data["GenericJointPainLocation"].Answer : "<span class='blank'></span>"%>")) +
            printview.checkbox("Poor Balance",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("10") ? "true" : "false"%>,true) +
            printview.checkbox("Joint Stiffness",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("11") ? "true" : "false"%>,true) +
            printview.col(2,
                printview.checkbox("Amputation:",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("12") ? "true" : "false"%>,true) +
                printview.span("Location: <%= data != null && data.ContainsKey("GenericAmputationLocation") && data["GenericAmputationLocation"].Answer.IsNotNullOrEmpty() ? data["GenericAmputationLocation"].Answer : "<span class='blank'></span>"%>"))) +
        printview.col(4,
            printview.checkbox("Weight Bearing Restrictions:",<%= data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer.Split(',').Contains("13") ? "true" : "false"%>,true) +
            printview.span("Location: <%= data != null && data.ContainsKey("GenericWeightBearingRestrictionLocation") && data["GenericWeightBearingRestrictionLocation"].Answer.IsNotNullOrEmpty() ? data["GenericWeightBearingRestrictionLocation"].Answer : "<span class='blank'></span>"%>") +
            printview.checkbox("Full",<%= data != null && data.ContainsKey("GenericWeightBearingRestriction") && data["GenericWeightBearingRestriction"].Answer.Split(',').Contains("0") ? "true" : "false"%>) +
            printview.checkbox("Partial",<%= data != null && data.ContainsKey("GenericWeightBearingRestriction") && data["GenericWeightBearingRestriction"].Answer.Split(',').Contains("1") ? "true" : "false"%>)) +
        printview.span("Comments:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericMusculoskeletalComments") && data["GenericMusculoskeletalComments"].Answer.IsNotNullOrEmpty() ? data["GenericMusculoskeletalComments"].Answer : ""%>",false,2),
        "Musculoskeletal");
    printview.addsection(
        printview.span("(M1800) Grooming: Current ability to tend safely to personal hygiene needs (i.e., washing face and hands, hair care, shaving or make up, teeth or denture care, fingernail care).",true) +
        printview.checkbox("0 &ndash; Able to groom self unaided, with or without the use of assistive devices or adapted methods.",<%= data != null && data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Grooming utensils must be placed within reach before able to complete grooming activities.",<%= data != null && data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Someone must assist the patient to groom self.",<%= data != null && data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Patient depends entirely upon someone else for grooming needs.",<%= data != null && data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer.Split(',').Contains("03") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1810) Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts and blouses, managing zippers, buttons, and snaps:",true) +
        printview.checkbox("0 &ndash; Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.",<%= data != null && data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Able to dress upper body without assistance if clothing is laid out or handed to the patient.",<%= data != null && data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Someone must help the patient put on upper body clothing.",<%= data != null && data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Patient depends entirely upon another person to dress the upper body.",<%= data != null && data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer.Split(',').Contains("03") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1820) Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes:",true) +
        printview.checkbox("0 &ndash; Able to obtain, put on, and remove clothing and shoes without assistance.",<%= data != null && data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.",<%= data != null && data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.",<%= data != null && data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Patient depends entirely upon another person to dress lower body.",<%= data != null && data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer.Split(',').Contains("03") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).",true) +
        printview.checkbox("0 &ndash; Able to bathe self in shower or tub independently, including getting in and out of tub/shower.",<%= data != null && data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.",<%= data != null && data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Able to bathe in shower or tub with the intermittent assistance of another person:<ul><li>(a) for intermittent supervision or encouragement or reminders, OR</li><li>(b) to get in and out of the shower or tub, OR</li><li>(c) for washing difficult to reach areas.</li></ul>",<%= data != null && data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.",<%= data != null && data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.",<%= data != null && data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the assistance or supervision of another person throughout the bath.",<%= data != null && data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
        printview.checkbox("6 &ndash; Unable to participate effectively in bathing and is bathed totally by another person.",<%= data != null && data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',').Contains("06") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.",true) +
        printview.checkbox("0 &ndash; Able to get to and from the toilet and transfer independently with or without a device.",<%= data != null && data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.",<%= data != null && data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).",<%= data != null && data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.",<%= data != null && data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Is totally dependent in toileting.",<%= data != null && data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer.Split(',').Contains("04") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1845) Toileting Hygiene: Current ability to maintain perineal hygiene safely, adjust clothes and/or incontinence pads before and after using toilet, commode, bedpan, urinal. If managing ostomy, includes cleaning area around stoma, but not managing equipment.",true) +
        printview.checkbox("0 &ndash; Able to manage toileting hygiene and clothing management without assistance.",<%= data != null && data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Able to manage toileting hygiene and clothing management without assistance if supplies/implements are laid out for the patient.",<%= data != null && data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Someone must help the patient to maintain toileting hygiene and/or adjust clothing.",<%= data != null && data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Patient depends entirely upon another person to maintain toileting hygiene.",<%= data != null && data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer.Split(',').Contains("03") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1850) Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.",true) +
        printview.checkbox("0 &ndash; Able to independently transfer.",<%= data != null && data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Able to transfer with minimal human assistance or with use of an assistive device.",<%= data != null && data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Able to bear weight and pivot during the transfer process but unable to transfer self.",<%= data != null && data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Unable to transfer self and is unable to bear weight or pivot when transferred by another person.",<%= data != null && data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Bedfast, unable to transfer but is able to turn and position self in bed.",<%= data != null && data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; Bedfast, unable to transfer and is unable to turn and position self.",<%= data != null && data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer.Split(',').Contains("05") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.",true) +
        printview.checkbox("0 &ndash; Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (i.e., needs no human assistance or assistive device).",<%= data != null && data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.",<%= data != null && data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Requires use of a two-handed device (e.g., walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.",<%= data != null && data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Able to walk only with the supervision or assistance of another person at all times.",<%= data != null && data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Chairfast, unable to ambulate but is able to wheel self independently.",<%= data != null && data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; Chairfast, unable to ambulate and is unable to wheel self.",<%= data != null && data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
        printview.checkbox("6 &ndash; Bedfast, unable to ambulate or be up in a chair.",<%= data != null && data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer.Split(',').Contains("06") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1870) Feeding or Eating: Current ability to feed self meals and snacks safely. Note: This refers only to the process of eating, chewing, and swallowing, not preparing the food to be eaten.",true) +
        printview.checkbox("0 &ndash; Able to independently feed self.",<%= data != null && data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Able to feed self independently but requires:<ul><li>(a) meal set-up; OR</li><li>(b) intermittent assistance or supervision from another person; OR</li><li>(c) a liquid, pureed or ground meat diet.</li></ul>",<%= data != null && data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Unable to feed self and must be assisted or supervised throughout the meal/snack.",<%= data != null && data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Able to take in nutrients orally and receives supplemental nutrients through a nasogastric tube or gastrostomy.",<%= data != null && data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Unable to take in nutrients orally and is fed nutrients through a nasogastric tube or gastrostomy.",<%= data != null && data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; Unable to take in nutrients orally or by tube feeding.",<%= data != null && data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer.Split(',').Contains("05") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1880) Current Ability to Plan and Prepare Light Meals (e.g., cereal, sandwich) or reheat delivered meals safely:",true) +
        printview.checkbox("0 &ndash; <ul><li>(a) Able to independently plan and prepare all light meals for self or reheat delivered meals; OR</li><li>(b) Is physically, cognitively, and mentally able to prepare light meals on a regular basis but has not routinely performed light meal preparation in the past (i.e., prior to this home care admission).</li></ul>",<%= data != null && data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Unable to prepare light meals on a regular basis due to physical, cognitive, or mental limitations.",<%= data != null && data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Unable to prepare any light meals or reheat any delivered meals.",<%= data != null && data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer.Split(',').Contains("02") ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1890) Ability to Use Telephone: Current ability to answer the phone safely, including dialing numbers, and effectively using the telephone to communicate.",true) +
        printview.checkbox("0 &ndash; Able to dial numbers and answer calls appropriately and as desired.",<%= data != null && data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Able to use a specially adapted telephone (i.e., large numbers on the dial, teletype phone for the deaf) and call essential numbers.",<%= data != null && data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Able to answer the telephone and carry on a normal conversation but has difficulty with placing calls.",<%= data != null && data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer.Split(',').Contains("02") ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Able to answer the telephone only some of the time or is able to carry on only a limited conversation.",<%= data != null && data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer.Split(',').Contains("03") ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Unable to answer the telephone at all but can listen if assisted with equipment.",<%= data != null && data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer.Split(',').Contains("04") ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; Totally unable to use the telephone.",<%= data != null && data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer.Split(',').Contains("05") ? "true" : "false"%>) +
        printview.checkbox("NA &ndash; Patient does not have a telephone.",<%= data != null && data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer.Split(',').Contains("NA") ? "true" : "false"%>));
    printview.addsection(
        printview.checkbox("Physical therapy to evaluate.",<%= data != null && data.ContainsKey("485NursingInterventions") && data["485NursingInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("Occupational therapy to evaluate.",<%= data != null && data.ContainsKey("485NursingInterventions") && data["485NursingInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("SN to assess/instruct on pain management, proper body mechanics and safety measures.",<%= data != null && data.ContainsKey("485NursingInterventions") && data["485NursingInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("SN to assess for patient adherence to appropriate activity levels.",<%= data != null && data.ContainsKey("485NursingInterventions") && data["485NursingInterventions"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.checkbox("SN to assess patient&rsquo;s compliance with home exercise program.",<%= data != null && data.ContainsKey("485NursingInterventions") && data["485NursingInterventions"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct the <%= data != null && data.ContainsKey("485InstructRomExcercisePerson") && data["485InstructRomExcercisePerson"].Answer.IsNotNullOrEmpty() ? data["485InstructRomExcercisePerson"].Answer : "<span class='blank'></span>"%> on proper ROM exercises and body alignment techniques. ",<%= data != null && data.ContainsKey("485NursingInterventions") && data["485NursingInterventions"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485ADLComments") && data["485ADLComments"].Answer.IsNotNullOrEmpty() ? data["485ADLComments"].Answer : ""%>",false,2),
        "Interventions");
    printview.addsection(
        printview.checkbox("Patient will have increased mobility, self care, endurance, ROM and decreased pain by the end of the episode.",<%= data != null && data.ContainsKey("485NursingGoals") && data["485NursingGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("Patient will maintain optimal joint function, increased mobility and independence in ADL&rsquo;s by the end of the episode.",<%= data != null && data.ContainsKey("485NursingGoals") && data["485NursingGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("Patient&rsquo;s strength, endurance and mobility will be improved.",<%= data != null && data.ContainsKey("485NursingGoals") && data["485NursingGoals"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("The <%= data != null && data.ContainsKey("485DemonstrateROMExcercisePerson") && data["485DemonstrateROMExcercisePerson"].Answer.IsNotNullOrEmpty() ? data["485DemonstrateROMExcercisePerson"].Answer : "<span class='blank'></span>"%> will demonstrate proper ROM exercise and body alignment techniques.",<%= data != null && data.ContainsKey("485NursingGoals") && data["485NursingGoals"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.span("Additional Goals:",true) +
        printview.span("<%= data != null && data.ContainsKey("485ADLGoalComments") && data["485ADLGoalComments"].Answer.IsNotNullOrEmpty() ? data["485ADLGoalComments"].Answer : ""%>",false,2),
        "Goals");
    printview.addsection(
        printview.span("(M1900) Prior Functioning ADL/IADL: Indicate the patient&rsquo;s usual ability with everyday activities prior to this current illness, exacerbation, or injury. Check only one box in each row.",true) +
        printview.col(2,
            printview.span("a. Self-Care (e.g., grooming, dressing, and bathing)") +
            printview.col(3,
                printview.checkbox("0 &ndash; Independent",<%= data != null && data.ContainsKey("M1900SelfCareFunctioning") && data["M1900SelfCareFunctioning"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
                printview.checkbox("1 &ndash; Need Some Help",<%= data != null && data.ContainsKey("M1900SelfCareFunctioning") && data["M1900SelfCareFunctioning"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
                printview.checkbox("2 &ndash; Dependent",<%= data != null && data.ContainsKey("M1900SelfCareFunctioning") && data["M1900SelfCareFunctioning"].Answer.Split(',').Contains("02") ? "true" : "false"%>)) +
            printview.span("b. Ambulation") +
            printview.col(3,
                printview.checkbox("0 &ndash; Independent",<%= data != null && data.ContainsKey("M1900Ambulation") && data["M1900Ambulation"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
                printview.checkbox("1 &ndash; Need Some Help",<%= data != null && data.ContainsKey("M1900Ambulation") && data["M1900Ambulation"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
                printview.checkbox("2 &ndash; Dependent",<%= data != null && data.ContainsKey("M1900Ambulation") && data["M1900Ambulation"].Answer.Split(',').Contains("02") ? "true" : "false"%>)) +
            printview.span("c. Transfer") +
            printview.col(3,
                printview.checkbox("0 &ndash; Independent",<%= data != null && data.ContainsKey("M1900Transfer") && data["M1900Transfer"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
                printview.checkbox("1 &ndash; Need Some Help",<%= data != null && data.ContainsKey("M1900Transfer") && data["M1900Transfer"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
                printview.checkbox("2 &ndash; Dependent",<%= data != null && data.ContainsKey("M1900Transfer") && data["M1900Transfer"].Answer.Split(',').Contains("02") ? "true" : "false"%>)) +
            printview.span("d. Household tasks (e.g., light meal preparation, laundry, shopping)") +
            printview.col(3,
                printview.checkbox("0 &ndash; Independent",<%= data != null && data.ContainsKey("M1900HouseHoldTasks") && data["M1900HouseHoldTasks"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
                printview.checkbox("1 &ndash; Need Some Help",<%= data != null && data.ContainsKey("M1900HouseHoldTasks") && data["M1900HouseHoldTasks"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
                printview.checkbox("2 &ndash; Dependent",<%= data != null && data.ContainsKey("M1900HouseHoldTasks") && data["M1900HouseHoldTasks"].Answer.Split(',').Contains("02") ? "true" : "false"%>))));
    printview.addsection(
        printview.col(2,
            printview.span("Age 65+",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericAge65Plus") && data["GenericAge65Plus"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericAge65Plus") && data["GenericAge65Plus"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Diagnosis (3 or more co-existing)",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericHypotensionDiagnosis") && data["GenericHypotensionDiagnosis"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericHypotensionDiagnosis") && data["GenericHypotensionDiagnosis"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Prior history of falls within 3 months",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericPriorFalls") && data["GenericPriorFalls"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericPriorFalls") && data["GenericPriorFalls"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Incontinence",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericFallIncontinence") && data["GenericFallIncontinence"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericFallIncontinence") && data["GenericFallIncontinence"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Visual impairment",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericVisualImpairment") && data["GenericVisualImpairment"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericVisualImpairment") && data["GenericVisualImpairment"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Impaired functional mobility",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericImpairedFunctionalMobility") && data["GenericImpairedFunctionalMobility"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericImpairedFunctionalMobility") && data["GenericImpairedFunctionalMobility"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Environmental hazards",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericEnvHazards") && data["GenericEnvHazards"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericEnvHazards") && data["GenericEnvHazards"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Poly Pharmacy (4 or more prescriptions)",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericPolyPharmacy") && data["GenericPolyPharmacy"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericPolyPharmacy") && data["GenericPolyPharmacy"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Pain affecting level of function",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericPainAffectingFunction") && data["GenericPainAffectingFunction"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericPainAffectingFunction") && data["GenericPainAffectingFunction"].Answer == "0" ? "true" : "false"%>)) +
            printview.span("Cognitive impairment",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericCognitiveImpairment") && data["GenericCognitiveImpairment"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericCognitiveImpairment") && data["GenericCognitiveImpairment"].Answer == "0" ? "true" : "false"%>))),
        "Fall Assessment");
    printview.addsection(
        printview.span("(M1910) Has this patient had a multi-factor Fall Risk Assessment (such as falls history, use of multiple medications, mental impairment, toileting frequency, general mobility/transferring impairment, environmental hazards)?",true) +
        printview.col(3,
            printview.checkbox("0 &ndash; No falls risk assessment conducted.",<%= data != null && data.ContainsKey("M1910FallRiskAssessment") && data["M1910FallRiskAssessment"].Answer.Split(',').Contains("00") ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes &amp; it does not indicate a risk for falls.",<%= data != null && data.ContainsKey("M1910FallRiskAssessment") && data["M1910FallRiskAssessment"].Answer.Split(',').Contains("01") ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Yes &amp; it indicates a risk for falls.",<%= data != null && data.ContainsKey("M1910FallRiskAssessment") && data["M1910FallRiskAssessment"].Answer.Split(',').Contains("02") ? "true" : "false"%>)));
    printview.addsection(
        printview.checkbox("SN to instruct patient to wear proper footwear when ambulating.",<%= data != null && data.ContainsKey("485InstructInterventions") && data["485InstructInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct patient to use prescribed assistive device when ambulating.",<%= data != null && data.ContainsKey("485InstructInterventions") && data["485InstructInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct patient to change positions slowly.",<%= data != null && data.ContainsKey("485InstructInterventions") && data["485InstructInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct the <%= data != null && data.ContainsKey("485InstructRemoveClutterPerson") && data["485InstructRemoveClutterPerson"].Answer.IsNotNullOrEmpty() ? data["485InstructRemoveClutterPerson"].Answer : "<span class='blank'></span>"%> to remove clutter from patient&rsquo;s path such as clothes, books, shoes, electrical cords, or other items that may cause patient to trip.",<%= data != null && data.ContainsKey("485InstructInterventions") && data["485InstructInterventions"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct the <%= data != null && data.ContainsKey("485InstructContactForFallPerson") && data["485InstructContactForFallPerson"].Answer.IsNotNullOrEmpty() ? data["485InstructContactForFallPerson"].Answer : "<span class='blank'></span>"%> to contact agency to report any fall with or without minor injury and to call 911 for fall resulting in serious injury or causing severe pain or immobility.",<%= data != null && data.ContainsKey("485InstructInterventions") && data["485InstructInterventions"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.checkbox("HHA to assist with ADL&rsquo;s &amp; IADL&rsquo;s per HHA care plan.",<%= data != null && data.ContainsKey("485InstructInterventions") && data["485InstructInterventions"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485IADLComments") && data["485IADLComments"].Answer.IsNotNullOrEmpty() ? data["485IADLComments"].Answer : ""%>",false,2),
        "Interventions");
    printview.addsection(
        printview.checkbox("The patient will be free from falls during the episode.",<%= data != null && data.ContainsKey("485InstructGoals") && data["485InstructGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("The patient will be free from injury during the episode.",<%= data != null && data.ContainsKey("485InstructGoals") && data["485InstructGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.span("Additional Goals:",true) +
        printview.span("<%= data != null && data.ContainsKey("485IADLGoalComments") && data["485IADLGoalComments"].Answer.IsNotNullOrEmpty() ? data["485IADLGoalComments"].Answer : ""%>",false,2),
        "Goals");
</script>