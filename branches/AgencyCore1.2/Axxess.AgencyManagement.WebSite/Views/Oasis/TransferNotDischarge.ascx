﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
            "<script type='text/javascript'>acore.renamewindow('Oasis-C Transfer, Not Discharge | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','transfernotdischarge');</script>")
%>
<div id="transferInPatientNotDischargedTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_nottransfer">Clinical Record Items</a></li>
        <li><a href="#riskassessment_nottransfer">Risk Assessment</a></li>
        <li><a href="#cardiacstatus_nottransfer">Cardiac Status</a></li>
        <li><a href="#medications_nottransfer">Medications</a></li>
        <li><a href="#emergentcare_nottransfer">Emergent Care</a></li>
        <li><a href="#dischargeAdd_nottransfer">Transfer</a></li>
    </ul>
    <div id="clinicalRecord_nottransfer" class="general">
     <% Html.RenderPartial("~/Views/Oasis/TransferNotDischarge/Demographics.ascx", Model); %>
    </div>
    <div id="riskassessment_nottransfer" class="general loading">
    </div>
    <div id="cardiacstatus_nottransfer" class="general loading">
    </div>
    <div id="medications_nottransfer" class="general loading">
    </div>
    <div id="emergentcare_nottransfer" class="general loading">
    </div>
    <div id="dischargeAdd_nottransfer" class="general loading">
    </div>
</div>
