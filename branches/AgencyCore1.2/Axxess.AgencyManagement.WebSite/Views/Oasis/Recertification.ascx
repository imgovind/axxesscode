﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
            "<script type='text/javascript'>acore.renamewindow('Oasis-C Recertification | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','recertification');</script>")
%>
<div id="recertificationTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_recertification">Clinical Record Items</a></li>
        <li><a href="#patienthistory_recertification">Patient History & Diagnoses</a></li>
        <li><a href="#prognosis_recertification">Prognosis</a></li>
        <li><a href="#supportiveassistance_recertification">Supportive Assistance</a></li>
        <li><a href="#sensorystatus_recertification">Sensory Status</a></li>
        <li><a href="#pain_recertification">Pain</a></li>
        <li><a href="#integumentarystatus_recertification">Integumentary Status</a></li>
        <li><a href="#respiratorystatus_recertification">Respiratory Status</a></li>
        <li><a href="#endocrine_recertification">Endocrine</a></li>
        <li><a href="#cardiacstatus_recertification">Cardiac Status</a></li>
        <li><a href="#eliminationstatus_recertification">Elimination Status</a></li>
        <li><a href="#nutrition_recertification">Nutrition</a></li>
        <li><a href="#behaviourialstatus_recertification">Neuro/Behaviourial Status</a></li>
        <li><a href="#adl_recertification">ADL/IADLs</a></li>
        <li><a href="#suppliesworksheet_recertification">Supplies Worksheet</a></li>
        <li><a href="#medications_recertification">Medications</a></li>
        <li><a href="#therapyneed_recertification">Therapy Need & Plan Of Care</a></li>
        <li><a href="#ordersdisciplinetreatment_recertification">Orders for Discipline and Treatment</a></li>
    </ul>
    <!-- Add validation back in later -->
    <div id="clinicalRecord_recertification" class="general"><% Html.RenderPartial("~/Views/Oasis/Recertification/Demographics.ascx", Model); %></div>
    <div id="patienthistory_recertification" class="general loading"></div>
    <div id="prognosis_recertification" class="general loading"></div>
    <div id="supportiveassistance_recertification" class="general loading"></div>
    <div id="sensorystatus_recertification" class="general loading"></div>
    <div id="pain_recertification" class="general loading"></div>
    <div id="integumentarystatus_recertification" class="general loading"></div>
    <div id="respiratorystatus_recertification" class="general loading"></div>
    <div id="endocrine_recertification" class="general loading"></div>
    <div id="cardiacstatus_recertification" class="general loading"></div>
    <div id="eliminationstatus_recertification" class="general loading"></div>
    <div id="nutrition_recertification" class="general loading"></div>
    <div id="behaviourialstatus_recertification" class="general loading"></div>
    <div id="adl_recertification" class="general loading"></div>
    <div id="suppliesworksheet_recertification" class="general loading"></div>
    <div id="medications_recertification" class="general loading"></div>
    <div id="therapyneed_recertification" class="general loading"></div>
    <div id="ordersdisciplinetreatment_recertification" class="general loading"></div>
</div>
<div id="Recertification_ValidationContainer" class="hidden"></div>