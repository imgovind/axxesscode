﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisTransferInPatientNotDischargedCardiacForm" })) {
        var data = Model.ToDictionary(); %>
        <%= Html.Hidden("TransferInPatientNotDischarged_Id", Model.Id)%>
        <%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
        <%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", Model.PatientId)%>
        <%= Html.Hidden("TransferInPatientNotDischarged_EpisodeId", Model.EpisodeId)%>
        <%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS M1500</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1500');">(M1500)</a> Symptoms in Heart Failure Patients: If patient has been diagnosed with heart failure, did the patient exhibit symptoms indicated by clinical heart failure guidelines (including dyspnea, orthopnea, edema, or weight gain) at any point since the previous OASIS assessment?</div>
                <%=Html.Hidden("TransferInPatientNotDischarged_M1500HeartFailureSymptons", " ", new { @id = "" })%>
                <div class="margin">
                    <div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1500HeartFailureSymptons", "00", data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "00" ? true : false, new { @id = "TransferInPatientNotDischarged_M1500HeartFailureSymptons0", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1500HeartFailureSymptons0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1500HeartFailureSymptons", "01", data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "01" ? true : false, new { @id = "TransferInPatientNotDischarged_M1500HeartFailureSymptons1", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1500HeartFailureSymptons1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                    </div><div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1500HeartFailureSymptons", "02", data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "02" ? true : false, new { @id = "TransferInPatientNotDischarged_M1500HeartFailureSymptons2", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1500HeartFailureSymptons2"><span class="float_left">2 &ndash;</span><span class="normal margin">Not assessed</span></label>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1500');">?</div>
                        </div>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M1500HeartFailureSymptons", "NA", data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "NA" ? true : false, new { @id = "TransferInPatientNotDischarged_M1500HeartFailureSymptonsNA", @class = "radio float_left" })%>
                        <label for="TransferInPatientNotDischarged_M1500HeartFailureSymptonsNA"><span class="float_left">NA</span><span class="normal margin">Patient does not have diagnosis of heart failure</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="TransferInPatientNotDischarged_M1510" class="oasis">
        <legend>OASIS M1510</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1510');">(M1510)</a> Heart Failure Follow-up: If patient has been diagnosed with heart failure and has exhibited symptoms indicative of heart failure since the previous OASIS assessment, what action(s) has (have) been taken to respond? (Mark all that apply.)</div>
                <div class="margin">
                    <div>
                        <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowupNoAction" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientNotDischarged_M1510HeartFailureFollowupNoAction' class='radio float_left' name='TransferInPatientNotDischarged_M1510HeartFailureFollowupNoAction' type='checkbox' value='1' {0} />", data.ContainsKey("M1510HeartFailureFollowupNoAction") && data["M1510HeartFailureFollowupNoAction"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientNotDischarged_M1510HeartFailureFollowupNoAction"><span class="float_left">0 &ndash;</span><span class="normal margin">No action taken</span></label>
                    </div><div>
                        <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowupPhysicianCon" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientNotDischarged_M1510HeartFailureFollowupPhysicianCon' class='radio float_left' name='TransferInPatientNotDischarged_M1510HeartFailureFollowupPhysicianCon' type='checkbox' value='1' {0} />", data.ContainsKey("M1510HeartFailureFollowupPhysicianCon") && data["M1510HeartFailureFollowupPhysicianCon"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientNotDischarged_M1510HeartFailureFollowupPhysicianCon"><span class="float_left">1 &ndash;</span><span class="normal margin">Patient&rsquo;s physician (or other primary care practitioner) contacted the same day</span></label>
                    </div><div>
                        <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowupAdvisedEmg" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientNotDischarged_M1510HeartFailureFollowupAdvisedEmg' class='radio float_left' name='TransferInPatientNotDischarged_M1510HeartFailureFollowupAdvisedEmg' type='checkbox' value='1' {0} />", data.ContainsKey("M1510HeartFailureFollowupAdvisedEmg") && data["M1510HeartFailureFollowupAdvisedEmg"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientNotDischarged_M1510HeartFailureFollowupAdvisedEmg"><span class="float_left">2 &ndash;</span><span class="normal margin">Patient advised to get emergency treatment (e.g., call 911 or go to emergency room)</span></label>
                    </div><div>
                        <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowupParameters" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientNotDischarged_M1510HeartFailureFollowupParameters' class='radio float_left' name='TransferInPatientNotDischarged_M1510HeartFailureFollowupParameters' type='checkbox' value='1' {0} />", data.ContainsKey("M1510HeartFailureFollowupParameters") && data["M1510HeartFailureFollowupParameters"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientNotDischarged_M1510HeartFailureFollowupParameters"><span class="float_left">3 &ndash;</span><span class="normal margin">Implemented physician-ordered patient-specific established parameters for treatment</span></label>
                    </div><div>
                        <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowupInterventions" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientNotDischarged_M1510HeartFailureFollowupInterventions' class='radio float_left' name='TransferInPatientNotDischarged_M1510HeartFailureFollowupInterventions' type='checkbox' value='1' {0} />", data.ContainsKey("M1510HeartFailureFollowupInterventions") && data["M1510HeartFailureFollowupInterventions"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientNotDischarged_M1510HeartFailureFollowupInterventions"><span class="float_left">4 &ndash;</span><span class="normal margin">Patient education or other clinical interventions</span></label>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1510');">?</div>
                        </div>
                        <input name="TransferInPatientNotDischarged_M1510HeartFailureFollowupChange" type="hidden" value=" " />
                        <%= string.Format("<input id='TransferInPatientNotDischarged_M1510HeartFailureFollowupChange' class='radio float_left' name='TransferInPatientNotDischarged_M1510HeartFailureFollowupNoAction' type='checkbox' value='1' {0} />", data.ContainsKey("M1510HeartFailureFollowupNoAction") && data["M1510HeartFailureFollowupNoAction"].Answer == "1" ? "checked='checked'" : "") %>
                        <label for="TransferInPatientNotDischarged_M1510HeartFailureFollowupChange"><span class="float_left">5 &ndash;</span><span class="normal margin">Obtained change in care plan orders (e.g., increased monitoring by agency, change in visit frequency, telehealth, etc.)</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="TransferNotDischarge.FormSubmit($(this));">Save/Continue</a></li>
            <li><a href="javascript:void(0);" onclick="TransferNotDischarge.FormSubmit($(this));">Save/Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.Validate('{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "TransferInPatientNotDischarged")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.hideIfRadioEquals("TransferInPatientNotDischarged_M1500HeartFailureSymptons", "00|02|NA", $("#TransferInPatientNotDischarged_M1510"));
    Oasis.noneOfTheAbove($("#TransferInPatientNotDischarged_M1510HeartFailureFollowupNoAction"), $("#TransferInPatientNotDischarged_M1510 input"));
</script>