﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Assessment>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<% var data = Model.ToDictionary(); %>
<head>
    <%= string.Format("<title>Transfer Not Discharge | {0}{1}, {2}{3}</title>",data.ContainsKey("M0040LastName")?data["M0040LastName"].Answer:"",data.ContainsKey("M0040Suffix")?" "+data["M0040Suffix"].Answer:"",data.ContainsKey("M0040FirstName")?data["M0040FirstName"].Answer:"",data.ContainsKey("M0040MI")?" "+data["M0040MI"].Answer:"") %>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body>
    <div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Discharge</h1>
        <h2>Demographics</h2>
        <div class="tricol">
            <span><strong>Time In:</strong>00:00</span>
            <span><strong>Time Out:</strong>00:00</span>
            <span><strong>Visit Date:</strong>00/00/0000</span>
        </div><div class="bicol">
            <span><strong>(M0020) Patient ID Number:</strong><%= data.ContainsKey("M0020PatientIdNumber") ? data["M0020PatientIdNumber"].Answer : "" %></span>
            <span><strong>(M0030) Discharge Date:</strong><%= data.ContainsKey("M0030SocDate") ? data["M0030SocDate"].Answer : "" %></span>
            <span><strong>(M0032) Resuption of Care Date:</strong><%= data.ContainsKey("M0032ROCDate") ? data["M0032ROCDate"].Answer : "" %></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0032ROCDateNotApplicable") && data["M0032ROCDateNotApplicable"].Answer == "1" ? "&#x2713;" : "" %></span> NA &ndash; Not Applicable</span>
        </div><div>
            <span><strong>Episode Start Date:</strong><%= data.ContainsKey("GenericEpisodeStartDate") ? data["GenericEpisodeStartDate"].Answer : "" %></span>
        </div><div class="bicol">
            <span><strong>(M0040) Patient Name:</strong>(Last Suffix First MI)</span>
            <span><%= data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : ""%> <%= data.ContainsKey("M0040Suffix") ? data["M0040Suffix"].Answer : ""%> <%= data.ContainsKey("M0040FirstName") ? data["M0040FirstName"].Answer : ""%> <%= data.ContainsKey("M0040MI") ? data["M0040MI"].Answer : "" %></span>
            <span><strong>(M0064) Social Security Number:</strong><%= data.ContainsKey("M0064PatientSSN") ? data["M0064PatientSSN"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0064PatientSSNUnknown") && data["M0064PatientSSNUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Unknown or Not Available</span>
        </div><div class="bicol">
            <span><strong>(M0050) Patient State:</strong><%= data.ContainsKey("M0050PatientState") ? data["M0050PatientState"].Answer : ""%></span>
            <span><strong>(M0060) Patient Zip Code:</strong><%= data.ContainsKey("M0060PatientZipCode") ? data["M0060PatientZipCode"].Answer : ""%></span>
        </div><div class="bicol">
            <span><strong>(M0063) Medicare Number:</strong><%= data.ContainsKey("M0063PatientMedicareNumber") ? data["M0063PatientMedicareNumber"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0063PatientMedicareNumberUnknown") && data["M0063PatientMedicareNumberUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No Medicare</span>
            <span><strong>(M0065) Medicaid Number:</strong><%= data.ContainsKey("M0065PatientMedicaidNumber") ? data["M0065PatientMedicaidNumber"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0065PatientMedicaidNumberUnknown") && data["M0065PatientMedicaidNumberUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No Medicaid</span>
        </div><div class="bicol">
            <span><strong>(M0066) Birth Date:</strong><%= data.ContainsKey("M0066PatientDoB") ? data["M0066PatientDoB"].Answer : ""%></span>
            <span><strong>(M0069) Gender:</strong><%= data.ContainsKey("M0069Gender") ? data["M0069Gender"].Answer == "1" ? "Male" : "Female" : "" %></span>
        </div><div class="bicol">
            <span><strong>(M0080) Discipline of Person Completing Assessment:</strong><% if (data.ContainsKey("M0080DisciplinePerson")) { switch(data["M0080DisciplinePerson"].Answer) { case "01": %>1 &ndash; RN<% break; case "02": %>2 &ndash; PT<% break; case "03": %>3 &ndash; SLP/ST<% break; case "04": %>4 &ndash; OT<% break; } } %></span>
            <span><strong>(M0090) Date Assessment Completed:</strong><%= data.ContainsKey("M0090AssessmentCompleted") ? data["M0090AssessmentCompleted"].Answer : ""%></span>
        </div><div>
            <span><strong>(M0100) This Assessment is Currently Being Completed for the Following Reason:</strong></span>
            <span><strong>Start/Resuption of Care</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Discharge&mdash;further visits planned</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Resumption of care (after inpatient stay)</span>
            <span><strong>Follow-Up</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Recertification (follow-up) reassessment <em>[Go to M0110]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Other follow-up <em>[Go to M0110]</em></span>
            <span><strong>Transfer to an Inpatient Facility</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "06" ? "&#x2713;" : ""%></span> 6 &ndash; Transfer to inpatient facility&mdash;patient not discharged from agency <em>[Go to M1040]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "07" ? "&#x2713;" : ""%></span> 7 &ndash; Transfer to inpatient facility&mdash;patient discharged from agency <em>[Go to M1040]</em></span>
            <span><strong>Discharge from Agency – Not to an Inpatient Facility</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "08" ? "&#x2713;" : ""%></span> 8 &ndash; Death at home <em>[Go to M0903]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "09" ? "&#x2713;" : ""%></span> 9 &ndash; Discharge from agency <em>[Go to M1040]</em></span>
        </div><div>
            <span><strong>(M0102) Date of Physician-ordered Discharge (Resumption of Care):</strong><%= data.ContainsKey("M0102PhysicianOrderedDate") && data["M0102PhysicianOrderedDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer + " <em>[Go to M0110]</em>" : "" %></span>
            <span>If the physician indicated a specific Discharge (resumption of care) date when the patient was referred for home health services, record the date specified.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && data["M0102PhysicianOrderedDateNotApplicable"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No specific SOC date ordered by physician</span>
        </div><div>
            <span><strong>(M0104) Date of Referral:</strong><%= data.ContainsKey("M0104ReferralDate") && data["M0104ReferralDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer : "" %></span>
            <span>Indicate the date that the written or documented orders from the physician or physician designee for initiation or resumption of care were received by the HHA.</span>
        </div><div>
            <span><strong>(M0110) Episode Timing:</strong><%= data.ContainsKey("M0104ReferralDate") && data["M0104ReferralDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer : "" %></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Early</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Later</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "UK" ? "&#x2713;" : ""%></span> UK &ndash; Unknown</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Not Applicable/No Medicare case mix group to be defined by this assessment </span>
            <span>Is the Medicare home health payment episode for which this assessment will define a case mix group an &ldquo;early&rdquo; episode or a &ldquo;later&rdquo; episode in the patient&rsquo;s current sequence of adjacent Medicare home health payment episodes?</span>
        </div><div>
            <span><strong>(M0140) Race/Ethnicity <em>(Mark all that apply)</em>:</strong></span>
            <span class="bicol">
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceAMorAN") && data["M0140RaceAMorAN"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; American Indian or Alaska Native</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceAsia") && data["M0140RaceAsia"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Asian</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceBalck") && data["M0140RaceBalck"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Black or African American</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceHispanicOrLatino") && data["M0140RaceHispanicOrLatino"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Hispanic or Latino</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceNHOrPI") && data["M0140RaceNHOrPI"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Native Hawaiian or Pacific Islander</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceWhite") && data["M0140RaceWhite"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; White</span>        
            </span>
        </div><div>
            <span><strong>(M0150) Current Payment Sources for Home Care <em>(Mark all that apply)</em>:</strong></span>
            <span class="bicol">
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceNone") && data["M0150PaymentSourceNone"].Answer == "1" ? "&#x2713;" : ""%></span> 0 &ndash; None&mdash;No charge for current services</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCREFFS") && data["M0150PaymentSourceMCREFFS"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Medicare (traditional fee-for-service)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCREHMO") && data["M0150PaymentSourceMCREHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Medicare (HMO/Managed Care/Advantage plan)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCAIDFFS") && data["M0150PaymentSourceMCAIDFFS"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Medicaid (traditional fee-for-service)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMACIDHMO") && data["M0150PaymentSourceMACIDHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Medicaid (HMO/Managed Care)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceWRKCOMP") && data["M0150PaymentSourceWRKCOMP"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Worker&rsquo;s compensation</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceTITLPRO") && data["M0150PaymentSourceTITLPRO"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; Title progams (e.g. Title III, V, or XX)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceOTHGOVT") && data["M0150PaymentSourceOTHGOVT"].Answer == "1" ? "&#x2713;" : ""%></span> 7 &ndash; Other government (e.g. TriCare, VA etc)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourcePRVINS") && data["M0150PaymentSourcePRVINS"].Answer == "1" ? "&#x2713;" : ""%></span> 8 &ndash; Private Insurance</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourcePRVHMO") && data["M0150PaymentSourcePRVHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 9 &ndash; Private HMO/managed care</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceSelfPay") && data["M0150PaymentSourceSelfPay"].Answer == "1" ? "&#x2713;" : ""%></span> 10 &ndash; Self-pay</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceOtherSRS") && data["M0150PaymentSourceOtherSRS"].Answer == "1" ? "&#x2713;" : ""%></span> 11 &ndash; Other<%= data.ContainsKey("M0150PaymentSourceOther") && data["M0150PaymentSourceOther"].Answer.IsNotNullOrEmpty() ? ": " + data["M0150PaymentSourceOther"].Answer : "" %></span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceUnknown") && data["M0150PaymentSourceUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Unknown</span>        
            </span>
        </div>
        <h2>Risk Assessment</h2>
        <div>
            <span><strong>(M1040) Influenza Vaccine: Did the patient receive the influenza vaccine from your agency for this year&rsquo;s influenza season (October 1 through March 31) during this episode of care?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes <em>[Go to M1050]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Does not apply because entire episode of care (SOC/ROC to Transfer/Discharge) is outside this influenza season. <em>[Go to M1050]</em></span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Discharge</h1>
        <h2>Risk Assessment</h2>
        <div>
            <span><strong>(M1045) Reason Influenza Vaccine not received: If the patient did not receive the influenza vaccine from your agency during this episode of care, state reason:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Received from another health care provider (e.g., physician)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Received from your agency previously during this year&rsquo;s flu season</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Offered and declined</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Assessed and determined to have medical contraindication(s)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Not indicated; patient does not meet age/condition guidelines for influenza vaccine</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "06" ? "&#x2713;" : ""%></span> 6 &ndash; Inability to obtain vaccine due to declared shortage</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "07" ? "&#x2713;" : ""%></span> 7 &ndash; None of the above</span>
        </div><div>
            <span><strong>(M1050) Pneumococcal Vaccine: Did the patient receive pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge)?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1050PneumococcalVaccine") && data["M1050PneumococcalVaccine"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1050PneumococcalVaccine") && data["M1050PneumococcalVaccine"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes <em>[Go to M1500 at TRN; Go to M1230 at DC]</em></span>
        </div><div>
            <span><strong>(M1055) Reason PPV not received: If patient did not receive the pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge), state reason:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Patient has received PPV in the past</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Offered and declined</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Assessed and determined to have medical contraindication(s)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Not indicated; patient does not meet age/condition guidelines for PPV</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; None of the above</span>
        </div>
		<h2>Cardiac Status</h2>
        <div>
            <span><strong>(M1500) Symptoms in Heart Failure Patients: If patient has been diagnosed with heart failure, did the patient exhibit symptoms indicated by clinical heart failure guidelines (including dyspnea, orthopnea, edema, or weight gain) at any point since the previous OASIS assessment?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No <em>[Go to M2004 at TRN; Go to M1600 at DC]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "00" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "00" ? "&#x2713;" : ""%></span> 2 &ndash; Not assessed <em>[Go to M2004 at TRN; Go to M1600 at DC]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "00" ? "&#x2713;" : ""%></span> NA &ndash; Patient does not have diagnosis of heart failure <em>[Go to M2004 at TRN; Go to M1600 at DC]</em></span>
        </div><div>
            <span><strong>(M1510) Heart Failure Follow-up: If patient has been diagnosed with heart failure and has exhibited symptoms indicative of heart failure since the previous OASIS assessment, what action(s) has (have) been taken to respond? <em>(Mark all that apply)</em></strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupNoAction") && data["M1510HeartFailureFollowupNoAction"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; No action taken</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupPhysicianCon") && data["M1510HeartFailureFollowupPhysicianCon"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Patient&rsquo;s physician (or other primary care practitioner) contacted the same day</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupAdvisedEmg") && data["M1510HeartFailureFollowupAdvisedEmg"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Patient advised to get emergency treatment (e.g., call 911 or go to emergency room)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupParameters") && data["M1510HeartFailureFollowupParameters"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Implemented physician-ordered patient-specific established parameters for treatment</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupInterventions") && data["M1510HeartFailureFollowupInterventions"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Patient education or other clinical interventions</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupChange") && data["M1510HeartFailureFollowupChange"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; Obtained change in care plan orders (e.g., increased monitoring by agency, change in visit frequency, telehealth, etc.)</span>
        </div>
        <h2>Medications</h2>
        <div>
            <span><strong>(M2004) Medication Intervention: If there were any clinically significant medication issues since the previous OASIS assessment, was a physician or the physician-designee contacted within one calendar day of the assessment to resolve clinically significant medication issues, including reconciliation?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; No clinically significant medication issues identified since the previous OASIS assessment</span>
        </div><div>
            <span><strong>(M2015) Patient/Caregiver Drug Education Intervention: Since the previous OASIS assessment, was the patient/caregiver instructed by agency staff or other health care provider to monitor the effectiveness of drug therapy, drug reactions, and side effects, and how and when to report problems that may occur?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Patient not taking any drugs</span>
        </div>
		<h2>Emergent Care</h2>
		<div>
			<span><strong>(M2300) Emergent Care: Since the last time OASIS data were collected, has the patient utilized a hospital emergency department (includes holding/observation)?</strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No <em>[Go to M2400]</em></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes, used hospital emergency department WITHOUT hospital admission</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Yes, used hospital emergency department WITH hospital admission</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "UK" ? "&#x2713;" : ""%></span> UK &ndash; Unknown <em>[Go to M2400]</em></span>            
		</div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Transfer</h1>
		<div>
			<span><strong>(M2310) Reason for Emergent Care: For what reason(s) did the patient receive emergent care (with or without hospitalization)? <em>(Mark all that apply)</em></strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareMed") && data["M2310ReasonForEmergentCareMed"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Improper medication administration, medication side effects, toxicity, anaphylaxis</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareFall") && data["M2310ReasonForEmergentCareFall"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Injury caused by fall</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareResInf") && data["M2310ReasonForEmergentCareResInf"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Respiratory infection (e.g., pneumonia, bronchitis)</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareOtherResInf") && data["M2310ReasonForEmergentCareOtherResInf"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Other respiratory problem</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareHeartFail") && data["M2310ReasonForEmergentCareHeartFail"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Heart failure (e.g., fluid overload)</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareCardiac") && data["M2310ReasonForEmergentCareCardiac"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; Cardiac dysrhythmia (irregular heartbeat)</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareMyocardial") && data["M2310ReasonForEmergentCareMyocardial"].Answer == "1" ? "&#x2713;" : ""%></span> 7 &ndash; Myocardial infarction or chest pain</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareHeartDisease") && data["M2310ReasonForEmergentCareHeartDisease"].Answer == "1" ? "&#x2713;" : ""%></span> 8 &ndash; Other heart disease</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareStroke") && data["M2310ReasonForEmergentCareStroke"].Answer == "1" ? "&#x2713;" : ""%></span> 9 &ndash; Stroke (CVA) or TIA</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareHypo") && data["M2310ReasonForEmergentCareHypo"].Answer == "1" ? "&#x2713;" : ""%></span> 10 &ndash; Hypo/Hyperglycemia, diabetes out of control</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareGI") && data["M2310ReasonForEmergentCareGI"].Answer == "1" ? "&#x2713;" : ""%></span> 11 &ndash; GI bleeding, obstruction, constipation, impaction</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareDehMal") && data["M2310ReasonForEmergentCareDehMal"].Answer == "1" ? "&#x2713;" : ""%></span> 12 &ndash; Dehydration, malnutrition</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareUrinaryInf") && data["M2310ReasonForEmergentCareUrinaryInf"].Answer == "1" ? "&#x2713;" : ""%></span> 13 &ndash; Urinary tract infection</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareIV") && data["M2310ReasonForEmergentCareIV"].Answer == "1" ? "&#x2713;" : ""%></span> 14 &ndash; IV catheter-related infection or complication</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareWoundInf") && data["M2310ReasonForEmergentCareWoundInf"].Answer == "1" ? "&#x2713;" : ""%></span> 15 &ndash; Wound infection or deterioration</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareUncontrolledPain") && data["M2310ReasonForEmergentCareUncontrolledPain"].Answer == "1" ? "&#x2713;" : ""%></span> 16 &ndash; Uncontrolled pain</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareMental") && data["M2310ReasonForEmergentCareMental"].Answer == "1" ? "&#x2713;" : ""%></span> 17 &ndash; Acute mental/behavioral health problem</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareDVT") && data["M2310ReasonForEmergentCareDVT"].Answer == "1" ? "&#x2713;" : ""%></span> 18 &ndash; Deep vein thrombosis, pulmonary embolus</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareOther") && data["M2310ReasonForEmergentCareOther"].Answer == "1" ? "&#x2713;" : ""%></span> 19 &ndash; Other than above reasons</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareUK") && data["M2310ReasonForEmergentCareUK"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Reason unknown</span>            
		</div>
        <h2>Transfer</h2>
        <div><table>
            <tbody>
                <tr><td colspan="4"><strong>(M2400) Intervention Synopsis: (Check only one box in each row.) Since the previous OASIS assessment, were the following interventions BOTH included in the physician-ordered plan of care AND implemented?</strong></td></tr>
                <tr><th>Plan/Intervention</th><th>No</th><th>Yes</th><th>NA &ndash; Not Applicable</th></tr>
                <tr>
                    <td>a. Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Patient is not diabetic or is bilateral amputee</span></td>
                </tr><tr>
                    <td>b. Falls prevention interventions</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for falls since the last OASIS assessment</span></td>
                </tr><tr>
                    <td>c. Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Formal assessment indicates patient did not meet criteria for depression AND patient did not have diagnosis of depression since the last OASIS assessment</span></td>
                </tr><tr>
                    <td>d. Intervention(s) to monitor and mitigate pain</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Formal assessment did not indicate pain since the last OASIS assessment</span></td>
                </tr><tr>
                    <td>e. Intervention(s) to prevent pressure ulcers</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Formal assessment indicates the patient was not at risk of pressure ulcers since the last OASIS assessment</span></td>
                </tr><tr>
                    <td>f. Pressure ulcer treatment based on principles of moist wound healing</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Dressings that support the principles of moist wound healing not indicated for this patient’s pressure ulcers OR patient has no pressure ulcers with need for moist wound healing</span></td>
                </tr>
            </tbody>
        </table></div>
        <div>
            <span><strong>(M2410) To which Inpatient Facility has the patient been admitted?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Hospital <em>[Go to M2430]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Rehabilitation facility <em>[Go to M0903]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Nursing home <em>[Go to M2440]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Hospice <em>[Go to M0903]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; No inpatient facility admission <em>[Omit &ldquo;NA&rdquo; option on TRN]</em></span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Transfer</h1>
        <div>
			<span><strong>(M2430) Reason for Hospitalization: For what reason(s) did the patient require hospitalization? <em>(Mark all that apply.)</em></strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationMed") && data["M2430ReasonForHospitalizationMed"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Improper medication administration, medication side effects, toxicity, anaphylaxis</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationFall") && data["M2430ReasonForHospitalizationFall"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Injury caused by fall</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationInfection") && data["M2430ReasonForHospitalizationInfection"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Respiratory infection (e.g., pneumonia, bronchitis)</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationOtherRP") && data["M2430ReasonForHospitalizationOtherRP"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Other respiratory problem</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationHeartFail") && data["M2430ReasonForHospitalizationHeartFail"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Heart failure (e.g., fluid overload)</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationCardiac") && data["M2430ReasonForHospitalizationCardiac"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; Cardiac dysrhythmia (irregular heartbeat)</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationMyocardial") && data["M2430ReasonForHospitalizationMyocardial"].Answer == "1" ? "&#x2713;" : ""%></span> 7 &ndash; Myocardial infarction or chest pain</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationHeartDisease") && data["M2430ReasonForHospitalizationHeartDisease"].Answer == "1" ? "&#x2713;" : ""%></span> 8 &ndash; Other heart disease</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationStroke") && data["M2430ReasonForHospitalizationStroke"].Answer == "1" ? "&#x2713;" : ""%></span> 9 &ndash; Stroke (CVA) or TIA</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationHypo") && data["M2430ReasonForHospitalizationHypo"].Answer == "1" ? "&#x2713;" : ""%></span> 10 &ndash; Hypo/Hyperglycemia, diabetes out of control</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationGI") && data["M2430ReasonForHospitalizationGI"].Answer == "1" ? "&#x2713;" : ""%></span> 11 &ndash; GI bleeding, obstruction, constipation, impaction</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationDehMal") && data["M2430ReasonForHospitalizationDehMal"].Answer == "1" ? "&#x2713;" : ""%></span> 12 &ndash; Dehydration, malnutrition</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationUrinaryInf") && data["M2430ReasonForHospitalizationUrinaryInf"].Answer == "1" ? "&#x2713;" : ""%></span> 13 &ndash; Urinary tract infection</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationIV") && data["M2430ReasonForHospitalizationIV"].Answer == "1" ? "&#x2713;" : ""%></span> 14 &ndash; IV catheter-related infection or complication</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationWoundInf") && data["M2430ReasonForHospitalizationWoundInf"].Answer == "1" ? "&#x2713;" : ""%></span> 15 &ndash; Wound infection or deterioration</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationUncontrolledPain") && data["M2430ReasonForHospitalizationUncontrolledPain"].Answer == "1" ? "&#x2713;" : ""%></span> 16 &ndash; Uncontrolled pain</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationMental") && data["M2430ReasonForHospitalizationMental"].Answer == "1" ? "&#x2713;" : ""%></span> 17 &ndash; Acute mental/behavioral health problem</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationDVT") && data["M2430ReasonForHospitalizationDVT"].Answer == "1" ? "&#x2713;" : ""%></span> 18 &ndash; Deep vein thrombosis, pulmonary embolus</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationScheduled") && data["M2430ReasonForHospitalizationScheduled"].Answer == "1" ? "&#x2713;" : ""%></span> 19 &ndash; Scheduled treatment or procedure</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationOther") && data["M2430ReasonForHospitalizationOther"].Answer == "1" ? "&#x2713;" : ""%></span> 20 &ndash; Other than above reasons</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2430ReasonForHospitalizationUK") && data["M2430ReasonForHospitalizationUK"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Reason unknown</span>            
		</div><div>
			<span><strong>(M2440) For what Reason(s) was the patient Admitted to a Nursing Home? (Mark all that apply.)</strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2440ReasonPatientAdmittedTherapy") && data["M2440ReasonPatientAdmittedTherapy"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Therapy services</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2440ReasonPatientAdmittedRespite") && data["M2440ReasonPatientAdmittedRespite"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Respite care</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2440ReasonPatientAdmittedHospice") && data["M2440ReasonPatientAdmittedHospice"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Hospice care</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2440ReasonPatientAdmittedPermanent") && data["M2440ReasonPatientAdmittedPermanent"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Permanent placement</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2440ReasonPatientAdmittedUnsafe") && data["M2440ReasonPatientAdmittedUnsafe"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Unsafe for care at home</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2440ReasonPatientAdmittedOther") && data["M2440ReasonPatientAdmittedOther"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; Other</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2440ReasonPatientAdmittedUnknown") && data["M2440ReasonPatientAdmittedUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Unknown</span>            
		</div><div>
            <span><strong>(M0903) Date of Last (Most Recent) Home Visit:</strong></span>
            <span><%= data.ContainsKey("M0903LastHomeVisitDate") ? data["M0903LastHomeVisitDate"].Answer : "&nbsp;"%></span>
        </div><div>
            <span><strong>(M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer, or death (at home) of the patient.</strong></span>
            <span><%= data.ContainsKey("M0906DischargeDate") ? data["M0906DischargeDate"].Answer : ""%></span>
        </div>
    </div>
</body>
</html>

