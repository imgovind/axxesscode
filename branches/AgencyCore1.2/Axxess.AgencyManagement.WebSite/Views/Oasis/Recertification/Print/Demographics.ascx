﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.span("(M0020) Patient ID Number:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0020PatientIdNumber") && data["M0020PatientIdNumber"].Answer.IsNotNullOrEmpty() ? data["M0020PatientIdNumber"].Answer : ""%>",false,1) +
            printview.span("(M0040) Patient Name:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0040LastName") && data["M0040LastName"].Answer.IsNotNullOrEmpty() ? data["M0040LastName"].Answer : ""%><%= data != null && data.ContainsKey("M0040Suffix") && data["M0040Suffix"].Answer.IsNotNullOrEmpty() ? " " + data["M0040Suffix"].Answer : ""%><%= data != null && data.ContainsKey("M0040FirstName") && data["M0040FirstName"].Answer.IsNotNullOrEmpty() ? ", " + data["M0040FirstName"].Answer : ""%><%= data != null && data.ContainsKey("M0040MI") && data["M0040MI"].Answer.IsNotNullOrEmpty() ? " " + data["M0040MI"].Answer : ""%>",false,1) +
            printview.span("(M0050) State:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0050PatientState") && data["M0050PatientState"].Answer.IsNotNullOrEmpty() ? data["M0050PatientState"].Answer : ""%>",false,1) +
            printview.span("(M0060) Zip:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0060PatientZipCode") && data["M0060PatientZipCode"].Answer.IsNotNullOrEmpty() ? data["M0060PatientZipCode"].Answer : ""%>",false,1) +
            printview.span("(M0069) Gender:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0069Gender") && data["M0069Gender"].Answer == "1" ? "Male" : "" %><%= data != null && data.ContainsKey("M0069Gender") && data["M0069Gender"].Answer == "2" ? "Female" : "" %>",false,1) +
            printview.span("(M0064) Social Security Number:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0064PatientSSNUnknown") && data["M0064PatientSSNUnknown"].Answer == "1" ? "UK &ndash; Unknown" : (data != null && data.ContainsKey("M0064PatientSSN") && data["M0064PatientSSN"].Answer.IsNotNullOrEmpty() ? data["M0064PatientSSN"].Answer : "" )%>",false,1) +
            printview.span("(M0063) Medicare Number:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0063PatientMedicareNumberUnknown") && data["M0063PatientMedicareNumberUnknown"].Answer == "1" ? "NA &ndash; No Medicare" : (data != null && data.ContainsKey("M0063PatientMedicareNumber") && data["M0063PatientMedicareNumber"].Answer.IsNotNullOrEmpty() ? data["M0063PatientMedicareNumber"].Answer : "")%>",false,1) +
            printview.span("(M0065) Medicaid Number:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0065PatientMedicaidNumberUnknown") && data["M0065PatientMedicaidNumberUnknown"].Answer == "1" ? "NA &ndash; No Medicaid" : (data != null && data.ContainsKey("M0065PatientMedicaidNumber") && data["M0065PatientMedicaidNumber"].Answer.IsNotNullOrEmpty() ? data["M0065PatientMedicaidNumber"].Answer : "")%>",false,1) +
            printview.span("(M0066) Birth Date:",true) +
            printview.span("<%= data != null && data.ContainsKey("M0066PatientDoB") && data["M0066PatientDoB"].Answer.IsNotNullOrEmpty() ? data["M0066PatientDoB"].Answer : ""%>",false,1) +
            printview.span("(M0140) Race/Ethnicity (Below):",true)) +
        printview.col(3,
            printview.checkbox("1 &ndash; American Indian or Alaska Native",<%= data != null && data.ContainsKey("M0140RaceAMorAN") && data["M0140RaceAMorAN"].Answer == "1" ? "true" : "false" %>) +
            printview.checkbox("2 &ndash; Asian",<%= data != null && data.ContainsKey("M0140RaceAsia") && data["M0140RaceAsia"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Black or Affrican American",<%= data != null && data.ContainsKey("M0140RaceBalck") && data["M0140RaceBalck"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Hispanic or Latino",<%= data != null && data.ContainsKey("M0140RaceHispanicOrLatino") && data["M0140RaceHispanicOrLatino"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; Native Hawaiian or Pacific Islander",<%= data != null && data.ContainsKey("M0140RaceNHOrPI") && data["M0140RaceNHOrPI"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("6 &ndash; White",<%= data != null && data.ContainsKey("M0140RaceWhite") && data["M0140RaceWhite"].Answer == "1" ? "true" : "false"%>)
        ), "Patient Information");
    printview.addsection(
        printview.col(2,
            printview.col(2,
                printview.span("(M0030) Start of Care Date:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0030SocDate") && data["M0030SocDate"].Answer.IsNotNullOrEmpty() ? data["M0030SocDate"].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("Episode Start Date:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericEpisodeStartDate") && data["GenericEpisodeStartDate"].Answer.IsNotNullOrEmpty() ? data["GenericEpisodeStartDate"].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("(M0032) Resumption of Care:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0032ROCDateNotApplicable") && data["M0032ROCDateNotApplicable"].Answer == "1" ? "NA &ndash; Not Applicable" : (data != null && data.ContainsKey("M0032ROCDate") && data["M0032ROCDate"].Answer.IsNotNullOrEmpty() ? data["M0032ROCDate"].Answer : "")%>",false,1)) +
            printview.col(2,
                printview.span("(M0090) Date Completed:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0090AssessmentCompleted") && data["M0090AssessmentCompleted"].Answer.IsNotNullOrEmpty() ? data["M0090AssessmentCompleted"].Answer : ""%>",false,1)) +
            printview.span("<strong>(M0080) Discipline of Person Completing Assessment:</strong> <%= data != null && data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer.IsNotNullOrEmpty() ? (data["M0080DisciplinePerson"].Answer == "01" ? "RN" : "") + (data["M0080DisciplinePerson"].Answer == "02" ? "PT" : "") + (data["M0080DisciplinePerson"].Answer == "03" ? "SLP/ST" : "") + (data["M0080DisciplinePerson"].Answer == "04" ? "OT" : "") : "<span class='blank'></span>" %>") +
            printview.span("<strong>(M0010) CMS Certification Number:</strong> <%= data != null && data.ContainsKey("M0010CertificationNumber") && data["M0010CertificationNumber"].Answer.IsNotNullOrEmpty() ? data["M0010CertificationNumber"].Answer : "<span class='blank'></span>" %>") +
            printview.col(2,
                printview.span("(M0014) Branch State:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0014BranchState") && data["M0014BranchState"].Answer.IsNotNullOrEmpty() ? data["M0014BranchState"].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("(M0016) Branch ID Number:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0016BranchId") && data["M0016BranchId"].Answer.IsNotNullOrEmpty() ? data["M0016BranchId"].Answer : ""%>",false,1)) +
            printview.span("<strong>(M0018) National Provider Identifier (NPI):</strong> <%= data != null && data.ContainsKey("M0018NationalProviderIdUnknown") && data["M0018NationalProviderIdUnknown"].Answer == "1" ? "UK &ndash; Unknown" : (data != null && data.ContainsKey("M0018NationalProviderId") && data["M0018NationalProviderId"].Answer.IsNotNullOrEmpty() ? data["M0018NationalProviderId"].Answer : "<span class='blank'></span>")%>") +
            printview.span("<strong>(M0102) Date of Physician-Ordered Start of Care:</strong> <%= data != null && data.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && data["M0102PhysicianOrderedDateNotApplicable"].Answer == "1" ? "NA &ndash; No Specific SOC date ordered by physician" : (data != null && data.ContainsKey("M0102PhysicianOrderedDate") && data["M0102PhysicianOrderedDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer : "<span class='blank'></span>") %>") +
            printview.col(2,
                printview.span("(M0104) Date of Referral:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0104ReferralDate") && data["M0104ReferralDate"].Answer.IsNotNullOrEmpty() ? data["M0104ReferralDate"].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("(M0110) Episode Timing:",true) +
                printview.span("<%= data != null && data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "01" ? "Early" : ""%><%= data != null && data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "02" ? "Later" : ""%><%= data != null && data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "UK" ? "Unknown" : ""%><%= data != null && data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "NA" ? "Not Applicable/No Medicare case mix group" : ""%>",false,1)) +
            printview.span("(M0100) Reason for this Assessment:",true) +
            printview.span("4 &ndash; Recertification (follow-up) reassessment.")) +
        printview.span("(M0150) Payment Source:",true) +
        printview.col(3,
            printview.checkbox("0 &ndash; None; no charge for current services",<%= data != null && data.ContainsKey("M0150PaymentSourceNone") && data["M0150PaymentSourceNone"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Medicare (Traditional Fee-for-Service)",<%= data != null && data.ContainsKey("M0150PaymentSourceMCREFFS") && data["M0150PaymentSourceMCREFFS"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Medicare (HMO/Managed Care/Adv. Plan)",<%= data != null && data.ContainsKey("M0150PaymentSourceMCREHMO") && data["M0150PaymentSourceMCREHMO"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Medicaid (Traditional Fee-for-Service)",<%= data != null && data.ContainsKey("M0150PaymentSourceMCAIDFFS") && data["M0150PaymentSourceMCAIDFFS"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Medicaid (HMO/Managed Care)",<%= data != null && data.ContainsKey("M0150PaymentSourceMACIDHMO") && data["M0150PaymentSourceMACIDHMO"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; Workers&rsquo; Compensation",<%= data != null && data.ContainsKey("M0150PaymentSourceWRKCOMP") && data["M0150PaymentSourceWRKCOMP"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("6 &ndash; Title Programs (e.g., Title III, V, or XX)",<%= data != null && data.ContainsKey("M0150PaymentSourceTITLPRO") && data["M0150PaymentSourceTITLPRO"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("7 &ndash; Other government (e.g., TriCare, VA, etc.)",<%= data != null && data.ContainsKey("M0150PaymentSourceOTHGOVT") && data["M0150PaymentSourceOTHGOVT"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("8 &ndash; Private Insurance",<%= data != null && data.ContainsKey("M0150PaymentSourcePRVINS") && data["M0150PaymentSourcePRVINS"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("9 &ndash; Private HMO/Managed Care",<%= data != null && data.ContainsKey("M0150PaymentSourcePRVHMO") && data["M0150PaymentSourcePRVHMO"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("10 &ndash; Self-pay",<%= data != null && data.ContainsKey("M0150PaymentSourceSelfPay") && data["M0150PaymentSourceSelfPay"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("UK &ndash; Unknown",<%= data != null && data.ContainsKey("M0150PaymentSourceUnknown") && data["M0150PaymentSourceUnknown"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("11 &ndash; Other (specify)",<%= data != null && data.ContainsKey("M0150PaymentSourceOtherSRS") && data["M0150PaymentSourceOtherSRS"].Answer == "1" ? "true" : "false"%>) +
            printview.span("<%= data != null && data.ContainsKey("M0150PaymentSourceOther") && data["M0150PaymentSourceOther"].Answer.IsNotNullOrEmpty() ? data["M0150PaymentSourceOther"].Answer : ""%>",false,1)
        ),"Episode Information");
</script>