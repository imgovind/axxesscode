﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
            printview.span("%3Cspan class=%22auto float_left%22%3ESensory Perception%3C/span%3E%3Cspan class=%22auto float_right%22%3E<%= data != null && data.ContainsKey("485BradenScaleSensory") && data["485BradenScaleSensory"].Answer.IsNotNullOrEmpty() ? data["485BradenScaleSensory"].Answer : "<span class='blank'></span>"%>%3C/span%3E",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
            printview.span("1. Completely Limited. Unresponsive (does not moan, flinch, or grasp) to painful stimuli, due to diminished level of consciousness or sedation OR limited ability to feel pain over most of body.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("2. Very Limited. Responds only to painful stimuli. Cannot communicate discomfort except by moaning or restlessness OR has a sensory impairment which limits the ability to feel pain or discomfort over 1/2 of body.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("3. Slightly Limited. Responds to verbal commands, but cannot always communicate discomfort or the need to be turned OR has some sensory impairment which limits ability to feel pain or discomfort in 1 or 2 extremities.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("4. No Impairment. Responds to verbal commands. Has no sensory deficit which would limit ability to feel or voice pain or discomfort.") +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E",
        "Braden Scale");
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
            printview.span("%3Cspan class=%22auto float_left%22%3EMoisture%3C/span%3E%3Cspan class=%22auto float_right%22%3E<%= data != null && data.ContainsKey("485BradenScaleMoisture") && data["485BradenScaleMoisture"].Answer.IsNotNullOrEmpty() ? data["485BradenScaleMoisture"].Answer : "<span class='blank'></span>"%>%3C/span%3E",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
            printview.span("1. Constantly Moist. Skin is kept moist almost constantly by perspiration, urine, etc. Dampness is detected every time patient is moved or turned.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("2. Often Moist. Skin is often, but not always moist. Linen must be changed as often as 3 times in 24 hours.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("3. Occasionally Moist. Skin is occasionally moist, requiring an extra linen change approximately once a day.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("4. Rarely Moist. Skin is usually dry; Linen only requires changing at routine intervals.") +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
            printview.span("%3Cspan class=%22auto float_left%22%3EActivity%3C/span%3E%3Cspan class=%22auto float_right%22%3E<%= data != null && data.ContainsKey("485BradenScaleActivity") && data["485BradenScaleActivity"].Answer.IsNotNullOrEmpty() ? data["485BradenScaleActivity"].Answer : "<span class='blank'></span>"%>%3C/span%3E",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
            printview.span("1. Bedfast. Confined to bed.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("2. Chairfast. Ability to walk severely limited or non-existent. Cannot bear own weight and/or must be assisted into chair or wheelchair.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("3. Walks Occasionally. Walks occasionally during day, but for very short distances, with or without assistance. Spends majority of day in bed or chair.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("4. Walks Frequently. Walks outside bedroom twice a day and inside room at least once every two hours during waking hours.") +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
            printview.span("%3Cspan class=%22auto float_left%22%3EMobility%3C/span%3E%3Cspan class=%22auto float_right%22%3E<%= data != null && data.ContainsKey("485BradenScaleMobility") && data["485BradenScaleMobility"].Answer.IsNotNullOrEmpty() ? data["485BradenScaleMobility"].Answer : "<span class='blank'></span>"%>%3C/span%3E",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
            printview.span("1. Completely Immobile. Does not make even slight changes in body or extremity position without assistance.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("2. Very Limited. Makes occasional slight changes in body or extremity position but unable to make frequent or significant changes independently.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("3. Slightly Limited. Makes frequent though slight changes in body or extremity position independently.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("4. No Limitation. Makes major and frequent changes in position without assistance.") +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
            printview.span("%3Cspan class=%22auto float_left%22%3ENutrition%3C/span%3E%3Cspan class=%22auto float_right%22%3E<%= data != null && data.ContainsKey("485BradenScaleNutrition") && data["485BradenScaleNutrition"].Answer.IsNotNullOrEmpty() ? data["485BradenScaleNutrition"].Answer : "<span class='blank'></span>"%>%3C/span%3E",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
            printview.span("1. Very Poor. Never eats a complete meal. Rarely eats more than 1/3 of any food offered. Eats 2 servings or less of protein (meat or dairy products) per day. Takes fluids poorly. Does not take a liquid dietary supplement OR is NPO and/or maintained on clear liquids or IVs for more than 5 days.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("2. Probably Inadequate. Rarely eats a complete meal and generally eats only about 1/2 of any food offered. Protein intake includes only 3 servings of meat or dairy products per day. Occasionally will take a dietary supplement OR receives less than optimum amount of liquid diet or tube feeding.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("3. Adequate. Eats over half of most meals. Eats a total of 4 servings of protein (meat, dairy products) per day. Occasionally will refuse a meal, but will usually take a supplement when offered OR is on a tube feeding or TPN regimen which probably meets most of nutritional needs.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("4. Excellent. Eats most of every meal. Never refuses a meal. Usually eats a total of 4 or more servings of meat and dairy products. Occasionally eats between meals. Does not require supplementation.") +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
            printview.span("%3Cspan class=%22auto float_left%22%3EFriction &amp; Shear%3C/span%3E%3Cspan class=%22auto float_right%22%3E<%= data != null && data.ContainsKey("485BradenScaleFriction") && data["485BradenScaleFriction"].Answer.IsNotNullOrEmpty() ? data["485BradenScaleFriction"].Answer : "<span class='blank'></span>"%>%3C/span%3E",true) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E" +
            printview.span("1. Problem. Requires moderate to maximum assistance in moving. Complete lifting without sliding against sheets is impossible. Frequently slides down in bed or chair, requiring frequent repositioning with maximum assistance. Spasticity, contractures or agitation leads to almost constant friction.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("2. Potential Problem. Moves feebly or requires minimum assistance. During a move skin probably slides to some extent against sheets, chair, restraints or other devices. Maintains relatively good position in chair or bed most of the time but occasionally slides down.") +
        "%3C/td%3E%3Ctd%3E" +
            printview.span("3. No Apparent Problem. Moves in bed and in chair independently and has sufficient muscle strength to lift up completely during move. Maintains good position in bed or chair.") +
        "%3C/td%3E%3Ctd%3E&nbsp;%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22 class=%22align_right%22%3E" +
            printview.span("<strong>Total:</strong> <%= data != null && data.ContainsKey("485BradenScaleTotal") && data["485BradenScaleTotal"].Answer.IsNotNullOrEmpty() && data["485BradenScaleTotal"].Answer != "NaN" ? data["485BradenScaleTotal"].Answer : "<span class='blank'></span>"%>") +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");
    printview.addsection(
        printview.col(2,
            printview.col(2,
                printview.span("Skin Turgor:",true) +
                printview.checkbox("Good",<%= data != null && data.ContainsKey("GenericSkinTurgor") && data["GenericSkinTurgor"].Answer == "Good" ? "true" : "false"%>)) +
            printview.col(2,
                printview.checkbox("Fair",<%= data != null && data.ContainsKey("GenericSkinTurgor") && data["GenericSkinTurgor"].Answer == "Fair" ? "true" : "false"%>) +
                printview.checkbox("Poor",<%= data != null && data.ContainsKey("GenericSkinTurgor") && data["GenericSkinTurgor"].Answer == "Poor" ? "true" : "false"%>)) +
            printview.col(2,
                printview.span("Skin Color:",true) +
                printview.checkbox("Pink/WNL",<%= data != null && data.ContainsKey("GenericSkinColor") && data["GenericSkinColor"].Answer.Split(',').Contains("1") ? "true" : "false"%>)) +
            printview.col(2,
                printview.checkbox("Pallor",<%= data != null && data.ContainsKey("GenericSkinColor") && data["GenericSkinColor"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Jaundice",<%= data != null && data.ContainsKey("GenericSkinColor") && data["GenericSkinColor"].Answer.Split(',').Contains("3") ? "true" : "false"%>)) +
            printview.col(2,
                printview.span("") +
                printview.checkbox("Cyanotic",<%= data != null && data.ContainsKey("GenericSkinColor") && data["GenericSkinColor"].Answer.Split(',').Contains("4") ? "true" : "false"%>)) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericSkinColorOther") && data["GenericSkinColorOther"].Answer.IsNotNullOrEmpty() ? data["GenericSkinColorOther"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericSkinColor") && data["GenericSkinColor"].Answer.Split(',').Contains("5") ? "true" : "false"%>)) +
        printview.col(2,
            printview.col(2,
                printview.span("Condition:",true) +
                printview.checkbox("Dry",<%= data != null && data.ContainsKey("GenericSkinCondition") && data["GenericSkinCondition"].Answer.Split(',').Contains("1") ? "true" : "false"%>)) +
            printview.col(2,
                printview.checkbox("Wound",<%= data != null && data.ContainsKey("GenericSkinCondition") && data["GenericSkinCondition"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Ulcer",<%= data != null && data.ContainsKey("GenericSkinCondition") && data["GenericSkinCondition"].Answer.Split(',').Contains("3") ? "true" : "false"%>)) +
            printview.col(2,
                printview.span("") +
                printview.checkbox("Incision",<%= data != null && data.ContainsKey("GenericSkinCondition") && data["GenericSkinCondition"].Answer.Split(',').Contains("4") ? "true" : "false"%>)) +
            printview.col(2,
                printview.checkbox("Rash",<%= data != null && data.ContainsKey("GenericSkinCondition") && data["GenericSkinCondition"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Other <%= data != null && data.ContainsKey("GenericSkinConditionOther") && data["GenericSkinConditionOther"].Answer.IsNotNullOrEmpty() ? data["GenericSkinConditionOther"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericSkinCondition") && data["GenericSkinCondition"].Answer.Split(',').Contains("6") ? "true" : "false"%>))) +
        printview.col(2,
            printview.span("Instructed on measures to control infections?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericInstructedControlInfections") && data["GenericInstructedControlInfections"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericInstructedControlInfections") && data["GenericInstructedControlInfections"].Answer == "0" ? "true" : "false"%>)) +
            printview.col(2,
                printview.span("Nail Condition:",true) +
                printview.col(2,
                    printview.checkbox("Good",<%= data != null && data.ContainsKey("GenericNailsBad") && data["GenericNailsBad"].Answer == "Good" ? "true" : "false"%>) +
                    printview.checkbox("Problem",<%= data != null && data.ContainsKey("GenericNailsBad") && data["GenericNailsBad"].Answer == "Problem" ? "true" : "false"%>))) +
            printview.col(2,
                printview.span("<%= data != null && data.ContainsKey("GenericNailsProblemOther") && data["GenericNailsProblemOther"].Answer.IsNotNullOrEmpty() ? data["GenericNailsProblemOther"].Answer : ""%>",false,1) +
                printview.span("<strong>Skin Temp.:</strong><%= data != null && data.ContainsKey("GenericSkinTemp") && data["GenericSkinTemp"].Answer.IsNotNullOrEmpty() ? (data["GenericSkinTemp"].Answer == "0" ? "<span class='blank'></span>" : "") + (data["GenericSkinTemp"].Answer == "1" ? "Warm" : "") + (data["GenericSkinTemp"].Answer == "2" ? "Cool" : "") + (data["GenericSkinTemp"].Answer == "3" ? "Clammy" : "") + (data["GenericSkinTemp"].Answer == "4" ? "Other" : "") : "<span class='blank'></span>"%>")) +
            printview.span("Is patient using pressure-relieving device(s)?",true) +
            printview.col(3,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericPressureRelievingDevice") && data["GenericPressureRelievingDevice"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericPressureRelievingDevice") && data["GenericPressureRelievingDevice"].Answer == "0" ? "true" : "false"%>) +
                printview.span("Type <%= data != null && data.ContainsKey("GenericPressureRelievingDeviceType") && data["GenericPressureRelievingDeviceType"].Answer.IsNotNullOrEmpty() ? (data["GenericPressureRelievingDeviceType"].Answer == "0" ? "<span class='blank'></span>" : "") + (data["GenericPressureRelievingDeviceType"].Answer == "1" ? "Low Air Mattress" : "") + (data["GenericPressureRelievingDeviceType"].Answer == "2" ? "Gel Cushion" : "") + (data["GenericPressureRelievingDeviceType"].Answer == "3" ? "Egg Crate" : "") + (data["GenericPressureRelievingDeviceType"].Answer == "4" ? (data != null && data.ContainsKey("GenericPressureRelievingDeviceTypeOther") && data["GenericPressureRelievingDeviceTypeOther"].Answer.IsNotNullOrEmpty() ? data["GenericPressureRelievingDeviceTypeOther"].Answer : "Other") : "") : "<span class='blank'></span>"%>"))) +
        printview.span("Comments:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericIntegumentaryStatusComments") && data["GenericIntegumentaryStatusComments"].Answer.IsNotNullOrEmpty() ? data["GenericIntegumentaryStatusComments"].Answer : ""%>",false,2),
        "Integumentary Status");
    printview.addsection(
        printview.col(2,
            printview.span("Does patient have IV access?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericIVAccess") && data["GenericIVAccess"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericIVAccess") && data["GenericIVAccess"].Answer == "0" ? "true" : "false"%>))) +<%
    if (data == null || data.ContainsKey("GenericIVAccess") == false || data["GenericIVAccess"].Answer != "0") { %>
        printview.col(3,
            printview.span("<strong>Date of Insertion:</strong><%= data != null && data.ContainsKey("GenericIVAccessDate") && data["GenericIVAccessDate"].Answer.IsNotNullOrEmpty() ? data["GenericIVAccessDate"].Answer : "<span class='blank'></span>"%>") +
            printview.span("<strong>IV Access Type:</strong><%= data != null && data.ContainsKey("GenericIVAccessType") && data["GenericIVAccessType"].Answer.IsNotNullOrEmpty() ? (data["GenericIVAccessType"].Answer == "0" ? "<span class='blank'></span>" : "") + (data["GenericIVAccessType"].Answer == "1" ? "Saline Lock" : "") + (data["GenericIVAccessType"].Answer == "2" ? "PICC Line" : "") + (data["GenericIVAccessType"].Answer == "3" ? "Central Line" : "") + (data["GenericIVAccessType"].Answer == "4" ? "Port-A-Cath" : "") + (data["GenericIVAccessType"].Answer == "5" ? "Med-A-Port" : "") + (data["GenericIVAccessType"].Answer == "6" ? "Other" : "") : "<span class='blank'></span>"%>") +
            printview.span("<strong>IV Location:</strong><%= data != null && data.ContainsKey("GenericIVLocation") && data["GenericIVLocation"].Answer.IsNotNullOrEmpty() ? data["GenericIVLocation"].Answer : "<span class='blank'></span>"%>") +
            printview.span("<strong>Condition of IV Site:</strong><%= data != null && data.ContainsKey("GenericIVConditionOfIVSite") && data["GenericIVConditionOfIVSite"].Answer.IsNotNullOrEmpty() ? (data["GenericIVConditionOfIVSite"].Answer == "0" ? "<span class='blank'></span>" : "") + (data["GenericIVConditionOfIVSite"].Answer == "1" ? "WNL" : "") + (data["GenericIVConditionOfIVSite"].Answer == "2" ? "Phlebitis" : "") + (data["GenericIVConditionOfIVSite"].Answer == "3" ? "Redness" : "") + (data["GenericIVConditionOfIVSite"].Answer == "4" ? "Swelling" : "") + (data["GenericIVConditionOfIVSite"].Answer == "5" ? "Pallor" : "") + (data["GenericIVConditionOfIVSite"].Answer == "6" ? "Warmth" : "") + (data["GenericIVConditionOfIVSite"].Answer == "7" ? "Bleeding" : "") : "<span class='blank'></span>"%>") +
            printview.span("<strong>Dressing Changed on this visit:</strong><%= data != null && data.ContainsKey("GenericIVSiteDressing") && data["GenericIVSiteDressing"].Answer.IsNotNullOrEmpty() ? (data["GenericIVSiteDressing"].Answer == "0" ? "<span class='short blank'></span>" : "") + (data["GenericIVSiteDressing"].Answer == "1" ? "N/A" : "") + (data["GenericIVSiteDressing"].Answer == "2" ? "Yes" : "") + (data["GenericIVSiteDressing"].Answer == "3" ? "No" : "") : "<span class='blank'></span>"%>") +
            printview.span("<strong>Date Dressing Last Changed:</strong><%= data != null && data.ContainsKey("GenericIVAccessDateOfLastDressingChange") && data["GenericIVAccessDateOfLastDressingChange"].Answer.IsNotNullOrEmpty() ? data["GenericIVAccessDateOfLastDressingChange"].Answer : "<span class='blank'></span>"%>") +
            printview.span("<strong>Condition of Dressing:</strong><%= data != null && data.ContainsKey("GenericIVConditionOfDressing") && data["GenericIVConditionOfDressing"].Answer.IsNotNullOrEmpty() ? (data["GenericIVConditionOfDressing"].Answer == "0" ? "<span class='blank'></span>" : "") + (data["GenericIVConditionOfDressing"].Answer == "1" ? "Dry &amp; Intact/WNL" : "") + (data["GenericIVConditionOfDressing"].Answer == "2" ? "Bloody" : "") + (data["GenericIVConditionOfDressing"].Answer == "3" ? "Soiled" : "") + (data["GenericIVConditionOfDressing"].Answer == "4" ? "Other" : "") : "<span class='blank'></span>"%>")) +
        printview.col(2,
            printview.col(2,
                printview.span("Flush:",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericFlush") && data["GenericFlush"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data != null && data.ContainsKey("GenericFlush") && data["GenericFlush"].Answer == "0" ? "true" : "false"%>))) +
            printview.span("flushed with <%= data != null && data.ContainsKey("GenericIVAccessFlushed") && data["GenericIVAccessFlushed"].Answer.IsNotNullOrEmpty() ? data["GenericIVAccessFlushed"].Answer : "<span class='blank'></span>"%>/ml of <%= data != null && data.ContainsKey("GenericIVSiteDressingUnit") && data["GenericIVSiteDressingUnit"].Answer.IsNotNullOrEmpty() ? data["GenericIVSiteDressingUnit"].Answer : "<span class='blank'></span>"%>")) +<%
    } %>"","IV Access");
    printview.addsection(
        printview.checkbox("SN to instruct the <%= data != null && data.ContainsKey("485InstructInfectionSignsSymptomsPerson") && data["485InstructInfectionSignsSymptomsPerson"].Answer.IsNotNullOrEmpty() ? data["485InstructInfectionSignsSymptomsPerson"].Answer : "<span class='blank'></span>"%> on signs and symptoms of infection and infiltration. ",<%= data != null && data.ContainsKey("485IntegumentaryIVInterventions") && data["485IntegumentaryIVInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("SN to change central line dressing every <%= data != null && data.ContainsKey("485ChangeCentralLineEvery") && data["485ChangeCentralLineEvery"].Answer.IsNotNullOrEmpty() ? data["485ChangeCentralLineEvery"].Answer : "<span class='blank'></span>"%> using sterile technique.",<%= data != null && data.ContainsKey("485IntegumentaryIVInterventions") && data["485IntegumentaryIVInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct the <%= data != null && data.ContainsKey("485InstructChangeCentralLinePerson") && data["485InstructChangeCentralLinePerson"].Answer.IsNotNullOrEmpty() ? data["485InstructChangeCentralLinePerson"].Answer : "<span class='blank'></span>"%> to change central line dressing every <%= data != null && data.ContainsKey("485InstructChangeCentralLineEvery") && data["485InstructChangeCentralLineEvery"].Answer.IsNotNullOrEmpty() ? data["485InstructChangeCentralLineEvery"].Answer : "<span class='blank'></span>"%> using sterile technique. ",<%= data != null && data.ContainsKey("485IntegumentaryIVInterventions") && data["485IntegumentaryIVInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("SN to change <%= data != null && data.ContainsKey("485ChangePortDressingType") && data["485ChangePortDressingType"].Answer.IsNotNullOrEmpty() ? data["485ChangePortDressingType"].Answer : "<span class='blank'></span>"%> port dressing using sterile technique every <%= data != null && data.ContainsKey("485ChangePortDressingEvery") && data["485ChangePortDressingEvery"].Answer.IsNotNullOrEmpty() ? data["485ChangePortDressingEvery"].Answer : "<span class='blank'></span>"%>.",<%= data != null && data.ContainsKey("485IntegumentaryIVInterventions") && data["485IntegumentaryIVInterventions"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485IntegumentaryIVInterventionComments") && data["485IntegumentaryIVInterventionComments"].Answer.IsNotNullOrEmpty() ? data["485IntegumentaryIVInterventionComments"].Answer : ""%>",false,2),
        "Interventions");
    printview.addsection(
        printview.checkbox("IV will remain patent and free from signs and symptoms of infection.",<%= data != null && data.ContainsKey("485IntegumentaryIVGoals") && data["485IntegumentaryIVGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("The <%= data != null && data.ContainsKey("485InstructChangeDress") && data["485InstructChangeDress"].Answer.IsNotNullOrEmpty() ? data["485InstructChangeDress"].Answer : "<span class='blank'></span>"%> will demonstrate understanding of changing <%= data != null && data.ContainsKey("485InstructChangeDressSterile") && data["485InstructChangeDressSterile"].Answer.IsNotNullOrEmpty() ? data["485InstructChangeDressSterile"].Answer : "<span class='blank'></span>"%> dressing using sterile technique. ",<%= data != null && data.ContainsKey("485IntegumentaryIVGoals") && data["485IntegumentaryIVGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.span("Additional Goals:",true) +
        printview.span("<%= data != null && data.ContainsKey("485IntegumentaryIVGoalComments") && data["485IntegumentaryIVGoalComments"].Answer.IsNotNullOrEmpty() ? data["485IntegumentaryIVGoalComments"].Answer : ""%>",false,2),
        "Goals");
    printview.addsection(
        printview.span("(M1306) Does this patient have at least one Unhealed Pressure Ulcer at Stage II or Higher or designated as &ldquo;unstageable&rdquo;?",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes",<%= data != null && data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "01" ? "true" : "false"%>)));<%
    if (data == null || data.ContainsKey("M1306UnhealedPressureUlcers") == false || data["M1306UnhealedPressureUlcers"].Answer != "00") { %>
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth colspan=%226%22%3E" +
        printview.span("(M1308) Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each Stage:",true) +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth colspan=%224%22%3E" +
        printview.span("Stage Description&mdash; Unhealed Pressure Ulcer",true) +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Column 1",true) +
        printview.span("Number Currently Present") +
        "%3C/th%3E%3Cth%3E" +
        printview.span("Column 2",true) +
        printview.span("Number of those listed in Column 1 that were present on admission") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("a. Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer with red pink wound bed, without slough. May also present as an intact or open/ruptured serum-filled blister.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerCurrent") && data["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerAdmission") && data["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : ""%>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("b. Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscles are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") && data["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerAdmission") && data["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : ""%>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("c. Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") && data["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedStageIVUlcerAdmission") && data["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : ""%>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("d.1 Unstageable: Known or likely but unstageable due to non-removable dressing or device.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerCurrent") && data["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerAdmission") && data["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : ""%>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("d.2 Unstageable: Known or likely but unstageable due to coverage of wound bed by slough and/or eschar.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") && data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission") && data["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : ""%>") +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        printview.span("d.3 Unstageable: Suspected deep tissue injury in evolution.") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent") && data["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer : ""%>") +
        "%3C/td%3E%3Ctd%3E" +
        printview.span("<%= data != null && data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission") && data["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer.IsNotNullOrEmpty() ? data["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : ""%>") +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E");<%
    } %>
    printview.addsection(
        printview.span("(M1322) Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. The area may be painful, firm, soft, warmer or cooler as compared to adjacent tissue.",true) +
        printview.col(5,
            printview.checkbox("0 &ndash; Zero",<%= data != null && data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; One",<%= data != null && data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Two",<%= data != null && data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Three",<%= data != null && data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Four or More",<%= data != null && data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "04" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1324) Stage of Most Problematic Unhealed (Observable) Pressure Ulcer",true) +
        printview.col(4,
            printview.checkbox("1 &ndash; Stage I",<%= data != null && data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Stage II",<%= data != null && data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Stage III",<%= data != null && data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Stage IV",<%= data != null && data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "04" ? "true" : "false"%>)) +
        printview.checkbox("NA &ndash; No observable pressure ulcer or unhealed pressure ulcer",<%= data != null && data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "NA" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1330) Does this patient have a Stasis Ulcer?",true) +
        printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Yes, patient has BOTH observable and unobservable stasis ulcers",<%= data != null && data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Yes, patient has observable stasis ulcers ONLY",<%= data != null && data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing)",<%= data != null && data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "03" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1332) Current Number of (Observable) Stasis Ulcer(s)",true) +
        printview.col(4,
            printview.checkbox("1 &ndash; One",<%= data != null && data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Two",<%= data != null && data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Three",<%= data != null && data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Four or more",<%= data != null && data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "04" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1334) Status of Most Problematic (Observable) Stasis Ulcer",true) +
        printview.col(4,
            printview.checkbox("0 &ndash; Newly epithelialized",<%= data != null && data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Fully granulating",<%= data != null && data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Early/partial granulation",<%= data != null && data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Not healing",<%= data != null && data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "03" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1340) Does this patient have a Surgical Wound? ",true) +
        printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Yes, patient has at least one (observable) surgical wound",<%= data != null && data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Surgical wound known but not observable due to non-removable dressing",<%= data != null && data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "02" ? "true" : "false"%>));<%
    if (data == null || data.ContainsKey("M1340SurgicalWound") == false || data["M1340SurgicalWound"].Answer != "00") { %>
    printview.addsection(
        printview.span("(M1342) Status of Most Problematic (Observable) Surgical Wound ",true) +
        printview.col(4,
            printview.checkbox("0 &ndash; Newly epithelialized",<%= data != null && data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Fully granulating",<%= data != null && data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Early/partial granulation",<%= data != null && data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Not healing",<%= data != null && data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "03" ? "true" : "false"%>)));<%
    } %>
    printview.addsection(
        printview.span("(M1350) Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy, other than those described above that is receiving intervention by the home health agency?",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; No",<%= data != null && data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes",<%= data != null && data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "01" ? "true" : "false"%>)));<%
    for (int i = 1; i < 6; i++) { %>
    printview.addsection(
        printview.col(2,
            printview.col(2,
                printview.span("Location:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericLocation" + i) && data["GenericLocation" + i].Answer.IsNotNullOrEmpty() ? data["GenericLocation" + i].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("Onset Date:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericOnsetDate" + i) && data["GenericOnsetDate" + i].Answer.IsNotNullOrEmpty() ? data["GenericOnsetDate" + i].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("Wound Type:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericWoundType" + i) && data["GenericWoundType" + i].Answer.IsNotNullOrEmpty() ? data["GenericWoundType" + i].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("Pressure Ulcer Stage:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericPressureUlcerStage" + i) && data["GenericPressureUlcerStage" + i].Answer.IsNotNullOrEmpty() && data["GenericPressureUlcerStage" + i].Answer != "0" ? data["GenericPressureUlcerStage" + i].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("Measurements:",true) +
                printview.col(2,
                    printview.span("Length:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericMeasurementLength" + i) && data["GenericMeasurementLength" + i].Answer.IsNotNullOrEmpty() ? data["GenericMeasurementLength" + i].Answer : ""%>",false,1))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Width:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericMeasurementWidth" + i) && data["GenericMeasurementWidth" + i].Answer.IsNotNullOrEmpty() ? data["GenericMeasurementWidth" + i].Answer : ""%>",false,1)) +
                printview.col(2,
                    printview.span("Depth:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericMeasurementDepth" + i) && data["GenericMeasurementDepth" + i].Answer.IsNotNullOrEmpty() ? data["GenericMeasurementDepth" + i].Answer : ""%>",false,1))) +
            printview.col(2,
                printview.span("Wound Bed:",true) +
                printview.col(2,
                    printview.span("Granulation %:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericWoundBedGranulation" + i) && data["GenericWoundBedGranulation" + i].Answer.IsNotNullOrEmpty() ? data["GenericWoundBedGranulation" + i].Answer : ""%>",false,1))) +
            printview.col(2,
                printview.col(2,
                    printview.span("Slough %:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericWoundBedSlough" + i) && data["GenericWoundBedSlough" + i].Answer.IsNotNullOrEmpty() ? data["GenericWoundBedSlough" + i].Answer : ""%>",false,1)) +
                printview.col(2,
                    printview.span("Eschar %:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericWoundBedEschar" + i) && data["GenericWoundBedEschar" + i].Answer.IsNotNullOrEmpty() ? data["GenericWoundBedEschar" + i].Answer : ""%>",false,1))) +
            printview.col(2,
                printview.span("Surrounding Tissue:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericSurroundingTissue" + i) && data["GenericSurroundingTissue" + i].Answer.IsNotNullOrEmpty() && data["GenericSurroundingTissue" + i].Answer != "0" ? data["GenericSurroundingTissue" + i].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("Drainage:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericDrainage" + i) && data["GenericDrainage" + i].Answer.IsNotNullOrEmpty() && data["GenericDrainage" + i].Answer != "0" ? data["GenericDrainage" + i].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("Drainage Amount:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericDrainageAmount" + i) && data["GenericDrainageAmount" + i].Answer.IsNotNullOrEmpty() && data["GenericDrainageAmount" + i].Answer != "0" ? data["GenericDrainageAmount" + i].Answer : ""%>",false,1)) +
            printview.col(2,
                printview.span("Odor:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericOdor" + i) && data["GenericOdor" + i].Answer.IsNotNullOrEmpty() && data["GenericOdor" + i].Answer != "0" ? data["GenericOdor" + i].Answer : ""%>",false,1)) +
            printview.span("Tunneling:",true) +
            printview.col(2,
                printview.col(2,
                    printview.span("Length:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericTunnelingLength" + i) && data["GenericTunnelingLength" + i].Answer.IsNotNullOrEmpty() ? data["GenericTunnelingLength" + i].Answer : ""%>",false,1)) +
                printview.col(2,
                    printview.span("Time:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericTunnelingTime" + i) && data["GenericTunnelingTime" + i].Answer.IsNotNullOrEmpty() ? data["GenericTunnelingTime" + i].Answer : ""%>",false,1))) +
            printview.span("Undermining:",true) +
            printview.col(2,
                printview.col(2,
                    printview.span("Length:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericUnderminingLength" + i) && data["GenericUnderminingLength" + i].Answer.IsNotNullOrEmpty() ? data["GenericUnderminingLength" + i].Answer : ""%>",false,1)) +
                printview.col(2,
                    printview.span("Time:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericUnderminingTimeFrom" + i) && data["GenericUnderminingTimeFrom" + i].Answer.IsNotNullOrEmpty() ? data["GenericUnderminingTimeFrom" + i].Answer : ""%><%= data != null && data.ContainsKey("GenericUnderminingTimeTo" + i) && data["GenericUnderminingTimeTo" + i].Answer.IsNotNullOrEmpty() ? " to " + data["GenericUnderminingTimeTo" + i].Answer + " o&rsquo;clock" : ""%>",false,1))) +
            printview.span("Device:",true) +
            printview.col(2,
                printview.col(2,
                    printview.span("Type:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericDeviceType" + i) && data["GenericDeviceType" + i].Answer.IsNotNullOrEmpty() ? data["GenericDeviceType" + i].Answer : ""%>",false,1)) +
                printview.col(2,
                    printview.span("Setting:") +
                    printview.span("<%= data != null && data.ContainsKey("GenericDeviceSetting" + i) && data["GenericDeviceSetting" + i].Answer.IsNotNullOrEmpty() ? data["GenericDeviceSetting" + i].Answer : ""%>",false,1)))),
        "Wound <%= i %>");<%
    } %>
    printview.addsection(
        printview.span("Treatment Performed:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericTreatmentPerformed") && data["GenericTreatmentPerformed"].Answer.IsNotNullOrEmpty() ? data["GenericTreatmentPerformed"].Answer : ""%>",false,2) +
        printview.span("Narrative:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericNarrative") && data["GenericNarrative"].Answer.IsNotNullOrEmpty() ? data["GenericNarrative"].Answer : ""%>",false,2));
    printview.addsection(
        printview.checkbox("SN to instruct the <%= data != null && data.ContainsKey("485InstructTurningRepositionPerson") && data["485InstructTurningRepositionPerson"].Answer.IsNotNullOrEmpty() ? data["485InstructTurningRepositionPerson"].Answer : "<span class='blank'></span>"%> on turning/repositioning every 2 hours.",<%= data != null && data.ContainsKey("485IntegumentaryInterventions") && data["485IntegumentaryInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct the <%= data != null && data.ContainsKey("485InstructReduceFrictionPerson") && data["485InstructReduceFrictionPerson"].Answer.IsNotNullOrEmpty() ? data["485InstructReduceFrictionPerson"].Answer : "<span class='blank'></span>"%> on methods to reduce friction and shear.",<%= data != null && data.ContainsKey("485IntegumentaryInterventions") && data["485IntegumentaryInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct the <%= data != null && data.ContainsKey("485InstructPadBonyProminencesPerson") && data["485InstructPadBonyProminencesPerson"].Answer.IsNotNullOrEmpty() ? data["485InstructPadBonyProminencesPerson"].Answer : "<span class='blank'></span>"%> to pad all bony prominences.",<%= data != null && data.ContainsKey("485IntegumentaryInterventions") && data["485IntegumentaryInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("SN to perform/instruct on wound care as follows: <%= data != null && data.ContainsKey("485InstructWoundCarePersonFrequency") && data["485InstructWoundCarePersonFrequency"].Answer.IsNotNullOrEmpty() ? data["485InstructWoundCarePersonFrequency"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("485IntegumentaryInterventions") && data["485IntegumentaryInterventions"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.checkbox("SN to assess skin for breakdown every visit.",<%= data != null && data.ContainsKey("485IntegumentaryInterventions") && data["485IntegumentaryInterventions"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct the <%= data != null && data.ContainsKey("485InstructSignsWoundInfectionPerson") && data["485InstructSignsWoundInfectionPerson"].Answer.IsNotNullOrEmpty() ? data["485InstructSignsWoundInfectionPerson"].Answer : "<span class='blank'></span>"%> on signs/symptoms of wound infection to report to physician, to include increased temp > 100.5, chills, increase in drainage, foul odor, redness, pain and any other significant changes.",<%= data != null && data.ContainsKey("485IntegumentaryInterventions") && data["485IntegumentaryInterventions"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
        printview.checkbox("May discontinue wound care when wound(s) have healed.",<%= data != null && data.ContainsKey("485IntegumentaryInterventions") && data["485IntegumentaryInterventions"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
        printview.checkbox("SN to assess wound for S&amp;S of infection, healing status, wound deterioration, and complications.",<%= data != null && data.ContainsKey("485IntegumentaryInterventions") && data["485IntegumentaryInterventions"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
        printview.checkbox("SN to perform/instruct on incision/suture site care &amp; dressing.",<%= data != null && data.ContainsKey("485IntegumentaryInterventions") && data["485IntegumentaryInterventions"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485IntegumentaryInterventionComments") && data["485IntegumentaryInterventionComments"].Answer.IsNotNullOrEmpty() ? data["485IntegumentaryInterventionComments"].Answer : ""%>",false,2),
        "Interventions");
    printview.addsection(
        printview.checkbox("Wound(s) will heal without complication by the end of the episode.",<%= data != null && data.ContainsKey("StartOfCare_485IntegumentaryGoals") && data["StartOfCare_485IntegumentaryGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("Wound(s) will be free from signs and symptoms of infection during 60 day episode.",<%= data != null && data.ContainsKey("StartOfCare_485IntegumentaryGoals") && data["StartOfCare_485IntegumentaryGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("Wound(s) will decrease in size by <%= data != null && data.ContainsKey("485WoundSizeDecreasePercent") && data["485WoundSizeDecreasePercent"].Answer.IsNotNullOrEmpty() ? data["485WoundSizeDecreasePercent"].Answer : ""%>% by the end of the episode.",<%= data != null && data.ContainsKey("StartOfCare_485IntegumentaryGoals") && data["StartOfCare_485IntegumentaryGoals"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("Patient skin integrity will remain intact during this episode.",<%= data != null && data.ContainsKey("StartOfCare_485IntegumentaryGoals") && data["StartOfCare_485IntegumentaryGoals"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.checkbox("<%= data != null && data.ContainsKey("485DemonstrateSterileDressingTechniquePerson") && data["485DemonstrateSterileDressingTechniquePerson"].Answer.IsNotNullOrEmpty() ? data["485DemonstrateSterileDressingTechniquePerson"].Answer : ""%> will demonstrate understanding of changing <%= data != null && data.ContainsKey("485DemonstrateSterileDressingTechniqueType") && data["485DemonstrateSterileDressingTechniqueType"].Answer.IsNotNullOrEmpty() ? data["485DemonstrateSterileDressingTechniqueType"].Answer : ""%> dressing using sterile technique.",<%= data != null && data.ContainsKey("StartOfCare_485IntegumentaryGoals") && data["StartOfCare_485IntegumentaryGoals"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.span("Additional Goals:",true) +
        printview.span("<%= data != null && data.ContainsKey("485IntegumentaryGoalComments") && data["485IntegumentaryGoalComments"].Answer.IsNotNullOrEmpty() ? data["485IntegumentaryGoalComments"].Answer : ""%>",false,2),
        "Goals");
</script>