﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.checkbox("LOC:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.span("") +
            printview.span("Oriented to:") +
            printview.checkbox("Person",<%= data != null && data.ContainsKey("GenericNeurologicalOriented") && data["GenericNeurologicalOriented"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Place",<%= data != null && data.ContainsKey("GenericNeurologicalOriented") && data["GenericNeurologicalOriented"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Time",<%= data != null && data.ContainsKey("GenericNeurologicalOriented") && data["GenericNeurologicalOriented"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Pupils:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.span("") +
            printview.span("") +
            printview.checkbox("Bilateral",<%= data != null && data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "0" ? "true" : "false"%>) +
            printview.checkbox("Left",<%= data != null && data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("Right",<%= data != null && data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "2" ? "true" : "false"%>) +
            printview.checkbox("Vision:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true)) +
        printview.col(6,
            printview.checkbox("WNL",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Blurred Vision",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Cataracts",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Corrective Lenses",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Glaucoma",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Legally Blind",<%= data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Speech:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeurologicalSpeech") && data["GenericNeurologicalSpeech"].Answer.IsNotNullOrEmpty() ? (data["GenericNeurologicalSpeech"].Answer == "0" ? "" : "") + (data["GenericNeurologicalSpeech"].Answer == "1" ? "Clear" : "") + (data["GenericNeurologicalSpeech"].Answer == "2" ? "Slurred" : "") + (data["GenericNeurologicalSpeech"].Answer == "3" ? "Aphasic" : "") + (data["GenericNeurologicalSpeech"].Answer == "4" ? "Other" : "") : ""%>",false,1) +
            printview.checkbox("Paralysis:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("5") ? "true" : "false"%>,true) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeurologicalParalysisLocation") && data["GenericNeurologicalParalysisLocation"].Answer.IsNotNullOrEmpty() ? "Location: " + data["GenericNeurologicalParalysisLocation"].Answer : "" %>",false,1) +
            printview.checkbox("Tremors:",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("9") ? "true" : "false"%>,true) +
            printview.span("<%= data != null && data.ContainsKey("GenericNeurologicalTremorsLocation") && data["GenericNeurologicalTremorsLocation"].Answer.IsNotNullOrEmpty() ? "Location: " + data["GenericNeurologicalTremorsLocation"].Answer : ""%>",false,1) +
            printview.checkbox("Quadripiegia",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("6") ? "true" : "false"%>,true) +
            printview.checkbox("Parapiegia",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("7") ? "true" : "false"%>,true) +
            printview.checkbox("Seizures",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("8") ? "true" : "false"%>,true) +
            printview.checkbox("Dizziness",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("10") ? "true" : "false"%>,true) +
            printview.checkbox("Headache",<%= data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer.Split(',').Contains("11") ? "true" : "false"%>,true)) +
        printview.span("Comments:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericNeuroEmoBehaviorComments") && data["GenericNeuroEmoBehaviorComments"].Answer.IsNotNullOrEmpty() ? data["GenericNeuroEmoBehaviorComments"].Answer : ""%>",false,2),
        "Neurological");
    printview.addsection(
        printview.col(4,
            printview.checkbox("Oriented",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Comatose",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Forgetful",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Agitated",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Depressed",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Disoriented",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Lethargic",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("485MentalStatusOther") && data["485MentalStatusOther"].Answer.IsNotNullOrEmpty() ? data["485MentalStatusOther"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer.Split(',').Contains("8") ? "true" : "false"%>)) + 
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485MentalStatusComments") && data["485MentalStatusComments"].Answer.IsNotNullOrEmpty() ? data["485MentalStatusComments"].Answer : ""%>",false,2),
        "Mental Status");
    printview.addsection(
        printview.col(4,
            printview.checkbox("WNL (Within Normal Limits)",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Withdrawn",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Expresses Depression",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Impaired Decision Making",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Difficulty coping w/Health Status",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Combative",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Irritability",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericBehaviourOther") && data["GenericBehaviourOther"].Answer.IsNotNullOrEmpty() ? data["GenericBehaviourOther"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer.Split(',').Contains("8") ? "true" : "false"%>)) + 
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericBehaviourComments") && data["GenericBehaviourComments"].Answer.IsNotNullOrEmpty() ? data["GenericBehaviourComments"].Answer : ""%>",false,2),
        "Behaviour Status");
    printview.addsection(
        printview.col(3,
            printview.checkbox("WNL (No Issues Identified)",<%= data != null && data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Poor Home Environment",<%= data != null && data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("No emotional support from family/CG",<%= data != null && data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Verbalize desire for spiritual support",<%= data != null && data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericPsychosocialOther") && data["GenericPsychosocialOther"].Answer.IsNotNullOrEmpty() ? data["GenericPsychosocialOther"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer.Split(',').Contains("5") ? "true" : "false"%>)) + 
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericPsychosocialComments") && data["GenericPsychosocialComments"].Answer.IsNotNullOrEmpty() ? data["GenericPsychosocialComments"].Answer : ""%>",false,2),
        "Psychosocial");
    printview.addsection(
        printview.col(4,
            printview.checkbox("Motivated Learner",<%= data != null && data.ContainsKey("485PatientStrengths") && data["485PatientStrengths"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Strong Support System",<%= data != null && data.ContainsKey("485PatientStrengths") && data["485PatientStrengths"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Absence of Multiple Diagnosis",<%= data != null && data.ContainsKey("485PatientStrengths") && data["485PatientStrengths"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Enhanced Socioeconomic Status",<%= data != null && data.ContainsKey("485PatientStrengths") && data["485PatientStrengths"].Answer.Split(',').Contains("4") ? "true" : "false"%>)) +
        printview.span("Other:",true) +
        printview.span("<%= data != null && data.ContainsKey("485PatientStrengthOther") && data["485PatientStrengthOther"].Answer.IsNotNullOrEmpty() ? data["485PatientStrengthOther"].Answer : ""%>",false,2),
        "Patient Strengths");
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth%3E%3C/th%3E%3Cth%3ENot at all%3Cbr /%3E(0 &ndash; 1 day)%3C/th%3E%3Cth%3EServeral Days%3Cbr /%3E(2 &ndash; 6 days)%3C/th%3E%3Cth%3EMore than half of the days%3Cbr /%3E(7 &ndash; 11 days)%3C/th%3E%3Cth%3ENearly every day%3Cbr /%3E(12 &ndash; 14 days)%3C/th%3E%3Cth%3EN/A Unable to respond%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3Ea) Little interest or pleasure in doing things%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "02" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "03" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA",<%= data != null && data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd%3Eb) Feeling down, depressed, or hopeless?%3C/td%3E%3Ctd%3E" +
        printview.checkbox("0",<%= data != null && data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "00" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("1",<%= data != null && data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "01" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("2",<%= data != null && data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "02" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("3",<%= data != null && data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "03" ? "true" : "false"%>) +
        "%3C/td%3E%3Ctd%3E" +
        printview.checkbox("NA",<%= data != null && data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "NA" ? "true" : "false"%>) +
        "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E","PHQ-2&copy;");
    printview.addsection(
        printview.checkbox("SN to notify physicain this pateint was screened for depression using PHQ-2&copy; scale and meets criteria for further evaluation for depression.",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("SN to perform a neurological assessment each visit.",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("SN to assess/instruct on seizure disorder signs &amp; symptoms and appropriate actions during seizure activity.",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct the <%= data != null && data.ContainsKey("485InstructSeizurePrecautionPerson") && data["485InstructSeizurePrecautionPerson"].Answer.IsNotNullOrEmpty() ? data["485InstructSeizurePrecautionPerson"].Answer : "<span class='blank'></span>"%> on seizure precautions.",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct caregiver on orientation techniques to use when patient becomes disoriented.",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.checkbox("MSW: <%= data != null && data.ContainsKey("485MSWCommunityAssistanceVisits") && data["485MSWCommunityAssistanceVisits"].Answer.IsNotNullOrEmpty() && data["485MSWCommunityAssistanceVisits"].Answer == "1" ? "1&ndash;2" : (data != null && data.ContainsKey("485MSWCommunityAssistanceVisitAmount") && data["485MSWCommunityAssistanceVisitAmount"].Answer.IsNotNullOrEmpty() ? data["485MSWCommunityAssistanceVisitAmount"].Answer : "<span class='blank'></span>")%> visits, every 60 days for community resource assistance. ",<%= data != null && data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485BehaviorComments") && data["485BehaviorComments"].Answer.IsNotNullOrEmpty() ? data["485BehaviorComments"].Answer : ""%>",false,2),
        "Interventions");
    printview.addsection(
        printview.checkbox("Neuro status will be within normal limits and free of S&amp;S of complications or further deterioration.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("Patient will exhibit stable neuro status evidenced by increased alertness, mobility and self-care by the end of the episode.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("Patient will exhibit stable neuro status evidence by absence of seizure activity by the end of the episode.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("Patient will have optimal cognitive functioning within parameters established for the stage of disease by the end of the episode.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.checkbox("Patient will remain free from increased confusion during the episode.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.checkbox("The <%= data != null && data.ContainsKey("485VerbalizeSeizurePrecautionsPerson") && data["485VerbalizeSeizurePrecautionsPerson"].Answer.IsNotNullOrEmpty() ? data["485VerbalizeSeizurePrecautionsPerson"].Answer : "<span class='blank'></span>"%> will verbalize understanding of seizure precautions.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
        printview.checkbox("Caregiver will verbalize understanding of proper orientation techniques to use when patient becomes disoriented.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
        printview.checkbox("Patient&rsquo;s community resource needs will be met with assistance of social worker.",<%= data != null && data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
        printview.span("Additional Goals:",true) +
        printview.span("<%= data != null && data.ContainsKey("485BehaviorGoalComments") && data["485BehaviorGoalComments"].Answer.IsNotNullOrEmpty() ? data["485BehaviorGoalComments"].Answer : ""%>",false,2),
        "Goals");
</script>