﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationTherapyNeedForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id) %>
<%= Html.Hidden("Recertification_Action", "Edit") %>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("Recertification_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "Recertification") %>
<%= Html.Hidden("categoryType", "TherapyNeed")%> 
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS M2200</legend>
        <div class="wide_column">
            <div class="row">
                <label for="Recertification_M2200NumberOfTherapyNeed" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2200');">(M2200)</a> Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)? (Enter zero [&ldquo;000&rdquo;] if no therapy visits indicated.)</label>
                <div id="Recertification_M2200Right" class="float_right">
                    <label>Number of therapy visits indicated (total of physical, occupational and speech-language pathology combined).</label>
                    <%=Html.TextBox("Recertification_M2200NumberOfTherapyNeed", data.ContainsKey("M2200NumberOfTherapyNeed") ? data["M2200NumberOfTherapyNeed"].Answer : "", new { @id = "Recertification_M2200NumberOfTherapyNeed", @class = "vitals numeric", @maxlength = "3" })%>
                </div>
                <div class="clear"></div>
                <div class="float_right oasis">
                    <input name="Recertification_M2200TherapyNeedNA" value="" type="hidden" />
                    <%= string.Format("<input id='Recertification_M2200TherapyNeedNA' class='radio' name='Recertification_M2200TherapyNeedNA' value='1' type='checkbox' {0} onclick=\"Oasis.blockText($(this),'#Recertification_M2200NumberOfTherapyNeed');\"/>", data.ContainsKey("M2200TherapyNeedNA") && data["M2200TherapyNeedNA"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="Recertification_M2200TherapyNeedNA">NA &ndash; Not Applicable: No case mix group defined by this assessment.</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2200');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Interventions</legend>
        <%string[] therapyInterventions = data.ContainsKey("485TherapyInterventions") && data["485TherapyInterventions"].Answer != "" ? data["485TherapyInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485TherapyInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485TherapyInterventions1' class='radio float_left' name='Recertification_485TherapyInterventions' value='1' type='checkbox' {0} />", therapyInterventions != null && therapyInterventions.Contains("1") ? "checked='checked'" : "")%>
                <label for="Recertification_485TherapyInterventions1" class="radio">Physical therapist to evaluate and submit plan of treatment.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485TherapyInterventions2' class='radio float_left' name='Recertification_485TherapyInterventions' value='2' type='checkbox' {0} />", therapyInterventions != null && therapyInterventions.Contains("2") ? "checked='checked'" : "")%>
                <label for="Recertification_485TherapyInterventions2" class="radio">Occupational therapist to evaluate and submit plan of treatment.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485TherapyInterventions3' class='radio float_left' name='Recertification_485TherapyInterventions' value='3' type='checkbox' {0} />", therapyInterventions != null && therapyInterventions.Contains("3") ? "checked='checked'" : "")%>
                <label for="Recertification_485TherapyInterventions3" class="radio">Speech therapist to evaluate and submit plan of treatment.</label>
            </div><div class="row">
                <label for="Recertification_485TherapyOrderTemplates">Additional Orders:</label>
                <%  var therapyOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485TherapyOrderTemplates") && data["485TherapyOrderTemplates"].Answer != "" ? data["485TherapyOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485TherapyOrderTemplates", therapyOrderTemplates)%>
                <%= Html.TextArea("Recertification_485TherapyComments", data.ContainsKey("485TherapyComments") ? data["485TherapyComments"].Answer : "", 5, 70, new { @id = "Recertification_485TherapyComments" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
           <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Recertification.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('Recertification_ValidationContainer','{0}','{1}','{2}','Recertification');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div> 
<% } %>
<script type="text/javascript">
    Oasis.hideIfChecked($("#Recertification_M2200TherapyNeedNA"), $("#Recertification_M2200Right"));
</script>