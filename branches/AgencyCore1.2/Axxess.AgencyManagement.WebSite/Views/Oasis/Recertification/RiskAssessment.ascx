﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationRiskAssessmentForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("Recertification_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<%= Html.Hidden("categoryType", "RiskAssessment")%>
<div class="wrapper main">
    <fieldset>
        <legend>Interventions</legend>
        <input type="hidden" name="Recertification_485RiskAssessmentIntervention" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485RiskAssessmentIntervention1' name='Recertification_485RiskAssessmentIntervention' value='1' class='radio float_left' type='checkbox' {0} />", data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer == "1" ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RiskAssessmentIntervention1" class="radio">SN to assist patient to obtain ERS button.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RiskAssessmentIntervention2' name='Recertification_485RiskAssessmentIntervention' value='2' class='radio float_left' type='checkbox' {0} />", data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer == "2" ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RiskAssessmentIntervention2" class="radio">SN to develop individualized emergency plan with patient.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RiskAssessmentIntervention3' name='Recertification_485RiskAssessmentIntervention' value='3' class='radio float_left' type='checkbox' {0} />", data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer == "3" ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RiskAssessmentIntervention3" class="radio">SN to instruct patient on importance of receiving influenza and pneumococcal vaccines.</label>
            </div><div class="row">
                <label for="Recertification_485RiskInterventionTemplates" class="float_left">Additional Orders</label>
                <%  var riskInterventionTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "Sample Template", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485RiskInterventionTemplates") && data["485RiskInterventionTemplates"].Answer != "" ? data["485RiskInterventionTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("Recertification_485RiskInterventionTemplates", riskInterventionTemplates) %>
                <%= Html.TextArea("Recertification_485RiskInterventionComments", data.ContainsKey("485RiskInterventionComments") ? data["485RiskInterventionComments"].Answer : "", 5, 70, new { @id = "Recertification_485RiskInterventionComments" }) %>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Goals</legend>
        <input type="hidden" name="Recertification_485RiskAssessmentGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485RiskAssessmentGoals1' name='Recertification_485RiskAssessmentGoals' value='1' class='radio float_left' type='checkbox' {0} />", data.ContainsKey("485RiskAssessmentGoals") && data["485RiskAssessmentGoals"].Answer == "1" ? "checked='checked'" : "" ) %>
                <label for="Recertification_485RiskAssessmentGoals1" class="radio">The patient will have no hospitalizations during the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485RiskAssessmentGoals2' name='Recertification_485RiskAssessmentGoals' value='2' class='radio float_left' type='checkbox' {0} />", data.ContainsKey("485RiskAssessmentGoals") && data["485RiskAssessmentGoals"].Answer == "2" ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485RiskAssessmentGoals2">The</label>
                    <%  var verbalizeEmergencyPlanPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485VerbalizeEmergencyPlanPerson") && data["485VerbalizeEmergencyPlanPerson"].Answer != "" ? data["485VerbalizeEmergencyPlanPerson"].Answer : "Patient/Caregiver"); %>
                    <%= Html.DropDownList("Recertification_485VerbalizeEmergencyPlanPerson", verbalizeEmergencyPlanPerson) %>
                    <label for="Recertification_485RiskAssessmentGoals2">will verbalize understanding of individualized emergency plan by the end of the episode.</label>
                </span>
            </div><div class="row">
                <label for="Recertification_485RiskGoalTemplates" class="float_left">Additional Goals</label>
                <%  var riskGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "Sample Template", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485RiskGoalTemplates") && data["485RiskGoalTemplates"].Answer != "" ? data["485RiskGoalTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("Recertification_485RiskGoalTemplates", riskGoalTemplates) %>
                <%= Html.TextArea("Recertification_485RiskGoalComments", data.ContainsKey("485RiskGoalComments") ? data["485RiskGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485RiskGoalComments" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);"  onclick="Recertification.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
           <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowOasisValidationModal('Recertification_ValidationContainer','{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "Recertification")%></li>
        </ul>
    </div>
</div>
<% } %>