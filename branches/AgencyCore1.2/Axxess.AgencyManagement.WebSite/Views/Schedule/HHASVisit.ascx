﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "HHASVisitForm" })) {
         var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('HHA Supervisory Visit | ",
        Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "",
        "','hhasVisit');</script>")%>
<%= Html.Hidden("HHASVisit_PatientId", Model.PatientId)%>
<%= Html.Hidden("HHASVisit_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("HHASVisit_EventId", Model.EventId)%>
<%= Html.Hidden("Type", "HHASVisit")%>
<div class="wrapper main">
    <fieldset>
        <legend>HHA Supervisory Visit Information</legend>
        <div class="column">
            <div class="row">
                <label for="HHASVisit_PreviousNotes" class="float_left">View Previous Notes:</label>
                <div class="float_right"><select name="HHASVisit_PreviousNotes" id="HHASVisit_PreviousNotes"><option value=" " selected="true"></option><option value="00">0</option></select></div>
            </div><div class="row">
                <label class="float_left">Time:</label>
                <div class="float_right">
                    <label for="HHASVisit_TimeIn">In:&nbsp; &nbsp;</label><%= Html.TextBox("HHASVisit_TimeIn",data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : "", new { @class = "st", @id = "HHASVisit_TimeIn" })%> <div class="clear"></div>
                    <label for="HHASVisit_TimeOut">Out:</label><%= Html.TextBox("HHASVisit_TimeOut",data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : "", new { @class = "st", @id = "HHASVisit_TimeOut" })%>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="HHASVisit_VisitDate" class="float_left">Visit Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("HHASVisit_VisitDate").Value(Model.EventDate.FormatWith("MM/dd/yyy")).MaxDate(Model.EndDate).MinDate(Model.StartDate).HtmlAttributes(new { @id = "HHASVisit_VisitDate", @class = "date required" })%></div>
            </div><div class="row">
                <label for="HHASVisit_AssociatedMileage" class="float_left">Associated Mileage:</label>
                <div class="float_right"><%= Html.TextBox("HHASVisit_AssociatedMileage",data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : "", new { @class = "st digitd",@maxlength=6 ,@id = "HHASVisit_AssociatedMileage" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>HHA Supervisory Visit Evaluation</legend>
        <div class="wide_column">
            <div class="row">
                <div class="float_left"><span class="alphali">1.</span>Arrives for assigned visits as scheduled:</div>
                <div class="float_right">
                    <%= Html.RadioButton("HHASVisit_ArriveOnTime", "1", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "1" ? true : false, new { @id = "HHASVisit_ArriveOnTimeY", @class = "radio" })%><label class="inlineradio" for="HHASVisit_ArriveOnTimeY">Yes</label>
                    <%= Html.RadioButton("HHASVisit_ArriveOnTime", "0", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "0" ? true : false, new { @id = "HHASVisit_ArriveOnTimeN", @class = "radio" })%><label class="inlineradio" for="HHASVisit_ArriveOnTimeN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">2.</span>Follows client&rsquo;s care of plan:</div>
                <div class="float_right">
                    <%= Html.RadioButton("HHASVisit_FollowPOC", "1", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "1" ? true : false, new { @id = "HHASVisit_FollowPOCY", @class = "radio" })%><label class="inlineradio" for="HHASVisit_FollowPOCY">Yes</label>
                    <%= Html.RadioButton("HHASVisit_FollowPOC", "0", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "0" ? true : false, new { @id = "HHASVisit_FollowPOCN", @class = "radio" })%><label class="inlineradio" for="HHASVisit_FollowPOCN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">3.</span>Demonstrates positive and helpful attitude towards the client and others:</div>
                <div class="float_right">
                    <%= Html.RadioButton("HHASVisit_HasPositiveAttitude", "1", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "1" ? true : false, new { @id = "HHASVisit_HasPositiveAttitudeY", @class = "radio" })%><label class="inlineradio" for="HHASVisit_HasPositiveAttitudeY">Yes</label>
                    <%= Html.RadioButton("HHASVisit_HasPositiveAttitude", "0", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "0" ? true : false, new { @id = "HHASVisit_HasPositiveAttitudeN", @class = "radio" })%><label class="inlineradio" for="HHASVisit_HasPositiveAttitudeN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">4.</span>Informs Nurse Supervisor of client needs and changes in condition as appropriate:</div>
                <div class="float_right">
                    <%= Html.RadioButton("HHASVisit_InformChanges", "1", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "1" ? true : false, new { @id = "HHASVisit_InformChangesY", @class = "radio" })%><label class="inlineradio" for="HHASVisit_InformChangesY">Yes</label>
                    <%= Html.RadioButton("HHASVisit_InformChanges", "0", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "0" ? true : false, new { @id = "HHASVisit_InformChangesN", @class = "radio" })%><label class="inlineradio" for="HHASVisit_InformChangesN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">5.</span>Aide Implements Universal Precautions per agency policy:</div>
                <div class="float_right">
                    <%= Html.RadioButton("HHASVisit_IsUniversalPrecautions", "1", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "1" ? true : false, new { @id = "HHASVisit_IsUniversalPrecautionsY", @class = "radio" })%><label class="inlineradio" for="HHASVisit_IsUniversalPrecautionsY">Yes</label>
                    <%= Html.RadioButton("HHASVisit_IsUniversalPrecautions", "0", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "0" ? true : false, new { @id = "HHASVisit_IsUniversalPrecautionsN", @class = "radio" })%><label class="inlineradio" for="HHASVisit_IsUniversalPrecautionsN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">6.</span>Any changes made to client care of plan at this time:</div>
                <div class="float_right">
                    <%= Html.RadioButton("HHASVisit_POCChanges", "1", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "1" ? true : false, new { @id = "HHASVisit_POCChangesY", @class = "radio" })%><label class="inlineradio" for="HHASVisit_POCChangesY">Yes</label>
                    <%= Html.RadioButton("HHASVisit_POCChanges", "0", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "0" ? true : false, new { @id = "HHASVisit_POCChangesN", @class = "radio" })%><label class="inlineradio" for="HHASVisit_POCChangesN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">7.</span>Patient/CG satisfied with care and services provided by aide:</div>
                <div class="float_right">
                    <%= Html.RadioButton("HHASVisit_IsServicesSatisfactory", "1", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "1" ? true : false, new { @id = "HHASVisit_IsServicesSatisfactoryY", @class = "radio" })%><label class="inlineradio" for="HHASVisit_IsServicesSatisfactoryY">Yes</label>
                    <%= Html.RadioButton("HHASVisit_IsServicesSatisfactory", "0", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "0" ? true : false, new { @id = "HHASVisit_IsServicesSatisfactoryN", @class = "radio" })%><label class="inlineradio" for="HHASVisit_IsServicesSatisfactoryN">No</label>
                </div>
            </div><div class="row">
                <div><span class="alphali">8.</span>Additional Comments/Findings:</div>
                <div><%= Html.TextArea("HHASVisit_AdditionalComments", data.ContainsKey("AdditionalComments") ? data["AdditionalComments"].Answer : "")%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="HHASVisit_Signature" class="float_left">Signature</label>
                <div class="float_right"><%= Html.Password("HHASVisit_Clinician", "", new { @class = "", @id = "HHASVisit_Clinician" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="HHASVisit_SignatureDate" class="float_left">Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("HHASVisit_SignatureDate").Value(data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer.ToDateTime().ToString("MM/dd/yyy") : DateTime.Today.ToString("MM/dd/yyy")).MaxDate(DateTime.Now).MinDate(Model.StartDate).HtmlAttributes(new { @id = "HHASVisit_SignatureDate" })%></div>
            </div>
        </div>
    </fieldset>
    <input type="hidden" name="button" value="" id="HHASVisit_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="HHASVisitRemove(); $('#HHASVisit_Button').val($(this).html()).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="HHASVisitAdd(); $('#HHASVisit_Button').val($(this).html()).closest('form').submit();">Submit</a></li>
            <li><a href="javascript:void(0);" onclick="HHASVisitRemove();UserInterface.CloseWindow('hhasVisit');">Close</a></li>
        </ul>
    </div>
</div>
<% } %>  
<script type="text/javascript">
    $("#HHASVisit_TimeIn").timeEntry({ spinnerImage: '/Images/gui/spinnerUpDown.png',
        spinnerSize: [15, 16, 0], spinnerIncDecOnly: true
    });
    $("#HHASVisit_TimeOut").timeEntry({ spinnerImage: '/Images/gui/spinnerUpDown.png',
        spinnerSize: [15, 16, 0], spinnerIncDecOnly: true
    });
    $("#window_hhasVisit form label.error").css({ 'position': 'relative', 'float': 'left' });
    function HHASVisitAdd() {
        $("#HHASVisit_TimeIn").removeClass('required').addClass('required');
        $("#HHASVisit_TimeOut").removeClass('required').addClass('required');
        $("#HHASVisit_Clinician").removeClass('required').addClass('required');
        $("#HHASVisit_SignatureDate").removeClass('required').addClass('required');
    }
    function HHASVisitRemove() {
        $("#HHASVisit_TimeIn").removeClass('required');
        $("#HHASVisit_TimeOut").removeClass('required');
        $("#HHASVisit_Clinician").removeClass('required');
        $("#HHASVisit_SignatureDate").removeClass('required');
    }
</script>