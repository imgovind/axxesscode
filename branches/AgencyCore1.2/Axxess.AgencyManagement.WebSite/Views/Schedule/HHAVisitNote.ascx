﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "HHAideVisitForm" })) {
        var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Home Health Aide Progress Note | ",
        Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "",
        "','hhAideVisit');</script>")%>
<%= Html.Hidden("HHAideVisit_PatientId", Model.PatientId)%>
<%= Html.Hidden("HHAideVisit_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("HHAideVisit_EventId", Model.EventId)%>
<%= Html.Hidden("Type", "HHAideVisit")%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="2">Home Health Aide Progress Note</th></tr>
            <tr>
                <td colspan="2">
                    <div class="third">
                        <label for="HHAideVisit_PreviousSummary" class="float_left">View Previous Note:</label>
                        <div class="float_right"><select name="HHAideVisit_PreviousSummary" id="HHAideVisit_PreviousSummary"><option value=" " selected="true"></option><option value="00">0</option></select></div>
                    </div><div class="third">
                        <label for="HHAideVisit_PatientName" class="float_left">Patient Name:</label>
                        <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Patients, "HHAideVisit_PatientName", Model.Patient.Id.ToString(), new { @id = "HHAideVisit_PatientName", @disabled = "disabled" })%></div>
                    </div><div class="third">
                        <label for="HHAideVisit_MR" class="float_left">MR#:</label>
                        <div class="float_right"><%= Html.TextBox("HHAideVisit_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty, new { @id = "HHAideVisit_MR", @readonly = "readonly" })%></div>
                    </div><div class="third">
                        <label for="HHAideVisit_VisitDate" class="float_left">Visit Date:</label>
                        <div class="float_right"><%= Html.Telerik().DatePicker().Name("HHAideVisit_VisitDate").Value(Model.EventDate.FormatWith("MM/dd/yyy")).MaxDate(Model.EndDate).MinDate(Model.StartDate).MaxDate(Model.EndDate).MinDate(Model.StartDate).HtmlAttributes(new { @id = "HHAideVisit_VisitDate", @class = "date required" })%></div>
                    </div><div class="third">
                        <label for="HHAideVisit_TimeIn" class="float_left">Time In:</label>
                        <div class="float_right"><%= Html.TextBox("HHAideVisit_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : string.Empty, new { @id = "HHAideVisit_TimeIn", @class = "loc" })%></div>
                    </div><div class="third">
                        <label for="HHAideVisit_TimeOut" class="float_left">Time Out:</label>
                        <div class="float_right"><%= Html.TextBox("HHAideVisit_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : string.Empty, new { @id = "HHAideVisit_TimeOut", @class = "loc" })%></div>
                    </div><div class="third">
                        <label for="HHAideVisit_EpsPeriod" class="float_left">Episode/Period:</label>
                        <div class="float_right"><%= Html.TextBox("HHAideVisit_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = "HHAideVisit_EpsPeriod", @readonly = "readonly" })%></div>
                    </div><div class="third">
                        <label for="HHAideVisit_VisitDate" class="float_left">HHA Frequency:</label>
                        <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Physicians, "HHAideVisit_Physician", Model.PhysicianId.ToString(), new { @id = "HHAideVisit_Physician" })%></div>
                    </div><div class="third">
                        <label for="HHAideVisit_DNR" class="float_left">DNR:</label>
                        <div class="float_right"><%= Html.RadioButton("HHAideVisit_DNR", "1", data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? true : false, new { @id = "HHAideVisit_DNR1", @class = "radio" })%><label for="HHAideVisit_DNR1" class="inlineradio">Yes</label><%=Html.RadioButton("HHAideVisit_DNR", "0", data.ContainsKey("DNR") && data["DNR"].Answer == "0" ? true : false, new { @id = "HHAideVisit_DNR2", @class = "radio" })%><label for="HHAideVisit_DNR2" class="inlineradio">No</label></div>
                    </div><div class="half">
                        <label for="HHAideVisit_PrimaryDiagnosis" class="float_left">Primary Diagnosis:</label>
                        <div class="float_right"><%= Html.TextBox("HHAideVisit_PrimaryDiagnosis", data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : string.Empty, new { @id = "HHAideVisit_PrimaryDiagnosis" })%></div>
                    </div><div class="half">
                        <label for="HHAideVisit_PrimaryDiagnosis1" class="float_left">Secondary Diagnosis:</label>
                        <div class="float_right"><%= Html.TextBox("HHAideVisit_PrimaryDiagnosis1", data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : string.Empty, new { @id = "HHAideVisit_PrimaryDiagnosis1" })%></div>
                    </div>
                </td>
            </tr><% string[] isVitalSignParameter = data.ContainsKey("IsVitalSignParameter") && data["IsVitalSignParameter"].Answer != "" ? data["IsVitalSignParameter"].Answer.Split(',') : null; %><input name="HHAideVisit_IsVitalSignParameter" value=" " type="hidden" />
            <tr><th>Vital Sign Parameters &mdash; <%= string.Format("<input class='radio' id='HHAideVisit_IsVitalSignParameter' name='HHAideVisit_IsVitalSignParameter' value='1' type='checkbox' {0} />", isVitalSignParameter != null && isVitalSignParameter.Contains("1") ? "checked='checked'" : "")%><label for="HHAideVisit_IsVitalSignParameter">N/A</label></th><% string[] isVitalSigns = data.ContainsKey("IsVitalSigns") && data["IsVitalSigns"].Answer != "" ? data["IsVitalSigns"].Answer.Split(',') : null; %><input name="HHAideVisit_IsVitalSigns" value=" " type="hidden" /><th>Vital Signs&mdash; <%= string.Format("<input class='radio' id='HHAideVisit_IsVitalSigns' name='HHAideVisit_IsVitalSigns' value='1' type='checkbox' {0} />", isVitalSigns != null && isVitalSigns.Contains("1") ? "checked='checked'" : "")%><label for="HHAideVisit_IsVitalSigns">N/A</label></th></tr>
            <tr>
                <td>
                    <table class="fixed vitalsigns">
                        <tbody>
                            <tr><th></th><th>SBP</th><th>DBP</th><th>HR</th><th>Resp</th><th>Temp</th><th>Weight</th></tr>
                            <tr>
                                <th>greater than (&gt;)</th>
                                <td><%= Html.TextBox("HHAideVisit_SystolicBPGreaterThan", data.ContainsKey("SystolicBPGreaterThan") ? data["SystolicBPGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_SystolicBPGreaterThan", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_DiastolicBPGreaterThan", data.ContainsKey("DiastolicBPGreaterThan") ? data["DiastolicBPGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_DiastolicBPGreaterThan", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_PulseGreaterThan", data.ContainsKey("PulseGreaterThan") ? data["PulseGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_PulseGreaterThan", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_RespirationGreaterThan", data.ContainsKey("RespirationGreaterThan") ? data["RespirationGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_RespirationGreaterThan", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_TempGreaterThan", data.ContainsKey("TempGreaterThan") ? data["TempGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_TempGreaterThan", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_WeightGreaterThan", data.ContainsKey("WeightGreaterThan") ? data["WeightGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_WeightGreaterThan", @class = "fill" })%></td>
                            </tr><tr>
                                <th>less than (&lt;)</th>
                                <td><%= Html.TextBox("HHAideVisit_SystolicBPLessThan", data.ContainsKey("SystolicBPLessThan") ? data["SystolicBPLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_SystolicBPLessThan", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_DiastolicBPLessThan", data.ContainsKey("DiastolicBPLessThan") ? data["DiastolicBPLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_DiastolicBPLessThan", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_PulseLessThan", data.ContainsKey("PulseLessThan") ? data["PulseLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_PulseLessThan", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_RespirationLessThan", data.ContainsKey("RespirationLessThan") ? data["RespirationLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_RespirationLessThan", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_TempLessThan", data.ContainsKey("TempLessThan") ? data["TempLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_TempLessThan", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_WeightLessThan", data.ContainsKey("WeightLessThan") ? data["WeightLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_WeightLessThan", @class = "fill" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td><td>
                    <table class="fixed vitalsignparameter">
                        <tbody>
                            <tr><th></th><th>BP</th><th>HR</th><th>Temp</th><th>Resp</th><th>Weight</th></tr>
                            <tr>
                                <th>Val.</th>
                                <td><%= Html.TextBox("HHAideVisit_VitalSignBPVal", data.ContainsKey("VitalSignBPVal") ? data["VitalSignBPVal"].Answer : "", new { @id = "HHAideVisit_VitalSignBPVal", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_VitalSignHRVal", data.ContainsKey("VitalSignHRVal") ? data["VitalSignHRVal"].Answer : "", new { @id = "HHAideVisit_VitalSignHRVal", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_VitalSignTempVal", data.ContainsKey("VitalSignTempVal") ? data["VitalSignTempVal"].Answer : "", new { @id = "HHAideVisit_VitalSignTempVal", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_VitalSignRespVal", data.ContainsKey("VitalSignRespVal") ? data["VitalSignRespVal"].Answer : "", new { @id = "HHAideVisit_VitalSignRespVal", @class = "fill" })%></td>
                                <td><%= Html.TextBox("HHAideVisit_VitalSignWeightVal", data.ContainsKey("VitalSignWeightVal") ? data["VitalSignWeightVal"].Answer : "", new { @id = "HHAideVisit_VitalSignWeightVal", @class = "fill" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2">Tasks</th></tr>
            <tr>
                <td>
                    <table class="fixed">
                        <thead>
                            <tr><th colspan="3">Assignment</th><th colspan="3">Status</th></tr>
                            <tr><th colspan="3">Personal Care</th><th>Completed</th><th>Refuse</th><th>N/A</th></tr>
                            <tr>
                                <td colspan="3">Bed Bath</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareBedBath", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareBedBath", "2", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareBedBath", "1", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareBedBath", "0", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Assist with Chair Bath</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareAssistWithChairBath", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareAssistWithChairBath", "2", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareAssistWithChairBath", "1", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareAssistWithChairBath", "0", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Tub Bath</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareTubBath", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareTubBath", "2", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareTubBath", "1", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareTubBath", "0", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Shower</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareShower", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareShower", "2", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareShower", "1", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareShower", "0", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Shower w/Chair</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareShowerWithChair", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareShowerWithChair", "2", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareShowerWithChair", "1", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareShowerWithChair", "0", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Shampoo Hair</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareShampooHair", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareShampooHair", "2", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareShampooHair", "1", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareShampooHair", "0", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Hair Care/Comb Hair</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareHairCare", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareHairCare", "2", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareHairCare", "1", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareHairCare", "0", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Oral Care</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareOralCare", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareOralCare", "2", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareOralCare", "1", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareOralCare", "0", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Skin Care</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareSkinCare", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareSkinCare", "2", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareSkinCare", "1", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareSkinCare", "0", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Pericare</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCarePericare", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCarePericare", "2", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCarePericare", "1", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCarePericare", "0", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Nail Care</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareNailCare", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareNailCare", "2", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareNailCare", "1", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareNailCare", "0", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Shave</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareShave", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareShave", "2", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareShave", "1", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareShave", "0", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Assist with Dressing</td>
                                <td><%=Html.Hidden("HHAideVisit_PersonalCareAssistWithDressing", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_PersonalCareAssistWithDressing", "2", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareAssistWithDressing", "1", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_PersonalCareAssistWithDressing", "0", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <th colspan="3">Nutrition</th><th>Completed</th><th>Refuse</th><th>N/A</th>
                            </tr><tr>
                                <td colspan="3">Meal Set-up</td>
                                <td><%=Html.Hidden("HHAideVisit_NutritionMealSetUp", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_NutritionMealSetUp", "2", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_NutritionMealSetUp", "1", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_NutritionMealSetUp", "0", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Assist with Feeding</td>
                                <td><%=Html.Hidden("HHAideVisit_NutritioAssistWithFeeding", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_NutritioAssistWithFeeding", "2", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_NutritioAssistWithFeeding", "1", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_NutritioAssistWithFeeding", "0", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr>
                        </thead>
                    </table>
                </td><td>
                    <table class="fixed">
                        <thead>
                            <tr><th colspan="3">Assignment</th><th colspan="3">Status</th></tr>
                            <tr><th colspan="3">Elimination</th><th>Completed</th><th>Refuse</th><th>N/A</th></tr>
                            <tr>
                                <td colspan="3">Assist with Bed Pan/Urinal</td>
                                <td><%=Html.Hidden("HHAideVisit_EliminationAssistWithBedPan", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_EliminationAssistWithBedPan", "2", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationAssistWithBedPan", "1", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationAssistWithBedPan", "0", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Assist with BSC</td>
                                <td><%=Html.Hidden("HHAideVisit_EliminationAssistWithBSC", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_EliminationAssistBSC", "2", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationAssistWithBSC", "1", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationAssistWithBSC", "0", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Incontinence Care</td>
                                <td><%=Html.Hidden("HHAideVisit_EliminationIncontinenceCare", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_EliminationIncontinenceCare", "2", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationIncontinenceCare", "1", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationIncontinenceCare", "0", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Empty Drainage Bag</td>
                                <td><%=Html.Hidden("HHAideVisit_EliminationEmptyDrainageBag", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_EliminationEmptyDrainageBag", "2", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationEmptyDrainageBag", "1", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationEmptyDrainageBag", "0", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Record Bowel Movement</td>
                                <td><%=Html.Hidden("HHAideVisit_EliminationRecordBowelMovement", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_EliminationRecordBowelMovement", "2", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationRecordBowelMovement", "1", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationRecordBowelMovement", "0", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Catheter Care</td>
                                <td><%=Html.Hidden("HHAideVisit_EliminationCatheterCare", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_EliminationCatheterCare", "2", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationCatheterCare", "1", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_EliminationCatheterCare", "0", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <th colspan="3">Activity</th><th>Completed</th><th>Refuse</th><th>N/A</th>
                            </tr><tr>
                                <td colspan="3">Dangle on Side of Bed</td>
                                <td><%=Html.Hidden("HHAideVisit_ActivityDangleOnSideOfBed", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_ActivityDangleOnSideOfBed", "2", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_ActivityDangleOnSideOfBed", "1", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_ActivityDangleOnSideOfBed", "0", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Turn &amp; Position</td>
                                <td><%=Html.Hidden("HHAideVisit_ActivityTurnPosition", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_ActivityTurnPosition", "2", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_ActivityTurnPosition", "1", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_ActivityTurnPosition", "0", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Assist with Transfer</td>
                                <td><%=Html.Hidden("HHAideVisit_ActivityAssistWithTransfer", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_ActivityAssistWithTransfer", "2", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_ActivityAssistWithTransfer", "1", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_ActivityAssistWithTransfer", "0", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Assist with Ambulation</td>
                                <td><%=Html.Hidden("HHAideVisit_ActivityAssistWithAmbulation", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_ActivityAssistWithAmbulation", "2", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_ActivityAssistWithAmbulation", "1", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_ActivityAssistWithAmbulation", "0", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Range of Motion</td>
                                <td><%=Html.Hidden("HHAideVisit_ActivityRangeOfMotion", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_ActivityRangeOfMotion", "2", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_ActivityRangeOfMotion", "1", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_ActivityRangeOfMotion", "0", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <th colspan="3">Household Task</th><th>Completed</th><th>Refuse</th><th>N/A</th>
                            </tr><tr>
                                <td colspan="3">Make Bed</td>
                                <td><%=Html.Hidden("HHAideVisit_HouseholdTaskMakeBed", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_HouseholdTaskMakeBed", "2", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_HouseholdTaskMakeBed", "1", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_HouseholdTaskMakeBed", "0", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Change Linen</td>
                                <td><%=Html.Hidden("HHAideVisit_HouseholdTaskChangeLinen", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_HouseholdTaskChangeLinen", "2", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_HouseholdTaskChangeLinen", "1", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_HouseholdTaskChangeLinen", "0", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Light Housekeeping</td>
                                <td><%=Html.Hidden("HHAideVisit_HouseholdTaskLightHousekeeping", " ", new { @id = "" })%><%=Html.RadioButton("HHAideVisit_HouseholdTaskLightHousekeeping", "2", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_HouseholdTaskLightHousekeeping", "1", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                                <td><%=Html.RadioButton("HHAideVisit_HouseholdTaskLightHousekeeping", "0", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                            </tr><tr>
                                <td colspan="3">Other (Describe):</td><td colspan="3"><%= Html.TextBox("HHAideVisit_HouseholdTaskOther", data.ContainsKey("HouseholdTaskOther") ? data["HouseholdTaskOther"].Answer : string.Empty, new { @id = "HHAideVisit_HouseholdTaskOther", @class = "fill" })%></td>
                            </tr>
                        </thead>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2">Comments</th></tr>
            <tr><td colspan="2"><%= Html.TextArea("HHAideVisit_Comment", data.ContainsKey("Comment") ? data["Comment"].Answer : string.Empty, new { @id = "HHAideVisit_Comment", @class = "fill" })%></td></tr>
            <tr><th colspan="2">Electronic Signature</th></tr>
            <tr>
                <td colspan="2">
                    <div class="third">
                        <label for="HHAideVisit_ClinicianSignature" class="float_left">Clinician Signature:</label>
                        <div class="float_right"><%= Html.Password("HHAideVisit_Clinician", "", new { @id = "HHAideVisit_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="HHAideVisit_ClinicianSignatureDate" class="float_left">Date:</label>
                        <div class="float_right"><%= Html.Telerik().DatePicker().Name("HHAideVisit_SignatureDate").Value(data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer.ToDateTime().ToString("MM/dd/yyy") : DateTime.Today.ToString("MM/dd/yyy")).HtmlAttributes(new { @id = "HHAideVisit_SignatureDate", @class = "date" })%></div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="HHAideVisit_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="HHAideVisitRemove(); $('#HHAideVisit_Button').val($(this).html()).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="HHAideVisitAdd(); $('#HHAideVisit_Button').val($(this).html()).closest('form').submit();">Submit</a></li>
            <li><a href="javascript:void(0);" onclick="HHAideVisitRemove(); UserInterface.CloseWindow('hhAideVisit');">Close</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $("#HHAideVisit_IsVitalSignParameter").click(function() {
    if ($('#HHAideVisit_IsVitalSignParameter').is(':checked')) $("#window_hhAideVisit .vitalsigns").each(function() { $(this).hide(); });
        else $("#window_hhAideVisit .vitalsigns").each(function() { $(this).show(); });
    });
    if ($('#HHAideVisit_IsVitalSignParameter').is(':checked')) $("#window_hhAideVisit .vitalsigns").each(function() { $(this).hide(); });
    else $("#window_hhAideVisit .vitalsigns").each(function() { $(this).show(); });

    $("#HHAideVisit_IsVitalSigns").click(function() {
    if ($('#HHAideVisit_IsVitalSigns').is(':checked')) $("#window_hhAideVisit .vitalsignparameter").each(function() { $(this).hide(); });
    else $("#window_hhAideVisit .vitalsignparameter").each(function() { $(this).show(); });
    });
    if ($('#HHAideVisit_IsVitalSigns').is(':checked')) $("#window_hhAideVisit .vitalsignparameter").each(function() { $(this).hide(); });
    else $("#window_hhAideVisit .vitalsignparameter").each(function() { $(this).show(); });
    
    $("#HHAideVisit_TimeIn").timeEntry({ spinnerImage: '/Images/gui/spinnerUpDown.png',
        spinnerSize: [15, 16, 0], spinnerIncDecOnly: true
    });
    $("#HHAideVisit_TimeOut").timeEntry({ spinnerImage: '/Images/gui/spinnerUpDown.png',
        spinnerSize: [15, 16, 0], spinnerIncDecOnly: true
    });
    $("#HHAideVisit_MR").attr('readonly', true);
    $("#HHAideVisit_EpsPeriod").attr('readonly', true);
    function HHAideVisitAdd() {
        $("#HHAideVisit_TimeIn").removeClass('required').addClass('required');
        $("#HHAideVisit_TimeOut").removeClass('required').addClass('required');
        $("#HHAideVisit_Clinician").removeClass('required').addClass('required');
        $("#HHAideVisit_SignatureDate").removeClass('required').addClass('required');
    }
    function HHAideVisitRemove() {
        $("#HHAideVisit_TimeIn").removeClass('required');
        $("#HHAideVisit_TimeOut").removeClass('required');
        $("#HHAideVisit_Clinician").removeClass('required');
        $("#HHAideVisit_SignatureDate").removeClass('required');
    }
   
</script>
