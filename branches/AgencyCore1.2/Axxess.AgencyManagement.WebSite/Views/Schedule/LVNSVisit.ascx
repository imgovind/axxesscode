﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "LVNSVisitForm" })) {
        var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('LVN Supervisory Visit | ",
        Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "",
        "','lvnsVisit');</script>")%>
<%= Html.Hidden("LVNSVisit_PatientId", Model.PatientId)%>
<%= Html.Hidden("LVNSVisit_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("LVNSVisit_EventId", Model.EventId)%>
<%= Html.Hidden("Type", "LVNSVisit")%>
<div class="wrapper main">
    <fieldset>
        <legend>Licensed Vocational Nurse Supervisory Visit Information</legend>
        <div class="column">
            <div class="row">
                <label for="LVNSVisit_PreviousNotes" class="float_left">View Previous Notes:</label>
                <div class="float_right"><select name="LVNSVisit_PreviousNotes" id="LVNSVisit_PreviousNotes"><option value=" " selected="true"></option><option value="00">0</option></select></div>
            </div><div class="row">
                <label class="float_left">Time:</label>
                <div class="float_right">
                    <label for="LVNSVisit_TimeIn">In:&nbsp; &nbsp;</label><%= Html.TextBox("LVNSVisit_TimeIn",data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : "", new { @class = "st", @id = "LVNSVisit_TimeIn" })%><div class="clear"></div>
                    <label for="LVNSVisit_TimeOut">Out:</label><%= Html.TextBox("LVNSVisit_TimeOut",data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : "", new { @class = "st", @id = "LVNSVisit_TimeOut" })%>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="LVNSVisit_VisitDate" class="float_left">Visit Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("LVNSVisit_VisitDate").Value(Model.EventDate.FormatWith("MM/dd/yyy")).MaxDate(Model.EndDate).MinDate(Model.StartDate).HtmlAttributes(new { @id = "LVNSVisit_VisitDate", @class = "required date" })%></div>
            </div><div class="row">
                <label for="LVNSVisit_AssociatedMileage" class="float_left">Associated Mileage:</label>
                <div class="float_right"><%= Html.TextBox("LVNSVisit_AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : "", new { @class = "st digits", @maxlength = 6, @id = "LVNSVisit_AssociatedMileage" })%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Licensed Vocational Nurse Supervisory Visit Evaluation</legend>
        <div class="wide_column">
            <div class="row">
                <div class="float_left"><span class="alphali">1.</span>Arrives for assigned visits as scheduled:</div>
                <div class="float_right">
                    <%= Html.RadioButton("LVNSVisit_ArriveOnTime", "1", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "1" ? true : false, new { @id = "LVNSVisit_ArriveOnTimeY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_ArriveOnTimeY">Yes</label>
                    <%= Html.RadioButton("LVNSVisit_ArriveOnTime", "0", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "0" ? true : false, new { @id = "LVNSVisit_ArriveOnTimeN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_ArriveOnTimeN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">2.</span>Follows client&rsquo;s care of plan:</div>
                <div class="float_right">
                    <%= Html.RadioButton("LVNSVisit_FollowPOC", "1", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "1" ? true : false, new { @id = "LVNSVisit_FollowPOCY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_FollowPOCY">Yes</label>
                    <%= Html.RadioButton("LVNSVisit_FollowPOC", "0", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "0" ? true : false, new { @id = "LVNSVisit_FollowPOCN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_FollowPOCN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">3.</span>Demonstrates positive and helpful attitude towards the client and others:</div>
                <div class="float_right">
                    <%= Html.RadioButton("LVNSVisit_HasPositiveAttitude", "1", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "1" ? true : false, new { @id = "LVNSVisit_HasPositiveAttitudeY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_HasPositiveAttitudeY">Yes</label>
                    <%= Html.RadioButton("LVNSVisit_HasPositiveAttitude", "0", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "0" ? true : false, new { @id = "LVNSVisit_HasPositiveAttitudeN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_HasPositiveAttitudeN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">4.</span>Informs Nurse Supervisor/Case Manager of client needs and changes in condition:</div>
                <div class="float_right">
                    <%= Html.RadioButton("LVNSVisit_InformChanges", "1", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "1" ? true : false, new { @id = "LVNSVisit_InformChangesY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_InformChangesY">Yes</label>
                    <%= Html.RadioButton("LVNSVisit_InformChanges", "0", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "0" ? true : false, new { @id = "LVNSVisit_InformChangesN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_InformChangesN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">5.</span>Implements Universal Precautions per agency policy:</div>
                <div class="float_right">
                    <%= Html.RadioButton("LVNSVisit_IsUniversalPrecautions", "1", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "1" ? true : false, new { @id = "LVNSVisit_IsUniversalPrecautionsY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_IsUniversalPrecautionsY">Yes</label>
                    <%= Html.RadioButton("LVNSVisit_IsUniversalPrecautions", "0", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "0" ? true : false, new { @id = "LVNSVisit_IsUniversalPrecautionsN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_IsUniversalPrecautionsN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">6.</span>Any changes made to client care of plan at this time:</div>
                <div class="float_right">
                    <%= Html.RadioButton("LVNSVisit_POCChanges", "1", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "1" ? true : false, new { @id = "LVNSVisit_POCChangesY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_POCChangesY">Yes</label>
                    <%= Html.RadioButton("LVNSVisit_POCChanges", "0", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "0" ? true : false, new { @id = "LVNSVisit_POCChangesN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_POCChangesN">No</label>
                </div>
            </div><div class="row">
                <div class="float_left"><span class="alphali">7.</span>Patient/CG satisfied with care and services provided by LVN/LPN:</div>
                <div class="float_right">
                    <%= Html.RadioButton("LVNSVisit_IsServicesSatisfactory", "1", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "1" ? true : false, new { @id = "LVNSVisit_IsServicesSatisfactoryY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_IsServicesSatisfactoryY">Yes</label>
                    <%= Html.RadioButton("LVNSVisit_IsServicesSatisfactory", "0", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "0" ? true : false, new { @id = "LVNSVisit_IsServicesSatisfactoryN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_IsServicesSatisfactoryN">No</label>
                </div>
            </div><div class="row">
                <div><span class="alphali">8.</span>Additional Comments/Findings:</div>
                <div><%= Html.TextArea("LVNSVisit_AdditionalComments", data.ContainsKey("AdditionalComments") ? data["AdditionalComments"].Answer : "")%></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row">
                <label for="LVNSVisit_Signature" class="float_left">Signature</label>
                <div class="float_right"><%= Html.Password("LVNSVisit_Clinician", "", new { @class = "", @id = "LVNSVisit_Clinician" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="LVNSVisit_SignatureDate" class="float_left">Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("LVNSVisit_SignatureDate").Value(data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer.ToDateTime().ToString("MM/dd/yyy") : DateTime.Today.ToString("MM/dd/yyy")).MaxDate(DateTime.Now).MinDate(Model.StartDate).HtmlAttributes(new { @id = "LVNSVisit_SignatureDate" })%></div>
            </div>
        </div>
    </fieldset>
    <input type="hidden" name="button" value="" id="LVNSVisit_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="LVNSVisitRemove(); $('#LVNSVisit_Button').val($(this).html()).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="LVNSVisitAdd(); $('#LVNSVisit_Button').val($(this).html()).closest('form').submit();">Submit</a></li>
            <li><a href="javascript:void(0);" onclick="LVNSVisitRemove(); UserInterface.CloseWindow('lvnsVisit');">Close</a></li>
        </ul>
    </div>
</div>
<% } %>  
<script type="text/javascript">
    $("#LVNSVisit_TimeIn").timeEntry({ spinnerImage: '/Images/gui/spinnerUpDown.png',
        spinnerSize: [15, 16, 0], spinnerIncDecOnly: true
    });
    $("#LVNSVisit_TimeOut").timeEntry({ spinnerImage: '/Images/gui/spinnerUpDown.png',
        spinnerSize: [15, 16, 0], spinnerIncDecOnly: true
    });
    function LVNSVisitAdd() {
        $("#LVNSVisit_TimeIn").removeClass('required').addClass('required');
        $("#LVNSVisit_TimeOut").removeClass('required').addClass('required');
        $("#LVNSVisit_Clinician").removeClass('required').addClass('required');
        $("#LVNSVisit_SignatureDate").removeClass('required').addClass('required');
    }
    function LVNSVisitRemove() {
        $("#LVNSVisit_TimeIn").removeClass('required');
        $("#LVNSVisit_TimeOut").removeClass('required');
        $("#LVNSVisit_Clinician").removeClass('required');
        $("#LVNSVisit_SignatureDate").removeClass('required');
    }
    
</script>