﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Logon>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Login - Axxess Home Health Management System</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group
        .Add("account.css")
        .Combined(true)
        .Compress(true).CacheDurationInDays(3))
    %>
</head>
<body>
    <div id="login-wrapper">
        <div id="login-window">
            <div class="box-header"><img src="/Images/icons/axxess_icon.png" alt="Axxess Logo" /><span class="title">Axxess&trade; Login</span></div>
            <div class="box">
                <div id="messages"></div>
                <% using (Html.BeginForm("LogOn", "Account", FormMethod.Post, new { @id = "loginForm", @class = "login", @returnUrl = Request.QueryString["returnUrl"] })) %>
                <% { %>
                <img src="/images/gui/axxess_logo.jpeg" alt="Axxess Logo" id="logo" />
                <div class="row">
                    <%= Html.LabelFor(m => m.UserName) %>
                    <%= Html.TextBoxFor(m => m.UserName, new { @id="txtUsername", @class = "required" })%>
                </div>
                <div class="row">
                    <%= Html.LabelFor(m => m.Password) %>
                    <%= Html.PasswordFor(m => m.Password, new { @id="txtPassword", @class = "required" })%>
                </div>
                <div class="forgot">
                    <a href="/Forgot">Forgot your password?</a>
                </div>
                <div class="row tl">
                    <input type="checkbox" id="rememberme" checked="checked" class="checkbox" />
                    <label class="checkbox tl strong" for="rememberme">Remember me</label>
                </div>
                <input type="submit" value="Login" class="button" />
                <% } %>
            </div>
        </div>
    </div>
    <% Html.Telerik().ScriptRegistrar().jQuery(true)
         .DefaultGroup(group => group
             .Add("/Plugins/jquery.validate.js")
             .Add("/Plugins/jquery.form.js")
             .Add("/Plugins/jquery.blockUI.js")
             .Add("/Modules/Utility.js")
             .Add("/Modules/Account.js")
             .Compress(true).Combined(true).CacheDurationInDays(3))
        .OnDocumentReady(() =>
        { 
    %>
    Logon.Init();
    <% 
        }).Render(); %>
</body>
</html>

