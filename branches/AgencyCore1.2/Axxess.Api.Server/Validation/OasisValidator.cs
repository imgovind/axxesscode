﻿namespace Axxess.Api.Server
{
    using System;
    using System.Reflection;
    using System.Collections.Generic;

    using OASIS_Validation;

    using Axxess.Api.Contracts;

    internal static class OasisValidator
    {
        private static string ICD9_PATH = Environment.CurrentDirectory;
        private static string DICT_PATH = Environment.CurrentDirectory;

        public static List<ValidationError> CheckDataString(string oasisDataString)
        {
            Console.WriteLine("CheckDataString Method: {0}", oasisDataString);
            List<ValidationError> errors = new List<ValidationError>();

            try
            {
                int errorCount = 0;
                string[] dupList = new string[] { };
                string[] errorTypeList = new string[] { };
                string[] descriptionList = new string[] { };

                clsEasyValidate easyValidator = new clsEasyValidate();
                easyValidator.ErrorCheck_Oasis(oasisDataString, ICD9_PATH, DICT_PATH, true, ref errorCount, ref descriptionList, ref dupList, ref errorTypeList);

                Console.WriteLine("Error Count: {0}", errorCount);
                
                if (descriptionList.Length == dupList.Length && errorTypeList.Length == dupList.Length)
                {
                    for (int i = 0; i < descriptionList.Length; i++)
                    {
                        ValidationError error = new ValidationError { ErrorType = errorTypeList[i], Description = descriptionList[i], ErrorDup = dupList[i] };
                        errors.Add(error);
                        Console.WriteLine(error.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            
            return errors;
        }
    }
}
