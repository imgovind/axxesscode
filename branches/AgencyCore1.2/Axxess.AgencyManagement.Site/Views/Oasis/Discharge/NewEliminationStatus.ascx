﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyEliminationForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1600) Has this patient been treated for a Urinary Tract Infection in the past
                14 days?
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1600UrinaryTractInfection", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1600UrinaryTractInfection", "00", data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - No<br />
                <%=Html.RadioButton("DischargeFromAgency_M1600UrinaryTractInfection", "01", data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Yes<br />
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.RadioButton("DischargeFromAgency_M1600UrinaryTractInfection", "NA", data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
                - Patient on prophylactic treatment<br />
                <%=Html.RadioButton("DischargeFromAgency_M1600UrinaryTractInfection", "UK", data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "UK" ? true : false, new { @id = "" })%>&nbsp;UK
                - Unknown [Omit “UK” option on DC]
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1610) Urinary Incontinence or Urinary Catheter Presence:
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("DischargeFromAgency_M1610UrinaryIncontinence", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M1610UrinaryIncontinence", "00", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - No incontinence or catheter (includes anuria or ostomy for urinary drainage)
            [ Go to M1620 ]<br />
            <%=Html.RadioButton("DischargeFromAgency_M1610UrinaryIncontinence", "01", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Patient is incontinent<br />
            <%=Html.RadioButton("DischargeFromAgency_M1610UrinaryIncontinence", "02", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Patient requires a urinary catheter (i.e., external, indwelling, intermittent,
            suprapubic) [ Go to M1620 ]
        </div>
    </div>
</div>
<div class="rowOasis" id="discharge_M1615">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1615) When does Urinary Incontinence occur?
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1615UrinaryIncontinenceOccur", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1615UrinaryIncontinenceOccur", "00", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Timed-voiding defers incontinence<br />
                <%=Html.RadioButton("DischargeFromAgency_M1615UrinaryIncontinenceOccur", "01", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Occasional stress incontinence<br />
                <%=Html.RadioButton("DischargeFromAgency_M1615UrinaryIncontinenceOccur", "02", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - During the night only<br />
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.RadioButton("DischargeFromAgency_M1615UrinaryIncontinenceOccur", "03", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - During the day only<br />
                <%=Html.RadioButton("DischargeFromAgency_M1615UrinaryIncontinenceOccur", "04", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - During the day and night
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1620) Bowel Incontinence Frequency:
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.Hidden("DischargeFromAgency_M1620BowelIncontinenceFrequency", " ", new { @id = "" })%>
                <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "00", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Very rarely or never has bowel incontinence<br />
                <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "01", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Less than once weekly<br />
                <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "02", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - One to three times weekly<br />
                <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "03", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Four to six times weekly<br />
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "04", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - On a daily basis<br />
                <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "05", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - More often than once daily<br />
                <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "NA", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
                - Patient has ostomy for bowel elimination<br />
                <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "UK", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "UK" ? true : false, new { @id = "" })%>&nbsp;UK
                - Unknown [Omit “UK” option on FU, DC]
            </div>
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Discharge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
