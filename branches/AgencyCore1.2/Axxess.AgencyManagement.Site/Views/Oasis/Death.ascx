﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="oasisAssWindowContainer">
    <div id="deathTabs" class=" tabs vertical-tabs vertical-tabs-left OasisContainer">
        <ul>
            <li><a href="#clinicalRecord_death">Clinical Record Items</a></li>
            <li><a href="#deathAdd_death">Death</a></li>
        </ul>
        <div style="width: 179px;">
            <input id="dischargeFromAgencyDeathValidation" type="button" value="Validate" onclick="DeathAtHome.Validate('<%=Model.Id%>');" /></div>
        <div id="clinicalRecord_death" class="general abs">
            <% Html.RenderPartial("~/Views/Oasis/Death/NewDemographics.ascx", Model); %>
        </div>
        <div id="deathAdd_death" class="general abs">
        </div>
    </div>
</div>
<% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/DeathAtHome.js"))
       .OnDocumentReady(() =>
        {%>
DeathAtHome.Init();
<%}); 
%>