﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpRespiratoryForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1400) When is the patient dyspneic or noticeably Short of Breath?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M1400PatientDyspneic", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M1400PatientDyspneic", "00", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Patient is not short of breath<br />
            <%=Html.RadioButton("FollowUp_M1400PatientDyspneic", "01", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - When walking more than 20 feet, climbing stairs<br />
            <%=Html.RadioButton("FollowUp_M1400PatientDyspneic", "02", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - With moderate exertion (e.g., while dressing, using commode or bedpan, walking
            distances less than 20 feet)<br />
            <%=Html.RadioButton("FollowUp_M1400PatientDyspneic", "03", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - With minimal exertion (e.g., while eating, talking, or performing other ADLs)
            or with agitation<br />
            <%=Html.RadioButton("FollowUp_M1400PatientDyspneic", "04", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - At rest (during day or night)<br />
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="FollowUp.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="FollowUp.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
