﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpMedicationForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M2030) Management of Injectable Medications: Patient's current ability to prepare
                and take all prescribed injectable medications reliably and safely, including administration
                of correct dosage at the appropriate times/intervals. Excludes IV medications.
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("FollowUp_M2030ManagementOfInjectableMedications", " ", new { @id = "" })%>
            <%=Html.RadioButton("FollowUp_M2030ManagementOfInjectableMedications", "00", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - Able to independently take the correct medication(s) and proper dosage(s) at the
            correct times.<br />
            <%=Html.RadioButton("FollowUp_M2030ManagementOfInjectableMedications", "01", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Able to take injectable medication(s) at the correct times if:<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) individual syringes are prepared in
            advance by another person; OR<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) another person develops a drug diary
            or chart.<br />
            <%=Html.RadioButton("FollowUp_M2030ManagementOfInjectableMedications", "02", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Able to take medication(s) at the correct times if given reminders by another
            person based on the frequency of the injection<br />
            <%=Html.RadioButton("FollowUp_M2030ManagementOfInjectableMedications", "03", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Unable to take injectable medication unless administered by another person.<br />
            <%=Html.RadioButton("FollowUp_M2030ManagementOfInjectableMedications", "NA", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No injectable medications prescribed.
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="FollowUp.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="FollowUp.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
