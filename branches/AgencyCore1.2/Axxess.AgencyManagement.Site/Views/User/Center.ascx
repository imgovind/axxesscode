﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="employee_window" class="abs window ui-widget-content">
    <div class="abs window_inner ">
        <div class="window_top ">
            <span class="float_left">Employee</span><span class="float_right"><a href="javascript:void(0);"
                class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                    href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content">
            <div class="temp-container">
                <div id='employee_Splitter1' class="xSplitter splitterContainer">
                    <div class="xsp-pane sidepanel">
                        <div class="side">
                            <div class="sideTop">
                                <div class="patientMenu">
                                    <ul>
                                        <li>
                                            <div>
                                                <input type="button" onclick="JQD.open_window('#newpatient');" value="New Employee" />
                                            </div>
                                        </li>
                                        <li>
                                            <div>
                                                <input type="button" onclick="JQD.open_window_external('#editPatient');" value="Edit Employee" />
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="spacingDiv">
                                </div>
                                <div class="row">
                                    <div class="divLabel">
                                        <label>
                                            View:</label></div>
                                    <div class="inputs">
                                        <span class="input_wrapper blank">
                                            <select name="list" class="sideDropDown">
                                                <option value="0">All Employees</option>
                                                <option value="1">Active Employees</option>
                                                <option value="2">Released Employees</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="spacingDiv">
                                </div>
                                <div class="row">
                                    <div class="divLabel">
                                        <label>
                                            Filter:</label></div>
                                    <div class="inputs">
                                        <span class="input_wrapper blank">
                                            <select name="list" class="sideDropDown">
                                                <option value="0">All</option>
                                                <option value="1">Skilled Nurse</option>
                                                <option value="2">Home Health Aide</option>
                                                <option value="3">Physical Therapist</option>
                                                <option value="4">Speech Therapist</option>
                                                <option value="5">Occup. Therapist</option>
                                                <option value="6">Med Soc Worker</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="spacingDiv">
                                </div>
                                <div class="row">
                                    <div class="divLabel">
                                        <label>
                                            Find:</label></div>
                                    <div class="inputs">
                                        <span class="input_wrapper">
                                            <input class="text" name="" value="" type="text" size="18" style="width: 184px;" /></span>
                                    </div>
                                </div>
                                <div class="spacingDiv">
                                </div>
                            </div>
                            <div class="sideBottomEmp">
                                <table id="EmployeeList">
                                    <thead>
                                        <tr>
                                            <th width="100">
                                                Last Name
                                            </th>
                                            <th width="100">
                                                First Name
                                            </th>
                                            <th width="100">
                                                Employee Id
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                Jones
                                            </td>
                                            <td>
                                                Anthony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Michaels
                                            </td>
                                            <td>
                                                Jessica
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jonah
                                            </td>
                                            <td>
                                                Paul
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Brown
                                            </td>
                                            <td>
                                                Tony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Schmidth
                                            </td>
                                            <td>
                                                Troy
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jones
                                            </td>
                                            <td>
                                                Anthony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Michaels
                                            </td>
                                            <td>
                                                Jessica
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jonah
                                            </td>
                                            <td>
                                                Paul
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Brown
                                            </td>
                                            <td>
                                                Tony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Schmidth
                                            </td>
                                            <td>
                                                Troy
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jones
                                            </td>
                                            <td>
                                                Anthony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Michaels
                                            </td>
                                            <td>
                                                Jessica
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jonah
                                            </td>
                                            <td>
                                                Paul
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Brown
                                            </td>
                                            <td>
                                                Tony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Schmidth
                                            </td>
                                            <td>
                                                Troy
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jones
                                            </td>
                                            <td>
                                                Anthony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Michaels
                                            </td>
                                            <td>
                                                Jessica
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jonah
                                            </td>
                                            <td>
                                                Paul
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Brown
                                            </td>
                                            <td>
                                                Tony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Schmidth
                                            </td>
                                            <td>
                                                Troy
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jonah
                                            </td>
                                            <td>
                                                Paul
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Brown
                                            </td>
                                            <td>
                                                Tony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Schmidth
                                            </td>
                                            <td>
                                                Troy
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jones
                                            </td>
                                            <td>
                                                Anthony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Michaels
                                            </td>
                                            <td>
                                                Jessica
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jonah
                                            </td>
                                            <td>
                                                Paul
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Brown
                                            </td>
                                            <td>
                                                Tony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Schmidth
                                            </td>
                                            <td>
                                                Troy
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jones
                                            </td>
                                            <td>
                                                Anthony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Michaels
                                            </td>
                                            <td>
                                                Jessica
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jonah
                                            </td>
                                            <td>
                                                Paul
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jones
                                            </td>
                                            <td>
                                                Anthony
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Michaels
                                            </td>
                                            <td>
                                                Jessica
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Jonah
                                            </td>
                                            <td>
                                                Paul
                                            </td>
                                            <td>
                                                54285157g5
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="abs window_main xsp-pane ">
                        <div id='employee_Splitter12' class='abs xSplitter'>
                            <div class='abs xsp-pane mainTop'>
                                <div class="mainTopContainer">
                                    <div class="mainTopContainerLeft">
                                        <div>
                                            <div>
                                                <div class="patientMenu">
                                                    <ul>
                                                        <li>New Orders |</li>
                                                        <li>New Progress Notes |</li>
                                                        <li>New Communication Notes |</li>
                                                        <li>Attach Document |</li>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <label>
                                                        <b style="color: #53868B;">Patient Information&nbsp;&nbsp;&nbsp;</b></label></div>
                                            </div>
                                            <div class="underLine">
                                            </div>
                                        </div>
                                        <div class="mainTopContent">
                                            <div class="mainTopContentLeft">
                                                <div class="row">
                                                    <div class="fixedRow">
                                                        <label>
                                                            Employee ID #:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Span9" class="input_wrapper blank">345632&nbsp;</span>
                                                    </div>
                                                    <br />
                                                    <div class="fixedRow">
                                                        <label>
                                                            Employee Name:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Span10" class="input_wrapper blank">Paul Jones&nbsp;</span>
                                                    </div>
                                                    <br />
                                                    <div class="fixedRow">
                                                        <label>
                                                            Address:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Span13" class="input_wrapper blank">10641 Steppington Dr</span>
                                                        <br />
                                                        <span id="Span14" class="input_wrapper blank">Dallas&nbsp;,TX</span>
                                                    </div>
                                                    <div class="breakLineRow">
                                                    </div>
                                                    <div class="fixedRow">
                                                        <label>
                                                            Gender:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Span37" class="input_wrapper blank">Male</span>
                                                    </div>
                                                    <br />
                                                    <div class="fixedRow">
                                                        <label>
                                                            Birth Date:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Span38" class="input_wrapper blank">12/12/1954</span>
                                                    </div>
                                                    <br />
                                                    <div class="inputs" style="float: right;">
                                                        <br />
                                                        <span class="input_wrapper blank"><a href="http://maps.google.com/maps?hl=en&tab=wl">
                                                            Map </a>| <a href="javascript:void(0);">Direction</a>&nbsp;</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mainTopContentRight">
                                                <div class="fixedRow">
                                                    <label>
                                                        Home Phone:&nbsp;&nbsp;&nbsp;</label></div>
                                                <div class="inputs">
                                                    <span id="Span35" class="input_wrapper blank">214-678-8970</span>
                                                </div>
                                                <br />
                                                <div class="fixedRow">
                                                    <label>
                                                        Alt Phone:&nbsp;&nbsp;&nbsp;</label></div>
                                                <div class="inputs">
                                                    <span id="Span36" class="input_wrapper blank">469-678-8970</span>
                                                </div>
                                                <div class="breakLineRow">
                                                </div>
                                                <div class="row">
                                                    <div class="fixedRow">
                                                        <label>
                                                            Email:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Span49" class="input_wrapper blank">birhanuabagaro@gmail.com</span>
                                                    </div>
                                                    <br />
                                                    <div class="fixedRow">
                                                        <label>
                                                            Branch Location:&nbsp;&nbsp;&nbsp;</label></div>
                                                    <div class="inputs">
                                                        <span id="Span51" class="input_wrapper blank">Dallas, TX</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="underLine">
                                        </div>
                                        <div class="NoteContainer">
                                            <div class="NoteContainerLeft">
                                                <div class="fixedRow">
                                                    <label>
                                                        Notes&nbsp;&nbsp;&nbsp;</label></div>
                                            </div>
                                            <div class="NoteContainerRight">
                                                <input id="Button2" type="button" onclick="JQD.open_window('#editNote');" value="Edit Notes" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mainTopContainerRight">
                                        <div>
                                            <b>Reports for this Employee</b>
                                            <div class="underLine">
                                            </div>
                                            <ul>
                                                <li><a href="javascript:void(0);">Employee Profile</a> </li>
                                                <li><a href="javascript:void(0);" onclick="JQD.open_window('#schedule_window');">View
                                                    Employee schedule</a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='abs xsp-pane'>
                                <div class="submenu">
                                    <ul class="pureCssMenu">
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">485</a></li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">Orders</a></li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">Nursing
                                            <![if gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
                                            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
                                        </li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">Therapy|<![if
                                            gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
                                            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
                                        </li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">PT</a></li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">OT</a></li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">ST</a></li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">HHA<![if
                                            gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
                                            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
                                        </li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">MSW<![if
                                            gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
                                            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
                                        </li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">Progress
                                            Notes</a></li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">Visits</a></li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">Scheduled
                                            Visits</a></li>
                                        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">Missed Visits</a></li>
                                    </ul>
                                </div>
                                <table id="Table5" class="data sortable">
                                    <thead>
                                        <tr>
                                            <th style="width: 25px;">
                                                select
                                            </th>
                                            <th>
                                                Date
                                            </th>
                                            <th>
                                                Type
                                            </th>
                                            <th>
                                                Status
                                            </th>
                                            <th>
                                                Employee
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class='abs xsp-dragbar xsp-dragbar-hframe'>
                                &nbsp;</div>
                        </div>
                    </div>
                    <div class='abs xsp-dragbar xsp-dragbar-vframe'>
                        &nbsp;</div>
                </div>
            </div>
        </div>
        <div class="abs window_bottom">
            User Landing Screen
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
<% Html.Telerik().ScriptRegistrar().Scripts(script => script.Add("/Models/Employee.js"))
.OnDocumentReady(() =>
{%>
Employee.Init();
<%}); %>