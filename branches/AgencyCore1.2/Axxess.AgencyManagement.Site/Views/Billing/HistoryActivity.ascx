﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="submenu">
    <ul class="pureCssMenu">
        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">To be transmitted</a></li>
        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">Transmitted
            and Paid</a></li>
        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">Transmitted
            Only <![if gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
        </li>
        <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);"><span>Actions</span><![if
            gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
            <ul class="pureCssMenum">
                <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">Reactivate
                    Claim</a></li>
                <li class="pureCssMenui"><a class="pureCssMenui" href="javascript:void(0);">Hold Claim</a></li>
            </ul>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
        </li>
    </ul>
</div>
<%Html.Telerik().Grid<CliamViewData>()
                                                .Name("BillingHistoryActivityGrid")
                                                .Columns(columns =>
                                                {
                                                    columns.Bound(c => c.Type)
                                              .ClientTemplate("<input type='checkbox' name='checkedRecords' value='' />")
                                              .Title("Select")
                                              .Width(50)
                                              .HtmlAttributes(new { style = "text-align:center" });
                                                    columns.Bound(p => p.Type);
                                                    columns.Bound(p => p.EpisodeRange).Title("Episode");
                                                    columns.Bound(p => p.StatusName).Title("Status");
                                                    columns.Bound(p => p.Id)
                             .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"\">Edit</a>")
                             .Title("Action").Width(180);
                                                    columns.Bound(p => p.EpisodeId).HeaderHtmlAttributes(new { style = "display:none" }).HtmlAttributes(new { style = "display:none" }).Width(0);
                                                })
                                                .DataBinding(dataBinding => dataBinding.Ajax().Select("HistoryActivity", "Billing", new { patientId = Model.Id }))
                                                .Sortable()
                                                .Selectable()
                                                .Scrollable()
                                                .Footer(false).HtmlAttributes(new { Style = "min-width:100px;" })
                                                .Render();                                                
                                                                                                
%>
