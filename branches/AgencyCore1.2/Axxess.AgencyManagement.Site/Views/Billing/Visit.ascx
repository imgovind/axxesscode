﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<% using (Html.BeginForm("VisitVerify", "Billing", FormMethod.Post, new { @id = "billingVisit" }))%>
<%  { %>
<fieldset>
    <%=Html.Hidden("Id",Model.Id) %>
    <%=Html.Hidden("episodeId",Model.EpisodeId) %>
    <%=Html.Hidden("patientId",Model.PatientId) %>
    <%var notVerifiedVisits = Model.NotVerifiedVisits.ToObject<List<ScheduleEvent>>(); %>
    <%var completedVisit = notVerifiedVisits.Where(v => v.Status == "9" || v.Status == "10" || v.Status == "2" || v.Status == "3");  %>
    <table class="claim" style="text-align: left;">
        <thead>
            <tr>
                <th colspan="6">
                    Episode :
                    <%=Model.EpisodeStartDate.ToShortDateString()%>
                    -
                    <%=Model.EpisodeEndDate.ToShortDateString()%>
                </th>
            </tr>
            <tr>
                <th colspan="6">
                    Billable Visits
                </th>
            </tr>
            <tr>
                <th>
                    Visit Type
                </th>
                <th>
                    Date
                </th>
                <th>
                    HCPCS
                </th>
                <th>
                    Status
                </th>
                <th>
                    Units
                </th>
                <th>
                    Charge
                </th>
            </tr>
        </thead>
        <tbody>
            <% if (completedVisit != null)
               { %>
            <% var ptVisists = completedVisit.Where(e => e.Discipline == "PT").ToList(); %>
            <%if (ptVisists != null && ptVisists.Count > 0)
              { %>
            <tr>
                <td colspan="9">
                    <b>Physical Therapy (0421)</b>
                </td>
            </tr>
            <% 
                int pt = 1;
                foreach (var visit in ptVisists)
                {%>
            <tr>
                <td>
                    <%=pt%>
                    .<input name="Visit" type="checkbox" value='<%=visit.EventId%>' checked="checked" />
                    <%=visit.DisciplineTaskName%>
                </td>
                <td>
                    <%=visit.EventDate%>
                </td>
                <td>
                    G0151
                </td>
                <td>
                    <%=visit.StatusName%>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <% pt++;
                } %>
            <%} %>
            <% var snVisists = completedVisit.Where(e => e.Discipline == "Nursing").ToList(); %>
            <%if (snVisists != null && snVisists.Count > 0)
              { %>
            <tr>
                <td colspan="9">
                    <b>Skilled Nursing (0551)</b>
                </td>
            </tr>
            <%  int sn = 1;
                foreach (var visit in snVisists)
                {%>
            <tr>
                <td>
                    <%=sn%>
                    .<input name="Visit" type="checkbox" value='<%=visit.EventId%>' checked="checked" />
                    <%=visit.DisciplineTaskName%>
                </td>
                <td>
                    <%=visit.EventDate%>
                </td>
                <td>
                    G0154
                </td>
                <td>
                    <%=visit.StatusName%>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <% sn++;
                } %>
            <%} %>
            <% var stVisists = completedVisit.Where(e => e.Discipline == "ST").ToList(); %>
            <%if (stVisists != null && stVisists.Count > 0)
              { %>
            <tr>
                <td colspan="9">
                    <b>Speech Therapy (0440)</b>
                </td>
            </tr>
            <%  int st = 1;
                foreach (var visit in stVisists)
                {%>
            <tr>
                <td>
                    <%=st%>
                    .<input name="Visit" type="checkbox" value='<%=visit.EventId%>' checked="checked" />
                    <%=visit.DisciplineTaskName%>
                </td>
                <td>
                    <%=visit.EventDate%>
                </td>
                <td>
                    G0153
                </td>
                <td>
                    <%=visit.StatusName%>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <% st++;
                } %>
            <%} %>
            <% var otVisists = completedVisit.Where(e => e.Discipline == "OT").ToList(); %>
            <%if (otVisists != null && otVisists.Count > 0)
              { %>
            <tr>
                <td colspan="9">
                    <b>Occupational therapy (0431)</b>
                </td>
            </tr>
            <%  int ot = 1;
                foreach (var visit in otVisists)
                {%>
            <tr>
                <td>
                    <%=ot%>
                    .<input name="Visit" type="checkbox" value='<%=visit.EventId%>' checked="checked" />
                    <%=visit.DisciplineTaskName%>
                </td>
                <td>
                    <%=visit.EventDate%>
                </td>
                <td>
                    G0152
                </td>
                <td>
                    <%=visit.StatusName%>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <% ot++;
                } %>
            <%} %>
            <% var hhaVisists = completedVisit.Where(e => e.Discipline == "OT").ToList(); %>
            <%if (hhaVisists != null && hhaVisists.Count > 0)
              { %>
            <tr>
                <td colspan="9">
                    <b>Social Worker(0561)</b>
                </td>
            </tr>
            <%  int hha = 1;
                foreach (var visit in hhaVisists)
                {%>
            <tr>
                <td>
                    <%=hha%>
                    .<input name="Visit" type="checkbox" value='<%=visit.EventId%>' checked="checked" />
                    <%=visit.DisciplineTaskName%>
                </td>
                <td>
                    <%=visit.EventDate%>
                </td>
                <td>
                    G0155
                </td>
                <td>
                    <%=visit.StatusName%>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <% hha++;
                } %>
            <%} %>
            <%} %>
        </tbody>
    </table>
    <br />
    <%var nonCompletedVisit = notVerifiedVisits.Where(v => v.Status == "1" || v.Status == "4" || v.Status == "7" || v.Status == "8" || v.Status == "9" || v.Status == "12");  %>
    <table class="claim" style="text-align: left;">
        <thead>
            <tr>
                <th colspan="6">
                    Non Billable Visits
                </th>
            </tr>
            <tr>
                <th>
                    Visit Type
                </th>
                <th>
                    Date
                </th>
                <th>
                    HCPCS
                </th>
                <th>
                    Status
                </th>
                <th>
                    Units
                </th>
                <th>
                    Charge
                </th>
            </tr>
        </thead>
        <tbody>
            <% if (nonCompletedVisit != null)
               { %>
            <% var ptVisists = nonCompletedVisit.Where(e => e.Discipline == "PT").ToList(); %>
            <%if (ptVisists != null && ptVisists.Count > 0)
              { %>
            <tr>
                <td colspan="9">
                    <b>Physical Therapy (0421)</b>
                </td>
            </tr>
            <% 
                int pt = 1;
                foreach (var visit in ptVisists)
                {%>
            <tr>
                <td>
                    <%=pt%>
                    .<input name="Visit" type="checkbox" value='<%=visit.EventId%>' />
                    <%=visit.DisciplineTaskName%>
                </td>
                <td>
                    <%=visit.EventDate%>
                </td>
                <td>
                    G0151
                </td>
                <td>
                    <%=visit.StatusName%>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <% pt++;
                } %>
            <%} %>
            <% var snVisists = nonCompletedVisit.Where(e => e.Discipline == "Nursing").ToList(); %>
            <%if (snVisists != null && snVisists.Count > 0)
              { %>
            <tr>
                <td colspan="9">
                    <b>Skilled Nursing (0551)</b>
                </td>
            </tr>
            <%  int sn = 1;
                foreach (var visit in snVisists)
                {%>
            <tr>
                <td>
                    <%=sn%>
                    .<input name="Visit" type="checkbox" value='<%=visit.EventId%>' />
                    <%=visit.DisciplineTaskName%>
                </td>
                <td>
                    <%=visit.EventDate%>
                </td>
                <td>
                    G0154
                </td>
                <td>
                    <%=visit.StatusName%>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <% sn++;
                } %>
            <%} %>
            <% var stVisists = nonCompletedVisit.Where(e => e.Discipline == "ST").ToList(); %>
            <%if (stVisists != null && stVisists.Count > 0)
              { %>
            <tr>
                <td colspan="9">
                    <b>Speech Therapy (0440)</b>
                </td>
            </tr>
            <%  int st = 1;
                foreach (var visit in stVisists)
                {%>
            <tr>
                <td>
                    <%=st%>
                    .<input name="Visit" type="checkbox" value='<%=visit.EventId%>' />
                    <%=visit.DisciplineTaskName%>
                </td>
                <td>
                    <%=visit.EventDate%>
                </td>
                <td>
                    G0153
                </td>
                <td>
                    <%=visit.StatusName%>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <% st++;
                } %>
            <%} %>
            <% var otVisists = nonCompletedVisit.Where(e => e.Discipline == "OT").ToList(); %>
            <%if (otVisists != null && otVisists.Count > 0)
              { %>
            <tr>
                <td colspan="9">
                    <b>Occupational therapy (0431)</b>
                </td>
            </tr>
            <%  int ot = 1;
                foreach (var visit in otVisists)
                {%>
            <tr>
                <td>
                    <%=ot%>
                    .<input name="Visit" type="checkbox" value='<%=visit.EventId%>' />
                    <%=visit.DisciplineTaskName%>
                </td>
                <td>
                    <%=visit.EventDate%>
                </td>
                <td>
                    G0152
                </td>
                <td>
                    <%=visit.StatusName%>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <% ot++;
                } %>
            <%} %>
            <% var hhaVisists = nonCompletedVisit.Where(e => e.Discipline == "OT").ToList(); %>
            <%if (hhaVisists != null && hhaVisists.Count > 0)
              { %>
            <tr>
                <td colspan="9">
                    <b>Social Worker(0561)</b>
                </td>
            </tr>
            <%  int hha = 1;
                foreach (var visit in hhaVisists)
                {%>
            <tr>
                <td>
                    <%=hha%>
                    .<input name="Visit" type="checkbox" value='<%=visit.EventId%>' />
                    <%=visit.DisciplineTaskName%>
                </td>
                <td>
                    <%=visit.EventDate%>
                </td>
                <td>
                    G0155
                </td>
                <td>
                    <%=visit.StatusName%>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <% hha++;
                } %>
            <%} %>
            <%} %>
        </tbody>
    </table>
    <div class="row">
        <div style="text-align: center; vertical-align: middle; width: 100%;" class="buttons">
            <ul style="margin: 0px; width: 100%; text-align: center;">
                <li><span class="button orange_btn"><span><span>Back</span></span><input type="button"
                    name="" id=" " onclick="Billing.NavigateBack(0);" /></span></li>
                <li><span class="button   send_form_btn"><span><span>Verify and Next</span></span><input
                    type="submit" name="" id=" " onclick="Billing.Navigate(2,'#billingVisit');" /></span></li>
            </ul>
        </div>
    </div>
</fieldset>
<%} %>