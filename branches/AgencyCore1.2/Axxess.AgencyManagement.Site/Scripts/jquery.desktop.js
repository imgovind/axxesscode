//
// Namespace - Module Pattern.
//
var JQD = (function($) {
    return {
        //
        // Clear active states, hide menus.
        //
        clear_active: function() {

            $('a.active, tr.active').removeClass('active');
            $('ul.menu').hide();
        },

        //
        // Zero out window z-index.
        //
        window_flat: function() {
            $('div.window').removeClass('window_stack');
        },

        //
        // Resize modal window.
        //
        window_resize: function(el) {
            // Nearest parent window.
            var win = $(el).closest('div.window');

            Patient.CalculateGridHeight();
            Schedule.CalculateGridHeight();

            // Is it maximized already?
            if (win.hasClass('window_full')) {
                // Restore window position.
                win.removeClass('window_full').css({
                    'top': win.attr('data-t'),
                    'left': win.attr('data-l'),
                    'right': win.attr('data-r'),
                    'bottom': win.attr('data-b'),
                    'width': win.attr('data-w'),
                    'height': win.attr('data-h')
                });
            }
            else {
                win.attr({
                    // Save window position.
                    'data-t': win.css('top'),
                    'data-l': win.css('left'),
                    'data-r': win.css('right'),
                    'data-b': win.css('bottom'),
                    'data-w': win.css('width'),
                    'data-h': win.css('height')
                }).addClass('window_full').css({
                    // Maximize dimensions.
                    'top': '0',
                    'left': '0',
                    'right': '0',
                    'bottom': '0',
                    'width': '100%',
                    'height': '100%'
                });
            }

            // Bring window to front.
            JQD.window_flat();
            win.addClass('window_stack');
        },

        //
        // Initialize the desktop.
        //
        init_desktop: function() {
            //            // Cancel mousedown, right-click.
            //            $(document).mousedown(function(ev) {
            //                if (!$(ev.target).closest('a').length) {
            //                    JQD.clear_active();
            //                    return false;
            //                }
            //            }).bind('contextmenu', function() {
            //                return false;
            //          });        

            // Make windows movable.
            $('div.window').mousedown(function() {
                // Bring window to front.
                JQD.window_flat();
                $(this).addClass('window_stack');
            }).draggable({
                // Confine to desktop.
                // Movable via top bar only.
                containment: 'parent',
                handle: 'div.window_top'
            }).resizable({
                containment: 'parent',
                minWidth: 800,
                minHeight: 400

                // Double-click top bar to resize, ala Windows OS.
            }).find('div.window_top').prepend(unescape("%3Cimg src=%22/images/icons/axxess_icon.png%22 alt=%22Icon%22 /%3E")).dblclick(function() {
                JQD.window_resize(this);

                // Double click top bar icon to close, ala Windows OS.
            }).find('img').dblclick(function() {
                // Traverse to the close button, and hide its taskbar button.
                $($(this).closest('div.window_top').find('a.window_close').attr('href')).hide('fast');

                // Close the window itself.
                $(this).closest('div.window').hide();

                // Stop propagation to window's top bar.
                return false;
            });
            $('div.window_top').find('.float_left').removeClass('float_left').addClass('abs');
            $('body').prepend(unescape("%3Cdiv id=%22tab%22%3E%3C/div%3E"));
            var d = new Date();
            $('#bar_bottom').append(unescape("%3Cspan class=%22float_right%22%3E&copy; 2008 &ndash; " + d.getFullYear() + " Axxess Technology Solutions, Inc., All Rights Reserved%3C/span%3E"));
            // Get action buttons for each window.
            $('a.window_min, a.window_resize, a.window_close').mousedown(function() {
                JQD.clear_active();
                // Stop propagation to window's top bar.
                return false;
            });
            // Minimize the window.
            $('a.window_min').click(function() {
                $(this).closest('div.window').hide();
            });
            // Maximize or restore the window.
            $('a.window_resize').click(function() {
                JQD.window_resize(this);

            });
            // Close the window.
            $('a.window_close').click(function() {
                $(this).closest('div.window').hide();
                $($(this).attr('href')).hide('fast');
            });

            $('table.data').find('tr').live('click', function() {
                // Highlight row, ala Mac OS X.
                $(this).closest('tr').addClass('active');
            });
        },
        open_window: function(openWindow) {
            // Get the link's target.
            var x = $(openWindow);
            JQD.window_flat();
            x.show().addClass('window_stack');
            $(x.find(".window_content").first()).scrollTop(0);
        },
        alternate: function(table) {
            // Take object table and get all it's tbodies.
            var tableBodies = table.getElementsByTagName("tbody");
            // Loop through these tbodies
            for (var i = 0; i < tableBodies.length; i++) {
                // Take the tbody, and get all it's rows
                var tableRows = tableBodies[i].getElementsByTagName("tr");
                // Loop through these rows
                // Start at 1 because we want to leave the heading row untouched
                for (var j = 0; j < tableRows.length; j++) {
                    // Check if j is even, and apply classes for both possible results
                    if ((j % 2) == 0) {
                        if (!(tableRows[j].className.indexOf('odd') == -1)) {
                            tableRows[j].className = tableRows[j].className.replace('odd', 'even');
                        } else {
                            if (tableRows[j].className.indexOf('even') == -1) {
                                tableRows[j].className += " even";
                            }
                        }
                    } else {
                        if (!(tableRows[j].className.indexOf('even') == -1)) {
                            tableRows[j].className = tableRows[j].className.replace('even', 'odd');
                        } else {
                            if (tableRows[j].className.indexOf('odd') == -1) {
                                tableRows[j].className += " odd";
                            }
                        }
                    }
                }
            }
        },
        loadReportPara: function(para) {

            if ($('#reportContent').children(":first").attr('id') == para) {
                return true;
            }
            else {
                $("#reportPara").append($('#reportContent').children(":first"));
                $('#reportContent').empty();

                $('#reportContent').append($('#' + para));
            }
        }
    };

    // Pass in jQuery.
})(jQuery);
