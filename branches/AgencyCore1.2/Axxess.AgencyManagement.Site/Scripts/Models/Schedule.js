﻿var Schedule = {
    _scheduleWindow: "",
    _patientId: "",
    _EpisodeId: "",
    _patientName: "",
    _EpisodeStartDate: "",
    _EpisodeEndDate: "",
    _tableId: "nursingScheduleTable",
    _Discipline: "Nursing",
    _DisciplineIndex: 0,
    GetTableId: function() {
        return Schedule._tableId;
    },
    SetTableId: function(tableId) {
        Schedule._tableId = tableId;
    },
    GetId: function() {
        return Schedule._patientId;
    },
    SetId: function(patientId) {
        Schedule._patientId = patientId;
    },
    SetName: function(patientName) {
        Schedule._patientName = patientName;
    }
    ,
    SetPatientRowIndex: function(patientRowIndex) {
        Schedule._patientRowIndex = patientRowIndex;
    },
    GetEpisodeId: function() {
        return Schedule._EpisodeId;
    },
    SetEpisodeId: function(episodeId) {
        Schedule._EpisodeId = episodeId;
    },
    SetStartDate: function(episodeStartDate) {
        Schedule._EpisodeStartDate = episodeStartDate;
    },
    SetEndDate: function(episodeEndDate) {
        Schedule._EpisodeEndDate = episodeEndDate;
    },
    SetDiscipline: function(discipline) {
        Schedule._Discipline = discipline;
    },
    SetDisciplineIndex: function(disciplineIndex) {
        Schedule._DisciplineIndex = disciplineIndex;
    }
    ,
    CalculateGridHeight: function() {
        setTimeout(function() {
            $('#ScheduleSplitter1').children().height($('#ScheduleSplitter1').height() + 4);
            var gridContentHeight = $('#ScheduleLeftSide').height() - $('#ScheduleFilterContainer').height() - 25 - 20 + 7;

            if ($.browser.msie) {
                $('#SchedulePatientSelectionGridContainer').find(".t-grid-content").height(gridContentHeight);
            }
            else {
                $('#SchedulePatientSelectionGridContainer').find(".t-grid-content").height(gridContentHeight);
            }

        }, 500);
    },
    CalculateActivityGrid: function() {
        setTimeout(function() {
            var activityGridRow = $('#scheduleBottomPanel').height();
            if ($.browser.msie) {
                $('#ScheduleActivityGrid').find(".t-grid-content").height(activityGridRow - 335);
            }
            else {
                $('#ScheduleActivityGrid').find(".t-grid-content").height(activityGridRow - 333);
            }
        }, 500);
    },
    Init: function() {
        // JQD.open_window('#schedule_window');

        jQuery.event.add(window, "load", function() {
            Schedule.CalculateGridHeight();
            Schedule.CalculateActivityGrid();
        });
        jQuery.event.add(window, "resize", function() {
            Schedule.CalculateGridHeight();
            Schedule.CalculateActivityGrid();
        });

        Lookup.loadDisciplines("#nursingScheduleTable", "Nursing");
        var input = "Discipline=Nursing";
        Schedule.addTableRow("#nursingScheduleTable", '', input);
        Lookup.loadDisciplines("#PTScheduleTable", "PT");
        var inputPt = "Discipline=PT";
        Schedule.addTableRow("#PTScheduleTable", '', inputPt);
        Lookup.loadDisciplines("#OTScheduleTable", "OT");
        var inputOt = "Discipline=OT";
        Schedule.addTableRow("#OTScheduleTable", '', inputOt);
        Lookup.loadDisciplines("#STScheduleTable", "ST");
        var inputSt = "Discipline=ST";
        Schedule.addTableRow("#STScheduleTable", '', inputSt);
        Lookup.loadDisciplines("#HHAScheduleTable", "HHA");
        var inputHHA = "Discipline=HHA";
        Schedule.addTableRow("#HHAScheduleTable", '', inputHHA);
        Lookup.loadDisciplines("#MSWScheduleTable", "MSW");
        var inputMSW = "Discipline=MSW";
        Schedule.addTableRow("#MSWScheduleTable", '', inputMSW);
        Lookup.loadDisciplines("#OrdersScheduleTable", "Orders");
        var inputOrders = "Discipline=Orders";
        Schedule.addTableRow("#OrdersScheduleTable", '', inputOrders);
        var inputClaim = "Discipline=Claim";
        Schedule.addTableRow("#ClaimScheduleTable", '', inputClaim);
        Lookup.loadDisciplines("#ClaimScheduleTable", "Claim");

        Lookup.loadMultipleDisciplines("#multipleScheduleTable");

        $("select.scheduleStatusDropDown").change(function() {
            var patientGrid = $('#SchedulePatientSelectionGrid').data('tGrid');

            patientGrid.rebind({ statusId: $("select.scheduleStatusDropDown").val(), paymentSourceId: $("select.schedulePaymentDropDown").val(), name: $("#txtSearch_Schedule_Selection").val() });
        });

        $("select.schedulePaymentDropDown").change(function() {
            var patientGrid = $('#SchedulePatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.scheduleStatusDropDown").val(), paymentSourceId: $("select.schedulePaymentDropDown").val(), name: $("#txtSearch_Schedule_Selection").val() });
        });

        $('#txtSearch_Schedule_Selection').keypress(function() {
            var patientGrid = $('#SchedulePatientSelectionGrid').data('tGrid');
            patientGrid.rebind({ statusId: $("select.scheduleStatusDropDown").val(), paymentSourceId: $("select.schedulePaymentDropDown").val(), name: $("#txtSearch_Schedule_Selection").val() });
        });
    },
    Add: function(date) {
        if ($(this).closest('td').hasClass('Inactive')) {
            return true;
        }
        else {
            if (Schedule.GetTableId() == "multipleScheduleTable") {
                return true;
            }
            else {

                Schedule.CurrentTableAdd(date);
            }
        }
    }
    ,
    CurrentTableAdd: function(currentday) {

        var name = "#" + Schedule.GetTableId();
        var $table = $(name);
        $(".currentDate", $table).attr("readOnly", true);
        var length = $('tbody  tr', $table).length
        $('tbody  tr', $table).find('.action').each(function() { $(this).removeAttr('disabled'); });
        $('tbody  tr:last', $table).find('.currentDate').val($.format.date(currentday, 'MM/dd/yyyy'));
        var input = "Discipline=" + Schedule._Discipline;
        Schedule.addTableRow(name, $.format.date(currentday, 'MM/dd/yyyy'), input);
    }
    ,

    RenderCalendar: function(data) {
        var events1 = eval('(' + data + ')');
        var options = {
            containerId: "#createCalendar1",
            calendarStartDate: new Date(Schedule._EpisodeStartDate),
            calendarEndDate: new Date((new Date(new Date(Schedule._EpisodeStartDate).getFullYear(), new Date(Schedule._EpisodeStartDate).getMonth() + 1, 1)) - 1),
            navLinks: {
                enableToday: true,
                enableNextYear: false,
                enablePrevYear: false,
                p: '',
                n: '',
                t: '',
                showMore: ''
            },
            onEventLinkClick: function(event) {
                alert("event link click");
                return true;
            },
            onEventBlockClick: function(event) {
                alert("block clicked");
                return true;
            },
            onEventBlockOver: function(event) {
                // alert(event.Title + " - " + event.Description);
                return true;
            },
            onEventBlockOut: function(event) {
                return true;
            },
            onDayLinkClick: function(event) {
                if ($(this).closest('td').hasClass('Inactive')) {
                    return true;
                }
                else {
                    var currentday = event.data.Date;
                    if (Schedule.GetTableId() == "multipleScheduleTable") {
                        return true;
                    }
                    else {
                        var name = "#" + Schedule.GetTableId();
                        Schedule.CurrentTableAdd(currentday);
                    }
                }
                return true;
            },
            onDayCellClick: function(date) {
                // alert(date.toLocaleDateString());
                return true;
            }
                       ,
            onDayCellMouseOver: function(event) {
                // alert(event.data.Date);
                return true;
            }

        };


        $.jMonthCalendar.Initialize(options, events1);
        var cal1 = new Date(options.calendarStartDate.setMonth(options.calendarStartDate.getMonth() + 1)).setDate(1);
        var options2 = {
            containerId: "#createCalendar2",
            calendarStartDate: new Date(cal1),
            calendarEndDate: new Date((new Date(new Date(cal1).getFullYear(), new Date(cal1).getMonth() + 1, 1)) - 1),
            navLinks: {
                enableToday: true,
                enableNextYear: false,
                enablePrevYear: false,
                p: '',
                n: '',
                t: '',
                showMore: ''
            },
            onEventLinkClick: function(event) {
                alert("event link click");
                return true;
            },
            onEventBlockClick: function(event) {
                alert("block clicked");
                return true;
            },
            onEventBlockOver: function(event) {
                // alert(event.Title + " - " + event.Description);
                return true;
            },
            onEventBlockOut: function(event) {
                return true;
            },
            onDayLinkClick: function(event) {

                return true;
            },
            onDayCellClick: function(date) {
                // alert(date.toLocaleDateString());
                return true;
            }
                       ,
            onDayCellMouseOver: function(event) {
                // alert(event.data.Date);
                return true;
            }
        };
        $.jMonthCalendar.Initialize(options2, events1);
        var cal2 = new Date(options2.calendarStartDate.setMonth(options2.calendarStartDate.getMonth() + 1)).setDate(1);
        var options3 = {
            containerId: "#createCalendar3",
            calendarStartDate: new Date(cal2),
            calendarEndDate: new Date(Schedule._EpisodeEndDate),
            navLinks: {
                enableToday: true,
                enableNextYear: false,
                enablePrevYear: false,
                p: '',
                n: '',
                t: '',
                showMore: ''
            },
            onEventLinkClick: function(event) {
                alert("event link click");
                return true;
            },
            onEventBlockClick: function(event) {
                alert("block clicked");
                return true;
            },
            onEventBlockOver: function(event) {
                //alert(event.Title + " - " + event.Description);
                return true;
            },
            onEventBlockOut: function(event) {
                return true;
            },
            onDayLinkClick: function(event) {
                if ($(this).closest('td').hasClass('Inactive')) {
                    return true;
                }
                else {
                    var currentday = event.data.Date;
                    if (Schedule.GetTableId() == "multipleScheduleTable") {
                        return true;
                    }
                    else {
                        var name = "#" + Schedule.GetTableId();
                        Schedule.CurrentTableAdd(currentday);
                    }

                }
                return true;
            },
            onDayCellClick: function(event) {
                // alert(date.toLocaleDateString());
                return true;
            }
                       ,
            onDayCellMouseOver: function(event) {
                // alert(event.data.Date);
                return true;
            }
        };
        $.jMonthCalendar.Initialize(options3, events1);

    }
    ,
    RebindActivity: function() {

        var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
        scheduleActivityGrid.rebind({ episodeId: Schedule._EpisodeId, patientId: Schedule._patientId, discipline: Schedule._Discipline });
    }
    ,
    LoadPatientInfo: function() {

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Schedule/Get",
            data: "id=" + Schedule._patientId + "&discipline=" + Schedule._Discipline,
            beforeSend: function() {

            },
            success: function(result) {
                var data = eval(result);
                $("#schedule_PatientId").text((data.PatientIdNumber !== null ? data.PatientIdNumber : ""));
                $("#schedulePatientInfo").text((data.FirstName !== null && data.FirstName !== undefined ? data.FirstName : "") + " " + (data.LastName != null && data.LastName != undefined ? data.LastName : ""));
                $("#schedulePatientInfo").val(Schedule._patientId);
                if (data.Episode != null) {
                    Schedule.SetEpisodeId(data.Episode.Id);
                    if (data.Episode.StartDateFormatted !== null) {
                        Schedule.SetStartDate(data.Episode.StartDateFormatted);
                    }
                    else {
                        Schedule.SetStartDate(new Date());
                    }
                    if (data.Episode.EndDateFormatted != null) {
                        Schedule.SetEndDate(data.Episode.EndDateFormatted);
                    }
                    else {
                        Schedule.SetEndDate(new Date());
                    }
                    $("#EpisodStartDate").text((data.Episode.StartDateFormatted !== null ? data.Episode.StartDateFormatted : "________"));
                    $("#EpisodeEndDate").text((data.Episode.EndDateFormatted !== null && data.Episode.EndDateFormatted !== undefined ? data.Episode.EndDateFormatted : "________"));
                    if (data.Episode.HasNext) {
                        if (data.Episode.NextEpisode != null) {
                            $("#nextEpisode").val(data.Episode.NextEpisode.Id);
                            $("#nextEpisode").show();
                        }
                        else {
                            $("#nextEpisode").hide();
                        }
                    }
                    else {
                        $("#nextEpisode").hide();
                    }
                    if (data.Episode.HasPrevious) {
                        if (data.Episode.PreviousEpisode != null) {
                            $("#previousEpisode").val(data.Episode.PreviousEpisode.Id);
                            $("#previousEpisode").show();
                        }
                        else {
                            $("#previousEpisode").hide();
                        }
                    }
                    else {
                        $("#previousEpisode").hide();
                    }
                    Schedule.RenderCalendar(data.Episode.ScheduleEvent);
                }
                else {
                    $("#EpisodStartDate").text("________");
                    $("#EpisodeEndDate").text("________");
                    Schedule.SetStartDate(new Date());
                    Schedule.SetEndDate(new Date());
                }

                Schedule.RebindActivity();

            }
            ,
            error: function() {

                alert("error");
            }
        });
    },
    OnPatientRowSelected: function(e) {
        var PatientID = e.row.cells[3].innerHTML;
        Schedule.SetId(PatientID);
        Schedule.loadCalendar(PatientID, Schedule._Discipline);
        Schedule.loadFirstActivity(PatientID);
    },
    NoPatientBind: function(id) {
        Schedule.loadCalendar(id, Schedule._Discipline);
        Schedule.loadFirstActivity(id);
    }
    ,
    addTableRow: function(jQtable, currentday, input) {
        var $table = $(jQtable);
        Lookup.loadUsersPerControl();
        Lookup.loadDisciplinePerControl(input);
        var tds = '<tr>';
        tds += '<td class=""> <select  value=""  class="DisciplineTask"> <option value="0">Select Discipline</option>' + Lookup._Discipline[input] + '</select> </td>';
        tds += '<td><select  value="" onfocus="" class="Users">' + Lookup._Users + '</select> </td>';
        tds += '<td><input onclick="javaScript:void(0);"  value="" type="text" class="currentDate"/> </td>';
        tds += '<td><input class="action" type="button" onclick="Oasis.DeleteRow($(this));" value="Delete" /></td>';
        tds += '</tr>';

        if ($('tbody', $table).length > 0) {
            $('tbody', $table).append(tds);
        } else {
            $($table).append(tds);
        }
        $(".currentDate", $table).attr("readOnly", true);
        $('tbody tr:last', $table).find('.action').attr('disabled', 'disabled');

    },
    ScheduleInputFix: function(control, Type, tableName) {
        var tableTr = $('tbody tr', $(tableName));
        var last = $('tbody tr:last', $(tableName));
        var len = tableTr.length - 1;
        var i = 1;
        var jsonData = '[';
        $(tableTr).each(function() {
            if (len + 1 > i) {
                jsonData += '{"DisciplineTask":"' + $(this).find('.DisciplineTask').val() + '","UserId":"' + $(this).find('.Users').val() + '","EventDate":"' + $(this).find('.currentDate').val() + '","Discipline":"' + $(this).find('.DisciplineTask').find(":selected").attr("data") + '"}';
            }
            if (len > i) {
                jsonData += ',';
            }
            i++
        });
        jsonData += ']';
        control.closest('form').find('input[name= ' + Type + '_Schedule][type=hidden]').val(jsonData.toString());
        control.closest('form').find('input[name=episodeId][type=hidden]').val($("#ScheduleEpisodeID").val());
        control.closest('form').find('input[name=patientId][type=hidden]').val($("#SchedulePatientID").val());
    }
    ,
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var patientId = $("#SchedulePatientID").val();
                    var episodeId = $("#ScheduleEpisodeID").val()
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                    control.closest("table").find('tbody').empty();
                    Schedule.CurrentTableAdd('');
                }
                else {
                    alert(resultObject.errorMessage);
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    HandlerHelperMultiple: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {

                    var tabstrip = $("#ScheduleTabStrip").data("tTabStrip");
                    var item = $("li", tabstrip.element)[Schedule._DisciplineIndex];
                    tabstrip.select(item);
                    var patientId = $("#SchedulePatientID").val();
                    var episodeId = $("#ScheduleEpisodeID").val()
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                }
                else {
                    alert(resultObject.errorMessage);
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }

    ,
    FormSubmit: function(control) {
        var form = control.closest("form");
        form.validate();
        Schedule.HandlerHelper(form, control);
    },
    FormSubmitMultiple: function(control) {
        control.closest('form').find('input[name=episodeId][type=hidden]').val($("#ScheduleEpisodeID").val());
        control.closest('form').find('input[name=patientId][type=hidden]').val($("#SchedulePatientID").val());
        control.closest('form').find('input[name=Discipline][type=hidden]').val($('tbody tr td:first-child .MultipleDisciplineTask', control.closest('table')).find(":selected").attr("data"));
        var form = control.closest("form");
        form.validate();
        Schedule.HandlerHelperMultiple(form, control);

    },
    ReassignHelper: function(form, control, patientId, episodeId) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);

                if (resultObject.isSuccessful) {
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });

                }
                else {
                    alert(resultObject.errorMessage);
                }
            },
            error: function() {
                alert("Assigning Problem");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    SubmitReassign: function(control, patientId, episodeId) {
        
        var form = control.closest("form");
        form.validate();
        Schedule.ReassignHelper(form, control, patientId, episodeId);
    }
    ,
    ClearRows: function(table) {
        $('tbody tr', table).each(function() {
            $(this).remove();
        });
    }
    ,
    Patient: function() {
        $("#patientScheduleView").show();
        $("#employeeScheduleView").hide();
    },
    Employee: function() {
        $("#patientScheduleView").hide();
        $("#employeeScheduleView").show();
    },
    OnSelect: function(e) {

        var content = $(e.contentElement);
        var tableControl = $('table', content);
        if ($(tableControl).attr('id') == "multipleScheduleTable") {
            return true;
        }

        Schedule.SetDisciplineIndex($(e.item).index());
        Schedule.SetTableId($(tableControl).attr('id'));
        Schedule.SetDiscipline($(tableControl).attr('data'));

        if (Schedule._Discipline == "Multiple") {
            return;
        }
        else {
            var patientId = $("#SchedulePatientID").val();
            var episodeId = $("#ScheduleEpisodeID").val()
            Schedule.loadCalendarNavigation(episodeId, patientId);
            var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
            scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });

        }
    },
    RebindCalendar: function() {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Schedule/GetEpisode",
            data: "patientId=" + Schedule._patientId + "&episodeId=" + Schedule._EpisodeId + "&discipline=" + Schedule._Discipline,
            success: function(result) {
                var data = eval(result);
                $("#EpisodStartDate").text((data.StartDateFormatted !== null ? data.StartDateFormatted : "________"));
                $("#EpisodeEndDate").text((data.EndDateFormatted !== null && data.EndDateFormatted !== undefined ? data.EndDateFormatted : "________"));
                if (data.HasNext) {
                    if (data.NextEpisode != null) {
                        $("#nextEpisode").val(data.NextEpisode.Id);
                        $("#nextEpisode").show();
                    }
                    else {
                        $("#nextEpisode").hide();
                    }
                }
                else {
                    $("#nextEpisode").hide();
                }
                if (data.HasPrevious) {
                    if (data.PreviousEpisode != null) {
                        $("#previousEpisode").val(data.PreviousEpisode.Id);
                        $("#previousEpisode").show();
                    }
                    else {
                        $("#previousEpisode").hide();
                    }
                }
                else {
                    $("#previousEpisode").hide();
                }

                Schedule.RenderCalendar(data.ScheduleEvent);
            }
        });
    }
    ,
    Delete: function(cont, eventId, employeeId) {
        if (confirm("Are you sure you want to delete")) {

            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Schedule/Delete",
                data: "patientId=" + $("#SchedulePatientID").val() + "&eventId=" + eventId + "&employeeId=" + employeeId + "&episodeId=" + $("#ScheduleEpisodeID").val(),
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        var patientId = $("#SchedulePatientID").val();
                        var episodeId = $("#ScheduleEpisodeID").val()
                        var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                        Schedule.loadCalendarNavigation(episodeId, patientId);
                    }
                }
            });
        }
    }
    ,
    ReAssign: function(control, episodeId, patientId, id, oldEmployeeId) {
        Lookup.loadUsersPerControl();
        control.hide();
        control.parent().append("<form action=\"/Schedule/ReAssign\" method=\"post\"><input name=\"episodeId\" type=\"hidden\" value=\" " + episodeId + " \" /><input name=\"oldUserId\" type=\"hidden\" value=\"" + oldEmployeeId + "\" /><input name=\"patientId\" type=\"hidden\" value=\"" + patientId + "\" /><input name=\"eventId\" type=\"hidden\" value=\"" + id + "\"/><select name=\"userId\" style=\"width:90%;\" class=\"Users\">" + Lookup._Users + "</select><input type=\"button\" value=\"Save\" onclick=\"Schedule.SubmitReassign($(this),'" + patientId + "','" + episodeId + "');\"/><input type=\"button\" value=\"Cancel\" onclick=\"Schedule.CancelReassign($(this));\" /></form>");

    }
    ,
    CancelReassign: function(control) {
        var a = control.parent().parent().find('a').show();
        control.parent().remove();
        a.html('ReAssign');

    },
    CloseNewEvent: function(control) {
        control.closest("table").find('tbody').empty();
        Schedule.CurrentTableAdd('');

    },
    NavigateEpisode: function(episodeId, patientId) {
        Schedule.SetEpisodeId(episodeId);
        $("#nursingTab").click();
        Schedule.loadCalendarNavigation(episodeId, patientId);
        var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
    },
    loadCalendarNavigation: function(EpisodeId, PatientId) {

        $('#scheduleTop').load('Schedule/CalendarNav', { patientId: PatientId, episodeId: EpisodeId, discipline: Schedule._Discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleTop').html('<p>There was an error making the AJAX request</p>');
            }
        }
);
    }
    ,
    loadMasterCalendar: function(_patientId, _EpisodeId) {
        $('#masterCalendarResult').load('Schedule/MasterCalendar', { patientId: _patientId, episodeId: _EpisodeId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {

                $('#masterCalendarResult').html('<p>There was an error making the request</p>');
            }
            else if (textStatus == "success") {

                JQD.open_window('#masterCalendar');
                $("table.masterCalendar tbody tr td.lastTd .events").each(function() {
                    $(this).css({
                        position: 'relative',
                        left: -100,
                        top: 0
                    });
                }
                );
            }
        }
);
    },
    loadMasterCalendarNavigation: function(EpisodeId, PatientId) {

        $('#masterCalendarResult').load('Schedule/MasterCalendar', { patientId: PatientId, episodeId: EpisodeId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#masterCalendarResult').html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {

                JQD.open_window('#masterCalendar');
            }
        }
);
    }
    ,
    loadCalendar: function(PatientId, Discipline) {
        $('#scheduleTop').load('Schedule/Calendar', { patientId: PatientId, discipline: Discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleTop').html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {

            }
        }
);
    }
    ,
    loadActivity: function(PatientId, EpisodeId) {

        $('#scheduleBottomPanel').load('Schedule/Calendar', { patientId: PatientId, episodeId: EpisodeId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleBottomPanel').html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {

            }
        }
);

    },
    loadFirstActivity: function(patientId) {

        $('#scheduleBottomPanel').load('Schedule/ActivityFirstTime', { patientId: patientId, discipline: Schedule._Discipline }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleBottomPanel').html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {

            }
        }
);

    }
    ,
    EventMouseOver: function(control) {

        var currentControl = $('.events', $(control));

        currentControl.show();
    }
    ,
    EventMouseOut: function(control) {
        $('.events', $(control)).hide();
    }
,
    loadMasterSchedulingCenter: function() {

        $('#scheduleLandingResult').load('Schedule/Center', function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#scheduleLandingResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#schedule_window');
            }
            else if (textStatus == "success") {

                JQD.open_window('#schedule_window');
                Schedule.win_onload();
                jQuery.event.add(window, "resize", Schedule.win_onresize);
                Schedule.Init();
                Lookup.loadUsers();

            }
        }
);
    }
    ,
    win_onresize: function() {
        var cw = xClientWidth();

        var w = $(".window").width() - 2;
        var h = $(".window").height() - 55.5;
        Schedule._scheduleWindow.paint(w, h, 200, 200, w / 4);
    },
    win_onload: function() {
        var cw = xClientWidth();
        var w = $(".window").width() - 2;
        var h = $(".window").height() - 55.5;
        Schedule._scheduleWindow = new xSplitter('ScheduleSplitter1', 0, 0, w, h, true, 6, 200, 200, 20, true, 2, null, 0);
    }

}
