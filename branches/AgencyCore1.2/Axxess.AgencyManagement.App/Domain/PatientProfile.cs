﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;

    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Domain;

    public class PatientProfile
    {
        public Agency Agency { get; set; }
        public Patient Patient { get; set; }
        public PatientEpisode CurrentEpisode { get; set; }
        public Assessment CurrentAssessment { get; set; }
        public AgencyPhysician Physician { get; set; }
    }
}
