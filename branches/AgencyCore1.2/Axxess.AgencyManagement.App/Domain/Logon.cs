﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class Logon
    {
        [Required]
        [DisplayName("E-mail")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [DisplayName("Keep me logged in")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
