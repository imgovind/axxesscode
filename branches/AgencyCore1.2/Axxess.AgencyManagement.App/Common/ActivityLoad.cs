﻿namespace Axxess.AgencyManagement.App.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;


    public static class ActivityLoad
    {
        public static string Url(Guid episodeId, Guid patientId, Guid eventId, string type)
        {
            string url = "";
            string assessmentType = Convert.ToString((new Activities()).GetValueFromDescription(type));
            switch (type)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                    url = string.Format("SOC.loadSoc('{0}','{1}','StartOfCare');", eventId, patientId);
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                    url = string.Format("ROC.loadRoc('{0}','{1}','ResumptionOfCare');", eventId, patientId);
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                    url = string.Format("FollowUp.loadFollowUp('{0}','{1}','FollowUp');", eventId, patientId);
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                    url = string.Format("Recertification.loadRecertification('{0}','{1}','Recertification');", eventId, patientId);
                    break;
                case "TransferInPatientNotDischarged":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                    url = string.Format("TransferNotDischarge.loadTransferNotDischarge('{0}','{1}','TransferInPatientNotDischarged');", eventId, patientId);
                    break;
                case "TransferInPatientDischarged":
                    url = string.Format("TransferForDischarge.loadTransferForDischarge('{0}','{1}','TransferInPatientDischarged');", eventId, patientId);
                    break;

                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathPT":
                case "OASISCDeathOT":
                    url = string.Format("DeathAtHome.loadDeathAtHome('{0}','{1}','DischargeFromAgencyDeath');", eventId, patientId);
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargePT":
                case "OASISCDischargeOT":
                    url = string.Format("Discharge.loadDischarge('{0}','{1}','DischargeFromAgency');", eventId, patientId);
                    break;
                case "PhysicianOrder":
                    url = string.Format("Patient.EditOrder($(this),'{0}','{1}'); JQD.open_window('#editOrderNote');", eventId, patientId);
                    break;
                case "HCFA485":
                    url = string.Format("UserInterface.ShowEditPlanofCare('{0}');", eventId);
                    break;
                case "CommunicatioNote":
                    url = string.Format("Patient.EditCommunicationNote('{0}'); JQD.open_window('#editCommunicationNote');", eventId);
                    break;
                case "DischargeSummary":
                    url = string.Format("Schedule.loadDischargeSummary('{0}','{1}','{2}');", episodeId, patientId, eventId);
                    break;
                case "SkilledNurseVisit":
                    url = string.Format("Oasis.EditDischarge('{0}','DischargeFromAgency'); JQD.open_window('#editDischarge');", eventId);
                    break;
                case "SixtyDaySummary":
                    url = string.Format("Schedule.loadSixtyDaySummary('{0}','{1}','{2}');", episodeId, patientId, eventId);
                    break;
                case "TransferSummary":
                    url = string.Format("Schedule.loadTransferSummary('{0}','{1}','{2}');", episodeId, patientId, eventId);
                    break;
                case "LVNSupervisoryVisit":
                    url = string.Format("Schedule.loadLVNSVisit('{0}','{1}','{2}');", episodeId, patientId, eventId);
                    break;
                case "HHAideSupervisoryVisit":
                    url = string.Format("Schedule.loadHHASVisit('{0}','{1}','{2}');", episodeId, patientId, eventId);
                    break;
                case "MSW":
                    url = string.Format("Oasis.EditDischarge('{0}','DischargeFromAgency'); JQD.open_window('#editDischarge');", eventId);
                    break;
                case "HHAideVisit":
                    url = string.Format("Schedule.loadHHAVisitNote('{0}','{1}','{2}');", episodeId, patientId, eventId);
                    break;
                case "HHAideCarePlan":
                    url = string.Format("Schedule.loadHHACarePlan('{0}','{1}','{2}');", episodeId, patientId, eventId);
                    break;
            }
            return url;
        }
    }
}
