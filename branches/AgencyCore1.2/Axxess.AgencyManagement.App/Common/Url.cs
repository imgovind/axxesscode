﻿namespace Axxess.AgencyManagement.App.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    
    using Enums;
    using Extensions;

    public static class Url
    {
        public static void Set(ScheduleEvent scheduleEvent, bool addReassignLink, bool usePrintIcon)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0 && !scheduleEvent.EpisodeId.IsEmpty() && !scheduleEvent.PatientId.IsEmpty() && !scheduleEvent.EventId.IsEmpty())
            {
                string printUrl = Print(scheduleEvent, usePrintIcon);
                string detailUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.GetTaskDetails('{0}', '{1}', '{2}'); \">Details</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                string deleteUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.Delete('{0}','{1}','{2}','{3}');\" >Delete</a>", scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.UserId);
                string reassignUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ReAssign($(this), '{0}','{1}','{2}','{3}');\" class=\"reassign\">ReAssign</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.UserId);

                scheduleEvent.SetStatusName();

                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;

                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"OasisValidation.SetEpisodeId('{0}');SOC.loadSoc('{1}','{2}','StartOfCare');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"OasisValidation.SetEpisodeId('{0}');ROC.loadRoc('{1}','{2}','ResumptionOfCare');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"OasisValidation.SetEpisodeId('{0}');FollowUp.loadFollowUp('{1}','{2}','FollowUp');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"OasisValidation.SetEpisodeId('{0}');Recertification.loadRecertification('{1}','{2}','Recertification');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"OasisValidation.SetEpisodeId('{0}');TransferNotDischarge.loadTransferNotDischarge('{1}','{2}','TransferInPatientNotDischarged');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCTransferDischarge:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"OasisValidation.SetEpisodeId('{0}');TransferForDischarge.loadTransferForDischarge('{1}','{2}','TransferInPatientDischarged');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"OasisValidation.SetEpisodeId('{0}');DeathAtHome.loadDeathAtHome('{1}','{2}','DischargeFromAgencyDeath');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"OasisValidation.SetEpisodeId('{0}');Discharge.loadDischarge('{1}','{2}','DischargeFromAgency');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditOrder('{0}','{1}');\">{2}</a>", scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HCFA485:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.CommunicationNote:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Patient.EditCommunicationNote('{0}');\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.DischargeSummary:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadDischargeSummary('{0}','{1}','{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadSnVisit('{0}','{1}','{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SixtyDaySummary:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadSixtyDaySummary('{0}','{1}','{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.TransferSummary:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadTransferSummary('{0}','{1}','{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadLVNSVisit('{0}','{1}','{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadHHASVisit('{0}','{1}','{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HHAideVisit:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadHHAVisitNote('{0}','{1}','{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadHHACarePlan('{0}','{1}','{2}');\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                }

                if (Current.HasRight(Permissions.EditTaskDetails))
                {
                    scheduleEvent.ActionUrl = detailUrl;
                }

                if (Current.HasRight(Permissions.DeleteTasks))
                {
                    if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                    {
                        scheduleEvent.ActionUrl += " | " + deleteUrl;
                    }
                    else
                    {
                        scheduleEvent.ActionUrl = deleteUrl;
                    }
                }

                if (addReassignLink)
                {
                    if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                    {
                        scheduleEvent.ActionUrl += " | " + reassignUrl;
                    }
                    else
                    {
                        scheduleEvent.ActionUrl = reassignUrl;
                    }
                }

                if (Current.HasRight(Permissions.PrintClinicalDocuments))
                {
                    scheduleEvent.PrintUrl = printUrl;
                }

                if (scheduleEvent.IsCompleted())
                {
                    scheduleEvent.ActionUrl = detailUrl;
                    scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                }

                if (scheduleEvent.IsMissedVisit)
                {
                    scheduleEvent.ActionUrl = detailUrl;
                    scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.MissedVisitPopup($(this), '{0}');\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                }
            }
        }

        public static string Print(Guid episodeId, Guid patientId, Guid eventId, DisciplineTasks task, int status, bool usePrintIcon)
        {
            var scheduleEvent = new ScheduleEvent
            {
                EpisodeId = episodeId,
                PatientId = patientId,
                EventId = eventId,
                DisciplineTask = (int)task,
                Status = status.ToString()
            };

            return Print(scheduleEvent, usePrintIcon);
        }

        public static string Print(ScheduleEvent scheduleEvent, bool usePrintIcon)
        {
            string printUrl = string.Empty;
            string linkText = scheduleEvent.DisciplineTaskName;
            if (usePrintIcon)
            {
                linkText = "<img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" />";
            }

            DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;

            switch (task)
            {
                case DisciplineTasks.OASISCStartofCare:
                case DisciplineTasks.OASISCStartofCarePT:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/StartofCare/View/{0}/{1}/{2}',function(){{ SOC.loadSoc('{2}','{1}','StartOfCare');acore.closeprintview(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','StartOfCare','Approve','startofcare'); acore.closeprintview(); }},function (){{ Oasis.OasisStatusAction('{2}','{1}','{0}','StartOfCare','Return','startofcare'); acore.closeprintview(); }})\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.OASISCResumptionofCare:
                case DisciplineTasks.OASISCResumptionofCarePT:
                case DisciplineTasks.OASISCResumptionofCareOT:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/ResumptionOfCare/View/{0}/{1}/{2}')\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.OASISCFollowUp:
                case DisciplineTasks.OASISCFollowupPT:
                case DisciplineTasks.OASISCFollowupOT:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Followup/View/{0}/{1}/{2}')\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.OASISCRecertification:
                case DisciplineTasks.OASISCRecertificationPT:
                case DisciplineTasks.OASISCRecertificationOT:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Recertification/View/{0}/{1}/{2}')\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.OASISCTransfer:
                case DisciplineTasks.OASISCTransferPT:
                case DisciplineTasks.OASISCTransferOT:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/TransferNotDischarge/View/{0}/{1}/{2}')\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.OASISCTransferDischarge:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/TransferDischarge/View/{0}/{1}/{2}')\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.OASISCDeath:
                case DisciplineTasks.OASISCDeathPT:
                case DisciplineTasks.OASISCDeathOT:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/DischargeDeath/View/{0}/{1}/{2}',function(){{ DeathAtHome.loadDeathAtHome('{2}','{1}','DischargeFromAgencyDeath');acore.closeprintview(); }}, function () {{ Oasis.OasisStatusAction('{2}','{1}','{0}','DischargeFromAgencyDeath','Approve','deathathome'); }}, function () {{Oasis.OasisStatusAction('{2}','{1}','{0}','DischargeFromAgencyDeath','Return','deathathome');  }});\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.OASISCDischarge:
                case DisciplineTasks.OASISCDischargePT:
                case DisciplineTasks.OASISCDischargeOT:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Discharge/View/{0}/{1}/{2}')\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.PhysicianOrder:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/Order/View/{0}/{1}/{2}')\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.HCFA485:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/485/View/{0}/{1}/{2}')\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.CommunicationNote:
                    break;
                case DisciplineTasks.DischargeSummary:
                    if (Current.IsCaseManager)
                    {
                        printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/DischargeSummary/View/{0}/{1}/{2}',function(){{ Schedule.loadDischargeSummary('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{ }},function(){{ }})\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    }
                    else if (Current.IsClinician)
                    {
                        printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/DischargeSummary/View/{0}/{1}/{2}',{3})\">{4}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, "function(){ acore.closeprintview(); }", linkText);
                    }
                    else
                    {
                        printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/DischargeSummary/View/{0}/{1}/{2}')\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    }
                    break;
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.PTWithINR:
                case DisciplineTasks.PTWithINRPRNSNV:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNAssessment:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.SNVDCPlanning:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/SNVisit/View/{0}/{1}/{2}',function(){{ Schedule.loadSnVisit('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{Schedule.ProcessNote('Approve','{0}','{1}','{2}'); }},function(){{  Schedule.ProcessNote('Return','{0}','{1}','{2}'); }})\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.SixtyDaySummary:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/SixtyDaySummary/View/{0}/{1}/{2}',function(){{ Schedule.loadSixtyDaySummary('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{Schedule.ProcessNote('Approve','{0}','{1}','{2}'); }},function(){{Schedule.ProcessNote('Return','{0}','{1}','{2}'); }})\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.TransferSummary:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/TransferSummary/View/{0}/{1}/{2}',function(){{ Schedule.loadTransferSummary('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{ Schedule.ProcessNote('Approve','{0}','{1}','{2}'); }},function(){{Schedule.ProcessNote('Return','{0}','{1}','{2}');  }})\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.LVNSupervisoryVisit:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/LVNSupervisoryVisit/View/{0}/{1}/{2}',function(){{ Schedule.loadLVNSVisit('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{Schedule.ProcessNote('Approve','{0}','{1}','{2}');  }},function(){{Schedule.ProcessNote('Return','{0}','{1}','{2}');  }})\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.HHAideSupervisoryVisit:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/HHAideSupervisoryVisit/View/{0}/{1}/{2}',function(){{ Schedule.loadHHASVisit('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{ Schedule.ProcessNote('Approve','{0}','{1}','{2}'); }},function(){{Schedule.ProcessNote('Return','{0}','{1}','{2}');  }})\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.HHAideVisit:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/HHAVisitNote/View/{0}/{1}/{2}',function(){{ Schedule.loadHHAVisitNote('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{ Schedule.ProcessNote('Approve','{0}','{1}','{2}'); }},function(){{Schedule.ProcessNote('Return','{0}','{1}','{2}'); }})\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
                case DisciplineTasks.HHAideCarePlan:
                    printUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"acore.openprintview('/HHACarePlan/View/{0}/{1}/{2}',function(){{ Schedule.loadHHACarePlan('{0}','{1}','{2}'); acore.closeprintview(); }},function(){{ }},function(){{ }})\">{3}</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, linkText);
                    break;
            }
            return printUrl;
        }
    }
}
