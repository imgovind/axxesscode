﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System.Web.Mvc;

    using Web;

    public class PingController : BaseController
    {
        public PingController()
        {

        }

        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index()
        {
            return View();
        }
    }
}
