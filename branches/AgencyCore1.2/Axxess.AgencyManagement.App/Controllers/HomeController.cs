﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System.Xml;
    using System.Web.Mvc;
    using System.ServiceModel.Syndication;

    using Web;
    using Enums;
    using Domain;
    using Services;
    using Security;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    [Compress]
    [Authorize]
    [HandleError]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HomeController : BaseController
    {
        #region Private Members/Constructor

        private readonly IUserService userService;
        private readonly ILoginRepository loginRepository;
        private readonly IErrorRepository errorRepository;

        public HomeController(IMembershipDataProvider membershipDataProvider, IUserService userService)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");

            this.userService = userService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.errorRepository = membershipDataProvider.ErrorRepository;
        }

        #endregion

        #region HomeController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            if (Current.User.ChangePassword)
            {
                return View(Change.Password);
            }
            if (Current.User.ChangeSignature)
            {
                return View(Change.Signature);
            }
            return View(Change.Nothing);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Error()
        {
            return View("~/Views/Shared/Error.aspx");
        }

       
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Logs()
        {
            return View("~/Views/Shared/Logs.aspx", errorRepository.GetSome());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Dashboard()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult FirstVisit([Bind] PasswordChange password, [Bind] SignatureChange signature)
        {
            var viewData = Validate<JsonViewData>(
                           new Validation(() => string.IsNullOrEmpty(password.CurrentPassword), "Temporary passcode is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(password.NewPassword), "New passcode is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(password.NewPasswordConfirm), "Confirm passcode is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(signature.NewSignature), "Signature is required.<br/>"),
                           new Validation(() => string.IsNullOrEmpty(signature.NewSignatureConfirm), "Confirm signature is required.<br/>"),
                           new Validation(() => !password.NewPassword.IsEqual(password.NewPasswordConfirm), "The passwords you have entered do not match."),
                           new Validation(() => !signature.NewSignature.IsEqual(signature.NewSignatureConfirm), "The signatures you have entered do not match.")
                      );
            if (viewData.isSuccessful)
            {
                if (!userService.IsPasswordCorrect(Current.UserId, password.CurrentPassword))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The temporary password provided does not match the one on file.";
                }
                else
                {
                    var login = loginRepository.Find(Current.LoginId);
                    if (login != null)
                    {
                        string passwordsalt = string.Empty;
                        string passwordHash = string.Empty;

                        var saltedHash = new SaltedHash();
                        saltedHash.GetHashAndSalt(password.NewPassword, out passwordHash, out passwordsalt);
                        login.PasswordSalt = passwordsalt;
                        login.PasswordHash = passwordHash;
                        login.IsPasswordChangeRequired = false;
                        login.IsAxxessAdmin = false;

                        string signaturesalt = string.Empty;
                        string signatureHash = string.Empty;

                        saltedHash.GetHashAndSalt(signature.NewSignature, out signatureHash, out signaturesalt);
                        login.SignatureSalt = signaturesalt;
                        login.SignatureHash = signatureHash;
                        
                        if (loginRepository.Update(login))
                        {
                            AxxessPrincipal principal = null;
                            if (Cacher.TryGet<AxxessPrincipal>(Current.User.Name, out principal))
                            {
                                var identity = (AxxessIdentity)principal.Identity;
                                identity.ChangePassword = false;
                                identity.ChangeSignature = false;
                                Cacher.Set<AxxessPrincipal>(Current.User.Name, principal);
                            }
                            viewData.isSuccessful = true;
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your profile could not be saved.";
                    }
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Admin()
        {
            if (!Current.IsAxxessAdmin)
            {
                RedirectToAction("Index");
            }
            return View("Admin");
        }

        #endregion
    }
}
