﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Linq;

    using Web;
    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.App.Services;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;
    using System.Collections.Generic;

    [Compress]
    [Authorize]
    [HandleError]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AgencyController : BaseController
    {
        #region Constructor

        private readonly IAgencyService agencyService;
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPhysicianRepository physicianRepository;

        public AgencyController(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider coreDataProvider,IAgencyService agencyService)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "dataProvider");
            Check.Argument.IsNotNull(agencyService, "agencyService");

            this.agencyService = agencyService;
            this.loginRepository = coreDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region Agency Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Impersonate()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Impersonate(Guid AgencyId)
        {
            Check.Argument.IsNotEmpty(AgencyId, "AgencyId");

            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Could not impersonate Agency" };

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add([Bind] Agency agency)
        {
            Check.Argument.IsNotNull(agency, "agency");

            var viewData = new JsonViewData();

            if (agency.IsValid)
            {
                if (loginRepository.Find(agency.AgencyAdminUsername) == null)
                {
                    if (!agencyService.CreateAgency(agency))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in saving the agency.";
                    }
                    else
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Agency was saved successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The Admin e-mail address provided is already in use.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agency.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id)
        {
            return PartialView(agencyRepository.Get(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Agency agency)
        {
            Check.Argument.IsNotNull(agency, "agency");

            var viewData = new JsonViewData();

            if (agency.IsValid)
            {
                if (!agencyService.EditAgency(agency))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the agency.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Agency was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agency.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewScheduledTasks)]
        public ActionResult Schedule()
        {
            return PartialView();
        }

        [GridAction]
        [Demand(Permissions.ViewScheduledTasks)]
        public ActionResult ScheduleList()
        {
            return View(new GridModel(agencyService.GetSchedule()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewScheduledTasks)]
        public ActionResult RecertsPastDueGrid()
        {
            return PartialView("Oasis/RecertsPastDue");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewScheduledTasks)]
        public ActionResult RecertsUpcomingGrid()
        {
            return PartialView("Oasis/RecertsUpcoming");
        }

        [GridAction]
        [Demand(Permissions.ViewScheduledTasks)]
        public ActionResult RecertsPastDue()
        {
            return Json(new GridModel(agencyService.GetRecertsPastDue()));
        }

        [GridAction]
        [Demand(Permissions.ViewScheduledTasks)]
        public ActionResult RecertsUpcoming()
        {
            return View(new GridModel(agencyService.GetRecertsUpcoming()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessCaseManagement)]
        public ActionResult CaseManagement()
        {
            ViewData["GroupName"] = "EventDate";
            return PartialView("CaseManagement");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessCaseManagement)]
        public ActionResult CaseManagementContent(string groupName)
        {
            ViewData["GroupName"] = groupName;
            return PartialView("CaseManagementContent", agencyService.GetCaseManagerSchedule().OrderBy(m => m.EventDate).ToList());
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessCaseManagement)]
        public ActionResult CaseManagementGrid()
        {
            return View(new GridModel(agencyService.GetCaseManagerSchedule()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public ActionResult OrdersManagement()
        {
            return PartialView("OrdersManagement");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public ActionResult OrdersToBeSent()
        {
            return View(new GridModel(agencyService.GetOrdersToBeSent()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public JsonResult MarkOrdersAsSent(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Orders could not be marked as sent to Physician" };

            if (formCollection.Count > 0)
            {
                if (agencyService.MarkOrdersAsSent(formCollection))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Orders have been marked as sent to Physician";
                }
            }
            else
            {
                viewData.errorMessage = "No Orders were selected";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public ActionResult MarkOrderAsReturned(Guid id, OrderType type, DateTime receivedDate)
        {
            agencyService.MarkOrderAsReturned(id, type, receivedDate);

            return View(new GridModel(agencyService.GetOrdersPendingSignature()));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public ActionResult OrdersPendingSignature()
        {
            return View(new GridModel(agencyService.GetOrdersPendingSignature()));
        }

        [GridAction]
        [Demand(Permissions.ManageUsers)]
        public ActionResult Users()
        {
            return View(new GridModel(userRepository.GetAgencyUsers(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Blankforms()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult Grid()
        {
            return View(new GridModel(agencyRepository.All()));
        }

        #endregion

        #region Contact Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewLists)]
        public ActionResult Contacts()
        {
            return PartialView("Contact/List");
        }

        [GridAction]
        [Demand(Permissions.ViewLists)]
        public ActionResult ContactList()
        {
            return View(new GridModel(agencyRepository.GetContacts(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManageContact)]
        public ActionResult NewContact()
        {
            return PartialView("Contact/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageContact)]
        public JsonResult AddContact([Bind] AgencyContact contact)
        {
            Check.Argument.IsNotNull(contact, "contact");

            var viewData = new JsonViewData();

            if (contact.IsValid)
            {
                if (!agencyService.CreateContact(contact))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the contact.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Contact was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = contact.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageContact)]
        public ActionResult EditContact(Guid Id)
        {
            return PartialView("Contact/Edit", agencyRepository.FindContact(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageContact)]
        public ActionResult UpdateContact([Bind] AgencyContact contact)
        {
            Check.Argument.IsNotNull(contact, "contact");

            var viewData = new JsonViewData();

            if (contact.IsValid)
            {
                if (agencyRepository.FindContact(Current.AgencyId, contact.Id) != null)
                {
                    contact.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditContact(contact))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the contact.";
                    }
                    else
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Contact was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected contact don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = contact.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicians)]
        public JsonResult DeleteContact(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Contact could not be deleted. Please try again." };
            if (agencyRepository.DeleteContact(Current.AgencyId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Contact has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Contact could not be deleted.";
            }
            return Json(viewData);
        }

        #endregion

        #region Hospital Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManageHospital)]
        public ActionResult NewHospital()
        {
            return PartialView("Hospital/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageHospital)]
        public ActionResult AddHospital([Bind] AgencyHospital hospital)
        {
            Check.Argument.IsNotNull(hospital, "hospital");

            var viewData = new JsonViewData();

            if (hospital.IsValid)
            {
                hospital.AgencyId = Current.AgencyId;
                if (!agencyRepository.AddHospital(hospital))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the hospital.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Hospital was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = hospital.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewLists)]
        public ActionResult Hospitals()
        {
            return PartialView("Hospital/List");
        }

        [GridAction]
        [Demand(Permissions.ViewLists)]
        public ActionResult HospitalList()
        {
            return View(new GridModel(agencyRepository.GetHospitals(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageHospital)]
        public ActionResult EditHospital(Guid Id)
        {
            return PartialView("Hospital/Edit", agencyRepository.FindHospital(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageHospital)]
        public ActionResult UpdateHospital([Bind] AgencyHospital hospital)
        {
            Check.Argument.IsNotNull(hospital, "hospital");

            var viewData = new JsonViewData();

            if (hospital.IsValid)
            {
                if (agencyRepository.FindHospital(Current.AgencyId, hospital.Id) != null)
                {
                    hospital.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditHospital(hospital))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the hospital.";
                    }
                    else
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Hospital was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected Hospial don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = hospital.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageHospital)]
        public JsonResult DeleteHospital(Guid Id)
        {
            Check.Argument.IsNotEmpty(Id, "Id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this hospital. Please try again." };
            if (agencyRepository.FindHospital(Current.AgencyId, Id) != null)
            {
                if (agencyRepository.DeleteHospital(Current.AgencyId, Id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your data is successfully deleted.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the hospital.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected Hospial don't exist.";
            }
            return Json(viewData);
        }

        #endregion

        #region Physician Actions

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicians)]
        public JsonResult GetPhysicians()
        {
            return Json(physicianRepository.GetAgencyPhysicians(Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicians)]
        public JsonResult GetPhysician(Guid physicianId)
        {
            Check.Argument.IsNotEmpty(physicianId, "physicianId");
            return Json(physicianRepository.Get(physicianId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public ActionResult Physicians()
        {
            return PartialView("Physician/List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewLists)]
        public ActionResult PhysicianList()
        {
            return View(new GridModel(physicianRepository.GetAgencyPhysicians(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManagePhysicians)]
        public ActionResult NewPhysician()
        {
            return PartialView("Physician/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicians)]
        public ActionResult AddPhysician([Bind] AgencyPhysician agencyPhysician)
        {
            Check.Argument.IsNotNull(agencyPhysician, "agencyPhysician");

            var viewData = new JsonViewData();

            if (agencyPhysician.IsValid)
            {
                agencyPhysician.AgencyId = Current.AgencyId;
                if (!physicianRepository.Add(agencyPhysician))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in Saving the data.";

                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Data successfully Saved";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agencyPhysician.ValidationMessage;
            }

            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditPhysician(Guid Id)
        {
            return PartialView("Physician/Edit", physicianRepository.Get(Id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicians)]
        public ActionResult UpdatePhysician([Bind] AgencyPhysician physician)
        {
            Check.Argument.IsNotNull(physician, "physician");
            var viewData = new JsonViewData();

            if (physician.IsValid)
            {
                if (physicianRepository.Edit(physician))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Physician has been successfully edited";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Physician data could not be saved.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = physician.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicians)]
        public JsonResult DeletePhysician(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "Id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be deleted. Please try again." };
            if (physicianRepository.Delete(Current.AgencyId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Physician has been deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Physician could not be deleted.";
            }
            return Json(viewData);
        }

        #endregion

        #region Location Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewLocation()
        {
            return PartialView("Location/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid)
            {
                if (!agencyService.CreateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Location.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Locations()
        {
            return PartialView("Location/List");
        }

        [GridAction]
        public ActionResult LocationList()
        {
            return View(new GridModel(agencyRepository.GetBranches(Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditLocation(Guid Id)
        {
            return PartialView("Location/Edit", agencyRepository.FindLocation(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid)
            {
                if (agencyRepository.FindLocation(Current.AgencyId, location.Id) != null)
                {
                    location.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditLocation(location))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the location.";
                    }
                    else
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Location was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected location don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        #endregion

        #region Insurance Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManageInsurance)]
        public ActionResult NewInsurance()
        {
            return PartialView("Insurance/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageInsurance)]
        public ActionResult AddInsurance([Bind] AgencyInsurance insurance, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(insurance, "insurance");
            
            List<Validation> rules = new List<Validation>();
            var viewData = new JsonViewData();
            var keys = formCollection.AllKeys;
            if (keys.Contains("RateDiscipline"))
            {
                var disciplineList = formCollection["RateDiscipline"].ToArray();
                var visitRatesList = new List<ChargeRate>();
                disciplineList.ForEach(l =>
                {
                    if (keys.Contains(l + "_Charge") && keys.Contains(l + "_ChargeType"))
                    {
                        visitRatesList.Add(new ChargeRate { RateDiscipline = l, Charge = formCollection[l + "_Charge"], ChargeType = formCollection[l + "_ChargeType"] });
                        rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_Charge"]) ? !formCollection[l + "_Charge"].IsDouble() : false, "Wrong entry"));
                    }
                });
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    insurance.Charge = visitRatesList.ToXml();
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "One of the charge rate is not in correct format";
                    return Json(viewData);
                }
            }
            
            
            if (insurance.IsValid)
            {
                insurance.AgencyId = Current.AgencyId;
                if (!agencyRepository.AddInsurance(insurance))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the insurance.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Insurance was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = insurance.ValidationMessage;
            }
           

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageInsurance)]
        public JsonResult GetInsurances()
        {
            return Json(agencyService.GetInsurances());
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewLists)]
        public ActionResult Insurances()
        {
            return PartialView("Insurance/List");
        }

        [GridAction]
        [Demand(Permissions.ViewLists)]
        public ActionResult InsuranceList()
        {
            var data = agencyRepository.GetInsurances(Current.AgencyId);
            return View(new GridModel(data));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageInsurance)]
        public ActionResult EditInsurance(int Id)
        {
            return PartialView("Insurance/Edit", agencyRepository.FindInsurance(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageInsurance)]
        public ActionResult UpdateInsurance([Bind] AgencyInsurance insurance, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(insurance, "insurance");
            List<Validation> rules = new List<Validation>();
            var viewData = new JsonViewData();
            var keys = formCollection.AllKeys;
            if (keys.Contains("RateDiscipline"))
            {
                var disciplineList = formCollection["RateDiscipline"].ToArray();
                var visitRatesList = new List<ChargeRate>();
                disciplineList.ForEach(l =>
                {
                    if (keys.Contains(l + "_Charge") && keys.Contains(l + "_ChargeType"))
                    {
                        visitRatesList.Add(new ChargeRate { RateDiscipline = l, Charge = formCollection[l + "_Charge"], ChargeType = formCollection[l + "_ChargeType"] });
                        rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_Charge"]) ? !formCollection[l + "_Charge"].IsDouble() : false, "Wrong entry"));
                    }
                });
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    insurance.Charge = visitRatesList.ToXml();
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "One of the charge rate is not in correct format";
                    return Json(viewData);
                }
            }
            if (insurance.IsValid)
            {
                if (agencyRepository.FindInsurance(Current.AgencyId, insurance.Id) != null)
                {
                    insurance.AgencyId = Current.AgencyId;
                    if (!agencyRepository.EditInsurance(insurance))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the insurance.";
                    }
                    else
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Insurance was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected insurance don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = insurance.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManageInsurance)]
        public JsonResult DeleteInsurance(int Id)
        {
            Check.Argument.IsNotNegativeOrZero(Id, "Id");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance. Please try again." };
            if (agencyRepository.FindInsurance(Current.AgencyId, Id) != null)
            {
                if (agencyRepository.DeleteInsurance(Current.AgencyId, Id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your data is successfully deleted.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in deleting the insurance.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected insurance don't exist.";
            }
            return Json(viewData);
        }

        #endregion

        #region Infection Reports Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewIncidentAccidentInfectionReport)]
        public ActionResult NewInfectionReport()
        {
            return PartialView("Infection/New", new VisitNoteViewData());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public ActionResult InfectionReports()
        {
            return PartialView("Infection/List");
        }

        [GridAction]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult InfectionReportList()
        {
            return View(new GridModel(agencyRepository.GetHospitals(Current.AgencyId)));
        }

        #endregion

        #region Incident Reports Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewIncidentAccidentInfectionReport)]
        public ActionResult NewIncidentReport()
        {
            return PartialView("Incident/New", new VisitNoteViewData());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public ActionResult IncidentReports()
        {
            return PartialView("Incident/List");
        }

        [GridAction]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult IncidentReportList()
        {
            return View(new GridModel(agencyRepository.GetHospitals(Current.AgencyId)));
        }

        #endregion

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitRates()
        {
            return PartialView("VisitRate", agencyRepository.GetMainLocation(Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitRateContent(Guid branchId)
        {
            return PartialView("VisitRateContent", agencyRepository.FindLocation(Current.AgencyId, branchId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EditCost(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData();
            List<Validation> rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            if (keys.Contains("AgencyLocationId") && formCollection["AgencyLocationId"].IsNotNullOrEmpty())
            {
                var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, formCollection["AgencyLocationId"].ToGuid());
                if (agencyLocation != null)
                {
                    if (keys.Contains("RateDiscipline"))
                    {
                        var disciplineList = formCollection["RateDiscipline"].ToArray();
                        var visitRatesList = new List<CostRate>();
                        disciplineList.ForEach(l =>
                        {
                            if (keys.Contains(l + "_PerVisit") && keys.Contains(l + "_PerHour") && keys.Contains(l + "_PerUnit"))
                            {
                                visitRatesList.Add(new CostRate { RateDiscipline = l, PerVisit = formCollection[l + "_PerVisit"], PerHour = formCollection[l + "_PerHour"], PerUnit = formCollection[l + "_PerUnit"] });
                                rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_PerVisit"]) ? !formCollection[l + "_PerVisit"].IsDouble() : false, "Wrong entry"));
                                rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_PerHour"]) ? !formCollection[l + "_PerHour"].IsDouble() : false, "Wrong entry"));
                                rules.Add(new Validation(() => !string.IsNullOrEmpty(formCollection[l + "_PerUnit"]) ? !formCollection[l + "_PerUnit"].IsDouble() : false, "Wrong entry"));
                            }
                        });
                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            agencyLocation.Cost = visitRatesList.ToXml(); 
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "One of the cost rate is not in correct format";
                            return Json(viewData);
                        }

                        if (agencyRepository.EditBranchCost(agencyLocation))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Your data is successfully edited.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Your data is not successfully edited try again.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error on the entered data.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected branch don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Selected branch don't exist.";
            }
            return Json(viewData);
        }
        
    }
}
