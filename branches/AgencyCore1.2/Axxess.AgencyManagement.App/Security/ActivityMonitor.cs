﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Timers;
    using System.Diagnostics;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    internal sealed class ActivityMonitor
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
                instance.StartTimer();
            }

            internal static readonly ActivityMonitor instance = new ActivityMonitor();
        }

        #endregion

        #region Private Members

        private Timer timer;
        private static Dictionary<string, AxxessPrincipal> tempPrincipalList;
        private static Dictionary<string, AxxessPrincipal> activePrincipalList;

        private readonly System.Threading.ReaderWriterLockSlim readWriteLock = new System.Threading.ReaderWriterLockSlim();

        #endregion

        #region Public Instance

        public static ActivityMonitor Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private ActivityMonitor()
        {
            activePrincipalList = new Dictionary<string, AxxessPrincipal>();
        }

        private void StartTimer()
        {
            this.timer = new System.Timers.Timer(TimeSpan.FromMinutes(1).TotalMilliseconds);
            this.timer.Elapsed += new ElapsedEventHandler(this.Timer_Elapsed);
            this.timer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            using (readWriteLock.AcquireWriteLock())
            {
                this.VerifyActiveIdentities();
            }
        }

        private void VerifyActiveIdentities()
        {
            if (activePrincipalList.Count > 0)
            {
                tempPrincipalList = new Dictionary<string, AxxessPrincipal>(activePrincipalList);
                DateTime expirationTime = DateTime.Now.Subtract(TimeSpan.FromMinutes(AppSettings.SessionTimeoutInMinutes));

                foreach (KeyValuePair<string, AxxessPrincipal> kvPair in activePrincipalList)
                {
                    string name = kvPair.Key;
                    AxxessPrincipal principal = kvPair.Value;

                    AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                    
                    if (identity.LastSecureActivity < expirationTime)
                    {
                        identity.IsAuthenticated = false;
                    }

                    if (!identity.IsAuthenticated)
                    {
                        if (tempPrincipalList.ContainsKey(name))
                        {
                            tempPrincipalList.Remove(name);
                        }
                    }
                }
                activePrincipalList = tempPrincipalList;
            }
        }

        #endregion

        #region Public Methods

        public AxxessPrincipal Get(string username)
        {
            AxxessPrincipal principal = null;

            using (readWriteLock.AcquireReadLock())
            {
                if (activePrincipalList.ContainsKey(username))
                {
                    principal = activePrincipalList[username];
                    
                    AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                    identity.LastSecureActivity = DateTime.Now;
                }
            }
            return principal;
        }

        public void Add(AxxessPrincipal principal)
        {
            using (readWriteLock.AcquireUpgradeableLock())
            {
                if (activePrincipalList.ContainsKey(principal.Identity.Name))
                {
                    using (readWriteLock.AcquireWriteLock())
                    {
                        activePrincipalList[principal.Identity.Name] = principal;
                    }
                }
                else
                {
                    using (readWriteLock.AcquireWriteLock())
                    {
                        activePrincipalList.Add(principal.Identity.Name, principal);
                    }
                }
            }
        }

        public void Remove(string username)
        {
            using (readWriteLock.AcquireWriteLock())
            {
                if (activePrincipalList.ContainsKey(username))
                {
                    activePrincipalList.Remove(username);
                }
            }
        }

        public int Count
        {
            get
            {
                using (readWriteLock.AcquireReadLock())
                {
                    return activePrincipalList.Count;
                }
            }
        }

        #endregion
    }
}
