﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
   public class EmergencyViewData
    {
       public Guid Id { get; set; }

       public Guid PatientId { get; set; }

       public string FirstName { get; set; }

       public string LastName { get; set; }

       public string PrimaryPhone { get; set; }

       public string AlternatePhone { get; set; }

       public string EmailAddress { get; set; }

       public string Relationship { get; set; }

       public string AddressLine1 { get; set; }

       public string AddressLine2 { get; set; }

       public string AddressCity { get; set; }

       public string AddressState { get; set; }

       public string AddressZipCode { get; set; }

       public bool Primary { get; set; }

       public string[] EmergencyContactPrimaryPhoneArray { get; set; }

       public string[] EmergencyContactAltPhoneArray { get; set; }
    }
}
