﻿namespace Axxess.AgencyManagement.App.Components
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class PhysicianExporter : BaseExporter
    {
        private IList<AgencyPhysician> physicians;
        public PhysicianExporter(IList<AgencyPhysician> physicians)
            : base()
        {
            this.physicians = physicians;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Physicians";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("Physicians");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Row headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("NPI");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Email");
            headerRow.CreateCell(3).SetCellValue("Address");
            headerRow.CreateCell(4).SetCellValue("Phone Number");
            headerRow.CreateCell(5).SetCellValue("Fax Phone");
            headerRow.RowStyle = headerStyle;

            if (this.physicians.Count > 0)
            {
                int i = 1;
                this.physicians.ForEach(p =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(p.NPI);
                    dataRow.CreateCell(1).SetCellValue(p.DisplayName);
                    dataRow.CreateCell(2).SetCellValue(p.EmailAddress);
                    dataRow.CreateCell(3).SetCellValue(p.AddressFull);
                    dataRow.CreateCell(4).SetCellValue(p.PhoneWork.ToPhone());
                    dataRow.CreateCell(5).SetCellValue(p.FaxNumber.ToPhone());
                    i++;
                });
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
        }
    }
}
