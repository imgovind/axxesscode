﻿namespace Axxess.AgencyManagement.App
{
    using System.Web.Mvc;

    using StructureMap;

    using OpenForum.Core;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC;

    using Web;

    public static class Bootstrapper
    {
        static Bootstrapper()
        {
            Axxess.Core.Infrastructure.Container.InitializeWith(new StructureMapDependencyResolver());

            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());
            ObjectFactory.Initialize(x =>
            {
                x.AddRegistry<CoreRegistry>();
                x.AddRegistry<ApplicationRegistry>();
                x.AddRegistry<ForumRegistry>();
            });

            AreaRegistration.RegisterAllAreas();
        }

        public static void Run()
        {
            Axxess.Core.Infrastructure.Container.ResolveAll<IStartupTask>().ForEach(t => t.Execute());
            Module.Register(new ReportModule());
            Module.Register(new UserModule());
            Module.Register(new ScheduleModule());
            Module.Register(new AssetModule());
            Module.Register(new OasisModule());
            Module.Register(new PatientModule());
            Module.Register(new AgencyModule());
            Module.Register(new AccountModule());
            Module.Register(new DefaultModule());
            OpenForumManager.SimpleInitialize();
        }
    }
}
