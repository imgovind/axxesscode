﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Domain;
    
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Enums;

    using Axxess.Api.Contracts;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.LookUp.Domain;
   

    public interface IAssessmentService
    {
        Assessment SaveAssessment(FormCollection formCollection, HttpFileCollectionBase httpFiles);
        Assessment AddAssessment(ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode);
        Assessment AddAssessment(ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode, string MedicationProfile);
        Assessment GetAssessment(Guid assessmentId, string assessmentType);
        ValidationInfoViewData Validate(Guid assessmentId, Guid patientId ,Guid episodeId, string assessmentType);
        string OasisHeader(Agency agency);
        string OasisFooter(int totalNumberOfRecord);
        List<SubmissionBodyFormat> GetOasisSubmissionFormatInstructions();
        string GetOasisSubmissionFormat(List<SubmissionBodyFormat> instructions, IDictionary<string, Question> assessment, int versionNumber);
        bool UpdatePlanofCare(FormCollection formCollection);
        void GeneratePlanofCare(ScheduleEvent scheduleEvent, Patient patient, Assessment assessment);
        IDictionary<string, Question> GetEpisodeAssessmentQuestions(Guid episodeId, Guid patientId);
        Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId);
        IAssessment InsertMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication,string MedicationType);
        IAssessment DeleteMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication);
        bool UpdateMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication, string MedicationType);
        Medication GetMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Guid Id);
        bool UpdateMedicationForDischarge(Guid AssessmentId, Guid PatientId, string AssessmentType, Guid Id, DateTime DischargeDate);
        IDictionary<string, Question> Allergies(Guid assessmentId, string AssessmentType);
        IDictionary<string, Question> Diagnosis(Guid assessmentId, string AssessmentType);
        bool UpdateAssessmentStatus(Guid Id, Guid patientId, Guid episodeId, string assessmentType, string status, string reason);
        bool MarkAsExported(List<string> OasisSelected);
        bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, string name, Guid assetId);
        bool AddSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, int supplyId, string quantity , string date);
        List<Supply> GetAssessmentSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType);
        bool UpdateSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply);
        bool DeleteSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply);
    }
}
