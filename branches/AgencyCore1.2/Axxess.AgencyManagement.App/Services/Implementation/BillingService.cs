﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Script.Serialization;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Enums;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Security;
    using Axxess.Membership.Enums;
    using System.Xml.Linq;
    using System.Net;
    using System.IO;
    using System.Text.RegularExpressions;

    public class BillingService : IBillingService
    {

        #region Private Members

        private IUserRepository userRepository;
        private IAssessmentService assessmentService;
        private IPatientRepository patientRepository;
        private IBillingRepository billingRepository;
        private IReferralRepository referrralRepository;
        private IPhysicianRepository physicianRepository;
        private IAgencyRepository agencyRepository;
        #endregion

        #region Constructor

        public BillingService(IAgencyManagementDataProvider dataProvider, IAssessmentService assessmentService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            this.assessmentService = assessmentService;
            this.patientRepository = dataProvider.PatientRepository;
            this.userRepository = dataProvider.UserRepository;
            this.referrralRepository = dataProvider.ReferralRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
        }

        public string Generate(List<Guid> rapToGenerate, List<Guid> finalToGenerate)
        {
            Agency agency = agencyRepository.Get(Current.AgencyId);
            List<Rap> rapClaim = new List<Rap>();
            List<Final> finlaClaim = new List<Final>();
            var requestArr = "";
            string strResult = String.Empty;
            if (rapToGenerate != null)
            {
                rapToGenerate.ForEach(rap =>
                {
                    Rap claim = null;
                    if (rap != Guid.Empty)
                    {
                        claim = billingRepository.GetRap(Current.AgencyId, rap);
                    }
                    if (claim != null)
                    {
                        rapClaim.Add(claim);
                    }
                });
            }
            if (finalToGenerate != null)
            {
                finalToGenerate.ForEach(final =>
                {
                    Final claim = null;
                    if (final != Guid.Empty)
                    {
                        claim = billingRepository.GetFinal(Current.AgencyId, final);
                    }
                    if (claim != null)
                    {
                        finlaClaim.Add(claim);
                    }
                });
            }

            if (agency != null)
            {
                AgencyLocation agencyLocation = agencyRepository.GetMainLocation(agency.Id);
                if (agencyLocation != null)
                {
                    List<object> patients = new List<object>();
                    int rapCount = rapClaim.Count;
                    foreach (var rap in rapClaim)
                    {
                        var diganosis = XElement.Parse(rap.DiagonasisCode);
                        List<object> claims = new List<object>();
                        var rapObj = new
                        {
                            claim_id = rap.Id,
                            claim_type = "RAP",
                            claim_physician_upin = rap.PhysicianNPI,
                            claim_physician_last_name = rap.PhysicianLastName,
                            claim_physician_first_name = rap.PhysicianFirstName,
                            claim_first_visit_date = rap.FirstBillableVisitDate.ToShortDateString(),
                            claim_episode_start_date = rap.EpisodeStartDate.ToShortDateString(),
                            claim_episode_end_date = rap.EpisodeStartDate.ToShortDateString(),
                            claim_hipps_code = rap.HippsCode,
                            claim_oasis_key = rap.ClaimKey,
                            claim_diagnosis_code1 = (diganosis != null && diganosis.Element("code1") != null ? Regex.Replace(diganosis.Element("code1").Value, @"[.]", "") : ""),
                            claim_diagnosis_code2 = (diganosis != null && diganosis.Element("code2") != null ? Regex.Replace(diganosis.Element("code2").Value, @"[.]", "") : ""),
                            claim_diagnosis_code3 = (diganosis != null && diganosis.Element("code3") != null ? Regex.Replace(diganosis.Element("code3").Value, @"[.]", "") : ""),
                            claim_diagnosis_code4 = (diganosis != null && diganosis.Element("code4") != null ? Regex.Replace(diganosis.Element("code4").Value, @"[.]", "") : ""),
                            claim_diagnosis_code5 = (diganosis != null && diganosis.Element("code5") != null ? Regex.Replace(diganosis.Element("code5").Value, @"[.]", "") : ""),
                            claim_diagnosis_code6 = (diganosis != null && diganosis.Element("code6") != null ? Regex.Replace(diganosis.Element("code6").Value, @"[.]", "") : "")
                        };
                        claims.Add(rapObj);
                        if (finlaClaim != null && finlaClaim.Count > 0)
                        {
                            var final = finlaClaim.Find(f => f.PatientId == rap.PatientId && f.EpisodeId == rap.EpisodeId);
                            if (final != null)
                            {
                                var visits = final.VerifiedVisits != null && final.VerifiedVisits != "" ? final.VerifiedVisits.ToObject<List<ScheduleEvent>>() : new List<ScheduleEvent>();
                                List<object> visistList = new List<object>();
                                if (visits.Count > 0)
                                {
                                    visits.ForEach(v =>
                                    {
                                        visistList.Add(new { date = v.EventDate, type = v.Discipline == "Nursing" ? "SN" : v.Discipline });
                                    });
                                }
                                var finalObj = new
                                {
                                    claim_id = final.Id,
                                    claim_type = "FINAL",
                                    claim_physician_upin = final.PhysicianNPI,
                                    claim_physician_last_name = final.PhysicianLastName,
                                    claim_physician_first_name = final.PhysicianFirstName,
                                    claim_first_visit_date = final.FirstBillableVisitDate.ToShortDateString(),
                                    claim_episode_start_date = final.EpisodeStartDate.ToShortDateString(),
                                    claim_episode_end_date = final.EpisodeEndDate.ToShortDateString(),
                                    claim_hipps_code = final.HippsCode,
                                    claim_oasis_key = final.ClaimKey,
                                    claim_diagnosis_code1 = (diganosis != null && diganosis.Element("code1") != null ? Regex.Replace(diganosis.Element("code1").Value, @"[.]", "") : ""),
                                    claim_diagnosis_code2 = (diganosis != null && diganosis.Element("code2") != null ? Regex.Replace(diganosis.Element("code2").Value, @"[.]", "") : ""),
                                    claim_diagnosis_code3 = (diganosis != null && diganosis.Element("code3") != null ? Regex.Replace(diganosis.Element("code3").Value, @"[.]", "") : ""),
                                    claim_diagnosis_code4 = (diganosis != null && diganosis.Element("code4") != null ? Regex.Replace(diganosis.Element("code4").Value, @"[.]", "") : ""),
                                    claim_diagnosis_code5 = (diganosis != null && diganosis.Element("code5") != null ? Regex.Replace(diganosis.Element("code5").Value, @"[.]", "") : ""),
                                    claim_diagnosis_code6 = (diganosis != null && diganosis.Element("code6") != null ? Regex.Replace(diganosis.Element("code6").Value, @"[.]", "") : ""),
                                    claim_visits = visistList
                                };
                                claims.Add(finalObj);
                                finlaClaim.RemoveAll(f => f.PatientId == rap.PatientId && f.EpisodeId == rap.EpisodeId);
                            }
                        }
                        var patient = new
                        {
                            id = "",
                            patient_gender = rap.Gender.Substring(0, 1),
                            patient_medicare_num = rap.MedicareNumber,
                            patient_record_num = rap.PatientIdNumber,
                            patient_dob = rap.DOB.ToShortDateString(),
                            patient_doa = rap.StartofCareDate.ToShortDateString(),
                            patient_dod = "",
                            patient_address = rap.AddressLine1,
                            patient_address2 = rap.AddressLine2,
                            patient_city = rap.AddressCity,
                            patient_state = rap.AddressStateCode,
                            patient_zip = rap.AddressZipCode,
                            patient_cbsa = agencyLocation.CBSA,
                            patient_last_name = rap.LastName,
                            patient_first_name = rap.FirstName,
                            patient_middle_initial = "",
                            claims_arr = claims
                        };
                        patients.Add(patient);
                    }

                    if (finlaClaim != null && finlaClaim.Count > 0)
                    {
                        foreach (var final in finlaClaim)
                        {
                            List<object> claims = new List<object>();
                            var diganosis = XElement.Parse(final.DiagonasisCode);
                            var visits = final.VerifiedVisits != null && final.VerifiedVisits != "" ? final.VerifiedVisits.ToObject<List<ScheduleEvent>>() : new List<ScheduleEvent>();
                            List<object> visistList = new List<object>();
                            if (visits.Count > 0)
                            {
                                visits.ForEach(v =>
                                {
                                    visistList.Add(new { date = v.EventDate, type = v.Discipline == "Nursing" ? "SN" : v.Discipline });
                                });
                            }
                            var finalObj = new
                            {
                                claim_id = final.Id,
                                claim_type = "FINAL",
                                claim_physician_upin = final.PhysicianNPI,
                                claim_physician_last_name = final.PhysicianLastName,
                                claim_physician_first_name = final.PhysicianFirstName,
                                claim_first_visit_date = final.FirstBillableVisitDate.ToShortDateString(),
                                claim_episode_start_date = final.EpisodeStartDate.ToShortDateString(),
                                claim_episode_end_date = final.EpisodeEndDate.ToShortDateString(),
                                claim_hipps_code = final.HippsCode,
                                claim_oasis_key = final.ClaimKey,
                                claim_diagnosis_code1 = (diganosis != null && diganosis.Element("code1") != null ? Regex.Replace(diganosis.Element("code1").Value, @"[.]", "") : ""),
                                claim_diagnosis_code2 = (diganosis != null && diganosis.Element("code2") != null ? Regex.Replace(diganosis.Element("code2").Value, @"[.]", "") : ""),
                                claim_diagnosis_code3 = (diganosis != null && diganosis.Element("code3") != null ? Regex.Replace(diganosis.Element("code3").Value, @"[.]", "") : ""),
                                claim_diagnosis_code4 = (diganosis != null && diganosis.Element("code4") != null ? Regex.Replace(diganosis.Element("code4").Value, @"[.]", "") : ""),
                                claim_diagnosis_code5 = (diganosis != null && diganosis.Element("code5") != null ? Regex.Replace(diganosis.Element("code5").Value, @"[.]", "") : ""),
                                claim_diagnosis_code6 = (diganosis != null && diganosis.Element("code6") != null ? Regex.Replace(diganosis.Element("code6").Value, @"[.]", "") : ""),
                                claim_visits = visistList
                            };
                            claims.Add(finalObj);
                            var patient = new
                            {
                                id = "",
                                patient_gender = final.Gender.Substring(0, 1),
                                patient_medicare_num = final.MedicareNumber,
                                patient_record_num = final.PatientIdNumber,
                                patient_dob = final.DOB.ToShortDateString(),
                                patient_doa = final.StartofCareDate.ToShortDateString(),
                                patient_dod = "",
                                patient_address = final.AddressLine1,
                                patient_address2 = final.AddressLine2,
                                patient_city = final.AddressCity,
                                patient_state = final.AddressStateCode,
                                patient_zip = final.AddressZipCode,
                                patient_cbsa = agencyLocation.CBSA,
                                patient_last_name = final.LastName,
                                patient_first_name = final.FirstName,
                                patient_middle_initial = "",
                                claims_arr = claims
                            };
                            patients.Add(patient);
                        }
                    }
                    var agencyClaim = new
                    {
                        format = "ansi837",
                        user_login_name = Current.User.Name,
                        user_agency_name = agency.Name,
                        user_tax_id = agency.TaxId,
                        user_payor = agency.Payor,
                        user_national_provider_id = agency.NationalProviderNumber,
                        user_address_1 = agencyLocation.AddressLine1,
                        user_address_2 = agencyLocation.AddressLine2,
                        user_city = agencyLocation.AddressCity,
                        user_state = agencyLocation.AddressStateCode,
                        user_zip = agencyLocation.AddressZipCode,
                        user_phone = agencyLocation.PhoneWork,
                        user_fax = agencyLocation.FaxNumber,
                        user_CBSA_code = agencyLocation.CBSA,
                        patients_arr = patients
                    };
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    requestArr = jss.Serialize(agencyClaim);
                }
            }
            if (requestArr != "")
            {
                try
                {
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    string postData = ("request=" + requestArr);
                    byte[] data = encoding.GetBytes(postData);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://panda.axxessconsult.com/information_services/837_api.php");
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = data.Length;
                    Stream newStream = request.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Stream receiveStream = response.GetResponseStream();
                        Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                        StreamReader readStream = new StreamReader(receiveStream, encode);
                        strResult = readStream.ReadToEnd();
                    }
                }
                catch (Exception ex)
                {
                }
            }
            return strResult;
        }

        public bool VerifyFinal(int claim, string type)
        {
            Check.Argument.IsNotNegativeOrZero(claim, "claim");

            bool result = false;
            var currentClaim = billingRepository.GetClaim(Current.AgencyId, claim);
            if (currentClaim != null)
            {
                switch (type)
                {
                    case "Info":
                        currentClaim.IsFinalInfoVerified = true;
                        billingRepository.UpdateClaimStatus(currentClaim);
                        result = true;
                        break;
                    case "Visit":
                        currentClaim.IsVisitVerified = true;
                        billingRepository.UpdateClaimStatus(currentClaim);
                        result = true;
                        break;
                    case "Supply":
                        currentClaim.IsSupplyVerified = true;
                        billingRepository.UpdateClaimStatus(currentClaim);
                        result = true;
                        break;
                }
            }

            return result;

        }

        public bool VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> Visit)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            var scheduleEvents = patientRepository.GetEpisodeSchedules(Current.AgencyId, patientId, episodeId);
            var visitList = new List<ScheduleEvent>();
            var claim = billingRepository.GetFinal(Current.AgencyId, Id);
            Visit.ForEach(v =>
            {
                var visit = scheduleEvents.FirstOrDefault(s => s.EventId == v);
                if (visit != null)
                {
                    visitList.Add(visit);
                }
            });
            claim.IsVisitVerified = true;
            claim.VerifiedVisits = visitList.ToXml();
            claim.Modified = DateTime.Now;
            result = billingRepository.UpdateFinalStatus(claim);
            return result;
        }

        public IList<ClaimViewData> Activity(Guid patientId)
        {

            var raps = billingRepository.GetRapProcessed(patientId, Current.AgencyId);
            var finals = billingRepository.GetFinalProcessed(patientId, Current.AgencyId);
            var claims = new List<ClaimViewData>();
            if (raps != null && raps.Count > 0)
            {
                raps.ForEach(rap =>
                {

                    claims.Add(
                        new ClaimViewData
                        {
                            Id = rap.Id,
                            FirstName = rap.FirstName,
                            LastName = rap.LastName,
                            MedicareNumber = rap.MedicareNumber,
                            PaymentDate = rap.PaymentDate,
                            Type = "Rap",
                            Created = rap.Created,
                            Status = rap.Status,
                            EpisodeStartDate = rap.EpisodeStartDate,
                            EpisodeEndDate = rap.EpisodeEndDate,

                        }
                        );


                });
            }
            if (finals != null && finals.Count > 0)
            {
                finals.ForEach(final =>
                {

                    claims.Add(
                        new ClaimViewData
                        {
                            Id = final.Id,
                            Type = "Final",
                            FirstName = final.FirstName,
                            LastName = final.LastName,
                            MedicareNumber = final.MedicareNumber,
                            PaymentDate = final.PaymentDate,
                            Created = final.Created,
                            Status = final.Status,
                            EpisodeStartDate = final.EpisodeStartDate,
                            EpisodeEndDate = final.EpisodeEndDate
                        }
                        );

                });
            }

            return claims;

        }

        public IList<ClaimViewData> PendingClaims()
        {

            var raps = billingRepository.GetRaps(Current.AgencyId);
            var finals = billingRepository.GetFinals(Current.AgencyId);
            var claims = new List<ClaimViewData>();
            if (raps != null && raps.Count > 0)
            {
                raps.ForEach(rap =>
                {
                    if (rap.Status == 19 || rap.Status == 14 || rap.Status == 16 || rap.Status == 17)
                    {
                        claims.Add(
                            new ClaimViewData
                            {
                                Id = rap.Id,
                                FirstName = rap.FirstName,
                                LastName = rap.LastName,
                                MedicareNumber = rap.MedicareNumber,
                                PaymentDate = rap.PaymentDate,
                                Type = "Rap",
                                Created = rap.Created,
                                Status = rap.Status,
                                EpisodeStartDate = rap.EpisodeStartDate,
                                EpisodeEndDate = rap.EpisodeEndDate,

                            }
                            );
                    }

                });
            }
            if (finals != null && finals.Count > 0)
            {
                finals.ForEach(final =>
                {
                    if (final.Status == 19 || final.Status == 14 || final.Status == 16 || final.Status == 17)
                    {
                        claims.Add(
                            new ClaimViewData
                            {
                                Id = final.Id,
                                Type = "Final",
                                FirstName = final.FirstName,
                                LastName = final.LastName,
                                MedicareNumber = final.MedicareNumber,
                                PaymentDate = final.PaymentDate,
                                Created = final.Created,
                                Status = final.Status,
                                EpisodeStartDate = final.EpisodeStartDate,
                                EpisodeEndDate = final.EpisodeEndDate
                            }
                            );
                    }
                });
            }

            return claims;

        }

        public IList<TypeOfBill> GetAllUnProcessedBill()
        {
            IList<TypeOfBill> claims = null;
            var bill = billingRepository.AllUnProcessedBill(Current.AgencyId);
            if (bill != null)
            {
                claims = new List<TypeOfBill>();

                if (bill.Raps != null && bill.Raps.Count > 0)
                {
                    bill.Raps.ForEach(r =>
                    {
                        if (r.IsFirstBillableVisit && r.IsOasisComplete)
                        {
                            claims.Add(new TypeOfBill
                            {
                                PatientName = r.DisplayName,
                                EpisodeRange = r.EpisodeRange,
                                Type = "RAP"
                            });
                        }
                    });
                }

                if (bill.Finals != null && bill.Finals.Count > 0)
                {
                    bill.Finals.ForEach(f =>
                    {
                        if (f.IsRapGenerated && f.AreVisitsComplete && f.AreOrdersComplete)
                        {
                            claims.Add(new TypeOfBill
                            {
                                PatientName = f.DisplayName,
                                EpisodeRange = f.EpisodeRange,
                                Type = "Final"
                            });
                        }
                    });
                }
            }
            return claims;
        }

        #endregion
    }
}
