﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Enums;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Domain;
    using Security;
    using Axxess.Membership.Enums;

    public class PayrollService : IPayrollService
    {
        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;

        public PayrollService(IAgencyManagementDataProvider agencyManagementDataProvider, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
        }

        public List<VisitSummary> GetSummary(DateTime startDate, DateTime endDate)
        {
            var list = new List<VisitSummary>();

            var users = userRepository.GetAgencyUsers(Current.AgencyId);

            users.ForEach(user =>
            {
                int count = Container.Resolve<IUserService>().GetSchedule(user.Id, startDate, endDate).Count;
                if (count > 0)
                {
                    var summary = new VisitSummary();
                    summary.UserId = user.Id;
                    summary.UserName = user.DisplayName;
                    summary.VisitCount = count;
                    list.Add(summary);
                }
            });

            return list;
        }

        public List<PayrollDetail> GetDetails(DateTime startDate, DateTime endDate)
        {
            var list = new List<PayrollDetail>();

            var users = userRepository.GetAgencyUsers(Current.AgencyId);

            users.ForEach(user =>
            {
                int count = userService.GetSchedule(user.Id, startDate, endDate).Count;
                if (count > 0)
                {
                    var detail = new PayrollDetail();
                    detail.Name = user.DisplayName;
                    detail.Visits = userService.GetSchedule(user.Id, startDate, endDate);
                    list.Add(detail);
                }
            });

            return list;
        }

        
    }
}
