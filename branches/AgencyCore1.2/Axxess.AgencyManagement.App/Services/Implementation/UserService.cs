﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Security;
    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    using Axxess.Membership.Enums;

    public class UserService : IUserService
    {
        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IPatientRepository patientRepository;

        public UserService(IAgencyManagementDataProvider agencyManagmentDataProvider, IMembershipDataProvider membershipDataProvider, IPatientService patientService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");

            this.patientService = patientService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.patientRepository = agencyManagmentDataProvider.PatientRepository;
        }

        public bool IsEmailAddressUnique(string emailAddress)
        {
            return loginRepository.Find(emailAddress) == null;
        }

        public bool IsPasswordCorrect(Guid userId, string password)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(password, login.PasswordHash, login.PasswordSalt))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsSignatureCorrect(Guid userId, string signature)
        {
            if (!userId.IsEmpty() && signature.IsNotNullOrEmpty())
            {
                var user = userRepository.Get(userId, Current.AgencyId);
                if (user != null)
                {
                    var login = loginRepository.Find(user.LoginId);
                    if (login != null)
                    {
                        var saltedHash = new SaltedHash();
                        if (saltedHash.VerifyHashAndSalt(signature, login.SignatureHash, login.SignatureSalt))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool CreateUser(User user)
        {
            try
            {
                string salt = string.Empty;
                string passwordHash = string.Empty;
                Guid userId = Guid.Empty;

                var saltedHash = new SaltedHash();
                saltedHash.GetHashAndSalt(user.Password, out passwordHash, out salt);

                Guid loginId = loginRepository.Add(user.EmailAddress, passwordHash, salt, Roles.ApplicationUser.ToString(), user.FirstName, true, user.AllowWeekendAccess, user.EarliestLoginTime, user.AutomaticLogoutTime);
                if (!loginId.IsEmpty())
                {
                    user.LoginId = loginId;
                    user.AgencyId = Current.AgencyId;
                    user.Profile = new UserProfile();
                    user.Profile.EmailWork = user.EmailAddress;
                    if (user.TitleTypeOther.IsNotNullOrEmpty())
                    {
                        user.TitleType = user.TitleTypeOther;
                    }
                    if (user.CredentialsOther.IsNotNullOrEmpty())
                    {
                        user.Credentials = user.CredentialsOther;
                    }
                    if (userRepository.Add(user))
                    {
                        var bodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName, "agencyname", Current.AgencyName, "emailaddress", user.EmailAddress, "password", user.Password);
                        Notify.User(AppSettings.NoReplyEmail, user.EmailAddress, "Welcome to Axxess Home Health Management Software", bodyText);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO Log Exception
                return false;
            }

            return false;
        }

        public List<UserVisit> GetSchedule(Guid userId)
        {
            var schedule = new List<UserVisit>();

            var userEvents = userRepository.GetSchedule(Current.AgencyId, userId);

            userEvents.ForEach(ue =>
            {
                if (ue.DisciplineTask != (int)DisciplineTasks.Rap && ue.DisciplineTask != (int)DisciplineTasks.Final)
                {
                    if (!ue.IsMissedVisit)
                    {
                        var visitNote = string.Empty;
                        var episode = patientRepository.GetEpisode(Current.AgencyId, ue.EpisodeId, ue.PatientId);
                        if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleList = episode.Schedule.ToObject<List<ScheduleEvent>>();
                            if (scheduleList != null && scheduleList.Count > 0)
                            {
                                var scheduledEvent = scheduleList.Find(e => e.EventId == ue.EventId);
                                if (scheduledEvent != null && !scheduledEvent.IsMissedVisit && !scheduledEvent.IsCompleted())
                                {
                                    var user = userRepository.Get(ue.UserId, Current.AgencyId);
                                    var patient = patientRepository.GetPatient<Patient>(ue.PatientId, Current.AgencyId);
                                    if (scheduledEvent.Comments.IsNotNullOrEmpty())
                                    {
                                        visitNote = scheduledEvent.Comments;
                                    }

                                    Common.Url.Set(scheduledEvent, false, false);
                                    schedule.Add(new UserVisit
                                    {
                                        Id = ue.EventId,
                                        UserId = ue.UserId,
                                        EpisodeId = episode.Id,
                                        PatientId = patient.Id,
                                        VisitDate = ue.EventDate.ToZeroFilled(),
                                        EpisodeNotes = episode.Detail.Comments,
                                        VisitNotes = visitNote,
                                        PatientName = patient.DisplayName.ToTitleCase(),
                                        UserDisplayName = user.DisplayName,
                                        Url = scheduledEvent.Url,
                                        StatusName = ue.StatusName,
                                        TaskName = ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), ue.DisciplineTask.ToString())).GetDescription()
                                    });
                                }
                            }
                        }
                    }
                }
            });

            return schedule;
        }

        public List<UserVisit> GetSchedule(Guid userId, DateTime start, DateTime end)
        {
            var schedule = new List<UserVisit>();

            var userEvents = userRepository.GetSchedule(Current.AgencyId, userId, start, end);

            userEvents.ForEach(ue =>
            {
                if (ue.DisciplineTask != (int)DisciplineTasks.Rap && ue.DisciplineTask != (int)DisciplineTasks.Final)
                {
                    if (!ue.IsMissedVisit)
                    {
                        var episode = patientRepository.GetEpisode(Current.AgencyId, ue.EpisodeId, ue.PatientId);
                        var visitNote = string.Empty;
                        if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                        {
                            var scheduleList = episode.Schedule.ToObject<List<ScheduleEvent>>();
                            if (scheduleList != null && scheduleList.Count > 0)
                            {
                                var scheduledEvent = scheduleList.Find(e => e.EventId == ue.EventId);
                                if (scheduledEvent != null && !scheduledEvent.IsMissedVisit)
                                {
                                    var user = userRepository.Get(ue.UserId, Current.AgencyId);
                                    var patient = patientRepository.GetPatient<Patient>(ue.PatientId, Current.AgencyId);

                                    if (scheduledEvent.Comments.IsNotNullOrEmpty())
                                    {
                                        visitNote = scheduledEvent.Comments;
                                    }

                                    Common.Url.Set(scheduledEvent, false, false);
                                    schedule.Add(new UserVisit
                                    {
                                        Id = ue.EventId,
                                        UserId = ue.UserId,
                                        VisitDate = ue.EventDate.ToZeroFilled(),
                                        VisitRate = "$0.00",
                                        Surcharge = "$0",
                                        EpisodeId = episode.Id,
                                        PatientId = patient.Id,
                                        EpisodeNotes = episode.Detail.Comments,
                                        VisitNotes = visitNote,
                                        PatientName = patient.DisplayName,
                                        UserDisplayName = user.DisplayName,
                                        Url = scheduledEvent.Url,
                                        StatusName = ue.StatusName,
                                        TaskName = ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), ue.DisciplineTask.ToString())).GetDescription()
                                    });
                                }
                            }
                        }
                    }
                }
            });

            return schedule;
        }

        public bool UpdateProfile(User user)
        {
            var result = false;

            if (userRepository.UpdateProfile(user))
            {
                result = true;
                if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty() || user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                {
                    User userInfo = userRepository.Get(user.Id, Current.AgencyId);
                    Login login = loginRepository.Find(user.LoginId);
                    if (userInfo != null && login != null)
                    {
                        if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty())
                        {
                            string passwordsalt = string.Empty;
                            string passwordHash = string.Empty;

                            var saltedHash = new SaltedHash();
                            saltedHash.GetHashAndSalt(user.PasswordChanger.NewPassword, out passwordHash, out passwordsalt);
                            login.PasswordSalt = passwordsalt;
                            login.PasswordHash = passwordHash;
                        }

                        if (user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                        {
                            string signaturesalt = string.Empty;
                            string signatureHash = string.Empty;

                            var saltedHash = new SaltedHash();
                            saltedHash.GetHashAndSalt(user.SignatureChanger.NewSignature, out signatureHash, out signaturesalt);
                            login.SignatureSalt = signaturesalt;
                            login.SignatureHash = signatureHash;
                        }

                        if (!loginRepository.Update(login))
                        {
                            result = false;
                        }
                    }
                }
                
            }

            return result;
        }
    }
}
