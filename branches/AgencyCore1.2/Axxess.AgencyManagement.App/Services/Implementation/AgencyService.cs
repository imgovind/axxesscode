﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Enums;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;

    using Enums;
    using Domain;
    using Common;
    using ViewData;
    using Security;
    using Extensions;

    public class AgencyService : IAgencyService
    {
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPlanofCareRepository planofCareRepository;

        public AgencyService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, ILookUpDataProvider lookUpDataProvider, IOasisCDataProvider oasisDataprovider)
        {
            Check.Argument.IsNotNull(oasisDataprovider, "oasisDataprovider");
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.planofCareRepository = oasisDataprovider.PlanofCareRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
        }

        public bool CreateAgency(Agency agency)
        {
            try
            {
                Guid agencyId = Guid.Empty;

                // Create Admin Login
                string salt = string.Empty;
                string passwordHash = string.Empty;

                var saltedHash = new SaltedHash();
                saltedHash.GetHashAndSalt(agency.AgencyAdminPassword, out passwordHash, out salt);

                Guid loginId = loginRepository.Add(agency.AgencyAdminUsername, passwordHash, salt, Roles.ApplicationUser.ToString(), agency.AgencyAdminFirstName);
                if (!loginId.IsEmpty())
                {
                    // Create Agency Data
                    agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();

                    if (agencyRepository.Add(agency))
                    {
                        // Create Agency Location
                        var location = new AgencyLocation();
                        location.Name = agency.LocationName;
                        location.PhoneWork = agency.PhoneArray.ToArray().PhoneEncode();
                        location.AddressLine1 = agency.AddressLine1;
                        location.AddressLine2 = agency.AddressLine2;
                        location.AddressCity = agency.AddressCity;
                        location.AddressStateCode = agency.AddressStateCode;
                        location.AddressZipCode = agency.AddressZipCode;
                        location.AgencyId = agencyId;
                        location.IsMainOffice = true;
                        var zipCode = lookupRepository.GetZipCode(agency.AddressZipCode);
                        location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
                        location.MedicareProviderNumber = agency.MedicareProviderNumber;

                        if (agencyRepository.AddLocation(location))
                        {
                            // Create Admin User for Agency
                            var user = new User();
                            user.LoginId = loginId;
                            user.AgencyId = agencyId;
                            user.FirstName = agency.AgencyAdminFirstName;
                            user.LastName = agency.AgencyAdminLastName;
                            user.AgencyRoleList = new List<string>() { ((int)AgencyRoles.Administrator).ToString() };
                            user.Profile = new UserProfile();
                            user.TitleType = TitleTypes.Administrator.GetDescription();
                            user.Status = (int)UserStatus.Active;
                            user.AgencyLocationId = location.Id;
                            user.PermissionsArray = GeneratePermissions();
                            userRepository.Add(user);

                            if (!user.Id.IsEmpty())
                            {
                                var bodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName, "agencyname", agency.Name, "emailaddress", agency.AgencyAdminUsername, "password", agency.AgencyAdminPassword);
                                Notify.User(AppSettings.NoReplyEmail, agency.AgencyAdminUsername, "Welcome to Axxess Home Health Management Software", bodyText);
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        private List<string> GeneratePermissions()
        {
            var list = new List<string>();

            var permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            foreach (Permissions permission in permissions)
            {
                ulong permissionId = (ulong)permission;
                if (permissionId != 0)
                {
                    list.Add((permissionId).ToString());
                }
            }

            return list;
        }

        public bool CreateLocation(AgencyLocation location)
        {
            var result = false;
            var zipCode = lookupRepository.GetZipCode(location.AddressZipCode);
            location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
            location.AgencyId = Current.AgencyId;

            if (agencyRepository.AddLocation(location))
            {
                result = true;
            }

            return result;
        }

        public bool EditAgency(Agency agency)
        {
            var result = false;

            return result;
        }

        public Agency GetAgency(Guid Id)
        {
            Agency agencyViewData = agencyRepository.Get(Id);
            if (agencyViewData != null)
            {
                return agencyViewData;
            }
            return null;
        }

        public bool CreateContact(AgencyContact contact)
        {
            contact.AgencyId = Current.AgencyId;
            if (contact.ContactTypeOther.IsNotNullOrEmpty())
            {
                contact.ContactType = contact.ContactTypeOther;
            }
            return agencyRepository.AddContact(contact);
        }

        public List<UserVisit> GetSchedule()
        {
            var schedule = new List<UserVisit>();
            var userEvents = userRepository.GetSchedules(Current.AgencyId);
            userEvents.ForEach(ue =>
            {
                var user = userRepository.Get(ue.UserId, Current.AgencyId);
                if (user != null && !ue.PatientId.IsEmpty())
                {
                    var patient = patientRepository.GetPatient<Patient>(ue.PatientId, Current.AgencyId);
                    if (patient != null && !ue.EpisodeId.IsEmpty())
                    {
                        var episode = patientRepository.GetEpisode(Current.AgencyId, ue.EpisodeId, patient.Id);
                        if (episode != null)
                        {
                            var visitNote = string.Empty;
                            if (episode.Schedule.IsNotNullOrEmpty())
                            {
                                var scheduleList = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                if (scheduleList != null && scheduleList.Count > 0)
                                {
                                    var scheduledEvent = scheduleList.Find(e => e.EventId == ue.EventId);
                                    if (scheduledEvent != null && scheduledEvent.Comments.IsNotNullOrEmpty())
                                    {
                                        visitNote = scheduledEvent.Comments;
                                    }

                                }
                            }
                            schedule.Add(new UserVisit
                            {
                                Id = ue.EventId,
                                UserId = ue.UserId,
                                EpisodeId = episode.Id,
                                PatientId = patient.Id,
                                VisitDate = ue.EventDate.ToZeroFilled(),
                                EpisodeNotes = episode.Detail.Comments,
                                VisitNotes = visitNote,
                                PatientName = patient.DisplayName,
                                UserDisplayName = user.DisplayName,
                                StatusName = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), ue.Status)).GetDescription(),
                                TaskName = ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), ue.DisciplineTask.ToString())).GetDescription()
                            });
                        }
                    }
                }
            });
            return schedule;
        }

        public List<ScheduleEvent> GetCaseManagerSchedule()
        {
            var schedule = new List<ScheduleEvent>();
            var patients = patientRepository.Find((int)PatientStatus.Active, Current.AgencyId);
            patients.ForEach(p =>
            {
                var scheduleEvents = patientRepository.GetScheduledEvents(Current.AgencyId, p.Id);
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    Common.Url.Set(scheduleEvent, false, false);
                    if (scheduleEvent.Status == ((int)ScheduleStatus.OrderSubmittedToCaseManager).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.NoteReopened).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisReopened).ToString())
                    {
                        scheduleEvent.PatientName = p.FirstName + " " + p.LastName;
                        schedule.Add(scheduleEvent);
                    }
                });
            });
            return schedule;
        }

        public List<RecertEvent> GetRecertsPastDue()
        {
            var pastDueRecerts = new List<RecertEvent>();
            var pastDueEpisodes = patientRepository.GetPastDueRecerts(Current.AgencyId);

            pastDueEpisodes.ForEach(episode =>
            {
                var pastDueRecert = new RecertEvent();
                var patient = patientRepository.Get(episode.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    pastDueRecert.PatientName = patient.DisplayName;
                    pastDueRecert.PatientIdNumber = patient.PatientIdNumber;
                }
                pastDueRecert.TargetDate = episode.EndDate.ToShortDateString().ToZeroFilled();
                if (episode.Schedule.IsNotNullOrEmpty())
                {
                    var schedule = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (!schedule.ContainsRecertAssessment())
                    {
                        pastDueRecert.AssignedTo = "Unassigned";
                        pastDueRecert.Status = "Not Scheduled";
                    }
                    else
                    {
                        var recert = schedule.GetRecertAssessment();
                        if (recert != null)
                        {
                            pastDueRecert.AssignedTo = recert.UserName;
                            pastDueRecert.Status = recert.StatusName;
                        }
                    }
                }
                pastDueRecerts.Add(pastDueRecert);
            });

            return pastDueRecerts;
        }

        public List<RecertEvent> GetRecertsUpcoming()
        {
            var upcomingRecerts = new List<RecertEvent>();
            var upcomingEpisodes = patientRepository.GetUpcomingRecerts(Current.AgencyId);

            upcomingEpisodes.ForEach(episode =>
            {
                var upcomingRecert = new RecertEvent();
                var patient = patientRepository.Get(episode.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    upcomingRecert.PatientName = patient.DisplayName;
                    upcomingRecert.PatientIdNumber = patient.PatientIdNumber;
                }
                upcomingRecert.TargetDate = episode.EndDate.ToShortDateString().ToZeroFilled();
                if (episode.Schedule.IsNotNullOrEmpty())
                {
                    var schedule = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (!schedule.ContainsRecertAssessment())
                    {
                        upcomingRecert.AssignedTo = "Unassigned";
                        upcomingRecert.Status = "Not Scheduled";
                    }
                    else
                    {
                        var recert = schedule.GetRecertAssessment();
                        if (recert != null)
                        {
                            upcomingRecert.AssignedTo = recert.UserName;
                            upcomingRecert.Status = recert.StatusName;
                        }
                    }
                }
                upcomingRecerts.Add(upcomingRecert);
            });

            return upcomingRecerts;
        }

        public List<InsuranceViewData> GetInsurances()
        {
            var listOfInsurnace = new List<InsuranceViewData>();
            lookupRepository.Insurances().ForEach(i =>
            {
                listOfInsurnace.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            }
                );
            agencyRepository.GetInsurances(Current.AgencyId).ForEach(i =>
            {
                listOfInsurnace.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            }
                );
            return listOfInsurnace;
        }

        public List<Order> GetOrdersToBeSent()
        {
            var orders = new List<Order>();

            var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician);
            physicianOrders.ForEach(po =>
            {
                orders.Add(new Order
                {
                    Id = po.Id,
                    Type = OrderType.PhysicianOrder,
                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                    Number = po.OrderNumber,
                    PatientName = po.DisplayName,
                    PhysicianName = po.PhysicianName,
                    PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
                    CreatedDate = po.Created.ToShortDateString().ToZeroFilled()
                });
            });

            var planofCareOrders = planofCareRepository.GetPlanofCareByStatus(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician);
            planofCareOrders.ForEach(poc =>
            {
                var patient = poc.PatientData.ToObject<Patient>();
                var physician = poc.PhysicianData.ToObject<AgencyPhysician>();
                orders.Add(new Order
                {
                    Id = poc.Id,
                    Type = OrderType.HCFA485,
                    Text = DisciplineTasks.HCFA485.GetDescription(),
                    Number = poc.OrderNumber,
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                    CreatedDate = poc.Created.ToShortDateString().ToZeroFilled()
                });
            });

            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public List<Order> GetOrdersPendingSignature()
        {
            var orders = new List<Order>();

            var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, (int)ScheduleStatus.OrderSentToPhysician);
            physicianOrders.ForEach(po =>
            {
                orders.Add(new Order
                {
                    Id = po.Id,
                    Type = OrderType.PhysicianOrder,
                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                    Number = po.OrderNumber,
                    PatientName = po.DisplayName,
                    PhysicianName = po.PhysicianName,
                    SentDate = po.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = po.Created.ToShortDateString().ToZeroFilled(),
                    ReceivedDate = DateTime.Today
                });
            });

            var planofCareOrders = planofCareRepository.GetPlanofCareByStatus(Current.AgencyId, (int)ScheduleStatus.OrderSentToPhysician);
            planofCareOrders.ForEach(poc =>
            {
                var patient = poc.PatientData.ToObject<Patient>();
                var physician = poc.PhysicianData.ToObject<AgencyPhysician>();
                orders.Add(new Order
                {
                    Id = poc.Id,
                    Type = OrderType.HCFA485,
                    Text = DisciplineTasks.HCFA485.GetDescription(),
                    Number = poc.OrderNumber,
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = poc.Created.ToShortDateString().ToZeroFilled(),
                    ReceivedDate = DateTime.Today
                });
            });

            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public bool MarkOrdersAsSent(FormCollection formCollection)
        {
            var result = true;
            foreach (var key in formCollection.AllKeys)
            {
                string answers = formCollection.GetValues(key).Join(",");
                string[] answerArray = answers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (key.ToInteger() == (int)OrderType.PhysicianOrder)
                {
                    answerArray.ForEach(answer =>
                    {
                        if (!patientRepository.UpdateOrderStatus(Current.AgencyId, answer.ToGuid(), (int)ScheduleStatus.OrderSentToPhysician, DateTime.MinValue))
                        {
                            result = false;
                            return;
                        }
                    });
                }

                if (key.ToInteger() == (int)OrderType.HCFA485)
                {
                    answerArray.ForEach(answer =>
                    {
                        var planofCare = planofCareRepository.Get(Current.AgencyId, answer.ToGuid());
                        if (planofCare != null)
                        {
                            var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = ((int)ScheduleStatus.OrderSentToPhysician).ToString();
                                patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent);
                            }
                            planofCare.Status = (int)ScheduleStatus.OrderSentToPhysician;
                            planofCare.SentDate = DateTime.Now;
                            if (!planofCareRepository.Update(planofCare))
                            {
                                result = false;
                                return;
                            }
                        }
                    });
                }
            }

            return result;
        }

        public void MarkOrderAsReturned(Guid id, OrderType type, DateTime receivedDate)
        {
            if (type == OrderType.PhysicianOrder)
            {
                patientRepository.UpdateOrderStatus(Current.AgencyId, id, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, receivedDate);
            }

            if (type == OrderType.HCFA485)
            {
                var planofCare = planofCareRepository.Get(Current.AgencyId, id);
                if (planofCare != null)
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                    if (scheduledEvent != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                        patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent);
                    }
                    var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                    }
                    planofCare.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                    planofCare.ReceivedDate = receivedDate;
                    planofCareRepository.Update(planofCare);
                }
            }
        }
    }
}
