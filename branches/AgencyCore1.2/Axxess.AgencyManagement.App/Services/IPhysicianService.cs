﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;

    using Axxess.AgencyManagement.Domain;

    public interface IPhysicianService
    {
        bool CreatePhysician(AgencyPhysician physician);
    }
}
