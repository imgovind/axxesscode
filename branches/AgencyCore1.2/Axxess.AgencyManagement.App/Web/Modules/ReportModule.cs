﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class ReportModule : Module
    {
        public override string Name
        {
            get { return "Report"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "ReportPatientRoster",
                "Report/Patient/Roster",
                new { controller = this.Name, action = "PatientRoster", id = UrlParameter.Optional });
            routes.MapRoute(
               "ReportPatientEmergencyList",
               "Report/Patient/EmergencyList",
               new { controller = this.Name, action = "PatientEmergencyList", id = UrlParameter.Optional });
            routes.MapRoute(
               "ReportPatientBirthdayList",
               "Report/Patient/Birthdays",
               new { controller = this.Name, action = "PatientBirthdayList", id = UrlParameter.Optional });
            routes.MapRoute(
               "ReportPatientAddressList",
               "Report/Patient/AddressList",
               new { controller = this.Name, action = "PatientAddressList", id = UrlParameter.Optional });
            routes.MapRoute(
               "ReportPatientPhysicians",
               "Report/Patient/Physician",
               new { controller = this.Name, action = "PatientByPhysicians", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportClinicalOpenOasis",
               "Report/Clinical/OpenOasis",
               new { controller = this.Name, action = "ClinicalOpenOasis", id = UrlParameter.Optional });
            routes.MapRoute(
               "ReportClinicalOrders",
               "Report/Clinical/Orders",
               new { controller = this.Name, action = "ClinicalOrders", id = UrlParameter.Optional });

        }
    }
}
