﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class AgencyModule : Module
    {
        public override string Name
        {
            get { return "Agency"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               "NewContact",
               "Contact/New",
               new { controller = this.Name, action = "NewContact", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddContact",
               "Contact/Add",
               new { controller = this.Name, action = "AddContact", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "EditContact",
              "Contact/Edit",
              new { controller = this.Name, action = "EditContact", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "UpdateContact",
              "Contact/Update",
              new { controller = this.Name, action = "UpdateContact", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeleteContact",
              "Contact/Delete",
              new { controller = this.Name, action = "DeleteContact", id = new IsGuid() }
            );

            routes.MapRoute(
               "ContactGrid",
               "Contact/Grid",
               new { controller = this.Name, action = "Contacts", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "ContactList",
               "Contact/List",
               new { controller = this.Name, action = "ContactList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewHospital",
               "Hospital/New",
               new { controller = this.Name, action = "NewHospital", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddHospital",
               "Hospital/Add",
               new { controller = this.Name, action = "AddHospital", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "EditHospital",
              "Hospital/Edit",
              new { controller = this.Name, action = "EditHospital", id = new IsGuid() }
            );

            routes.MapRoute(
              "UpdateHospital",
              "Hospital/Update",
              new { controller = this.Name, action = "UpdateHospital", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeleteHospital",
              "Hospital/Delete",
              new { controller = this.Name, action = "DeleteHospital", id = new IsGuid() }
            );

            routes.MapRoute(
               "HospitalGrid",
               "Hospital/Grid",
               new { controller = this.Name, action = "Hospitals", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "HospitalList",
               "Hospital/List",
               new { controller = this.Name, action = "HospitalList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewLocation",
               "Location/New",
               new { controller = this.Name, action = "NewLocation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddLocation",
               "Location/Add",
               new { controller = this.Name, action = "AddLocation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "EditLocation",
              "Location/Edit",
              new { controller = this.Name, action = "EditLocation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "UpdateLocation",
              "Location/Update",
              new { controller = this.Name, action = "UpdateLocation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeleteLocation",
              "Location/Delete",
              new { controller = this.Name, action = "DeleteLocation", id = new IsGuid() }
            );

            routes.MapRoute(
               "LocationGrid",
               "Location/Grid",
               new { controller = this.Name, action = "Locations", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "LocationList",
               "Location/List",
               new { controller = this.Name, action = "LocationList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewInsurance",
               "Insurance/New",
               new { controller = this.Name, action = "NewInsurance", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddInsurance",
               "Insurance/Add",
               new { controller = this.Name, action = "AddInsurance", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "EditInsurance",
             "Insurance/Edit",
             new { controller = this.Name, action = "EditInsurance", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "UpdateInsurance",
             "Insurance/Update",
             new { controller = this.Name, action = "UpdateInsurance", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeleteInsurance",
              "Insurance/Delete",
              new { controller = this.Name, action = "DeleteInsurance", id = new IsGuid() }
            );

            routes.MapRoute(
               "InsuranceGrid",
               "Insurance/Grid",
               new { controller = this.Name, action = "Insurances", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "InsuranceList",
               "Insurance/List",
               new { controller = this.Name, action = "InsuranceList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewPhysician",
               "Physician/New",
               new { controller = this.Name, action = "NewPhysician", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddPhysician",
               "Physician/Add",
               new { controller = this.Name, action = "AddPhysician", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "EditPhysician",
             "Physician/Edit",
             new { controller = this.Name, action = "EditPhysician", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "UpdatePhysician",
             "Physician/Update",
             new { controller = this.Name, action = "UpdatePhysician", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "DeletePhysician",
              "Physician/Delete",
              new { controller = this.Name, action = "DeletePhysician", id = new IsGuid() }
            );

            routes.MapRoute(
               "PhysicianGrid",
               "Physician/Grid",
               new { controller = this.Name, action = "Physicians", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "PhysicianList",
              "Physician/List",
              new { controller = this.Name, action = "PhysicianList", id = UrlParameter.Optional }
            );

            
        }
    }
}
