﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain;

    using Axxess.Core;

    using SubSonic.Repository;

    public class ErrorRepository : IErrorRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public ErrorRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IErrorRepository Members

        public bool Add(Error error)
        {
            if (error != null)
            {
                error.Created = DateTime.Now;
                database.Add<Error>(error);
                return true;
            }
            return false;
        }

        public List<Error> GetSome()
        {
            IList<Error> errors = database.Find<Error>(e => e.Server == Environment.MachineName);
            var sortedErrors = from error in errors
                               orderby error.Created descending
                               select error;
            return sortedErrors.Take(30).ToList();
        }

        #endregion
    }
}
