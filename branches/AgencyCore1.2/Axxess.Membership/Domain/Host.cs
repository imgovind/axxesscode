﻿namespace Axxess.Membership.Domain
{
    using System;

    public class Host
    {
        public byte Id { get; set; }
        public Guid ApplicationId { get; set; }
        public string IPAddress { get; set; }
        public string EnvironmentName { get; set; }
        public string ComputerName { get; set; }
    }
}
