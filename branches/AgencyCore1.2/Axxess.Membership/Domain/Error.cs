﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Xml.Linq;

    public class Error
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Details { get; set; }
        public string Message { get; set; }
        public string Server { get; set; }
        public DateTime Created { get; set; }
    }
}
