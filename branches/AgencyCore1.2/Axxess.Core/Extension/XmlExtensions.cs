﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Diagnostics;

    using Axxess.Core.Infrastructure;
    using System.Collections;

    public static class XmlExtensions
    {
        [DebuggerStepThrough]
        public static string ToXml<T>(this T instance)
        {
            if (instance == null)
                return string.Empty;

            return Serializer.Serialize(instance);
        }

        [DebuggerStepThrough]
        public static T ToObject<T>(this string text)
        {
            if (text.IsNullOrEmpty())
                return default(T);
            return Serializer.Deserialize<T>(text);
        }

        public static string ToExceptionXml(this Exception exception)
        {
            if (exception == null)
            {
                return string.Empty;
            }

            XElement root = new XElement(exception.GetType().ToString());

            if (exception.Message != null)
            {
                root.Add(new XElement("Message", exception.Message));
            }

            if (exception.StackTrace != null)
            {
                root.Add
                (
                    new XElement("StackTrace",
                        from frame in exception.StackTrace.Split('\n')
                        let prettierFrame = frame.Substring(6).Trim()
                        select new XElement("Frame", prettierFrame))
                );
            }

            if (exception.Data.Count > 0)
            {
                root.Add
                (
                    new XElement("Data",
                        from entry in
                            exception.Data.Cast<DictionaryEntry>()
                        let key = entry.Key.ToString().Replace(" ", "")
                        let value = (entry.Value == null || entry.Value.ToString() == string.Empty) ?
                            "null" : entry.Value.ToString()
                        select new XElement(key, value))
                );
            }

            if (exception.InnerException != null)
            {
                root.Add(new XElement("InnerException", 
                    exception.InnerException.ToExceptionXml()));
            }

            return root.ToString();
        }
    }
}
