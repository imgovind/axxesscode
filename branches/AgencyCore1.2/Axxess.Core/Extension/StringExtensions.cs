﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Text;
    using System.Diagnostics;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using System.Collections.Generic;
    using System.Threading;

    public static class StringExtensions
    {
        private static readonly Regex SSNExpression = new Regex(@"^\d{9}$", RegexOptions.Singleline | RegexOptions.Compiled);
        private static readonly Regex PhoneNumberExpression = new Regex(@"^\d{10}$", RegexOptions.Singleline | RegexOptions.Compiled);
        private static readonly Regex EmailExpression = new Regex(@"^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$", RegexOptions.Singleline | RegexOptions.Compiled);
        private static readonly Regex ValidDateExpression = new Regex(@"([1-9]|0[1-9]|1[012])[- /.]([1-9]|0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d", RegexOptions.Singleline | RegexOptions.Compiled);

        [DebuggerStepThrough]
        public static string ToTitleCase(this string target)
        {
            if (target.IsNotNullOrEmpty())
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;

                return textInfo.ToTitleCase(target);
            }
            return string.Empty;
        }

        [DebuggerStepThrough]
        public static string ToPhone(this string target)
        {
            if (target.IsNotNullOrEmpty() && target.Length >= 10)
            {
                return string.Format("({0}) {1}-{2}", target.Substring(0, 3), target.Substring(3, 3), target.Substring(6, 4));
            }
            return string.Empty;
        }

        [DebuggerStepThrough]
        public static bool HasValue(this string target)
        {
            return !string.IsNullOrEmpty(target);
        }

        [DebuggerStepThrough]
        public static bool ToBoolean(this string target)
        {
            Check.Argument.IsNotEmpty(target, "target");
            return Convert.ToBoolean(target);
        }

        [DebuggerStepThrough]
        public static bool GuidTryParse(this string s, out Guid result)
        {
            if (s == null)
                throw new ArgumentNullException("String is null in GuidTryParse()");

            try
            {
                result = new Guid(s);
                return true;
            }
            catch (FormatException)
            {
                result = Guid.Empty;
                return false;
            }
            catch (OverflowException)
            {
                result = Guid.Empty;
                return false;
            }
        }

        [DebuggerStepThrough]
        public static int ToInteger(this string target)
        {
            Check.Argument.IsNotEmpty(target, "target");
            int i;
            if (!int.TryParse(target, out i))
                throw new InvalidCastException(target + " is an invalid integer.");

            return i;
        }

        [DebuggerStepThrough]
        public static bool IsEmail(this string target)
        {
            return !string.IsNullOrEmpty(target) && EmailExpression.IsMatch(target);
        }

        [DebuggerStepThrough]
        public static bool IsPhone(this string target)
        {
            return !string.IsNullOrEmpty(target) && PhoneNumberExpression.IsMatch(target);
        }

        [DebuggerStepThrough]
        public static bool IsSSN(this string target)
        {
            return !string.IsNullOrEmpty(target) && SSNExpression.IsMatch(target);
        }

        [DebuggerStepThrough]
        public static bool IsValidDate(this string target)
        {
            return !string.IsNullOrEmpty(target) && ValidDateExpression.IsMatch(target);
        }

        [DebuggerStepThrough]
        public static double ToDouble(this string target)
        {
            Check.Argument.IsNotEmpty(target, "target");
            double d;
            if (!double.TryParse(target, out d))
                throw new InvalidCastException(target + " is an invalid double.");

            return d;
        }
        public static bool IsDouble(this string target)
        {
            Check.Argument.IsNotEmpty(target, "target");
            double d;
            return double.TryParse(target, out d);
        }
        [DebuggerStepThrough]
        public static DateTime ToDateTime(this string target)
        {
            Check.Argument.IsNotEmpty(target, "target");
            DateTime dt;
            if (!DateTime.TryParse(target, out dt))
                throw new InvalidCastException(target + "is an invalid DateTime.");

            return dt;
        }

        [DebuggerStepThrough]
        public static DateTime ToMySqlDate(this string target)
        {
            Check.Argument.IsNotEmpty(target, "target");

            DateTime dt;
            if (!DateTime.TryParse(target, out dt))
                throw new InvalidCastException(target + "is an invalid DateTime.");

            return dt;
        }

        [DebuggerStepThrough]
        public static string[] ToArray(this string target)
        {
            Check.Argument.IsNotEmpty(target, "target");
            return target.Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }

        [DebuggerStepThrough]
        public static List<int> ToList(this string target)
        {
            Check.Argument.IsNotEmpty(target, "target");

            List<int> list = new List<int>();
            string[] array = target.Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            array.ForEach(s => { list.Add(s.ToInteger()); });

            return list;
        }

        [DebuggerStepThrough]
        public static Guid ToGuid(this string target)
        {
            if (string.IsNullOrEmpty(target))
            {
                return Guid.Empty;
            }
            return new Guid(target);
        }

        [DebuggerStepThrough]
        public static bool IsNullOrEmpty(this string target)
        {
            return string.IsNullOrEmpty(target);
        }

        [DebuggerStepThrough]
        public static bool IsNotNullOrEmpty(this string target)
        {
            return !string.IsNullOrEmpty(target);
        }

        [DebuggerStepThrough]
        public static string FormatWith(this string target, params object[] args)
        {
            Check.Argument.IsNotEmpty(target, "target");

            return string.Format(CultureInfo.CurrentCulture, target, args);
        }

        [DebuggerStepThrough]
        public static bool IsEqual(this string original, string compare)
        {
            if (original != null && compare != null)
            {
                if (original.Trim().Equals(compare.Trim(), StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        [DebuggerStepThrough]
        public static string PartOfString(this string original, int startIndex, int length)
        {
            if (original != null && original.Length > length)
            {
                return original.Substring(startIndex, length);
            }
            return original;
        }

        [DebuggerStepThrough]
        public static string Join(this string[] values, string delimiter)
        {
            bool first = true;
            StringBuilder sb = new StringBuilder();
            
            foreach (string item in values)
            {
                if (item == " " || item == "")
                    continue;

                if (!first)
                {
                    sb.Append(delimiter);
                }
                else
                {
                    first = false;
                }

                sb.Append(item);
            }
            return sb.ToString();
        }
    }
}
