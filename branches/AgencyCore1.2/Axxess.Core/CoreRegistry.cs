﻿namespace Axxess.Core
{
    using System;

    using StructureMap;
    using StructureMap.Configuration.DSL;

    using Axxess.Core.Infrastructure;

    public class CoreRegistry : Registry
    {
        public CoreRegistry()
        {
            For<IWebConfiguration>().Use<WebConfiguration>();
            For<ILog>().Use<CleverLog>();
            For<ICache>().Use<MembaseCache>();
            For<INotification>().Use<EmailNotification>();
            For<ISessionStore>().Use<HttpContextSession>();
        }
    }
}
