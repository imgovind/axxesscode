﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.ComponentModel;

    public class CustomDescriptionAttribute : DescriptionAttribute
    {
        public CustomDescriptionAttribute(string description, string shortDescription)
            : base(description)
        {
            this.ShortDescription = shortDescription;
        }

        public string ShortDescription { get; private set; }
    }
}
