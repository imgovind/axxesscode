﻿namespace Axxess.Core.Infrastructure
{
    public interface IStartupTask
    {
        void Execute();
    }
}
