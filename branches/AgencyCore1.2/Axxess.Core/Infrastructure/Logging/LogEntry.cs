﻿namespace Axxess.Core.Infrastructure
{
    using System;

    public class LogEntry
    {
        public LogEntry(string message)
        {
            this.Message = message;
            this.Date = DateTime.UtcNow;
        }

        public LogEntry(Exception e) : this(e.Message) { }

        public int Impression { get; set; }
        public int LineNumber { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
        public string FileName { get; set; }
        public string MethodName { get; set; }
        public string ExceptionType { get; set; }
        public LogPriority Priority { get; set; }
        public DateTime Date { get; set; }
    }
}
