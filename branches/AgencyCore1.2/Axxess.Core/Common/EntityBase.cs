﻿using System;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using System.Collections.Generic;

using Axxess.Core.Infrastructure;
using SubSonic.SqlGeneration.Schema;

namespace Axxess.Core
{
    [Serializable]
    public abstract class EntityBase : IDisposable
    {
        #region Validation
        private List<Validation> rules = new List<Validation>();

        protected virtual void AddValidationRule(Validation validation)
        {
            if (!rules.Contains(validation))
            {
                rules.Add(validation);
            }
        }

        protected abstract void AddValidationRules();
        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public bool IsValid
        {
            get
            {
                ClearRules();
                AddValidationRules();
                return AreRulesValid();
            }
        }

        private void ClearRules()
        {
            rules.Clear();
        }

        [XmlIgnore]
        [ScriptIgnore]
        private EntityValidator validator; 

        private bool AreRulesValid()
        {
            validator = new EntityValidator(
                  this.rules.ToArray()
                   );
            validator.Validate();
            return validator.IsValid;
        }

        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public virtual string ValidationMessage
        {
            get
            {
                return validator.Message;
            }
        }

        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public List<Validation> Rules
        {
            get
            {
                return rules;
            }
        }

        #endregion

        #region IDisposable

        private bool isDisposed;
        protected bool IsDisposed
        {
            get { return isDisposed; }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.IsDisposed)
                return;

            if (disposing)
            {
                rules.Clear();
                isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
