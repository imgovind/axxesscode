﻿namespace Axxess.LookUp
{
    internal enum CacheKey
    {
        Npi,
        Races,
        States,
        Supplies,
        Insurances,
        PaymentSources,
        DiagnosisCodes,
        ProcedureCodes,
        DisciplineTasks,
        ReferralSources,
        SupplyCategories,
        AdmissionSources,
        DrugClassifications
    }
}
