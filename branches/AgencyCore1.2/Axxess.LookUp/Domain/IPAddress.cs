﻿namespace Axxess.LookUp.Domain
{
    using SubSonic.SqlGeneration.Schema;

    public class IPAddress
    {
        [SubSonicPrimaryKey]
        public int Ip { get; set; }
        public string ZipCode { get; set; }
    }
}
