﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class ReferralSource
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
