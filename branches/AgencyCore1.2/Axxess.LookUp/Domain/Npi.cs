﻿namespace Axxess.LookUp.Domain
{
    public class Npi
    {
        public string Id { get; set; }
        public string EntityTypeCode { get; set; }
        public string ProviderFirstName { get; set; }
        public string ProviderMiddleName { get; set; }
        public string ProviderLastName { get; set; }
        public string ProviderCredentialText { get; set; }
        public string ProviderFirstLineBusinessMailingAddress { get; set; }
        public string ProviderSecondLineBusinessMailingAddress { get; set; }
        public string ProviderBusinessMailingAddressCityName { get; set; }
        public string ProviderBusinessMailingAddressStateName { get; set; }
        public string ProviderBusinessMailingAddressPostalCode { get; set; }
        public string ProviderBusinessMailingAddressCountryCode { get; set; }
        public string ProviderBusinessMailingAddressTelephoneNumber { get; set; }
        public string ProviderBusinessMailingAddressFaxNumber { get; set; }
        public string ProviderFirstLineBusinessPracticeLocationAddress { get; set; }
        public string ProviderSecondLineBusinessPracticeLocationAddress { get; set; }
        public string ProviderBusinessPracticeLocationAddressCityName { get; set; }
        public string ProviderBusinessPracticeLocationAddressStateName { get; set; }
        public string ProviderBusinessPracticeLocationAddressPostalCode { get; set; }
        public string ProviderBusinessPracticeLocationAddressCountryCode { get; set; }
        public string ProviderBusinessPracticeLocationAddressTelephoneNumber { get; set; }
        public string ProviderBusinessPracticeLocationAddressFaxNumber { get; set; }
    }
}
