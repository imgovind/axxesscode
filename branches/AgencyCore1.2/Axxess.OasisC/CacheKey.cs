﻿namespace Axxess.OasisC
{
    internal enum CacheKey
    {
        OasisGuide,
        SubmissionBodyFormat,
        SubmissionHeaderFormat,
        SubmissionFooterFormat
    }
}
