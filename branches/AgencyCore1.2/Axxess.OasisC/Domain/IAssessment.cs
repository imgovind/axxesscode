﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Enums;

    public interface IAssessment
    {
        Guid Id { get; set; }
        bool IsNew { get; set; }
        int Status { get; set; }
        Guid AgencyId { get; set; }
        Guid PatientId { get; set; }
        Guid EpisodeId { get; set; }
        string ClaimKey { get; set; }
        string OasisData { get; set; }
        string HippsCode { get; set; }
        string HippsVersion { get; set; }
        string SubmissionFormat { get; set; }
        string MedicationProfile { get; set; }
        DateTime AssessmentDate { get; set; }
        int VersionNumber { get; set; }
        AssessmentType Type { get; set; }
        string Supply { get; set; }
        string ValidationError { get; set; }
        List<Question> Questions { get; set; }

        string PatientName { get; set; }
        string Insurance { get; set; }

        DateTime Created { get; set; }
        DateTime Modified { get; set; }
        bool IsDeprecated { get; set; }
    }
}
