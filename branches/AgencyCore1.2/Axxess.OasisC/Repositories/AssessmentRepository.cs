﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Extensions;

    using SubSonic.Repository;
    using Axxess.OasisC.Enums;

    public class AssessmentRepository : IAssessmentRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AssessmentRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssessmentRepository Members

        public bool Add(Assessment assessment)
        {
            if (assessment != null)
            {
                assessment.Created = DateTime.Now;
                assessment.Modified = DateTime.Now;
                database.InsertAny(assessment);
                return true;
            }
            return false;
        }

        public bool Update(Assessment assessment)
        {
            bool result = false;
            if (assessment != null)
            {
                if (assessment.Questions != null)
                {
                    assessment.OasisData = assessment.Questions.ToXml();
                }
                assessment.Modified = DateTime.Now;
                database.UpdateAny(assessment);
                result = true;
            }
            return result;
        }

        public Assessment Get(Guid assessmentId, string assessmentType, Guid agencyId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(assessmentType, assessmentId);
                if (assessment != null)
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                }
            }
            return assessment;
        }

        public Assessment Get(Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType, Guid agencyId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(assessmentId, PatientId, EpisodeId, assessmentType);
                if (assessment != null)
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                }
            }
            return assessment;
        }

        public List<Assessment> GetAllByStatus(Guid agencyId, int status)
        {
            var assessments = new List<Assessment>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
                {
                    var data = database.FindAnyByStatus(assessmentType.ToString(), status);
                    assessments.AddRange(data);
                }
            }
            return assessments;
        }

        #endregion
    }
}
