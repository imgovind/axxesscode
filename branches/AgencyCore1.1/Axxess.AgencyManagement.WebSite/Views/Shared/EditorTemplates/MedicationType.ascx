﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var medicationType = new List<MedicationType>(); medicationType.Add(new MedicationType { Value = "N", Text = "New" }); medicationType.Add(new MedicationType { Value = "C", Text = "Changed" }); medicationType.Add(new MedicationType { Value = "U", Text = "UnChanged" });%>
<%= Html.Telerik().DropDownList()
                .Name("MedicationType")
            .BindTo(new SelectList(medicationType, "Value", "Text" ))
%>