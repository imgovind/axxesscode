﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareManagementForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id)%>
<%= Html.Hidden("StartOfCare_Action", "Edit")%>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "StartOfCare")%>
<%= Html.Hidden("categoryType", "Caremanagement")%>
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2100');">(M2100)</a> Types and Sources of Assistance: Determine the level of caregiver ability and willingness to provide assistance for the following activities, if assistance is needed. (Check only one box in each row.)</label>
                <table class="form bordergrid">
                    <thead>
                        <tr>
                            <th colspan="2">Type of Assistance</th>
                            <th>No assistance needed in this area</th>
                            <th>Caregiver(s) currently provide assistance</th>
                            <th>Caregiver(s) need training/ supportive services to provide assistance</th>
                            <th>Caregiver(s) not likely to provide assistance</th>
                            <th>Unclear if Caregiver(s) will provide assistance</th>
                            <th>Assistance needed, but no Caregiver(s) available</th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td colspan="2">
                                <span class="float_left">a.</span><span class="radio">ADL assistance (e.g., transfer/ ambulation, bathing, dressing, toileting, eating/feeding)</span>
                                <%=Html.Hidden("StartOfCare_M2100ADLAssistance")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ADLAssistance", "00", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2100ADLAssistance0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ADLAssistance0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ADLAssistance", "01", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2100ADLAssistance1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ADLAssistance1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ADLAssistance", "02", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "02" ? true : false, new { @id = "StartOfCare_M2100ADLAssistance2", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ADLAssistance2" class="radio">2</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ADLAssistance", "03", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "03" ? true : false, new { @id = "StartOfCare_M2100ADLAssistance3", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ADLAssistance3" class="radio">3</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ADLAssistance", "04", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "04" ? true : false, new { @id = "StartOfCare_M2100ADLAssistance4", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ADLAssistance4" class="radio">4</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ADLAssistance", "05", data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "05" ? true : false, new { @id = "StartOfCare_M2100ADLAssistance5", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ADLAssistance5" class="radio">5</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">b.</span><span class="radio">IADL assistance (e.g., meals, housekeeping, laundry, telephone, shopping, finances)</span>
                                <%=Html.Hidden("StartOfCare_M2100IADLAssistance")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100IADLAssistance", "00", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2100IADLAssistance0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100IADLAssistance0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100IADLAssistance", "01", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2100IADLAssistance1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100IADLAssistance1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100IADLAssistance", "02", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "02" ? true : false, new { @id = "StartOfCare_M2100IADLAssistance2", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100IADLAssistance2" class="radio">2</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100IADLAssistance", "03", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "03" ? true : false, new { @id = "StartOfCare_M2100IADLAssistance3", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100IADLAssistance3" class="radio">3</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100IADLAssistance", "04", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "04" ? true : false, new { @id = "StartOfCare_M2100IADLAssistance4", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100IADLAssistance4" class="radio">4</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100IADLAssistance", "05", data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "05" ? true : false, new { @id = "StartOfCare_M2100IADLAssistance5", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100IADLAssistance5" class="radio">5</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">c.</span><span class="radio">Medication administration (e.g., oral, inhaled or injectable)</span>
                                <%=Html.Hidden("StartOfCare_M2100MedicationAdministration")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicationAdministration", "00", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2100MedicationAdministration0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicationAdministration0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicationAdministration", "01", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2100MedicationAdministration1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicationAdministration1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicationAdministration", "02", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "02" ? true : false, new { @id = "StartOfCare_M2100MedicationAdministration2", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicationAdministration2" class="radio">2</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicationAdministration", "03", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "03" ? true : false, new { @id = "StartOfCare_M2100MedicationAdministration3", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicationAdministration3" class="radio">3</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicationAdministration", "04", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "04" ? true : false, new { @id = "StartOfCare_M2100MedicationAdministration4", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicationAdministration4" class="radio">4</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicationAdministration", "05", data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "05" ? true : false, new { @id = "StartOfCare_M2100MedicationAdministration5", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicationAdministration5" class="radio">5</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">d.</span><span class="radio">Medical procedures/ treatments (e.g., changing wound dressing)</span>
                                <%=Html.Hidden("StartOfCare_M2100MedicalProcedures")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicalProcedures", "00", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2100MedicalProcedures0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicalProcedures0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicalProcedures", "01", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2100MedicalProcedures1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicalProcedures1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicalProcedures", "02", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "02" ? true : false, new { @id = "StartOfCare_M2100MedicalProcedures2", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicalProcedures2" class="radio">2</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicalProcedures", "03", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "03" ? true : false, new { @id = "StartOfCare_M2100MedicalProcedures3", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicalProcedures3" class="radio">3</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicalProcedures", "04", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "04" ? true : false, new { @id = "StartOfCare_M2100MedicalProcedures4", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicalProcedures4" class="radio">4</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100MedicalProcedures", "05", data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "05" ? true : false, new { @id = "StartOfCare_M2100MedicalProcedures5", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100MedicalProcedures5" class="radio">5</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">e.</span><span class="radio">Management of Equipment (includes oxygen, IV/infusion equipment, enteral/ parenteral nutrition, ventilator therapy equipment or supplies)</span>
                                <%=Html.Hidden("StartOfCare_M2100ManagementOfEquipment")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ManagementOfEquipment", "00", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2100ManagementOfEquipment0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ManagementOfEquipment0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ManagementOfEquipment", "01", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2100ManagementOfEquipment1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ManagementOfEquipment1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ManagementOfEquipment", "02", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "02" ? true : false, new { @id = "StartOfCare_M2100ManagementOfEquipment2", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ManagementOfEquipment2" class="radio">2</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ManagementOfEquipment", "03", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "03" ? true : false, new { @id = "StartOfCare_M2100ManagementOfEquipment3", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ManagementOfEquipment3" class="radio">3</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ManagementOfEquipment", "04", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "04" ? true : false, new { @id = "StartOfCare_M2100ManagementOfEquipment4", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ManagementOfEquipment4" class="radio">4</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100ManagementOfEquipment", "05", data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "05" ? true : false, new { @id = "StartOfCare_M2100ManagementOfEquipment5", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100ManagementOfEquipment5" class="radio">5</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">f.</span><span class="radio">Supervision and safety (e.g., due to cognitive impairment)</span>
                                <%=Html.Hidden("StartOfCare_M2100SupervisionAndSafety")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100SupervisionAndSafety", "00", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2100SupervisionAndSafety0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100SupervisionAndSafety0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100SupervisionAndSafety", "01", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2100SupervisionAndSafety1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100SupervisionAndSafety1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100SupervisionAndSafety", "02", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "02" ? true : false, new { @id = "StartOfCare_M2100SupervisionAndSafety2", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100SupervisionAndSafety2" class="radio">2</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100SupervisionAndSafety", "03", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "03" ? true : false, new { @id = "StartOfCare_M2100SupervisionAndSafety3", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100SupervisionAndSafety3" class="radio">3</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100SupervisionAndSafety", "04", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "04" ? true : false, new { @id = "StartOfCare_M2100SupervisionAndSafety4", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100SupervisionAndSafety4" class="radio">4</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100SupervisionAndSafety", "05", data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "05" ? true : false, new { @id = "StartOfCare_M2100SupervisionAndSafety5", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100SupervisionAndSafety5" class="radio">5</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <span class="float_left">g.</span><span class="radio">Advocacy or facilitation of patient&rsquo;s participation in appropriate medical care (includes transporta-tion to or from appointments)</span>
                                <%=Html.Hidden("StartOfCare_M2100FacilitationPatientParticipation")%>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100FacilitationPatientParticipation", "00", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "00" ? true : false, new { @id = "StartOfCare_M2100FacilitationPatientParticipation0", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100FacilitationPatientParticipation0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100FacilitationPatientParticipation", "01", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2100FacilitationPatientParticipation1", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100FacilitationPatientParticipation1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100FacilitationPatientParticipation", "02", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "02" ? true : false, new { @id = "StartOfCare_M2100FacilitationPatientParticipation2", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100FacilitationPatientParticipation2" class="radio">2</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100FacilitationPatientParticipation", "03", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "03" ? true : false, new { @id = "StartOfCare_M2100FacilitationPatientParticipation3", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100FacilitationPatientParticipation3" class="radio">3</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100FacilitationPatientParticipation", "04", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "04" ? true : false, new { @id = "StartOfCare_M2100FacilitationPatientParticipation4", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100FacilitationPatientParticipation4" class="radio">4</label>
                            </td><td>
                                <%=Html.RadioButton("StartOfCare_M2100FacilitationPatientParticipation", "05", data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "05" ? true : false, new { @id = "StartOfCare_M2100FacilitationPatientParticipation5", @class = "radio float_left" })%>
                                <label for="StartOfCare_M2100FacilitationPatientParticipation5" class="radio">5</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2100');">?</div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2110');">(M2110)</a> How Often does the patient receive ADL or IADL assistance from any caregiver(s) (other than home health agency staff)?</div>
                <%=Html.Hidden("StartOfCare_M2110FrequencyOfADLOrIADLAssistance")%>
                <div class="margin">
                    <div>
                        <%=Html.RadioButton("StartOfCare_M2110FrequencyOfADLOrIADLAssistance", "01", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "01" ? true : false, new { @id = "StartOfCare_M2110FrequencyOfADLOrIADLAssistance1", @class = "radio float_left" })%>
                        <label for="StartOfCare_M2110FrequencyOfADLOrIADLAssistance1"><span class="float_left">1 &ndash;</span><span class="normal margin">At least daily</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("StartOfCare_M2110FrequencyOfADLOrIADLAssistance", "02", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "02" ? true : false, new { @id = "StartOfCare_M2110FrequencyOfADLOrIADLAssistance2", @class = "radio float_left" })%>
                        <label for="StartOfCare_M2110FrequencyOfADLOrIADLAssistance2"><span class="float_left">2 &ndash;</span><span class="normal margin">Three or more times per week</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("StartOfCare_M2110FrequencyOfADLOrIADLAssistance", "03", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "03" ? true : false, new { @id = "StartOfCare_M2110FrequencyOfADLOrIADLAssistance3", @class = "radio float_left" })%>
                        <label for="StartOfCare_M2110FrequencyOfADLOrIADLAssistance3"><span class="float_left">3 &ndash;</span><span class="normal margin">One to two times per week</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("StartOfCare_M2110FrequencyOfADLOrIADLAssistance", "04", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "04" ? true : false, new { @id = "StartOfCare_M2110FrequencyOfADLOrIADLAssistance4", @class = "radio float_left" })%>
                        <label for="StartOfCare_M2110FrequencyOfADLOrIADLAssistance4"><span class="float_left">4 &ndash;</span><span class="normal margin">Received, but less often than weekly</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("StartOfCare_M2110FrequencyOfADLOrIADLAssistance", "05", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "05" ? true : false, new { @id = "StartOfCare_M2110FrequencyOfADLOrIADLAssistance5", @class = "radio float_left" })%>
                        <label for="StartOfCare_M2110FrequencyOfADLOrIADLAssistance5"><span class="float_left">5 &ndash;</span><span class="normal margin">No assistance received</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2110');">?</div>
                        </div>
                        <%=Html.RadioButton("StartOfCare_M2110FrequencyOfADLOrIADLAssistance", "UK", data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "UK" ? true : false, new { @id = "StartOfCare_M2110FrequencyOfADLOrIADLAssistance6", @class = "radio float_left" })%>
                        <label for="StartOfCare_M2110FrequencyOfADLOrIADLAssistance6"><span class="float_left">UK &ndash;</span><span class="normal margin">Unknown </span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"SOC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('StartOfCare_ValidationContainer','{0}','{1}','{2}','StartOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>