﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationBehaviourialForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("Recertification_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<%= Html.Hidden("categoryType", "Behavioral")%>
<div class="wrapper main">
    <fieldset>
        <legend>Neurological</legend>
        <div class="column">
            <div class="row">
                <%string[] genericNeurologicalStatus = data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer != "" ? data["GenericNeurologicalStatus"].Answer.Split(',') : null; %>
                <input type="hidden" name="Recertification_GenericNeurologicalStatus" value=" " />
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericNeurologicalStatus1' class='radio float_left' name='Recertification_GenericNeurologicalStatus' value='1' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericNeurologicalStatus1">LOC:</label>
                </div><div id="Recertification_GenericNeurologicalStatus1More" class="rel float_right">
                    <div class="float_right">
                        <%  var LOC = new SelectList(new[]{
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "Alert", Value = "1" },
                                new SelectListItem { Text = "Lethargic", Value = "2" },
                                new SelectListItem { Text = "Comatose", Value = "3" },
                                new SelectListItem { Text = "Disoriented", Value = "4" },
                                new SelectListItem { Text = "Forgetful", Value = "5"},
                                new SelectListItem { Text = "Other", Value = "6" }
                            }, "Value", "Text", data.ContainsKey("GenericNeurologicalLOC") ? data["GenericNeurologicalLOC"].Answer : "0");%>
                        <%= Html.DropDownList("Recertification_GenericNeurologicalLOC", LOC, new { @id = "Recertification_GenericNeurologicalLOC" })%>
                    </div>
                    <div class="clear"></div>
                    <div class="float_right">
                        <label>Oriented to:</label>
                        <% string[] genericNeurologicalOriented = data.ContainsKey("GenericNeurologicalOriented") && data["GenericNeurologicalOriented"].Answer != "" ? data["GenericNeurologicalOriented"].Answer.Split(',') : null; %>
                        <input type="hidden" name="Recertification_GenericNeurologicalOriented" value=" " />
                        <%= string.Format("<input id='Recertification_GenericNeurologicalOriented1' class='radio' name='Recertification_GenericNeurologicalOriented' value='1' type='checkbox' {0} />", genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("1")  ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericNeurologicalOriented1" class="inlineradio">Person</label>
                        <%= string.Format("<input id='Recertification_GenericNeurologicalOriented2' class='radio' name='Recertification_GenericNeurologicalOriented' value='2' type='checkbox' {0} />", genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("2")  ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericNeurologicalOriented2" class="inlineradio">Place</label>
                        <%= string.Format("<input id='Recertification_GenericNeurologicalOriented3' class='radio' name='Recertification_GenericNeurologicalOriented' value='3' type='checkbox' {0} />", genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("3")  ? "checked='checked'" : "" ) %>
                        <label for="Recertification_GenericNeurologicalOriented3" class="inlineradio">Time</label>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericNeurologicalStatus2' class='radio float_left' name='Recertification_GenericNeurologicalStatus' value='2' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericNeurologicalStatus2">Pupils:</label>
                </div><div id="Recertification_GenericNeurologicalStatus2More" class="rel float_right">
                    <div class="float_right">
                        <%  var pupils = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "PERRLA/WNL", Value = "1" },
                                new SelectListItem { Text = "Sluggish", Value = "2" },
                                new SelectListItem { Text = "Non-Reactive", Value = "3" },
                                new SelectListItem { Text = "Other", Value = "4" }
                            }, "Value", "Text", data.ContainsKey("GenericNeurologicalPupils") ? data["GenericNeurologicalPupils"].Answer : "0");%>
                        <%= Html.DropDownList("Recertification_GenericNeurologicalPupils", pupils, new { @id = "Recertification_GenericNeurologicalPupils" })%>
                    </div>
                    <div class="clear"></div>
                    <div class="float_right">
                        <%= Html.RadioButton("Recertification_GenericNeurologicalPupilsPosition", "0", data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "0" ? true : false, new { @id = "GenericNeurologicalPupilsPosition0", @class = "radio" })%>
                        <label for="GenericNeurologicalPupilsPosition0" class="inlineradio">Bilateral</label>
                        <%= Html.RadioButton("Recertification_GenericNeurologicalPupilsPosition", "1", data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "1" ? true : false, new { @id = "GenericNeurologicalPupilsPosition1", @class = "radio" })%>
                        <label for="GenericNeurologicalPupilsPosition1" class="inlineradio">Left</label>
                        <%= Html.RadioButton("Recertification_GenericNeurologicalPupilsPosition", "2", data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "2" ? true : false, new { @id = "GenericNeurologicalPupilsPosition2", @class = "radio" })%>
                        <label for="GenericNeurologicalPupilsPosition2" class="inlineradio">Right</label>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericNeurologicalStatus3' class='radio float_left' name='Recertification_GenericNeurologicalStatus' value='3' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericNeurologicalStatus3">Vision:</label>
                </div><div id="Recertification_GenericNeurologicalStatus3More" class="float_right">
                    <% string[] genericNeurologicalVisionStatus = data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer != "" ? data["GenericNeurologicalVisionStatus"].Answer.Split(',') : null; %>
                    <div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericNeurologicalVisionStatus1' class='radio float_left' name='Recertification_GenericNeurologicalVisionStatus' value='1' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("1") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericNeurologicalVisionStatus1" class="fixed inlineradio">WNL</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericNeurologicalVisionStatus2' class='radio float_left' name='Recertification_GenericNeurologicalVisionStatus' value='2' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("2") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericNeurologicalVisionStatus2" class="fixed inlineradio">Blurred Vision</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericNeurologicalVisionStatus3' class='radio float_left' name='Recertification_GenericNeurologicalVisionStatus' value='3' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("3") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericNeurologicalVisionStatus3" class="fixed inlineradio">Cataracts</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericNeurologicalVisionStatus4' class='radio float_left' name='Recertification_GenericNeurologicalVisionStatus' value='4' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("4") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericNeurologicalVisionStatus4" class="fixed inlineradio">Corrective Lenses</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericNeurologicalVisionStatus5' class='radio float_left' name='Recertification_GenericNeurologicalVisionStatus' value='5' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("5") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericNeurologicalVisionStatus5" class="fixed inlineradio">Glaucoma</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericNeurologicalVisionStatus6' class='radio float_left' name='Recertification_GenericNeurologicalVisionStatus' value='6' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("6") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericNeurologicalVisionStatus6" class="fixed inlineradio">Legally Blind</label>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericNeurologicalStatus4' class='radio float_left' name='Recertification_GenericNeurologicalStatus' value='4' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericNeurologicalStatus4">Speech:</label>
                </div><div class="float_right">
                    <%  var speech = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Clear", Value = "1" },
                            new SelectListItem { Text = "Slurred", Value = "2" },
                            new SelectListItem { Text = "Aphasic", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.ContainsKey("GenericNeurologicalSpeech") ? data["GenericNeurologicalSpeech"].Answer : "0");%>
                    <%= Html.DropDownList("Recertification_GenericNeurologicalSpeech", speech, new { @id = "Recertification_GenericNeurologicalSpeech" })%>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericNeurologicalStatus5' class='radio float_left' name='Recertification_GenericNeurologicalStatus' value='5' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("5") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericNeurologicalStatus5">Paralysis</label>
                </div><div id="Recertification_GenericNeurologicalStatus5More" class="float_right">
                    <em>(location)</em>
                    <%=Html.TextBox("Recertification_GenericNeurologicalParalysisLocation", data.ContainsKey("GenericNeurologicalParalysisLocation") ? data["GenericNeurologicalParalysisLocation"].Answer : "", new { @id = "Recertification_GenericNeurologicalParalysisLocation", @class = "loc", @maxlength = "20" })%>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <%= string.Format("<input id='Recertification_GenericNeurologicalStatus6' class='radio float_left' name='Recertification_GenericNeurologicalStatus' value='6' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericNeurologicalStatus6" class="radio">Quadripiegia</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericNeurologicalStatus7' class='radio float_left' name='Recertification_GenericNeurologicalStatus' value='7' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericNeurologicalStatus7" class="radio">Parapiegia</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericNeurologicalStatus8' class='radio float_left' name='Recertification_GenericNeurologicalStatus' value='8' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("8") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericNeurologicalStatus8" class="radio">Seizures</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericNeurologicalStatus9' class='radio float_left' name='Recertification_GenericNeurologicalStatus' value='9' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("9") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericNeurologicalStatus9" class="radio">Tremors</label>
                </div><div id="Recertification_GenericNeurologicalStatus9More" class="float_right">
                    <em>(location)</em>
                    <%=Html.TextBox("Recertification_GenericNeurologicalTremorsLocation", data.ContainsKey("GenericNeurologicalTremorsLocation") ? data["GenericNeurologicalTremorsLocation"].Answer : "", new { @id = "Recertification_GenericNeurologicalTremorsLocation", @class = "loc", @maxlength = "20" })%>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericNeurologicalStatus10' class='radio float_left' name='Recertification_GenericNeurologicalStatus' value='10' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("10") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericNeurologicalStatus10" class="radio">Dizziness</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_GenericNeurologicalStatus11' class='radio float_left' name='Recertification_GenericNeurologicalStatus' value='11' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("11") ? "checked='checked'" : "" ) %>
                <label for="Recertification_GenericNeurologicalStatus11" class="radio">Headache</label>
            </div><div class="row">
                <label for="Recertification_GenericNeuroEmoBehaviorComments">Comments:</label>
                <%=Html.TextArea("Recertification_GenericNeuroEmoBehaviorComments", data.ContainsKey("GenericNeuroEmoBehaviorComments") ? data["GenericNeuroEmoBehaviorComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericNeuroEmoBehaviorComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="loc485">
        <legend>Mental Status (Locator #19)</legend>
        <% string[] mentalStatus = data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer != "" ? data["485MentalStatus"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485MentalStatus" value=" " />
        <div class="wide_column">
            <div class="row">
                <table class="form">
                    <tr>
                        <td>
                            <%= string.Format("<input id='Recertification_485MentalStatus1' class='radio float_left' name='Recertification_485MentalStatus' value='1' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("1") ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485MentalStatus1" class="radio">Oriented</label>
                        </td><td>
                            <%= string.Format("<input id='Recertification_485MentalStatus2' class='radio float_left' name='Recertification_485MentalStatus' value='2' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("2") ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485MentalStatus2" class="radio">Comatose</label>
                        </td><td>
                            <%= string.Format("<input id='Recertification_485MentalStatus3' class='radio float_left' name='Recertification_485MentalStatus' value='3' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("3") ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485MentalStatus3" class="radio">Forgetful</label>
                        </td><td>
                            <%= string.Format("<input id='Recertification_485MentalStatus4' class='radio float_left' name='Recertification_485MentalStatus' value='7' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("7") ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485MentalStatus4" class="radio">Agitated</label>
                        </td><td>
                            <%= string.Format("<input id='Recertification_485MentalStatus5' class='radio float_left' name='Recertification_485MentalStatus' value='4' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("4") ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485MentalStatus5" class="radio">Depressed</label>
                        </td>
                    </tr><tr>
                        <td>
                            <%= string.Format("<input id='Recertification_485MentalStatus6' class='radio float_left' name='Recertification_485MentalStatus' value='5' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("5") ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485MentalStatus6" class="radio">Disoriented</label>
                        </td><td>
                            <%= string.Format("<input id='Recertification_485MentalStatus7' class='radio float_left' name='Recertification_485MentalStatus' value='6' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("6") ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485MentalStatus7" class="radio">Lethargic</label>
                        </td><td>
                            <%= string.Format("<input id='Recertification_485MentalStatus8' class='radio float_left' name='Recertification_485MentalStatus' value='8' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("8") ? "checked='checked'" : "" ) %>
                            <label for="Recertification_485MentalStatus8" class="radio more fixed">Other</label>
                        </td><td colspan="2">
                            <div id="Recertification_485MentalStatus8More"><label for="Recertification_485MentalStatusOther"><em>(Specify)</em></label><%=Html.TextBox("Recertification_485MentalStatusOther", data.ContainsKey("485MentalStatusOther") ? data["485MentalStatusOther"].Answer : "", new { @id = "Recertification_485MentalStatusOther", @class = "loc", @maxlength = "24" })%></div>
                        </td>
                    </tr><tr>
                        <td colspan="5">
                            <label for="Recertification_485MentalStatusComments">Additional Orders <em>(specify)</em>:</label>
                            <%=Html.TextArea("Recertification_485MentalStatusComments", data.ContainsKey("485MentalStatusComments") ? data["485MentalStatusComments"].Answer : "", 5, 70, new { @id = "Recertification_485MentalStatusComments" })%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Behaviour Status</legend>
        <div class="column">
            <div class="row">
                <% string[] genericBehaviour = data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer != "" ? data["GenericBehaviour"].Answer.Split(',') : null; %>
                <input type="hidden" name="Recertification_GenericPsychosocial" value="'" />
                <div>
                    <%= string.Format("<input id='Recertification_GenericBehaviour1' class='radio float_left' name='Recertification_GenericBehaviour' value='1' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("1") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericBehaviour1" class="radio">WNL (Within Normal Limits)</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_GenericBehaviour2' class='radio float_left' name='Recertification_GenericBehaviour' value='2' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("2") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericBehaviour2" class="radio">Withdrawn</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_GenericBehaviour3' class='radio float_left' name='Recertification_GenericBehaviour' value='3' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("3") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericBehaviour3" class="radio">Expresses Depression</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_GenericBehaviour4' class='radio float_left' name='Recertification_GenericBehaviour' value='4' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("4") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericBehaviour4" class="radio">Impaired Decision Making</label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div>
                    <%= string.Format("<input id='Recertification_GenericBehaviour5' class='radio float_left' name='Recertification_GenericBehaviour' value='5' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("5") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericBehaviour5" class="radio">Difficulty coping w/ Altered Health Status</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_GenericBehaviour6' class='radio float_left' name='Recertification_GenericBehaviour' value='6' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("6") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericBehaviour6" class="radio">Combative</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_GenericBehaviour7' class='radio float_left' name='Recertification_GenericBehaviour' value='7' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("7") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericBehaviour7" class="radio">Irritability</label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_left">
                        <%= string.Format("<input id='Recertification_GenericBehaviour8' class='radio float_left' name='Recertification_GenericBehaviour' value='8' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("8") ? "checked='checked'" : "")%>
                        <label for="Recertification_GenericBehaviour8" class="radio">Other</label>
                    </div><div id="Recertification_GenericBehaviour8More" class="float_right">
                        <label for="Recertification_GenericBehaviourOther"><em>(Specify)</em></label>
                        <%=Html.TextBox("Recertification_GenericBehaviourOther", data.ContainsKey("GenericBehaviourOther") ? data["GenericBehaviourOther"].Answer : "", new { @id = "Recertification_GenericBehaviourOther", @maxlength = "20" })%>
                    </div>
                </div>
            </div>
        </div><div class="wide_column">
            <div class="row">
                <label for="Recertification_GenericBehaviourComments">Comments:</label>
                <%=Html.TextArea("Recertification_GenericBehaviourComments", data.ContainsKey("GenericBehaviourComments") ? data["GenericBehaviourComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericBehaviourComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="half float_left">
        <legend>Psychosocial</legend>
        <% string[] genericPsychosocial = data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer != "" ? data["GenericPsychosocial"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_GenericPsychosocial" value=" " />
        <div class="column">
            <div class="row">
                <div>
                    <%= string.Format("<input id='Recertification_GenericPsychosocial1' class='radio float_left' name='Recertification_GenericPsychosocial' value='1' type='checkbox' {0} />", genericPsychosocial!=null && genericPsychosocial.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericPsychosocial1" class="radio">WNL ( No Issues Identified )</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_GenericPsychosocial2' class='radio float_left' name='Recertification_GenericPsychosocial' value='2' type='checkbox' {0} />", genericPsychosocial!=null && genericPsychosocial.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericPsychosocial2" class="radio">Poor Home Environment</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_GenericPsychosocial3' class='radio float_left' name='Recertification_GenericPsychosocial' value='3' type='checkbox' {0} />", genericPsychosocial!=null && genericPsychosocial.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericPsychosocial3" class="radio">No emotional support from family/CG</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_GenericPsychosocial4' class='radio float_left' name='Recertification_GenericPsychosocial' value='4' type='checkbox' {0} />", genericPsychosocial!=null && genericPsychosocial.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_GenericPsychosocial4" class="radio">Verbalize desire for spiritual support</label>
                    <div class="clear"></div>
                </div><div class="float_left">
                    <%= string.Format("<input id='Recertification_GenericPsychosocial5' class='radio float_left' name='Recertification_GenericPsychosocial' value='5' type='checkbox' {0} />", genericPsychosocial != null && genericPsychosocial.Contains("5") ? "checked='checked'" : "")%>
                    <label for="Recertification_GenericPsychosocial5" class="radio">Other</label>
                    <div class="clear"></div>
                </div><div id="Recertification_GenericPsychosocial5More" class="float_right">
                    <label for="Recertification_GenericPsychosocialOther"><em>(Specify)</em></label>
                    <%=Html.TextBox("Recertification_GenericPsychosocialOther", data.ContainsKey("GenericPsychosocialOther") ? data["GenericPsychosocialOther"].Answer : "", new { @id = "Recertification_GenericPsychosocialOther", @maxlength = "20" })%>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericPsychosocialComments">Comments:</label>
                <%=Html.TextArea("Recertification_GenericPsychosocialComments", data.ContainsKey("GenericPsychosocialComments") ? data["GenericPsychosocialComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericPsychosocialComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Patient Strengths</legend>
        <% string[] patientStrengths = data.ContainsKey("485PatientStrengths") && data["485PatientStrengths"].Answer != "" ? data["485PatientStrengths"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485PatientStrengths" value="" />
        <div class="column">
            <div class="row">
                <div>
                    <%= string.Format("<input id='Recertification_485PatientStrengths1' class='radio float_left' name='Recertification_485PatientStrengths' value='1' type='checkbox' {0} />", patientStrengths!=null && patientStrengths.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_485PatientStrengths1" class="radio">Motivated Learner</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_485PatientStrengths2' class='radio float_left' name='Recertification_485PatientStrengths' value='2' type='checkbox' {0} />", patientStrengths!=null && patientStrengths.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_485PatientStrengths2" class="radio">Strong Support System</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_485PatientStrengths3' class='radio float_left' name='Recertification_485PatientStrengths' value='3' type='checkbox' {0} />", patientStrengths!=null && patientStrengths.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_485PatientStrengths3" class="radio">Absence of Multiple Diagnosis</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='Recertification_485PatientStrengths4' class='radio float_left' name='Recertification_485PatientStrengths' value='4' type='checkbox' {0} />", patientStrengths!=null && patientStrengths.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="Recertification_485PatientStrengths4" class="radio">Enhanced Socioeconomic Status</label>
                    <div class="clear"></div>
                </div>
            </div><div class="row">
                <label for="Recertification_485PatientStrengthOther" class="float_left">Other <em>(specify)</em>:</label>
                <div class="float_right">
                    <%=Html.TextBox("Recertification_485PatientStrengthOther", data.ContainsKey("485PatientStrengthOther") ? data["485PatientStrengthOther"].Answer : "", new { @id = "Recertification_485PatientStrengthOther", @maxlength = "70" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset id="phq">
        <legend>PHQ-2&copy;*</legend>
        <div class="wide_column">
            <div class="row">
                <table class="form bordergrid">
                    <thead>
                        <tr>
                            <th></th>
                            <th>
                                Not at all<br />
                                (0 &ndash; 1 day)
                            </th><th>
                                Several days<br />
                                (2 &ndash; 6 days)
                            </th><th>
                                More than half of the days<br />
                                (7 &ndash; 11 days)
                            </th><th>
                                Nearly every day<br />
                                (12 &ndash; 14 days)
                            </th><th>
                                N/A Unable to respond
                            </th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td>
                                a) Little interest or pleasure in doing things
                                <%=Html.Hidden("Recertification_M1730DepressionScreeningInterest")%>
                            </td><td>
                                <%=Html.RadioButton("Recertification_M1730DepressionScreeningInterest", "00", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "00" ? true : false, new { @id = "M1730DepressionScreeningInterest0", @class = "radio float_left" })%>
                                <label for="M1730DepressionScreeningInterest0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("Recertification_M1730DepressionScreeningInterest", "01", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "01" ? true : false, new { @id = "M1730DepressionScreeningInterest1", @class = "radio float_left" })%>
                                <label for="M1730DepressionScreeningInterest1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("Recertification_M1730DepressionScreeningInterest", "02", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "02" ? true : false, new { @id = "M1730DepressionScreeningInterest2", @class = "radio float_left" })%>
                                <label for="M1730DepressionScreeningInterest2" class="radio">2</label>
                            </td><td>
                                <%=Html.RadioButton("Recertification_M1730DepressionScreeningInterest", "03", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "03" ? true : false, new { @id = "M1730DepressionScreeningInterest3", @class = "radio float_left" })%>
                                <label for="M1730DepressionScreeningInterest3" class="radio">3</label>
                            </td><td>
                                <%=Html.RadioButton("Recertification_M1730DepressionScreeningInterest", "NA", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "NA" ? true : false, new { @id = "M1730DepressionScreeningInterestNA", @class = "radio float_left" })%>
                                <label for="M1730DepressionScreeningInterestNA" class="radio">NA</label>
                            </td>
                        </tr><tr>
                            <td>
                                b) Feeling down, depressed, or hopeless?
                                <%=Html.Hidden("Recertification_M1730DepressionScreeningHopeless")%>
                            </td><td>
                                <%=Html.RadioButton("Recertification_M1730DepressionScreeningHopeless", "00", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "00" ? true : false, new { @id = "Recertification_M1730DepressionScreeningHopeless0", @class = "radio float_left" })%>
                                <label for="Recertification_M1730DepressionScreeningHopeless0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("Recertification_M1730DepressionScreeningHopeless", "01", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "01" ? true : false, new { @id = "Recertification_M1730DepressionScreeningHopeless1", @class = "radio float_left" })%>
                                <label for="Recertification_M1730DepressionScreeningHopeless1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("Recertification_M1730DepressionScreeningHopeless", "02", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "02" ? true : false, new { @id = "Recertification_M1730DepressionScreeningHopeless2", @class = "radio float_left" })%>
                                <label for="Recertification_M1730DepressionScreeningHopeless2" class="radio">2</label>
                            </td><td>
                                <%=Html.RadioButton("Recertification_M1730DepressionScreeningHopeless", "03", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "03" ? true : false, new { @id = "Recertification_M1730DepressionScreeningHopeless3", @class = "radio float_left" })%>
                                <label for="Recertification_M1730DepressionScreeningHopeless3" class="radio">3</label>
                            </td><td>
                                <%=Html.RadioButton("Recertification_M1730DepressionScreeningHopeless", "NA", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "NA" ? true : false, new { @id = "Recertification_M1730DepressionScreeningHopelessNA", @class = "radio float_left" })%>
                                <label for="Recertification_M1730DepressionScreeningHopelessNA" class="radio">NA</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right"><em>Copyright© Pfizer Inc. All rights reserved. Reproduced with permission.</em></div>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] behaviorInterventions = data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer != "" ? data["485BehaviorInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485BehaviorInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorInterventions1' class='radio float_left' name='Recertification_485BehaviorInterventions' value='1' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485BehaviorInterventions1" class="radio">SN to notify physicain this pateint was screened for depression using PHQ-2 scale and meets criteria for further evaluation for depression.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorInterventions2' class='radio float_left' name='Recertification_485BehaviorInterventions' value='2' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485BehaviorInterventions2" class="radio">SN to perform a neurological assessment each visit.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorInterventions3' class='radio float_left' name='Recertification_485BehaviorInterventions' value='3' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485BehaviorInterventions3" class="radio">SN to assess/instruct on seizure disorder signs &amp; symptoms and appropriate actions during seizure activity.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorInterventions4' class='radio float_left' name='Recertification_485BehaviorInterventions' value='4' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485BehaviorInterventions4">SN to instruct the</label>
                    <%  var instructSeizurePrecautionPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructSeizurePrecautionPerson") && data["485InstructSeizurePrecautionPerson"].Answer != "" ? data["485InstructSeizurePrecautionPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485InstructSeizurePrecautionPerson", instructSeizurePrecautionPerson)%>
                    <label for="Recertification_485BehaviorInterventions4">on seizure precautions.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorInterventions5' class='radio float_left' name='Recertification_485BehaviorInterventions' value='5' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485BehaviorInterventions5" class="radio">SN to instruct caregiver on orientation techniques to use when patient becomes disoriented.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorInterventions6' class='radio float_left' name='Recertification_485BehaviorInterventions' value='6' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485BehaviorInterventions6">MSW:</label>
                    <%=Html.Hidden("Recertification_485MSWCommunityAssistanceVisits")%>
                    <%=Html.RadioButton("Recertification_485MSWCommunityAssistanceVisits", "1", data.ContainsKey("485MSWCommunityAssistanceVisits") && data["485MSWCommunityAssistanceVisits"].Answer == "1" ? true : false, new { @id = "Recertification_485MSWCommunityAssistanceVisits1", @class = "radio" })%>
                    <label for="Recertification_485MSWCommunityAssistanceVisits1">1-2 OR</label>
                    <%=Html.RadioButton("Recertification_485MSWCommunityAssistanceVisits", "0", data.ContainsKey("485MSWCommunityAssistanceVisits") && data["485MSWCommunityAssistanceVisits"].Answer == "0" ? true : false, new { @id = "Recertification_485MSWCommunityAssistanceVisits0", @class = "radio" })%>
                    <%=Html.TextBox("Recertification_485MSWCommunityAssistanceVisitAmount", data.ContainsKey("485MSWCommunityAssistanceVisitAmount") ? data["485MSWCommunityAssistanceVisitAmount"].Answer : "", new { @id = "Recertification_485MSWCommunityAssistanceVisitAmount", @class = "zip", @maxlength = "5" })%>
                    <label for="Recertification_485BehaviorInterventions6">visits, every 60 days for community resource assistance.</label>
                </span>
            </div><div class="row">
                <label for="Recertification_485BehaviorOrderTemplates">Additional Orders:</label>
                <%  var behaviorOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485BehaviorOrderTemplates") && data["485BehaviorOrderTemplates"].Answer != "" ? data["485BehaviorOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485BehaviorOrderTemplates", behaviorOrderTemplates)%>
                <%= Html.TextArea("Recertification_485BehaviorComments", data.ContainsKey("485BehaviorComments") ? data["485BehaviorComments"].Answer : "", 5, 70, new { @id = "Recertification_485BehaviorComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <% string[] behaviorGoals = data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer != "" ? data["485BehaviorGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="Recertification_485BehaviorGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorGoals1' class='radio float_left' name='Recertification_485BehaviorGoals' value='1' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485BehaviorGoals1" class="radio">Neuro status will be within normal limits and free of S&amp;S of complications or further deterioration.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorGoals2' class='radio float_left' name='Recertification_485BehaviorGoals' value='2' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485BehaviorGoals2" class="radio">Patient will exhibit stable neuro status evidenced by increased alertness, mobility and self-care by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorGoals3' class='radio float_left' name='Recertification_485BehaviorGoals' value='3' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485BehaviorGoals3" class="radio">Patient will exhibit stable neuro status evidence by absence of seizure activity by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorGoals4' class='radio float_left' name='Recertification_485BehaviorGoals' value='4' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485BehaviorGoals4" class="radio">Patient will have optimal cognitive functioning within parameters established for the stage of disease by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorGoals5' class='radio float_left' name='Recertification_485BehaviorGoals' value='5' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485BehaviorGoals5" class="radio">Patient will remain free from increased confusion during the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorGoals6' class='radio float_left' name='Recertification_485BehaviorGoals' value='6' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="Recertification_485BehaviorGoals6">The</label>
                    <%  var verbalizeSeizurePrecautionsPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485VerbalizeSeizurePrecautionsPerson") && data["485VerbalizeSeizurePrecautionsPerson"].Answer != "" ? data["485VerbalizeSeizurePrecautionsPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("Recertification_485VerbalizeSeizurePrecautionsPerson", verbalizeSeizurePrecautionsPerson)%>
                    <label for="Recertification_485BehaviorGoals6">will verbalize understanding of seizure precautions.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorGoals7' class='radio float_left' name='Recertification_485BehaviorGoals' value='7' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485BehaviorGoals7" class="radio">Caregiver will verbalize understanding of proper orientation techniques to use when patient becomes disoriented.</label>
            </div><div class="row">
                <%= string.Format("<input id='Recertification_485BehaviorGoals8' class='radio float_left' name='Recertification_485BehaviorGoals' value='8' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("8") ? "checked='checked'" : "" ) %>
                <label for="Recertification_485BehaviorGoals8" class="radio">Patient&rsquo;s community resource needs will be met with assistance of social worker.</label>
            </div><div class="row">
                <label for="Recertification_485BehaviorGoals">Additional Goals:</label>
                <%  var behaviorGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485BehaviorGoalTemplates") && data["485BehaviorGoalTemplates"].Answer != "" ? data["485BehaviorGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485BehaviorGoalTemplates", behaviorGoalTemplates)%>
                <%=Html.TextArea("Recertification_485BehaviorGoalComments", data.ContainsKey("485BehaviorGoalComments") ? data["485BehaviorGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485BehaviorGoalComments" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Recertification.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('Recertification_ValidationContainer','{0}','{1}','{2}','Recertification');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfChecked($("#Recertification_GenericNeurologicalStatus1"), $("#Recertification_GenericNeurologicalStatus1More"));
    Oasis.showIfChecked($("#Recertification_GenericNeurologicalStatus2"), $("#Recertification_GenericNeurologicalStatus2More"));
    Oasis.showIfChecked($("#Recertification_GenericNeurologicalStatus3"), $("#Recertification_GenericNeurologicalStatus3More"));
    Oasis.showIfChecked($("#Recertification_GenericNeurologicalStatus4"), $("#Recertification_GenericNeurologicalSpeech"));
    Oasis.showIfChecked($("#Recertification_GenericNeurologicalStatus5"), $("#Recertification_GenericNeurologicalStatus5More"));
    Oasis.showIfChecked($("#Recertification_GenericNeurologicalStatus9"), $("#Recertification_GenericNeurologicalStatus9More"));
    Oasis.showIfChecked($("#Recertification_485MentalStatus8"), $("#Recertification_485MentalStatus8More"));
    Oasis.showIfChecked($("#Recertification_GenericBehaviour8"), $("#Recertification_GenericBehaviour8More"));
    Oasis.showIfChecked($("#Recertification_GenericPsychosocial5"), $("#Recertification_GenericPsychosocial5More"));
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>