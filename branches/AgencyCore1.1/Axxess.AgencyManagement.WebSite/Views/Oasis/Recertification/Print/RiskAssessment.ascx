﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("SN to assist patient to obtain ERS button.",<%= data != null && data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("SN to develop individualized emergency plan with patient.",<%= data != null && data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct patient on importance of receiving influenza and pneumococcal vaccines.",<%= data != null && data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485RiskInterventionComments") && data["485RiskInterventionComments"].Answer.IsNotNullOrEmpty() ? data["485RiskInterventionComments"].Answer : ""%>",false,2),
        "Interventions");
    printview.addsection(
        printview.checkbox("The patient will have no hospitalizations during the episode.",<%= data != null && data.ContainsKey("485RiskAssessmentGoals") && data["485RiskAssessmentGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("The <%= data != null && data.ContainsKey("485VerbalizeEmergencyPlanPerson") && data["485VerbalizeEmergencyPlanPerson"].Answer.IsNotNullOrEmpty() ? data["485VerbalizeEmergencyPlanPerson"].Answer : "<span class='blank'></span>"%> will verbalize understanding of individualized emergency plan by the end of the episode.",<%= data != null && data.ContainsKey("485RiskAssessmentGoals") && data["485RiskAssessmentGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.span("Additional Goals:",true) +
        printview.span("<%= data != null && data.ContainsKey("485RiskGoalComments") && data["485RiskGoalComments"].Answer.IsNotNullOrEmpty() ? data["485RiskGoalComments"].Answer : ""%>",false,2),
        "Goals");
</script>