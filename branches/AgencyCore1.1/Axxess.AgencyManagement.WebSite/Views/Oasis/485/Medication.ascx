﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfile>" %>
<div>
    <%= Html.Telerik().Grid<Medication>()
        .Name("485MedicationGrid")
            .DataKeys(keys =>  {
                keys.Add(M => M.Id);
            })
            .ToolBar(commands => commands.Insert().ButtonType(GridButtonType.ImageAndText).HtmlAttributes(new { id = "485MedicatonGridInsertButton", style = "margin-left:0" }))
            .DataBinding(dataBinding => {
                dataBinding.Ajax()
                    .Select("Medication", "Patient", new { MedId = Model.Id, medicationCategory = "Active" })
                    .Insert("InsertMedication", "Patient", new { MedId = Model.Id, medicationCategory = "Active" })
                    .Update("UpdateMedication", "Patient", new { MedId = Model.Id, medicationCategory = "Active" })
                    .Delete("DeleteMedication", "Patient", new { MedId = Model.Id, medicationCategory = "Active" });
            })
            .Columns(columns => {
                columns.Bound(M => M.IsLongStanding).ClientTemplate("<input type='checkbox' disabled='disabled' name='IsLongStanding' <#=IsLongStanding? checked='checked' : '' #> />").Width(30);
                columns.Bound(M => M.StartDate).Format("{0:d}").Width(80);
                columns.Bound(M => M.MedicationDosage).Title("Name & Dose");
                columns.Bound(M => M.Route).Title("Route").Width(110);
                columns.Bound(M => M.Frequency).Title("Frequency").Width(110);
                columns.Bound(M => M.MedicationType).Title("Type").Width(70).ClientTemplate("<label><#=MedicationType!=null&&MedicationType!=''? MedicationType.Text : ''#> </label>");
                columns.Bound(M => M.Classification).Width(120);
                columns.Command(commands =>
                {
                    commands.Edit();
                    commands.Delete();
                }).Width(135).Title("Commands");
            }).ClientEvents(e => e.OnEdit("Patient.onPlanofCareMedicationEdit")).Editable(editing => editing.Mode(GridEditMode.InLine))
            .Pageable().Scrollable().Sortable()        
    %>
</div>

<script type="text/javascript">
    $("#485MedicatonGridInsertButton").html("<span class=\"t-add t-icon\"></span>Add New Medication");
    $("#485MedicationGrid .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22PlanOfCare.SaveMedications('<%= Model.PatientId %>');%22%3ESave &amp; Close%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
</script>