﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
        "<script type='text/javascript'>acore.renamewindow('Oasis-C Discharge from Agency | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','discharge');</script>")
%>
<div id="dischargeTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_discharge">Clinical Record Items</a></li>
        <li><a href="#riskassessment_discharge">Risk Assessment</a></li>
        <li><a href="#sensorystatus_discharge">Sensory Status</a></li>
        <li><a href="#pain_discharge">Pain</a></li>
        <li><a href="#integumentarystatus_discharge">Integumentary Status</a></li>
        <li><a href="#respiratorystatus_discharge">Respiratory Status</a></li>
        <li><a href="#cardiacstatus_discharge">Cardiac Status</a></li>
        <li><a href="#eliminationstatus_discharge">Elimination Status</a></li>
        <li><a href="#behaviourialstatus_discharge">Neuro/Behaviourial Status</a></li>
        <li><a href="#adl_discharge">ADL/IADLs</a></li>
        <li><a href="#medications_discharge">Medications</a></li>
        <li><a href="#caremanagement_discharge">Care Management</a></li>
        <li><a href="#emergentcare_discharge">Emergent Care</a></li>
        <li><a href="#dischargeAdd_discharge">Discharge</a></li>
    </ul>
    <!-- Add validation back in later -->
    <div id="clinicalRecord_discharge" class="general"><% Html.RenderPartial("~/Views/Oasis/Discharge/Demographics.ascx", Model); %></div>
    <div id="riskassessment_discharge" class="general loading"></div>
    <div id="sensorystatus_discharge" class="general loading"></div>
    <div id="pain_discharge" class="general loading"></div>
    <div id="integumentarystatus_discharge" class="general loading"></div>
    <div id="respiratorystatus_discharge" class="general loading"></div>
    <div id="cardiacstatus_discharge" class="general loading"></div>
    <div id="eliminationstatus_discharge" class="general loading"></div>
    <div id="behaviourialstatus_discharge" class="general loading"></div>
    <div id="adl_discharge" class="general loading"></div>
    <div id="medications_discharge" class="general loading"></div>
    <div id="caremanagement_discharge" class="general loading"></div>
    <div id="emergentcare_discharge" class="general loading"></div>
    <div id="dischargeAdd_discharge" class="general loading"></div>
</div>
