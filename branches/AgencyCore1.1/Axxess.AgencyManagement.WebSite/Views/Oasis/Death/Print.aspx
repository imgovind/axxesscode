﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Assessment>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<% var data = Model.ToDictionary(); %>
<head>
    <%= string.Format("<title>Discharge | {0}{1}, {2}{3}</title>",data.ContainsKey("M0040LastName")?data["M0040LastName"].Answer:"",data.ContainsKey("M0040Suffix")?" "+data["M0040Suffix"].Answer:"",data.ContainsKey("M0040FirstName")?data["M0040FirstName"].Answer:"",data.ContainsKey("M0040MI")?" "+data["M0040MI"].Answer:"") %>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body>
    <div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Death</h1>
        <h2>Demographics</h2>
        <div class="tricol">
            <span><strong>Time In:</strong>00:00</span>
            <span><strong>Time Out:</strong>00:00</span>
            <span><strong>Visit Date:</strong>00/00/0000</span>
        </div><div class="bicol">
            <span><strong>(M0020) Patient ID Number:</strong><%= data.ContainsKey("M0020PatientIdNumber") ? data["M0020PatientIdNumber"].Answer : "" %></span>
            <span><strong>(M0030) Discharge Date:</strong><%= data.ContainsKey("M0030SocDate") ? data["M0030SocDate"].Answer : "" %></span>
            <span><strong>(M0032) Resuption of Care Date:</strong><%= data.ContainsKey("M0032ROCDate") ? data["M0032ROCDate"].Answer : "" %></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0032ROCDateNotApplicable") && data["M0032ROCDateNotApplicable"].Answer == "1" ? "&#x2713;" : "" %></span> NA &ndash; Not Applicable</span>
        </div><div>
            <span><strong>Episode Start Date:</strong><%= data.ContainsKey("GenericEpisodeStartDate") ? data["GenericEpisodeStartDate"].Answer : "" %></span>
        </div><div class="bicol">
            <span><strong>(M0040) Patient Name:</strong>(Last Suffix First MI)</span>
            <span><%= data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : ""%> <%= data.ContainsKey("M0040Suffix") ? data["M0040Suffix"].Answer : ""%> <%= data.ContainsKey("M0040FirstName") ? data["M0040FirstName"].Answer : ""%> <%= data.ContainsKey("M0040MI") ? data["M0040MI"].Answer : "" %></span>
            <span><strong>(M0064) Social Security Number:</strong><%= data.ContainsKey("M0064PatientSSN") ? data["M0064PatientSSN"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0064PatientSSNUnknown") && data["M0064PatientSSNUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Unknown or Not Available</span>
        </div><div class="bicol">
            <span><strong>(M0050) Patient State:</strong><%= data.ContainsKey("M0050PatientState") ? data["M0050PatientState"].Answer : ""%></span>
            <span><strong>(M0060) Patient Zip Code:</strong><%= data.ContainsKey("M0060PatientZipCode") ? data["M0060PatientZipCode"].Answer : ""%></span>
        </div><div class="bicol">
            <span><strong>(M0063) Medicare Number:</strong><%= data.ContainsKey("M0063PatientMedicareNumber") ? data["M0063PatientMedicareNumber"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0063PatientMedicareNumberUnknown") && data["M0063PatientMedicareNumberUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No Medicare</span>
            <span><strong>(M0065) Medicaid Number:</strong><%= data.ContainsKey("M0065PatientMedicaidNumber") ? data["M0065PatientMedicaidNumber"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0065PatientMedicaidNumberUnknown") && data["M0065PatientMedicaidNumberUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No Medicaid</span>
        </div><div class="bicol">
            <span><strong>(M0066) Birth Date:</strong><%= data.ContainsKey("M0066PatientDoB") ? data["M0066PatientDoB"].Answer : ""%></span>
            <span><strong>(M0069) Gender:</strong><%= data.ContainsKey("M0069Gender") ? data["M0069Gender"].Answer == "1" ? "Male" : "Female" : "" %></span>
        </div><div class="bicol">
            <span><strong>(M0080) Discipline of Person Completing Assessment:</strong><% if (data.ContainsKey("M0080DisciplinePerson")) { switch(data["M0080DisciplinePerson"].Answer) { case "01": %>1 &ndash; RN<% break; case "02": %>2 &ndash; PT<% break; case "03": %>3 &ndash; SLP/ST<% break; case "04": %>4 &ndash; OT<% break; } } %></span>
            <span><strong>(M0090) Date Assessment Completed:</strong><%= data.ContainsKey("M0090AssessmentCompleted") ? data["M0090AssessmentCompleted"].Answer : ""%></span>
        </div><div>
            <span><strong>(M0100) This Assessment is Currently Being Completed for the Following Reason:</strong></span>
            <span><strong>Start/Resuption of Care</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Discharge&mdash;further visits planned</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Resumption of care (after inpatient stay)</span>
            <span><strong>Follow-Up</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Recertification (follow-up) reassessment <em>[Go to M0110]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Other follow-up <em>[Go to M0110]</em></span>
            <span><strong>Transfer to an Inpatient Facility</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "06" ? "&#x2713;" : ""%></span> 6 &ndash; Transfer to inpatient facility&mdash;patient not discharged from agency <em>[Go to M1040]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "07" ? "&#x2713;" : ""%></span> 7 &ndash; Transfer to inpatient facility&mdash;patient discharged from agency <em>[Go to M1040]</em></span>
            <span><strong>Discharge from Agency – Not to an Inpatient Facility</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "08" ? "&#x2713;" : ""%></span> 8 &ndash; Death at home <em>[Go to M0903]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "09" ? "&#x2713;" : ""%></span> 9 &ndash; Discharge from agency <em>[Go to M1040]</em></span>
        </div><div>
            <span><strong>(M0102) Date of Physician-ordered Discharge (Resumption of Care):</strong><%= data.ContainsKey("M0102PhysicianOrderedDate") && data["M0102PhysicianOrderedDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer + " <em>[Go to M0110]</em>" : "" %></span>
            <span>If the physician indicated a specific Discharge (resumption of care) date when the patient was referred for home health services, record the date specified.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && data["M0102PhysicianOrderedDateNotApplicable"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No specific SOC date ordered by physician</span>
        </div><div>
            <span><strong>(M0104) Date of Referral:</strong><%= data.ContainsKey("M0104ReferralDate") && data["M0104ReferralDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer : "" %></span>
            <span>Indicate the date that the written or documented orders from the physician or physician designee for initiation or resumption of care were received by the HHA.</span>
        </div><div>
            <span><strong>(M0110) Episode Timing:</strong><%= data.ContainsKey("M0104ReferralDate") && data["M0104ReferralDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer : "" %></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Early</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Later</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "UK" ? "&#x2713;" : ""%></span> UK &ndash; Unknown</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Not Applicable/No Medicare case mix group to be defined by this assessment </span>
            <span>Is the Medicare home health payment episode for which this assessment will define a case mix group an &ldquo;early&rdquo; episode or a &ldquo;later&rdquo; episode in the patient&rsquo;s current sequence of adjacent Medicare home health payment episodes?</span>
        </div><div>
            <span><strong>(M0140) Race/Ethnicity <em>(Mark all that apply)</em>:</strong></span>
            <span class="bicol">
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceAMorAN") && data["M0140RaceAMorAN"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; American Indian or Alaska Native</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceAsia") && data["M0140RaceAsia"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Asian</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceBalck") && data["M0140RaceBalck"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Black or African American</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceHispanicOrLatino") && data["M0140RaceHispanicOrLatino"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Hispanic or Latino</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceNHOrPI") && data["M0140RaceNHOrPI"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Native Hawaiian or Pacific Islander</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceWhite") && data["M0140RaceWhite"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; White</span>        
            </span>
        </div><div>
            <span><strong>(M0150) Current Payment Sources for Home Care <em>(Mark all that apply)</em>:</strong></span>
            <span class="bicol">
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceNone") && data["M0150PaymentSourceNone"].Answer == "1" ? "&#x2713;" : ""%></span> 0 &ndash; None&mdash;No charge for current services</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCREFFS") && data["M0150PaymentSourceMCREFFS"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Medicare (traditional fee-for-service)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCREHMO") && data["M0150PaymentSourceMCREHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Medicare (HMO/Managed Care/Advantage plan)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCAIDFFS") && data["M0150PaymentSourceMCAIDFFS"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Medicaid (traditional fee-for-service)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMACIDHMO") && data["M0150PaymentSourceMACIDHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Medicaid (HMO/Managed Care)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceWRKCOMP") && data["M0150PaymentSourceWRKCOMP"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Worker&rsquo;s compensation</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceTITLPRO") && data["M0150PaymentSourceTITLPRO"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; Title progams (e.g. Title III, V, or XX)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceOTHGOVT") && data["M0150PaymentSourceOTHGOVT"].Answer == "1" ? "&#x2713;" : ""%></span> 7 &ndash; Other government (e.g. TriCare, VA etc)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourcePRVINS") && data["M0150PaymentSourcePRVINS"].Answer == "1" ? "&#x2713;" : ""%></span> 8 &ndash; Private Insurance</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourcePRVHMO") && data["M0150PaymentSourcePRVHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 9 &ndash; Private HMO/managed care</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceSelfPay") && data["M0150PaymentSourceSelfPay"].Answer == "1" ? "&#x2713;" : ""%></span> 10 &ndash; Self-pay</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceOtherSRS") && data["M0150PaymentSourceOtherSRS"].Answer == "1" ? "&#x2713;" : ""%></span> 11 &ndash; Other<%= data.ContainsKey("M0150PaymentSourceOther") && data["M0150PaymentSourceOther"].Answer.IsNotNullOrEmpty() ? ": " + data["M0150PaymentSourceOther"].Answer : "" %></span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceUnknown") && data["M0150PaymentSourceUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Unknown</span>        
            </span>
        </div>
        <h2>Death</h2>
        <div>
            <span><strong>(M0903) Date of Last (Most Recent) Home Visit:</strong></span>
            <span><%= data.ContainsKey("M0903LastHomeVisitDate") ? data["M0903LastHomeVisitDate"].Answer : "&nbsp;"%></span>
        </div><div>
            <span><strong>(M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer, or death (at home) of the patient.</strong></span>
            <span><%= data.ContainsKey("M0906DischargeDate") ? data["M0906DischargeDate"].Answer : "&nbsp;"%></span>
        </div>
    </div>
</body>
</html>