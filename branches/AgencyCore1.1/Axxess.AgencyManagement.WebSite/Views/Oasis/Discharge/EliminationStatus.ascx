﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyEliminationForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("DischargeFromAgency_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<%= Html.Hidden("categoryType", "Elimination")%>
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS M1600/M1610</legend>
        <div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1600');">(M1600)</a> Has this patient been treated for a Urinary Tract Infection in the past 14 days?<%=Html.Hidden("DischargeFromAgency_M1600UrinaryTractInfection", " ", new { @id = "" })%>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1600UrinaryTractInfection", "00", data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1600UrinaryTractInfection0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1600UrinaryTractInfection0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1600UrinaryTractInfection", "01", data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1600UrinaryTractInfection1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1600UrinaryTractInfection1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                    <div class="clear"></div>
                </div><div>
                <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1600');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1600UrinaryTractInfection", "NA", data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "NA" ? true : false, new { @id = "DischargeFromAgency_M1600UrinaryTractInfectionNA", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1600UrinaryTractInfectionNA"><span class="float_left">NA &ndash;</span><span class="normal margin">Patient on prophylactic treatment</span></label>
                    <div class="clear"></div>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1610');">(M1610)</a> Urinary Incontinence or Urinary Catheter Presence:<%=Html.Hidden("DischargeFromAgency_M1610UrinaryIncontinence", " ", new { @id = "" })%>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1610UrinaryIncontinence", "00", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1610UrinaryIncontinence0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1610UrinaryIncontinence0"><span class="float_left">0 &ndash;</span><span class="normal margin">No incontinence or catheter (includes anuria or ostomy for urinary drainage) <em>[Go to M1620]</em></span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1610UrinaryIncontinence", "01", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1610UrinaryIncontinence1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1610UrinaryIncontinence1"><span class="float_left">1 &ndash;</span><span class="normal margin">Patient is incontinent</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1610');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1610UrinaryIncontinence", "02", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1610UrinaryIncontinence2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1610UrinaryIncontinence2"><span class="float_left">2 &ndash;</span><span class="normal margin">Patient requires a urinary catheter (i.e., external, indwelling, intermittent, suprapubic) <em>[Go to M1620]</em></span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset id="dischargeFromAgency_M1615" class="oasis">
        <legend>OASIS M1615</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1615');">(M1615)</a> When does Urinary Incontinence occur?
                    <%=Html.Hidden("DischargeFromAgency_M1615UrinaryIncontinenceOccur")%>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1615UrinaryIncontinenceOccur", "00", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1615UrinaryIncontinenceOccur0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1615UrinaryIncontinenceOccur0"><span class="float_left">0 &ndash;</span><span class="normal margin">Timed-voiding defers incontinence</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1615UrinaryIncontinenceOccur", "01", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1615UrinaryIncontinenceOccur1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1615UrinaryIncontinenceOccur1"><span class="float_left">1 &ndash;</span><span class="normal margin">Occasional stress incontinence</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1615UrinaryIncontinenceOccur", "02", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1615UrinaryIncontinenceOccur2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1615UrinaryIncontinenceOccur2"><span class="float_left">2 &ndash;</span><span class="normal margin">During the night only</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1615UrinaryIncontinenceOccur", "03", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1615UrinaryIncontinenceOccur3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1615UrinaryIncontinenceOccur3"><span class="float_left">3 &ndash;</span><span class="normal margin">During the day only</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1615');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1615UrinaryIncontinenceOccur", "04", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1615UrinaryIncontinenceOccur4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1615UrinaryIncontinenceOccur4"><span class="float_left">4 &ndash;</span><span class="normal margin">During the day and night</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>OASIS M1620</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1620');">(M1620)</a> Bowel Incontinence Frequency:
                    <%=Html.Hidden("DischargeFromAgency_M1620BowelIncontinenceFrequency")%>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "00", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1620BowelIncontinenceFrequency0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1620BowelIncontinenceFrequency0"><span class="float_left">0 &ndash;</span><span class="normal margin">Very rarely or never has bowel incontinence</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "01", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1620BowelIncontinenceFrequency1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1620BowelIncontinenceFrequency1"><span class="float_left">1 &ndash;</span><span class="normal margin">Less than once weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "02", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1620BowelIncontinenceFrequency2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1620BowelIncontinenceFrequency2"><span class="float_left">2 &ndash;</span><span class="normal margin">One to three times weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "03", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1620BowelIncontinenceFrequency3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1620BowelIncontinenceFrequency3"><span class="float_left">3 &ndash;</span><span class="normal margin">Four to six times weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "04", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1620BowelIncontinenceFrequency4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1620BowelIncontinenceFrequency4"><span class="float_left">4 &ndash;</span><span class="normal margin">On a daily basis</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "05", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "05" ? true : false, new { @id = "DischargeFromAgency_M1620BowelIncontinenceFrequency5", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1620BowelIncontinenceFrequency5"><span class="float_left">5 &ndash;</span><span class="normal margin">More often than once daily</span></label>
                    <div class="clear"></div>
                </div><div>
                 <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1620');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1620BowelIncontinenceFrequency", "NA", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "NA" ? true : false, new { @id = "DischargeFromAgency_M1620BowelIncontinenceFrequencyNA", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1620BowelIncontinenceFrequencyNA"><span class="float_left">NA &ndash; </span><span class="normal margin">Patient has ostomy for bowel elimination</span></label>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.Validate('{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "StartOfCare")%></li>
        </ul>
    </div>
</div>
<%= string.Format("<script type='text/javascript'>if({0}=='00'||{0}=='02') {{ $(\"#dischargeFromAgency_M1615\").append(unescape(\"%3Cdiv class=%22shade%22%3E%3C/div%3E\")); $(\"input[name=DischargeFromAgency_M1615UrinaryIncontinenceOccur ]\").attr('checked', false);  }}</script>", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer.IsNotNullOrEmpty() ? data["M1610UrinaryIncontinence"].Answer : "-1")%>
<% } %>
<script type="text/javascript">
    $("input[name=DischargeFromAgency_M1610UrinaryIncontinence]").click(function() {
        if ($(this).val() == "00" || $(this).val() == "02") {
            $("#dischargeFromAgency_M1615").find(".shade").remove().end().append(unescape("%3Cdiv class=%22shade%22%3E%3C/div%3E"));
            $("input[name=DischargeFromAgency_M1615UrinaryIncontinenceOccur]").attr('checked', false);
        } else $("#dischargeFromAgency_M1615 .shade").remove();
    });
  </script>
