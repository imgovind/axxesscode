﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyPainForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("DischargeFromAgency_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<%= Html.Hidden("categoryType", "Pain")%>
<div class="wrapper main">
<fieldset class="oasis">
    <legend>OASIS</legend>
    <div class="wide_column">
        <div class="row">
            <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1242');">(M1242)</a> Frequency of Pain Interfering with patient&rsquo;s activity or movement</label><%= Html.Hidden("DischargeFromAgency_M1242PainInterferingFrequency", " ", new { @id = "" }) %>
            <div class="margin">
                <div><%= Html.RadioButton("DischargeFromAgency_M1242PainInterferingFrequency", "00", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1242PainInterferingFrequency00", @class = "radio float_left" })%><label for="DischargeFromAgency_M1242PainInterferingFrequency00"><span class="float_left">0 &ndash;</span><span class="normal margin">Patient has no pain</span></label></div>
                <div><%= Html.RadioButton("DischargeFromAgency_M1242PainInterferingFrequency", "01", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1242PainInterferingFrequency01", @class = "radio float_left" })%><label for="DischargeFromAgency_M1242PainInterferingFrequency01"><span class="float_left">1 &ndash;</span><span class="normal margin">Patient has pain that does not interfere with activity or movement</span></label></div>
                <div><%= Html.RadioButton("DischargeFromAgency_M1242PainInterferingFrequency", "02", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1242PainInterferingFrequency02", @class = "radio float_left" })%><label for="DischargeFromAgency_M1242PainInterferingFrequency02"><span class="float_left">2 &ndash;</span><span class="normal margin">Less often than daily</span></label></div>
                <div><%= Html.RadioButton("DischargeFromAgency_M1242PainInterferingFrequency", "03", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1242PainInterferingFrequency03", @class = "radio float_left" })%><label for="DischargeFromAgency_M1242PainInterferingFrequency03"><span class="float_left">3 &ndash;</span><span class="normal margin">Daily, but not constantly</span></label></div>
                <div><div class="float_right oasis"><div class="tooltip_oasis" onclick="Oasis.ToolTip('M1242');">?</div></div><%= Html.RadioButton("DischargeFromAgency_M1242PainInterferingFrequency", "04", data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1242PainInterferingFrequency04", @class = "radio float_left" })%><label for="DischargeFromAgency_M1242PainInterferingFrequency04"><span class="float_left">4 &ndash;</span><span class="normal margin">All of the time</span></label></div>
            </div>
        </div>
    </div>
</fieldset>
<div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.Validate('{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "StartOfCare")%></li>
        </ul>
    </div>
</div>
<% } %>
