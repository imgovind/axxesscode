﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyRespiratoryForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("DischargeFromAgency_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<%= Html.Hidden("categoryType", "Respiratory")%> 
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1400');">(M1400)</a> When is the patient dyspneic or noticeably Short of Breath?</label>
                <%= Html.Hidden("DischargeFromAgency_M1400PatientDyspneic") %>
                <div>
                    <%= Html.RadioButton("DischargeFromAgency_M1400PatientDyspneic", "00", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1400PatientDyspneic0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1400PatientDyspneic0"><span class="float_left">0 &ndash;</span><span class="normal margin">Patient is not short of breath</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("DischargeFromAgency_M1400PatientDyspneic", "01", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1400PatientDyspneic1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1400PatientDyspneic1"><span class="float_left">1 &ndash;</span><span class="normal margin">When walking more than 20 feet, climbing stairs</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("DischargeFromAgency_M1400PatientDyspneic", "02", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1400PatientDyspneic2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1400PatientDyspneic2"><span class="float_left">2 &ndash;</span><span class="normal margin">With moderate exertion (e.g., while dressing, using commode or bedpan, walking distances less than 20 feet)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("DischargeFromAgency_M1400PatientDyspneic", "03", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1400PatientDyspneic3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1400PatientDyspneic3"><span class="float_left">3 &ndash;</span><span class="normal margin">With minimal exertion (e.g., while eating, talking, or performing other ADLs) or with agitation</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1400');">?</div>
                    </div>
                    <%= Html.RadioButton("DischargeFromAgency_M1400PatientDyspneic", "04", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1400PatientDyspneic4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1400PatientDyspneic4"><span class="float_left">4 &ndash;</span><span class="normal margin">At rest (during day or night)</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1410');">(M1410)</a> Respiratory Treatments utilized at home: (Mark all that apply)</label>
                <div>
                    <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsOxygen" value=" " type="hidden" />
                    <%= string.Format("<input id='DischargeFromAgency_M1410HomeRespiratoryTreatmentsOxygen' class='radio float_left M1410' name='DischargeFromAgency_M1410HomeRespiratoryTreatmentsOxygen' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen") && data["M1410HomeRespiratoryTreatmentsOxygen"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgency_M1410HomeRespiratoryTreatmentsOxygen"><span class="float_left">1 &ndash;</span><span class="normal margin">Oxygen (intermittent or continuous)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsVentilator" value=" " type="hidden" />
                    <%= string.Format("<input id='DischargeFromAgency_M1410HomeRespiratoryTreatmentsVentilator' class='radio float_left M1410' name='DischargeFromAgency_M1410HomeRespiratoryTreatmentsVentilator' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator") && data["M1410HomeRespiratoryTreatmentsVentilator"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgency_M1410HomeRespiratoryTreatmentsVentilator"><span class="float_left">2 &ndash;</span><span class="normal margin">Ventilator (continually or at night)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsContinuous" value=" " type="hidden" />
                    <%= string.Format("<input id='DischargeFromAgency_M1410HomeRespiratoryTreatmentsContinuous' class='radio float_left M1410' name='DischargeFromAgency_M1410HomeRespiratoryTreatmentsContinuous' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous") && data["M1410HomeRespiratoryTreatmentsContinuous"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgency_M1410HomeRespiratoryTreatmentsContinuous"><span class="float_left">3 &ndash;</span><span class="normal margin">Continuous / Bi-level positive airway pressure</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1400');">?</div>
                    </div>
                    <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsNone" value=" " type="hidden" />
                    <%= string.Format("<input id='DischargeFromAgency_M1410HomeRespiratoryTreatmentsNone' class='radio float_left' name='DischargeFromAgency_M1410HomeRespiratoryTreatmentsNone' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsNone") && data["M1410HomeRespiratoryTreatmentsNone"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="DischargeFromAgency_M1410HomeRespiratoryTreatmentsNone"><span class="float_left">4 &ndash;</span><span class="normal margin">None of the above</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.Validate('{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "StartOfCare")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.noneOfTheAbove($("#DischargeFromAgency_M1410HomeRespiratoryTreatmentsNone"), $(".M1410"));
</script>