﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpPatientHistoryForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("FollowUp_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<%= Html.Hidden("categoryType", "PatientHistory")%>
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>Diagnoses, Symptom Control, and Payment Diagnoses</legend>
        <div class="wide_column">
            <div class="row">
                <div class="instructions">
                    <p>List each diagnosis for which the patient is receiving home care (Column 1) and enter its ICD-9-C M code at the level of highest specificity (no surgical/procedure codes) (Column 2). Diagnoses are listed in the order that best reflect the seriousness of each condition and support the disciplines and services provided. Rate the degree of symptom control for each condition (Column 2). Choose one value that represents the degree of symptom control appropriate for each diagnosis: V-codes (for M1020 or M1022) or E-codes (for M1022 only) may be used. ICD-9-C M sequencing requirements must be followed if multiple coding is indicated for any diagnoses. If a V-code is reported in place of a case mix diagnosis, then optional item M1024 Payment Diagnoses (Columns 3 and 4) may be completed. A case mix diagnosis is a diagnosis that determines the Medicare P P S case mix group. Do not assign symptom control ratings for V- or E-codes.</p>
                    <strong>Code each row according to the following directions for each column:</strong>
                    <ul>
                        <li><span class="float_left">Column 1:</span><p>Enter the description of the diagnosis.</p></li>
                        <li><span class="float_left">Column 2:</span><p>Enter the ICD-9-C M code for the diagnosis described in Column 1;</p>
                            <ul>
                                <li>Rate the degree of symptom control for the condition listed in Column 1 using thefollowing scale:</li>
                                <li><span class="float_left">0.</span><p>Asymptomatic, no treatment needed at this time</p></li>
                                <li><span class="float_left">1.</span><p>Symptoms well controlled with current therapy</p></li>
                                <li><span class="float_left">2.</span><p>Symptoms controlled with difficulty, affecting daily functioning; patient needsongoing monitoring</p></li>
                                <li><span class="float_left">3.</span><p>Symptoms poorly controlled; patient needs frequent adjustment in treatment anddose monitoring</p></li>
                                <li><span class="float_left">4.</span><p>Symptoms poorly controlled; history of re-hospitalizations Note that in Column2 the rating for symptom control of each diagnosis should not be used to determinethe sequencing of the diagnoses listed in Column 1. These are separate items andsequencing may not coincide. Sequencing of diagnoses should reflect the seriousnessof each condition and support the disciplines and services provided.</p></li>
                            </ul>
                        </li>
                        <li><span class="float_left">Column 3:</span><p>(OPTIONAL) If a V-code is assigned to any row in Column 2, in place ofa case mix diagnosis, it may be necessary to complete optional item M1024 PaymentDiagnoses (Columns 3 and 4). See OASIS-C Guidance Manual.</p></li>
                        <li><span class="float_left">Column 4:</span><p>(OPTIONAL) If a V-code in Column 2 is reported in place of a case mixdiagnosis that requires multiple diagnosis codes under ICD-9-C M coding guidelines,enter the diagnosis descriptions and the ICD-9-C M codes in the same row in Columns 3 and 4. For example, if the case mix diagnosis is a manifestation code, record the diagnosis description and ICD-9-C M code for the underlying condition in Column 3 of that row and the diagnosis description and ICD-9-C M code for the manifestation in Column 4 of that row. Otherwise, leave Column 4 blank in that row.</p></li>
                    </ul>
                </div>
                <table class="form bordergrid">
                    <thead>
                        <tr class="align_center">
                            <td colspan="2">
                                <div class="halfOfTd"><h4>Column 1</h4></div>
                                <div class="halfOfTd"><h4>Column 2</h4></div>
                            </td><td>
                                <h4>Column 3</h4>
                            </td><td>
                                <h4>Column 4</h4>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <label for="FollowUp_M1020PrimaryDiagnosis" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1020');">(M1020)</a> Primary Diagnosis</label>
                                </div><div class="halfOfTd">
                                    <div>ICD-9-C M and symptom control rating for each condition. Note that the sequencing of these ratings may not match the sequencing of the diagnoses.</div>
                                    <div for="FollowUp_M1020ICD9M" class="strong">ICD-9-C M/ Symptom Control Rating</div>
                                    <em>(V-codes are allowed)</em>
                                </div>
                            </td><td>
                                <div>Complete if a V-code is assigned under certain circumstances to Column 2 in place of a case mix diagnosis.</div>
                                <div for="FollowUp_M1024ICD9MA3" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1024');">(M1024)</a> Description/ ICD-9-C M</div>
                                <em>(V- or E-codes NOT allowed)</em>
                            </td><td>
                                <div>Complete only if the V-code in Column 2 is reported in place of a case mix diagnosis that is a multiple coding situation (e.g., a manifestation code).</div>
                                <div for="FollowUp_M1024ICD9MA4" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1024');">(M1024)</a> Description/ ICD-9-C M</div>
                                <em>(V- or E-codes NOT allowed)</em>
                            </td>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">a.</span>
                                    <%= Html.TextBox("FollowUp_M1020PrimaryDiagnosis", data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1020PrimaryDiagnosis" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">a.</span>
                                    <%= Html.TextBox("FollowUp_M1020ICD9M", data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer : "", new { @class = "icd", @id = "FollowUp_M1020ICD9M" }) %>
                                    <label for="FollowUp_M1020SymptomControlRating">Severity:</label>
                                    <%  var severity = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1020SymptomControlRating") ? data["M1020SymptomControlRating"].Answer : " ");%>
                                    <%= Html.DropDownList("FollowUp_M1020SymptomControlRating", severity, new { @id = "FollowUp_M1020SymptomControlRating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis") ? data["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer : "0");%>
                                    <%= Html.DropDownList("FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis", OE, new { @id = "FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("FollowUp_M1020PrimaryDiagnosisDate").Value(data.ContainsKey("M1020PrimaryDiagnosisDate") && data["M1020PrimaryDiagnosisDate"].Answer.IsNotNullOrEmpty() ? data["M1020PrimaryDiagnosisDate"].Answer : "").HtmlAttributes(new { @id = "FollowUp_M1020PrimaryDiagnosisDate", @class = "diagnosisDate date" })%>
                                    <div class="float_right oasis">
                                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1020');">?</div>
                                    </div>
                                </div>
                            </td><td>
                                <span class="alphali">a.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesA3", data.ContainsKey("M1024PaymentDiagnosesA3") ? data["M1024PaymentDiagnosesA3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesA3" })%>
                                <%= Html.TextBox("FollowUp_M1024ICD9MA3", data.ContainsKey("M1024ICD9MA3") ? data["M1024ICD9MA3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9MA3" })%>
                            </td><td>
                                <span class="alphali">a.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesA4", data.ContainsKey("M1024PaymentDiagnosesA4") ? data["M1024PaymentDiagnosesA4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesA4" }) %>
                                <%= Html.TextBox("FollowUp_M1024ICD9MA4", data.ContainsKey("M1024ICD9MA4") ? data["M1024ICD9MA4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9MA4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2"><label for="FollowUp_M1022PrimaryDiagnosis1" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1022');">(M1022)</a> Other Diagnoses</label></td>
                            <td></td>
                            <td></td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">b.</span>
                                    <%= Html.TextBox("FollowUp_M1022PrimaryDiagnosis1", data.ContainsKey("M1022PrimaryDiagnosis1") ? data["M1022PrimaryDiagnosis1"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1022PrimaryDiagnosis1" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">b.</span>
                                    <%=Html.TextBox("FollowUp_M1022ICD9M1", data.ContainsKey("M1022ICD9M1") ? data["M1022ICD9M1"].Answer : "", new { @class = "icd", @id = "FollowUp_M1022ICD9M1" }) %>
                                    <label for="FollowUp_M1022OtherDiagnose1Rating">Severity:</label>
                                    <%  var severity1 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose1Rating") ? data["M1022OtherDiagnose1Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("FollowUp_M1022OtherDiagnose1Rating", severity1, new { @id = "FollowUp_M1022OtherDiagnose1Rating", @class = "severity" }) %>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE1 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis1") ? data["485ExacerbationOrOnsetPrimaryDiagnosis1"].Answer : "0");%>
                                    <%= Html.DropDownList("FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis1", OE1, new { @id = "FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis1", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("FollowUp_M1022PrimaryDiagnosis1Date").Value(data.ContainsKey("M1022PrimaryDiagnosis1Date") && data["M1022PrimaryDiagnosis1Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis1Date"].Answer : "").HtmlAttributes(new { @id = "FollowUp_M1022PrimaryDiagnosis1Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">b.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesB3", data.ContainsKey("M1024PaymentDiagnosesB3") ? data["M1024PaymentDiagnosesB3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesB3" }) %>
                                <%= Html.TextBox("FollowUp_M1024ICD9MB3", data.ContainsKey("M1024ICD9MB3") ? data["M1024ICD9MB3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9MB3" })%>
                            </td><td>
                                <span class="alphali">b.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesB4", data.ContainsKey("M1024PaymentDiagnosesB4") ? data["M1024PaymentDiagnosesB4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesB4" }) %>
                                <%= Html.TextBox("FollowUp_M1024ICD9MB4", data.ContainsKey("M1024ICD9MB4") ? data["M1024ICD9MB4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9MB4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">c.</span>
                                    <%= Html.TextBox("FollowUp_M1022PrimaryDiagnosis2", data.ContainsKey("M1022PrimaryDiagnosis2") ? data["M1022PrimaryDiagnosis2"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1022PrimaryDiagnosis2" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">c.</span>
                                    <%=Html.TextBox("FollowUp_M1022ICD9M2", data.ContainsKey("M1022ICD9M2") ? data["M1022ICD9M2"].Answer : "", new { @class = "icd", @id = "FollowUp_M1022ICD9M2" }) %>
                                    <label for="FollowUp_M1022OtherDiagnose2Rating">Severity:</label>
                                    <%  var severity2 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose2Rating") ? data["M1022OtherDiagnose2Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("FollowUp_M1022OtherDiagnose2Rating", severity2, new { @id = "FollowUp_M1022OtherDiagnose2Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE2 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis2") ? data["485ExacerbationOrOnsetPrimaryDiagnosis2"].Answer : "0");%>
                                    <%= Html.DropDownList("FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis2", OE2, new { @id = "FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis2", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("FollowUp_M1022PrimaryDiagnosis2Date").Value(data.ContainsKey("M1022PrimaryDiagnosis2Date") && data["M1022PrimaryDiagnosis2Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis2Date"].Answer : "").HtmlAttributes(new { @id = "FollowUp_M1022PrimaryDiagnosis2Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">c.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesC3", data.ContainsKey("M1024PaymentDiagnosesC3") ? data["M1024PaymentDiagnosesC3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesC3" }) %>
                                <%= Html.TextBox("FollowUp_M1024ICD9MC3", data.ContainsKey("M1024ICD9MC3") ? data["M1024ICD9MC3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9MC3" })%>
                            </td><td>
                                <span class="alphali">c.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesC4", data.ContainsKey("M1024PaymentDiagnosesC4") ? data["M1024PaymentDiagnosesC4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesC4" }) %>
                                <%= Html.TextBox("FollowUp_M1024ICD9MC4", data.ContainsKey("M1024ICD9MC4") ? data["M1024ICD9MC4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9MC4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">d.</span>
                                    <%= Html.TextBox("FollowUp_M1022PrimaryDiagnosis3", data.ContainsKey("M1022PrimaryDiagnosis3") ? data["M1022PrimaryDiagnosis3"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1022PrimaryDiagnosis3" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">d.</span>
                                    <%=Html.TextBox("FollowUp_M1022ICD9M3", data.ContainsKey("M1022ICD9M3") ? data["M1022ICD9M3"].Answer : "", new { @class = "icd", @id = "FollowUp_M1022ICD9M3" }) %>
                                    <label for="FollowUp_M1022OtherDiagnose3Rating">Severity:</label>
                                    <%  var severity3 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose3Rating") ? data["M1022OtherDiagnose3Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("FollowUp_M1022OtherDiagnose3Rating", severity3, new { @id = "FollowUp_M1022OtherDiagnose3Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE3 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis3") ? data["485ExacerbationOrOnsetPrimaryDiagnosis3"].Answer : "0");%>
                                    <%= Html.DropDownList("FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis3", OE3, new { @id = "FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis3", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("FollowUp_M1022PrimaryDiagnosis3Date").Value(data.ContainsKey("M1022PrimaryDiagnosis3Date") && data["M1022PrimaryDiagnosis3Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis3Date"].Answer : "").HtmlAttributes(new { @id = "FollowUp_M1022PrimaryDiagnosis3Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">d.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesD3", data.ContainsKey("M1024PaymentDiagnosesD3") ? data["M1024PaymentDiagnosesD3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesD3" }) %>
                                <%= Html.TextBox("FollowUp_M1024ICD9MD3", data.ContainsKey("M1024ICD9MD3") ? data["M1024ICD9MD3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9MD3" })%>
                            </td><td>
                                <span class="alphali">d.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesD4", data.ContainsKey("M1024PaymentDiagnosesD4") ? data["M1024PaymentDiagnosesD4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesD4" }) %>
                                <%= Html.TextBox("FollowUp_M1024ICD9MD4", data.ContainsKey("M1024ICD9MD4") ? data["M1024ICD9MD4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9MD4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">e.</span>
                                    <%= Html.TextBox("FollowUp_M1022PrimaryDiagnosis4", data.ContainsKey("M1022PrimaryDiagnosis4") ? data["M1022PrimaryDiagnosis4"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1022PrimaryDiagnosis4" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">e.</span>
                                    <%=Html.TextBox("FollowUp_M1022ICD9M4", data.ContainsKey("M1022ICD9M4") ? data["M1022ICD9M4"].Answer : "", new { @class = "icd", @id = "FollowUp_M1022ICD9M4" }) %>
                                    <label for="FollowUp_M1022OtherDiagnose4Rating">Severity:</label>
                                    <%  var severity4 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose4Rating") ? data["M1022OtherDiagnose4Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("FollowUp_M1022OtherDiagnose4Rating", severity4, new { @id = "FollowUp_M1022OtherDiagnose4Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE4 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis4") ? data["485ExacerbationOrOnsetPrimaryDiagnosis4"].Answer : "0");%>
                                    <%= Html.DropDownList("FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis4", OE3, new { @id = "FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis4", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("FollowUp_M1022PrimaryDiagnosis4Date").Value(data.ContainsKey("M1022PrimaryDiagnosis4Date") && data["M1022PrimaryDiagnosis4Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis4Date"].Answer : "").HtmlAttributes(new { @id = "FollowUp_M1022PrimaryDiagnosis4Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">e.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesE3", data.ContainsKey("M1024PaymentDiagnosesE3") ? data["M1024PaymentDiagnosesE3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesE3" }) %>
                                <%= Html.TextBox("FollowUp_M1024ICD9ME3", data.ContainsKey("M1024ICD9ME3") ? data["M1024ICD9ME3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9ME3" })%>
                            </td><td>
                                <span class="alphali">e.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesE4", data.ContainsKey("M1024PaymentDiagnosesE4") ? data["M1024PaymentDiagnosesE4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesE4" }) %>
                                <%= Html.TextBox("FollowUp_M1024ICD9ME4", data.ContainsKey("M1024ICD9ME4") ? data["M1024ICD9ME4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9ME4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">f.</span>
                                    <%= Html.TextBox("FollowUp_M1022PrimaryDiagnosis5", data.ContainsKey("M1022PrimaryDiagnosis5") ? data["M1022PrimaryDiagnosis5"].Answer : "", new { @class = "diagnosis", @id = "FollowUp_M1022PrimaryDiagnosis5" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">f.</span>
                                    <%=Html.TextBox("FollowUp_M1022ICD9M5", data.ContainsKey("M1022ICD9M5") ? data["M1022ICD9M5"].Answer : "", new { @class = "icd", @id = "FollowUp_M1022ICD9M5" }) %>
                                    <label for="FollowUp_M1022OtherDiagnose5Rating">Severity:</label>
                                    <%  var severity5 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose5Rating") ? data["M1022OtherDiagnose5Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("FollowUp_M1022OtherDiagnose5Rating", severity5, new { @id = "FollowUp_M1022OtherDiagnose5Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE5 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis5") ? data["485ExacerbationOrOnsetPrimaryDiagnosis5"].Answer : "0");%>
                                    <%= Html.DropDownList("FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis5", OE3, new { @id = "FollowUp_485ExacerbationOrOnsetPrimaryDiagnosis5", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("FollowUp_M1022PrimaryDiagnosis5Date").Value(data.ContainsKey("M1022PrimaryDiagnosis5Date") && data["M1022PrimaryDiagnosis5Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis5Date"].Answer : "").HtmlAttributes(new { @id = "FollowUp_M1022PrimaryDiagnosis5Date", @class = "diagnosisDate date" })%>
                                </div>
                                <div class="float_right oasis">
                                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1022');">?</div>
                                </div>
                            </td><td>
                                <span class="alphali">f.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesF3", data.ContainsKey("M1024PaymentDiagnosesF3") ? data["M1024PaymentDiagnosesF3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesF3" }) %>
                                <%= Html.TextBox("FollowUp_M1024ICD9MF3", data.ContainsKey("M1024ICD9MF3") ? data["M1024ICD9MF3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9MF3" })%>
                                <div class="float_right oasis">
                                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1024');">?</div>
                                </div>
                            </td><td>
                                <span class="alphali">f.</span>
                                <%= Html.TextBox("FollowUp_M1024PaymentDiagnosesF4", data.ContainsKey("M1024PaymentDiagnosesF4") ? data["M1024PaymentDiagnosesF4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "FollowUp_M1024PaymentDiagnosesF4" }) %>
                                <%= Html.TextBox("FollowUp_M1024ICD9MF4", data.ContainsKey("M1024ICD9MF4") ? data["M1024ICD9MF4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "FollowUp_M1024ICD9MF4" })%>
                                <div class="float_right oasis">
                                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1024');">?</div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Therapies the Patient Receives at Home</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1030');">(M1030)</a> Therapies the patient receives at home (Mark all that apply)</label>
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <input name="FollowUp_M1030HomeTherapiesInfusion" value=" " type="hidden" />
                                <%= string.Format("<input name='FollowUp_M1030HomeTherapiesInfusion' value='1' class='radio float_left' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesInfusion") && data["M1030HomeTherapiesInfusion"].Answer == "1" ? "checked='checked'" : "" ) %>
                                <label for="FollowUp_M1030HomeTherapiesInfusion"><span class="float_left">1 &ndash;</span><span class="normal margin">Intravenous or infusion therapy (excludes TPN)</span></label>
                            </td><td>
                                <input name="FollowUp_M1030HomeTherapiesParNutrition" value=" " type="hidden" />
                                <%= string.Format("<input name='FollowUp_M1030HomeTherapiesParNutrition' value='1' class='radio float_left' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesParNutrition") && data["M1030HomeTherapiesParNutrition"].Answer == "1" ? "checked='checked'" : "" ) %>
                                <label for="FollowUp_M1030HomeTherapiesParNutrition"><span class="float_left">2 &ndash;</span><span class="normal margin">Parenteral nutrition (TPN or lipids)</span></label>
                            </td>
                        </tr><tr>
                            <td>
                                <input name="FollowUp_M1030HomeTherapiesEntNutrition" value=" " type="hidden" />
                                <%= string.Format("<input name='FollowUp_M1030HomeTherapiesEntNutrition' value='1' class='radio float_left' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesEntNutrition") && data["M1030HomeTherapiesEntNutrition"].Answer == "1" ? "checked='checked'" : "" ) %>
                                <label for="FollowUp_M1030HomeTherapiesEntNutrition"><span class="float_left">3 &ndash;</span><span class="normal margin">Enteral nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial entry into the alimentary canal)</span></label>
                            </td><td>
                                <input name="FollowUp_M1030HomeTherapiesNone" value=" " type="hidden" />
                                <%= string.Format("<input name='FollowUp_M1030HomeTherapiesNone' value='1' class='radio float_left' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesNone") && data["M1030HomeTherapiesNone"].Answer == "1" ? "checked='checked'" : "" ) %>
                                <label for="FollowUp_M1030HomeTherapiesNone"><span class="float_left">4 &ndash;</span><span class="normal margin">None of the above</span></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1030');">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.Validate('{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "FollowUp")%></li>
        </ul>
    </div>
</div>
<% } %>
