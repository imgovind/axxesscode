﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpMedicationForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("FollowUp_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<%= Html.Hidden("categoryType", "Medications")%> 
<div class="wrapper main">
  <fieldset id="soc_M2030" class="oasis">
        <legend>OASIS M2030</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2030');">(M2030)</a> Management of Injectable Medications: Patient&rsquo;s current ability to prepare and take all prescribed injectable medications reliably and safely, including administration of correct dosage at the appropriate times/intervals. Excludes IV medications.</label>
                <%=Html.Hidden("FollowUp_M2030ManagementOfInjectableMedications", " ", new { @id = "" })%>
                <div class="margin">
                    <div>
                        <%=Html.RadioButton("FollowUp_M2030ManagementOfInjectableMedications", "00", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "00" ? true : false, new { @id = "FollowUp_M2030ManagementOfInjectableMedications0", @class = "radio float_left" })%>
                        <label for="FollowUp_M2030ManagementOfInjectableMedications0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently take the correct medication(s) and proper dosage(s) at the correct times.</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("FollowUp_M2030ManagementOfInjectableMedications", "01", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "01" ? true : false, new { @id = "FollowUp_M2030ManagementOfInjectableMedications1", @class = "radio float_left" })%>
                        <label for="FollowUp_M2030ManagementOfInjectableMedications1">
                            <span class="float_left">1 &ndash;</span>
                            <span class="normal margin">Able to take injectable medication(s) at the correct times if:
                                <ul>
                                    <li><span class="float_left">(a)</span><span class="radio">individual syringes are prepared in advance by another person; OR</span></li>
                                    <li><span class="float_left">(b)</span><span class="radio">another person develops a drug diary or chart.</span></li>
                                </ul>
                            </span>
                        </label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("FollowUp_M2030ManagementOfInjectableMedications", "02", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "02" ? true : false, new { @id = "FollowUp_M2030ManagementOfInjectableMedications2", @class = "radio float_left" })%>
                        <label for="FollowUp_M2030ManagementOfInjectableMedications2"><span class="float_left">2 &ndash;</span><span class="normal margin">Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("FollowUp_M2030ManagementOfInjectableMedications", "03", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "03" ? true : false, new { @id = "FollowUp_M2030ManagementOfInjectableMedications3", @class = "radio float_left" })%>
                        <label for="FollowUp_M2030ManagementOfInjectableMedications3"><span class="float_left">3 &ndash;</span><span class="normal margin">Unable to take injectable medication unless administered by another person.</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2030');">?</div>
                        </div>
                        <%=Html.RadioButton("FollowUp_M2030ManagementOfInjectableMedications", "NA", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "NA" ? true : false, new { @id = "FollowUp_M2030ManagementOfInjectableMedications4", @class = "radio float_left" })%>
                        <label for="FollowUp_M2030ManagementOfInjectableMedications4"><span class="float_left">NA &ndash;</span><span class="normal margin">No injectable medications prescribed.</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
<div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.Validate('{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "StartOfCare")%></li>
        </ul>
    </div>
</div>
<% } %>