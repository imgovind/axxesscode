﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpEliminationForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("FollowUp_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<%= Html.Hidden("categoryType", "Elimination")%>
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS M1610</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1610');">(M1610)</a> Urinary Incontinence or Urinary Catheter Presence:<%=Html.Hidden("FollowUp_M1610UrinaryIncontinence", " ", new { @id = "" })%>
                </div>
                <div class="margin">
                <div>
                    <%=Html.RadioButton("FollowUp_M1610UrinaryIncontinence", "00", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "00" ? true : false, new { @id = "FollowUp_M1610UrinaryIncontinence0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1610UrinaryIncontinence0"><span class="float_left">0 &ndash;</span><span class="normal margin">No incontinence or catheter (includes anuria or ostomy for urinary drainage) <em>[Go to M1620]</em></span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1610UrinaryIncontinence", "01", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "01" ? true : false, new { @id = "FollowUp_M1610UrinaryIncontinence1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1610UrinaryIncontinence1"><span class="float_left">1 &ndash;</span><span class="normal margin">Patient is incontinent</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1610');">?</div>
                    </div>
                    <%=Html.RadioButton("FollowUp_M1610UrinaryIncontinence", "02", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "02" ? true : false, new { @id = "FollowUp_M1610UrinaryIncontinence2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1610UrinaryIncontinence2"><span class="float_left">2 &ndash;</span><span class="normal margin">Patient requires a urinary catheter (i.e., external, indwelling, intermittent, suprapubic) <em>[Go to M1620]</em></span></label>
                </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M1620/M1630</legend>
        <div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1620');">(M1620)</a> Bowel Incontinence Frequency:
                    <%=Html.Hidden("FollowUp_M1620BowelIncontinenceFrequency")%>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "00", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "00" ? true : false, new { @id = "FollowUp_M1620BowelIncontinenceFrequency0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1620BowelIncontinenceFrequency0"><span class="float_left">0 &ndash;</span><span class="normal margin">Very rarely or never has bowel incontinence</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "01", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "01" ? true : false, new { @id = "FollowUp_M1620BowelIncontinenceFrequency1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1620BowelIncontinenceFrequency1"><span class="float_left">1 &ndash;</span><span class="normal margin">Less than once weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "02", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "02" ? true : false, new { @id = "FollowUp_M1620BowelIncontinenceFrequency2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1620BowelIncontinenceFrequency2"><span class="float_left">2 &ndash;</span><span class="normal margin">One to three times weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "03", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "03" ? true : false, new { @id = "FollowUp_M1620BowelIncontinenceFrequency3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1620BowelIncontinenceFrequency3"><span class="float_left">3 &ndash;</span><span class="normal margin">Four to six times weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "04", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "04" ? true : false, new { @id = "FollowUp_M1620BowelIncontinenceFrequency4", @class = "radio float_left" })%>
                    <label for="FollowUp_M1620BowelIncontinenceFrequency4"><span class="float_left">4 &ndash;</span><span class="normal margin">On a daily basis</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "05", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "05" ? true : false, new { @id = "FollowUp_M1620BowelIncontinenceFrequency5", @class = "radio float_left" })%>
                    <label for="FollowUp_M1620BowelIncontinenceFrequency5"><span class="float_left">5 &ndash;</span><span class="normal margin">More often than once daily</span></label>
                    <div class="clear"></div>
                </div><div>
                <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1620');">?</div>
                    </div>
                    <%=Html.RadioButton("FollowUp_M1620BowelIncontinenceFrequency", "NA", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "NA" ? true : false, new { @id = "FollowUp_M1620BowelIncontinenceFrequencyNA", @class = "radio float_left" })%>
                    <label for="FollowUp_M1620BowelIncontinenceFrequencyNA"><span class="float_left">NA &ndash; </span><span class="normal margin">Patient has ostomy for bowel elimination</span></label>
                    <div class="clear"></div>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1630');">(M1630)</a> Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that (within the last 14 days): a) was related to an inpatient facility stay, or b) necessitated a change in medical or treatment regimen?
                    <%=Html.Hidden("FollowUp_M1630OstomyBowelElimination")%>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1630OstomyBowelElimination", "00", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "00" ? true : false, new { @id = "FollowUp_M1630OstomyBowelElimination0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1630OstomyBowelElimination0"><span class="float_left">0 &ndash;</span><span class="normal margin">Patient does not have an ostomy for bowel elimination.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1630OstomyBowelElimination", "01", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "01" ? true : false, new { @id = "FollowUp_M1630OstomyBowelElimination1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1630OstomyBowelElimination1"><span class="float_left">1 &ndash;</span><span class="normal margin">Patient&rsquo;s ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1630');">?</div>
                    </div>
                    <%=Html.RadioButton("FollowUp_M1630OstomyBowelElimination", "02", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "02" ? true : false, new { @id = "FollowUp_M1630OstomyBowelElimination2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1630OstomyBowelElimination2"><span class="float_left">2 &ndash;</span><span class="normal margin">The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.</span></label>
                </div>
            </div>
        </div>
    </fieldset>
  
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.Validate('{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "StartOfCare")%></li>
        </ul>
    </div>
</div>
<% } %>

