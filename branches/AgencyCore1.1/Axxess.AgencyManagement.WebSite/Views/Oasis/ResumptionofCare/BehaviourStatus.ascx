﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCareBehaviourialForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id)%>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit")%>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("ResumptionOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare")%>
<%= Html.Hidden("categoryType", "Behavioral")%>
<div class="wrapper main">
    <fieldset>
        <legend>Neurological</legend>
        <div class="column">
            <div class="row">
                <%string[] genericNeurologicalStatus = data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer != "" ? data["GenericNeurologicalStatus"].Answer.Split(',') : null; %>
                <input type="hidden" name="ResumptionOfCare_GenericNeurologicalStatus" value=" " />
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalStatus1' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalStatus' value='1' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericNeurologicalStatus1">LOC:</label>
                </div><div id="ResumptionOfCare_GenericNeurologicalStatus1More" class="rel float_right">
                    <div class="float_right">
                        <%  var LOC = new SelectList(new[]{
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "Alert", Value = "1" },
                                new SelectListItem { Text = "Lethargic", Value = "2" },
                                new SelectListItem { Text = "Comatose", Value = "3" },
                                new SelectListItem { Text = "Disoriented", Value = "4" },
                                new SelectListItem { Text = "Forgetful", Value = "5"},
                                new SelectListItem { Text = "Other", Value = "6" }
                            }, "Value", "Text", data.ContainsKey("GenericNeurologicalLOC") ? data["GenericNeurologicalLOC"].Answer : "0");%>
                        <%= Html.DropDownList("ResumptionOfCare_GenericNeurologicalLOC", LOC, new { @id = "ResumptionOfCare_GenericNeurologicalLOC" })%>
                    </div>
                    <div class="clear"></div>
                    <div class="float_right">
                        <label>Oriented to:</label>
                        <% string[] genericNeurologicalOriented = data.ContainsKey("GenericNeurologicalOriented") && data["GenericNeurologicalOriented"].Answer != "" ? data["GenericNeurologicalOriented"].Answer.Split(',') : null; %>
                        <input type="hidden" name="ResumptionOfCare_GenericNeurologicalOriented" value=" " />
                        <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalOriented1' class='radio' name='ResumptionOfCare_GenericNeurologicalOriented' value='1' type='checkbox' {0} />", genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("1")  ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericNeurologicalOriented1" class="inlineradio">Person</label>
                        <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalOriented2' class='radio' name='ResumptionOfCare_GenericNeurologicalOriented' value='2' type='checkbox' {0} />", genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("2")  ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericNeurologicalOriented2" class="inlineradio">Place</label>
                        <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalOriented3' class='radio' name='ResumptionOfCare_GenericNeurologicalOriented' value='3' type='checkbox' {0} />", genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("3")  ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericNeurologicalOriented3" class="inlineradio">Time</label>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalStatus2' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalStatus' value='2' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericNeurologicalStatus2">Pupils:</label>
                </div><div id="ResumptionOfCare_GenericNeurologicalStatus2More" class="rel float_right">
                    <div class="float_right">
                        <%  var pupils = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "PERRLA/WNL", Value = "1" },
                                new SelectListItem { Text = "Sluggish", Value = "2" },
                                new SelectListItem { Text = "Non-Reactive", Value = "3" },
                                new SelectListItem { Text = "Other", Value = "4" }
                            }, "Value", "Text", data.ContainsKey("GenericNeurologicalPupils") ? data["GenericNeurologicalPupils"].Answer : "0");%>
                        <%= Html.DropDownList("ResumptionOfCare_GenericNeurologicalPupils", pupils, new { @id = "ResumptionOfCare_GenericNeurologicalPupils" })%>
                    </div>
                    <div class="clear"></div>
                    <div class="float_right">
                        <%= Html.RadioButton("ResumptionOfCare_GenericNeurologicalPupilsPosition", "0", data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "0" ? true : false, new { @id = "GenericNeurologicalPupilsPosition0", @class = "radio" })%>
                        <label for="GenericNeurologicalPupilsPosition0" class="inlineradio">Bilateral</label>
                        <%= Html.RadioButton("ResumptionOfCare_GenericNeurologicalPupilsPosition", "1", data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "1" ? true : false, new { @id = "GenericNeurologicalPupilsPosition1", @class = "radio" })%>
                        <label for="GenericNeurologicalPupilsPosition1" class="inlineradio">Left</label>
                        <%= Html.RadioButton("ResumptionOfCare_GenericNeurologicalPupilsPosition", "2", data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "2" ? true : false, new { @id = "GenericNeurologicalPupilsPosition2", @class = "radio" })%>
                        <label for="GenericNeurologicalPupilsPosition2" class="inlineradio">Right</label>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalStatus3' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalStatus' value='3' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericNeurologicalStatus3">Vision:</label>
                </div><div id="ResumptionOfCare_GenericNeurologicalStatus3More" class="float_right">
                    <% string[] genericNeurologicalVisionStatus = data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer != "" ? data["GenericNeurologicalVisionStatus"].Answer.Split(',') : null; %>
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalVisionStatus1' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalVisionStatus' value='1' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("1") ? "checked='checked'" : "")%>
                        <label for="ResumptionOfCare_GenericNeurologicalVisionStatus1" class="fixed inlineradio">WNL</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalVisionStatus2' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalVisionStatus' value='2' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("2") ? "checked='checked'" : "")%>
                        <label for="ResumptionOfCare_GenericNeurologicalVisionStatus2" class="fixed inlineradio">Blurred Vision</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalVisionStatus3' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalVisionStatus' value='3' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("3") ? "checked='checked'" : "")%>
                        <label for="ResumptionOfCare_GenericNeurologicalVisionStatus3" class="fixed inlineradio">Cataracts</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalVisionStatus4' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalVisionStatus' value='4' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("4") ? "checked='checked'" : "")%>
                        <label for="ResumptionOfCare_GenericNeurologicalVisionStatus4" class="fixed inlineradio">Corrective Lenses</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalVisionStatus5' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalVisionStatus' value='5' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("5") ? "checked='checked'" : "")%>
                        <label for="ResumptionOfCare_GenericNeurologicalVisionStatus5" class="fixed inlineradio">Glaucoma</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalVisionStatus6' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalVisionStatus' value='6' type='checkbox' {0} />", genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("6") ? "checked='checked'" : "")%>
                        <label for="ResumptionOfCare_GenericNeurologicalVisionStatus6" class="fixed inlineradio">Legally Blind</label>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalStatus4' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalStatus' value='4' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericNeurologicalStatus4">Speech:</label>
                </div><div class="float_right">
                    <%  var speech = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Clear", Value = "1" },
                            new SelectListItem { Text = "Slurred", Value = "2" },
                            new SelectListItem { Text = "Aphasic", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.ContainsKey("GenericNeurologicalSpeech") ? data["GenericNeurologicalSpeech"].Answer : "0");%>
                    <%= Html.DropDownList("ResumptionOfCare_GenericNeurologicalSpeech", speech, new { @id = "ResumptionOfCare_GenericNeurologicalSpeech" })%>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalStatus5' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalStatus' value='5' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("5") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericNeurologicalStatus5">Paralysis</label>
                </div><div id="ResumptionOfCare_GenericNeurologicalStatus5More" class="float_right">
                    <em>(location)</em>
                    <%=Html.TextBox("ResumptionOfCare_GenericNeurologicalParalysisLocation", data.ContainsKey("GenericNeurologicalParalysisLocation") ? data["GenericNeurologicalParalysisLocation"].Answer : "", new { @id = "ResumptionOfCare_GenericNeurologicalParalysisLocation", @class = "loc", @maxlength = "20" })%>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalStatus6' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalStatus' value='6' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericNeurologicalStatus6" class="radio">Quadripiegia</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalStatus7' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalStatus' value='7' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericNeurologicalStatus7" class="radio">Parapiegia</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalStatus8' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalStatus' value='8' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("8") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericNeurologicalStatus8" class="radio">Seizures</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalStatus9' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalStatus' value='9' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("9") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericNeurologicalStatus9" class="radio">Tremors</label>
                </div><div id="ResumptionOfCare_GenericNeurologicalStatus9More" class="float_right">
                    <em>(location)</em>
                    <%=Html.TextBox("ResumptionOfCare_GenericNeurologicalTremorsLocation", data.ContainsKey("GenericNeurologicalTremorsLocation") ? data["GenericNeurologicalTremorsLocation"].Answer : "", new { @id = "ResumptionOfCare_GenericNeurologicalTremorsLocation", @class = "loc", @maxlength = "20" })%>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalStatus10' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalStatus' value='10' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("10") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericNeurologicalStatus10" class="radio">Dizziness</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericNeurologicalStatus11' class='radio float_left' name='ResumptionOfCare_GenericNeurologicalStatus' value='11' type='checkbox' {0} />", genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("11") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericNeurologicalStatus11" class="radio">Headache</label>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericNeuroEmoBehaviorComments">Comments:</label>
                <%=Html.TextArea("ResumptionOfCare_GenericNeuroEmoBehaviorComments", data.ContainsKey("GenericNeuroEmoBehaviorComments") ? data["GenericNeuroEmoBehaviorComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_GenericNeuroEmoBehaviorComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="loc485">
        <legend>Mental Status (Locator #19)</legend>
        <% string[] mentalStatus = data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer != "" ? data["485MentalStatus"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485MentalStatus" value=" " />
        <div class="wide_column">
            <div class="row">
                <table class="form">
                    <tr>
                        <td>
                            <%= string.Format("<input id='ResumptionOfCare_485MentalStatus1' class='radio float_left' name='ResumptionOfCare_485MentalStatus' value='1' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("1") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485MentalStatus1" class="radio">Oriented</label>
                        </td><td>
                            <%= string.Format("<input id='ResumptionOfCare_485MentalStatus2' class='radio float_left' name='ResumptionOfCare_485MentalStatus' value='2' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("2") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485MentalStatus2" class="radio">Comatose</label>
                        </td><td>
                            <%= string.Format("<input id='ResumptionOfCare_485MentalStatus3' class='radio float_left' name='ResumptionOfCare_485MentalStatus' value='3' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("3") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485MentalStatus3" class="radio">Forgetful</label>
                        </td><td>
                            <%= string.Format("<input id='ResumptionOfCare_485MentalStatus4' class='radio float_left' name='ResumptionOfCare_485MentalStatus' value='7' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("7") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485MentalStatus4" class="radio">Agitated</label>
                        </td><td>
                            <%= string.Format("<input id='ResumptionOfCare_485MentalStatus5' class='radio float_left' name='ResumptionOfCare_485MentalStatus' value='4' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("4") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485MentalStatus5" class="radio">Depressed</label>
                        </td>
                    </tr><tr>
                        <td>
                            <%= string.Format("<input id='ResumptionOfCare_485MentalStatus6' class='radio float_left' name='ResumptionOfCare_485MentalStatus' value='5' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("5") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485MentalStatus6" class="radio">Disoriented</label>
                        </td><td>
                            <%= string.Format("<input id='ResumptionOfCare_485MentalStatus7' class='radio float_left' name='ResumptionOfCare_485MentalStatus' value='6' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("6") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485MentalStatus7" class="radio">Lethargic</label>
                        </td><td>
                            <%= string.Format("<input id='ResumptionOfCare_485MentalStatus8' class='radio float_left' name='ResumptionOfCare_485MentalStatus' value='8' type='checkbox' {0} />", mentalStatus!=null && mentalStatus.Contains("8") ? "checked='checked'" : "" ) %>
                            <label for="ResumptionOfCare_485MentalStatus8" class="radio more fixed">Other</label>
                        </td><td colspan="2">
                            <div id="ResumptionOfCare_485MentalStatus8More"><label for="ResumptionOfCare_485MentalStatusOther"><em>(Specify)</em></label><%=Html.TextBox("ResumptionOfCare_485MentalStatusOther", data.ContainsKey("485MentalStatusOther") ? data["485MentalStatusOther"].Answer : "", new { @id = "ResumptionOfCare_485MentalStatusOther", @class = "loc", @maxlength = "24" })%></div>
                        </td>
                    </tr><tr>
                        <td colspan="5">
                            <label for="ResumptionOfCare_485MentalStatusComments">Additional Orders <em>(specify)</em>:</label>
                            <%=Html.TextArea("ResumptionOfCare_485MentalStatusComments", data.ContainsKey("485MentalStatusComments") ? data["485MentalStatusComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485MentalStatusComments" })%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Behaviour Status</legend>
        <div class="column">
            <div class="row">
                <% string[] genericBehaviour = data.ContainsKey("GenericBehaviour") && data["GenericBehaviour"].Answer != "" ? data["GenericBehaviour"].Answer.Split(',') : null; %>
                <input type="hidden" name="ResumptionOfCare_GenericPsychosocial" value="'" />
                <div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericBehaviour1' class='radio float_left' name='ResumptionOfCare_GenericBehaviour' value='1' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericBehaviour1" class="radio">WNL (Within Normal Limits)</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericBehaviour2' class='radio float_left' name='ResumptionOfCare_GenericBehaviour' value='2' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("2") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericBehaviour2" class="radio">Withdrawn</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericBehaviour3' class='radio float_left' name='ResumptionOfCare_GenericBehaviour' value='3' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("3") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericBehaviour3" class="radio">Expresses Depression</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericBehaviour4' class='radio float_left' name='ResumptionOfCare_GenericBehaviour' value='4' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("4") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericBehaviour4" class="radio">Impaired Decision Making</label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericBehaviour5' class='radio float_left' name='ResumptionOfCare_GenericBehaviour' value='5' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("5") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericBehaviour5" class="radio">Difficulty coping w/ Altered Health Status</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericBehaviour6' class='radio float_left' name='ResumptionOfCare_GenericBehaviour' value='6' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("6") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericBehaviour6" class="radio">Combative</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericBehaviour7' class='radio float_left' name='ResumptionOfCare_GenericBehaviour' value='7' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("7") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericBehaviour7" class="radio">Irritability</label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericBehaviour8' class='radio float_left' name='ResumptionOfCare_GenericBehaviour' value='8' type='checkbox' {0} />", genericBehaviour != null && genericBehaviour.Contains("8") ? "checked='checked'" : "")%>
                        <label for="ResumptionOfCare_GenericBehaviour8" class="radio">Other</label>
                    </div><div id="ResumptionOfCare_GenericBehaviour8More" class="float_right">
                        <label for="ResumptionOfCare_GenericBehaviourOther"><em>(Specify)</em></label>
                        <%=Html.TextBox("ResumptionOfCare_GenericBehaviourOther", data.ContainsKey("GenericBehaviourOther") ? data["GenericBehaviourOther"].Answer : "", new { @id = "ResumptionOfCare_GenericBehaviourOther", @maxlength = "20" })%>
                    </div>
                </div>
            </div>
        </div><div class="wide_column">
            <div class="row">
                <label for="ResumptionOfCare_GenericBehaviourComments">Comments:</label>
                <%=Html.TextArea("ResumptionOfCare_GenericBehaviourComments", data.ContainsKey("GenericBehaviourComments") ? data["GenericBehaviourComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_GenericBehaviourComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="half float_left">
        <legend>Psychosocial</legend>
        <% string[] genericPsychosocial = data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer != "" ? data["GenericPsychosocial"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_GenericPsychosocial" value=" " />
        <div class="column">
            <div class="row">
                <div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericPsychosocial1' class='radio float_left' name='ResumptionOfCare_GenericPsychosocial' value='1' type='checkbox' {0} />", genericPsychosocial!=null && genericPsychosocial.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericPsychosocial1" class="radio">WNL ( No Issues Identified )</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericPsychosocial2' class='radio float_left' name='ResumptionOfCare_GenericPsychosocial' value='2' type='checkbox' {0} />", genericPsychosocial!=null && genericPsychosocial.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericPsychosocial2" class="radio">Poor Home Environment</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericPsychosocial3' class='radio float_left' name='ResumptionOfCare_GenericPsychosocial' value='3' type='checkbox' {0} />", genericPsychosocial!=null && genericPsychosocial.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericPsychosocial3" class="radio">No emotional support from family/CG</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericPsychosocial4' class='radio float_left' name='ResumptionOfCare_GenericPsychosocial' value='4' type='checkbox' {0} />", genericPsychosocial!=null && genericPsychosocial.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericPsychosocial4" class="radio">Verbalize desire for spiritual support</label>
                    <div class="clear"></div>
                </div><div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericPsychosocial5' class='radio float_left' name='ResumptionOfCare_GenericPsychosocial' value='5' type='checkbox' {0} />", genericPsychosocial != null && genericPsychosocial.Contains("5") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericPsychosocial5" class="radio">Other</label>
                    <div class="clear"></div>
                </div><div id="ResumptionOfCare_GenericPsychosocial5More" class="float_right">
                    <label for="ResumptionOfCare_GenericPsychosocialOther"><em>(Specify)</em></label>
                    <%=Html.TextBox("ResumptionOfCare_GenericPsychosocialOther", data.ContainsKey("GenericPsychosocialOther") ? data["GenericPsychosocialOther"].Answer : "", new { @id = "ResumptionOfCare_GenericPsychosocialOther", @maxlength = "20" })%>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericPsychosocialComments">Comments:</label>
                <%=Html.TextArea("ResumptionOfCare_GenericPsychosocialComments", data.ContainsKey("GenericPsychosocialComments") ? data["GenericPsychosocialComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_GenericPsychosocialComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Patient Strengths</legend>
        <% string[] patientStrengths = data.ContainsKey("485PatientStrengths") && data["485PatientStrengths"].Answer != "" ? data["485PatientStrengths"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485PatientStrengths" value="" />
        <div class="column">
            <div class="row">
                <div>
                    <%= string.Format("<input id='ResumptionOfCare_485PatientStrengths1' class='radio float_left' name='ResumptionOfCare_485PatientStrengths' value='1' type='checkbox' {0} />", patientStrengths!=null && patientStrengths.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_485PatientStrengths1" class="radio">Motivated Learner</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_485PatientStrengths2' class='radio float_left' name='ResumptionOfCare_485PatientStrengths' value='2' type='checkbox' {0} />", patientStrengths!=null && patientStrengths.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_485PatientStrengths2" class="radio">Strong Support System</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_485PatientStrengths3' class='radio float_left' name='ResumptionOfCare_485PatientStrengths' value='3' type='checkbox' {0} />", patientStrengths!=null && patientStrengths.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_485PatientStrengths3" class="radio">Absence of Multiple Diagnosis</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_485PatientStrengths4' class='radio float_left' name='ResumptionOfCare_485PatientStrengths' value='4' type='checkbox' {0} />", patientStrengths!=null && patientStrengths.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_485PatientStrengths4" class="radio">Enhanced Socioeconomic Status</label>
                    <div class="clear"></div>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_485PatientStrengthOther" class="float_left">Other <em>(specify)</em>:</label>
                <div class="float_right">
                    <%=Html.TextBox("ResumptionOfCare_485PatientStrengthOther", data.ContainsKey("485PatientStrengthOther") ? data["485PatientStrengthOther"].Answer : "", new { @id = "ResumptionOfCare_485PatientStrengthOther", @maxlength = "70" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1700');">(M1700)</a> Cognitive Functioning: Patient&rsquo;s current (day of assessment) level of alertness, orientation, comprehension, concentration, and immediate memory for simple commands.</label>
                <%=Html.Hidden("ResumptionOfCare_M1700CognitiveFunctioning")%>
                <div>
                    <%=Html.RadioButton("ResumptionOfCare_M1700CognitiveFunctioning", "00", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1700CognitiveFunctioning0", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1700CognitiveFunctioning0"><span class="float_left">0 &ndash;</span><span class="normal margin">Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1700CognitiveFunctioning", "01", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1700CognitiveFunctioning1", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1700CognitiveFunctioning1"><span class="float_left">1 &ndash;</span><span class="normal margin">Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1700CognitiveFunctioning", "02", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1700CognitiveFunctioning2", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1700CognitiveFunctioning2"><span class="float_left">2 &ndash;</span><span class="normal margin">Requires assistance and some direction in specific situations (e.g., on all tasks involving shifting of attention), or consistently requires low stimulus environment due to distractibility.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1700CognitiveFunctioning", "03", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "03" ? true : false, new { @id = "ResumptionOfCare_M1700CognitiveFunctioning3", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1700CognitiveFunctioning3"><span class="float_left">3 &ndash;</span><span class="normal margin">Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift attention and recall directions more than half the time.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1700');">?</div>
                    </div>
                    <%=Html.RadioButton("ResumptionOfCare_M1700CognitiveFunctioning", "04", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "04" ? true : false, new { @id = "ResumptionOfCare_M1700CognitiveFunctioning4", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1700CognitiveFunctioning4"><span class="float_left">4 &ndash;</span><span class="normal margin">Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1710');">(M1710)</a> When Confused (Reported or Observed Within the Last 14 Days):</label>
                <%=Html.Hidden("ResumptionOfCare_M1710WhenConfused")%>
                <div>
                    <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "00", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1710WhenConfused0", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1710WhenConfused0"><span class="float_left">0 &ndash;</span><span class="normal margin">Never</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "01", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1710WhenConfused1", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1710WhenConfused1"><span class="float_left">1 &ndash;</span><span class="normal margin">In new or complex situations only</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "02", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1710WhenConfused2", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1710WhenConfused2"><span class="float_left">2 &ndash;</span><span class="normal margin">On awakening or at night only</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "03", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "03" ? true : false, new { @id = "ResumptionOfCare_M1710WhenConfused3", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1710WhenConfused3"><span class="float_left">3 &ndash;</span><span class="normal margin">During the day and evening, but not constantly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "04", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "04" ? true : false, new { @id = "ResumptionOfCare_M1710WhenConfused4", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1710WhenConfused4"><span class="float_left">4 &ndash;</span><span class="normal margin">Constantly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1710');">?</div>
                    </div>
                    <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "NA", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "NA" ? true : false, new { @id = "ResumptionOfCare_M1710WhenConfusedNA", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1710WhenConfusedNA"><span class="float_left">NA &ndash;</span><span class="normal margin">Patient nonresponsive</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1720');">(M1720)</a> When Anxious (Reported or Observed Within the Last 14 Days):</label>
                <%=Html.Hidden("ResumptionOfCare_M1720WhenAnxious")%>
                <div>
                    <%=Html.RadioButton("ResumptionOfCare_M1720WhenAnxious", "00", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1720WhenAnxious0", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1720WhenAnxious0"><span class="float_left">0 &ndash;</span><span class="normal margin">None of the time</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1720WhenAnxious", "01", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1720WhenAnxious1", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1720WhenAnxious1"><span class="float_left">1 &ndash;</span><span class="normal margin">Less often than daily</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1720WhenAnxious", "02", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1720WhenAnxious2", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1720WhenAnxious2"><span class="float_left">2 &ndash;</span><span class="normal margin">Daily, but not constantly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1720WhenAnxious", "03", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "03" ? true : false, new { @id = "ResumptionOfCare_M1720WhenAnxious3", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1720WhenAnxious3"><span class="float_left">3 &ndash;</span><span class="normal margin">All of the time</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1720');">?</div>
                    </div>
                    <%=Html.RadioButton("ResumptionOfCare_M1720WhenAnxious", "NA", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "NA" ? true : false, new { @id = "ResumptionOfCare_M1720WhenAnxiousNA", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1720WhenAnxiousNA"><span class="float_left">NA &ndash;</span><span class="normal margin">Patient nonresponsive</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1730');">(M1730)</a> Depression Screening: Has the patient been screened for depression, using a standardized depression screening tool?</label>
                <%=Html.Hidden("ResumptionOfCare_M1730DepressionScreening")%>
                <div>
                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreening", "00", data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1730DepressionScreening0", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1730DepressionScreening0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreening", "01", data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1730DepressionScreening1", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1730DepressionScreening1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes, patient was screened using the PHQ-2&copy;* scale. (Instructions for this two-question tool: Ask patient: &ldquo;Over the last two weeks, how often have you been bothered by any of the following problems&rdquo;)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreening", "02", data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1730DepressionScreening2", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1730DepressionScreening2"><span class="float_left">2 &ndash;</span><span class="normal margin">Yes, with a different standardized assessment-and the patient meets criteria for further evaluation for depression.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1730');">?</div>
                    </div>
                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreening", "03", data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "03" ? true : false, new { @id = "ResumptionOfCare_M1730DepressionScreening3", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1730DepressionScreening3"><span class="float_left">3 &ndash;</span><span class="normal margin">Yes, patient was screened with a different standardized assessment-and the patient does not meet criteria for further evaluation for depression.</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1740');">(M1740)</a> Cognitive, behavioral, and psychiatric symptoms that are demonstrated at least once a week (Reported or Observed): (Mark all that apply.)</label>
                <div>
                    <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit" value="" type="hidden" />
                    <%= string.Format("<input id='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit' class='radio float_left M1740' name='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"><span class="float_left">1 &ndash;</span><span class="normal margin">Memory deficit: failure to recognize familiar persons/places, inability to recall events of past 24 hours, significant memory loss so that supervision is required</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsImpDes" value="" type="hidden" />
                    <%= string.Format("<input id='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsImpDes' class='radio float_left M1740' name='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsImpDes' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes") && data["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsImpDes"><span class="float_left">2 &ndash;</span><span class="normal margin">Impaired decision-making: failure to perform usual ADLs or IADLs, inability to appropriately stop activities, jeopardizes safety through actions</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsVerbal" value="" type="hidden" />
                    <%= string.Format("<input id='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsVerbal' class='radio float_left M1740' name='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsVerbal' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal") && data["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsVerbal"><span class="float_left">3 &ndash;</span><span class="normal margin">Verbal disruption: yelling, threatening, excessive profanity, sexual references, etc.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsPhysical" value="" type="hidden" />
                    <%= string.Format("<input id='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsPhysical' class='radio float_left M1740' name='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsPhysical' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical") && data["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsPhysical"><span class="float_left">4 &ndash;</span><span class="normal margin">Physical aggression: aggressive or combative to self and others (e.g., hits self, throws objects, punches, dangerous maneuvers with wheelchair or other objects)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsSIB" value="" type="hidden" />
                    <%= string.Format("<input id='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsSIB' class='radio float_left M1740' name='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsSIB' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB") && data["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsSIB"><span class="float_left">5 &ndash;</span><span class="normal margin">Disruptive, infantile, or socially inappropriate behavior (excludes verbal actions)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsDelusional" value="" type="hidden" />
                    <%= string.Format("<input id='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsDelusional' class='radio float_left M1740' name='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsDelusional' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional") && data["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsDelusional"><span class="float_left">6 &ndash;</span><span class="normal margin">Delusional, hallucinatory, or paranoid behavior</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1740');">?</div>
                    </div>
                    <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsNone" value="" type="hidden" />
                    <%= string.Format("<input id='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsNone' class='radio float_left M1740' name='ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsNone' value='1' type='checkbox' {0} />", data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsNone") && data["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsNone"><span class="float_left">7 &ndash;</span><span class="normal margin">None of the above behaviors demonstrated</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1745');">(M1745)</a> Frequency of Disruptive Behavior Symptoms (Reported or Observed) Any physical, verbal, or other disruptive/dangerous symptoms that are injurious to self or others or jeopardize personal safety.</label>
                <%=Html.Hidden("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency")%>
                <div>
                    <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "00", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "00" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency0", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency0"><span class="float_left">0 &ndash;</span><span class="normal margin">Never</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "01", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "01" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency1", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency1"><span class="float_left">1 &ndash;</span><span class="normal margin">Less than once a month</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "02", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "02" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency2", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency2"><span class="float_left">2 &ndash;</span><span class="normal margin">Once a month</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "03", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "03" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency3", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency3"><span class="float_left">3 &ndash;</span><span class="normal margin">Several times each month</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "04", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "04" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency4", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency4"><span class="float_left">4 &ndash;</span><span class="normal margin">Several times a week</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1745');">?</div>
                    </div>
                    <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "05", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "05" ? true : false, new { @id = "M1745DisruptiveBehaviorSymptomsFrequency5", @class = "radio float_left" })%>
                    <label for="M1745DisruptiveBehaviorSymptomsFrequency5"><span class="float_left">5 &ndash;</span><span class="normal margin">At least daily</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1750');">(M1750)</a> Is this patient receiving Psychiatric Nursing Services at home provided by a qualified psychiatric nurse?</label>
                <%=Html.Hidden("ResumptionOfCare_M1750PsychiatricNursingServicing")%>
                <div>
                    <%=Html.RadioButton("ResumptionOfCare_M1750PsychiatricNursingServicing", "0", data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "0" ? true : false, new { @id = "M1750PsychiatricNursingServicing0", @class = "radio float_left" })%>
                    <label for="M1750PsychiatricNursingServicing0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1750');">?</div>
                    </div>
                    <%=Html.RadioButton("ResumptionOfCare_M1750PsychiatricNursingServicing", "1", data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "1" ? true : false, new { @id = "M1750PsychiatricNursingServicing1", @class = "radio float_left" })%>
                    <label for="M1750PsychiatricNursingServicing1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="phq">
        <legend>PHQ-2&copy;*</legend>
        <div class="wide_column">
            <div class="row">
                <table class="form bordergrid">
                    <thead>
                        <tr>
                            <th></th>
                            <th>
                                Not at all<br />
                                (0 &ndash; 1 day)
                            </th><th>
                                Several days<br />
                                (2 &ndash; 6 days)
                            </th><th>
                                More than half of the days<br />
                                (7 &ndash; 11 days)
                            </th><th>
                                Nearly every day<br />
                                (12 &ndash; 14 days)
                            </th><th>
                                N/A Unable to respond
                            </th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td>
                                a) Little interest or pleasure in doing things
                                <%=Html.Hidden("ResumptionOfCare_M1730DepressionScreeningInterest")%>
                            </td><td>
                                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningInterest", "00", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "00" ? true : false, new { @id = "M1730DepressionScreeningInterest0", @class = "radio float_left" })%>
                                <label for="M1730DepressionScreeningInterest0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningInterest", "01", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "01" ? true : false, new { @id = "M1730DepressionScreeningInterest1", @class = "radio float_left" })%>
                                <label for="M1730DepressionScreeningInterest1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningInterest", "02", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "02" ? true : false, new { @id = "M1730DepressionScreeningInterest2", @class = "radio float_left" })%>
                                <label for="M1730DepressionScreeningInterest2" class="radio">2</label>
                            </td><td>
                                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningInterest", "03", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "03" ? true : false, new { @id = "M1730DepressionScreeningInterest3", @class = "radio float_left" })%>
                                <label for="M1730DepressionScreeningInterest3" class="radio">3</label>
                            </td><td>
                                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningInterest", "NA", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "NA" ? true : false, new { @id = "M1730DepressionScreeningInterestNA", @class = "radio float_left" })%>
                                <label for="M1730DepressionScreeningInterestNA" class="radio">NA</label>
                            </td>
                        </tr><tr>
                            <td>
                                b) Feeling down, depressed, or hopeless?
                                <%=Html.Hidden("ResumptionOfCare_M1730DepressionScreeningHopeless")%>
                            </td><td>
                                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningHopeless", "00", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1730DepressionScreeningHopeless0", @class = "radio float_left" })%>
                                <label for="ResumptionOfCare_M1730DepressionScreeningHopeless0" class="radio">0</label>
                            </td><td>
                                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningHopeless", "01", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1730DepressionScreeningHopeless1", @class = "radio float_left" })%>
                                <label for="ResumptionOfCare_M1730DepressionScreeningHopeless1" class="radio">1</label>
                            </td><td>
                                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningHopeless", "02", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1730DepressionScreeningHopeless2", @class = "radio float_left" })%>
                                <label for="ResumptionOfCare_M1730DepressionScreeningHopeless2" class="radio">2</label>
                            </td><td>
                                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningHopeless", "03", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "03" ? true : false, new { @id = "ResumptionOfCare_M1730DepressionScreeningHopeless3", @class = "radio float_left" })%>
                                <label for="ResumptionOfCare_M1730DepressionScreeningHopeless3" class="radio">3</label>
                            </td><td>
                                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningHopeless", "NA", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "NA" ? true : false, new { @id = "ResumptionOfCare_M1730DepressionScreeningHopelessNA", @class = "radio float_left" })%>
                                <label for="ResumptionOfCare_M1730DepressionScreeningHopelessNA" class="radio">NA</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right"><em>Copyright© Pfizer Inc. All rights reserved. Reproduced with permission.</em></div>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] behaviorInterventions = data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer != "" ? data["485BehaviorInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485BehaviorInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorInterventions1' class='radio float_left' name='ResumptionOfCare_485BehaviorInterventions' value='1' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485BehaviorInterventions1" class="radio">SN to notify physicain this pateint was screened for depression using PHQ-2 scale and meets criteria for further evaluation for depression.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorInterventions2' class='radio float_left' name='ResumptionOfCare_485BehaviorInterventions' value='2' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485BehaviorInterventions2" class="radio">SN to perform a neurological assessment each visit.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorInterventions3' class='radio float_left' name='ResumptionOfCare_485BehaviorInterventions' value='3' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485BehaviorInterventions3" class="radio">SN to assess/instruct on seizure disorder signs &amp; symptoms and appropriate actions during seizure activity.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorInterventions4' class='radio float_left' name='ResumptionOfCare_485BehaviorInterventions' value='4' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485BehaviorInterventions4">SN to instruct the</label>
                    <%  var instructSeizurePrecautionPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485InstructSeizurePrecautionPerson") && data["485InstructSeizurePrecautionPerson"].Answer != "" ? data["485InstructSeizurePrecautionPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructSeizurePrecautionPerson", instructSeizurePrecautionPerson)%>
                    <label for="ResumptionOfCare_485BehaviorInterventions4">on seizure precautions.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorInterventions5' class='radio float_left' name='ResumptionOfCare_485BehaviorInterventions' value='5' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485BehaviorInterventions5" class="radio">SN to instruct caregiver on orientation techniques to use when patient becomes disoriented.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorInterventions6' class='radio float_left' name='ResumptionOfCare_485BehaviorInterventions' value='6' type='checkbox' {0} />", behaviorInterventions!=null && behaviorInterventions.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485BehaviorInterventions6">MSW:</label>
                    <%=Html.Hidden("ResumptionOfCare_485MSWCommunityAssistanceVisits")%>
                    <%=Html.RadioButton("ResumptionOfCare_485MSWCommunityAssistanceVisits", "1", data.ContainsKey("485MSWCommunityAssistanceVisits") && data["485MSWCommunityAssistanceVisits"].Answer == "1" ? true : false, new { @id = "ResumptionOfCare_485MSWCommunityAssistanceVisits1", @class = "radio" })%>
                    <label for="ResumptionOfCare_485MSWCommunityAssistanceVisits1">1-2 OR</label>
                    <%=Html.RadioButton("ResumptionOfCare_485MSWCommunityAssistanceVisits", "0", data.ContainsKey("485MSWCommunityAssistanceVisits") && data["485MSWCommunityAssistanceVisits"].Answer == "0" ? true : false, new { @id = "ResumptionOfCare_485MSWCommunityAssistanceVisits0", @class = "radio" })%>
                    <%=Html.TextBox("ResumptionOfCare_485MSWCommunityAssistanceVisitAmount", data.ContainsKey("485MSWCommunityAssistanceVisitAmount") ? data["485MSWCommunityAssistanceVisitAmount"].Answer : "", new { @id = "ResumptionOfCare_485MSWCommunityAssistanceVisitAmount", @class = "zip", @maxlength = "5" })%>
                    <label for="ResumptionOfCare_485BehaviorInterventions6">visits, every 60 days for community resource assistance.</label>
                </span>
            </div><div class="row">
                <label for="ResumptionOfCare_485BehaviorOrderTemplates">Additional Orders:</label>
                <%  var behaviorOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485BehaviorOrderTemplates") && data["485BehaviorOrderTemplates"].Answer != "" ? data["485BehaviorOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_485BehaviorOrderTemplates", behaviorOrderTemplates)%>
                <%= Html.TextArea("ResumptionOfCare_485BehaviorComments", data.ContainsKey("485BehaviorComments") ? data["485BehaviorComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485BehaviorComments" })%>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <% string[] behaviorGoals = data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer != "" ? data["485BehaviorGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485BehaviorGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorGoals1' class='radio float_left' name='ResumptionOfCare_485BehaviorGoals' value='1' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485BehaviorGoals1" class="radio">Neuro status will be within normal limits and free of S&amp;S of complications or further deterioration.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorGoals2' class='radio float_left' name='ResumptionOfCare_485BehaviorGoals' value='2' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485BehaviorGoals2" class="radio">Patient will exhibit stable neuro status evidenced by increased alertness, mobility and self-care by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorGoals3' class='radio float_left' name='ResumptionOfCare_485BehaviorGoals' value='3' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485BehaviorGoals3" class="radio">Patient will exhibit stable neuro status evidence by absence of seizure activity by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorGoals4' class='radio float_left' name='ResumptionOfCare_485BehaviorGoals' value='4' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485BehaviorGoals4" class="radio">Patient will have optimal cognitive functioning within parameters established for the stage of disease by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorGoals5' class='radio float_left' name='ResumptionOfCare_485BehaviorGoals' value='5' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485BehaviorGoals5" class="radio">Patient will remain free from increased confusion during the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorGoals6' class='radio float_left' name='ResumptionOfCare_485BehaviorGoals' value='6' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485BehaviorGoals6">The</label>
                    <%  var verbalizeSeizurePrecautionsPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                        }, "Value", "Text", data.ContainsKey("485VerbalizeSeizurePrecautionsPerson") && data["485VerbalizeSeizurePrecautionsPerson"].Answer != "" ? data["485VerbalizeSeizurePrecautionsPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485VerbalizeSeizurePrecautionsPerson", verbalizeSeizurePrecautionsPerson)%>
                    <label for="ResumptionOfCare_485BehaviorGoals6">will verbalize understanding of seizure precautions.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorGoals7' class='radio float_left' name='ResumptionOfCare_485BehaviorGoals' value='7' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485BehaviorGoals7" class="radio">Caregiver will verbalize understanding of proper orientation techniques to use when patient becomes disoriented.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485BehaviorGoals8' class='radio float_left' name='ResumptionOfCare_485BehaviorGoals' value='8' type='checkbox' {0} />", behaviorGoals!=null && behaviorGoals.Contains("8") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485BehaviorGoals8" class="radio">Patient&rsquo;s community resource needs will be met with assistance of social worker.</label>
            </div><div class="row">
                <label for="ResumptionOfCare_485BehaviorGoals">Additional Goals:</label>
                <%  var behaviorGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485BehaviorGoalTemplates") && data["485BehaviorGoalTemplates"].Answer != "" ? data["485BehaviorGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_485BehaviorGoalTemplates", behaviorGoalTemplates)%>
                <%=Html.TextArea("ResumptionOfCare_485BehaviorGoalComments", data.ContainsKey("485BehaviorGoalComments") ? data["485BehaviorGoalComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485BehaviorGoalComments" })%>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"ROC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('ResumptionOfCare_ValidationContainer','{0}','{1}','{2}','ResumptionOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNeurologicalStatus1"), $("#ResumptionOfCare_GenericNeurologicalStatus1More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNeurologicalStatus2"), $("#ResumptionOfCare_GenericNeurologicalStatus2More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNeurologicalStatus3"), $("#ResumptionOfCare_GenericNeurologicalStatus3More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNeurologicalStatus4"), $("#ResumptionOfCare_GenericNeurologicalSpeech"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNeurologicalStatus5"), $("#ResumptionOfCare_GenericNeurologicalStatus5More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNeurologicalStatus9"), $("#ResumptionOfCare_GenericNeurologicalStatus9More"));
    Oasis.showIfChecked($("#ResumptionOfCare_485MentalStatus8"), $("#ResumptionOfCare_485MentalStatus8More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericBehaviour8"), $("#ResumptionOfCare_GenericBehaviour8More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericPsychosocial5"), $("#ResumptionOfCare_GenericPsychosocial5More"));
    Oasis.noneOfTheAbove($("#ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsNone"), $(".M1740"));
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>