﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCareRespiratoryForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id) %>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit") %>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("ResumptionOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare") %>
<%= Html.Hidden("categoryType", "Respiratory")%> 
<div class="wrapper main">
    <fieldset>
        <legend>Respiratory</legend>
        <% string[] respiratoryCondition = data.ContainsKey("GenericRespiratoryCondition") && data["GenericRespiratoryCondition"].Answer != "" ? data["GenericRespiratoryCondition"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_GenericRespiratoryCondition" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericRespiratoryCondition1' class='radio float_left' name='ResumptionOfCare_GenericRespiratoryCondition' value='1' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericRespiratoryCondition1" class="radio">WNL (Within Normal Limits)</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericRespiratoryCondition2' class='radio float_left' name='ResumptionOfCare_GenericRespiratoryCondition' value='2' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("2") ? "checked='checked'" : "" ) %><label for="ResumptionOfCare_GenericRespiratoryCondition2" class="radio">Lung Sounds:</label></td><% string[] respiratorySounds = data.ContainsKey("GenericRespiratorySounds") && data["GenericRespiratorySounds"].Answer != "" ? data["GenericRespiratorySounds"].Answer.Split(',') : null; %>
                <input type="hidden" name="ResumptionOfCare_GenericRespiratorySounds" value=" " />
                <div id="ResumptionOfCare_GenericRespiratoryCondition2More" class="rel">
                    <table class="form">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='ResumptionOfCare_GenericRespiratorySounds1' class='radio float_left' name='ResumptionOfCare_GenericRespiratorySounds' value='1' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("1") ? "checked='checked'" : "" ) %>
                                    <label for="ResumptionOfCare_GenericRespiratorySounds1" class="fixed inlineradio">CTA</label>
                                    <%= Html.TextBox("ResumptionOfCare_GenericLungSoundsCTAText", data.ContainsKey("GenericLungSoundsCTAText") ? data["GenericLungSoundsCTAText"].Answer : "", new { @id = "ResumptionOfCare_GenericLungSoundsCTAText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='ResumptionOfCare_GenericRespiratorySounds2' class='radio float_left' name='ResumptionOfCare_GenericRespiratorySounds' value='2' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("2") ? "checked='checked'" : "" ) %>
                                    <label for="ResumptionOfCare_GenericRespiratorySounds2" class="fixed inlineradio">Rales</label>
                                    <%= Html.TextBox("ResumptionOfCare_GenericLungSoundsRalesText", data.ContainsKey("GenericLungSoundsRalesText") ? data["GenericLungSoundsRalesText"].Answer : "", new { @id = "ResumptionOfCare_GenericLungSoundsRalesText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='ResumptionOfCare_GenericRespiratorySounds3' class='radio float_left' name='ResumptionOfCare_GenericRespiratorySounds' value='3' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("3") ? "checked='checked'" : "" ) %>
                                    <label for="ResumptionOfCare_GenericRespiratorySounds3" class="fixed inlineradio">Rhonchi</label>
                                    <%= Html.TextBox("ResumptionOfCare_GenericLungSoundsRhonchiText", data.ContainsKey("GenericLungSoundsRhonchiText") ? data["GenericLungSoundsRhonchiText"].Answer : "", new { @id = "ResumptionOfCare_GenericLungSoundsRhonchiText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='ResumptionOfCare_GenericRespiratorySounds4' class='radio float_left' name='ResumptionOfCare_GenericRespiratorySounds' value='4' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("4") ? "checked='checked'" : "" ) %>
                                    <label for="ResumptionOfCare_GenericRespiratorySounds4" class="fixed inlineradio">Wheezes</label>
                                    <%= Html.TextBox("ResumptionOfCare_GenericLungSoundsWheezesText", data.ContainsKey("GenericLungSoundsWheezesText") ? data["GenericLungSoundsWheezesText"].Answer : "", new { @id = "ResumptionOfCare_GenericLungSoundsWheezesText", @class = "st", @maxlength = "20" }) %>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='ResumptionOfCare_GenericRespiratorySounds5' class='radio float_left' name='ResumptionOfCare_GenericRespiratorySounds' value='5' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("5") ? "checked='checked'" : "" ) %>
                                    <label for="ResumptionOfCare_GenericRespiratorySounds5" class="fixed inlineradio">Crackles</label>
                                    <%= Html.TextBox("ResumptionOfCare_GenericLungSoundsCracklesText", data.ContainsKey("GenericLungSoundsCracklesText") ? data["GenericLungSoundsCracklesText"].Answer : "", new { @id = "ResumptionOfCare_GenericLungSoundsCracklesText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='ResumptionOfCare_GenericRespiratorySounds6' class='radio float_left' name='ResumptionOfCare_GenericRespiratorySounds' value='6' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("6") ? "checked='checked'" : "" ) %>
                                    <label for="ResumptionOfCare_GenericRespiratorySounds6" class="fixed inlineradio">Diminished</label>
                                    <%= Html.TextBox("ResumptionOfCare_GenericLungSoundsDiminishedText", data.ContainsKey("GenericLungSoundsDiminishedText") ? data["GenericLungSoundsDiminishedText"].Answer : "", new { @id = "ResumptionOfCare_GenericLungSoundsDiminishedText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='ResumptionOfCare_GenericRespiratorySounds7' class='radio float_left' name='ResumptionOfCare_GenericRespiratorySounds' value='7' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("7") ? "checked='checked'" : "" ) %>
                                    <label for="ResumptionOfCare_GenericRespiratorySounds7" class="fixed inlineradio">Absent</label>
                                    <%= Html.TextBox("ResumptionOfCare_GenericLungSoundsAbsentText", data.ContainsKey("GenericLungSoundsAbsentText") ? data["GenericLungSoundsAbsentText"].Answer : "", new { @id = "ResumptionOfCare_GenericLungSoundsAbsentText", @class = "st", @maxlength = "20" }) %>
                                </td><td>
                                    <%= string.Format("<input id='ResumptionOfCare_GenericRespiratorySounds8' class='radio float_left' name='ResumptionOfCare_GenericRespiratorySounds' value='8' type='checkbox' {0} />", respiratorySounds!=null && respiratorySounds.Contains("8") ? "checked='checked'" : "" ) %>
                                    <label for="ResumptionOfCare_GenericRespiratorySounds8" class="fixed inlineradio">Stridor</label>
                                    <%= Html.TextBox("ResumptionOfCare_GenericLungSoundsStridorText", data.ContainsKey("GenericLungSoundsStridorText") ? data["GenericLungSoundsStridorText"].Answer : "", new { @id = "ResumptionOfCare_GenericLungSoundsStridorText", @class = "st", @maxlength = "20" }) %>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericRespiratoryCondition3' class='radio float_left' name='ResumptionOfCare_GenericRespiratoryCondition' value='3' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericRespiratoryCondition3" class="fixed inlineradio">Cough:</label>
                <div id="ResumptionOfCare_GenericRespiratoryCondition3More" class="rel">
                    <%  var coughList = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "N/A", Value = "1" },
                            new SelectListItem { Text = "Productive", Value = "1" },
                            new SelectListItem { Text = "Nonproductive", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.ContainsKey("GenericCoughList") && data["GenericCoughList"].Answer != "" ? data["GenericCoughList"].Answer : "0"); %>
                    <%= Html.DropDownList("ResumptionOfCare_GenericCoughList", coughList) %>
                    <label for="ResumptionOfCare_GenericCoughDescribe">Describe</label>
                    <%= Html.TextBox("ResumptionOfCare_GenericCoughDescribe", data.ContainsKey("GenericCoughDescribe") ? data["GenericCoughDescribe"].Answer : "", new { @id = "ResumptionOfCare_GenericCoughDescribe", @class = "st", @maxlength = "20" })%>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericRespiratoryCondition4' class='radio float_left' name='ResumptionOfCare_GenericRespiratoryCondition' value='4' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericRespiratoryCondition4" class="fixed inlineradio">O<sub>2</sub></label>
                <div id="ResumptionOfCare_GenericRespiratoryCondition4More" class="rel">
                    <div id="ResumptionOfCare_GenericRespiratoryCondition4More" class="rel">
                        <label for="ResumptionOfCare_Generic02AtText">At:</label>
                        <%= Html.TextBox("ResumptionOfCare_Generic02AtText", data.ContainsKey("Generic02AtText") ? data["Generic02AtText"].Answer : "", new { @id = "ResumptionOfCare_Generic02AtText", @class = "st", @maxlength = "20" }) %>
                        <label for="ResumptionOfCare_GenericLPMVia">LPM via:</label>
                        <%  var via = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "Nasal Cannula", Value = "1" },
                                new SelectListItem { Text = "Mask", Value = "2" },
                                new SelectListItem { Text = "Trach", Value = "3" },
                                new SelectListItem { Text = "Other", Value = "4" }
                            }, "Value", "Text", data.ContainsKey("GenericLPMVia") ? data["GenericLPMVia"].Answer : "0");%>
                        <%= Html.DropDownList("ResumptionOfCare_GenericLPMVia", via, new { @id = "ResumptionOfCare_GenericLPMVia" })%>
                    </div>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericRespiratoryCondition5' class='radio float_left' name='ResumptionOfCare_GenericRespiratoryCondition' value='5' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericRespiratoryCondition5" class="fixed inlineradio">O<sub>2</sub> Sat:</label>
                <div id="ResumptionOfCare_GenericRespiratoryCondition5More" class="rel">
                    <%= Html.TextBox("ResumptionOfCare_Generic02SatText", data.ContainsKey("Generic02SatText") ? data["Generic02SatText"].Answer : "", new { @id = "ResumptionOfCare_Generic02SatText", @class = "st", @maxlength = "20" }) %>
                    <label>On</label>
                    <%  var satList = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Room Air", Value = "Room Air" },
                            new SelectListItem { Text = "02", Value = "02" }
                        }, "Value", "Text", data.ContainsKey("Generic02SatList") && data["Generic02SatList"].Answer != "" ? data["Generic02SatList"].Answer : "0"); %>
                    <%= Html.DropDownList("ResumptionOfCare_Generic02SatList", satList) %>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericRespiratoryCondition6' class='radio float_left' name='ResumptionOfCare_GenericRespiratoryCondition' value='6' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericRespiratoryCondition6" class="fixed inlineradio">Nebulizer:</label>
                <%= Html.TextBox("ResumptionOfCare_GenericNebulizerText", data.ContainsKey("GenericNebulizerText") ? data["GenericNebulizerText"].Answer : "", new { @id = "ResumptionOfCare_GenericNebulizerText", @class = "st", @maxlength = "20" }) %>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericRespiratoryCondition7' class='radio float_left' name='ResumptionOfCare_GenericRespiratoryCondition' value='7' type='checkbox' {0} />", respiratoryCondition!=null && respiratoryCondition.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericRespiratoryCondition7" class="fixed inlineradio">Tracheostomy</label>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericRespiratoryComments" class="strong">Comments:</label>
                <%= Html.TextArea("ResumptionOfCare_GenericRespiratoryComments", data.ContainsKey("GenericRespiratoryComments") ? data["GenericRespiratoryComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_GenericRespiratoryComments" }) %>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1400');">(M1400)</a> When is the patient dyspneic or noticeably Short of Breath?</label>
                <%= Html.Hidden("ResumptionOfCare_M1400PatientDyspneic") %>
                <div>
                    <%= Html.RadioButton("ResumptionOfCare_M1400PatientDyspneic", "00", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1400PatientDyspneic0", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1400PatientDyspneic0"><span class="float_left">0 &ndash;</span><span class="normal margin">Patient is not short of breath</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1400PatientDyspneic", "01", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1400PatientDyspneic1", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1400PatientDyspneic1"><span class="float_left">1 &ndash;</span><span class="normal margin">When walking more than 20 feet, climbing stairs</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1400PatientDyspneic", "02", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1400PatientDyspneic2", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1400PatientDyspneic2"><span class="float_left">2 &ndash;</span><span class="normal margin">With moderate exertion (e.g., while dressing, using commode or bedpan, walking distances less than 20 feet)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1400PatientDyspneic", "03", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "03" ? true : false, new { @id = "ResumptionOfCare_M1400PatientDyspneic3", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1400PatientDyspneic3"><span class="float_left">3 &ndash;</span><span class="normal margin">With minimal exertion (e.g., while eating, talking, or performing other ADLs) or with agitation</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1400');">?</div>
                    </div>
                    <%= Html.RadioButton("ResumptionOfCare_M1400PatientDyspneic", "04", data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "04" ? true : false, new { @id = "ResumptionOfCare_M1400PatientDyspneic4", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1400PatientDyspneic4"><span class="float_left">4 &ndash;</span><span class="normal margin">At rest (during day or night)</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1410');">(M1410)</a> Respiratory Treatments utilized at home: (Mark all that apply)</label>
                <div>
                    <input name="ResumptionOfCare_M1410HomeRespiratoryTreatmentsOxygen" value=" " type="hidden" />
                    <%= string.Format("<input id='ResumptionOfCare_M1410HomeRespiratoryTreatmentsOxygen' class='radio float_left M1410' name='ResumptionOfCare_M1410HomeRespiratoryTreatmentsOxygen' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen") && data["M1410HomeRespiratoryTreatmentsOxygen"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="ResumptionOfCare_M1410HomeRespiratoryTreatmentsOxygen"><span class="float_left">1 &ndash;</span><span class="normal margin">Oxygen (intermittent or continuous)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="ResumptionOfCare_M1410HomeRespiratoryTreatmentsVentilator" value=" " type="hidden" />
                    <%= string.Format("<input id='ResumptionOfCare_M1410HomeRespiratoryTreatmentsVentilator' class='radio float_left M1410' name='ResumptionOfCare_M1410HomeRespiratoryTreatmentsVentilator' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator") && data["M1410HomeRespiratoryTreatmentsVentilator"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_M1410HomeRespiratoryTreatmentsVentilator"><span class="float_left">2 &ndash;</span><span class="normal margin">Ventilator (continually or at night)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <input name="ResumptionOfCare_M1410HomeRespiratoryTreatmentsContinuous" value=" " type="hidden" />
                    <%= string.Format("<input id='ResumptionOfCare_M1410HomeRespiratoryTreatmentsContinuous' class='radio float_left M1410' name='ResumptionOfCare_M1410HomeRespiratoryTreatmentsContinuous' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous") && data["M1410HomeRespiratoryTreatmentsContinuous"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_M1410HomeRespiratoryTreatmentsContinuous"><span class="float_left">3 &ndash;</span><span class="normal margin">Continuous / Bi-level positive airway pressure</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1400');">?</div>
                    </div>
                    <input name="ResumptionOfCare_M1410HomeRespiratoryTreatmentsNone" value=" " type="hidden" />
                    <%= string.Format("<input id='ResumptionOfCare_M1410HomeRespiratoryTreatmentsNone' class='radio float_left M1410' name='ResumptionOfCare_M1410HomeRespiratoryTreatmentsNone' value='1' type='checkbox' {0} />", data.ContainsKey("M1410HomeRespiratoryTreatmentsNone") && data["M1410HomeRespiratoryTreatmentsNone"].Answer == "1" ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_M1410HomeRespiratoryTreatmentsNone"><span class="float_left">4 &ndash;</span><span class="normal margin">None of the above</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] respiratoryInterventions = data.ContainsKey("485RespiratoryInterventions") && data["485RespiratoryInterventions"].Answer != "" ? data["485RespiratoryInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485RespiratoryInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions1' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='1' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("1") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485RespiratoryInterventions1">SN to instruct the</label>
                    <%  var instructNebulizerUsePerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructNebulizerUsePerson") && data["485InstructNebulizerUsePerson"].Answer != "" ? data["485InstructNebulizerUsePerson"].Answer : "Patient/Caregiver"); %>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructNebulizerUsePerson", instructNebulizerUsePerson) %>
                    <label for="ResumptionOfCare_485RespiratoryInterventions1">on proper use of nebulizer/inhaler, and assess return demonstration.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions2' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='2' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("2") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485RespiratoryInterventions2">SN to assess O<sub>2</sub> saturation on room air (freq)</label>
                    <%= Html.TextBox("ResumptionOfCare_485AssessOxySaturationFrequency", data.ContainsKey("485AssessOxySaturationFrequency") ? data["485AssessOxySaturationFrequency"].Answer : "", new { @id = "ResumptionOfCare_485AssessOxySaturationFrequency", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions3' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='3' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("3") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485RespiratoryInterventions3">SN to assess O<sub>2</sub> saturation on O<sub>2</sub> @</label>
                    <%=Html.TextBox("ResumptionOfCare_485AssessOxySatOnOxyAt", data.ContainsKey("485AssessOxySatOnOxyAt") ? data["485AssessOxySatOnOxyAt"].Answer : "", new { @id = "ResumptionOfCare_485AssessOxySatOnOxyAt", @class = "st", @maxlength = "15" })%>
                    <label for="ResumptionOfCare_485RespiratoryInterventions3">LPM/</label>
                    <%=Html.TextBox("ResumptionOfCare_485AssessOxySatLPM", data.ContainsKey("485AssessOxySatLPM") ? data["485AssessOxySatLPM"].Answer : "", new { @id = "ResumptionOfCare_485AssessOxySatLPM", @class = "st", @maxlength = "15" })%>
                    <label for="ResumptionOfCare_485RespiratoryInterventions3">(freq)</label>
                    <%=Html.TextBox("ResumptionOfCare_485AssessOxySatOnOxyFrequency", data.ContainsKey("485AssessOxySatOnOxyFrequency") ? data["485AssessOxySatOnOxyFrequency"].Answer : "", new { @id = "ResumptionOfCare_485AssessOxySatOnOxyFrequency", @class = "st", @maxlength = "15" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions4' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='4' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485RespiratoryInterventions4">SN to instruct the</label>
                    <%  var instructSobFactorsPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructSobFactorsPerson") && data["485InstructSobFactorsPerson"].Answer != "" ? data["485InstructSobFactorsPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructSobFactorsPerson", instructSobFactorsPerson)%>
                    <label for="ResumptionOfCare_485RespiratoryInterventions4">on factors that contribute to SOB.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions5' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='5' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("5") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485RespiratoryInterventions5">SN to instruct the</label>
                    <%  var instructAvoidSmokingPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485InstructAvoidSmokingPerson") && data["485InstructAvoidSmokingPerson"].Answer != "" ? data["485InstructAvoidSmokingPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485InstructAvoidSmokingPerson", instructAvoidSmokingPerson)%>
                    <label for="ResumptionOfCare_485RespiratoryInterventions5">to avoid smoking or allowing people to smoke in patient&rsquo;s home. Instruct patient to avoid irritants/allergens known to increase SOB.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions6' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='6' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485RespiratoryInterventions6" class="radio">SN to instruct patient on energy conserving measures including frequent rest periods, small frequent meals, avoiding large meals/overeating, controlling stress.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions7' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='7' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485RespiratoryInterventions7" class="radio">SN to instruct caregiver on proper suctioning technique.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions8' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='8' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("8") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485RespiratoryInterventions8">SN to instruct the </label>
                    <%  var recognizePulmonaryDysfunctionPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485RecognizePulmonaryDysfunctionPerson") && data["485RecognizePulmonaryDysfunctionPerson"].Answer != "" ? data["485RecognizePulmonaryDysfunctionPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485RecognizePulmonaryDysfunctionPerson", recognizePulmonaryDysfunctionPerson)%>
                    <label for="ResumptionOfCare_485RespiratoryInterventions8">on methods to recognize pulmonary dysfunction and relieve complications.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions9' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='9' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("9") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485RespiratoryInterventions9">Report to physician O<sub>2</sub> saturation less than</label>
                    <%=Html.TextBox("ResumptionOfCare_485OxySaturationLessThanPercent", data.ContainsKey("485OxySaturationLessThanPercent") ? data["485OxySaturationLessThanPercent"].Answer : "", new { @id = "ResumptionOfCare_485OxySaturationLessThanPercent", @class = "st", @maxlength = "15" })%>
                    <label for="ResumptionOfCare_485RespiratoryInterventions9">%.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions10' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='10' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("10") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485RespiratoryInterventions10" class="radio">SN to assess/instruct on signs &amp; symptoms of pulmonary complications.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions11' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='11' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("11") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485RespiratoryInterventions11" class="radio">SN to instruct on all aspects of trach care and equipment. SN to instruct PT/CG on emergency procedures for complications and dislodgment of tube.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryInterventions12' class='radio float_left' name='ResumptionOfCare_485RespiratoryInterventions' value='12' type='checkbox' {0} />", respiratoryInterventions!=null && respiratoryInterventions.Contains("12") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485RespiratoryInterventions12" class="radio">SN to perform trach care using sterile technique (i.e. suctioning, dressing change).</label>
            </div><div class="row">
                <label for="ResumptionOfCare_485RespiratoryOrderTemplates" class="strong">Additional Orders:</label>
                <%  var respiratoryOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485RespiratoryOrderTemplates") && data["485RespiratoryOrderTemplates"].Answer != "" ? data["485RespiratoryOrderTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("ResumptionOfCare_485RespiratoryOrderTemplates", respiratoryOrderTemplates) %>
                <%= Html.TextArea("ResumptionOfCare_485RespiratoryInterventionComments", data.ContainsKey("485RespiratoryInterventionComments") ? data["485RespiratoryInterventionComments"].Answer : "", 2, 70, new { @id = "ResumptionOfCare_485RespiratoryInterventionComments" }) %>
            </div>
        </div>
    </fieldset><fieldset class="goals">
        <legend>Goals</legend>
        <% string[] respiratoryGoals = data.ContainsKey("485RespiratoryGoals") && data["485RespiratoryGoals"].Answer != "" ? data["485RespiratoryGoals"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485RespiratoryGoals" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryGoals1' class='radio float_left' name='ResumptionOfCare_485RespiratoryGoals' value='1' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485RespiratoryGoals1" class="radio">Respiratory status will improve with reduced shortness of breath and improved lung sounds by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryGoals2' class='radio float_left' name='ResumptionOfCare_485RespiratoryGoals' value='2' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485RespiratoryGoals2" class="radio">PT/CG will verbalize and demonstrate correct use and care of oxygen and equipment by the end of the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryGoals3' class='radio float_left' name='ResumptionOfCare_485RespiratoryGoals' value='3' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485RespiratoryGoals3" class="radio">Patient will be free from signs and symptoms of respiratory distress during the episode.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryGoals4' class='radio float_left' name='ResumptionOfCare_485RespiratoryGoals' value='4' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("4") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485RespiratoryGoals4">Patient and caregiver will verbalize an understanding of factors that contribute to shortness of breath by:</label>
                    <%=Html.TextBox("ResumptionOfCare_485VerbalizeFactorsSobDate", data.ContainsKey("485VerbalizeFactorsSobDate") ? data["485VerbalizeFactorsSobDate"].Answer : "", new { @id = "ResumptionOfCare_485VerbalizeFactorsSobDate", @class = "st", @maxlength = "10" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryGoals5' class='radio float_left' name='ResumptionOfCare_485RespiratoryGoals' value='5' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("5") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485RespiratoryGoals5">Patient will verbalize an understanding of energy conserving measures by:</label>
                    <%=Html.TextBox("ResumptionOfCare_485VerbalizeEnergyConserveDate", data.ContainsKey("485VerbalizeEnergyConserveDate") ? data["485VerbalizeEnergyConserveDate"].Answer : "", new { @id = "ResumptionOfCare_485VerbalizeEnergyConserveDate", @class = "st", @maxlength = "10" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryGoals6' class='radio float_left' name='ResumptionOfCare_485RespiratoryGoals' value='6' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("6") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485RespiratoryGoals6">The</label>
                    <%  var verbalizeSafeOxyManagementPerson = new SelectList(new[] {
                            new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                            new SelectListItem { Text = "Patient", Value = "Patient" },
                            new SelectListItem { Text = "Caregiver", Value = "Caregiver" }
                        }, "Value", "Text", data.ContainsKey("485VerbalizeSafeOxyManagementPerson") && data["485VerbalizeSafeOxyManagementPerson"].Answer != "" ? data["485VerbalizeSafeOxyManagementPerson"].Answer : "Patient/Caregiver");%>
                    <%= Html.DropDownList("ResumptionOfCare_485VerbalizeSafeOxyManagementPerson", verbalizeSafeOxyManagementPerson)%>
                    <label for="ResumptionOfCare_485RespiratoryGoals6">will verbalize and demonstrate safe management of oxygen by:</label>
                    <%=Html.TextBox("ResumptionOfCare_485VerbalizeSafeOxyManagementPersonDate", data.ContainsKey("485VerbalizeSafeOxyManagementPersonDate") ? data["485VerbalizeSafeOxyManagementPersonDate"].Answer : "", new { @id = "ResumptionOfCare_485VerbalizeSafeOxyManagementPersonDate", @class = "st", @maxlength = "10" })%>.
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485RespiratoryGoals7' class='radio float_left' name='ResumptionOfCare_485RespiratoryGoals' value='7' type='checkbox' {0} />", respiratoryGoals!=null && respiratoryGoals.Contains("7") ? "checked='checked'" : "" ) %>
                <span class="radio">
                    <label for="ResumptionOfCare_485RespiratoryGoals7">Patient will return demonstrate proper use of nebulizer treatment by </label>
                    <%=Html.TextBox("ResumptionOfCare_485DemonstrateNebulizerUseDate", data.ContainsKey("485DemonstrateNebulizerUseDate") ? data["485DemonstrateNebulizerUseDate"].Answer : "", new { @id = "ResumptionOfCare_485DemonstrateNebulizerUseDate", @class = "st", @maxlength = "10" })%>.
                </span>
            </div><div class="row">
                <label for="ResumptionOfCare_485RespiratoryGoalTemplates" class="strong">Additional Goals:</label>
                <%  var respiratoryGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485RespiratoryGoalTemplates") && data["485RespiratoryGoalTemplates"].Answer != "" ? data["485RespiratoryGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_485RespiratoryGoalTemplates", respiratoryGoalTemplates) %>
                <%= Html.TextArea("ResumptionOfCare_485RespiratoryGoalComments", data.ContainsKey("485RespiratoryGoalComments") ? data["485RespiratoryGoalComments"].Answer : "", 2, 70, new { @id = "ResumptionOfCare_485RespiratoryGoalComments" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"ROC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('ResumptionOfCare_ValidationContainer','{0}','{1}','{2}','ResumptionOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratoryCondition2"), $("#ResumptionOfCare_GenericRespiratoryCondition2More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratoryCondition3"), $("#ResumptionOfCare_GenericRespiratoryCondition3More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratorySounds1"), $("#ResumptionOfCare_GenericLungSoundsCTAText"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratorySounds2"), $("#ResumptionOfCare_GenericLungSoundsRalesText"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratorySounds3"), $("#ResumptionOfCare_GenericLungSoundsRhonchiText"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratorySounds4"), $("#ResumptionOfCare_GenericLungSoundsWheezesText"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratorySounds5"), $("#ResumptionOfCare_GenericLungSoundsCracklesText"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratorySounds6"), $("#ResumptionOfCare_GenericLungSoundsDiminishedText"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratorySounds7"), $("#ResumptionOfCare_GenericLungSoundsAbsentText"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratorySounds8"), $("#ResumptionOfCare_GenericLungSoundsStridorText"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratoryCondition4"), $("#ResumptionOfCare_GenericRespiratoryCondition4More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratoryCondition5"), $("#ResumptionOfCare_GenericRespiratoryCondition5More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericRespiratoryCondition6"), $("#ResumptionOfCare_GenericNebulizerText"));
    Oasis.noneOfTheAbove($("#ResumptionOfCare_M1410HomeRespiratoryTreatmentsNone"), $("#window_resumptionofcare .M1410"));
    Oasis.interventions($(".interventions"));
    Oasis.goals($(".goals"));
</script>