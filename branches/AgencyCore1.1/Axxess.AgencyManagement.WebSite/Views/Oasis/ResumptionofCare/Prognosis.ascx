﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCarePrognosisForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id)%>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit")%>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("ResumptionOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare")%>
<%= Html.Hidden("categoryType", "Prognosis")%> 
<div class="wrapper main">
    <fieldset class="half float_left loc485">
        <legend>Prognosis (Locator #20)</legend>
        <div class="column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <%= Html.RadioButton("ResumptionOfCare_485Prognosis", "Guarded", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Guarded" ? true : false, new { @id = "ResumptionOfCare_485PrognosisGuarded", @class = "radio float_left" })%>
                                <label for="ResumptionOfCare_485PrognosisGuarded" class="radio">Guarded</label>
                            </td><td>
                                <%= Html.RadioButton("ResumptionOfCare_485Prognosis", "Poor", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Poor" ? true : false, new { @id = "ResumptionOfCare_485PrognosisPoor", @class = "radio float_left" })%>
                                <label for="ResumptionOfCare_485PrognosisPoor" class="radio">Poor</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= Html.RadioButton("ResumptionOfCare_485Prognosis", "Fair", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Fair" ? true : false, new { @id = "ResumptionOfCare_485PrognosisFair", @class = "radio float_left" })%>
                                <label for="ResumptionOfCare_485PrognosisFair" class="radio">Fair</label>
                            </td><td>
                                <%= Html.RadioButton("ResumptionOfCare_485Prognosis", "Good", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Good" ? true : false, new { @id = "ResumptionOfCare_485PrognosisGood", @class = "radio float_left" })%>
                                <label for="ResumptionOfCare_485PrognosisGood" class="radio">Good</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <%= Html.RadioButton("ResumptionOfCare_485Prognosis", "Excellent", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Excellent" ? true : false, new { @id = "ResumptionOfCare_485PrognosisExcellent", @class = "radio float_left" })%>
                                <label for="ResumptionOfCare_485PrognosisExcellent" class="radio">Excellent</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Advanced Directives</legend>
        <div class="column">
            <div class="row">
                <label class="float_left">Are there any Advanced Directives?</label>
                <div class="float_right">
                    <%= Html.Hidden("ResumptionOfCare_485AdvancedDirectives") %>
                    <%= Html.RadioButton("ResumptionOfCare_485AdvancedDirectives", "Yes", data.ContainsKey("485AdvancedDirectives") && data["485AdvancedDirectives"].Answer == "Yes" ? true : false, new { @id = "ResumptionOfCare_485AdvancedDirectivesYes", @class = "radio" })%>
                    <label for="ResumptionOfCare_485AdvancedDirectivesYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("ResumptionOfCare_485AdvancedDirectives", "No", data.ContainsKey("485AdvancedDirectives") && data["485AdvancedDirectives"].Answer == "No" ? true : false, new { @id = "ResumptionOfCare_485AdvancedDirectivesNo", @class = "radio" })%>
                    <label for="ResumptionOfCare_485AdvancedDirectivesNo" class="inlineradio">No</label>
                </div>
            </div><div class="row">
                <div class="strong">Intent</div>
                <div>
                    <%= Html.Hidden("ResumptionOfCare_485AdvancedDirectivesIntent") %>
                    <%= Html.RadioButton("ResumptionOfCare_485AdvancedDirectivesIntent", "DNR", data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "DNR" ? true : false, new { @id = "ResumptionOfCare_485AdvancedDirectivesIntentDNR", @class = "radio 485AdvancedDirectivesIntent" })%>
                    <label for="ResumptionOfCare_485AdvancedDirectivesIntentDNR" class="inlineradio">DNR</label>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_485AdvancedDirectivesIntent", "Living Will", data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "Living Will" ? true : false, new { @id = "ResumptionOfCare_485AdvancedDirectivesIntentLivingWill", @class = "radio 485AdvancedDirectivesIntent" })%>
                    <label for="ResumptionOfCare_485AdvancedDirectivesIntentLivingWill" class="inlineradio">Living Will</label>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_485AdvancedDirectivesIntent", "Medical Power of Attorney", data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "Medical Power of Attorney" ? true : false, new { @id = "ResumptionOfCare_485AdvancedDirectivesIntentMPOA", @class = "radio 485AdvancedDirectivesIntent" })%>
                    <label for="ResumptionOfCare_485AdvancedDirectivesIntentMPOA" class="inlineradio">Medical Power of Attorney</label>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_485AdvancedDirectivesIntent", "Other", data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "Other" ? true : false, new { @id = "ResumptionOfCare_485AdvancedDirectivesIntentOther", @class = "radio" })%>
                    <label for="ResumptionOfCare_485AdvancedDirectivesIntentOther" class="inlineradio">Other</label>
                    <div id="ResumptionOfCare_485AdvancedDirectivesIntentOtherMore" class="float_right"><label for="ResumptionOfCare_485AdvancedDirectivesIntentOther"><em>(Specify)</em></label><%= Html.TextBox("ResumptionOfCare_485AdvancedDirectivesIntentOther", data.ContainsKey("485AdvancedDirectivesIntentOther") ? data["485AdvancedDirectivesIntentOther"].Answer : "", new { @id = "ResumptionOfCare_485AdvancedDirectivesIntentOther", @maxlength="50" }) %></div>
                </div>
            </div><div class="row">
                <label class="float_left">Copy on file at agency?</label>
                <div class="float_right">
                    <%= Html.Hidden("ResumptionOfCare_485AdvancedDirectivesCopyOnFile") %>
                    <%= Html.RadioButton("ResumptionOfCare_485AdvancedDirectivesCopyOnFile", "Yes", data.ContainsKey("485AdvancedDirectivesCopyOnFile") && data["485AdvancedDirectivesCopyOnFile"].Answer == "Yes" ? true : false, new { @id = "ResumptionOfCare_485AdvancedDirectivesCopyOnFileYes", @class = "radio" })%>
                    <label for="ResumptionOfCare_485AdvancedDirectivesCopyOnFileYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("ResumptionOfCare_485AdvancedDirectivesCopyOnFile", "No", data.ContainsKey("485AdvancedDirectivesCopyOnFile") && data["485AdvancedDirectivesCopyOnFile"].Answer == "No" ? true : false, new { @id = "ResumptionOfCare_485AdvancedDirectivesCopyOnFileNo", @class = "radio" })%>
                    <label for="ResumptionOfCare_485AdvancedDirectivesCopyOnFileNo" class="inlineradio">No</label>
                </div>
            </div><div class="row">
                <label class="float_left">Patient was provided written and verbal information on Advance Directives</label>
                <div class="float_right">
                    <%= Html.Hidden("ResumptionOfCare_485AdvancedDirectivesWrittenAndVerbal") %>
                    <%= Html.RadioButton("ResumptionOfCare_485AdvancedDirectivesWrittenAndVerbal", "Yes", data.ContainsKey("485AdvancedDirectivesWrittenAndVerbal") && data["485AdvancedDirectivesWrittenAndVerbal"].Answer == "Yes" ? true : false, new { @id = "ResumptionOfCare_485AdvancedDirectivesWrittenAndVerbalYes", @class = "radio" })%>
                    <label for="ResumptionOfCare_485AdvancedDirectivesWrittenAndVerbalYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("ResumptionOfCare_485AdvancedDirectivesWrittenAndVerbal", "No", data.ContainsKey("485AdvancedDirectivesWrittenAndVerbal") && data["485AdvancedDirectivesWrittenAndVerbal"].Answer == "No" ? true : false, new { @id = "ResumptionOfCare_485AdvancedDirectivesWrittenAndVerbalNo", @class = "radio" })%>
                    <label for="ResumptionOfCare_485AdvancedDirectivesWrittenAndVerbalNo" class="inlineradio">No</label>
                </div>
            </div><div class="row">
                <label class="float_left">Is the Patient DNR (Do Not Resuscitate)?</label>
                <div class="float_right">
                    <%= Html.Hidden("ResumptionOfCare_GenericPatientDNR") %>
                    <%= Html.RadioButton("ResumptionOfCare_GenericPatientDNR", "Yes", data.ContainsKey("GenericPatientDNR") && data["GenericPatientDNR"].Answer == "Yes" ? true : false, new { @id = "ResumptionOfCare_GenericPatientDNRYes", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericPatientDNRYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("ResumptionOfCare_GenericPatientDNR", "No", data.ContainsKey("GenericPatientDNR") && data["GenericPatientDNR"].Answer == "No" ? true : false, new { @id = "ResumptionOfCare_GenericPatientDNRNo", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericPatientDNRNo" class="inlineradio">No</label>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_485AdvancedDirectivesComment" class="strong">Comments</label>
                <%= Html.TextArea("ResumptionOfCare_485AdvancedDirectivesComment", data.ContainsKey("485AdvancedDirectivesComment") ? data["485AdvancedDirectivesComment"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485AdvancedDirectivesComment" })%>
            </div>
        </div>
    </fieldset><fieldset class="half float_left loc485">
        <legend>Functional Limitations (locator #18.A)</legend>
        <input name="ResumptionOfCare_485FunctionLimitations" value=" " type="hidden" />
        <% string[] functionLimitations = data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer != "" ? data["485FunctionLimitations"].Answer.Split(',') : null; %>
        <div class="column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='ResumptionOfCare_485FunctionLimitations1' name='ResumptionOfCare_485FunctionLimitations' value='1' class='radio float_left' type='checkbox' {0} />", functionLimitations!=null && functionLimitations.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_485FunctionLimitations1" class="radio">Amputation</label>
                            </td><td>
                                <%= string.Format("<input id='ResumptionOfCare_485FunctionLimitations5' name='ResumptionOfCare_485FunctionLimitations' value='5' class='radio float_left' type='checkbox' {0} />", functionLimitations!=null && functionLimitations.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_485FunctionLimitations5" class="radio">Paralysis</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='ResumptionOfCare_485FunctionLimitations9' name='ResumptionOfCare_485FunctionLimitations' value='9' class='radio float_left' type='checkbox' {0} />", functionLimitations!=null && functionLimitations.Contains("9") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_485FunctionLimitations9" class="radio">Legally Blind</label>
                            </td><td>
                                <%= string.Format("<input id='ResumptionOfCare_485FunctionLimitations2' name='ResumptionOfCare_485FunctionLimitations' value='2' class='radio float_left' type='checkbox' {0} />", functionLimitations!=null && functionLimitations.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_485FunctionLimitations2" class="radio">Bowel/Bladder Incontinence</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='ResumptionOfCare_485FunctionLimitations6' name='ResumptionOfCare_485FunctionLimitations' value='6' class='radio float_left' type='checkbox' {0} />", functionLimitations!=null && functionLimitations.Contains("6") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_485FunctionLimitations6" class="radio">Endurance</label>
                            </td><td>
                                <%= string.Format("<input id='ResumptionOfCare_485FunctionLimitationsA' name='ResumptionOfCare_485FunctionLimitations' value='A' class='radio float_left' type='checkbox' {0} />", functionLimitations!=null && functionLimitations.Contains("A") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_485FunctionLimitationsA" class="radio">Dyspnea</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='ResumptionOfCare_485FunctionLimitations3' name='ResumptionOfCare_485FunctionLimitations' value='3' class='radio float_left' type='checkbox' {0} />", functionLimitations!=null && functionLimitations.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_485FunctionLimitations3" class="radio">Contracture</label>
                            </td><td>
                                <%= string.Format("<input id='ResumptionOfCare_485FunctionLimitations7' name='ResumptionOfCare_485FunctionLimitations' value='7' class='radio float_left' type='checkbox' {0} />", functionLimitations!=null && functionLimitations.Contains("7") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_485FunctionLimitations7" class="radio">Ambulation</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='ResumptionOfCare_485FunctionLimitations4' name='ResumptionOfCare_485FunctionLimitations' value='4' class='radio float_left' type='checkbox' {0} />", functionLimitations!=null && functionLimitations.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_485FunctionLimitations4" class="radio">Hearing</label>
                            </td><td>
                                <%= string.Format("<input id='ResumptionOfCare_485FunctionLimitations8' name='ResumptionOfCare_485FunctionLimitations' value='8' class='radio float_left' type='checkbox' {0} />", functionLimitations!=null && functionLimitations.Contains("8") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_485FunctionLimitations8" class="radio">Speech</label>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <%= string.Format("<input id='ResumptionOfCare_485FunctionLimitationsB' name='ResumptionOfCare_485FunctionLimitations' value='B' class='radio float_left' type='checkbox' {0} />", functionLimitations!=null && functionLimitations.Contains("B") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_485FunctionLimitationsB" class="radio">Other (Specify)</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <%= Html.TextArea("ResumptionOfCare_485FunctionLimitationsOther", data.ContainsKey("485FunctionLimitationsOther") ? data["485FunctionLimitationsOther"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485FunctionLimitationsOther" }) %>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"ROC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('ResumptionOfCare_ValidationContainer','{0}','{1}','{2}','ResumptionOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfRadioEquals("ResumptionOfCare_485AdvancedDirectivesIntent", "Other", $("#ResumptionOfCare_485AdvancedDirectivesIntentOtherMore"));
    Oasis.showIfChecked($("#ResumptionOfCare_485FunctionLimitationsB"), $("#ResumptionOfCare_485FunctionLimitationsOther"));
</script>