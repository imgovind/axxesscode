﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(3,
            printview.span("<strong>Onset Date:</strong> <%= data != null && data.ContainsKey("GenericPainOnSetDate") && data["GenericPainOnSetDate"].Answer.IsNotNullOrEmpty() ? data["GenericPainOnSetDate"].Answer : "<span class='blank'></span>"%>") +
            printview.span("<strong>Duration:</strong> <%= data != null && data.ContainsKey("GenericDurationOfPain") && data["GenericDurationOfPain"].Answer.IsNotNullOrEmpty() ? data["GenericDurationOfPain"].Answer : "<span class='blank'></span>"%>") +
            printview.span("<strong>Pain Intensity:</strong> <%= data != null && data.ContainsKey("GenericIntensityOfPain") && data["GenericIntensityOfPain"].Answer.IsNotNullOrEmpty() ? data["GenericIntensityOfPain"].Answer : "<span class='blank'></span>"%>") +
            printview.span("<strong>Description:</strong> <%= data != null && data.ContainsKey("GenericQualityOfPain") && data["GenericQualityOfPain"].Answer.IsNotNullOrEmpty() ? (data["GenericQualityOfPain"].Answer == "0" ? "<span class='blank'></span>" : "") + (data["GenericQualityOfPain"].Answer == "1" ? "Aching" : "") + (data["GenericQualityOfPain"].Answer == "2" ? "Throbbing" : "") + (data["GenericQualityOfPain"].Answer == "3" ? "Burning" : "") + (data["GenericQualityOfPain"].Answer == "4" ? "Sharp" : "") + (data["GenericQualityOfPain"].Answer == "5" ? "Tender" : "") + (data["GenericQualityOfPain"].Answer == "6" ? "Other" : "") : "<span class='blank'></span>" %>") +
            printview.span("<strong>Primary Site:</strong> <%= data != null && data.ContainsKey("GenericLocationOfPain") && data["GenericLocationOfPain"].Answer.IsNotNullOrEmpty() ? data["GenericLocationOfPain"].Answer : "<span class='blank'></span>"%>") +
            printview.span("<strong>Pain Management Effectiveness:</strong> <%= data != null && data.ContainsKey("GenericMedicationEffectiveness") && data["GenericMedicationEffectiveness"].Answer.IsNotNullOrEmpty() ? (data["GenericMedicationEffectiveness"].Answer == "0" ? "N/A" : "") + (data["GenericMedicationEffectiveness"].Answer == "1" ? "Effective" : "") + (data["GenericMedicationEffectiveness"].Answer == "2" ? "Not Effective" : "") : "<span class='blank'></span>" %>")) +
        printview.span("What makes pain better:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericWhatMakesPainBetter") && data["GenericWhatMakesPainBetter"].Answer.IsNotNullOrEmpty() ? data["GenericWhatMakesPainBetter"].Answer : ""%>",false,2) +
        printview.span("What makes pain worse:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericWhatMakesPainWorse") && data["GenericWhatMakesPainWorse"].Answer.IsNotNullOrEmpty() ? data["GenericWhatMakesPainWorse"].Answer : ""%>",false,2) +
        printview.span("Patient&rsquo;s pain goal:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericPatientPainGoal") && data["GenericPatientPainGoal"].Answer.IsNotNullOrEmpty() ? data["GenericPatientPainGoal"].Answer : ""%>",false,2) +
        printview.span("Comments:",true) +
        printview.span("<%= data != null && data.ContainsKey("GenericPatientPainComment") && data["GenericPatientPainComment"].Answer.IsNotNullOrEmpty() ? data["GenericPatientPainComment"].Answer : ""%>",false,2),
        "Pain Scale");
    printview.addsection(
        printview.span("(M1240) Has this patient had a formal Pain Assessment using a standardized pain assessment tool (appropriate to the patient&rsquo;s ability to communicate the severity of pain)?",true) +
        printview.col(3,
            printview.checkbox("0 &ndash; No standardized assessment conducted",<%= data != null && data.ContainsKey("M1240FormalPainAssessment") && data["M1240FormalPainAssessment"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Yes, and it does not indicate severe pain",<%= data != null && data.ContainsKey("M1240FormalPainAssessment") && data["M1240FormalPainAssessment"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Yes, and it indicates severe pain",<%= data != null && data.ContainsKey("M1240FormalPainAssessment") && data["M1240FormalPainAssessment"].Answer == "02" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1242) Frequency of Pain Interfering with patient&rsquo;s activity or movement",true) +
        printview.col(2,
            printview.checkbox("0 &ndash; Patient has no pain",<%= data != null && data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "00" ? "true" : "false"%>) +
            printview.checkbox("1 &ndash; Patient has pain that does not interfere with activity or movement",<%= data != null && data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "01" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Less often than daily",<%= data != null && data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "02" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Daily, but not constantly",<%= data != null && data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "03" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; All of the time",<%= data != null && data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "04" ? "true" : "false"%>)));
    printview.addsection(
        printview.checkbox("SN to assess pain level and effectiveness of pain medications and current pain management therapy every visit.",<%= data != null && data.ContainsKey("485PainInterventions") && data["485PainInterventions"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct patient to take pain medication before pain becomes severe to achieve better pain control.",<%= data != null && data.ContainsKey("485PainInterventions") && data["485PainInterventions"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct patient on nonpharmacologic pain relief measures, including relaxation techniques, massage, stretching, positioning, and hot/cold packs.",<%= data != null && data.ContainsKey("485PainInterventions") && data["485PainInterventions"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.checkbox("SN to report to physician if patient experiences pain level not acceptable to patient, pain level greater than <%= data != null && data.ContainsKey("485PainTooGreatLevel") && data["485PainTooGreatLevel"].Answer.IsNotNullOrEmpty() ? data["485PainTooGreatLevel"].Answer : "<span class='blank'></span>"%>, pain medications not effective, patient unable to tolerate pain medications, pain affecting ability to perform patient&rsquo;s normal activities. ",<%= data != null && data.ContainsKey("485PainInterventions") && data["485PainInterventions"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485PainInterventionComments") && data["485PainInterventionComments"].Answer.IsNotNullOrEmpty() ? data["485PainInterventionComments"].Answer : ""%>",false,2),
        "Interventions");
    printview.addsection(
        printview.checkbox("Patient will verbalize understanding of proper use of pain medication by the end of the episode.",<%= data != null && data.ContainsKey("485PainGoals") && data["485PainGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("PT/CG will verbalize knowledge of pain medication regimen and pain relief measures by the end of the episode.",<%= data != null && data.ContainsKey("485PainGoals") && data["485PainGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("Patient will have absence or control of pain as evidenced by optimal mobility and activity necessary for functioning and performing ADLs by the end of the episode.",<%= data != null && data.ContainsKey("485PainGoals") && data["485PainGoals"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.span("Additional Goals:",true) +
        printview.span("<%= data != null && data.ContainsKey("485PainGoalComments") && data["485PainGoalComments"].Answer.IsNotNullOrEmpty() ? data["485PainGoalComments"].Answer : ""%>",false,2),
        "Goals");
</script>