﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCareSensoryStatusForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id) %>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit") %>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("ResumptionOfCare_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare") %>
<%= Html.Hidden("categoryType", "Sensory")%>
<div class="wrapper main">
    <fieldset>
        <legend>Eyes</legend>
        <% string[] eyes = data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer != "" ? data["GenericEyes"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_GenericEyes" value="" />
        <div class="wide_column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEyes1' name='ResumptionOfCare_GenericEyes' value='1' {0} />", eyes!=null && eyes.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEyes1" class="radio">WNL (Within Normal Limits)</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEyes2' name='ResumptionOfCare_GenericEyes' value='2' {0} />", eyes!=null && eyes.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEyes2" class="radio">Glasses</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEyes3' name='ResumptionOfCare_GenericEyes' value='3' {0} />", eyes!=null && eyes.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEyes3" class="radio">Contacts Left</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEyes4' name='ResumptionOfCare_GenericEyes' value='4' {0} />", eyes!=null && eyes.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEyes4" class="radio">Contacts Right</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEyes5' name='ResumptionOfCare_GenericEyes' value='5' {0} />", eyes!=null && eyes.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEyes5" class="radio">Blurred Vision</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEyes6' name='ResumptionOfCare_GenericEyes' value='6' {0} />", eyes!=null && eyes.Contains("6") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEyes6" class="radio">Glaucoma</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEyes7' name='ResumptionOfCare_GenericEyes' value='7' {0} />", eyes!=null && eyes.Contains("7") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEyes7" class="radio">Cataracts</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEyes8' name='ResumptionOfCare_GenericEyes' value='8' {0} />", eyes!=null && eyes.Contains("8") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEyes8" class="radio">Macular Degeneration</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEyes13' name='ResumptionOfCare_GenericEyes' value='13' {0} />", eyes!=null && eyes.Contains("13") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEyes13" class="radio more small">Other</label>
                            </td><td>
                                <div id="ResumptionOfCare_GenericEyes13More"><label for="ResumptionOfCare_GenericEyesOtherDetails"><em>(Specify)</em></label><%= Html.TextBox("ResumptionOfCare_GenericEyesOtherDetails", data.ContainsKey("GenericEyesOtherDetails") ? data["GenericEyesOtherDetails"].Answer : "", new { @id = "ResumptionOfCare_GenericEyesOtherDetails", @maxlength = "20" })%></div>
                            </td><td colspan="2">
                                <label for="ResumptionOfCare_GenericEyesLastEyeExamDate" class="radio more small">Date of Last Eye Exam</label>
                                <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_GenericEyesLastEyeExamDate").Value(data.ContainsKey("GenericEyesLastEyeExamDate") && data["GenericEyesLastEyeExamDate"].Answer.IsNotNullOrEmpty() ? data["GenericEyesLastEyeExamDate"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_GenericEyesLastEyeExamDate", @class = "date" }) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Ears</legend>
        <% string[] ears = data.ContainsKey("GenericEars") && data["GenericEars"].Answer != "" ? data["GenericEars"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_GenericEars" value="" />
        <div class="wide_column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEars1' name='ResumptionOfCare_GenericEars' value='1' {0} />", ears!=null && ears.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEars1" class="radio">WNL (Within Normal Limits)</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEars3' name='ResumptionOfCare_GenericEars' value='3' {0} />", ears!=null && ears.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEars3" class="radio">Deaf</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEars4' name='ResumptionOfCare_GenericEars' value='4' {0} />", ears!=null && ears.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEars4" class="radio">Drainage</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEars5' name='ResumptionOfCare_GenericEars' value='5' {0} />", ears!=null && ears.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEars5" class="radio">Pain</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEars2' name='ResumptionOfCare_GenericEars' value='2' {0} />", ears!=null && ears.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEars2" class="radio more small">Hearing Impaired</label>
                            </td><td>
                                <div id="ResumptionOfCare_GenericEars2More">
                                    <%= Html.Hidden("ResumptionOfCare_GenericEarsHearingImpairedPosition")%>
                                    <%= Html.RadioButton("ResumptionOfCare_GenericEarsHearingImpairedPosition", "0", data.ContainsKey("GenericEarsHearingImpairedPosition") && data["GenericEarsHearingImpairedPosition"].Answer == "0" ? true : false, new { @id = "ResumptionOfCare_GenericEarsHearingImpairedPosition0", @class = "radio" }) %>
                                    <label for="ResumptionOfCare_GenericEarsHearingImpairedPosition0" class="inlineradio">Bilateral</label>
                                    <%= Html.RadioButton("ResumptionOfCare_GenericEarsHearingImpairedPosition", "1", data.ContainsKey("GenericEarsHearingImpairedPosition") && data["GenericEarsHearingImpairedPosition"].Answer == "1" ? true : false, new { @id = "ResumptionOfCare_GenericEarsHearingImpairedPosition1", @class = "radio" }) %>
                                    <label for="ResumptionOfCare_GenericEarsHearingImpairedPosition1" class="inlineradio">Left</label>
                                    <%= Html.RadioButton("ResumptionOfCare_GenericEarsHearingImpairedPosition", "2", data.ContainsKey("GenericEarsHearingImpairedPosition") && data["GenericEarsHearingImpairedPosition"].Answer == "2" ? true : false, new { @id = "ResumptionOfCare_GenericEarsHearingImpairedPosition2", @class = "radio" })%>
                                    <label for="ResumptionOfCare_GenericEarsHearingImpairedPosition2" class="inlineradio">Right</label>
                                </div>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEars6' name='ResumptionOfCare_GenericEars' value='6' {0} />", ears!=null && ears.Contains("6") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericEars6" class="radio more small">Hearing Aids</label>
                            </td><td>
                                <div id="ResumptionOfCare_GenericEars6More">
                                    <%= Html.Hidden("ResumptionOfCare_GenericEarsHearingAidsPosition") %>
                                    <%= Html.RadioButton("ResumptionOfCare_GenericEarsHearingAidsPosition", "0", data.ContainsKey("GenericEarsHearingAidsPosition") && data["GenericEarsHearingAidsPosition"].Answer == "0" ? true : false, new { @id = "ResumptionOfCare_GenericEarsHearingAidsPosition0", @class = "radio" })%>
                                    <label for="ResumptionOfCare_GenericEarsHearingAidsPosition0" class="inlineradio">Bilateral</label>
                                    <%= Html.RadioButton("ResumptionOfCare_GenericEarsHearingAidsPosition", "1", data.ContainsKey("GenericEarsHearingAidsPosition") && data["GenericEarsHearingAidsPosition"].Answer == "1" ? true : false, new { @id = "ResumptionOfCare_GenericEarsHearingAidsPosition1", @class = "radio" })%>
                                    <label for="ResumptionOfCare_GenericEarsHearingAidsPosition1" class="inlineradio">Left</label>
                                    <%= Html.RadioButton("ResumptionOfCare_GenericEarsHearingAidsPosition", "2", data.ContainsKey("GenericEarsHearingAidsPosition") && data["GenericEarsHearingAidsPosition"].Answer == "2" ? true : false, new { @id = "ResumptionOfCare_GenericEarsHearingAidsPosition2", @class = "radio" }) %>
                                    <label for="ResumptionOfCare_GenericEarsHearingAidsPosition2" class="inlineradio">Right</label>
                                </div>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericEars7' name='ResumptionOfCare_GenericEars' value='7' {0} />", ears != null && ears.Contains("7") ? "checked='checked'" : "")%>
                                <label for="ResumptionOfCare_GenericEars7" class="radio more small">Other</label>
                            </td><td colspan="3">
                                <div id="ResumptionOfCare_GenericEars7More"><label for="ResumptionOfCare_GenericEarsOtherDetails"><em>(Specify)</em></label><%= Html.TextBox("ResumptionOfCare_GenericEarsOtherDetails", data.ContainsKey("GenericEarsOtherDetails") ? data["GenericEarsOtherDetails"].Answer : "", new { @id = "ResumptionOfCare_GenericEarsOtherDetails", @maxlength = "20" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Nose</legend>
        <% string[] nose = data.ContainsKey("GenericNose") && data["GenericNose"].Answer != "" ? data["GenericNose"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_GenericNose" value=" " />
        <div class="wide_column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericNose1' name='ResumptionOfCare_GenericNose' value='1' {0} />", nose!=null && nose.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNose1" class="radio">WNL (Within Normal Limits)</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericNose2' name='ResumptionOfCare_GenericNose' value='2' {0} />", nose!=null && nose.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNose2" class="radio">Congestion</label>
                            </td><td colspan="2">
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericNose3' name='ResumptionOfCare_GenericNose' value='3' {0} />", nose!=null && nose.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNose3" class="radio">Loss of Smell</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericNose4' name='ResumptionOfCare_GenericNose' value='4' {0} />", nose!=null && nose.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNose4" class="radio more small fixed">Nose Bleeds</label>
                            </td><td>
                                <div id="ResumptionOfCare_GenericNose4More"><label for="ResumptionOfCare_GenericNoseBleedsFrequency"><em>How often?</em></label><%= Html.TextBox("ResumptionOfCare_GenericNoseBleedsFrequency", data.ContainsKey("GenericNoseBleedsFrequency") ? data["GenericNoseBleedsFrequency"].Answer : "", new { @class = "oe", @id = "ResumptionOfCare_GenericNoseBleedsFrequency", @maxlength = "10" })%></div>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericNose5' name='ResumptionOfCare_GenericNose' value='5' {0} />", nose!=null && nose.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="ResumptionOfCare_GenericNose5" class="radio more small fixed">Other</label>
                            </td><td>
                                <div id="ResumptionOfCare_GenericNose5More"><label for="ResumptionOfCare_GenericNoseOtherDetails"><em>(Specify)</em></label><%= Html.TextBox("ResumptionOfCare_GenericNoseOtherDetails", data.ContainsKey("GenericNoseOtherDetails") ? data["GenericNoseOtherDetails"].Answer : "", new { @id = "ResumptionOfCare_GenericNoseOtherDetails", @maxlength = "20" })%></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset class="half float_left">
        <legend>Mouth</legend>
        <% string[] mouth = data.ContainsKey("GenericMouth") && data["GenericMouth"].Answer != "" ? data["GenericMouth"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_GenericMouth" value="" />
        <div class="column">
            <div class="row">
                <div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericMouth1' name='ResumptionOfCare_GenericMouth' value='1' {0} />", mouth != null && mouth.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericMouth1" class="radio">WNL (Within Normal Limits)</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericMouth2' name='ResumptionOfCare_GenericMouth' value='2' {0} />", mouth != null && mouth.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericMouth2" class="radio">Dentures</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericMouth3' name='ResumptionOfCare_GenericMouth' value='3' {0} />", mouth != null && mouth.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericMouth3" class="radio">Difficulty chewing</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericMouth4' name='ResumptionOfCare_GenericMouth' value='4' {0} />", mouth != null && mouth.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericMouth4" class="radio">Dysphagia</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericMouth5' name='ResumptionOfCare_GenericMouth' value='5' {0} />", mouth != null && mouth.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericMouth5" class="radio">Other</label>
                    <div id="ResumptionOfCare_GenericMouth5More" class="float_right"><label for="ResumptionOfCare_GenericMouthOther"><em>(Specify)</em></label><%= Html.TextBox("ResumptionOfCare_GenericMouthOther", data.ContainsKey("GenericMouthOther") ? data["GenericMouthOther"].Answer : "", new { @id = "ResumptionOfCare_GenericMouthOther", @maxlength = "20" })%></div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Throat</legend>
        <% string[] throat = data.ContainsKey("GenericThroat") && data["GenericThroat"].Answer != "" ? data["GenericThroat"].Answer.Split(',') : null; %>
        <div class="column">
            <div class="row">
                <div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericThroat1' name='ResumptionOfCare_GenericThroat' value='1' {0} />", throat != null && throat.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericThroat1" class="radio">WNL (Within Normal Limits)</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericThroat2' name='ResumptionOfCare_GenericThroat' value='2' {0} />", throat != null && throat.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericThroat2" class="radio">Sore throat</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericThroat3' name='ResumptionOfCare_GenericThroat' value='3' {0} />", throat != null && throat.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericThroat3" class="radio">Hoarseness</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input class='radio float_left' type='checkbox' id='ResumptionOfCare_GenericThroat4' name='ResumptionOfCare_GenericThroat' value='4' {0} />", throat != null && throat.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericThroat4" class="radio">Other</label>
                    <div id="ResumptionOfCare_GenericThroat4More" class="float_right"><label for="ResumptionOfCare_GenericThroatOther"><em>(Specify)</em></label><%= Html.TextBox("ResumptionOfCare_GenericThroatOther", data.ContainsKey("GenericThroatOther") ? data["GenericThroatOther"].Answer : "", new { @id = "ResumptionOfCare_GenericThroatOther", @maxlength = "20" })%></div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1200');">(M1200)</a> Vision (with corrective lenses if the patient usually wears them)</label>
                <%= Html.Hidden("ResumptionOfCare_M1200Vision") %>
                <div>
                    <%= Html.RadioButton("ResumptionOfCare_M1200Vision", "00", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1200Vision00", @class = "radio float_left" }) %>
                    <label for="ResumptionOfCare_M1200Vision00"><span class="float_left">0 &ndash;</span><span class="normal margin">Normal vision: sees adequately in most situations; can see medication labels, newsprint.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1200Vision", "01", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1200Vision01", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1200Vision01"><span class="float_left">1 &ndash;</span><span class="normal margin">Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm&rsquo;s length.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1200');">?</div>
                    </div>
                    <%= Html.RadioButton("ResumptionOfCare_M1200Vision", "02", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1200Vision02", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1200Vision02"><span class="float_left">2 &ndash;</span><span class="normal margin">Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1210');">(M1210)</a> Ability to hear (with hearing aid or hearing appliance if normally used)</label>
                <%= Html.Hidden("ResumptionOfCare_M1210Hearing") %>
                <div>
                    <%= Html.RadioButton("ResumptionOfCare_M1210Hearing", "00", data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1210Hearing00", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1210Hearing00"><span class="float_left">0 &ndash;</span><span class="normal margin">Adequate: hears normal conversation without difficulty.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1210Hearing", "01", data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1210Hearing01", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1210Hearing01"><span class="float_left">1 &ndash;</span><span class="normal margin">Mildly to Moderately Impaired: difficulty hearing in some environments or speaker may need to increase volume or speak distinctly.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1210Hearing", "02", data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1210Hearing02", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1210Hearing02"><span class="float_left">2 &ndash;</span><span class="normal margin">Severely Impaired: absence of useful hearing.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1210');">?</div>
                    </div>
                    <%= Html.RadioButton("ResumptionOfCare_M1210Hearing", "UK", data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "UK" ? true : false, new { @id = "ResumptionOfCare_M1210HearingUK", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1210HearingUK"><span class="float_left">UK &ndash;</span><span class="normal margin">Unable to assess hearing.</span></label>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1220');">(M1220)</a> Understanding of Verbal Content in patient&rsquo;s own language (with hearing aid or device if used)</label>
                <%= Html.Hidden("ResumptionOfCare_M1220VerbalContent") %>
                <div>
                    <%= Html.RadioButton("ResumptionOfCare_M1220VerbalContent", "00", data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1220VerbalContent00", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1220VerbalContent00"><span class="float_left">0 &ndash;</span><span class="normal margin">Understands: clear comprehension without cues or repetitions.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1220VerbalContent", "01", data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1220VerbalContent01", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1220VerbalContent01"><span class="float_left">1 &ndash;</span><span class="normal margin">Usually Understands: understands most conversations, but misses some part/intent of message. Requires cues at times to understand.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1220VerbalContent", "02", data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1220VerbalContent02", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1220VerbalContent02"><span class="float_left">2 &ndash;</span><span class="normal margin">Sometimes Understands: understands only basic conversations or simple, direct phrases. Frequently requires cues to understand.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1220VerbalContent", "03", data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "03" ? true : false, new { @id = "ResumptionOfCare_M1220VerbalContent03", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1220VerbalContent03"><span class="float_left">3 &ndash;</span><span class="normal margin">Rarely/Never Understands.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1220');">?</div>
                    </div>
                    <%= Html.RadioButton("ResumptionOfCare_M1220VerbalContent", "UK", data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "UK" ? true : false, new { @id = "ResumptionOfCare_M1220VerbalContentUK", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1220VerbalContentUK"><span class="float_left">UK &ndash;</span><span class="normal margin">Unable to assess understanding.</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1230');">(M1230)</a> Speech and Oral (Verbal) Expression of Language (in patient&rsquo;s own language)</label>
                <%= Html.Hidden("ResumptionOfCare_M1230SpeechAndOral") %>
                <div>
                    <%= Html.RadioButton("ResumptionOfCare_M1230SpeechAndOral", "00", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1230SpeechAndOral00", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1230SpeechAndOral00"><span class="float_left">0 &ndash;</span><span class="normal margin">Expresses complex ideas, feelings, and needs clearly, completely, and easily in all situations with no observable impairment.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1230SpeechAndOral", "01", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1230SpeechAndOral01", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1230SpeechAndOral01"><span class="float_left">1 &ndash;</span><span class="normal margin">Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional errors in word choice, grammar or speech intelligibility; needs minimal prompting or assistance).</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1230SpeechAndOral", "02", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1230SpeechAndOral02", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1230SpeechAndOral02"><span class="float_left">2 &ndash;</span><span class="normal margin">Expresses simple ideas or needs with moderate difficulty (needs prompting or assistance, errors in word choice, organization or speech intelligibility). Speaks in phrases or short sentences.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1230SpeechAndOral", "03", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "03" ? true : false, new { @id = "ResumptionOfCare_M1230SpeechAndOral03", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1230SpeechAndOral03"><span class="float_left">3 &ndash;</span><span class="normal margin">Has severe difficulty expressing basic ideas or needs and requires maximal assistance or guessing by listener. Speech limited to single words or short phrases.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%= Html.RadioButton("ResumptionOfCare_M1230SpeechAndOral", "04", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "04" ? true : false, new { @id = "ResumptionOfCare_M1230SpeechAndOral04", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1230SpeechAndOral04"><span class="float_left">4 &ndash;</span><span class="normal margin">Unable to express basic needs even with maximal prompting or assistance but is not comatose or unresponsive (e.g., speech is nonsensical or unintelligible).</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1230');">?</div>
                    </div>
                    <%= Html.RadioButton("ResumptionOfCare_M1230SpeechAndOral", "05", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "05" ? true : false, new { @id = "ResumptionOfCare_M1230SpeechAndOral05", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1230SpeechAndOral05"><span class="float_left">5 &ndash;</span><span class="normal margin">Patient nonresponsive or unable to speak.</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Interventions</legend>
        <% string[] sensoryStatusIntervention = data.ContainsKey("485SensoryStatusIntervention") && data["485SensoryStatusIntervention"].Answer != "" ? data["485SensoryStatusIntervention"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485SensoryStatusIntervention" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input class='radio float_left' id='ResumptionOfCare_485SensoryStatusIntervention1' name='ResumptionOfCare_485SensoryStatusIntervention' value='1' type='checkbox' {0} />", sensoryStatusIntervention!=null && sensoryStatusIntervention.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_485SensoryStatusIntervention1" class="radio">ST to evaluate.</label>
            </div><div class="row">
                <label for="ResumptionOfCare_485SensoryStatusOrderTemplates" class="strong">Additional Orders:</label>
                <%  var sensoryStatusOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485SensoryStatusOrderTemplates") && data["485SensoryStatusOrderTemplates"].Answer != "" ? data["485SensoryStatusOrderTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("ResumptionOfCare_485SensoryStatusOrderTemplates", sensoryStatusOrderTemplates) %>
                <%= Html.TextArea("ResumptionOfCare_485SensoryStatusInterventionComments", data.ContainsKey("485SensoryStatusInterventionComments") ? data["485SensoryStatusInterventionComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485SensoryStatusInterventionComments" }) %>
            </div>
        </div>
    </fieldset><fieldset>
        <legend>Goals</legend>
        <div class="wide_column">
            <div class="row">
                <label for="ResumptionOfCare_485SensoryStatusGoalTemplates" class="strong">Additional Goals:</label>
                <%  var sensoryStatusGoalTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485SensoryStatusGoalTemplates") && data["485SensoryStatusGoalTemplates"].Answer != "" ? data["485SensoryStatusGoalTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("ResumptionOfCare_485SensoryStatusGoalTemplates", sensoryStatusGoalTemplates) %>
                <%= Html.TextArea("ResumptionOfCare_485SensoryStatusGoalComments", data.ContainsKey("485SensoryStatusGoalComments") ? data["485SensoryStatusGoalComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485SensoryStatusGoalComments" }) %>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"ROC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('ResumptionOfCare_ValidationContainer','{0}','{1}','{2}','ResumptionOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfChecked($("#ResumptionOfCare_GenericEyes13"), $("#ResumptionOfCare_GenericEyes13More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericEars2"), $("#ResumptionOfCare_GenericEars2More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericEars6"), $("#ResumptionOfCare_GenericEars6More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericEars7"), $("#ResumptionOfCare_GenericEars7More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNose4"), $("#ResumptionOfCare_GenericNose4More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericNose5"), $("#ResumptionOfCare_GenericNose5More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericMouth5"), $("#ResumptionOfCare_GenericMouth5More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericThroat4"), $("#ResumptionOfCare_GenericThroat4More"));
</script>