﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisTransferInPatientDischargedDemographicsForm" })) {
        var data = Model.ToDictionary(); %>
        <%= Html.Hidden("TransferInPatientDischarged_Id", Model.Id)%>
        <%= Html.Hidden("TransferInPatientDischarged_Action", "Edit")%>
        <%= Html.Hidden("TransferInPatientDischarged_PatientGuid", Model.PatientId)%>
        <%= Html.Hidden("TransferInPatientDischarged_EpisodeId", Model.EpisodeId)%>
        <%= Html.Hidden("assessment", "TransferInPatientDischarged")%>
<fieldset class="oasis">
    <legend>Patient Information</legend>
    <div class="column">
        <div class="row">
            <label for="TransferInPatientDischarged_M0020PatientIdNumber" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0020');">(M0020)</a> ID Number:</label>
            <div class="float_right oasis"><%= Html.TextBox("TransferInPatientDischarged_M0020PatientIdNumber", data.ContainsKey("M0020PatientIdNumber") ? data["M0020PatientIdNumber"].Answer : "", new { @id = "TransferInPatientDischarged_M0020PatientIdNumber" }) %><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0020');">?</div></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0040FirstName" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0040');">(M0040)</a> First Name:</label>
            <div class="float_right oasis"><%= Html.TextBox("TransferInPatientDischarged_M0040FirstName", data.ContainsKey("M0040FirstName") ? data["M0040FirstName"].Answer : "", new { @id = "TransferInPatientDischarged_M0040FirstName"}) %><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0040');">?</div></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0040MI" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0040');">(M0040)</a> MI:</label>
            <div class="float_right oasis"><%= Html.TextBox("TransferInPatientDischarged_M0040MI", data.ContainsKey("M0040MI") ? data["M0040MI"].Answer : "", new { @id = "TransferInPatientDischarged_M0040MI", @class = "mi" }) %><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0040');">?</div></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0040LastName" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0040');">(M0040)</a> Last Name:</label>
            <div class="float_right oasis"><%= Html.TextBox("TransferInPatientDischarged_M0040LastName", data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "", new { @id = "TransferInPatientDischarged_M0040LastName" }) %><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0040');">?</div></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0040Suffix" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0040');">(M0040)</a> Suffix:</label>
            <div class="float_right oasis"><%= Html.TextBox("TransferInPatientDischarged_M0040Suffix", data.ContainsKey("M0040Suffix") ? data["M0040Suffix"].Answer : "", new { @id = "TransferInPatientDischarged_M0040Suffix", @class = "mi" }) %><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0040');">?</div></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0050PatientState" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0050');">(M0050)</a> State, <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0060');">(M0060)</a> Zip:</label>
            <div class="float_right oasis"><select class="AddressStateCode" name="TransferInPatientDischarged_M0050PatientState" id="TransferInPatientDischarged_M0050PatientState"><option value="0" selected>-- Select State --</option></select><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0050');">?</div><%= Html.TextBox("TransferInPatientDischarged_M0060PatientZipCode", data.ContainsKey("M0060PatientZipCode") ? data["M0060PatientZipCode"].Answer : "", new { @id = "TransferInPatientDischarged_M0060PatientZipCode", @class="zip" }) %><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0060');">?</div></div>
        </div><div class="row">
            <label class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0069');">(M0069)</a> Gender:</label><%= Html.Hidden("TransferInPatientDischarged_M0069Gender", " ", new { }) %>
            <div class="float_right oasis"><%= Html.RadioButton("TransferInPatientDischarged_M0069Gender", "1", data.ContainsKey("M0069Gender") && data["M0069Gender"].Answer == "1" ? true : false, new { @id = "TransferInPatientDischarged_M0069GenderMale", @class = "radio" })%><label for="TransferInPatientDischarged_M0069GenderMale" class="inlineradio">Male</label><%= Html.RadioButton("TransferInPatientDischarged_M0069Gender", "2", data.ContainsKey("M0069Gender") && data["M0069Gender"].Answer == "2" ? true : false, new { @id = "TransferInPatientDischarged_M0069GenderFemale", @class = "radio" })%><label for="TransferInPatientDischarged_M0069GenderFemale" class="inlineradio">Female</label><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0069');">?</div></div>
        </div>
    </div><div class="column">
        <div class="row">
            <label for="TransferInPatientDischarged_M0064PatientSSN" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0064');">(M0064)</a> Social Security Number:</label>
            <div class="float_right oasis"><input type="text" id="TransferInPatientDischarged_M0064PatientSSN" name="TransferInPatientDischarged_M0064PatientSSN" value=""><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0064');">?</div></div>
            <div class="clear"></div><div class="align_right"><input type="hidden" name="TransferInPatientDischarged_M0064PatientSSNUnknown" value="" />
                <%= string.Format("<input id='TransferInPatientDischarged_M0064PatientSSNUnknown' name='TransferInPatientDischarged_M0064PatientSSNUnknown' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M0064PatientSSNUnknown") && data["M0064PatientSSNUnknown"].Answer == "1" ? "checked='checked'" : "")%>
                <label for="TransferInPatientDischarged_M0064PatientSSNUnknown">UK – Unknown or Not Available</label></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0063PatientMedicareNumber" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0063');">(M0063)</a> Medicare Number:</label>
            <div class="float_right oasis"><%= Html.TextBox("TransferInPatientDischarged_M0063PatientMedicareNumber", data.ContainsKey("M0063PatientMedicareNumber") ? data["M0063PatientMedicareNumber"].Answer : "", new { @id = "TransferInPatientDischarged_M0063PatientMedicareNumber" }) %><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0063');">?</div></div>
            <div class="clear"></div><div class="align_right"><input type="hidden" name="TransferInPatientDischarged_M0063PatientMedicareNumberUnknown" value="" />
                <%= string.Format("<input id='TransferInPatientDischarged_M0063PatientMedicareNumberUnknown' name='TransferInPatientDischarged_M0063PatientMedicareNumberUnknown' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M0063PatientMedicareNumberUnknown") && data["M0063PatientMedicareNumberUnknown"].Answer == "1" ? "checked='checked'" : "")%>
                <label for="TransferInPatientDischarged_M0063PatientMedicareNumberUnknown" >NA – No Medicare</label></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0065PatientMedicaidNumber" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0065');">(M0065)</a> Medicaid Number:</label>
            <div class="float_right oasis"><%= Html.TextBox("TransferInPatientDischarged_M0065PatientMedicaidNumber", data.ContainsKey("M0065PatientMedicaidNumber") ? data["M0065PatientMedicaidNumber"].Answer : "", new { @id = "TransferInPatientDischarged_M0065PatientMedicaidNumber" }) %><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0065');">?</div></div>
            <div class="clear"></div><div class="align_right"><input type="hidden" name="TransferInPatientDischarged_M0065PatientMedicaidNumberUnknown" value="" />
                <%= string.Format("<input id='TransferInPatientDischarged_M0065PatientMedicaidNumberUnknown' name='TransferInPatientDischarged_M0065PatientMedicaidNumberUnknown' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M0065PatientMedicaidNumberUnknown") && data["M0065PatientMedicaidNumberUnknown"].Answer == "1" ? "checked='checked'" : "")%>
                <label for="TransferInPatientDischarged_M0065PatientMedicaidNumberUnknown">NA – No Medicaid</label></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0066PatientDoB" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0066');">(M0066)</a> Birth Date:</label>
            <div class="float_right oasis"><%= Html.Telerik().DatePicker().Name("TransferInPatientDischarged_M0066PatientDoB").Value(data.ContainsKey("M0066PatientDoB") ? data["M0066PatientDoB"].Answer : DateTime.Today.ToString("MM/dd/yyy")).HtmlAttributes(new { @id = "TransferInPatientDischarged_M0066PatientDoB", @class = "date" })%><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0066');">?</div></div>
        </div>
    </div>
</fieldset>
<fieldset class="oasis">
    <legend>Episode Information</legend>
    <div class="column">
        <div class="row">
            <label for="TransferInPatientDischarged_M0030SocDate" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0030');">(M0030)</a> Start of Care Date:</label>
            <div class="float_right oasis"><%= Html.Telerik().DatePicker().Name("TransferInPatientDischarged_M0030SocDate").Value(data.ContainsKey("M0030SocDate") ? data["M0030SocDate"].Answer : DateTime.Today.ToString("MM/dd/yyy")).HtmlAttributes(new { @id = "TransferInPatientDischarged_M0030SocDate", @class = "date" })%><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0030');">?</div></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_GenericEpisodeStartDate" class="float_left">Episode Start Date:</label>
            <div class="float_right"><%= Html.Telerik().DatePicker().Name("TransferInPatientDischarged_GenericEpisodeStartDate").Value(data.ContainsKey("GenericEpisodeStartDate") ? data["GenericEpisodeStartDate"].Answer : DateTime.Today.ToString("MM/dd/yyy")).HtmlAttributes(new { @id = "TransferInPatientDischarged_GenericEpisodeStartDate", @class = "date" })%></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0032ROCDate" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0032');">(M0032)</a> Resumption of Care Date:</label>
            <div class="float_right oasis"><%= Html.Telerik().DatePicker().Name("TransferInPatientDischarged_M0032ROCDate").Value(data.ContainsKey("M0032ROCDate") ? data["M0032ROCDate"].Answer : DateTime.Today.ToString("MM/dd/yyy")).HtmlAttributes(new { @id = "TransferInPatientDischarged_M0032ROCDate", @class = "date" })%><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0032');">?</div></div>
            <div class="clear"></div><div class="align_right">
                <%= string.Format("<input id='TransferInPatientDischarged_M0032ROCDateNotApplicable' name='TransferInPatientDischarged_M0032ROCDateNotApplicable' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M0032ROCDateNotApplicable") && data["M0032ROCDateNotApplicable"].Answer == "1" ? "checked='checked'" : "") %>
                NA - Not Applicable</div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0090AssessmentCompleted" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0090');">(M0090)</a> Date Assessment Completed:</label>
            <div class="float_right oasis"><%= Html.Telerik().DatePicker().Name("TransferInPatientDischarged_M0090AssessmentCompleted").Value(data.ContainsKey("M0090AssessmentCompleted") ? data["M0090AssessmentCompleted"].Answer : DateTime.Today.ToString("MM/dd/yyy")).HtmlAttributes(new { @id = "TransferInPatientDischarged_M0090AssessmentCompleted", @class = "date" })%><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0090');">?</div></div>
        </div>
    </div><div class="column">
        <div class="row">
            <label for="TransferInPatientDischarged_M0080DisciplinePerson" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0080');">(M0080)</a> Discipline of Person Completing Assessment:</label>
            <div class="float_right oasis"><%= Html.Hidden("TransferInPatientDischarged_M0080DisciplinePerson", " ", new { @id = "" })%><select name="TransferInPatientDischarged_M0080DisciplinePerson" id="TransferInPatientDischarged_M0080DisciplinePerson"><option value="01" '<% if (data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "01") { %>selected="selected"<% } %>'">1 - RN</option><option value="02" '<% if (data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "02") { %>selected="selected"<% } %>'">2 - PT</option><option value="03" '<% if (data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "03") { %>selected="selected"<% } %>'">3 - SLP/ST</option><option value="04" '<% if (data.ContainsKey("M0080DisciplinePerson") && data["M0080DisciplinePerson"].Answer == "04") { %>selected="selected"<% } %>'">4 - OT</option></select><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0080');">?</div></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0010CertificationNumber" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0010');">(M0010)</a> CMS Certification Number:</label>
            <div class="float_right oasis"><%= Html.TextBox("TransferInPatientDischarged_M0010CertificationNumber", data.ContainsKey("M0010CertificationNumber") ? data["M0010CertificationNumber"].Answer : "", new { @id = "TransferInPatientDischarged_M0010CertificationNumber" }) %><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0010');">?</div></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0014BranchState" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0014');">(M0014)</a> Branch State:</label>
            <div class="float_right oasis"><select class="AddressStateCode" name="TransferInPatientDischarged_M0014BranchState" id="TransferInPatientDischarged_M0014BranchState"><option value="" selected>-- Select State --</option></select><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0014');">?</div></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0016BranchId" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0016');">(M0016)</a> Branch ID Number:</label>
            <div class="float_right oasis"><%= Html.TextBox("TransferInPatientDischarged_M0016BranchId", data.ContainsKey("M0016BranchId") ? data["M0016BranchId"].Answer : "", new { @id = "TransferInPatientDischarged_M0016BranchId" }) %><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0016');">?</div></div>
        </div><div class="row">
            <label for="TransferInPatientDischarged_M0018NationalProviderId" class="float_left"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0018');">(M0018)</a> National Provider Identifier (NPI):</label>
            <div class="float_right oasis"><%= Html.TextBox("TransferInPatientDischarged_M0018NationalProviderId", data.ContainsKey("M0018NationalProviderId") ? data["M0018NationalProviderId"].Answer : "", new { @id = "TransferInPatientDischarged_M0018NationalProviderId" }) %><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0018');">?</div></div>
            <div class="clear"></div><div class="align_right"><input type="hidden" name="TransferInPatientDischarged_M0018NationalProviderIdUnknown" value=" " />
                <%= string.Format("<input type='checkbox' id='TransferInPatientDischarged_M0018NationalProviderIdUnknown' name='TransferInPatientDischarged_M0018NationalProviderIdUnknown' class='radio' value='1' {0} />", data.ContainsKey("M0018NationalProviderIdUnknown") && data["M0018NationalProviderIdUnknown"].Answer == "1" ? "checked='checked'" : "") %>
                <label for="TransferInPatientDischarged_M0018NationalProviderIdUnknown">UK – Unknown or Not Available</label></div>
        </div>
    </div>
</fieldset>
<fieldset class="oasis half float_left">
    <legend>Assessment Reason</legend>
    <div class="column">
        <div class="row strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0100');">(M0100)</a> Reason for this Assessment:</div>
        <div class="row"><div>Start/Resumption of Care:</div>
            <div><input id="TransferInPatientDischarged_M0100AssessmentType1" name="TransferInPatientDischarged_M0100AssessmentType" type="radio" class="radio float_left" value="01" /><label for="TransferInPatientDischarged_M0100AssessmentType1"><span class="float_left">1 &ndash;</span><span class="normal margin">Start of care&mdash;further visits planned</span></label></div>
            <div><input id="TransferInPatientDischarged_M0100AssessmentType3" name="TransferInPatientDischarged_M0100AssessmentType" type="radio" class="radio float_left" value="03" /><label for="TransferInPatientDischarged_M0100AssessmentType3"><span class="float_left">3 &ndash;</span><span class="normal margin">Resumption of care (after inpatient stay)</span></label></div>
        </div><div class="row"><div>Follow-Up:</div>
            <div><input id="TransferInPatientDischarged_M0100AssessmentType4" name="TransferInPatientDischarged_M0100AssessmentType" type="radio" class="radio float_left" value="04" /><label for="TransferInPatientDischarged_M0100AssessmentType4"><span class="float_left">4 &ndash;</span><span class="normal margin">Recertification (follow-up) reassessment <em>[Go to M0110]</em></span></label></div>
            <div><input id="TransferInPatientDischarged_M0100AssessmentType5" name="TransferInPatientDischarged_M0100AssessmentType" type="radio" class="radio float_left" value="05" /><label for="TransferInPatientDischarged_M0100AssessmentType5"><span class="float_left">5 &ndash;</span><span class="normal margin">Other follow-up <em>[Go to M0110]</em></span></label></div>
        </div><div class="row"><div>Transfer to an Inpatient Facility:</div>
            <div><input id="TransferInPatientDischarged_M0100AssessmentType6" name="TransferInPatientDischarged_M0100AssessmentType" type="radio" class="radio float_left" value="06" /><label for="TransferInPatientDischarged_M0100AssessmentType6"><span class="float_left">6 &ndash;</span><span class="normal margin">Transferred to an inpatient facility—patient not discharged from agency <em>[Go to M1040]</em></span></label></div>
            <div><input id="TransferInPatientDischarged_M0100AssessmentType7" name="TransferInPatientDischarged_M0100AssessmentType" type="radio" class="radio float_left" value="07" /><label for="TransferInPatientDischarged_M0100AssessmentType7"><span class="float_left">7 &ndash;</span><span class="normal margin">Transferred to an inpatient facility—patient discharged from agency <em>[Go to M1040]</em></span></label></div>
        </div><div class="row"><div>Discharge from Agency &mdash; Not to an Inpatient Facility:</div>
            <div><input id="TransferInPatientDischarged_M0100AssessmentType8" name="TransferInPatientDischarged_M0100AssessmentType" type="radio" class="radio float_left" value="08" /><label for="TransferInPatientDischarged_M0100AssessmentType8"><span class="float_left">8 &ndash;</span><span class="normal margin">Death at home <em>[Go to M0903]</em></span></label></div>
            <div><input id="TransferInPatientDischarged_M0100AssessmentType9" name="TransferInPatientDischarged_M0100AssessmentType" type="radio" class="radio float_left" value="09" /><label for="TransferInPatientDischarged_M0100AssessmentType9"><span class="float_left">9 &ndash;</span><span class="normal margin">Discharge from agency <em>[Go to M1040]</em></span></label></div>
            <div class="float_right oasis"><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0100');">?</div></div>
        </div>
    </div>
</fieldset>
<fieldset class="oasis half float_right">
    <legend>Date for Start/Resumption of Care</legend>
    <div class="column">
        <div class="row">
            <label for="TransferInPatientDischarged_M0102PhysicianOrderedDate" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0102');">(M0102)</a> Date of Physician-ordered Start/Resumption of Care:</label>
            <p>If the physician indicated a specific start of care (resumption of care) date when the patient was referred for home health services, record the date specified.</p>
            <div class="float_right oasis"><%= Html.Telerik().DatePicker().Name("TransferInPatientDischarged_M0102PhysicianOrderedDate").Value((data.ContainsKey("M0102PhysicianOrderedDate") && data["M0102PhysicianOrderedDate"].Answer.IsNotNullOrEmpty()) ? data["M0102PhysicianOrderedDate"].Answer : DateTime.Today.ToString("MM/dd/yyy")).HtmlAttributes(new { @id = "TransferInPatientDischarged_M0102PhysicianOrderedDate", @class = "date" })%><input type="hidden" name="TransferInPatientDischarged_M0102PhysicianOrderedDateNotApplicable" value="" /><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0102');">?</div></div>
            <div class="clear"></div><div class="align_right"><em>[Go to M0110, if date entered]</em><br />
                <%= string.Format("<input id='TransferInPatientDischarged_M0102PhysicianOrderedDateNotApplicable' name='TransferInPatientDischarged_M0102PhysicianOrderedDateNotApplicable' class='radio' type='checkbox' value='1' {0} />", data.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && data["M0102PhysicianOrderedDateNotApplicable"].Answer == "1" ? "checked='checked'" : "") %>
                <label for="TransferInPatientDischarged_M0102PhysicianOrderedDateNotApplicable">NA – No specific SOC date ordered by physician</label></div>
        </div>
    </div>
</fieldset>
<fieldset class="oasis half float_right">
    <legend>Date of Referral</legend>
    <div class="column">
        <div class="row">
            <label for="TransferInPatientDischarged_M0104ReferralDate" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0104');">(M0104)</a> Date of Referral:</label>
            <p>Indicate the date that the written or verbal referral for initiation or resumption of care was received by the HHA.</p>
            <div class="float_right oasis"><%= Html.Telerik().DatePicker().Name("TransferInPatientDischarged_M0104ReferralDate").Value((data.ContainsKey("M0104ReferralDate") && data["M0104ReferralDate"].Answer.IsNotNullOrEmpty()) ? data["M0104ReferralDate"].Answer : DateTime.Today.ToString("MM/dd/yyy")).HtmlAttributes(new { @id = "TransferInPatientDischarged_M0104ReferralDate", @class = "date" })%><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0104');">?</div></div>
        </div>
    </div>
</fieldset>
<fieldset class="oasis half float_right">
    <legend>Episode Timing</legend>
    <div class="column">
        <div class="row">
            <label for="TransferInPatientDischarged_M0110EpisodeTiming" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0110');">(M0110)</a> Episode Timing:</label>
            <p>Is the Medicare home health payment episode for which this assessment will define a case mix group an &ldquo;early&rdquo; episode or a &ldquo;later&rdquo; episode in the patient&rsquo;s current sequence of adjacent Medicare home health payment episodes?</p>
            <div class="float_right oasis"><%= Html.Hidden("TransferInPatientDischarged_M0110EpisodeTiming", " ", new { @id = "" }) %><select name="TransferInPatientDischarged_M0110EpisodeTiming" id="TransferInPatientDischarged_M0110EpisodeTiming"><option value="01" '<% if (data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "01") { %>selected='selected'<% } %>'>Early</option><option value="02" '<% if (data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "02") { %>selected='selected'<% } %>'>Later</option><option value="UK" '<% if (data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "UK") { %>selected='selected'<% } %>'>Unknown</option><option value="NA" '<% if (data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "NA") { %>selected='selected'<% } %>'>Not Applicable/No Medicare case mix group</option></select><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0110');">?</div></div>
        </div>
    </div>
</fieldset>
<div class="clear"></div>
<fieldset class="oasis">
    <legend>Race/Ethnicity</legend>
    <table class="form">
        <thead><tr class="firstrow"><th colspan="3"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0140');">(M0140)</a> Race/Ethnicity (Mark all that apply)</th></tr></thead>
        <tbody>
            <tr>
                <td><input type="hidden" name="TransferInPatientDischarged_M0140RaceAMorAN" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0140RaceAMorAN' name='TransferInPatientDischarged_M0140RaceAMorAN' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0140RaceAMorAN") && data["M0140RaceAMorAN"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0140RaceAMorAN"><span class="float_left">1 &ndash;</span><span class="normal margin">American Indian or Alaska Native</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0140RaceAsia" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0140RaceAsia' name='TransferInPatientDischarged_M0140RaceAsia' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0140RaceAsia") && data["M0140RaceAsia"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0140RaceAsia"><span class="float_left">2 &ndash;</span><span class="normal margin">Asian</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0140RaceBalck" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0140RaceBalck' name='TransferInPatientDischarged_M0140RaceBalck' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0140RaceBalck") && data["M0140RaceBalck"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0140RaceBalck"><span class="float_left">3 &ndash;</span><span class="normal margin">Black or African-American</span></label></td>
            </tr><tr>
                <td><input type="hidden" name="TransferInPatientDischarged_M0140RaceHispanicOrLatino" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0140RaceHispanicOrLatino' name='TransferInPatientDischarged_M0140RaceHispanicOrLatino' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0140RaceHispanicOrLatino") && data["M0140RaceHispanicOrLatino"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0140RaceHispanicOrLatino"><span class="float_left">4 &ndash;</span><span class="normal margin">Hispanic or Latino</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0140RaceNHOrPI" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0140RaceNHOrPI' name='TransferInPatientDischarged_M0140RaceNHOrPI' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0140RaceNHOrPI") && data["M0140RaceNHOrPI"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0140RaceNHOrPI"><span class="float_left">5 &ndash;</span><span class="normal margin">Native Hawaiian or Pacific Islander</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0140RaceWhite" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0140RaceWhite' name='TransferInPatientDischarged_M0140RaceWhite' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0140RaceWhite") && data["M0140RaceWhite"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0140RaceWhite"><span class="float_left">6 &ndash;</span><span class="normal margin">White</span></label><div class="float_right oasis"><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0140');">?</div></div></td>
            </tr>
        </tbody>
    </table>
</fieldset>
<fieldset class="oasis">
    <legend>Payment Source</legend>
    <table class="form">
        <thead><tr class="firstrow"><th colspan="3"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M0150');">(M0150)</a> Payment Source (Mark all that apply)</th></tr></thead>
        <tbody>
            <tr>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourceNone" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourceNone' name='TransferInPatientDischarged_M0150PaymentSourceNone' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourceNone") && data["M0150PaymentSourceNone"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourceNone"><span class="float_left">0 &ndash;</span><span class="normal margin">None; no charge for current services</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourceMCREFFS" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourceMCREFFS' name='TransferInPatientDischarged_M0150PaymentSourceMCREFFS' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourceMCREFFS") && data["M0150PaymentSourceMCREFFS"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourceMCREFFS"><span class="float_left">1 &ndash;</span><span class="normal margin">Medicare (traditional fee-for-service)</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourceMCREHMO" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourceMCREHMO' name='TransferInPatientDischarged_M0150PaymentSourceMCREHMO' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourceMCREHMO") && data["M0150PaymentSourceMCREHMO"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourceMCREHMO"><span class="float_left">2 &ndash;</span><span class="normal margin">Medicare (HMO/managed care/Advantage plan)</span></label></td>
            </tr><tr>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourceMCAIDFFS" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourceMCAIDFFS' name='TransferInPatientDischarged_M0150PaymentSourceMCAIDFFS' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourceMCAIDFFS") && data["M0150PaymentSourceMCAIDFFS"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourceMCAIDFFS"><span class="float_left">3 &ndash;</span><span class="normal margin">Medicaid (traditional fee-for-service)</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourceMACIDHMO" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourceMACIDHMO' name='TransferInPatientDischarged_M0150PaymentSourceMACIDHMO' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourceMACIDHMO") && data["M0150PaymentSourceMACIDHMO"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourceMACIDHMO"><span class="float_left">4 &ndash;</span><span class="normal margin">Medicaid (HMO/managed care)</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourceWRKCOMP" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourceWRKCOMP' name='TransferInPatientDischarged_M0150PaymentSourceWRKCOMP' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourceWRKCOMP") && data["M0150PaymentSourceWRKCOMP"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourceWRKCOMP"><span class="float_left">5 &ndash;</span><span class="normal margin">Workers' compensation</span></label></td>
            </tr><tr>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourceTITLPRO" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourceTITLPRO' name='TransferInPatientDischarged_M0150PaymentSourceTITLPRO' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourceTITLPRO") && data["M0150PaymentSourceTITLPRO"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourceTITLPRO"><span class="float_left">6 &ndash;</span><span class="normal margin">Title programs (e.g., Title III, V, or XX)</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourceOTHGOVT" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourceOTHGOVT' name='TransferInPatientDischarged_M0150PaymentSourceOTHGOVT' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourceOTHGOVT") && data["M0150PaymentSourceOTHGOVT"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourceOTHGOVT"><span class="float_left">7 &ndash;</span><span class="normal margin">Other government (e.g., TriCare, VA, etc.)</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourcePRVINS" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourcePRVINS' name='TransferInPatientDischarged_M0150PaymentSourcePRVINS' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourcePRVINS") && data["M0150PaymentSourcePRVINS"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourcePRVINS"><span class="float_left">8 &ndash;</span><span class="normal margin">Private insurance</span></label></td>
            </tr><tr>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourcePRVHMO" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourcePRVHMO' name='TransferInPatientDischarged_M0150PaymentSourcePRVHMO' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourcePRVHMO") && data["M0150PaymentSourcePRVHMO"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourcePRVHMO"><span class="float_left">9 &ndash;</span><span class="normal margin">Private HMO/managed care</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourceSelfPay" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourceSelfPay' name='TransferInPatientDischarged_M0150PaymentSourceSelfPay' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourceSelfPay") && data["M0150PaymentSourceSelfPay"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourceSelfPay"><span class="float_left">10 &ndash;</span><span class="normal margin">Self-pay</span></label></td>
                <td><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourceUnknown" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourceUnknown' name='TransferInPatientDischarged_M0150PaymentSourceUnknown' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourceUnknown") && data["M0150PaymentSourceUnknown"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourceUnknown"><span class="float_left">UK</span><span class="normal margin">Unknown</span></label></td>
            </tr><tr>
                <td colspan="3"><input type="hidden" name="TransferInPatientDischarged_M0150PaymentSourceOtherSRS" value="" />
                    <%= string.Format("<input id='TransferInPatientDischarged_M0150PaymentSourceOtherSRS' name='TransferInPatientDischarged_M0150PaymentSourceOtherSRS' type='checkbox' class='radio float_left' value='1' {0} />", data.ContainsKey("M0150PaymentSourceOtherSRS") && data["M0150PaymentSourceOtherSRS"].Answer == "1" ? "checked='checked'" : "") %>
                    <label for="TransferInPatientDischarged_M0150PaymentSourceOtherSRS"><span class="float_left">11 &ndash;</span><span class="normal margin more">Other (specify)</span></label><%= Html.TextBox("TransferInPatientDischarged_M0150PaymentSourceOther", data.ContainsKey("M0150PaymentSourceOther") ? data["M0150PaymentSourceOther"].Answer : "", new { @id = "TransferInPatientDischarged_M0150PaymentSourceOther" }) %><div class="float_right oasis"><div class="tooltip_oasis" onclick="Oasis.ToolTip('M0150');">?</div></div></td>
            </tr>
        </tbody>
    </table>
</fieldset>
<div class="buttons"><ul>
    <li><a href="javascript:void(0);" onclick="TransferForDischarge.FormSubmit($(this));">Save/Continue</a></li>
    <li><a href="javascript:void(0);" onclick="TransferForDischarge.FormSubmit($(this));">Save/Exit</a></li>
</ul><ul class="float_right">
    <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.Validate('{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "TransferInPatientDischarged")%></li>
</ul></div>
<% } %>
