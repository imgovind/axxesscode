﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<% using (Html.BeginForm("Update", "User", FormMethod.Post, new { @id = "editUserForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_User_Id" }) %>
<div class="wrapper main">
    <fieldset>
        <legend>User Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_User_LastName">Last Name:</label><div class="float_right"><%=Html.TextBox("LastName", Model.LastName, new { @id = "Edit_User_LastName", @maxlength = "30", @class = "required" })%></div></div>
            <div class="row"><label for="Edit_User_FirstName">First Name:</label><div class="float_right"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_User_FirstName", @maxlength = "30", @class = "required" })%></div></div>
            <div class="row"><label for="Edit_User_Suffix">Suffix:</label><div class="float_right"><%= Html.TextBox("Profile.Suffix", Model.Suffix, new { @id = "Edit_User_Suffix", @maxlength = "30", @class = "" })%></div></div>
            <div class="row"><label for="Edit_User_TitleType">Title:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.TitleTypes, "TitleType", Model.TitleType, new { @id = "Edit_User_TitleType", @class = "TitleType valid" })%></div></div>
            <div id="Edit_User_TitleOther_Div" class="row"><label for="Edit_User_OtherTitleType">Other Title (specify):</label><div class="float_right"><%=Html.TextBox("TitleTypeOther", Model.TitleTypeOther, new { @id = "Edit_User_OtherTitleType", @class = "text input_wrapper", @maxlength = "30" })%></div></div>
            <div class="row"></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_User_AddressLine1">Address Line 1:</label><div class="float_right"><%=Html.TextBox("Profile.AddressLine1", Model.Profile.AddressLine1, new { @id = "Edit_User_AddressLine1", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_User_AddressLine2">Address Line 2:</label><div class="float_right"><%=Html.TextBox("Profile.AddressLine2", Model.Profile.AddressLine2, new { @id = "Edit_User_AddressLine2", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_User_AddressCity">City:</label><div class="float_right"><%=Html.TextBox("Profile.AddressCity", Model.Profile.AddressCity, new { @id = "Edit_User_AddressCity", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_User_AddressStateCode">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", Model.Profile.AddressStateCode, new { @id = "Edit_User_AddressStateCode", @class = "AddressStateCode valid" })%><%=Html.TextBox("Profile.AddressZipCode", Model.Profile.AddressZipCode, new { @id = "Edit_User_AddressZipCode", @class = "text numeric input_wrapper zip", @size = "5", @maxlength = "5" })%></div></div>
            <div class="row"><label for="Edit_User_HomePhoneArray1">Home Phone:</label><div class="float_right"><%=Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(0, 3) : "", new { @id = "Edit_User_HomePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(3, 3) : "", new { @id = "Edit_User_HomePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(6, 4) : "", new { @id = "Edit_User_HomePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_User_MobilePhoneArray1">Mobile Phone:</label><div class="float_right"><%=Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(0, 3) : "", new { @id = "Edit_User_MobilePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(3, 3) : "", new { @id = "Edit_User_MobilePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(6, 4) : "", new { @id = "Edit_User_MobilePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a id="Edit_User_SaveButton" href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('edituser');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
