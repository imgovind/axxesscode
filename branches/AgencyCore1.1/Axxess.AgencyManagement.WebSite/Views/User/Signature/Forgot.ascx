﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Reset ",
        Axxess.AgencyManagement.App.Current.User.DisplayName,
        "&rsquo;s Signature','forgotsignature');</script>")%>
<div class="wrapper main">
    <fieldset>
        <legend>Forgot Signature</legend>
        <div class="wide_column">
            <div class="row">Click on the button below to reset your signature. An e-mail with a temporary signature will be sent to <%= Model %>.</div>
            <div id="resetSignatureMessage" class="errormessage"></div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a id="lnkRequestSignatureReset" href="javascript:void(0);">Reset Signature</a></li>
        </ul>
    </div>
</div>



