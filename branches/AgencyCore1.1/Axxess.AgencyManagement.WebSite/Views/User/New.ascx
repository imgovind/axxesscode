﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('New User | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','newuser');</script>")%>
<% using (Html.BeginForm("Add", "User", FormMethod.Post, new { @id = "newUserForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row"><label for="New_User_EmailAddress" class="float_left">E-mail Address:</label><div class="float_right"> <%=Html.TextBox("EmailAddress", "", new { @id = "New_User_EmailAddress", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
            <div class="row"><label for="New_User_FirstName" class="float_left">First Name:</label><div class="float_right"> <%=Html.TextBox("FirstName", "", new { @id = "New_User_FirstName", @class = "text required names input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="New_User_LastName" class="float_left">Last Name:</label><div class="float_right"> <%=Html.TextBox("LastName", "", new { @id = "New_User_LastName", @class = "text required names input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="New_User_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%=Html.TextBox("Profile.AddressLine1", "", new { @id = "New_User_AddressLine1", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_User_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%=Html.TextBox("Profile.AddressLine2", "", new { @id = "New_User_AddressLine2", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_User_AddressCity" class="float_left">City:</label><div class="float_right"><%=Html.TextBox("Profile.AddressCity", "", new { @id = "New_User_AddressCity", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_User_AddressStateCode" class="float_left">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", "", new { @id = "New_User_AddressStateCode", @class = "AddressStateCode valid" })%><%=Html.TextBox("Profile.AddressZipCode", "", new { @id = "New_User_AddressZipCode", @class = "text numeric input_wrapper zip", @size = "5", @maxlength = "5" })%></div></div>
            <div class="row"><label for="New_User_HomePhoneArray1" class="float_left">Home Phone:</label><div class="float_right"><%=Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="New_User_MobilePhoneArray1" class="float_left">Mobile Phone:</label><div class="float_right"><%=Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="New_User_Password" class="float_left">Password:</label><div class="float_right"> <%=Html.TextBox("Password", "", new { @id = "New_User_Password", @class = "text required password", @maxlength = "20" })%><div class="buttons float_right"><ul><li><a href="javascript:void(0);" id="New_User_GeneratePassword">Generate</a></li></ul></div></div></div>
            <div class="row"><label for="New_User_CustomId" class="float_left">Agency Custom Employee Id:</label><div class="float_right"> <%=Html.TextBox("CustomId", "", new { @id = "New_User_CustomId", @class = "text input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="New_User_LocationId" class="float_left">Agency Branch:</label><div class="float_right"> <%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "New_User_LocationId", @class = "BranchLocation required" })%></div></div>
            <div class="row"><label for="New_User_TitleType" class="float_left">Title:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.TitleTypes, "TitleType", "", new { @id = "New_User_TitleType", @class = "TitleType valid" })%></div></div>
            <div class="row"><label for="New_User_OtherTitleType" class="float_left">Other Title (specify):</label><div class="float_right"><%=Html.TextBox("TitleTypeOther", "", new { @id = "New_User_OtherTitleType", @class = "text input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="New_User_Credentials" class="float_left">Credentials:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.CredentialTypes, "Credentials", "", new { @id = "New_User_Credentials", @class = "valid requireddropdown" })%></div></div>
            <div class="row"><label for="New_User_OtherCredentials" class="float_left">Other Credentials (specify):</label><div class="float_right"><%=Html.TextBox("CredentialsOther", "", new { @id = "New_User_OtherCredentials", @class = "text input_wrapper", @maxlength = "10" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Roles</legend>
        <table class="form">
            <tbody>
                <tr class="firstrow">
                    <td><input id="New_User_Role_1" type="checkbox" class="radio required" name="AgencyRoleList" value="1" />&nbsp;<label for="New_User_Role_1">Administrator</label></td>
                    <td><input id="New_User_Role_2" type="checkbox" class="radio required" name="AgencyRoleList" value="2" />&nbsp;<label for="New_User_Role_2">Director of Nursing</label></td>
                    <td><input id="New_User_Role_3" type="checkbox" class="radio required" name="AgencyRoleList" value="3" />&nbsp;<label for="New_User_Role_3">Case Manager</label></td>
                    <td><input id="New_User_Role_4" type="checkbox" class="radio required" name="AgencyRoleList" value="4" />&nbsp;<label for="New_User_Role_4">Nursing</label></td>
                    <td><input id="New_User_Role_5" type="checkbox" class="radio required" name="AgencyRoleList" value="5" />&nbsp;<label for="New_User_Role_5">Clerk (non-clinical)</label></td>
                </tr><tr>
                    <td><input id="New_User_Role_6" type="checkbox" class="radio required" name="AgencyRoleList" value="6" />&nbsp;<label for="New_User_Role_6">Physical Therapist</label></td>
                    <td><input id="New_User_Role_7" type="checkbox" class="radio required" name="AgencyRoleList" value="7" />&nbsp;<label for="New_User_Role_7">Occupational Therapist</label></td>
                    <td><input id="New_User_Role_8" type="checkbox" class="radio required" name="AgencyRoleList" value="8" />&nbsp;<label for="New_User_Role_8">Speech Therapist</label></td>
                    <td><input id="New_User_Role_9" type="checkbox" class="radio required" name="AgencyRoleList" value="9" />&nbsp;<label for="New_User_Role_9">Medicare Social Worker</label></td>
                    <td><input id="New_User_Role_10" type="checkbox" class="radio required" name="AgencyRoleList" value="10" />&nbsp;<label for="New_User_Role_10">Home Health Aide</label></td>
                </tr><tr>
                    <td><input id="New_User_Role_11" type="checkbox" class="radio required" name="AgencyRoleList" value="11" />&nbsp;<label for="New_User_Role_11">Scheduler</label></td>
                    <td><input id="New_User_Role_12" type="checkbox" class="radio required" name="AgencyRoleList" value="12" />&nbsp;<label for="New_User_Role_12">Biller</label></td>
                    <td><input id="New_User_Role_13" type="checkbox" class="radio required" name="AgencyRoleList" value="13" />&nbsp;<label for="New_User_Role_13">Quality Assurance</label></td>
                    <td><input id="New_User_Role_14" type="checkbox" class="radio required" name="AgencyRoleList" value="14" />&nbsp;<label for="New_User_Role_14">Physician</label></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Permissions</legend>
        <div class="wide_column"><input id="New_User_AllPermissions" type="checkbox" class="radio" value="" />&nbsp;<label for="New_User_AllPermissions">Select all permissions</label></div>
        <div class="column">
             <%= Html.PermissionList("General") %>
             <%= Html.PermissionList("Clerical") %>
             <%= Html.PermissionList("Clinical") %>
             <%= Html.PermissionList("OASIS")%>
         </div><div class="column">
             <%= Html.PermissionList("Billing")%>
             <%= Html.PermissionList("QA") %>
             <%= Html.PermissionList("Schedule Management") %>
             <%= Html.PermissionList("Case Management")%>
             <%= Html.PermissionList("Administration")%>
             <%= Html.PermissionList("Reporting")%>
         </div>
    </fieldset>
    <fieldset>
        <legend>Access & Restrictions</legend>
        <div class="column">
            <div class="row"><label for="New_User_AllowWeekendAccess" class="float_left">Allow Weekend Access?</label><div class="float_right"><%= Html.CheckBox("AllowWeekendAccess", false, new { @id="New_User_AllowWeekendAccess" })%></div></div>
            <div class="row"><label for="New_User_EarliestLoginTime" class="float_left">Earliest Login Time:</label><div class="float_right"><input type="text" size="10" id="New_User_EarliestLoginTime" name="EarliestLoginTime" class="spinners" /></div></div>
            <div class="row"><label for="New_User_AutomaticLogoutTime" class="float_left">Automatic Logout Time:</label><div class="float_right"><input type="text" size="10" id="New_User_AutomaticLogoutTime" name="AutomaticLogoutTime" class="spinners" /></div></div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add User</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newuser');">Cancel</a></li>
    </ul></div>
</div>
<%} %>

<script type="text/javascript">
    $("#New_User_EarliestLoginTime").timeEntry({ spinnerImage: '/Images/gui/spinnerUpDown.png',
        spinnerSize: [15, 16, 0], spinnerIncDecOnly: true
    });
    $("#New_User_AutomaticLogoutTime").timeEntry({ spinnerImage: '/Images/gui/spinnerUpDown.png',
        spinnerSize: [15, 16, 0], spinnerIncDecOnly: true
    });
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row div.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>
