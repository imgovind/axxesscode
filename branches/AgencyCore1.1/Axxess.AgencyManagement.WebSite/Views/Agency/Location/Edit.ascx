﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%  using (Html.BeginForm("Update", "Location", FormMethod.Post, new { @id = "editLocationForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Location_Id" })%>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Edit Location | ",
        Model != null ? Model.Name.ToTitleCase() : "",
        "','editlocation');</script>")%>
<div class="wrapper main">
    <fieldset>
        <legend>Location Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Location_Name" class="float_left">Location Name:</label><div class="float_right"><%=Html.TextBox("Name", Model.Name, new { @id = "Edit_Location_Name", @class = "text input_wrapper required", @maxlength = "20", @tabindex = "48" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Location_CustomId" class="float_left">Custom ID:</label><div class="float_right"><%=Html.TextBox("CustomId", Model.CustomId, new { @id = "Edit_Location_CustomId", @class = "text input_wrapper required", @tabindex = "58" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Location Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Location_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%=Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Location_AddressLine1", @class = "text input_wrapper required", @tabindex = "58" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%=Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Location_AddressLine2", @class = "text input_wrapper", tabindex = "59" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressCity" class="float_left">City:</label><div class="float_right"><%=Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Location_AddressCity", @class = "text input_wrapper required", @tabindex = "60" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressStateCode" class="float_left">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Location_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Location_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Location_PhoneArray1" class="float_left">Primary Phone:</label><div class="float_right"><%=Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(0, 3) : "", new { @id = "Edit_Location_PhoneArray1", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "4", @tabindex = "22" })%>&nbsp;-&nbsp;<%=Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(3, 3) : "", new { @id = "Edit_Location_PhoneArray2", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "3", @tabindex = "22" })%>&nbsp;-&nbsp;<%=Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(6, 4) : "", new { @id = "Edit_Location_PhoneArray3", @class = "input_wrappermultible autotext required digits phone_long", @maxlength = "4", @size = "5", @tabindex = "22" })%></div></div>
            <div class="row"><label for="Edit_Location_FaxNumberArray1" class="float_left">Fax Number:</label><div class="float_right"><%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Location_FaxNumberArray1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4", @tabindex = "22" })%>&nbsp;-&nbsp;<%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Location_FaxNumberArray2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3", @tabindex = "22" })%>&nbsp;-&nbsp;<%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Location_FaxNumberArray3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5", @tabindex = "22" })%></div></div>
        </div>
        <table class="form"><tbody>
            <tr class="linesep vert">
                <td><label for="Edit_Location_Comments">Comments:</label><div><%= Html.TextArea("Comments", Model.Comments, new { @id = "Edit_Location_Comments" })%></div></td>
            </tr>
        </tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editlocation');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
