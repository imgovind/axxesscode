﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div class="form_wrapper">
    <fieldset>
        <legend>OASIS Forms</legend>
        <div class="column">
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/StartofCare/View/Blank')">OASIS-C Start of Care/Resumption of Care</a></div>
            <div class="row"><a href="">OASIS-C Follow-Up/Recertification</a></div>
        </div>
        <div class="column">
            <div class="row"><a href="">OASIS-C Discharge</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Nursing Forms</legend>
        <div class="column">
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/HHACarePlan/View/Blank')">HHA Care Plan</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/TransferSummary/View/Blank')">Transfer Summary</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/HHAVisitNote/View/Blank')">HHA Visit Note</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/SixtyDaySummary/View/Blank')">60 Day Summary</a></div>
        </div><div class="column">
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/LVNSupervisoryVisit/View/Blank')">LVN Supervisory Visit</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/HHAideSupervisoryVisit/View/Blank')">HHA Supervisory Visit</a></div>
            <div class="row"><a href="javascript:void(0);" onclick="acore.openprintview('/DischargeSummary/View/Blank')">Discharge Summary</a></div>
        </div>
    </fieldset>
</div>