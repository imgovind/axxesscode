﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Agencies", "Export", FormMethod.Post)) { %>
    <div class="wrapper">
        <%= Html.Telerik().Grid<Agency>().Name("List_Agencies").ToolBar(commnds => commnds.Custom()).Columns(columns => {
        columns.Bound(a => a.Name).Title("Name").Sortable(false);
        columns.Bound(a => a.TaxId).Title("Tax Id").Sortable(false).Width(100);
        columns.Bound(a => a.ContactPersonDisplayName).Title("Contact Person").Sortable(false);
        columns.Bound(a => a.ContactPersonEmail).Title("Contact Email").Width(200).Sortable(false);
        columns.Bound(a => a.ContactPersonPhoneFormatted).Title("Contact Phone").Width(150).Sortable(false);
        columns.Bound(a => a.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Agency.Edit('<#=Id#>');\">Edit</a>").Title("Action").Width(80);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "Agency")).Pageable(paging => paging.PageSize(10)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
        %>
    </div>
<%} %>
<script type="text/javascript">
    $("#List_Agencies .t-grid-toolbar").html("");
    $("#List_Agencies .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22acore.open('newagency'); return false;%22%3ENew Agency%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
    $(".t-grid-content").css({ 'height': 'auto' });
</script>