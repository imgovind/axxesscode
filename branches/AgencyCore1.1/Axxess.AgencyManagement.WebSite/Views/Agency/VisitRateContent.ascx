﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<% var data = Model != null ? Model.ToCostRateDictionary() : new Dictionary<string, CostRate>(); %>
 <table class="fixed insurance">
     <col/><col width=60 /><col /> <col width=60 /><col width=80 />
         <tbody>
                    <tr><th colspan="4" class="align_center borderBottom">Visit Cost Rates <em>(Employee Pay Rates)</em></th></tr>
                    <tr><th class="borderBottom align_left">Disciplines</th><th class="borderBottom align_center" >Per Visit</th><th class="borderBottom  align_center">Per Hour </th><th class="borderBottom  align_center">Per 15 Minute Increment (Unit) </th></tr>
                    <tr>
                        <td><label class="float_left">Skilled Nurse</label><%=Html.Hidden("RateDiscipline", "SkilledNurse")%>  </td>
                        <td class="align_center"><%= Html.TextBox("SkilledNurse_PerVisit", data.ContainsKey("SkilledNurse") ? data["SkilledNurse"].PerVisit : string.Empty, new { @id = "New_Insurance_SkilledNursePerVisit", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("SkilledNurse_PerHour", data.ContainsKey("SkilledNurse") ? data["SkilledNurse"].PerHour : string.Empty, new { @id = "New_Insurance_SkilledNursePerHour", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("SkilledNurse_PerUnit", data.ContainsKey("SkilledNurse") ? data["SkilledNurse"].PerUnit : string.Empty, new { @id = "New_Insurance_SkilledNursePerUnit", @class = "rates currency" })%></td>
                    </tr>
                    <tr>  
                        <td><label class="float_left">Physical Therapy</label><%=Html.Hidden("RateDiscipline", "PhysicalTherapy")%>  </td>
                        <td class="align_center"><%= Html.TextBox("PhysicalTherapy_PerVisit", data.ContainsKey("PhysicalTherapy") ? data["PhysicalTherapy"].PerVisit : string.Empty, new { @id = "New_Insurance_PhysicalTherapyPerVisit", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("PhysicalTherapy_PerHour", data.ContainsKey("PhysicalTherapy") ? data["PhysicalTherapy"].PerHour : string.Empty, new { @id = "New_Insurance_PhysicalTherapyPerHour", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("PhysicalTherapy_PerUnit", data.ContainsKey("PhysicalTherapy") ? data["PhysicalTherapy"].PerUnit : string.Empty, new { @id = "New_Insurance_PhysicalTherapyPerUnit", @class = "rates currency" })%></td>
                    </tr>
                    <tr>  
                        <td><label class="float_left">Occupational Therapy</label><%=Html.Hidden("RateDiscipline", "OccupationalTherapy")%></td>
                        <td class="align_center"><%= Html.TextBox("OccupationalTherapy_PerVisit", data.ContainsKey("OccupationalTherapy") ? data["OccupationalTherapy"].PerVisit : string.Empty, new { @id = "New_Insurance_OccupationalTherapyPerVisit", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("OccupationalTherapy_PerHour", data.ContainsKey("OccupationalTherapy") ? data["OccupationalTherapy"].PerHour : string.Empty, new { @id = "New_Insurance_OccupationalTherapyPerHour", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("OccupationalTherapy_PerUnit", data.ContainsKey("OccupationalTherapy") ? data["OccupationalTherapy"].PerUnit : string.Empty, new { @id = "New_Insurance_OccupationalTherapyPerUnit", @class = "rates currency" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Speech Therapy</label><%=Html.Hidden("RateDiscipline", "SpeechTherapy")%></td>
                        <td class="align_center"><%= Html.TextBox("SpeechTherapy_PerVisit", data.ContainsKey("SpeechTherapy") ? data["SpeechTherapy"].PerVisit : string.Empty, new { @id = "New_Insurance_SpeechTherapyPerVisit", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("SpeechTherapy_PerHour", data.ContainsKey("SpeechTherapy") ? data["SpeechTherapy"].PerHour : string.Empty, new { @id = "New_Insurance_SpeechTherapyPerHoure", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("SpeechTherapy_PerUnit", data.ContainsKey("SpeechTherapy") ? data["SpeechTherapy"].PerUnit : string.Empty, new { @id = "New_Insurance_SpeechTherapyPerUnit", @class = "rates currency" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Medicare Social Worker</label><%=Html.Hidden("RateDiscipline", "MedicareSocialWorker")%></td>
                        <td class="align_center"><%= Html.TextBox("MedicareSocialWorker_PerVisit", data.ContainsKey("MedicareSocialWorker") ? data["MedicareSocialWorker"].PerVisit : string.Empty, new { @id = "New_Insurance_MedicareSocialWorkerPerVisit", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("MedicareSocialWorker_PerHour", data.ContainsKey("MedicareSocialWorker") ? data["MedicareSocialWorker"].PerHour : string.Empty, new { @id = "New_Insurance_MedicareSocialWorkerPerHour", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("MedicareSocialWorker_PerUnit", data.ContainsKey("MedicareSocialWorker") ? data["MedicareSocialWorker"].PerUnit : string.Empty, new { @id = "New_Insurance_MedicareSocialWorkerPerUnit", @class = "rates currency" })%></td>
                    </tr>
                     <tr>
                        <td><label class="float_left">Home Health Aide</label><%=Html.Hidden("RateDiscipline", "HomeHealthAide")%></td>
                        <td class="align_center"><%= Html.TextBox("HomeHealthAide_PerVisit", data.ContainsKey("HomeHealthAide") ? data["HomeHealthAide"].PerVisit : string.Empty, new { @id = "New_Insurance_HomeHealthAidePerVisit", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("HomeHealthAide_PerHour", data.ContainsKey("HomeHealthAide") ? data["HomeHealthAide"].PerHour : string.Empty, new { @id = "New_Insurance_HomeHealthAidePerHour", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("HomeHealthAide_PerUnit", data.ContainsKey("HomeHealthAide") ? data["HomeHealthAide"].PerUnit : string.Empty, new { @id = "New_Insurance_HomeHealthAidePerUnit", @class = "rates currency" })%></td>
                    </tr>
                    <tr>  
                        <td><label class="float_left">Attendant</label><%=Html.Hidden("RateDiscipline", "Attendant")%></td>
                        <td class="align_center"><%= Html.TextBox("Attendant_PerVisit", data.ContainsKey("Attendant") ? data["Attendant"].PerVisit : string.Empty, new { @id = "New_Insurance_AttendantPerVisit", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("Attendant_PerHour", data.ContainsKey("Attendant") ? data["Attendant"].PerHour : string.Empty, new { @id = "New_Insurance_AttendantPerHour", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("Attendant_PerUnit", data.ContainsKey("Attendant") ? data["Attendant"].PerUnit : string.Empty, new { @id = "New_Insurance_AttendantPerUnit", @class = "rates currency" })%></td>
                    </tr>
                    <tr>  
                        <td><label class="float_left">Companion Care</label><%=Html.Hidden("RateDiscipline", "CompanionCare")%></td>
                        <td class="align_center"><%= Html.TextBox("CompanionCare_PerVisit", data.ContainsKey("CompanionCare") ? data["CompanionCare"].PerVisit : string.Empty, new { @id = "New_Insurance_CompanionCarePerVisit", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("CompanionCare_PerHour", data.ContainsKey("CompanionCare") ? data["CompanionCare"].PerHour : string.Empty, new { @id = "New_Insurance_CompanionCarePerHour", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("CompanionCare_PerUnit", data.ContainsKey("CompanionCare") ? data["CompanionCare"].PerUnit : string.Empty, new { @id = "New_Insurance_CompanionCarePerUnit", @class = "rates currency" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Homemaker Services </label><%=Html.Hidden("RateDiscipline", "HomemakerServices")%></td>
                        <td class="align_center"><%= Html.TextBox("HomemakerServices_PerVisit", data.ContainsKey("HomemakerServices") ? data["HomemakerServices"].PerVisit : string.Empty, new { @id = "New_Insurance_HomemakerServicesPerVisit", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("HomemakerServices_PerHour", data.ContainsKey("HomemakerServices") ? data["HomemakerServices"].PerHour : string.Empty, new { @id = "New_Insurance_HomemakerServicesPerHour", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("HomemakerServices_PerUnit", data.ContainsKey("HomemakerServices") ? data["HomemakerServices"].PerUnit : string.Empty, new { @id = "New_Insurance_HomemakerServicesPerUnit", @class = "rates currency" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Private Duty Sitter</label><%=Html.Hidden("RateDiscipline", "PrivateDutySitter")%></td>
                        <td class="align_center"><%= Html.TextBox("PrivateDutySitter_PerVisit", data.ContainsKey("PrivateDutySitter") ? data["PrivateDutySitter"].PerVisit : string.Empty, new { @id = "New_Insurance_PrivateDutySitterPerVisit", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("PrivateDutySitter_PerHour", data.ContainsKey("PrivateDutySitter") ? data["PrivateDutySitter"].PerHour : string.Empty, new { @id = "New_Insurance_PrivateDutySitterPerHour", @class = "rates currency" })%></td>
                        <td class="align_center"><%= Html.TextBox("PrivateDutySitter_PerUnit", data.ContainsKey("PrivateDutySitter") ? data["PrivateDutySitter"].PerUnit : string.Empty, new { @id = "New_Insurance_PrivateDutySitterPerUnit", @class = "rates currency" })%></td>
                    </tr>
       </tbody>
  </table>
