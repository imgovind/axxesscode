﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleActivityArgument>" %>
<% Html.Telerik().Grid<ScheduleEvent>().Name("ScheduleActivityGrid").Columns(columns => {
       columns.Bound(s => s.EventId).Visible(false);
       columns.Bound(s => s.Url).ClientTemplate("<#=Url#>").Title("Task");
       columns.Bound(s => s.EventDate).Format("{0:MM/dd/yyyy}").Title("Target Date").Width(100);
       columns.Bound(s => s.UserName).Title("Assigned To").Width(150);
       columns.Bound(s => s.StatusName).Title("Status");
       columns.Bound(s => s.StatusComment).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip red_note\" href=\"javascript:void(0);\" tooltip=\"<#=StatusComment#>\"> </a>");
       columns.Bound(s => s.Comments).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=Comments#>\"> </a>");
       columns.Bound(s => s.EpisodeNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip blue_note\" href=\"javascript:void(0);\" tooltip=\"<#=EpisodeNotes#>\"></a>");
       columns.Bound(s => s.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(30);
       columns.Bound(s => s.AttachmentUrl).Title(" ").ClientTemplate("<#=AttachmentUrl#>").Width(30);
       columns.Bound(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Title("Action").Width(200);
   }).ClientEvents(c => c.OnRowDataBound("Schedule.tooltip")).DataBinding(dataBinding => dataBinding.Ajax().Select("Activity", "Schedule", new { episodeId = Model.EpisodeId, patientId = Model.PatientId, discipline = Model.Discpline })).Sortable().Scrollable().Footer(false).Render();
%>
<script type="text/javascript"> $('#ScheduleActivityGrid .t-grid-content').css({'height':'auto'}); </script>