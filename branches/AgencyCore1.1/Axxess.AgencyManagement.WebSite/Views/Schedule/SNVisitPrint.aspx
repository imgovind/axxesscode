﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions!=null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Skilled Nurse Progress Note<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body></body><%
string[] skinColor = data != null && data.ContainsKey("GenericSkinColor") && data["GenericSkinColor"].Answer != "" ? data["GenericSkinColor"].Answer.Split(',') : null;
string[] skinCondition = data != null && data.ContainsKey("GenericSkinCondition") && data["GenericSkinCondition"].Answer != "" ? data["GenericSkinCondition"].Answer.Split(',') : null;
string[] genericCardioVascular = data != null && data.ContainsKey("GenericCardioVascular") && data["GenericCardioVascular"].Answer != "" ? data["GenericCardioVascular"].Answer.Split(',') : null;
string[] genericPittingEdemaType = data != null && data.ContainsKey("GenericPittingEdemaType") && data["GenericPittingEdemaType"].Answer != "" ? data["GenericPittingEdemaType"].Answer.Split(',') : null;
string[] genericNeurologicalStatus = data != null && data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer != "" ? data["GenericNeurologicalStatus"].Answer.Split(',') : null;
string[] genericNeurologicalOriented = data != null && data.ContainsKey("GenericNeurologicalOriented") && data["GenericNeurologicalOriented"].Answer != "" ? data["GenericNeurologicalOriented"].Answer.Split(',') : null;
string[] genericNeurologicalVisionStatus = data != null && data.ContainsKey("GenericNeurologicalVisionStatus") && data["GenericNeurologicalVisionStatus"].Answer != "" ? data["GenericNeurologicalVisionStatus"].Answer.Split(',') : null;
string[] genericMusculoskeletal = data != null && data.ContainsKey("GenericMusculoskeletal") && data["GenericMusculoskeletal"].Answer != "" ? data["GenericMusculoskeletal"].Answer.Split(',') : null;
string[] genericAssistiveDevice = data != null && data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer != "" ? data["GenericAssistiveDevice"].Answer.Split(',') : null;
string[] genericBoundType = data != null && data.ContainsKey("GenericBoundType") && data["GenericBoundType"].Answer != "" ? data["GenericBoundType"].Answer.Split(',') : null;
string[] genericDigestive = data != null && data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer != "" ? data["GenericDigestive"].Answer.Split(',') : null;
string[] genericDigestiveLastBM = data != null && data.ContainsKey("GenericDigestiveLastBM") && data["GenericDigestiveLastBM"].Answer != "" ? data["GenericDigestiveLastBM"].Answer.Split(',') : null;
string[] genericDigestiveLastBMAbnormalStool = data != null && data.ContainsKey("GenericDigestiveLastBMAbnormalStool") && data["GenericDigestiveLastBMAbnormalStool"].Answer != "" ? data["GenericDigestiveLastBMAbnormalStool"].Answer.Split(',') : null;
string[] genericDigestiveOstomy = data != null && data.ContainsKey("GenericDigestiveOstomy") && data["GenericDigestiveOstomy"].Answer != "" ? data["GenericDigestiveOstomy"].Answer.Split(',') : null;
string[] isDiabeticCareApplied = data != null && data.ContainsKey("GenericIsDiabeticCareApplied") && data["GenericIsDiabeticCareApplied"].Answer != "" ? data["GenericIsDiabeticCareApplied"].Answer.Split(',') : null;
string[] isIVApplied = data != null && data.ContainsKey("GenericIsIVApplied") && data["GenericIsIVApplied"].Answer != "" ? data["GenericIsIVApplied"].Answer.Split(',') : null;
string[] genericNutrition = data != null && data.ContainsKey("GenericNutrition") && data["GenericNutrition"].Answer != "" ? data["GenericNutrition"].Answer.Split(',') : null;
string[] genericNutritionDiet = data != null && data.ContainsKey("GenericNutritionDiet") && data["GenericNutritionDiet"].Answer != "" ? data["GenericNutritionDiet"].Answer.Split(',') : null;
string[] genericNutritionEnteralFeeding = data != null && data.ContainsKey("GenericNutritionEnteralFeeding") && data["GenericNutritionEnteralFeeding"].Answer != "" ? data["GenericNutritionEnteralFeeding"].Answer.Split(',') : null;
string[] genericNutritionResidualProblem = data != null && data.ContainsKey("GenericNutritionResidualProblem") && data["GenericNutritionResidualProblem"].Answer != "" ? data["GenericNutritionResidualProblem"].Answer.Split(',') : null;
string[] genericGU = data != null && data.ContainsKey("GenericGU") && data["GenericGU"].Answer != "" ? data["GenericGU"].Answer.Split(',') : null;
string[] genericGUUrine = data != null && data.ContainsKey("GenericGUUrine") && data["GenericGUUrine"].Answer != "" ? data["GenericGUUrine"].Answer.Split(',') : null;
string[] diabeticCareDiabeticManagement = data != null && data.ContainsKey("GenericDiabeticCareDiabeticManagement") && data["GenericDiabeticCareDiabeticManagement"].Answer != "" ? data["GenericDiabeticCareDiabeticManagement"].Answer.Split(',') : null;
string[] diabeticCareInsulinAdministeredby = data != null && data.ContainsKey("GenericDiabeticCareInsulinAdministeredby") && data["GenericDiabeticCareInsulinAdministeredby"].Answer != "" ? data["GenericDiabeticCareInsulinAdministeredby"].Answer.Split(',') : null;
string[] genericHyperglycemia = data != null && data.ContainsKey("GenericDiabeticCareHyperglycemia") && data["GenericDiabeticCareHyperglycemia"].Answer != "" ? data["GenericDiabeticCareHyperglycemia"].Answer.Split(',') : null;
string[] genericHypoglycemia = data != null && data.ContainsKey("GenericDiabeticCareHypoglycemia") && data["GenericDiabeticCareHypoglycemia"].Answer != "" ? data["GenericDiabeticCareHypoglycemia"].Answer.Split(',') : null;
string[] infectionControl = data.ContainsKey("InfectionControl") && data["InfectionControl"].Answer != "" ? data["InfectionControl"].Answer.Split(',') : null;
string[] isCareCoordinationAndCommunicationApplied = data != null && data.ContainsKey("GenericIsCareCoordinationAndCommunicationApplied") && data["GenericIsCareCoordinationAndCommunicationApplied"].Answer != "" ? data["GenericIsCareCoordinationAndCommunicationApplied"].Answer.Split(',') : null;
string[] genericCareCoordination = data != null && data.ContainsKey("GenericCareCoordination") && data["GenericCareCoordination"].Answer != "" ? data["GenericCareCoordination"].Answer.Split(',') : null;
string[] pateintReceivedDischarge = data != null && data.ContainsKey("GenericPateintReceivedDischarge") && data["GenericPateintReceivedDischarge"].Answer != "" ? data["GenericPateintReceivedDischarge"].Answer.Split(',') : null;
string[] interventions = data != null && data.ContainsKey("GenericInterventions") && data["GenericInterventions"].Answer != "" ? data["GenericInterventions"].Answer.Split(',') : null;
string[] responseToTeachingInterventions = data != null && data.ContainsKey("GenericResponseToTeachingInterventions") && data["GenericResponseToTeachingInterventions"].Answer != "" ? data["GenericResponseToTeachingInterventions"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().Globalization(true) 
     .DefaultGroup(group => group
     .Add("jquery-1.4.2.min.js")
     .Add("/Modules/printview.js")
     .Compress(true).Combined(true).CacheDurationInDays(5))
     .OnDocumentReady(() => {  %>
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : ""%><%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : ""%>%3Cbr /%3E<%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3ESkilled Nurse%3Cbr /%3EProgress Note%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            '<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %>' +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null ? string.Format(" {0} &ndash; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) : "" %>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EHomebound Status:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("HomeboundStatus") ? data["HomeboundStatus"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EHome Environment:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("HomeEnvironment") ? data["HomeEnvironment"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETertiary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("PrimaryDiagnosis2") ? data["PrimaryDiagnosis2"].Answer : ""%>' +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : ""%><%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : ""%>%3Cbr /%3E<%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3ESkilled Nurse%3Cbr /%3EProgress Note%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            '<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %>' +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cimg src=%22/Images/axxess_print.png%22 class=%22axxess%22 /%3E%3Cimg src=%22/Images/acore_print.png%22 class=%22acore%22 /%3E";
        printview.addsection(
            "<table class='fixed'><tr><td><h3>Vital Signs</h3>" + 
            printview.col(4,
                printview.span("Temp:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericTemp") ? data["GenericTemp"].Answer : ""%> <%= data != null && data.ContainsKey("GenericTempType") ? data["GenericTempType"].Answer : ""%>") +
                printview.span("Resp:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericResp") ? data["GenericResp"].Answer : ""%> <%= data != null && data.ContainsKey("GenericRespType") ? data["GenericRespType"].Answer : ""%>") +
                printview.span("Apical Pulse:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericPulseApical") ? data["GenericPulseApical"].Answer : ""%> <%= data != null && data.ContainsKey("GenericPulseApicalRegular") ? data["GenericPulseApicalRegular"].Answer : ""%>") +
                printview.span("Radial Pulse:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericPulseRadial") ? data["GenericPulseRadial"].Answer : ""%> <%= data != null && data.ContainsKey("GenericPulseRadialRegular") ? data["GenericPulseRadialRegular"].Answer : ""%>") +
                printview.span("BP",true) +
                printview.span("Lying",true) +
                printview.span("Sitting",true) +
                printview.span("Standing",true) +
                printview.span("Left:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericBPLeftLying") ? data["GenericBPLeftLying"].Answer : ""%>") +
                printview.span("<%= data != null && data.ContainsKey("GenericBPLeftSitting") ? data["GenericBPLeftSitting"].Answer : ""%>") +
                printview.span("<%= data != null && data.ContainsKey("GenericBPLeftStanding") ? data["GenericBPLeftStanding"].Answer : ""%>") +
                printview.span("Right:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericBPRightLying") ? data["GenericBPRightLying"].Answer : ""%>") +
                printview.span("<%= data != null && data.ContainsKey("GenericBPRightSitting") ? data["GenericBPRightSitting"].Answer : ""%>") +
                printview.span("<%= data != null && data.ContainsKey("GenericBPRightStanding") ? data["GenericBPRightStanding"].Answer : ""%>") +
                printview.span("Weight:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericWeight") ? data["GenericWeight"].Answer : ""%> <%= data != null && data.ContainsKey("GenericWeightUnit") ? data["GenericWeightUnit"].Answer : ""%>") +
                printview.span("Pulse Oximetry:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericPulseOximetry") ? data["GenericPulseOximetry"].Answer : ""%> <%= data != null && data.ContainsKey("GenericPulseOximetryUnit") ? data["GenericPulseOximetryUnit"].Answer : ""%>") ) +
            printview.span("Comments:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericVitlaSignComment") ? data["GenericVitlaSignComment"].Answer : ""%>") +
            "</td><td><h3>Pain Profile</h3>" +
            printview.col(4,
                printview.span("Pain Intensity:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericIntensityOfPain") ? data["GenericIntensityOfPain"].Answer : ""%>") +
                printview.span("Description:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericQualityOfPain") ? data["GenericQualityOfPain"].Answer : ""%>") +
                printview.span("Duration:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericDurationOfPain") ? data["GenericDurationOfPain"].Answer : ""%>") +
                printview.span("Primary Site:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericLocationOfPain") ? data["GenericLocationOfPain"].Answer : ""%>") ) +
            printview.span("Frequency of Pain Interfering with patient&rsquo;s activity or movement",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericFrequencyOfPain") ? data["GenericFrequencyOfPain"].Answer : ""%>") +
            printview.span("Current Pain Management Effectiveness",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericMedicationEffectiveness") ? data["GenericMedicationEffectiveness"].Answer : ""%>") +
            printview.span("Comments:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPainProfileComment") ? data["GenericPainProfileComment"].Answer : ""%>") +
            "</td></tr></table>");
        printview.addsection(
            "<table class='fixed'><tr><td><h3>Skin</h3>" + 
            printview.col(6,
                printview.span("Color:",true) +
                printview.checkbox("Pink/WNL",<%= skinColor != null && skinColor.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Pallor",<%= skinColor != null && skinColor.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Jaundice",<%= skinColor != null && skinColor.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Cyanotic",<%= skinColor != null && skinColor.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Other <%= data != null && data.ContainsKey("GenericSkinColorOther") ? data["GenericSkinColorOther"].Answer : ""%>",<%= skinColor != null && skinColor.Contains("5") ? "true" : "false"%>) ) +
            printview.col(4,
                printview.span("Temp:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericSkinTemp") ? data["GenericSkinTemp"].Answer : ""%>") +
                printview.span("Turgor:",true) +
                printview.span("<%= data != null && data.ContainsKey("GenericSkinTurgor") ? data["GenericSkinTurgor"].Answer : ""%>")) +
            printview.col(5,
                printview.span("Condition:",true) +
                printview.checkbox("Dry",<%= skinCondition != null && skinCondition.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Diaphoretic",<%= skinCondition != null && skinCondition.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Wound",<%= skinCondition != null && skinCondition.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Ulcer",<%= skinCondition != null && skinCondition.Contains("4") ? "true" : "false"%>) +
                printview.span("") +
                printview.checkbox("Incision",<%= skinCondition != null && skinCondition.Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Rash",<%= skinCondition != null && skinCondition.Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= skinCondition != null && skinCondition.Contains("7") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("GenericSkinConditionOther") ? data["GenericSkinConditionOther"].Answer : ""%>") ) +
            printview.span("Comments:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericSkinComment") ? data["GenericSkinComment"].Answer : ""%>") +
            "</td><td><h3>Respiratory</h3>" +
            printview.col(2,
                printview.col(2,
                    printview.span("Breath Sounds:", true) +
                    printview.span("<%= data != null && data.ContainsKey("GenericRespiratoryBreathSounds") ? data["GenericRespiratoryBreathSounds"].Answer : ""%>") )+
                printview.col(3,
                    printview.checkbox("Bilateral",<%= data != null && data.ContainsKey("GenericRespiratoryBreathSoundsPosition") && data["GenericRespiratoryBreathSoundsPosition"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("Left",<%= data != null && data.ContainsKey("GenericRespiratoryBreathSoundsPosition") && data["GenericRespiratoryBreathSoundsPosition"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("Right",<%= data != null && data.ContainsKey("GenericRespiratoryBreathSoundsPosition") && data["GenericRespiratoryBreathSoundsPosition"].Answer == "2" ? "true" : "false"%>) ) ) +
            printview.col(2,
                printview.col(2,
                    printview.span("Dyspnea:",true) +
                    printview.span("<%= data != null && data.ContainsKey("GenericRespiratoryDyspnea") ? data["GenericRespiratoryDyspnea"].Answer : ""%>") ) +
                printview.col(2,
                    printview.span("Cough:",true) +
                    printview.span("<%= data != null && data.ContainsKey("GenericCoughList") ? data["GenericCoughList"].Answer : ""%>") ) +
                printview.col(2,
                    printview.span("O<sub>2</sub> at:",true) +
                    printview.span("<%= data != null && data.ContainsKey("Generic02AtText") ? data["Generic02AtText"].Answer : ""%>") ) +
                printview.col(2,
                    printview.span("LPM via:",true) +
                    printview.span("<%= data != null && data.ContainsKey("GenericLPMVia") ? data["GenericLPMVia"].Answer : ""%>") ) +
                printview.col(2,
                    printview.span("Freq:",true) +
                    printview.span("<%= data != null && data.ContainsKey("GenericRespiratoryFreq") ? data["GenericRespiratoryFreq"].Answer : ""%>") ) +
                printview.checkbox("O<sub>2</sub> Precautions",<%= data != null && data.ContainsKey("RespiratoryO2Precautions") && data["RespiratoryO2Precautions"].Answer == "1" ? "true" : "false"%>,true) ) +
            printview.span("Comments:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericRespiratoryComment") ? data["GenericRespiratoryComment"].Answer : ""%>") +
            "</td></tr></table>");
        printview.addsection(
            "<table class='fixed'><tr><td><h3>Cardiovascular</h3>" + 
            printview.checkbox("WNL (Within Normal Limits)",<%= genericCardioVascular != null && genericCardioVascular.Contains("1") ? "true" : "false"%>,true) +
            printview.col(2,
                printview.checkbox("Heart Rhythm <%= data != null && data.ContainsKey("GenericHeartSoundsType") ? data["GenericHeartSoundsType"].Answer : ""%>",<%= genericCardioVascular != null && genericCardioVascular.Contains("2") ? "true" : "false"%>,true) +
                printview.checkbox("Cap. Refill <%= data != null && data.ContainsKey("GenericCapRefillLessThan3") ? data["GenericCapRefillLessThan3"].Answer : ""%>",<%= genericCardioVascular != null && genericCardioVascular.Contains("3") ? "true" : "false"%>,true) +
                printview.col(2,
                    printview.checkbox("Pulses:",<%= genericCardioVascular != null && genericCardioVascular.Contains("4") ? "true" : "false"%>,true) +
                    printview.span("Radial: <%= data != null && data.ContainsKey("GenericCardiovascularRadial") ? data["GenericCardiovascularRadial"].Answer : ""%>")) +
                printview.col(3,
                    printview.checkbox("Bilateral",<%= data != null && data.ContainsKey("GenericCardiovascularRadialPosition") && data["GenericCardiovascularRadialPosition"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("Left",<%= data != null && data.ContainsKey("GenericCardiovascularRadialPosition") && data["GenericCardiovascularRadialPosition"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("Right",<%= data != null && data.ContainsKey("GenericCardiovascularRadialPosition") && data["GenericCardiovascularRadialPosition"].Answer == "2" ? "true" : "false"%>)) +
                printview.col(2,
                    printview.span("") +
                    printview.span("Pedal: <%= data != null && data.ContainsKey("GenericCardiovascularPedal") ? data["GenericCardiovascularPedal"].Answer : ""%>")) +
                printview.col(3,
                    printview.checkbox("Bilateral",<%= data != null && data.ContainsKey("GenericCardiovascularPedal") && data["GenericCardiovascularPedal"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("Left",<%= data != null && data.ContainsKey("GenericCardiovascularPedal") && data["GenericCardiovascularPedal"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("Right",<%= data != null && data.ContainsKey("GenericCardiovascularPedal") && data["GenericCardiovascularPedal"].Answer == "2" ? "true" : "false"%>)) +
                printview.col(2,
                    printview.checkbox("Edema:",<%= genericCardioVascular != null && genericCardioVascular.Contains("5") ? "true" : "false"%>,true) +
                    printview.span("Location: <%= data != null && data.ContainsKey("GenericEdemaLocation") ? data["GenericEdemaLocation"].Answer : ""%>")) +
                printview.col(3,
                    printview.checkbox("Bilateral",<%= data != null && data.ContainsKey("GenericEdemaLocationPosition") && data["GenericEdemaLocationPosition"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("Left",<%= data != null && data.ContainsKey("GenericEdemaLocationPosition") && data["GenericEdemaLocationPosition"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("Right",<%= data != null && data.ContainsKey("GenericEdemaLocationPosition") && data["GenericEdemaLocationPosition"].Answer == "2" ? "true" : "false"%>)) +
                printview.col(2,
                    printview.span("") +
                    printview.checkbox("Non-Pitting: <%= data != null && data.ContainsKey("GenericEdemaNonPitting") ? data["GenericEdemaNonPitting"].Answer : ""%>",<%= genericPittingEdemaType != null && genericPittingEdemaType.Contains("1") ? "true" : "false"%>)) +
                printview.col(3,
                    printview.checkbox("Bilateral",<%= data != null && data.ContainsKey("GenericEdemaNonPittingPosition") && data["GenericEdemaNonPittingPosition"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("Left",<%= data != null && data.ContainsKey("GenericEdemaNonPittingPosition") && data["GenericEdemaNonPittingPosition"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("Right",<%= data != null && data.ContainsKey("GenericEdemaNonPittingPosition") && data["GenericEdemaNonPittingPosition"].Answer == "2" ? "true" : "false"%>)) +
                printview.col(2,
                    printview.span("") +
                    printview.checkbox("Pitting: <%= data != null && data.ContainsKey("GenericEdemaPitting") ? data["GenericEdemaPitting"].Answer : ""%>",<%= genericPittingEdemaType != null && genericPittingEdemaType.Contains("2") ? "true" : "false"%>))) +
            printview.span("Comments:",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericCardiovascularComment") ? data["GenericCardiovascularComment"].Answer : ""%>") +
            "</td><td><h3>Nuerological</h3>" +
            printview.col(2,
                printview.col(2,
                    printview.checkbox("LOC",<%= genericNeurologicalStatus != null && genericNeurologicalStatus.Contains("1") ? "true" : "false"%>,true) +
                    printview.span("<%= data.ContainsKey("GenericNeurologicalLOC") ? data["GenericNeurologicalLOC"].Answer : "" %>") ) +
                printview.col(3,
                    printview.checkbox("Person",<%= genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("1")  ? "true" : "false"%>) +
                    printview.checkbox("Place",<%= genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("2")  ? "true" : "false"%>) +
                    printview.checkbox("Time",<%= genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("3")  ? "true" : "false"%>) ) +
                printview.col(2,
                    printview.checkbox("Pupils",<%= genericNeurologicalStatus != null && genericNeurologicalStatus.Contains("2") ? "true" : "false"%>,true) +
                    printview.span("<%= data.ContainsKey("GenericNeurologicalPupils") ? data["GenericNeurologicalPupils"].Answer : ""%>") ) +
                printview.col(3,
                    printview.checkbox("Bilateral",<%= data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("Left",<%= data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("Right",<%= data.ContainsKey("GenericNeurologicalPupilsPosition") && data["GenericNeurologicalPupilsPosition"].Answer == "2" ? "true" : "false"%>) ) ) +
            printview.col(4,
                printview.checkbox("Vision",<%= genericNeurologicalStatus != null && genericNeurologicalStatus.Contains("3") ? "true" : "false"%>,true) +
                printview.checkbox("WNL",<%= genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Blurred Vision",<%= genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Cataracts",<%= genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("3") ? "true" : "false"%>) +
                printview.span("") +
                printview.checkbox("Corr. Lenses",<%= genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Glaucoma",<%= genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Legally Blind",<%= genericNeurologicalVisionStatus != null && genericNeurologicalVisionStatus.Contains("6") ? "true" : "false"%>)) +
            printview.col(4,
                printview.checkbox("Speech <%= data.ContainsKey("GenericNeurologicalSpeech") ? data["GenericNeurologicalSpeech"].Answer : ""%>",<%= genericNeurologicalStatus != null && genericNeurologicalStatus.Contains("4") ? "true" : "false"%>,true) +
                printview.checkbox("Paralysis <%= data.ContainsKey("GenericNeurologicalParalysisLocation") ? data["GenericNeurologicalParalysisLocation"].Answer : ""%>",<%= genericNeurologicalStatus != null && genericNeurologicalStatus.Contains("5") ? "true" : "false"%>,true) +
                printview.checkbox("Quadripiegia",<%= genericNeurologicalStatus != null && genericNeurologicalStatus.Contains("6") ? "true" : "false"%>,true) +
                printview.checkbox("Parapiegia",<%= genericNeurologicalStatus != null && genericNeurologicalStatus.Contains("7") ? "true" : "false"%>,true) +
                printview.checkbox("Seizures",<%= genericNeurologicalStatus != null && genericNeurologicalStatus.Contains("8") ? "true" : "false"%>,true) +
                printview.checkbox("Tremors <%= data.ContainsKey("GenericNeurologicalTremorsLocation") ? data["GenericNeurologicalTremorsLocation"].Answer : ""%>",<%= genericNeurologicalStatus != null && genericNeurologicalStatus.Contains("9") ? "true" : "false"%>,true) +
                printview.checkbox("Dizziness",<%= genericNeurologicalStatus != null && genericNeurologicalStatus.Contains("10") ? "true" : "false"%>,true) +
                printview.checkbox("Headache",<%= genericNeurologicalStatus != null && genericNeurologicalStatus.Contains("11") ? "true" : "false"%>,true) +
                printview.span("HOH:",true) +
                printview.checkbox("Bilateral",<%= data.ContainsKey("NeurologicalHOHPosition") && data["NeurologicalHOHPosition"].Answer == "0" ? "true" : "false" %>) +
                printview.checkbox("Left",<%= data.ContainsKey("NeurologicalHOHPosition") && data["NeurologicalHOHPosition"].Answer == "1" ? "true" : "false" %>) +
                printview.checkbox("Right",<%= data.ContainsKey("NeurologicalHOHPosition") && data["NeurologicalHOHPosition"].Answer == "2" ? "true" : "false" %>) ) +
            printview.span("Comments",true) +
            printview.span("<%= data.ContainsKey("GenericNeurologicalComment") ? data["GenericNeurologicalComment"].Answer : "" %>") +
            "</td></tr></table>");
        printview.addsection(
            "<table class='fixed'><tr><td><h3>Musculoskeletal</h3>" + 
            printview.checkbox("WNL (Within Normal Limits)",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("1") ? "true" : "false"%>,true) +
            printview.col(2,
                printview.checkbox("Grip Strength: <%= data.ContainsKey("GenericMusculoskeletalHandGrips") ? data["GenericMusculoskeletalHandGrips"].Answer : ""%>",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("2") ? "true" : "false"%>,true) +
                printview.col(3,
                    printview.checkbox("Bilateral",<%= data.ContainsKey("GenericMusculoskeletalHandGripsPosition") && data["GenericMusculoskeletalHandGripsPosition"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("Left",<%= data.ContainsKey("GenericMusculoskeletalHandGripsPosition") && data["GenericMusculoskeletalHandGripsPosition"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("Right",<%= data.ContainsKey("GenericMusculoskeletalHandGripsPosition") && data["GenericMusculoskeletalHandGripsPosition"].Answer == "2" ? "true" : "false"%>) ) ) +
            printview.col(3,
                printview.checkbox("Impared Motor Skill: <%= data.ContainsKey("GenericMusculoskeletalImpairedMotorSkills") ? data["GenericMusculoskeletalImpairedMotorSkills"].Answer : ""%>",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("3") ? "true" : "false"%>,true) +
                printview.checkbox("Limited ROM: <%= data.ContainsKey("GenericLimitedROMLocation") ? data["GenericLimitedROMLocation"].Answer : ""%>",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("4") ? "true" : "false"%>,true) +
                printview.checkbox("Mobility: <%= data.ContainsKey("GenericMusculoskeletalMobility") ? data["GenericMusculoskeletalMobility"].Answer : ""%>",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("5") ? "true" : "false"%>,true)) +
            printview.col(2,
                printview.checkbox("Type Assistive Device:",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("6") ? "true" : "false"%>,true) +
                printview.col(2,
                    printview.checkbox("Cane",<%= genericAssistiveDevice != null && genericAssistiveDevice.Contains("1") ? "true" : "false"%>) +
                    printview.checkbox("Crutches",<%= genericAssistiveDevice != null && genericAssistiveDevice.Contains("2") ? "true" : "false"%>)) +
                printview.col(2,
                    printview.span("") +                    
                    printview.checkbox("Walker",<%= genericAssistiveDevice != null && genericAssistiveDevice.Contains("3") ? "true" : "false"%>) +
                printview.col(2,
                    printview.checkbox("Wheelchair",<%= genericAssistiveDevice != null && genericAssistiveDevice.Contains("4") ? "true" : "false"%>)) +
                    printview.checkbox("Other: <%= data.ContainsKey("GenericAssistiveDeviceOther") ? data["GenericAssistiveDeviceOther"].Answer : ""%>",<%= genericAssistiveDevice != null && genericAssistiveDevice.Contains("5") ? "true" : "false"%>))) +
            printview.col(3,
                printview.checkbox("Contracture: <%= data.ContainsKey("GenericContractureLocation") ? data["GenericContractureLocation"].Answer : ""%>",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("7") ? "true" : "false"%>,true) +
                printview.checkbox("Weakness",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("8") ? "true" : "false"%>,true) +
                printview.checkbox("Joint Pain: <%= data.ContainsKey("GenericJointPainLocation") ? data["GenericJointPainLocation"].Answer : ""%>",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("9") ? "true" : "false"%>,true) +
                printview.checkbox("Poor Balance",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("10") ? "true" : "false"%>,true) +
                printview.checkbox("Joint Stiffness",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("11") ? "true" : "false"%>,true) +
                printview.checkbox("Amputation: <%= data.ContainsKey("GenericAmputationLocation") ? data["GenericAmputationLocation"].Answer : ""%>",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("12") ? "true" : "false"%>,true)) +
            printview.col(2,
                printview.checkbox("Weight Bearing Restrictions:",<%= genericMusculoskeletal != null && genericMusculoskeletal.Contains("13") ? "true" : "false"%>,true) +
                printview.col(3,
                    printview.span("<%= data.ContainsKey("GenericWeightBearingRestrictionLocation") ? data["GenericWeightBearingRestrictionLocation"].Answer : ""%>") +
                    printview.checkbox("Full",<%= data.ContainsKey("GenericWeightBearingRestriction") && data["GenericWeightBearingRestriction"].Answer == "0" ? "true" : "false"%>) +
                    printview.checkbox("Partial",<%= data.ContainsKey("GenericWeightBearingRestriction") && data["GenericWeightBearingRestriction"].Answer == "1" ? "true" : "false"%>))) +
            printview.span("Comments:",true) +
            printview.span("<%= data.ContainsKey("GenericMusculoskeletalComment") ? data["GenericMusculoskeletalComment"].Answer : ""%>") +
            "</td><td><h3>Gastrointestinal</h3>" +
            printview.col(2,
                printview.checkbox("WNL (Within Normal Limits)",<%= genericDigestive != null && genericDigestive.Contains("1") ? "true" : "false"%>,true) +
                printview.checkbox("Bowel Sounds: <%= data.ContainsKey("GenericDigestiveBowelSoundsType") ? data["GenericDigestiveBowelSoundsType"].Answer : ""%>",<%= genericDigestive != null && genericDigestive.Contains("2") ? "true" : "false"%>,true) +
                printview.checkbox("Abdominal Palpation: <%= data.ContainsKey("GenericAbdominalPalpation") ? data["GenericAbdominalPalpation"].Answer : ""%>",<%= genericDigestive != null && genericDigestive.Contains("3") ? "true" : "false"%>,true) +
                printview.checkbox("Bowel Incontinence",<%= genericDigestive != null && genericDigestive.Contains("4") ? "true" : "false"%>,true) +
                printview.checkbox("Nausea",<%= genericDigestive != null && genericDigestive.Contains("5") ? "true" : "false"%>,true) +
                printview.checkbox("Vomiting",<%= genericDigestive != null && genericDigestive.Contains("6") ? "true" : "false"%>,true) +
                printview.checkbox("GERD",<%= genericDigestive != null && genericDigestive.Contains("7") ? "true" : "false"%>,true) +
                printview.checkbox("Abd Girth: <%= data.ContainsKey("GenericDigestiveAbdGirthLength") ? data["GenericDigestiveAbdGirthLength"].Answer : ""%>",<%= genericDigestive != null && genericDigestive.Contains("8") ? "true" : "false"%>,true)) +
            printview.span("Elimination:",true) +
            printview.col(4,
                printview.checkbox("Last BM:",<%= genericDigestive != null && genericDigestive.Contains("11") ? "true" : "false"%>,true) +
                printview.span("<%= data.ContainsKey("GenericDigestiveLastBMDate") ? data["GenericDigestiveLastBMDate"].Answer : ""%>") +
                printview.checkbox("WNL:",<%= genericDigestiveLastBM != null && genericDigestiveLastBM.Contains("1") ? "true" : "false"%>,true) ) +
            printview.col(2,
                printview.checkbox("Abnormal Stool:",<%= genericDigestiveLastBM != null && genericDigestiveLastBM.Contains("2") ? "true" : "false"%>,true) +
                printview.col(2,
                    printview.checkbox("Gray",<%= genericDigestiveLastBMAbnormalStool != null && genericDigestiveLastBMAbnormalStool.Contains("1") ? "true" : "false"%>) +
                    printview.checkbox("Tarry",<%= genericDigestiveLastBMAbnormalStool != null && genericDigestiveLastBMAbnormalStool.Contains("2") ? "true" : "false"%>) +
                    printview.checkbox("Black",<%= genericDigestiveLastBMAbnormalStool != null && genericDigestiveLastBMAbnormalStool.Contains("3") ? "true" : "false"%>) +
                    printview.checkbox("Fresh Blood",<%= genericDigestiveLastBMAbnormalStool != null && genericDigestiveLastBMAbnormalStool.Contains("4") ? "true" : "false"%>) ) ) +
            printview.col(4,
                printview.checkbox("Constipation:",<%= genericDigestiveLastBM != null && genericDigestiveLastBM.Contains("3") ? "true" : "false"%>,true) +
                printview.checkbox("Chronic",<%= data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Chronic" ? "true" : "false"%>) +
                printview.checkbox("Acute",<%= data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Acute" ? "true" : "false"%>) +
                printview.checkbox("Occasional",<%= data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Occasional" ? "true" : "false"%>) +
                printview.checkbox("Diarrhea:",<%= genericDigestiveLastBM != null && genericDigestiveLastBM.Contains("4") ? "true" : "false"%>,true) +
                printview.checkbox("Chronic",<%= data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Chronic" ? "true" : "false"%>) +
                printview.checkbox("Acute",<%= data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Acute" ? "true" : "false"%>) +
                printview.checkbox("Occasional",<%= data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Occasional" ? "true" : "false"%>) ) +
            printview.span("Ostomy:",true) +
            printview.col(3,
                printview.checkbox("Ostomy Type:",<%= genericDigestiveOstomy != null && genericDigestiveOstomy.Contains("1") ? "true" : "false"%>,true) +
                printview.checkbox("Stoma Appearance:",<%= genericDigestiveOstomy != null && genericDigestiveOstomy.Contains("2") ? "true" : "false"%>,true) +
                printview.checkbox("Surrounding Skin:",<%= genericDigestiveOstomy != null && genericDigestiveOstomy.Contains("3") ? "true" : "false"%>,true) +
                printview.span("<%= data.ContainsKey("GenericDigestiveOstomyType") ? data["GenericDigestiveOstomyType"].Answer : ""%>") +
                printview.span("<%= data.ContainsKey("GenericDigestiveStomaAppearance") ? data["GenericDigestiveStomaAppearance"].Answer : ""%>") +
                printview.span("<%= data.ContainsKey("GenericDigestiveSurSkinType") ? data["GenericDigestiveSurSkinType"].Answer : ""%>")) +
            printview.span("Comments:",true) +
            printview.span("<%= data.ContainsKey("GenericGastrointestinalComment") ? data["GenericGastrointestinalComment"].Answer : ""%>") +
            "</td></tr></table>");
        printview.addsection(
            "<table class='fixed'><tr><td><h3>Nutrition</h3>" + 
            printview.col(2,
                printview.checkbox("WNL (Within Normal Limits)",<%= genericNutrition != null && genericNutrition.Contains("1") ? "true" : "false"%>,true) +
                printview.checkbox("Dysphagia",<%= genericNutrition != null && genericNutrition.Contains("2") ? "true" : "false"%>,true) +
                printview.checkbox("Decreased Appetite",<%= genericNutrition != null && genericNutrition.Contains("3") ? "true" : "false"%>,true) ) +
            printview.col(3,
                printview.checkbox("Weight:",<%= genericNutrition != null && genericNutrition.Contains("4") ? "true" : "false"%>,true) +
                printview.checkbox("Loss",<%= data.ContainsKey("GenericNutritionWeightGainLoss") && data["GenericNutritionWeightGainLoss"].Answer == "Loss" ? "true" : "false"%>) +
                printview.checkbox("Gain",<%= data.ContainsKey("GenericNutritionWeightGainLoss") && data["GenericNutritionWeightGainLoss"].Answer == "Gain" ? "true" : "false"%>) +
                printview.checkbox("Diet:",<%= genericNutrition != null && genericNutrition.Contains("5") ? "true" : "false"%>,true) +
                printview.checkbox("Adequate",<%= data.ContainsKey("GenericNutritionDietAdequate") && data["GenericNutritionDietAdequate"].Answer == "Adequate" ? "true" : "false"%>) +
                printview.checkbox("Inadequate",<%= data.ContainsKey("GenericNutritionDietAdequate") && data["GenericNutritionDietAdequate"].Answer == "Inadequate" ? "true" : "false"%>) ) +
            printview.col(2,
                printview.checkbox("Enteral Feeding:",<%= genericNutrition != null && genericNutrition.Contains("6") ? "true" : "false"%>,true) +
                printview.col(3,
                    printview.checkbox("NG",<%= genericNutritionEnteralFeeding != null && genericNutritionEnteralFeeding.Contains("1") ? "true" : "false"%>) +
                    printview.checkbox("PEG",<%= genericNutritionEnteralFeeding != null && genericNutritionEnteralFeeding.Contains("2") ? "true" : "false"%>) +
                    printview.checkbox("Dobhof",<%= genericNutritionEnteralFeeding != null && genericNutritionEnteralFeeding.Contains("3") ? "true" : "false"%>) ) ) +
            printview.checkbox("Tube Placement Checked",<%= genericNutrition != null && genericNutrition.Contains("7") ? "true" : "false"%>,true) +
            printview.col(2,
                printview.checkbox("Residual Checked:",<%= genericNutrition != null && genericNutrition.Contains("8") ? "true" : "false"%>,true) +
                printview.span("Amount: <%= data.ContainsKey("GenericNutritionResidualCheckedAmount") ? data["GenericNutritionResidualCheckedAmount"].Answer + "cc" : "" %>") ) +
            printview.span("Comments:",true) +
            printview.span("<%= data.ContainsKey("GenericNutritionComment") ? data["GenericNutritionComment"].Answer : ""%>") +
            "</td><td><h3>Genitournary</h3>" +
            printview.col(2,
                printview.checkbox("WNL (Within Normal Limits)",<%= genericGU != null && genericGU.Contains("1") ? "true" : "false" %>,true) +
                printview.checkbox("Incontinence",<%= genericGU != null && genericGU.Contains("2") ? "true" : "false"%>,true) +
                printview.checkbox("Bladder Distention",<%= genericGU != null && genericGU.Contains("3") ? "true" : "false"%>,true) +
                printview.checkbox("Discharge",<%= genericGU != null && genericGU.Contains("4") ? "true" : "false"%>,true) +
                printview.checkbox("Frequency",<%= genericGU != null && genericGU.Contains("5") ? "true" : "false"%>,true) +
                printview.checkbox("Dysuria",<%= genericGU != null && genericGU.Contains("6") ? "true" : "false"%>,true) +
                printview.checkbox("Retention",<%= genericGU != null && genericGU.Contains("7") ? "true" : "false"%>,true) +
                printview.checkbox("Urgency",<%= genericGU != null && genericGU.Contains("8") ? "true" : "false"%>,true) +
                printview.checkbox("Oliguria",<%= genericGU != null && genericGU.Contains("9") ? "true" : "false"%>,true) ) +
            printview.col(3,
                printview.checkbox("Catheter/Device:",<%= genericGU != null && genericGU.Contains("10") ? "true" : "false"%>,true) +
                printview.span("<%= data.ContainsKey("GenericGUCatheterList") ? data["GenericGUCatheterList"].Answer : ""%>") +
                printview.span("Last changed: <%= data.ContainsKey("GenericGUCatheterLastChanged") ? data["GenericGUCatheterLastChanged"].Answer : ""%><%= data.ContainsKey("GenericGUCatheterFrequency") ? " " + data["GenericGUCatheterFrequency"].Answer + "Fr" : ""%><%= data.ContainsKey("GenericGUCatheterAmount") ? data["GenericGUCatheterAmount"].Answer + "cc" : ""%>") ) +
            printview.col(4,
                printview.checkbox("Urine:",<%= genericGU != null && genericGU.Contains("11") ? "true" : "false"%>,true) +
                printview.checkbox("Cloudy",<%= genericGUUrine != null && genericGUUrine.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Odorous",<%= genericGUUrine != null && genericGUUrine.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Sediment",<%= genericGUUrine != null && genericGUUrine.Contains("3") ? "true" : "false"%>) +
                printview.span("") +
                printview.checkbox("Hematuria",<%= genericGUUrine!=null && genericGUUrine.Contains("4") ? "true" : "false" %>) +
                printview.checkbox("Other",<%= genericGUUrine != null && genericGUUrine.Contains("5") ? "true" : "false"%>) +
                printview.span("<%= data.ContainsKey("GenericGUOtherText") ? data["GenericGUOtherText"].Answer : ""%>") ) +
            printview.span("Comments:",true) +
            printview.span("<%= data.ContainsKey("GenericGenitourinaryComment") ? data["GenericGenitourinaryComment"].Answer : ""%>") +
            "</td></tr></table>");
        printview.addsection(
            "<table class='fixed'><tr><td><h3>Diabetic Care</h3>" + 
            printview.col(4,
                printview.span("BG:",true) +
                printview.span("<%= data.ContainsKey("GenericBloodSugarLevelText") ? data["GenericBloodSugarLevelText"].Answer + " mg/dl " : ""%><%= data.ContainsKey("GenericBloodSugarLevel") ? data["GenericBloodSugarLevel"].Answer : ""%>") +
                printview.span("Performed by:",true) +
                printview.span("<%= data.ContainsKey("GenericBloodSugarCheckedBy") ? data["GenericBloodSugarCheckedBy"].Answer : ""%>") ) +
            printview.span("Diabetic Management:",true) +
            printview.col(2,
                printview.checkbox("Diet",<%= diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Oral Hypoglycemic",<%= diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Exercise",<%= diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Insulin",<%= diabeticCareDiabeticManagement != null && diabeticCareDiabeticManagement.Contains("4") ? "true" : "false"%>) ) +
            printview.span("Insulin Administered by:",true) +
            printview.col(4,
                printview.checkbox("N/A",<%= diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Patient",<%= diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Caregiver",<%= diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("SN",<%= diabeticCareInsulinAdministeredby != null && diabeticCareInsulinAdministeredby.Contains("4") ? "true" : "false"%>) ) +
            printview.span("S&amp;S of Hyperglycemia:",true) +
            printview.col(3,
                printview.checkbox("Fatigue",<%= genericHyperglycemia != null && genericHyperglycemia.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Blurred Vision",<%= genericHyperglycemia != null && genericHyperglycemia.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Polydipsia",<%= genericHyperglycemia != null && genericHyperglycemia.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Polyuria",<%= genericHyperglycemia != null && genericHyperglycemia.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Polyphagia",<%= genericHyperglycemia != null && genericHyperglycemia.Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Other <%= data.ContainsKey("GenericDiabeticCareHyperglycemiaOther") ? data["GenericDiabeticCareHyperglycemiaOther"].Answer : ""%>",<%= genericHyperglycemia != null && genericHyperglycemia.Contains("6") ? "true" : "false"%>) ) +
            printview.span("S&amp;S of Hypoglycemia:",true) +
            printview.col(3,
                printview.checkbox("Anxious",<%= genericHypoglycemia != null && genericHypoglycemia.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Dizziness",<%= genericHypoglycemia != null && genericHypoglycemia.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Fatigue",<%= genericHypoglycemia != null && genericHypoglycemia.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Perspiration",<%= genericHypoglycemia != null && genericHypoglycemia.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Weakness",<%= genericHypoglycemia != null && genericHypoglycemia.Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Other <%= data.ContainsKey("GenericDiabeticCareHypoglycemiaOther") ? data["GenericDiabeticCareHypoglycemiaOther"].Answer : ""%>",<%= genericHypoglycemia != null && genericHypoglycemia.Contains("6") ? "true" : "false"%>) ) +
            printview.span("Comments:",true) +
            printview.span("<%= data.ContainsKey("GenericDiabeticCareComment") ? data["GenericDiabeticCareComment"].Answer : ""%>") +
            "</td><td><h3>IV</h3>" +
            printview.col(4,
                printview.span("IV Access",true) +
                printview.span("<%= data.ContainsKey("GenericIVAccess") ? data["GenericIVAccess"].Answer : ""%>") +
                printview.span("IV Location",true) +
                printview.span("<%= data.ContainsKey("GenericIVLocation") ? data["GenericIVLocation"].Answer : ""%>") ) +
            printview.col(2,
                printview.span("Condition of IV Site",true) +
                printview.span("<%= data.ContainsKey("GenericIVConditionOfIVSite") ? data["GenericIVConditionOfIVSite"].Answer : ""%>") +
                printview.span("Condition of Dressing",true) +
                printview.span("<%= data.ContainsKey("GenericIVConditionOfDressing") ? data["GenericIVConditionOfDressing"].Answer : ""%>") ) +
            printview.span("IV site dressing changed performed on this visit:",true) +
            printview.span("<%= data.ContainsKey("GenericIVSiteDressing") ? data["GenericIVSiteDressing"].Answer : ""%>") +
            printview.col(2,
                printview.col(3,
                    printview.span("Flush:",true) +
                    printview.checkbox("Yes",<%= data.ContainsKey("GenericFlush") && data["GenericFlush"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data.ContainsKey("GenericFlush") && data["GenericFlush"].Answer == "0" ? "true" : "false"%>) ) +
                printview.span("flushed with <%= data.ContainsKey("GenericIVAccessFlushed") ? data["GenericIVAccessFlushed"].Answer + "/ml" : "<span class='blank'></span>"%><%= data.ContainsKey("GenericIVSiteDressingUnit") ? " of " + data["GenericIVSiteDressingUnit"].Answer : ""%>") ) +
            printview.span("Comments:",true) +
            printview.span("<%= data.ContainsKey("GenericIVComment") ? data["GenericIVComment"].Answer : ""%>") +
            "<h3>Infection Control</h3>" + 
            printview.col(2,
                printview.checkbox("Universal Precautions Observed",<%= infectionControl != null && infectionControl.Contains("1") ? "true" : "false"%>,true) +
                printview.checkbox("Sharps/Waste Disposal",<%= infectionControl != null && infectionControl.Contains("2") ? "true" : "false"%>,true) +
                printview.span("New Infection",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data.ContainsKey("GenericIsNewInfection") && data["GenericIsNewInfection"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data.ContainsKey("GenericIsNewInfection") && data["GenericIsNewInfection"].Answer == "0" ? "true" : "false"%>) ) ) +
            printview.span("Comments:",true) +
            printview.span("<%= data.ContainsKey("GenericInfectionControlComment") ? data["GenericInfectionControlComment"].Answer : ""%>") +
            "</td></tr></table>");
        printview.addsection(
            "<table class='fixed'><tr><td><h3>Care Plan &amp; Discharge Planning</h3>" +
            printview.col(2,
                printview.span("Care Plan:",true) +
                printview.span("<%= data.ContainsKey("GenericCarePlan") ? data["GenericCarePlan"].Answer : ""%>") ) +
            printview.col(2,
                printview.span("Progress toward goals:",true) +
                printview.span("<%= data.ContainsKey("GenericCarePlanProgressTowardsGoals") ? data["GenericCarePlanProgressTowardsGoals"].Answer : ""%>") ) +
            printview.span("New/Changed Orders or Updates to Care Plan:",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data.ContainsKey("GenericCarePlanIsChanged") && data["GenericCarePlanIsChanged"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data.ContainsKey("GenericCarePlanIsChanged") && data["GenericCarePlanIsChanged"].Answer == "0" ? "true" : "false"%>) ) +
            printview.span("Comments:",true) +
            printview.span("<%= data.ContainsKey("GenericCarePlanComment") ? data["GenericCarePlanComment"].Answer : ""%>") +
            printview.col(2,
                printview.span("Discharge Plan Discussed:",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data.ContainsKey("GenericDischargePlanningDiscussed") && data["GenericDischargePlanningDiscussed"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data.ContainsKey("GenericDischargePlanningDiscussed") && data["GenericDischargePlanningDiscussed"].Answer == "0" ? "true" : "false"%>) ) ) +
            printview.span("Discharge Planning Discussed with:",true) +
            printview.span("<%= data.ContainsKey("GenericDischargePlanningDiscussed") ? data["GenericDischargePlanningDiscussed"].Answer : ""%>") +
            printview.col(2,
                printview.span("Reason for Discharge:",true) +
                printview.span("<%= data.ContainsKey("GenericDischargeReasonForDischarge") ? data["GenericDischargeReasonForDischarge"].Answer : ""%>") ) +
            printview.col(2,
                printview.span("Physician Notified of Discharge:",true) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data.ContainsKey("GenericDischargePhysicianNotifiedOfDischarge") && data["GenericDischargePhysicianNotifiedOfDischarge"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data.ContainsKey("GenericDischargePhysicianNotifiedOfDischarge") && data["GenericDischargePhysicianNotifiedOfDischarge"].Answer == "0" ? "true" : "false"%>) ) ) +
            printview.checkbox("Patient received discharge notice per agency policy",<%= pateintReceivedDischarge != null && pateintReceivedDischarge.Contains("1") ? "true" : "false"%>) +
            printview.span("Comments:",true) +
            printview.span("<%= data.ContainsKey("GenericDischargeComment") ? data["GenericDischargeComment"].Answer : ""%>") +
            "</td><td><h3>Interventions</h3>" +
            printview.col(2,
                printview.checkbox("Skilled Observation/Assessment",<%= interventions != null && interventions.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Patient/CG teaching",<%= interventions != null && interventions.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("IM Injection/SQ Injection",<%= interventions != null && interventions.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Instructed on Safety Precautions",<%= interventions != null && interventions.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Instructed on Emergency Preparedness",<%= interventions != null && interventions.Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Peg/GT Tube Site care",<%= interventions != null && interventions.Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Medication Adminstration",<%= interventions != null && interventions.Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Prep./Admin. Insulin",<%= interventions != null && interventions.Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Foot care performed",<%= interventions != null && interventions.Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Diabetic Monitoring/Care",<%= interventions != null && interventions.Contains("10") ? "true" : "false"%>) +
                printview.checkbox("Administer Enteral nutrition",<%= interventions != null && interventions.Contains("11") ? "true" : "false"%>) +
                printview.checkbox("Wound Care/Dressing Change",<%= interventions != null && interventions.Contains("12") ? "true" : "false"%>) +
                printview.checkbox("Foley Change",<%= interventions != null && interventions.Contains("13") ? "true" : "false"%>) +
                printview.checkbox("Glucometer calibration",<%= interventions != null && interventions.Contains("14") ? "true" : "false"%>) +
                printview.checkbox("IV Site Change",<%= interventions != null && interventions.Contains("15") ? "true" : "false"%>) +
                printview.checkbox("IV Tubing Change ",<%= interventions != null && interventions.Contains("16") ? "true" : "false"%>) +
                printview.checkbox("Trachea care",<%= interventions != null && interventions.Contains("17") ? "true" : "false"%>) +
                printview.checkbox("Foley Irrigation ",<%= interventions != null && interventions.Contains("18") ? "true" : "false"%>) +
                printview.checkbox("IV Site Dressing Change ",<%= interventions != null && interventions.Contains("19") ? "true" : "false"%>) +
                printview.checkbox("Diet Teaching <%= data.ContainsKey("GenericDietTeaching") ? data["GenericDietTeaching"].Answer : ""%>",<%= interventions != null && interventions.Contains("20") ? "true" : "false"%>) +
                printview.checkbox("Venipuncture/Lab: <%= data.ContainsKey("GenericVenipuncture") ? data["GenericVenipuncture"].Answer : ""%>",<%= interventions != null && interventions.Contains("21") ? "true" : "false"%>) +
                printview.checkbox("Instructed on Medication <%= data.ContainsKey("GenericInstructedOnMedication") ? data["GenericInstructedOnMedication"].Answer : ""%>",<%= interventions != null && interventions.Contains("22") ? "true" : "false"%>) ) +
            printview.checkbox("Instructed on Disease Process to inlude: <%= data.ContainsKey("GenericInstructedOnDiseaseProcess") ? data["GenericInstructedOnDiseaseProcess"].Answer : ""%>",<%= interventions != null && interventions.Contains("23") ? "true" : "false"%>) +
            printview.span("Comments:",true) +
            printview.span("<%= data.ContainsKey("GenericInterventionsComment") ? data["GenericInterventionsComment"].Answer : ""%>") +
            "</td></tr></table>");
        printview.addsection(
            "<table class='fixed'><tr><td><h3>Care Coordination</h3>" +
            printview.span("Care Coordination with:",true) +
            printview.col(6,
                printview.checkbox("SN",<%= genericCareCoordination != null && genericCareCoordination.Contains("1") ? "true" : "false"%>,true) +
                printview.checkbox("PT",<%= genericCareCoordination != null && genericCareCoordination.Contains("2") ? "true" : "false"%>,true) +
                printview.checkbox("ST",<%= genericCareCoordination != null && genericCareCoordination.Contains("3") ? "true" : "false"%>,true) +
                printview.checkbox("MSW",<%= genericCareCoordination != null && genericCareCoordination.Contains("4") ? "true" : "false"%>,true) +
                printview.checkbox("HHA",<%= genericCareCoordination != null && genericCareCoordination.Contains("5") ? "true" : "false"%>,true) +
                printview.checkbox("MD",<%= genericCareCoordination != null && genericCareCoordination.Contains("6") ? "true" : "false"%>,true) ) +
            printview.checkbox("<strong>Other</strong> <%= data.ContainsKey("GenericCareCoordinationOther") ? data["GenericCareCoordinationOther"].Answer : ""%>",<%= genericCareCoordination != null && genericCareCoordination.Contains("7") ? "true" : "false"%>) +
            printview.span("Regarding:",true) +
            printview.span("<%= data.ContainsKey("GenericCareCoordinationRegarding") ? data["GenericCareCoordinationRegarding"].Answer : ""%>") +
            "</td><td><h3>Response to Teaching/Interventions</h3>" +
            printview.checkbox("Verbalize <%= data.ContainsKey("GenericResponseToTeachingInterventionsUVI") ? data["GenericResponseToTeachingInterventionsUVI"].Answer : "<span class='blank'></span>"%> understanding of verbal instructions/teaching given.",<%= responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Able to return <%= data.ContainsKey("GenericResponseToTeachingInterventionsCDP") ? data["GenericResponseToTeachingInterventionsCDP"].Answer : "<span class='blank'></span>"%> correct demonstration of procedure/technique instructed on.",<%= responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Treatment/Procedure well tolerated by patient.",<%= responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Infusion well tolerated by patient.",<%= responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Other <%= data.ContainsKey("GenericResponseToTeachingInterventionsOther") ? data["GenericResponseToTeachingInterventionsOther"].Answer : ""%>",<%= responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("5") ? "true" : "false"%>) +
            "</td></tr></table>");
        printview.addsection(
            "<table class='fixed'><tr><td><h3>Narrative &amp; Teaching</h3>" + 
            printview.span("<%= data.ContainsKey("GenericNarrativeComment") ? data["GenericNarrativeComment"].Answer : ""%>") +
            "</td></tr></table>");
        printview.addsection(
            "<table class='fixed'><tr><td><h3>Response to Teaching/Interventions</h3>" + 
            printview.col(2,
                printview.checkbox("Verbalize <%= data.ContainsKey("GenericResponseToTeachingInterventionsUVI") ? data["GenericResponseToTeachingInterventionsUVI"].Answer : "<span class='blank'></span>"%> understanding of verbal instructions/ teaching given.",<%= responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Able to return <%= data.ContainsKey("GenericResponseToTeachingInterventionsCDP") ? data["GenericResponseToTeachingInterventionsCDP"].Answer : "<span class='blank'></span>"%> correct demonstration of procedure/ technique instructed on.",<%= responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Treatment/Procedure well tolerated by patient.",<%= responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Infusion well tolerated by patient.",<%= responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Other: <%= data.ContainsKey("GenericResponseToTeachingInterventionsOther") ? data["GenericResponseToTeachingInterventionsOther"].Answer : ""%>",<%= responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("5") ? "true" : "false"%>)) +
            "</td></tr></table>");
        printview.addsection(
            printview.col(2,
                printview.span("Clinician Signature:",true) +
                printview.span("Date:",true) +
                printview.span("<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : ""%>") +
                printview.span("<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() ? Model.SignatureDate : ""%>") ) );
     <%
     }).Render(); %>
</html>