﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Coordination of Care/Transfer Summary<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body>
    <div class="page largerfont"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <div>
            <table class="fixed"><tbody>
                <tr>
                    <td colspan="2">
                        <%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "<br />" : "" %>
                        <%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : ""%> <%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : ""%><br />
                        <%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>
                    </td><th class="h1">Coordination of Care/<br />Transfer Summary</th>
                </tr><tr>
                    <td colspan="3">
                        <span class="big">Patient Name: <%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></span><br />
                        <span class="quadcol">
                            <span><strong>Completion Date:</strong></span>
                            <span class="trip"><%= data != null && data.ContainsKey("CompletedDate") && data["CompletedDate"].Answer.IsNotNullOrEmpty() ? data["CompletedDate"].Answer.ToDateTime().ToString("MM/dd/yyy") : "" %></span>
                            <span><strong>Episode Period:</strong></span>
                            <span class="trip"><%= data != null ? string.Format(" {0} &ndash; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) : ""%></span>
                            <span><strong>MR#</strong></span>
                            <span class="trip"><%= data != null ? Model.Patient.PatientIdNumber : ""%></span>
                            <span><strong>Physician:</strong></span>
                            <span class="trip"><%= data != null ? Model.PhysicianDisplayName.ToTitleCase() : ""%></span>
                            <span><strong>Primary Diagnosis:</strong></span>
                            <span class="trip"><%= data != null && data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : ""%></span>
                            <span><strong>Secondary Diagnosis:</strong></span>
                            <span class="trip"><%= data != null && data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : ""%></span>
                            <span><strong>Tertiary Diagnosis:</strong></span>
                            <span class="trip"><%= data != null && data.ContainsKey("PrimaryDiagnosis2") ? data["PrimaryDiagnosis2"].Answer : ""%></span>
                            <span><strong>Homebound Status:</strong></span>
                            <span class="trip"><%= data != null && data.ContainsKey("HomeboundStatus") ? data["HomeboundStatus"].Answer : ""%></span>
                        </span>
                    </td>
                </tr>
            </tbody></table>
        </div>
        <h3>Functional Limitations</h3>
        <div class="tricol"><% string[] functionLimitations = data != null && data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null; %>
            <span class="checklabel"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("1") ? "&#x2713;" : ""%></span>Amputation</span>
            <span class="checklabel"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("5") ? "&#x2713;" : ""%></span>Legally Blind</span>
            <span class="checklabel"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("9") ? "&#x2713;" : ""%></span>Endurance</span>
            <span class="checklabel"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("2") ? "&#x2713;" : ""%></span>Contracture</span>
            <span class="checklabel"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("6") ? "&#x2713;" : ""%></span>Hearing</span>
            <span class="checklabel"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("A") ? "&#x2713;" : ""%></span>Paralysis</span>
            <span class="checklabel"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("3") ? "&#x2713;" : ""%></span>Bowel/Bladder Incontinence</span>
            <span class="checklabel"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("7") ? "&#x2713;" : ""%></span>Dyspnea</span>
            <span class="checklabel"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("B") ? "&#x2713;" : ""%></span>Ambulation</span>
            <span class="checklabel"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("4") ? "&#x2713;" : ""%></span>Speech</span>
            <span class="checklabel"><span class="checkbox"><%= functionLimitations != null && functionLimitations.Contains("8") ? "&#x2713;" : ""%></span>Other (Specify): <span class="auto trip"><%= data != null && data.ContainsKey("FunctionLimitationsOther") ? data["FunctionLimitationsOther"].Answer : "" %></span></span>
        </div>
        <h3>Patient Condition</h3>
        <div class="pentcol"><% string[] patientCondition = data != null && data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("1") ? "&#x2713;" : ""%></span>Stable</span>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("2") ? "&#x2713;" : ""%></span>Improved</span>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("3") ? "&#x2713;" : ""%></span>Unchanged</span>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("4") ? "&#x2713;" : ""%></span>Unstable</span>
            <span class="checklabel"><span class="checkbox"><%= patientCondition != null && patientCondition.Contains("5") ? "&#x2713;" : ""%></span>Declined</span>
        </div>
        <h3>Service(s) Provided</h3>
        <div class="tricol"><% string[] serviceProvided = data != null && data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("1") ? "&#x2713;" : ""%></span>SN</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("2") ? "&#x2713;" : ""%></span>PT</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("3") ? "&#x2713;" : ""%></span>OT</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("4") ? "&#x2713;" : ""%></span>ST</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("5") ? "&#x2713;" : ""%></span>MSW</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("6") ? "&#x2713;" : ""%></span>HHA</span>
            <span class="checklabel"><span class="checkbox"><%= serviceProvided != null && serviceProvided.Contains("7") ? "&#x2713;" : ""%></span>Other <span class="auto trip"><%= data != null && data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : "" %></span></span>
        </div>
        <h3>Vital Sign Ranges</h3>
        <div>
            <table class="fixed"><tbody>
                <tr><th></th><th>BP</th><th>HR</th><th>Resp</th><th>Temp</th><th>Weight</th><th>BG</th></tr>
                <tr>
                    <td><strong>Lowest</strong></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignBPMin") ? data["VitalSignBPMin"].Answer : ""%></span></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignHRMin") ? data["VitalSignHRMin"].Answer : ""%></span></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignRespMin") ? data["VitalSignRespMin"].Answer : ""%></span></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignTempMin") ? data["VitalSignTempMin"].Answer : ""%></span></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignWeightMin") ? data["VitalSignWeightMin"].Answer : ""%></span></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignBGMin") ? data["VitalSignBGMin"].Answer : ""%></span></td>
                </tr><tr>
                    <td><strong>Highest</strong></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignBPMax") ? data["VitalSignBPMax"].Answer : ""%></span></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignHRMax") ? data["VitalSignHRMax"].Answer : ""%></span></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignRespMax") ? data["VitalSignRespMax"].Answer : ""%></span></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignTempMax") ? data["VitalSignTempMax"].Answer : ""%></span></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignWeightMax") ? data["VitalSignWeightMax"].Answer : ""%></span></td>
                    <td><span class="trip"><%= data != null && data.ContainsKey("VitalSignBGMax") ? data["VitalSignBGMax"].Answer : ""%></span></td>
                </tr>
            </tbody></table>
        </div>
        <h3>Summary of Care Provided by HHA</h3>
        <div><span class="deca overflow"><%= data != null && data.ContainsKey("SummaryOfCareProvided") && data["SummaryOfCareProvided"].Answer.IsNotNullOrEmpty() ? data["SummaryOfCareProvided"].Answer : ""%></span></div>
        <h3>Transfer Facility Information</h3>
        <div>
            <span class="hexcol">
                <span><strong>Facility:</strong></span>
                <span class="trip"><%= data != null && data.ContainsKey("Facility") ? data["Facility"].Answer : ""%></span>
                <span><strong>Phone:</strong></span>
                <span class="trip"><%= data != null && data.ContainsKey("Phone") ? data["Phone"].Answer : ""%></span>
                <span><strong>Contact:</strong></span>
                <span class="trip"><%= data != null && data.ContainsKey("Contact") ? data["Contact"].Answer : ""%></span>
            </span>
            <span><strong>Services Providing:</strong></span>
            <span class="deca overflow"><%= data != null && data.ContainsKey("ServicesProviding") && data["ServicesProviding"].Answer.IsNotNullOrEmpty() ? data["ServicesProviding"].Answer : ""%></span>
        </div><div class="bicol">
            <span class="trip"><strong>Clinician Signature:</strong><%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : ""%></span>
            <span class="trip"><strong>Date:</strong><%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() ? Model.SignatureDate : ""%></span>
        </div>
    </div>
</body>
</html>
