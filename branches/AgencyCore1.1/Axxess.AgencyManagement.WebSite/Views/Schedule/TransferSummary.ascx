﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "TransferSummaryForm" })) {
        var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Coordination of Care/Transfer Summary | ",
        Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "",
        "','transferSummary');</script>")%>
<%= Html.Hidden("TransferSummary_PatientId", Model.PatientId)%>
<%= Html.Hidden("TransferSummary_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("TransferSummary_EventId", Model.EventId)%>
<%= Html.Hidden("Type", "TransferSummary")%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="3">Coordination of Care/Transfer Summary</th></tr>
            <tr>
                <td colspan="3">
                    <div class="third">
                        <label for="TransferSummary_PreviousSummary" class="float_left">View Previous Summary:</label>
                        <div class="float_right"><select name="TransferSummary_PreviousSummary" id="TransferSummary_PreviousSummary"><option value=" " selected="true"></option><option value="00">0</option></select></div>
                    </div><div class="third">
                        <label for="TransferSummary_PatientName" class="float_left">Patient Name:</label>
                        <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Patients, "TransferSummary_PatientName", Model.Patient.Id.ToString(), new { @id = "TransferSummary_PatientName", @disabled = "disabled" })%></div>
                    </div><div class="third">
                        <label for="TransferSummary_MR" class="float_left">MR#:</label>
                        <div class="float_right"><%= Html.TextBox("TransferSummary_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty, new { @id = "TransferSummary_MR", @readonly = "readonly" })%></div>
                    </div><div class="third">
                        <label for="TransferSummary_VisitDate" class="float_left">Visit Date:</label>
                        <div class="float_right"><%= Html.Telerik().DatePicker().Name("TransferSummary_VisitDate").Value(Model.EventDate.FormatWith("MM/dd/yyy")).MaxDate(Model.EndDate).MinDate(Model.StartDate).HtmlAttributes(new { @id = "TransferSummary_VisitDate", @class = "date required" })%></div>
                    </div><div class="third">
                        <label for="TransferSummary_EpsPeriod" class="float_left">Episode/Period:</label>
                        <div class="float_right"><%= Html.TextBox("TransferSummary_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = "TransferSummary_EpsPeriod", @readonly = "readonly" })%></div>
                    </div><div class="third">
                        <label for="TransferSummary_VisitDate" class="float_left">Physician:</label>
                        <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Physicians, "TransferSummary_Physician", Model.PhysicianId.ToString(), new { @id = "TransferSummary_Physician" })%></div>
                    </div><div class="third">
                        <label for="HHACarePlan_PrimaryDiagnosis" class="float_left">Primary Diagnosis:</label>
                        <div class="float_right"><%= Html.TextBox("HHACarePlan_PrimaryDiagnosis", data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : string.Empty, new { @id = "HHACarePlan_PrimaryDiagnosis" })%></div>
                    </div><div class="third">
                        <label for="HHACarePlan_PrimaryDiagnosis1" class="float_left">Secondary Diagnosis:</label>
                        <div class="float_right"><%= Html.TextBox("HHACarePlan_PrimaryDiagnosis1", data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : string.Empty, new { @id = "HHACarePlan_PrimaryDiagnosis1" })%></div>
                    </div><div class="third">
                        <label for="TransferSummary_HomeboundStatus" class="float_left">Homebound Status:</label>
                        <div class="float_right"><% var homeboundStatus = new SelectList(new[] { new SelectListItem { Text = "", Value = "" }, new SelectListItem { Text = "N/A", Value = "N/A" }, new SelectListItem { Text = "Exhibits considerable & taxing effort to leave home", Value = "Exhibits considerable & taxing effort to leave home" }, new SelectListItem { Text = "Requires the assistance of another to get up and moving safely", Value = "Requires the assistance of another to get up and moving safely" }, new SelectListItem { Text = "Severe Dyspnea", Value = "Severe Dyspnea" }, new SelectListItem { Text = "Unable to safely leave home unassisted", Value = "Unable to safely leave home unassisted" }, new SelectListItem { Text = "Unsafe to leave home due to cognitive or psychiatric impairments", Value = "Unsafe to leave home due to cognitive or psychiatric impairments" }, new SelectListItem { Text = "Unable to leave home due to medical restriction(s)", Value = "Unable to leave home due to medical restriction(s)" }, new SelectListItem { Text = "Other", Value = "Other" } }, "Value", "Text", data.ContainsKey("HomeboundStatus") ? data["HomeboundStatus"].Answer : "0");%><%= Html.DropDownList("TransferSummary_HomeboundStatus", homeboundStatus, new { @id = "TransferSummary_HomeboundStatus" })%><%= Html.TextBox("TransferSummary_HomeboundStatusOther", data.ContainsKey("HomeboundStatusOther") ? data["HomeboundStatusOther"].Answer : string.Empty, new { @id = "TransferSummary_HomeboundStatusOther", @style = "display:none;" })%></div>
                    </div>
                </td>
            </tr>
            <tr><th>Functional Limitations</th><th>Patient Condition</th><th>Service(s) Provided</th></tr>
            <tr>
                <td>
                    <table class="fixed align_left">
                        <tbody><% string[] functionLimitations = data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null; %><input name="TransferSummary_FunctionLimitations" value=" " type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='TransferSummary_FunctionLimitations1' name='TransferSummary_FunctionLimitations' value='1' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("1") ? "checked='checked'" : "")%><label for="TransferSummary_FunctionLimitations1" class="radio">Amputation</label></td>
                                <td><%= string.Format("<input id='TransferSummary_FunctionLimitations5' name='TransferSummary_FunctionLimitations' value='5' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("5") ? "checked='checked'" : "")%><label for="TransferSummary_FunctionLimitations5" class="radio">Paralysis</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='TransferSummary_FunctionLimitations9' name='TransferSummary_FunctionLimitations' value='9' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("9") ? "checked='checked'" : "")%><label for="TransferSummary_FunctionLimitations9" class="radio">Legally Blind</label></td>
                                <td><%= string.Format("<input id='TransferSummary_FunctionLimitations2' name='TransferSummary_FunctionLimitations' value='2' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("2") ? "checked='checked'" : "")%><label for="TransferSummary_FunctionLimitations2" class="radio">Bowel/Bladder Incontinence</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='TransferSummary_FunctionLimitations6' name='TransferSummary_FunctionLimitations' value='6' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("6") ? "checked='checked'" : "")%><label for="TransferSummary_FunctionLimitations6" class="radio">Endurance</label></td>
                                <td><%= string.Format("<input id='TransferSummary_FunctionLimitationsA' name='TransferSummary_FunctionLimitations' value='A' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("A") ? "checked='checked'" : "")%><label for="TransferSummary_FunctionLimitationsA" class="radio">Dyspnea</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='TransferSummary_FunctionLimitations3' name='TransferSummary_FunctionLimitations' value='3' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("3") ? "checked='checked'" : "")%><label for="TransferSummary_FunctionLimitations3" class="radio">Contracture</label></td>
                                <td><%= string.Format("<input id='TransferSummary_FunctionLimitations7' name='TransferSummary_FunctionLimitations' value='7' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("7") ? "checked='checked'" : "")%><label for="TransferSummary_FunctionLimitations7" class="radio">Ambulation</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='TransferSummary_FunctionLimitations4' name='TransferSummary_FunctionLimitations' value='4' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("4") ? "checked='checked'" : "")%><label for="TransferSummary_FunctionLimitations4" class="radio">Hearing</label></td>
                                <td><%= string.Format("<input id='TransferSummary_FunctionLimitations8' name='TransferSummary_FunctionLimitations' value='8' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("8") ? "checked='checked'" : "")%><label for="TransferSummary_FunctionLimitations8" class="radio">Speech</label></td>
                            </tr><tr>
                                <td colspan="2"><%= string.Format("<input id='TransferSummary_FunctionLimitationsB' name='TransferSummary_FunctionLimitations' value='B' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("B") ? "checked='checked'" : "")%><label for="TransferSummary_FunctionLimitationsB" class="radio">Other (Specify)</label> <%= Html.TextBox("TransferSummary_FunctionLimitationsOther", data.ContainsKey("FunctionLimitationsOther") ? data["FunctionLimitationsOther"].Answer : string.Empty, new { @id = "TransferSummary_FunctionLimitationsOther", @class = "oe" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td><td>
                    <table class="fixed align_left">
                        <tbody><% string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %><input name="TransferSummary_PatientCondition" value=" " type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='TransferSummary_PatientConditionStable' name='TransferSummary_PatientCondition' value='1' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("1") ? "checked='checked'" : "")%><label for="TransferSummary_PatientConditionStable" class="radio">Stable</label></td>
                                <td><%= string.Format("<input id='TransferSummary_PatientConditionImproved' name='TransferSummary_PatientCondition' value='2' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("2") ? "checked='checked'" : "")%><label for="TransferSummary_PatientConditionImproved" class="radio">Improved</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='TransferSummary_PatientConditionUnchanged' name='TransferSummary_PatientCondition' value='3' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("3") ? "checked='checked'" : "")%><label for="TransferSummary_PatientConditionUnchanged" class="radio">Unchanged</label></td>
                                <td><%= string.Format("<input id='TransferSummary_PatientConditionUnstable' name='TransferSummary_PatientCondition' value='4' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("4") ? "checked='checked'" : "")%><label for="TransferSummary_PatientConditionUnstable" class="radio">Unstable</label></td>
                            </tr><tr>
                                <td colspan="2"><%= string.Format("<input id='TransferSummary_PatientConditionDeclined' name='TransferSummary_PatientCondition' value='5' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("5") ? "checked='checked'" : "")%><label for="TransferSummary_PatientConditionDeclined" class="radio">Declined</label></td>
                            </tr>
                        </tbody>
                    </table>
                </td><td>
                    <table class="fixed align_left">
                        <tbody><% string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %><input name="TransferSummary_ServiceProvided" value="" type="hidden" />
                            <tr>
                                <td><%= string.Format("<input id='TransferSummary_ServiceProvidedSN' name='TransferSummary_ServiceProvided' value='1' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("1") ? "checked='checked'" : "")%><label for="TransferSummary_ServiceProvidedSN" class="radio">SN</label></td>
                                <td><%= string.Format("<input id='TransferSummary_ServiceProvidedPT' name='TransferSummary_ServiceProvided' value='2' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("2") ? "checked='checked'" : "")%><label for="TransferSummary_ServiceProvidedPT" class="radio">PT</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='TransferSummary_ServiceProvidedOT' name='TransferSummary_ServiceProvided' value='3' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("3") ? "checked='checked'" : "")%><label for="TransferSummary_ServiceProvidedOT" class="radio">OT</label></td>
                                <td><%= string.Format("<input id='TransferSummary_ServiceProvidedST' name='TransferSummary_ServiceProvided' value='4' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("4") ? "checked='checked'" : "")%><label for="TransferSummary_ServiceProvidedST" class="radio">ST</label></td>
                            </tr><tr>
                                <td><%= string.Format("<input id='TransferSummary_ServiceProvidedMSW' name='TransferSummary_ServiceProvided' value='5' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("5") ? "checked='checked'" : "")%><label for="TransferSummary_ServiceProvidedMSW" class="radio">MSW</label></td>
                                <td><%= string.Format("<input id='TransferSummary_ServiceProvidedHHA' name='TransferSummary_ServiceProvided' value='6' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("6") ? "checked='checked'" : "")%><label for="TransferSummary_ServiceProvidedHHA" class="radio">HHA</label></td>
                            </tr><tr>
                                <td colspan="2"><%= string.Format("<input id='TransferSummary_ServiceProvidedOther' name='TransferSummary_ServiceProvided' value='7' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("7") ? "checked='checked'" : "")%><label for="TransferSummary_ServiceProvidedOther" class="radio">Other</label> <%= Html.TextBox("TransferSummary_ServiceProvidedOtherValue", data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : string.Empty, new { @id = "TransferSummary_ServiceProvidedOtherValue", @class = "oe" })%></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><th>Vital Sign Ranges:</th><th>Summary Of Care Provided By HHA</th><th>Transfer Facility Information</th></tr>
            <tr>
                <td>
                    <table class="fixed">
                        <thead>
                            <tr><th></th><th>BP</th><th>HR</th><th>Resp</th><th>Temp</th><th>Weight</th><th>BG</th></tr>
                            <tr>
                                <th>Lowest</th>
                                <td><%= Html.TextBox("TransferSummary_VitalSignBPMin", data.ContainsKey("VitalSignBPMin") ? data["VitalSignBPMin"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignBPMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("TransferSummary_VitalSignHRMin", data.ContainsKey("VitalSignHRMin") ? data["VitalSignHRMin"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignHRMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("TransferSummary_VitalSignRespMin", data.ContainsKey("VitalSignRespMin") ? data["VitalSignRespMin"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignRespMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("TransferSummary_VitalSignTempMin", data.ContainsKey("VitalSignTempMin") ? data["VitalSignTempMin"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignTempMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("TransferSummary_VitalSignWeightMin", data.ContainsKey("VitalSignWeightMin") ? data["VitalSignWeightMin"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignWeightMin", @class = "fill" })%></td>
                                <td><%= Html.TextBox("TransferSummary_VitalSignBGMin", data.ContainsKey("VitalSignBGMin") ? data["VitalSignBGMin"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignBGMin", @class = "fill" })%></td>
                            </tr><tr>
                                <th>Highest</th>
                                <td><%= Html.TextBox("TransferSummary_VitalSignBPMax", data.ContainsKey("VitalSignBPMax") ? data["VitalSignBPMax"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignBPMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("TransferSummary_VitalSignHRMax", data.ContainsKey("VitalSignHRMax") ? data["VitalSignHRMax"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignHRMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("TransferSummary_VitalSignRespMax", data.ContainsKey("VitalSignRespMax") ? data["VitalSignRespMax"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignRespMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("TransferSummary_VitalSignTempMax", data.ContainsKey("VitalSignTempMax") ? data["VitalSignTempMax"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignTempMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("TransferSummary_VitalSignWeightMax", data.ContainsKey("VitalSignWeightMax") ? data["VitalSignWeightMax"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignWeightMax", @class = "fill" })%></td>
                                <td><%= Html.TextBox("TransferSummary_VitalSignBGMax", data.ContainsKey("VitalSignBGMax") ? data["VitalSignBGMax"].Answer : string.Empty, new { @id = "TransferSummary_VitalSignBGMax", @class = "fill" })%></td>
                            </tr>
                        </thead>
                    </table>
                </td><td>
                    <%= Html.TextArea("TransferSummary_SummaryOfCareProvided", data.ContainsKey("SummaryOfCareProvided") ? data["SummaryOfCareProvided"].Answer : string.Empty, new { @class = "fill", @id = "TransferSummary_SummaryOfCareProvided" })%>
                </td><td>
                    <label for="TransferSummary_Facility" class="float_left">Facility:</label>
                    <div class="float_right"><%= Html.TextBox("TransferSummary_Facility", data.ContainsKey("Facility") ? data["Facility"].Answer : string.Empty, new { @id = "TransferSummary_Facility" })%></div>
                    <div class="clear"></div>
                    <label for="TransferSummary_Phone1" class="float_left">Phone:</label>
                    <div class="float_right"><%= Html.TextBox("TransferSummary_Phone1", data.ContainsKey("Phone1") ? data["Phone1"].Answer : string.Empty, new { @id = "TransferSummary_Phone1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3" })%> - <%= Html.TextBox("TransferSummary_Phone2", data.ContainsKey("Phone2") ? data["Phone2"].Answer : string.Empty, new { @id = "TransferSummary_Phone2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3" })%> - <%= Html.TextBox("TransferSummary_Phone3", data.ContainsKey("Phone3") ? data["Phone3"].Answer : string.Empty, new { @id = "TransferSummary_Phone3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4" })%></div>
                    <div class="clear"></div>
                    <label for="TransferSummary_Contact" class="float_left">Contact:</label>
                    <div class="float_right"><%= Html.TextBox("TransferSummary_Contact", data.ContainsKey("Contact") ? data["Contact"].Answer : string.Empty, new { @id = "TransferSummary_Contact" })%></div>
                    <div class="clear"></div>
                    <label class="float_left">Services Providing:</label>
                    <div class="clear"></div>
                    <div class="float_right"><%= Html.TextArea("TransferSummary_ServicesProviding", data.ContainsKey("ServicesProviding") ? data["ServicesProviding"].Answer : string.Empty, new { @class = "fill", @id = "TransferSummary_ServicesProviding" })%></div>
                </td>
            </tr>
            <tr><th colspan="3">Electronic Signature</th></tr>
            <tr>
                <td colspan="3">
                    <div class="third">
                        <label for="TransferSummary_ClinicianSignature" class="float_left">Clinician:</label>
                        <div class="float_right"><%= Html.Password("TransferSummary_Clinician", "", new { @id = "TransferSummary_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="TransferSummary_ClinicianSignatureDate" class="float_left">Date:</label>
                        <div class="float_right"><%= Html.Telerik().DatePicker().Name("TransferSummary_SignatureDate").Value(data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer.ToDateTime().ToString("MM/dd/yyy") : DateTime.Today.ToString("MM/dd/yyy")).MaxDate(DateTime.Now).MinDate(Model.StartDate).HtmlAttributes(new { @id = "TransferSummary_SignatureDate", @class = "date" })%></div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="TransferSummary_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="TransferSummaryRemove(); $('#TransferSummary_Button').val($(this).html()).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="TransferSummaryAdd(); $('#TransferSummary_Button').val($(this).html()).closest('form').submit();">Submit</a></li>
            <li><a href="javascript:void(0);" onclick="TransferSummaryRemove(); UserInterface.CloseWindow('transferSummary');">Close</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">

    $("#TransferSummary_MR").attr('readonly', true);
    $("#TransferSummary_EpsPeriod").attr('readonly', true);
    function TransferSummaryAdd() {
        $("#TransferSummary_Clinician").removeClass('required').addClass('required');
        $("#TransferSummary_SignatureDate").removeClass('required').addClass('required');
    }
    function TransferSummaryRemove() {
        $("#TransferSummary_Clinician").removeClass('required');
        $("#TransferSummary_SignatureDate").removeClass('required');
    }
</script>