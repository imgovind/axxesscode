﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleViewData>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Schedule Center | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','schedulecenter');</script>")%>
<% string[] stabs = new string[] { "Nursing", "HHA" }; %>
<div class="wrapper layout">
    <div class="layout_left">
        <div class="top">
            <div class="buttons heading"><ul><li><a href="javascript:void(0);" onclick="javascript:acore.open('newpatient');" title="Add New Patient">Add New Patient</a></li></ul></div>
            <div class="row">Select Patient</div>
            <div class="row"><label>View:</label><div><select name="list" class="scheduleStatusDropDown"><option value="1">Active Patients</option><option value="2">Discharged Patients</option></select></div></div>
            <div class="row"><label>Filter:</label><div><select name="list" class="schedulePaymentDropDown"><option value="0">All</option><option value="1">Medicare (traditional)</option><option value="2">Medicare (HMO/managed care)</option><option value="3">Medicaid (traditional)</option><option value="4">Medicaid (HMO/managed care)</option><option value="5">Workers' compensation</option><option value="6">Title programs </option><option value="7">Other government</option><option value="8">Private</option><option value="9">Private HMO/managed care</option><option value="10">Self Pay</option><option value="11">Unknown</option></select></div></div>
            <div class="row"><label>Find:</label><div><input id="txtSearch_Schedule_Selection" class="text" name="" value="" type="text" /></div></div>
        </div>
        <div class="bottom"><% Html.RenderPartial("Patients", Model.Patients); %></div>
    </div>
    <div id="scheduleMainResult" class="layout_main">
    <% if (Model.Episode != null) { %>
        <div class="top">
            <div id="scheduleTop"><% Html.RenderPartial("Calendar", Model.Episode); %></div>
            <div id="schedule_collapsed"><a href="javascript:void(0);" onclick="Schedule.ShowScheduler()" class="show_scheduler">Show Scheduler</a></div>
        <% if (Current.HasRight(Permissions.EditEpisode)) { %>
            <% Html.Telerik().TabStrip().Name("ScheduleTabStrip").ClientEvents(events => events.OnSelect("Schedule.OnSelect")).Items(tabstrip => { %>
            <% for (int sindex = 0; sindex < stabs.Length; sindex++) { %>
                <% string stitle = stabs[sindex]; %>
                <% tabstrip.Add().Text(stitle).HtmlAttributes(new { id = stitle + "_Tab" }).Content(() => { %>
                    <% using (Html.BeginForm("Add", "Schedule", FormMethod.Post)) { %>
            <%= Html.Hidden("patientId") %>
            <div class="tabcontents">
                <table id="<%= stitle %>ScheduleTable" data="<%= stitle %>" class="scheduleTables purgable">
                    <thead><tr><th>Discipline</th><th>Employee</th><th>Date</th><th>Action</th></tr></thead>
                    <tbody></tbody>
                </table>
                <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" /><input type="hidden" name="episodeId" value="" class="scheduleValue" />
                <div class="buttons"><ul>
                    <li><%= String.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ScheduleInputFix($(this),'Patient','#{0}ScheduleTable');\">Save</a>",stitle) %></li>
                    <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
                </ul></div>
            </div><%
                        }
                    });
                }
               tabstrip.Add().Text("Orders/Care Plans").HtmlAttributes(new { id = "Orders_Tab" }).Content(() =>
               { 
                    using (Html.BeginForm("Add", "Schedule", FormMethod.Post)) { %>
            <%= Html.Hidden("patientId") %>
            <div class="tabcontents">
                <table id="OrdersScheduleTable" data="Orders" class="scheduleTables purgable">
                    <thead><tr><th>Discipline</th><th>Employee</th><th>Date</th><th>Action</th></tr></thead>
                    <tbody></tbody>
                </table>
                <input type="hidden" name="Patient_Schedule" value="" class="scheduleValue" /><input type="hidden" name="episodeId" value="" />
                <div class="buttons float_right"><ul>
                    <li><a href="javascript:void(0);" onclick="Schedule.ScheduleInputFix($(this),'Patient','#OrdersScheduleTable');">Save</a></li>
                    <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
                </ul></div>
            </div><%
                    }
                });
                tabstrip.Add().Text("Add Multiple Schedule").Content(() => {
                    using (Html.BeginForm("AddMultiple", "Schedule", FormMethod.Post, new { @id = " ", })) { %>
            <%= Html.Hidden("patientId", "", new { @id = "" })%>
            <div class="tabcontents">
                <table id="multipleScheduleTable" data="Multiple" class="scheduleTables">
                    <thead><tr><th>Discipline</th><th>Employee</th><th>Date</th></tr></thead>
                    <tbody><tr>
                        <td><select name="DisciplineTask" class="MultipleDisciplineTask"><option value="0">Select Discipline</option></select></td>
                        <td><select name="userId" onfocus="" class="suppliesCode Users"><option value="0">Select Employee</option></select></td>
                        <td class="daterange"><%= Html.Telerik().DatePicker().Name("StartDate").Value(Model.Episode.StartDate).MinDate(Model.Episode.StartDate).MaxDate(Model.Episode.EndDate)%><span>to</span><%= Html.Telerik().DatePicker().Name("EndDate").Value(Model.Episode.StartDate).MinDate(Model.Episode.StartDate).MaxDate(Model.Episode.EndDate)%></td>
                    </tr></tbody>
                </table>
                <input type="hidden" name="episodeId" value="" class="scheduleValue" /><input type="hidden" name="Discipline" value="" /><input type="hidden" name="IsBillable" value="" />
                <div class="buttons float_right"><ul>
                    <li><a href="javascript:void(0);" onclick="Schedule.FormSubmitMultiple($(this));">Save</a></li>
                    <li><a href="javascript:void(0);" onclick="Schedule.CloseNewEvent($(this));">Cancel</a></li>
                </ul></div>
            </div><%
                    }
                });
            }).SelectedIndex(0).Render();
        } %>
        </div>
        <div id="scheduleBottomPanel" class="bottom"><% Html.RenderPartial("Activities", new ScheduleActivityArgument { EpisodeId=Model.Episode.Id,PatientId=Model.Episode.PatientId,Discpline="Nursing"}); %></div><%
    } else { %><div class="abs center">No Patient Data Found</div><%
    } %>
    </div>
</div>