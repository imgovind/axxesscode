﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<ScheduleEvent>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Missed Visit<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body></body><%
Html.Telerik().ScriptRegistrar().Globalization(true) 
     .DefaultGroup(group => group
     .Add("jquery-1.4.2.min.js")
     .Add("/Modules/printview.js")
     .Compress(true).Combined(true).CacheDurationInDays(5))
     .OnDocumentReady(() => {  %>
        printview.cssclass = "largerfont";
        printview.firstheader =  printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : ""%><%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : ""%>%3Cbr /%3E<%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3EMissed Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            '<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %>' +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cimg src=%22/Images/axxess_print.png%22 class=%22axxess%22 /%3E%3Cimg src=%22/Images/acore_print.png%22 class=%22acore%22 /%3E";
        printview.addsection(
            printview.col(2,
                printview.span("Type of Visit:",1) +
                printiview.span("<%= Model != null ? Model.DisciplineTaskName : "" %>",0,2) +
                printview.span("Date of Visit:",1) +
                printiview.span("<%= Model != null ? Model.EventDate : "" %>",0,2) +
                printview.span("Order Generated:",1) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data != null && data.ContainsKey("IsOrderGenerated") && data["IsOrderGenerated"].Answer == "true" ? "true" : "false"%> ) +
                    printview.checkbox("No",<%= data != null && data.ContainsKey("IsOrderGenerated") && data["IsOrderGenerated"].Answer == "false" ? "true" : "false"%> )) +
                printview.span("Physician Office Notified:",1) +
                printview.col(2,
                    printview.checkbox("Yes",<%= data != null && data.ContainsKey("IsPhysicianOfficeNotified") && data["IsPhysicianOfficeNotified"].Answer == "true" ? "true" : "false"%> ) +
                    printview.checkbox("No",<%= data != null && data.ContainsKey("IsPhysicianOfficeNotified") && data["IsPhysicianOfficeNotified"].Answer == "false" ? "true" : "false"%> )) +
                printview.span("Reason:",1) +
                printiview.span("<%= data != null && data.ContainsKey("Reason") && data["Reason"].Answer.IsNotNullOrEmpty() ? data["Reason"].Answer : ""%>",0,2)) +
            printview.span("Comments:",1) +
            printiview.span("<%= data != null && data.ContainsKey("Comments") && data["Comments"].Answer.IsNotNullOrEmpty() ? data["Comments"].Answer : ""%>",0,10),
            "Missed Visit Details");<%
     }).Render(); %>
</html>
