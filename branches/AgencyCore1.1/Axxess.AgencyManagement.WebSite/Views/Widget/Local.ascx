﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="digiclock"></div>
<div class="weatherFeed">
    <div id="weatherItem"></div>
    <div id="weatherCity" class="weatherCity"></div>
    <div id="weatherTemp" class="weatherTemp"></div>
    <div id="weatherDesc" class="weatherDesc"></div>
    <div id="weatherWind" class="weatherWind"></div>
    <div id="weatherRange" class="weatherRange"></div>
    <div id="weatherLink" class="weatherLink"><a id="lnkWeather" target="_blank">Full forecast</a></div>
</div>

<script type="text/javascript">
    $.simpleWeather({
        zipcode: '<%= Html.ZipCode() %>',
        unit: 'f',
        success: function(weather) {
            $("#weatherCity").html(weather.city + ', ' + weather.region);
            $("#weatherTemp").html(weather.temp + '&deg; ' + weather.units.temp);
            $("#weatherDesc").html(weather.currently);
            $("#weatherRange").html('<strong>High</strong>: ' + weather.high + '&deg; ' + weather.units.temp + ' <strong>Low</strong>: ' + weather.low + '&deg; ' + weather.units.temp);
            $("#weatherWind").html('<strong>Wind</strong>: ' + weather.wind.direction + ' ' + weather.wind.speed + ' ' + weather.units.speed);
            $("#lnkWeather").attr("href", weather.link);
            $("#weatherItem").css("background-image", 'url(' + weather.image + ')');
            $("#weatherItem").addClass("weatherItem");
        },
        error: function(error) {
            $("#weatherItem").html('<p>' + error + '</p>');
        }
    });
    $('#digiclock').jdigiclock();
</script>