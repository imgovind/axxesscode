﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th>Patient Name</th>
				<th>TOB</th>
				<th>Episode</th>
			</tr>
        </thead>
	    <tbody id="claimsWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div id="claimsWidgetMore" class="widget-more"><a onclick="UserInterface.ShowBillingCenter();" href="javascript:void(0);">More &raquo;</a></div>