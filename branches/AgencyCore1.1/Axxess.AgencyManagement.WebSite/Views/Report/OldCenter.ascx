﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%--<% @reportArr = new Array({PatientEmergencyContact} => 'Patient Emergency Contact','Patient Birthday Listing','Patient Address Listing'); %>
,
                         {"",""}
--%>
<% string[,] patientMenuArr = new string[,]  {{"PatientReport","Patient Reports"},
                         {"PatientEmergencyContact","Patient Emergency Contact"},
                         {"PatientBirthdayListing","Patient Birthday Listing"},
                         {"PatientAddressBook","Patient Address Listing"},
                         {"PatientRoster","Patient Roster"},
                         {"PatientSocCertPeriodListing","Patient SOC Cert Period Listing"},
                         {"PatientOnCallListing","Patient Roster for on Call Nurse"},
                         {"PatientByPhysicianListing","Patient by Physician Listing"},
                         {"PatientByResponsibleEmployeeListing","Patient by Responsible Employee Listing"}};
   string[,] ClinicalMenuArr = new string[,]  {{"ClinicaltReport","Clinical Reports"},
                         {"ClinicalUnsignedOrder","Clinical Unsigned Orders"},
                         {"ClinicalOutstandingResert","Clinical Outstanding Reserts"},
                         {"ClinicalIncompleteOasis","Clinical Incomplete OASIS"}};
   string[,] ScheduleMenuArr = new string[,]  {{"ScheduleReport","Schedule Reports"},
                         {"PatientEmergencyContact","Patient Emergency Contact"},
                         {"PatientBirthdayListing","Patient Birthday Listing"},
                         {"PatientAddressBookForm","Patient Address Listing"}};
   string[,] BillingMenuArr = new string[,]  {{"BillingReport","Billing Reports"},
                         {"PatientEmergencyContact","Patient Emergency Contact"},
                         {"PatientEmergencyContact","Patient Emergency Contact"},
                         {"PatientBirthdayListing","Patient Birthday Listing"},
                         {"PatientAddressBook","Patient Address Listing"},
                         {"PatientRoster","Patient Roster"},
                         {"PatientSocCertPeriodListing","Patient SOC Cert Period Listing"},
                         {"PatientOnCallListing","Patient Roster for on Call Nurse"},
                         {"PatientBirthdayListing","Patient Birthday Listing"},
                         {"PatientAddressBookForm","Patient Address Listing"}};
   string[,] EmployeeMenuArr = new string[,]  {{"EmployeeReport","Employee Reports"},
                         {"PatientEmergencyContact","Patient Emergency Contact"},
                         {"PatientBirthdayListing","Patient Birthday Listing"},
                         {"PatientAddressBookForm","Patient Address Listing"}};
   string[,] StatisticalMenuArr = new string[,]  {{"StatisticalReport","Statistical Reports"},
                         {"PatientEmergencyContact","Patient Emergency Contact"},
                         {"PatientBirthdayListing","Patient Birthday Listing"},
                         {"PatientAddressBookForm","Patient Address Listing"}};
%>

<div id="reportTabs" class="vertical-tabs vertical-tabs-left ReportContainer">
    <ul class="verttab">
        <li id="patient_parent"><%=patientMenuArr[0, 1]%>
            <div class="tabs">
                <% for (int i = 1; i < (patientMenuArr.Length)/2; i++)
                   {%>
                    <a title="Report/<%=patientMenuArr[i,0]%>"><%=patientMenuArr[i, 1]%></a>
                <%  } %>
            </div>
        </li>
        <li id="clinical_parent"><%=ClinicalMenuArr[0, 1]%>
            <div class="tabs">
                <% for (int i = 1; i < (ClinicalMenuArr.Length) / 2; i++)
                   {%>
                    <a title="Report/<%=ClinicalMenuArr[i,0]%>"><%=ClinicalMenuArr[i, 1]%></a>
                <%  } %>
            </div>
        </li>
        <li id="schedule_parent"><%=ScheduleMenuArr[0, 1]%>
            <div class="tabs">
                <% for (int i = 1; i < (ScheduleMenuArr.Length) / 2; i++)  {%>
                    <a title="Report/<%=ScheduleMenuArr[i,0]%>"><%=ScheduleMenuArr[i, 1]%></a>
                <%  } %>
            </div>
        </li>
        <li id="billing_parent"><%=BillingMenuArr[0, 1]%>
            <div class="tabs">
                <% for (int i = 1; i < (BillingMenuArr.Length) / 2; i++)  {%>
                    <a title="Report/<%=BillingMenuArr[i,0]%>"><%=BillingMenuArr[i, 1]%></a>
                <%  } %>
            </div>
        </li>
        <li id="employee_parent"><%=EmployeeMenuArr[0, 1]%>
            <div class="tabs">
                <% for (int i = 1; i < (EmployeeMenuArr.Length) / 2; i++)  {%>
                    <a title="Report/<%=EmployeeMenuArr[i,0]%>"><%=EmployeeMenuArr[i, 1]%></a>
                <%  } %>
            </div>
        </li>
        <li id="statistical_parent"><%=StatisticalMenuArr[0, 1]%>
            <div class="tabs">
                <% for (int i = 1; i < (StatisticalMenuArr.Length) / 2; i++)  {%>
                    <a title="Report/<%=StatisticalMenuArr[i,0]%>"><%=StatisticalMenuArr[i, 1]%></a>
                <%  } %>
            </div>
        </li>
    </ul>
    <div id="report_emergency_contact" class="general">
 	    <div class="report_main_page_boxes_header">
            <span class="left_menu_top"><span class="r4 c1">&nbsp;</span> <span class="r3 c1">&nbsp;</span>
                <span class="r2 c1">&nbsp;</span> <span class="r1 c1">&nbsp;</span> <span class="r0 c1">
                    &nbsp;</span> </span>
            <div class="report_main_page_boxes">
                <h3> <%=patientMenuArr[0, 1]%></h3>
                <div class="report_main_page_box_list">
                <% var len = (patientMenuArr.Length)/2;
                   if (len > 6) len = 6;
                   for (int i = 1; i < len; i++)
                   {%>
                    <a title="Report/<%=patientMenuArr[i,0]%>"><%=patientMenuArr[i,1]%></a>
                <%  } %>
                
                 <a title="Report/<%=patientMenuArr[0,0]%>" class="red_text right_align">more...</a>
                </div>
            </div>
        </div>
  	    <div class="report_main_page_boxes_header">
            <span class="left_menu_top"><span class="r4 c1">&nbsp;</span> <span class="r3 c1">&nbsp;</span>
                <span class="r2 c1">&nbsp;</span> <span class="r1 c1">&nbsp;</span> <span class="r0 c1">
                    &nbsp;</span> </span>
            <div class="report_main_page_boxes">
                <h3> <%=ClinicalMenuArr[0, 1]%></h3>
                <div class="report_main_page_box_list">
                <%  len = (ClinicalMenuArr.Length)/2;
                   if (len > 6) len = 6;
                   for (int i = 1; i < len; i++)                   {%>
                    <a title="Report/<%=ClinicalMenuArr[i,0]%>"><%=ClinicalMenuArr[i, 1]%></a>
                <%  } %>
                
                 <a title="Report/<%=ClinicalMenuArr[0,0]%>" class="red_text right_align">more...</a>
                </div>
            </div>
        </div> 	    
        <div class="report_main_page_boxes_header">
            <span class="left_menu_top"><span class="r4 c1">&nbsp;</span> <span class="r3 c1">&nbsp;</span>
                <span class="r2 c1">&nbsp;</span> <span class="r1 c1">&nbsp;</span> <span class="r0 c1">
                    &nbsp;</span> </span>
            <div class="report_main_page_boxes">
                <h3> <%=ScheduleMenuArr[0, 1]%></h3>
                <div class="report_main_page_box_list">
                <%  len = (ScheduleMenuArr.Length)/2;
                   if (len > 6) len = 6;
                   for (int i = 1; i < len; i++)         
                   {%>
                    <a title="Report/<%=ScheduleMenuArr[i,0]%>"><%=ScheduleMenuArr[i, 1]%></a>
                <%  } %>
                
                 <a title="Report/<%=ScheduleMenuArr[0,0]%>" class="red_text right_align">more...</a>
                </div>
            </div>
        </div> 	   
        <div class="report_main_page_boxes_header">
            <span class="left_menu_top"><span class="r4 c1">&nbsp;</span> <span class="r3 c1">&nbsp;</span>
                <span class="r2 c1">&nbsp;</span> <span class="r1 c1">&nbsp;</span> <span class="r0 c1">
                    &nbsp;</span> </span>
            <div class="report_main_page_boxes">
                <h3> <%=BillingMenuArr[0, 1]%></h3>
                <div class="report_main_page_box_list">
                <%  len = (BillingMenuArr.Length) / 2;
                   if (len > 6) len = 6;
                   for (int i = 1; i < len; i++)                        {%>
                    <a title="Report/<%=BillingMenuArr[i,0]%>"><%=BillingMenuArr[i, 1]%></a>
                <%  } %>
                
                 <a title="Report/<%=BillingMenuArr[0,0]%>" class="red_text right_align">more...</a>
                </div>
            </div>
        </div> 	    
        <div class="report_main_page_boxes_header">
            <span class="left_menu_top"><span class="r4 c1">&nbsp;</span> <span class="r3 c1">&nbsp;</span>
                <span class="r2 c1">&nbsp;</span> <span class="r1 c1">&nbsp;</span> <span class="r0 c1">
                    &nbsp;</span> </span>
            <div class="report_main_page_boxes">
                <h3> <%=EmployeeMenuArr[0, 1]%></h3>
                <div class="report_main_page_box_list">
                <%  len = (EmployeeMenuArr.Length) / 2;
                   if (len > 6) len = 6;
                   for (int i = 1; i < len; i++)                          {%>
                    <a title="Report/<%=EmployeeMenuArr[i,0]%>"><%=EmployeeMenuArr[i, 1]%></a>
                <%  } %>
                
                 <a title="Report/<%=EmployeeMenuArr[0,0]%>" class="red_text right_align">more...</a>
                </div>
            </div>
        </div>
        <div class="report_main_page_boxes_header">
            <span class="left_menu_top"><span class="r4 c1">&nbsp;</span> <span class="r3 c1">&nbsp;</span>
                <span class="r2 c1">&nbsp;</span> <span class="r1 c1">&nbsp;</span> <span class="r0 c1">
                    &nbsp;</span> </span>
            <div class="report_main_page_boxes">
                <h3> <%=StatisticalMenuArr[0, 1]%></h3>
                <div class="report_main_page_box_list">
               <%  len = (StatisticalMenuArr.Length) / 2;
                   if (len > 6) len = 6;
                   for (int i = 1; i < len; i++)                      {%>
                    <a title="Report/<%=StatisticalMenuArr[i,0]%>"><%=StatisticalMenuArr[i, 1]%></a>
                <%  } %>
                
                 <a title="Report/<%=StatisticalMenuArr[0,0]%>" class="red_text right_align">more...</a>
                </div>
            </div>
        </div>
    </div>
</div>
