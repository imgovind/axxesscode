﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<div class="wrapper">
    <fieldset>
        <legend>Patient By Physician Listing</legend>
        <div class="column">
            <div class="row"><label for="Report_Patient_Physician_PhysicanId" class="float_left">Physician:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Physicians, "AgencyPhysicianId", "", new { @id = "Report_Patient_Physician_PhysicanId", @class = "Physicians" })%></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientPhysician();">Generate Report</a></li></ul></div>
    </fieldset>
    <div class="clear">&nbsp;</div>
    <div class="ReportGrid">
         <%= Html.Telerik().Grid<PatientRoster>().Name("Report_Patient_Physician_Grid").Columns(columns =>
            {
                columns.Bound(r => r.PatientDisplayName).Title("Patient");
                columns.Bound(r => r.PatientAddressLine1).Title("Address");
                columns.Bound(r => r.PatientAddressCity).Title("City");
                columns.Bound(r => r.PatientAddressStateCode).Title("State");
                columns.Bound(r => r.PatientAddressZipCode).Title("Zip Code");
                columns.Bound(r => r.PatientPhone).Title("Home Phone");
                columns.Bound(r => r.PatientGender);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("PatientByPhysicians", "Report", new { AgencyPhysicianId = Guid.Empty }))
           .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">    $('#Report_Patient_Physician_Grid .t-grid-content').css({ 'height': 'auto' }); </script>