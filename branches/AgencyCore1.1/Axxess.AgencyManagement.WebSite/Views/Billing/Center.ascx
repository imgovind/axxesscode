﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Billing Center | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','billingcenter');</script>")%>
<div id="Billing_CenterContent" class="main wrapper">
    <% Html.RenderPartial("RapGrid", Model); %>
    <% Html.RenderPartial("FinalGrid", Model); %>
    <% if (Current.HasRight(Permissions.GenerateClaimFiles))
       { %>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="Billing.loadGenerate('#Billing_CenterContent');">Generate Selected</a></li>
        <li><a href="javascript:void(0);" onclick="">Generate All Completed</a></li>
    </ul></div>
    <% } %>
</div>
