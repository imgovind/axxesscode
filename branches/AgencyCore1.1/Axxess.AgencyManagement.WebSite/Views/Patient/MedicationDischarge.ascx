﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="float_right">
    <%= Html.Telerik().DatePicker().Name("DischargeDate").Value(DateTime.Today).HtmlAttributes(new { @id = "Medication_DischargeDate", @class = "text date" })%></div>
