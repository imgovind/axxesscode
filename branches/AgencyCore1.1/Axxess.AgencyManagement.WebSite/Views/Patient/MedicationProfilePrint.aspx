﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<MedicationProfileViewData>" %>
<% var data = Model != null ? Model.Questions : new Dictionary<string, Question>(); %>
<% var medications = Model != null&& Model.MedicationProfile !=null && Model.MedicationProfile.Medication.IsNotNullOrEmpty() ? Model.MedicationProfile.Medication.ToObject<List<Medication>>() : new List<Medication>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%=Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Medication Profile<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body> </body><% string[] allergies = data != null && data.ContainsKey("485Allergies") && data["485Allergies"].Answer != "" ? data["485Allergies"].Answer.Split(',') : null;

Html.Telerik().ScriptRegistrar().Globalization(true) 
     .DefaultGroup(group => group
     .Add("jquery-1.4.2.min.js")
     .Add("/Modules/printview.js")
     .Compress(true).Combined(true).CacheDurationInDays(5))
     .OnDocumentReady(() => {  %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : ""%><%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : ""%>%3Cbr /%3E<%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3EMedication Profile%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            '<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %>' +
            "%28<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>%29 %3C/span%3E%3Cspan class=%22quadcol%22%3E %3Cspan %3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null ? string.Format(" {0} &ndash; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) : "" %>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= Model.PhysicianDisplayName.ToTitleCase() %>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPharmacy Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= Model.PharmacyName%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPharmacy Phone:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= Model.PharmacyPhone.ToPhone() %>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETertiary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("PrimaryDiagnosis2") ? data["PrimaryDiagnosis2"].Answer : ""%>' +
            "%3C/span%3E%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : ""%><%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : ""%>%3Cbr /%3E<%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3EMedication Profile/%3Cbr /%3ECase Conference%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            '<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %>' +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E"; 
        printview.footer = "%3Cimg src=%22/Images/axxess_print.png%22 class=%22axxess%22 /%3E%3Cimg src=%22/Images/acore_print.png%22 class=%22acore%22 /%3E";
        printview.addsection(
            printview.col(1,
                printview.span("Allergies:") +
                printview.span("<%= data != null && data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : ""%>",0,1) ));
            <% var medicationContent = "%3Ctable %3E%3Ctr class=%22padleft%22 %3E%3Ctd style=%22width:70px;%22%3E%3Cspan class=%22strong%22 %3EStart Date%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22strong%22 %3EMedication & Dosage%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22strong%22 %3ERoute%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22strong%22 %3EFrequency%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan class=%22strong%22 %3EClassification%3C/span%3E%3C/td%3E%3C/tr%3E";
               if(medications.Count > 0 ){ foreach(var medication in medications){
                   if (medication.MedicationCategory == "Active")
                   {
                       medicationContent = medicationContent + string.Format("%3Ctr class=%22padleft%22 %3E%3Ctd style=%22width:70px;%22%3E%3Cspan%3E{0}%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan%3E{1}%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan%3E{2}%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan%3E{3}%3C/span%3E%3C/td%3E%3Ctd%3E%3Cspan%3E{4}%3C/span%3E%3C/td%3E%3C/tr%3E", medication.StartDate.ToShortDateString(), medication.MedicationDosage, medication.Route, medication.Frequency, medication.Classification);
                   }
                   }}
               medicationContent += "%3C/table%3E";
             %>
              printview.addsection("<%=medicationContent %>","Current Medication");
            <%
     }).Render(); %>
</html>
