﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<% using (Html.BeginForm("SignMedicationHistory", "Patient", FormMethod.Post, new { @id = "newMedicationProfileSnapShotForm" }))
   { %>
<%
var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, Question>(); %>
<% = Html.Hidden("MedId",Model.MedicationProfile.Id) %>
<% = Html.Hidden("PatientId", Model.Patient.Id)%>
<% = Html.Hidden("EpisodeId", Model.EpisodeId)%>
<div class="visitContainer">
    <table class="visitNote" style="border-top: solid 0.25px #8B8878;" cellpadding="0">
        <tbody>
            <tr>
                <td>
                    <div class="newRow">
                       <div class="two"><label for="PatientName" class="bigtext" style="font-weight: bold;">Patient Name:</label><%=Html.Label(Model.Patient.FirstName+" "+ Model.Patient.LastName) %> (<%=Html.Label(Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty)%>)</div>
                       <div class="two"><label for="MR">Episode/Period:</label><%= Html.Label(string.Format(" {0} - {1}", Model!=null&&Model.StartDate!=null? Model.StartDate.ToShortDateString():string.Empty, Model!=null&&Model.StartDate!=null?Model.EndDate.ToShortDateString():string.Empty))%></div>
                    </div>
                </td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td>
                    <div class="newRow">
                        <div class="two">
                        <label>Allergies :</label> <%= Html.Label(data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : string.Empty)%>
                        </div>
                        <div class="two">
                            <div class="two">
                                Pharmacy:
                                <%= Html.Label( Model.PharmacyName.IsNotNullOrEmpty()?Model.PharmacyName:string.Empty)%>
                            </div>
                            <div class="two">
                                Phone:<%= Html.Label(Model.PharmacyPhone.IsNotNullOrEmpty()?Model.PharmacyPhone:string.Empty)%>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="newRow one medication">
        <%= Html.Telerik().Grid<Medication>()
        .Name("MedicatonProfileGrid")
        .DataKeys(keys => 
        {
            keys.Add(M => M.Id);
        })
                                  
        .DataBinding(dataBinding =>
        {
            dataBinding.Ajax()
                .Select("MedicationSnapshot", "Patient", new { MedId = Model.MedicationProfile.Id });
        })
        .Columns(columns =>
        {
             columns.Bound(M => M.IsLongStanding)
           .ClientTemplate("<input type='checkbox' disabled='disabled' name='IsLongStanding' <#=IsLongStanding? checked='checked' : '' #> />").Width(30);
            columns.Bound(M => M.StartDate).Format("{0:d}").Width(90);
            columns.Bound(M => M.MedicationDosage).Title("Medication & Dosage");
            columns.Bound(M => M.Route).Title("Route");
            columns.Bound(M => M.Frequency).Title("Frequency");
            columns.Bound(M => M.MedicationType).ClientTemplate("<label><#=MedicationType!=null&&MedicationType!=''? MedicationType.Text : ''#> </label>");
            columns.Bound(M => M.Classification);
        }) .Footer(false)   %>
        
    </div>   
    <table class="visitNote" style="border-top: solid 0.25px #8B8878;" cellpadding="0">
        <tbody>
            <tr>
                <td>
                    <div class="one">
                        <b>Drug Regimen Review Acknowledgment: </b>I have reviewed all the listed medications
                        for potential adverse effects, drug reactions, including ineffective drug therapy,
                        significant side effects, significant drug interactions, duplicate drug therapy,
                        and noncompliance with drug therapy.
                    </div>
                    <div class="newRow">
                        <div class="two">
                            <label class="bigtext" style="font-weight: bold;">Clinician Signature:</label><%= Html.Password("Signature", "", new { @id = "MedicationProfile_ClinicianSignature" })%>
                        </div>
                        <div class="two">
                            <label class="bigtext" style="font-weight: bold;">Date:</label>
                            <%= Html.Telerik().DatePicker().Name("SignedDate").Value(DateTime.Today.ToString("MM/dd/yyy")).HtmlAttributes(new { @id = "MedicationProfile_ClinicianSignatureDate", @class = "date" })%>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <div>
                        <div class="buttons"><ul>
                            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Sign</a></li>
                            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('medicationprofilesnapshot');">Close</a></li>
                        </ul></div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<%} %>

 