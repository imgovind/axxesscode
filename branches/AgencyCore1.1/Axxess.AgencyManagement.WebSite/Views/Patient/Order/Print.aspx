﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PhysicianOrder>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Physician Order Print View<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body>
    <div class="page largerfont"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <div>
            <table class="fixed"><tbody>
                <tr>
                    <td colspan="4">
                        <%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "<br />" : "" %>
                        <%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : ""%> <%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : ""%><br />
                        <%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>
                    </td><th colspan="2" class="h1">Physician Order</th>
                </tr><tr>
                    <td colspan="3">
                        <span class="big">Patient Name: <%= Model.Patient.LastName %>, <%= Model.Patient.FirstName %> <%= Model.Patient.MiddleInitial %></span><br />
                        <%= Model.Patient.AddressLine1.IsNotNullOrEmpty() ? Model.Patient.AddressLine1.ToTitleCase() + "<br />" : ""%>
                        <%= Model.Patient.AddressLine2.IsNotNullOrEmpty() ? Model.Patient.AddressLine2.ToTitleCase() + "<br />" : ""%>
                        <%= Model.Patient.AddressCity.IsNotNullOrEmpty() ? Model.Patient.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Patient.AddressStateCode.IsNotNullOrEmpty() ? Model.Patient.AddressStateCode + "&nbsp; " : ""%><%= Model.Patient.AddressZipCode.IsNotNullOrEmpty() ? Model.Patient.AddressZipCode : ""%><br />
                        <%= Model.Patient.PhoneHome.IsNotNullOrEmpty() ? "Phone: " + Model.Patient.PhoneHome.ToPhone() + "<br />" : ""%>
                        <%= Model.Patient.MedicareNumber.IsNotNullOrEmpty() ? "HIC: " + Model.Patient.MedicareNumber : "" %>
                    </td><td colspan="3"><span>
                        Physician Name: <%= Model.Physician.LastName%>, <%= Model.Physician.FirstName%></span><br />
                        <%= Model.Physician.AddressLine1.IsNotNullOrEmpty() ? Model.Physician.AddressLine1.ToTitleCase() + "<br />" : ""%>
                        <%= Model.Physician.AddressLine2.IsNotNullOrEmpty() ? Model.Physician.AddressLine2.ToTitleCase() + "<br />" : ""%>
                        <%= Model.Physician.AddressCity.IsNotNullOrEmpty() ? Model.Physician.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Physician.AddressStateCode.IsNotNullOrEmpty() ? Model.Physician.AddressStateCode + "&nbsp; " : ""%><%= Model.Physician.AddressZipCode.IsNotNullOrEmpty() ? Model.Physician.AddressZipCode : ""%><br />
                        <%= Model.Physician.PhoneWork.IsNotNullOrEmpty() ? "Phone: " + Model.Physician.PhoneWork.ToPhone() + "<br />" : ""%>
                        <%= Model.Physician.FaxNumber.IsNotNullOrEmpty() ? "Fax: " + Model.Physician.FaxNumber.ToPhone() + "<br />" : ""%>
                        <%= Model.Physician.NPI.IsNotNullOrEmpty() ? "NPI: " + Model.Physician.NPI : "" %>
                    </td>
                </tr>
            </tbody></table>
        </div>
        <h1>Physician Order</h1>
        <div class="tricol">
            <span><strong>Order Date:</strong> <%= Model.OrderDate != null ? Model.OrderDate.ToShortDateString() : ""%></span>
            <span><strong>Order #:</strong> <%= Model.OrderNumber != null ? Model.OrderNumber.ToString() : "" %></span>
            <span><strong>Date Received:</strong> <%= Model.ReceivedDate != null ? Model.ReceivedDate.ToShortDateString() : "" %></span>
        </div>
        <h3>Allergies</h3>
        <div><span><%= Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies : "" %></span></div>
        <h3>Episode</h3>
        <div class="bicol">
            <span><strong>Episode</strong></span>
            <span><%= Model.EpisodeStartDate %> &ndash; <%= Model.EpisodeEndDate %></span>
            <span><strong>Diagnosis</strong></span>
            <span class="bicol">
                <span><%= Model.PrimaryDiagnosisText %></span>
                <span><%= Model.PrimaryDiagnosisCode %></span>
                <span><%= Model.SecondaryDiagnosisText %></span>
                <span><%= Model.SecondaryDiagnosisCode %></span>
            </span>
        </div><div>
            <span><strong>Orders:</strong></span>
            <span class="trnt"><%= Model.Text %></span>
        </div><div>
            <table><tbody>
                <tr>
                    <td>Clinician Signature<span class="mono"><%= Model.SignatureText %></span></td>
                    <td>Date<span class="mono"><%= Model.SignatureDate.ToShortDateString() %></span></td>
                </tr><tr>
                    <td>Physician Signature<span class="mono"></span></td>
                    <td>Date<span class="mono"></span></td>
                </tr>
            </tbody></table>
        </div>
    </div>
</body>
</html>
