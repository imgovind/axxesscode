﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("NewEmergencyContact", "Patient", FormMethod.Post, new { @id = "newEmergencyContactForm" }))%>
<%  { %>
<%=Html.Hidden("PatientId", Model, new { @id = "txtNew_EmergencyContact_PatientID" })%>
   <div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
      <fieldset>
          <div class="column">
                    <div class="row"> 
                        <label for="FirstName" class="float_left">First Name:</label><div class="float_right"> <%=Html.TextBox("FirstName", "", new { @id = "txtNew_EmergencyContact_FirstName", @class = "text input_wrapper required", @maxlength = "100", @tabindex = "1" })%></div>
                    </div>
                    <div class="row">
                        <label for="LastName" class="float_left">Last Name:</label><div class="float_right"> <%=Html.TextBox("LastName", "", new { @id = "txtNew_EmergencyContact_LastName", @class = "text input_wrapper required", @maxlength = "100", @tabindex = "2" })%></div>
                    </div>
                     <div class="row">
                        <label for="Relationship" class="float_left">Relationship:</label><div class="float_right"><%=Html.TextBox("Relationship", "", new { @id = "txtNew_EmergencyContact_Relationship", @class = "text input_wrapper", @tabindex = "3" })%></div>
                    </div>
         </div>
         <div class="column">
                    <div class="row">
                        <label for="PhoneHome" class="float_left"> Primary Phone:</label>
                         <div class="float_right">
                    <span class="input_wrappermultible">
                        <%=Html.TextBox("PhonePrimaryArray", "", new { @id = "txtNew_EmergencyContact_PrimaryPhoneArray1", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3", @tabindex = "4" })%>
                    </span>- <span class="input_wrappermultible">
                        <%=Html.TextBox("PhonePrimaryArray", "", new { @id = "txtNew_EmergencyContact_PrimaryPhoneArray2", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3", @tabindex = "5" })%>
                    </span>- <span class="input_wrappermultible">
                        <%=Html.TextBox("PhonePrimaryArray", "", new { @id = "txtNew_EmergencyContact_PrimaryPhoneArray3", @class = "autotext required digits phone_long", @maxlength = "4", @size = "5", @tabindex = "6" })%>
                    </span>
                </div>
                    </div>
                    <div class="row">
                        <label for="Email" class="float_left">Email:</label><div class="float_right"> <%=Html.TextBox("EmailAddress", "", new { @id = "txtNew_EmergencyContact_Email", @class = "text email input_wrapper", @maxlength = "100", @tabindex = "7" })%></div>
                    </div>
                    <div class="row">
                        <label for="SetPrimary" class="float_left">Set Primary:</label><div class="float_left"> <%=Html.CheckBox("IsPrimary",new { @id = "txtNew_EmergencyContact_SetPrimary" })%></div>
                    </div>
                 </div>
       </fieldset>
 <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newemergencycontact');">Cancel</a></li>
        </ul>
    </div>
<%} %>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row div.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>