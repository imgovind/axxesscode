﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('List Patients | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','listpatients');</script>")%>
<% using (Html.BeginForm("Patients", "Export", FormMethod.Post)) { %>
<div class="wrapper">
    <%= Html.Telerik().Grid<Patient>().Name("List_Patient_Grid").ToolBar(commnds => commnds.Custom()).Columns(columns => {
            columns.Bound(p => p.PatientIdNumber).Title("Id").Width(90);
            columns.Bound(p => p.DisplayName).Title("Name").Width(120);
            columns.Bound(p => p.AddressFull).Title("Address").Sortable(false);
            columns.Bound(p => p.DOBFormatted).Title("Date of Birth").Width(100).Sortable(false);
            columns.Bound(p => p.Gender).Width(80).Sortable(false);
            columns.Bound(p => p.PhoneHomeFormatted).Title("Phone").Width(120).Sortable(false);
            columns.Bound(p => p.StatusName).Title("Status").Width(80).Sortable(false);
            columns.Bound(p => p.Id).Width(90).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPatient('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.Delete('<#=Id#>');\" class=\"deletePatient\">Delete</a>").Title("Action").Width(180);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Patient")).ClientEvents(events => events.OnDataBound("Patient.BindGridButton")).Pageable(paging => paging.PageSize(15)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
    $("#List_Patient_Grid .t-grid-toolbar").html(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewPatient(); return false;%22%3ENew Patient%3C/a%3E%3C/li%3E%3C/ul%3E%3Cul class=%22float_right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExcel Export%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
    $(".t-grid-content").css({ 'height': 'auto' });
</script>