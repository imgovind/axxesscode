﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div id="patientInfo" class="hidden">
<fieldset>
    <div class="wide_column">
        <div class="row"><label for="Patient_Info_Id" class="float_left">Id:</label><div class="float_right"><%= Model.PatientIdNumber %></div></div>
        <div class="row"><label for="Patient_Info_Name" class="float_left">Name:</label><div class="float_right"><%= Model.DisplayName %></div></div>
        <div class="row"><label for="Patient_Info_Gender" class="float_left">Gender:</label><div class="float_right"><%= Model.Gender %></div></div>
        <div class="row"><label for="Patient_Info_Address1" class="float_left">Address:</label><div class="float_right"><%= Model.AddressFirstRow.ToTitleCase() %></div></div>
        <div class="row"><label for="Patient_Info_Address2" class="float_left">City, State, Zip:</label><div class="float_right"><%= Model.AddressSecondRow.ToTitleCase() %></div></div>
        <div class="row"><label for="Patient_Info_HomePhone" class="float_left">Home Phone:</label><div class="float_right"><%= Model.PhoneHome.ToPhone() %></div></div>
        <div class="row"><label for="Patient_Info_AltPhone" class="float_left">Mobile Phone:</label><div class="float_right"><%= Model.PhoneMobile.ToPhone() %></div></div>
        <div class="row"><label for="Patient_Info_SocDate" class="float_left">Start of Care Date:</label><div class="float_right"><%= Model.StartOfCareDateFormatted %></div></div>
        <div class="row"><label for="Patient_Info_DOB" class="float_left">Date of Birth:</label><div class="float_right"><%= Model.DOBFormatted %></div></div>
        <div class="row"><label for="Patient_Info_Medicare" class="float_left">Medicare #:</label><div class="float_right"><%= Model.MedicareNumber %></div></div>
        <div class="row"><label for="Patient_Info_Medicaid" class="float_left">Medicaid #:</label><div class="float_right"><%= Model.MedicaidNumber %></div></div>
        <% if (Model.PhysicianContacts != null && Model.PhysicianContacts.Count > 0)
           { %>
        <div class="row"><label for="Patient_Info_Physician" class="float_left">Physician Name:</label><div class="float_right"><%= Model.PhysicianContacts[0].DisplayName%></div></div>
        <div class="row"><label for="Patient_Info_PhysicianPhone" class="float_left">Physician Phone:</label><div class="float_right"><%= Model.PhysicianContacts[0].PhoneWork.ToPhone()%></div></div>
        <% } %>
        <% if (Model.EmergencyContacts != null && Model.EmergencyContacts.Count > 0)
           { %>
        <div class="row"><label for="Patient_Info_EmergencyContact" class="float_left">Emergency Name:</label><div class="float_right"><%= Model.EmergencyContacts[0].DisplayName %></div></div>
        <div class="row"><label for="Patient_Info_EmergencyContactPhone" class="float_left">Emergency Phone:</label><div class="float_right"><%= Model.EmergencyContacts[0].PrimaryPhone.ToPhone() %></div></div>
        <% } %>
    </div>
</fieldset>
</div>
