﻿<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style type="text/css" media="screen">
		p {
			margin: 0 0 10px 0;
			font-family: arial, sans-serif;
			font-size: 12px;
		}

		body, div, td, th, textarea, input, h2, h3 {
			font-family: arial, sans-serif;
			font-size: 11px;
		}
	</style>
	<body style="font-family: arial, sans-serif; font-size: 12px;">
		<p>Hi,</p>
		<p>You requested a password change.</p>
		<p>To complete your password change request, go to:<br />
		<a href='http://agencycore.axxessweb.com/Login'>http://agencycore.axxessweb.com/Login</a>
		</p>
		<p>Use this temporary password to login:<br />
		Password:  <%=password%>
		</p>
		<p>You will be required to change your password after your first login. Please choose a password you can easily remember.</p>
		<p>Thanks,<br />
		The Axxess&trade; team</p>
		<p>This is an automated e-mail, please do not reply.</p>
	</body>
</html>