var axxess_window = (function($) {
    return {
        buildwindow: function(id, icon, windowname, status, width, height, resize, center) {
            if (center != true) center = false;
            if (resize != false) resize = true;
            $('#desktop').append(unescape("%3Cdiv id=%22window_" + id + "%22 class=%22abs stack window" + (resize ? "" : " no-resize") + "%22" +
                ((parseInt(width) > 0 && parseInt(height) > 0) ? " style=%22width:" + parseInt(width) + (width.indexOf("%") > 0 ? "%" : "px") +
                ";height:" + parseInt(height) + (height.indexOf("%") > 0 ? "%" : "px") + ";" + (center ? ("top:" + (height.indexOf("%") > 0 ?
                50 - (parseInt(height) / 2) + "%" : (parseInt($("#desktop").attr('clientHeight')) / 2) - (parseInt(height) / 2) + "px") + ";l" +
                "eft:" + (width.indexOf("%") > 0 ? 50 - (parseInt(width) / 2) + "%" : (parseInt($("#desktop").attr('clientWidth')) / 2) -
                (parseInt(width) / 2) + "px") + ";") : "") + "%22" : "") + "%3E%3Cdiv class=%22abs window_inner%22%3E%3Cdiv class=%22window_t" +
                "op%22%3E%3Cspan class=%22float_left%22%3E%3Cimg src=%22/images/icons/" + icon + "%22 alt=%22Icon%22 /%3E%3C/span%3E%3Cspan c" +
                "lass=%22abs%22%3E" + windowname + "%3C/span%3E%3Cspan class=%22float_right%22%3E%3Ca href=%22javascript:axxess_desktop.minim" +
                "ize('" + id + "');%22 class=%22window_min%22%3E%3C/a%3E%3Ca href=%22javascript:axxess_desktop.resize('" + id + "');%22 class" +
                "=%22window_resize%22%3E%3C/a%3E%3Ca href=%22javascript:axxess_desktop.close('" + id + "');%22 class=%22window_close%22%3E%3C" +
                "/a%3E%3C/span%3E%3C/div%3E%3Cdiv id=%22window_" + id + "_content%22 class=%22abs window_content loading%22%3E%3C/div%3E%3Cdi" +
                "v class=%22abs window_bottom%22%3E" + status + "%3C/div%3E%3C/div%3E%3C/div%3E"));
        },
        buildtask: function(id, icon, taskname) {
            $('#task').append(unescape("%3Cli id=%22task_" + id + "%22%3E%3Ca href=%22javascript:axxess_desktop.taskbar('" + id + "');%22 tit" +
                "le=%22" + taskname + "%22%3E%3Cimg src=%22/images/icons/" + icon + "%22 alt=%22Icon%22 /%3E" + taskname + "%3C/a%3E"));
        },
        buildmenu: function(id, menu, menuname, url, onCallBack) {
            menu.append(unescape("%3Cli%3E%3Ca href=%22javascript:axxess_desktop.open('" + id + "', '" + url + "', " + onCallBack + ");%22%3E" +
                menuname + "%3C/a%3E%3C/li%3E"));
        },
        init: function(id, windowname, url, onCallBack, menu, menuname, status, width, height, resize, center, taskname, icon) {
            if (width == undefined) width = "-1";
            if (height == undefined) height = "-1";
            if (resize == undefined) resize = true;
            if (center == undefined) center = false;
            if (menu == undefined) menu = "mainmenu";
            if (status == undefined) status = windowname;
            menu = $("#" + menu);
            if (icon == undefined) icon = "axxess_icon.png";
            if (taskname == undefined) taskname = windowname;
            if (menuname == undefined) menuname = windowname;
            this.buildwindow(id, icon, windowname, status, width, height, resize, center);
            this.buildtask(id, icon, taskname);
            if (menu) this.buildmenu(id, menu, menuname, url, onCallBack);
        },
        loadWindow: function(id, windowname, url, onCallBack, menu, menuname, status, taskname, icon) {
            if ($(id).length == 0) {
                this.init(id, windowname, url, onCallBack, menu, menuname, status, taskname, icon);
            } else {

            }
        },
        appendToTitle: function(title, obj) {
            var id = axxess_desktop.getid(obj);
            $('#window_' + id + ' .window_top span.abs').append(title);
            $('#task_' + id + ' a').append(title);
            $('#task_' + id + ' a').attr('title', $('#task_' + id + ' a').attr('title') + title);
        },
        prependToTitle: function(title, obj) {
            var id = axxess_desktop.getid(obj);
            $('#window_' + id + ' .window_top span.abs').prepend(title);
            $('#task_' + id + ' img').after(title);
            $('#task_' + id + ' a').attr('title', title + $('#task_' + id + ' a').attr('title'));
        },
        newTitle: function(title, obj) {
            var id = axxess_desktop.getid(obj);
            $('#window_' + id + ' .window_top span.abs').html(title);
            $('#task_' + id + ' a').html($('#task_' + id + ' a img'));
            $('#task_' + id + ' img').after(title);
            $('#task_' + id + ' a').attr('title', title);
        }
    };
})(jQuery);
