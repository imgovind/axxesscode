﻿var Payroll = {
    InitSearch: function() {
        $("#searchPayrollForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'html',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        $('#payrollSearchResult').html(result);
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    LoadDetail: function(userId) {
        var startDate = new Date($("#startDate").data("tDatePicker").value());
        var endDate = new Date($("#endDate").data("tDatePicker").value());
        startDate = (startDate.getMonth() + 1) + "/" + startDate.getDate() + "/" + startDate.getFullYear();
        endDate = (endDate.getMonth() + 1) + "/" + endDate.getDate() + "/" + endDate.getFullYear();

        $('#payrollSearchResult').load('Payroll/Detail', { userId: userId, startDate: startDate, endDate: endDate }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#payrollSearchResult').html('<p>There was an error making the AJAX request</p>');
            }
        });
    },
    LoadDetails: function() {
        var startDate = new Date($("#startDate").data("tDatePicker").value());
        var endDate = new Date($("#endDate").data("tDatePicker").value());
        startDate = (startDate.getMonth() + 1) + "/" + startDate.getDate() + "/" + startDate.getFullYear();
        endDate = (endDate.getMonth() + 1) + "/" + endDate.getDate() + "/" + endDate.getFullYear();

        $('#payrollSearchResult').load('Payroll/Details', { startDate: startDate, endDate: endDate }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#payrollSearchResult').html('<p>There was an error making the AJAX request</p>');
            }
        });
    }
}
