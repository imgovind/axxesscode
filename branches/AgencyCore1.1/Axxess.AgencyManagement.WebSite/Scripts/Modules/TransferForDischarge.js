﻿var TransferForDischarge = {
    _TransferForDischargeId: "",
    _patientId: "",
    _EpisodeId: "",
    GetId: function() {
        return TransferForDischarge._patientId;
    },
    SetId: function(patientId) {
        TransferForDischarge._patientId = patientId;
    },
    GetTransferForDischargeId: function() {
        return TransferForDischarge._TransferForDischargeId;
    },
    SetTransferForDischargeId: function(TransferForDischargeId) {
        TransferForDischarge._TransferForDischargeId = TransferForDischargeId;
    },
    GetSOCEpisodeId: function() {
        return TransferForDischarge._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        TransferForDischarge._EpisodeId = EpisodeId;
    },
    InitNew: function() {

        $("input[name=TransferInPatientDischarged_M1040InfluenzaVaccine]").click(function() {
            if ($(this).val() == "01" || $(this).val() == "NA") {

                $("#transfer_M1045").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M1045InfluenzaVaccineNotReceivedReason ]").attr('checked', false);

            }
            else if ($(this).val() == "00") {
                $("#transfer_M1045").unblock();
            }
        });


        $("input[name=TransferInPatientDischarged_M1050PneumococcalVaccine]").click(function() {
            if ($(this).val() == 1) {

                $("#transfer_M1055").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M1055PPVNotReceivedReason ]").attr('checked', false);

            }
            else if ($(this).val() == 0) {
                $("#transfer_M1055").unblock();
            }
        });

        $("input[name=TransferInPatientDischarged_M1500HeartFailureSymptons]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02" || $(this).val() == "NA") {

                $("#transfer_M1510").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M1510HeartFailureFollowup ]").attr('checked', false);

            }
            else if ($(this).val() == "01") {
                $("#transfer_M1510").unblock();
            }
        });

        $("input[name=TransferInPatientDischarged_M2300EmergentCare]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "UK") {

                $("#transfer_M2310").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M2310ReasonForEmergentCare ]").attr('checked', false);

            }
            else if ($(this).val() == "01" || $(this).val() == "02") {
                $("#transfer_M2310").unblock();
            }
        });

        $("input[name=TransferInPatientDischarged_M2410TypeOfInpatientFacility]").click(function() {
            if ($(this).val() == 1) {
                $("#transfer_M2440").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#transfer_M2430").unblock();
                $("input[name=TransferInPatientDischarged_M2440ReasonPatientAdmitted]").attr('checked', false);
            }
            else if ($(this).val() == "02" || $(this).val() == "04") {
                $("#transfer_M2440").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#transfer_M2430").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M2430ReasonForHospitalization]").attr('checked', false);
                $("input[name=TransferInPatientDischarged_M2440ReasonPatientAdmitted]").attr('checked', false);

            }
            else if ($(this).val() == "03") {
                $("#transfer_M2440").unblock();
                $("#transfer_M2430").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=TransferInPatientDischarged_M2430ReasonForHospitalization]").attr('checked', false);

            }

        });

    }
    ,
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var actionType = control.html();
                    if (actionType == "Save/Continue") {
                        $("input[name='TransferInPatientDischarged_Id']").val(resultObject.Assessment.Id);
                        $("input[name='TransferInPatientDischarged_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='TransferInPatientDischarged_Action']").val('Edit');
                        Oasis.NextTab("#editTransferInPatientDischargedTabs.tabs");
                    }
                    else if (actionType == "Save/Exit") {
                        Oasis.Close(control);
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save") {
                        $("input[name='TransferInPatientDischarged_Id']").val(resultObject.Assessment.Id);
                        $("input[name='TransferInPatientDischarged_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='TransferInPatientDischarged_Action']").val('Edit');
                    }
                }
                else {
                    alert(resultObject.errorMessage);
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control, formType) {
        var form = control.closest("form");
        form.validate();
        TransferForDischarge.HandlerHelper(form, control);
    },
    Validate: function() {
        OasisValidation.Validate(TransferForDischarge._TransferForDischargeId, "TransferInPatientDischarged");
    }
    ,
    loadTransferForDischarge: function(id, patientId, assessmentType) {
        acore.open("transferfordischarge", 'Oasis/TransferForDischarge', function() {
            $("#transferInPatientDischargedTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
            $("#transferInPatientDischargedTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
            TransferForDischarge.InitNew();
            $("#transferInPatientDischargedTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                TransferForDischarge.loadTransferForDischargeParts(event, ui);
                TransferForDischarge.InitNew();
            });
        }, { Id: id, PatientId: patientId, AssessmentType: assessmentType });
    },
    loadTransferForDischargeParts: function(event, ui) {

        $($(ui.tab).attr('href')).empty();
        $($(ui.tab).attr('href')).addClass("loading");
        $($(ui.tab).attr('href')).load('Oasis/TransferForDischargeCategory', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
                $($(ui.tab).attr('href')).removeClass("loading");
            }
        });
    }

}