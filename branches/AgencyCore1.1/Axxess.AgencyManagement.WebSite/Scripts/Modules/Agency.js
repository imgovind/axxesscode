﻿var Agency = {
    InitNew: function() {
        Lookup.loadStates();

        $('#New_Agency_Phone1').autotab({ target: 'New_Agency_Phone2', format: 'numeric' });
        $('#New_Agency_Phone2').autotab({ target: 'New_Agency_Phone3', format: 'numeric', previous: 'New_Agency_Phone1' });

        $('#New_Agency_ContactPhone1').autotab({ target: 'New_Agency_ContactPhone2', format: 'numeric' });
        $('#New_Agency_ContactPhone2').autotab({ target: 'New_Agency_ContactPhone3', format: 'numeric', previous: 'New_Agency_ContactPhone1' });
        $('#New_Agency_ContactPhone3').autotab({ target: 'New_Agency_ContactPersonFirstName', format: 'numeric', previous: 'New_Agency_ContactPhone2' });

        $(".numeric").numeric();
        $(".names").alpha({ nocaps: false });

        U.postUrl("/LookUp/NewShortGuid", null, function(data) {
            $("#New_Agency_AdminPassword").val(data.text);
        });

        $("#New_Agency_GeneratePassword").click(function() {
            U.postUrl("/LookUp/NewShortGuid", null, function(data) {
                $("#New_Agency_AdminPassword").val(data.text);
            });
        });

        $("input[name=New_Agency_SameAsAdmin]").click(function() {
            if ($(this).is(':checked')) {
                $("#New_Agency_ContactPersonEmail").val($("#New_Agency_AdminUsername").val());
                $("#New_Agency_ContactPersonFirstName").val($("#New_Agency_AdminFirstName").val());
                $("#New_Agency_ContactPersonLastName").val($("#New_Agency_AdminLastName").val());
            }
            else {
                $("#New_Agency_ContactPersonEmail").val('');
                $("#New_Agency_ContactPersonFirstName").val('');
                $("#New_Agency_ContactPersonLastName").val('');
            }
        });

        $("#newAgencyForm").validate({
            messages: {
                AgencyAdminUsername: "",
                AgencyAdminPassword: "",
                AgencyAdminFirstName: "",
                AgencyAdminLastName: "",
                Name: "",
                TaxId: "",
                AddressLine1: "",
                AddressCity: "",
                AddressZipCode: "",
                AddressStateCode: "",
                PhoneArray: "",
                ContactPersonEmail: "",
                ContactPersonFirstName: "",
                ContactPersonLastName: "",
                ContactPhoneArray: ""
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("New agency successfully added.", { theme: 'success', life: 5000 });
                            acore.close("newagency");
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEdit: function() {

    },
    loadCaseManagement: function(groupName) {
        $("#caseManagementContentId").empty();
        $("#caseManagementContentId").addClass("loading");
        $("#caseManagementContentId").load('Agency/CaseManagementContent', { groupName: groupName }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $("#caseManagementContentId");
            }
            else if (textStatus == "success") {
                $("#caseManagementContentId").removeClass("loading");
            }
        });
    },
    loadVisitRateContent: function(branchId) {
        $("#Edit_VisitRate_Container").empty();
        $("#Edit_VisitRate_Container").addClass("loading");
        $("#Edit_VisitRate_Container").load('Agency/VisitRateContent', { branchId: branchId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $("#Edit_VisitRate_Container");
            }
            else if (textStatus == "success") {
                $("#Edit_VisitRate_Container").removeClass("loading");
            }
        });
    },
    RebindCaseManagement: function() {
        var grid = $('#caseManagementGrid').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    },
    InitEditCost: function() {
        $("#editVisitCostForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            UserInterface.CloseWindow('visitrates');
                            $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    MarkOrdersAsSent: function() {
        var fields = $("input.OrdersToBeSent:checked").serializeArray();
        U.postUrl("/Agency/MarkOrdersAsSent", fields, function(data) {
            if (data.isSuccessful) {
                Agency.RebindOrders();
                Patient.Rebind();
                Schedule.Rebind();
            } else $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 });
        });
    },
    MarkOrdersAsReturned: function() {
        var fields = $("input.OrdersPendingSignature:checked").serializeArray();
        U.postUrl("/Agency/MarkOrdersAsReturned", fields, function(data) {
            if (data.isSuccessful) {
                Agency.RebindOrders();
                Patient.Rebind();
                Schedule.Rebind();
            } else $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 });
        });
    },
    RebindOrders: function() {
        var ordersToBeSent = $('#List_OrdersToBeSent').data('tGrid');
        if (ordersToBeSent != null) {
            ordersToBeSent.rebind();
        }
        var ordersPending = $('#List_OrdersPendingSignature').data('tGrid');
        if (ordersPending != null) {
            ordersPending.rebind();
        }
    },
    onEditPendingOrder: function(e) {
        alert('hello');
    }
}