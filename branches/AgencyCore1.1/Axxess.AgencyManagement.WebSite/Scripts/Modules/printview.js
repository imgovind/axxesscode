﻿var printview = (function($) {
    return {
        firstheader: "",
        header: "",
        footer: "",
        cssclass: "",
        addsection: function(content, title) {
            if ($('.page').length < 1) $('body').html(unescape("%3Cdiv class=%22page " + printview.cssclass + "%22%3E%3Cdiv class=%22header%22%3E" +
                (printview.firstheader == "" ? printview.header : printview.firstheader) + printview.footer + "%3C/div%3E%3C/div%3E"));
            $('.footer:last').remove();
            if (title == undefined) content = "%3Cdiv class=%22content%22%3E" + content + "%3C/div%3E";
            else content = "%3Ch3%3E" + title + "%3C/h3%3E%3Cdiv class=%22content%22%3E" + content + "%3C/div%3E";
            $('.page:last').append(unescape(content + "%3Cdiv class=%22footer%22%3E" + printview.footer + "%3C/div%3E"));
            if ($('.page:last').attr('scrollHeight') > $('.page:last').attr('offsetHeight')) {
                if (title != undefined) $('h3:last').remove();
                $('.content:last').remove();
                $('body').append(unescape("%3Cdiv class=%22page%22%3E%3Cdiv class=%22header%22%3E" + printview.header + "%3C/div%3E" + content + "%3Cdiv class=%2" +
                    "2footer%22%3E" + printview.footer + "%3C/div%3E%3C/div%3E"));
            }
        },
        col: function(num, content, strong, height) {
            switch (num) {
                case 1: css = ""; break;
                case 2: css = "bicol "; break;
                case 3: css = "tricol "; break;
                case 4: css = "quadcol "; break;
                case 5: css = "pentcol "; break;
                case 6: css = "hexcol "; break;
            }
            if (content == "" && height > 0) {
                while (height > 0) {
                    content += "%3Cspan class=%22blank_line%22%3E%3C/span%3E";
                    height--;
                }
            }
            return "%3Cspan class=%22" + css + "%22%3E" + (strong ? "%3Cstrong%3E" : "") + content + (strong ? "%3C/strong%3E" : "") + "%3C/span%3E";
        },
        checkbox: function(label, checked, strong) {
            return "%3Cspan class=%22checklabel%22%3E%3Cspan class=%22checkbox%22%3E" + (checked ? "&#x2713;" : "") + "%3C/span%3E" + (strong ? "%3Cstrong%3E" :
                "") + label + (strong ? "%3C/strong%3E" : "") + "%3C/span%3E";
        },
        span: function(content, strong, height) {
            if (content == undefined) content = "";
            return printview.col(1, content, strong, height);
        }
    };
})(jQuery);