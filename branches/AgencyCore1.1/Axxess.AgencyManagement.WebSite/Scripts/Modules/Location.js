﻿var Location = {
    InitNew: function() {
        $("#newLocationForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Agency.RebindLocation();
                            $.jGrowl("New Location successfully added.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('newlocation');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEdit: function() {
        $("#editLocationForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Location.RebindList();
                            $.jGrowl("Location successfully updated.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('editlocation');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Delete: function(Id) {
        if (confirm("Are you sure you want to delete this location/branch?")) {
            var input = "Id=" + Id;
            U.postUrl("/Location/Delete", { Id: Id }, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    if (resultObject.isSuccessful) {
                        Location.RebindList();
                        $.jGrowl("Branch/Location successfully deleted.", { theme: 'success', life: 5000 });

                    } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                }
            });
        }
    },
    RebindList: function() {
        var grid = $('#List_Location').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    }
}