﻿var DeathAtHome = {
    _DeathId: "",
    _patientId: "",
    _EpisodeId: "",
    GetId: function() {
        return DeathAtHome._patientId;
    },
    SetId: function(patientId) {
        DeathAtHome._patientId = patientId;
    },
    GetDeathId: function() {
        return DeathAtHome._DeathId;
    },
    SetDeathId: function(DeathId) {
        DeathAtHome._DeathId = DeathId;
    },
    GetSOCEpisodeId: function() {
        return DeathAtHome._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        DeathAtHome._EpisodeId = EpisodeId;
    },
    InitNew: function() {
    },
    DeathAtHomeHelper: function(form, control, action) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {

                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    var category = $('[name=categoryType]').val();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#deathTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('deathathome');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save &amp; Check for Errors") {
                        action();
                    }
                    else if (actionType == "Check for Errors") {
                        action();
                    }
                }
                else {
                    $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                }
            },
            error: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    var category = $('[name=categoryType]').val();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#deathTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('deathathome');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save &amp; Check for Errors") {
                        action();
                    }
                    else if (actionType == "Check for Errors") {
                        action();
                    }
                }
                else {
                    $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control, action) {
        var form = control.closest("form");
        form.validate();
        DeathAtHome.DeathAtHomeHelper(form, control, action);
    },
    Validate: function() {
        OasisValidation.Validate(DeathAtHome._DeathId, "DischargeFromAgencyDeath");
    },
    loadDeathAtHome: function(id, patientId, assessmentType) {
        acore.open("deathathome", 'Oasis/Death', function() {
            $("#deathTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
            $("#deathTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
            DeathAtHome.InitNew();
            $("#deathTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                DeathAtHome.loadDeathAtHomeParts(event, ui);
                DeathAtHome.InitNew();
            });
        }, { Id: id, PatientId: patientId, AssessmentType: assessmentType });
    },
    loadDeathAtHomeParts: function(event, ui) {
        $($(ui.tab).attr('href')).empty();
        $($(ui.tab).attr('href')).addClass("loading");
        $($(ui.tab).attr('href')).load('Oasis/DeathCategory', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
                $($(ui.tab).attr('href')).removeClass("loading");
            }
        });
    }
}