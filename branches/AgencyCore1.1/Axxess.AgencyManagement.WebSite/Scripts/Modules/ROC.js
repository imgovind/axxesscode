﻿var ROC = {
    _ROCId: "",
    _patientId: "",
    _EpisodeId: "",
    GetId: function() {
        return ROC._patientId;
    },
    SetId: function(patientId) {
        ROC._patientId = patientId;
    },
    GetROCId: function() {
        return ROC._ROCId;
    },
    SetROCId: function(ROCId) {
        ROC._ROCId = ROCId;
    },
    GetSOCEpisodeId: function() {
        return ROC._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        ROC._EpisodeId = EpisodeId;
    },
    InitNew: function() {

    },
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#rocTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('resumptionofcare');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save") {
                    }
                }
                else {
                   $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                                    }
            },
            error: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#rocTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('resumptionofcare');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save") {
                    }
                }
                else {
                    $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control) {
        var form = control.closest("form");
        form.find('.form-omitted :input').val("").end().find('.form-omitted:input').val("");
        form.validate();
        ROC.HandlerHelper(form, control);

    },
    Validate: function(id) {
        OasisValidation.Validate(id, "ResumptionOfCare");
    },
    loadRoc: function(id, patientId, assessmentType) {
        acore.open("resumptionofcare", 'Oasis/Roc', function() {
            $("#rocTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
            $("#rocTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
            ROC.InitNew(); Oasis.Init();
            $("#rocTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                ROC.loadROCParts(event, ui);
            });
        }, { Id: id, PatientId: patientId, AssessmentType: assessmentType });
    },
    loadROCParts: function(event, ui) {
        $($(ui.tab).attr('href')).empty();
        $($(ui.tab).attr('href')).addClass("loading");
        $($(ui.tab).attr('href')).load('Oasis/RocCategory', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
                $($(ui.tab).attr('href')).removeClass("loading");
                Oasis.CalculateNutritionScore('ResumptionOfCare');
                ROC.InitNew();
                Oasis.Init();
            }
        });
    }

}