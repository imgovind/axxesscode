var axxess_desktop = (function($) {
    return {
        // Static Variables for cascading windows
        csstop: 1, cssleft: 1, openwindows: 0, maxwindows: 10,
        // Clear active states, hide menus.
        clear_active: function() { $('a.active, tr.active').removeClass('active'); $('ul.menu').hide(); },
        // Zero out window z-index.
        window_flat: function() { $('div.window').removeClass('window_stack'); },
        // Resize window.
        resize: function(id) {
            var win = axxess_desktop.getwin(id);
            if (win.hasClass('window_full')) axxess_desktop.restore(id);
            else axxess_desktop.maximize(id);
            axxess_desktop.window_flat();
            win.addClass('window_stack');
        },
        // Get ID by Window Object
        getid: function(obj) { return obj.attr('id').substring(obj.attr('id').lastIndexOf("_") + 1, obj.attr('id').length); },
        // Get Window Object by ID
        getwin: function(id) { return $('#window_' + id); },
        // Get Window Content Div by ID
        getcontentdiv: function(id) { return $('#window_' + id + '_content'); },
        // Get Task Bar Object by ID
        gettask: function(id) { return $('#task_' + id); },
        // Get Window Data via AJAX
        getdata: function(id, url, onSuccess, input) {
            var contentdiv = axxess_desktop.getcontentdiv(id);
            contentdiv.load(url, input, function(responseText, textStatus, XMLHttpRequest) {
                axxess_desktop.getcontentdiv(id).removeClass("loading");
                if (textStatus == 'error') {
                    contentdiv.html('<div class="ajaxerror"><img src="/Images/icons/error.png" /><h1>There was an error loading this window.</h1><br />Please exit out and try again. If this problem persists, contact Axxess for further assistance.</div>');
                }
                else if (textStatus == "success") {
                    if (onSuccess != null && typeof (onSuccess) == 'function') {
                        onSuccess();
                    }
                }
            });
        },
        // Minimize
        minimize: function(id) { axxess_desktop.getwin(id).hide(); },
        // Restore from maximized
        restore: function(id) {
            var win = axxess_desktop.getwin(id);
            win.removeClass('window_full').css({
                'top': win.attr('data-t'), 'left': win.attr('data-l'), 'right': win.attr('data-r'), 'bottom': win.attr('data-b'),
                'width': win.attr('data-w'), 'height': win.attr('data-h')
            });
        },
        // Maximize window
        maximize: function(id) {
            var win = axxess_desktop.getwin(id);
            win.attr({ 'data-t': win.css('top'), 'data-l': win.css('left'), 'data-r': win.css('right'), 'data-b': win.css('bottom'),
                'data-w': win.css('width'), 'data-h': win.css('height')
            }).addClass('window_full').css({ 'top': '0', 'left': '0', 'right': '0', 'bottom': '0', 'width': '100%', 'height': '100%' });
        },
        // Open window
        open: function(id, url, onSuccess, input) {
            if (axxess_desktop.openwindows < axxess_desktop.maxwindows) {
                var task = axxess_desktop.gettask(id);
                var win = axxess_desktop.getwin(id);
                if (task.is(':hidden')) {
                    axxess_desktop.getdata(id, url, onSuccess, input);
                    task.show();
                    var height = parseInt($('#desktop').attr('clientHeight') * .85);
                    var width = parseInt($('#desktop').attr('clientWidth') * .85);
                    if (!(parseInt(win.css('width')) > 0 && parseInt(win.css('height')) > 0)) {
                        if (height < 555) height = 555;
                        if (width < 1000) width = 1000;
                        win.css({ 'height': (height.toString() + "px"), 'width': (width.toString() + "px") });
                        if (parseInt(win.css('height')) + (++axxess_desktop.csstop * 25) > $('#desktop').attr('clientHeight'))
                            axxess_desktop.csstop = axxess_desktop.cssleft = 0;
                        if (parseInt(win.css('width')) + (++axxess_desktop.cssleft * 25) > $('#desktop').attr('clientWidth'))
                            axxess_desktop.csstop = axxess_desktop.cssleft = 0;
                    }
                    if (!(parseInt(win.css('top')) > 0 || parseInt(win.css('left')) > 0) && !(win.hasClass('window_full'))) win.css({ 'top': (axxess_desktop.csstop * 25).toString() + "px", 'left': (axxess_desktop.cssleft * 25).toString() + "px" });
                    axxess_desktop.openwindows++;
                }
                axxess_desktop.window_flat();
                win.addClass('window_stack').show();
                axxess_desktop.clear_active();
                //alert(axxess_desktop.openwindows);
            } else {
                axxess_desktop.clear_active();
                $.jGrowl("You can only have " + axxess_desktop.maxwindows + " concurrent windows at once. Please close down some of your windows to proceed", { theme: 'error', life: 5000 });
            }
        },
        openModal: function(id, url, onSuccess, input, height, width) {
            var task = axxess_desktop.gettask(id);
            var win = axxess_desktop.getwin(id);
            if (task.is(':hidden')) {
                axxess_desktop.getdata(id, url, onSuccess, input);
                task.show();
                var height = height;
                var width = width;

                win.css({ 'height': (height.toString() + "px"), 'width': (width.toString() + "px") });
                if (parseInt(win.css('height')) + (++axxess_desktop.csstop * 25) > $('#desktop').attr('clientHeight'))
                    axxess_desktop.csstop = axxess_desktop.cssleft = 0;
                if (parseInt(win.css('width')) + (++axxess_desktop.cssleft * 25) > $('#desktop').attr('clientWidth'))
                    axxess_desktop.csstop = axxess_desktop.cssleft = 0;
                win.css({ 'top': (axxess_desktop.csstop * 25).toString() + "px", 'left': (axxess_desktop.cssleft * 25).toString() + "px" });
            }
            axxess_desktop.window_flat();
            win.addClass('window_stack').show();
            axxess_desktop.clear_active();
        },
        openPrint: function(url, controls, editClick, approveClick) {
            $('#shade').show();
            $('#printbox').show();
            $('#printcontrols').show();
            $('#printview').attr('src', url).ready(function() { $(this).removeClass('loading'); })
            if (controls) $('#printcontrols .optional').each(function() { $(this).show(); });
            $('#printedit').bind('click', editClick);
            $('#printapprove').bind('click', approveClick);
        },
        print: function() {
            window.frames['printview'].focus();
            window.frames['printview'].print();
        },
        closePrint: function() {
            $('#shade').hide();
            $('#printbox').hide();
            $('#printcontrols').hide();
            $('#printcontrols .optional').each(function() { $(this).hide(); });
            $('#printedit').unbind('click');
            $('#printapprove').unbind('click');
            $('#printview').attr('src', '').addClass('loading');
        },
        close: function(id) {
            axxess_desktop.getwin(id).hide();
            axxess_desktop.gettask(id).hide();
            axxess_desktop.getcontentdiv(id).addClass("loading").empty();
            axxess_desktop.openwindows--;
            //alert(axxess_desktop.openwindows);
        },
        // Add Menu
        addmenu: function(name, id, submenu, icon) {
            if (icon != undefined) icon = "%3Cimg src=%22/Images/icons/" + icon + "%22 alt=%22Menu Icon%22 /%3E";
            else icon = "";
            if (id == undefined) id = name;
            if (submenu == undefined) submenu = "mainmenu";
            $("#" + submenu).append(unescape("%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22menu_trigger%22%3E" + icon + name +
                "%3C/a%3E%3Cul id=%22" + id + "%22 class=%22menu%22%3E%3C/ul%3E%3C/li%3E"));
        },
        addmenulink: function(name, href, submenu) {
            if (submenu == undefined) submenu = "mainmenu";
            $("#" + submenu).append(unescape("%3Cli%3E%3Ca href=%22" + href + "%22 target=%22_blank%22 %3E" + name + "%3C/a%3E%3C/li%3E"));
        },
        // Taskbar function.
        taskbar: function(id) {
            var win = axxess_desktop.getwin(id);
            if (win.hasClass('window_stack')) {
                axxess_desktop.window_flat();
                win.hide();
            } else {
                axxess_desktop.window_flat();
                win.show().addClass('window_stack');
            }
        },
        // Initialize the desktop.
        init: function() {
            $('.window_content').scroll(function() {
                $(".t-datepicker").each(function() { $(this).data("tDatePicker").hidePopup(); });
            });
            if (window.location !== window.top.location) window.top.location = window.location;
            // Various Window UI controls.
            $('div.window').mousedown(function() {
                axxess_desktop.window_flat();
                $(this).addClass('window_stack');
            }).draggable({ containment: 'parent', handle: 'div.window_top .abs'
            }).resizable({ containment: 'parent', handles: 'n, ne, e, se, s, sw, w, nw', minWidth: 1000, minHeight: 555
            }).find('div.window_top').dblclick(function() {
                axxess_desktop.resize(axxess_desktop.getid($(this).closest('div.window')));
            }).find('img').dblclick(function() {
                $($(this).closest('div.window_top').find('a.window_close').attr('href')).hide('fast');
                $(this).closest('div.window').hide();
                return false;
            });
            $('.no-resize').resizable("option", "disabled", true).removeClass('ui-state-disabled');
            $('#mainmenu ul a.menu_trigger').append(unescape("%3Cimg src=%22/Images/gui/more.png%22 alt=%22More%22 /%3E"));
        }
    };
})(jQuery);

