﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PasswordReset>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reset Password - Axxess Home Health Management System</title>
    <link type="text/css" href="/Content/account.css" rel="Stylesheet" />
</head>
<body>
    <div id="forgotPassword-wrapper">
        <div class="box-header forgotPassword">
            AXXESS&trade; PASSWORD RESET
        </div>
        <div class="box">
            <div id="messages" class="notification info">
                <span class="strong">Forgot your Password?</span><br />
                Enter your login E-mail and fill the security check.
            </div>
            <div id="formContainer">
                <% using (Html.BeginForm("ForgotPassword", "Account", FormMethod.Post, new { @id = "forgotPasswordForm", @class = "forgotPassword" })) %>
                <% { %>
                <div class="row">
                    <%= Html.LabelFor(m => m.UserName) %>
                    <%= Html.TextBoxFor(m => m.UserName, new { @class = "required" })%>
                </div>
                <div class="row">
                    <%= Html.LabelFor(m => m.captchaValid) %>
                    <span>Enter both words below, separated by a space. </span>
                    <%= Html.GenerateCaptcha() %>
                </div>
                <div class="row tr">
                    <input type="submit" value="Send" class="button" style="width: 90px!important;" />
                    <br />
                </div>
                <% } %>
            </div>
        </div>
    </div>
    <% Html.Telerik().ScriptRegistrar().jQuery(true)
         .DefaultGroup(group => group
             .Add("/Plugins/jquery.validate.js")
             .Add("/Plugins/jquery.form.js")
             .Add("/Plugins/jquery.blockUI.js")
             .Add("/Models/Utility.js")
             .Add("/Models/Account.js")
             .Compress(true))
        .OnDocumentReady(() =>
        { 
    %>
    ResetPassword.Init();
    <% 
        }).Render(); %>
</body>
</html>
