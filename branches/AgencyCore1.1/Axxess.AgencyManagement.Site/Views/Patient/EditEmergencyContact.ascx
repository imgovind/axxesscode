﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEmergencyContact>" %>
<% using (Html.BeginForm("EditEmergencyContact", "Patient", FormMethod.Post, new { @id = "editEmergencyContactForm" }))%>
<%  { %>
<%=Html.Hidden("PatientId", Model.PatientId, new { @id = "txtEdit_EmergencyContactPatientID" })%>
<%=Html.Hidden("Id", Model.Id, new { @id ="txtEdit_EmergencyContactID" })%>
<div id="editEmergencyContactValidaton" class="marginBreak" style="display: none">
</div>
<div class="marginBreak">
    <div class="rowBreak">
        <!--[if !IE]>start section content top<![endif]-->
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="FirstName">
                            First Name:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("FirstName", Model.FirstName, new { @id = "txtEdit_EmergencyContact_FirstName", @class = "text input_wrapper", @maxlength = "100", @tabindex = "33" })%>
                    </div>
                    <div class="row">
                        <label for="LastName">
                            Last Name:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("LastName", Model.LastName, new { @id = "txtEdit_EmergencyContact_LastName", @class = "text input_wrapper", @maxlength = "100", @tabindex = "34" })%>
                    </div>
                    <div class="row">
                        <label for="PhoneHome">
                            Primary Phone:</label>
                        <div class="inputs">
                            <span class="input_wrappermultible">
                                <%=Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length >= 3 ? Model.PrimaryPhone.Substring(0, 3) : "", new { @id = "txtEdit_EmergencyContact_PrimaryPhoneArray1", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length >= 6 ? Model.PrimaryPhone.Substring(3, 3) : "", new { @id = "txtEdit_EmergencyContact_PrimaryPhoneArray2", @class = "autotext digits", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhonePrimaryArray", Model.PrimaryPhone != null && Model.PrimaryPhone != "" && Model.PrimaryPhone.Length >6 ? Model.PrimaryPhone.Substring(6) : "", new { @id = "txtEdit_EmergencyContact_PrimaryPhoneArray3", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "" })%>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="PhoneMobile">
                            Alt Phone:</label>
                        <div class="inputs">
                            <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneAlternateArray", Model.AlternatePhone != null && Model.AlternatePhone != ""&&Model.AlternatePhone.Length>=3 ? Model.AlternatePhone.Substring(0, 3) : "", new { @id = "txtEdit_EmergencyContact_AltPhoneArray1", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneAlternateArray", Model.AlternatePhone != null && Model.AlternatePhone != "" && Model.AlternatePhone.Length >=6 ? Model.AlternatePhone.Substring(3, 3) : "", new { @id = "txtEdit_EmergencyContact_AltPhoneArray2", @class = "autotext digits", @style = "width: 49px; padding: 0px; margin: 0px;", @maxlength = "3", @size = "3", @tabindex = "" })%>
                            </span>- <span class="input_wrappermultible">
                                <%=Html.TextBox("PhoneAlternateArray", Model.AlternatePhone != null && Model.AlternatePhone != "" && Model.AlternatePhone.Length > 6 ? Model.AlternatePhone.Substring(6) : "", new { @id = "txtEdit_EmergencyContact_AltPhoneArray3", @class = "autotext digits", @style = "width: 49.5px; padding: 0px; margin: 0px;", @maxlength = "4", @size = "5", @tabindex = "" })%>
                            </span>
                        </div>
                    </div>
                    <div class="row" style="vertical-align: middle;">
                        <label for="Email">
                            Email:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("EmailAddress", Model.EmailAddress, new { @id = "txtEdit_EmergencyContact_Email", @class = "text email input_wrapper", @maxlength = "100", @tabindex = "41" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="Relationship">
                            Relationship:</label>
                        <div class="inputs">
                            <%=Html.TextBox("Relationship", Model.Relationship, new { @id = "txtEdit_EmergencyContact_Relationship", @class = "text input_wrapper", @tabindex = "42" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine1">
                            Address Line 1:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "txtEdit_EmergencyContact_AddressLine1", @class = "text input_wrapper", @tabindex = "43" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine2">
                            Address Line 2:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "txtEdit_EmergencyContact_AddressLine2", @class = "text input_wrapper", tabindex = "44" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressCity">
                            City:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressCity", Model.AddressCity, new { @id = "txtEdit_EmergencyContact_AddressCity", @class = "text input_wrapper", @tabindex = "45" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressStateCode">
                            State, Zip Code :</label>
                        <div class="inputs">
                             <%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "txtEdit_EmergencyContact_AddressStateCode", @class = "AddressStateCode input_wrapper required selectDropDown", @tabindex = "", @style = "width: 119px;" })%>
                           
                            &nbsp;
                            <%=Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "txtEdit_EmergencyContact_AddressZipCode", @class = "text digits isValidUSZip", @tabindex = "62", @style = "width: 54px; padding: 0px; margin: 0px;", @size = "5", @maxlength = "5" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="SetPrimary">
                            Set Primary:</label>
                        <div class="inputs">
                            <%=Html.CheckBox("IsPrimary",Model.IsPrimary ,new { @id = "txtEdit_EmergencyContact_SetPrimary" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="buttons">
        <ul>
            <li>
                <input name="" type="submit" value="Add" /></li>
            <li>
                <input name="" type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
            <li>
                <input name="" type="reset" value="Reset" /></li>
        </ul>
    </div>
</div>
<%} %>
