﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="report" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left">Report </span><span class="float_right"><a href="javascript:void(0);"
                class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                    href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="arrowlistmenu abs">
            <ul id="navmenu-h">
                <li class="menuheader expandable"><a href="javascript:void(0);">Patient Listings &nbsp;&nbsp;</a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);" onclick="JQD.loadReportPara('patientRegistry');">Patient
                            Register</a></li>
                        <li><a href="javascript:void(0);">Patient Survay Listing </a></li>
                        <li><a href="javascript:void(0);">Patient SOC Cert Period Listing </a></li>
                        <li><a href="javascript:void(0);">Patient On Call Listing</a></li>
                        <li><a href="javascript:void(0);">Patient Address/ Phone Listing</a></li>
                        <li><a href="javascript:void(0);">Patient Birthday Listing</a></li>
                        <li><a href="javascript:void(0);">Patient Country/MSA Listing </a></li>
                        <li><a href="javascript:void(0);">Patient Priority/AOB Listing</a></li>
                        <li><a href="javascript:void(0);">Patient Emergency Contacts</a></li>
                        <li><a href="javascript:void(0);">Patient By Primary Physician Listing </a></li>
                        <li><a href="javascript:void(0);">Incomplete Paperwork Listing </a></li>
                        <li><a href="javascript:void(0);">Hospice Cert Period Listing</a></li>
                        <li><a href="javascript:void(0);">Referral Listing</a></li>
                        <li><a href="javascript:void(0);">Authorizations Listing </a></li>
                        <li><a href="javascript:void(0);">Authorizations Due Tracking Listing </a></li>
                        <li><a href="javascript:void(0);">Responsible Employee Listing</a></li>
                        <li><a href="javascript:void(0);">Patient Duplicate Drug Class Listing</a></li>
                        <li><a href="javascript:void(0);">Patient Discharge Questionnarie</a></li>
                        <li><a href="javascript:void(0);">Patient Precautions Listing </a></li>
                        <li><a href="javascript:void(0);">Patient Priority/AOB Listing</a></li>
                        <li><a href="javascript:void(0);">Patient By Diagnosis Group Listing</a></li>
                    </ul>
                </li>
                <li class="menuheader expandable"><a href="javascript:void(0);" class="m2">Oasis&nbsp;&nbsp;</a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);" onclick="JQD.loadReportPara('OasisAssesList');">Oasis
                            Assessment Listing</a></li>
                        <li><a href="javascript:void(0);">Resumption of Care Assessment Listing</a></li>
                        <li><a href="javascript:void(0);">Oasis Quality Improvement Report</a></li>
                        <li><a href="javascript:void(0);">Oasis Quality Improvement Trend Report</a></li>
                        <li><a href="javascript:void(0);">Oasis Survey Questions Listing</a></li>
                        <li><a href="javascript:void(0);">Blank Oasis Assessments</a></li>
                        <li><a href="javascript:void(0);">Blank Comprehensive Assessments</a></li>
                    </ul>
                </li>
                <li class="menuheader expandable"><a href="javascript:void(0);" class="m3">Employee
                    Listings&nbsp;&nbsp;</span></a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);" onclick="JQD.loadReportPara('employeeList');">Employee
                            Register</a></li>
                        <li><a href="javascript:void(0);">Employee Phone or SSN Listing</a></li>
                        <li><a href="javascript:void(0);">Employee Birthday Listing</a></li>
                        <li><a href="javascript:void(0);">Employee Length of Service Report</a></li>
                        <li><a href="javascript:void(0);">Employee Events Listing</a></li>
                        <li><a href="javascript:void(0);">Employee Required Events Status Report</a></li>
                        <li><a href="javascript:void(0);">Events History Report</a></li>
                    </ul>
                </li>
                <li class="menuheader expandable"><a href="javascript:void(0);" class="m4">Verbal Orders
                    &nbsp;&nbsp;</span></a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);">Verbal Orders</a></li>
                        <li><a href="javascript:void(0);">Unsigned Physician Orders</a></li>
                        <li><a href="javascript:void(0);">Progress Summary</a></li>
                    </ul>
                </li>
                <li class="menuheader expandable"><a href="javascript:void(0);" class="m5">H485&nbsp;&nbsp;</a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);" onclick="JQD.loadReportPara('RecertDueList');">Recent.
                            Due Listing </a></li>
                        <li><a href="javascript:void(0);">Unverified/Unsigned 485s</a></li>
                    </ul>
                </li>
                <li class="menuheader expandable"><a href="javascript:void(0);" class="m6">Other Listings&nbsp;&nbsp;</a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);">Misc. Code Listing (Referral Types, etc)</a></li>
                        <li><a href="javascript:void(0);">Physician Listing</a></li>
                        <li><a href="javascript:void(0);">Hospital Listing</a></li>
                        <li><a href="javascript:void(0);">Phrases Listing</a></li>
                        <li><a href="javascript:void(0);">Services Listing</a></li>
                        <li><a href="javascript:void(0);">Supply Code Listing</a></li>
                        <li><a href="javascript:void(0);">Insurance Company Listing</a></li>
                        <li><a href="javascript:void(0);">Care Plan Item Review Listing</a></li>
                        <li><a href="javascript:void(0);">Patient Discharge Facility Date Listing</a></li>
                    </ul>
                </li>
                <li class="menuheader expandable"><a href="javascript:void(0);" class="m7">Historical
                    Reports &nbsp;&nbsp;<!--<span style="background-image:url(/assets/images/sm_lk6.gif); display:block; float:right; width:10px; height:14px;"></span>-->
                </a>
                    <ul class="categoryitems">
                        <li><a href="javascript:void(0);" class="m7">By Patient&nbsp;&nbsp;<span></span></a>
                            <ul>
                                <li><a href="javascript:void(0);" onclick="JQD.loadReportPara('patientVisByDate');">
                                    Patient Visits History By Day</a></li>
                                <li><a href="http://www.ruby-lang.org/en/">Patient Visits History By Service </a>
                                </li>
                                <li><a href="http://www.python.org/">Patient Visits &amp; Hours Summary</a></li>
                                <li><a href="http://www.perl.org/">Patient Visits, Admissions,Discharges</a></li>
                                <li><a href="http://java.sun.com/">Patient Admissions &amp; Discharges</a></li>
                                <li><a href="http://en.wikipedia.org/wiki/C_Sharp">SOC Oasis Admissions Report</a></li>
                                <li><a href="http://en.wikipedia.org/wiki/C_Sharp">Patient &amp; Visit Counts By Insurance</a></li>
                                <li><a href="http://www.perl.org/">Patient Hospice Summary </a></li>
                                <li><a href="http://java.sun.com/">Patient cert Period Ledger</a></li>
                                <li><a href="http://www.php.net/">Duplicated Patient &amp; Visit Counts By Discipline</a></li>
                                <li><a href="http://www.ruby-lang.org/en/">Active Patients Not Visited</a></li>
                                <li><a href="http://www.python.org/">Patient &amp; H485 Cert Periods By Physician</a></li>
                                <li><a href="http://www.perl.org/">Patient Admission/Length of Stay Report</a></li>
                                <li><a href="http://java.sun.com/">Patient Authorization Visits History</a></li>
                                <li><a href="http://en.wikipedia.org/wiki/C_Sharp">Supervised Visits Report</a></li>
                                <li><a href="http://en.wikipedia.org/wiki/C_Sharp">Patient Visit Deviations</a></li>
                                <li><a href="http://www.perl.org/">Nursing Note Not Received Listing</a></li>
                                <li><a href="http://java.sun.com/">Authorizations Status Report</a></li>
                                <li><a href="http://en.wikipedia.org/wiki/C_Sharp">Supply Usage By Patient Listing</a></li>
                                <li><a href="http://en.wikipedia.org/wiki/C_Sharp">Missed Visit Report</a></li>
                                <li><a href="http://www.perl.org/">Visit Deviations By Primary Physician Report</a></li>
                            </ul>
                        </li>
                        <li class="menuheader expandable"><a href="javascript:void(0);" class="m8">By Employee&nbsp;&nbsp;<span></span></a>
                            <ul>
                                <li><a href="javascript:void(0);" onclick="JQD.loadReportPara('empVisHis');">Employee
                                    Visits History</a></li>
                                <li><a href="http://www.ruby-lang.org/en/">Employee Visits &amp; Hours Summary</a></li>
                                <li><a href="http://www.python.org/">Employee Efficiency</a></li>
                                <li><a href="http://www.perl.org/">Admissions By Employee</a></li>
                                <li><a href="http://java.sun.com/">Supply Usage By Employee Listing</a></li>
                            </ul>
                        </li>
                        <li class="menuheader expandable"><a href="javascript:void(0);" class="m9">By Insurance/Service&nbsp;&nbsp;<span></span></a>
                            <ul>
                                <li><a href="javascript:void(0);" onclick="JQD.loadReportPara('sumByInsu');">Summary
                                    By Insurance Co./Service</a></li>
                                <li><a href="http://www.ruby-lang.org/en/">Visits By Service/Insurance Summary</a></li>
                                <li><a href="http://www.python.org/">Visits By Service Code</a></li>
                                <li><a href="http://www.perl.org/">Possible Billing Errors</a></li>
                            </ul>
                        </li>
                        <li class="menuheader expandable"><a href="javascript:void(0);" class="m10">Historical
                            Reports-other&nbsp;&nbsp;<span></span></a>
                            <ul>
                                <li><a href="javascript:void(0);" onclick="JQD.loadReportPara('admByPhy');">Admissions
                                    By Physician</a></li>
                                <li><a href="http://www.ruby-lang.org/en/">Discharges By Physician</a></li>
                                <li><a href="http://www.python.org/">Visits By Batch Number</a></li>
                                <li><a href="http://www.perl.org/">Supply Usage By Item Listing</a></li>
                                <li><a href="http://java.sun.com/">Invoice Aging Report</a></li>
                                <li><a href="http://en.wikipedia.org/wiki/C_Sharp">Archived Visits Report</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="abs window_content">
            <div class="window_aside abs">
            </div>
            <div id="reportContent" class="window_main abs">
                <div id="" class="reportLeftContent">
                    <div class="fix">
                        <h4>
                            Admission By Physician-Report Parameters</h4>
                        <div class="underLine">
                        </div>
                        <div class="row">
                            <div class="row" style="width: 33%; float: left;">
                                <div class="row">
                                    <label>
                                        Physicain</label></div>
                                <div class="break">
                                </div>
                                <div class="inputs">
                                    <span class="input_wrapper blank">
                                        <select name="list" style="width: 155px;">
                                            <option value="0">Skilled Nurse</option>
                                            <option value="1">Home Health Aide</option>
                                            <option value="2">Physical Therapist</option>
                                            <option value="3">Speech Therapist</option>
                                            <option value="4">Occup. Therapist</option>
                                            <option value="5">Med Soc Worker</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="row" style="width: 33%; float: left;">
                                <div class="row">
                                    <label>
                                        Office:</label></div>
                                <div class="break">
                                </div>
                                <div class="inputs">
                                    <span class="input_wrapper blank">
                                        <select name="list" style="width: 155px;">
                                            <option value="0">Skilled Nurse</option>
                                            <option value="1">Home Health Aide</option>
                                            <option value="2">Physical Therapist</option>
                                            <option value="3">Speech Therapist</option>
                                            <option value="4">Occup. Therapist</option>
                                            <option value="5">Med Soc Worker</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div>
                                    <label>
                                        Admission Dates:</label></div>
                                <div class="break">
                                </div>
                                <div class="inputs">
                                    <span class="input_wrapper blank">
                                        <input type="text" id="Text31" />
                                    </span>through <span class="input_wrapper blank">
                                        <input type="text" id="Text32" />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <input type="checkbox" />
                                <label>
                                    Create Export File After Report Prints</label>
                            </div>
                        </div>
                        <div class="row">
                            <fieldset class="sortList">
                                <legend>Sorted BY</legend>
                                <ul>
                                    <li><span>
                                        <input type="radio" />
                                        Physician</span> </li>
                                </ul>
                            </fieldset>
                        </div>
                        <div class="row">
                            <div class="buttons">
                                <ul>
                                    <li><span class="button send_form_btn"><span><span>Add</span></span>
                                        <input id="Submit8" name="" type="submit" /></span></li>
                                    <li><span class="button red_btn cancel_btn"><span><span>Cancel</span></span><input
                                        id="Button19" name="" type="button" /></span></li>
                                    <li><span class="button orange_btn"><span><span>Reset</span></span><input id="Reset8"
                                        name="" type="reset" /></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="abs window_bottom">
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
<div id="reportPara" style="display: none;">
    <div id="patientRegistry" class="reportLeftContent">
        <div class="fix">
            <h4>
                Patient Register
            </h4>
            <div class="underLine">
            </div>
            <div class="row">
                <div class="row" style="width: 25%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            Locality:</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">All</option>
                                <option value="1">Medicare (traditional)</option>
                                <option value="2">Medicare (HMO/managed care)</option>
                                <option value="3">Medicaid (traditional)</option>
                                <option value="4">Medicaid (HMO/managed care) </option>
                                <option value="5">Workers' compensation</option>
                                <option value="6">Title programs </option>
                                <option value="7">Other government</option>
                                <option value="8">Private</option>
                                <option value="9">Private HMO/managed care</option>
                                <option value="10">Self Pay</option>
                                <option value="11">Unknown</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 25%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            Office:</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 25%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            County:</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">All</option>
                                <option value="1">Medicare (traditional)</option>
                                <option value="2">Medicare (HMO/managed care)</option>
                                <option value="3">Medicaid (traditional)</option>
                                <option value="4">Medicaid (HMO/managed care) </option>
                                <option value="5">Workers' compensation</option>
                                <option value="6">Title programs </option>
                                <option value="7">Other government</option>
                                <option value="8">Private</option>
                                <option value="9">Private HMO/managed care</option>
                                <option value="10">Self Pay</option>
                                <option value="11">Unknown</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 25%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            Status:</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">All</option>
                                <option value="1">Medicare (traditional)</option>
                                <option value="2">Medicare (HMO/managed care)</option>
                                <option value="3">Medicaid (traditional)</option>
                                <option value="4">Medicaid (HMO/managed care) </option>
                                <option value="5">Workers' compensation</option>
                                <option value="6">Title programs </option>
                                <option value="7">Other government</option>
                                <option value="8">Private</option>
                                <option value="9">Private HMO/managed care</option>
                                <option value="10">Self Pay</option>
                                <option value="11">Unknown</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row" style="width: 25%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            Primary Insurance:</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">All</option>
                                <option value="1">Medicare (traditional)</option>
                                <option value="2">Medicare (HMO/managed care)</option>
                                <option value="3">Medicaid (traditional)</option>
                                <option value="4">Medicaid (HMO/managed care) </option>
                                <option value="5">Workers' compensation</option>
                                <option value="6">Title programs </option>
                                <option value="7">Other government</option>
                                <option value="8">Private</option>
                                <option value="9">Private HMO/managed care</option>
                                <option value="10">Self Pay</option>
                                <option value="11">Unknown</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 25%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            Addtional Insurance:</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 25%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            &nbsp;&nbsp;</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">All</option>
                                <option value="1">Medicare (traditional)</option>
                                <option value="2">Medicare (HMO/managed care)</option>
                                <option value="3">Medicaid (traditional)</option>
                                <option value="4">Medicaid (HMO/managed care) </option>
                                <option value="5">Workers' compensation</option>
                                <option value="6">Title programs </option>
                                <option value="7">Other government</option>
                                <option value="8">Private</option>
                                <option value="9">Private HMO/managed care</option>
                                <option value="10">Self Pay</option>
                                <option value="11">Unknown</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 25%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            &nbsp;&nbsp;
                        </label>
                    </div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">All</option>
                                <option value="1">Medicare (traditional)</option>
                                <option value="2">Medicare (HMO/managed care)</option>
                                <option value="3">Medicaid (traditional)</option>
                                <option value="4">Medicaid (HMO/managed care) </option>
                                <option value="5">Workers' compensation</option>
                                <option value="6">Title programs </option>
                                <option value="7">Other government</option>
                                <option value="8">Private</option>
                                <option value="9">Private HMO/managed care</option>
                                <option value="10">Self Pay</option>
                                <option value="11">Unknown</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row" style="width: 16%; float: left;">
                    <div style="float: left; width: 103px;">
                        <label>
                            Team:</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input style="width: 103px;" />
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 25%; float: left;">
                    <div style="float: left; width: 167px;">
                        <label>
                            Primary Physician :</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 167px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 19%; float: left;">
                    <div style="float: left; width: 123px;">
                        <label>
                            Primary Diagnosis</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input style="width: 103px;" />
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 20%; float: left;">
                    <div style="float: left; width: 137px;">
                        <label>
                            &nbsp;&nbsp;
                        </label>
                    </div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank"><span class="input_wrapper blank">
                            <input style="width: 103px;" />
                        </span>
                            <input type="checkbox" />
                    </div>
                </div>
                <div class="row" style="width: 18%; float: left;">
                    <div style="float: left; width: 123px;">
                        <label>
                            Zip Code</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input style="width: 103px;" />
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row" style="width: 26%; float: left;">
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Medicare Patients Only
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Medicaide Patients Only
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Include Hospitalized Patients
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Include Inactive Patients
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Show Inactive Date
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Show Escort Required
                    </div>
                </div>
                <div class="row" style="width: 37%; float: left;">
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Show Cert Period Dates in stead of Caseworker
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Show Cert Period Dates & Other 485 info.
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />Show All Patient Diagnosis
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Include Only Currently Admitted Patients
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Extra Space Between Patients
                        <div class="input_wrapper blank" style="width: 100%;">
                            <input type="checkbox" />
                            Include Only Patients Requiring Escort
                        </div>
                    </div>
                </div>
                <div class="row" style="width: 37%; float: left;">
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Print Patient Office Instead of Comment
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />Use Primary Insurance Filter (only primary) to check for
                        Patient Secondary Insuarances also
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Creat Export File After Report Prints
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Print to PDF &nbsp;&nbsp;
                        <input type="checkbox" />
                        Preview PDF
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row" style="width: 60%; float: left;">
                    <div class="row" style="width: 100%; float: left;">
                        <div style="float: left; width: 100%;">
                            <label>
                                Serviced(Visited):</label></div>
                        <div class="break">
                        </div>
                        <div class="inputs" style="float: left;">
                            <span class="input_wrapper blank">
                                <input type="text" id="SerVisStart" />
                            </span>through <span class="input_wrapper blank">
                                <input type="text" id="SerVisEnd" />
                            </span>
                        </div>
                    </div>
                    <div class="row" style="width: 100%; float: left;">
                        <div style="float: left; width: 100%;">
                            <label>
                                Patient was On Service based upon admission and discharges for the period:</label></div>
                        <div class="break">
                        </div>
                        <div class="inputs" style="float: left;">
                            <span class="input_wrapper blank">
                                <input />
                            </span>through <span class="input_wrapper blank">
                                <input />
                            </span>
                        </div>
                    </div>
                    <div class="row" style="width: 100%; float: left;">
                        <div style="float: left; width: 100%;">
                            <label>
                                Patient was Discharged during the following time period:</label></div>
                        <div style="clear: both; width: 100%">
                        </div>
                        <div class="inputs" style="float: left;">
                            <span class="input_wrapper blank">
                                <input />
                            </span>through <span class="input_wrapper blank">
                                <input />
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row" style="width: 40%; float: left;">
                    <div class="row" style="width: 100%; float: left;">
                        <div style="float: left; width: 170px;">
                            <label>
                                Insurance Reporting Group:</label></div>
                        <div class="break">
                        </div>
                        <div class="inputs" style="float: left;">
                            <span class="input_wrapper blank">
                                <input />
                            </span>
                        </div>
                    </div>
                    <div class="row" style="width: 100%; float: left;">
                        <div style="float: left; width: 100%;">
                            <label>
                                Patient:</label></div>
                        <div class="break">
                        </div>
                        <div class="inputs" style="float: left;">
                            <span class="input_wrapper blank">
                                <input />
                            </span>
                        </div>
                    </div>
                    <div class="row" style="width: 100%; float: left;">
                        <div style="float: left; width: 100%;">
                            <label>
                                Patient was Discharged during the following time period:</label></div>
                        <div class="break">
                        </div>
                        <div class="inputs" style="float: left;">
                            <span class="input_wrapper blank">
                                <input />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <fieldset class="sortList">
                    <legend>Sorted BY</legend>
                    <ul>
                        <li>
                            <input type="radio" />
                            Patient Last Name </li>
                        <li>
                            <input type="radio" />
                            Resposible Employee </li>
                        <li>
                            <input type="radio" />
                            Primary Insurance ID# </li>
                        <li>
                            <input type="radio" />
                            Date Of Service </li>
                        <li>
                            <input type="radio" />
                            CBSA/Patient Name </li>
                    </ul>
                </fieldset>
            </div>
            <div class="row">
                <div class="buttons">
                    <ul>
                        <li><span class="button send_form_btn"><span><span>Run</span></span>
                            <input id="Submit7" name="" type="submit" /></span></li>
                        <li><span class="button red_btn cancel_btn"><span><span>Cancel</span></span><input
                            id="Button18" name="" type="button" /></span></li>
                        <li><span class="button orange_btn"><span><span>Reset</span></span><input id="Reset7"
                            name="" type="reset" /></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="OasisAssesList" class="reportLeftContent">
        <div class="fix">
            <h4>
                Oasis Assessment Listing
            </h4>
            <div class="underLine">
            </div>
            <div class="row">
                <div class="row" style="width: 60%; float: left;">
                    <div class="row" style="width: 100%; float: left;">
                        <div style="float: left; width: 100%;">
                            <label>
                                Assessment Dates:</label></div>
                        <div class="break">
                        </div>
                        <div class="inputs" style="float: left;">
                            <span class="input_wrapper blank">
                                <input type="text" id="Text35" />
                            </span>through <span class="input_wrapper blank">
                                <input type="text" id="Text37" />
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row" style="width: 40%; float: left;">
                    <div class="row" style="width: 100%; float: left;">
                        <div style="float: left; width: 170px;">
                            <label>
                                Office:</label></div>
                        <div class="break">
                        </div>
                        <div class="inputs" style="float: left;">
                            <span class="input_wrapper blank">
                                <input />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row" style="width: 45%; float: left;">
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Unverfied Assessments Only
                    </div>
                    <div class="input_wrapper blank" style="width: 100%;">
                        <input type="checkbox" />
                        Show Only Assessments not yet sent to the State
                    </div>
                </div>
                <div class="row" style="width: 55%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            State Sent Dates:</label></div>
                    <div class="break">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" id="Text39" />
                        </span>through <span class="input_wrapper blank">
                            <input type="text" id="Text40" />
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <fieldset class="sortList">
                    <legend>Sorted By</legend>
                    <ul>
                        <li>
                            <input type="radio" />
                            Patient </li>
                        <li>
                            <input type="radio" />
                            Assessment Date </li>
                        <li>
                            <input type="radio" />
                            State Sent Dates </li>
                    </ul>
                </fieldset>
            </div>
            <div class="row">
                <div class="row" style="width: 100%; float: left;">
                    <div style="float: left;">
                        <label>
                            Assessment Types:</label></div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 190px;">
                                <option value="0">All</option>
                                <option value="1">Start Of Care</option>
                                <option value="2">Resumption of Care</option>
                                <option value="3">Recertification</option>
                                <option value="4">Other Follow Up</option>
                                <option value="5">Discharge from Agency</option>
                                <option value="6">Transfer - Not Discharged</option>
                                <option value="7">Transfer - Discharged</option>
                                <option value="8">Died at Home</option>
                                <option value="9">Physical Therapy</option>
                                <option value="10">Hospice</option>
                                <option value="11">Pediatric</option>
                                <option value="12">General (Non-Oasis)</option>
                                <option value="13">Occupational Therapy</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="buttons">
                    <ul>
                        <li><span class="button send_form_btn"><span><span>Run</span></span>
                            <input id="Submit9" name="" type="button" onclick="JQD.open_window('#sampleRep1');" /></span></li>
                        <li><span class="button red_btn cancel_btn"><span><span>Cancel</span></span><input
                            id="Button20" name="" type="button" /></span></li>
                        <li><span class="button orange_btn"><span><span>Reset</span></span><input id="Reset9"
                            name="" type="reset" /></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="employeeList" class="reportLeftContent">
        <div class="fix">
            <h4>
                Employee Register
            </h4>
            <div class="underLine">
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 27%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            Locality:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">All</option>
                                <option value="1">Medicare (traditional)</option>
                                <option value="2">Medicare (HMO/managed care)</option>
                                <option value="3">Medicaid (traditional)</option>
                                <option value="4">Medicaid (HMO/managed care) </option>
                                <option value="5">Workers' compensation</option>
                                <option value="6">Title programs </option>
                                <option value="7">Other government</option>
                                <option value="8">Private</option>
                                <option value="9">Private HMO/managed care</option>
                                <option value="10">Self Pay</option>
                                <option value="11">Unknown</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 25%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Office:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 20%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Team:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" style="width: 150px" />
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 25%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            &nbsp;&nbsp;
                        </label>
                    </div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; padding-left: 5px; width: 170px;">
                        <input type="checkbox" />
                        <label>
                            Include Inactive Employees</label></div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 27%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            Status:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">All</option>
                                <option value="1">Medicare (traditional)</option>
                                <option value="2">Medicare (HMO/managed care)</option>
                                <option value="3">Medicaid (traditional)</option>
                                <option value="4">Medicaid (HMO/managed care) </option>
                                <option value="5">Workers' compensation</option>
                                <option value="6">Title programs </option>
                                <option value="7">Other government</option>
                                <option value="8">Private</option>
                                <option value="9">Private HMO/managed care</option>
                                <option value="10">Self Pay</option>
                                <option value="11">Unknown</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 45%; float: left;">
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Include Employee SSN#</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Show Employee Benefits Status</label></div>
                </div>
                <div class="row" style="width: 20%; float: left;">
                    <div style="float: left; width: 130px;">
                        <label>
                            Zip Code:
                        </label>
                    </div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left; width: 130px;">
                        <span class="input_wrapper blank">
                            <input type="text" style="width: 130px;" />
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 27%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            Employment/Stats Type:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">All</option>
                                <option value="1">Medicare (traditional)</option>
                                <option value="2">Medicare (HMO/managed care)</option>
                                <option value="3">Medicaid (traditional)</option>
                                <option value="4">Medicaid (HMO/managed care) </option>
                                <option value="5">Workers' compensation</option>
                                <option value="6">Title programs </option>
                                <option value="7">Other government</option>
                                <option value="8">Private</option>
                                <option value="9">Private HMO/managed care</option>
                                <option value="10">Self Pay</option>
                                <option value="11">Unknown</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 45%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Employment/Stats Class:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 23%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Benefits:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" />
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%;">
                <div class="row" style="width: 27%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Employee With No Visits Since:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" />
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 45%; float: left;">
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Create Export File After Report Prints</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Include Employee Driver's License# in Export File</label></div>
                </div>
                <div class="row" style="width: 19%; float: left;">
                    <div style="float: left; width: 123px;">
                        <label>
                            Employee</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input style="width: 103px;" />
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%;">
                <fieldset class="sortList">
                    <legend>Sorted BY</legend>
                    <ul>
                        <li>
                            <input type="radio" />
                            Last Name </li>
                        <li>
                            <input type="radio" />
                            Zip Code </li>
                    </ul>
                </fieldset>
            </div>
            <div class="row" style="width: 100%;">
                <div class="buttons">
                    <ul>
                        <li><span class="button send_form_btn"><span><span>Run</span></span>
                            <input id="Submit10" name="" type="submit" /></span></li>
                        <li><span class="button red_btn cancel_btn"><span><span>Cancel</span></span><input
                            id="Button21" name="" type="button" /></span></li>
                        <li><span class="button orange_btn"><span><span>Reset</span></span><input id="Reset10"
                            name="" type="reset" /></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="RecertDueList" class="reportLeftContent">
        <div class="fix">
            <h4>
                Recert. Due Listing
            </h4>
            <div class="underLine">
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 55%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Recent. Thru Dates:(Cert Thru Dates of Patient's H485)</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" id="Text33" />
                        </span>through <span class="input_wrapper blank">
                            <input type="text" id="Text34" />
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 45%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Office:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 20%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Team:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" style="width: 120px" />
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 40%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            Responsible Employee:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">All</option>
                                <option value="1">Medicare (traditional)</option>
                                <option value="2">Medicare (HMO/managed care)</option>
                                <option value="3">Medicaid (traditional)</option>
                                <option value="4">Medicaid (HMO/managed care) </option>
                                <option value="5">Workers' compensation</option>
                                <option value="6">Title programs </option>
                                <option value="7">Other government</option>
                                <option value="8">Private</option>
                                <option value="9">Private HMO/managed care</option>
                                <option value="10">Self Pay</option>
                                <option value="11">Unknown</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 27%; float: left;">
                    <div style="float: left; width: 170px;">
                        <label>
                            Insurance:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 170px;">
                                <option value="0">All</option>
                                <option value="1">Medicare (traditional)</option>
                                <option value="2">Medicare (HMO/managed care)</option>
                                <option value="3">Medicaid (traditional)</option>
                                <option value="4">Medicaid (HMO/managed care) </option>
                                <option value="5">Workers' compensation</option>
                                <option value="6">Title programs </option>
                                <option value="7">Other government</option>
                                <option value="8">Private</option>
                                <option value="9">Private HMO/managed care</option>
                                <option value="10">Self Pay</option>
                                <option value="11">Unknown</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 100%; float: left;">
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Show only H485s that have not already been Recerted</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Show Patient as Recerted if there is a new H485 at all no matter insuarnce</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Show Recert Due Date as First Day of Next Period</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Show Inactive Patient Also</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Show OASIS grace period date (four days previous to Due Date)</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Create Export File After Report Prints</label></div>
                </div>
            </div>
            <div class="row" style="width: 100%;">
                <div class="row" style="width: 100%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Show Grace Period as the following number of days, instead of the standard OASIS
                            grace period:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" />
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%;">
                <fieldset class="sortList">
                    <legend>Sorted BY</legend>
                    <ul>
                        <li>
                            <input type="radio" />
                            Patient Name </li>
                        <li>
                            <input type="radio" />
                            Due Date </li>
                        <li>
                            <input type="radio" />
                            Office/Responsible Employee </li>
                        <li>
                            <input type="radio" />
                            Office/Due Date</li>
                        <li>
                            <input type="radio" />
                            Insurance/Due Date</li>
                    </ul>
                </fieldset>
            </div>
            <div class="row" style="width: 100%;">
                <div class="buttons">
                    <ul>
                        <li><span class="button send_form_btn"><span><span>Run</span></span>
                            <input id="Submit11" name="" type="submit" /></span></li>
                        <li><span class="button red_btn cancel_btn"><span><span>Cancel</span></span><input
                            id="Button22" name="" type="button" /></span></li>
                        <li><span class="button orange_btn"><span><span>Reset</span></span><input id="Reset11"
                            name="" type="reset" /></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="patientVisByDate" class="reportLeftContent">
        <div class="fix">
            <h4>
                Patient Visits History-By Date</h4>
            <div class="underLine">
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 33%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Patient</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 33%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Office:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 33%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Insurance Company:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 55%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Visit Dates:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" id="Text41" />
                        </span>through <span class="input_wrapper blank">
                            <input type="text" id="Text42" />
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 45%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Patient Team:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" style="width: 120px" />
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 50%; float: left;">
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Page Break After Each Patient</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Charting Notes Required (By Ins Co.)</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Nursing Notes Required (By Ins Co.)</label></div>
                </div>
                <div class="row" style="width: 50%; float: left;">
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Include Patient's Primary Insurance #</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Check Multiple Insurances</label></div>
                </div>
            </div>
            <div class="row" style="width: 100%;">
                <div style="float: left; width: 100%;">
                    <input type="checkbox" />
                    <label>
                        Create Export File After Report Prints</label></div>
                <div style="clear: both; width: 100%">
                </div>
                <div style="float: left; width: 100%;">
                    <input type="checkbox" />
                    <label>
                        Include Admission/Discharge Information in Export File</label></div>
            </div>
            <div class="row" style="width: 100%;">
                <fieldset class="sortList">
                    <legend>Sorted BY</legend>
                    <ul>
                        <li>
                            <input type="radio" />
                            Patient/Day </li>
                    </ul>
                </fieldset>
            </div>
            <div class="row" style="width: 100%;">
                <div class="buttons">
                    <ul>
                        <li><span class="button send_form_btn"><span><span>Run</span></span>
                            <input id="Submit12" name="" type="submit" /></span></li>
                        <li><span class="button red_btn cancel_btn"><span><span>Cancel</span></span><input
                            id="Button23" name="" type="button" /></span></li>
                        <li><span class="button orange_btn"><span><span>Reset</span></span><input id="Reset12"
                            name="" type="reset" /></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="empVisHis" class="reportLeftContent">
        <div class="fix">
            <h4>
                Employee Visits History-Report Parameters
            </h4>
            <div class="underLine">
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 33%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Employee</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 33%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Office:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 33%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Insurance Company:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 55%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Visit Dates:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" id="Text44" />
                        </span>through <span class="input_wrapper blank">
                            <input type="text" id="Text45" />
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 45%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Employment/Stats Class:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" style="width: 120px" />
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 55%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Employee Team:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" id="Text46" />
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 45%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Service:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 50%; float: left;">
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Page Break After Each Employee</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Print Service Description instead of Code
                        </label>
                    </div>
                </div>
                <div class="row" style="width: 50%; float: left;">
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Exclude Billed Hours and Amount</label></div>
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Exclude Non-Payroll Service Hours from Totals</label></div>
                </div>
            </div>
            <div class="row" style="width: 100%;">
                <div style="float: left; width: 100%;">
                    <input type="checkbox" />
                    <label>
                        Create Export File After Report Prints</label></div>
            </div>
            <div class="row" style="width: 100%;">
                <fieldset class="sortList">
                    <legend>Sorted BY</legend>
                    <ul>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Employee/Service</span> </li>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Employee ID#/Date </span></li>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Employee Name/Date </span></li>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Employee/Patient </span></li>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Employee/Summary By Service </span></li>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Employee/Patient Office/Insurance/Summary By Service </span></li>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Employee/Patient </span></li>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Odometer Readings </span></li>
                    </ul>
                </fieldset>
            </div>
            <div class="row" style="width: 100%;">
                <div class="buttons">
                    <ul>
                        <li><span class="button send_form_btn"><span><span>Run</span></span>
                            <input id="Submit13" name="" type="submit" /></span></li>
                        <li><span class="button red_btn cancel_btn"><span><span>Cancel</span></span><input
                            id="Button24" name="" type="button" /></span></li>
                        <li><span class="button orange_btn"><span><span>Reset</span></span><input id="Reset13"
                            name="" type="reset" /></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="sumByInsu" class="reportLeftContent">
        <div class="fix">
            <h4>
                Summary By Insurance Co./Service-Report Parameters</h4>
            <div class="underLine">
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 33%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Insurance Co.</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 33%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Office:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 33%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            County:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 55%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Dates:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" id="Text43" />
                        </span>through <span class="input_wrapper blank">
                            <input type="text" id="Text47" />
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 45%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            MSA Code:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" style="width: 120px" />
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 50%; float: left;">
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Show Unbilled Visits Only</label></div>
                </div>
                <div class="row" style="width: 50%; float: left;">
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Exclude Unbillabel Visits(Supervisory, Auxiliaary,etc.)
                        </label>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%;">
                <fieldset class="sortList">
                    <legend>Sorted BY</legend>
                    <ul>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Insurance Company Information</span> </li>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Insurance Company/Patient Detail Information </span></li>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Insurance Company/Service Detail Information </span></li>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Service Information </span></li>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Patient/Employee/Service </span></li>
                    </ul>
                </fieldset>
            </div>
            <div class="row" style="width: 100%;">
                <div class="buttons">
                    <ul>
                        <li><span class="button send_form_btn"><span><span>Run</span></span>
                            <input id="Submit14" name="" type="submit" /></span></li>
                        <li><span class="button red_btn cancel_btn"><span><span>Cancel</span></span><input
                            id="Button25" name="" type="button" /></span></li>
                        <li><span class="button orange_btn"><span><span>Reset</span></span><input id="Reset14"
                            name="" type="reset" /></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="admByPhy" class="reportLeftContent">
        <div class="fix">
            <h4>
                Admission By Physician-Report Parameters</h4>
            <div class="underLine">
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 33%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Physicain</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
                <div class="row" style="width: 33%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Office:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <select name="list" style="width: 155px;">
                                <option value="0">Skilled Nurse</option>
                                <option value="1">Home Health Aide</option>
                                <option value="2">Physical Therapist</option>
                                <option value="3">Speech Therapist</option>
                                <option value="4">Occup. Therapist</option>
                                <option value="5">Med Soc Worker</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 55%; float: left;">
                    <div style="float: left; width: 100%;">
                        <label>
                            Admission Dates:</label></div>
                    <div style="clear: both; width: 100%">
                    </div>
                    <div class="inputs" style="float: left;">
                        <span class="input_wrapper blank">
                            <input type="text" id="Text48" />
                        </span>through <span class="input_wrapper blank">
                            <input type="text" id="Text49" />
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="width: 100%; display: block">
                <div class="row" style="width: 50%; float: left;">
                    <div style="float: left; width: 100%;">
                        <input type="checkbox" />
                        <label>
                            Create Export File After Report Prints</label></div>
                </div>
            </div>
            <div class="row" style="width: 100%;">
                <fieldset class="sortList">
                    <legend>Sorted BY</legend>
                    <ul>
                        <li><span style="display: block;">
                            <input type="radio" />
                            Physician</span> </li>
                    </ul>
                </fieldset>
            </div>
            <div class="row" style="width: 100%;">
                <div class="buttons">
                    <ul>
                        <li><span class="button send_form_btn"><span><span>Run</span></span>
                            <input id="Submit15" name="" type="submit" /></span></li>
                        <li><span class="button red_btn cancel_btn"><span><span>Cancel</span></span><input
                            id="Button26" name="" type="button" /></span></li>
                        <li><span class="button orange_btn"><span><span>Reset</span></span><input id="Reset15"
                            name="" type="reset" /></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
