﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="edit485" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left">Edit 485 - Plan of Care</span><span class="float_right"><a
                href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                    href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content general_form">
            <% using (Html.BeginForm("Edit485", "Oasis", FormMethod.Post, new { @id = "edit485Form" }))%>
            <%  { %>
            <%=Html.Hidden("Id", "", new { @id = "txtEdit_PatientID" })%>
            <div id="editPatientValidaton" class="marginBreak" style="display: none">
            </div>
            <div class="margin">
                <div class="row485">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th colspan="3">
                                Medications: Dose/Frequency/Route (N)ew (C)hanged (#10)
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <textarea id="txtEdit_PlanOfCare_Medications" name="txtEdit_PlanOfCare_Medications"
                                    style="width: 99%; overflow: auto;" cols="100" rows="8"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="margin">
                <div class="row485">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th colspan="3">
                                DME and Supplies (#14)
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <textarea id="txtEdit_PlanOfCare_DMESupplies" name="txtEdit_PlanOfCare_DMESupplies"
                                    style="width: 99.2%; overflow: auto;" cols="100" rows="8"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="margin">
                <div class="row485">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th colspan="3">
                                Safety Measures (#15)
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <textarea id="txtEdit_PlanOfCare_SafetyMeasures" name="txtEdit_PlanOfCare_SafetyMeasures"
                                    style="width: 99.2%; overflow: auto;" cols="100" rows="8"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="margin">
                <div class="row485">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th colspan="3">
                                Nutritional Requirements (#16)
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <textarea id="txtEdit_PlanOfCare_NutritionalRequirements" name="txtEdit_PlanOfCare_NutritionalRequirements"
                                    style="width: 99.2%; overflow: auto;" cols="100" rows="8"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="margin">
                <div class="row485">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th colspan="3">
                                Allergies (#17)
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <textarea id="txtEdit_PlanOfCare_Allergies" name="txtEdit_PlanOfCare_Allergies" style="width: 99.2%;
                                    overflow: auto;" cols="100" rows="8"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="margin">
                <div class="row485">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th colspan="3">
                                Functional Limitations (#18.A)
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <ul class="columns">
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="" type="hidden" />
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="1" type="checkbox" />
                                        &nbsp; Amputation</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="5" type="checkbox" />
                                        &nbsp; Paralysis</li>
                                    <li style="width: 32%;">
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="9" type="checkbox" />
                                        &nbsp; Legally Blind</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="2"
                                            type="checkbox" />
                                        &nbsp; Bowel/Bladder Incontinence</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="6" type="checkbox" />
                                        &nbsp; Endurance</li>
                                    <li style="width: 32%;">
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="A" type="checkbox" />
                                        &nbsp; Dyspnea w/exertion</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="3" type="checkbox" />
                                        &nbsp; Contracture</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="7" type="checkbox" />
                                        &nbsp; Ambulation</li>
                                    <li style="width: 32%;">
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="4" type="checkbox" />
                                        &nbsp; Hearing</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="8" type="checkbox" />
                                        &nbsp; Speech</li>
                                    <li>
                                        <input name="txtEdit_PlanOfCare_FunctionLimitations" value="B" type="checkbox" />
                                        &nbsp; Other&nbsp;
                                        <input type="text" id="txtEdit_PlanOfCare_FunctionLimitationsOther" name="txtEdit_PlanOfCare_FunctionLimitationsOther" style="width: 120px;" /></li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="margin">
                <div class="row485">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th colspan="3">
                                Activities Permitted (#18.B)
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <ul class="columns">
                                    <li style="width: 33%;">
                                        <input type="hidden" name="txtEdit_PlanOfCare_ActivitiesPermitted" value="" />
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="1" type="checkbox" />&nbsp;
                                        Complete bed rest</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="9" type="checkbox" />&nbsp;Partial
                                        weight bearing</li>
                                    <li style="width: 32%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="11" type="checkbox" />&nbsp;
                                        Wheelchair</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="7" type="checkbox" />&nbsp;
                                        Bed rest with BRP</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="4" type="checkbox" />&nbsp;Independent
                                        at home</li>
                                    <li style="width: 32%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="6" type="checkbox" />&nbsp;
                                        Walker</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="2" type="checkbox" />&nbsp;
                                        Up as tolerated</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="10" type="checkbox" />&nbsp;
                                        Crutches</li>
                                    <li style="width: 32%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="C" type="checkbox" />&nbsp;
                                        No Restriction</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="8" type="checkbox" />&nbsp;
                                        Transfer bed-chair</li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="5" type="checkbox" />&nbsp;
                                        Cane</li>
                                    <li style="width: 32%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="D" type="checkbox" />&nbsp;Other&nbsp;
                                        <input type="text" name="txtEdit_PlanOfCare_ActivitiesPermittedOther" id="txtEdit_PlanOfCare_ActivitiesPermittedOther" style="width: 100px;" /></li>
                                    <li style="width: 33%;">
                                        <input name="txtEdit_PlanOfCare_ActivitiesPermitted" value="3" type="checkbox" />&nbsp;
                                        Exercise prescribed</li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="margin">
                <div class="row485">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th colspan="3">
                                Mental Status (#19)
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <ul class="columns">
                                    <li style="width: 24%;">
                                        <input name="txtEdit_PlanOfCare_MentalStatus" value="1" type="checkbox" />&nbsp;
                                        Oriented</li>
                                    <li style="width: 25%;">
                                        <input name="txtEdit_PlanOfCare_MentalStatus" value="3" type="checkbox" />&nbsp;
                                        Forgetful</li>
                                    <li style="width: 25%;">
                                        <input name="txtEdit_PlanOfCare_MentalStatus" value="5" type="checkbox" />&nbsp;
                                        Disoriented</li>
                                    <li style="width: 24%;">
                                        <input name="txtEdit_PlanOfCare_MentalStatus" value="7" type="checkbox" />&nbsp;
                                        Agitated</li>
                                    <li style="width: 24%;">
                                        <input name="txtEdit_PlanOfCare_MentalStatus" value="2" type="checkbox" />&nbsp;
                                        Comatose</li>
                                    <li style="width: 25%;">
                                        <input name="txtEdit_PlanOfCare_MentalStatus" value="4" type="checkbox" />&nbsp;
                                        Depressed</li>
                                    <li style="width: 25%;">
                                        <input name="txtEdit_PlanOfCare_MentalStatus" value="6" type="checkbox" />&nbsp;
                                        Lethargic</li>
                                    <li style="width: 24%;">
                                        <input name="txtEdit_PlanOfCare_MentalStatus" value="8" type="checkbox" />&nbsp;
                                        Other&nbsp;
                                        <input type="text" id="txtEdit_PlanOfCare_MentalStatusOther" name="txtEdit_PlanOfCare_MentalStatusOther" style="width: 100px;" /></li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="margin">
                <div class="row485">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th colspan="3">
                                Prognosis (#20)
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <ul class="columns">
                                    <li style="width: 19%;">
                                        <input name="New_StartOfCare_485Prognosis" value=" " type="hidden" />
                                        <input name="New_StartOfCare_485Prognosis" value="Guarded" type="radio" />&nbsp;Guarded</li>
                                    <li style="width: 19%;">
                                        <input name="New_StartOfCare_485Prognosis" value="Poor" type="radio" />&nbsp;Poor</li>
                                    <li style="width: 19%;">
                                        <input name="New_StartOfCare_485Prognosis" value="Fair" type="radio" />&nbsp;Fair</li>
                                    <li style="width: 19%;">
                                        <input name="New_StartOfCare_485Prognosis" value="Good" type="radio" />&nbsp;Good</li>
                                    <li style="width: 19%;">
                                        <input name="New_StartOfCare_485Prognosis" value="Excellent" type="radio" />&nbsp;Excellent</li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="margin">
                <div class="row485">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th colspan="3">
                                Orders for Discipline and Treatments (Specify Amount/Frequency/Duration) (#21)
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <textarea id="txtEdit_PlanOfCare_Interventions" name="txtEdit_PlanOfCare_Interventions"
                                    style="width: 99.2%; overflow: auto;" cols="100" rows="30"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="margin">
                <div class="row485">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th colspan="3">
                                Goals/Rehabilitation Potential/Discharge Plans (#22)
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <textarea id="txtEdit_PlanOfCare_Goals" name="txtEdit_PlanOfCare_Goals" style="width: 99.2%;
                                    overflow: auto;" cols="100" rows="30"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="buttons buttonfix">
                    <ul>
                        <li>
                            <input name="" type="submit" value="Save" /></li>
                        <li>
                            <input name="" type="submit" value="Save & Complete" /></li>
                        <li>
                            <input name="" type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
                    </ul>
                </div>
            </div>
            <%} %>
        </div>
        <div class="abs window_bottom">
            485 Plan of Care Narratives
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
