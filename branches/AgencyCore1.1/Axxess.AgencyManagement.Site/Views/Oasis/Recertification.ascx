﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="recertification" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span id="NewRecertificationTitle" class="float_left">Recertification</span><span
                class="float_right"><a href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);"
                    class="window_resize"> </a><a href="javascript:void(0);" class="window_close">
                </a></span>
        </div>
        <div class="abs window_content general_form oasisAssWindowContent">
            <div class="oasisAssWindowContainer">
                <div id="recertificationTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
                    <ul>
                        <li><a href="#clinicalRecord_recertification">Clinical Record Items</a></li>
                        <li><a href="#patienthistory_recertification">Patient History & Diagnoses</a></li>
                        <li><a href="#sensorystatus_recertification">Sensory Status</a></li>
                        <li><a href="#integumentarystatus_recertification">Integumentary Status</a></li>
                        <li><a href="#respiratorystatus_recertification">Respiratory Status</a></li>
                        <li><a href="#eliminationstatus_recertification">Elimination Status</a></li>
                        <li><a href="#adl_recertification">ADL/IADLs</a></li>
                        <li><a href="#medications_recertification">Medications</a></li>
                        <li><a href="#therapyneed_recertification">Therapy Need & Plan Of Care</a></li>
                    </ul>
                    <div id="clinicalRecord_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationDemographicsForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "New")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0010) CMS Certification Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0010CertificationNumber" name="New_Recertification_M0010CertificationNumber"
                                        type="text" class="text" value="" />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0014) Branch State:</div>
                                </div>
                                <div class="right marginOasis">
                                    <select class="AddressStateCode" name="New_Recertification_M0014BranchState" id="New_Recertification_M0014BranchState">
                                        <option value="0" selected>** Select State **</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0016) Branch ID Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0016BranchId" name="New_Recertification_M0016BranchId"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0018) National Provider Identifier (NPI)</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0018NationalProviderId" name="New_Recertification_M0018NationalProviderId"
                                        type="text" class="text" /><br />
                                    <input type="hidden" name="New_Recertification_M0018NationalProviderIdUK" value=" " />
                                    <input type="checkbox" id="New_Recertification_M0018NationalProviderIdUK" name="New_Recertification_M0018NationalProviderIdUK" />&nbsp;UK
                                    – Unknown or Not Available</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0020) Patient ID Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0020PatientIdNumber" name="New_Recertification_M0020PatientIdNumber"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0030) Start of Care Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0030SocDate" name="New_Recertification_M0030SocDate"
                                        type="text" class="text" /></div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0040) Patient Name:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0040PatientName" name="New_Recertification_M0040PatientName"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0050) Patient State of Residence:</div>
                                </div>
                                <div class="right marginOasis">
                                    <select class="AddressStateCode" name="New_Recertification_M0050PatientState" id="New_Recertification_M0050PatientState">
                                        <option value="0" selected>** Select State **</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0060) Patient Zip Code:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0060PatientZipCode" name="New_Recertification_M0060PatientZipCode"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0063) Medicare Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0063PatientMedicareNumber" name="New_Recertification_M0063PatientMedicareNumber"
                                        type="text" class="text" /><br />
                                    <input type="hidden" name="New_Recertification_M0063PatientMedicareNumberUK" value=" " />
                                    <input id="New_Recertification_M0063PatientMedicareNumberUK" name="New_Recertification_M0063PatientMedicareNumberUK"
                                        type="checkbox" />&nbsp;NA – No Medicare</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0064) Social Security Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0064PatientSSN" name="New_Recertification_M0064PatientSSN"
                                        type="text" class="text" /><br />
                                    <input type="hidden" name="New_Recertification_M0064PatientSSNUK" value=" " />
                                    <input id="New_Recertification_M0064PatientSSNUK" name="New_Recertification_M0064PatientSSNUK"
                                        type="checkbox" />&nbsp;UK – Unknown or Not Available</div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0065) Medicaid Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0065PatientMedicaidNumber" name="New_Recertification_M0065PatientMedicaidNumber"
                                        type="text" class="text" /><br />
                                    <input type="hidden" name="New_Recertification_M0065PatientMedicaidNumberUK" value=" " />
                                    <input id="New_Recertification_M0065PatientMedicaidNumberUK" name="New_Recertification_M0065PatientMedicaidNumberUK"
                                        type="checkbox" />&nbsp;NA – No Medicaid</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0066) Birth Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0066PatientDoB" name="New_Recertification_M0066PatientDoB"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow  title">
                                    <div class="padding">
                                        (M0069) Gender:</div>
                                </div>
                                <div class="right marginOasis">
                                    <%=Html.Hidden("New_Recertification_M0069Gender", " ", new { @id = "" })%>
                                    <%=Html.RadioButton("New_Recertification_M0069Gender", "Male", new { @id = "" })%>Male
                                    <%=Html.RadioButton("New_Recertification_M0069Gender", "Female", new { @id = "" })%>Female
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0080) Discipline of Person Completing Assessment:
                                    </div>
                                </div>
                                <div class="padding">
                                    <%=Html.Hidden("New_Recertification_M0080DisciplinePerson", " ", new { @id = "" })%>
                                    <%=Html.RadioButton("New_Recertification_M0080DisciplinePerson", "1", new { @id = "" })%>&nbsp;1
                                    - RN
                                    <%=Html.RadioButton("New_Recertification_M0080DisciplinePerson", "2", new { @id = "" })%>&nbsp;2
                                    - PT
                                    <%=Html.RadioButton("New_Recertification_M0080DisciplinePerson", "3", new { @id = "" })%>&nbsp;3
                                    - SLP/ST
                                    <%=Html.RadioButton("New_Recertification_M0080DisciplinePerson", "4", new { @id = "" })%>&nbsp;4
                                    - OT
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0090) Date Assessment Completed:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="New_Recertification_M0090AssessmentCompleted" name="New_Recertification_M0090AssessmentCompleted"
                                        type="text" class="text" /></div>
                            </div>
                        </div>
                        <div class="rowOasis assessmentType">
                            <div class="insiderow title">
                                <div class="padding">
                                    (M0100) This Assessment is Currently Being Completed for the Following Reason:</div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow margin">
                                    <u>Start/Resumption of Care</u></div>
                                <div class="insiderow margin">
                                    <input name="New_Recertification_M0100AssessmentType" type="checkbox" value="1" />&nbsp;1
                                    – Start of care—further visits planned<br />
                                    <input name="New_Recertification_M0100AssessmentType" type="checkbox" value="3" />&nbsp;3
                                    – Resumption of care (after inpatient stay)<br />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow">
                                    <u>Follow-Up</u></div>
                                <div class="insiderow">
                                    <input name="New_Recertification_M0100AssessmentType" type="checkbox" value="4" />&nbsp;4
                                    – Recertification (follow-up) reassessment [ Go to M0110 ]<br />
                                    <input name="New_Recertification_M0100AssessmentType" type="checkbox" value="5" />&nbsp;5
                                    – Other follow-up [ Go to M0110 ]<br />
                                </div>
                            </div>
                            <div class="insideColFull">
                                <div class="insiderow margin">
                                    <u>Transfer to an Inpatient Facility</u></div>
                                <div class="insiderow margin">
                                    <input name="New_Recertification_M0100AssessmentType" type="checkbox" value="6" />&nbsp;6
                                    – Transferred to an inpatient facility—patient not discharged from agency [ Go to
                                    M1040]<br />
                                    <input name="New_Recertification_M0100AssessmentType" type="checkbox" value="7" />&nbsp;7
                                    – Transferred to an inpatient facility—patient discharged from agency [ Go to M1040
                                    ]<br />
                                </div>
                            </div>
                            <div class="insideColFull">
                                <div class="insiderow margin">
                                    <u>Discharge from Agency — Not to an Inpatient Facility</u></div>
                                <div class="insiderow margin">
                                    <input name="New_Recertification_M0100AssessmentType" type="checkbox" value="8" />&nbsp;8
                                    – Death at home [ Go to M0903 ]<br />
                                    <input name="New_Recertification_M0100AssessmentType" type="checkbox" value="9" />&nbsp;9
                                    – Discharge from agency [ Go to M1040 ]<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow title">
                                <div class="padding">
                                    (M0140) Race/Ethnicity: (Mark all that apply.)</div>
                            </div>
                            <div class="insideCol">
                                <div class="padding">
                                    <input name="New_Recertification_M0140RaceEthnicity" type="hidden" value=" " />
                                    <input name="New_Recertification_M0140RaceEthnicity" type="checkbox" value="1" />&nbsp;1
                                    - American Indian or Alaska Native<br />
                                    <input name="New_Recertification_M0140RaceEthnicity" type="checkbox" value="2" />&nbsp;2
                                    - Asian<br />
                                    <input name="New_Recertification_M0140RaceEthnicity" type="checkbox" value="3" />&nbsp;3
                                    - Black or African-American<br />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="padding">
                                    <input name="New_Recertification_M0140RaceEthnicity" type="checkbox" value="4" />&nbsp;4
                                    - Hispanic or Latino<br />
                                    <input name="New_Recertification_M0140RaceEthnicity" type="checkbox" value="5" />&nbsp;5
                                    - Native Hawaiian or Pacific Islander<br />
                                    <input name="New_Recertification_M0140RaceEthnicity" type="checkbox" value="6" />&nbsp;6
                                    - White
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull title">
                                <div class="padding">
                                    (M0150) Current Payment Sources for Home Care: (Mark all that apply.)</div>
                            </div>
                            <div class="insideCol ">
                                <div class="padding">
                                    <input name="New_Recertification_M0150PaymentSource" type="hidden" value=" " />
                                    <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="0" />&nbsp;0
                                    - None; no charge for current services<br />
                                    <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="1" />&nbsp;1
                                    - Medicare (traditional fee-for-service)<br />
                                    <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="2" />&nbsp;2
                                    - Medicare (HMO/managed care/Advantage plan)<br />
                                    <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="3" />&nbsp;3
                                    - Medicaid (traditional fee-for-service)<br />
                                    <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="4" />&nbsp;4
                                    - Medicaid (HMO/managed care)
                                    <br />
                                    <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="5" />&nbsp;5
                                    - Workers' compensation<br />
                                    <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="6" />&nbsp;6
                                    - Title programs (e.g., Title III, V, or XX)<br />
                                </div>
                            </div>
                            <div class="insideCol adjust">
                                <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="7" />&nbsp;7
                                - Other government (e.g., TriCare, VA, etc.)<br />
                                <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="8" />&nbsp;8
                                - Private insurance<br />
                                <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="9" />&nbsp;9
                                - Private HMO/managed care<br />
                                <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="10" />&nbsp;10
                                - Self-pay<br />
                                <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="11" />&nbsp;11
                                - Other (specify)&nbsp;&nbsp;&nbsp;
                                <input id="New_Recertification_M0150PaymentSourceOther" type="text" name="New_Recertification_M0150PaymentSourceOther" /><br />
                                <input name="New_Recertification_M0150PaymentSource" type="checkbox" value="12" />&nbsp;UK
                                - Unknown<br />
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="patienthistory_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationPatientHistoryForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "New")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1020/1022/1024) Diagnoses, Symptom Control, and Payment Diagnoses: List each diagnosis
                                        for which the patient is receiving home care (Column 1) and enter its ICD-9-C M
                                        code at the level of highest specificity (no surgical/procedure codes) (Column 2).
                                        Diagnoses are listed in the order that best reflect the seriousness of each condition
                                        and support the disciplines and services provided. Rate the degree of symptom control
                                        for each condition (Column 2). Choose one value that represents the degree of symptom
                                        control appropriate for each diagnosis: V-codes (for M1020 or M1022) or E-codes
                                        (for M1022 only) may be used. ICD-9-C M sequencing requirements must be followed
                                        if multiple coding is indicated for any diagnoses. If a V-code is reported in place
                                        of a case mix diagnosis, then optional item M1024 Payment Diagnoses (Columns 3 and
                                        4) may be completed. A case mix diagnosis is a diagnosis that determines the Medicare
                                        P P S case mix group. Do not assign symptom control ratings for V- or E-codes.<br />
                                        <b>Code each row according to the following directions for each column:<br />
                                        </b>Column 1: Enter the description of the diagnosis.<br />
                                        Column 2: Enter the ICD-9-C M code for the diagnosis described in Column 1;<br />
                                        Rate the degree of symptom control for the condition listed in Column 1 using the
                                        following scale:<br />
                                        0 - Asymptomatic, no treatment needed at this time<br />
                                        1 - Symptoms well controlled with current therapy<br />
                                        2 - Symptoms controlled with difficulty, affecting daily functioning; patient needs
                                        ongoing monitoring<br />
                                        3 - Symptoms poorly controlled; patient needs frequent adjustment in treatment and
                                        dose monitoring<br />
                                        4 - Symptoms poorly controlled; history of re-hospitalizations Note that in Column
                                        2 the rating for symptom control of each diagnosis should not be used to determine
                                        the sequencing of the diagnoses listed in Column 1. These are separate items and
                                        sequencing may not coincide. Sequencing of diagnoses should reflect the seriousness
                                        of each condition and support the disciplines and services provided.<br />
                                        Column 3: (OPTIONAL) If a V-code is assigned to any row in Column 2, in place of
                                        a case mix diagnosis, it may be necessary to complete optional item M1024 Payment
                                        Diagnoses (Columns 3 and 4). See OASIS-C Guidance Manual.<br />
                                        Column 4: (OPTIONAL) If a V-code in Column 2 is reported in place of a case mix
                                        diagnosis that requires multiple diagnosis codes under ICD-9-C M coding guidelines,
                                        enter the diagnosis descriptions and the ICD-9-C M codes in the same row in Columns
                                        3 and 4. For example, if the case mix diagnosis is a manifestation code, record
                                        the diagnosis description and ICD-9-C M code for the underlying condition in Column
                                        3 of that row and the diagnosis description and ICD-9-C M code for the manifestation
                                        in Column 4 of that row. Otherwise, leave Column 4 blank in that row.
                                    </div>
                                </div>
                                <div class="margin">
                                    <table class="agency-data-table" id="daignosisTabel">
                                        <thead>
                                            <tr>
                                                <th colspan="2">
                                                    (M1020) Primary Diagnosis & (M1022) Other Diagnoses
                                                </th>
                                                <th colspan="2">
                                                    (M1024) Payment Diagnoses (OPTIONAL)
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Column 1
                                                </th>
                                                <th>
                                                    Column 2
                                                </th>
                                                <th>
                                                    Column 3
                                                </th>
                                                <th>
                                                    Column 4
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Diagnoses (Sequencing of diagnoses should reflect the seriousness of each condition
                                                    and support the disciplines and services provided.)
                                                </td>
                                                <td>
                                                    ICD-9-C M and symptom control rating for each condition. Note that the sequencing
                                                    of these ratings may not match the sequencing of the diagnoses
                                                </td>
                                                <td>
                                                    Complete if a V-code is assigned under certain circumstances to Column 2 in place
                                                    of a case mix diagnosis.
                                                </td>
                                                <td>
                                                    Complete only if the V-code in Column 2 is reported in place of a case mix diagnosis
                                                    that is a multiple coding situation (e.g., a manifestation code).
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Description
                                                </td>
                                                <td>
                                                    ICD-9-C M / Symptom Control Rating
                                                </td>
                                                <td>
                                                    Description/ ICD-9-C M
                                                </td>
                                                <td>
                                                    Description/ ICD-9-C M
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <u>(M1020) Primary Diagnosis<br />
                                                    </u>a.&nbsp;<input type="text" /><br />
                                                    <br />
                                                </td>
                                                <td>
                                                    <u>(V-codes are allowed)<br />
                                                    </u>a.&nbsp;<input type="text" /><br />
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;
                                                    <select>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <u>(V- or E-codes NOT allowed)<br />
                                                    </u>a.&nbsp;<input type="text" /><br />
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;
                                                    <input type="text" />
                                                </td>
                                                <td>
                                                    <u>(V- or E-codes NOT allowed)<br />
                                                    </u>a.&nbsp;<input type="text" /><br />
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;<input type="text" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <u>(M1022) Other Diagnoses<br />
                                                    </u>b.&nbsp;<input type="text" /><br />
                                                </td>
                                                <td>
                                                    <u>(V- or E-codes are allowed)<br />
                                                    </u>b.&nbsp;<input type="text" /><br />
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;
                                                    <select>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <u>(V- or E-codes NOT allowed)<br />
                                                    </u>b.&nbsp;<input type="text" /><br />
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;<input type="text" />
                                                </td>
                                                <td>
                                                    <u>(V- or E-codes NOT allowed)<br />
                                                    </u>b.&nbsp;<input type="text" /><br />
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;<input type="text" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <button id="addrow">
                                        Add Row</button>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1030) Therapies the patient receives at home: (Mark all that apply.)
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1030HomeTherapies" type="hidden" value=" " />
                                    <input name="New_Recertification_M1030HomeTherapies" type="checkbox" value="1" />&nbsp;1 - Intravenous
                                    or infusion therapy (excludes TPN)<br />
                                    <input name="New_Recertification_M1030HomeTherapies" type="checkbox" value="2" />&nbsp;2 - Parenteral
                                    nutrition (TPN or lipids)<br />
                                    <input name="New_Recertification_M1030HomeTherapies" type="checkbox" value="3" />&nbsp;3 - Enteral
                                    nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial entry
                                    into the alimentary canal)<br />
                                    <input name="New_Recertification_M1030HomeTherapies" type="checkbox" value="4" />&nbsp;4 - None
                                    of the above
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="sensorystatus_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationSensoryStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "New")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1200) Vision (with corrective lenses if the patient usually wears them):
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1200Vision" type="hidden" value=" " />
                                    <input name="New_Recertification_M1200Vision" type="radio" value="0" />&nbsp;0 -
                                    Normal vision: sees adequately in most situations; can see medication labels, newsprint.<br />
                                    <input name="New_Recertification_M1200Vision" type="radio" value="1" />&nbsp;1 -
                                    Partially impaired: cannot see medication labels or newsprint, but can see obstacles
                                    in path, and the surrounding layout; can count fingers at arm's length.<br />
                                    <input name="New_Recertification_M1200Vision" type="radio" value="2" />&nbsp;2 -
                                    Severely impaired: cannot locate objects without hearing or touching them or patient
                                    nonresponsive.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1242) Frequency of Pain Interfering with patient's activity or movement:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1242PainInterferingFrequency" type="hidden" value=" " />
                                    <input name="New_Recertification_M1242PainInterferingFrequency" type="radio" value="0" />&nbsp;0
                                    - Patient has no pain<br />
                                    <input name="New_Recertification_M1242PainInterferingFrequency" type="radio" value="1" />&nbsp;1
                                    - Patient has pain that does not interfere with activity or movement<br />
                                    <input name="New_Recertification_M1242PainInterferingFrequency" type="radio" value="2" />&nbsp;2
                                    - Less often than daily<br />
                                    <input name="New_Recertification_M1242PainInterferingFrequency" type="radio" value="3" />&nbsp;3
                                    - Daily, but not constantly<br />
                                    <input name="New_Recertification_M1242PainInterferingFrequency" type="radio" value="4" />&nbsp;4
                                    - All of the time
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="integumentarystatus_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationIntegumentaryStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "New")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow  title">
                                    <div class="margin">
                                        (M1306) Does this patient have at least one Unhealed Pressure Ulcer at Stage II
                                        or Higher or designated as "unstageable"?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1306UnhealedPressureUlcers" type="hidden" value=" " />
                                    <input name="New_Recertification_M1306UnhealedPressureUlcers" type="radio" value="0" />&nbsp;0
                                    - No [ Go to M1322 ]<br />
                                    <input name="New_Recertification_M1306UnhealedPressureUlcers" type="radio" value="1" />&nbsp;1
                                    - Yes
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="new_recertification_M1308">
                            <div class="insideColFull">
                                <div class="insideColFull title">
                                    <div class="margin">
                                        (M1308) Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each
                                        Stage: (Enter "0" if none; excludes Stage I pressure ulcers)
                                    </div>
                                </div>
                                <div class="insideColFull">
                                    <div class="margin">
                                        <table class="agency-data-table" id="Table2">
                                            <thead>
                                                <tr>
                                                    <th>
                                                    </th>
                                                    <th>
                                                        Column 1<br />
                                                        Complete at SOC/ROC/FU & D/C
                                                    </th>
                                                    <th>
                                                        Column 2<br />
                                                        Complete at FU & D/C
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        Stage description – unhealed pressure ulcers
                                                    </th>
                                                    <th>
                                                        Number Currently Present
                                                    </th>
                                                    <th>
                                                        Number of those listed in Column 1 that were present on admission (most recent SOC
                                                        / ROC)
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        a. Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer
                                                        with red pink wound bed, without slough. May also present as an intact or open/ruptured
                                                        serum-filled blister.
                                                    </td>
                                                    <td>
                                                        <input name="New_Recertification_M1308NumberNonEpithelializedStageTwoUlcerCurrent"
                                                            id="New_Recertification_M1308NumberNonEpithelializedStageTwoUlcerCurrent" type="text" />
                                                    </td>
                                                    <td>
                                                        <input name="New_Recertification_M1308NumberNonEpithelializedStageTwoUlcerAdmission"
                                                            id="New_Recertification_M1308NumberNonEpithelializedStageTwoUlcerAdmission" type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        b. Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone,
                                                        tendon, or muscles are not exposed. Slough may be present but does not obscure the
                                                        depth of tissue loss. May include undermining and tunneling.
                                                    </td>
                                                    <td>
                                                        <input id="New_Recertification_M1308NumberNonEpithelializedStageThreeUlcerCurrent"
                                                            name="New_Recertification_M1308NumberNonEpithelializedStageThreeUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="New_Recertification_M1308NumberNonEpithelializedStageThreeUlcerAdmission"
                                                            name="New_Recertification_M1308NumberNonEpithelializedStageThreeUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        c. Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough
                                                        or eschar may be present on some parts of the wound bed. Often includes undermining
                                                        and tunneling.
                                                    </td>
                                                    <td>
                                                        <input id="New_Recertification_M1308NumberNonEpithelializedStageFourUlcerCurrent"
                                                            name="New_Recertification_M1308NumberNonEpithelializedStageFourUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="New_Recertification_M1308NumberNonEpithelializedStageIVUlcerAdmission"
                                                            name="New_Recertification_M1308NumberNonEpithelializedStageIVUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        d.1 Unstageable: Known or likely but unstageable due to non-removable dressing or
                                                        device
                                                    </td>
                                                    <td>
                                                        <input id="New_Recertification_M1308NumberNonEpithelializedUnstageableIUlcerCurrent"
                                                            name="New_Recertification_M1308NumberNonEpithelializedUnstageableIUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="New_Recertification_M1308NumberNonEpithelializedUnstageableIUlcerAdmission"
                                                            name="New_Recertification_M1308NumberNonEpithelializedUnstageableIUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        d.2 Unstageable: Known or likely but unstageable due to coverage of wound bed by
                                                        slough and/or eschar.
                                                    </td>
                                                    <td>
                                                        <input id="New_Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"
                                                            name="New_Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="New_Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"
                                                            name="New_Recertification_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        d.3 Unstageable: Suspected deep tissue injury in evolution.
                                                    </td>
                                                    <td>
                                                        <input id="New_Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"
                                                            name="New_Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="New_Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"
                                                            name="New_Recertification_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1322) Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable
                                            redness of a localized area usually over a bony prominence. The area may be painful,
                                            firm, soft, warmer or cooler as compared to adjacent tissue.
                                        </div>
                                    </div>
                                    <div class="insideCol">
                                        <div class="margin">
                                            <input name="New_Recertification_M1322CurrentNumberStageIUlcer" type="hidden" value=" " />
                                            <input name="New_Recertification_M1322CurrentNumberStageIUlcer" type="radio" value="0" />&nbsp;0
                                            <br />
                                            <input name="New_Recertification_M1322CurrentNumberStageIUlcer" type="radio" value="1" />&nbsp;1
                                            <br />
                                            <input name="New_Recertification_M1322CurrentNumberStageIUlcer" type="radio" value="2" />&nbsp;2<br />
                                        </div>
                                    </div>
                                    <div class="insideCol">
                                        <div class="margin">
                                            <input name="New_Recertification_M1322CurrentNumberStageIUlcer" type="radio" value="3" />&nbsp;3<br />
                                            <input name="New_Recertification_M1322CurrentNumberStageIUlcer" type="radio" value="4" />&nbsp;4
                                            or more
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1324) Stage of Most Problematic Unhealed (Observable) Pressure Ulcer:
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="New_Recertification_M1324MostProblematicUnhealedStage" type="hidden"
                                            value=" " />
                                        <input name="New_Recertification_M1324MostProblematicUnhealedStage" type="radio"
                                            value="1" />&nbsp;1 - Stage I<br />
                                        <input name="New_Recertification_M1324MostProblematicUnhealedStage" type="radio"
                                            value="2" />&nbsp;2 - Stage II<br />
                                        <input name="New_Recertification_M1324MostProblematicUnhealedStage" type="radio"
                                            value="3" />&nbsp;3 - Stage III<br />
                                        <input name="New_Recertification_M1324MostProblematicUnhealedStage" type="radio"
                                            value="4" />&nbsp;4 - Stage IV<br />
                                        <input name="New_Recertification_M1324MostProblematicUnhealedStage" type="radio"
                                            value="NA" />&nbsp;NA - No observable pressure ulcer or unhealed pressure ulcer
                                    </div>
                                </div>
                            </div>
                            <div class="insideCol">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1330) Does this patient have a Stasis Ulcer?
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="New_Recertification_M1330StasisUlcer" type="hidden" value=" " />
                                        <input name="New_Recertification_M1330StasisUlcer" type="radio" value="0" />&nbsp;0
                                        - No [ Go to M1340 ]
                                        <br />
                                        <input name="New_Recertification_M1330StasisUlcer" type="radio" value="1" />&nbsp;1
                                        - Yes, patient has BOTH observable and unobservable stasis ulcers
                                        <br />
                                        <input name="New_Recertification_M1330StasisUlcer" type="radio" value="2" />&nbsp;2
                                        - Yes, patient has observable stasis ulcers ONLY<br />
                                        <input name="New_Recertification_M1330StasisUlcer" type="radio" value="3" />&nbsp;3
                                        - Yes, patient has unobservable stasis ulcers ONLY (known but not observable due
                                        to non-removable dressing) [ Go to M1340 ]
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="new_recertification_M1332AndM1334">
                            <div class="insideCol">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1332) Current Number of (Observable) Stasis Ulcer(s):
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="New_Recertification_M1332CurrentNumberStasisUlcer" type="hidden" value=" " />
                                        <input name="New_Recertification_M1332CurrentNumberStasisUlcer" type="radio" value="1" />&nbsp;1
                                        - One<br />
                                        <input name="New_Recertification_M1332CurrentNumberStasisUlcer" type="radio" value="2" />&nbsp;2
                                        - Two<br />
                                        <input name="New_Recertification_M1332CurrentNumberStasisUlcer" type="radio" value="3" />&nbsp;3
                                        - Three<br />
                                        <input name="New_Recertification_M1332CurrentNumberStasisUlcer" type="radio" value="4" />&nbsp;4
                                        - Four or more<br />
                                    </div>
                                </div>
                            </div>
                            <div class="insideCol">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1334) Status of Most Problematic (Observable) Stasis Ulcer:
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="New_Recertification_M1334StasisUlcerStatus" type="hidden" value=" " />
                                        <input name="New_Recertification_M1334StasisUlcerStatus" type="radio" value="0" />&nbsp;0
                                        - Newly epithelialized
                                        <br />
                                        <input name="New_Recertification_M1334StasisUlcerStatus" type="radio" value="1" />&nbsp;1
                                        - Fully granulating
                                        <br />
                                        <input name="New_Recertification_M1334StasisUlcerStatus" type="radio" value="2" />&nbsp;2
                                        - Early/partial granulation<br />
                                        <input name="New_Recertification_M1334StasisUlcerStatus" type="radio" value="3" />&nbsp;3
                                        - Not healing
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1340) Does this patient have a Surgical Wound?
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="New_Recertification_M1340SurgicalWound" type="hidden" value=" " />
                                        <input name="New_Recertification_M1340SurgicalWound" type="radio" value="0" />&nbsp;0
                                        - No [ Go to M1350 ]<br />
                                        <input name="New_Recertification_M1340SurgicalWound" type="radio" value="1" />&nbsp;1
                                        - Yes, patient has at least one (observable) surgical wound<br />
                                        <input name="New_Recertification_M1340SurgicalWound" type="radio" value="2" />&nbsp;2
                                        - Surgical wound known but not observable due to non-removable dressing [ Go to
                                        M1350 ]
                                    </div>
                                </div>
                            </div>
                            <div class="insideCol" id="new_recertification_M1342">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1342) Status of Most Problematic (Observable) Surgical Wound:
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="New_Recertification_M1342SurgicalWoundStatus" type="hidden" value=" " />
                                        <input name="New_Recertification_M1342SurgicalWoundStatus" type="radio" value="0" />&nbsp;0
                                        - Newly epithelialized
                                        <br />
                                        <input name="New_Recertification_M1342SurgicalWoundStatus" type="radio" value="1" />&nbsp;1
                                        - Fully granulating
                                        <br />
                                        <input name="New_Recertification_M1342SurgicalWoundStatus" type="radio" value="2" />&nbsp;2
                                        - Early/partial granulation<br />
                                        <input name="New_Recertification_M1342SurgicalWoundStatus" type="radio" value="3" />&nbsp;3
                                        - Not healing
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div>
                                    <div class="insiderow title">
                                        <div class="margin">
                                            (M1350) Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy,
                                            other than those described above that is receiving intervention by the home health
                                            agency?
                                        </div>
                                    </div>
                                    <div class="margin">
                                        <input name="New_Recertification_M1350SkinLesionOpenWound" type="hidden" value=" " />
                                        <input name="New_Recertification_M1350SkinLesionOpenWound" type="radio" value="0" />&nbsp;0
                                        - No &nbsp;&nbsp;
                                        <input name="New_Recertification_M1350SkinLesionOpenWound" type="radio" value="1" />&nbsp;1
                                        - Yes
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="respiratorystatus_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationRespiratoryStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "New")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        <div class="margin">
                                            (M1400) When is the patient dyspneic or noticeably Short of Breath?
                                        </div>
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1400PatientDyspneic" type="hidden" value=" " />
                                    <input name="New_Recertification_M1400PatientDyspneic" type="radio" value="0" />&nbsp;0
                                    - Patient is not short of breath<br />
                                    <input name="New_Recertification_M1400PatientDyspneic" type="radio" value="1" />&nbsp;1
                                    - When walking more than 20 feet, climbing stairs<br />
                                    <input name="New_Recertification_M1400PatientDyspneic" type="radio" value="2" />&nbsp;2
                                    - With moderate exertion (e.g., while dressing, using commode or bedpan, walking
                                    distances less than 20 feet)<br />
                                    <input name="New_Recertification_M1400PatientDyspneic" type="radio" value="3" />&nbsp;3
                                    - With minimal exertion (e.g., while eating, talking, or performing other ADLs)
                                    or with agitation<br />
                                    <input name="New_Recertification_M1400PatientDyspneic" type="radio" value="4" />&nbsp;4
                                    - At rest (during day or night)<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="eliminationstatus_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationEliminationStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "New")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1610) Urinary Incontinence or Urinary Catheter Presence:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1610UrinaryIncontinence" type="hidden" value=" " />
                                    <input name="New_Recertification_M1610UrinaryIncontinence" type="radio" value="0" />&nbsp;0
                                    - No incontinence or catheter (includes anuria or ostomy for urinary drainage) [
                                    Go to M1620 ]<br />
                                    <input name="New_Recertification_M1610UrinaryIncontinence" type="radio" value="1" />&nbsp;1
                                    - Patient is incontinent<br />
                                    <input name="New_Recertification_M1610UrinaryIncontinence" type="radio" value="2" />&nbsp;2
                                    - Patient requires a urinary catheter (i.e., external, indwelling, intermittent,
                                    suprapubic) [ Go to M1620 ]
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1620) Bowel Incontinence Frequency:
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="New_Recertification_M1620BowelIncontinenceFrequency" type="hidden" value=" " />
                                        <input name="New_Recertification_M1620BowelIncontinenceFrequency" type="radio" value="0" />&nbsp;0
                                        - Very rarely or never has bowel incontinence<br />
                                        <input name="New_Recertification_M1620BowelIncontinenceFrequency" type="radio" value="1" />&nbsp;1
                                        - Less than once weekly<br />
                                        <input name="New_Recertification_M1620BowelIncontinenceFrequency" type="radio" value="2" />&nbsp;2
                                        - One to three times weekly<br />
                                        <input name="New_Recertification_M1620BowelIncontinenceFrequency" type="radio" value="3" />&nbsp;3
                                        - Four to six times weekly<br />
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="New_Recertification_M1620BowelIncontinenceFrequency" type="hidden" value=" " />
                                        <input name="New_Recertification_M1620BowelIncontinenceFrequency" type="radio" value="4" />&nbsp;4
                                        - On a daily basis<br />
                                        <input name="New_Recertification_M1620BowelIncontinenceFrequency" type="radio" value="5" />&nbsp;5
                                        - More often than once daily<br />
                                        <input name="New_Recertification_M1620BowelIncontinenceFrequency" type="radio" value="NA" />&nbsp;NA
                                        - Patient has ostomy for bowel elimination<br />
                                        <input name="New_Recertification_M1620BowelIncontinenceFrequency" type="radio" value="UK" />&nbsp;UK
                                        - Unknown [Omit “UK” option on FU, DC]
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1630) Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel
                                        elimination that (within the last 14 days): a) was related to an inpatient facility
                                        stay, or b) necessitated a change in medical or treatment regimen?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1630OstomyBowelElimination" type="hidden" value=" " />
                                    <input name="New_Recertification_M1630OstomyBowelElimination" type="radio" value="0" />&nbsp;0
                                    - Patient does not have an ostomy for bowel elimination.<br />
                                    <input name="New_Recertification_M1630OstomyBowelElimination" type="radio" value="1" />&nbsp;1
                                    - Patient's ostomy was not related to an inpatient stay and did not necessitate
                                    change in medical or treatment regimen.<br />
                                    <input name="New_Recertification_M1630OstomyBowelElimination" type="radio" value="2" />&nbsp;2
                                    - The ostomy was related to an inpatient stay or did necessitate change in medical
                                    or treatment regimen.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="adl_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationADLForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "New")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1810) Current Ability to Dress Upper Body safely (with or without dressing aids)
                                        including undergarments, pullovers, front-opening shirts and blouses, managing zippers,
                                        buttons, and snaps:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1810CurrentAbilityToDressUpper" type="hidden" value=" " />
                                    <input name="New_Recertification_M1810CurrentAbilityToDressUpper" type="radio" value="0" />&nbsp;0
                                    - Able to get clothes out of closets and drawers, put them on and remove them from
                                    the upper body without assistance.<br />
                                    <input name="New_Recertification_M1810CurrentAbilityToDressUpper" type="radio" value="1" />&nbsp;1
                                    - Able to dress upper body without assistance if clothing is laid out or handed
                                    to the patient.<br />
                                    <input name="New_Recertification_M1810CurrentAbilityToDressUpper" type="radio" value="2" />&nbsp;2
                                    - Someone must help the patient put on upper body clothing.<br />
                                    <input name="New_Recertification_M1810CurrentAbilityToDressUpper" type="radio" value="3" />&nbsp;3
                                    - Patient depends entirely upon another person to dress the upper body.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1820) Current Ability to Dress Lower Body safely (with or without dressing aids)
                                        including undergarments, slacks, socks or nylons, shoes:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1820CurrentAbilityToDressLower" type="hidden" value=" " />
                                    <input name="New_Recertification_M1820CurrentAbilityToDressLower" type="radio" value="0" />&nbsp;0
                                    - Able to obtain, put on, and remove clothing and shoes without assistance.<br />
                                    <input name="New_Recertification_M1820CurrentAbilityToDressLower" type="radio" value="1" />&nbsp;1
                                    - Able to dress lower body without assistance if clothing and shoes are laid out
                                    or handed to the patient.<br />
                                    <input name="New_Recertification_M1820CurrentAbilityToDressLower" type="radio" value="2" />&nbsp;2
                                    - Someone must help the patient put on undergarments, slacks, socks or nylons, and
                                    shoes.<br />
                                    <input name="New_Recertification_M1820CurrentAbilityToDressLower" type="radio" value="3" />&nbsp;3
                                    - Patient depends entirely upon another person to dress lower body.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing
                                        face, washing hands, and shampooing hair).
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1830CurrentAbilityToBatheEntireBody" type="hidden"
                                        value=" " />
                                    <input name="New_Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="0" />&nbsp;0 - Able to bathe self in shower or tub independently, including
                                    getting in and out of tub/shower.<br />
                                    <input name="New_Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="1" />&nbsp;1 - With the use of devices, is able to bathe self in shower
                                    or tub independently, including getting in and out of the tub/shower.<br />
                                    <input name="New_Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="2" />&nbsp;2 - Able to bathe in shower or tub with the intermittent assistance
                                    of another person:<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) for intermittent supervision or encouragement
                                    or reminders, OR
                                    <br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) to get in and out of the shower or
                                    tub, OR<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c) for washing difficult to reach areas.<br />
                                    <input name="New_Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="3" />&nbsp;3 - Able to participate in bathing self in shower or tub,
                                    but requires presence of another person throughout the bath for assistance or supervision.<br />
                                    <input name="New_Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="4" />&nbsp;4 - Unable to use the shower or tub, but able to bathe self
                                    independently with or without the use of devices at the sink, in chair, or on commode.<br />
                                    <input name="New_Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="5" />&nbsp;5 - Unable to use the shower or tub, but able to participate
                                    in bathing self in bed, at the sink, in bedside chair, or on commode, with the assistance
                                    or supervision of another person throughout the bath.<br />
                                    <input name="New_Recertification_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="6" />&nbsp;6 - Unable to participate effectively in bathing and is bathed
                                    totally by another person.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside
                                        commode safely and transfer on and off toilet/commode.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1840ToiletTransferring" type="hidden" value=" " />
                                    <input name="New_Recertification_M1840ToiletTransferring" type="radio" value="0" />&nbsp;0
                                    - Able to get to and from the toilet and transfer independently with or without
                                    a device.<br />
                                    <input name="New_Recertification_M1840ToiletTransferring" type="radio" value="1" />&nbsp;1
                                    - When reminded, assisted, or supervised by another person, able to get to and from
                                    the toilet and transfer.<br />
                                    <input name="New_Recertification_M1840ToiletTransferring" type="radio" value="2" />&nbsp;2
                                    - Unable to get to and from the toilet but is able to use a bedside commode (with
                                    or without assistance).<br />
                                    <input name="New_Recertification_M1840ToiletTransferring" type="radio" value="3" />&nbsp;3
                                    - Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal
                                    independently.<br />
                                    <input name="New_Recertification_M1840ToiletTransferring" type="radio" value="4" />&nbsp;4
                                    - Is totally dependent in toileting.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1850) Transferring: Current ability to move safely from bed to chair, or ability
                                        to turn and position self in bed if patient is bedfast.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1850Transferring" type="hidden" value=" " />
                                    <input name="New_Recertification_M1850Transferring" type="radio" value="0" />&nbsp;0
                                    - Able to independently transfer.<br />
                                    <input name="New_Recertification_M1850Transferring" type="radio" value="1" />&nbsp;1
                                    - Able to transfer with minimal human assistance or with use of an assistive device.<br />
                                    <input name="New_Recertification_M1850Transferring" type="radio" value="2" />&nbsp;2
                                    - Able to bear weight and pivot during the transfer process but unable to transfer
                                    self.<br />
                                    <input name="New_Recertification_M1850Transferring" type="radio" value="3" />&nbsp;3
                                    - Unable to transfer self and is unable to bear weight or pivot when transferred
                                    by another person.<br />
                                    <input name="New_Recertification_M1850Transferring" type="radio" value="4" />&nbsp;4
                                    - Bedfast, unable to transfer but is able to turn and position self in bed.<br />
                                    <input name="New_Recertification_M1850Transferring" type="radio" value="5" />&nbsp;5
                                    - Bedfast, unable to transfer and is unable to turn and position self.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing
                                        position, or use a wheelchair, once in a seated position, on a variety of surfaces.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M1860AmbulationLocomotion" type="hidden" value=" " />
                                    <input name="New_Recertification_M1860AmbulationLocomotion" type="radio" value="0" />&nbsp;0
                                    - Able to independently walk on even and uneven surfaces and negotiate stairs with
                                    or without railings (i.e., needs no human assistance or assistive device).<br />
                                    <input name="New_Recertification_M1860AmbulationLocomotion" type="radio" value="1" />&nbsp;1
                                    - With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able
                                    to independently walk on even and uneven surfaces and negotiate stairs with or without
                                    railings.<br />
                                    <input name="New_Recertification_M1860AmbulationLocomotion" type="radio" value="2" />&nbsp;2
                                    - Requires use of a two-handed device (e.g., walker or crutches) to walk alone on
                                    a level surface and/or requires human supervision or assistance to negotiate stairs
                                    or steps or uneven surfaces.<br />
                                    <input name="New_Recertification_M1860AmbulationLocomotion" type="radio" value="3" />&nbsp;3
                                    - Able to walk only with the supervision or assistance of another person at all
                                    times.<br />
                                    <input name="New_Recertification_M1860AmbulationLocomotion" type="radio" value="4" />&nbsp;4
                                    - Chairfast, unable to ambulate but is able to wheel self independently.<br />
                                    <input name="New_Recertification_M1860AmbulationLocomotion" type="radio" value="5" />&nbsp;5
                                    - Chairfast, unable to ambulate and is unable to wheel self.<br />
                                    <input name="New_Recertification_M1860AmbulationLocomotion" type="radio" value="6" />&nbsp;6
                                    - Bedfast, unable to ambulate or be up in a chair.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="medications_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationMedicationsForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "New")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2030) Management of Injectable Medications: Patient's current ability to prepare
                                        and take all prescribed injectable medications reliably and safely, including administration
                                        of correct dosage at the appropriate times/intervals. Excludes IV medications.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="New_Recertification_M2030ManagementOfInjectableMedications" type="hidden"
                                        value=" " />
                                    <input name="New_Recertification_M2030ManagementOfInjectableMedications" type="radio"
                                        value="0" />&nbsp;0 - Able to independently take the correct medication(s) and
                                    proper dosage(s) at the correct times.<br />
                                    <input name="New_Recertification_M2030ManagementOfInjectableMedications" type="radio"
                                        value="1" />&nbsp;1 - Able to take injectable medication(s) at the correct times
                                    if:<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) individual syringes are prepared in
                                    advance by another person; OR<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) another person develops a drug diary
                                    or chart.<br />
                                    <input name="New_Recertification_M2030ManagementOfInjectableMedications" type="radio"
                                        value="2" />&nbsp;2 - Able to take medication(s) at the correct times if given
                                    reminders by another person based on the frequency of the injection<br />
                                    <input name="New_Recertification_M2030ManagementOfInjectableMedications" type="radio"
                                        value="3" />&nbsp;3 - Unable to take injectable medication unless administered
                                    by another person.<br />
                                    <input name="New_Recertification_M2030ManagementOfInjectableMedications" type="radio"
                                        value="NA" />&nbsp;NA - No injectable medications prescribed.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="therapyneed_recertification" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationTherapyNeedForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("Recertification_Id", "")%>
                        <%= Html.Hidden("Recertification_Action", "New")%>
                        <%= Html.Hidden("Recertification_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "Recertification")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2200) Therapy Need: In the home health plan of care for the Medicare payment episode
                                        for which this assessment will define a case mix group, what is the indicated need
                                        for therapy visits (total of reasonable and necessary physical, occupational, and
                                        speech-language pathology visits combined)? (Enter zero [ “000” ] if no therapy
                                        visits indicated.)
                                    </div>
                                </div>
                                <div class="margin">
                                    <input type="text" name="New_Recertification_M2200NumberOfTherapyNeed" id="New_Recertification_M2200NumberOfTherapyNeed" />&nbsp;Number
                                    of therapy visits indicated (total of physical, occupational and speech-language
                                    pathology combined).<br />
                                    <input name="New_Recertification_M2200TherapyNeed" type="hidden" value=" " />
                                    <input name="New_Recertification_M2200TherapyNeed" type="checkbox" value="NA" />&nbsp;NA
                                    - Not Applicable: No case mix group defined by this assessment.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save" class="SaveContinue" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this),'New');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
<% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/Recertification.js"))
       .OnDocumentReady(() =>
        {%>
Recertification.Init();
<%}); 
%>