﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationEliminationForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] genericGU = data.ContainsKey("GenericGU") && data["GenericGU"].Answer != "" ? data["GenericGU"].Answer.Split(',') : null; %>
        <tr>
            <th>
                GU
            </th>
            <th>
                Digestive
            </th>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input type="hidden" name="Recertification_GenericGU" value=" " />
                        <input name="Recertification_GenericGU" value="1" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        WNL (Within Normal Limits) </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericGU" value="2" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Incontinence </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericGU" value="3" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Bladder Distention </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericGU" value="4" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Burning </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericGU" value="5" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Frequency </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericGU" value="6" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("6")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Dysuria </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericGU" value="7" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("7")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Retention </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericGU" value="8" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("8")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Urgency </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericGU" value="9" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("9")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Urostomy </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericGU" value="10" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("10")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Catheter: </li>
                    <li>
                        <%var genericGUCatheterList = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "Foley", Value = "Foley" },
                   new SelectListItem { Text = "Suprapubic", Value = "Suprapubic"} 
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericGUCatheterList") && data["GenericGUCatheterList"].Answer != "" ? data["GenericGUCatheterList"].Answer : "0");%>
                        <%= Html.DropDownList("Recertification_GenericGUCatheterList", genericGUCatheterList)%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>Last Changed
                        <%=Html.TextBox("Recertification_GenericGUCatheterLastChanged", data.ContainsKey("GenericGUCatheterLastChanged") ? data["GenericGUCatheterLastChanged"].Answer : "", new { @id = "Recertification_GenericGUCatheterLastChanged", @size = "10", @maxlength = "10" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericGUCatheterFrequency", data.ContainsKey("GenericGUCatheterFrequency") ? data["GenericGUCatheterFrequency"].Answer : "", new { @id = "Recertification_GenericGUCatheterFrequency", @size = "5", @maxlength = "5" })%>
                        Fr </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericGUCatheterAmount", data.ContainsKey("GenericGUCatheterAmount") ? data["GenericGUCatheterAmount"].Answer : "", new { @id = "Recertification_GenericGUCatheterAmount", @size = "5", @maxlength = "5" })%>
                        cc </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericGU" value="11" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("11")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Urine: </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <%string[] genericGUUrine = data.ContainsKey("GenericGUUrine") && data["GenericGUUrine"].Answer != "" ? data["GenericGUUrine"].Answer.Split(',') : null; %>
                        <input type="hidden" name="Recertification_GenericGUUrine" value=" " />
                        <input name="Recertification_GenericGUUrine" value="1" type="checkbox" '<% if( genericGUUrine!=null && genericGUUrine.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Cloudy
                        <br />
                        <input name="Recertification_GenericGUUrine" value="2" type="checkbox" '<% if( genericGUUrine!=null && genericGUUrine.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Odorous
                        <br />
                        <input name="Recertification_GenericGUUrine" value="3" type="checkbox" '<% if( genericGUUrine!=null && genericGUUrine.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Sediment
                        <br />
                        <input name="Recertification_GenericGUUrine" value="4" type="checkbox" '<% if( genericGUUrine!=null && genericGUUrine.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Hematuria
                        <br />
                        <input name="Recertification_GenericGUUrine" value="5" type="checkbox" '<% if( genericGUUrine!=null && genericGUUrine.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Other
                        <%=Html.TextBox("Recertification_GenericGUOtherText", data.ContainsKey("GenericGUOtherText") ? data["GenericGUOtherText"].Answer : "", new { @id = "Recertification_GenericGUOtherText", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericGU" value="12" type="checkbox" '<% if( genericGU!=null && genericGU.Contains("12")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        External Genitalia: </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericGUNormal", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericGUNormal", "1", data.ContainsKey("GenericGUNormal") && data["GenericGUNormal"].Answer == "1" ? true : false, new { @id = "" })%>
                        Normal
                        <br />
                        <%=Html.RadioButton("Recertification_GenericGUNormal", "0", data.ContainsKey("GenericGUNormal") && data["GenericGUNormal"].Answer == "0" ? true : false, new { @id = "" })%>
                        Abnormal
                        <br />
                        As per:<br />
                        <%=Html.Hidden("Recertification_GenericGUClinicalAssessment", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericGUClinicalAssessment", "1", data.ContainsKey("GenericGUClinicalAssessment") && data["GenericGUClinicalAssessment"].Answer == "1" ? true : false, new { @id = "" })%>
                        Clinician Assessment
                        <br />
                        <%=Html.RadioButton("Recertification_GenericGUClinicalAssessment", "0", data.ContainsKey("GenericGUClinicalAssessment") && data["GenericGUClinicalAssessment"].Answer == "0" ? true : false, new { @id = "" })%>
                        Pt/CG Report </li>
                </ul>
            </td>
            <td>
                <%string[] genericDigestive = data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer != "" ? data["GenericDigestive"].Answer.Split(',') : null; %>
                <ul>
                    <li>
                        <input type="hidden" name="Recertification_GenericDigestive" name=" " />
                        <input name="Recertification_GenericDigestive" value="1" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        WNL </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericDigestive" value="2" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Nausea/Vomiting </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericDigestive" value="3" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        NPO </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericDigestive" value="4" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Reflux/Indigestion </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericDigestive" value="5" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Diarrhea </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericDigestive" value="6" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("6")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Constipation </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericDigestive" value="7" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("7")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Bowel Incontinence </li>
                </ul>
                <ul>
                    <li>
                        <input name="Recertification_GenericDigestive" value="8" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("8")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Bowel Sounds: </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericDigestiveBowelSoundsType", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericDigestiveBowelSoundsType", "Hyperactive", data.ContainsKey("GenericDigestiveBowelSoundsType") && data["GenericDigestiveBowelSoundsType"].Answer == "Hyperactive" ? true : false, new { @id = "" })%>
                        Hyperactive
                        <br />
                        <%=Html.RadioButton("Recertification_GenericDigestiveBowelSoundsType", "Hypoactive", data.ContainsKey("GenericDigestiveBowelSoundsType") && data["GenericDigestiveBowelSoundsType"].Answer == "Hypoactive" ? true : false, new { @id = "" })%>
                        Hypoactive
                        <br />
                        <%=Html.RadioButton("Recertification_GenericDigestiveBowelSoundsType", "Normal", data.ContainsKey("GenericDigestiveBowelSoundsType") && data["GenericDigestiveBowelSoundsType"].Answer == "Normal" ? true : false, new { @id = "" })%>
                        Normal
                        <br />
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">
                        <input name="Recertification_GenericDigestive" value="9" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("9")  ){ %>checked="checked"<% }%>'" />
                        Abd Girth: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericDigestiveAbdGirthLength", data.ContainsKey("GenericDigestiveAbdGirthLength") ? data["GenericDigestiveAbdGirthLength"].Answer : "", new { @id = "Recertification_GenericDigestiveAbdGirthLength", @size = "5", @maxlength = "5" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">
                        <input name="Recertification_GenericDigestive" value="10" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("10")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Last BM: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericDigestiveLastBMDate", data.ContainsKey("GenericDigestiveLastBMDate") ? data["GenericDigestiveLastBMDate"].Answer : "", new { @id = "Recertification_GenericDigestiveLastBMDate", @size = "10", @maxlength = "10" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>As per:
                        <%=Html.Hidden("Recertification_GenericDigestiveLastBMAsper", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericDigestiveLastBMAsper", "Clinician Assessment", data.ContainsKey("GenericDigestiveLastBMAsper") && data["GenericDigestiveLastBMAsper"].Answer == "Clinician Assessment" ? true : false, new { @id = "" })%>
                        Clinician Assessment
                        <%=Html.RadioButton("Recertification_GenericDigestiveLastBMAsper", "Pt/CG Report", data.ContainsKey("GenericDigestiveLastBMAsper") && data["GenericDigestiveLastBMAsper"].Answer == "Pt/CG Report" ? true : false, new { @id = "" })%>
                        Pt/CG Report </li>
                </ul>
                <ul class="columns">
                    <%string[] genericDigestiveLastBM = data.ContainsKey("GenericDigestiveLastBM") && data["GenericDigestiveLastBM"].Answer != "" ? data["GenericDigestiveLastBM"].Answer.Split(',') : null; %>
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <input type="hidden" name="Recertification_GenericDigestiveLastBM" value=" " />
                        <input name="Recertification_GenericDigestiveLastBM" value="1" type="checkbox" '<% if(  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        WNL </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <input name="Recertification_GenericDigestiveLastBM" value="2" type="checkbox" '<% if(  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Abnormal Stool: </li>
                    <li>
                        <%string[] genericDigestiveLastBMAbnormalStool = data.ContainsKey("GenericDigestiveLastBMAbnormalStool") && data["GenericDigestiveLastBMAbnormalStool"].Answer != "" ? data["GenericDigestiveLastBMAbnormalStool"].Answer.Split(',') : null; %>
                        <input type="hidden" name="Recertification_GenericDigestiveLastBMAbnormalStool" value=" " />
                        <input name="Recertification_GenericDigestiveLastBMAbnormalStool" value="1" type="checkbox" '<% if(  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Gray </li>
                    <li>
                        <input name="Recertification_GenericDigestiveLastBMAbnormalStool" value="2" type="checkbox" '<% if(  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Tarry </li>
                    <li>
                        <input name="Recertification_GenericDigestiveLastBMAbnormalStool" value="3" type="checkbox" '<% if(  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Fresh Blood </li>
                    <li>
                        <input name="Recertification_GenericDigestiveLastBMAbnormalStool" value="4" type="checkbox" '<% if(  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Black </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <input name="Recertification_GenericDigestiveLastBM" value="3" type="checkbox" '<% if(  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Constipation: </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericDigestiveLastBMConstipationType", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericDigestiveLastBMConstipationType", "Chronic", data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Chronic" ? true : false, new { @id = "" })%>
                        Chronic </li>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericDigestiveLastBMConstipationType", "Acute", data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Acute" ? true : false, new { @id = "" })%>
                        Acute </li>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericDigestiveLastBMConstipationType", "Occasional", data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Occasional" ? true : false, new { @id = "" })%>
                        Occasional </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <input name="Recertification_GenericDigestiveLastBM" value="4" type="checkbox" '<% if(  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Lax/Enema Use: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericDigestiveLaxEnemaUseDesc", data.ContainsKey("GenericDigestiveLaxEnemaUseDesc") ? data["GenericDigestiveLaxEnemaUseDesc"].Answer : "", new { @id = "Recertification_GenericDigestiveLaxEnemaUseDesc", @size = "15", @maxlength = "15" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <input name="Recertification_GenericDigestiveLastBM" value="5" type="checkbox" '<% if(  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Hemorrhoids: </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericDigestiveHemorrhoidsType", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericDigestiveHemorrhoidsType", "Internal", data.ContainsKey("GenericDigestiveHemorrhoidsType") && data["GenericDigestiveHemorrhoidsType"].Answer == "Internal" ? true : false, new { @id = "" })%>
                        <i>Internal</i> </li>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericDigestiveHemorrhoidsType", "External", data.ContainsKey("GenericDigestiveHemorrhoidsType") && data["GenericDigestiveHemorrhoidsType"].Answer == "External" ? true : false, new { @id = "" })%>
                        <i>External</i> </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericDigestive" value="11" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("11") ){ %>checked="checked"<% }%>'" />&nbsp;
                        Ostomy: </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>Ostomy Type(s):
                        <%=Html.TextBox("Recertification_GenericDigestiveOstomyType", data.ContainsKey("GenericDigestiveOstomyType") ? data["GenericDigestiveOstomyType"].Answer : "", new { @id = "Recertification_GenericDigestiveOstomyType", @size = "25", @maxlength = "25" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <%string[] genericDigestiveOstomy = data.ContainsKey("GenericDigestiveOstomy") && data["GenericDigestiveOstomy"].Answer != "" ? data["GenericDigestiveOstomy"].Answer.Split(',') : null; %>
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <input type="hidden" name="Recertification_GenericDigestiveOstomy" value=" " />
                        <input name="Recertification_GenericDigestiveOstomy" value="1" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("1") ){ %>checked="checked"<% }%>'" />&nbsp;
                        Stoma Appearance: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericDigestiveStomaType", data.ContainsKey("GenericDigestiveStomaType") ? data["GenericDigestiveStomaType"].Answer : "", new { @id = "Recertification_GenericDigestiveStomaType", @size = "15", @maxlength = "15" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <input name="Recertification_GenericDigestiveOstomy" value="2" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("2") ){ %>checked="checked"<% }%>'" />&nbsp;
                        Stool Appearance: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericDigestiveStoolAppearanceDesc", data.ContainsKey("GenericDigestiveStoolAppearanceDesc") ? data["GenericDigestiveStoolAppearanceDesc"].Answer : "", new { @id = "Recertification_GenericDigestiveStoolAppearanceDesc", @size = "15", @maxlength = "15" })%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <input name="Recertification_GenericDigestiveOstomy" value="3" type="checkbox" '<% if( genericDigestive!=null && genericDigestive.Contains("3") ){ %>checked="checked"<% }%>'" />&nbsp;
                        Surrounding Skin: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericDigestiveSurSkinType", data.ContainsKey("GenericDigestiveSurSkinType") ? data["GenericDigestiveSurSkinType"].Answer : "", new { @id = "Recertification_GenericDigestiveSurSkinType", @size = "15", @maxlength = "15" })%>
                    </li>
                    <li>
                        <input type="hidden" name="Recertification_GenericDigestiveSurSkinIntact" id="Recertification_GenericDigestiveSurSkinIntact" />
                        <input name="Recertification_GenericDigestiveSurSkinIntact" value="1" type="checkbox" '<% if(data.ContainsKey("GenericDigestiveSurSkinIntact") && data["GenericDigestiveSurSkinIntact"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;
                        Intact </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ul>
                    <li>Comments:<br />
                        <%=Html.TextArea("Recertification_GenericGUDigestiveComments", data.ContainsKey("GenericGUDigestiveComments") ? data["GenericGUDigestiveComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericGUDigestiveComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1610) Urinary Incontinence or Urinary Catheter Presence:
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("Recertification_M1610UrinaryIncontinence", " ", new { @id = "" })%>
            <%=Html.RadioButton("Recertification_M1610UrinaryIncontinence", "00", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - No incontinence or catheter (includes anuria or ostomy for urinary drainage)
            [ Go to M1620 ]<br />
            <%=Html.RadioButton("Recertification_M1610UrinaryIncontinence", "01", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Patient is incontinent<br />
            <%=Html.RadioButton("Recertification_M1610UrinaryIncontinence", "02", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Patient requires a urinary catheter (i.e., external, indwelling, intermittent,
            suprapubic) [ Go to M1620 ]
        </div>
    </div>
</div>

<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1620) Bowel Incontinence Frequency:
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.Hidden("Recertification_M1620BowelIncontinenceFrequency", " ", new { @id = "" })%>
                <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "00", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Very rarely or never has bowel incontinence<br />
                <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "01", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Less than once weekly<br />
                <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "02", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - One to three times weekly<br />
                <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "03", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Four to six times weekly<br />
            </div>
        </div>
        <div class="insideCol">
            <div class="padding">
                <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "04", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - On a daily basis<br />
                <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "05", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - More often than once daily<br />
                <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "NA", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
                - Patient has ostomy for bowel elimination<br />
                <%=Html.RadioButton("Recertification_M1620BowelIncontinenceFrequency", "UK", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "UK" ? true : false, new { @id = "" })%>&nbsp;UK
                - Unknown [Omit “UK” option on FU, DC]
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1630) Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel
                elimination that (within the last 14 days): a) was related to an inpatient facility
                stay, or b) necessitated a change in medical or treatment regimen?
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("Recertification_M1630OstomyBowelElimination", " ", new { @id = "" })%>
            <%=Html.RadioButton("Recertification_M1630OstomyBowelElimination", "00", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Patient does not have an ostomy for bowel elimination.<br />
            <%=Html.RadioButton("Recertification_M1630OstomyBowelElimination", "01", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Patient's ostomy was not related to an inpatient stay and did not necessitate
            change in medical or treatment regimen.<br />
            <%=Html.RadioButton("Recertification_M1630OstomyBowelElimination", "02", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - The ostomy was related to an inpatient stay or did necessitate change in medical
            or treatment regimen.
        </div>
    </div>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>
                Dialysis
            </th>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Is patient on dialysis? </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericPatientOnDialysis", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericPatientOnDialysis", "1", data.ContainsKey("GenericPatientOnDialysis") && data["GenericPatientOnDialysis"].Answer == "1" ? true : false, new { @id = "" })%>
                        Yes </li>
                    <li>
                        <%=Html.RadioButton("Recertification_GenericPatientOnDialysis", "0", data.ContainsKey("GenericPatientOnDialysis") && data["GenericPatientOnDialysis"].Answer == "0" ? true : false, new { @id = "" })%>
                        No </li>
                </ul>
            </td>
        </tr>
        <%string[] genericDialysis = data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer != "" ? data["GenericDialysis"].Answer.Split(',') : null; %>
        <tr>
            <td>
                <ul>
                    <li>
                        <input type="hidden" name="Recertification_GenericDialysis" value=" " />
                        <input name="Recertification_GenericDialysis" value="1" type="checkbox" '<% if( genericDialysis!=null && genericDialysis.Contains("1") ){ %>checked="checked"<% }%>'" />
                        Hemodialysis </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">
                        <input name="Recertification_GenericDialysis" value="2" type="checkbox" '<% if( genericDialysis!=null && genericDialysis.Contains("2") ){ %>checked="checked"<% }%>'" />
                        AV Graft / Fistula Site: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericAVGraftSite", data.ContainsKey("GenericAVGraftSite") ? data["GenericAVGraftSite"].Answer : "", new { @id = "Recertification_GenericAVGraftSite", @size = "25", @maxlength = "25" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">
                        <input name="Recertification_GenericDialysis" value="3" type="checkbox" '<% if( genericDialysis!=null && genericDialysis.Contains("3") ){ %>checked="checked"<% }%>'" />
                        Central Venous Catheter Access Site: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericVenousCatheterSite", data.ContainsKey("GenericVenousCatheterSite") ? data["GenericVenousCatheterSite"].Answer : "", new { @id = "Recertification_GenericVenousCatheterSite", @size = "25", @maxlength = "25" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_GenericDialysis" value="4" type="checkbox" '<% if( genericDialysis!=null && genericDialysis.Contains("4") ){ %>checked="checked"<% }%>'" />
                        Peritoneal Dialysis </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_GenericDialysis" value="5" type="checkbox" '<% if( genericDialysis!=null && genericDialysis.Contains("5") ){ %>checked="checked"<% }%>'" />
                        CCPD (Continuous Cyclic Peritoneal Dialysis) </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_GenericDialysis" value="6" type="checkbox" '<% if( genericDialysis!=null && genericDialysis.Contains("6") ){ %>checked="checked"<% }%>'" />
                        IPD (Intermittent Peritoneal Dialysis) </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_GenericDialysis" value="7" type="checkbox" '<% if( genericDialysis!=null && genericDialysis.Contains("7") ){ %>checked="checked"<% }%>'" />
                        CAPD (Continuous Ambulatory Peritoneal Dialysis) </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_GenericDialysis" value="8" type="checkbox" '<% if( genericDialysis!=null && genericDialysis.Contains("8") ){ %>checked="checked"<% }%>'" />
                        Catheter site free from signs and symptoms of infection </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">
                        <input name="Recertification_GenericDialysis" value="9" type="checkbox" '<% if( genericDialysis!=null && genericDialysis.Contains("9") ){ %>checked="checked"<% }%>'" />
                        Other: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericDialysisOtherDesc", data.ContainsKey("GenericDialysisOtherDesc") ? data["GenericDialysisOtherDesc"].Answer : "", new { @id = "Recertification_GenericDialysisOtherDesc", @size = "25", @maxlength = "25" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Dialysis Center:</li><li>
                        <%=Html.TextBox("Recertification_GenericDialysisCenter", data.ContainsKey("GenericDialysisCenter") ? data["GenericDialysisCenter"].Answer : "", new { @id = "Recertification_GenericDialysisCenter", @size = "25", @maxlength = "25" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Phone Number:</li><li>
                        <%=Html.TextBox("Recertification_GenericDialysisCenterPhone", data.ContainsKey("GenericDialysisCenterPhone") ? data["GenericDialysisCenterPhone"].Answer : "", new { @id = "Recertification_GenericDialysisCenterPhone", @size = "25", @maxlength = "25" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Contact Person:</li><li>
                        <%=Html.TextBox("Recertification_GenericDialysisCenterContact", data.ContainsKey("GenericDialysisCenterContact") ? data["GenericDialysisCenterContact"].Answer : "", new { @id = "Recertification_GenericDialysisCenterContact", @size = "25", @maxlength = "25" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <%string[] eliminationStatusInterventions = data.ContainsKey("485EliminationStatusInterventions") && data["485EliminationStatusInterventions"].Answer != "" ? data["485EliminationStatusInterventions"].Answer.Split(',') : null; %>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485EliminationStatusInterventions" value=" " />
                <input name="Recertification_485EliminationStatusInterventions" value="1" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("1") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient on bladder training program, including timed voiding
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="2" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("2") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructUTISymptomsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructUTISymptomsPerson") && data["485InstructUTISymptomsPerson"].Answer != "" ? data["485InstructUTISymptomsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructUTISymptomsPerson", instructUTISymptomsPerson)%>
                on signs/symptoms of UTI to report to MD/SN. SN may obtain urinalysis and urine
                culture & sensitivity (C&S) test as needed for signs/symptoms of UTI, to include
                pain, foul odor, cloudy or blood-tinged urine and fever
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="3" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("3") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to change foley catheter with
                <%=Html.TextBox("Recertification_485ChangeFoleyCatheterWith", data.ContainsKey("485ChangeFoleyCatheterWith") ? data["485ChangeFoleyCatheterWith"].Answer : "", new { @id = "Recertification_485ChangeFoleyCatheterWith", @size = "5", @maxlength = "5" })%>
                Fr
                <%=Html.TextBox("Recertification_485ChangeFoleyCatheterQuantity", data.ContainsKey("485ChangeFoleyCatheterQuantity") ? data["485ChangeFoleyCatheterQuantity"].Answer : "", new { @id = "Recertification_485ChangeFoleyCatheterQuantity", @size = "5", @maxlength = "5" })%>
                cc catheter every
                <%=Html.TextBox("Recertification_485ChangeFoleyCatheterFrequency", data.ContainsKey("485ChangeFoleyCatheterFrequency") ? data["485ChangeFoleyCatheterFrequency"].Answer : "", new { @id = "Recertification_485ChangeFoleyCatheterFrequency", @size = "5", @maxlength = "5" })%>
                beginning on
                <%=Html.TextBox("Recertification_485ChangeFoleyCatheterDate", data.ContainsKey("485ChangeFoleyCatheterDate") ? data["485ChangeFoleyCatheterDate"].Answer : "", new { @id = "Recertification_485ChangeFoleyCatheterDate", @size = "5", @maxlength = "5" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="4" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("4") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to change suprapubic tube with
                <%=Html.TextBox("Recertification_485ChangeSuprapubicTubeWith", data.ContainsKey("485ChangeSuprapubicTubeWith") ? data["485ChangeSuprapubicTubeWith"].Answer : "", new { @id = "Recertification_485ChangeSuprapubicTubeWith", @size = "5", @maxlength = "5" })%>
                Fr
                <%=Html.TextBox("Recertification_485ChangeSuprapubicTubeQuantity", data.ContainsKey("485ChangeSuprapubicTubeQuantity") ? data["485ChangeSuprapubicTubeQuantity"].Answer : "", new { @id = "Recertification_485ChangeSuprapubicTubeQuantity", @size = "5", @maxlength = "5" })%>
                cc catheter every
                <%=Html.TextBox("Recertification_485ChangeSuprapubicTubeFrequency", data.ContainsKey("485ChangeSuprapubicTubeFrequency") ? data["485ChangeSuprapubicTubeFrequency"].Answer : "", new { @id = "Recertification_485ChangeSuprapubicTubeFrequency", @size = "5", @maxlength = "5" })%>
                beginning on
                <%=Html.TextBox("Recertification_485ChangeSuprapubicTubeDate", data.ContainsKey("485ChangeSuprapubicTubeDate") ? data["485ChangeSuprapubicTubeDate"].Answer : "", new { @id = "Recertification_485ChangeSuprapubicTubeDate", @size = "5", @maxlength = "5" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="5" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("5") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to irrigate suprapubic tube with 100-250cc of sterile normal saline as needed
                for blockage, leakage
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="6" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("6") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to irrigate foley with 100-250cc of sterile normal saline as needed for blockage,
                leakage
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="7" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("7") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructProperFoleyPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructProperFoleyPerson") && data["485InstructProperFoleyPerson"].Answer != "" ? data["485InstructProperFoleyPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructProperFoleyPerson", instructProperFoleyPerson)%>
                on proper foley care
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="8" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("8") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to allow
                <%=Html.TextBox("Recertification_485AllowFoleyVisitsAmount", data.ContainsKey("485AllowFoleyVisitsAmount") ? data["485AllowFoleyVisitsAmount"].Answer : "", new { @id = "Recertification_485AllowFoleyVisitsAmount", @size = "15", @maxlength = "15" })%>
                additional visits for dislodgement, blockage, or leakage of foley or drainage system
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="9" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("9") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient/caregiver on ostomy management as follows:
                <%=Html.TextBox("Recertification_485InstructOstomyManagementDesc", data.ContainsKey("485InstructOstomyManagementDesc") ? data["485InstructOstomyManagementDesc"].Answer : "", new { @id = "Recertification_485InstructOstomyManagementDesc", @size = "40", @maxlength = "50" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="10" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("10") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to perform ostomy care as follows:
                <%=Html.TextBox("Recertification_485PerformOstomyCareDesc", data.ContainsKey("485PerformOstomyCareDesc") ? data["485PerformOstomyCareDesc"].Answer : "", new { @id = "Recertification_485PerformOstomyCareDesc", @size = "40", @maxlength = "50" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="11" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("11") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to digitally disimpact patient for constipation unrelieved by medications for
                <%=Html.TextBox("Recertification_485DigitalDisimpactDays", data.ContainsKey("485DigitalDisimpactDays") ? data["485DigitalDisimpactDays"].Answer : "", new { @id = "Recertification_485DigitalDisimpactDays", @size = "3", @maxlength = "3" })%>
                days
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="12" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("12") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct
                <%var instructMeasureIntakeOutputPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructMeasureIntakeOutputPerson") && data["485InstructMeasureIntakeOutputPerson"].Answer != "" ? data["485InstructMeasureIntakeOutputPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructMeasureIntakeOutputPerson", instructMeasureIntakeOutputPerson)%>
                on measuring and recording intake and output
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="13" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("13") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient to increase activity to alleviate constipation
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="14" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("14") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to administer enema
                <%=Html.TextBox("Recertification_485AdministerEnemaType", data.ContainsKey("485AdministerEnemaType") ? data["485AdministerEnemaType"].Answer : "", new { @id = "Recertification_485AdministerEnemaType", @size = "15", @maxlength = "15" })%>
                if no bowel movement in
                <%=Html.TextBox("Recertification_485AdministerEnemaDays", data.ContainsKey("485AdministerEnemaDays") ? data["485AdministerEnemaDays"].Answer : "", new { @id = "Recertification_485AdministerEnemaDays", @size = "3", @maxlength = "3" })%>
                days
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="15" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("15") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructConstipationSignsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructConstipationSignsPerson") && data["485InstructConstipationSignsPerson"].Answer != "" ? data["485InstructConstipationSignsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructConstipationSignsPerson", instructConstipationSignsPerson)%>
                on signs and symptoms of constipation to report to SN or physician
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="16" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("16") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructAcidRefluxFoodsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructAcidRefluxFoodsPerson") && data["485InstructAcidRefluxFoodsPerson"].Answer != "" ? data["485InstructAcidRefluxFoodsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructAcidRefluxFoodsPerson", instructAcidRefluxFoodsPerson)%>
                on foods that contribute to acid reflux/indigestion
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusInterventions" value="17" type="checkbox" '<% if( eliminationStatusInterventions!=null && eliminationStatusInterventions.Contains("17") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient not to eat 4 hours before bedtime to reduce acid reflux/indigestion
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Orders: &nbsp;
                <%var eliminationInterventionTemplates = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "-----------", Value = "-2" },
                   new SelectListItem { Text = "Erase", Value = "-1"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485EliminationInterventionTemplates") && data["485EliminationInterventionTemplates"].Answer != "" ? data["485EliminationInterventionTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485EliminationInterventionTemplates", eliminationInterventionTemplates)%>
                <%=Html.TextArea("Recertification_485EliminationInterventionComments", data.ContainsKey("485EliminationInterventionComments") ? data["485EliminationInterventionComments"].Answer : "", 5, 70, new { @id = "Recertification_485EliminationInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] eliminationStatusGoals = data.ContainsKey("485EliminationStatusGoals") && data["485EliminationStatusGoals"].Answer != "" ? data["485EliminationStatusGoals"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485EliminationStatusGoals" name=" " />
                <input name="Recertification_485EliminationStatusGoals" value="1" type="checkbox" '<% if( eliminationStatusGoals!=null && eliminationStatusGoals.Contains("1") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Foley will remain patent during this episode and patient will be free of signs and
                symptoms of UTI
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusGoals" value="2" type="checkbox" '<% if( eliminationStatusGoals!=null && eliminationStatusGoals.Contains("2") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Suprapubic tube will remain patent during this episode and patient will be free
                of signs and symptoms of UTI
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusGoals" value="3" type="checkbox" '<% if( eliminationStatusGoals!=null && eliminationStatusGoals.Contains("3") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will be without signs/symptoms of UTI (pain, foul odor, cloudy or blood-tinged
                urine and fever) during this episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusGoals" value="4" type="checkbox" '<% if( eliminationStatusGoals!=null && eliminationStatusGoals.Contains("4") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var independentOstomyManagementPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485IndependentOstomyManagementPerson") && data["485IndependentOstomyManagementPerson"].Answer != "" ? data["485IndependentOstomyManagementPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485IndependentOstomyManagementPerson", independentOstomyManagementPerson)%>
                will be independent in ostomy management by:
                <%=Html.TextBox("Recertification_485IndependentOstomyManagementDate", data.ContainsKey("485IndependentOstomyManagementDate") ? data["485IndependentOstomyManagementDate"].Answer : "", new { @id = "Recertification_485IndependentOstomyManagementDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusGoals" value="5" type="checkbox" '<% if( eliminationStatusGoals!=null && eliminationStatusGoals.Contains("5") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will be free from signs and symptoms of constipation during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusGoals" value="6" type="checkbox" '<% if( eliminationStatusGoals!=null && eliminationStatusGoals.Contains("6") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeAcidRefluxFoodPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485VerbalizeAcidRefluxFoodPerson") && data["485VerbalizeAcidRefluxFoodPerson"].Answer != "" ? data["485VerbalizeAcidRefluxFoodPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485VerbalizeAcidRefluxFoodPerson", verbalizeAcidRefluxFoodPerson)%>
                will verbalize understanding of foods that contribute to acid reflux/indigestion
                by:
                <%=Html.TextBox("Recertification_485VerbalizeAcidRefluxFoodDate", data.ContainsKey("485VerbalizeAcidRefluxFoodDate") ? data["485VerbalizeAcidRefluxFoodDate"].Answer : "", new { @id = "Recertification_485VerbalizeAcidRefluxFoodDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusGoals" value="7" type="checkbox" '<% if( eliminationStatusGoals!=null && eliminationStatusGoals.Contains("7") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will verbalize understanding not to eat 4 hours before bedtime to reduce
                acid reflux/indigestion by:
                <%=Html.TextBox("Recertification_485VerbalizeReduceAcidRefluxDate", data.ContainsKey("485VerbalizeReduceAcidRefluxDate") ? data["485VerbalizeReduceAcidRefluxDate"].Answer : "", new { @id = "Recertification_485VerbalizeReduceAcidRefluxDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485EliminationStatusGoals" value="8" type="checkbox" '<% if( eliminationStatusGoals!=null && eliminationStatusGoals.Contains("8") ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will not develop any signs and symptoms of dehydration during the episode
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Goals: &nbsp;
                <%var eliminationGoalTemplates = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "-----------", Value = "-2" },
                   new SelectListItem { Text = "Erase", Value = "-1"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485EliminationGoalTemplates") && data["485EliminationGoalTemplates"].Answer != "" ? data["485EliminationGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485EliminationGoalTemplates", eliminationGoalTemplates)%>
                <%=Html.TextArea("Recertification_485EliminationGoalComments", data.ContainsKey("485EliminationGoalComments") ? data["485EliminationGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485EliminationGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
