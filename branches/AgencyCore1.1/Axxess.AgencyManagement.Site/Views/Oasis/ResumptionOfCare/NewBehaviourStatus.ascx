﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCareBehaviourialForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id)%>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit")%>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "ResumptionOfCare")%>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th colspan="2">
                Neuro/Emotional/Behavioral Status
            </th>
        </tr>
        <tr>
            <th width="50%">
                Neurological
            </th>
            <th width="50%">
                Psychosocial
            </th>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>Oriented to: </li>
                </ul>
                <%string[] genericNeurologicalOriented = data.ContainsKey("GenericNeurologicalOriented") && data["GenericNeurologicalOriented"].Answer != "" ? data["GenericNeurologicalOriented"].Answer.Split(',') : null; %>
                <ul class="columns">
                    <li>&nbsp; </li>
                    <li>
                        <input type="hidden" name="ResumptionOfCare_GenericNeurologicalOriented" value=" " />
                        <input name="ResumptionOfCare_GenericNeurologicalOriented" value="1" type="checkbox" '<% if( genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        Person </li>
                </ul>
                <ul class="columns">
                    <li>&nbsp; </li>
                    <li>
                        <input name="ResumptionOfCare_GenericNeurologicalOriented" value="2" type="checkbox" '<% if( genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Place </li>
                </ul>
                <ul class="columns">
                    <li>&nbsp; </li>
                    <li>
                        <input name="ResumptionOfCare_GenericNeurologicalOriented" value="3" type="checkbox" '<% if( genericNeurologicalOriented!=null && genericNeurologicalOriented.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        Time </li>
                </ul>
                <%string[] genericNeurologicalStatus = data.ContainsKey("GenericNeurologicalStatus") && data["GenericNeurologicalStatus"].Answer != "" ? data["GenericNeurologicalStatus"].Answer.Split(',') : null; %>
                <ul class="columns">
                    <li>
                        <input type="hidden" name="ResumptionOfCare_GenericNeurologicalStatus" value=" " />
                        <input name="ResumptionOfCare_GenericNeurologicalStatus" value="1" type="checkbox" '<% if( genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        Disoriented </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericNeurologicalStatus" value="2" type="checkbox" '<% if( genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Forgetful </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericNeurologicalStatus" value="3" type="checkbox" '<% if( genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        PERRL </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericNeurologicalStatus" value="4" type="checkbox" '<% if( genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("4")  ){ %>checked="checked"<% }%>'" />
                        Seizures </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericNeurologicalStatus" value="5" type="checkbox" '<% if( genericNeurologicalStatus!=null && genericNeurologicalStatus.Contains("5")  ){ %>checked="checked"<% }%>'" />
                        Tremors </li>
                    <li class="span">Location(s)
                        <%=Html.TextBox("ResumptionOfCare_GenericNeurologicalTremorsLocation", data.ContainsKey("GenericNeurologicalTremorsLocation") ? data["GenericNeurologicalTremorsLocation"].Answer : "", new { @id = "ResumptionOfCare_GenericNeurologicalTremorsLocation", @size = "30", @maxlength = "30" })%>
                    </li>
                </ul>
            </td>
            <td>
                <%string[] genericPsychosocial = data.ContainsKey("GenericPsychosocial") && data["GenericPsychosocial"].Answer != "" ? data["GenericPsychosocial"].Answer.Split(',') : null; %>
                <ul class="columns">
                    <li>
                        <input type="hidden" name="ResumptionOfCare_GenericPsychosocial" value="" />
                        <input name="ResumptionOfCare_GenericPsychosocial" value="1" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("1")  ){ %>checked="checked"<% }%>'" />
                        WNL (Within Normal Limits) </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericPsychosocial" value="2" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("2")  ){ %>checked="checked"<% }%>'" />
                        Poor Home Environment </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericPsychosocial" value="3" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("3")  ){ %>checked="checked"<% }%>'" />
                        Poor Coping Skills </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericPsychosocial" value="4" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("4")  ){ %>checked="checked"<% }%>'" />
                        Agitated </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericPsychosocial" value="5" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("5")  ){ %>checked="checked"<% }%>'" />
                        Depressed Mood </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericPsychosocial" value="6" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("6")  ){ %>checked="checked"<% }%>'" />
                        Impaired Decision Making </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericPsychosocial" value="7" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("7")  ){ %>checked="checked"<% }%>'" />
                        Demonstrated/Expressed Anxiety </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericPsychosocial" value="8" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("8")  ){ %>checked="checked"<% }%>'" />
                        Inappropriate Behavior </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="ResumptionOfCare_GenericPsychosocial" value="9" type="checkbox" '<% if( genericPsychosocial!=null && genericPsychosocial.Contains("9")  ){ %>checked="checked"<% }%>'" />
                        Irritability </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ul class="columns">
                    <li>Comments:<br />
                        <%=Html.TextArea("ResumptionOfCare_GenericNeuroEmoBehaviorComments", data.ContainsKey("GenericNeuroEmoBehaviorComments") ? data["GenericNeuroEmoBehaviorComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_GenericNeuroEmoBehaviorComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1700) Cognitive Functioning: Patient's current (day of assessment) level of alertness,
                orientation, comprehension, concentration, and immediate memory for simple commands.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("ResumptionOfCare_M1700CognitiveFunctioning", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_M1700CognitiveFunctioning", "00", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Alert/oriented, able to focus and shift attention, comprehends and recalls
                task directions independently.<br />
                <%=Html.RadioButton("ResumptionOfCare_M1700CognitiveFunctioning", "01", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar
                conditions.<br />
                <%=Html.RadioButton("ResumptionOfCare_M1700CognitiveFunctioning", "02", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Requires assistance and some direction in specific situations (e.g., on all tasks
                involving shifting of attention), or consistently requires low stimulus environment
                due to distractibility.<br />
                <%=Html.RadioButton("ResumptionOfCare_M1700CognitiveFunctioning", "03", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Requires considerable assistance in routine situations. Is not alert and oriented
                or is unable to shift attention and recall directions more than half the time.<br />
                <%=Html.RadioButton("ResumptionOfCare_M1700CognitiveFunctioning", "04", data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Totally dependent due to disturbances such as constant disorientation, coma, persistent
                vegetative state, or delirium.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1710) When Confused (Reported or Observed Within the Last 14 Days):
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1710WhenConfused", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "00", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - Never<br />
            <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "01", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - In new or complex situations only<br />
            <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "02", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - On awakening or at night only<br />
            <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "03", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - During the day and evening, but not constantly<br />
            <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "04", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Constantly<br />
            <%=Html.RadioButton("ResumptionOfCare_M1710WhenConfused", "NA", data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - Patient nonresponsive
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1720) When Anxious (Reported or Observed Within the Last 14 Days):
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1720WhenAnxious", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1720WhenAnxious", "00", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - None of the time<br />
            <%=Html.RadioButton("ResumptionOfCare_M1720WhenAnxious", "01", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Less often than daily<br />
            <%=Html.RadioButton("ResumptionOfCare_M1720WhenAnxious", "02", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Daily, but not constantly<br />
            <%=Html.RadioButton("ResumptionOfCare_M1720WhenAnxious", "03", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - All of the time<br />
            <%=Html.RadioButton("ResumptionOfCare_M1720WhenAnxious", "NA", data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - Patient nonresponsive
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1730) Depression Screening: Has the patient been screened for depression, using
                a standardized depression screening tool?
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("ResumptionOfCare_M1730DepressionScreening", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreening", "00", data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - No<br />
                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreening", "01", data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;0&nbsp;1
                - Yes, patient was screened using the PHQ-2©* scale. (Instructions for this two-question
                tool: Ask patient: “Over the last two weeks, how often have you been bothered by
                any of the following problems”)<br />
                <div class="row485">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>
                                    PHQ-2©*
                                </th>
                                <th>
                                    Not at all 0 - 1 day
                                </th>
                                <th>
                                    Several days 2 - 6 days
                                </th>
                                <th>
                                    More than half of the days 7 – 11 days
                                </th>
                                <th>
                                    Nearly every day 12 – 14 days
                                </th>
                                <th>
                                    N/A Unable to respond
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    a) Little interest or pleasure in doing things
                                </td>
                                <td>
                                    <%=Html.Hidden("ResumptionOfCare_M1730DepressionScreeningInterest", " ", new { @id = "" })%>
                                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningInterest", "00", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "00" ? true : false, new { @id = "" })%>
                                    &nbsp;0
                                </td>
                                <td>
                                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningInterest", "01", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                                </td>
                                <td>
                                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningInterest", "02", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                                </td>
                                <td>
                                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningInterest", "03", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                                </td>
                                <td>
                                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningInterest", "NA", data.ContainsKey("M1730DepressionScreeningInterest") && data["M1730DepressionScreeningInterest"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b) Feeling down, depressed, or hopeless?
                                </td>
                                <td>
                                    <%=Html.Hidden("ResumptionOfCare_M1730DepressionScreeningHopeless", " ", new { @id = "" })%>
                                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningHopeless", "00", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "00" ? true : false, new { @id = "" })%>
                                    &nbsp;0
                                </td>
                                <td>
                                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningHopeless", "01", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                                </td>
                                <td>
                                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningHopeless", "02", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                                </td>
                                <td>
                                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningHopeless", "03", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                                </td>
                                <td>
                                    <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreeningHopeless", "NA", data.ContainsKey("M1730DepressionScreeningHopeless") && data["M1730DepressionScreeningHopeless"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreening", "02", data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;0&nbsp;2
                - Yes, with a different standardized assessment-and the patient meets criteria for
                further evaluation for depression.<br />
                <%=Html.RadioButton("ResumptionOfCare_M1730DepressionScreening", "03", data.ContainsKey("M1730DepressionScreening") && data["M1730DepressionScreening"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;0&nbsp;3
                - Yes, patient was screened with a different standardized assessment-and the patient
                does not meet criteria for further evaluation for depression.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1740) Cognitive, behavioral, and psychiatric symptoms that are demonstrated at
                least once a week (Reported or Observed): (Mark all that apply.)
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"
                    value="" type="hidden" />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;1
                - Memory deficit: failure to recognize familiar persons/places, inability to recall
                events of past 24 hours, significant memory loss so that supervision is required<br />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsImpDes"
                    value="" type="hidden" />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsImpDes"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes") && data["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;2
                - Impaired decision-making: failure to perform usual ADLs or IADLs, inability to
                appropriately stop activities, jeopardizes safety through actions<br />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsVerbal"
                    value="" type="hidden" />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsVerbal"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal") && data["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;3
                - Verbal disruption: yelling, threatening, excessive profanity, sexual references,
                etc.<br />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsPhysical"
                    value="" type="hidden" />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsPhysical"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical") && data["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;4
                - Physical aggression: aggressive or combative to self and others (e.g., hits self,
                throws objects, punches, dangerous maneuvers with wheelchair or other objects)<br />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsSIB" value=""
                    type="hidden" />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsSIB" value="1"
                    type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB") && data["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;5
                - Disruptive, infantile, or socially inappropriate behavior (excludes verbal actions)<br />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsDelusional"
                    value="" type="hidden" />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsDelusional"
                    value="1" type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional") && data["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;6
                - Delusional, hallucinatory, or paranoid behavior<br />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsNone" value=""
                    type="hidden" />
                <input name="ResumptionOfCare_M1740CognitiveBehavioralPsychiatricSymptomsNone" value="1"
                    type="checkbox" '<% if( data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsNone") && data["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;7
                - None of the above behaviors demonstrated
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1745) Frequency of Disruptive Behavior Symptoms (Reported or Observed) Any physical,
                verbal, or other disruptive/dangerous symptoms that are injurious to self or others
                or jeopardize personal safety.
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "00", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - Never<br />
            <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "01", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Less than once a month<br />
            <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "02", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Once a month<br />
            <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "03", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Several times each month<br />
            <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "04", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Several times a week<br />
            <%=Html.RadioButton("ResumptionOfCare_M1745DisruptiveBehaviorSymptomsFrequency", "05", data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
            - At least daily
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="padding">
                (M1750) Is this patient receiving Psychiatric Nursing Services at home provided
                by a qualified psychiatric nurse?<br />
                &nbsp;
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("ResumptionOfCare_M1750PsychiatricNursingServicing", " ", new { @id = "" })%>
            <%=Html.RadioButton("ResumptionOfCare_M1750PsychiatricNursingServicing", "0", data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;0
            - No<br />
            <%=Html.RadioButton("ResumptionOfCare_M1750PsychiatricNursingServicing", "1", data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "1" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes
        </div>
    </div>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0">
        <%string[] behaviorInterventions = data.ContainsKey("485BehaviorInterventions") && data["485BehaviorInterventions"].Answer != "" ? data["485BehaviorInterventions"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="ResumptionOfCare_485BehaviorInterventions" value=" " />
                <input name="ResumptionOfCare_485BehaviorInterventions" value="1" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                <strong>*SN TO NOTIFY PHYSICIAN THIS PATIENT WAS SCREENED FOR DEPRESSION USING THE PHQ-2
                    SCALE AND MEETS CRITERIA FOR FURTHER EVALUATION FOR DEPRESSION</strong>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485BehaviorInterventions" value="2" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess for changes in neurological status every visit
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485BehaviorInterventions" value="3" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess patient's communication skills every visit
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485BehaviorInterventions" value="4" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructSeizurePrecautionPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485InstructSeizurePrecautionPerson") && data["485InstructSeizurePrecautionPerson"].Answer != "" ? data["485InstructSeizurePrecautionPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("ResumptionOfCare_485InstructSeizurePrecautionPerson", instructSeizurePrecautionPerson)%>
                on seizure precautions
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485BehaviorInterventions" value="5" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("5")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct caregiver on orientation techniques to use when patient becomes disoriented
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485BehaviorInterventions" value="6" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("6")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                MSW:
                <%=Html.Hidden("ResumptionOfCare_485MSWProvideServiceNumberVisits", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_485MSWProvideServiceNumberVisits", "1", data.ContainsKey("485MSWProvideServiceNumberVisits") && data["485MSWProvideServiceNumberVisits"].Answer == "1" ? true : false, new { @id = "" })%>
                1-2 OR
                <%=Html.RadioButton("ResumptionOfCare_485MSWProvideServiceNumberVisits", "0", data.ContainsKey("485MSWProvideServiceNumberVisits") && data["485MSWProvideServiceNumberVisits"].Answer == "0" ? true : false, new { @id = "" })%>
                <%=Html.TextBox("ResumptionOfCare_485MSWProvideServiceVisitAmount", data.ContainsKey("485MSWProvideServiceVisitAmount") ? data["485MSWProvideServiceVisitAmount"].Answer : "", new { @id = "ResumptionOfCare_485MSWProvideServiceVisitAmount", @size = "5", @maxlength = "5" })%>
                visits, every 60 days for provider services
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485BehaviorInterventions" value="7" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("7")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                MSW:
                <%=Html.Hidden("ResumptionOfCare_485MSWLongTermPlanningVisits", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_485MSWLongTermPlanningVisits", "1", data.ContainsKey("485MSWLongTermPlanningVisits") && data["485MSWLongTermPlanningVisits"].Answer == "1" ? true : false, new { @id = "" })%>
                1-2 OR
                <%=Html.RadioButton("ResumptionOfCare_485MSWLongTermPlanningVisits", "0", data.ContainsKey("485MSWLongTermPlanningVisits") && data["485MSWLongTermPlanningVisits"].Answer == "0" ? true : false, new { @id = "" })%>
                <%=Html.TextBox("ResumptionOfCare_485MSWLongTermPlanningVisitAmount", data.ContainsKey("485MSWLongTermPlanningVisitAmount") ? data["485MSWLongTermPlanningVisitAmount"].Answer : "", new { @id = "ResumptionOfCare_485MSWLongTermPlanningVisitAmount", @size = "5", @maxlength = "5" })%>
                visits, every 60 days for long term planning
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485BehaviorInterventions" value="8" type="checkbox" '<% if( behaviorInterventions!=null && behaviorInterventions.Contains("8")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                MSW:
                <%=Html.Hidden("ResumptionOfCare_485MSWCommunityAssistanceVisits", " ", new { @id = "" })%>
                <%=Html.RadioButton("ResumptionOfCare_485MSWCommunityAssistanceVisits", "1", data.ContainsKey("485MSWCommunityAssistanceVisits") && data["485MSWCommunityAssistanceVisits"].Answer == "1" ? true : false, new { @id = "" })%>
                1-2 OR
                <%=Html.RadioButton("ResumptionOfCare_485MSWCommunityAssistanceVisits", "0", data.ContainsKey("485MSWCommunityAssistanceVisits") && data["485MSWCommunityAssistanceVisits"].Answer == "0" ? true : false, new { @id = "" })%>
                <%=Html.TextBox("ResumptionOfCare_485MSWCommunityAssistanceVisitAmount", data.ContainsKey("485MSWCommunityAssistanceVisitAmount") ? data["485MSWCommunityAssistanceVisitAmount"].Answer : "", new { @id = "ResumptionOfCare_485MSWCommunityAssistanceVisitAmount", @size = "5", @maxlength = "5" })%>
                visits, every 60 days for community resource assistance
            </td>
        </tr>
        <tr>
            <td colspan="100%">
                Additional Orders: &nbsp;
                <%var behaviorOrderTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485BehaviorOrderTemplates") && data["485BehaviorOrderTemplates"].Answer != "" ? data["485BehaviorOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_485BehaviorOrderTemplates", behaviorOrderTemplates)%>
                <br />
                <%=Html.TextArea("ResumptionOfCare_485BehaviorComments", data.ContainsKey("485BehaviorComments") ? data["485BehaviorComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485BehaviorComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] behaviorGoals = data.ContainsKey("485BehaviorGoals") && data["485BehaviorGoals"].Answer != "" ? data["485BehaviorGoals"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="ResumptionOfCare_485BehaviorGoals" value=" " />
                <input name="ResumptionOfCare_485BehaviorGoals" value="1" type="checkbox" '<% if( behaviorGoals!=null && behaviorGoals.Contains("1")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will remain free from increased confusion during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485BehaviorGoals" value="2" type="checkbox" '<% if( behaviorGoals!=null && behaviorGoals.Contains("2")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeSeizurePrecautionsPerson = new SelectList(new[]
               { 
                   new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" },
                   new SelectListItem { Text = "Patient", Value = "Patient" },
                   new SelectListItem { Text = "Caregiver", Value = "Caregiver"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485VerbalizeSeizurePrecautionsPerson") && data["485VerbalizeSeizurePrecautionsPerson"].Answer != "" ? data["485VerbalizeSeizurePrecautionsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("ResumptionOfCare_485VerbalizeSeizurePrecautionsPerson", verbalizeSeizurePrecautionsPerson)%>
                will verbalize understanding of seizure precautions
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485BehaviorGoals" value="3" type="checkbox" '<% if( behaviorGoals!=null && behaviorGoals.Contains("3")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Caregiver will verbalize understanding of proper orientation techniques to use when
                patient becomes disoriented
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="ResumptionOfCare_485BehaviorGoals" value="4" type="checkbox" '<% if( behaviorGoals!=null && behaviorGoals.Contains("4")){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient's community resource needs will be met with assistance of social worker
            </td>
        </tr>
        <tr>
            <td colspan="100%">
                <%var behaviorGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485BehaviorGoalTemplates") && data["485BehaviorGoalTemplates"].Answer != "" ? data["485BehaviorGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("ResumptionOfCare_485BehaviorGoalTemplates", behaviorGoalTemplates)%>
                <br />
                <%=Html.TextArea("ResumptionOfCare_485BehaviorGoalComments", data.ContainsKey("485BehaviorGoalComments") ? data["485BehaviorGoalComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485BehaviorGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] mentalStatus = data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer != "" ? data["485MentalStatus"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="4">
                Mental Status (locator #19)
            </th>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="ResumptionOfCare_485MentalStatus" value=" " />
                <input name="ResumptionOfCare_485MentalStatus" value="1" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("1")){ %>checked="checked"<% }%>'" />&nbsp;
                Oriented
            </td>
            <td>
                <input name="ResumptionOfCare_485MentalStatus" value="2" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("2")){ %>checked="checked"<% }%>'" />&nbsp;
                Comatose
            </td>
            <td>
                <input name="ResumptionOfCare_485MentalStatus" value="3" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("3")){ %>checked="checked"<% }%>'" />&nbsp;
                Forgetful
            </td>
            <td>
                <input name="ResumptionOfCare_485MentalStatus" value="7" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("7")){ %>checked="checked"<% }%>'" />&nbsp;
                Agitated
            </td>
        </tr>
        <tr>
            <td>
                <input name="ResumptionOfCare_485MentalStatus" value="4" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("4")){ %>checked="checked"<% }%>'" />&nbsp;
                Depressed
            </td>
            <td>
                <input name="ResumptionOfCare_485MentalStatus" value="5" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("5")){ %>checked="checked"<% }%>'" />&nbsp;
                Disoriented
            </td>
            <td>
                <input name="ResumptionOfCare_485MentalStatus" value="6" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("6")){ %>checked="checked"<% }%>'" />&nbsp;
                Lethargic
            </td>
            <td>
                <input name="ResumptionOfCare_485MentalStatus" value="8" type="checkbox" '<% if( mentalStatus!=null && mentalStatus.Contains("8")){ %>checked="checked"<% }%>'" />&nbsp;
                Other (specify):&nbsp;
                <%=Html.TextBox("ResumptionOfCare_485MentalStatusOther", data.ContainsKey("485MentalStatusOther") ? data["485MentalStatusOther"].Answer : "", new { @id = "ResumptionOfCare_485MentalStatusOther", @size = "20", @maxlength = "20" })%>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <ul class="columns">
                    <li>Additional Orders(Specify):<br />
                        <%=Html.TextArea("ResumptionOfCare_485MentalStatusComments", data.ContainsKey("485MentalStatusComments") ? data["485MentalStatusComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_485MentalStatusComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="ROC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="ROC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
