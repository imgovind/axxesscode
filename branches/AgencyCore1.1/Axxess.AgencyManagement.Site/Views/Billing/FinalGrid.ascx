﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<span style="padding: 50px;"><b style="font-weight: bolder;">Final</b> </span>
<table class="claim">
    <thead>
        <tr>
            <th>
            </th>
            <th>
            </th>
            <th>
                Patient Name
            </th>
            <th>
                Patient ID
            </th>
            <th>
                Episode Date
            </th>
            <th>
                Rap Generated
            </th>
            <th>
                Visit
            </th>
            <th>
                Order
            </th>
            <th>
                Final
            </th>
        </tr>
    </thead>
    <tbody>
        <% var finals = Model.Finals.Where(c => !c.IsGenerated && c.EpisodeEndDate < DateTime.Now); %>
        <% int j = 1;%>
        <% foreach (var final in finals)
           {%>
        <% if (j % 2 == 0)
           {%>
        <tr class="even">
            <%}
           else
           { %>
            <tr class="odd">
                <%} %>
                <td>
                    <%=j %>
                </td>
                <td>
                    <%
                        if (final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified)
                        { %>
                    <input name="FinalSelected" type="checkbox" value='<%=final.Id%>' />
                    <% }
               else
               { %>
                    <input name="FinalSelected" type="checkbox" value='<%=final.Id%>' disabled="disabled" />
                    <% }%>
                </td>
                <td>
                    <% if (final.IsOasisComplete && final.IsFirstBillableVisit)
                       {%>
                    <a onclick="Billing.loadFinal('<%=final.EpisodeId%>','<%=final.PatientId%>');">
                        <%=final.DisplayName%></a>
                    <%}
                       else
                       { %>
                    <a onclick="Billing.loadFinal('<%=final.EpisodeId%>','<%=final.PatientId%>');">
                        <%=final.DisplayName%></a>
                    <%} %>
                </td>
                <td>
                    <%=final.PatientIdNumber%>
                </td>
                <td>
                    <% if (final.EpisodeStartDate != null)
                       {%>
                    <%=final.EpisodeStartDate.ToShortDateString()%>
                    <%} %>
                    <% if (final.EpisodeEndDate != null)
                       {%>
                    -<%=final.EpisodeEndDate.ToShortDateString()%>
                    <%} %>
                </td>
                <td>
                    <%if (final.IsRapGenerated)
                      { %>
                    <img src="../../images/GreenIcon.png" width="16" height="16" alt="" border="0">
                    <%}
                      else
                      { %>
                    <img src="../../Images/RedIcon.png" width="16" height="16" alt="" border="0">
                    <%} %>
                </td>
                <td>
                    <%if (final.AreVisitsComplete)
                      { %>
                    <img src="../../Images/GreenIcon.png" width="16" height="16" alt="" border="0">
                    <%}
                      else
                      { %>
                    <img src="../../Images/RedIcon.png" width="16" height="16" alt="" border="0">
                    <%} %>
                </td>
                <td>
                    <%if (final.AreOrdersComplete)
                      { %>
                    <img src="../../images/GreenIcon.png" width="16" height="16" alt="" border="0">
                    <%}
                      else
                      { %>
                    <img src="../../Images/RedIcon.png" width="16" height="16" alt="" border="0">
                    <%} %>
                </td>
                <td>
                    <% if (final.AreOrdersComplete && final.AreVisitsComplete)
                       {%>
                    <img src="../../images/GreenIcon.png" width="16" height="16" alt="" border="0">
                    <%}
                       else
                       { %>
                    <img src="../../Images/RedIcon.png" width="16" height="16" alt="" border="0">
                    <%} %>
                </td>
            </tr>
            <% j++;
           } %>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9" style="height: 30px; background: #AA9494;">
                <span>
                    <input value="Generate Selected" type="button" onclick="Billing.loadGenerate('#Billing_CenterContent');" />
                    <input value="Generate All Completed" type="button" /></span>
            </td>
        </tr>
    </tfoot>
</table>
