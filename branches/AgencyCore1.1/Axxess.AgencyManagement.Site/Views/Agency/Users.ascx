﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="agencyUserList" class="abs window">
<div class="abs window_inner">
    <div class="window_top">
        <span class="float_left">User List</span> <span class="float_right"><a href="javascript:void(0);"
            class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                href="javascript:void(0);" class="window_close"></a></span>
    </div>
    <div class="abs window_content general_form">
        <div class="submitoasisCont">
            
                <!--[if !IE]>start table_wrapper<![endif]-->
                <%= Html.Telerik().Grid<Employee>()
                        .Name("AgencyUsersGrid")
                        .Columns(columns =>
		                {
                            columns.Bound(e => e.FirstName).Title("First Name").Width(60);
                            columns.Bound(e => e.LastName).Title("Last Name").Width(90);
                        })
                        .DataBinding(dataBinding => dataBinding.Ajax().Select("Users", "Agency"))
                        .Pageable(paging => paging.PageSize(13))
                        .Sortable()
                        .Scrollable(scrolling => scrolling.Enabled(true).Height(369))
                %>
                <!--[if !IE]>end table_wrapper<![endif]-->
            </div>
    </div>
    <div class="abs window_bottom">
        List of Users
    </div>
</div>
<span class="abs ui-resizable-handle ui-resizable-se"></span></div>
