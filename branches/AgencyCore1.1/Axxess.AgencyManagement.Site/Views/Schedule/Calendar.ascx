﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<%  if (Model != null)
    {%>
<% if (Model.Id == Guid.Empty)
   {%>
<%}
   else
   { %>
<%
    var scheduleEvents = Model.Schedule.ToObject<List<ScheduleEvent>>().OrderBy(o => o.EventDate.ToDateTime()).ToList(); 
%>
<%var calnder1StartDate = DateUtilities.GetStartOfMonth(Model.StartDate.Month, Model.StartDate.Year); %>
<%var calnder1EndDate = DateUtilities.GetEndOfMonth(Model.StartDate.Month, Model.StartDate.Year); %>
<%var currentDate1 = calnder1StartDate.AddDays(-(int)calnder1StartDate.DayOfWeek);%>
<%var calnder2StartDate = calnder1EndDate.AddDays(1); %>
<%var calnder2EndDate = DateUtilities.GetEndOfMonth(calnder2StartDate.Month, calnder2StartDate.Year); %>
<%var currentDate2 = calnder2StartDate.AddDays(-(int)calnder2StartDate.DayOfWeek);%>
<%var calnder3StartDate = calnder2EndDate.AddDays(1); %>
<%var calnder3EndDate = DateUtilities.GetEndOfMonth(calnder3StartDate.Month, calnder3StartDate.Year); %>
<%var currentDate3 = calnder3StartDate.AddDays(-(int)calnder3StartDate.DayOfWeek);%>
<div class='mainTop row' style="height: auto;">
    <%=Html.Hidden("SchedulePatientID", Model.PatientId, new { @id="SchedulePatientID"})%>
    <%=Html.Hidden("ScheduleEpisodeID", Model.Id, new { @id = "ScheduleEpisodeID" })%>
    <div class="scheduleMenu" style="padding-left: 5px;">
        <label>
            <b style="color: #53868B;">Patient Information : &nbsp;&nbsp;&nbsp;</b>[&nbsp;<span
                id="schedulePatientInfo" onclick="Patient.PatientInfo(this,'<%=Model.PatientId %>');"
                class="input_wrapper blank"><%=Model.DisplayName%></span>&nbsp;]&nbsp;&nbsp;&nbsp;
            <a href="javascript:void(0);" onclick="Schedule.loadMasterCalendar('<%=Model.PatientId %>','<%=Model.Id %>');">
                Master Calendar</a>
        </label>
    </div>
    <div class="mainTopContainer" style="padding-left: 5px; margin: 0 auto;">
        <div style="float: left; width: 90%;">
            <% 
                if (Model.HasPrevious)
                {%>
            <span id="previousEpisode" style="float: left;"><b><a onclick="Schedule.NavigateEpisode('<%= Model.PreviousEpisode.Id%>','<%=Model.PatientId %>');">
                << Previous Episode</a> </b></span>
            <%} %>
            <span style="margin-left: 35%;"><b><span id="EpisodStartDate" class="input_wrapper blank">
                <%=Model.StartDate.ToShortDateString()%></span>-<span id="EpisodeEndDate" class="input_wrapper blank"><%=Model.EndDate.ToShortDateString() %>
                </span></b></span>
            <% if (Model.HasNext)
               {%>
            <span id="nextEpisode" style="float: right;"><b><a onclick="Schedule.NavigateEpisode('<%= Model.NextEpisode.Id%>','<%=Model.PatientId %>');">
                Next Episode >></a> </b>
                <%} %>
            </span>
        </div>
        <div class="row">
            <div id="createCalendar1" class="calendarWidth">
                <table cellpadding="0" tablespacing="0" class="MonthlyCalendar">
                    <thead id="CalendarHead" style="height: 50px;">
                        <tr>
                            <td colspan="7">
                                <div class="FormHeader MonthNavigation">
                                    <div class="MonthName">
                                        <%= string.Format("{0:MMMM} {0:yyyy}", calnder1StartDate)%></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="DateHeader Weekend" title="Su">
                                <span>Su</span>
                            </th>
                            <th class="DateHeader" title="Mo">
                                <span>Mo</span>
                            </th>
                            <th class="DateHeader" title="Tu">
                                <span>Tu</span>
                            </th>
                            <th class="DateHeader" title="We">
                                <span>We</span>
                            </th>
                            <th class="DateHeader" title="Th">
                                <span>Th</span>
                            </th>
                            <th class="DateHeader" title="Fr">
                                <span>Fr</span>
                            </th>
                            <th class="DateHeader Weekend" title="Sa">
                                <span>Sa</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="CalendarBody">
                        <%var calendar1Weeks = DateUtilities.Weeks(calnder1StartDate.Month, calnder1StartDate.Year); %>
                        <%  for (int i = 0; i <= calendar1Weeks; i++)
                            { %>
                        <tr>
                            <%int addedDate = (i) * 7; %>
                            <%for (int j = 0; j <= 6; j++)
                              { %>
                            <%var specificDate = currentDate1.AddDays(j + addedDate);%>
                            <%if (specificDate < Model.StartDate || specificDate > calnder1EndDate)
                              { %>
                            <td class="DateBox Inactive ui-droppable" date='<%=specificDate%>'>
                                <div class="DateLabel">
                                    <a></a>
                                </div>
                            </td>
                            <%}
                              else
                              {%>
                            <% 
                                var currentSchedules = scheduleEvents.FindAll(e => e.EventDate == specificDate.ToShortDateString());  %>
                            <%var count = currentSchedules.Count;

                              ScheduleEvent evnt = null;
                              if (count != 0)
                              {
                                  evnt = currentSchedules.First();
                              }
                              var displine = "";
                              var status = 0;
                              if (evnt != null)
                              {
                                  displine = evnt.Discipline;
                                  status = int.Parse(evnt.Status);
                              }
                                   
                             
                            %>
                            <%if (displine == "Nursing")
                              {
                                  if (status == 1 || status == 7 || status == 8)
                                  {%>
                            <td class="DateBox ui-droppable" style="background-color: #FFB5C5;" date='<%=specificDate%>'>
                                <%}
                                  else if (status == 2 || status == 10)
                                  { %>
                                <td class="DateBox ui-droppable" style="background-color: #EE799F;" date='<%=specificDate%>'>
                                    <%}
                                  else if (status == 3)
                                  { %>
                                    <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                        <%} %>
                                        <%}
                              else if (displine == "OT")
                              {

                                  if (status == 1)
                                  {%>
                                        <td class="DateBox ui-droppable" style="background-color: #FBF5E6;" date='<%=specificDate%>'>
                                            <%}
                                  else if (status == 2)
                                  { %>
                                            <td class="DateBox ui-droppable" style="background-color: #CDAA7D;" date='<%=specificDate%>'>
                                                <%}
                                  else if (status == 3)
                                  { %>
                                                <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                    <%} %>
                                                    <%}
                              else if (displine == "ST")
                              {
                                  if (status == 1)
                                  {%>
                                                    <td class="DateBox ui-droppable" style="background-color: #F0F8FF;" date='<%=specificDate%>'>
                                                        <%}
                                  else if (status == 2)
                                  { %>
                                                        <td class="DateBox ui-droppable" style="background-color: #8EE5EE;" date='<%=specificDate%>'>
                                                            <%}
                                  else if (status == 3)
                                  { %>
                                                            <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                <%} %>
                                                                <%}
                              else if (displine == "PT")
                              {
                                  if (status == 1)
                                  {%>
                                                                <td class="DateBox ui-droppable" style="background-color: #FFE1FF;" date='<%=specificDate%>'>
                                                                    <%}
                                  else if (status == 2)
                                  { %>
                                                                    <td class="DateBox ui-droppable" style="background-color: #CD96CD;" date='<%=specificDate%>'>
                                                                        <%}
                                  else if (status == 3)
                                  { %>
                                                                        <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                            <%} %>
                                                                            <%}
                              else if (displine == "MSW")
                              {
                                  if (status == 1)
                                  {%>
                                                                            <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                <%}
                                  else if (status == 2)
                                  { %>
                                                                                <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                    <%}
                                  else if (status == 3)
                                  { %>
                                                                                    <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                                        <%} %>
                                                                                        <%}
                              else if (displine == "HHA")
                              {
                                  if (status == 1)
                                  {%>
                                                                                        <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                            <%}
                                  else if (status == 2)
                                  { %>
                                                                                            <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                                <%}
                                  else if (status == 3)
                                  { %>
                                                                                                <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                                                    <%} %>
                                                                                                    <%}
                              else if (displine == "Orders")
                              {
                                  if (status == 1)
                                  {%>
                                                                                                    <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                                        <%}
                                  else if (status == 2)
                                  { %>
                                                                                                        <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                                            <%}
                                  else if (status == 3)
                                  { %>
                                                                                                            <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                                                                <%} %>
                                                                                                                <%}
                              else if (displine == "Claim")
                              {
                                  if (status == 13)
                                  {%>
                                                                                                                <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                                                    <%}
                                  else if (status == 2)
                                  { %>
                                                                                                                    <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                                                        <%}
                                  else if (status == 3)
                                  { %>
                                                                                                                        <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                                                                            <%} %>
                                                                                                                            <%}
                              else
                              { %>
                                                                                                                            <td class="DateBox ui-droppable" date='<%=specificDate%>'>
                                                                                                                                <%} %>
                                                                                                                                <div class="DateLabel" onclick="Schedule.Add('<%=specificDate.ToShortDateString()%>');">
                                                                                                                                    <a>
                                                                                                                                        <%= string.Format("{0}", specificDate.Day)%></a>
                                                                                                                                </div>
                                                                                                                            </td>
                                                                                                                            <%} %>
                                                                                                                            <%} %>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
            </div>
            <div id="createCalendar2" class="calendarWidth">
                <table cellpadding="0" tablespacing="0" class="MonthlyCalendar">
                    <thead id="Thead1" style="height: 50px;">
                        <tr>
                            <td colspan="7">
                                <div class="FormHeader MonthNavigation">
                                    <div class="MonthName">
                                        <%= string.Format("{0:MMMM} {0:yyyy}", calnder2StartDate)%>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="DateHeader Weekend" title="Su">
                                <span>Su</span>
                            </th>
                            <th class="DateHeader" title="Mo">
                                <span>Mo</span>
                            </th>
                            <th class="DateHeader" title="Tu">
                                <span>Tu</span>
                            </th>
                            <th class="DateHeader" title="We">
                                <span>We</span>
                            </th>
                            <th class="DateHeader" title="Th">
                                <span>Th</span>
                            </th>
                            <th class="DateHeader" title="Fr">
                                <span>Fr</span>
                            </th>
                            <th class="DateHeader Weekend" title="Sa">
                                <span>Sa</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="Tbody1">
                        <%var calendar2Weeks = DateUtilities.Weeks(calnder2StartDate.Month, calnder2StartDate.Year); %>
                        <%  for (int i = 0; i <= calendar2Weeks; i++)
                            { %>
                        <tr>
                            <%int addedDate = (i) * 7; %>
                            <%for (int j = 0; j <= 6; j++)
                              { %>
                            <%var specificDate = currentDate2.AddDays(j + addedDate);%>
                            <%if (specificDate < calnder2StartDate || specificDate > calnder2EndDate)
                              { %>
                            <td class="DateBox  Inactive ui-droppable" date='<%=specificDate%>'>
                                <div class="DateLabel">
                                    <a></a>
                                </div>
                            </td>
                            <%}
                              else
                              {%>
                            <%
                                 
                                var currentSchedules = scheduleEvents.FindAll(e => e.EventDate == specificDate.ToShortDateString());  %>
                            <%var count = currentSchedules.Count;

                              ScheduleEvent evnt = null;
                              if (count != 0)
                              {
                                  evnt = currentSchedules.First();
                              }
                              var displine = "";
                              var status = 0;
                              if (evnt != null)
                              {
                                  displine = evnt.Discipline;
                                  status = int.Parse(evnt.Status);
                              }
                                   
                             
                            %>
                            <%if (displine == "Nursing")
                              {
                                  if (status == 1 || status == 7 || status == 8)
                                  {%>
                            <td class="DateBox ui-droppable" style="background-color: #FFB5C5;" date='<%=specificDate%>'>
                                <%}
                                  else if (status == 2 || status == 10)
                                  { %>
                                <td class="DateBox ui-droppable" style="background-color: #EE799F;" date='<%=specificDate%>'>
                                    <%}
                                  else if (status == 3)
                                  { %>
                                    <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                        <%} %>
                                        <%}
                              else if (displine == "OT")
                              {

                                  if (status == 1)
                                  {%>
                                        <td class="DateBox ui-droppable" style="background-color: #FBF5E6;" date='<%=specificDate%>'>
                                            <%}
                                  else if (status == 2)
                                  { %>
                                            <td class="DateBox ui-droppable" style="background-color: #CDAA7D;" date='<%=specificDate%>'>
                                                <%}
                                  else if (status == 3)
                                  { %>
                                                <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                    <%} %>
                                                    <%}
                              else if (displine == "ST")
                              {
                                  if (status == 1)
                                  {%>
                                                    <td class="DateBox ui-droppable" style="background-color: #F0F8FF;" date='<%=specificDate%>'>
                                                        <%}
                                  else if (status == 2)
                                  { %>
                                                        <td class="DateBox ui-droppable" style="background-color: #8EE5EE;" date='<%=specificDate%>'>
                                                            <%}
                                  else if (status == 3)
                                  { %>
                                                            <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                <%} %>
                                                                <%}
                              else if (displine == "PT")
                              {
                                  if (status == 1)
                                  {%>
                                                                <td class="DateBox ui-droppable" style="background-color: #FFE1FF;" date='<%=specificDate%>'>
                                                                    <%}
                                  else if (status == 2)
                                  { %>
                                                                    <td class="DateBox ui-droppable" style="background-color: #CD96CD;" date='<%=specificDate%>'>
                                                                        <%}
                                  else if (status == 3)
                                  { %>
                                                                        <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                            <%} %>
                                                                            <%}
                              else if (displine == "MSW")
                              {
                                  if (status == 1)
                                  {%>
                                                                            <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                <%}
                                  else if (status == 2)
                                  { %>
                                                                                <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                    <%}
                                  else if (status == 3)
                                  { %>
                                                                                    <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                                        <%} %>
                                                                                        <%}
                              else if (displine == "HHA")
                              {
                                  if (status == 1)
                                  {%>
                                                                                        <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                            <%}
                                  else if (status == 2)
                                  { %>
                                                                                            <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                                <%}
                                  else if (status == 3)
                                  { %>
                                                                                                <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                                                    <%} %>
                                                                                                    <%}
                              else if (displine == "Orders")
                              {
                                  if (status == 1)
                                  {%>
                                                                                                    <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                                        <%}
                                  else if (status == 2)
                                  { %>
                                                                                                        <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                                            <%}
                                  else if (status == 3)
                                  { %>
                                                                                                            <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                                                                <%} %>
                                                                                                                <%}
                              else
                              { %>
                                                                                                                <td class="DateBox ui-droppable" date='<%=specificDate%>'>
                                                                                                                    <%} %>
                                                                                                                    <div class="DateLabel" onclick="Schedule.Add('<%=specificDate.ToShortDateString()%>');">
                                                                                                                        <a>
                                                                                                                            <%= string.Format("{0}", specificDate.Day)%></a>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                                <%} %>
                                                                                                                <%} %>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
            </div>
            <div id="createCalendar3s" class="calendarWidth">
                <table cellpadding="0" tablespacing="0" class="MonthlyCalendar">
                    <thead id="Thead2" style="height: 50px;">
                        <tr>
                            <td colspan="7">
                                <div class="FormHeader MonthNavigation">
                                    <div class="MonthName">
                                        <%= string.Format("{0:MMMM} {0:yyyy}", calnder3StartDate)%>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="DateHeader Weekend" title="Su">
                                <span>Su</span>
                            </th>
                            <th class="DateHeader" title="Mo">
                                <span>Mo</span>
                            </th>
                            <th class="DateHeader" title="Tu">
                                <span>Tu</span>
                            </th>
                            <th class="DateHeader" title="We">
                                <span>We</span>
                            </th>
                            <th class="DateHeader" title="Th">
                                <span>Th</span>
                            </th>
                            <th class="DateHeader" title="Fr">
                                <span>Fr</span>
                            </th>
                            <th class="DateHeader Weekend" title="Sa">
                                <span>Sa</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="Tbody2">
                        <%var calendar3Weeks = DateUtilities.Weeks(calnder3StartDate.Month, calnder3StartDate.Year); %>
                        <%  
                            for (int i = 0; i <= calendar3Weeks; i++)
                            { %>
                        <tr>
                            <%int addedDate = (i) * 7; %>
                            <%for (int j = 0; j <= 6; j++)
                              { %>
                            <%var specificDate = currentDate3.AddDays(j + addedDate);%>
                            <%if (specificDate < calnder3StartDate || specificDate > Model.EndDate)
                              { %>
                            <td class="DateBox  Inactive ui-droppable" date='<%=specificDate%>'>
                                <div class="DateLabel">
                                    <a></a>
                                </div>
                            </td>
                            <%}
                              else
                              {%>
                            <% var currentSchedules = scheduleEvents.FindAll(e => e.EventDate == specificDate.ToShortDateString());  %>
                            <%var count = currentSchedules.Count;

                              ScheduleEvent evnt = null;
                              if (count != 0)
                              {
                                  evnt = currentSchedules.First();
                              }
                              var displine = "";
                              var status = 0;
                              if (evnt != null)
                              {
                                  displine = evnt.Discipline;
                                  status = int.Parse(evnt.Status);
                              }
                                   
                             
                            %>
                            <%if (displine == "Nursing")
                              {
                                  if (status == 1 || status == 7 || status == 8)
                                  {%>
                            <td class="DateBox ui-droppable" style="background-color: #FFB5C5;" date='<%=specificDate%>'>
                                <%}
                                  else if (status == 2 || status == 10)
                                  { %>
                                <td class="DateBox ui-droppable" style="background-color: #EE799F;" date='<%=specificDate%>'>
                                    <%}
                                  else if (status == 3)
                                  { %>
                                    <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                        <%} %>
                                        <%}
                              else if (displine == "OT")
                              {

                                  if (status == 1)
                                  {%>
                                        <td class="DateBox ui-droppable" style="background-color: #FBF5E6;" date='<%=specificDate%>'>
                                            <%}
                                  else if (status == 2)
                                  { %>
                                            <td class="DateBox ui-droppable" style="background-color: #CDAA7D;" date='<%=specificDate%>'>
                                                <%}
                                  else if (status == 3)
                                  { %>
                                                <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                    <%} %>
                                                    <%}
                              else if (displine == "ST")
                              {
                                  if (status == 1)
                                  {%>
                                                    <td class="DateBox ui-droppable" style="background-color: #F0F8FF;" date='<%=specificDate%>'>
                                                        <%}
                                  else if (status == 2)
                                  { %>
                                                        <td class="DateBox ui-droppable" style="background-color: #8EE5EE;" date='<%=specificDate%>'>
                                                            <%}
                                  else if (status == 3)
                                  { %>
                                                            <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                <%} %>
                                                                <%}
                              else if (displine == "PT")
                              {
                                  if (status == 1)
                                  {%>
                                                                <td class="DateBox ui-droppable" style="background-color: #FFE1FF;" date='<%=specificDate%>'>
                                                                    <%}
                                  else if (status == 2)
                                  { %>
                                                                    <td class="DateBox ui-droppable" style="background-color: #CD96CD;" date='<%=specificDate%>'>
                                                                        <%}
                                  else if (status == 3)
                                  { %>
                                                                        <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                            <%} %>
                                                                            <%}
                              else if (displine == "MSW")
                              {
                                  if (status == 1)
                                  {%>
                                                                            <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                <%}
                                  else if (status == 2)
                                  { %>
                                                                                <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                    <%}
                                  else if (status == 3)
                                  { %>
                                                                                    <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                                        <%} %>
                                                                                        <%}
                              else if (displine == "HHA")
                              {
                                  if (status == 1)
                                  {%>
                                                                                        <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                            <%}
                                  else if (status == 2)
                                  { %>
                                                                                            <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                                <%}
                                  else if (status == 3)
                                  { %>
                                                                                                <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                                                    <%} %>
                                                                                                    <%}
                              else if (displine == "Orders")
                              {
                                  if (status == 1)
                                  {%>
                                                                                                    <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                                        <%}
                                  else if (status == 2)
                                  { %>
                                                                                                        <td class="DateBox ui-droppable" style="background-color: #9ACD32;" date='<%=specificDate%>'>
                                                                                                            <%}
                                  else if (status == 3)
                                  { %>
                                                                                                            <td class="DateBox ui-droppable" style="background-color: #FF0000;" date='<%=specificDate%>'>
                                                                                                                <%} %>
                                                                                                                <%}
                              else
                              { %>
                                                                                                                <td class="DateBox ui-droppable" date='<%=specificDate%>'>
                                                                                                                    <%} %>
                                                                                                                    <div class="DateLabel" onclick="Schedule.Add('<%=specificDate.ToShortDateString()%>');">
                                                                                                                        <a>
                                                                                                                            <%= string.Format("{0}", specificDate.Day)%></a>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                                <%} %>
                                                                                                                <%} %>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<%} %>
<%}
    else
    { %>
You need to create the episode.
<%} %>