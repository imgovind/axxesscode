﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<%var scheduleEvents = Model.Schedule.ToObject<List<ScheduleEvent>>().OrderBy(o => o.EventDate.ToDateTime()).ToList(); %>
<%var startWeekDay = (int)Model.StartDate.DayOfWeek; %>
<%var startDate = Model.StartDate; %>
<%var endDate = Model.EndDate; %>
<%var currentDate = Model.StartDate.AddDays(-startWeekDay);%>
<div style="width: 100%; height: 100%;">
    <table style="width: 100%; height: 100%;" border="1px" id="masterCalendarTable" class="masterCalendar">
        <thead>
            <tr>
                <th colspan="3">
                    <span>Patient Name:&nbsp;&nbsp;</span><a style="text-decoration: underline;" id=""
                        onclick="Patient.PatientInfoCenter($(this),'<%=Model.PatientId %>');" class="">
                        <%=Model.DisplayName %></a>
                </th>
                <th colspan="5">
                    Frequencies:
                    <%=Model.Frequencies %>
                </th>
            </tr>
            <tr>
                <th colspan="2">
                    <%if (Model.HasPrevious)
                      { %>
                    <b><a href="javascript:void(0);" onclick="Schedule.loadMasterCalendarNavigation('<%=Model.PreviousEpisode.Id%>','<%=Model.PatientId %>');">
                        << Previous Episode</a> </b>
                    <%} %>
                </th>
                <th colspan="4">
                    <%=string.Format("{0:MM/dd/yyyy}" ,Model.StartDate) + " - "+string.Format("{0:MM/dd/yyyy}" ,Model.EndDate) %>
                </th>
                <th colspan="2">
                    <%if (Model.HasNext)
                      { %>
                    <b><a href="javascript:void(0);" onclick="Schedule.loadMasterCalendarNavigation('<%=Model.NextEpisode.Id%>','<%=Model.PatientId %>');">
                        Next Episode >></a> </b>
                    <%} %>
                </th>
            </tr>
            <tr>
                <th>
                </th>
                <th>
                    Sun
                </th>
                <th>
                    Mon
                </th>
                <th>
                    Tue
                </th>
                <th>
                    Wed
                </th>
                <th>
                    Thu
                </th>
                <th>
                    Fri
                </th>
                <th>
                    Sat
                </th>
            </tr>
        </thead>
        <tbody>
            <%for (int i = 1; i <= 10; i++)
              {%>
            <tr>
                <td style="width: 60px;">
                    Week
                    <%=i %>
                </td>
                <%int addedDate = (i - 1) * 7; %>
                <%for (int j = 0; j <= 6; j++)
                  {%>
                <%var specificDate = currentDate.AddDays(j + addedDate);%>
                <%if (specificDate < startDate || specificDate > endDate)
                  { %>
                <td>
                </td>
                <%}
                  else
                  {%>
                <%if (j == 6)
                  {%>
                <td onmouseover="Schedule.EventMouseOver($(this));" onmouseout="Schedule.EventMouseOut($(this));"
                    class="lastTd">
                    <%}
                  else
                  { %>
                    <td onmouseover="Schedule.EventMouseOver($(this));" onmouseout="Schedule.EventMouseOut($(this));">
                        <%} %>
                        <span>
                            <%=string.Format("{0:MM/dd}", specificDate)%></span>
                        <br />
                        <% var currentSchedules = scheduleEvents.FindAll(e => e.EventDate.ToDateTime() == specificDate);  %>
                        <%if (currentSchedules.Count != 0)
                          { %>
                        <%foreach (var evnt in currentSchedules)
                          {
                              if (evnt.Discipline == "Orders")
                              {
                                  continue;
                              }   
                          
                        %>
                        <%var status = int.Parse(evnt.Status);
                          if (status == 2 || status == 11 || status == 10)
                          {%>
                        <b>
                            <%=EnumExtensions.GetEnumShortDescription((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), evnt.DisciplineTask)).ToUpper()%></b>
                        <%}
                          else
                          {%>
                        <%=EnumExtensions.GetEnumShortDescription((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), evnt.DisciplineTask)).ToUpper()%>
                        <%} %>
                        <%} %>
                        <div class="events">
                            <ul>
                                <%foreach (var evnt in currentSchedules)
                                  {%>
                                <li><span class="eventTitle">
                                    <%=EnumExtensions.GetEnumShortDescription((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), evnt.DisciplineTask)).ToUpper()%></span>
                                    <span class="desc">Employee:
                                        <%= evnt.UserName%></span> <span class="desc">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status:
                                            <%= evnt.StatusName%>
                                        </span></li>
                                <%} %>
                            </ul>
                        </div>
                        <%}
                          else
                          {%>
                        <br />
                        <%} %>
                    </td>
                    <%}%><%} %>
            </tr>
            <%} %>
        </tbody>
        <tfoot>
        </tfoot>
    </table>
</div>
