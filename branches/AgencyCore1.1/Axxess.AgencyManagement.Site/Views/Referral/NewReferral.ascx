﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<!--[if !IE]>start forms<![endif]-->
<% using (Html.BeginForm("Add", "Referral", FormMethod.Post, new { @id = "newReferralForm" }))%>
<%  { %>
<div id="newReferralValidaton" class="marginBreak " style="display: none">
</div>
<div class="marginBreak">
    <b>Referred By</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="source">
                            Source:</label>
                        <div class="inputs">
                            <select name="ReferralSource" id="txtNew_Patient_AdmissionSource" class="input_wrapper AdmissionSource"
                                tabindex="64">
                                <option value="0" selected="selected">** Select **</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label for="ReferralDate">
                            Referral Date:</label>
                        <div class="inputs">
                            <%= Html.Telerik().DatePicker()
                                        .Name("ReferralDate")
                                        .Value(DateTime.Today)
                                         .HtmlAttributes(new { @id = "txtNew_Referral_Date", @class = "text date"})
                            %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="ReferrerFirstName">
                            First Name:&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("ReferrerFirstName", "", new { @id = "txtAdd_Referrer_FirstName", @class = "text names input_wrapper ", @maxlength = "20", @tabindex = "1" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="ReferrerLastName">
                            Last Name:&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("ReferrerLastName", "", new { @id = "txtAdd_Referrer_LastName", @class = "text names input_wrapper", @maxlength = "20", @tabindex = "1" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="marginBreak">
    <b>Patient Demographics</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row" style="vertical-align: middle;">
                        <label for="FirstName">
                            First Name:&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("FirstName", "", new { @id = "txtAdd_Referral_FirstName", @maxlength = "30", @class = "text required names input_wrapper" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="LastName">
                            Last Name:&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("LastName", "", new { @id = "txtAdd_Referral_LastName", @maxlength = "30", @class = "text required names input_wrapper" })%>
                        </div>
                    </div>
                    <div class="row ">
                        <label for="MedicareNo">
                            Medicare No:&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("MedicareNo", " ", new { @id = "txtAdd_Referral_MedicareNo", @maxlength = "10", @class = "text MedicareNo input_wrapper" })%>
                        </div>
                    </div>
                    <div class="row ">
                        <label for="MedicaidNo">
                            Medicaid No:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("MedicaidNo", " ", new { @id = "txtAdd_Referral_MedicaidNo", @maxlength = "10", @class = "text MedicaidNo input_wrapper" })%>
                    </div>
                    <div class="row ">
                        <label for="SSN">
                            SSN:&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("SSN", "", new { @id = "txtAdd_Referral_SSN", @maxlength = "9", @class = "text ssn input_wrapper" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="DateOfBirth">
                            Date of Birth :</label>
                        <div class="inputs">
                            <%=Html.TextBox("DateOfBirth", "", new { @id = "txtAdd_Referral_DateOfBirth", @class = "text required date input_wrapper" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="Gender">
                            Gender :</label>
                        <div id="referralRadio" class="inputs">
                            <%=Html.RadioButton("Gender", "Female", new { @id = "", @class = "required" })%>Female
                            <%=Html.RadioButton("Gender", "Male", new { @id = "", @class = "required" })%>Male
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="HomePhoneArray">
                            Home Phone:</label>
                        <div class="inputs">
                            <input type="text" class="autotext numeric required" style="width: 51.5px; padding: 0px;
                                margin: 0px;" name="HomePhoneArray" id="txtAdd_Referral_HomePhone1" maxlength="3"
                                size="3" />
                            -
                            <input type="text" class="autotext numeric required" style="width: 51.5px; padding: 0px;
                                margin: 0px;" name="HomePhoneArray" id="txtAdd_Referral_HomePhone2" maxlength="3"
                                size="3" />
                            -
                            <input type="text" class="autotext numeric required" style="width: 52px; padding: 0px;
                                margin: 0px;" name="HomePhoneArray" id="txtAdd_Referral_HomePhone3" maxlength="4"
                                size="5" />
                        </div>
                    </div>
                    <div class="row">
                        <label for="Email">
                            Email:</label>
                        <div class="inputs">
                            <%=Html.TextBox("Email", "", new { @id = "txtAdd_Referral_Email", @class = "text email input_wrapper" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine1">
                            Address Line 1:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressLine1", "", new { @id = "txtAdd_Referral_AddressLine1", @maxlength = "20", @class = "text required input_wrapper" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine2">
                            Address Line 2:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressLine2", "", new { @id = "txtAdd_Referral_AddressLine2", @maxlength = "20", @class = "text input_wrapper" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressCity">
                            City:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressCity", "", new { @id = "txtAdd_Referral_AddressCity", @maxlength = "20", @class = "text required input_wrapper" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressStateCode">
                            State :</label>
                        <div class="inputs">
                            <select id="txtAdd_Referral_AddressStateCode" name="AddressStateCode" tabindex="17"
                                class="AddressStateCode selectDropDown input_wrapper">
                                <option value="0" selected>** Select State **</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressZipCode">
                            Zip Code :</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressZipCode", "", new { @id = "txtAdd_Referral_AddressZipCode", @class = "text numeric required input_wrapper", @size = "5", @maxlength = "5" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AssignedTo">
                            Staff Assigned To:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AssignedTo", "", new { @id = "txtAdd_Referral_AssignedTo", @class = "text input_wrapper" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="margin">
    <div class="row485">
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <th colspan='6'>
                    Services Required
                </th>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" value="0" name="ServicesRequiredCollection" />
                    SNV
                </td>
                <td>
                    <input type="checkbox" value="1" name="ServicesRequiredCollection" />
                    HHA
                </td>
                <td>
                    <input type="checkbox" value="2" name="ServicesRequiredCollection" />
                    PT
                </td>
                <td>
                    <input type="checkbox" value="3" name="ServicesRequiredCollection" />
                    OT
                </td>
                <td>
                    <input type="checkbox" value="4" name="ServicesRequiredCollection" />
                    SP
                </td>
                <td>
                    <input type="checkbox" value="5" name="ServicesRequiredCollection" />
                    MSW
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="margin">
    <div class="row485">
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <th colspan='5'>
                    DME Needed
                </th>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" value="0" name="DMECollection" />
                    Bedside Commode
                </td>
                <td>
                    <input type="checkbox" value="1" name="DMECollection" />
                    Cane
                </td>
                <td>
                    <input type="checkbox" value="2" name="DMECollection" />
                    Elevated Toilet Seat
                </td>
                <td>
                    <input type="checkbox" value="3" name="DMECollection" />
                    Grab Bars
                </td>
                <td>
                    <input type="checkbox" value="4" name="DMECollection" />
                    Hospital Bed
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" value="5" name="DMECollection" />
                    Nebulizer
                </td>
                <td>
                    <input type="checkbox" value="6" name="DMECollection" />
                    Oxygen
                </td>
                <td>
                    <input type="checkbox" value="7" name="DMECollection" />
                    Tub/Shower Bench
                </td>
                <td>
                    <input type="checkbox" value="8" name="DMECollection" />
                    Walker
                </td>
                <td>
                    <input type="checkbox" value="9" name="DMECollection" />
                    Wheelchair
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <input type="checkbox" value="10" id="Add_DME_Other" name="DMECollection" />
                    other &nbsp;&nbsp;&nbsp;
                    <%=Html.TextBox("OtherDME", "", new { @id = "txtAdd_Referral_OtherDME", @class = "text" ,@style="display:none;"})%>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="margin">
    <div class="rowTable">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <th colspan="2">
                    <b>Primary Physician</b>
                </th>
            </tr>
            <tr>
                <td style="width: 50%">
                    <label>
                        Select from the list below:</label>
                    <select style="width: 150px; float: left" class="Physicians input_wrapper" tabindex="17"
                        name="AgencyPhysicians" id="txtNew_Referral_PhysicianDropDown">
                        <option value="0" selected="selected">** Select Physician **</option>
                    </select>
                </td>
                <td style="width: 50%">
                    <label>
                        NPI Search:</label>&nbsp;
                    <input type="text" name="txtNew_Referral_NpiNumber" id="txtNew_Referral_NpiNumber"
                        maxlength="10" style="width: 100px;" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="rowBreak">
                        <div class="contentDivider">
                            <div class="patientfieldset">
                                <div class="fix">
                                    <div class="row">
                                        <label for="PhysicianFirstName">
                                            First Name:</label>
                                        <div class="inputs">
                                            <%=Html.TextBox("PhysicianFirstName", "", new { @id = "txtAdd_Referral_PhysicianFirstName", @class = "text names input_wrapper" })%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="PhysicianLastName">
                                            Last Name:</label>
                                        <div class="inputs">
                                            <%=Html.TextBox("PhysicianLastName", "", new { @id = "txtAdd_Referral_PhysicianLastName", @class = "text names input_wrapper" })%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="ReferralPhysicianNPI">
                                            NPI No:</label>
                                        <div class="inputs">
                                            <%=Html.TextBox("ReferralPhysicianNPI", "", new { @id = "txtAdd_Referral_PhysicianNPI", @class = "text input_wrapper", @tabindex = "8" })%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="contentDivider">
                            <div class="patientfieldset">
                                <div class="fix">
                                    <div class="row">
                                        <label for="PhysicianPhoneArray">
                                            Phone:</label>
                                        <div class="inputs">
                                            <input type="text" class="autotext numeric" style="width: 51.5px; padding: 0px; margin: 0px;"
                                                name="PhysicianPhoneArray" id="txtAdd_Referral_PhysicianPhone1" maxlength="3"
                                                size="3" />
                                            -
                                            <input type="text" class="autotext numeric" style="width: 51.5px; padding: 0px; margin: 0px;"
                                                name="PhysicianPhoneArray" id="txtAdd_Referral_PhysicianPhone2" maxlength="3"
                                                size="3" />
                                            -
                                            <input type="text" class="autotext numeric" style="width: 52px; padding: 0px; margin: 0px;"
                                                name="PhysicianPhoneArray" id="txtAdd_Referral_PhysicianPhone3" maxlength="4"
                                                size="5" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="PhysicianFax">
                                            Fax:</label>
                                        <div class="inputs">
                                            <%=Html.TextBox("PhysicianFax", "", new { @id = "txtAdd_Referral_PhysicianFax", @class = "text input_wrapper" })%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="PhysicianEmail">
                                            Email:</label>
                                        <div class="inputs">
                                            <%=Html.TextBox("PhysicianEmail", "", new { @id = "txtAdd_Referral_PhysicianEmail", @class = "text email input_wrapper" })%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="row">
    <div class="buttons">
        <ul>
            <li>
                <input type="submit" value="Save" /></li>
            <li>
                <input type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
            <li>
                <input type="reset" value="Reset" /></li>
        </ul>
    </div>
</div>
<%} %>
<!--[if !IE]>end forms<![endif]-->
