﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="editOrderNote" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left" id="edit_Order_Title">Edit Order </span><span class="float_right">
                <a href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content general_form">
            <% using (Html.BeginForm("EditOrder", "Patient", FormMethod.Post, new { @id = "editOrderForm" }))%>
            <%  { %>
            <%=Html.Hidden("EpisodeId", "", new { @id = "txtEdit_Order_EpisodeId" })%>
             <%=Html.Hidden("OldEpisodeId", "", new { @id = "txtEdit_Order_OldEpisodeId" })%>
            <%=Html.Hidden("PatientId", "", new { @id = "txtEdit_Order_PatientID" })%>
            <%=Html.Hidden("Id", "", new { @id = "txtEdit_OrderID" })%>
            <div id="editOrderValidaton" class="marginBreak " style="display: none">
            </div>
            <div class="row">
                <div class="contentDivider">
                    <div class="row">
                        <label for="FirstName">
                            &nbsp;&nbsp;&nbsp; Patient Name:&nbsp;&nbsp;&nbsp;</label>
                        <span name="OrderPatient" id="txtEdit_Order_Patient"></span>
                    </div>
                    <div class="row ">
                        <label for="phone">
                            &nbsp;&nbsp;&nbsp;&nbsp;Physician:&nbsp;&nbsp;&nbsp;</label>
                        <select style="width: 150px;" class="Physicians input_wrapper required selectDropDown"
                            tabindex="17" name="PhysicianId" id="txtEdit_Order_Physician">
                            <option value="0" selected>** Select Physician **</option>
                        </select>
                    </div>
                </div>
                <div class="contentDivider">
                    <div class="row">
                        <label for="FirstName">
                            &nbsp;&nbsp;&nbsp; Order Date:&nbsp;&nbsp;&nbsp;</label>
                        <%= Html.Telerik().DatePicker()
                                        .Name("OrderDate")
                                        .Value(DateTime.Today)
                                        .HtmlAttributes(new {@id = "txtEdit_Order_Date"})
                                                                         
                        %>
                    </div>
                    <div class="row">
                        <label for="FirstName">
                            &nbsp;&nbsp;&nbsp; Assign to Clinician :&nbsp;&nbsp;&nbsp;</label>
                        <select style="width: 150px;" class="Employees input_wrapper required selectDropDown"
                            tabindex="17" name="EmployeeId" id="txtEdit_Order_Assign">
                            <option value="0" selected>** Select Employee **</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label for="FirstName" class="row">
                        &nbsp;&nbsp;&nbsp; Order Summary (Optional)&nbsp;&nbsp;&nbsp;</label>
                    <div class="marginBreakNotes">
                        <%=Html.TextBox("OrderSummary", "", new { @id = "txtEdit_Order_Summary", @style = "width:100%;" })%>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="FirstName">
                    &nbsp;&nbsp;&nbsp; Order Text&nbsp;&nbsp;&nbsp;</label>
                <div class="marginBreakNotes">
                    <%=Html.TextArea("OrderText", new { @id = "txtEdit_Order_text", @class = "notesTextArea required" })%>
                </div>
            </div>
            <div class="row">
                <div class="marginBreakNotes">
                    <div style="float: left; padding-top: 10px;">
                        <input name="OrderProcessType" value="1" id="edit_order_checkbox" type="checkbox"
                            style="float: left;" />
                        Check To Send Electronically
                    </div>
                    <div class="buttons buttonfix" style="float: left; padding-left: 10px;">
                        <ul>
                            <li>
                                <input id="edit_order_save"  type="submit" value="Save" /></li>
                            <li>
                                <input type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
                            <li>
                                <input id="edit_order_sendElectronically" type="submit" value="Send Electronically" /></li>
                        </ul>
                    </div>
                </div>
            </div>
            <%} %>
        </div>
    </div>
</div>
