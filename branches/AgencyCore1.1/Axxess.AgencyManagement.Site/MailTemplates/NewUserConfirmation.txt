﻿Hi <%=firstname%>,
<br /><br />
<%=agencyname%> has granted you access to Axxess&trade; Home Health Management System.
<br /><br />
To begin using, go to:<br />
<a href='http://agencycore.axxessweb.com/Login'>http://agencycore.axxessweb.com/Login</a>
<br /><br />
Use the credentials below to login:
<br /><br />
Username:
<%=emailaddress%>
<br />
Password:
<%=password%>
<br /><br />
You will be required to change your password after your first login. Please choose a password you can easily remember.
<br /><br />
Thanks,
<br /><br />
The Axxess&trade; team