﻿var DeathAtHome = {
    _DeathId: "",
    _patientId: "",
    _EpisodeId: "",
    GetId: function() {
        return DeathAtHome._patientId;
    },
    SetId: function(patientId) {
        DeathAtHome._patientId = patientId;
    },
    GetDeathId: function() {
        return DeathAtHome._DeathId;
    },
    SetDeathId: function(DeathId) {
        DeathAtHome._DeathId = DeathId;
    },
    GetSOCEpisodeId: function() {
        return DeathAtHome._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        DeathAtHome._EpisodeId = EpisodeId;
    },
    Init: function() {
    }
    ,
    DeathAtHomeHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {

                    var actionType = control.val();
                    if (actionType == "Save/Continue") {
                        $("input[name='DischargeFromAgencyDeath_Id']").val(resultObject.Assessment.Id);
                        $("input[name='DischargeFromAgencyDeath_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='DischargeFromAgencyDeath_Action']").val('Edit');
                        Oasis.NextTab("#editDeathTabs.tabs");

                    }
                    else if (actionType == "Save/Exit") {
                        Oasis.Close(control);
                        Oasis.RebindActivity();
                    }
                    else if (actionType == "Save") {
                        $("input[name='DischargeFromAgencyDeath_Id']").val(resultObject.Assessment.Id);
                        $("input[name='DischargeFromAgencyDeath_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='DischargeFromAgencyDeath_Action']").val('Edit');
                    }

                }
                else {
                    alert(resultObject.errorMessage);
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control) {
        var form = control.closest("form");
        form.validate();
        DeathAtHome.DeathAtHomeHelper(form, control);
    },
    Validate: function() {
        OasisValidation.Validate(DeathAtHome._DeathId, "DischargeFromAgencyDeath");
    },

    loadDeathAtHome: function(id, patientId, assessmentType) {
        $('#followUpResult').load('Oasis/NewDeathContent', { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#followUpResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#death');
            }
            else if (textStatus == "success") {
                JQD.open_window('#death');
                $("#deathTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
                $("#deathTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
                $("#deathTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                    DeathAtHome.loadDeathAtHomeParts(event, ui);
                });
                DeathAtHome.Init();
            }
        }
);
    }
    ,
    loadDeathAtHomeParts: function(event, ui) {

        $($(ui.tab).attr('href')).load('Oasis/NewDeathPartContent', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
            }
        }
);
    }
        ,
    DischargeFromAgencyDeath: function() {
        var id = Oasis.GetId();
        var data = 'id=' + id;
        $("#editDeath").clearForm();
        $("#editDeath div").unblock();
        Oasis.BlockAssessmentType();
        $.ajax({
            url: '/Patient/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var patient = eval(result);
                var patientName = (patient.FirstName !== null ? patient.FirstName : "") + " " + (patient.LastName != null ? patient.LastName : "");
                $("#DischargeFromAgencyDeathTitle").text("New Death at home - " + patientName);
                $("input[name='DischargeFromAgencyDeath_Id']").val("");
                $("input[name='DischargeFromAgencyDeath_PatientGuid']").val(id);
                $("input[name='DischargeFromAgencyDeath_Action']").val('New');
                $("#DischargeFromAgencyDeath_M0020PatientIdNumber").val(patient.PatientIdNumber);
                $("#DischargeFromAgencyDeath_M0030SocDate").val(patient.StartOfCareDateFormatted);
                $("#DischargeFromAgencyDeath_M0040FirstName").val(patient.FirstName);
                $("#DischargeFromAgencyDeath_M0040MI").val(patient.MiddleInitial);
                $("#DischargeFromAgencyDeath_M0040LastName").val(patient.LastName);
                $("#DischargeFromAgencyDeath_M0050PatientState").val(patient.AddressStateCode);
                $("#DischargeFromAgencyDeath_M0060PatientZipCode").val(patient.AddressZipCode);
                $("#DischargeFromAgencyDeath_M0063PatientMedicareNumber").val(patient.MedicareNumber);
                $("#DischargeFromAgencyDeath_M0064PatientSSN").val(patient.SSN);
                $("#DischargeFromAgencyDeath_M0066PatientDoB").val(patient.DOBFormatted);
                $('input[name=DischargeFromAgencyDeath_M0069Gender][value=' + patient.Gender.toString() + ']').attr('checked', true);
                $("input[name='DischargeFromAgencyDeath_M0100AssessmentType'][value='08']").attr('checked', true);
                if (patient.EthnicRace !== null) {
                    var EthnicRaceArray = (patient.EthnicRace).split(';');
                    var i = 0;
                    for (i = 0; i < EthnicRaceArray.length; i++) {

                        if (EthnicRaceArray[i] == 1) {
                            $('input[name=DischargeFromAgencyDeath_M0140RaceAMorAN][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 2) {
                            $('input[name=DischargeFromAgencyDeath_M0140RaceAsia][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 3) {
                            $('input[name=DischargeFromAgencyDeath_M0140RaceBalck][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 4) {
                            $('input[name=DischargeFromAgencyDeath_M0140RaceHispanicOrLatino][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 5) {
                            $('input[name=DischargeFromAgencyDeath_M0140RaceNHOrPI][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 6) {
                            $('input[name=DischargeFromAgencyDeath_M0140RaceWhite][value=1]').attr('checked', true);
                        }
                    }
                }
                if (patient.PaymentSource !== null) {
                    var PaymentSourceArray = (patient.PaymentSource).split(';');
                    var i = 0;
                    for (i = 0; i < PaymentSourceArray.length; i++) {
                        if (PaymentSourceArray[i] == 0) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceNone][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 1) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceMCREFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 2) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceMCREHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 3) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceMCAIDFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 4) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceMACIDHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 5) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceWRKCOMP][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 6) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceTITLPRO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 7) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceOTHGOVT][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 8) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourcePRVINS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 9) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourcePRVHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 10) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceSelfPay][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 12) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                            $("#DischargeFromAgencyDeath_M0150PaymentSourceOther").val(patient.OtherPaymentSource);
                        }
                        if (PaymentSourceArray[i] == 11) {
                            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceUnknown][value=1]').attr('checked', true);
                        }
                    }
                }

            }
        });
    }
    ,
    EditDeathAtHome: function(id, patientId, assessmentType) {
        //var patientId = Oasis.GetId();
        $("#editDeath").clearForm();
        Oasis.BlockAssessmentType();
        var data = 'Id=' + id + "&assessmentType=" + assessmentType;
        Oasis.Refresh("#editDeath");
        $.ajax({
            url: '/Oasis/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                getNewRows(result);
            }
        });
        getNewRows = function(result) {
            var patient = eval(result);

            var firstName = result["M0040FirstName"] != null && result["M0040FirstName"] != undefined ? result["M0040FirstName"].Answer : "";
            var lastName = result["M0040LastName"] != null && result["M0040LastName"] != undefined ? result["M0040LastName"].Answer : "";
            $("#DischargeFromAgencyDeathTitle").text("Edit Death at home - " + firstName + " " + lastName);
            DeathAtHome.SetDeathId(id);
            $("input[name='DischargeFromAgencyDeath_Id']").val(id);
            $("input[name='DischargeFromAgencyDeath_Action']").val('Edit');
            $("input[name='DischargeFromAgencyDeath_PatientGuid']").val(patientId);
            $("#DischargeFromAgencyDeath_M0010CertificationNumber").val(result["M0010CertificationNumber"] != null && result["M0010CertificationNumber"] != undefined ? result["M0010CertificationNumber"].Answer : "");
            $("#DischargeFromAgencyDeath_M0014BranchState").val(result["M0014BranchState"] != null && result["M0014BranchState"] != undefined ? result["M0014BranchState"].Answer : "");
            $("#DischargeFromAgencyDeath_M0016BranchId").val(result["M0016BranchId"] != null && result["M0016BranchId"] != undefined ? result["M0016BranchId"].Answer : "");
            var nationalProviderIdUK = result["M0018NationalProviderIdUnknown"];
            if (nationalProviderIdUK != null && nationalProviderIdUK != undefined) {
                if (nationalProviderIdUK.Answer == 1) {

                    $('input[name=DischargeFromAgencyDeath_M0018NationalProviderIdUnknown][value=1]').attr('checked', true);
                    $("#DischargeFromAgencyDeath_M0018NationalProviderId").val(" ");
                }
                else {
                    $('input[name=DischargeFromAgencyDeath_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                    $("#DischargeFromAgencyDeath_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
                }
            }
            else {
                $('input[name=DischargeFromAgencyDeath_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                $("#DischargeFromAgencyDeath_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
            }
            $("#DischargeFromAgencyDeath_M0020PatientIdNumber").val(result["M0020PatientIdNumber"] != null && result["M0020PatientIdNumber"] != undefined ? result["M0020PatientIdNumber"].Answer : "");
            $("#DischargeFromAgencyDeath_M0030SocDate").val(result["M0030SocDate"] != null && result["M0030SocDate"] != undefined ? result["M0030SocDate"].Answer : "");
            $("#DischargeFromAgencyDeath_GenericEpisodeStartDate").val(result["GenericEpisodeStartDate"] != null && result["GenericEpisodeStartDate"] != undefined ? result["GenericEpisodeStartDate"].Answer : "");
            var rOCDateNotApplicable = result["M0032ROCDateNotApplicable"];
            if (rOCDateNotApplicable != null && rOCDateNotApplicable != undefined) {
                if (rOCDateNotApplicable.Answer == 1) {

                    $('input[name=DischargeFromAgencyDeath_M0032ROCDateNotApplicable][value=1]').attr('checked', true);
                    $("#DischargeFromAgencyDeath_M0032ROCDate").val(" ");
                }
                else {
                    $('input[name=DischargeFromAgencyDeath_M0032ROCDateNotApplicable][value=1]').attr('checked', false);
                    $("#DischargeFromAgencyDeath_M0032ROCDate").val(result["M0032ROCDate"] != null && result["M0032ROCDate"] != undefined ? result["M0032ROCDate"].Answer : "");
                }
            }
            else {
                $('input[name=DischargeFromAgencyDeath_M0032ROCDateNotApplicable][value=1]').attr('checked', false);
                $("#DischargeFromAgencyDeath_M0032ROCDate").val(result["M0032ROCDate"] != null && result["M0032ROCDate"] != undefined ? result["M0032ROCDate"].Answer : "");
            }
            $("#DischargeFromAgencyDeath_M0040FirstName").val(firstName);
            $("#DischargeFromAgencyDeath_M0040MI").val(result["M0040MI"] != null && result["M0040MI"] != undefined ? result["M0040MI"].Answer : "");
            $("#DischargeFromAgencyDeath_M0040LastName").val(lastName);
            $("#DischargeFromAgencyDeath_M0050PatientState").val(result["M0050PatientState"] != null && result["M0050PatientState"] != undefined ? result["M0050PatientState"].Answer : "");
            $("#DischargeFromAgencyDeath_M0060PatientZipCode").val(result["M0060PatientZipCode"] != null && result["M0060PatientZipCode"] != undefined ? result["M0060PatientZipCode"].Answer : "");
            var patientMedicareNumberUK = result["M0063PatientMedicareNumberUnknown"];
            if (patientMedicareNumberUK != null && patientMedicareNumberUK != undefined) {
                if (patientMedicareNumberUK.Answer == 1) {

                    $('input[name=DischargeFromAgencyDeath_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', true);
                    $("#DischargeFromAgencyDeath_M0063PatientMedicareNumber").val(" ");
                }
                else {
                    $('input[name=DischargeFromAgencyDeath_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', false);
                    $("#DischargeFromAgencyDeath_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
                }
            }
            else {
                $('input[name=DischargeFromAgencyDeath_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', false);
                $("#DischargeFromAgencyDeath_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
            }
            var patientSSNUK = result["M0064PatientSSNUnknown"];
            if (patientSSNUK != null && patientSSNUK != undefined) {
                if (patientSSNUK.Answer == 1) {

                    $('input[name=DischargeFromAgencyDeath_M0064PatientSSNUnknown][value=1]').attr('checked', true);
                    $("#DischargeFromAgencyDeath_M0064PatientSSN").val(" ");
                }
                else {
                    $('input[name=DischargeFromAgencyDeath_M0064PatientSSNUnknown][value=1]').attr('checked', false);
                    $("#DischargeFromAgencyDeath_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
                }
            }
            else {
                $('input[name=DischargeFromAgencyDeath_M0064PatientSSNUnknown][value=1]').attr('checked', false);
                $("#DischargeFromAgencyDeath_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
            }
            var patientMedicaidNumberUK = result["M0065PatientMedicaidNumberUnknown"];
            if (patientMedicaidNumberUK != null && patientMedicaidNumberUK != undefined) {
                if (patientMedicaidNumberUK.Answer == 1) {

                    $('input[name=DischargeFromAgencyDeath_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', true);
                    $("#DischargeFromAgencyDeath_M0065PatientMedicaidNumber").val(" ");
                }
                else {
                    $('input[name=DischargeFromAgencyDeath_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', false);
                    $("#DischargeFromAgencyDeath_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
                }
            } else {
                $('input[name=DischargeFromAgencyDeath_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', false);
                $("#DischargeFromAgencyDeath_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
            }

            $("#DischargeFromAgencyDeath_M0066PatientDoB").val(result["M0066PatientDoB"] != null && result["M0066PatientDoB"] != undefined ? result["M0066PatientDoB"].Answer : "");
            $('input[name=DischargeFromAgencyDeath_M0069Gender][value=' + (result["M0069Gender"] != null && result["M0069Gender"] != undefined ? result["M0069Gender"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgencyDeath_M0080DisciplinePerson][value=' + (result["M0080DisciplinePerson"] != null && result["M0080DisciplinePerson"] != undefined ? result["M0080DisciplinePerson"].Answer : "") + ']').attr('checked', true);
            $("#DischargeFromAgencyDeath_M0090AssessmentCompleted").val(result["M0090AssessmentCompleted"] != null && result["M0090AssessmentCompleted"] != undefined ? result["M0090AssessmentCompleted"].Answer : "");
            $("input[name='DischargeFromAgencyDeath_M0100AssessmentType'][value='08']").attr('checked', true);

            var physicianOrderedDateNotApplicable = result["M0102PhysicianOrderedDateNotApplicable"];
            if (physicianOrderedDateNotApplicable != null && physicianOrderedDateNotApplicable != undefined) {
                if (physicianOrderedDateNotApplicable.Answer == 1) {

                    $('input[name=DischargeFromAgencyDeath_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', true);
                    $("#DischargeFromAgencyDeath_M0102PhysicianOrderedDate").val(" ");
                }
                else {
                    $('input[name=DischargeFromAgencyDeath_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                    $("#DischargeFromAgencyDeath_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
                }
            }
            else {
                $('input[name=DischargeFromAgencyDeath_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                $("#DischargeFromAgencyDeath_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
            }

            $("#DischargeFromAgencyDeath_M0104ReferralDate").val(result["M0104ReferralDate"] != null && result["M0104ReferralDate"] != undefined ? result["M0104ReferralDate"].Answer : "");

            $('input[name=DischargeFromAgencyDeath_M0110EpisodeTiming][value=' + (result["M0110EpisodeTiming"] != null && result["M0110EpisodeTiming"] != undefined ? result["M0110EpisodeTiming"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0140RaceAMorAN][value=' + (result["M0140RaceAMorAN"] != null && result["M0140RaceAMorAN"] != undefined ? result["M0140RaceAMorAN"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0140RaceAsia][value=' + (result["M0140RaceAsia"] != null && result["M0140RaceAsia"] != undefined ? result["M0140RaceAsia"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0140RaceBalck][value=' + (result["M0140RaceBalck"] != null && result["M0140RaceBalck"] != undefined ? result["M0140RaceBalck"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0140RaceHispanicOrLatino][value=' + (result["M0140RaceHispanicOrLatino"] != null && result["M0140RaceHispanicOrLatino"] != undefined ? result["M0140RaceHispanicOrLatino"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0140RaceNHOrPI][value=' + (result["M0140RaceNHOrPI"] != null && result["M0140RaceNHOrPI"] != undefined ? result["M0140RaceNHOrPI"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0140RaceWhite][value=' + (result["M0140RaceWhite"] != null && result["M0140RaceWhite"] != undefined ? result["M0140RaceWhite"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceNone][value=' + (result["M0150PaymentSourceNone"] != null && result["M0150PaymentSourceNone"] != undefined ? result["M0150PaymentSourceNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceMCREFFS][value=' + (result["M0150PaymentSourceMCREFFS"] != null && result["M0150PaymentSourceMCREFFS"] != undefined ? result["M0150PaymentSourceMCREFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceMCREHMO][value=' + (result["M0150PaymentSourceMCREHMO"] != null && result["M0150PaymentSourceMCREHMO"] != undefined ? result["M0150PaymentSourceMCREHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceMCAIDFFS][value=' + (result["M0150PaymentSourceMCAIDFFS"] != null && result["M0150PaymentSourceMCAIDFFS"] != undefined ? result["M0150PaymentSourceMCAIDFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceMACIDHMO][value=' + (result["M0150PaymentSourceMACIDHMO"] != null && result["M0150PaymentSourceMACIDHMO"] != undefined ? result["M0150PaymentSourceMACIDHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceWRKCOMP][value=' + (result["M0150PaymentSourceWRKCOMP"] != null && result["M0150PaymentSourceWRKCOMP"] != undefined ? result["M0150PaymentSourceWRKCOMP"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceTITLPRO][value=' + (result["M0150PaymentSourceTITLPRO"] != null && result["M0150PaymentSourceTITLPRO"] != undefined ? result["M0150PaymentSourceTITLPRO"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceOTHGOVT][value=' + (result["M0150PaymentSourceOTHGOVT"] != null && result["M0150PaymentSourceOTHGOVT"] != undefined ? result["M0150PaymentSourceOTHGOVT"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourcePRVINS][value=' + (result["M0150PaymentSourcePRVINS"] != null && result["M0150PaymentSourcePRVINS"] != undefined ? result["M0150PaymentSourcePRVINS"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourcePRVHMO][value=' + (result["M0150PaymentSourcePRVHMO"] != null && result["M0150PaymentSourcePRVHMO"] != undefined ? result["M0150PaymentSourcePRVHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceSelfPay][value=' + (result["M0150PaymentSourceSelfPay"] != null && result["M0150PaymentSourceSelfPay"] != undefined ? result["M0150PaymentSourceSelfPay"].Answer : "") + ']').attr('checked', true);

            var paymentSourceOtherSRS = result["M0150PaymentSourceOtherSRS"];
            if (paymentSourceOtherSRS != null && paymentSourceOtherSRS != undefined) {
                if (paymentSourceOtherSRS.Answer == 1) {

                    $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                    $("#DischargeFromAgencyDeath_M0150PaymentSourceOther").val(result["M0150PaymentSourceOther"] != null && result["M0150PaymentSourceOther"] != undefined ? result["M0150PaymentSourceOther"].Answer : "");

                }
                else {
                    $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceOtherSRS][value=1]').attr('checked', false);

                }
            }
            $('input[name=DischargeFromAgencyDeath_M0150PaymentSourceUnknown][value=' + (result["M0150PaymentSourceUnknown"] != null && result["M0150PaymentSourceUnknown"] != undefined ? result["M0150PaymentSourceUnknown"].Answer : "") + ']').attr('checked', true);



            $("#DischargeFromAgencyDeath_M0903LastHomeVisitDate").val(result["M0903LastHomeVisitDate"] != null && result["M0903LastHomeVisitDate"] != undefined ? result["M0903LastHomeVisitDate"].Answer : "");
            $("#DischargeFromAgencyDeath_M0906DischargeDate").val(result["M0906DischargeDate"] != null && result["M0906DischargeDate"] != undefined ? result["M0906DischargeDate"].Answer : "");
        };
    }

}