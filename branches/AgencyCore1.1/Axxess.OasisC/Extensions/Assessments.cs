﻿namespace Axxess.OasisC.Extensions
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    public static class Assessments
    {
        public static IDictionary<string, Question> ToDictionary(this Assessment assessment)
        {
            IDictionary<string, Question> questions = new Dictionary<string, Question>();
            if (assessment.Questions != null)
            {
                var key = string.Empty;
                assessment.Questions.ForEach(question =>
                {
                    if (question.Type == QuestionType.Moo)
                    {
                        key = string.Format("{0}{1}", question.Code, question.Name);
                        if (!questions.ContainsKey(key))
                        {
                            questions.Add(key, question);
                        }
                    }
                    else if (question.Type == QuestionType.PlanofCare)
                    {
                        key = string.Format("485{0}", question.Name);
                          if (!questions.ContainsKey(key))
                          {
                              questions.Add(key, question);
                          }
                    }
                    else if (question.Type == QuestionType.Generic)
                    {
                        key = string.Format("Generic{0}", question.Name);
                          if (!questions.ContainsKey(key))
                          {
                              questions.Add(key, question);
                          }
                    }
                    else
                    {
                        key = string.Format("{0}", question.Name);
                          if (!questions.ContainsKey(key))
                          {
                              questions.Add(key, question);
                          }
                    }
                });
            }
            return questions;
        }

        public static IDictionary<string, Question> ToDictionary(this PlanofCare planofCare)
        {
            IDictionary<string, Question> questions = new Dictionary<string, Question>();
            if (planofCare.Questions != null)
            {
                planofCare.Questions.ForEach(question =>
                {
                    string key = string.Empty;
                    if (question.Type == QuestionType.Moo)
                    {
                        key = string.Format("{0}{1}", question.Code, question.Name);
                    }
                    else if (question.Type == QuestionType.PlanofCare)
                    {
                        key = string.Format("485{0}", question.Name);
                    }
                    else
                    {
                        key = string.Format("{0}", question.Name);
                    }
                    if (!questions.ContainsKey(key))
                    {
                        questions.Add(key, question);
                    }
                });
            }
            return questions;
        }
    }
}
