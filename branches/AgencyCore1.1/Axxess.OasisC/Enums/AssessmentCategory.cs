﻿namespace Axxess.OasisC.Enums
{
    using System.ComponentModel;

    public enum AssessmentCategory
    {
        [Description("Demographics")]
        Demographics = 0,
        [Description("Patient History & Diagnoses")]
        PatientHistory = 1,
        [Description("Risk Assessment")]
        RiskAssessment = 2,
        [Description("Prognosis")]
        Prognosis = 3,
        [Description("Supportive Assistance")]
        SupportiveAssistance = 4,
        [Description("Sensory Status")]
        SensoryStatus = 5,
        [Description("Pain")]
        Pain = 6,
        [Description("Integumentary Status")]
        IntegumentaryStatus = 7,
        [Description("Respiratory Status")]
        RespiratoryStatus = 8,
        [Description("Endocrine")]
        Endocrine = 9,
        [Description("Cardiac Status")]
        CardiacStatus = 10,
        [Description("Elimination Status")]
        EliminationStatus = 11,
        [Description("Nutrition")]
        Nutrition = 12,
        [Description("Neuro/Behaviourial Status")]
        BehaviourialStatus = 13,
        [Description("ADL/IADLst")]
        ADL = 14,
        [Description("Supplies Worksheet")]
        SuppliesWorksheet = 15,
        [Description("Supplies Used This Visit")]
        Supplies = 16,
        [Description("Medications")]
        Medications = 17,
        [Description("Care Management")]
        CareManagement = 18,
        [Description("Therapy Need & Plan Of Care")]
        TherapyNeedPlanOfCare = 19,
        [Description("Orders for Discipline and Treatment")]
        Orders = 20
    }
}