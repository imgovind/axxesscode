﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;

    using SubSonic.Repository;

    public class CachedDataRepository : ICachedDataRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public CachedDataRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region ICachedDataRepository Member

        public List<OasisGuide> GetOasisGuides()
        {
            List<OasisGuide> oasisGuides = null;
            if (!Cacher.TryGet(CacheKey.OasisGuide.ToString(), out oasisGuides))
            {
                oasisGuides = database.All<OasisGuide>().ToList();
                Cacher.Set(CacheKey.OasisGuide.ToString(), oasisGuides);
            }
            return oasisGuides;
        }

        public OasisGuide GetOasisGuide(string mooCode)
        {
            List<OasisGuide> oasisGuides = GetOasisGuides();
            return oasisGuides.Where(o => o.Id.IsEqual(mooCode)).SingleOrDefault();
        }

        public List<SubmissionBodyFormat> GetSubmissionFormatInstructions()
        {
            List<SubmissionBodyFormat> submissionformat = null;
            if (!Cacher.TryGet(CacheKey.SubmissionBodyFormat.ToString(), out submissionformat))
            {
                submissionformat = database.All<SubmissionBodyFormat>().ToList();
                Cacher.Set(CacheKey.SubmissionBodyFormat.ToString(), submissionformat);
            }
            return submissionformat;
        }

        public List<SubmissionHeaderFormat> GetSubmissionHeaderFormatInstructions()
        {
            List<SubmissionHeaderFormat> submissionformat = null;
            if (!Cacher.TryGet(CacheKey.SubmissionHeaderFormat.ToString(), out submissionformat))
            {
                submissionformat = database.All<SubmissionHeaderFormat>().ToList();
                Cacher.Set(CacheKey.SubmissionHeaderFormat.ToString(), submissionformat);
            }
            return submissionformat;
        }

        public List<SubmissionFooterFormat> GetSubmissionFooterFormatInstructions()
        {
            List<SubmissionFooterFormat> submissionformat = null;
            if (!Cacher.TryGet(CacheKey.SubmissionFooterFormat.ToString(), out submissionformat))
            {
                submissionformat = database.All<SubmissionFooterFormat>().ToList();
                Cacher.Set(CacheKey.SubmissionFooterFormat.ToString(), submissionformat);
            }
            return submissionformat;
        }

        #endregion

    }
}
