﻿namespace Axxess.OasisC.Domain
{
    using System;

    using Enums;

    [Serializable]
    public class TransferDischargeAssessment : Assessment
    {
        public TransferDischargeAssessment()
        {
            this.Type = AssessmentType.TransferInPatientDischarged;
        }
    }
}
