﻿namespace Axxess.OasisC.Domain
{
    using System;

    using Enums;

    [Serializable]
    public class RecertificationAssessment : Assessment
    {
        public RecertificationAssessment()
        {
            this.Type = AssessmentType.Recertification;
        }
    }
}
