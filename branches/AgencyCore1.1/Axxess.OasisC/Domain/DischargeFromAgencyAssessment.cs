﻿namespace Axxess.OasisC.Domain
{
    using System;

    using Enums;

    [Serializable]
    public class DischargeFromAgencyAssessment : Assessment
    {
        public DischargeFromAgencyAssessment()
        {
            this.Type = AssessmentType.DischargeFromAgency;
        }
    }
}
