﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    public class PlanofCare
    {
        public PlanofCare()
        {
            this.Questions = new List<Question>();
        }
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid UserId { get; set; }
        public Guid AssessmentId { get; set; }
        public string AssessmentType { get; set; } 
        public string Data { get; set; }
        public int Status { get; set; }
        public string EpisodeStart { get; set; }
        public string EpisodeEnd { get; set; }
        public long OrderNumber { get; set; }
        public Guid PatientId { get; set; }
        public DateTime Created { get; set; }
        public DateTime SentDate { get; set; }
        public string PhysicianSignatureText { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime Modified { get; set; }
        public string AgencyData { get; set; }
        public string PatientData { get; set; }
        public string PhysicianData { get; set; }
        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }
        public bool IsDeprecated { get; set; }

        [SubSonicIgnore]
        public string MedicationProfileData { get; set; }

        [SubSonicIgnore]
        public List<Question> Questions { get; set; }
    }
}
