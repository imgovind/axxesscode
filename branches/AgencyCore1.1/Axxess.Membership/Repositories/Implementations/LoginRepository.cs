﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class LoginRepository : ILoginRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public LoginRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IUserRepository Members

        public Guid Add(string emailAddress, string password, string salt, string role, string displayName)
        {
            Check.Argument.IsNotEmpty(salt, "salt");
            Check.Argument.IsNotEmpty(role, "role");
            Check.Argument.IsNotEmpty(password, "password");
            Check.Argument.IsNotInvalidEmail(emailAddress, "emailAddress");

            var newLogin = new Login();
            try
            {
                newLogin.Id = Guid.NewGuid();
                newLogin.EmailAddress = emailAddress;
                newLogin.PasswordHash = password;
                newLogin.PasswordSalt = salt;
                newLogin.Role = role;
                newLogin.DisplayName = displayName;
                newLogin.IsActive = true;
                newLogin.IsLocked = false;
                newLogin.IsAxxessAdmin = false;
                newLogin.IsPasswordChangeRequired = true;
                newLogin.LastLoginDate = DateTime.Now;
                newLogin.Created = DateTime.Now;
                database.Add<Login>(newLogin);
            }
            catch (Exception ex)
            {
                newLogin.Id = Guid.Empty;
                //TODO: Log exception
            }
            return newLogin.Id;
        }

        public Guid Add(string emailAddress, string password, string salt, string role, string displayName, bool changePassword, bool weekendAccess, string earliestLoginTime, string automaticLogoutTime)
        {
            Check.Argument.IsNotEmpty(salt, "salt");
            Check.Argument.IsNotEmpty(role, "role");
            Check.Argument.IsNotEmpty(password, "password");
            Check.Argument.IsNotInvalidEmail(emailAddress, "emailAddress");

            var newLogin = new Login();
            try
            {
                newLogin.Id = Guid.NewGuid();
                newLogin.EmailAddress = emailAddress;
                newLogin.PasswordHash = password;
                newLogin.PasswordSalt = salt;
                newLogin.Role = role;
                newLogin.DisplayName = displayName;
                newLogin.IsActive = true;
                newLogin.IsLocked = false;
                newLogin.IsAxxessAdmin = false;
                newLogin.IsPasswordChangeRequired = changePassword;
                newLogin.LastLoginDate = DateTime.Now;
                newLogin.AllowWeekendAccess = weekendAccess;
                newLogin.AutomaticLogoutTime = automaticLogoutTime;
                newLogin.EarliestLoginTime = earliestLoginTime;
                newLogin.Created = DateTime.Now;
                database.Add<Login>(newLogin);
            }
            catch (Exception ex)
            {
                //TODO: Log exception
            }
            return newLogin.Id;
        }

        public Login Find(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            return database.Single<Login>(l => l.Id == id);
        }

        public bool ChangePassword(Guid loginId, string password, string salt, bool requirePasswordChange)
        {
            Check.Argument.IsNotEmpty(salt, "salt");
            Check.Argument.IsNotEmpty(loginId, "loginId");
            Check.Argument.IsNotEmpty(password, "password");

            var login = database.Single<Login>(l => l.Id == loginId);

            if (login != null)
            {
                login.PasswordHash = password;
                login.PasswordSalt = salt;
                login.IsPasswordChangeRequired = requirePasswordChange;
                database.Update<Login>(login);

                return true;
            }
            return false;
        }

        public Login Find(string emailAddress)
        {
            Check.Argument.IsNotInvalidEmail(emailAddress, "emailAddress");

            return database.Single<Login>(l => l.EmailAddress == emailAddress.Trim());
        }

        public bool Update(Login login)
        {
            if (login != null)
            {
                database.Update<Login>(login);
                return true;
            }
            return false;
        }
        public bool Update(Guid loginId, string emailAddress, string password, string salt, string role, string displayName)
        {
            Check.Argument.IsNotEmpty(salt, "salt");
            Check.Argument.IsNotEmpty(role, "role");
            Check.Argument.IsNotEmpty(loginId, "loginId");
            Check.Argument.IsNotEmpty(password, "password");
            Check.Argument.IsNotInvalidEmail(emailAddress, "emailAddress");

            bool result = false;
            var login = database.Single<Login>(l => l.Id == loginId);

            if (login.EmailAddress != emailAddress && Find(emailAddress) != null)
            {
                throw new ArgumentException("\"{0}\" already exits. Specify a different email address.".FormatWith(emailAddress));
            }

            if (login != null)
            {
                login.EmailAddress = emailAddress;
                login.PasswordHash = password;
                login.DisplayName = displayName;
                login.PasswordSalt = salt;
                login.Role = role;
                database.Update<Login>(login);
                result = true;
            }

            return result;
        }

        public bool Update(Guid loginId, string emailAddress, string password, string salt, string role, string displayName, bool changePassword, bool weekendAccess, string earliestLoginTime, string automaticLogoutTime)
        {
            Check.Argument.IsNotEmpty(salt, "salt");
            Check.Argument.IsNotEmpty(role, "role");
            Check.Argument.IsNotEmpty(loginId, "loginId");
            Check.Argument.IsNotEmpty(password, "password");
            Check.Argument.IsNotInvalidEmail(emailAddress, "emailAddress");

            bool result = false;
            var login = database.Single<Login>(l => l.Id == loginId);

            if (login.EmailAddress != emailAddress && Find(emailAddress) != null)
            {
                throw new ArgumentException("\"{0}\" already exits. Specify a different email address.".FormatWith(emailAddress));
            }

            if (login != null)
            {
                login.EmailAddress = emailAddress;
                login.PasswordHash = password;
                login.PasswordSalt = salt;
                login.Role = role;
                login.DisplayName = displayName;
                login.IsPasswordChangeRequired = changePassword;
                login.AutomaticLogoutTime = automaticLogoutTime;
                login.EarliestLoginTime = earliestLoginTime;
                login.AllowWeekendAccess = weekendAccess;
                database.Update<Login>(login);
                result = true;
            }

            return result;
        }

        public bool Delete(Guid loginId)
        {
            Check.Argument.IsNotEmpty(loginId, "loginId");

            bool result = false;
            var login = database.Single<Login>(l => l.Id == loginId);

            if (login != null)
            {
                database.Delete<Login>(loginId);
                result = true;
            }
            return result;
        }

        public ICollection<Login> GetAll()
        {
            return database.All<Login>().ToList().AsReadOnly();
        }

        public ICollection<Login> AutoComplete(string searchString)
        {
            return database.Find<Login>(u => u.EmailAddress.StartsWith(searchString)).ToList();
        }

        #endregion
    }
}
