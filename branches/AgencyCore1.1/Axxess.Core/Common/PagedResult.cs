﻿namespace Axxess.Core
{
    using System.Diagnostics;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class PagedResult<T>
    {
        private readonly int total;
        private readonly ReadOnlyCollection<T> result;

        public PagedResult(IEnumerable<T> result, int total)
        {
            Check.Argument.IsNotNull(result, "result");
            Check.Argument.IsNotNegative(total, "total");
            
            this.total = total;
            this.result = new ReadOnlyCollection<T>(new List<T>(result));
        }

        public PagedResult()
            : this(new List<T>(), 0)
        {
        }

        public ICollection<T> Result
        {
            [DebuggerStepThrough]
            get
            {
                return result;
            }
        }

        public int Total
        {
            [DebuggerStepThrough]
            get
            {
                return total;
            }
        }

        public bool IsEmpty
        {
            [DebuggerStepThrough]
            get
            {
                return result.Count == 0;
            }
        }
    }
}
