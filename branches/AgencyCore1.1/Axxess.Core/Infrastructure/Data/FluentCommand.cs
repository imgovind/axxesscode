﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Diagnostics;

    using MySql.Data.MySqlClient;

    public partial class FluentCommand<T>
    {
        public delegate T ResultMapDelegate(DataReader reader);

        private readonly string commandText;

        protected ResultMapDelegate mapper;
        protected readonly DbCommand command;

        public FluentCommand(string commandText)
        {
            command = CreateCommand();
            command.CommandText = commandText;
            this.commandText = commandText;
        }

        public FluentCommand(string commandText, DbConnection connection)
        {
            command = CreateCommand();
            command.Connection = connection;
            command.CommandText = commandText;
            this.commandText = commandText;
        }

        protected DbCommand CreateCommand()
        {
            return new MySqlCommand();
        }

        public FluentCommand<T> SetConnection(DbConnection connection)
        {
            command.Connection = connection;
            return this;
        }

        public FluentCommand<T> SetMap(ResultMapDelegate resultMapDelegate)
        {
            mapper = resultMapDelegate;
            return this;
        }

        public void SetTransaction(DbTransaction transaction)
        {
            command.Transaction = transaction;
            return;
        }

        public List<T> AsList()
        {
            Check.Argument.IsNotNull(mapper, "You must call SetMap to use AsList");

            Debug.WriteLine("Executing SQL: " + commandText);

            var returnList = new List<T>();
            using (command)
            {
                using (DbDataReader reader = command.ExecuteReader())
                {
                    var myReader = new DataReader(reader);
                    while (reader.Read())
                    {
                        T data = mapper.Invoke(myReader);
                        returnList.Add(data);
                    }
                }
            }
            return returnList;
        }

        public IEnumerable<T> AsEnumerable()
        {
            Check.Argument.IsNotNull(mapper, "You must call SetMap to use AsEnumerable");

            Debug.WriteLine("Executing SQL: " + commandText);

            using (command)
            {
                using (DbDataReader reader = command.ExecuteReader())
                {
                    var myReader = new DataReader(reader);
                    while (reader.Read())
                    {
                        T data = mapper.Invoke(myReader);
                        yield return data;
                    }
                }
            }
        }

        public IList<T> AsList(ResultMapDelegate map)
        {
            mapper = map;
            return AsList();
        }

        public IEnumerable<T> AsEnumerable(ResultMapDelegate map)
        {
            mapper = map;
            return AsEnumerable();
        }

        public DbDataReader AsDataReader()
        {
            Debug.WriteLine("Executing SQL: " + commandText);

            using (command)
            {
                using (DbDataReader reader = command.ExecuteReader())
                {
                    return reader;
                }
            }
        }

        public T AsScalar()
        {
            Debug.WriteLine("Executing SQL: " + commandText);

            using (command)
            {
                return (T)command.ExecuteScalar();
            }
        }

        public T AsSingle()
        {
            Check.Argument.IsNotNull(mapper, "You must call SetMap to use AsSingle");

            Debug.WriteLine("Executing SQL: " + commandText);

            using (command)
            {
                using (DbDataReader reader = command.ExecuteReader())
                {
                    var myReader = new DataReader(reader);
                    if (reader.Read())
                    {
                        return mapper.Invoke(myReader);
                    }
                }
            }
            return default(T);
        }

        public int AsNonQuery()
        {
            Debug.WriteLine("Executing SQL: " + commandText);

            using (command)
            {
                return command.ExecuteNonQuery();
            }
        }

        public T AsScalar(params object[] list)
        {
            ResetParams(list);
            return (T)command.ExecuteScalar();
        }

        public T AsSingle(params object[] list)
        {
            Check.Argument.IsNotNull(mapper, "You must call SetMap to use AsSingle");

            ResetParams(list);
            Debug.WriteLine("Executing SQL: " + commandText);

            using (DbDataReader reader = command.ExecuteReader())
            {
                var myReader = new DataReader(reader);
                if (reader.Read())
                {
                    T data = mapper.Invoke(myReader);
                    return data;
                }
            }
            return default(T);
        }

        public int AsNonQuery(params object[] list)
        {
            ResetParams(list);
            return command.ExecuteNonQuery();
        }

        private void ResetParams(params object[] list)
        {
            Debug.Assert(list.Length == command.Parameters.Count);
            for (int i = 0; i < list.Length; i++)
            {
                command.Parameters[i].Value = SetParameterValue(list[i]);
            }
        }

        protected object SetParameterValue(object value)
        {
            return value ?? DBNull.Value;
        }

        protected object SetGuidParamValue(object value)
        {
            if (value == null)
                return DBNull.Value;
            return value.ToString();
        }

    }
}
