﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Net.Mail;
    using System.Threading;

    public class EmailNotification : INotification
    {
        #region INotification Members

        public void Send(string fromAddress, string toAddress, string subject, string body)
        {
            using (MailMessage mail = CreateMessage(fromAddress, toAddress, subject, body))
            {
                SendMail(mail);
            }
        }

        public void SendAsync(string fromAddress, string toAddress, string subject, string body)
        {
            ThreadPool.QueueUserWorkItem(state => Send(fromAddress, toAddress, subject, body));
        }

        #endregion

        #region Static Methods

        private static void SendMail(MailMessage mail)
        {
            try
            {
                SmtpClient smtp = new SmtpClient
                {
                   EnableSsl = AppSettings.EnableSSLMail
                };

                smtp.Send(mail);
            }
            catch (Exception)
            {
            }
        }

        private static MailMessage CreateMessage(string fromAddress, string toAddress, string subject, string body)
        {
            MailMessage message = new MailMessage
            {
                Sender = new MailAddress(AppSettings.NoReplyEmail),
                From = new MailAddress(fromAddress, AppSettings.NoReplyDisplayName),
                Subject = subject,
                Body = body,
                IsBodyHtml = true,
            };

            string[] tos = toAddress.Split(new char[1] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string to in tos)
            {
                message.To.Add(new MailAddress(to));
            }

            return message;
        }

        #endregion
    }
}
