﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace Axxess.Core.Infrastructure
{
    public class CleverLog : ILog
    {
        #region ILog Members

        public void Info(string message)
        {
            LogEngine.Instance.Add(new LogEntry(message) { Priority = LogPriority.Info });
        }

        public void Warning(string message)
        {
            LogEngine.Instance.Add(new LogEntry(message) { Priority = LogPriority.Warn });
        }

        public void Error(string message)
        {
            LogEngine.Instance.Add(new LogEntry(message) { Priority = LogPriority.Error });
        }

        public void Exception(Exception e)
        {
            StackTrace stackTrace = new StackTrace();
            StackFrame stackFrame = stackTrace.GetFrame(1);
            MethodBase methodBase = stackFrame.GetMethod();

            LogEngine.Instance.Add(
                new LogEntry(e) { 
                    Priority = LogPriority.Fatal, 
                    MethodName = methodBase.Name,
                    FileName = stackFrame.GetFileName(),
                    LineNumber = stackFrame.GetFileLineNumber(),
                    ExceptionType = e.GetType().ToString()
                });
        }

        #endregion
    }
}
