﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Diagnostics;

    public static class GuidExtension
    {
        [DebuggerStepThrough]
        public static bool IsEmpty(this Guid target)
        {
            return target == Guid.Empty;
        }
    }

}
