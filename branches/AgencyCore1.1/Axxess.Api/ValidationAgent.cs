﻿namespace Axxess.Api
{
    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using System;
    using System.ServiceModel;

    public class ValidationAgent : BaseAgent<IValidationService>
    {
        #region Overrides

        public override string ToString()
        {
            return "OasisValidationService";
        }

        #endregion

        #region Base Service Methods

        public bool Ping()
        {
            return Service.Ping();
        }

        #endregion

        #region Validation Methods

        public List<ValidationError> ValidateAssessment(string oasisDataString)
        {
            var validationErrors = new List<ValidationError>();
            BaseAgent<IValidationService>.Call(v => validationErrors = v.ValidateAssessment(oasisDataString), this.ToString());
            return validationErrors;
        }

        #endregion

    }
}
