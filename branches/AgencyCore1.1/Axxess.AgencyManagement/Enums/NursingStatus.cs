﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;

    public enum NursingStatus
    {
        Scheduled,
        [Description("Not Started")]
        NotStarted,
        [Description("Past Due")]
        PastDue,
        [Description("Completed")]
        Completed,      
        [Description("Submitted With Signature")]
        Submitted,
        [Description("Missed Visit")]
        MissedVisit
    }
}
