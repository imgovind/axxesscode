﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;

    public enum HHAStatus
    {
        [Description("Visit")]
        Visit,
        [Description("Supervisory Visit")]
        SupervisoryVisit
    }
}
