﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;

    public enum InvoiceType
    {
        [Description("UB-04")]
        UB = 1,
        [Description("HCFA 1500")]
        HCFA = 2,
        [Description("Invoice")]
        Invoice = 3
    }
}
