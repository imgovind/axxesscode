﻿namespace Axxess.AgencyManagement.Enums
{
    public enum UserStatus : byte
    {
        Active = 1,
        Inactive = 2
    }
}
