﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using SubSonic.Repository;

    public class MessageRepository : IMessageRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public MessageRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IMessageRepository Members

        public bool Add(Message message)
        {
            bool result = false;
            if (message != null)
            {
                message.Id = Guid.NewGuid();
                message.Created = DateTime.Now;
                database.Add<Message>(message);
                result = true;
            }

            return result;
        }

        public bool Delete(Guid id)
        {
            var message = database.Single<Message>(m => m.Id == id);
            if (message != null)
            {
                database.Delete<Message>(id);
                return true;
            }
            return false;
        }

        public PagedResult<Message> GetUserMessages(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");

            var query = database.Find<Message>(m => m.RecipientId == userId)
                .OrderByDescending(m => m.Created);

            query.ForEach(m =>
            {
                if (!m.PatientId.IsEmpty())
                {
                    var patient = database.Single<Patient>(p => p.Id == m.PatientId);
                    if (patient != null)
                    {
                        m.PatientName = patient.DisplayName;
                    }
                }
            });

            return new PagedResult<Message>(query, query.Count());
        }

        public PagedResult<Message> GetMessagesByDateCreated(DateTime startDate, DateTime endDate)
        {
            Check.Argument.IsNotInvalidDate(endDate, "endDate");
            Check.Argument.IsNotInvalidDate(startDate, "startDate");

            var query = database.Find<Message>(m => m.Created >= startDate && m.Created <= endDate)
                .OrderBy(m => m.Created);

            return new PagedResult<Message>(query, query.Count());
        }

        public PagedResult<Message> GetReadMessages()
        {
            var query = database.Find<Message>(m => m.MarkAsRead == true)
                         .OrderBy(m => m.Created);

            return new PagedResult<Message>(query, query.Count());
        }

        public Message GetMessage(Guid id, bool markAsRead)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var message = database.Single<Message>(m => m.Id == id);

            if (message != null)
            {
                message.MarkAsRead = markAsRead;
                database.Update<Message>(message);

                var patient = database.Single<Patient>(p => p.Id == message.PatientId);
                if (patient != null)
                {
                    message.PatientName = patient.DisplayName;
                }
            }

            return message;
        }

        public int MessageCount(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");

            return database.Find<Message>(m => m.MarkAsRead == false && m.RecipientId == userId).Count;
        }

        #endregion
    }
}
