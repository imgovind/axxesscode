﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;

    using AutoMapper;

    using SubSonic.Repository;

    public class AgencyRepository : IAgencyRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region Agency Methods

        public bool Delete(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id);
            if (agency != null)
            {
                agency.IsDeprecated = true;
                agency.Modified = DateTime.Now;
                database.Update<Agency>(agency);
                return true;
            }

            return false;
        }

        public Agency Get(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id && a.IsDeprecated == false);
            if (agency != null)
            {
                agency.MainLocation = GetMainLocation(id);
            }

            return agency;
        }

        public bool Add(Agency agency)
        {
            if (agency != null)
            {
                agency.Id = Guid.NewGuid();
                agency.Created = DateTime.Now;
                agency.Modified = DateTime.Now;

                database.Add<Agency>(agency);
                return true;
            }
            return false;
        }

        public bool AddLocation(AgencyLocation agencyLocation)
        {
            if (agencyLocation != null)
            {
                agencyLocation.Id = Guid.NewGuid();
                if (agencyLocation.PhoneArray != null && agencyLocation.PhoneArray.Count > 0)
                {
                    agencyLocation.PhoneWork = agencyLocation.PhoneArray.ToArray().PhoneEncode();
                }
                if (agencyLocation.FaxNumberArray != null && agencyLocation.FaxNumberArray.Count > 0)
                {
                    agencyLocation.FaxNumber = agencyLocation.FaxNumberArray.ToArray().PhoneEncode();
                }
                agencyLocation.Created = DateTime.Now;
                agencyLocation.Modified = DateTime.Now;

                database.Add<AgencyLocation>(agencyLocation);
                return true;
            }

            return false;
        }

        public AgencyLocation GetMainLocation(Guid agencyId)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true && l.IsDeprecated == false);
        }

        public IList<AgencyLocation> GetBranches(Guid agencyId)
        {
            return database.GetPaged<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false, "Created", 0, 20).ToList();
        }

        public AgencyLocation FindLocation(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == Id && l.IsDeprecated == false);
        }

        public bool EditLocation(AgencyLocation location)
        {
            var result = false;
            var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id && l.IsDeprecated == false);
            if (location != null && existingLocation != null)
            {
                if (location.PhoneArray.Count > 0)
                {
                    existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                }
                if (location.FaxNumberArray.Count > 0)
                {
                    existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingLocation.Name = location.Name;
                existingLocation.AddressLine1 = location.AddressLine1;
                existingLocation.AddressLine2 = location.AddressLine2;
                existingLocation.AddressCity = location.AddressCity;
                existingLocation.AddressStateCode = location.AddressStateCode;
                existingLocation.AddressZipCode = location.AddressZipCode;
                existingLocation.Comments = location.Comments;
                existingLocation.Modified = DateTime.Now;
                database.Update<AgencyLocation>(existingLocation);
                result = true;
            }
            return result;
        }

        public bool EditBranchCost(AgencyLocation location)
        {
            var result = false;
            var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
            if (location != null && existingLocation != null)
            {
                existingLocation.Cost = location.Cost;
                existingLocation.Modified = DateTime.Now;
                database.Update<AgencyLocation>(existingLocation);
                result = true;
            }
            return result;
        }

        public IEnumerable<Agency> All()
        {
            return database.All<Agency>().AsEnumerable<Agency>();
        }

        public bool Edit(Agency agencyInfo)
        {
            Check.Argument.IsNotNull(agencyInfo, "agencyViewData");

            bool result = false;
            var agency = database.Single<Agency>(a => a.Id == agencyInfo.Id && a.IsDeprecated == false);

            if (agency != null)
            {
                agency.Name = agencyInfo.Name;
                agency.TaxId = agencyInfo.TaxId;
                agency.ContactPersonEmail = agency.ContactPersonEmail;
                agency.ContactPersonFirstName = agency.ContactPersonFirstName;
                agency.ContactPersonLastName = agency.ContactPersonLastName;
                agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                agency.TaxIdType = agencyInfo.TaxIdType;
                agency.Payor = agencyInfo.Payor;
                agency.NationalProviderNumber = agencyInfo.NationalProviderNumber;
                agency.MedicareProviderNumber = agencyInfo.MedicareProviderNumber;
                agency.MedicaidProviderNumber = agencyInfo.MedicaidProviderNumber;
                agency.HomeHealthAgencyId = agencyInfo.HomeHealthAgencyId;
                agency.Modified = DateTime.Now;

                database.Update<Agency>(agencyInfo);
                result = true;
            }

            return result;
        }

        #endregion

        #region Contact Methods

        public bool AddContact(AgencyContact contact)
        {
            var result = false;
            if (contact != null)
            {
                contact.Id = Guid.NewGuid();
                if (contact.PhonePrimaryArray.Count > 0)
                {
                    contact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    contact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                contact.Created = DateTime.Now;
                contact.Modified = DateTime.Now;

                database.Add<AgencyContact>(contact);
                result = true;
            }
            return result;
        }

        public IList<AgencyContact> GetContacts(Guid agencyId)
        {
            return database.Find<AgencyContact>(c => c.AgencyId == agencyId && c.IsDeprecated == false).ToList();
        }

        public AgencyContact FindContact(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyContact>(c => c.Id == Id && c.AgencyId == agencyId && c.IsDeprecated == false);
        }

        public bool EditContact(AgencyContact contact)
        {
            var result = false;
            var existingContact = database.Single<AgencyContact>(c => c.Id == contact.Id && c.AgencyId == contact.AgencyId && c.IsDeprecated == false);
            if (contact != null)
            {

                if (contact.PhonePrimaryArray.Count > 0)
                {
                    existingContact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    existingContact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                existingContact.FirstName = contact.FirstName;
                existingContact.LastName = contact.LastName;
                existingContact.CompanyName = contact.CompanyName;
                existingContact.AddressLine1 = contact.AddressLine1;
                existingContact.AddressLine2 = contact.AddressLine2;
                existingContact.AddressCity = contact.AddressCity;
                existingContact.AddressStateCode = contact.AddressStateCode;
                existingContact.AddressZipCode = contact.AddressZipCode;
                existingContact.EmailAddress = contact.EmailAddress;
                existingContact.ContactType = contact.ContactType;
                existingContact.ContactTypeOther = contact.ContactTypeOther;
                existingContact.Modified = DateTime.Now;
                database.Update<AgencyContact>(existingContact);
                result = true;
            }
            return result;
        }

        public bool DeleteContact(Guid agencyId, Guid id)
        {
            var contact = database.Single<AgencyContact>(c => c.Id == id && c.AgencyId == agencyId);
            if (contact != null)
            {
                contact.IsDeprecated = true;
                contact.Modified = DateTime.Now;
                database.Update<AgencyContact>(contact);
                return true;
            }
            return false;
        }

        #endregion

        #region Insurance

        public bool AddInsurance(AgencyInsurance insurance)
        {
            var result = false;
            if (insurance != null)
            {
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    insurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    insurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                insurance.Created = DateTime.Now;
                insurance.Modified = DateTime.Now;

                database.Add<AgencyInsurance>(insurance);
                result = true;
            }
            return result;
        }

        public AgencyInsurance GetInsurance(int insuranceId, Guid agencyId)
        {
            return database.Single<AgencyInsurance>(i => i.Id == insuranceId && i.AgencyId == agencyId);
        }

        public IList<AgencyInsurance> GetInsurances(Guid agencyId)
        {
            return database.Find<AgencyInsurance>(i => i.AgencyId == agencyId && i.IsDeprecated == false).ToList();
        }

        public AgencyInsurance FindInsurance(Guid agencyId, int Id)
        {
            return database.Single<AgencyInsurance>(i => i.Id == Id && i.AgencyId == agencyId && i.IsDeprecated == false);
        }

        public bool EditInsurance(AgencyInsurance insurance)
        {
            var result = false;
            var existingInsurance = database.Single<AgencyInsurance>(I => I.Id == insurance.Id && I.AgencyId == insurance.AgencyId);
            if (insurance != null)
            {

                if (insurance.PhoneNumberArray.Count > 0)
                {
                    existingInsurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    existingInsurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingInsurance.PayorType = insurance.PayorType;
                existingInsurance.InvoiceType = insurance.InvoiceType;
                existingInsurance.ChargeGrouping = insurance.ChargeGrouping;
                existingInsurance.ChargeType = insurance.ChargeType;
                existingInsurance.ParentInsurance = insurance.ParentInsurance;
                existingInsurance.Name = insurance.Name;
                existingInsurance.AddressLine1 = insurance.AddressLine1;
                existingInsurance.AddressLine2 = insurance.AddressLine2;
                existingInsurance.AddressCity = insurance.AddressCity;
                existingInsurance.AddressStateCode = insurance.AddressStateCode;
                existingInsurance.AddressZipCode = insurance.AddressZipCode;
                existingInsurance.ProviderCode = insurance.ProviderCode;
                existingInsurance.ProviderNumber = insurance.ProviderNumber;
                existingInsurance.HealthPlanId = insurance.HealthPlanId;
                existingInsurance.OtherProviderId = insurance.OtherProviderId;
                existingInsurance.Ub04Locator81cca = insurance.Ub04Locator81cca;
                existingInsurance.ClearingHouse = insurance.ClearingHouse;
                existingInsurance.InterchangeReceiverId = insurance.InterchangeReceiverId;
                existingInsurance.PayorId = insurance.PayorId;
                existingInsurance.ContactPersonFirstName = insurance.ContactPersonFirstName;
                existingInsurance.ContactPersonLastName = insurance.ContactPersonLastName;
                existingInsurance.ContactEmailAddress = insurance.ContactEmailAddress;
                existingInsurance.CurrentBalance = insurance.CurrentBalance;
                existingInsurance.WorkWeekStartDay = insurance.WorkWeekStartDay;
                existingInsurance.IsVisitAuthorizationRequired = insurance.IsVisitAuthorizationRequired;
                existingInsurance.DefaultFiscalIntermediary = insurance.DefaultFiscalIntermediary;
                existingInsurance.Charge = insurance.Charge;
                existingInsurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(existingInsurance);
                result = true;
            }
            return result;
        }
        public bool DeleteInsurance(Guid agencyId, int Id)
        {
            var insurance = database.Single<AgencyInsurance>(i => i.Id == Id && i.AgencyId == agencyId);
            if (insurance != null)
            {
                insurance.IsDeprecated = true;
                insurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(insurance);
                return true;
            }
            return false;
        }

        #endregion

        #region Hospital

        public bool AddHospital(AgencyHospital hospital)
        {
            var result = false;
            if (hospital != null)
            {
                hospital.Id = Guid.NewGuid();
                if (hospital.PhoneArray.Count > 0)
                {
                    hospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    hospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                hospital.Created = DateTime.Now;
                hospital.Modified = DateTime.Now;

                database.Add<AgencyHospital>(hospital);
                result = true;
            }
            return result;
        }

        public IList<AgencyHospital> GetHospitals(Guid agencyId)
        {
            return database.Find<AgencyHospital>(h => h.AgencyId == agencyId && h.IsDeprecated == false).ToList();
        }

        public AgencyHospital FindHospital(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId && h.IsDeprecated == false);
        }

        public bool EditHospital(AgencyHospital hospital)
        {
            var result = false;
            var existingHospital = database.Single<AgencyHospital>(h => h.Id == hospital.Id && h.AgencyId == hospital.AgencyId);
            if (hospital != null)
            {

                if (hospital.PhoneArray.Count > 0)
                {
                    existingHospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    existingHospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingHospital.Name = hospital.Name;
                existingHospital.ContactPersonFirstName = hospital.ContactPersonFirstName;
                existingHospital.ContactPersonLastName = hospital.ContactPersonLastName;
                existingHospital.AddressLine1 = hospital.AddressLine1;
                existingHospital.AddressLine2 = hospital.AddressLine2;
                existingHospital.AddressCity = hospital.AddressCity;
                existingHospital.AddressStateCode = hospital.AddressStateCode;
                existingHospital.AddressZipCode = hospital.AddressZipCode;
                existingHospital.EmailAddress = hospital.EmailAddress;
                existingHospital.Comment = hospital.Comment;
                existingHospital.Modified = DateTime.Now;

                database.Update<AgencyHospital>(existingHospital);
                result = true;
            }
            return result;

        }

        public bool DeleteHospital(Guid agencyId, Guid Id)
        {
            var hospital = database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId);
            if (hospital != null)
            {
                hospital.IsDeprecated = true;
                hospital.Modified = DateTime.Now;
                database.Update<AgencyHospital>(hospital);
                return true;
            }
            return false;
        }

        #endregion
    }
}
