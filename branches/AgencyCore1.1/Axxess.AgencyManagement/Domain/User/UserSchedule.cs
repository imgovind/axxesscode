﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class UserSchedule
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid UserId { get; set; }
        public Guid PatientId { get; set; }
        public string Visits { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
