﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class RecertEvent
    {
        public string Task { get; set; }
        public string Status { get; set; }
        public string EpisodeId { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string AssignedTo { get; set; }
        public string TargetDate { get; set; }
    }
}
