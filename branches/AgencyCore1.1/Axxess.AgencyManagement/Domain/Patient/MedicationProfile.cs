﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    public class MedicationProfile
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public string Medication { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhone { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (this.Medication.IsNotNullOrEmpty())
            {
                var medications = this.Medication.ToObject<List<Medication>>();
                medications.ForEach(m =>
                {
                    sb.AppendFormat("{0} {1} {2}", m.MedicationDosage, m.Frequency, m.Route);
                    sb.AppendLine();
                });
            }
            return sb.ToString();
        }
    }
}
