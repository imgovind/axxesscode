﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;

    [XmlRoot()]    
   public class MedicationType
    {
        [XmlAttribute]
       public string Text { get; set; }
        [XmlAttribute]
       public string Value { get; set; }
    }
}
