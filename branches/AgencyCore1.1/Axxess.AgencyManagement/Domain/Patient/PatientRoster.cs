﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class PatientRoster
    {
        public Guid Id { get; set; }
        public string PatientId { get; set; }
        public string PatientLastName { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientDisplayName { get; set; }
        public string PatientGender { get; set; }
        public string PatientMedicareNumber { get; set; }
        public DateTime PatientDOB { get; set; }
        public string PatientPhone { get; set; }
        public string PatientAddressLine1 { get; set; }
        public string PatientAddressLine2 { get; set; }
        public string PatientAddressCity { get; set; }
        public string PatientAddressStateCode { get; set; }
        public string PatientAddressZipCode { get; set; }
        public string PatientSoC { get; set; }
        public string PatientPrimaryDiagnosis { get; set; }
        public string PatientSecondaryDiagnosis { get; set; }
        public string PhysicianName { get; set; }
        public string PhysicianPhone { get; set; }
        public string PhysicianFacsimile { get; set; }
        public string PatientInsurance { get; set; }
        public string PatientCaseManager { get; set; }
        public string PhysicianPhoneHome { get; set; }
        public string PhysicianPhoneMobile { get; set; }
        public string PhysicianEmailAddress { get; set; }
        public string ResponsibleEmployee { get; set; }
    }
}
