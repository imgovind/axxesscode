﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;

    public class Claim
    {
        public int Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string PatientIdNumber { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public bool IsOasisComplete { get; set; }
        public bool IsFirstBillableVisit { get; set; }
        public bool IsRapGenerated { get; set; }
        public bool IsRapVerified { get; set; }
        public DateTime RapModified { get; set; }
        public string RapRemark { get; set; }
        public DateTime Created { get; set; }        

        public string MedicareNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public DateTime StartofCareDate { get; set; }
        public string PhysicianNPI { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }

        public DateTime FirstBillableVisitDate { get; set; }
        public string DiagonasisCode { get; set; }
        public string HippsCode { get; set; }
        public string ClaimKey { get; set; }

        public bool AreOrdersComplete { get; set; }
        public bool AreVisitsComplete { get; set; }
        public bool IsFinalGenerated { get; set; }
        public DateTime FinalModified { get; set; }       

        public string VerifiedVisits { get; set; }
        public int PrimaryInsuranceId { get; set; }

        public bool IsVisitVerified { get; set; }
        public bool IsSupplyVerified { get; set; }
        public bool IsFinalInfoVerified { get; set; }
        public string FinalRemark { get; set; }
        

        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }
        [SubSonicIgnore]
        public string NotVerifiedVisits { get; set; }


    }
}
