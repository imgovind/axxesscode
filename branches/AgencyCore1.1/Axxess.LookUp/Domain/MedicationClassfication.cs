﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.LookUp.Domain
{
   public class MedicationClassfication
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
