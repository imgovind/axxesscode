﻿namespace Axxess.LookUp.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface ILookupRepository
    {
        IList<Supply> Supplies();
        Supply GetSupply(int Id);
        IList<EthnicRace> Races();
        IList<AmericanState> States();
        IList<PaymentSource> PaymentSources();
        IList<DiagnosisCode> DiagnosisCodes();
        IList<ProcedureCode> ProcedureCodes();
        IList<Insurance> Insurances();
        IList<SupplyCategory> SupplyCategories();
        IList<ReferralSource> ReferralSources();
        IList<AdmissionSource> AdmissionSources();
        IList<DrugClassification> DrugClassifications();
        IList<DisciplineTask> DisciplineTasks(string Discipline);
        IList<DisciplineTask> DisciplineTasks();
        Npi GetNpiData(string npi);
        AdmissionSource GetAdmissionSource(int sourceId);
        IList<Npi> GetNpis(string q, int limit);
        ZipCode GetZipCode(string zipCode);
        bool VerifyPecos(string npi);
        IPAddress GetIPAddress(int ipAddress);
        string GetZipCodeFromIpAddress(int ipAddress);
        IList<MedicationRoute> MedicationRoute(string q, int limit);
        IList<MedicationClassfication> MedicationClassfication(string q, int limit);
        IList<MedicationDosage> MedicationDosage(string q, int limit);

        Insurance GetInsurance(int insuranceId);
    }
}
