﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Web;
    using Enums;
    using Domain;
    using Services;
    using ViewData;

    using Axxess.AgencyManagement.Domain;
    using Telerik.Web.Mvc;

    [Compress]
    [Authorize]
    [HandleError]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ReportController : BaseController
    {
        #region Constructor / Member

        private readonly IReportService reportService;
        public ReportController(IReportService reportService)
        {
            Check.Argument.IsNotNull(reportService, "reportService");
            this.reportService = reportService;
        }

        #endregion

        #region ReportController Actions

        #region General Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Center()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Dashboard()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Patient()
        {
            return PartialView("Patient/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Clinical()
        {
            return PartialView("Clinical/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Schedule()
        {
            return PartialView("Schedule/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Billing()
        {
            return PartialView("Billing/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Employee()
        {
            return PartialView("Employee/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Statistical()
        {
            return PartialView("Statistical/Home");
        }

        #endregion

        #region Patient Reports

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessReports)]
        public ActionResult PatientRoster()
        {
            return PartialView("Patient/Roster");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessReports)]
        public ActionResult PatientRoster([Bind] ReportParameters parameters)
        {
            return View(new GridModel(reportService.GetPatientRoster(parameters.StatusId, parameters.AddressBranchCode)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessReports)]
        public ActionResult PatientEmergencyList()
        {
            return PartialView("Patient/EmergencyList");
        }
        
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessReports)]
        public ActionResult PatientEmergencyList([Bind] ReportParameters parameters)
        {
            return View(new GridModel(reportService.GetPatientEmergencyContacts(parameters.StatusId, parameters.AddressBranchCode)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessReports)]
        public ActionResult PatientBirthdayList()
        {
            return PartialView("Patient/BirthdayList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessReports)]
        public ActionResult PatientBirthdayList([Bind] ReportParameters parameters)
        {
            return View(new GridModel(reportService.GetPatientBirthdays(parameters.AddressBranchCode)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessReports)]
        public ActionResult PatientAddressList()
        {
            return PartialView("Patient/AddressList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessReports)]
        public ActionResult PatientAddressList([Bind] ReportParameters parameters)
        {
            return View(new GridModel(reportService.GetPatientAddressListing(parameters.AddressBranchCode)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessReports)]
        public ActionResult PatientByPhysicians()
        {
            return PartialView("Patient/Physician");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessReports)]
        public ActionResult PatientByPhysicians([Bind] ReportParameters parameters)
        {
            return View(new GridModel(reportService.GetPatientByPhysician(parameters.AgencyPhysicianId)));
        }
        

        

       
        

        [GridAction]
        public JsonResult PatientBirthdayWidget()
        {
            var viewData = new List<Birthday>();
            viewData = reportService.GetCurrentBirthdays();
            return Json(new GridModel(viewData));
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientSocCertPeriodListing()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult PatientSocCertPeriodListingResult(Guid Name, string AddressStateCode, Guid AddressBranchCode, string ReportStep)
        {
            if (ReportStep != "2")
            {
                return View(new GridModel(new List<PatientSocCertPeriod>()));
            }
            var viewData = new List<PatientSocCertPeriod>();
            viewData = reportService.GetPatientSocCertPeriod(new DateTime(1936, 1, 1), DateTime.Now);
            return View(new GridModel(viewData));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientOnCallListing()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult PatientOnCallListingResult(Guid Name, string AddressStateCode, Guid AddressBranchCode, string ReportStep)
        {
            if (ReportStep != "2")
            {
                return View(new GridModel(new List<PatientOnCallListing>()));
            }
            var viewData = new List<PatientOnCallListing>();
            viewData = reportService.GetPatientOnCallListing(new DateTime(1936, 1, 1), DateTime.Now);
            return View(new GridModel(viewData));
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientByResponsibleEmployeeListing()
        {
            return PartialView();
        }

        #endregion

        #region Clinical Reports

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessReports)]
        public ActionResult ClinicalOpenOasis()
        {
            return PartialView("Clinical/OpenOasis");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessReports)]
        public ActionResult ClinicalOpenOasis([Bind] ReportParameters parameters)
        {
            return View(new GridModel(reportService.GetAllOpenOasis()));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessReports)]
        public ActionResult ClinicalOrders()
        {
            return PartialView("Clinical/Orders");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessReports)]
        public ActionResult ClinicalOrders([Bind] ReportParameters parameters)
        {
            return View(new GridModel(reportService.GetOrders(parameters.StatusId)));
        }

        #endregion

        #endregion
    }
}
