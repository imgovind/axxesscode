﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;

    using Web;
    using Services;
    using Components;
    using Enums;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
  
    using Telerik.Web.Mvc;
    using Axxess.AgencyManagement.Enums;

    [Compress]
    [Authorize]
    [HandleError]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ExportController : BaseController
    {
        #region Private Members/Constructor

        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IReferralRepository referralRepository;
        private readonly IPhysicianRepository physicianRepository;

        public ExportController(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region ExportController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ExportListToExcel)]
        public FileResult Contacts()
        {
            var contacts = agencyRepository.GetContacts(Current.AgencyId);
            var export = new ContactExporter(contacts);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Contacts.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ExportListToExcel)]
        public FileResult Hospitals()
        {
            var hospitals = agencyRepository.GetHospitals(Current.AgencyId);
            var export = new HosptialExporter(hospitals);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Hospitals.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ExportListToExcel)]
        public FileResult Physicians()
        {
            var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId);
            var export = new PhysicianExporter(physicians);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Physicians.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ExportListToExcel)]
        public FileResult Referrals()
        {
            var referrals = referralRepository.GetAll(Current.AgencyId, ReferralStatus.Pending);
            var export = new ReferralExporter(referrals.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Referrals.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ExportListToExcel)]
        public FileResult Patients()
        {
            var patients = patientRepository.GetAllByAgencyId(Current.AgencyId);
            var export = new PatientExporter(patients.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Patients.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ExportListToExcel)]
        public FileResult Users()
        {
            var users = userRepository.GetAgencyUsers(Current.AgencyId);
            var export = new UserExporter(users.ToList());
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "Users.xls");
        }

        #endregion
    }
}
