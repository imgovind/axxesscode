﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web;
    using System.Text;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Collections.Generic;

    using Axxess.LookUp;
    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    using Axxess.OasisC.Repositories;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using System.Web.Routing;

    public static class HtmlHelperExtensions
    {
        private static IUserRepository userRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.UserRepository;
            }
        }

        private static IAssetRepository assetRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AssetRepository;
            }
        }

        private static IPatientRepository patientRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.PatientRepository;
            }
        }

        private static IAgencyRepository agencyRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AgencyRepository;
            }
        }

        private static IPhysicianRepository physicianRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.PhysicianRepository;
            }
        }

        private static ILookupRepository lookupRepository
        {
            get
            {
                ILookUpDataProvider dataProvider = Container.Resolve<ILookUpDataProvider>();
                return dataProvider.LookUpRepository;
            }
        }

        private static ICachedDataRepository cachedDataRepository
        {
            get
            {
                IOasisCDataProvider dataProvider = Container.Resolve<IOasisCDataProvider>();
                return dataProvider.CachedDataRepository;
            }
        }

        public static MvcHtmlString ZipCode(this HtmlHelper html)
        {
            //var ipAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            //if (ipAddress.IsNullOrEmpty())
            //{
            //    ipAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            //}

            //int intAddress = BitConverter.ToInt32(System.Net.IPAddress.Parse(ipAddress).GetAddressBytes(), 0);

            //var zipcode = lookupRepository.GetZipCodeFromIpAddress(intAddress);

            //if (zipcode.IsNullOrEmpty())
            //{
            //    zipcode = "75243";
            //}
            var zipCode = "75243";
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null && agency.MainLocation != null)
            {
                zipCode = agency.MainLocation.AddressZipCode;
            }
            return MvcHtmlString.Create(zipCode);
        }

        public static MvcHtmlString Asset(this HtmlHelper html, Guid assetId)
        {
            var sb = new StringBuilder();
            if (!assetId.IsEmpty())
            {
                var asset = assetRepository.Get(assetId, Current.AgencyId);
                if (asset != null)
                {
                    sb.AppendFormat("<a href=\"/Asset/{0}\">{1}</a>&nbsp;", asset.Id.ToString(), asset.FileName);
                }
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Recipients(this HtmlHelper html)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div class=\"recipientList\" id=\"recipientList\">");
            sb.AppendLine("<div class=\"inboxSubHeader\"><span>Recipient List</span></div>");
            sb.AppendLine("<div class=\"recipientPanel\"><div class=\"recipient\"><input type=\"checkbox\" class=\"contact\" id=\"New_Message_CheckallRecipients\" />&nbsp;<label for=\"New_Message_CheckallRecipients\" class=\"bold\">Select All</label></div>");

            var recipients = userRepository.GetAgencyUsers(Current.AgencyId);
            if (recipients.Count > 0)
            {
                int counter = 1;
                recipients.ForEach(r =>
                {
                    if (r.Id != Current.UserId)
                    {
                        sb.AppendFormat("<div class=\"recipient\"><input name=\"Recipients\" type=\"checkbox\" id=\"New_Message_Recipient_{0}\" value=\"{1}\" title=\"{2}\" onclick=\"Message.AddRemoveRecipient('New_Message_Recipient_{0}');\" />&nbsp;<label for=\"New_Message_Recipient_{0}\">{2}</label></div>", counter.ToString(), r.Id.ToString(), r.DisplayName);
                        sb.AppendLine();
                        counter++;
                    }
                });
            }
            sb.AppendLine("</div>");
            sb.AppendLine("</div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Agencies(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var agencies = agencyRepository.All();
            tempItems = from agency in agencies
                        select new SelectListItem
                        {
                            Text = agency.Name,
                            Value = agency.Id.ToString(),
                            Selected = (agency.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Agency --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Insurances(this HtmlHelper html, string name, string value, bool addNew, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var standardInsurances = lookupRepository.Insurances();
            tempItems = from standardInsurance in standardInsurances
                        select new SelectListItem
                        {
                            Text = standardInsurance.Name,
                            Value = standardInsurance.Id.ToString(),
                            Selected = (standardInsurance.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Insurance --",
                Value = "0"
            });

            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            if (addNew)
            {
                items.Insert(1, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
                items.Insert(2, new SelectListItem
                {
                    Text = "** Add New Insurance **",
                    Value = "new"
                });
                items.Insert(3, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString ReportBranchList(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var branches = agencyRepository.GetBranches(Current.AgencyId);
            tempItems = from branch in branches
                        select new SelectListItem
                        {
                            Text = branch.Name,
                            Value = branch.Id.ToString(),
                            Selected = (branch.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- All Branches --",
                Value = Guid.Empty.ToString(),
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DisciplineTaskList(this HtmlHelper html, string discipline, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var tasks = lookupRepository.DisciplineTasks(discipline);
            tempItems = from task in tasks
                        select new SelectListItem
                        {
                            Text = task.Task,
                            Value = task.Id.ToString(),
                            Selected = (task.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Task --",
                Value = "",
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LookupSelectList(this HtmlHelper html, SelectListTypes listType, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            switch (listType)
            {
                case SelectListTypes.States:
                    var states = lookupRepository.States();
                    tempItems = from state in states
                                select new SelectListItem
                                {
                                    Text = state.Name,
                                    Value = state.Code,
                                    Selected = (state.Code.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select State --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.Insurance:
                    var insurances = agencyRepository.GetInsurances(Current.AgencyId);
                    tempItems = from insurance in insurances
                                select new SelectListItem
                                {
                                    Text = insurance.Name,
                                    Value = insurance.Id.ToString(),
                                    Selected = (insurance.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Insurance --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.ContactTypes:
                    var contactTypes = new List<string>();
                    Array contactTypeValues = Enum.GetValues(typeof(ContactTypes));
                    foreach (ContactTypes contactType in contactTypeValues)
                    {
                        contactTypes.Add(contactType.GetDescription());
                    }

                    tempItems = from type in contactTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Contact Type --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.CredentialTypes:
                    var credentialTypes = new List<string>();
                    Array credentialValues = Enum.GetValues(typeof(CredentialTypes));
                    foreach (CredentialTypes credentialType in credentialValues)
                    {
                        credentialTypes.Add(credentialType.GetDescription());
                    }

                    tempItems = from type in credentialTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Unknown --",
                        Value = ""
                    });
                    break;
                case SelectListTypes.TitleTypes:
                    var titleTypes = new List<string>();
                    Array titleValues = Enum.GetValues(typeof(TitleTypes));
                    foreach (TitleTypes titleType in titleValues)
                    {
                        titleTypes.Add(titleType.GetDescription());
                    }

                    tempItems = from type in titleTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Title Type --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.AdmissionSources:
                    var adminSources = lookupRepository.AdmissionSources();
                    tempItems = from source in adminSources
                                select new SelectListItem
                                {
                                    Text = string.Format("({0}) {1}", source.Code, source.Description),
                                    Value = source.Id.ToString(),
                                    Selected = (source.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Admission Source --",
                        Value = "0",
                    });
                    break;
                case SelectListTypes.Users:
                    var users = userRepository.GetAgencyUsers(Current.AgencyId);
                    tempItems = from user in users
                                select new SelectListItem
                                {
                                    Text = user.DisplayName,
                                    Value = user.Id.ToString(),
                                    Selected = (user.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select User --",
                        Value = Guid.Empty.ToString(),
                    });
                    break;
                case SelectListTypes.Branches:
                    var branches = agencyRepository.GetBranches(Current.AgencyId);
                    tempItems = from branch in branches
                                select new SelectListItem
                                {
                                    Text = branch.Name,
                                    Value = branch.Id.ToString(),
                                    Selected = (branch.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    break;
                case SelectListTypes.Patients:
                    var patients = patientRepository.GetAllByAgencyId(Current.AgencyId);
                    tempItems = from patient in patients
                                select new SelectListItem
                                {
                                    Text = patient.DisplayName,
                                    Value = patient.Id.ToString(),
                                    Selected = (patient.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Patient --",
                        Value = Guid.Empty.ToString()
                    });
                    break;
                case SelectListTypes.Physicians:
                    var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId);
                    tempItems = from physician in physicians
                                select new SelectListItem
                                {
                                    Text = string.Format("{0} (NPI: {1})", physician.DisplayName, physician.NPI),
                                    Value = physician.Id.ToString(),
                                    Selected = (physician.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Physician --",
                        Value = Guid.Empty.ToString()
                    });

                    break;
                case SelectListTypes.PaymentSource:
                    var paymentSources = lookupRepository.PaymentSources();
                    tempItems = from paymentSource in paymentSources
                                select new SelectListItem
                                {
                                    Text = paymentSource.Name,
                                    Value = paymentSource.Id.ToString(),
                                    Selected = (paymentSource.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                      items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Payment Source --",
                        Value = "0"
                    });
                      items.Insert(11, new SelectListItem
                      {
                          Text = "Contract",
                          Value = "13"
                      });
                      items.Insert(4, new SelectListItem
                      {
                          Text = "Medicaid (Traditional)",
                          Value = "13"
                      });
                    break;
                default:
                    break;
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Physicians(this HtmlHelper html, string name, string value, bool addNew, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId);
            tempItems = from physician in physicians
                        select new SelectListItem
                        {
                            Text = string.Format("{0} ({1})", physician.DisplayName, physician.NPI),
                            Value = physician.Id.ToString(),
                            Selected = (physician.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Physician --",
                Value = Guid.Empty.ToString()
            });

            if (addNew)
            {
                items.Insert(1, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
                items.Insert(2, new SelectListItem
                {
                    Text = "** Add New Physician **",
                    Value = "new"
                });
                items.Insert(3, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PermissionList(this HtmlHelper html, string category)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("<strong>{0}</strong>", category);
            sb.Append("<table class=\"form\">");

            Permissions[] permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            foreach (Permissions permission in permissions)
            {
                ulong id = (ulong)permission;
                Permission p = permission.GetPermission();
                if (p.Category == category)
                {
                    sb.AppendLine("<tr><td>");
                    sb.AppendFormat("<input id=\"New_User_Permission_{0}\" type=\"checkbox\" value=\"{0}\" name=\"PermissionsArray\" class=\"required radio float_left\" />", id.ToString());
                    sb.AppendFormat("<label for=\"New_User_Permission_{0}\" class=\"radio bold\">{1}<span style=\"font-weight:normal;\"> - {2}</span></label>", id.ToString(), p.Description, p.LongDescription);
                    sb.AppendLine("</td></tr>");
                }
            }
            sb.AppendLine("</table>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString CaseManagers(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetCaseManagerUsers(Current.AgencyId);
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Case Manager --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Clinicians(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetClinicalUsers(Current.AgencyId);
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Clinician --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

    }
}
