﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class OasisModule : Module
    {
        public override string Name
        {
            get { return "Oasis"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "485Print",
                "485/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "View485", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "485Medication",
                "485/Medication/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "View485Medication", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "SocPrint",
                "StartofCare/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SocPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "SocBlank",
               "StartofCare/View/Blank",
               new { controller = this.Name, action = "SocBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "RocPrint",
                "ResumptionOfCare/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "RocPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "RecertPrint",
                "Recertification/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "RecertPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "TransferNotDischargePrint",
                "TransferNotDischarge/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "TransferNotDischargePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "TransferDischargePrint",
                "TransferDischarge/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "TransferDischargePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "DischargePrint",
                "Discharge/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "DischargePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "DeathPrint",
                "DischargeDeath/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "DeathPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "FollowUpPrint",
                "Followup/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "FollowUpPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            
        }
    }
}
