﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class ScheduleModule : Module
    {
        public override string Name
        {
            get { return "Schedule"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               "MissedVisit",
               "Visit/Miss",
               new { controller = this.Name, action = "MissedVisit", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             "HHACarePlanBlank",
             "HHACarePlan/View/Blank",
             new { controller = this.Name, action = "HHACarePlanBlank", id = UrlParameter.Optional });

            routes.MapRoute(
             "HHACarePlanPrint",
             "HHACarePlan/View/{episodeId}/{patientId}/{eventId}",
             new { controller = this.Name, action = "HHACarePlanPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
             "TransferSummaryBlank",
             "TransferSummary/View/Blank",
             new { controller = this.Name, action = "TransferSummaryBlank", id = UrlParameter.Optional });
            
            routes.MapRoute(
             "TransferSummaryPrint",
             "TransferSummary/View/{episodeId}/{patientId}/{eventId}",
             new { controller = this.Name, action = "TransferSummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
             "HHAVisitNoteBlank",
             "HHAVisitNote/View/Blank",
             new { controller = this.Name, action = "HHAVisitNoteBlank", id = UrlParameter.Optional });

            routes.MapRoute(
             "HHAVisitNotePrint",
             "HHAVisitNote/View/{episodeId}/{patientId}/{eventId}",
             new { controller = this.Name, action = "HHAVisitNotePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            
            routes.MapRoute(
             "SixtyDaySummaryBlank",
             "SixtyDaySummary/View/Blank",
             new { controller = this.Name, action = "SixtyDaySummaryBlank", id = UrlParameter.Optional });

            routes.MapRoute(
             "SixtyDaySummaryPrint",
             "SixtyDaySummary/View/{episodeId}/{patientId}/{eventId}",
             new { controller = this.Name, action = "SixtyDaySummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
             "LVNSVisitBlank",
             "LVNSupervisoryVisit/View/Blank",
             new { controller = this.Name, action = "LVNSVisitBlank", id = UrlParameter.Optional });

            routes.MapRoute(
             "LVNSVisitPrint",
             "LVNSupervisoryVisit/View/{episodeId}/{patientId}/{eventId}",
             new { controller = this.Name, action = "LVNSVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
             "HHASVisitBlank",
             "HHAideSupervisoryVisit/View/Blank",
             new { controller = this.Name, action = "HHASVisitBlank", id = UrlParameter.Optional });

            routes.MapRoute(
             "HHASVisitPrint",
             "HHAideSupervisoryVisit/View/{episodeId}/{patientId}/{eventId}",
             new { controller = this.Name, action = "HHASVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
             "DischargeSummaryBlank",
             "DischargeSummary/View/Blank",
             new { controller = this.Name, action = "DischargeSummaryBlank", id = UrlParameter.Optional });

            routes.MapRoute(
             "DischargeSummaryPrint",
             "DischargeSummary/View/{episodeId}/{patientId}/{eventId}",
             new { controller = this.Name, action = "DischargeSummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
           "SNVisitPrint",
           "SNVisit/View/{episodeId}/{patientId}/{eventId}",
            new { controller = this.Name, action = "SNVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
        }
    }
}
