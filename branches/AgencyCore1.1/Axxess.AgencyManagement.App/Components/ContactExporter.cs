﻿namespace Axxess.AgencyManagement.App.Components
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class ContactExporter : BaseExporter
    {
        private IList<AgencyContact> contacts;
        public ContactExporter(IList<AgencyContact> contacts)
            : base()
        {
            this.contacts = contacts;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Contacts";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("Contacts");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Row headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Last Name");
            headerRow.CreateCell(1).SetCellValue("First Name");
            headerRow.CreateCell(2).SetCellValue("Type");
            headerRow.CreateCell(3).SetCellValue("Company");
            headerRow.CreateCell(4).SetCellValue("Address 1");
            headerRow.CreateCell(5).SetCellValue("Address 2");
            headerRow.CreateCell(6).SetCellValue("City");
            headerRow.CreateCell(7).SetCellValue("State");
            headerRow.CreateCell(8).SetCellValue("Zip Code");
            headerRow.CreateCell(9).SetCellValue("Primary Phone");
            headerRow.CreateCell(10).SetCellValue("Alternate Phone");
            headerRow.CreateCell(11).SetCellValue("Email Address");
            headerRow.RowStyle = headerStyle;

            if (this.contacts.Count > 0)
            {
                int i = 1;
                this.contacts.ForEach(c =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(c.LastName);
                    dataRow.CreateCell(1).SetCellValue(c.FirstName);
                    dataRow.CreateCell(2).SetCellValue(c.ContactType);
                    dataRow.CreateCell(3).SetCellValue(c.CompanyName);
                    dataRow.CreateCell(4).SetCellValue(c.AddressLine1);
                    dataRow.CreateCell(5).SetCellValue(c.AddressLine2);
                    dataRow.CreateCell(6).SetCellValue(c.AddressCity);
                    dataRow.CreateCell(7).SetCellValue(c.AddressStateCode);
                    dataRow.CreateCell(8).SetCellValue(c.AddressZipCode);
                    dataRow.CreateCell(9).SetCellValue(c.PhonePrimaryFormatted);
                    dataRow.CreateCell(10).SetCellValue(c.PhoneAlternate.ToPhone());
                    dataRow.CreateCell(11).SetCellValue(c.EmailAddress);
                    i++;
                });
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
        }
    }
}
