﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public sealed class UserSession
    {
       public Guid AgencyId { get; set; }
       public string AgencyName { get; set; }

       public Guid LoginId { get; set; }
       public Guid UserId { get; set; }
       public string FullName { get; set; }
       public string SignatureHash { get; set; }
       public string SignatureSalt { get; set; }
       public string AgencyRoles { get; set; }
    }
}
