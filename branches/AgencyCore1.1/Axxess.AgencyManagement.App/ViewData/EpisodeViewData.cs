﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Domain;

    public class EpisodeViewData
    {
        public EpisodeViewData()
        {
            this.EpisodeDetail = new EpisodeDetail();
        }
        public Guid Id { get; set; }

        public Guid PatientId { get; set; }

        public bool IsNext { get; set; }

        public Guid NextId { get; set; }

        public bool IsPrevious { get; set; }

        public Guid PreviousId { get; set; }

        public DateTime Start { get; set; }

        public string StartDateFormatted { get { return this.Start.ToShortDateString(); } }
        
        public DateTime End { get; set; }

        public string Schedule { get; set; }

        public string EndDateFormatted { get { return this.End.ToShortDateString(); } }

        public string DisplayName { get; set; }

        public string StartOfCareDateFormated { get; set; }

        public EpisodeDetail EpisodeDetail { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string ScheduleEvent
        {
            get
            {
                if (this.Schedule == null)
                {
                    return string.Empty;
                }
                return JsonSerializer.Serialize(this.Schedule.ToString().ToObject<List<ScheduleEvent>>());
            }
        }
       
    }
}
