﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;

    using Axxess.AgencyManagement.Enums;

    public class EmployeeViewData
    {
        public Guid Id { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public AgencyRoles AgencyRole { get; set; }
        public string CustomAgencyEmployeeId { get; set; }
        public UserStatus EmploymentStatus { get; set; }
        public string Suffix { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public Guid AgencyLocationId { get; set; }
        public byte DepartmentId { get; set; }
        public byte JobTitleId { get; set; }
        public bool AllowWeekendAccess { get; set; }
        public string EarliestLoginTime { get; set; }
        public string AutomaticLogoutTime { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
