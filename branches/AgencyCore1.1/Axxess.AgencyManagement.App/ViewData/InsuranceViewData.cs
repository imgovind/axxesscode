﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.ViewData
{
   public class InsuranceViewData
    {
       public int Id { get; set; }
       public string Name { get; set; }
    }
}
