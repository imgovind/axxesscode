﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Enums;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;
    
    using Security;
    using Axxess.Membership.Enums;
    using System.Text;

    public class MessageService : IMessageService
    {
        private readonly IUserRepository userRepository;
        private readonly IMessageRepository messageRepository;

        public MessageService(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "dataProvider");

            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.messageRepository = agencyManagementDataProvider.MessageRepository;
        }

        public bool SendMessage(Message message)
        {
            var result = false;
            var names = new StringBuilder();

            message.FromId = Current.UserId;
            message.FromName = Current.UserFullName;

            message.Recipients.ForEach(u =>
            {
                var user = userRepository.Get(u, Current.AgencyId);
                if (user != null)
                {
                    names.AppendFormat("{0}; ", user.DisplayName);
                }
            });

            message.Recipients.ForEach(r =>
            {
                message.RecipientId = r;
                message.RecipientNames = names.ToString();
                result = messageRepository.Add(message);
            });
            return result;
        }

    }
}
