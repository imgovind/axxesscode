﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenForum.Core.DataAccess;
using SubSonic.SqlGeneration.Schema;

using Axxess.Core.Infrastructure;

namespace OpenForum.Core.Models
{
    [Serializable]
    public class Post
    {
        public Post()
        {
            DateTime now = DateTime.Now;
            CreatedDate = now;
            LastPostDate = now;
            this.Replies = new List<Reply>();
        }

        public Post(string title, string body)
            : this()
        {
            Title = title;
            Body = body;
        }

        public int Id { get; private set; }
        public string CreatedBy { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastPostBy { get; set; }
        public Guid LastPostById { get; set; }
        public DateTime LastPostDate { get; set; }
        public string Title { get; set; }
        private string _body;
        public string Body
        {
            get { return _body; }
            set { _body = HtmlHelper.FixUp(value); }
        }
        public int ViewCount { get; set; }

        [SubSonicIgnore]
        public List<Reply> Replies{ get; set; }

        [SubSonicIgnore]
        public int ReplyCount
        {
            get
            {
                return Container.Resolve<IPostRepository>().FindReplies(this.Id).Count;
            }
        }
        
        [SubSonicIgnore]
        public ForumUser User { get; set; }

        public void IncrementViewCount()
        {
            ViewCount++;
        }

        protected void OnValidate(ChangeAction action)
        {
            Validate();
        }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Title))
            {
                throw new OpenForumException("Please provide a title for your post.");
            }

            if (string.IsNullOrEmpty(Body))
            {
                throw new OpenForumException("Please provide text for your post.");
            }
            else
            {
                HtmlHelper.Validate(Body);
            }
        }
    }
}
