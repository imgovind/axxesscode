﻿namespace Axxess.Api.Services
{
    using System;
    using System.ServiceModel;

    using Axxess.Api.Contracts;

    public class BaseService : MarshalByRefObject, IService
    {
        #region Fault Helper

        public FaultException<T> GetFault<T>(string reason) where T : DefaultFault
        {
            T fault = Activator.CreateInstance<T>();
            fault.CorrelationId = Guid.Empty;
            fault.ErrorId = 0;
            fault.ErrorMessage = reason;
            return GetFault<T>(fault, reason);
        }

        public FaultException<T> GetFault<T>(T fault, string reason) where T : DefaultFault
        {
            FaultException<T> faultException = (FaultException<T>)Activator.CreateInstance(typeof(FaultException<T>), new object[] { fault, reason });
            return faultException;
        }

        #endregion

        #region IService Members

        bool IService.Ping()
        {
            return true;
        }

        #endregion
    }
}
