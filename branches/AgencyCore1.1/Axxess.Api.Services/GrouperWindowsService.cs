﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceProcess;

using Axxess.Api.Contracts;

namespace Axxess.Api.Services
{
    public partial class GrouperWindowsService : ServiceBase
    {
        public ServiceHost serviceHost = null;

        public GrouperWindowsService()
        {
            InitializeComponent();
            this.ServiceName = "GrouperService";
        }

        protected override void OnStart(string[] args)
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            serviceHost = new ServiceHost(typeof(GrouperService));
            serviceHost.Open();

            Windows.EventLog.WriteEntry("Grouper Service Started.", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }

            Windows.EventLog.WriteEntry("Grouper Service Stopped.", EventLogEntryType.Warning);
        }
    }
}
