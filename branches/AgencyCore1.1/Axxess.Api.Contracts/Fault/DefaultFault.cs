﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Fault/2010/11/", Name = "DefaultFaultContract")]
    public class DefaultFault
    {
        #region Constructors

        public DefaultFault()
            : this(-1, String.Empty, Guid.Empty)
        {
        }

        public DefaultFault(int errorId, string errorMessage, Guid correlationId)
        {
            this.ErrorId = errorId;
            this.ErrorMessage = errorMessage;
            this.CorrelationId = correlationId;
        }

        #endregion Constructors

        #region Properties

        [DataMember(IsRequired = true, Name = "ErrorId", Order = 1)]
        public int ErrorId { get; set; }

        [DataMember(IsRequired = true, Name = "ErrorMessage", Order = 2)]
        public string ErrorMessage { get; set; }

        [DataMember(IsRequired = true, Name = "CorrelationId", Order = 3)]
        public Guid CorrelationId { get; set; }

        #endregion Properties
    }
}
