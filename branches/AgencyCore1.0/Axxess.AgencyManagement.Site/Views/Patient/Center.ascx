﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientCenterViewData>" %>
<div class="temp-container">
    <div id='patient_Splitter1' class="xSplitter splitterContainer">
        <div class="xsp-pane sidepanel">
            <div id="PatientLeftSide" class="side">
                <div id="PatientFilterContainer" class="sideTop">
                    <div class="spacingDiv">
                    </div>
                    <div class="spacingDiv">
                    </div>
                    <div class="spacingDiv">
                    </div>
                    <div class="spacingDiv">
                    </div>
                    <div class="row">
                        <div class="divLabel">
                            <label>
                                View:</label></div>
                        <div class="inputs">
                            <span class="input_wrapper blank">
                                <select name="list" class="patientStatusDropDown sideDropDown">
                                    <option value="0">All Patients</option>
                                    <option value="1">Active Patients</option>
                                    <option value="2">Discharged Patients</option>
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="spacingDiv">
                    </div>
                    <div class="row">
                        <div class="divLabel">
                            <label>
                                Filter:</label></div>
                        <div class="inputs">
                            <span class="input_wrapper blank">
                                <select name="list" class="patientPaymentDropDown sideDropDown">
                                    <option value="0">All</option>
                                    <option value="1">Medicare (traditional)</option>
                                    <option value="2">Medicare (HMO/managed care)</option>
                                    <option value="3">Medicaid (traditional)</option>
                                    <option value="4">Medicaid (HMO/managed care) </option>
                                    <option value="5">Workers' compensation</option>
                                    <option value="6">Title programs </option>
                                    <option value="7">Other government</option>
                                    <option value="8">Private</option>
                                    <option value="9">Private HMO/managed care</option>
                                    <option value="10">Self Pay</option>
                                    <option value="11">Unknown</option>
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="spacingDiv">
                    </div>
                    <div class="row">
                        <div class="divLabel">
                            <label>
                                Find:</label></div>
                        <div class="inputs">
                            <span class="input_wrapper">
                                <input class="text" id="txtSearch_Patient_Selection" value="" type="text" size="18"
                                    style="width: 184px;" /></span>
                        </div>
                    </div>
                    <div class="spacingDiv">
                    </div>
                </div>
                <hr class="xsp-dragbar-hframe" style="width: 100%;" />
                <%Html.RenderPartial("~/Views/Patient/ListGrid.ascx", Model.Patients); %>
            </div>
        </div>
        <div class="abs window_main xsp-pane" style="overflow: hidden;">
            <div id="patient_Splitter12" class='abs' style="height: 100%;">
                <div class="mainTop" id="patientMaintopResult">
                    <%if (Model.Patients != null && Model.Patients.Count > 0)
                      {%>
                    <%Html.RenderPartial("~/Views/Patient/Info.ascx", Model.Patients.First()); %>
                    <%}
                      else
                      { %>
                    <span>There is no any patient in your system.</span>
                    <%} %>
                </div>
                <hr class="xsp-dragbar-hframe" style="width: 100%;" />
                <div id="patientBottomPanel" style="height: 100%;">
                    <%Html.RenderPartial("~/Views/Patient/Activity.ascx", Model.ScheduleEvents); %>
                </div>
            </div>
        </div>
        <div id="PatientLandingVerticalBar" class='abs xsp-dragbar xsp-dragbar-vframe'>
            &nbsp;</div>
    </div>
</div>
<% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/Patient.js").Add("/Models/Oasis.js").Add("/Models/PlanOfCare.js"))
       .OnDocumentReady(() =>
        {%>
Patient.Init(); Oasis.Init();
<%}); 
%>
