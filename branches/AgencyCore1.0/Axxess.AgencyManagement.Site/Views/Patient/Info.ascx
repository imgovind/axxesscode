﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="mainTopContainer">
<%
    if (Model.Id == Guid.Empty)
  {%>
There is no patient information.
<%}
  else
  { %>
    <div class="patientMenu">
        <ul>
            <li><a href="javascript:void(0);" onclick="Patient.loadNewOrder('<%=Model.Id %>');">
                New Orders</a> </li>
            <li>New Progress Notes</li>
            <li><a href="javascript:void(0);" onclick="Patient.loadNewCommunicationNote('<%=Model.Id %>');">
                New Communication Notes</a> </li>
            <li>New Discharge Summary</li>
            <li>New Task</li>
        </ul>
    </div>
    <div class="mainTopContainerLeft">
        <div style="padding: 5px;">
            <div>
                <label>
                    <b style="color: #53868B;">[&nbsp;<span id="patientLandingName" onclick="Patient.PatientInfo(this,'<%= Model.Id%>');"
                        class="input_wrapper blank"><%=Model.DisplayName%></span>&nbsp;]</b> &nbsp;&nbsp;[&nbsp;<a
                            href="javascript:void(0);" onclick="Patient.loadEditPatient('<%= Model.Id%>'); ">edit
                            patient</a>&nbsp;]</label>
            </div>
            <div class="underLine">
            </div>
        </div>
        <div class="mainTopContent">
        </div>
    </div>
    <div class="mainTopContainerRight">
        <div style="padding: 5px;">
            <b>Quick Reports for this patient</b>
            <div class="underLine">
            </div>
            <ul>
                <li><a href="javascript:void(0);">Patient Profile</a> </li>
                <li><a href="javascript:void(0);" onclick="JQD.open_window('#schedule_window');">Patient
                    schedule</a> </li>
                <li><a href="javascript:void(0);">Medication Profile</a> </li>
            </ul>
        </div>
    </div>
    <%} %>
</div>
