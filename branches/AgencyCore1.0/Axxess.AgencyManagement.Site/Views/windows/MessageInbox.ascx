﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="messageInbox" class="abs window">
    <div style="background-color: Transparent;" class="abs window_inner">
        <div class="window_top">
            <span class="float_left">Inbox</span><span class="float_right"><a class="window_min"
                href="javascript:void(0);"></a><a class="window_resize" href="javascript:void(0);">
                </a><a class="window_close" href="javascript:void(0);"></a></span>
        </div>
        <div id="messageInboxResult" style="display: block;" class="abs window_content inboxBody">
        </div>
        <div class="abs window_bottom">
            Message Center
        </div>
    </div>
</div>
<% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/Message.js"))
       .OnDocumentReady(() =>
        {%>
<%}); 
%>