﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Final>" %>
<% Html.Telerik().TabStrip()
       .HtmlAttributes(new { @style = "height:100%" })
            .Name("FinalTabStrip")
            .Items(parent =>
            {
                parent.Add()
                    .Text("Step 1 of 4 : Patient Demographics ")
                    .LoadContentFrom("Info", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId })
                    .Selected(true);

                parent.Add()
                    .Text("Step 2 of 4 : Verify Visit")
                    .LoadContentFrom("Visit", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId });

                parent.Add()
                    .Text("Step 3 of 4 : Verify Supply")
                    .LoadContentFrom("Supply", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId });
                parent.Add()
                  .Text("Step 4 of 4 :Summary")
                  .LoadContentFrom("Summary", "Billing", new { episodeId = Model.EpisodeId, patientId = Model.PatientId });

            })
            .Render();
%>

<script type="text/javascript">


    $('.t-content').css({ height: "auto" });

</script>

