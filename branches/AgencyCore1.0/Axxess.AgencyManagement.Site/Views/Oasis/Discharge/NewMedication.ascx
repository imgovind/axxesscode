﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyMedicationForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="margin">
                (M2004) Medication Intervention: If there were any clinically significant medication
                issues since the previous OASIS assessment, was a physician or the physician-designee
                contacted within one calendar day of the assessment to resolve clinically significant
                medication issues, including reconciliation?
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("DischargeFromAgency_M2004MedicationIntervention", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M2004MedicationIntervention", "00", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - No<br />
            <%=Html.RadioButton("DischargeFromAgency_M2004MedicationIntervention", "01", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes<br />
            <%=Html.RadioButton("DischargeFromAgency_M2004MedicationIntervention", "NA", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No clinically significant medication issues identified since the previous OASIS
            assessment
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="margin">
                (M2015) Patient/Caregiver Drug Education Intervention: Since the previous OASIS
                assessment, was the patient/caregiver instructed by agency staff or other health
                care provider to monitor the effectiveness of drug therapy, drug reactions, and
                side effects, and how and when to report problems that may occur?
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention", "00", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - No<br />
            <%=Html.RadioButton("DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention", "01", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes<br />
            <%=Html.RadioButton("DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention", "NA", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - Patient not taking any drugs
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M2020) Management of Oral Medications: Patient's current ability to prepare and
                take all oral medications reliably and safely, including administration of the correct
                dosage at the appropriate times/intervals. Excludes injectable and IV medications.
                (NOTE: This refers to ability, not compliance or willingness.)
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("DischargeFromAgency_M2020ManagementOfOralMedications", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M2020ManagementOfOralMedications", "00", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - Able to independently take the correct oral medication(s) and proper dosage(s)
            at the correct times.<br />
            <%=Html.RadioButton("DischargeFromAgency_M2020ManagementOfOralMedications", "01", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Able to take medication(s) at the correct times if:<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) individual dosages are prepared in
            advance by another person; OR<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) another person develops a drug diary
            or chart.<br />
            <%=Html.RadioButton("DischargeFromAgency_M2020ManagementOfOralMedications", "02", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Able to take medication(s) at the correct times if given reminders by another
            person at the appropriate times<br />
            <%=Html.RadioButton("DischargeFromAgency_M2020ManagementOfOralMedications", "03", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Unable to take medication unless administered by another person.<br />
            <%=Html.RadioButton("DischargeFromAgency_M2020ManagementOfOralMedications", "NA", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No oral medications prescribed.
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M2030) Management of Injectable Medications: Patient's current ability to prepare
                and take all prescribed injectable medications reliably and safely, including administration
                of correct dosage at the appropriate times/intervals. Excludes IV medications.
            </div>
        </div>
        <div class="padding">
            <%=Html.Hidden("DischargeFromAgency_M2030ManagementOfInjectableMedications", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M2030ManagementOfInjectableMedications", "00", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
            - Able to independently take the correct medication(s) and proper dosage(s) at the
            correct times.<br />
            <%=Html.RadioButton("DischargeFromAgency_M2030ManagementOfInjectableMedications", "01", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Able to take injectable medication(s) at the correct times if:<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) individual syringes are prepared in
            advance by another person; OR<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) another person develops a drug diary
            or chart.<br />
            <%=Html.RadioButton("DischargeFromAgency_M2030ManagementOfInjectableMedications", "02", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Able to take medication(s) at the correct times if given reminders by another
            person based on the frequency of the injection<br />
            <%=Html.RadioButton("DischargeFromAgency_M2030ManagementOfInjectableMedications", "03", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Unable to take injectable medication unless administered by another person.<br />
            <%=Html.RadioButton("DischargeFromAgency_M2030ManagementOfInjectableMedications", "NA", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No injectable medications prescribed.
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Discharge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
