﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyOrdersForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<div class="insideColFull">
    <div class="insiderow title">
        <div class="margin">
            (M2400) Intervention Synopsis: (Check only one box in each row.) Since the previous
            OASIS assessment, were the following interventions BOTH included in the physician-ordered
            plan of care AND implemented?
        </div>
    </div>
    <div class="margin">
        <table class="agency-data-table" id="Table7">
            <thead>
                <tr>
                    <th>
                        Plan / Intervention
                    </th>
                    <th>
                        No
                    </th>
                    <th>
                        Yes
                    </th>
                    <th colspan="2">
                        Not Applicable
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        a. Diabetic foot care including monitoring for the presence of skin lesions on the
                        lower extremities and patient/caregiver education on proper foot care
                    </td>
                    <td>
                        <%=Html.Hidden("DischargeFromAgency_M2400DiabeticFootCare", " ", new { @id = "" })%>
                        <%=Html.RadioButton("DischargeFromAgency_M2400DiabeticFootCare", "00", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400DiabeticFootCare", "01", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400DiabeticFootCare", "NA", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Patient is not diabetic or is bilateral amputee
                    </td>
                </tr>
                <tr>
                    <td>
                        b. Falls prevention interventions
                    </td>
                    <td>
                        <%=Html.Hidden("DischargeFromAgency_M2400FallsPreventionInterventions", " ", new { @id = "" })%>
                        <%=Html.RadioButton("DischargeFromAgency_M2400FallsPreventionInterventions", "00", data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400FallsPreventionInterventions", "01", data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400FallsPreventionInterventions", "NA", data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for
                        falls since the last OASIS assessment
                    </td>
                </tr>
                <tr>
                    <td>
                        c. Depression intervention(s) such as medication, referral for other treatment,
                        or a monitoring plan for current treatment
                    </td>
                    <td>
                        <%=Html.Hidden("DischargeFromAgency_M2400DepressionIntervention", " ", new { @id = "" })%>
                        <%=Html.RadioButton("DischargeFromAgency_M2400DepressionIntervention", "00", data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400DepressionIntervention", "01", data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400DepressionIntervention", "NA", data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Formal assessment indicates patient did not meet criteria for depression AND patient
                        did not have diagnosis of depression since the last OASIS assessment
                    </td>
                </tr>
                <tr>
                    <td>
                        d. Intervention(s) to monitor and mitigate pain
                    </td>
                    <td>
                        <%=Html.Hidden("DischargeFromAgency_M2400PainIntervention", " ", new { @id = "" })%>
                        <%=Html.RadioButton("DischargeFromAgency_M2400PainIntervention", "00", data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400PainIntervention", "01", data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400PainIntervention", "NA", data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Formal assessment did not indicate pain since the last OASIS assessment
                    </td>
                </tr>
                <tr>
                    <td>
                        e. Intervention(s) to prevent pressure ulcers
                    </td>
                    <td>
                        <%=Html.Hidden("DischargeFromAgency_M2400PressureUlcerIntervention", " ", new { @id = "" })%>
                        <%=Html.RadioButton("DischargeFromAgency_M2400PressureUlcerIntervention", "00", data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400PressureUlcerIntervention", "01", data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400PressureUlcerIntervention", "NA", data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Formal assessment indicates the patient was not at risk of pressure ulcers since
                        the last OASIS assessment
                    </td>
                </tr>
                <tr>
                    <td>
                        f. Pressure ulcer treatment based on principles of moist wound healing
                    </td>
                    <td>
                        <%=Html.Hidden("DischargeFromAgency_M2400PressureUlcerTreatment", " ", new { @id = "" })%>
                        <%=Html.RadioButton("DischargeFromAgency_M2400PressureUlcerTreatment", "00", data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400PressureUlcerTreatment", "01", data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("DischargeFromAgency_M2400PressureUlcerTreatment", "NA", data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Dressings that support the principles of moist wound healing not indicated for this
                        patient’s pressure ulcers OR patient has no pressure ulcers with need for moist
                        wound healing
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="margin">
                (M2410) To which Inpatient Facility has the patient been admitted?
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("DischargeFromAgency_M2410TypeOfInpatientFacility", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M2410TypeOfInpatientFacility", "01", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Hospital [ Go to M2430 ]<br />
            <%=Html.RadioButton("DischargeFromAgency_M2410TypeOfInpatientFacility", "02", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Rehabilitation facility [ Go to M0903 ]<br />
            <%=Html.RadioButton("DischargeFromAgency_M2410TypeOfInpatientFacility", "03", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Nursing home [ Go to M2440 ]<br />
            <%=Html.RadioButton("DischargeFromAgency_M2410TypeOfInpatientFacility", "04", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Hospice [ Go to M0903 ]<br />
            <%=Html.RadioButton("DischargeFromAgency_M2410TypeOfInpatientFacility", "NA", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No inpatient facility admission [Omit "NA" option on TRN]<br />
        </div>
    </div>
    <div class="insideCol" id="discharge_M2420">
        <div class="insiderow title">
            <div class="margin">
                (M2420) Discharge Disposition: Where is the patient after discharge from your agency?
                (Choose only one answer.)
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("DischargeFromAgency_M2420DischargeDisposition", " ", new { @id = "" })%>
            <%=Html.RadioButton("DischargeFromAgency_M2420DischargeDisposition", "01", data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Patient remained in the community (without formal assistive services)<br />
            <%=Html.RadioButton("DischargeFromAgency_M2420DischargeDisposition", "02", data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Patient remained in the community (with formal assistive services)<br />
            <%=Html.RadioButton("DischargeFromAgency_M2420DischargeDisposition", "03", data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Patient transferred to a non-institutional hospice<br />
            <%=Html.RadioButton("DischargeFromAgency_M2420DischargeDisposition", "04", data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Unknown because patient moved to a geographic location not served by this agency<br />
            <%=Html.RadioButton("DischargeFromAgency_M2420DischargeDisposition", "UK", data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "UK" ? true : false, new { @id = "" })%>&nbsp;UK
            - Other unknown<br />
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="margin">
                (M0903) Date of Last (Most Recent) Home Visit:
            </div>
        </div>
        <div class="margin">
            <%=Html.TextBox("DischargeFromAgency_M0903LastHomeVisitDate", data.ContainsKey("M0903LastHomeVisitDate") ? data["M0903LastHomeVisitDate"].Answer : "", new { @id = "DischargeFromAgency_M0903LastHomeVisitDate" })%>
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="margin">
                (M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer,
                or death (at home) of the patient.
            </div>
        </div>
        <div class="margin">
            <%=Html.TextBox("DischargeFromAgency_M0906DischargeDate", data.ContainsKey("M0906DischargeDate") ? data["M0906DischargeDate"].Answer : "", new { @id = "DischargeFromAgency_M0906DischargeDate" })%>
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
    </ul>
</div>
<%  } %>
