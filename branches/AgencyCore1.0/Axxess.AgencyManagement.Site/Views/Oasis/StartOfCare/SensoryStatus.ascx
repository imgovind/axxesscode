﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareSensoryStatusForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id)%>
<%= Html.Hidden("StartOfCare_Action", "Edit")%>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "StartOfCare")%>
<div class="row485">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th colspan="2">
                Sensory Status
            </th>
        </tr>
        <tr>
            <td rowspan="2" valign="top">
                <%string[] eyes = data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer != "" ? data["GenericEyes"].Answer.Split(',') : null; %>
                <strong>Eyes:</strong>
                <ul>
                    <li>
                        <input type="hidden" name="StartOfCare_GenericEyes" value="" />&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="1" '<% if(  eyes!=null && eyes.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        WNL (Within Normal Limits) </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="2" '<% if(  eyes!=null && eyes.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Glasses </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="3" '<% if(  eyes!=null && eyes.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Contacts Left </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="4" '<% if(  eyes!=null && eyes.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Contacts Right </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="5" '<% if(  eyes!=null && eyes.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Blurred Vision </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="6" '<% if(  eyes!=null && eyes.Contains("6")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Glaucoma </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="7" '<% if(  eyes!=null && eyes.Contains("7")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Cataracts </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="8" '<% if(  eyes!=null && eyes.Contains("8")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Macular Degeneration </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="9" '<% if(  eyes!=null && eyes.Contains("9")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Redness </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="10" '<% if(  eyes!=null && eyes.Contains("10")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Drainage </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="11" '<% if(  eyes!=null && eyes.Contains("11")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Itching </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="12" '<% if(  eyes!=null && eyes.Contains("12")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Watering </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEyes" value="13" '<% if(  eyes!=null && eyes.Contains("13")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Other&nbsp;
                        <%=Html.TextBox("StartOfCare_GenericEyesOtherDetails", data.ContainsKey("GenericEyesOtherDetails") ? data["GenericEyesOtherDetails"].Answer : "", new { @id = "StartOfCare_GenericEyesOtherDetails", @size = "10", @maxlength = "10" })%>
                    </li>
                    <li>&nbsp;</li>
                </ul>
                <ul>
                    <li>Date of Last Eye Exam:
                        <%=Html.TextBox("StartOfCare_GenericEyesLastEyeExamDate", data.ContainsKey("GenericEyesLastEyeExamDate") ? data["GenericEyesLastEyeExamDate"].Answer : "", new { @id = "StartOfCare_GenericEyesLastEyeExamDate", @size = "10", @maxlength = "10" })%>
                    </li>
                </ul>
            </td>
            <td>
                <strong>Ears:</strong>
                <%string[] ears = data.ContainsKey("GenericEars") && data["GenericEars"].Answer != "" ? data["GenericEars"].Answer.Split(',') : null; %>
                <ul>
                    <li>
                        <input type="hidden" name="StartOfCare_GenericEars" value="" />&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericEars" value="1" '<% if(  ears!=null && ears.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        WNL (Within Normal Limits) </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEars" value="2" '<% if(  ears!=null && ears.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Hearing Impaired&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="hidden" name="StartOfCare_GenericEarsHearingImpairedPosition" value="" />&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericEarsHearingImpairedPosition" value="1" '<% if( data.ContainsKey("GenericEarsHearingImpairedPosition") && data["GenericEarsHearingImpairedPosition"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Left&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericEarsHearingImpairedPosition" value="2" '<% if( data.ContainsKey("GenericEarsHearingImpairedPosition") && data["GenericEarsHearingImpairedPosition"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Right </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEars" value="3" '<% if(  ears!=null && ears.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Deaf </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEars" value="4" '<% if(  ears!=null && ears.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Drainage </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEars" value="5" '<% if(  ears!=null && ears.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Pain </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericEars" value="6" '<% if(  ears!=null && ears.Contains("6")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Hearing Aids&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="hidden" name="StartOfCare_GenericEarsHearingAidsPosition" value="" />&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericEarsHearingAidsPosition" value="1" '<% if( data.ContainsKey("GenericEarsHearingAidsPosition") && data["GenericEarsHearingAidsPosition"].Answer == "1" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Left &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericEarsHearingAidsPosition" value="2" '<% if( data.ContainsKey("GenericEarsHearingAidsPosition") && data["GenericEarsHearingAidsPosition"].Answer == "2" ){ %>checked="checked"<% }%>'" />&nbsp;
                        Right </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Nose:</strong>
                <%string[] nose = data.ContainsKey("GenericNose") && data["GenericNose"].Answer != "" ? data["GenericNose"].Answer.Split(',') : null; %>
                <ul>
                    <li>
                        <input type="hidden" name="StartOfCare_GenericNose" value=" " />&nbsp;
                        <input type="checkbox" name="StartOfCare_GenericNose" value="1" '<% if(  nose!=null && nose.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        WNL (Within Normal Limits) </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericNose" value="2" '<% if(  nose!=null && nose.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Congestion </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericNose" value="3" '<% if(  nose!=null && nose.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Loss of Smell </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericNose" value="4" '<% if(  nose!=null && nose.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Nose Bleeds &nbsp;<i>How often?</i>
                        <%=Html.TextBox("StartOfCare_GenericNoseBleedsFrequency", data.ContainsKey("GenericNoseBleedsFrequency") ? data["GenericNoseBleedsFrequency"].Answer : "", new { @id = "StartOfCare_GenericNoseBleedsFrequency", @size = "20", @maxlength = "50" })%>
                    </li>
                </ul>
                <ul>
                    <li>
                        <input type="checkbox" name="StartOfCare_GenericNose" value="5" '<% if(  nose!=null && nose.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Other &nbsp;
                        <%=Html.TextBox("StartOfCare_GenericNoseOtherDetails", data.ContainsKey("GenericNoseOtherDetails") ? data["GenericNoseOtherDetails"].Answer : "", new { @id = "StartOfCare_GenericNoseOtherDetails", @size = "20", @maxlength = "50" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1200) Vision (with corrective lenses if the patient usually wears them):
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("StartOfCare_M1200Vision", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M1200Vision", "00", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Normal vision: sees adequately in most situations; can see medication
                labels, newsprint.<br />
                <%=Html.RadioButton("StartOfCare_M1200Vision", "01", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "01" ? true : false, new { @id = "" })%>
                &nbsp;1 - Partially impaired: cannot see medication labels or newsprint, but can
                see obstacles in path, and the surrounding layout; can count fingers at arm's length.<br />
                <%=Html.RadioButton("StartOfCare_M1200Vision", "02", data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Severely impaired: cannot locate objects without hearing or touching them or patient
                nonresponsive.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1210) Ability to hear (with hearing aid or hearing appliance if normally used):
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("StartOfCare_M1210Hearing", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M1210Hearing", "00", data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Adequate: hears normal conversation without difficulty.<br />
                <%=Html.RadioButton("StartOfCare_M1210Hearing", "01", data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Mildly to Moderately Impaired: difficulty hearing in some environments or speaker
                may need to increase volume or speak distinctly.<br />
                <%=Html.RadioButton("StartOfCare_M1210Hearing", "02", data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Severely Impaired: absence of useful hearing.<br />
                <%=Html.RadioButton("StartOfCare_M1210Hearing", "UK", data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "UK" ? true : false, new { @id = "" })%>&nbsp;UK
                - Unable to assess hearing.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1220) Understanding of Verbal Content in patient's own language (with hearing
                aid or device if used):
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("StartOfCare_M1220VerbalContent", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M1220VerbalContent", "00", data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Understands: clear comprehension without cues or repetitions.<br />
                <%=Html.RadioButton("StartOfCare_M1220VerbalContent", "01", data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Usually Understands: understands most conversations, but misses some part/intent
                of message. Requires cues at times to understand.<br />
                <%=Html.RadioButton("StartOfCare_M1220VerbalContent", "02", data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Sometimes Understands: understands only basic conversations or simple, direct
                phrases. Frequently requires cues to understand.<br />
                <%=Html.RadioButton("StartOfCare_M1220VerbalContent", "03", data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Rarely/Never Understands.<br />
                <%=Html.RadioButton("StartOfCare_M1220VerbalContent", "UK", data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "UK" ? true : false, new { @id = "" })%>&nbsp;UK
                - Unable to assess understanding.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="padding">
                (M1230) Speech and Oral (Verbal) Expression of Language (in patient's own language):
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("StartOfCare_M1230SpeechAndOral", " ", new { @id = "" })%>
                <%=Html.RadioButton("StartOfCare_M1230SpeechAndOral", "00", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "00" ? true : false, new { @id = "" })%>
                &nbsp;0 - Expresses complex ideas, feelings, and needs clearly, completely, and
                easily in all situations with no observable impairment.<br />
                <%=Html.RadioButton("StartOfCare_M1230SpeechAndOral", "01", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional
                errors in word choice, grammar or speech intelligibility; needs minimal prompting
                or assistance).<br />
                <%=Html.RadioButton("StartOfCare_M1230SpeechAndOral", "02", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Expresses simple ideas or needs with moderate difficulty (needs prompting or assistance,
                errors in word choice, organization or speech intelligibility). Speaks in phrases
                or short sentences.<br />
                <%=Html.RadioButton("StartOfCare_M1230SpeechAndOral", "03", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Has severe difficulty expressing basic ideas or needs and requires maximal assistance
                or guessing by listener. Speech limited to single words or short phrases.<br />
                <%=Html.RadioButton("StartOfCare_M1230SpeechAndOral", "04", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Unable to express basic needs even with maximal prompting or assistance but is
                not comatose or unresponsive (e.g., speech is nonsensical or unintelligible).<br />
                <%=Html.RadioButton("StartOfCare_M1230SpeechAndOral", "05", data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Patient nonresponsive or unable to speak.
            </div>
        </div>
    </div>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] sensoryStatusIntervention = data.ContainsKey("485SensoryStatusIntervention") && data["485SensoryStatusIntervention"].Answer != "" ? data["485SensoryStatusIntervention"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="StartOfCare_485SensoryStatusIntervention" value=" " />
                <input name="StartOfCare_485SensoryStatusIntervention" value="1" type="checkbox" '<% if(  sensoryStatusIntervention!=null && sensoryStatusIntervention.Contains("1")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to administer ear medication as follows:
                <br />
                <%=Html.TextBox("StartOfCare_485AdministerEarMedicationOrder", data.ContainsKey("485AdministerEarMedicationOrder") ? data["485AdministerEarMedicationOrder"].Answer : "", new { @id = "StartOfCare_485AdministerEarMedicationOrder", @size = "100", @maxlength = "100" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485SensoryStatusIntervention" value="2" type="checkbox" '<% if(  sensoryStatusIntervention!=null && sensoryStatusIntervention.Contains("2")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instill ophthalmic medication as follows:<br />
                <%=Html.TextBox("StartOfCare_485InstillOphathalmicMedicationOrders", data.ContainsKey("485InstillOphathalmicMedicationOrders") ? data["485InstillOphathalmicMedicationOrders"].Answer : "", new { @id = "StartOfCare_485InstillOphathalmicMedicationOrders", @size = "100", @maxlength = "100" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485SensoryStatusIntervention" value="3" type="checkbox" '<% if(  sensoryStatusIntervention!=null && sensoryStatusIntervention.Contains("3")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                ST
                <%=Html.TextBox("StartOfCare_485STEvaluateSensoryStatusFrequency", data.ContainsKey("485STEvaluateSensoryStatusFrequency") ? data["485STEvaluateSensoryStatusFrequency"].Answer : "", new { @id = "StartOfCare_485STEvaluateSensoryStatusFrequency", @size = "10", @maxlength = "10" })%>
                <input type="text" name="StartOfCare_485STEvaluateSensoryStatusFrequency" id="StartOfCare_485STEvaluateSensoryStatusFrequency"
                    size="10" maxlength="10" value="" />
                (freq) to evaluate week of:
                <%=Html.TextBox("StartOfCare_485STEvaluateSensoryStatusEvalDate", data.ContainsKey("485STEvaluateSensoryStatusEvalDate") ? data["485STEvaluateSensoryStatusEvalDate"].Answer : "", new { @id = "StartOfCare_485STEvaluateSensoryStatusEvalDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="StartOfCare_485SensoryStatusIntervention" value="4" type="checkbox" '<% if(  sensoryStatusIntervention!=null && sensoryStatusIntervention.Contains("4")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to provide patient with written instructions in large font
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Orders: &nbsp;
                <%var sensoryStatusOrderTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485SensoryStatusOrderTemplates") && data["485SensoryStatusOrderTemplates"].Answer != "" ? data["485SensoryStatusOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485SensoryStatusOrderTemplates", sensoryStatusOrderTemplates)%>
                <select id="StartOfCare_485SensoryStatusOrderTemplates" name="StartOfCare_485SensoryStatusOrderTemplates">
                    <option value="0">&nbsp;</option>
                    <option value="-2">-----------</option>
                    <option value="-1">Erase</option>
                </select>
                <br />
                <%=Html.TextArea("StartOfCare_485SensoryStatusInterventionComments", data.ContainsKey("485SensoryStatusInterventionComments") ? data["485SensoryStatusInterventionComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485SensoryStatusInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td>
                Additional Goals: &nbsp;
                <%var sensoryStatusGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485SensoryStatusGoalTemplates") && data["485SensoryStatusGoalTemplates"].Answer != "" ? data["485SensoryStatusGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485SensoryStatusGoalTemplates", sensoryStatusGoalTemplates)%>
                <br />
                <%=Html.TextArea("StartOfCare_485SensoryStatusGoalComments", data.ContainsKey("485SensoryStatusGoalComments") ? data["485SensoryStatusGoalComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485SensoryStatusGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="SOC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="SOC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
