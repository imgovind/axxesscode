﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationCardiacForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] genericCardioVascular = data.ContainsKey("GenericCardioVascular") && data["GenericCardioVascular"].Answer != "" ? data["GenericCardioVascular"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Cardiovascular
            </th>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="Recertification_GenericCardioVascular" value=" " />
                <input name="Recertification_GenericCardioVascular" value="1" type="checkbox" '<% if( genericCardioVascular!=null && genericCardioVascular.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                WNL (Within Normal Limits)
            </td>
            <td>
                <ul class="columns">
                    <li class="spacer">
                        <input name="Recertification_GenericCardioVascular" value="2" type="checkbox" '<% if( genericCardioVascular!=null && genericCardioVascular.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Dizziness: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericDizzinessDesc", data.ContainsKey("GenericDizzinessDesc") ? data["GenericDizzinessDesc"].Answer : "", new { @id = "Recertification_GenericDizzinessDesc", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">
                        <input name="Recertification_GenericCardioVascular" value="3" type="checkbox" '<% if( genericCardioVascular!=null && genericCardioVascular.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Chest Pain: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericChestPainDesc", data.ContainsKey("GenericChestPainDesc") ? data["GenericChestPainDesc"].Answer : "", new { @id = "Recertification_GenericChestPainDesc", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>
                        <input name="Recertification_GenericCardioVascular" value="4" type="checkbox" '<% if( genericCardioVascular!=null && genericCardioVascular.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Edema: </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericEdemaDesc1", data.ContainsKey("GenericEdemaDesc1") ? data["GenericEdemaDesc1"].Answer : "", new { @id = "Recertification_GenericEdemaDesc1", @size = "50", @maxlength = "50" })%>
                    </li>
                    <li>
                        <%var genericEdemaList1 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "1+", Value = "1+" },
                   new SelectListItem { Text = "2+", Value = "2+"} ,
                   new SelectListItem { Text = "3+", Value = "3+" },
                   new SelectListItem { Text = "4+", Value = "4+" } 
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericEdemaList1") && data["GenericEdemaList1"].Answer != "" ? data["GenericEdemaList1"].Answer : "0");%>
                        <%= Html.DropDownList("Recertification_GenericEdemaList1", genericEdemaList1)%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericEdemaDesc2", data.ContainsKey("GenericEdemaDesc2") ? data["GenericEdemaDesc2"].Answer : "", new { @id = "Recertification_GenericEdemaDesc2", @size = "50", @maxlength = "50" })%>
                    </li>
                    <li>
                        <%var genericEdemaList2 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "1+", Value = "1+" },
                   new SelectListItem { Text = "2+", Value = "2+"} ,
                   new SelectListItem { Text = "3+", Value = "3+" },
                   new SelectListItem { Text = "4+", Value = "4+" } 
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericEdemaList2") && data["GenericEdemaList2"].Answer != "" ? data["GenericEdemaList2"].Answer : "0");%>
                        <%= Html.DropDownList("Recertification_GenericEdemaList2", genericEdemaList2)%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericEdemaDesc3", data.ContainsKey("GenericEdemaDesc3") ? data["GenericEdemaDesc3"].Answer : "", new { @id = "Recertification_GenericEdemaDesc3", @size = "50", @maxlength = "50" })%>
                    </li>
                    <li>
                        <%var genericEdemaList3 = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "1+", Value = "1+" },
                   new SelectListItem { Text = "2+", Value = "2+"} ,
                   new SelectListItem { Text = "3+", Value = "3+" },
                   new SelectListItem { Text = "4+", Value = "4+" } 
                   
               }
                   , "Value", "Text", data.ContainsKey("GenericEdemaList3") && data["GenericEdemaList3"].Answer != "" ? data["GenericEdemaList3"].Answer : "0");%>
                        <%= Html.DropDownList("Recertification_GenericEdemaList3", genericEdemaList3)%>
                    </li>
                </ul>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericCardioVascular" value="5" type="checkbox" '<% if( genericCardioVascular!=null && genericCardioVascular.Contains("5")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Dependent Edema: </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <%string[] genericPittingEdemaType = data.ContainsKey("GenericPittingEdemaType") && data["GenericPittingEdemaType"].Answer != "" ? data["GenericPittingEdemaType"].Answer.Split(',') : null; %>
                        <input type="hidden" name="Recertification_GenericPittingEdemaType" value="" />
                        <input name="Recertification_GenericPittingEdemaType" value="1" type="checkbox"  '<% if( genericPittingEdemaType!=null && genericPittingEdemaType.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Pitting </li>
                    <li>
                        <input name="Recertification_GenericPittingEdemaType" value="2" type="checkbox" '<% if( genericPittingEdemaType!=null && genericPittingEdemaType.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Nonpitting </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericCardioVascular" value="6" type="checkbox" '<% if( genericCardioVascular!=null && genericCardioVascular.Contains("6")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Heart Sounds: </li>
                </ul>
                <ul class="columns">
                    <%string[] genericHeartSoundsType = data.ContainsKey("GenericHeartSoundsType") && data["GenericHeartSoundsType"].Answer != "" ? data["GenericHeartSoundsType"].Answer.Split(',') : null; %>
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <input type="hidden" name="Recertification_GenericHeartSoundsType" value="" />
                        <input name="Recertification_GenericHeartSoundsType" value="1" type="checkbox" '<% if( genericHeartSoundsType!=null && genericHeartSoundsType.Contains("1")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Murmur </li>
                    <li>
                        <input name="Recertification_GenericHeartSoundsType" value="2" type="checkbox" '<% if( genericHeartSoundsType!=null && genericHeartSoundsType.Contains("2")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Gallop </li>
                    <li>
                        <input name="Recertification_GenericHeartSoundsType" value="3" type="checkbox" '<% if( genericHeartSoundsType!=null && genericHeartSoundsType.Contains("3")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Click </li>
                    <li>
                        <input name="Recertification_GenericHeartSoundsType" value="4" type="checkbox" '<% if( genericHeartSoundsType!=null && genericHeartSoundsType.Contains("4")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Irregular </li>
                </ul>
            </td>
            <td>
                <ul class="columns">
                    <li class="spacer">
                        <input name="Recertification_GenericCardioVascular" value="7" type="checkbox" '<% if( genericCardioVascular!=null && genericCardioVascular.Contains("7")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Neck Vein Distention: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericNeckVeinDistentionDesc", data.ContainsKey("GenericNeckVeinDistentionDesc") ? data["GenericNeckVeinDistentionDesc"].Answer : "", new { @id = "Recertification_GenericNeckVeinDistentionDesc", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">
                        <input name="Recertification_GenericCardioVascular" value="8" type="checkbox" '<% if( genericCardioVascular!=null && genericCardioVascular.Contains("8")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Peripheral Pulses: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericPeripheralPulsesDesc", data.ContainsKey("GenericPeripheralPulsesDesc") ? data["GenericPeripheralPulsesDesc"].Answer : "", new { @id = "Recertification_GenericPeripheralPulsesDesc", @size = "20", @maxlength = "20" })%>
                    </li>
                </ul>
            </td>
            <td>
                <ul class="columns">
                    <li>
                        <input name="Recertification_GenericCardioVascular" value="9" type="checkbox" '<% if( genericCardioVascular!=null && genericCardioVascular.Contains("9")  ){ %>checked="checked"<% }%>'" />&nbsp;
                        Cap Refill: </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp; </li>
                    <li>
                        <%=Html.Hidden("Recertification_GenericCapRefillLessThan3", " ", new { @id = "" })%>
                        <%=Html.RadioButton("Recertification_GenericCapRefillLessThan3", "1", data.ContainsKey("GenericCapRefillLessThan3") && data["GenericCapRefillLessThan3"].Answer == "1" ? true : false, new { @id = "" })%>
                        &nbsp; <3 sec
                        <br />
                        <%=Html.RadioButton("Recertification_GenericCapRefillLessThan3", "0", data.ContainsKey("GenericCapRefillLessThan3") && data["GenericCapRefillLessThan3"].Answer == "0" ? true : false, new { @id = "" })%>&nbsp;
                        >3 sec </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Pacemaker:</li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericPacemakerDate", data.ContainsKey("GenericPacemakerDate") ? data["GenericPacemakerDate"].Answer : "", new { @id = "Recertification_GenericPacemakerDate", @size = "10", @maxlength = "10" })%>
                        (Insertion date) </li>
                </ul>
            </td>
            <td>
                <ul class="columns">
                    <li class="spacer">AICD: </li>
                    <li>
                        <%=Html.TextBox("Recertification_GenericAICDDate", data.ContainsKey("GenericAICDDate") ? data["GenericAICDDate"].Answer : "", new { @id = "Recertification_GenericAICDDate", @size = "10", @maxlength = "10" })%>
                        (Insertion date) </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ul>
                    <li>Comments:<br />
                        <%=Html.TextArea("Recertification_GenericCardiovascularComments", data.ContainsKey("GenericCardiovascularComments") ? data["GenericCardiovascularComments"].Answer : "", 5, 70, new { @id = "Recertification_GenericCardiovascularComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] cardiacStatusInterventions = data.ContainsKey("485CardiacStatusInterventions") && data["485CardiacStatusInterventions"].Answer != "" ? data["485CardiacStatusInterventions"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Interventions
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485CardiacStatusInterventions" value=" " />
                <input name="Recertification_485CardiacStatusInterventions" value="1" type="checkbox" '<% if( cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("1")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient on daily weight self-monitoring program where the patient
                utilizes the same scales on a hard, flat surface each morning prior to breakfast
                and after urination. Report to SN weight
                <%=Html.Hidden("Recertification_485WeightSelfMonitorGain", " ", new { @id = "" })%>
                <%=Html.RadioButton("Recertification_485WeightSelfMonitorGain", "1", data.ContainsKey("485WeightSelfMonitorGain") && data["485WeightSelfMonitorGain"].Answer == "1" ? true : false, new { @id = "" })%>
                gain
                <%=Html.RadioButton("Recertification_485WeightSelfMonitorGain", "0", data.ContainsKey("485WeightSelfMonitorGain") && data["485WeightSelfMonitorGain"].Answer == "0" ? true : false, new { @id = "" })%>
                loss of
                <%=Html.TextBox("Recertification_485WeightSelfMonitorGainDay", data.ContainsKey("485WeightSelfMonitorGainDay") ? data["485WeightSelfMonitorGainDay"].Answer : "", new { @id = "Recertification_485WeightSelfMonitorGainDay", @size = "3", @maxlength = "3" })%>
                lb/1 day,
                <%=Html.TextBox("Recertification_485WeightSelfMonitorGainWeek", data.ContainsKey("485WeightSelfMonitorGainWeek") ? data["485WeightSelfMonitorGainWeek"].Answer : "", new { @id = "Recertification_485WeightSelfMonitorGainWeek", @size = "3", @maxlength = "3" })%>
                lb/1 week
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485CardiacStatusInterventions" value="2" type="checkbox" '<% if( cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("2")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to assess patient's weight log every visit
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485CardiacStatusInterventions" value="3" type="checkbox" '<% if( cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("3")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the
                <%var instructRecognizeCardiacDysfunctionPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485InstructRecognizeCardiacDysfunctionPerson") && data["485InstructRecognizeCardiacDysfunctionPerson"].Answer != "" ? data["485InstructRecognizeCardiacDysfunctionPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485InstructRecognizeCardiacDysfunctionPerson", instructRecognizeCardiacDysfunctionPerson)%>
                on measures to recognize cardiac dysfunction and relieve complications
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485CardiacStatusInterventions" value="4" type="checkbox" '<% if( cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("4")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient on measures to detect and alleviate edema
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485CardiacStatusInterventions" value="5" type="checkbox" '<% if( cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("5")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct patient when (s)he starts feeling chest pain, tightness, or squeezing
                in the chest to take nitroglycerin. Patient may take nitroglycerin one time every
                5 minutes. If no relief after 3 doses, call 911
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485CardiacStatusInterventions" value="6" type="checkbox" '<% if( cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("6")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                SN to instruct the patient the following symptoms could be signs of a heart attack:
                chest discomfort, discomfort in one or both arms, back, neck, jaw, stomach, shortness
                of breath, cold sweat, nausea, or dizziness. Instruct patient on signs and symptoms
                that necessitate calling 911
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485CardiacStatusInterventions" value="7" type="checkbox" '<% if( cardiacStatusInterventions!=null && cardiacStatusInterventions.Contains("7")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                No blood pressure or venipuncture in
                <%=Html.TextBox("Recertification_485NoBloodPressureArm", data.ContainsKey("485NoBloodPressureArm") ? data["485NoBloodPressureArm"].Answer : "", new { @id = "Recertification_485NoBloodPressureArm", @size = "10", @maxlength = "10" })%>
                arm
            </td>
        </tr>
        <tr>
            <td colspan="100%">
                Additional Orders: &nbsp;
                <%var cardiacOrderTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485CardiacOrderTemplates") && data["485CardiacOrderTemplates"].Answer != "" ? data["485CardiacOrderTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485CardiacOrderTemplates", cardiacOrderTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485CardiacInterventionComments", data.ContainsKey("485CardiacInterventionComments") ? data["485CardiacInterventionComments"].Answer : "", 5, 70, new { @id = "Recertification_485CardiacInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] cardiacStatusGoals = data.ContainsKey("485CardiacStatusGoals") && data["485CardiacStatusGoals"].Answer != "" ? data["485CardiacStatusGoals"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="2">
                Goals
            </th>
        </tr>
        <tr>
            <td width="15px">
                <input type="hidden" name="Recertification_485CardiacStatusGoals" value=" " />
                <input name="Recertification_485CardiacStatusGoals" value="1" type="checkbox" '<% if( cardiacStatusGoals!=null && cardiacStatusGoals.Contains("1")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient weight will be maintained between
                <%=Html.TextBox("Recertification_485WeightMaintainedMin", data.ContainsKey("485WeightMaintainedMin") ? data["485WeightMaintainedMin"].Answer : "", new { @id = "Recertification_485WeightMaintainedMin", @size = "10", @maxlength = "10" })%>
                lbs and
                <%=Html.TextBox("Recertification_485WeightMaintainedMax", data.ContainsKey("485WeightMaintainedMax") ? data["485WeightMaintainedMax"].Answer : "", new { @id = "Recertification_485WeightMaintainedMax", @size = "10", @maxlength = "10" })%>
                lbs during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485CardiacStatusGoals" value="2" type="checkbox" '<% if( cardiacStatusGoals!=null && cardiacStatusGoals.Contains("2")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient's blood pressure will remain within established parameters during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485CardiacStatusGoals" value="3" type="checkbox" '<% if( cardiacStatusGoals!=null && cardiacStatusGoals.Contains("3")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient's pulse will remain within established parameters during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485CardiacStatusGoals" value="4" type="checkbox" '<% if( cardiacStatusGoals!=null && cardiacStatusGoals.Contains("4")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                Patient will remain free from chest pain, or chest pain will be relieved with nitroglycerin,
                during the episode
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485CardiacStatusGoals" value="5" type="checkbox" '<% if( cardiacStatusGoals!=null && cardiacStatusGoals.Contains("5")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeCardiacSymptomsPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485VerbalizeCardiacSymptomsPerson") && data["485VerbalizeCardiacSymptomsPerson"].Answer != "" ? data["485VerbalizeCardiacSymptomsPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485VerbalizeCardiacSymptomsPerson", verbalizeCardiacSymptomsPerson)%>
                will verbalize understanding of symptoms of cardiac complications and when to call
                911 by:
                <%=Html.TextBox("Recertification_485VerbalizeCardiacSymptomsDate", data.ContainsKey("485VerbalizeCardiacSymptomsDate") ? data["485VerbalizeCardiacSymptomsDate"].Answer : "", new { @id = "Recertification_485VerbalizeCardiacSymptomsDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td width="15px">
                <input name="Recertification_485CardiacStatusGoals" value="6" type="checkbox" '<% if( cardiacStatusGoals!=null && cardiacStatusGoals.Contains("6")  ){ %>checked="checked"<% }%>'" />
            </td>
            <td>
                The
                <%var verbalizeEdemaRelieverPerson = new SelectList(new[] { new SelectListItem { Text = "Patient/Caregiver", Value = "Patient/Caregiver" }, new SelectListItem { Text = "Patient", Value = "Patient" }, new SelectListItem { Text = "Caregiver", Value = "Caregiver" } }, "Value", "Text", data.ContainsKey("485VerbalizeEdemaRelieverPerson") && data["485VerbalizeEdemaRelieverPerson"].Answer != "" ? data["485VerbalizeEdemaRelieverPerson"].Answer : "Patient/Caregiver");%>
                <%= Html.DropDownList("Recertification_485VerbalizeEdemaRelieverPerson", verbalizeEdemaRelieverPerson)%>
                will verbalize and demonstrate edema-relieving measures by:
                <%=Html.TextBox("Recertification_485VerbalizeEdemaRelieverDate", data.ContainsKey("485VerbalizeEdemaRelieverDate") ? data["485VerbalizeEdemaRelieverDate"].Answer : "", new { @id = "Recertification_485VerbalizeEdemaRelieverDate", @size = "10", @maxlength = "10" })%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Goals: &nbsp;
                <%var cardiacGoalTemplates = new SelectList(new[] { new SelectListItem { Text = "", Value = "0" }, new SelectListItem { Text = "-----------", Value = "-2" }, new SelectListItem { Text = "Erase", Value = "-1" } }, "Value", "Text", data.ContainsKey("485CardiacGoalTemplates") && data["485CardiacGoalTemplates"].Answer != "" ? data["485CardiacGoalTemplates"].Answer : "0");%>
                <%= Html.DropDownList("Recertification_485CardiacGoalTemplates", cardiacGoalTemplates)%>
                <br />
                <%=Html.TextArea("Recertification_485CardiacGoalComments", data.ContainsKey("485CardiacGoalComments") ? data["485CardiacGoalComments"].Answer : "", 5, 70, new { @id = "Recertification_485CardiacGoalComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="Recertification.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="Recertification.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
