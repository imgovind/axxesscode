﻿var Discharge = {
    _DischargeId: "",
    _patientId: "",
    _EpisodeId: "",
    GetId: function() {
        return Discharge._patientId;
    },
    SetId: function(patientId) {
        Discharge._patientId = patientId;
    },
    GetDischargeId: function() {
        return Discharge._DischargeId;
    },
    SetDischargeId: function(DischargeId) {
        Discharge._DischargeId = DischargeId;
    },
    GetSOCEpisodeId: function() {
        return Discharge._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        Discharge._EpisodeId = EpisodeId;
    },
    Init: function() {

        $("input[name=DischargeFromAgency_M1040InfluenzaVaccine]").click(function() {
            if ($(this).val() == "01" || $(this).val() == "NA") {

                $("#discharge_M1045").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1045InfluenzaVaccineNotReceivedReason ]").attr('checked', false);

            }
            else if ($(this).val() == "00") {
                $("#discharge_M1045").unblock();
            }
        });


        $("input[name=DischargeFromAgency_M1050PneumococcalVaccine]").click(function() {
            if ($(this).val() == 1) {

                $("#discharge_M1055").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1055PPVNotReceivedReason ]").attr('checked', false);

            }
            else if ($(this).val() == 0) {
                $("#discharge_M1055").unblock();
            }
        });
        $("input[name=DischargeFromAgency_M1306UnhealedPressureUlcers]").click(function() {
            if ($(this).val() == 0) {

                $("#discharge_M1307").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });

                $("#discharge_M1308").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#discharge_M13010_12_14").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#discharge_M1320").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1307NonEpithelializedStageTwoUlcer ]").attr('checked', false);
                $("#DischargeFromAgency_M1307NonEpithelializedStageTwoUlcerDate").val('');
                $("#discharge_M1308").find(':input').each(function() { $(this).val(''); });
                $("#discharge_M13010_12_14").find(':input').each(function() { $(this).val(''); });
                $("input[name=DischargeFromAgency_M1320MostProblematicPressureUlcerStatus ]").attr('checked', false);
            }
            else if ($(this).val() == 1) {
                $("#discharge_M1307").unblock();
                $("#discharge_M1308").unblock();
                $("#discharge_M13010_12_14").unblock();
                $("#discharge_M1320").unblock();
            }
        });

        $("input[name=DischargeFromAgency_M1330StasisUlcer]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "03") {
                $("#discharge_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1332CurrentNumberStasisUlcer ]").attr('checked', false);
                $("input[name=DischargeFromAgency_M1334StasisUlcerStatus ]").attr('checked', false);
            }
            else {
                $("#discharge_M1332AndM1334").unblock();
            }
        });
        $("input[name=DischargeFromAgency_M1340SurgicalWound]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02") {
                $("#discharge_M1342").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });

                $("input[name=DischargeFromAgency_M1342SurgicalWoundStatus]").attr('checked', false);

            }
            else {
                $("#discharge_M1342").unblock();
            }
        });
        $("input[name=DischargeFromAgency_M1500HeartFailureSymptons]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02" || $(this).val() == "NA") {
                $("#discharge_M1510").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1510HeartFailureFollowup ]").attr('checked', false);
            }
            else if ($(this).val() == "01") {
                $("#discharge_M1510").unblock();
            }
        });

        $("input[name=DischargeFromAgency_M1610UrinaryIncontinence]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02") {
                $("#discharge_M1615").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M1615UrinaryIncontinenceOccur]").attr('checked', false);
            }
            else {
                $("#discharge_M1615").unblock();
            }
        });

        $("input[name=DischargeFromAgency_M2300EmergentCare]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "UK") {
                $("#discharge_M2310").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M2310ReasonForEmergentCare ]").attr('checked', false);
            }
            else if ($(this).val() == "01" || $(this).val() == "02") {
                $("#discharge_M2310").unblock();
            }
        });

        $("input[name=DischargeFromAgency_M2410TypeOfInpatientFacility]").click(function() {
            if ($(this).val() == "02" || $(this).val() == "04") {
                $("#discharge_M2420").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=DischargeFromAgency_M2420DischargeDisposition]").attr('checked', false);
            }
            else {
                $("#discharge_M2420").unblock();
            }
        });
    }
    ,
    HandlerHelper: function(form, formType, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    var actionType = control.val();
                    if (actionType == "Save/Continue") {
                        $("input[name='DischargeFromAgency_Id']").val(resultObject.Assessment.Id);
                        $("input[name='DischargeFromAgency_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='DischargeFromAgency_Action']").val('Edit');
                        if (formType == "New")
                            Oasis.NextTab("#dischargeTabs.tabs");
                        else if (formType == "Edit")
                            Oasis.NextTab("#editDischargeTabs.tabs");

                    }
                    else if (actionType == "Save/Exit") {
                        Oasis.Close(control);
                        Oasis.RebindActivity();
                    }
                    else if (actionType == "Save") {
                        $("input[name='DischargeFromAgency_Id']").val(resultObject.Assessment.Id);
                        $("input[name='DischargeFromAgency_PatientGuid']").val(resultObject.Assessment.PatientId);
                        $("input[name='DischargeFromAgency_Action']").val('Edit');
                    }
                }
                else {
                    alert(resultObject.errorMessage);
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
    ,
    FormSubmit: function(control, formType) {
        var form = control.closest("form");
        form.validate();
        Discharge.HandlerHelper(form, formType, control);
    },
    Validate: function() {
        OasisValidation.Validate(Discharge._DischargeId, "DischargeFromAgency");
    }
    ,
    DischargeFromAgency: function() {
        var id = Oasis.GetId();
        var data = 'id=' + id;
        $("#editDischarge").clearForm();
        $("#editDischarge div").unblock();
        Oasis.BlockAssessmentType();
        $.ajax({
            url: '/Patient/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var patient = eval(result);
                var patientName = (patient.FirstName !== null ? patient.FirstName : "") + " " + (patient.LastName != null ? patient.LastName : "");
                $("#DischargeFromAgencyTitle").text("New Discharge From Agency - " + patientName);
                $("input[name='DischargeFromAgency_Id']").val("");
                $("input[name='DischargeFromAgency_PatientGuid']").val(id);
                $("input[name='DischargeFromAgency_Action']").val('New');
                $("#DischargeFromAgency_M0020PatientIdNumber").val(patient.PatientIdNumber);
                $("#DischargeFromAgency_M0030SocDate").val(patient.StartOfCareDateFormatted);
                $("#DischargeFromAgency_M0040FirstName").val(patient.FirstName);
                $("#DischargeFromAgency_M0040MI").val(patient.MiddleInitial);
                $("#DischargeFromAgency_M0040LastName").val(patient.LastName);
                $("#DischargeFromAgency_M0050PatientState").val(patient.AddressStateCode);
                $("#DischargeFromAgency_M0060PatientZipCode").val(patient.AddressZipCode);
                $("#DischargeFromAgency_M0063PatientMedicareNumber").val(patient.MedicareNumber);
                $("#DischargeFromAgency_M0064PatientSSN").val(patient.SSN);
                $("#DischargeFromAgency_M0066PatientDoB").val(patient.DOBFormatted);
                $('input[name=DischargeFromAgency_M0069Gender][value=' + patient.Gender.toString() + ']').attr('checked', true);
                $("input[name='DischargeFromAgency_M0100AssessmentType'][value='09']").attr('checked', true);
                if (patient.EthnicRace !== null) {
                    var EthnicRaceArray = (patient.EthnicRace).split(';');
                    var i = 0;
                    for (i = 0; i < EthnicRaceArray.length; i++) {

                        if (EthnicRaceArray[i] == 1) {
                            $('input[name=DischargeFromAgency_M0140RaceAMorAN][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 2) {
                            $('input[name=DischargeFromAgency_M0140RaceAsia][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 3) {
                            $('input[name=DischargeFromAgency_M0140RaceBalck][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 4) {
                            $('input[name=DischargeFromAgency_M0140RaceHispanicOrLatino][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 5) {
                            $('input[name=DischargeFromAgency_M0140RaceNHOrPI][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 6) {
                            $('input[name=DischargeFromAgency_M0140RaceWhite][value=1]').attr('checked', true);
                        }
                    }
                }
                if (patient.PaymentSource !== null) {
                    var PaymentSourceArray = (patient.PaymentSource).split(';');
                    var i = 0;
                    for (i = 0; i < PaymentSourceArray.length; i++) {
                        if (PaymentSourceArray[i] == 0) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourceNone][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 1) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourceMCREFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 2) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourceMCREHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 3) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourceMCAIDFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 4) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourceMACIDHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 5) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourceWRKCOMP][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 6) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourceTITLPRO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 7) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourceOTHGOVT][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 8) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourcePRVINS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 9) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourcePRVHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 10) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourceSelfPay][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 12) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                            $("#DischargeFromAgency_M0150PaymentSourceOther").val(patient.OtherPaymentSource);
                        }
                        if (PaymentSourceArray[i] == 11) {
                            $('input[name=DischargeFromAgency_M0150PaymentSourceUnknown][value=1]').attr('checked', true);
                        }
                    }
                }

            }
        });
    }
    ,
    EditDischarge: function(id, patientId, assessmentType) {
        //var patientId = Oasis.GetId();
        $("#editDischarge").clearForm();
        Oasis.BlockAssessmentType();
        var data = 'Id=' + id + "&assessmentType=" + assessmentType;
        $.ajax({
            url: '/Oasis/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                getNewRows(result);
            }
        });
        getNewRows = function(result) {
            var patient = eval(result);


            var firstName = result["M0040FirstName"] != null && result["M0040FirstName"] != undefined ? result["M0040FirstName"].Answer : "";
            var lastName = result["M0040LastName"] != null && result["M0040LastName"] != undefined ? result["M0040LastName"].Answer : "";
            $("#DischargeFromAgencyTitle").text("Edit Discharge from Agency - " + firstName + " " + lastName);
            Discharge.SetDischargeId(id);
            $("input[name='DischargeFromAgency_Id']").val(id);
            $("input[name='DischargeFromAgency_Action']").val('Edit');
            $("input[name='DischargeFromAgency_PatientGuid']").val(patientId);
            $("#DischargeFromAgency_M0010CertificationNumber").val(result["M0010CertificationNumber"] != null && result["M0010CertificationNumber"] != undefined ? result["M0010CertificationNumber"].Answer : "");
            $("#DischargeFromAgency_M0014BranchState").val(result["M0014BranchState"] != null && result["M0014BranchState"] != undefined ? result["M0014BranchState"].Answer : "");
            $("#DischargeFromAgency_M0016BranchId").val(result["M0016BranchId"] != null && result["M0016BranchId"] != undefined ? result["M0016BranchId"].Answer : "");
            var nationalProviderIdUK = result["M0018NationalProviderIdUnknown"];
            if (nationalProviderIdUK != null && nationalProviderIdUK != undefined) {
                if (nationalProviderIdUK.Answer == 1) {

                    $('input[name=DischargeFromAgency_M0018NationalProviderIdUnknown][value=1]').attr('checked', true);
                    $("#DischargeFromAgency_M0018NationalProviderId").val(" ");
                }
                else {
                    $('input[name=DischargeFromAgency_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                    $("#DischargeFromAgency_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
                }
            }
            else {
                $("#DischargeFromAgency_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
            }
            $("#DischargeFromAgency_M0020PatientIdNumber").val(result["M0020PatientIdNumber"] != null && result["M0020PatientIdNumber"] != undefined ? result["M0020PatientIdNumber"].Answer : "");
            $("#DischargeFromAgency_M0030SocDate").val(result["M0030SocDate"] != null && result["M0030SocDate"] != undefined ? result["M0030SocDate"].Answer : "");
            $("#DischargeFromAgency_GenericEpisodeStartDate").val(result["GenericEpisodeStartDate"] != null && result["GenericEpisodeStartDate"] != undefined ? result["GenericEpisodeStartDate"].Answer : "");
            $("#DischargeFromAgency_M0040FirstName").val(firstName);
            $("#DischargeFromAgency_M0040MI").val(result["M0040MI"] != null && result["M0040MI"] != undefined ? result["M0040MI"].Answer : "");
            $("#DischargeFromAgency_M0040LastName").val(lastName);
            $("#DischargeFromAgency_M0050PatientState").val(result["M0050PatientState"] != null && result["M0050PatientState"] != undefined ? result["M0050PatientState"].Answer : "");
            $("#DischargeFromAgency_M0060PatientZipCode").val(result["M0060PatientZipCode"] != null && result["M0060PatientZipCode"] != undefined ? result["M0060PatientZipCode"].Answer : "");
            var patientMedicareNumberUK = result["M0063PatientMedicareNumberUK"];
            if (patientMedicareNumberUK != null && patientMedicareNumberUK != undefined) {
                if (patientMedicareNumberUK.Answer == 1) {

                    $('input[name=DischargeFromAgency_M0063PatientMedicareNumberUK][value=1]').attr('checked', true);
                    $("#DischargeFromAgency_M0063PatientMedicareNumber").val(" ");
                }
                else {
                    $('input[name=DischargeFromAgency_M0063PatientMedicareNumberUK][value=1]').attr('checked', false);
                    $("#DischargeFromAgency_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
                }
            }
            else {
                $('input[name=DischargeFromAgency_M0063PatientMedicareNumberUK][value=1]').attr('checked', false);
                $("#DischargeFromAgency_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
            }
            var patientSSNUK = result["M0064PatientSSNUK"];
            if (patientSSNUK != null && patientSSNUK != undefined) {
                if (patientSSNUK.Answer == 1) {

                    $('input[name=DischargeFromAgency_M0064PatientSSNUK][value=1]').attr('checked', true);
                    $("#DischargeFromAgency_M0064PatientSSN").val(" ");
                }
                else {
                    $('input[name=DischargeFromAgency_M0064PatientSSNUK][value=1]').attr('checked', false);
                    $("#DischargeFromAgency_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
                }
            }
            else {
                $("#DischargeFromAgency_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
            }
            var patientMedicaidNumberUK = result["M0065PatientMedicaidNumberUK"];
            if (patientMedicaidNumberUK != null && patientMedicaidNumberUK != undefined) {
                if (patientMedicaidNumberUK.Answer == 1) {

                    $('input[name=DischargeFromAgency_M0065PatientMedicaidNumberUK][value=1]').attr('checked', true);
                    $("#DischargeFromAgency_M0065PatientMedicaidNumber").val(" ");
                }
                else {
                    $('input[name=DischargeFromAgency_M0065PatientMedicaidNumberUK][value=1]').attr('checked', false);
                    $("#DischargeFromAgency_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
                }
            }
            else {
                $("#DischargeFromAgency_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
            }

            $("#DischargeFromAgency_M0066PatientDoB").val(result["M0066PatientDoB"] != null && result["M0066PatientDoB"] != undefined ? result["M0066PatientDoB"].Answer : "");
            $('input[name=DischargeFromAgency_M0069Gender][value=' + (result["M0069Gender"] != null && result["M0069Gender"] != undefined ? result["M0069Gender"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M0080DisciplinePerson][value=' + (result["M0080DisciplinePerson"] != null && result["M0080DisciplinePerson"] != undefined ? result["M0080DisciplinePerson"].Answer : "") + ']').attr('checked', true);
            $("#DischargeFromAgency_M0090AssessmentCompleted").val(result["M0090AssessmentCompleted"] != null && result["M0090AssessmentCompleted"] != undefined ? result["M0090AssessmentCompleted"].Answer : "");
            $("input[name='DischargeFromAgency_M0100AssessmentType'][value='09']").attr('checked', true);


            $('input[name=DischargeFromAgency_M0140RaceAMorAN][value=' + (result["M0140RaceAMorAN"] != null && result["M0140RaceAMorAN"] != undefined ? result["M0140RaceAMorAN"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0140RaceAsia][value=' + (result["M0140RaceAsia"] != null && result["M0140RaceAsia"] != undefined ? result["M0140RaceAsia"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0140RaceBalck][value=' + (result["M0140RaceBalck"] != null && result["M0140RaceBalck"] != undefined ? result["M0140RaceBalck"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0140RaceHispanicOrLatino][value=' + (result["M0140RaceHispanicOrLatino"] != null && result["M0140RaceHispanicOrLatino"] != undefined ? result["M0140RaceHispanicOrLatino"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0140RaceNHOrPI][value=' + (result["M0140RaceNHOrPI"] != null && result["M0140RaceNHOrPI"] != undefined ? result["M0140RaceNHOrPI"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0140RaceWhite][value=' + (result["M0140RaceWhite"] != null && result["M0140RaceWhite"] != undefined ? result["M0140RaceWhite"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0150PaymentSourceNone][value=' + (result["M0150PaymentSourceNone"] != null && result["M0150PaymentSourceNone"] != undefined ? result["M0150PaymentSourceNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0150PaymentSourceMCREFFS][value=' + (result["M0150PaymentSourceMCREFFS"] != null && result["M0150PaymentSourceMCREFFS"] != undefined ? result["M0150PaymentSourceMCREFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0150PaymentSourceMCREHMO][value=' + (result["M0150PaymentSourceMCREHMO"] != null && result["M0150PaymentSourceMCREHMO"] != undefined ? result["M0150PaymentSourceMCREHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0150PaymentSourceMCAIDFFS][value=' + (result["M0150PaymentSourceMCAIDFFS"] != null && result["M0150PaymentSourceMCAIDFFS"] != undefined ? result["M0150PaymentSourceMCAIDFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0150PaymentSourceMACIDHMO][value=' + (result["M0150PaymentSourceMACIDHMO"] != null && result["M0150PaymentSourceMACIDHMO"] != undefined ? result["M0150PaymentSourceMACIDHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0150PaymentSourceWRKCOMP][value=' + (result["M0150PaymentSourceWRKCOMP"] != null && result["M0150PaymentSourceWRKCOMP"] != undefined ? result["M0150PaymentSourceWRKCOMP"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0150PaymentSourceTITLPRO][value=' + (result["M0150PaymentSourceTITLPRO"] != null && result["M0150PaymentSourceTITLPRO"] != undefined ? result["M0150PaymentSourceTITLPRO"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0150PaymentSourceOTHGOVT][value=' + (result["M0150PaymentSourceOTHGOVT"] != null && result["M0150PaymentSourceOTHGOVT"] != undefined ? result["M0150PaymentSourceOTHGOVT"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0150PaymentSourcePRVINS][value=' + (result["M0150PaymentSourcePRVINS"] != null && result["M0150PaymentSourcePRVINS"] != undefined ? result["M0150PaymentSourcePRVINS"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0150PaymentSourcePRVHMO][value=' + (result["M0150PaymentSourcePRVHMO"] != null && result["M0150PaymentSourcePRVHMO"] != undefined ? result["M0150PaymentSourcePRVHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=DischargeFromAgency_M0150PaymentSourceSelfPay][value=' + (result["M0150PaymentSourceSelfPay"] != null && result["M0150PaymentSourceSelfPay"] != undefined ? result["M0150PaymentSourceSelfPay"].Answer : "") + ']').attr('checked', true);

            var paymentSourceOtherSRS = result["M0150PaymentSourceOtherSRS"];
            if (paymentSourceOtherSRS != null && paymentSourceOtherSRS != undefined) {
                if (paymentSourceOtherSRS.Answer == 1) {

                    $('input[name=DischargeFromAgency_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                    $("#DischargeFromAgency_M0150PaymentSourceOther").val(result["M0150PaymentSourceOther"] != null && result["M0150PaymentSourceOther"] != undefined ? result["M0150PaymentSourceOther"].Answer : "");

                }
                else {
                    $('input[name=DischargeFromAgency_M0150PaymentSourceOtherSRS][value=1]').attr('checked', false);

                }
            }

            $('input[name=DischargeFromAgency_M0150PaymentSourceUnknown][value=' + (result["M0150PaymentSourceUnknown"] != null && result["M0150PaymentSourceUnknown"] != undefined ? result["M0150PaymentSourceUnknown"].Answer : "") + ']').attr('checked', true);



            var influenzaVaccine = "";
            if (result["M1040InfluenzaVaccine"] != null && result["M1040InfluenzaVaccine"] != undefined) {
                influenzaVaccine = result["M1040InfluenzaVaccine"].Answer
            }
            $('input[name=DischargeFromAgency_M1040InfluenzaVaccine][value=' + (influenzaVaccine != "" ? influenzaVaccine : "") + ']').attr('checked', true);

            if (influenzaVaccine == "01" || influenzaVaccine == "NA") {
                $("#discharge_M1045").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else if (influenzaVaccine == "00") {
                $('input[name=DischargeFromAgency_M1045InfluenzaVaccineNotReceivedReason][value=' + (result["M1045InfluenzaVaccineNotReceivedReason"] != null && result["M1045InfluenzaVaccineNotReceivedReason"] != undefined ? result["M1045InfluenzaVaccineNotReceivedReason"].Answer : "") + ']').attr('checked', true);
                $("#discharge_M1045").unblock();
            }
            var pneumococcalVaccine = "";
            if (result["M1050PneumococcalVaccine"] != null && result["M1050PneumococcalVaccine"] != undefined) {
                pneumococcalVaccine = result["M1050PneumococcalVaccine"].Answer
            }
            $('input[name=DischargeFromAgency_M1050PneumococcalVaccine][value=' + (pneumococcalVaccine != "" ? pneumococcalVaccine : "") + ']').attr('checked', true);
            if (pneumococcalVaccine == "1") {

                $("#discharge_M1055").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else if (pneumococcalVaccine == "0") {
                $('input[name=DischargeFromAgency_M1055PPVNotReceivedReason][value=' + (result["M1055PPVNotReceivedReason"] != null && result["M1055PPVNotReceivedReason"] != undefined ? result["M1055PPVNotReceivedReason"].Answer : "") + ']').attr('checked', true);
                $("#discharge_M1055").unblock();
            }
            $('input[name=DischargeFromAgency_M1230SpeechAndOral][value=' + (result["M1230SpeechAndOral"] != null && result["M1230SpeechAndOral"] != undefined ? result["M1230SpeechAndOral"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1242PainInterferingFrequency][value=' + (result["M1242PainInterferingFrequency"] != null && result["M1242PainInterferingFrequency"] != undefined ? result["M1242PainInterferingFrequency"].Answer : "") + ']').attr('checked', true);
            var unhealedPressureUlcers = "";
            if (result["M1306UnhealedPressureUlcers"] != null && result["M1306UnhealedPressureUlcers"] != undefined) {
                unhealedPressureUlcers = result["M1306UnhealedPressureUlcers"].Answer
            }
            $('input[name=DischargeFromAgency_M1306UnhealedPressureUlcers][value=' + (unhealedPressureUlcers != "" ? unhealedPressureUlcers : "") + ']').attr('checked', true);
            if (unhealedPressureUlcers == "0") {

                $("#discharge_M1307").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });

                $("#discharge_M1308").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#discharge_M13010_12_14").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#discharge_M1320").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else if (unhealedPressureUlcers == "1") {
                $('input[name=DischargeFromAgency_M1307NonEpithelializedStageTwoUlcer][value=' + (result["M1307NonEpithelializedStageTwoUlcer"] != null && result["M1307NonEpithelializedStageTwoUlcer"] != undefined ? result["M1307NonEpithelializedStageTwoUlcer"].Answer : "") + ']').attr('checked', true);
                $("#DischargeFromAgency_M1307NonEpithelializedStageTwoUlcerDate").val(result["M1307NonEpithelializedStageTwoUlcerDate"] != null && result["M1307NonEpithelializedStageTwoUlcerDate"] != undefined ? result["M1307NonEpithelializedStageTwoUlcerDate"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedStageTwoUlcerCurrent").val(result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedStageTwoUlcerAdmission").val(result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedStageThreeUlcerCurrent").val(result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedStageThreeUlcerAdmission").val(result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedStageFourUlcerCurrent").val(result["M1308NumberNonEpithelializedStageFourUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageFourUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedStageIVUlcerAdmission").val(result["M1308NumberNonEpithelializedStageIVUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageIVUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer : "");
                $("#DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : "");
                $("#DischargeFromAgency_M1310PressureUlcerLength").val(result["M1310PressureUlcerLength"] != null && result["M1310PressureUlcerLength"] != undefined ? result["M1310PressureUlcerLength"].Answer : "");
                $("#DischargeFromAgency_M1312PressureUlcerWidth").val(result["M1312PressureUlcerWidth"] != null && result["M1312PressureUlcerWidth"] != undefined ? result["M1312PressureUlcerWidth"].Answer : "");
                $("#DischargeFromAgency_M1314PressureUlcerDepth").val(result["M1314PressureUlcerDepth"] != null && result["M1314PressureUlcerDepth"] != undefined ? result["M1314PressureUlcerDepth"].Answer : "");
                $('input[name=DischargeFromAgency_M1320MostProblematicPressureUlcerStatus][value=' + (result["M1320MostProblematicPressureUlcerStatus"] != null && result["M1320MostProblematicPressureUlcerStatus"] != undefined ? result["M1320MostProblematicPressureUlcerStatus"].Answer : "") + ']').attr('checked', true);
                $("#discharge_M1307").unblock();
                $("#discharge_M1308").unblock();
                $("#discharge_M13010_12_14").unblock();
                $("#discharge_M1320").unblock();
            }
            $('input[name=DischargeFromAgency_M1322CurrentNumberStageIUlcer][value=' + (result["M1322CurrentNumberStageIUlcer"] != null && result["M1322CurrentNumberStageIUlcer"] != undefined ? result["M1322CurrentNumberStageIUlcer"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1324MostProblematicUnhealedStage][value=' + (result["M1324MostProblematicUnhealedStage"] != null && result["M1324MostProblematicUnhealedStage"] != undefined ? result["M1324MostProblematicUnhealedStage"].Answer : "") + ']').attr('checked', true);
            var stasisUlcer = "";
            if (result["M1330StasisUlcer"] != null && result["M1330StasisUlcer"] != undefined) {
                stasisUlcer = result["M1330StasisUlcer"].Answer
            }
            $('input[name=DischargeFromAgency_M1330StasisUlcer][value=' + (stasisUlcer != "" ? stasisUlcer : "") + ']').attr('checked', true);
            if (stasisUlcer == "00" || stasisUlcer == "03") {
                $("#discharge_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=DischargeFromAgency_M1332CurrentNumberStasisUlcer][value=' + (result["M1332CurrentNumberStasisUlcer"] != null && result["M1332CurrentNumberStasisUlcer"] != undefined ? result["M1332CurrentNumberStasisUlcer"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M1334StasisUlcerStatus][value=' + (result["M1334StasisUlcerStatus"] != null && result["M1334StasisUlcerStatus"] != undefined ? result["M1334StasisUlcerStatus"].Answer : "") + ']').attr('checked', true);
                $("#discharge_M1332AndM1334").unblock();
            }
            var surgicalWound = "";
            if (result["M1340SurgicalWound"] != null && result["M1340SurgicalWound"] != undefined) {
                surgicalWound = result["M1340SurgicalWound"].Answer
            }
            $('input[name=DischargeFromAgency_M1340SurgicalWound][value=' + (surgicalWound != "" ? surgicalWound : "") + ']').attr('checked', true);
            if (surgicalWound == "00" || surgicalWound == "02") {
                $("#discharge_M1342").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=DischargeFromAgency_M1342SurgicalWoundStatus][value=' + (result["M1342SurgicalWoundStatus"] != null && result["M1342SurgicalWoundStatus"] != undefined ? result["M1342SurgicalWoundStatus"].Answer : "") + ']').attr('checked', true);
                $("#discharge_M1342").unblock();
            }
            $('input[name=DischargeFromAgency_M1350SkinLesionOpenWound][value=' + (result["M1350SkinLesionOpenWound"] != null && result["M1350SkinLesionOpenWound"] != undefined ? result["M1350SkinLesionOpenWound"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1400PatientDyspneic][value=' + (result["M1400PatientDyspneic"] != null && result["M1400PatientDyspneic"] != undefined ? result["M1400PatientDyspneic"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1410HomeRespiratoryTreatmentsOxygen][value=' + (result["M1410HomeRespiratoryTreatmentsOxygen"] != null && result["M1410HomeRespiratoryTreatmentsOxygen"] != undefined ? result["M1410HomeRespiratoryTreatmentsOxygen"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1410HomeRespiratoryTreatmentsVentilator][value=' + (result["M1410HomeRespiratoryTreatmentsVentilator"] != null && result["M1410HomeRespiratoryTreatmentsVentilator"] != undefined ? result["M1410HomeRespiratoryTreatmentsVentilator"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1410HomeRespiratoryTreatmentsContinuous][value=' + (result["M1410HomeRespiratoryTreatmentsContinuous"] != null && result["M1410HomeRespiratoryTreatmentsContinuous"] != undefined ? result["M1410HomeRespiratoryTreatmentsContinuous"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1410HomeRespiratoryTreatmentsNone][value=' + (result["M1410HomeRespiratoryTreatmentsNone"] != null && result["M1410HomeRespiratoryTreatmentsNone"] != undefined ? result["M1410HomeRespiratoryTreatmentsNone"].Answer : "") + ']').attr('checked', true);
            var heartFailureSymptons = "";
            if (result["M1500HeartFailureSymptons"] != null && result["M1500HeartFailureSymptons"] != undefined) {
                heartFailureSymptons = result["M1500HeartFailureSymptons"].Answer
            }

            $('input[name=DischargeFromAgency_M1500HeartFailureSymptons][value=' + (heartFailureSymptons != "" ? heartFailureSymptons : "") + ']').attr('checked', true);
            if (heartFailureSymptons == "00" || heartFailureSymptons == "02" || heartFailureSymptons == "NA") {
                $("#discharge_M1510").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else if (heartFailureSymptons == "01") {
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupNoAction][value=' + (result["M1510HeartFailureFollowupNoAction"] != null && result["M1510HeartFailureFollowupNoAction"] != undefined ? result["M1510HeartFailureFollowupNoAction"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupPhysicianCon][value=' + (result["M1510HeartFailureFollowupPhysicianCon"] != null && result["M1510HeartFailureFollowupPhysicianCon"] != undefined ? result["M1510HeartFailureFollowupPhysicianCon"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupAdvisedEmg][value=' + (result["M1510HeartFailureFollowupAdvisedEmg"] != null && result["M1510HeartFailureFollowupAdvisedEmg"] != undefined ? result["M1510HeartFailureFollowupAdvisedEmg"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupParameters][value=' + (result["M1510HeartFailureFollowupParameters"] != null && result["M1510HeartFailureFollowupParameters"] != undefined ? result["M1510HeartFailureFollowupParameters"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupInterventions][value=' + (result["M1510HeartFailureFollowupInterventions"] != null && result["M1510HeartFailureFollowupInterventions"] != undefined ? result["M1510HeartFailureFollowupInterventions"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M1510HeartFailureFollowupChange][value=' + (result["M1510HeartFailureFollowupChange"] != null && result["M1510HeartFailureFollowupChange"] != undefined ? result["M1510HeartFailureFollowupChange"].Answer : "") + ']').attr('checked', true);

                $("#discharge_M1510").unblock();
            }
            $('input[name=DischargeFromAgency_M1600UrinaryTractInfection][value=' + (result["M1600UrinaryTractInfection"] != null && result["M1600UrinaryTractInfection"] != undefined ? result["M1600UrinaryTractInfection"].Answer : "") + ']').attr('checked', true);
            var urinaryIncontinence = "";
            if (result["M1610UrinaryIncontinence"] != null && result["M1610UrinaryIncontinence"] != undefined) {
                urinaryIncontinence = result["M1610UrinaryIncontinence"].Answer
            }
            $('input[name=DischargeFromAgency_M1610UrinaryIncontinence][value=' + (urinaryIncontinence != "" ? urinaryIncontinence : "") + ']').attr('checked', true);
            if (urinaryIncontinence == "00" || urinaryIncontinence == "02") {
                $("#discharge_M1615").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=DischargeFromAgency_M1615UrinaryIncontinenceOccur][value=' + (result["M1615UrinaryIncontinenceOccur"] != null && result["M1615UrinaryIncontinenceOccur"] != undefined ? result["M1615UrinaryIncontinenceOccur"].Answer : "") + ']').attr('checked', true);
                $("#discharge_M1615").unblock();
            }
            $('input[name=DischargeFromAgency_M1620BowelIncontinenceFrequency][value=' + (result["M1620BowelIncontinenceFrequency"] != null && result["M1620BowelIncontinenceFrequency"] != undefined ? result["M1620BowelIncontinenceFrequency"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1700CognitiveFunctioning][value=' + (result["M1700CognitiveFunctioning"] != null && result["M1700CognitiveFunctioning"] != undefined ? result["M1700CognitiveFunctioning"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1710WhenConfused][value=' + (result["M1710WhenConfused"] != null && result["M1710WhenConfused"] != undefined ? result["M1710WhenConfused"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1720WhenAnxious][value=' + (result["M1720WhenAnxious"] != null && result["M1720WhenAnxious"] != undefined ? result["M1720WhenAnxious"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsImpDes][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsVerbal][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsPhysical][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsSIB][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsSIB"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsSIB"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsDelusional][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsNone][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsNone"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsNone"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency][value=' + (result["M1745DisruptiveBehaviorSymptomsFrequency"] != null && result["M1745DisruptiveBehaviorSymptomsFrequency"] != undefined ? result["M1745DisruptiveBehaviorSymptomsFrequency"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1800Grooming][value=' + (result["M1800Grooming"] != null && result["M1800Grooming"] != undefined ? result["M1800Grooming"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1810CurrentAbilityToDressUpper][value=' + (result["M1810CurrentAbilityToDressUpper"] != null && result["M1810CurrentAbilityToDressUpper"] != undefined ? result["M1810CurrentAbilityToDressUpper"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1820CurrentAbilityToDressLower][value=' + (result["M1820CurrentAbilityToDressLower"] != null && result["M1820CurrentAbilityToDressLower"] != undefined ? result["M1820CurrentAbilityToDressLower"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody][value=' + (result["M1830CurrentAbilityToBatheEntireBody"] != null && result["M1830CurrentAbilityToBatheEntireBody"] != undefined ? result["M1830CurrentAbilityToBatheEntireBody"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1840ToiletTransferring][value=' + (result["M1840ToiletTransferring"] != null && result["M1840ToiletTransferring"] != undefined ? result["M1840ToiletTransferring"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1845ToiletingHygiene][value=' + (result["M1845ToiletingHygiene"] != null && result["M1845ToiletingHygiene"] != undefined ? result["M1845ToiletingHygiene"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1850Transferring][value=' + (result["M1850Transferring"] != null && result["M1850Transferring"] != undefined ? result["M1850Transferring"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1860AmbulationLocomotion][value=' + (result["M1860AmbulationLocomotion"] != null && result["M1860AmbulationLocomotion"] != undefined ? result["M1860AmbulationLocomotion"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1870FeedingOrEating][value=' + (result["M1870FeedingOrEating"] != null && result["M1870FeedingOrEating"] != undefined ? result["M1870FeedingOrEating"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1880AbilityToPrepareLightMeal][value=' + (result["M1880AbilityToPrepareLightMeal"] != null && result["M1880AbilityToPrepareLightMeal"] != undefined ? result["M1880AbilityToPrepareLightMeal"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M1890AbilityToUseTelephone][value=' + (result["M1890AbilityToUseTelephone"] != null && result["M1890AbilityToUseTelephone"] != undefined ? result["M1890AbilityToUseTelephone"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2004MedicationIntervention][value=' + (result["M2004MedicationIntervention"] != null && result["M2004MedicationIntervention"] != undefined ? result["M2004MedicationIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention][value=' + (result["M2015PatientOrCaregiverDrugEducationIntervention"] != null && result["M2015PatientOrCaregiverDrugEducationIntervention"] != undefined ? result["M2015PatientOrCaregiverDrugEducationIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2020ManagementOfOralMedications][value=' + (result["M2020ManagementOfOralMedications"] != null && result["M2020ManagementOfOralMedications"] != undefined ? result["M2020ManagementOfOralMedications"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2030ManagementOfInjectableMedications][value=' + (result["M2030ManagementOfInjectableMedications"] != null && result["M2030ManagementOfInjectableMedications"] != undefined ? result["M2030ManagementOfInjectableMedications"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2100ADLAssistance][value=' + (result["M2100ADLAssistance"] != null && result["M2100ADLAssistance"] != undefined ? result["M2100ADLAssistance"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2100IADLAssistance][value=' + (result["M2100IADLAssistance"] != null && result["M2100IADLAssistance"] != undefined ? result["M2100IADLAssistance"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2100MedicationAdministration][value=' + (result["M2100MedicationAdministration"] != null && result["M2100MedicationAdministration"] != undefined ? result["M2100MedicationAdministration"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2100MedicalProcedures][value=' + (result["M2100MedicalProcedures"] != null && result["M2100MedicalProcedures"] != undefined ? result["M2100MedicalProcedures"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2100ManagementOfEquipment][value=' + (result["M2100ManagementOfEquipment"] != null && result["M2100ManagementOfEquipment"] != undefined ? result["M2100ManagementOfEquipment"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2100SupervisionAndSafety][value=' + (result["M2100SupervisionAndSafety"] != null && result["M2100SupervisionAndSafety"] != undefined ? result["M2100SupervisionAndSafety"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2100FacilitationPatientParticipation][value=' + (result["M2100FacilitationPatientParticipation"] != null && result["M2100FacilitationPatientParticipation"] != undefined ? result["M2100FacilitationPatientParticipation"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance][value=' + (result["M2110FrequencyOfADLOrIADLAssistance"] != null && result["M2110FrequencyOfADLOrIADLAssistance"] != undefined ? result["M2110FrequencyOfADLOrIADLAssistance"].Answer : "") + ']').attr('checked', true);
            var emergentCare = "";
            if (result["M2300EmergentCare"] != null && result["M2300EmergentCare"] != undefined) {
                emergentCare = result["M2300EmergentCare"].Answer
            }
            $('input[name=DischargeFromAgency_M2300EmergentCare][value=' + (emergentCare != "" ? emergentCare : "") + ']').attr('checked', true);
            if (emergentCare == "00" || emergentCare == "UK") {
                $("#discharge_M2310").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareMed][value=' + (result["M2310ReasonForEmergentCareMed"] != null && result["M2310ReasonForEmergentCareMed"] != undefined ? result["M2310ReasonForEmergentCareMed"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareFall][value=' + (result["M2310ReasonForEmergentCareFall"] != null && result["M2310ReasonForEmergentCareFall"] != undefined ? result["M2310ReasonForEmergentCareFall"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareResInf][value=' + (result["M2310ReasonForEmergentCareResInf"] != null && result["M2310ReasonForEmergentCareResInf"] != undefined ? result["M2310ReasonForEmergentCareResInf"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareOtherResInf][value=' + (result["M2310ReasonForEmergentCareOtherResInf"] != null && result["M2310ReasonForEmergentCareOtherResInf"] != undefined ? result["M2310ReasonForEmergentCareOtherResInf"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareHeartFail][value=' + (result["M2310ReasonForEmergentCareHeartFail"] != null && result["M2310ReasonForEmergentCareHeartFail"] != undefined ? result["M2310ReasonForEmergentCareHeartFail"].Answer : "") + ']').attr('checked', true);

                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareCardiac][value=' + (result["M2310ReasonForEmergentCareCardiac"] != null && result["M2310ReasonForEmergentCareCardiac"] != undefined ? result["M2310ReasonForEmergentCareCardiac"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareMyocardial][value=' + (result["M2310ReasonForEmergentCareMyocardial"] != null && result["M2310ReasonForEmergentCareMyocardial"] != undefined ? result["M2310ReasonForEmergentCareMyocardial"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareHeartDisease][value=' + (result["M2310ReasonForEmergentCareHeartDisease"] != null && result["M2310ReasonForEmergentCareHeartDisease"] != undefined ? result["M2310ReasonForEmergentCareHeartDisease"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareStroke][value=' + (result["M2310ReasonForEmergentCareStroke"] != null && result["M2310ReasonForEmergentCareStroke"] != undefined ? result["M2310ReasonForEmergentCareStroke"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareHypo][value=' + (result["M2310ReasonForEmergentCareHypo"] != null && result["M2310ReasonForEmergentCareHypo"] != undefined ? result["M2310ReasonForEmergentCareHypo"].Answer : "") + ']').attr('checked', true);

                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareGI][value=' + (result["M2310ReasonForEmergentCareGI"] != null && result["M2310ReasonForEmergentCareGI"] != undefined ? result["M2310ReasonForEmergentCareGI"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareDehMal][value=' + (result["M2310ReasonForEmergentCareDehMal"] != null && result["M2310ReasonForEmergentCareDehMal"] != undefined ? result["M2310ReasonForEmergentCareDehMal"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareUrinaryInf][value=' + (result["M2310ReasonForEmergentCareUrinaryInf"] != null && result["M2310ReasonForEmergentCareUrinaryInf"] != undefined ? result["M2310ReasonForEmergentCareUrinaryInf"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareIV][value=' + (result["M2310ReasonForEmergentCareIV"] != null && result["M2310ReasonForEmergentCareIV"] != undefined ? result["M2310ReasonForEmergentCareIV"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareWoundInf][value=' + (result["M2310ReasonForEmergentCareWoundInf"] != null && result["M2310ReasonForEmergentCareWoundInf"] != undefined ? result["M2310ReasonForEmergentCareWoundInf"].Answer : "") + ']').attr('checked', true);

                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareUncontrolledPain][value=' + (result["M2310ReasonForEmergentCareUncontrolledPain"] != null && result["M2310ReasonForEmergentCareUncontrolledPain"] != undefined ? result["M2310ReasonForEmergentCareUncontrolledPain"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareMental][value=' + (result["M2310ReasonForEmergentCareMental"] != null && result["M2310ReasonForEmergentCareMental"] != undefined ? result["M2310ReasonForEmergentCareMental"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareDVT][value=' + (result["M2310ReasonForEmergentCareDVT"] != null && result["M2310ReasonForEmergentCareDVT"] != undefined ? result["M2310ReasonForEmergentCareDVT"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareOther][value=' + (result["M2310ReasonForEmergentCareOther"] != null && result["M2310ReasonForEmergentCareOther"] != undefined ? result["M2310ReasonForEmergentCareOther"].Answer : "") + ']').attr('checked', true);
                $('input[name=DischargeFromAgency_M2310ReasonForEmergentCareUK][value=' + (result["M2310ReasonForEmergentCareUK"] != null && result["M2310ReasonForEmergentCareUK"] != undefined ? result["M2310ReasonForEmergentCareUK"].Answer : "") + ']').attr('checked', true);

                $("#discharge_M2310").unblock();
            }
            $('input[name=DischargeFromAgency_M2400DiabeticFootCare][value=' + (result["M2400DiabeticFootCare"] != null && result["M2400DiabeticFootCare"] != undefined ? result["M2400DiabeticFootCare"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2400FallsPreventionInterventions][value=' + (result["M2400FallsPreventionInterventions"] != null && result["M2400FallsPreventionInterventions"] != undefined ? result["M2400FallsPreventionInterventions"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2400DepressionIntervention][value=' + (result["M2400DepressionIntervention"] != null && result["M2400DepressionIntervention"] != undefined ? result["M2400DepressionIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2400PainIntervention][value=' + (result["M2400PainIntervention"] != null && result["M2400PainIntervention"] != undefined ? result["M2400PainIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2400PressureUlcerIntervention][value=' + (result["M2400PressureUlcerIntervention"] != null && result["M2400PressureUlcerIntervention"] != undefined ? result["M2400PressureUlcerIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=DischargeFromAgency_M2400PressureUlcerTreatment][value=' + (result["M2400PressureUlcerTreatment"] != null && result["M2400PressureUlcerTreatment"] != undefined ? result["M2400PressureUlcerTreatment"].Answer : "") + ']').attr('checked', true);
            var typeOfInpatientFacility = "";
            if (result["M2410TypeOfInpatientFacility"] != null && result["M2410TypeOfInpatientFacility"] != undefined) {
                typeOfInpatientFacility = result["M2410TypeOfInpatientFacility"].Answer
            }
            $('input[name=DischargeFromAgency_M2410TypeOfInpatientFacility][value=' + (typeOfInpatientFacility != "" ? typeOfInpatientFacility : "") + ']').attr('checked', true);
            if (typeOfInpatientFacility == "02" || typeOfInpatientFacility == "04") {
                $("#discharge_M2420").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=DischargeFromAgency_M2420DischargeDisposition][value=' + (result["M2420DischargeDisposition"] != null && result["M2420DischargeDisposition"] != undefined ? result["M2420DischargeDisposition"].Answer : "") + ']').attr('checked', true);
                $("#discharge_M2420").unblock();
            }
            $("#DischargeFromAgency_M0903LastHomeVisitDate").val(result["M0903LastHomeVisitDate"] != null && result["M0903LastHomeVisitDate"] != undefined ? result["M0903LastHomeVisitDate"].Answer : "");
            $("#DischargeFromAgency_M0906DischargeDate").val(result["M0906DischargeDate"] != null && result["M0906DischargeDate"] != undefined ? result["M0906DischargeDate"].Answer : "");
        };
    }
}