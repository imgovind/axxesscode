﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IAgencyRepository
    {
        bool Delete(Guid id);
        Agency Get(Guid id);
        IEnumerable<Agency> All();
        Guid Add(Agency agency);
        bool Edit(Agency agency);

        bool AddLocation(AgencyLocation agencyLocation);
        AgencyLocation GetMainLocation(Guid agencyId);
        IList<AgencyLocation> GetBranches(Guid agencyId);
        AgencyLocation FindLocation(Guid agencyId, Guid Id);
        bool EditLocation(AgencyLocation location);
        bool EditBranchCost(AgencyLocation location);

        bool AddContact(AgencyContact contact);
        IList<AgencyContact> GetContacts(Guid agencyId);
        AgencyContact FindContact(Guid agencyId, Guid Id);
        bool EditContact(AgencyContact contact);
        bool DeleteContact(Guid agencyId, Guid id);

        bool AddHospital(AgencyHospital hospital);
        IList<AgencyHospital> GetHospitals(Guid agencyId);
        AgencyHospital FindHospital(Guid agencyId, Guid Id);
        bool EditHospital(AgencyHospital hospital);
        bool DeleteHospital(Guid agencyId, Guid Id);
        
        bool AddInsurance(AgencyInsurance insurance);
        IList<AgencyInsurance> GetInsurances(Guid agencyId);
        AgencyInsurance GetInsurance(int insuranceId, Guid agencyId);
        AgencyInsurance FindInsurance(Guid agencyId, int Id);
        bool EditInsurance(AgencyInsurance insurance);
        bool DeleteInsurance(Guid agencyId, int Id);

    }
}
