﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;

    using AutoMapper;

    using SubSonic.Repository;

    public class AgencyRepository : IAgencyRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region Agency Methods

        public bool Delete(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            bool result = false;

            var agency = database.Single<Agency>(a => a.Id == id);

            if (agency != null)
            {
                database.Delete<Agency>(agency);
                result = true;
            }

            return result;
        }

        public Agency Get(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            var agency = database.Single<Agency>(a => a.Id == id);
            if (agency != null)
            {
                agency.MainLocation = GetMainLocation(id);
            }

            return agency;
        }

        public Guid Add(Agency agency)
        {
            try
            {
                agency.Id = Guid.NewGuid();
                agency.Created = DateTime.Now;
                agency.Modified = DateTime.Now;

                database.Add<Agency>(agency);
            }
            catch (Exception)
            {
                //TODO: Log Exception
                return Guid.Empty;
            }

            return agency.Id;

        }     

        public bool AddLocation(AgencyLocation agencyLocation)
        {
            var result = false;
            if (agencyLocation != null)
            {
                agencyLocation.Id = Guid.NewGuid();
                if (agencyLocation.PhoneArray != null && agencyLocation.PhoneArray.Count > 0)
                {
                    agencyLocation.PhoneWork = agencyLocation.PhoneArray.ToArray().PhoneEncode();
                }
                if (agencyLocation.FaxNumberArray != null && agencyLocation.FaxNumberArray.Count > 0)
                {
                    agencyLocation.FaxNumber = agencyLocation.FaxNumberArray.ToArray().PhoneEncode();
                }
                agencyLocation.Created = DateTime.Now;
                agencyLocation.Modified = DateTime.Now;

                database.Add<AgencyLocation>(agencyLocation);
                result = true;
            }

            return result;
        }

        public AgencyLocation GetMainLocation(Guid agencyId)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true);
        }

        public IList<AgencyLocation> GetBranches(Guid agencyId)
        {
            return database.GetPaged<AgencyLocation>(l => l.AgencyId == agencyId, "Created", 0, 20).ToList();
        }

        public AgencyLocation FindLocation(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == Id );
        }

        public bool EditLocation(AgencyLocation location)
        {
            var result = false;
            var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
            if (location != null && existingLocation != null)
            {
                if (location.PhoneArray.Count > 0)
                {
                    existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                }
                if (location.FaxNumberArray.Count > 0)
                {
                    existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingLocation.Name = location.Name;
                existingLocation.AddressLine1 = location.AddressLine1;
                existingLocation.AddressLine2 = location.AddressLine2;
                existingLocation.AddressCity = location.AddressCity;
                existingLocation.AddressStateCode = location.AddressStateCode;
                existingLocation.AddressZipCode = location.AddressZipCode;
                existingLocation.Comments = location.Comments;
                existingLocation.Modified = DateTime.Now;
                database.Update<AgencyLocation>(existingLocation);
                result = true;
            }
            return result;
        }

        public bool EditBranchCost(AgencyLocation location)
        {
            var result = false;
            var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
            if (location != null && existingLocation != null)
            {
                existingLocation.Cost = location.Cost;
                existingLocation.Modified = DateTime.Now;
                database.Update<AgencyLocation>(existingLocation);
                result = true;
            }
            return result;
        }

        public IEnumerable<Agency> All()
        {
            return database.All<Agency>().AsEnumerable<Agency>();
        }

        public bool Edit(Agency agencyInfo)
        {
            Check.Argument.IsNotNull(agencyInfo, "agencyViewData");

            bool result = false;
            var agency = database.Single<Agency>(a => a.Id == agencyInfo.Id);

            if (agency != null)
            {
                agency.Name = agencyInfo.Name;
                agency.TaxId = agencyInfo.TaxId;
                agency.ContactPersonEmail = agency.ContactPersonEmail;
                agency.ContactPersonFirstName = agency.ContactPersonFirstName;
                agency.ContactPersonLastName = agency.ContactPersonLastName;
                agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                agency.TaxIdType = agencyInfo.TaxIdType;
                agency.Payor = agencyInfo.Payor;
                agency.NationalProviderNumber = agencyInfo.NationalProviderNumber;
                agency.MedicareProviderNumber = agencyInfo.MedicareProviderNumber;
                agency.MedicaidProviderNumber = agencyInfo.MedicaidProviderNumber;
                agency.HomeHealthAgencyId = agencyInfo.HomeHealthAgencyId;
                agency.Modified = DateTime.Now;

                database.Update<Agency>(agencyInfo);
                result = true;
            }

            return result;
        }

        #endregion

        #region Contact Methods

        public bool AddContact(AgencyContact contact)
        {
            var result = false;
            if (contact != null)
            {
                contact.Id = Guid.NewGuid();
                if (contact.PhonePrimaryArray.Count > 0)
                {
                    contact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    contact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                contact.Created = DateTime.Now;
                contact.Modified = DateTime.Now;

                database.Add<AgencyContact>(contact);
                result = true;
            }
            return result;
        }

        public IList<AgencyContact> GetContacts(Guid agencyId)
        {
            return database.Find<AgencyContact>(c => c.AgencyId == agencyId).ToList();
        }

        public AgencyContact FindContact(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyContact>(c => c.Id == Id && c.AgencyId == agencyId);
        }

        public bool EditContact(AgencyContact contact)
        {
            var result = false;
            var existingContact = database.Single<AgencyContact>(I => I.Id == contact.Id && I.AgencyId == contact.AgencyId);
            if (contact != null)
            {

                if (contact.PhonePrimaryArray.Count > 0)
                {
                    existingContact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    existingContact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                existingContact.FirstName = contact.FirstName;
                existingContact.LastName = contact.LastName;
                existingContact.CompanyName = contact.CompanyName;               
                existingContact.AddressLine1 = contact.AddressLine1;
                existingContact.AddressLine2 = contact.AddressLine2;
                existingContact.AddressCity = contact.AddressCity;
                existingContact.AddressStateCode = contact.AddressStateCode;
                existingContact.AddressZipCode = contact.AddressZipCode;
                existingContact.EmailAddress = contact.EmailAddress;
                existingContact.ContactType = contact.ContactType;
                existingContact.ContactTypeOther = contact.ContactTypeOther;               
                existingContact.Modified = DateTime.Now;
                database.Update<AgencyContact>(existingContact);
                result = true;
            }
            return result;
        }

        public bool DeleteContact(Guid agencyId, Guid id)
        {
            bool result = false;
            var contact = database.Single<AgencyContact>(c => c.Id == id && c.AgencyId == agencyId);
            if (contact != null)
            {
                int i = database.Delete<AgencyContact>(id);
                if (i > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        #endregion

        #region Insurance

        public bool AddInsurance(AgencyInsurance insurance)
        {
            var result = false;
            if (insurance != null)
            {
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    insurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    insurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                insurance.Created = DateTime.Now;
                insurance.Modified = DateTime.Now;

                database.Add<AgencyInsurance>(insurance);
                result = true;
            }
            return result;
        }

        public AgencyInsurance GetInsurance(int insuranceId, Guid agencyId)
        {
            return database.Single<AgencyInsurance>(i => i.Id == insuranceId && i.AgencyId == agencyId);
        }

        public IList<AgencyInsurance> GetInsurances(Guid agencyId)
        {
            return database.Find<AgencyInsurance>(i => i.AgencyId == agencyId).ToList();
        }

        public AgencyInsurance FindInsurance(Guid agencyId, int Id)
        {
            return database.Single<AgencyInsurance>(i => i.Id == Id && i.AgencyId == agencyId);
        }

        public bool EditInsurance(AgencyInsurance insurance)
        {
            var result = false;
            var existingInsurance = database.Single<AgencyInsurance>(I => I.Id == insurance.Id && I.AgencyId == insurance.AgencyId);
            if (insurance != null)
            {

                if (insurance.PhoneNumberArray.Count > 0)
                {
                    existingInsurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    existingInsurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingInsurance.PayorType = insurance.PayorType;
                existingInsurance.InvoiceType = insurance.InvoiceType;
                existingInsurance.ChargeGrouping = insurance.ChargeGrouping;
                existingInsurance.ChargeType = insurance.ChargeType;
                existingInsurance.ParentInsurance = insurance.ParentInsurance;
                existingInsurance.Name = insurance.Name;              
                existingInsurance.AddressLine1 = insurance.AddressLine1;
                existingInsurance.AddressLine2 = insurance.AddressLine2;
                existingInsurance.AddressCity = insurance.AddressCity;
                existingInsurance.AddressStateCode = insurance.AddressStateCode;
                existingInsurance.AddressZipCode = insurance.AddressZipCode;
                existingInsurance.ProviderCode = insurance.ProviderCode;
                existingInsurance.ProviderNumber = insurance.ProviderNumber;
                existingInsurance.HealthPlanId = insurance.HealthPlanId;
                existingInsurance.OtherProviderId = insurance.OtherProviderId;
                existingInsurance.Ub04Locator81cca = insurance.Ub04Locator81cca;
                existingInsurance.ClearingHouse = insurance.ClearingHouse;
                existingInsurance.InterchangeReceiverId = insurance.InterchangeReceiverId;
                existingInsurance.PayorId = insurance.PayorId;
                existingInsurance.ContactPersonFirstName = insurance.ContactPersonFirstName;
                existingInsurance.ContactPersonLastName = insurance.ContactPersonLastName;
                existingInsurance.ContactEmailAddress = insurance.ContactEmailAddress;
                existingInsurance.CurrentBalance = insurance.CurrentBalance;
                existingInsurance.WorkWeekStartDay = insurance.WorkWeekStartDay;
                existingInsurance.IsVisitAuthorizationRequired = insurance.IsVisitAuthorizationRequired;
                existingInsurance.DefaultFiscalIntermediary = insurance.DefaultFiscalIntermediary;
                existingInsurance.Charge = insurance.Charge;
                existingInsurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(existingInsurance);
                result = true;
            }
            return result;
        }
        public bool DeleteInsurance(Guid agencyId, int Id)
        {
            bool result = false;
            var insurance = database.Single<AgencyInsurance>(i => i.Id == Id && i.AgencyId == agencyId);
            if (insurance != null)
            {
                int i = database.Delete<AgencyInsurance>(Id);
                if (i > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        #endregion

        #region Hospital

        public bool AddHospital(AgencyHospital hospital)
        {
            var result = false;
            if (hospital != null)
            {
                hospital.Id = Guid.NewGuid();
                if (hospital.PhoneArray.Count > 0)
                {
                    hospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    hospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                hospital.Created = DateTime.Now;
                hospital.Modified = DateTime.Now;

                database.Add<AgencyHospital>(hospital);
                result = true;
            }
            return result;
        }

        public IList<AgencyHospital> GetHospitals(Guid agencyId)
        {
            return database.All<AgencyHospital>().ToList();
        }

        public AgencyHospital FindHospital(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId);
        }

        public bool EditHospital(AgencyHospital hospital)
        {
            var result = false;
            var existingHospital = database.Single<AgencyHospital>(h => h.Id == hospital.Id && h.AgencyId == hospital.AgencyId);
            if (hospital != null)
            {
              
                if (hospital.PhoneArray.Count > 0)
                {
                    existingHospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    existingHospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingHospital.Name = hospital.Name;
                existingHospital.ContactPersonFirstName = hospital.ContactPersonFirstName;
                existingHospital.ContactPersonLastName = hospital.ContactPersonLastName;
                existingHospital.AddressLine1 = hospital.AddressLine1;
                existingHospital.AddressLine2 = hospital.AddressLine2;
                existingHospital.AddressCity = hospital.AddressCity;
                existingHospital.AddressStateCode = hospital.AddressStateCode;
                existingHospital.AddressZipCode = hospital.AddressZipCode;
                existingHospital.EmailAddress = hospital.EmailAddress;
                existingHospital.Comment = hospital.Comment;
                existingHospital.Modified = DateTime.Now;

                database.Update<AgencyHospital>(existingHospital);
                result = true;
            }
            return result;

        }

        public bool DeleteHospital(Guid agencyId, Guid Id)
        {
            bool result = false;
            var hospital = database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId);
            if (hospital != null)
            {
               int i= database.Delete<AgencyHospital>(Id);
               if (i > 0)
               {
                   result = true;
               }
            }
            return result;
        }

      
        
        #endregion
    }
}
