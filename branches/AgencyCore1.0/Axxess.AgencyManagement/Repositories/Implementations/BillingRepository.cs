﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;


    public class BillingRepository : IBillingRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public BillingRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IBillingRepository Members

        public bool AddClaim(Claim rap)
        {
            bool result = false;
            if (rap != null)
            {
                rap.Created = DateTime.Now;
                rap.RapModified = DateTime.Now;
                rap.FinalModified = DateTime.Now;
                rap.AgencyId = rap.AgencyId;
                database.Add<Claim>(rap);
                result = true;
            }
            return result;
        }

        public bool AddRap(Rap rap)
        {
            bool result = false;
            if (rap != null)
            {
                rap.Created = DateTime.Now;
                rap.Modified = DateTime.Now;
                rap.AgencyId = rap.AgencyId;
                database.Add<Rap>(rap);
                result = true;
            }
            return result;
        }

        public Claim GetClaim(Guid agencyId, int claimId)
        {
            Check.Argument.IsNotNegativeOrZero(claimId, "claimId");
            return database.Single<Claim>(c => (c.AgencyId == agencyId && c.Id == claimId));
        }

        public Rap GetRap(Guid agencyId, int claimId)
        {
            Check.Argument.IsNotNegativeOrZero(claimId, "claimId");
            return database.Single<Rap>(r => (r.AgencyId == agencyId && r.Id == claimId));
        }

        public Final GetFinal(Guid agencyId, int claimId)
        {
            Check.Argument.IsNotNegativeOrZero(claimId, "claimId");
            return database.Single<Final>(f => (f.AgencyId == agencyId && f.Id == claimId));
        }

        public Claim GetClaim(Guid agencyId, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            return database.Single<Claim>(c => (c.AgencyId == agencyId && c.PatientId == patientId && c.EpisodeId == episodeId));
        }

        public Rap GetRap(Guid agencyId, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            return database.Single<Rap>(r => (r.AgencyId == agencyId && r.PatientId == patientId && r.EpisodeId == episodeId));
        }

        public bool UpdateClaimStatus(Claim claim)
        {
            Check.Argument.IsNotNull(claim, "claim");
            var currentClaim = database.Single<Claim>(r => (r.AgencyId == claim.AgencyId && r.PatientId == claim.PatientId && r.EpisodeId == claim.EpisodeId));

            if (currentClaim != null)
            {
                try
                {
                    currentClaim.PatientIdNumber = claim.PatientIdNumber;
                    currentClaim.EpisodeStartDate = claim.EpisodeStartDate;
                    currentClaim.EpisodeEndDate = claim.EpisodeEndDate;
                    currentClaim.IsOasisComplete = claim.IsOasisComplete;
                    currentClaim.IsFirstBillableVisit = claim.IsFirstBillableVisit;
                    currentClaim.FirstBillableVisitDate = claim.FirstBillableVisitDate;
                    currentClaim.IsRapGenerated = claim.IsRapGenerated;
                    currentClaim.IsRapVerified = claim.IsRapVerified;

                    currentClaim.RapRemark = claim.RapRemark;
                    currentClaim.FinalRemark = claim.FinalRemark;
                    currentClaim.MedicareNumber = claim.MedicareNumber;
                    currentClaim.FirstName = claim.FirstName;
                    currentClaim.LastName = claim.LastName;
                    currentClaim.DOB = claim.DOB;
                    currentClaim.Gender = claim.Gender;
                    currentClaim.AddressLine1 = claim.AddressLine1;
                    currentClaim.AddressLine2 = claim.AddressLine2;
                    currentClaim.AddressCity = claim.AddressCity;
                    currentClaim.AddressStateCode = claim.AddressStateCode;
                    currentClaim.AddressZipCode = claim.AddressZipCode;
                    currentClaim.StartofCareDate = claim.StartofCareDate;
                    currentClaim.PhysicianNPI = claim.PhysicianNPI;
                    currentClaim.PhysicianFirstName = claim.PhysicianFirstName;
                    currentClaim.PhysicianLastName = claim.PhysicianLastName;
                    currentClaim.DiagonasisCode = claim.DiagonasisCode;
                    currentClaim.HippsCode = claim.HippsCode;
                    currentClaim.ClaimKey = claim.ClaimKey;
                    currentClaim.AreOrdersComplete = claim.AreOrdersComplete;
                    currentClaim.AreVisitsComplete = claim.AreVisitsComplete;
                    currentClaim.IsFinalGenerated = claim.IsFinalGenerated;

                    currentClaim.Created = claim.Created;
                    currentClaim.VerifiedVisits = claim.VerifiedVisits;
                    currentClaim.PrimaryInsuranceId = claim.PrimaryInsuranceId;
                    currentClaim.IsSupplyVerified = claim.IsSupplyVerified;
                    currentClaim.IsFinalInfoVerified = claim.IsFinalInfoVerified;
                    currentClaim.IsVisitVerified = claim.IsVisitVerified;
                    currentClaim.AgencyId = claim.AgencyId;
                    database.Update<Claim>(currentClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool AddFinal(Final final)
        {
            Check.Argument.IsNotNull(final, "final");
            bool result = false;
            try
            {
                final.Created = DateTime.Now;
                final.Modified = DateTime.Now;
                final.AgencyId = final.AgencyId;
                database.Add<Final>(final);
            }
            catch (Exception e)
            {
                return false;
            }
            return result;
        }

        public Final GetFinal(Guid agencyId, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            return database.Single<Final>(r => (r.AgencyId == agencyId && r.PatientId == patientId && r.EpisodeId == episodeId));
        }

        public bool UpdateFinalStatus(Final final)
        {
            Check.Argument.IsNotNull(final, "final");
            var currentFinal = database.Single<Final>(r => (r.AgencyId == final.AgencyId && r.PatientId == final.PatientId && r.EpisodeId == final.EpisodeId));

            if (currentFinal != null)
            {
                try
                {
                    currentFinal.PatientIdNumber = final.PatientIdNumber;
                    currentFinal.EpisodeStartDate = final.EpisodeStartDate;
                    currentFinal.EpisodeEndDate = final.EpisodeEndDate;
                    currentFinal.IsOasisComplete = final.IsOasisComplete;
                    currentFinal.IsFirstBillableVisit = final.IsFirstBillableVisit;
                    currentFinal.FirstBillableVisitDate = final.FirstBillableVisitDate;
                    currentFinal.Remark = final.Remark;
                    currentFinal.MedicareNumber = final.MedicareNumber;
                    currentFinal.FirstName = final.FirstName;
                    currentFinal.LastName = final.LastName;
                    currentFinal.DOB = final.DOB;
                    currentFinal.Gender = final.Gender;
                    currentFinal.AddressLine1 = final.AddressLine1;
                    currentFinal.AddressLine2 = final.AddressLine2;
                    currentFinal.AddressCity = final.AddressCity;
                    currentFinal.AddressStateCode = final.AddressStateCode;
                    currentFinal.AddressZipCode = final.AddressZipCode;
                    currentFinal.StartofCareDate = final.StartofCareDate;
                    currentFinal.PhysicianNPI = final.PhysicianNPI;
                    currentFinal.PhysicianFirstName = final.PhysicianFirstName;
                    currentFinal.PhysicianLastName = final.PhysicianLastName;
                    currentFinal.DiagonasisCode = final.DiagonasisCode;
                    currentFinal.HippsCode = final.HippsCode;
                    currentFinal.ClaimKey = final.ClaimKey;
                    currentFinal.AreOrdersComplete = final.AreOrdersComplete;
                    currentFinal.AreVisitsComplete = final.AreVisitsComplete;
                    currentFinal.Created = final.Created;
                    currentFinal.VerifiedVisits = final.VerifiedVisits;
                    currentFinal.PrimaryInsuranceId = final.PrimaryInsuranceId;
                    currentFinal.IsSupplyVerified = final.IsSupplyVerified;
                    currentFinal.IsFinalInfoVerified = final.IsFinalInfoVerified;
                    currentFinal.IsVisitVerified = final.IsVisitVerified;
                    currentFinal.AgencyId = final.AgencyId;
                    currentFinal.IsRapGenerated = final.IsRapGenerated;
                    currentFinal.Status = final.Status;
                    currentFinal.IsGenerated = final.IsGenerated;
                    database.Update<Final>(currentFinal);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateRapStatus(Rap rap)
        {

            Check.Argument.IsNotNull(rap, "rap");
            var currentRap = database.Single<Rap>(r => (r.AgencyId == rap.AgencyId && r.PatientId == rap.PatientId && r.EpisodeId == rap.EpisodeId));
            if (currentRap != null)
            {
                try
                {
                    currentRap.PatientIdNumber = rap.PatientIdNumber;
                    currentRap.EpisodeStartDate = rap.EpisodeStartDate;
                    currentRap.EpisodeEndDate = rap.EpisodeEndDate;
                    currentRap.IsOasisComplete = rap.IsOasisComplete;
                    currentRap.IsFirstBillableVisit = rap.IsFirstBillableVisit;
                    currentRap.FirstBillableVisitDate = rap.FirstBillableVisitDate;
                    currentRap.IsGenerated = rap.IsGenerated;
                    currentRap.IsVerified = rap.IsVerified;
                    currentRap.Modified = rap.Modified;
                    currentRap.Remark = rap.Remark;
                    currentRap.MedicareNumber = rap.MedicareNumber;
                    currentRap.FirstName = rap.FirstName;
                    currentRap.LastName = rap.LastName;
                    currentRap.DOB = rap.DOB;
                    currentRap.Gender = rap.Gender;
                    currentRap.AddressLine1 = rap.AddressLine1;
                    currentRap.AddressLine2 = rap.AddressLine2;
                    currentRap.AddressCity = rap.AddressCity;
                    currentRap.AddressStateCode = rap.AddressStateCode;
                    currentRap.AddressZipCode = rap.AddressZipCode;
                    currentRap.StartofCareDate = rap.StartofCareDate;
                    currentRap.PhysicianNPI = rap.PhysicianNPI;
                    currentRap.PhysicianFirstName = rap.PhysicianFirstName;
                    currentRap.PhysicianLastName = rap.PhysicianLastName;
                    currentRap.DiagonasisCode = rap.DiagonasisCode;
                    currentRap.HippsCode = rap.HippsCode;
                    currentRap.ClaimKey = rap.ClaimKey;
                    currentRap.AreOrdersComplete = rap.AreOrdersComplete;
                    currentRap.Created = rap.Created;
                    currentRap.PrimaryInsuranceId = rap.PrimaryInsuranceId;
                    currentRap.Status = rap.Status;
                    currentRap.AgencyId = rap.AgencyId;
                    database.Update<Rap>(currentRap);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public Bill AllUnProcessedBill(Guid agencyId)
        {
            var raps = database.Find<Rap>(r => r.IsGenerated == false && r.AgencyId == agencyId);
            var finals = database.Find<Final>(f => f.IsGenerated == false && f.AgencyId == agencyId);
            return new Bill { Raps = raps, Finals = finals };
        }

        public bool VerifyRap(Rap rap, Guid agencyId)
        {
            Check.Argument.IsNotNull(rap, "rap");
            var currentClaim = database.Single<Rap>(r => (r.AgencyId == agencyId && r.Id == rap.Id));
            if (currentClaim != null)
            {
                try
                {
                    currentClaim.Remark = rap.Remark;
                    currentClaim.IsVerified = true;
                    currentClaim.Modified = DateTime.Now;
                    database.Update<Rap>(currentClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;


        }

        public bool VerifyInfo(Final final, Guid agencyId)
        {
            Check.Argument.IsNotNull(final, "final");
            var currentClaim = database.Single<Final>(r => (r.AgencyId == agencyId && r.Id == final.Id));
            if (currentClaim != null)
            {
                try
                {
                    currentClaim.Remark = final.Remark;
                    currentClaim.IsFinalInfoVerified = true;
                    currentClaim.Modified = DateTime.Now;
                    database.Update<Final>(currentClaim);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool VisitVerify(int Id, Guid episodeId, Guid patientId, List<Guid> Visit, Guid agencyId)
        {
            return true;
        }

        public List<Rap> Raps(List<int> rapToGenerate, Guid agencyId)
        {
            List<Rap> rapClaim = new List<Rap>();
            if (rapToGenerate != null)
            {
                rapToGenerate.ForEach(rap =>
                {
                    Rap claim = null;
                    if (rap != 0)
                    {
                        claim = GetRap(agencyId, rap);
                    }
                    if (claim != null)
                    {
                        rapClaim.Add(claim);
                    }
                });
            }
            return rapClaim;
        }

        public List<Final> Finals(List<int> finalToGenerate, Guid agencyId)
        {
            List<Final> finlaClaim = new List<Final>();
            if (finalToGenerate != null)
            {
                finalToGenerate.ForEach(final =>
                {
                    Final claim = null;
                    if (final != 0)
                    {
                        claim = GetFinal(agencyId, final);
                    }
                    if (claim != null)
                    {
                        finlaClaim.Add(claim);
                    }
                });
            }
            return finlaClaim;
        }

        public bool UpdateRapStatus(List<int> rapToGenerate, Guid agencyId)
        {
            bool result = false;
            if (rapToGenerate != null)
            {
                rapToGenerate.ForEach(r =>
                {
                    var rap = GetRap(agencyId, r);
                    if (rap != null)
                    {
                        rap.Status = (int)ScheduleStatus.ClaimAccepted;
                        rap.IsGenerated = true;
                        UpdateRapStatus(rap);
                        var final = GetFinal(agencyId, rap.PatientId, rap.EpisodeId);
                        if (final != null)
                        {
                            final.IsRapGenerated = true;
                            UpdateFinalStatus(final);
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool UpdateFinalStatus(List<int> finalToGenerate, Guid agencyId)
        {
            bool result = false;
            if (finalToGenerate != null)
            {
                finalToGenerate.ForEach(f =>
                {
                    var final = GetFinal(agencyId, f);
                    if (final != null)
                    {
                        final.Status = (int)ScheduleStatus.ClaimAccepted;
                        final.IsGenerated = true;
                        UpdateFinalStatus(final);
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public IList<Rap> GetRaps(Guid agencyId)
        {
            return database.Find<Rap>(r => r.AgencyId == agencyId);
        }

        public IList<Final> GetFinals(Guid agencyId)
        {
            return database.Find<Final>(r => r.AgencyId == agencyId);
        }

        public IList<Rap> GetRapProcessed(Guid patientId, Guid agencyId)
        {
            return database.Find<Rap>(r => r.PatientId == patientId && r.AgencyId == agencyId);
        }

        public IList<Final> GetFinalProcessed(Guid patientId, Guid agencyId)
        {
            return database.Find<Final>(r => r.PatientId == patientId && r.AgencyId == agencyId);
        }

        public bool DeleteRap(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var rap = this.GetRap(agencyId, patientId, episodeId);
            try
            {
                if (rap != null)
                {
                    database.Delete<Rap>(rap);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DeleteFinal(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var final = this.GetFinal(agencyId, patientId, episodeId);
            try
            {
                if (final != null)
                {
                    database.Delete<Rap>(final);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion
    }
}
