﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;

    using AutoMapper;

    using SubSonic.Repository;

    public class AssetRepository : IAssetRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AssetRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssetRepository Methods

        public Asset Get(Guid id, Guid agencyId)
        {
            return database.Single<Asset>(a => a.Id == id && a.AgencyId == agencyId);
        }

        public bool Delete(Guid id)
        {
            bool result = false;
            if (!id.IsEmpty())
            {
                database.Delete<Asset>(id);
                result = true;
            }
            return result;
        }

        public bool Add(Asset asset)
        {
            bool result = false;

            if (asset != null)
            {
                asset.Id = Guid.NewGuid();
                asset.Created = DateTime.Now;
                asset.Modified = DateTime.Now;

                database.Add<Asset>(asset);
                result = true;
            }

            return result;
        }

        #endregion
    }
}
