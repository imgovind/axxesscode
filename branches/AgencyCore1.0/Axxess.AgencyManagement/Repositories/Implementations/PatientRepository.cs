﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using System.Transactions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Domain;
    using Extensions;

    using AutoMapper;

    using SubSonic.Repository;
    using SubSonic.DataProviders;

    public class PatientRepository : IPatientRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PatientRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }
        #endregion

        #region IPatientRepository Members

        public bool Delete(Guid agencyId, Guid id)
        {
            bool result = false;
            if (!id.IsEmpty())
            {
                var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == id);

                if (patient != null)
                {
                    database.Delete<Patient>(id);
                    result = true;
                }
            }
            return result;
        }

        public IList<Patient> GetAllByAgencyId(Guid agencyId)
        {
            var patients = database.Find<Patient>(p => p.AgencyId == agencyId);

            patients.ForEach(p =>
            {
                p.EmergencyContacts = GetEmergencyContacts(p.Id);
                p.PhysicianContacts = GetPatientPhysicians(p.Id);
                p.Physician = p.PhysicianContacts.SingleOrDefault(pc => pc.Primary);
                p.Branch = GetBranchName(agencyId, p.AgencyLocationId);
            });

            return patients.OrderBy(p => p.LastName).ToList();
        }

        public IList<Patient> GetPendingByAgencyId(Guid agencyId)
        {
            var patients = database.Find<Patient>(p => p.AgencyId == agencyId && p.Status == (int)PatientStatus.Pending);

            patients.ForEach(p =>
            {
                p.EmergencyContacts = GetEmergencyContacts(p.Id);
                p.PhysicianContacts = GetPatientPhysicians(p.Id);
                p.Physician = p.PhysicianContacts.SingleOrDefault(pc => pc.Primary);
                p.Branch = GetBranchName(agencyId, p.AgencyLocationId);
            });

            return patients.OrderBy(p => p.LastName).ToList();
        }

        private string GetBranchName(Guid agencyId, Guid locationId)
        {
            var location = database.Single<AgencyLocation>(l => l.Id == locationId && l.AgencyId == agencyId);
            if (location != null)
            {
                return location.Name;
            }
            return string.Empty;
        }

        public IList<Patient> Find(int statusId, Guid agencyId)
        {
            return database.Find<Patient>(p => p.Status == statusId && p.AgencyId == agencyId);
        }

        public IList<Patient> Find(int statusId, Guid branchCode, Guid agencyId)
        {
            if (statusId == 0 && branchCode.IsEmpty())
            {
                return database.Find<Patient>(p => p.AgencyId == agencyId);
            }
            if (branchCode.IsEmpty() && statusId > 0)
            {
                return database.Find<Patient>(p => p.Status == statusId && p.AgencyId == agencyId);
            }
            if (!branchCode.IsEmpty() && statusId == 0)
            {
                return database.Find<Patient>(p => p.AgencyLocationId == branchCode && p.AgencyId == agencyId);
            }
            return database.Find<Patient>(p => p.Status == statusId && p.AgencyLocationId == branchCode && p.AgencyId == agencyId);
        }

        public IList<Patient> GetAllByBirthDayRange(DateTime startDate, DateTime endDate, Guid agencyId)
        {
            return database.Find<Patient>(p => p.DOB >= startDate && p.DOB <= endDate && p.AgencyId == agencyId);
        }

        public bool Add(Patient patient)
        {
            bool result = false;

            if (patient != null)
            {
                patient.Id = Guid.NewGuid();
                if (patient.PhoneHomeArray != null && patient.PhoneHomeArray.Count > 0)
                {
                    patient.PhoneHome = patient.PhoneHomeArray.ToArray().PhoneEncode();
                }
                if (patient.PhoneMobileArray != null && patient.PhoneMobileArray.Count > 0)
                {
                    patient.PhoneMobile = patient.PhoneMobileArray.ToArray().PhoneEncode();
                }
                if (patient.EthnicRaces != null && patient.EthnicRaces.Count > 0)
                {
                    patient.Ethnicities = patient.EthnicRaces.ToArray().AddColons();
                }
                if (patient.PaymentSources != null && patient.PaymentSources.Count > 0)
                {
                    patient.PaymentSource = patient.PaymentSources.ToArray().AddColons();
                }
                if (patient.DMECollection != null && patient.DMECollection.Count > 0)
                {
                    patient.DME = patient.DMECollection.ToArray().AddColons();
                }
                if (patient.ServicesRequiredCollection != null && patient.ServicesRequiredCollection.Count > 0)
                {
                    patient.ServicesRequired = patient.ServicesRequiredCollection.ToArray().AddColons();
                }
                if (patient.PharmacyPhoneArray != null && patient.PharmacyPhoneArray.Count > 0)
                {
                    patient.PharmacyPhone = patient.PharmacyPhoneArray.ToArray().PhoneEncode();
                }
                patient.Created = DateTime.Now;
                patient.Modified = DateTime.Now;
                database.Add<Patient>(patient);
                result = true;
            }
            return result;
        }

        public bool AdmitPatient(PendingPatient pending)
        {
            bool result = false;

            if (pending != null)
            {
                var patient = database.Single<Patient>(p => p.Id == pending.Id && p.AgencyId == pending.AgencyId);
                if (patient != null)
                {
                    patient.PatientIdNumber = pending.PatientIdNumber;
                    patient.StartofCareDate = pending.StartofCareDate;
                    patient.ReferralDate = pending.ReferralDate;
                    patient.CaseManagerId = pending.CaseManagerId;
                    patient.UserId = pending.UserId;
                    patient.PrimaryInsurance = pending.PrimaryInsurance;
                    patient.SecondaryInsurance = pending.SecondaryInsurance;
                    patient.TertiaryInsurance = pending.TertiaryInsurance;
                    patient.Payer = pending.Payer;

                    patient.Status = (int)PatientStatus.Active;
                    patient.Modified = DateTime.Now;

                    database.Update<Patient>(patient);
                    result = true;
                }
            }
            return result;
        }

        public bool NonAdmitPatient(PendingPatient pending)
        {
            bool result = false;

            if (pending != null)
            {
                var patient = database.Single<Patient>(p => p.Id == pending.Id);
                if (patient != null)
                {
                    patient.NonAdmissionDate = pending.NonAdmitDate;
                    patient.NonAdmissionReason = pending.Reason;
                    patient.Comments = pending.Comments;
                    patient.Status = (int)PatientStatus.NonAdmission;
                    patient.Modified = DateTime.Now;

                    database.Update<Patient>(patient);
                    result = true;
                }
            }
            return result;
        }

        public bool Edit(Patient patient)
        {
            bool result = false;
            if (patient != null)
            {
                if (patient.PhoneHomeArray.Count > 0)
                {
                    patient.PhoneHome = patient.PhoneHomeArray.ToArray().PhoneEncode();
                }
                if (patient.PhoneMobileArray.Count > 0)
                {
                    patient.PhoneMobile = patient.PhoneMobileArray.ToArray().PhoneEncode();
                }
                if (patient.EthnicRaces.Count > 0)
                {
                    patient.Ethnicities = patient.EthnicRaces.ToArray().AddColons();
                }
                if (patient.PaymentSources.Count > 0)
                {
                    patient.PaymentSource = patient.PaymentSources.ToArray().AddColons();
                }
                if (patient.ServicesRequiredCollection.Count > 0)
                {
                    patient.ServicesRequired = patient.ServicesRequiredCollection.ToArray().AddColons();
                }
                if (patient.PharmacyPhoneArray.Count > 0)
                {
                    patient.PharmacyPhone = patient.PharmacyPhoneArray.ToArray().PhoneEncode();
                }
                if (patient.DMECollection.Count > 0)
                {
                    patient.DME = patient.DMECollection.ToArray().AddColons();
                }
                Patient patientToEdit = database.Single<Patient>(p => p.Id == patient.Id && p.AgencyId == patient.AgencyId);

                if (patientToEdit != null)
                {
                    patientToEdit.FirstName = patient.FirstName;
                    patientToEdit.LastName = patient.LastName;
                    patientToEdit.MiddleInitial = patient.MiddleInitial;
                    patientToEdit.Gender = patient.Gender;
                    patientToEdit.DOB = patient.DOB;
                    patientToEdit.MaritalStatus = patient.MaritalStatus;
                    patientToEdit.PatientIdNumber = patient.PatientIdNumber;
                    patientToEdit.MedicareNumber = patient.MedicareNumber;
                    patientToEdit.MedicaidNumber = patient.MedicaidNumber;
                    patientToEdit.SSN = patient.SSN;
                    patientToEdit.StartofCareDate = patient.StartofCareDate;
                    patientToEdit.Ethnicities = patient.Ethnicities;
                    patientToEdit.PaymentSource = patient.PaymentSource;
                    patientToEdit.OtherPaymentSource = patient.OtherPaymentSource;
                    patientToEdit.AddressLine1 = patient.AddressLine1;
                    patientToEdit.AddressLine2 = patient.AddressLine2;
                    patientToEdit.AddressCity = patient.AddressCity;
                    patientToEdit.AddressStateCode = patient.AddressStateCode;
                    patientToEdit.AddressZipCode = patient.AddressZipCode;
                    patientToEdit.PhoneHome = patient.PhoneHome;
                    patientToEdit.PhoneMobile = patient.PhoneMobile;
                    patientToEdit.EmailAddress = patient.EmailAddress;
                    patientToEdit.PharmacyName = patient.PharmacyName;
                    patientToEdit.PharmacyPhone = patient.PharmacyPhone;
                    patientToEdit.PrimaryInsurance = patient.PrimaryInsurance;
                    patientToEdit.SecondaryInsurance = patient.SecondaryInsurance;
                    patientToEdit.TertiaryInsurance = patient.TertiaryInsurance;
                    patientToEdit.AdmissionSource = patient.AdmissionSource;
                    patientToEdit.ReferralDate = patient.ReferralDate;
                    patientToEdit.ReferrerPhysician = patient.ReferrerPhysician;
                    patientToEdit.InternalReferral = patient.InternalReferral;
                    patientToEdit.OtherReferralSource = patient.OtherReferralSource;
                    patientToEdit.ServicesRequired = patient.ServicesRequired;
                    patientToEdit.DME = patient.DME;
                    patientToEdit.Payer = patient.Payer;
                    patientToEdit.Triage = patient.Triage;
                    patientToEdit.Comments = patient.Comments;
                    patientToEdit.CaseManagerId = patient.CaseManagerId;
                    patientToEdit.UserId = patient.UserId;
                    patientToEdit.Modified = DateTime.Now;
                    database.Update<Patient>(patientToEdit);
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }

            return result;
        }

        public bool AddPhoto(Guid patientId, Guid agencyId, Guid assetId)
        {
            bool result = false;
            if (!patientId.IsEmpty() && !agencyId.IsEmpty() && !assetId.IsEmpty())
            {
                var patient = database.Single<Patient>(p => p.Id == patientId && p.AgencyId == agencyId);

                if (patient != null)
                {
                    patient.PhotoId = assetId;
                    patient.Modified = DateTime.Now;
                    database.Update<Patient>(patient);
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }

            return result;
        }

        public bool SetStatus(Guid patientId, PatientStatus status)
        {
            bool result = false;
            if (!patientId.IsEmpty())
            {
                var patient = database.Single<Patient>(patientId);
                patient.Status = (int)status;
                database.Update<Patient>(patient);
                result = true;
            }
            return result;
        }

        public bool AddEmergencyContact(PatientEmergencyContact emergencyContactInfo)
        {
            var result = false;

            if (emergencyContactInfo != null)
            {
                emergencyContactInfo.Id = Guid.NewGuid();
                if (emergencyContactInfo.PhonePrimaryArray.Count >= 2)
                {
                    emergencyContactInfo.PrimaryPhone = emergencyContactInfo.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                database.Add<PatientEmergencyContact>(emergencyContactInfo);
                result = true;
            }
            return result;
        }

        private IList<AgencyPhysician> GetPatientPhysicians(Guid PatientID)
        {
            Check.Argument.IsNotEmpty(PatientID, "PatientID");
            IList<AgencyPhysician> physicians = new List<AgencyPhysician>();
            var patientPhysician = database.Find<PatientPhysician>(p => p.PatientId == PatientID);
            foreach (PatientPhysician physician in patientPhysician)
            {
                var agencyPhysician = database.Single<AgencyPhysician>(p => p.Id == physician.PhysicianId);
                agencyPhysician.Primary = physician.IsPrimary;
                physicians.Add(agencyPhysician);
            }
            return physicians;
        }

        public Patient Get(Guid id, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(id, "patientId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Patient patient = GetPatient<Patient>(id, agencyId);
            if (patient != null)
            {
                patient.EmergencyContacts = GetEmergencyContacts(id);
                patient.PhysicianContacts = GetPatientPhysicians(id);
                return patient;
            }
            return null;
        }

        public T GetPatient<T>(Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patient = new Patient();
            try
            {
                Mapper.CreateMap<Patient, T>();
                patient = database.Single<Patient>(p => p.Id == patientId && p.AgencyId == agencyId);
            }
            catch (Exception ex)
            {
                // TODO Log
            }

            return Mapper.Map<Patient, T>(patient);
        }

        public IList<PatientEmergencyContact> GetEmergencyContacts(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Find<PatientEmergencyContact>(p => p.PatientId == patientId).ToList();
        }

        public PatientEmergencyContact GetEmergencyContact(Guid patientId, Guid emergencyContactID)
        {
            Check.Argument.IsNotEmpty(emergencyContactID, "emergencyContactID");
            return database.Single<PatientEmergencyContact>(e => e.PatientId == patientId && e.Id == emergencyContactID);
        }

        public bool EditEmergencyContact(Guid agencyId, PatientEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            bool result = false;
            if (emergencyContact != null)
            {
                if (emergencyContact.PhonePrimaryArray.Count >= 2)
                {
                    emergencyContact.PrimaryPhone = emergencyContact.PhonePrimaryArray.ToArray().PhoneEncode();
                }

                var emergencyContactToEdit = database.Single<PatientEmergencyContact>(e => e.PatientId == emergencyContact.PatientId && e.Id == emergencyContact.Id);
                if (emergencyContactToEdit != null)
                {
                    emergencyContactToEdit.FirstName = emergencyContact.FirstName;
                    emergencyContactToEdit.LastName = emergencyContact.LastName;
                    emergencyContactToEdit.PrimaryPhone = emergencyContact.PrimaryPhone;
                    emergencyContactToEdit.EmailAddress = emergencyContact.EmailAddress;
                    emergencyContactToEdit.Relationship = emergencyContact.Relationship;
                    if (emergencyContact.IsPrimary)
                    {
                        if (SetPrimaryEmergencyContact(agencyId, emergencyContact.PatientId, emergencyContact.Id))
                        {
                            emergencyContactToEdit.IsPrimary = true;
                        }
                    }
                    database.Update<PatientEmergencyContact>(emergencyContactToEdit);
                    result = true;

                }
                else
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }

            return result;
        }

        public bool DeleteEmergencyContact(Guid id, Guid patientId)
        {
            bool result = false;
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                var emergencyContact = database.Single<PatientEmergencyContact>(r => r.PatientId == patientId && r.Id == id);
                if (emergencyContact != null)
                {
                    database.Delete<PatientEmergencyContact>(emergencyContact.Id);
                    result = true;
                }
            }
            return result;
        }

        public bool DeleteEmergencyContacts(Guid patientId)
        {
            bool result = false;
            if (!patientId.IsEmpty())
            {
                database.DeleteMany<PatientEmergencyContact>(pec => pec.PatientId == patientId);
                result = true;
            }
            return result;
        }

        public bool DeletePhysicianContact(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            var patientPhysician = database.Single<PatientPhysician>(p => p.PatientId == patientId && p.PhysicianId == id);
            if (patientPhysician != null)
            {
                database.Delete<PatientPhysician>(patientPhysician.Id);
                result = true;
            }
            return result;
        }

        public bool SetPrimaryEmergencyContact(Guid agencyId, Guid patientID, Guid emergencyContactId)
        {
            bool result = false;
            var patient = database.Single<Patient>(p => p.Id == patientID);
            if (patient != null)
            {
                var emergencyContacts = database.Find<PatientEmergencyContact>(p => p.PatientId == patientID);
                bool flag = false; ;
                foreach (PatientEmergencyContact contat in emergencyContacts)
                {
                    if (contat.Id == emergencyContactId)
                    {
                        contat.IsPrimary = true;
                        flag = true;
                    }
                    else
                    {
                        contat.IsPrimary = false;
                    }
                }
                if (flag)
                {
                    database.UpdateMany<PatientEmergencyContact>(emergencyContacts);
                    result = true;
                }
            }
            return result;
        }

        public bool AddMissedVisit(MissedVisit missedVisit)
        {
            if (missedVisit != null)
            {
                missedVisit.Date = DateTime.Now;
                database.Add<MissedVisit>(missedVisit);
                return true;
            }
            return false;
        }

        public MissedVisit GetMissedVisit(Guid agencyId, Guid id)
        {
            var missedVisit = database.Single<MissedVisit>(m => m.AgencyId == agencyId && m.Id == id);
            if (missedVisit != null)
            {
                var patient = database.Single<Patient>(p => p.Id == missedVisit.PatientId && p.AgencyId == agencyId);
                if (patient != null)
                {
                    missedVisit.PatientName = patient.DisplayName;
                }
                var scheduledEvent = GetSchedule(agencyId, missedVisit.EpisodeId, missedVisit.PatientId, id);
                if (scheduledEvent != null)
                {
                    missedVisit.VisitType = scheduledEvent.DisciplineTaskName;
                    var user = database.Single<User>(u => u.AgencyId == agencyId && u.Id == scheduledEvent.UserId);
                    if (user != null)
                    {
                        missedVisit.UserName = user.DisplayName;
                    }
                }
            }

            return missedVisit;
        }

        public List<MissedVisit> GetMissedVisits(Guid agencyId)
        {
            return database.Find<MissedVisit>(m => m.AgencyId == agencyId).ToList();
        }

        public bool AddOrder(PhysicianOrder order)
        {
            var result = false;
            if (order != null)
            {
                database.Add<PhysicianOrder>(order);
                result = true;
            }
            return result;
        }

        public PhysicianOrder GetOrder(Guid Id, Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patient = GetPatient<Patient>(patientId, agencyId);
            var physicianOrder = database.Single<PhysicianOrder>(o => o.PatientId == patientId && o.Id == Id);
            if (physicianOrder != null && patient != null)
            {
                physicianOrder.DisplayName = patient.DisplayName;
            }
            return physicianOrder;
        }

        public List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, int status)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            if (status > 0)
            {
                var orders = database.Find<PhysicianOrder>(o => o.AgencyId == agencyId && o.Status == status).ToList();
                orders.ForEach(o =>
                {
                    var patient = GetPatient<Patient>(o.PatientId, agencyId);
                    if (patient != null)
                    {
                        o.DisplayName = patient.DisplayName;
                    }

                    var physician = database.Single<AgencyPhysician>(p => p.Id == o.PhysicianId && p.AgencyId == agencyId);
                    if (physician != null)
                    {
                        o.PhysicianName = physician.DisplayName;
                    }
                });
                return orders;
            }
            return new List<PhysicianOrder>();
        }

        public bool UpdateOrderStatus(Guid agencyId, Guid orderId, int status, DateTime dateReceived)
        {
            Check.Argument.IsNotEmpty(orderId, "orderId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            bool result = false;
            var order = database.Single<PhysicianOrder>(o => o.Id == orderId && o.AgencyId == agencyId);
            if (order != null)
            {
                if (dateReceived != DateTime.MinValue)
                {
                    var physician = database.Single<AgencyPhysician>(p => p.Id == order.PhysicianId && p.AgencyId == agencyId);
                    if (physician != null)
                    {
                        order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                    }
                    order.ReceivedDate = dateReceived;
                }
                order.Status = status;
                if (status == (int)ScheduleStatus.OrderSentToPhysician)
                {
                    order.SentDate = DateTime.Now;
                }
                database.Update<PhysicianOrder>(order);
                result = true;

                var scheduledEvent = GetSchedule(agencyId, order.EpisodeId, order.PatientId, order.Id);
                if (scheduledEvent != null)
                {
                    scheduledEvent.Status = status.ToString();
                    UpdateEpisode(agencyId, scheduledEvent);
                }
            }
            return result;
        }

        public bool UpdateOrder(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");
            Check.Argument.IsNotEmpty(order.Id, "order.Id");
            bool result = false;
            var orderToEdit = database.Single<PhysicianOrder>(o => o.Id == order.Id && o.PatientId == order.PatientId);
            if (order != null)
            {
                orderToEdit.OrderDate = order.OrderDate;
                orderToEdit.Summary = order.Summary;
                orderToEdit.Text = order.Text;
                orderToEdit.PhysicianId = order.PhysicianId;
                orderToEdit.Status = order.Status;
                orderToEdit.SignatureDate = order.SignatureDate;
                orderToEdit.SignatureText = order.SignatureText;
                database.Update<PhysicianOrder>(orderToEdit);
                result = true;
            }
            return result;
        }

        public long GetNextOrderNumber()
        {
            var orderNumber = new OrderNumber();
            orderNumber.Created = DateTime.Now;
            database.Add<OrderNumber>(orderNumber);
            return orderNumber.Id;
        }

        public bool AddCommunicationNote(CommunicationNote communicationNote)
        {
            if (communicationNote != null)
            {
                communicationNote.Id = Guid.NewGuid();
                database.Add<CommunicationNote>(communicationNote);
                return true;
            }
            return false;
        }

        public CommunicationNote GetCommunicationNote(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Single<CommunicationNote>(c => c.PatientId == patientId && c.Id == Id);
        }

        public bool EditCommunicationNote(CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");
            Check.Argument.IsNotEmpty(communicationNote.Id, "communicationNote.Id");

            bool result = false;
            var communicationNoteToEdit = database.Single<CommunicationNote>(c => c.Id == communicationNote.Id && c.PatientId == communicationNote.PatientId);
            if (communicationNoteToEdit != null)
            {
                communicationNoteToEdit.Created = communicationNote.Created;
                communicationNoteToEdit.Text = communicationNote.Text;
                database.Update<CommunicationNote>(communicationNoteToEdit);
                result = true;
            }
            return result;
        }

        public PatientNote GetNote(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Single<PatientNote>(n => n.PatientId == patientId);
        }

        public Guid Note(Guid patientId, string patientNote)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var exstingNote = database.Single<PatientNote>(n => n.PatientId == patientId);
            PatientNote note = new PatientNote();
            if (exstingNote != null)
            {
                exstingNote.Note = patientNote;
                database.Update<PatientNote>(exstingNote);
                return exstingNote.Id;
            }
            else
            {
                try
                {
                    note.Id = Guid.NewGuid();
                    note.PatientId = patientId;
                    note.Note = patientNote;
                    database.Add<PatientNote>(note);
                }
                catch (Exception ex)
                {
                    return Guid.Empty;
                }
            }
            return note.Id;
        }

        public List<PatientEpisode> GetPatientEpisodes(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");

            return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId).ToList();
        }

        public List<PatientEpisode> GetUpcomingRecerts(Guid agencyId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId).OrderByDescending(e => e.EndDate);
            return allEpisodes.Where(e => e.EndDate >= DateTime.Now.AddDays(5) && e.EndDate <= DateTime.Now.AddDays(24)).ToList();
        }

        public List<PatientEpisode> GetPastDueRecerts(Guid agencyId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.IsRecertCompleted == false && e.EndDate < DateTime.Today).ToList();
        }

        public PatientEpisode GetCurrentEpisode(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId).OrderByDescending(e => e.StartDate);
            var episode = allEpisodes.Where(e => e.StartDate <= DateTime.Now).FirstOrDefault();
            if (episode != null)
            {
                var previousEpisode = allEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                var nextEpisode = allEpisodes.Where(e => e.StartDate > episode.StartDate).OrderBy(e => e.StartDate).FirstOrDefault();
                if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                {
                    episode.NextEpisode = nextEpisode;
                }
                if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                {
                    episode.PreviousEpisode = previousEpisode;
                }
            }
            else if (allEpisodes != null)
            {
                var futureEpisode = allEpisodes.LastOrDefault();
                if (futureEpisode != null)
                {
                    var previousEpisode = allEpisodes.Where(e => e.StartDate < futureEpisode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                    var nextEpisode = allEpisodes.Where(e => e.StartDate > futureEpisode.StartDate).OrderBy(e => e.StartDate).FirstOrDefault();
                    if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                    {
                        futureEpisode.NextEpisode = nextEpisode;
                    }
                    if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                    {
                        futureEpisode.PreviousEpisode = previousEpisode;
                    }
                }
                episode = futureEpisode;
            }
            return episode;
        }
        public DateRange GetCurrentEpisodeDate(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var epsoide = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.StartDate <= DateTime.Now && e.EndDate >= DateTime.Now).OrderBy(e => e.StartDate).FirstOrDefault();
            DateRange dateRange = new DateRange();
            if (epsoide != null)
            {
                dateRange.StartDate = (DateTime)epsoide.StartDate;
                dateRange.EndDate = (DateTime)epsoide.EndDate;
            }
            return dateRange;
        }
        public DateRange GetNextEpisode(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var epsoide = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.StartDate >= DateTime.Now).OrderBy(e => e.StartDate).FirstOrDefault();
            DateRange dateRange = new DateRange();
            if (epsoide != null)
            {
                dateRange.StartDate = (DateTime)epsoide.StartDate;
                dateRange.EndDate = (DateTime)epsoide.EndDate;
            }
            return dateRange;
        }

        public DateRange GetPreviousEpisode(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.EndDate <= DateTime.Now).OrderByDescending(e => e.EndDate).FirstOrDefault();
            DateRange dateRange = new DateRange();
            if (episode != null)
            {
                dateRange.StartDate = (DateTime)episode.StartDate;
                dateRange.EndDate = (DateTime)episode.EndDate;
            }
            return dateRange;
        }

        public PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date, string discipline)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");
            var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId).OrderByDescending(e => e.StartDate);
            var episode = allEpisodes.Where(e => e.StartDate <= date).FirstOrDefault();
            if (episode != null)
            {
                var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                if (patient != null)
                {
                    episode.DisplayName = patient.DisplayName;
                }
                var previousEpisode = allEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                var nextEpisode = allEpisodes.Where(e => e.StartDate > episode.StartDate).OrderBy(e => e.StartDate).FirstOrDefault();
                var events = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.Discipline == discipline).ToList();
                events.ForEach(evnt =>
                {
                    if (evnt.Url != "0" && evnt.Url != "")
                    {
                        var user = database.Single<User>(evnt.UserId);
                        if (user != null)
                        {
                            evnt.UserName = user.DisplayName;
                        }
                    }
                });
                episode.Schedule = events.ToXml();
                if (episode.Details.IsNotNullOrEmpty())
                {
                    episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                }
                if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                {
                    episode.NextEpisode = nextEpisode;
                    if (episode.NextEpisode.Details.IsNotNullOrEmpty())
                    {
                        episode.NextEpisode.Detail = episode.NextEpisode.Details.ToObject<EpisodeDetail>();
                    }
                }
                if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                {
                    episode.PreviousEpisode = previousEpisode;
                    if (episode.PreviousEpisode.Details.IsNotNullOrEmpty())
                    {
                        episode.PreviousEpisode.Detail = episode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
                    }
                }
            }
            else if (allEpisodes != null)
            {
                var futureEpisode = allEpisodes.LastOrDefault();
                if (futureEpisode != null)
                {
                    var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                    if (patient != null)
                    {
                        futureEpisode.DisplayName = patient.DisplayName;
                    }
                    var previousEpisode = allEpisodes.Where(e => e.StartDate < futureEpisode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                    var nextEpisode = allEpisodes.Where(e => e.StartDate > futureEpisode.StartDate).OrderBy(e => e.StartDate).FirstOrDefault();
                    var events = futureEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.Discipline == discipline).ToList();
                    events.ForEach(evnt =>
                    {
                        if (evnt.Url != "0" && evnt.Url != "")
                        {
                            var user = database.Single<User>(evnt.UserId);
                            if (user != null)
                            {
                                evnt.UserName = user.DisplayName;
                            }
                        }
                    });
                    futureEpisode.Schedule = events.ToXml();
                    if (futureEpisode.Details.IsNotNullOrEmpty())
                    {
                        futureEpisode.Detail = futureEpisode.Details.ToObject<EpisodeDetail>();
                    }
                    if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                    {
                        futureEpisode.NextEpisode = nextEpisode;
                        if (futureEpisode.NextEpisode.Details.IsNotNullOrEmpty())
                        {
                            futureEpisode.NextEpisode.Detail = futureEpisode.NextEpisode.Details.ToObject<EpisodeDetail>();
                        }
                    }
                    if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                    {
                        futureEpisode.PreviousEpisode = previousEpisode;
                        if (futureEpisode.PreviousEpisode.Details.IsNotNullOrEmpty())
                        {
                            futureEpisode.PreviousEpisode.Detail = futureEpisode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
                        }
                    }
                    episode = futureEpisode;
                }
            }
            return episode;
        }

        public List<ScheduleEvent> GetScheduledEvents(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patientEvents = new List<ScheduleEvent>();
            var patientEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId);
            patientEpisodes.ForEach(patientEpisode =>
            {
                if (patientEpisode.Details.IsNotNullOrEmpty())
                {
                    patientEpisode.Detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                }
                if (patientEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                    schedule.ForEach(s =>
                    {
                        if (!s.UserId.IsEmpty())
                        {
                            var user = database.Single<User>(s.UserId);
                            if (user != null)
                            {
                                s.UserName = user.DisplayName;
                            }
                        }
                        s.EpisodeNotes = patientEpisode.Detail.Comments;

                        if (s.IsMissedVisit)
                        {
                            var missedVisit = GetMissedVisit(agencyId, s.EventId);

                            if (missedVisit != null)
                            {
                                s.MissedVisitComments = missedVisit.ToString();
                            }
                        }
                    });
                    patientEvents.AddRange(schedule);
                }
            });
            return patientEvents;
        }

        public List<ScheduleEvent> GetEpisodeSchedules(Guid agencyId, Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var patientEpisode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId).SingleOrDefault();
            var patientEvents = new List<ScheduleEvent>();
            if (patientEpisode != null)
            {
                patientEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
            }
            return patientEvents;
        }

        public PatientEpisode GetEpisodeById(Guid agencyId, Guid episodeId, Guid patientId)
        {
            return database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
        }

        public PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
            var patientEpisode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
            if (patientEpisode != null)
            {
                if (patient != null)
                {
                    patientEpisode.DisplayName = patient.DisplayName;
                }
                var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId).OrderBy(e => e.StartDate);
                if (allEpisodes != null)
                {
                    var previousEpisode = allEpisodes.Where(e => e.StartDate < patientEpisode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                    var nextEpisode = allEpisodes.Where(e => e.StartDate > patientEpisode.StartDate).OrderBy(e => e.StartDate).FirstOrDefault();
                    if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                    {
                        patientEpisode.NextEpisode = nextEpisode;
                        if (patientEpisode.NextEpisode.Details.IsNotNullOrEmpty())
                        {
                            patientEpisode.NextEpisode.Detail = patientEpisode.NextEpisode.Details.ToObject<EpisodeDetail>();
                        }
                    }
                    if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                    {
                        patientEpisode.PreviousEpisode = previousEpisode;
                        if (patientEpisode.PreviousEpisode.Details.IsNotNullOrEmpty())
                        {
                            patientEpisode.PreviousEpisode.Detail = patientEpisode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
                        }
                    }
                }
                var schedule = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                schedule.ForEach(s =>
                {
                    if (!s.UserId.IsEmpty())
                    {
                        var user = database.Single<User>(s.UserId);
                        if (user != null)
                        {
                            s.UserName = user.DisplayName;
                        }
                    }
                });
                patientEpisode.Schedule = schedule.ToXml();
                if (patientEpisode.Details.IsNotNullOrEmpty())
                {
                    patientEpisode.Detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                }
            }

            return patientEpisode;
        }

        public PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId, string discipline)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotNull(discipline, "discipline");
            PatientEpisode episode = null;
            try
            {
                var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId).OrderBy(e => e.StartDate);
                if (allEpisodes != null)
                {
                    episode = allEpisodes.Where(e => e.Id == episodeId).SingleOrDefault();
                    if (episode != null)
                    {
                        var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                        if (patient != null)
                        {
                            episode.DisplayName = patient.DisplayName;
                        }
                        var previousEpisode = allEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                        var nextEpisode = allEpisodes.Where(e => e.StartDate > episode.StartDate).OrderBy(e => e.StartDate).FirstOrDefault();
                        List<ScheduleEvent> events = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.Discipline == discipline).ToList();
                        events.ForEach(evnt =>
                        {
                            if (!evnt.UserId.IsEmpty() && evnt.Url != "0")
                            {
                                var user = database.Single<User>(evnt.UserId);
                                if (user != null)
                                {
                                    evnt.UserName = user.DisplayName;
                                }
                            }
                        });
                        episode.Schedule = events.ToXml();
                        if (episode.Details.IsNotNullOrEmpty())
                        {
                            episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                        }

                        if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                        {
                            episode.NextEpisode = nextEpisode;
                            if (episode.NextEpisode.Details.IsNotNullOrEmpty())
                            {
                                episode.NextEpisode.Detail = episode.NextEpisode.Details.ToObject<EpisodeDetail>();
                            }
                        }

                        if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                        {
                            episode.PreviousEpisode = previousEpisode;
                            if (episode.PreviousEpisode.Details.IsNotNullOrEmpty())
                            {
                                episode.PreviousEpisode.Detail = episode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return episode;

        }

        public bool AddEpisode(PatientEpisode patientEpisode)
        {
            var result = false;
            if (patientEpisode != null)
            {
                patientEpisode.Details = patientEpisode.Detail.ToXml();
                patientEpisode.Created = DateTime.Now;
                patientEpisode.Modified = DateTime.Now;

                database.Add<PatientEpisode>(patientEpisode);
                result = true;
            }
            return result;
        }

        public bool UpdateEpisode(Guid agencyId, PatientEpisode patientEpisode)
        {
            var result = false;
            var episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientEpisode.PatientId && e.Id == patientEpisode.Id).SingleOrDefault();

            if (episode != null)
            {
                episode.IsActive = patientEpisode.IsActive;
                episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                patientEpisode.Detail.Assets.AddRange(episode.Detail.Assets);
                episode.Details = patientEpisode.Detail.ToXml();
                episode.Modified = DateTime.Now;
                episode.AssessmentId = patientEpisode.AssessmentId;
                episode.AssessmentType = patientEpisode.AssessmentType;
                database.Update<PatientEpisode>(episode);
                result = true;
            }
            return result;
        }

        public bool SetRecertFlag(Guid agencyId, Guid episodeId, Guid patientId, bool isRecertComplete)
        {
            var result = false;
            var episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId).SingleOrDefault();

            if (episode != null)
            {
                episode.IsRecertCompleted = isRecertComplete;
                episode.Modified = DateTime.Now;
                database.Update<PatientEpisode>(episode);
                result = true;
            }
            return result;
        }

        public void AddNewScheduleEvent(Guid agencyId, Guid patientId, ScheduleEvent newScheduledEvent)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(newScheduledEvent, "newScheduledEvent");

            var date = DateTime.Parse(newScheduledEvent.EventDate);
            var episodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId);
            var patientEpisode = episodes.Where(e => date >= e.StartDate && date <= e.EndDate).FirstOrDefault();
            if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
            {
                var events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                events.ForEach(e =>
                {
                    if (e.EventId == Guid.Empty)
                    {
                        events.Remove(e);
                    }
                });

                newScheduledEvent.EpisodeId = patientEpisode.Id;
                events.Add(newScheduledEvent);
                patientEpisode.Modified = DateTime.Now;
                patientEpisode.Schedule = events.ToXml();
                database.Update<PatientEpisode>(patientEpisode);
            }
        }

        public void AddNewUserEvent(Guid agencyId, Guid patientId, UserEvent newUserEvent)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(newUserEvent, "newUserEvent");

            var employeeEpisode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == newUserEvent.UserId);
            if (employeeEpisode != null)
            {
                var employeeEvents = employeeEpisode.Visits.ToObject<List<UserEvent>>();
                employeeEvents.Add(newUserEvent);
                employeeEpisode.Visits = employeeEvents.ToXml();
                database.Update<UserSchedule>(employeeEpisode);
            }
            else
            {
                var userSchedule = new UserSchedule();
                userSchedule.AgencyId = agencyId;
                userSchedule.Id = Guid.NewGuid();
                userSchedule.PatientId = patientId;
                userSchedule.UserId = newUserEvent.UserId;

                var newEventList = new List<UserEvent>();
                newEventList.Add(newUserEvent);
                userSchedule.Visits = newEventList.ToXml();
                userSchedule.Created = DateTime.Now;
                userSchedule.Modified = DateTime.Now;
                database.Add<UserSchedule>(userSchedule);
            }
        }

        public bool UpdateEpisode(Guid agencyId, ScheduleEvent scheduleEvent)
        {
            bool result = false;

            if (scheduleEvent != null)
            {
                var patientEpisode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.Id == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId);
                if (patientEpisode != null && !string.IsNullOrEmpty(patientEpisode.Schedule))
                {
                    List<ScheduleEvent> events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                    events.ForEach(e =>
                    {
                        if (e.EventId == scheduleEvent.EventId)
                        {
                            e.Discipline = scheduleEvent.Discipline;
                            e.UserId = scheduleEvent.UserId;
                            e.EventDate = scheduleEvent.EventDate;
                            e.Status = scheduleEvent.Status;
                            e.Discipline = scheduleEvent.Discipline;
                            e.DisciplineTask = scheduleEvent.DisciplineTask;
                            e.AssociatedMileage = scheduleEvent.AssociatedMileage;
                            e.ReturnReason = scheduleEvent.ReturnReason;
                            e.Comments = scheduleEvent.Comments;
                            e.IsBillable = scheduleEvent.IsBillable;
                            e.IsMissedVisit = scheduleEvent.IsMissedVisit;
                            e.Surcharge = scheduleEvent.Surcharge;
                            e.TargetDate = scheduleEvent.TargetDate;
                            e.TimeIn = scheduleEvent.TimeIn;
                            e.TimeOut = scheduleEvent.TimeOut;
                        }
                    });
                    patientEpisode.Modified = DateTime.Now;
                    patientEpisode.Schedule = events.ToXml();
                    database.Update<PatientEpisode>(patientEpisode);
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateEpisode(Guid agencyId, Guid episodeId, Guid patientId, List<ScheduleEvent> newEvents)
        {
            Check.Argument.IsNotNull(agencyId, "agencyId");
            Check.Argument.IsNotNull(newEvents, "newEvents");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            bool result = false;

            var patientEpisode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.Id == episodeId && e.PatientId == patientId);
            if (patientEpisode != null && !string.IsNullOrEmpty(patientEpisode.Schedule))
            {
                IWebConfiguration webConfig = Container.Resolve<IWebConfiguration>();

                using (SharedDbConnectionScope sharedConnection = new SharedDbConnectionScope(webConfig.ConnectionStrings("AgencyManagementConnectionString"), "MySql.Data.MySqlClient"))
                {
                    using (TransactionScope transaction = new TransactionScope())
                    {
                        List<ScheduleEvent> events = Convert.ToString(patientEpisode.Schedule).ToObject<List<ScheduleEvent>>();
                        events.ForEach(e =>
                        {
                            if (e.EventId == Guid.Empty)
                            {
                                events.Remove(e);
                            }
                            else
                            {
                                e.EpisodeId = episodeId;
                                e.PatientId = patientId;
                            }
                        });

                        newEvents.ForEach(ev =>
                        {
                            ev.EpisodeId = episodeId;
                            ev.PatientId = patientId;
                            events.Add(ev);
                        });

                        patientEpisode.Modified = DateTime.Now;
                        patientEpisode.Schedule = events.ToXml();
                        database.Update<PatientEpisode>(patientEpisode);
                        newEvents.ForEach(ev =>
                        {
                            var userEvent = new UserEvent { DisciplineTask = ev.DisciplineTask, EventDate = ev.EventDate, EventId = ev.EventId, UserId = ev.UserId, PatientId = patientId, Status = ev.Status, EpisodeId = ev.EpisodeId, Discipline = ev.Discipline };
                            var userEpisode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == ev.UserId);

                            if (userEpisode != null)
                            {
                                var employeeEvents = userEpisode.Visits.ToObject<List<UserEvent>>();
                                employeeEvents.Add(userEvent);
                                userEpisode.Visits = employeeEvents.ToXml();
                                database.Update<UserSchedule>(userEpisode);
                            }
                            else
                            {
                                var userSchedule = new UserSchedule();
                                userSchedule.Id = Guid.NewGuid();
                                userSchedule.AgencyId = agencyId;
                                userSchedule.PatientId = patientId;
                                userSchedule.UserId = ev.UserId;
                                var newEventList = new List<UserEvent>();
                                newEventList.Add(userEvent);
                                userSchedule.Visits = newEventList.ToXml();
                                userSchedule.Created = DateTime.Now;
                                userSchedule.Modified = DateTime.Now;
                                database.Add<UserSchedule>(userSchedule);
                            }
                        });

                        result = true;
                        transaction.Complete();
                    }
                }
            }
            return result;
        }

        public bool Reassign(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid userId)
        {
            bool result = false;
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(userId, "employeeId");

            var patientEpisode = database.Single<PatientEpisode>(pe => pe.AgencyId == agencyId && pe.PatientId == patientId && pe.Id == episodeId);
            if (patientEpisode != null && patientEpisode.Schedule.IsNotNullOrEmpty())
            {
                List<ScheduleEvent> events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                events.ForEach(ev =>
                {
                    if (ev.EventId == eventId)
                    {
                        ev.UserId = userId;

                    }
                });
                patientEpisode.Schedule = events.ToXml();
                database.Update<PatientEpisode>(patientEpisode);
                result = true;
            }
            return result;
        }

        public ScheduleEvent GetSchedule(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotNull(eventId, "eventId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            ScheduleEvent scheduleEvent = null;
            var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);

            if (episode != null)
            {
                scheduleEvent = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId == eventId).FirstOrDefault();
                if (scheduleEvent != null)
                {
                    var user = database.Single<User>(scheduleEvent.UserId);
                    if (user != null)
                    {
                        scheduleEvent.UserName = user.DisplayName;
                    }
                    scheduleEvent.StartDate = episode.StartDate;
                    scheduleEvent.EndDate = episode.EndDate;
                    var patient = database.Single<Patient>(p => p.Id == patientId);
                    if (patient != null)
                    {
                        scheduleEvent.PatientName = patient.DisplayName;
                    }

                    if (scheduleEvent.IsMissedVisit)
                    {
                        scheduleEvent.StatusName = "Missed Visit";
                    }
                    else
                    {
                        ScheduleStatus status = (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status);
                        if (status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue && DateTime.Parse(scheduleEvent.EventDate) < DateTime.Today)
                        {
                            scheduleEvent.StatusName = ScheduleStatus.CommonNotStarted.GetDescription();
                        }
                        else
                        {
                            scheduleEvent.StatusName = status.GetDescription();
                        }
                    }
                }
            }

            return scheduleEvent;
        }

        public bool DeleteEpisode(Guid agencyId, Patient patient, out PatientEpisode episodeDeleted)
        {
            Check.Argument.IsNotNull(patient, "patient");

            var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patient.Id && e.StartDate <= DateTime.Now && e.EndDate > DateTime.Now);
            episodeDeleted = episode;
            if (episode != null)
            {
                database.Delete<PatientEpisode>(episode);
            }
            var userSchedule = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patient.Id && e.UserId == patient.UserId);
            if (userSchedule != null)
            {
                database.Delete<UserSchedule>(userSchedule);
            }
            return true;
        }

        public bool DeleteScheduleEvent(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            bool result = false;
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");

            var patientEpsoide = database.Single<PatientEpisode>(pe => pe.AgencyId == agencyId && pe.PatientId == patientId && pe.Id == episodeId);
            if (patientEpsoide != null)
            {
                List<ScheduleEvent> events = patientEpsoide.Schedule.ToString().ToObject<List<ScheduleEvent>>();
                events.ForEach(ev =>
                {
                    if (ev.EventId == eventId)
                    {
                        events.Remove(ev);
                    }
                });
                patientEpsoide.Schedule = XElement.Parse(Serializer.Serialize(events)).ToString();
                database.Update<PatientEpisode>(patientEpsoide);
                result = true;
            }
            return result;
        }

        public MedicationProfileHistory GetLatestMedicationProfileHistory(Guid PatientId, Guid AgencyId, bool isSigned)
        {
            var medProfiles = database.Find<MedicationProfileHistory>(m => m.AgencyId == AgencyId && m.PatientId == PatientId && m.IsSigned == isSigned);
            if (medProfiles != null)
            {
                return medProfiles.OrderByDescending(m => m.SignedDate).FirstOrDefault();
            }
            return null;
        }

        public MedicationProfileHistory GetLatestMedicationProfileHistory(Guid PatientId, Guid AgencyId)
        {
            var medProfiles = database.Find<MedicationProfileHistory>(m => m.AgencyId == AgencyId && m.PatientId == PatientId);
            if (medProfiles != null)
            {
                return medProfiles.OrderByDescending(m => m.SignedDate).FirstOrDefault();
            }
            return null;
        }

        public MedicationProfileHistory GetMedicationProfileHistory(Guid Id, Guid AgencyId)
        {
            return database.Single<MedicationProfileHistory>(m => m.AgencyId == AgencyId && m.Id == Id);
        }

        public bool AddNewMedicationProfile(MedicationProfile medication)
        {
            try
            {
                database.Add<MedicationProfile>(medication);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AddNewMedicationHistory(MedicationProfileHistory medicationHistory)
        {
            try
            {
                database.Add<MedicationProfileHistory>(medicationHistory);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public MedicationProfile InsertMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType)
        {
            var medicationProfile = this.GetMedicationProfile(Id, agencyId);
            if (medicationProfile != null)
            {
                medication.Id = Guid.NewGuid();
                medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                medication.MedicationType = new MedicationType { Value = MedicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), MedicationType, true)).GetDescription() };
                if (medicationProfile.Medication.IsNullOrEmpty())
                {
                    var newList = new List<Medication>() { medication };
                    medicationProfile.Medication = newList.ToXml();
                    database.Update<MedicationProfile>(medicationProfile);
                }
                else
                {
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    existingList.Add(medication);
                    medicationProfile.Medication = existingList.ToXml();
                    database.Update<MedicationProfile>(medicationProfile);
                }
            }
            return medicationProfile;
        }

        public bool UpdateMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType)
        {
            var medicationProfile = this.GetMedicationProfile(Id, agencyId);
            string medicationString = string.Empty;
            if (medicationProfile != null)
            {
                medicationString = medicationProfile.Medication;
            }
            if (medicationString.IsNotNullOrEmpty() && medicationString != "")
            {
                var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == medication.Id))
                {
                    var med = existingList.Single(m => m.Id == medication.Id);
                    if (med.StartDate != medication.StartDate || med.Route != medication.Route || med.Frequency != medication.Frequency || med.MedicationDosage != medication.MedicationDosage)
                    {
                        medication.Id = Guid.NewGuid();
                        medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                        medication.MedicationType = new MedicationType { Value = "C", Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), MedicationType, true)).GetDescription() };
                        existingList.Add(medication);
                        med.MedicationCategory = "DC";
                        med.DCDate = DateTime.Now;
                        medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                        database.Update<MedicationProfile>(medicationProfile);
                        return true;
                    }
                    else
                    {
                        med.IsLongStanding = medication.IsLongStanding;
                        med.MedicationType = new MedicationType { Value = MedicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), MedicationType, true)).GetDescription() }; ;
                        med.Classification = medication.Classification;
                        if (med.MedicationCategory == "DC")
                        {
                            med.DCDate = medication.DCDate;
                        }
                        medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                        database.Update<MedicationProfile>(medicationProfile);
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateMedicationForDischarge(Guid MedId, Guid agencyId, Guid Id, DateTime DischargeDate)
        {
            var medicationProfile = this.GetMedicationProfile(MedId, agencyId);
            string medicationString = "";
            if (medicationProfile != null)
            {
                medicationString = medicationProfile.Medication;
            }
            bool result = false;
            if (medicationString.IsNotNullOrEmpty() && medicationString != "")
            {
                var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == Id))
                {
                    var med = existingList.Single(m => m.Id == Id);
                    med.DCDate = DischargeDate;
                    med.LastChangedDate = DateTime.Now;
                    med.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                    medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                    database.Update<MedicationProfile>(medicationProfile);
                    result = true;
                }
            }
            return result;
        }

        public MedicationProfile DeleteMedication(Guid MedId, Guid agencyId, Medication medication)
        {
            var medicationProfile = this.GetMedicationProfile(MedId, agencyId);
            string medicationString = "";
            if (medicationProfile != null)
            {
                medicationString = medicationProfile.Medication;
            }

            if (medicationProfile != null)
            {
                medicationString = medicationProfile.Medication;
            }
            if (!medicationString.IsNotNullOrEmpty())
            {
                return medicationProfile;
            }
            else
            {
                var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == medication.Id))
                {
                    existingList.RemoveAll(m => m.Id == medication.Id);
                    medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                    database.Update<MedicationProfile>(medicationProfile);
                }
            }
            return medicationProfile;
        }

        public bool UpdateMedicationProfileHistory(MedicationProfileHistory medicationProfileHistory)
        {
            var medication = this.GetMedicationProfileHistory(medicationProfileHistory.Id, medicationProfileHistory.AgencyId);
            if (medication != null)
            {
                medication.PharmacyName = medicationProfileHistory.PharmacyName;
                medication.PharmacyPhone = medicationProfileHistory.PharmacyPhone;
                medication.UserId = medicationProfileHistory.UserId;
                medication.SignedDate = medicationProfileHistory.SignedDate;
                medication.IsSigned = medicationProfileHistory.IsSigned;
                database.Update<MedicationProfileHistory>(medication);
                return true;
            }
            return false;
        }

        public bool DeleteMedicationProfileHistory(Guid Id, Guid agencyId)
        {
            var medication = this.GetMedicationProfileHistory(Id, agencyId);
            if (medication != null)
            {
                database.Delete<MedicationProfileHistory>(medication.Id);
            }
            return true;
        }

        public bool DeleteMedicationProfile(Guid Id, Guid agencyId)
        {
            var medication = this.GetMedicationProfile(Id, agencyId);
            if (medication != null)
            {
                database.Delete<MedicationProfile>(medication.Id);
            }
            return true;
        }

        public IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId, Guid agencyId)
        {
            return database.Find<MedicationProfileHistory>(m => m.PatientId == patientId && m.AgencyId == agencyId);
        }

        public MedicationProfile GetMedicationProfileByPatient(Guid PatientId, Guid AgencyId)
        {
            var medProfiles = database.Find<MedicationProfile>(m => m.AgencyId == AgencyId && m.PatientId == PatientId);
            if (medProfiles != null)
            {
                return medProfiles.FirstOrDefault();
            }
            return null;
        }

        public MedicationProfile GetMedicationProfile(Guid Id, Guid AgencyId)
        {
            return database.Single<MedicationProfile>(m => m.AgencyId == AgencyId && m.Id == Id); ;
        }

        public bool SaveMedicationProfile(MedicationProfile medicationProfile)
        {
            var medication = this.GetMedicationProfile(medicationProfile.Id, medicationProfile.AgencyId);
            if (medication != null)
            {
                medication.PharmacyName = medicationProfile.PharmacyName;
                medication.PharmacyPhone = medicationProfile.PharmacyPhone;
                medication.Modified = DateTime.Now;
                database.Update<MedicationProfile>(medication);
                return true;
            }
            return false;
        }

        public bool AddVisitNote(PatientVisitNote patientVisitNote)
        {
            try
            {
                database.Add<PatientVisitNote>(patientVisitNote);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateVisitNote(PatientVisitNote patientVisitNote)
        {
            var result = false;
            var visitNote = database.Single<PatientVisitNote>(p => p.Id == patientVisitNote.Id && p.PatientId == patientVisitNote.PatientId && p.EpisodeId == patientVisitNote.EpisodeId);
            if (visitNote != null)
            {
                database.Update<PatientVisitNote>(patientVisitNote);
                result = true;
            }
            else
            {
                database.Add<PatientVisitNote>(patientVisitNote);
                result = true;
            }
            return result;
        }

        public PatientVisitNote GetVisitNote(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            return database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == eventId && p.PatientId == patientId && p.EpisodeId == episodeId);
        }

        public PatientVisitNote GetHHAPlanOfCareVisitNote(Guid episodeId, Guid patientId)
        {
            var noteType = DisciplineTasks.HHAideCarePlan.ToString();
            return database.Find<PatientVisitNote>(p => p.EpisodeId == episodeId && p.PatientId == patientId && p.NoteType == noteType).LastOrDefault();
        }

        public IList<Patient> GetCaseManagerPatients(Guid userId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Find<Patient>(p => p.AgencyId == agencyId && p.CaseManagerId == userId);
        }

        #endregion
    }
}
