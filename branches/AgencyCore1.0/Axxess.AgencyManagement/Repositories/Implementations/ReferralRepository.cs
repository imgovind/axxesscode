﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;


    using SubSonic.Repository;

    using Domain;
    using Axxess.AgencyManagement.Enums;

    public class ReferralRepository : IReferralRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public ReferralRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IReferralRepository Members

        public bool Add(Referral referral)
        {
            var result = false;
            if (referral != null)
            {
                referral.Id = Guid.NewGuid();
                if (referral.PhoneHomeArray != null && referral.PhoneHomeArray.Count >= 2)
                {
                    referral.PhoneHome = referral.PhoneHomeArray.ToArray().PhoneEncode();
                }
                if (referral.ServicesRequiredCollection != null && referral.ServicesRequiredCollection.Count > 0)
                {
                    referral.ServicesRequired = referral.ServicesRequiredCollection.ToArray().AddColons();
                }
                if (referral.DMECollection != null && referral.DMECollection.Count > 0)
                {
                    referral.DME = referral.DMECollection.ToArray().AddColons();
                }
                if (referral.AgencyPhysicians != null && referral.AgencyPhysicians.Count > 0)
                {
                    var physicianList = new List<Physician>();

                    int i = 0;
                    foreach (var guid in referral.AgencyPhysicians)
                    {
                        if (!guid.IsEmpty())
                        {
                            physicianList.Add(new Physician { Id = guid, IsPrimary = (i == 0) });
                            i++;
                        }
                    }
                    referral.Physicians = physicianList.ToXml();
                }
                referral.Status = (byte)ReferralStatus.Pending;
                referral.Created = DateTime.Now;
                referral.Modified = DateTime.Now;
                database.Add<Referral>(referral);
                result = true;
            }

            return result;
        }

        public IEnumerable<Referral> GetAll(Guid agencyId, ReferralStatus status)
        {
            return database.Find<Referral>(r => r.AgencyId == agencyId && r.Status == (int) status);
        }

        public Referral Get(Guid agencyId, Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return database.Single<Referral>(r => r.AgencyId == agencyId && r.Id == id);
        }

        public void SetStatus(Guid agencyId, Guid id, ReferralStatus status)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            var referral = Get(agencyId, id);

            if (referral != null)
            {
                referral.Status = (int)status;
                database.Update<Referral>(referral);
            }
        }

        public bool Edit(Referral referral)
        {
            bool result = false;
            if (referral != null)
            {
                if (referral.ServicesRequiredCollection != null && referral.ServicesRequiredCollection.Count > 0)
                {
                    referral.ServicesRequired = referral.ServicesRequiredCollection.ToArray().AddColons();
                }
                if (referral.DMECollection != null && referral.DMECollection.Count > 0)
                {
                    referral.DME = referral.DMECollection.ToArray().AddColons();
                }
                if (referral.PhoneHomeArray != null && referral.PhoneHomeArray.Count > 0)
                {
                    referral.PhoneHome = referral.PhoneHomeArray.ToArray().PhoneEncode();
                }

                Referral referralToEdit = database.Single<Referral>(r => r.AgencyId == referral.AgencyId && r.Id == referral.Id);
                if (referralToEdit != null)
                {
                    referralToEdit.AdmissionSource = referral.AdmissionSource;
                    referralToEdit.ReferrerPhysician = referral.ReferrerPhysician;
                    referralToEdit.ReferralDate = referral.ReferralDate;
                    referralToEdit.OtherReferralSource = referral.OtherReferralSource;
                    referralToEdit.InternalReferral = referral.InternalReferral;
                    referralToEdit.FirstName = referral.FirstName;
                    referralToEdit.LastName = referral.LastName;
                    referralToEdit.MedicaidNumber = referral.MedicaidNumber;
                    referralToEdit.MedicareNumber = referral.MedicareNumber;
                    referralToEdit.SSN = referral.SSN;
                    referralToEdit.DOB = referral.DOB;
                    referralToEdit.Gender = referral.Gender;
                    referralToEdit.PhoneHome = referral.PhoneHome;
                    referralToEdit.EmailAddress = referral.EmailAddress;
                    referralToEdit.AddressLine1 = referral.AddressLine1;
                    referralToEdit.AddressLine2 = referral.AddressLine2;
                    referralToEdit.AddressCity = referral.AddressCity;
                    referralToEdit.AddressStateCode = referral.AddressStateCode;
                    referralToEdit.AddressZipCode = referral.AddressZipCode;
                    referralToEdit.ServicesRequired = referral.ServicesRequired;
                    referralToEdit.DME = referral.DME;
                    referralToEdit.OtherDME = referral.OtherDME;
                    referralToEdit.UserId = referral.UserId;
                    referralToEdit.Comments = referral.Comments;
                    if (referral.AgencyPhysicians != null && referral.AgencyPhysicians.Count > 0)
                    {
                        var physicianList = new List<Physician>();

                        int i = 0;
                        foreach (var guid in referral.AgencyPhysicians)
                        {
                            if (!guid.IsEmpty())
                            {
                                physicianList.Add(new Physician { Id = guid, IsPrimary = (i == 0) });
                                i++;
                            }
                        }
                        referralToEdit.Physicians = physicianList.ToXml();
                    }
                    referralToEdit.Modified = DateTime.Now;
                    database.Update<Referral>(referralToEdit);
                    result = true;
                }
            }

            return result;
        }

        public bool Delete(Guid agencyId, Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            bool result = false;
            Referral referral = database.Single<Referral>(r => r.AgencyId == agencyId && r.Id == id);
            if (referral != null)
            {
                database.Delete<Referral>(referral.Id);
                result = true;
            }
            return result;
        }

        #endregion
    }
}
