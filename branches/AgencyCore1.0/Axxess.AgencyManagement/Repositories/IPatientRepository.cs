﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;

    using Enums;
    using Domain;

    public interface IPatientRepository
    {
        bool Add(Patient patient);
        bool Edit(Patient patient);
        bool Delete(Guid agencyId, Guid id);
        bool SetStatus(Guid patient, PatientStatus status);
        bool AddEmergencyContact(PatientEmergencyContact emergencyContact);
        Patient Get(Guid Id, Guid agencyId);
        T GetPatient <T>(Guid patientId, Guid agencyId);
        IList<PatientEmergencyContact> GetEmergencyContacts(Guid patientId);
        IList<Patient> GetAllByAgencyId(Guid agencyId);
        IList<Patient> GetPendingByAgencyId(Guid agencyId);
        IList<Patient> Find(int statusId, Guid agencyId);
        IList<Patient> Find(int statusId, Guid branchCode, Guid agencyId);
        IList<Patient> GetAllByBirthDayRange(DateTime startDate, DateTime endDate, Guid agencyId);
        PatientEmergencyContact GetEmergencyContact(Guid patientId, Guid emergencyContactID);
        bool EditEmergencyContact(Guid agencyId, PatientEmergencyContact emergencyContact);
        bool DeleteEmergencyContacts(Guid patientId);
        bool DeleteEmergencyContact(Guid id, Guid patientId);
        bool DeletePhysicianContact(Guid id, Guid patientId);
        bool SetPrimaryEmergencyContact(Guid agencyId, Guid patientID, Guid emergencyContactId);
        bool AddOrder(PhysicianOrder order);
        bool AddCommunicationNote(CommunicationNote communicationNote);
        CommunicationNote GetCommunicationNote(Guid Id, Guid patientId);
        bool EditCommunicationNote(CommunicationNote communicationNote);
        PatientNote GetNote(Guid patientId);
        Guid Note(Guid patientId, string patientNote);

        PatientEpisode GetCurrentEpisode(Guid agencyId,Guid patientId);
        long GetNextOrderNumber();

        DateRange GetCurrentEpisodeDate(Guid agencyId, Guid patientId);
        DateRange GetNextEpisode(Guid agencyId, Guid patientId);
        DateRange GetPreviousEpisode(Guid agencyId, Guid patientId);
        List<ScheduleEvent> GetScheduledEvents(Guid agencyId, Guid patientId);
        PatientEpisode GetEpisodeById(Guid agencyId, Guid episodeId, Guid patientId);
        List<ScheduleEvent> GetEpisodeSchedules(Guid agencyId, Guid patientId, Guid episodeId);
        PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date, string discipline);
        PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId, string discipline);
        PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId);
        List<PatientEpisode> GetPatientEpisodes(Guid agencyId, Guid patientId);

        List<PatientEpisode> GetPastDueRecerts(Guid agencyId);
        List<PatientEpisode> GetUpcomingRecerts(Guid agencyId);

        List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, int status);
        PhysicianOrder GetOrder(Guid Id, Guid patientId, Guid agencyId);
        bool UpdateOrder(PhysicianOrder order);
        bool UpdateOrderStatus(Guid agencyId, Guid orderId, int status, DateTime dateReceived);

        bool AddEpisode(PatientEpisode patientEpisode);

        void AddNewUserEvent(Guid agencyId, Guid patientId, UserEvent newUserEvent);
        void AddNewScheduleEvent(Guid agencyId, Guid patientId, ScheduleEvent newEvent);

        bool UpdateEpisode(Guid agencyId, ScheduleEvent editEvent);
        bool UpdateEpisode(Guid agencyId, Guid episodeId, Guid patientId, List<ScheduleEvent> newEvents);
        bool Reassign(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
        ScheduleEvent GetSchedule(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        bool DeleteScheduleEvent(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);

        bool UpdateEpisode(Guid agencyId, PatientEpisode episode);
        bool SetRecertFlag(Guid agencyId, Guid episodeId, Guid patientId, bool isRecertComplete);
        MedicationProfileHistory GetMedicationProfileHistory(Guid Id, Guid AgencyId);
        MedicationProfileHistory GetLatestMedicationProfileHistory(Guid patientId, Guid agencyId, bool isSigned);
        MedicationProfileHistory GetLatestMedicationProfileHistory(Guid PatientId, Guid AgencyId);
        bool AddNewMedicationHistory(MedicationProfileHistory medicationHistory);
        bool AddNewMedicationProfile(MedicationProfile medication);

        MedicationProfile InsertMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType);
        bool UpdateMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType);
        bool UpdateMedicationForDischarge(Guid MedId, Guid agencyId, Guid Id, DateTime DischargeDate);
        MedicationProfile DeleteMedication(Guid MedId, Guid agencyId, Medication medication);
        bool UpdateMedicationProfileHistory(MedicationProfileHistory medicationProfile);
        bool DeleteMedicationProfileHistory(Guid Id, Guid agencyId);
        IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId, Guid agencyId);

        bool DeleteMedicationProfile(Guid Id, Guid agencyId);

        bool DeleteEpisode(Guid agencyId, Patient patient, out PatientEpisode episodeDeleted);
        MedicationProfile GetMedicationProfileByPatient(Guid PatientId, Guid AgencyId);
        MedicationProfile GetMedicationProfile(Guid Id, Guid AgencyId);
        bool SaveMedicationProfile(MedicationProfile medicationProfile);

        bool AddVisitNote(PatientVisitNote patientVisitNote);
        PatientVisitNote GetVisitNote(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        PatientVisitNote GetHHAPlanOfCareVisitNote(Guid episodeId, Guid patientId);
        bool UpdateVisitNote(PatientVisitNote patientVisitNote);
        IList<Patient> GetCaseManagerPatients(Guid userId, Guid agencyId);
        bool AddMissedVisit(MissedVisit missedVisit);
        MissedVisit GetMissedVisit(Guid agencyId, Guid id);
        List<MissedVisit> GetMissedVisits(Guid agencyId);

        bool AddPhoto(Guid patientId, Guid agencyId, Guid assetId);
        bool AdmitPatient(PendingPatient patient);
        bool NonAdmitPatient(PendingPatient pending);

    }
}
