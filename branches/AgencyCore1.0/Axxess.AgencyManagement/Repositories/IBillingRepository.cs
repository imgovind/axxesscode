﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.AgencyManagement.Domain;

    public interface IBillingRepository
    {
        bool AddClaim(Claim rap);
        bool AddRap(Rap rap);
        Claim GetClaim(Guid agencyId, int claimId);
        Rap GetRap(Guid agencyId, int claimId);
        Final GetFinal(Guid agencyId, int claimId);
        Claim GetClaim(Guid agencyId, Guid patientId, Guid episodeId);
        Rap GetRap(Guid agencyId, Guid patientId, Guid episodeId);
        Final GetFinal(Guid agencyId, Guid patientId, Guid episodeId);
        bool UpdateClaimStatus(Claim rap);
        bool AddFinal(Final final);
        bool UpdateFinalStatus(Final final);
        bool UpdateRapStatus(Rap rap);
        Bill AllUnProcessedBill(Guid agencyId);
        bool VerifyRap(Rap rap, Guid agencyId);
        bool VerifyInfo(Final final, Guid agencyId);
        bool VisitVerify(int Id, Guid episodeId, Guid patientId, List<Guid> Visit, Guid agencyId);
        List<Rap> Raps(List<int> rapToGenerate, Guid agencyId);
        List<Final> Finals(List<int> finalToGenerate, Guid agencyId);
        bool UpdateRapStatus(List<int> rapToGenerate, Guid agencyId);
        bool UpdateFinalStatus(List<int> finalToGenerate, Guid agencyId);
        IList<Rap> GetRaps(Guid agencyId);
        IList<Final> GetFinals(Guid agencyId);
        IList<Rap> GetRapProcessed(Guid patientId, Guid agencyId);
        IList<Final> GetFinalProcessed(Guid patientId, Guid agencyId);
        bool DeleteRap(Guid agencyId, Guid patientId, Guid episodeId);
        bool DeleteFinal(Guid agencyId, Guid patientId, Guid episodeId);
    }
}
