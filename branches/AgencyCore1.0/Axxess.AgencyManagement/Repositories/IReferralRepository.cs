﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Enums;

    public interface IReferralRepository
    {
        IEnumerable<Referral> GetAll(Guid agencyId, ReferralStatus status);
        bool Add(Referral referral);
        Referral Get(Guid agencyId,Guid id);
        bool Edit(Referral referral);
        bool Delete(Guid agencyId,Guid id);
        void SetStatus(Guid agencyId, Guid id, ReferralStatus status);
    }
}
