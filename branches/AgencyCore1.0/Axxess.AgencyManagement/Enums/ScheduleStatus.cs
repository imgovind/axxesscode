﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    using Axxess.AgencyManagement.Domain;

    public enum ScheduleStatus
    {
        [Description("Not Yet Started")]
        CommonNotStarted=0,
        //Orders status
        [Description("Not Yet Started")]
        OrderNotYetStarted = 100,
        [Description("Not Yet Due")]
        OrderNotYetDue = 105,
        [Description("Saved")]
        OrderSaved = 110,
        [Description("Pending Clinician Signature")]
        OrderPendingClinicianSignature = 115,
        [Description("Submitted To Case manager")]
        OrderSubmittedToCaseManager = 120,
        [Description("To Be Sent To Physician")]
        OrderToBeSentToPhysician = 125,
        [Description("Sent To Physician")]
        OrderSentToPhysician = 130,
        [Description("Returned W/ Physician Signature")]
        OrderReturnedWPhysicianSignature = 135,

        //Oasis assessment status     
        [Description("Not Yet Due")]
        OasisNotYetDue = 200,
        [Description("Not Started")]
        OasisNotStarted = 205,
        [Description("Saved")]
        OasisSaved = 210,
        [Description("Completed (Pending QA Review)")]
        OasisCompletedPendingReview = 215,
        [Description("Completed (Export Ready)")]
        OasisCompletedExportReady = 220,
        [Description("Exported")]
        OasisExported = 225,
        [Description("Returned For Review")]
        OasisReturnedForClinicianReview = 230,
        [Description("Reopened")]
        OasisReopened = 235,

        //Billing Status      
        [Description("Claim Created")]
        ClaimCreated = 300,
        [Description("Claim Submitted")]
        ClaimSubmitted = 305,
        [Description("Claim Rejected")]
        ClaimRejected = 310,
        [Description("Payment Pending")]
        ClaimPaymentPending = 315,
        [Description("Claim Accepted/Processing")]
        ClaimAccepted = 320,
        [Description("Claim With Errors")]
        ClaimWithErrors = 325,
        [Description("Paid Claim")]
        ClaimPaidClaim = 330,
        [Description("Cancelled Claim")]
        ClaimCancelledClaim = 335,

        //VisitNote
        [Description("Missed Visit")]
        NoteMissedVisit = 400,        
        [Description("Not Yet Started")]
        NoteNotStarted = 405,
        [Description("Not Yet Due")]
        NoteNotYetDue = 410,
        [Description("Saved")]
        NoteSaved = 415,
        [Description("Submitted With Signature")]
        NoteSubmittedWithSignature = 420,
        [Description("Completed")]
        NoteCompleted = 425,
        [Description("Returned For Review")]
        NoteReturned = 430,
        [Description("Reopened")]
        NoteReopened = 435
       
    }
}
