﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class PhysicianOrder : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid UserId { get; set; }
        public long OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime SentDate { get; set; }
        public string PhysicianSignatureText { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string Summary { get; set; }
        public string Text { get; set; }
        public int Status { get; set; }
        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public int OrderProcessType { get; set; }
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        [SubSonicIgnore]
        public string PhysicianName { get; set; }
        [SubSonicIgnore]
        public Guid OldEpisodeId { get; set; }
        [SubSonicIgnore]
        public Agency Agency { get; set; }
        [SubSonicIgnore]
        public Patient Patient { get; set; }
        [SubSonicIgnore]
        public AgencyPhysician Physician { get; set; }
        [SubSonicIgnore]
        public string EpisodeStartDate { get; set; }
        [SubSonicIgnore]
        public string EpisodeEndDate { get; set; }
        [SubSonicIgnore]
        public string Allergies { get; set; }
        [SubSonicIgnore]
        public string PrimaryDiagnosisCode { get; set; }
        [SubSonicIgnore]
        public string PrimaryDiagnosisText { get; set; }
        [SubSonicIgnore]
        public string SecondaryDiagnosisCode { get; set; }
        [SubSonicIgnore]
        public string SecondaryDiagnosisText { get; set; }
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.PatientId.IsEmpty(), "Patient is required."));
            AddValidationRule(new Validation(() => this.PhysicianId.IsEmpty(), "Physician is required."));
            AddValidationRule(new Validation(() => !this.OrderDate.ToString().IsValidDate(), "Created Date is invalid."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Text), "Order Description is required."));
        }

        #endregion

    }
}
