﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class Asset
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string FileName { get; set; }
        public string FileSize { get; set; }
        public string ContentType { get; set; }
        public byte[] Bytes { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
