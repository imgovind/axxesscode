﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;

    public class PatientVisitNote
    {
        public Guid Id { get; set; }
        public int Status { get; set; }
        public Guid UserId { get; set; }
        public string Note { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string NoteType { get; set; }
        public bool IsBillable { get; set; }
        public bool IsWoundCare { get; set; }
        public string WoundNote { get; set; }
        public bool IsSupplyExist { get; set; }
        public string Supply { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }

        [SubSonicIgnore]
        public List<NotesQuestion> Questions { get; set; }

    }
}
