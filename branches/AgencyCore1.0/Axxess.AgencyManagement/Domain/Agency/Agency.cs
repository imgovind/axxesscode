﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class Agency : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string TaxId { get; set; }
        public string TaxIdType { get; set; }
        public string Payor { get; set; }
        public string NationalProviderNumber { get; set; }
        public string MedicareProviderNumber { get; set; }
        public string MedicaidProviderNumber { get; set; }
        public string HomeHealthAgencyId { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonPhone { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }

        #endregion

        #region Domain
        [SubSonicIgnore]
        public string ContactPersonDisplayName
        {
            get
            {
                return string.Concat(this.ContactPersonFirstName, " ", this.ContactPersonLastName);
            }
        }
        [SubSonicIgnore]
        public string ContactPersonPhoneFormatted
        {
            get
            {
                return ContactPersonPhone.ToPhone();
            }
        }
        [ScriptIgnore]
        [SubSonicIgnore]
        public AgencyLocation MainLocation
        {
            get;
            set;
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AgencyAdminUsername { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AgencyAdminPassword { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AgencyAdminFirstName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AgencyAdminLastName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> ContactPhoneArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string LocationName { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressLine1 { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressLine2 { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressCity { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressStateCode { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressZipCode { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public string Phone { get; set; }
        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public List<string> PhoneArray { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Name), "Agency Name is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AgencyAdminUsername), "Admin E-mail is required.  <br />"));
            AddValidationRule(new Validation(() => !this.AgencyAdminUsername.IsEmail(), "Admin E-mail is not in a valid  format.  <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AgencyAdminPassword), "Admin Password is required.  <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AgencyAdminFirstName), "Admin FirstName is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AgencyAdminLastName), "Admin LastName is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Agency Address line is required.  <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Agency city is required.  <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Agency state is required.  <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Agency zip is required.  <br />"));

        }

        #endregion
    }
}
