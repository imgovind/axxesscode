﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    public class Bill
    {
        public IList<Rap> Raps { get; set; }
        public IList<Final> Finals { get; set; }
    }
}
