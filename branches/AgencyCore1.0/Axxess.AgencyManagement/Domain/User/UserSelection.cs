﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class UserSelection
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }
    }
}
