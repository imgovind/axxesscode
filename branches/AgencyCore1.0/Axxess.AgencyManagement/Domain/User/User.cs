﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;

    public class User : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public int Status { get; set; }
        public string Roles { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public string CustomId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Credentials { get; set; }
        public string TitleType { get; set; }
        public string Permissions { get; set; }
        public string ProfileData { get; set; }
        public Guid AgencyLocationId { get; set; }      
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        #endregion

        #region Domain
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                if (this.Status > 0)
                {
                    return ((UserStatus)Enum.ToObject(typeof(UserStatus), this.Status)).GetDescription();
                }
                return "";
            }
        }
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName, " ", this.Credentials);
            }
        }

        [SubSonicIgnore]
        public UserProfile Profile { get; set; }
        [SubSonicIgnore]
        public List<string> AgencyRoleList { get; set; }
        [SubSonicIgnore]
        public UserStatus UserStatus { get; set; }

        [SubSonicIgnore]
        public string EmailAddress { get; set; }
        [SubSonicIgnore]
        public string Password { get; set; }
        [SubSonicIgnore]
        public bool AllowWeekendAccess { get; set; }
        [SubSonicIgnore]
        public string EarliestLoginTime { get; set; }
        [SubSonicIgnore]
        public string AutomaticLogoutTime { get; set; }

        [SubSonicIgnore]
        public string TitleTypeOther { get; set; }
        [SubSonicIgnore]
        public string CredentialsOther { get; set; }

        [SubSonicIgnore]
        public string HomePhone
        {
            get
            {
                if (this.Profile != null && this.Profile.PhoneHome.IsNotNullOrEmpty())
                {
                    return this.Profile.PhoneHome.ToPhone();
                }
                return string.Empty;
            }
        }

        [SubSonicIgnore]
        public List<string> HomePhoneArray { get; set; }
        [SubSonicIgnore]
        public List<string> MobilePhoneArray { get; set; }
        [SubSonicIgnore]
        public List<string> PermissionsArray { get; set; }

        [SubSonicIgnore]
        public PasswordChange PasswordChanger { get; set; }
        [SubSonicIgnore]
        public SignatureChange SignatureChanger { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            if (this.Id.IsEmpty())
            {
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.EmailAddress), "User E-mail is required.  <br />"));
                AddValidationRule(new Validation(() => !this.EmailAddress.IsEmail(), "E-mail is not in a valid  format.  <br />"));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "User First Name is required. <br />"));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "User Last Name is required.  <br />"));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Password), "Password is required.  <br />"));
            }
            else
            {
                if (this.PasswordChanger != null && this.PasswordChanger.CurrentPassword.IsNotNullOrEmpty() && this.PasswordChanger.NewPassword.IsNotNullOrEmpty() && this.PasswordChanger.NewPasswordConfirm.IsNotNullOrEmpty())
                {
                    AddValidationRule(new Validation(() => !this.PasswordChanger.NewPassword.IsEqual(this.PasswordChanger.NewPasswordConfirm), "The passwords you have entered do not match."));
                }

                if (this.SignatureChanger != null && this.SignatureChanger.CurrentSignature.IsNotNullOrEmpty() && this.SignatureChanger.NewSignature.IsNotNullOrEmpty() && this.SignatureChanger.NewSignatureConfirm.IsNotNullOrEmpty())
                {
                    AddValidationRule(new Validation(() => !this.SignatureChanger.NewSignature.IsEqual(this.SignatureChanger.NewSignatureConfirm), "The signatures you have entered do not match."));
                }
            }
        }

        #endregion
    }
}
