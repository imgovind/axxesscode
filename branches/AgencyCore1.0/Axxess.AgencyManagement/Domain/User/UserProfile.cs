﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.SqlGeneration.Schema;

    public class UserProfile
    {
        #region Members

        public string SSN { get; set; }
        public DateTime DOB { get; set; }
        public DateTime EmploymentStartDate { get; set; }
        public DateTime TerminationDate { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string Nationality { get; set; }
        public string Ethnicity { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneMobile { get; set; }
        public string PhoneWork { get; set; }
        public string EmailWork { get; set; }
        public string EmailPersonal { get; set; }
        public bool HasMilitaryService { get; set; }
        public string DriversLicenseNumber { get; set; }
        public string DriversLicenseStateCode { get; set; }
        public DateTime DriversLicenseExpiration { get; set; }
        public Guid PhotoAssetId { get; set; }

        #endregion
    }
}
