﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;

    public class PatientSocCertPeriod
    {
        public Guid Id { get; set; }
        public string SocCertPeriod { get; set; }
        public string SocEndDate { get; set; }
        public string PatientPatientID { get; set; }
        public string PatientLastName { get; set; }
        public string PatientFirstName { get; set; }
        public string PhysicianName { get; set; }
        public string PatientSoC { get; set; }
        public string respEmp { get; set; }

    }

}
