﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class DiagnosisCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }
       
        public string FormatCode
        {
            get
            {
                return string.Format("{0}.{1}", this.Code.Substring(0, 3).PadLeft(3, '0'), this.Code.Substring((3)));
            }
        }
    }
}
