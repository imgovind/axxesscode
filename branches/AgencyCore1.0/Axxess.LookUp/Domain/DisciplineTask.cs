﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class DisciplineTask
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public string Discipline { get; set; }
        public bool IsBillable { get; set; }
    }
}
