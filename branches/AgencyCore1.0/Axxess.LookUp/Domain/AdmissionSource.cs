﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class AdmissionSource
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }
}
