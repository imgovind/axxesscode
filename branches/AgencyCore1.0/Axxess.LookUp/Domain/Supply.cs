﻿
namespace Axxess.LookUp.Domain
{
    using System;
    using System.Xml.Serialization;
    using System.Runtime.Serialization;
    using System.ComponentModel.DataAnnotations;

    using SubSonic.SqlGeneration.Schema;

    [KnownType(typeof(Supply))]
    [XmlRoot()]
    public class Supply
    {
        [XmlElement]
        public int Id { get; set; }
        [XmlElement]
        public string Code { get; set; }
        [XmlElement]
        public string Description { get; set; }
        [XmlElement]
        public byte CategoryId { get; set; }

        [XmlElement]
        [SubSonicIgnore]
        public string Quantity { get; set; }

        [XmlElement]
        [SubSonicIgnore]
        [UIHint("Date")]
        public string Date { get; set; }

        [SubSonicIgnore]
        [XmlIgnore]
        public string Url
        {
            get
            {
                return null;
            }
        }

        [XmlElement]
        [SubSonicIgnore]
        public Guid UniqueIdentifier { get; set; }
    }
}
