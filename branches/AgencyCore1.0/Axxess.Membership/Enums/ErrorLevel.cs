﻿namespace Axxess.Membership.Enums
{
    public enum ErrorLevel
    {
        Trivial,
        Regular,
        Important,
        Critical
    }
}
