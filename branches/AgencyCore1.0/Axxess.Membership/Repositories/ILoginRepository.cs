﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    using Axxess.Core.Infrastructure;

    public interface ILoginRepository
    {
        Login Find(Guid id);
        bool Delete(Guid loginId);
        ICollection<Login> GetAll();
        Login Find(string emailAddress);
        ICollection<Login> AutoComplete(string searchString);
        Guid Add(string emailAddress, string password, string salt, string role, string displayName);
        bool ChangePassword(Guid userId, string password, string salt, bool requirePasswordChange);
        Guid Add(string emailAddress, string password, string salt, string role, string displayName, bool changePassword, bool weekendAccess, string earliestLoginTime, string automaticLogoutTime);
        bool Update(Login login);
        bool Update(Guid userId, string emailAddress, string password, string salt, string role, string displayName);
        bool Update(Guid userId, string emailAddress, string password, string salt, string role, string displayName, bool changePassword, bool weekendAccess, string earliestLoginTime, string automaticLogoutTime);
    }
}
