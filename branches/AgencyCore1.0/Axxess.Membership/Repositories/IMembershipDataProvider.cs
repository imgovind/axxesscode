﻿namespace Axxess.Membership.Repositories
{
    public interface IMembershipDataProvider
    {
        //IHostRepository HostRepository { get; }
        //IAuditRepository AuditRepository { get; }
        IErrorRepository ErrorRepository { get; }
        ILoginRepository LoginRepository { get; }
        //ISettingRepository SettingRepository { get; }
        //IApplicationRepository AppRepository { get; }
    }
}
