﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Xml.Linq;

    public class Audit
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public Guid ApplicationId { get; set; }
        public string ActionLog { get; set; }
        public DateTime Created { get; set; }
    }
}
