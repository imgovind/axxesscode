﻿namespace Axxess.Core
{
    using System;

   public class DateRange
    {
       public DateTime StartDate { get; set; }
       public DateTime EndDate { get; set; }
       public string StartDateFormatted { get { return this.StartDate.ToShortDateString(); }  }
       public string EndDateFormatted { get { return this.EndDate.ToShortDateString(); } }
    }
}
