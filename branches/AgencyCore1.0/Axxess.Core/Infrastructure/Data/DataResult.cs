﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Data.Common;
    using System.Collections.Generic;

    public sealed class DataResult<T>
    {
        private ResultMapDelegate mapper;
        private readonly DbDataReader reader;
        
        public delegate T ResultMapDelegate(DataReader reader);

        public DataResult(DbDataReader reader)
        {
            this.reader = reader;
        }

        public DataResult<T> SetMap(ResultMapDelegate resultMapDelegate)
        {
            mapper = resultMapDelegate;
            return this;
        }

        public IList<T> AsList()
        {
            Check.Argument.IsNotNull(mapper, "mapper");

            List<T> returnList = new List<T>();

            var dataReader = new DataReader(reader);
            while (reader.Read())
            {
                T data = mapper.Invoke(dataReader);
                returnList.Add(data);
            }

            return returnList;
        }

        public IList<T> AsList(ResultMapDelegate map)
        {
            mapper = map;
            return AsList();
        }

        public T AsSingle()
        {
            Check.Argument.IsNotNull(mapper, "mapper");

            var dataReader = new DataReader(reader);
            if (reader.Read())
            {
                T data = mapper.Invoke(dataReader);
                return data;
            }
            return default(T);
        }

        public T AsSingle(ResultMapDelegate map)
        {
            mapper = map;
            return AsSingle();
        }
    }
}
