﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.ComponentModel;

    public class PermissionAttribute : DescriptionAttribute
    {
        public PermissionAttribute(string description, string longDescription, string category)
            : base(description)
        {
            this.Category = category;
            this.LongDescription = longDescription;
        }

        public string Category { get; private set; }
        public string LongDescription { get; private set; }
    }
}
