﻿namespace Axxess.Core.Infrastructure
{
    using System;
   
    public static class Notify
    {
        #region Static Methods

        public static void User(string fromAddress, string toAddress, string subject, string body)
        {
            INotification notification = Container.Resolve<INotification>();
            notification.Send(fromAddress, toAddress, subject, body);
        }

        public static void Webmaster(string subject, string body)
        {
            INotification notification = Container.Resolve<INotification>();
            notification.Send(AppSettings.NoReplyEmail, AppSettings.NoReplyEmail, subject, body);
        }

        #endregion
    }
}
