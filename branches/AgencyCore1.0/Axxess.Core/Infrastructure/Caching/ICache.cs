﻿namespace Axxess.Core.Infrastructure
{
    using System;

    public interface ICache
    {
        int Count { get; }
        void Remove(string key);
        bool Contains(string key);
        void Set<T>(string key, T value);
        void Set<T>(string key, T value, DateTime absoluteExpiration);
        void Set<T>(string key, T value, TimeSpan slidingExpiration);
        bool TryGet<T>(string key, out T value);
        T Get<T>(string key);
    }
}
