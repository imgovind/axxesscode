﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>60 Day Summary/Case Conference<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body></body><%
string[] functionLimitations = data != null && data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null;
string[] patientCondition = data != null && data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null;
string[] serviceProvided = data != null && data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null;
string[] recommendedService = data != null && data.ContainsKey("RecommendedService") && data["RecommendedService"].Answer != "" ? data["RecommendedService"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().Globalization(true) 
     .DefaultGroup(group => group
     .Add("jquery-1.4.2.min.js")
     .Add("/Modules/printview.js")
     .Compress(true).Combined(true).CacheDurationInDays(5))
     .OnDocumentReady(() => {  %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : ""%><%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : ""%>%3Cbr /%3E<%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3E60 Day Summary/%3Cbr /%3ECase Conference%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            '<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %>' +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3ECompletion Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("VisitDate") && data["VisitDate"].Answer.IsNotNullOrEmpty() ? data["VisitDate"].Answer.ToDateTime().ToString("MM/dd/yyy") : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null ? string.Format(" {0} &ndash; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) : "" %>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan class=%22dual%22%3E" +
            '<%= Model.PhysicianDisplayName.ToTitleCase() %>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EHomebound Status:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("HomeboundStatus") ? data["HomeboundStatus"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EHomebound Status:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("HomeboundStatus") ? data["HomeboundStatus"].Answer : ""%>' +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ETertiary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            '<%= data != null && data.ContainsKey("PrimaryDiagnosis2") ? data["PrimaryDiagnosis2"].Answer : ""%>' +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "%3Cbr /%3E" : ""%><%= Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : ""%><%= Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() : ""%>%3Cbr /%3E<%= Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : ""%><%= Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "&nbsp; " : ""%><%= Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3E60 Day Summary/%3Cbr /%3ECase Conference%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            '<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %>' +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "%3Cimg src=%22/Images/axxess_print.png%22 class=%22axxess%22 /%3E%3Cimg src=%22/Images/acore_print.png%22 class=%22acore%22 /%3E";
        printview.addsection(
            printview.col(3,
                printview.checkbox("Amputation",<%= functionLimitations != null && functionLimitations.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Legally Blind",<%= functionLimitations != null && functionLimitations.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Endurance",<%= functionLimitations != null && functionLimitations.Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Contracture",<%= functionLimitations != null && functionLimitations.Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Hearing",<%= functionLimitations != null && functionLimitations.Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Paralysis",<%= functionLimitations != null && functionLimitations.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Bowel/Bladder Incontinence",<%= functionLimitations != null && functionLimitations.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Dyspnea",<%= functionLimitations != null && functionLimitations.Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Ambulation",<%= functionLimitations != null && functionLimitations.Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Speech",<%= functionLimitations != null && functionLimitations.Contains("A") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= functionLimitations != null && functionLimitations.Contains("B") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("FunctionLimitationsOther") ? data["FunctionLimitationsOther"].Answer : ""%>",0,3) ),
            "Functional Limitations");
        printview.addsection(
            printview.col(5,
                printview.checkbox("Stable",<%= patientCondition != null && patientCondition.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Improved",<%= patientCondition != null && patientCondition.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Unchanged",<%= patientCondition != null && patientCondition.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Unstable",<%= patientCondition != null && patientCondition.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Declined",<%= patientCondition != null && patientCondition.Contains("5") ? "true" : "false"%>) ),
            "Patient Condition");
        printview.addsection(
            printview.col(3,
                printview.checkbox("SN",<%= serviceProvided != null && serviceProvided.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("PT",<%= serviceProvided != null && serviceProvided.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("OT",<%= serviceProvided != null && serviceProvided.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("ST",<%= serviceProvided != null && serviceProvided.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("MSW",<%= serviceProvided != null && serviceProvided.Contains("5") ? "true" : "false"%>) +
                printview.checkbox("HHA",<%= serviceProvided != null && serviceProvided.Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= serviceProvided != null && serviceProvided.Contains("7") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : ""%>",0,3) ),
            "Service(s) Provided");
        printview.addsection(
            "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth%3E%3C/th%3E%3Cth%3EBP%3C/th%3E%3Cth%3EHR%3C/th%3E%3Cth%3EResp%3C/th%3E%3Cth%3ETemp%3C/th%3E%3Cth%3EWeight%3C/th%3E%3Cth%3EBG%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3ELowest%3C/th%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignBPMin") ? data["VitalSignBPMin"].Answer : ""%>' +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignHRMin") ? data["VitalSignHRMin"].Answer : ""%>' +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignRespMin") ? data["VitalSignRespMin"].Answer : ""%>' +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignTempMin") ? data["VitalSignTempMin"].Answer : ""%>' +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignWeightMin") ? data["VitalSignWeightMin"].Answer : ""%>' +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignBGMin") ? data["VitalSignBGMin"].Answer : ""%>' + 
            "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth%3EHighest%3C/th%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignBPMax") ? data["VitalSignBPMax"].Answer : ""%>' +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignHRMax") ? data["VitalSignHRMax"].Answer : ""%>' +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignRespMax") ? data["VitalSignRespMax"].Answer : ""%>' +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignTempMax") ? data["VitalSignTempMax"].Answer : ""%>' +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignWeightMax") ? data["VitalSignWeightMax"].Answer : ""%>' +
            "%3C/td%3E%3Ctd class=%22dual%22%3E" +
            '<%= data != null && data.ContainsKey("VitalSignBGMax") ? data["VitalSignBGMax"].Answer : ""%>' +
            "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E",
            "Vital Sign Ranges");
        printview.addsection(
            printview.col(2,
                printview.col(3,
                    printview.span("DNR:",1) +
                    printview.checkbox("Yes",<%= data != null && data.ContainsKey("AreHospitalization") && data["AreHospitalization"].Answer == "1" ? "true" : "false"%>) +
                    printview.checkbox("No",<%= data != null && data.ContainsKey("AreHospitalization") && data["AreHospitalization"].Answer == "0" ? "true" : "false"%>) ) +
                printview.col(2,
                    printview.span("If Yes, Date(s):",1) +
                    printview.span("<%= data != null && data.ContainsKey("HospitalizationDate") ? data["HospitalizationDate"].Answer : ""%>",0,3) ) ) );
        printview.addsection(printview.span("<%= data != null && data.ContainsKey("SummaryOfCareProvided") ? data["SummaryOfCareProvided"].Answer : ""%>",0,5),"Summary of Care Provided");
        printview.addsection(printview.span("<%= data != null && data.ContainsKey("PatientCurrentCondition") ? data["PatientCurrentCondition"].Answer : ""%>",0,5),"Patient&rsquo;s Current Condition");
        printview.addsection(printview.span("<%= data != null && data.ContainsKey("Goals") ? data["Goals"].Answer : ""%>",0,5),"Goals");
        printview.addsection(
            printview.col(3,
                printview.checkbox("SN",<%= recommendedService != null && recommendedService.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("PT",<%= recommendedService != null && recommendedService.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("OT",<%= recommendedService != null && recommendedService.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("ST",<%= recommendedService != null && recommendedService.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("MSW",<%= recommendedService != null && recommendedService.Contains("5") ? "true" : "false"%>) +
                printview.checkbox("HHA",<%= recommendedService != null && recommendedService.Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= recommendedService != null && recommendedService.Contains("7") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : "" %>",0,3) ),
            "Recomended Services");
        printview.addsection(
            printview.col(2,
                printview.span("Clinician Signature:",true) +
                printview.span("Date:",true) +
                printview.span("<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : ""%>",0,4) +
                printview.span("<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() ? Model.SignatureDate : ""%>",0,4) ) );
     <%
     }).Render(); %>
</html>
