﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>

<% using (Html.BeginForm("UpdateEpisode", "Schedule", FormMethod.Post, new { @id = "editEpisodeForm" }))
   { %>
   <%= Html.Hidden("Id", Model.Id) %>
   <%= Html.Hidden("PatientId", Model.PatientId) %>
<div class="wrapper main">
    <fieldset>
        <legend>Patient</legend>
        <div class="wide_column">
            <div class="row">
                <span class="bigtext"><%= Model.DisplayName %></span>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Details</legend>
        <table class="form"><tbody>
            <tr>
                <td><label for="Edit_Episode_TargetDate" class="float_left">Episode Start Date:</label><br />
                <%= Model.StartDate.ToShortDateString() %>
                </td>
                <td><label for="Edit_Episode_VisitDate" class="float_left">Episode End Date:</label><br />
                <%= Model.EndDate.ToShortDateString() %>
                </td>
                <td><label for="Edit_Episode_IsActive" class="float_left">Inactivate Episode:</label><br />
                <%= Html.CheckBox("IsActive", Model.IsActive, new { @id = "Edit_Episode_IsActive", @class = "radio" })%>
                </td>
            </tr>
            <tr>
                <td><label for="Edit_Episode_CaseManager" class="float_left">Case Manager:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Users, "Detail.CaseManager", Model.Detail.CaseManager, new { @id = "Edit_Episode_CaseManager", @class = "Users required valid" })%>
                </td>
                <td><label for="Edit_Episode_Therapist" class="float_left">Therapist:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Users, "Detail.Therapist", Model.Detail.Therapist, new { @id = "Edit_Episode_Therapist", @class = "Users required valid" })%>
                </td>
                 <td><label for="Edit_Episode_Surcharge" class="float_left">Payroll Surcharge:</label><br />
                <%= Html.TextBox("Surcharge", Model.Detail.SurchargePayroll, new { @id = "Edit_Episode_Surcharge", @class = "text digits input_wrapper", @maxlength = "5" })%>
                </td>
            </tr>
            <tr>
                <td><label for="Edit_Episode_PrimaryPhysician" class="float_left">Primary Physician:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Physicians, "Detail.PrimaryPhysician", Model.Detail.PrimaryPhysician, new { @id = "Edit_Episode_PrimaryPhysician", @class = "Physicians" })%>
                </td>
                <td><label for="Edit_Episode_PrimaryInsurance" class="float_left">Primary Insurance:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Insurance, "Detail.PrimaryInsurance", Model.Detail.PrimaryInsurance, new { @id = "Edit_Episode_PrimaryInsurance", @class = "Insurance" })%>
                </td>
                <td><label for="Edit_Episode_SecondaryInsurance" class="float_left">Secondary Insurance:</label><br />
                <%= Html.LookupSelectList(SelectListTypes.Insurance, "Detail.SecondaryInsurance", Model.Detail.SecondaryInsurance, new { @id = "Edit_Episode_SecondaryInsurance", @class = "Insurance" })%>
                </td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide_column">
            <div class="row">
                <textarea id="Edit_Episode_Comments" name="Detail.Comments" rows="10"><%= Model.Detail.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editepisode');">Cancel</a></li>
    </ul></div>
</div>
<% } %>
