﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Reset ",
        Axxess.AgencyManagement.App.Current.User.DisplayName,
        "&rsquo;s Signature','forgotsignature');</script>")%>
<div class="wrapper main">
    <fieldset>
        <legend>Forgot Signature</legend>
        <div class="wide_column">
            <div class="row">Click on the button below to reset your signature. An e-mail with a temporary signature will be sent to <%= Model %>.</div>
            <div id="resetSignatureMessage" class="errormessage"></div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a id="lnkRequestSignatureReset" href="javascript:void(0);">Reset Signature</a></li>
        </ul>
    </div>
</div>

<div id="resetSignature" class="resetSignature hidden">
    <form id="resetSignatureForm" action="/Signature/Reset" method="post">
    <fieldset>
        <legend>Electronic Signature Reset</legend>
        <div class="wide_column">
            <div class="row">Enter the temporary signature from the e-mail along with your new signature below to complete the signature reset request.</div>
            <div class="row"><label for="Reset_Signature_CurrentSignature">Temporary Signature:</label><div class="float_right"><%=Html.Password("CurrentSignature", "", new { @id = "Reset_Signature_CurrentSignature", @class = "input_wrapper required", @maxlength = "15" })%></div></div>
            <div class="row"><label for="Reset_Signature_NewSignature">New Signature:</label><div class="float_right"><%=Html.Password("NewSignature", "", new { @id = "Reset_Signature_NewSignature", @class = "input_wrapper required", @maxlength = "15" })%></div></div>
            <div class="row"><label for="Reset_Signature_NewSignatureConfirm">Confirm New Signature:</label><div class="float_right"> <%=Html.Password("NewSignatureConfirm", "", new { @id = "Reset_Signature_NewSignatureConfirm", @class = "input_wrapper required", @maxlength = "15" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="U.closeDialog();">Cancel</a></li>
    </ul></div>
    </form>
</div>
