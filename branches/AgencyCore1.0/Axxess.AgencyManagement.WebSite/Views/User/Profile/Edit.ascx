﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<%  using (Html.BeginForm("Edit", "Profile", FormMethod.Post, new { @id = "editProfileForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Profile_Id" }) %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Edit Profile | ",
        Model != null ? (Model.LastName + ", " + Model.FirstName).ToTitleCase() : "",
        "','editprofile');</script>")%>
<div class="form_wrapper">
    <fieldset class="half float_left">
        <legend>Login Password</legend>
        <div class="column">
            <div class="row"><label for="Edit_Profile_CurrentPassword">Current Password:</label><div class="float_right"> <%=Html.Password("PasswordChanger.CurrentPassword", "", new { @id = "Edit_Profile_CurrentPassword", @class = "input_wrapper", @maxlength = "15" })%></div></div>
            <div class="row"><label for="Edit_Profile_NewPassword">New Password:</label><div class="float_right"><%=Html.Password("PasswordChanger.NewPassword", "", new { @id = "Edit_Profile_NewPassword", @class = "input_wrapper", @maxlength = "15" })%></div></div>
            <div class="row"><label for="Edit_Profile_NewPasswordConfirm">Confirm New Password:</label><div class="float_right"> <%=Html.Password("PasswordChanger.NewPasswordConfirm", "", new { @id = "Edit_Profile_NewPasswordConfirm", @class = "input_wrapper", @maxlength = "15" })%></div></div>
        </div>
    </fieldset>
    <fieldset class="half float_right">
        <legend>Electronic Signature</legend>
        <div class="column">
            <div class="row"><label for="Edit_Profile_CurrentSignature">Current Signature:</label><div class="float_right"> <%=Html.Password("SignatureChanger.CurrentSignature", "", new { @id = "Edit_Profile_CurrentSignature", @class = "input_wrapper", @maxlength = "15" })%></div></div>
            <div class="row"><label for="Edit_Profile_NewSignature">New Signature:</label><div class="float_right"><%=Html.Password("SignatureChanger.NewSignature", "", new { @id = "Edit_Profile_NewSignature", @class = "input_wrapper", @maxlength = "15" })%></div></div>
            <div class="row"><label for="Edit_Profile_NewSignatureConfirm">Confirm New Signature:</label><div class="float_right"> <%=Html.Password("SignatureChanger.NewSignatureConfirm", "", new { @id = "Edit_Profile_NewSignatureConfirm", @class = "input_wrapper", @maxlength = "15" })%></div></div>
        </div>
    </fieldset>
    <div class="clear">&nbsp;</div>
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Profile_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%= Html.TextBox("Profile.AddressLine1", Model.Profile.AddressLine1, new { @id = "Edit_Profile_AddressLine1", @maxlength = "20", @class = "text input_wrapper" }) %></div></div>
            <div class="row"><label for="Edit_Profile_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%= Html.TextBox("Profile.AddressLine2", Model.Profile.AddressLine2, new { @id = "Edit_Profile_AddressLine2", @maxlength = "20", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Profile_AddressCity" class="float_left">City:</label><div class="float_right"><%= Html.TextBox("Profile.AddressCity", Model.Profile.AddressCity, new { @id = "Edit_Profile_AddressCity", @maxlength = "20", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Profile_AddressStateCode" class="float_left">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", Model.Profile.AddressStateCode, new { @id = "Edit_Profile_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("AddressZipCode", Model.Profile.AddressZipCode, new { @id = "Edit_Profile_AddressZipCode", @class = "text numeric input_wrapper zip", @size = "5", @maxlength = "5" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="Edit_Profile_PhonePrimary1">Home Phone:</label><div class="float_right"><%=Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(0, 3) : "", new { @id = "Edit_Profile_PhonePrimary1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(3, 3) : "", new { @id = "Edit_Profile_PhonePrimary2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() ? Model.Profile.PhoneHome.Substring(6, 4) : "", new { @id = "Edit_Profile_PhonePrimary3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Profile_PhoneAlternate1">Mobile Phone:</label><div class="float_right"><%=Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(0, 3) : "", new { @id = "Edit_Profile_PhoneAlternate1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(3, 3) : "", new { @id = "Edit_Profile_PhoneAlternate2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() ? Model.Profile.PhoneMobile.Substring(6, 4) : "", new { @id = "Edit_Profile_PhoneAlternate3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editprofile');">Close</a></li>
    </ul></div>
</div>
<% } %>