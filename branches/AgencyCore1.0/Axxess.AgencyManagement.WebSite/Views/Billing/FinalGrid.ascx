﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<table class="claim">
    <thead>
        <tr><th colspan="7">Final</th></tr>
        <tr><th>Patient Name</th><th>Patient ID</th><th>Episode Date</th><th>Rap Generated</th><th>Visit</th><th>Order</th><th>Final</th></tr>
    </thead>
    <tbody><%
        var finals = Model.Finals.Where(c => !c.IsGenerated && c.EpisodeEndDate < DateTime.Now); 
        int j = 1;
        foreach (var final in finals) { %>
        <tr class='<%= j % 2 != 0 ? "odd" : "even" %>'>
            <td><span class="alphali"><%= j %></span><input id='FinalSelected<%= final.Id %>' name="FinalSelected" class="radio" type="checkbox" value='<%= final.Id %>' '<% if (!final.IsVisitVerified || !final.IsSupplyVerified || !final.IsFinalInfoVerified) { %>disabled="disabled"<% } %>' /><% if (final.IsOasisComplete && final.IsFirstBillableVisit) { %><a onclick="UserInterface.ShowFinal('<%= final.EpisodeId %>','<%= final.PatientId %>');"><% } else { %><a onclick="$.jGrowl('Error: Not Completed', { theme: 'error', life: 5000 })"><% } %><%= final.DisplayName %></a></td>
            <td><%= final.PatientIdNumber %></td>
            <td><% if (final.EpisodeStartDate != null) { %><%= final.EpisodeStartDate.ToShortDateString() %><% } %><% if (final.EpisodeEndDate != null) { %> &ndash; <%= final.EpisodeEndDate.ToShortDateString() %><% } %></td>
            <td class="align_center"><img src='<%= final.IsRapGenerated ? "/Images/icons/success.png" : "/Images/icons/error.png" %>' width="16" height="16" alt="" border="0" /></td>
            <td class="align_center"><img src='<%= final.AreVisitsComplete ? "/Images/icons/success.png" : "/Images/icons/error.png" %>' width="16" height="16" alt="" border="0" /></td>
            <td class="align_center"><img src='<%= final.AreOrdersComplete ? "/Images/icons/success.png" : "/Images/icons/error.png" %>' width="16" height="16" alt="" border="0" /></td>
            <td class="align_center"><img src='<%= final.AreOrdersComplete && final.AreVisitsComplete ? "/Images/icons/success.png" : "/Images/icons/error.png" %>' width="16" height="16" alt="" border="0" /></td>
        </tr>
        <% j++;
        } %>
    </tbody>
</table>
