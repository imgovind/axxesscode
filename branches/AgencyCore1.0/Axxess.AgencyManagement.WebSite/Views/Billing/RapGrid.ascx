﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<table class="claim">
    <thead class="align_center">
        <tr><th colspan="6">Rap</th></tr>
        <tr><th>Patient Name</th><th>Patient ID</th><th>Episode Date</th><th>Oasis</th><th>First Billable Visit</th><th>Verified</th></tr>
    </thead>
    <tbody><%
        var raps = Model.Raps.Where(c => !c.IsGenerated);
        int i = 1;
        foreach (var rap in raps) { %>
        <tr class='<%= i % 2 != 0 ? "odd" : "even" %>'>
            <td><span class="alphali"><%=i %></span><input id='RapSelected<%= rap.Id %>' name="RapSelected" class="radio" type="checkbox" value='<%= rap.Id %>' '<% if (!rap.IsVerified) { %>disabled="disabled"<% } %>' /><% if (rap.IsOasisComplete && rap.IsFirstBillableVisit) { %><a onclick="UserInterface.ShowRap('<%=rap.EpisodeId%>','<%=rap.PatientId%>');"><% } else { %><a onclick="$.jGrowl('Error: Not Completed', { theme: 'error', life: 5000 })"><% } %><%= rap.DisplayName %></a></td>
            <td><%= rap.PatientIdNumber %></td>
            <td><% if (rap.EpisodeStartDate != null) { %><%= rap.EpisodeStartDate.ToShortDateString() %><% } %><% if (rap.EpisodeEndDate != null) { %> &ndash; <%= rap.EpisodeEndDate.ToShortDateString() %><% } %></td>
            <td class="align_center"><img src='<%= rap.IsOasisComplete ? "/Images/icons/success.png" : "/Images/icons/error.png" %>' width="16" height="16" alt="" border="0" /></td>
            <td class="align_center"><img src='<%= rap.IsFirstBillableVisit ? "/Images/icons/success.png" : "/Images/icons/error.png" %>' width="16" height="16" alt="" border="0" /></td>
            <td class="align_center"><img src='<%= rap.IsVerified ? "/Images/icons/success.png" : "/Images/icons/error.png" %>' width="16" height="16" alt="" border="0" /></td>
        </tr>
        <% i++;
        } %>
    </tbody>
</table>
