﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientCenterViewData>" %>
<div class="temp-container">
    <div id='billing_Splitter1' class="xSplitter splitterContainer">
        <div class="xsp-pane sidepanel">
            <div id="billingLeftSide" class="side">
                <div id="billingFillterContainer" class="sideTop">
                    <div class="row">
                        <div class="divLabel">
                            <label>
                                View:</label></div>
                        <div class="inputs">
                            <span class="input_wrapper blank">
                                <select name="list" class="sideDropDown">
                                    <option value="0">All Patients</option>
                                    <option value="1">Active Patients</option>
                                    <option value="2">Discharged Patients</option>
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="spacingDiv">
                    </div>
                    <div class="row">
                        <div class="divLabel">
                            <label>
                                Filter:</label></div>
                        <div class="inputs">
                            <span class="input_wrapper blank">
                                <select name="list" class="sideDropDown">
                                    <option value="0">All</option>
                                    <option value="1">Medicare (traditional)</option>
                                    <option value="2">Medicare (HMO/managed care)</option>
                                    <option value="3">Medicaid (traditional)</option>
                                    <option value="4">Medicaid (HMO/managed care) </option>
                                    <option value="5">Workers' compensation</option>
                                    <option value="6">Title programs </option>
                                    <option value="7">Other government</option>
                                    <option value="8">Private</option>
                                    <option value="9">Private HMO/managed care</option>
                                    <option value="10">Self Pay</option>
                                    <option value="11">Unknown</option>
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="spacingDiv">
                    </div>
                    <div class="row">
                        <div class="divLabel">
                            <label>
                                Find:</label></div>
                        <div class="inputs">
                            <span class="input_wrapper">
                                <input class="text" name="" value="" type="text" size="18" style="width: 184px;" /></span>
                        </div>
                    </div>
                    <div class="spacingDiv">
                    </div>
                </div>
                <hr class="xsp-dragbar-hframe" style="width: 100%;" />
                <%Html.RenderPartial("~/Views/Billing/PatientList.ascx", Model.Patients); %>
            </div>
        </div>
        <div class="abs window_main xsp-pane ">
            <div id='billing_Splitter12' class='abs' style="height: 100%;">
                <div class='mainTop' >
                    <div class="mainTopContainer">
                        <div class="mainTopContainerLeft">
                            <div>
                                <div>
                                    <div class="oasisMenu">
                                        <ul>
                                            <li>New Claim |</li>
                                            <li>Copy Claim |</li>
                                        </ul>
                                    </div>
                                    <div>
                                        <label>
                                            <b style="color: #53868B;">Patient Information&nbsp;&nbsp;&nbsp;</b></label></div>
                                </div>
                                <div class="underLine">
                                </div>
                            </div>
                            <div class="mainTopContent">
                                <div class="mainTopContentShceLeft">
                                    <div class="row">
                                        <div class="fixedRow">
                                            <label>
                                                Patient ID #:&nbsp;&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span3" class="input_wrapper blank">345632&nbsp;</span>
                                        </div>
                                        <br />
                                        <div class="fixedRow">
                                            <label>
                                                Physician Source:&nbsp;&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span17" class="input_wrapper blank">Medicare </span>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="fixedRow">
                                            <label>
                                                <b>HHRG(Grouper):</b>&nbsp;&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span4" class="input_wrapper blank">C1F1S0&nbsp;</span>
                                        </div>
                                        <br />
                                        <div class="fixedRow">
                                            <label>
                                                <b>HIPPS</b>:&nbsp;&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span5" class="input_wrapper blank">HDFJ1</span>
                                        </div>
                                        <div class="breakLineRow">
                                        </div>
                                        <div class="fixedRow">
                                            <label>
                                                <b>AUTH:</b>&nbsp;&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span7" class="input_wrapper blank">23452345234254</span>
                                        </div>
                                        <!--   <div class="inputs" style="float: right;">
                                                                <br />
                                                                <span class="input_wrapper blank"><a href="http://maps.google.com/maps?hl=en&tab=wl">
                                                                    Map </a>| <a href="javascript:void(0);">Direction</a>&nbsp;</span>
                                                            </div>-->
                                    </div>
                                </div>
                                <div class="mainTopContentShceRight">
                                    <div class="row">
                                        <div class="fixedRowSche">
                                            <label>
                                                1. Standard Episode Rate:&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span18" class="input_wrapper blank">2179.00</span>
                                        </div>
                                        <br />
                                        <div class="fixedRowSche">
                                            <label>
                                                2. Case Mix Index:&nbsp;&nbsp;</label></div>
                                        <br />
                                        <div class="fixedRow">
                                            <label>
                                                Base:&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span20" class="input_wrapper blank">&nbsp;&nbsp; </span><span id="Span6"
                                                class="input_wrapper blank">0.56372 </span>
                                        </div>
                                        <br />
                                        <div class="fixedRow">
                                            <label>
                                                Clinical:&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span23" class="input_wrapper blank">C1 </span><span id="Span15" class="input_wrapper blank">
                                                &nbsp;0.32541 </span>
                                        </div>
                                        <br />
                                        <div class="fixedRow">
                                            <label>
                                                Functional:&nbsp;&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span24" class="input_wrapper blank">F5</span> <span id="Span16" class="input_wrapper blank">
                                                &nbsp;0.34535</span>
                                        </div>
                                        <br />
                                        <div class="fixedRow">
                                            <label>
                                                Services:&nbsp;&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span25" class="input_wrapper blank">S0</span> <span id="Span19" class="input_wrapper blank">
                                                &nbsp;0.0000</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fixedRowSche">
                                            <label>
                                                3. Adjustment Labour(4)X(5):&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span26" class="input_wrapper blank">2790.33</span>
                                        </div>
                                        <br />
                                        <div class="fixedRowSche">
                                            <label>
                                                4. Labor(3) X .22332:&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span28" class="input_wrapper blank">2167.19</span>
                                        </div>
                                        <div class="fixedRowSche">
                                            <label>
                                                5. MSA 3660 Factor:&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span31" class="input_wrapper blank">0.86170</span>
                                        </div>
                                        <div class="fixedRowSche">
                                            <label>
                                                6. Adjusted Labor(4) X (5):&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span32" class="input_wrapper blank">1867.47</span>
                                        </div>
                                        <div class="fixedRowSche">
                                            <label>
                                                7. Non-Labor(3) X .22332:&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span33" class="input_wrapper blank">623.14</span>
                                        </div>
                                        <div class="fixedRowSche">
                                            <label>
                                                8. Rural Adjustment:&nbsp;&nbsp;</label></div>
                                        <div class="inputs">
                                            <span id="Span34" class="input_wrapper blank">0.00</span>
                                        </div>
                                        <div class="underLine">
                                        </div>
                                        <div class="fixedRowSche">
                                            <label>
                                                <b>Prospective Pay:&nbsp;&nbsp;</b></label></div>
                                        <div class="inputs">
                                            <span id="Span8" class="input_wrapper blank">2490.60</span>
                                        </div>
                                        <div class="underLine">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mainTopContainerRight">
                            <div>
                                <b>Billing Report For This Patient</b>
                                <div class="underLine">
                                </div>
                                <ul>
                                    <li><a href="javascript:void(0);">PPS Analysis</a> </li>
                                    <li><a href="javascript:void(0);" onclick="JQD.open_window('#schedule_window');">Claim
                                        to be transmited</a> </li>
                                    <li><a href="javascript:void(0);">Claim transmitted and paid</a> </li>
                                    <li><a href="javascript:void(0);" onclick="JQD.open_window('#schedule_window');">Claim
                                        transmited only</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="xsp-dragbar-hframe" style="width: 100%; float:left;" />
                <div class=''  style="width: 100%; float:left;">
                   <%Html.RenderPartial("~/Views/Billing/HistoryActivity.ascx", Model.Patients.First()); %>
                </div>
            </div>
        </div>
        <div id="BillingHistoryVerticalBar" class='abs xsp-dragbar xsp-dragbar-vframe'>
            &nbsp;</div>
    </div>
</div>
