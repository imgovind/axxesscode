﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Agency>" %>
<%  using (Html.BeginForm("Update", "Agency", FormMethod.Post, new { @id = "editAgencyForm" })) { %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Edit Agency | ",
        Model != null ? Model.Name.ToTitleCase() : "",
        "','editagency');</script>")%>
<div class="form_wrapper">
    <fieldset>
        <legend>Admin Information</legend>
        <div class="column">
            <div class="row"><label for="AgencyAdminUsername">E-mail:</label><div class="float_right"><%=Html.TextBox("AgencyAdminUsername", "", new { @id = "Edit_Agency_AdminUsername", @class = "text required", @maxlength = "40", @tabindex = "1" })%></div></div>
            <div class="row"><label for="AgencyAdminPassword">Password:</label><div class="float_right"><%=Html.TextBox("AgencyAdminPassword", "", new { @id = "Edit_Agency_AdminPassword", @class = "text required password", @maxlength = "20", @tabindex = "2" })%><div class="buttons float_right"><ul><li><a href="javascript:void(0);" id="Edit_Agency_GeneratePassword">Generate</a></li></ul></div></div></div>
        </div><div class="column">
            <div class="row"><label for="AgencyAdminFirstName">First Name:</label><div class="float_right"><%=Html.TextBox("AgencyAdminFirstName", "", new { @id = "Edit_Agency_AdminFirstName", @class = "text required names input_wrapper", @maxlength = "20", @tabindex = "3" })%></div></div>
            <div class="row"><label for="AgencyAdminLastName">Last Name:</label><div class="float_right"><%=Html.TextBox("AgencyAdminLastName", "", new { @id = "Edit_Agency_AdminLastName", @class = "text required names input_wrapper", @maxlength = "20", @tabindex = "4" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Company Information</legend>
        <div class="column">
            <div class="row"><label for="Name">Company Name:</label><div class="float_right"><%=Html.TextBox("Name", "", new { @id = "Edit_Agency_CompanyName", @maxlength = "50", @class = "text required input_wrapper", @tabindex = "5" })%></div></div>
            <div class="row"><label for="TaxId">Tax Id:</label><div class="float_right"><%=Html.TextBox("TaxId", "", new { @id = "Edit_Agency_TaxId", @maxlength = "10", @class = "text required input_wrapper", @tabindex = "6" })%></div></div>
            <div class="row"><label for="TaxIdTypeId">Tax Id Type:</label><div class="float_right"><select id="Edit_Agency_TaxIdType" name="TaxIdType" tabindex="7"><option value="0">Choose Tax ID Type</option><option value="1">EIN (Employer Identification Number)</option><option value="2">SSN (Social Security Number)</option></select></div></div>
            <div class="row"><label for="Payor">Payor:</label><div class="float_right"><select id="Edit_Agency_Payor" name="Payor" tabindex="8"><option value="0">Choose Payor</option><option value="1">Palmetto GBA</option><option value="2">National Government Services (formerly known as United Government Services)</option><option value="3">Blue Cross Blue Shield of Alabama (AKA Chaba GBA)</option><option value="4">Anthem Health Plans of Maine</option></select></div></div>
            <div class="row">&nbsp;</div>
            <div class="row"><div class="float_right"><input type="checkbox" class="radio" name="Edit_Agency_SameAsAdmin" id="Edit_Agency_SameAsAdmin" tabindex="13" />&nbsp;Check here if the Admin is same as the Contact Person</div></div>
            <div class="row"><label for="ContactPersonEmail">Contact Person E-mail:</label><div class="float_right"><%=Html.TextBox("ContactPersonEmail", "", new { @id = "Edit_Agency_ContactPersonEmail", @class = "text required input_wrapper", @maxlength = "20", @tabindex = "14" })%></div></div>
            <div class="row"><label for="ContactPhoneArray">Contact Person Phone:</label><div class="float_right"><input type="text" class="autotext required numeric phone_short" name="ContactPhoneArray" id="Edit_Agency_ContactPhone1" maxlength="3" tabindex="15" /> - <input type="text" class="autotext required numeric phone_short" name="ContactPhoneArray" id="Edit_Agency_ContactPhone2" maxlength="3" tabindex="16" /> - <input type="text" class="autotext required numeric phone_long" name="ContactPhoneArray" id="Edit_Agency_ContactPhone3" maxlength="4" tabindex="17" /></div></div>
        </div><div class="column">
            <div class="row"><label for="NationalProviderNumber">National Provider Number:</label><div class="float_right"><%=Html.TextBox("NationalProviderNumber", " ", new { @id = "Edit_Agency_NationalProviderNo", @maxlength = "10", @class = "text input_wrapper", @tabindex = "9" })%></div></div>
            <div class="row"><label for="MedicareProviderNumber">Medicare Provider Number:</label><div class="float_right"><%=Html.TextBox("MedicareProviderNumber", " ", new { @id = "Edit_Agency_MedicareProviderNo", @maxlength = "10", @class = "text input_wrapper", @tabindex = "10" })%></div></div>
            <div class="row"><label for="MedicaidProviderNumber">Medicaid Provider Number:</label><div class="float_right"><%=Html.TextBox("MedicaidProviderNumber", " ", new { @id = "Edit_Agency_MedicaidProviderNo", @maxlength = "10", @class = "text input_wrapper", @tabindex = "11" })%></div></div>
            <div class="row"><label for="HomeHealthAgencyId">Unique HHA Agency ID Code:</label><div class="float_right"><%=Html.TextBox("HomeHealthAgencyId", " ", new { @id = "Edit_Agency_HomeHealthAgencyId", @maxlength = "10", @class = "text input_wrapper", @tabindex = "12" })%></div></div>
            <div class="row">&nbsp;</div><div class="row">&nbsp;</div>
            <div class="row"><label for="ContactPersonFirstName">Contact Person First Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonFirstName", "", new { @id = "Edit_Agency_ContactPersonFirstName", @maxlength = "50", @class = "text required names input_wrapper", @tabindex = "18" })%></div></div>
            <div class="row"><label for="ContactPersonLastName">Contact Person Last Name:</label><div class="float_right"><%=Html.TextBox("ContactPersonLastName", "", new { @id = "Edit_Agency_ContactPersonLastName", @class = "text required names input_wrapper", @maxlength = "20", @tabindex = "19" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Location Information</legend>
        <div class="column">
            <div class="row"><label for="AddressLine1">Address Line 1:</label><div class="float_right"><%=Html.TextBox("AddressLine1", "", new { @id = "Edit_Agency_AddressLine1", @maxlength = "20", @class = "text required input_wrapper", @tabindex = "20" })%></div></div>
            <div class="row"><label for="AddressCity">City:</label><div class="float_right"><%=Html.TextBox("AddressCity", "", new { @id = "Edit_Agency_AddressCity", @maxlength = "20", @class = "text required input_wrapper", @tabindex = "21" })%></div></div>
            <div class="row"><label for="AddressZipCode">State, Zip Code:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "Edit_Agency_AddressStateCode", @class = "AddressStateCode required valid" }) %><%=Html.TextBox("AddressZipCode", "", new { @id = "Edit_Agency_AddressZipCode", @class = "text numeric required zip", @maxlength = "5", @tabindex = "22" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="AddressLine2">Address Line 2:</label><div class="float_right"><%=Html.TextBox("AddressLine2", "", new { @id = "Edit_Agency_AddressLine2", @maxlength = "20", @class = "text input_wrapper", @tabindex = "23" })%></div></div>
            <div class="row"><label for="PhoneArray">Phone:</label><div class="float_right"><input type="text" class="autotext numeric required phone_short" name="PhoneArray" id="Edit_Agency_Phone1" maxlength="3" tabindex="25" /> - <input type="text" class="autotext numeric required phone_short" name="PhoneArray" id="Edit_Agency_Phone2" maxlength="3" tabindex="26" /> - <input type="text" class="autotext numeric required phone_long" name="PhoneArray" id="Edit_Agency_Phone3" maxlength="4" tabindex="27" /></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editagency');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
