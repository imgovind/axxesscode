﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('List Insurances/Payors | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','listinsurances');</script>")%>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyInsurance>().Name("List_AgencyInsurance").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(c => c.Name).Title("Insurance Name").Width(150).Sortable(false);
    columns.Bound(c => c.PayerTypeName).Title("Payer Type").Sortable(false);
    columns.Bound(c => c.PayorId).Title("Payor Id").Sortable(false);
    columns.Bound(c => c.InvoiceTypeName).Title("Invoice Type").Sortable(false);
    columns.Bound(c => c.HealthPlanId).Title("Health Plan Id").Sortable(false);
    columns.Bound(c => c.PhoneNumber).Title("Phone").Width(120);
    columns.Bound(c => c.ContactPerson).Title("Contact Person").Sortable(false);
    columns.Bound(c => c.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditInsurance('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Insurance.Delete('<#=Id#>');\" class=\"deleteContact\">Delete</a>").Title("Action").Width(100);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Insurance")).Pageable(paging => paging.PageSize(10)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $("#List_AgencyInsurance .t-grid-toolbar").html("");
<% if (Current.HasRight(Permissions.ManageInsurance)) { %>
    $("#List_AgencyInsurance .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewInsurance(); return false;%22%3ENew Insurance%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
<% } %>
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    $("#List_AgencyInsurance .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExcel Export%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
<% } %>
    $(".t-grid-content").css({ 'height': 'auto' });
</script>

