﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyPhysician>" %>
<% using (Html.BeginForm("Physicians", "Export", FormMethod.Post)) { %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('List Physicians | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','listphysicians');</script>")%>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyPhysician>().Name("List_Physician").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(p => p.NPI).Title("NPI").Sortable(true).Width(95);
    columns.Bound(p => p.DisplayName).Title("Name").Sortable(false).Width(170);
    columns.Bound(p => p.EmailAddress).Title("Email").Sortable(false).Width(130);
    columns.Bound(p => p.AddressFull).Title("Address").Sortable(true);
    columns.Bound(p => p.PhoneWorkFormatted).Title("Phone Number").Width(110);
    columns.Bound(p => p.FaxNumberFormatted).Title("Fax Number").Width(110);
    columns.Bound(p => p.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPhysician('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Physician.Delete('<#=Id#>');\" class=\"\">Delete</a>").Title("Action").Width(100);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Physician")).Pageable(paging => paging.PageSize(15)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
    $("#List_Physician .t-grid-toolbar").html("");
<% if (Current.HasRight(Permissions.ManagePhysicians)) { %>
    $("#List_Physician .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewPhysician(); return false;%22%3ENew Physician%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
<% } %>
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    $("#List_Physician .t-grid-toolbar").append(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExcel Export%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
<% } %>
    $(".t-grid-content").css({ 'height': 'auto' });
</script>
    
