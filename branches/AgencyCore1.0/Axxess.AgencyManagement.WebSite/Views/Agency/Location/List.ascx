﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('List Locations | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','listlocations');</script>")%>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyLocation>().Name("List_Location").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(l => l.Name).Title("Company Name").Sortable(false);
    columns.Bound(l => l.MedicareProviderNumber).Title("Provider Number").Sortable(false).Width(150);
    columns.Bound(l => l.AddressFull).Title("Address").Sortable(true);
    columns.Bound(l => l.PhoneWork).Title("Phone Number").Width(120);
    columns.Bound(l => l.FaxNumber).Title("Fax Number").Width(120);
    columns.Bound(l => l.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditLocation('<#=Id#>');\">Edit</a>").Title("Action").Width(100);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Location")).Pageable(paging => paging.PageSize(10)).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $("#List_Location .t-grid-toolbar").html(unescape("%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewLocation(); return false;%22%3ENew Branch%3C/a%3E%3C/li%3E%3C/ul%3E%3Cul class=%22float_right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExcel Export%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
    $(".t-grid-content").css({ 'height': 'auto' });
</script>
