﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<table class="claim">
    <thead class="align_center"><tr>
        <th><label>Group By:</label></th>
        <th><div class="buttons"><a class="groupLink" name="" type="" onclick="Agency.loadCaseManagement('PatientName');">Patient</a></div></th>
        <th><div class="buttons"><a class="groupLink" name="" type="" onclick="Agency.loadCaseManagement('EventDate');">Date</a></div></th>
        <th><div class="buttons"><a class="groupLink" name="" type="" onclick="Agency.loadCaseManagement('DisciplineTaskName');">Task</a></div></th>
        <th><div class="buttons"><a class="groupLink" name="" type="" onclick="Agency.loadCaseManagement('UserName');">Clinician</a></div></th>
        </tr></thead>
</table>
<div id="caseManagementContentId"> <% Html.RenderPartial("~/Views/Agency/CaseManagementContent.ascx", Model); %></div>