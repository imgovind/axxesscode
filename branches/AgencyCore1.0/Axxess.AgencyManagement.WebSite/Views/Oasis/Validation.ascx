﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ValidationInfoViewData>" %>
<div class="wrapper main">
<% var dictonary = new Dictionary<string, string>() { { AssessmentType.StartOfCare.ToString(), "startofcare" }, { AssessmentType.Recertification.ToString(), "recertification" }, { AssessmentType.ResumptionOfCare.ToString(), "resumptionofcare" }, { AssessmentType.FollowUp.ToString(), "followup" }, { AssessmentType.DischargeFromAgency.ToString(), "discharge" }, { AssessmentType.DischargeFromAgencyDeath.ToString(), "deathathome" }, { AssessmentType.TransferInPatientDischarged.ToString(), "transferfordischarge" }, { AssessmentType.TransferInPatientNotDischarged.ToString(), "transfernotdischarge" } }; %>
    <table id="validationErrorTable" class="oasis_val layout_auto">
        <tbody>
            <tr>
                <th colspan="3" class="align_center largefont">You have <strong><%=Model.validationError.Where(e => e.ErrorType == "WARNING").Count()%> warning<%= Model.validationError.Where(e => e.ErrorType == "WARNING").Count() != 1 ? "s" : "" %></strong> and <strong><%= Model.validationError.Where(e => e.ErrorType == "ERROR").Count() %> error<%= Model.validationError.Where(e => e.ErrorType == "ERROR").Count() != 1 ? "s" : "" %></strong>.</th>
            </tr><%
    if (Model.validationError != null) {
        foreach (var data in Model.validationError) { %>
            <%= data.ErrorType == "FATAL" ? "<tr class='red'>" : "<tr>" %>
                <td class='align_center'><%= data.ErrorType == "ERROR" || data.ErrorType == "FATAL" ? "<img src='/Images/icons/error.png'>" : "<img src='/Images/icons/warning.png'>"%></td>
                <td><%= data.ErrorDup %></td>
                <td><%= data.Description %></td>
            </tr><%
        }
    } %>
        </tbody>
    </table><%
    if (Model.Count == 0) { %>
    <table class="oasis_val layout_auto">
        <tbody>
            <tr>
                <td class="align_center largefont full-width">
                    <div class="row"><span class="hipps">HIPPS Code:</span><strong><%= Model.HIPPSCODE %></strong></div>
                    <div class="row"><span class="hipps">OASIS Claim Matching Key:</span><strong><%= Model.HIPPSKEY %></strong></div>
                </td>                
            </tr>
        </tbody>
    </table><%
    } %>
</div>
<% if (Model.validationError != null && Model.validationError.Where(e => e.ErrorType == "ERROR").Count() == 0) { %>
<script type="text/javascript">
    $("#oasis_validation_controls ul").html(unescape("%3Cli%3E" +
        "<%= string.Format("%3Ca href=%22javascript:void(0);%22 onclick='Oasis.OasisStatusAction(%22{0}%22,%22{1}%22,%22{2}%22,%22{3}%22,%22Submit%22,%22{4}%22);'%3EFinish%3C/a%3E", Model.AssessmentId, Model.PatientId, Model.EpisodeId, Model.AssessmentType, dictonary[Model.AssessmentType])%>" +
        "%3C/li%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22  onclick=%22UserInterface.CloseOasisValidationModal();%22%3ECancel%3C/a%3E%3C/li%3E"));
</script><%
} %>