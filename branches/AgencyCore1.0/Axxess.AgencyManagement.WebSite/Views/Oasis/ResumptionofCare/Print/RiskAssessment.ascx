﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.span("(M1032) Risk for Hospitalization: Which of the following signs or symptoms characterize this patient as at risk for hospitalization?",true) +
        printview.checkbox("1 &ndash; Recent decline in mental, emotional, or behavioral status",<%= data != null && data.ContainsKey("M1032HospitalizationRiskRecentDecline") && data["M1032HospitalizationRiskRecentDecline"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Multiple hospitalizations (2 or more) in the past 12 months",<%= data != null && data.ContainsKey("M1032HospitalizationRiskMultipleHosp") && data["M1032HospitalizationRiskMultipleHosp"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; History of falls (2 or more falls&mdash; or any fall with an injury&mdash; in the past year)",<%= data != null && data.ContainsKey("M1032HospitalizationRiskHistoryOfFall") && data["M1032HospitalizationRiskHistoryOfFall"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Taking five or more medications",<%= data != null && data.ContainsKey("M1032HospitalizationRiskMedications") && data["M1032HospitalizationRiskMedications"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; Frailty indicators, e.g., weight loss, self-reported exhaustion",<%= data != null && data.ContainsKey("M1032HospitalizationRiskFrailty") && data["M1032HospitalizationRiskFrailty"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("6 &ndash; Other",<%= data != null && data.ContainsKey("M1032HospitalizationRiskOther") && data["M1032HospitalizationRiskOther"].Answer == "1" ? "true" : "false"%>) +
        printview.checkbox("7 &ndash; None of the above",<%= data != null && data.ContainsKey("M1032HospitalizationRiskNone") && data["M1032HospitalizationRiskNone"].Answer == "1" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1036) Risk Factors, either present or past, likely to affect current health status and/or outcome:",true) +
        printview.col(3,
            printview.checkbox("1 &ndash; Smoking",<%= data != null && data.ContainsKey("M1036RiskFactorsSmoking") && data["M1036RiskFactorsSmoking"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("2 &ndash; Obesity",<%= data != null && data.ContainsKey("M1036RiskFactorsObesity") && data["M1036RiskFactorsObesity"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("3 &ndash; Alcohol dependency",<%= data != null && data.ContainsKey("M1036RiskFactorsAlcoholism") && data["M1036RiskFactorsAlcoholism"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("4 &ndash; Drug dependency",<%= data != null && data.ContainsKey("M1036RiskFactorsDrugs") && data["M1036RiskFactorsDrugs"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("5 &ndash; None of the above",<%= data != null && data.ContainsKey("M1036RiskFactorsNone") && data["M1036RiskFactorsNone"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("UK &ndash; Unknown",<%= data != null && data.ContainsKey("M1036RiskFactorsUnknown") && data["M1036RiskFactorsUnknown"].Answer == "1" ? "true" : "false"%>)));
    printview.addsection(
        printview.span("(M1034) Overall Status: Which description best fits the patient&rsquo;s overall status?",true) +
        printview.checkbox("0 &ndash; The patient is stable with no heightened risk(s) for serious complications and death (beyond those typical of the patient&rsquo;s age).",<%= data != null && data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; The patient is temporarily facing high health risk(s) but is likely to return to being stable without heightened risk(s) for serious complications and death (beyond those typical of the patient&rsquo;s age).",<%= data != null && data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; The patient is likely to remain in fragile health and have ongoing high risk(s) of serious complications and death.",<%= data != null && data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; The patient has serious progressive conditions that could lead to death within a year.",<%= data != null && data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "03" ? "true" : "false"%>) +
        printview.checkbox("UK &ndash; The patient&rsquo;s situation is unknown or unclear.",<%= data != null && data.ContainsKey("M1034OverallStatus") && data["M1034OverallStatus"].Answer == "UK" ? "true" : "false"%>));
    printview.addsection(
        printview.checkbox("SN to assist patient to obtain ERS button.",<%= data != null && data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("SN to develop individualized emergency plan with patient.",<%= data != null && data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.checkbox("SN to instruct patient on importance of receiving influenza and pneumococcal vaccines.",<%= data != null && data.ContainsKey("485RiskAssessmentIntervention") && data["485RiskAssessmentIntervention"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485RiskInterventionComments") && data["485RiskInterventionComments"].Answer.IsNotNullOrEmpty() ? data["485RiskInterventionComments"].Answer : ""%>",false,2),
        "Interventions");
    printview.addsection(
        printview.checkbox("The patient will have no hospitalizations during the episode.",<%= data != null && data.ContainsKey("485RiskAssessmentGoals") && data["485RiskAssessmentGoals"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.checkbox("The <%= data != null && data.ContainsKey("485VerbalizeEmergencyPlanPerson") && data["485VerbalizeEmergencyPlanPerson"].Answer.IsNotNullOrEmpty() ? data["485VerbalizeEmergencyPlanPerson"].Answer : "<span class='blank'></span>"%> will verbalize understanding of individualized emergency plan by the end of the episode.",<%= data != null && data.ContainsKey("485RiskAssessmentGoals") && data["485RiskAssessmentGoals"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
        printview.span("Additional Goals:",true) +
        printview.span("<%= data != null && data.ContainsKey("485RiskGoalComments") && data["485RiskGoalComments"].Answer.IsNotNullOrEmpty() ? data["485RiskGoalComments"].Answer : ""%>",false,2),
        "Goals");
</script>