﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisResumptionOfCareEliminationForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("ResumptionOfCare_Id", Model.Id) %>
<%= Html.Hidden("ResumptionOfCare_Action", "Edit") %>
<%= Html.Hidden("ResumptionOfCare_PatientGuid", Model.PatientId) %>
<%= Html.Hidden("ResumptionOfCare_EpisodeId", Model.EpisodeId) %>
<%= Html.Hidden("assessment", "ResumptionOfCare") %>
<%= Html.Hidden("categoryType", "Elimination") %>
<div class="wrapper main">
    <fieldset class="half float_left">
        <legend>GU</legend>
        <input type="hidden" name="ResumptionOfCare_GenericGU" value=" " />
        <%string[] genericGU = data.ContainsKey("GenericGU") && data["GenericGU"].Answer != "" ? data["GenericGU"].Answer.Split(',') : null; %>
        <div class="column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericGU1' class='radio float_left' name='ResumptionOfCare_GenericGU' value='1' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericGU1" class="radio">WNL (Within Normal Limits)</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericGU2' class='radio float_left' name='ResumptionOfCare_GenericGU' value='2' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("2") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericGU2" class="radio">Incontinence</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericGU3' class='radio float_left' name='ResumptionOfCare_GenericGU' value='3' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("3") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericGU3" class="radio">Bladder Distention</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericGU4' class='radio float_left' name='ResumptionOfCare_GenericGU' value='4' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericGU4" class="radio">Discharge</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericGU5' class='radio float_left' name='ResumptionOfCare_GenericGU' value='5' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericGU5" class="radio">Frequency</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericGU6' class='radio float_left' name='ResumptionOfCare_GenericGU' value='6' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericGU6" class="radio">Dysuria</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericGU7' class='radio float_left' name='ResumptionOfCare_GenericGU' value='7' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericGU7" class="radio">Retention</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericGU8' class='radio float_left' name='ResumptionOfCare_GenericGU' value='8' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("8") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericGU8" class="radio">Urgency</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericGU9' class='radio float_left' name='ResumptionOfCare_GenericGU' value='9' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("9") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericGU9" class="radio">Oliguria</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericGU10' class='radio float_left' name='ResumptionOfCare_GenericGU' value='10' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("10") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericGU10" class="radio">Catheter/Device:</label>
                </div><div id="ResumptionOfCare_GenericGU10More" class="rel float_right">
                    <div class="float_right">
                        <%  var genericGUCatheterList = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "N/A", Value = "1" },
                                new SelectListItem { Text = "Foley Catheter ", Value = "2" },
                                new SelectListItem { Text = "Condom Catheter", Value = "3" },
                                new SelectListItem { Text = "Suprapubic Catheter", Value = "4" },
                                new SelectListItem { Text = "Urostomy", Value = "5" },
                                new SelectListItem { Text = "Other", Value = "6" }
                            }, "Value", "Text", data.ContainsKey("GenericGUCatheterList") && data["GenericGUCatheterList"].Answer != "" ? data["GenericGUCatheterList"].Answer : "0"); %>
                        <%= Html.DropDownList("ResumptionOfCare_GenericGUCatheterList", genericGUCatheterList) %>
                    </div>
                    <div class="clear"></div>
                    <div class="float_right"><label for="ResumptionOfCare_GenericGUCatheterLastChanged">Last Changed</label><%=Html.TextBox("ResumptionOfCare_GenericGUCatheterLastChanged", data.ContainsKey("GenericGUCatheterLastChanged") ? data["GenericGUCatheterLastChanged"].Answer : "", new { @id = "ResumptionOfCare_GenericGUCatheterLastChanged", @class = "st", @maxlength = "10" })%></div>
                    <div class="clear"></div>
                    <div class="float_right">
                        <%=Html.TextBox("ResumptionOfCare_GenericGUCatheterFrequency", data.ContainsKey("GenericGUCatheterFrequency") ? data["GenericGUCatheterFrequency"].Answer : "", new { @id = "ResumptionOfCare_GenericGUCatheterFrequency", @class = "vitals", @maxlength = "5" })%>
                        <label for="ResumptionOfCare_GenericGUCatheterFrequency">Fr</label>
                        <%=Html.TextBox("ResumptionOfCare_GenericGUCatheterAmount", data.ContainsKey("GenericGUCatheterAmount") ? data["GenericGUCatheterAmount"].Answer : "", new { @id = "ResumptionOfCare_GenericGUCatheterAmount", @class = "vitals", @maxlength = "5" })%>
                        <label for="ResumptionOfCare_GenericGUCatheterAmount">cc</label>
                    </div>
                </div>
            </div><div class="row">
                <div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericGU11' class='radio float_left' name='ResumptionOfCare_GenericGU' value='11' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("11") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericGU11" class="radio">Urine:</label>
                </div><div id="ResumptionOfCare_GenericGU11More" class="rel float_right">
                    <div class="float_left">
                        <% string[] genericGUUrine = data.ContainsKey("GenericGUUrine") && data["GenericGUUrine"].Answer != "" ? data["GenericGUUrine"].Answer.Split(',') : null; %>
                        <input type="hidden" name="ResumptionOfCare_GenericGUUrine" value=" " />
                        <%= string.Format("<input id='ResumptionOfCare_GenericGUUrine1' class='radio float_left' name='ResumptionOfCare_GenericGUUrine' value='1' type='checkbox' {0} />", genericGUUrine!=null && genericGUUrine.Contains("1") ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericGUUrine1" class="fixed inlineradio">Cloudy</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericGUUrine2' class='radio float_left' name='ResumptionOfCare_GenericGUUrine' value='2' type='checkbox' {0} />", genericGUUrine!=null && genericGUUrine.Contains("2") ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericGUUrine2" class="fixed inlineradio">Odorous</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericGUUrine3' class='radio float_left' name='ResumptionOfCare_GenericGUUrine' value='3' type='checkbox' {0} />", genericGUUrine!=null && genericGUUrine.Contains("3") ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericGUUrine3" class="fixed inlineradio">Sediment</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericGUUrine4' class='radio float_left' name='ResumptionOfCare_GenericGUUrine' value='4' type='checkbox' {0} />", genericGUUrine!=null && genericGUUrine.Contains("4") ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericGUUrine4" class="fixed inlineradio">Hematuria</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericGUUrine5' class='radio float_left' name='ResumptionOfCare_GenericGUUrine' value='5' type='checkbox' {0} />", genericGUUrine!=null && genericGUUrine.Contains("5") ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericGUUrine5" class="fixed inlineradio">Other</label>
                    </div><div class="float_left">
                        <%=Html.TextBox("ResumptionOfCare_GenericGUOtherText", data.ContainsKey("GenericGUOtherText") ? data["GenericGUOtherText"].Answer : "", new { @id = "ResumptionOfCare_GenericGUOtherText", @class = "st", @maxlength = "20" })%>
                    </div>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericGU12' class='radio float_left' name='ResumptionOfCare_GenericGU' value='12' type='checkbox' {0} />", genericGU!=null && genericGU.Contains("12") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericGU12" class="radio">External Genitalia:</label>
                </div><div id="ResumptionOfCare_GenericGU12More" class="rel float_right">
                    <div class="float_right"><%=Html.Hidden("ResumptionOfCare_GenericGUNormal")%>
                        <%=Html.RadioButton("ResumptionOfCare_GenericGUNormal", "1", data.ContainsKey("GenericGUNormal") && data["GenericGUNormal"].Answer == "1" ? true : false, new { @id = "ResumptionOfCare_GenericGUNormal1", @class = "radio" })%>
                        <label for="ResumptionOfCare_GenericGUNormal1" class="inlineradio">Normal</label>
                        <%=Html.RadioButton("ResumptionOfCare_GenericGUNormal", "0", data.ContainsKey("GenericGUNormal") && data["GenericGUNormal"].Answer == "0" ? true : false, new { @id = "ResumptionOfCare_GenericGUNormal0", @class = "radio" })%>
                        <label for="ResumptionOfCare_GenericGUNormal0" class="inlineradio">Abnormal</label>
                    </div>
                    <div class="clear"></div>
                    <%=Html.Hidden("ResumptionOfCare_GenericGUClinicalAssessment")%>
                    <div class="float_right">
                        <label>As per:</label>
                        <div class="margin">
                        <%=Html.RadioButton("ResumptionOfCare_GenericGUClinicalAssessment", "1", data.ContainsKey("GenericGUClinicalAssessment") && data["GenericGUClinicalAssessment"].Answer == "1" ? true : false, new { @id = "ResumptionOfCare_GenericGUClinicalAssessment1", @class = "radio" })%>
                        <label for="ResumptionOfCare_GenericGUClinicalAssessment1" class="inlineradio">Clinician Assessment</label><div class="clear"></div>
                        <%=Html.RadioButton("ResumptionOfCare_GenericGUClinicalAssessment", "0", data.ContainsKey("GenericGUClinicalAssessment") && data["GenericGUClinicalAssessment"].Answer == "0" ? true : false, new { @id = "ResumptionOfCare_GenericGUClinicalAssessment0", @class = "radio" })%>
                        <label for="ResumptionOfCare_GenericGUClinicalAssessment0" class="inlineradio">Pt/CG Report</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Dialysis</legend>
        <div class="column">
            <div class="row">
                <label class="float_left">Is patient on dialysis?</label>
                <%=Html.Hidden("ResumptionOfCare_GenericPatientOnDialysis")%>
                <div class="float_right">
                    <%=Html.RadioButton("ResumptionOfCare_GenericPatientOnDialysis", "1", data.ContainsKey("GenericPatientOnDialysis") && data["GenericPatientOnDialysis"].Answer == "1" ? true : false, new { @id = "ResumptionOfCare_GenericPatientOnDialysis1", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericPatientOnDialysis1" class="inlineradio">Yes</label>
                    <%=Html.RadioButton("ResumptionOfCare_GenericPatientOnDialysis", "0", data.ContainsKey("GenericPatientOnDialysis") && data["GenericPatientOnDialysis"].Answer == "0" ? true : false, new { @id = "ResumptionOfCare_GenericPatientOnDialysis0", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericPatientOnDialysis0" class="inlineradio">No</label>
                    <% string[] genericDialysis = data.ContainsKey("GenericDialysis") && data["GenericDialysis"].Answer != "" ? data["GenericDialysis"].Answer.Split(',') : null; %>
                    <input type="hidden" name="ResumptionOfCare_GenericDialysis" value=" " />
                </div>
            </div>
        </div><div class="column">
            <div class="DialysisSpecifics row">
                <div class="row">
                    <label class="float_left">Dialysis Type</label>
                </div><div class="row">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDialysis1' class='radio float_left' name='ResumptionOfCare_GenericDialysis' value='1' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDialysis1" class="radio">Peritoneal Dialysis</label>
                </div><div class="row">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDialysis2' class='radio float_left' name='ResumptionOfCare_GenericDialysis' value='2' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDialysis2" class="radio">CCPD (Continuous Cyclic Peritoneal Dialysis)</label>
                </div><div class="row">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDialysis3' class='radio float_left' name='ResumptionOfCare_GenericDialysis' value='3' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDialysis3" class="radio">IPD (Intermittent Peritoneal Dialysis)</label>
                </div><div class="row">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDialysis4' class='radio float_left' name='ResumptionOfCare_GenericDialysis' value='4' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDialysis4" class="radio">CAPD (Continuous Ambulatory Peritoneal Dialysis)</label>
                </div><div class="row">
                    <div>
                        <%= string.Format("<input id='ResumptionOfCare_GenericDialysis5' class='radio float_left' name='ResumptionOfCare_GenericDialysis' value='5' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("5") ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericDialysis5" class="radio">Hemodialysis</label>
                    </div><div id="ResumptionOfCare_GenericDialysis5More" class="float_right rel margin">
                        <% string[] genericDialysisHemodialysis = data.ContainsKey("GenericDialysisHemodialysis") && data["GenericDialysisHemodialysis"].Answer != "" ? data["GenericDialysisHemodialysis"].Answer.Split(',') : null; %>
                        <input type="hidden" name="ResumptionOfCare_GenericDialysisHemodialysis" value=" " />
                        <div class="float_left">
                            <%= string.Format("<input id='ResumptionOfCare_GenericDialysisHemodialysis1' class='radio float_left' name='ResumptionOfCare_GenericDialysisHemodialysis' value='1' type='checkbox' {0} />", genericDialysisHemodialysis != null && genericDialysisHemodialysis.Contains("1") ? "checked='checked'" : "")%>
                            <label for="ResumptionOfCare_GenericDialysisHemodialysis1" class="radio">AV Graft/ Fistula Site:</label>
                        </div><div class="float_right">
                            <%=Html.TextBox("ResumptionOfCare_GenericDialysisHemodialysisGriftAV", data.ContainsKey("GenericDialysisHemodialysisGriftAV") ? data["GenericDialysisHemodialysisGriftAV"].Answer : "", new { @id = "ResumptionOfCare_GenericDialysisHemodialysisGriftAV", @maxlength = "25" })%>
                        </div>
                        <div class="clear"></div>
                        <div class="float_left">
                            <%= string.Format("<input id='ResumptionOfCare_GenericDialysisHemodialysis2' class='radio float_left' name='ResumptionOfCare_GenericDialysisHemodialysis' value='2' type='checkbox' {0} />", genericDialysisHemodialysis != null && genericDialysisHemodialysis.Contains("2") ? "checked='checked'" : "")%>
                            <label for="ResumptionOfCare_GenericDialysisHemodialysis2" class="radio">Central Venous Catheter Access Site:</label>
                        </div><div class="float_right">
                            <%=Html.TextBox("ResumptionOfCare_GenericDialysisHemodialysisCentralVenousAV", data.ContainsKey("GenericDialysisHemodialysisCentralVenousAV") ? data["GenericDialysisHemodialysisCentralVenousAV"].Answer : "", new { @id = "ResumptionOfCare_GenericDialysisHemodialysisCentralVenousAV", @maxlength = "25" })%>
                        </div>
                    </div>
                </div><div class="row">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDialysis6' class='radio float_left' name='ResumptionOfCare_GenericDialysis' value='6' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("6") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDialysis6" class="radio">Catheter site free from signs and symptoms of infection</label>
                </div><div class="row">
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericDialysis7' class='radio float_left' name='ResumptionOfCare_GenericDialysis' value='7' type='checkbox' {0} />", genericDialysis!=null && genericDialysis.Contains("7") ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericDialysis7" class="radio">Other:</label>
                    </div><div class="float_right">
                        <%=Html.TextBox("ResumptionOfCare_GenericDialysisOtherDesc", data.ContainsKey("GenericDialysisOtherDesc") ? data["GenericDialysisOtherDesc"].Answer : "", new { @id = "ResumptionOfCare_GenericDialysisOtherDesc", @maxlength = "25" })%>
                    </div>
                </div><div class="row">
                    <label for="ResumptionOfCare_GenericDialysisCenter" class="strong">Dialysis Center:</label>
                    <%=Html.TextArea("ResumptionOfCare_GenericDialysisCenter", data.ContainsKey("GenericDialysisCenter") ? data["GenericDialysisCenter"].Answer : "", 3,100,new { @id = "ResumptionOfCare_GenericDialysisCenter", @maxlength = "25" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="oasis">
        <legend>OASIS M1600/M1610</legend>
        <div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1600');">(M1600)</a> Has this patient been treated for a Urinary Tract Infection in the past 14 days?<%=Html.Hidden("ResumptionOfCare_M1600UrinaryTractInfection", " ", new { @id = "" })%>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1600UrinaryTractInfection", "00", data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1600UrinaryTractInfection0", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1600UrinaryTractInfection0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1600UrinaryTractInfection", "01", data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1600UrinaryTractInfection1", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1600UrinaryTractInfection1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1600UrinaryTractInfection", "NA", data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "NA" ? true : false, new { @id = "ResumptionOfCare_M1600UrinaryTractInfectionNA", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1600UrinaryTractInfectionNA"><span class="float_left">NA &ndash;</span><span class="normal margin">Patient on prophylactic treatment</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1600');">?</div>
                    </div>
                    <%=Html.RadioButton("ResumptionOfCare_M1600UrinaryTractInfection", "UK", data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "UK" ? true : false, new { @id = "ResumptionOfCare_M1600UrinaryTractInfectionUK", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1600UrinaryTractInfectionUK"><span class="float_left">UK &ndash;</span><span class="normal margin">Unknown</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1610');">(M1610)</a> Urinary Incontinence or Urinary Catheter Presence:<%=Html.Hidden("ResumptionOfCare_M1610UrinaryIncontinence", " ", new { @id = "" })%>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1610UrinaryIncontinence", "00", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1610UrinaryIncontinence0", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1610UrinaryIncontinence0"><span class="float_left">0 &ndash;</span><span class="normal margin">No incontinence or catheter (includes anuria or ostomy for urinary drainage)</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1610UrinaryIncontinence", "01", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1610UrinaryIncontinence1", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1610UrinaryIncontinence1"><span class="float_left">1 &ndash;</span><span class="normal margin">Patient is incontinent</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1610');">?</div>
                    </div>
                    <%=Html.RadioButton("ResumptionOfCare_M1610UrinaryIncontinence", "02", data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1610UrinaryIncontinence2", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1610UrinaryIncontinence2"><span class="float_left">2 &ndash;</span><span class="normal margin">Patient requires a urinary catheter (i.e., external, indwelling, intermittent, suprapubic)</span></label>
                </div>
            </div>
        </div>
    </fieldset><fieldset id="ROC_M1615" class="oasis">
        <legend>OASIS M1615</legend>
        <div class="wide_column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1615');">(M1615)</a> When does Urinary Incontinence occur?
                    <%=Html.Hidden("ResumptionOfCare_M1615UrinaryIncontinenceOccur")%>
                </div><div class="margin">
                    <div>
                        <%=Html.RadioButton("ResumptionOfCare_M1615UrinaryIncontinenceOccur", "00", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1615UrinaryIncontinenceOccur0", @class = "radio float_left" })%>
                        <label for="ResumptionOfCare_M1615UrinaryIncontinenceOccur0"><span class="float_left">0 &ndash;</span><span class="normal margin">Timed-voiding defers incontinence</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("ResumptionOfCare_M1615UrinaryIncontinenceOccur", "01", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1615UrinaryIncontinenceOccur1", @class = "radio float_left" })%>
                        <label for="ResumptionOfCare_M1615UrinaryIncontinenceOccur1"><span class="float_left">1 &ndash;</span><span class="normal margin">Occasional stress incontinence</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("ResumptionOfCare_M1615UrinaryIncontinenceOccur", "02", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1615UrinaryIncontinenceOccur2", @class = "radio float_left" })%>
                        <label for="ResumptionOfCare_M1615UrinaryIncontinenceOccur2"><span class="float_left">2 &ndash;</span><span class="normal margin">During the night only</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <%=Html.RadioButton("ResumptionOfCare_M1615UrinaryIncontinenceOccur", "03", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "03" ? true : false, new { @id = "ResumptionOfCare_M1615UrinaryIncontinenceOccur3", @class = "radio float_left" })%>
                        <label for="ResumptionOfCare_M1615UrinaryIncontinenceOccur3"><span class="float_left">3 &ndash;</span><span class="normal margin">During the day only</span></label>
                        <div class="clear"></div>
                    </div><div>
                        <div class="float_right oasis">
                            <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1615');">?</div>
                        </div>
                        <%=Html.RadioButton("ResumptionOfCare_M1615UrinaryIncontinenceOccur", "04", data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "04" ? true : false, new { @id = "ResumptionOfCare_M1615UrinaryIncontinenceOccur4", @class = "radio float_left" })%>
                        <label for="ResumptionOfCare_M1615UrinaryIncontinenceOccur4"><span class="float_left">4 &ndash;</span><span class="normal margin">During the day and night</span></label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>OASIS M1620/M1630</legend>
        <div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1620');">(M1620)</a> Bowel Incontinence Frequency:
                    <%=Html.Hidden("ResumptionOfCare_M1620BowelIncontinenceFrequency")%>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1620BowelIncontinenceFrequency", "00", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1620BowelIncontinenceFrequency0", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1620BowelIncontinenceFrequency0"><span class="float_left">0 &ndash;</span><span class="normal margin">Very rarely or never has bowel incontinence</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1620BowelIncontinenceFrequency", "01", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1620BowelIncontinenceFrequency1", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1620BowelIncontinenceFrequency1"><span class="float_left">1 &ndash;</span><span class="normal margin">Less than once weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1620BowelIncontinenceFrequency", "02", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1620BowelIncontinenceFrequency2", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1620BowelIncontinenceFrequency2"><span class="float_left">2 &ndash;</span><span class="normal margin">One to three times weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1620BowelIncontinenceFrequency", "03", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "03" ? true : false, new { @id = "ResumptionOfCare_M1620BowelIncontinenceFrequency3", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1620BowelIncontinenceFrequency3"><span class="float_left">3 &ndash;</span><span class="normal margin">Four to six times weekly</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1620BowelIncontinenceFrequency", "04", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "04" ? true : false, new { @id = "ResumptionOfCare_M1620BowelIncontinenceFrequency4", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1620BowelIncontinenceFrequency4"><span class="float_left">4 &ndash;</span><span class="normal margin">On a daily basis</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1620BowelIncontinenceFrequency", "05", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "05" ? true : false, new { @id = "ResumptionOfCare_M1620BowelIncontinenceFrequency5", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1620BowelIncontinenceFrequency5"><span class="float_left">5 &ndash;</span><span class="normal margin">More often than once daily</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1620BowelIncontinenceFrequency", "NA", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "NA" ? true : false, new { @id = "ResumptionOfCare_M1620BowelIncontinenceFrequencyNA", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1620BowelIncontinenceFrequencyNA"><span class="float_left">NA &ndash; </span><span class="normal margin">Patient has ostomy for bowel elimination</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1620');">?</div>
                    </div>
                    <%=Html.RadioButton("ResumptionOfCare_M1620BowelIncontinenceFrequency", "UK", data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "UK" ? true : false, new { @id = "ResumptionOfCare_M1620BowelIncontinenceFrequencyUK", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1620BowelIncontinenceFrequencyUK"><span class="float_left">UK &ndash;</span><span class="normal margin">Unknown</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="strong">
                    <a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1630');">(M1630)</a> Ostomy for Bowel Elimination: Does this patient have an ostomy for bowel elimination that (within the last 14 days): a) was related to an inpatient facility stay, or b) necessitated a change in medical or treatment regimen?
                    <%=Html.Hidden("ResumptionOfCare_M1630OstomyBowelElimination")%>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1630OstomyBowelElimination", "00", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "00" ? true : false, new { @id = "ResumptionOfCare_M1630OstomyBowelElimination0", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1630OstomyBowelElimination0"><span class="float_left">0 &ndash;</span><span class="normal margin">Patient does not have an ostomy for bowel elimination.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("ResumptionOfCare_M1630OstomyBowelElimination", "01", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "01" ? true : false, new { @id = "ResumptionOfCare_M1630OstomyBowelElimination1", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1630OstomyBowelElimination1"><span class="float_left">1 &ndash;</span><span class="normal margin">Patient&rsquo;s ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1630');">?</div>
                    </div>
                    <%=Html.RadioButton("ResumptionOfCare_M1630OstomyBowelElimination", "02", data.ContainsKey("M1630OstomyBowelElimination") && data["M1630OstomyBowelElimination"].Answer == "02" ? true : false, new { @id = "ResumptionOfCare_M1630OstomyBowelElimination2", @class = "radio float_left" })%>
                    <label for="ResumptionOfCare_M1630OstomyBowelElimination2"><span class="float_left">2 &ndash;</span><span class="normal margin">The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen.</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>GI</legend>
        <% string[] genericDigestive = data.ContainsKey("GenericDigestive") && data["GenericDigestive"].Answer != "" ? data["GenericDigestive"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_GenericDigestive" name=" " />
        <div class="column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericDigestive1' class='radio float_left' name='ResumptionOfCare_GenericDigestive' value='1' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("1") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericDigestive1" class="radio">WNL (Within Normal Limits)</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDigestive2' class='radio float_left' name='ResumptionOfCare_GenericDigestive' value='2' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDigestive2" class="radio">Bowel Sounds:</label>
                </div><div class="float_right">
                    <%  var bowelSounds = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Present/WNL", Value = "1" },
                            new SelectListItem { Text = "Hyperactive", Value = "2" },
                            new SelectListItem { Text = "Hypoactive", Value = "3" },
                            new SelectListItem { Text = "Absent", Value = "4" }
                        }, "Value", "Text", data.ContainsKey("GenericDigestiveBowelSoundsType") ? data["GenericDigestiveBowelSoundsType"].Answer : "0");%>
                    <%= Html.DropDownList("ResumptionOfCare_GenericDigestiveBowelSoundsType", bowelSounds, new { @id = "ResumptionOfCare_GenericDigestiveBowelSoundsType" })%>
                </div>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDigestive3' class='radio float_left' name='ResumptionOfCare_GenericDigestive' value='3' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDigestive3" class="radio">Abdominal Palpation:</label>
                </div><div class="float_right">
                    <%  var abdominalPalpation = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "Soft/WNL", Value = "1" },
                            new SelectListItem { Text = "Firm", Value = "2" },
                            new SelectListItem { Text = "Tender", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.ContainsKey("GenericAbdominalPalpation") ? data["GenericAbdominalPalpation"].Answer : "0");%>
                    <%= Html.DropDownList("ResumptionOfCare_GenericAbdominalPalpation", abdominalPalpation, new { @id = "ResumptionOfCare_GenericAbdominalPalpation" })%>
                </div>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericDigestive4' class='radio float_left' name='ResumptionOfCare_GenericDigestive' value='4' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("4") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericDigestive4" class="radio">Bowel Incontinence</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericDigestive5' class='radio float_left' name='ResumptionOfCare_GenericDigestive' value='5' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("5") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericDigestive5" class="radio">Nausea</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericDigestive6' class='radio float_left' name='ResumptionOfCare_GenericDigestive' value='6' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("6") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericDigestive6" class="radio">Vomiting</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_GenericDigestive7' class='radio float_left' name='ResumptionOfCare_GenericDigestive' value='7' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("7") ? "checked='checked'" : "" ) %>
                <label for="ResumptionOfCare_GenericDigestive7" class="radio">GERD</label>
            </div><div class="row">
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDigestive8' class='radio float_left' name='ResumptionOfCare_GenericDigestive' value='8' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("8") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDigestive8" class="radio">Abd Girth:</label>
                </div><div class="float_right">
                    <%= Html.TextBox("ResumptionOfCare_GenericDigestiveAbdGirthLength", data.ContainsKey("GenericDigestiveAbdGirthLength") ? data["GenericDigestiveAbdGirthLength"].Answer : "", new { @id = "ResumptionOfCare_GenericDigestiveAbdGirthLength", @class = "vitals", @maxlength = "5" })%>
                </div>
            </div><div class="row">
                <label for="ResumptionOfCare_GenericGUDigestiveComments">Comments:</label>
                <%= Html.TextArea("ResumptionOfCare_GenericGUDigestiveComments", data.ContainsKey("GenericGUDigestiveComments") ? data["GenericGUDigestiveComments"].Answer : "", 5, 70, new { @id = "ResumptionOfCare_GenericGUDigestiveComments" })%>
            </div>
        </div><div class="column">
            <div class="row">
                <div class="strong">Elimination</div>
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDigestive11' class='radio float_left' name='ResumptionOfCare_GenericDigestive' value='11' type='checkbox' {0} />", genericDigestive!=null && genericDigestive.Contains("11") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDigestive11" class="radio">Last BM:</label>
                </div><div id="ResumptionOfCare_GenericDigestive11More" class="float_right">
                    <label for="ResumptionOfCare_GenericDigestiveLastBMDate">Date:</label>
                    <%= Html.Telerik().DatePicker().Name("ResumptionOfCare_GenericDigestiveLastBMDate").Value(data.ContainsKey("GenericDigestiveLastBMDate") ? data["GenericDigestiveLastBMDate"].Answer : "").HtmlAttributes(new { @id = "ResumptionOfCare_GenericDigestiveLastBMDate", @class = "date" })%>
                </div>
                <div class="clear"></div>
                <% string[] genericDigestiveLastBM = data.ContainsKey("GenericDigestiveLastBM") && data["GenericDigestiveLastBM"].Answer != "" ? data["GenericDigestiveLastBM"].Answer.Split(',') : null; %>
                <input type="hidden" name="ResumptionOfCare_GenericDigestiveLastBM" value=" " />
                <div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericDigestiveLastBM1' class='radio float_left' name='ResumptionOfCare_GenericDigestiveLastBM' value='1' type='checkbox' {0} />",  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("1") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDigestiveLastBM1" class="radio">WNL</label>
                    <div class="clear"></div>
                </div><div>
                    <%= string.Format("<input id='ResumptionOfCare_GenericDigestiveLastBM2' class='radio float_left' name='ResumptionOfCare_GenericDigestiveLastBM' value='2' type='checkbox' {0} />",  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("2") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDigestiveLastBM2" class="radio">Abnormal Stool:</label>
                    <% string[] genericDigestiveLastBMAbnormalStool = data.ContainsKey("GenericDigestiveLastBMAbnormalStool") && data["GenericDigestiveLastBMAbnormalStool"].Answer != "" ? data["GenericDigestiveLastBMAbnormalStool"].Answer.Split(',') : null; %>
                    <input type="hidden" name="ResumptionOfCare_GenericDigestiveLastBMAbnormalStool" value=" " />
                </div><div id="ResumptionOfCare_GenericDigestiveLastBM2More" class="float_right">
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericDigestiveLastBMAbnormalStool1' class='radio float_left' name='ResumptionOfCare_GenericDigestiveLastBMAbnormalStool' value='1' type='checkbox' {0} />",  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("1") ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericDigestiveLastBMAbnormalStool1" class="fixed inlineradio">Gray</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericDigestiveLastBMAbnormalStool2' class='radio float_left' name='ResumptionOfCare_GenericDigestiveLastBMAbnormalStool' value='2' type='checkbox' {0} />",  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("2") ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericDigestiveLastBMAbnormalStool2" class="fixed inlineradio">Tarry</label>
                    </div>
                    <div class="clear"></div>
                    <div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericDigestiveLastBMAbnormalStool3' class='radio float_left' name='ResumptionOfCare_GenericDigestiveLastBMAbnormalStool' value='3' type='checkbox' {0} />",  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("3") ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericDigestiveLastBMAbnormalStool3" class="fixed inlineradio">Fresh Blood</label>
                    </div><div class="float_left">
                        <%= string.Format("<input id='ResumptionOfCare_GenericDigestiveLastBMAbnormalStool4' class='radio float_left' name='ResumptionOfCare_GenericDigestiveLastBMAbnormalStool' value='4' type='checkbox' {0} />",  genericDigestiveLastBMAbnormalStool!=null && genericDigestiveLastBMAbnormalStool.Contains("4") ? "checked='checked'" : "" ) %>
                        <label for="ResumptionOfCare_GenericDigestiveLastBMAbnormalStool4" class="fixed inlineradio">Black</label>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDigestiveLastBM3' class='radio float_left' name='ResumptionOfCare_GenericDigestiveLastBM' value='3' type='checkbox' {0} />",  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("3") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDigestiveLastBM3" class="radio">Constipation:</label>
                    <%=Html.Hidden("ResumptionOfCare_GenericDigestiveLastBMConstipationType")%>
                </div><div id="ResumptionOfCare_GenericDigestiveLastBM3More" class="float_right">
                    <%=Html.RadioButton("ResumptionOfCare_GenericDigestiveLastBMConstipationType", "Chronic", data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Chronic" ? true : false, new { @id = "ResumptionOfCare_GenericDigestiveLastBMConstipationTypeChronic", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericDigestiveLastBMConstipationTypeChronic" class="inlineradio">Chronic</label>
                    <%=Html.RadioButton("ResumptionOfCare_GenericDigestiveLastBMConstipationType", "Acute", data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Acute" ? true : false, new { @id = "ResumptionOfCare_GenericDigestiveLastBMConstipationTypeAcute", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericDigestiveLastBMConstipationTypeAcute" class="inlineradio">Acute</label>
                    <%=Html.RadioButton("ResumptionOfCare_GenericDigestiveLastBMConstipationType", "Occasional", data.ContainsKey("GenericDigestiveLastBMConstipationType") && data["GenericDigestiveLastBMConstipationType"].Answer == "Occasional" ? true : false, new { @id = "ResumptionOfCare_GenericDigestiveLastBMConstipationTypeOccasional", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericDigestiveLastBMConstipationTypeOccasional" class="inlineradio">Occasional</label>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDigestiveLastBM4' class='radio float_left' name='ResumptionOfCare_GenericDigestiveLastBM' value='4' type='checkbox' {0} />",  genericDigestiveLastBM!=null && genericDigestiveLastBM.Contains("4") ? "checked='checked'" : "" ) %>
                    <label for="ResumptionOfCare_GenericDigestiveLastBM4" class="radio">Diarrhea:</label>
                    <%=Html.Hidden("ResumptionOfCare_GenericDigestiveLastBMDiarrheaType")%>
                </div><div id="ResumptionOfCare_GenericDigestiveLastBM4More" class="float_right">
                    <%=Html.RadioButton("ResumptionOfCare_GenericDigestiveLastBMDiarrheaType", "Chronic", data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Chronic" ? true : false, new { @id = "ResumptionOfCare_GenericDigestiveLastBMDiarrheaTypeChronic", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericDigestiveLastBMDiarrheaTypeChronic" class="inlineradio">Chronic</label>
                    <%=Html.RadioButton("ResumptionOfCare_GenericDigestiveLastBMDiarrheaType", "Acute", data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Acute" ? true : false, new { @id = "ResumptionOfCare_GenericDigestiveLastBMDiarrheaTypeAcute", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericDigestiveLastBMDiarrheaTypeAcute" class="inlineradio">Acute</label>
                    <%=Html.RadioButton("ResumptionOfCare_GenericDigestiveLastBMDiarrheaType", "Occasional", data.ContainsKey("GenericDigestiveLastBMDiarrheaType") && data["GenericDigestiveLastBMDiarrheaType"].Answer == "Occasional" ? true : false, new { @id = "ResumptionOfCare_GenericDigestiveLastBMDiarrheaTypeOccasional", @class = "radio" })%>
                    <label for="ResumptionOfCare_GenericDigestiveLastBMDiarrheaTypeOccasional" class="inlineradio">Occasional</label>
                </div>
                <div class="clear"></div>
            </div><div class="row">
                <div class="strong">Ostomy:</div>
                <% string[] genericDigestiveOstomy = data.ContainsKey("GenericDigestiveOstomy") && data["GenericDigestiveOstomy"].Answer != "" ? data["GenericDigestiveOstomy"].Answer.Split(',') : null; %>
                <input type="hidden" name="ResumptionOfCare_GenericDigestiveOstomy" value=" " />
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDigestiveOstomy1' class='radio float_left' name='ResumptionOfCare_GenericDigestiveOstomy' value='1' type='checkbox' {0} />", genericDigestiveOstomy != null && genericDigestiveOstomy.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericDigestiveOstomy1" class="">Ostomy Type:</label>
                </div><div class="float_right">
                    <%  var ostomy = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "0" },
                            new SelectListItem { Text = "N/A", Value = "1" },
                            new SelectListItem { Text = "Ileostomy ", Value = "2" },
                            new SelectListItem { Text = "Colostomy", Value = "3" },
                            new SelectListItem { Text = "Other", Value = "4" }
                        }, "Value", "Text", data.ContainsKey("GenericDigestiveOstomyType") ? data["GenericDigestiveOstomyType"].Answer : "0");%>
                    <%= Html.DropDownList("ResumptionOfCare_GenericDigestiveOstomyType", ostomy, new { @id = "ResumptionOfCare_GenericDigestiveOstomyType" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDigestiveOstomy2' class='radio float_left' name='ResumptionOfCare_GenericDigestiveOstomy' value='2' type='checkbox' {0} />", genericDigestiveOstomy != null && genericDigestiveOstomy.Contains("1") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericDigestiveOstomy2" class="radio">Stoma Appearance:</label>
                </div><div class="float_right">
                    <%=Html.TextBox("ResumptionOfCare_GenericDigestiveStomaAppearance", data.ContainsKey("GenericDigestiveStomaAppearance") ? data["GenericDigestiveStomaAppearance"].Answer : "", new { @id = "ResumptionOfCare_GenericDigestiveStomaAppearance", @class = "st", @maxlength = "15" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <%= string.Format("<input id='ResumptionOfCare_GenericDigestiveOstomy3' class='radio float_left' name='ResumptionOfCare_GenericDigestiveOstomy' value='3' type='checkbox' {0} />", genericDigestiveOstomy != null && genericDigestiveOstomy.Contains("3") ? "checked='checked'" : "")%>
                    <label for="ResumptionOfCare_GenericDigestiveOstomy3" class="radio">Surrounding Skin:</label>
                </div><div class="float_right">
                    <%=Html.TextBox("ResumptionOfCare_GenericDigestiveSurSkinType", data.ContainsKey("GenericDigestiveSurSkinType") ? data["GenericDigestiveSurSkinType"].Answer : "", new { @id = "ResumptionOfCare_GenericDigestiveSurSkinType", @class = "st", @maxlength = "15" })%>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="interventions">
        <legend>Interventions</legend>
        <% string[] eliminationInterventions = data.ContainsKey("485EliminationInterventions") && data["485EliminationInterventions"].Answer != "" ? data["485EliminationInterventions"].Answer.Split(',') : null; %>
        <input type="hidden" name="ResumptionOfCare_485EliminationInterventions" value=" " />
        <div class="wide_column">
            <div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EliminationInterventions1' class='radio float_left' name='ResumptionOfCare_485EliminationInterventions' value='1' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("1") ? "checked='checked'" : "")%>
                <label for="ResumptionOfCare_485EliminationInterventions1" class="radio">SN to instruct on establishing bowel regimen.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EliminationInterventions2' class='radio float_left' name='ResumptionOfCare_485EliminationInterventions' value='2' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("2") ? "checked='checked'" : "")%>
                <label for="ResumptionOfCare_485EliminationInterventions2" class="radio">SN to instruct on application of appliance, care and storage of equipment and disposal of used supplies.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EliminationInterventions3' class='radio float_left' name='ResumptionOfCare_485EliminationInterventions' value='3' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("3") ? "checked='checked'" : "")%>
                <label for="ResumptionOfCare_485EliminationInterventions3" class="radio">SN to instruct on care of stoma, surrounding skin and use of skin barrier.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EliminationInterventions4' class='radio float_left' name='ResumptionOfCare_485EliminationInterventions' value='4' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("4") ? "checked='checked'" : "")%>
                <label for="ResumptionOfCare_485EliminationInterventions4" class="radio">SN to instruct on foley care, skin and perineal care, proper handling and storage of supplies.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EliminationInterventions5' class='radio float_left' name='ResumptionOfCare_485EliminationInterventions' value='5' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("5") ? "checked='checked'" : "")%>
                <label for="ResumptionOfCare_485EliminationInterventions5" class="radio">SN to instruct on adequate hydration, proper handling and maintenance of drainage bag.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EliminationInterventions6' class='radio float_left' name='ResumptionOfCare_485EliminationInterventions' value='6' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("6") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="ResumptionOfCare_485EliminationInterventions6">SN to change catheter every month and PRN using a</label>
                    <%=Html.TextBox("ResumptionOfCare_485EliminationFoleyCatheterType", data.ContainsKey("485EliminationFoleyCatheterType") ? data["485EliminationFoleyCatheterType"].Answer : "", new { @id = "ResumptionOfCare_485EliminationFoleyCatheterType" })%>
                    <label for="ResumptionOfCare_485EliminationInterventions6">F catheter.</label>
                </span>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EliminationInterventions7' class='radio float_left' name='ResumptionOfCare_485EliminationInterventions' value='7' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("7") ? "checked='checked'" : "")%>
                <label for="ResumptionOfCare_485EliminationInterventions7" class="radio">SN to instruct on intermittent catheterizations.</label>
            </div><div class="row">
                <%= string.Format("<input id='ResumptionOfCare_485EliminationInterventions8' class='radio float_left' name='ResumptionOfCare_485EliminationInterventions' value='8' type='checkbox' {0} />", eliminationInterventions != null && eliminationInterventions.Contains("8") ? "checked='checked'" : "")%>
                <span class="radio">
                    <label for="ResumptionOfCare_485EliminationInterventions8">SN to perform intermittent catheterization every</label>
                    <%=Html.TextBox("ResumptionOfCare_485EliminationCatheterizationNumber", data.ContainsKey("485EliminationCatheterizationNumber") ? data["485EliminationCatheterizationNumber"].Answer : "", new { @id = "ResumptionOfCare_485EliminationCatheterizationNumber" })%>
                    <label for="ResumptionOfCare_485EliminationInterventions8">&amp; prn using sterile technique.</label>
                </span>
            </div><div class="row">
                <label for="ResumptionOfCare_485EliminationOrderTemplates" class="strong">Additional Orders:</label>
                <%  var endocrineOrderTemplates = new SelectList(new[] {
                        new SelectListItem { Text = "", Value = "0" },
                        new SelectListItem { Text = "-----------", Value = "-2" },
                        new SelectListItem { Text = "Erase", Value = "-1" }
                    }, "Value", "Text", data.ContainsKey("485EliminationOrderTemplates") && data["485EliminationOrderTemplates"].Answer != "" ? data["485EliminationOrderTemplates"].Answer : "0"); %>
                <%= Html.DropDownList("ResumptionOfCare_485EliminationOrderTemplates", endocrineOrderTemplates)%>
                <%= Html.TextArea("ResumptionOfCare_485EliminationInterventionComments", data.ContainsKey("485EliminationInterventionComments") ? data["485EliminationInterventionComments"].Answer : "", 2, 70, new { @id = "ResumptionOfCare_485EliminationInterventionComments" })%>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="ROC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
           <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"ROC.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('ResumptionOfCare_ValidationContainer','{0}','{1}','{2}','ResumptionOfCare');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfChecked($("#ResumptionOfCare_GenericGU10"), $("#ResumptionOfCare_GenericGU10More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericGU11"), $("#ResumptionOfCare_GenericGU11More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericGUUrine5"), $("#ResumptionOfCare_GenericGUOtherText"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericGU12"), $("#ResumptionOfCare_GenericGU12More"));
    Oasis.showIfRadioEquals("ResumptionOfCare_GenericPatientOnDialysis", "1", $(".DialysisSpecifics"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDialysis5"), $("#ResumptionOfCare_GenericDialysis5More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDialysisHemodialysis1"), $("#ResumptionOfCare_GenericDialysisHemodialysisGriftAV"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDialysisHemodialysis2"), $("#ResumptionOfCare_GenericDialysisHemodialysisCentralVenousAV"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDialysis7"), $("#ResumptionOfCare_GenericDialysisOtherDesc"));
    Oasis.showIfRadioEquals("ResumptionOfCare_M1610UrinaryIncontinence", "01", $("#ROC_M1615"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDigestive2"), $("#ResumptionOfCare_GenericDigestiveBowelSoundsType"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDigestive3"), $("#ResumptionOfCare_GenericAbdominalPalpation"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDigestive8"), $("#ResumptionOfCare_GenericDigestiveAbdGirthLength"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDigestive11"), $("#ResumptionOfCare_GenericDigestive11More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDigestiveLastBM2"), $("#ResumptionOfCare_GenericDigestiveLastBM2More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDigestiveLastBM3"), $("#ResumptionOfCare_GenericDigestiveLastBM3More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDigestiveLastBM4"), $("#ResumptionOfCare_GenericDigestiveLastBM4More"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDigestiveOstomy1"), $("#ResumptionOfCare_GenericDigestiveOstomyType"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDigestiveOstomy2"), $("#ResumptionOfCare_GenericDigestiveStomaAppearance"));
    Oasis.showIfChecked($("#ResumptionOfCare_GenericDigestiveOstomy3"), $("#ResumptionOfCare_GenericDigestiveSurSkinType"));
    Oasis.interventions($(".interventions"));
</script>