﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Oasis Export | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','oasisExport');</script>")%>
<%  using (Html.BeginForm("Generate", "Oasis", FormMethod.Post, new { @id = "generateOasis", @style = "width: 100%; height: 100%;" })) { %>
<table style="width: 100%; height: 100%;">
    <tbody>
        <% if (Current.HasRight(Permissions.CreateOasisSubmitFile)) { %>
        <tr>
            <td style="height: 25px;">
                <div class="buttons float_left"><ul><li><a href="javascript:void(0);" onclick="GenerateSubmit($(this));">Generate Selected</a></li></ul></div>
            </td>
        </tr>
        <% } %>
        <tr>
            <td>
                <% Html.Telerik().Grid<Assessment>()
        .Name("generateOasisGrid")
        .Columns(columns =>
        {
            columns.Bound(o => o.Id)
                   .Template(o =>
            {
                %>
                <%= string.Format("<input name=\"OasisSelected\" type=\"checkbox\" value=\"{0}\" />", o.Identifier)%>
                <%
                    }).ClientTemplate("<input name='OasisSelected' type='checkbox' value='<#= Identifier #>'/>")
                   .Title("Check")
                   .Width(50)
                   .HtmlAttributes(new { style = "text-align:center" }).Sortable(false);

            columns.Bound(o => o.PatientName).Title("Patient Name");
            columns.Bound(o => o.TypeDescription).Title("Oasis Type");
            columns.Bound(o => o.Modified).Format("{0:MM/dd/yyyy}").Title("Assessment Date");
            columns.Bound(o => o.Insurance);
        })
        .DataBinding(dataBinding => dataBinding.Ajax()
                .Select("Export", "Oasis"))
                .Scrollable()
                .Sortable()
                .Render();
                %>
            </td>
        </tr>
        <tr>
            <td style="height: 1px;">
                <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Oasis.MarkAsExpotred('#generateOasis');">Mark Selected As Exported</a></li></ul></div>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<script type="text/javascript">
    $("#window_oasisExport .t-group-indicator").hide();
    $("#window_oasisExport .t-grouping-header").remove();
    $("#window_oasisExport .t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
    $("#window_oasisExport .t-grid").css({ 'top': '30px', 'bottom': '40px' });
    function GenerateSubmit(control) {
        var check = false;
        $("#generateOasis input[name=OasisSelected]").each(function() { if ($(this).is(":checked")) { check = true; } })
        if (check) { $(control).closest('form').submit(); } else { $.jGrowl("Select an assessment to generate.", { theme: 'error', life: 5000 }); }
    }
</script>

