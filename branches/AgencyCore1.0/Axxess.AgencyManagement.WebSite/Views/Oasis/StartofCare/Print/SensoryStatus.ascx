﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
            printview.checkbox("WNL (Within Normal Limits)",<%= data != null && data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.checkbox("Glasses",<%= data != null && data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.checkbox("Contacts Left",<%= data != null && data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true) +
            printview.checkbox("Contacts Right",<%= data != null && data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true) +
            printview.checkbox("Blurred Vision",<%= data != null && data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer.Split(',').Contains("5") ? "true" : "false"%>,true) +
            printview.checkbox("Glaucoma",<%= data != null && data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer.Split(',').Contains("6") ? "true" : "false"%>,true) +
            printview.checkbox("Cataracts",<%= data != null && data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer.Split(',').Contains("7") ? "true" : "false"%>,true) +
            printview.checkbox("Macular Degeneration",<%= data != null && data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer.Split(',').Contains("8") ? "true" : "false"%>,true) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericEyesOtherDetails") && data["GenericEyesOtherDetails"].Answer.IsNotNullOrEmpty() ? data["GenericEyesOtherDetails"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericEyes") && data["GenericEyes"].Answer.Split(',').Contains("13") ? "true" : "false"%>,true) + printview.span("") +
            printview.span("Date of Last Eye Exam",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericEyesLastEyeExamDate") && data["GenericEyesLastEyeExamDate"].Answer.IsNotNullOrEmpty() ? data["GenericEyesLastEyeExamDate"].Answer : ""%>",false,1)),
        "Eyes");
    printview.addsection(
        printview.col(4,
            printview.checkbox("WNL (Within Normal Limits)",<%= data != null && data.ContainsKey("GenericEars") && data["GenericEars"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.checkbox("Deaf",<%= data != null && data.ContainsKey("GenericEars") && data["GenericEars"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true) +
            printview.checkbox("Drainage",<%= data != null && data.ContainsKey("GenericEars") && data["GenericEars"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true) +
            printview.checkbox("Pain",<%= data != null && data.ContainsKey("GenericEars") && data["GenericEars"].Answer.Split(',').Contains("5") ? "true" : "false"%>,true) +
            printview.checkbox("Hearing Impaired",<%= data != null && data.ContainsKey("GenericEars") && data["GenericEars"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.checkbox("Bilateral",<%= data != null && data.ContainsKey("GenericEarsHearingImpairedPosition") && data["GenericEarsHearingImpairedPosition"].Answer == "0" ? "true" : "false"%>) +
            printview.checkbox("Left",<%= data != null && data.ContainsKey("GenericEarsHearingImpairedPosition") && data["GenericEarsHearingImpairedPosition"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("Right",<%= data != null && data.ContainsKey("GenericEarsHearingImpairedPosition") && data["GenericEarsHearingImpairedPosition"].Answer == "2" ? "true" : "false"%>) +
            printview.checkbox("Hearing Aids",<%= data != null && data.ContainsKey("GenericEars") && data["GenericEars"].Answer.Split(',').Contains("6") ? "true" : "false"%>,true) +
            printview.checkbox("Bilateral",<%= data != null && data.ContainsKey("GenericEarsHearingAidsPosition") && data["GenericEarsHearingAidsPosition"].Answer == "0" ? "true" : "false"%>) +
            printview.checkbox("Left",<%= data != null && data.ContainsKey("GenericEarsHearingAidsPosition") && data["GenericEarsHearingAidsPosition"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("Right",<%= data != null && data.ContainsKey("GenericEarsHearingAidsPosition") && data["GenericEarsHearingAidsPosition"].Answer == "2" ? "true" : "false"%>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericEarsOtherDetails") && data["GenericEarsOtherDetails"].Answer.IsNotNullOrEmpty() ? data["GenericEarsOtherDetails"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericEars") && data["GenericEars"].Answer.Split(',').Contains("7") ? "true" : "false"%>,true)),
        "Ears");
    printview.addsection(
        printview.col(3,
            printview.checkbox("WNL (Within Normal Limits)",<%= data != null && data.ContainsKey("GenericNose") && data["GenericNose"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.checkbox("Congestion",<%= data != null && data.ContainsKey("GenericNose") && data["GenericNose"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.checkbox("Loss of Smell",<%= data != null && data.ContainsKey("GenericNose") && data["GenericNose"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true) +
            printview.checkbox("Nose Bleeds",<%= data != null && data.ContainsKey("GenericNose") && data["GenericNose"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true) +
            printview.span("How often? <%= data != null && data.ContainsKey("GenericNoseBleedsFrequency") && data["GenericNoseBleedsFrequency"].Answer.IsNotNullOrEmpty() ? data["GenericNoseBleedsFrequency"].Answer : "<span class='blank'></span>"%>") +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericNoseOtherDetails") && data["GenericNoseOtherDetails"].Answer.IsNotNullOrEmpty() ? data["GenericNoseOtherDetails"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericNose") && data["GenericNose"].Answer.Split(',').Contains("5") ? "true" : "false"%>,true)),
        "Nose");
    printview.addsection(
        printview.col(3,
            printview.checkbox("WNL (Within Normal Limits)",<%= data != null && data.ContainsKey("GenericMouth") && data["GenericMouth"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.checkbox("Dentures",<%= data != null && data.ContainsKey("GenericMouth") && data["GenericMouth"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.checkbox("Difficulty chewing",<%= data != null && data.ContainsKey("GenericMouth") && data["GenericMouth"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true) +
            printview.checkbox("Dysphagia",<%= data != null && data.ContainsKey("GenericMouth") && data["GenericMouth"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericNoseOtherDetails") && data["GenericNoseOtherDetails"].Answer.IsNotNullOrEmpty() ? data["GenericNoseOtherDetails"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericMouth") && data["GenericMouth"].Answer.Split(',').Contains("5") ? "true" : "false"%>,true)),
        "Mouth");
    printview.addsection(
        printview.col(4,
            printview.checkbox("WNL (Within Normal Limits)",<%= data != null && data.ContainsKey("GenericMouth") && data["GenericMouth"].Answer.Split(',').Contains("1") ? "true" : "false"%>,true) +
            printview.checkbox("Sore throat",<%= data != null && data.ContainsKey("GenericMouth") && data["GenericMouth"].Answer.Split(',').Contains("2") ? "true" : "false"%>,true) +
            printview.checkbox("Hoarseness",<%= data != null && data.ContainsKey("GenericMouth") && data["GenericMouth"].Answer.Split(',').Contains("3") ? "true" : "false"%>,true) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("GenericNoseOtherDetails") && data["GenericNoseOtherDetails"].Answer.IsNotNullOrEmpty() ? data["GenericNoseOtherDetails"].Answer : "<span class='blank'></span>"%>",<%= data != null && data.ContainsKey("GenericMouth") && data["GenericMouth"].Answer.Split(',').Contains("4") ? "true" : "false"%>,true)),
        "Throat");
    printview.addsection(
        printview.span("(M1200) Vision (with corrective lenses if the patient usually wears them)",true) +
        printview.checkbox("0 &ndash; Normal vision: sees adequately in most situations; can see medication labels, newsprint.",<%= data != null && data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm&rsquo;s length.",<%= data != null && data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.",<%= data != null && data.ContainsKey("M1200Vision") && data["M1200Vision"].Answer == "02" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1210) Ability to hear (with hearing aid or hearing appliance if normally used)",true) +
        printview.checkbox("0 &ndash; Adequate: hears normal conversation without difficulty.",<%= data != null && data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Mildly to Moderately Impaired: difficulty hearing in some environments or speaker may need to increase volume or speak distinctly.",<%= data != null && data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Severely Impaired: absence of useful hearing.",<%= data != null && data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("UK &ndash; Unable to assess hearing.",<%= data != null && data.ContainsKey("M1210Hearing") && data["M1210Hearing"].Answer == "UK" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1220) Understanding of Verbal Content in patient&rsquo;s own language (with hearing aid or device if used)",true) +
        printview.checkbox("0 &ndash; Understands: clear comprehension without cues or repetitions.",<%= data != null && data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Usually Understands: understands most conversations, but misses some part/intent of message. Requires cues at times to understand.",<%= data != null && data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Sometimes Understands: understands only basic conversations or simple, direct phrases. Frequently requires cues to understand.",<%= data != null && data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Rarely/Never Understands.",<%= data != null && data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "03" ? "true" : "false"%>) +
        printview.checkbox("UK &ndash; Unable to assess understanding.",<%= data != null && data.ContainsKey("M1220VerbalContent") && data["M1220VerbalContent"].Answer == "UK" ? "true" : "false"%>));
    printview.addsection(
        printview.span("(M1230) Speech and Oral (Verbal) Expression of Language (in patient&rsquo;s own language)",true) +
        printview.checkbox("0 &ndash; Expresses complex ideas, feelings, and needs clearly, completely, and easily in all situations with no observable impairment.",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "00" ? "true" : "false"%>) +
        printview.checkbox("1 &ndash; Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional errors in word choice, grammar or speech intelligibility; needs minimal prompting or assistance).",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "01" ? "true" : "false"%>) +
        printview.checkbox("2 &ndash; Expresses simple ideas or needs with moderate difficulty (needs prompting or assistance, errors in word choice, organization or speech intelligibility). Speaks in phrases or short sentences.",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "02" ? "true" : "false"%>) +
        printview.checkbox("3 &ndash; Has severe difficulty expressing basic ideas or needs and requires maximal assistance or guessing by listener. Speech limited to single words or short phrases",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "03" ? "true" : "false"%>) +
        printview.checkbox("4 &ndash; Unable to express basic needs even with maximal prompting or assistance but is not comatose or unresponsive (e.g., speech is nonsensical or unintelligible).",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "04" ? "true" : "false"%>) +
        printview.checkbox("5 &ndash; Patient nonresponsive or unable to speak.",<%= data != null && data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "05" ? "true" : "false"%>));
    printview.addsection(
        printview.checkbox("ST to evaluate.",<%= data != null && data.ContainsKey("485SensoryStatusIntervention") && data["485SensoryStatusIntervention"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
        printview.span("Additional Orders:",true) +
        printview.span("<%= data != null && data.ContainsKey("485SensoryStatusInterventionComments") && data["485SensoryStatusInterventionComments"].Answer.IsNotNullOrEmpty() ? data["485SensoryStatusInterventionComments"].Answer : ""%>",false,2),
        "Interventions");
    printview.addsection(
        printview.span("Additional Goals:",true) +
        printview.span("<%= data != null && data.ContainsKey("485SensoryStatusGoalComments") && data["485SensoryStatusGoalComments"].Answer.IsNotNullOrEmpty() ? data["485SensoryStatusGoalComments"].Answer : ""%>",false,2),
        "Goals");
</script>