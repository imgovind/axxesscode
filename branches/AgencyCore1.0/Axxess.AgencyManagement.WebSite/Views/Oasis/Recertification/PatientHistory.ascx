﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisRecertificationPatientHistoryForm" })) {
        var data = Model.ToDictionary(); %>
<%= Html.Hidden("Recertification_Id", Model.Id)%>
<%= Html.Hidden("Recertification_Action", "Edit")%>
<%= Html.Hidden("Recertification_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("Recertification_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "Recertification")%>
<%= Html.Hidden("categoryType", "PatientHistory")%>
<div class="wrapper main">
    <fieldset class="half float_left loc485">
        <legend>Allergies (Locator #17)</legend>
        <div class="column">
            <div class="row">
                <%= Html.Hidden("Recertification_485Allergies") %>
                <%= Html.RadioButton("Recertification_485Allergies", "No", data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "No" ? true : false, new { @id = "Recert_Allergies_NKA", @class = "radio" })%>
                <label for="Recert_Allergies_NKA">NKA (Food/Drugs/Latex)</label>
            </div>
        </div><div class="column">
            <div class="row">
                <%= Html.RadioButton("Recertification_485Allergies", "Yes", data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "Yes" ? true : false, new { @id = "Recert_Allergies_Specify", @class = "radio" })%>
                <label for="Recert_Allergies_Specify">Allergic to:</label>
                <%= Html.TextArea("Recertification_485AllergiesDescription", data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : "", new { @id = "Recertification_485AllergiesDescription" })%>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Vital Sign Parameters:</legend>
        <div class="column">
            <div class="row">
                <label class="float_left">Temperature</label>
                <div class="float_right oasis">
                    <label for="Recertification_GenericTempGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("Recertification_GenericTempGreaterThan", data.ContainsKey("GenericTempGreaterThan") ? data["GenericTempGreaterThan"].Answer : "", new { @id = "Recertification_GenericTempGreaterThan", @class = "vitals" })%>
                    <label="Recertification_GenericTempLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("Recertification_GenericTempLessThan", data.ContainsKey("GenericTempLessThan") ? data["GenericTempLessThan"].Answer : "", new { @id = "Recertification_GenericTempLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Pulse</label>
                <div class="float_right oasis">
                    <label for="Recertification_GenericPulseGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("Recertification_GenericPulseGreaterThan", data.ContainsKey("GenericPulseGreaterThan") ? data["GenericPulseGreaterThan"].Answer : "", new { @id = "Recertification_GenericPulseGreaterThan", @class = "vitals" })%>
                    <label="Recertification_GenericPulseLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("Recertification_GenericPulseLessThan", data.ContainsKey("GenericPulseLessThan") ? data["GenericPulseLessThan"].Answer : "", new { @id = "Recertification_GenericPulseLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Respirations</label>
                <div class="float_right oasis">
                    <label for="Recertification_GenericRespirationGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("Recertification_GenericRespirationGreaterThan", data.ContainsKey("GenericRespirationGreaterThan") ? data["GenericRespirationGreaterThan"].Answer : "", new { @id = "Recertification_GenericRespirationGreaterThan", @class = "vitals" })%>
                    <label="Recertification_GenericRespirationLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("Recertification_GenericRespirationLessThan", data.ContainsKey("GenericRespirationLessThan") ? data["GenericRespirationLessThan"].Answer : "", new { @id = "Recertification_GenericRespirationLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Systolic BP</label>
                <div class="float_right oasis">
                    <label for="Recertification_GenericSystolicBPGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("Recertification_GenericSystolicBPGreaterThan", data.ContainsKey("GenericSystolicBPGreaterThan") ? data["GenericSystolicBPGreaterThan"].Answer : "", new { @id = "Recertification_GenericSystolicBPGreaterThan", @class = "vitals" })%>
                    <label="Recertification_GenericSystolicBPLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("Recertification_GenericSystolicBPLessThan", data.ContainsKey("GenericSystolicBPLessThan") ? data["GenericSystolicBPLessThan"].Answer : "", new { @id = "Recertification_GenericRespirationGreaterThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Diastolic BP</label>
                <div class="float_right oasis">
                    <label for="Recertification_GenericDiastolicBPGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("Recertification_GenericDiastolicBPGreaterThan", data.ContainsKey("GenericDiastolicBPGreaterThan") ? data["GenericDiastolicBPGreaterThan"].Answer : "", new { @id = "Recertification_GenericDiastolicBPGreaterThan", @class = "vitals" })%>
                    <label="Recertification_GenericDiastolicBPLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("Recertification_GenericDiastolicBPLessThan", data.ContainsKey("GenericDiastolicBPLessThan") ? data["GenericDiastolicBPLessThan"].Answer : "", new { @id = "Recertification_GenericDiastolicBPLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">O<sub>2</sub> Sat (percent)</label>
                <div class="float_right oasis">
                    <label for="Recertification_Generic02SatLessThan">less than (&lt;)</label>
                    <%=Html.TextBox("Recertification_Generic02SatLessThan", data.ContainsKey("Generic02SatLessThan") ? data["Generic02SatLessThan"].Answer : "", new { @id = "Recertification_Generic02SatLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Fasting blood sugar</label>
                <div class="float_right oasis">
                    <label for="Recertification_GenericFastingBloodSugarGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("Recertification_GenericFastingBloodSugarGreaterThan", data.ContainsKey("GenericFastingBloodSugarGreaterThan") ? data["GenericFastingBloodSugarGreaterThan"].Answer : "", new { @id = "Recertification_GenericFastingBloodSugarGreaterThan", @class = "vitals" })%>
                    <label="Recertification_GenericFastingBloodSugarLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("Recertification_GenericFastingBloodSugarLessThan", data.ContainsKey("GenericFastingBloodSugarLessThan") ? data["GenericFastingBloodSugarLessThan"].Answer : "", new { @id = "Recertification_GenericFastingBloodSugarLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Random blood sugar</label>
                <div class="float_right oasis">
                    <label for="Recertification_GenericRandomBloddSugarGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("Recertification_GenericRandomBloddSugarGreaterThan", data.ContainsKey("GenericRandomBloddSugarGreaterThan") ? data["GenericRandomBloddSugarGreaterThan"].Answer : "", new { @id = "Recertification_GenericRandomBloddSugarGreaterThan", @class = "vitals" })%>
                    <label="Recertification_GenericRandomBloodSugarLessThan">or less than (&lt;)</label>
                    <%=Html.TextBox("Recertification_GenericRandomBloodSugarLessThan", data.ContainsKey("GenericRandomBloodSugarLessThan") ? data["GenericRandomBloodSugarLessThan"].Answer : "", new { @id = "GenericRandomBloodSugarLessThan", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">Weight (lbs/week)</label>
                <div class="float_right oasis">
                    <label for="Recertification_GenericWeightGreaterThan">greater than (&gt;)</label>
                    <%=Html.TextBox("Recertification_GenericWeightGreaterThan", data.ContainsKey("GenericWeightGreaterThan") ? data["GenericWeightGreaterThan"].Answer : "", new { @id = "Recertification_GenericWeightGreaterThan", @class = "vitals" })%>
                    <label="Recertification_GenericWeightLessThan">or less than (<)</label>
                    <%=Html.TextBox("Recertification_GenericWeightLessThan", data.ContainsKey("GenericWeightLessThan") ? data["GenericWeightLessThan"].Answer : "", new { @id = "Recertification_GenericWeightLessThan", @class = "vitals" })%>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="half float_left">
        <legend>Vital Signs</legend>
        <div class="column">
            <div class="row">
                <label for="Recertification_GenericPulseApical" class="float_left">Apical Pulse:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("Recertification_GenericPulseApical", data.ContainsKey("GenericPulseApical") ? data["GenericPulseApical"].Answer : "", new { @id = "Recertification_GenericPulseApical", @class = "vitals" }) %>
                    <%= Html.Hidden("Recertification_GenericPulseApicalRegular") %>
                    <%= Html.RadioButton("Recertification_GenericPulseApicalRegular", "1", data.ContainsKey("GenericPulseApicalRegular") && data["GenericPulseApicalRegular"].Answer == "1" ? true : false, new { @id = "GenericPulseApicalRegular", @class = "radio" })%>
                    <label for="GenericPulseApicalRegular">(Reg)</label>
                    <%= Html.RadioButton("Recertification_GenericPulseApicalRegular", "2", data.ContainsKey("GenericPulseApicalRegular") && data["GenericPulseApicalRegular"].Answer == "2" ? true : false, new { @id = "GenericPulseApicalIrregular", @class = "radio" })%>
                    <label for="GenericPulseApicalIrregular">(Irreg)</label>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericPulseRadial" class="float_left">Radial Pulse:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("Recertification_GenericPulseRadial", data.ContainsKey("GenericPulseRadial") ? data["GenericPulseRadial"].Answer : "", new { @id = "Recertification_GenericPulseRadial", @class = "vitals" }) %>
                    <%= Html.Hidden("Recertification_GenericPulseRadialRegular") %>
                    <%= Html.RadioButton("Recertification_GenericPulseRadialRegular", "1", data.ContainsKey("GenericPulseRadialRegular") && data["GenericPulseRadialRegular"].Answer == "1" ? true : false, new { @id = "GenericPulseRadialRegular", @class = "radio" })%>
                    <label for="GenericPulseRadialRegular">(Reg)</label>
                    <%= Html.RadioButton("Recertification_GenericPulseRadialRegular", "2", data.ContainsKey("GenericPulseRadialRegular") && data["GenericPulseRadialRegular"].Answer == "2" ? true : false, new { @id = "GenericPulseRadialIrregular", @class = "radio" })%>
                    <label for="GenericPulseRadialIrregular">(Irreg)</label>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericHeight" class="float_left">Height:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("Recertification_GenericHeight", data.ContainsKey("GenericHeight") ? data["GenericHeight"].Answer : "", new { @id = "Recertification_GenericHeight", @class = "vitals" }) %>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericWeight" class="float_left">Weight:</label>
                <div class="float_right oasis">
                    <%= Html.TextBox("Recertification_GenericWeight", data.ContainsKey("GenericWeight") ? data["GenericWeight"].Answer : "", new { @id = "Recertification_GenericWeight", @class = "vitals" }) %>
                    <%= Html.Hidden("Recertification_GenericWeightActualStated")%>
                    <%= Html.RadioButton("Recertification_GenericWeightActualStated", "1", data.ContainsKey("GenericWeightActualStated") && data["GenericWeightActualStated"].Answer == "1" ? true : false, new { @id = "Recertification_GenericWeightActual", @class = "radio" })%>
                    <label for="Recertification_GenericWeightActual">Actual</label>
                    <%= Html.RadioButton("Recertification_GenericWeightActualStated", "2", data.ContainsKey("GenericWeightActualStated") && data["GenericWeightActualStated"].Answer == "2" ? true : false, new { @id = "Recertification_GenericWeightStated", @class = "radio" })%>
                    <label for="Recertification_GenericWeightStated">Stated</label>
                </div>
            </div><div class="row">
                <label for="Recertification_GenericTemp" class="float_left">Temp:</label>
                <div class="float_right oasis"><%= Html.TextBox("Recertification_GenericTemp", data.ContainsKey("GenericTemp") ? data["GenericTemp"].Answer : "", new { @id = "Recertification_GenericTemp", @class = "vitals" }) %></div>
            </div><div class="row">
                <label for="Recertification_GenericResp" class="float_left">Resp:</label>
                <div class="float_right oasis"><%= Html.TextBox("Recertification_GenericResp", data.ContainsKey("GenericResp") ? data["GenericResp"].Answer : "", new { @id = "Recertification_GenericResp", @class = "vitals" }) %></div>
            </div><div class="row">
                <label class="float_left">BP Lying</label>
                <div class="float_right oasis">
                    <label for="Recertification_GenericBPLeftLying">Left</label>
                    <%= Html.TextBox("Recertification_GenericBPLeftLying", data.ContainsKey("GenericBPLeftLying") ? data["GenericBPLeftLying"].Answer : "", new { @id = "Recertification_GenericBPLeftLying", @class = "vitals" })%>
                    <label for="Recertification_GenericBPRightLying">Right</label>
                    <%= Html.TextBox("Recertification_GenericBPRightLying", data.ContainsKey("GenericBPRightLying") ? data["GenericBPRightLying"].Answer : "", new { @id = "Recertification_GenericBPRightLying", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">BP Sitting</label>
                <div class="float_right oasis">
                    <label for="Recertification_GenericBPLeftSitting">Left</label>
                    <%= Html.TextBox("Recertification_GenericBPLeftSitting", data.ContainsKey("GenericBPLeftSitting") ? data["GenericBPLeftSitting"].Answer : "", new { @id = "Recertification_GenericBPLeftSitting", @class = "vitals" })%>
                    <label for="Recertification_GenericBPRightSitting">Right</label>
                    <%= Html.TextBox("Recertification_GenericBPRightSitting", data.ContainsKey("GenericBPRightSitting") ? data["GenericBPRightSitting"].Answer : "", new { @id = "Recertification_GenericBPRightSitting", @class = "vitals" })%>
                </div>
            </div><div class="row">
                <label class="float_left">BP Standing</label>
                <div class="float_right oasis">
                    <label for="Recertification_GenericBPLeftStanding">Left</label>
                    <%= Html.TextBox("Recertification_GenericBPLeftStanding", data.ContainsKey("GenericBPLeftStanding") ? data["GenericBPLeftStanding"].Answer : "", new { @id = "Recertification_GenericBPLeftStanding", @class = "vitals" })%>
                    <label for="Recertification_GenericBPRightStanding">Right</label>
                    <%= Html.TextBox("Recertification_GenericBPRightStanding", data.ContainsKey("GenericBPRightStanding") ? data["GenericBPRightStanding"].Answer : "", new { @id = "Recertification_GenericBPRightStanding", @class = "vitals" })%>
                </div>
            </div>
        </div>
    </fieldset><fieldset class="half float_right">
        <legend>Immunizations</legend>
        <div class="column">
            <div class="row">
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <label>Pneumonia</label>
                                <%= Html.Hidden("Recertification_485Pnemonia") %>
                            </td><td>
                                <select name="Recertification_485Pnemonia" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer == "" || !data.ContainsKey("485Pnemonia") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485Pnemonia") && data["485Pnemonia"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="Recertification_485PnemoniaDate">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("Recertification_485PnemoniaDate").Value(data.ContainsKey("485PnemoniaDate") && data["485PnemoniaDate"].Answer.IsNotNullOrEmpty() ? data["485PnemoniaDate"].Answer : "").HtmlAttributes(new { @id = "Recertification_485PnemoniaDate", @class = "date" }) %>
                            </td>
                        </tr><tr>
                            <td>
                                <label>Flu</label>
                                <%= Html.Hidden("Recertification_485Flu")%>
                            </td><td>
                                <select name="Recertification_485Flu" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485Flu") && data["485Flu"].Answer == "" || !data.ContainsKey("485Flu") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485Flu") && data["485Flu"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485Flu") && data["485Flu"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485Flu") && data["485Flu"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="Recertification_485FluDate">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("Recertification_485FluDate").Value(data.ContainsKey("485FluDate") && data["485FluDate"].Answer.IsNotNullOrEmpty() ? data["485FluDate"].Answer : "").HtmlAttributes(new { @id = "Recertification_485FluDate", @class = "date" })%>
                            </td>
                        </tr><tr>
                            <td>
                                <label>TB</label>
                                <%= Html.Hidden("Recertification_485TB")%>
                            </td><td>
                                <select name="Recertification_485TB" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485TB") && data["485TB"].Answer == "" || !data.ContainsKey("485TB") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485TB") && data["485TB"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485TB") && data["485TB"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485TB") && data["485TB"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="Recertification_485TBDate">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("Recertification_485TBDate").Value(data.ContainsKey("485FluDate") && data["485TBDate"].Answer.IsNotNullOrEmpty() ? data["485TBDate"].Answer : "").HtmlAttributes(new { @id = "Recertification_485TBDate", @class = "date" })%>
                            </td>
                        </tr><tr>
                            <td>
                                <label>TB Exposure</label>
                                <%= Html.Hidden("Recertification_485TBExposure")%>
                            </td><td>
                                <select name="Recertification_485TBExposure" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer == "" || !data.ContainsKey("485TBExposure") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485TBExposure") && data["485TBExposure"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="Recertification_485TBExposureDate">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("Recertification_485TBExposureDate").Value(data.ContainsKey("485TBExposureDate") && data["485TBExposureDate"].Answer.IsNotNullOrEmpty() ? data["485TBExposureDate"].Answer : "").HtmlAttributes(new { @id = "Recertification_485TBExposureDate", @class = "date" })%>
                            </td>
                        </tr><tr>
                            <th colspan="3" class="align_center">Additional Immunizations</th>
                        </tr><tr>
                            <td><%= Html.TextBox("Recertification_485AdditionalImmunization1Name", data.ContainsKey("485AdditionalImmunization1Name") ? data["485AdditionalImmunization1Name"].Answer : "", new { @id = "Recertification_485AdditionalImmunization1Name", @class = "fill" }) %></td>
                            <td>
                                <select name="Recertification_485AdditionalImmunization1" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485AdditionalImmunization1") && data["485AdditionalImmunization1"].Answer == "" || !data.ContainsKey("485AdditionalImmunization1") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485AdditionalImmunization1") && data["485AdditionalImmunization1"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485AdditionalImmunization1") && data["485AdditionalImmunization1"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485AdditionalImmunization1") && data["485AdditionalImmunization1"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="Recertification_485AdditionalImmunization1Date">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("Recertification_485AdditionalImmunization1Date").Value(data.ContainsKey("485AdditionalImmunization1Date") && data["485AdditionalImmunization1Date"].Answer.IsNotNullOrEmpty() ? data["485AdditionalImmunization1Date"].Answer : "").HtmlAttributes(new { @id = "Recertification_485AdditionalImmunization1Date", @class = "date" })%>
                            </td>
                        </tr><tr>
                            <td><%= Html.TextBox("Recertification_485AdditionalImmunization2Name", data.ContainsKey("485AdditionalImmunization2Name") ? data["485AdditionalImmunization2Name"].Answer : "", new { @id = "Recertification_485AdditionalImmunization2Name", @class = "fill" }) %></td>
                            <td>
                                <select name="Recertification_485AdditionalImmunization1" class="fill">
                                    <%= string.Format("<option value='' {0}></option>", data.ContainsKey("485AdditionalImmunization2") && data["485AdditionalImmunization2"].Answer == "" || !data.ContainsKey("485AdditionalImmunization2") ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='Yes' {0}>Yes</option>", data.ContainsKey("485AdditionalImmunization2") && data["485AdditionalImmunization2"].Answer == "Yes" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='No' {0}>No</option>", data.ContainsKey("485AdditionalImmunization2") && data["485AdditionalImmunization2"].Answer == "No" ? "selected='selected'" : "")%>
                                    <%= string.Format("<option value='UK' {0}>Unknown</option>", data.ContainsKey("485AdditionalImmunization2") && data["485AdditionalImmunization2"].Answer == "UK" ? "selected='selected'" : "")%>
                                </select>
                            </td><td>
                                <label for="Recertification_485AdditionalImmunization2Date">Date:</label>
                                <%= Html.Telerik().DatePicker().Name("Recertification_485AdditionalImmunization2Date").Value(data.ContainsKey("485AdditionalImmunization2Date") && data["485AdditionalImmunization2Date"].Answer.IsNotNullOrEmpty() ? data["485AdditionalImmunization2Date"].Answer : "").HtmlAttributes(new { @id = "Recertification_485AdditionalImmunization2Date", @class = "date" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><div class="row">
                <label for="Recertification_485ImmunizationComments" class="strong">Comments</label>
                <div><%= Html.TextArea("Recertification_485ImmunizationComments", data.ContainsKey("485ImmunizationComments") ? data["485ImmunizationComments"].Answer : "", 5, 70, new { @id = "Recertification_485ImmunizationComments" }) %></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="oasis">
        <legend>Diagnoses, Symptom Control, and Payment Diagnoses</legend>
        <div class="wide_column">
            <div class="row">
                <div class="instructions">
                    <p>List each diagnosis for which the patient is receiving home care (Column 1) and enter its ICD-9-C M code at the level of highest specificity (no surgical/procedure codes) (Column 2). Diagnoses are listed in the order that best reflect the seriousness of each condition and support the disciplines and services provided. Rate the degree of symptom control for each condition (Column 2). Choose one value that represents the degree of symptom control appropriate for each diagnosis: V-codes (for M1020 or M1022) or E-codes (for M1022 only) may be used. ICD-9-C M sequencing requirements must be followed if multiple coding is indicated for any diagnoses. If a V-code is reported in place of a case mix diagnosis, then optional item M1024 Payment Diagnoses (Columns 3 and 4) may be completed. A case mix diagnosis is a diagnosis that determines the Medicare P P S case mix group. Do not assign symptom control ratings for V- or E-codes.</p>
                    <strong>Code each row according to the following directions for each column:</strong>
                    <ul>
                        <li><span class="float_left">Column 1:</span><p>Enter the description of the diagnosis.</p></li>
                        <li><span class="float_left">Column 2:</span><p>Enter the ICD-9-C M code for the diagnosis described in Column 1;</p>
                            <ul>
                                <li>Rate the degree of symptom control for the condition listed in Column 1 using thefollowing scale:</li>
                                <li><span class="float_left">0.</span><p>Asymptomatic, no treatment needed at this time</p></li>
                                <li><span class="float_left">1.</span><p>Symptoms well controlled with current therapy</p></li>
                                <li><span class="float_left">2.</span><p>Symptoms controlled with difficulty, affecting daily functioning; patient needsongoing monitoring</p></li>
                                <li><span class="float_left">3.</span><p>Symptoms poorly controlled; patient needs frequent adjustment in treatment anddose monitoring</p></li>
                                <li><span class="float_left">4.</span><p>Symptoms poorly controlled; history of re-hospitalizations Note that in Column2 the rating for symptom control of each diagnosis should not be used to determinethe sequencing of the diagnoses listed in Column 1. These are separate items andsequencing may not coincide. Sequencing of diagnoses should reflect the seriousnessof each condition and support the disciplines and services provided.</p></li>
                            </ul>
                        </li>
                        <li><span class="float_left">Column 3:</span><p>(OPTIONAL) If a V-code is assigned to any row in Column 2, in place ofa case mix diagnosis, it may be necessary to complete optional item M1024 PaymentDiagnoses (Columns 3 and 4). See OASIS-C Guidance Manual.</p></li>
                        <li><span class="float_left">Column 4:</span><p>(OPTIONAL) If a V-code in Column 2 is reported in place of a case mixdiagnosis that requires multiple diagnosis codes under ICD-9-C M coding guidelines,enter the diagnosis descriptions and the ICD-9-C M codes in the same row in Columns 3 and 4. For example, if the case mix diagnosis is a manifestation code, record the diagnosis description and ICD-9-C M code for the underlying condition in Column 3 of that row and the diagnosis description and ICD-9-C M code for the manifestation in Column 4 of that row. Otherwise, leave Column 4 blank in that row.</p></li>
                    </ul>
                </div>
                <table class="form bordergrid">
                    <thead>
                        <tr class="align_center">
                            <td colspan="2">
                                <div class="halfOfTd"><h4>Column 1</h4></div>
                                <div class="halfOfTd"><h4>Column 2</h4></div>
                            </td><td>
                                <h4>Column 3</h4>
                            </td><td>
                                <h4>Column 4</h4>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <label for="Recertification_M1020PrimaryDiagnosis" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1020');">(M1020)</a> Primary Diagnosis</label>
                                </div><div class="halfOfTd">
                                    <div>ICD-9-C M and symptom control rating for each condition. Note that the sequencing of these ratings may not match the sequencing of the diagnoses.</div>
                                    <div for="Recertification_M1020ICD9M" class="strong">ICD-9-C M/ Symptom Control Rating</div>
                                    <em>(V-codes are allowed)</em>
                                </div>
                            </td><td>
                                <div>Complete if a V-code is assigned under certain circumstances to Column 2 in place of a case mix diagnosis.</div>
                                <div for="Recertification_M1024ICD9MA3" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1024');">(M1024)</a> Description/ ICD-9-C M</div>
                                <em>(V- or E-codes NOT allowed)</em>
                            </td><td>
                                <div>Complete only if the V-code in Column 2 is reported in place of a case mix diagnosis that is a multiple coding situation (e.g., a manifestation code).</div>
                                <div for="Recertification_M1024ICD9MA4" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1024');">(M1024)</a> Description/ ICD-9-C M</div>
                                <em>(V- or E-codes NOT allowed)</em>
                            </td>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">a.</span>
                                    <%= Html.TextBox("Recertification_M1020PrimaryDiagnosis", data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : "", new { @class = "diagnosis", @id = "Recertification_M1020PrimaryDiagnosis" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">a.</span>
                                    <%= Html.TextBox("Recertification_M1020ICD9M", data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer : "", new { @class = "icd", @id = "Recertification_M1020ICD9M" }) %>
                                    <label for="Recertification_M1020SymptomControlRating">Severity:</label>
                                    <%  var severity = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1020SymptomControlRating") ? data["M1020SymptomControlRating"].Answer : " ");%>
                                    <%= Html.DropDownList("Recertification_M1020SymptomControlRating", severity, new { @id = "Recertification_M1020SymptomControlRating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis") ? data["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer : "0");%>
                                    <%= Html.DropDownList("Recertification_485ExacerbationOrOnsetPrimaryDiagnosis", OE, new { @id = "Recertification_485ExacerbationOrOnsetPrimaryDiagnosis", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("Recertification_M1020PrimaryDiagnosisDate").Value(data.ContainsKey("M1020PrimaryDiagnosisDate") && data["M1020PrimaryDiagnosisDate"].Answer.IsNotNullOrEmpty() ? data["M1020PrimaryDiagnosisDate"].Answer : "").HtmlAttributes(new { @id = "Recertification_M1020PrimaryDiagnosisDate", @class = "diagnosisDate date" })%>
                                    <div class="float_right oasis">
                                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1020');">?</div>
                                    </div>
                                </div>
                            </td><td>
                                <span class="alphali">a.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesA3", data.ContainsKey("M1024PaymentDiagnosesA3") ? data["M1024PaymentDiagnosesA3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesA3" })%>
                                <%= Html.TextBox("Recertification_M1024ICD9MA3", data.ContainsKey("M1024ICD9MA3") ? data["M1024ICD9MA3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9MA3" })%>
                            </td><td>
                                <span class="alphali">a.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesA4", data.ContainsKey("M1024PaymentDiagnosesA4") ? data["M1024PaymentDiagnosesA4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesA4" }) %>
                                <%= Html.TextBox("Recertification_M1024ICD9MA4", data.ContainsKey("M1024ICD9MA4") ? data["M1024ICD9MA4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9MA4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2"><label for="Recertification_M1022PrimaryDiagnosis1" class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1022');">(M1022)</a> Other Diagnoses</label></td>
                            <td></td>
                            <td></td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">b.</span>
                                    <%= Html.TextBox("Recertification_M1022PrimaryDiagnosis1", data.ContainsKey("M1022PrimaryDiagnosis1") ? data["M1022PrimaryDiagnosis1"].Answer : "", new { @class = "diagnosis", @id = "Recertification_M1022PrimaryDiagnosis1" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">b.</span>
                                    <%=Html.TextBox("Recertification_M1022ICD9M1", data.ContainsKey("M1022ICD9M1") ? data["M1022ICD9M1"].Answer : "", new { @class = "icd", @id = "Recertification_M1022ICD9M1" }) %>
                                    <label for="Recertification_M1022OtherDiagnose1Rating">Severity:</label>
                                    <%  var severity1 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose1Rating") ? data["M1022OtherDiagnose1Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("Recertification_M1022OtherDiagnose1Rating", severity1, new { @id = "Recertification_M1022OtherDiagnose1Rating", @class = "severity" }) %>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE1 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis1") ? data["485ExacerbationOrOnsetPrimaryDiagnosis1"].Answer : "0");%>
                                    <%= Html.DropDownList("Recertification_485ExacerbationOrOnsetPrimaryDiagnosis1", OE1, new { @id = "Recertification_485ExacerbationOrOnsetPrimaryDiagnosis1", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("Recertification_M1022PrimaryDiagnosis1Date").Value(data.ContainsKey("M1022PrimaryDiagnosis1Date") && data["M1022PrimaryDiagnosis1Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis1Date"].Answer : "").HtmlAttributes(new { @id = "Recertification_M1022PrimaryDiagnosis1Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">b.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesB3", data.ContainsKey("M1024PaymentDiagnosesB3") ? data["M1024PaymentDiagnosesB3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesB3" }) %>
                                <%= Html.TextBox("Recertification_M1024ICD9MB3", data.ContainsKey("M1024ICD9MB3") ? data["M1024ICD9MB3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9MB3" })%>
                            </td><td>
                                <span class="alphali">b.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesB4", data.ContainsKey("M1024PaymentDiagnosesB4") ? data["M1024PaymentDiagnosesB4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesB4" }) %>
                                <%= Html.TextBox("Recertification_M1024ICD9MB4", data.ContainsKey("M1024ICD9MB4") ? data["M1024ICD9MB4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9MB4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">c.</span>
                                    <%= Html.TextBox("Recertification_M1022PrimaryDiagnosis2", data.ContainsKey("M1022PrimaryDiagnosis2") ? data["M1022PrimaryDiagnosis2"].Answer : "", new { @class = "diagnosis", @id = "Recertification_M1022PrimaryDiagnosis2" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">c.</span>
                                    <%=Html.TextBox("Recertification_M1022ICD9M2", data.ContainsKey("M1022ICD9M2") ? data["M1022ICD9M2"].Answer : "", new { @class = "icd", @id = "Recertification_M1022ICD9M2" }) %>
                                    <label for="Recertification_M1022OtherDiagnose2Rating">Severity:</label>
                                    <%  var severity2 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose2Rating") ? data["M1022OtherDiagnose2Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("Recertification_M1022OtherDiagnose2Rating", severity2, new { @id = "Recertification_M1022OtherDiagnose2Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE2 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis2") ? data["485ExacerbationOrOnsetPrimaryDiagnosis2"].Answer : "0");%>
                                    <%= Html.DropDownList("Recertification_485ExacerbationOrOnsetPrimaryDiagnosis2", OE2, new { @id = "Recertification_485ExacerbationOrOnsetPrimaryDiagnosis2", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("Recertification_M1022PrimaryDiagnosis2Date").Value(data.ContainsKey("M1022PrimaryDiagnosis2Date") && data["M1022PrimaryDiagnosis2Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis2Date"].Answer : "").HtmlAttributes(new { @id = "Recertification_M1022PrimaryDiagnosis2Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">c.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesC3", data.ContainsKey("M1024PaymentDiagnosesC3") ? data["M1024PaymentDiagnosesC3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesC3" }) %>
                                <%= Html.TextBox("Recertification_M1024ICD9MC3", data.ContainsKey("M1024ICD9MC3") ? data["M1024ICD9MC3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9MC3" })%>
                            </td><td>
                                <span class="alphali">c.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesC4", data.ContainsKey("M1024PaymentDiagnosesC4") ? data["M1024PaymentDiagnosesC4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesC4" }) %>
                                <%= Html.TextBox("Recertification_M1024ICD9MC4", data.ContainsKey("M1024ICD9MC4") ? data["M1024ICD9MC4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9MC4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">d.</span>
                                    <%= Html.TextBox("Recertification_M1022PrimaryDiagnosis3", data.ContainsKey("M1022PrimaryDiagnosis3") ? data["M1022PrimaryDiagnosis3"].Answer : "", new { @class = "diagnosis", @id = "Recertification_M1022PrimaryDiagnosis3" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">d.</span>
                                    <%=Html.TextBox("Recertification_M1022ICD9M3", data.ContainsKey("M1022ICD9M3") ? data["M1022ICD9M3"].Answer : "", new { @class = "icd", @id = "Recertification_M1022ICD9M3" }) %>
                                    <label for="Recertification_M1022OtherDiagnose3Rating">Severity:</label>
                                    <%  var severity3 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose3Rating") ? data["M1022OtherDiagnose3Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("Recertification_M1022OtherDiagnose3Rating", severity3, new { @id = "Recertification_M1022OtherDiagnose3Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE3 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis3") ? data["485ExacerbationOrOnsetPrimaryDiagnosis3"].Answer : "0");%>
                                    <%= Html.DropDownList("Recertification_485ExacerbationOrOnsetPrimaryDiagnosis3", OE3, new { @id = "Recertification_485ExacerbationOrOnsetPrimaryDiagnosis3", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("Recertification_M1022PrimaryDiagnosis3Date").Value(data.ContainsKey("M1022PrimaryDiagnosis3Date") && data["M1022PrimaryDiagnosis3Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis3Date"].Answer : "").HtmlAttributes(new { @id = "Recertification_M1022PrimaryDiagnosis3Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">d.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesD3", data.ContainsKey("M1024PaymentDiagnosesD3") ? data["M1024PaymentDiagnosesD3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesD3" }) %>
                                <%= Html.TextBox("Recertification_M1024ICD9MD3", data.ContainsKey("M1024ICD9MD3") ? data["M1024ICD9MD3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9MD3" })%>
                            </td><td>
                                <span class="alphali">d.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesD4", data.ContainsKey("M1024PaymentDiagnosesD4") ? data["M1024PaymentDiagnosesD4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesD4" }) %>
                                <%= Html.TextBox("Recertification_M1024ICD9MD4", data.ContainsKey("M1024ICD9MD4") ? data["M1024ICD9MD4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9MD4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">e.</span>
                                    <%= Html.TextBox("Recertification_M1022PrimaryDiagnosis4", data.ContainsKey("M1022PrimaryDiagnosis4") ? data["M1022PrimaryDiagnosis4"].Answer : "", new { @class = "diagnosis", @id = "Recertification_M1022PrimaryDiagnosis4" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">e.</span>
                                    <%=Html.TextBox("Recertification_M1022ICD9M4", data.ContainsKey("M1022ICD9M4") ? data["M1022ICD9M4"].Answer : "", new { @class = "icd", @id = "Recertification_M1022ICD9M4" }) %>
                                    <label for="Recertification_M1022OtherDiagnose4Rating">Severity:</label>
                                    <%  var severity4 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose4Rating") ? data["M1022OtherDiagnose4Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("Recertification_M1022OtherDiagnose4Rating", severity4, new { @id = "Recertification_M1022OtherDiagnose4Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE4 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis4") ? data["485ExacerbationOrOnsetPrimaryDiagnosis4"].Answer : "0");%>
                                    <%= Html.DropDownList("Recertification_485ExacerbationOrOnsetPrimaryDiagnosis4", OE3, new { @id = "Recertification_485ExacerbationOrOnsetPrimaryDiagnosis4", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("Recertification_M1022PrimaryDiagnosis4Date").Value(data.ContainsKey("M1022PrimaryDiagnosis4Date") && data["M1022PrimaryDiagnosis4Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis4Date"].Answer : "").HtmlAttributes(new { @id = "Recertification_M1022PrimaryDiagnosis4Date", @class = "diagnosisDate date" })%>
                                </div>
                            </td><td>
                                <span class="alphali">e.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesE3", data.ContainsKey("M1024PaymentDiagnosesE3") ? data["M1024PaymentDiagnosesE3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesE3" }) %>
                                <%= Html.TextBox("Recertification_M1024ICD9ME3", data.ContainsKey("M1024ICD9ME3") ? data["M1024ICD9ME3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9ME3" })%>
                            </td><td>
                                <span class="alphali">e.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesE4", data.ContainsKey("M1024PaymentDiagnosesE4") ? data["M1024PaymentDiagnosesE4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesE4" }) %>
                                <%= Html.TextBox("Recertification_M1024ICD9ME4", data.ContainsKey("M1024ICD9ME4") ? data["M1024ICD9ME4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9ME4" })%>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <div class="halfOfTd">
                                    <span class="alphali">f.</span>
                                    <%= Html.TextBox("Recertification_M1022PrimaryDiagnosis5", data.ContainsKey("M1022PrimaryDiagnosis5") ? data["M1022PrimaryDiagnosis5"].Answer : "", new { @class = "diagnosis", @id = "Recertification_M1022PrimaryDiagnosis5" }) %>
                                </div><div class="halfOfTd">
                                    <span class="alphali">f.</span>
                                    <%=Html.TextBox("Recertification_M1022ICD9M5", data.ContainsKey("M1022ICD9M5") ? data["M1022ICD9M5"].Answer : "", new { @class = "icd", @id = "Recertification_M1022ICD9M5" }) %>
                                    <label for="Recertification_M1022OtherDiagnose5Rating">Severity:</label>
                                    <%  var severity5 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = " " },
                                            new SelectListItem { Text = "0 ", Value = "00" },
                                            new SelectListItem { Text = "1", Value = "01" },
                                            new SelectListItem { Text = "2", Value = "02" },
                                            new SelectListItem { Text = "3", Value = "03" },
                                            new SelectListItem { Text = "4", Value = "04" }
                                        }, "Value", "Text", data.ContainsKey("M1022OtherDiagnose5Rating") ? data["M1022OtherDiagnose5Rating"].Answer : " ");%>
                                    <%= Html.DropDownList("Recertification_M1022OtherDiagnose5Rating", severity5, new { @id = "Recertification_M1022OtherDiagnose5Rating", @class = "severity" })%>
                                </div><div class="padDignosis">
                                    <label class="strong">O/E</label>
                                    <%  var OE5 = new SelectList(new[] {
                                            new SelectListItem { Text = "", Value = "0" },
                                            new SelectListItem { Text = "Exacerbation ", Value = "1" },
                                            new SelectListItem { Text = "Onset", Value = "2" }
                                        }, "Value", "Text", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis5") ? data["485ExacerbationOrOnsetPrimaryDiagnosis5"].Answer : "0");%>
                                    <%= Html.DropDownList("Recertification_485ExacerbationOrOnsetPrimaryDiagnosis5", OE3, new { @id = "Recertification_485ExacerbationOrOnsetPrimaryDiagnosis5", @class = "oe" })%>
                                    <%= Html.Telerik().DatePicker().Name("Recertification_M1022PrimaryDiagnosis5Date").Value(data.ContainsKey("M1022PrimaryDiagnosis5Date") && data["M1022PrimaryDiagnosis5Date"].Answer.IsNotNullOrEmpty() ? data["M1022PrimaryDiagnosis5Date"].Answer : "").HtmlAttributes(new { @id = "Recertification_M1022PrimaryDiagnosis5Date", @class = "diagnosisDate date" })%>
                                </div>
                                <div class="float_right oasis">
                                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1022');">?</div>
                                </div>
                            </td><td>
                                <span class="alphali">f.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesF3", data.ContainsKey("M1024PaymentDiagnosesF3") ? data["M1024PaymentDiagnosesF3"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesF3" }) %>
                                <%= Html.TextBox("Recertification_M1024ICD9MF3", data.ContainsKey("M1024ICD9MF3") ? data["M1024ICD9MF3"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9MF3" })%>
                                <div class="float_right oasis">
                                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1024');">?</div>
                                </div>
                            </td><td>
                                <span class="alphali">f.</span>
                                <%= Html.TextBox("Recertification_M1024PaymentDiagnosesF4", data.ContainsKey("M1024PaymentDiagnosesF4") ? data["M1024PaymentDiagnosesF4"].Answer : "", new { @class = "diagnosisM1024 icdtext", @id = "Recertification_M1024PaymentDiagnosesF4" }) %>
                                <%= Html.TextBox("Recertification_M1024ICD9MF4", data.ContainsKey("M1024ICD9MF4") ? data["M1024ICD9MF4"].Answer : "", new { @class = "ICDM1024 icdwidth", @id = "Recertification_M1024ICD9MF4" })%>
                                <div class="float_right oasis">
                                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1024');">?</div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset class="loc485">
        <legend>Surgical Procedure (Locator #12)</legend>
        <div class="wide_column">
            <div class="row">
                <table class="form layout_auto align_center">
                    <thead>
                        <tr>
                            <th>Surgical Procedure</th>
                            <th>Code</th>
                            <th>Date</th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td><%= Html.TextBox("Recertification_485SurgicalProcedureDescription1", data.ContainsKey("485SurgicalProcedureDescription1") ? data["485SurgicalProcedureDescription1"].Answer : "", new { @class = "procedureDiagnosis", @id = "Recertification_485SurgicalProcedureDescription1" })%></td>
                            <td><%= Html.TextBox("Recertification_485SurgicalProcedureCode1", data.ContainsKey("485SurgicalProcedureCode1") ? data["485SurgicalProcedureCode1"].Answer : "", new { @class = "procedureICD pad", @id = "Recertification_485SurgicalProcedureCode1" })%></td>
                            <td><%= Html.Telerik().DatePicker().Name("Recertification_485SurgicalProcedureCode1Date").Value(data.ContainsKey("485SurgicalProcedureCode1Date") && data["485SurgicalProcedureCode1Date"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode1Date"].Answer : "").HtmlAttributes(new { @id = "Recertification_485SurgicalProcedureCode1Date", @class = "date" })%></td>
                        </tr><tr>
                            <td><%= Html.TextBox("Recertification_485SurgicalProcedureDescription2", data.ContainsKey("485SurgicalProcedureDescription2") ? data["485SurgicalProcedureDescription2"].Answer : "", new { @class = "procedureDiagnosis", @id = "Recertification_485SurgicalProcedureDescription2" })%></td>
                            <td><%= Html.TextBox("Recertification_485SurgicalProcedureCode2", data.ContainsKey("485SurgicalProcedureCode2") ? data["485SurgicalProcedureCode2"].Answer : "", new { @class = "procedureICD pad", @id = "Recertification_485SurgicalProcedureCode2" })%></td>
                            <td><%= Html.Telerik().DatePicker().Name("Recertification_485SurgicalProcedureCode2Date").Value(data.ContainsKey("485SurgicalProcedureCode2Date") && data["485SurgicalProcedureCode2Date"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode2Date"].Answer : "").HtmlAttributes(new { @id = "Recertification_485SurgicalProcedureCode2Date", @class = "date" })%></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset><fieldset class="oasis">
        <legend>Therapies the Patient Receives at Home</legend>
        <div class="wide_column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1030');">(M1030)</a> Therapies the patient receives at home (Mark all that apply)</label>
                <table class="form">
                    <tbody>
                        <tr>
                            <td>
                                <input name="Recertification_M1030HomeTherapiesInfusion" value="" type="hidden" />
                                <%= string.Format("<input id='Recertification_M1030HomeTherapiesInfusion' name='Recertification_M1030HomeTherapiesInfusion' value='1' class='radio float_left M1030' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesInfusion") && data["M1030HomeTherapiesInfusion"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="Recertification_M1030HomeTherapiesInfusion"><span class="float_left">1 &ndash;</span><span class="normal margin">Intravenous or infusion therapy (excludes TPN)</span></label>
                            </td><td>
                                <input name="Recertification_M1030HomeTherapiesParNutrition" value="" type="hidden" />
                                <%= string.Format("<input id='Recertification_M1030HomeTherapiesParNutrition' name='Recertification_M1030HomeTherapiesParNutrition' value='1' class='radio float_left M1030' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesParNutrition") && data["M1030HomeTherapiesParNutrition"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="Recertification_M1030HomeTherapiesParNutrition"><span class="float_left">2 &ndash;</span><span class="normal margin">Parenteral nutrition (TPN or lipids)</span></label>
                            </td>
                        </tr><tr>
                            <td>
                                <input name="Recertification_M1030HomeTherapiesEntNutrition" value="" type="hidden" />
                                <%= string.Format("<input id='Recertification_M1030HomeTherapiesEntNutrition' name='Recertification_M1030HomeTherapiesEntNutrition' value='1' class='radio float_left M1030' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesEntNutrition") && data["M1030HomeTherapiesEntNutrition"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="Recertification_M1030HomeTherapiesEntNutrition"><span class="float_left">3 &ndash;</span><span class="normal margin">Enteral nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial entry into the alimentary canal)</span></label>
                            </td><td>
                                <input name="Recertification_M1030HomeTherapiesNone" value="" type="hidden" />
                                <%= string.Format("<input id='Recertification_M1030HomeTherapiesNone' name='Recertification_M1030HomeTherapiesNone' value='1' class='radio float_left M1030' type='checkbox' {0} />", data.ContainsKey("M1030HomeTherapiesNone") && data["M1030HomeTherapiesNone"].Answer == "1" ? "checked='checked'" : "")%>
                                <label for="Recertification_M1030HomeTherapiesNone"><span class="float_left">4 &ndash;</span><span class="normal margin">None of the above</span></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1030');">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Recertification.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
           <li><%=string.Format("<a href=\"javascript:void(0);\" onclick=\"Recertification.FormSubmit($(this),{0});\">Check for Errors</a>", "function(){" + string.Format("UserInterface.ShowOasisValidationModal('Recertification_ValidationContainer','{0}','{1}','{2}','Recertification');", Model.Id, Model.PatientId, Model.EpisodeId) + "}")%></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Oasis.showIfRadioEquals("Recertification_485Allergies", "Yes", $("#Recertification_485AllergiesDescription"));
    Oasis.noneOfTheAbove($("#Recertification_M1030HomeTherapiesNone"), $("#window_recertification .M1030"));
</script>