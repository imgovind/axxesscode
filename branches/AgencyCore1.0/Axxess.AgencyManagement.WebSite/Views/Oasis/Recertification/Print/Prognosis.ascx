﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% var data = Model.ToDictionary(); %>
<script type="text/javascript">
    printview.addsection(
        printview.col(5,
            printview.checkbox("Guarded",<%= data != null && data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Guarded" ? "true" : "false"%>) +
            printview.checkbox("Poor",<%= data != null && data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Poor" ? "true" : "false"%>) +
            printview.checkbox("Fair",<%= data != null && data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Fair" ? "true" : "false"%>) +
            printview.checkbox("Good",<%= data != null && data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Good" ? "true" : "false"%>) +
            printview.checkbox("Excellent",<%= data != null && data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Excellent" ? "true" : "false"%>)),"Prognosis");
    printview.addsection(
        printview.col(4,
            printview.checkbox("Amputation",<%= data != null && data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Paralysis",<%= data != null && data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("Legally Blind",<%= data != null && data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer.Split(',').Contains("9") ? "true" : "false"%>) +
            printview.checkbox("Bowel/Bladder Incontinence",<%= data != null && data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Endurance",<%= data != null && data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Dyspnea",<%= data != null && data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer.Split(',').Contains("A") ? "true" : "false"%>) +
            printview.checkbox("Contracture",<%= data != null && data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Ambulation",<%= data != null && data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("Hearing",<%= data != null && data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Speech",<%= data != null && data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
            printview.checkbox("Other (Specify)",<%= data != null && data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer.Split(',').Contains("B") ? "true" : "false"%>)) +
        printview.span("<%= data != null && data.ContainsKey("485FunctionLimitationsOther") && data["485FunctionLimitationsOther"].Answer.IsNotNullOrEmpty() ? data["485FunctionLimitationsOther"].Answer : ""%>",false,2),
        "Functional Limitations");
    printview.addsection(
        printview.col(2,
            printview.span("Are there any advanced directives?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("485AdvancedDirectives") && data["485AdvancedDirectives"].Answer == "Yes" ? "true" : "false" %>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("485AdvancedDirectives") && data["485AdvancedDirectives"].Answer == "No" ? "true" : "false" %>))) +<%
    if (data == null || data.ContainsKey("485AdvancedDirectives") == false || data["485AdvancedDirectives"].Answer != "No") { %>
        printview.col(5,
            printview.span("Intent:",true) +
            printview.checkbox("DNR",<%= data != null && data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "DNR" ? "true" : "false" %>) +
            printview.checkbox("Living Will",<%= data != null && data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "Living Will" ? "true" : "false" %>) +
            printview.checkbox("Med. Power of Attorney",<%= data != null && data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "Medical Power of Attorney" ? "true" : "false" %>) +
            printview.checkbox("Other <%= data != null && data.ContainsKey("485AdvancedDirectivesIntentOther") && data["485AdvancedDirectivesIntentOther"].Answer.IsNotNullOrEmpty() ? data["485AdvancedDirectivesIntentOther"].Answer : ""%>",<%= data != null && data.ContainsKey("485AdvancedDirectivesIntent") && data["485AdvancedDirectivesIntent"].Answer == "Other" ? "true" : "false" %>)) +
        printview.col(2,
            printview.span("Copy on file at agency?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("485AdvancedDirectivesCopyOnFile") && data["485AdvancedDirectivesCopyOnFile"].Answer == "Yes" ? "true" : "false" %>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("485AdvancedDirectivesCopyOnFile") && data["485AdvancedDirectivesCopyOnFile"].Answer == "No" ? "true" : "false" %>)) +
            printview.span("Patient was provided written and verbal information?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("485AdvancedDirectivesWrittenAndVerbal") && data["485AdvancedDirectivesWrittenAndVerbal"].Answer == "Yes" ? "true" : "false" %>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("485AdvancedDirectivesWrittenAndVerbal") && data["485AdvancedDirectivesWrittenAndVerbal"].Answer == "No" ? "true" : "false" %>)) +
            printview.span("Is the Patient DNR (Do Not Resuscitate)?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericPatientDNR") && data["GenericPatientDNR"].Answer == "Yes" ? "true" : "false" %>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericPatientDNR") && data["GenericPatientDNR"].Answer == "No" ? "true" : "false" %>))) +
        printview.span("Comments",true) +
        printview.span("<%= data != null && data.ContainsKey("485AdvancedDirectivesComment") && data["485AdvancedDirectivesComment"].Answer.IsNotNullOrEmpty() ? data["485AdvancedDirectivesComment"].Answer : ""%>",false,2) +<%
    } %>"","Advanced Directives");
</script>