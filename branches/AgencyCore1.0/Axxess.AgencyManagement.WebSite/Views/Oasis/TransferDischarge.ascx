﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("{0}{1}{2}{3}",
            "<script type='text/javascript'>acore.renamewindow('Oasis-C Transfer For Discharge | ",
        data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "",
        data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "",
        "','transferfordischarge');</script>")
%>
<div id="transferInPatientDischargedTabs" class=" tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#clinicalRecord_transfer">Clinical Record Items</a></li>
        <li><a href="#riskassessment_transfer">Risk Assessment</a></li>
        <li><a href="#cardiacstatus_transfer">Cardiac Status</a></li>
        <li><a href="#medications_transfer">Medications</a></li>
        <li><a href="#emergentcare_transfer">Emergent Care</a></li>
        <li><a href="#dischargeAdd_transfer">Transfer</a></li>
    </ul>
    <div id="clinicalRecord_transfer" class="general">
        <% Html.RenderPartial("~/Views/Oasis/TransferDischarge/Demographics.ascx", Model); %></div>
    <div id="riskassessment_transfer" class="general loading">
    </div>
    <div id="cardiacstatus_transfer" class="general loading">
    </div>
    <div id="medications_transfer" class="general loading">
    </div>
    <div id="emergentcare_transfer" class="general loading">
    </div>
    <div id="dischargeAdd_transfer" class="general loading">
    </div>
</div>
