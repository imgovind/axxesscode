﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpAdlForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("FollowUp_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<%= Html.Hidden("categoryType", "Adl")%> 
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1810');">(M1810)</a> Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts and blouses, managing zippers, buttons, and snaps:</label>
                <%=Html.Hidden("FollowUp_M1810CurrentAbilityToDressUpper")%>
                <div>
                    <%=Html.RadioButton("FollowUp_M1810CurrentAbilityToDressUpper", "00", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "00" ? true : false, new { @id = "FollowUp_M1810CurrentAbilityToDressUpper0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1810CurrentAbilityToDressUpper0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1810CurrentAbilityToDressUpper", "01", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "01" ? true : false, new { @id = "FollowUp_M1810CurrentAbilityToDressUpper1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1810CurrentAbilityToDressUpper1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to dress upper body without assistance if clothing is laid out or handed to the patient.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1810CurrentAbilityToDressUpper", "02", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "02" ? true : false, new { @id = "FollowUp_M1810CurrentAbilityToDressUpper2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1810CurrentAbilityToDressUpper2"><span class="float_left">2 &ndash;</span><span class="normal margin">Someone must help the patient put on upper body clothing.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1810');">?</div>
                    </div>
                    <%=Html.RadioButton("FollowUp_M1810CurrentAbilityToDressUpper", "03", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "03" ? true : false, new { @id = "FollowUp_M1810CurrentAbilityToDressUpper3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1810CurrentAbilityToDressUpper3"><span class="float_left">3 &ndash;</span><span class="normal margin">Patient depends entirely upon another person to dress the upper body.</span></label>
                </div>
            </div>
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1820');">(M1820)</a> Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes:</label>
                <%=Html.Hidden("FollowUp_M1820CurrentAbilityToDressLower")%>
                <div>
                    <%=Html.RadioButton("FollowUp_M1820CurrentAbilityToDressLower", "00", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "00" ? true : false, new { @id = "FollowUp_M1820CurrentAbilityToDressLower0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1820CurrentAbilityToDressLower0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to obtain, put on, and remove clothing and shoes without assistance.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1820CurrentAbilityToDressLower", "01", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "01" ? true : false, new { @id = "FollowUp_M1820CurrentAbilityToDressLower1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1820CurrentAbilityToDressLower1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1820CurrentAbilityToDressLower", "02", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "02" ? true : false, new { @id = "FollowUp_M1820CurrentAbilityToDressLower2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1820CurrentAbilityToDressLower2"><span class="float_left">2 &ndash;</span><span class="normal margin">Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1820CurrentAbilityToDressLower", "03", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "03" ? true : false, new { @id = "FollowUp_M1820CurrentAbilityToDressLower3", @class = "radio float_left" })%>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1820');">?</div>
                    </div>
                    <label for="FollowUp_M1820CurrentAbilityToDressLower3"><span class="float_left">3 &ndash;</span><span class="normal margin">Patient depends entirely upon another person to dress lower body.</span></label>
                </div>
            </div>
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1830');">(M1830)</a> Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).</label>
                <%=Html.Hidden("FollowUp_M1830CurrentAbilityToBatheEntireBody")%>
                <div>
                    <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "00", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "00" ? true : false, new { @id = "FollowUp_M1830CurrentAbilityToBatheEntireBody0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1830CurrentAbilityToBatheEntireBody0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to bathe self in shower or tub independently, including getting in and out of tub/shower.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "01", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "01" ? true : false, new { @id = "FollowUp_M1830CurrentAbilityToBatheEntireBody1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1830CurrentAbilityToBatheEntireBody1"><span class="float_left">1 &ndash;</span><span class="normal margin">With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "02", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "02" ? true : false, new { @id = "FollowUp_M1830CurrentAbilityToBatheEntireBody2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1830CurrentAbilityToBatheEntireBody2">
                        <span class="float_left">2 &ndash;</span>
                        <span class="normal margin">
                            Able to bathe in shower or tub with the intermittent assistance of another person:
                            <ul>
                                <li><span class="float_left">(a)</span><span class="radio">for intermittent supervision or encouragement or reminders, OR</span></li>
                                <li><span class="float_left">(b)</span><span class="radio">to get in and out of the shower or tub, OR</span></li>
                                <li><span class="float_left">(c)</span><span class="radio">for washing difficult to reach areas.</span></li>
                            </ul>
                        </span>
                    </label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "03", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "03" ? true : false, new { @id = "FollowUp_M1830CurrentAbilityToBatheEntireBody3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1830CurrentAbilityToBatheEntireBody3"><span class="float_left">3 &ndash;</span><span class="normal margin">Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "04", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "04" ? true : false, new { @id = "FollowUp_M1830CurrentAbilityToBatheEntireBody4", @class = "radio float_left" })%>
                    <label for="FollowUp_M1830CurrentAbilityToBatheEntireBody4"><span class="float_left">4 &ndash;</span><span class="normal margin">Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "05", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "05" ? true : false, new { @id = "FollowUp_M1830CurrentAbilityToBatheEntireBody5", @class = "radio float_left" })%>
                    <label for="FollowUp_M1830CurrentAbilityToBatheEntireBody5"><span class="float_left">5 &ndash;</span><span class="normal margin">Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the assistance or supervision of another person throughout the bath.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1830');">?</div>
                    </div>
                    <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "06", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "06" ? true : false, new { @id = "FollowUp_M1830CurrentAbilityToBatheEntireBody6", @class = "radio float_left" })%>
                    <label for="FollowUp_M1830CurrentAbilityToBatheEntireBody6"><span class="float_left">6 &ndash;</span><span class="normal margin">Unable to participate effectively in bathing and is bathed totally by another person.</span></label>
                </div>
            </div>
        </div><div class="column">
        <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1840');">(M1840)</a> Toilet Transferring: Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.</label>
                <%=Html.Hidden("FollowUp_M1840ToiletTransferring")%>
                <div>
                    <%=Html.RadioButton("FollowUp_M1840ToiletTransferring", "00", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "00" ? true : false, new { @id = "FollowUp_M1840ToiletTransferring0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1840ToiletTransferring0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to get to and from the toilet and transfer independently with or without a device.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1840ToiletTransferring", "01", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "01" ? true : false, new { @id = "FollowUp_M1840ToiletTransferring1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1840ToiletTransferring1"><span class="float_left">1 &ndash;</span><span class="normal margin">When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1840ToiletTransferring", "02", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "02" ? true : false, new { @id = "FollowUp_M1840ToiletTransferring2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1840ToiletTransferring2"><span class="float_left">2 &ndash;</span><span class="normal margin">Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1840ToiletTransferring", "03", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "03" ? true : false, new { @id = "FollowUp_M1840ToiletTransferring3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1840ToiletTransferring3"><span class="float_left">3 &ndash;</span><span class="normal margin">Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1840');">?</div>
                    </div>
                    <%=Html.RadioButton("FollowUp_M1840ToiletTransferring", "04", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "04" ? true : false, new { @id = "FollowUp_M1840ToiletTransferring4", @class = "radio float_left" })%>
                    <label for="FollowUp_M1840ToiletTransferring4"><span class="float_left">4 &ndash;</span><span class="normal margin">Is totally dependent in toileting.</span></label>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1850');">(M1850)</a> Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.</label>
                <%=Html.Hidden("FollowUp_M1850Transferring")%>
                <div>
                    <%=Html.RadioButton("FollowUp_M1850Transferring", "00", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "00" ? true : false, new { @id = "FollowUp_M1850Transferring0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1850Transferring0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently transfer.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1850Transferring", "01", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "01" ? true : false, new { @id = "FollowUp_M1850Transferring1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1850Transferring1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to transfer with minimal human assistance or with use of an assistive device.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1850Transferring", "02", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "02" ? true : false, new { @id = "FollowUp_M1850Transferring2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1850Transferring2"><span class="float_left">2 &ndash;</span><span class="normal margin">Able to bear weight and pivot during the transfer process but unable to transfer self.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1850Transferring", "03", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "03" ? true : false, new { @id = "FollowUp_M1850Transferring3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1850Transferring3"><span class="float_left">3 &ndash;</span><span class="normal margin">Unable to transfer self and is unable to bear weight or pivot when transferred by another person.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1850Transferring", "04", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "04" ? true : false, new { @id = "FollowUp_M1850Transferring4", @class = "radio float_left" })%>
                    <label for="FollowUp_M1850Transferring4"><span class="float_left">4 &ndash;</span><span class="normal margin">Bedfast, unable to transfer but is able to turn and position self in bed.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1850');">?</div>
                    </div>
                    <%=Html.RadioButton("FollowUp_M1850Transferring", "05", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "05" ? true : false, new { @id = "FollowUp_M1850Transferring5", @class = "radio float_left" })%>
                    <label for="FollowUp_M1850Transferring5"><span class="float_left">5 &ndash;</span><span class="normal margin">Bedfast, unable to transfer and is unable to turn and position self.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1860');">(M1860)</a> Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.</label>
                <%=Html.Hidden("FollowUp_M1860AmbulationLocomotion")%>
                <div>
                    <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "00", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "00" ? true : false, new { @id = "FollowUp_M1860AmbulationLocomotion0", @class = "radio float_left" })%>
                    <label for="FollowUp_M1860AmbulationLocomotion0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (i.e., needs no human assistance or assistive device).</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "01", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "01" ? true : false, new { @id = "FollowUp_M1860AmbulationLocomotion1", @class = "radio float_left" })%>
                    <label for="FollowUp_M1860AmbulationLocomotion1"><span class="float_left">1 &ndash;</span><span class="normal margin">With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "02", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "02" ? true : false, new { @id = "FollowUp_M1860AmbulationLocomotion2", @class = "radio float_left" })%>
                    <label for="FollowUp_M1860AmbulationLocomotion2"><span class="float_left">2 &ndash;</span><span class="normal margin">Requires use of a two-handed device (e.g., walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "03", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "03" ? true : false, new { @id = "FollowUp_M1860AmbulationLocomotion3", @class = "radio float_left" })%>
                    <label for="FollowUp_M1860AmbulationLocomotion3"><span class="float_left">3 &ndash;</span><span class="normal margin">Able to walk only with the supervision or assistance of another person at all times.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "04", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "04" ? true : false, new { @id = "FollowUp_M1860AmbulationLocomotion4", @class = "radio float_left" })%>
                    <label for="FollowUp_M1860AmbulationLocomotion4"><span class="float_left">4 &ndash;</span><span class="normal margin">Chairfast, unable to ambulate but is able to wheel self independently.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "05", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "05" ? true : false, new { @id = "FollowUp_M1860AmbulationLocomotion5", @class = "radio float_left" })%>
                    <label for="FollowUp_M1860AmbulationLocomotion5"><span class="float_left">5 &ndash;</span><span class="normal margin">Chairfast, unable to ambulate and is unable to wheel self.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1860');">?</div>
                    </div>
                    <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "06", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "06" ? true : false, new { @id = "FollowUp_M1860AmbulationLocomotion6", @class = "radio float_left" })%>
                    <label for="FollowUp_M1860AmbulationLocomotion6"><span class="float_left">6 &ndash;</span><span class="normal margin">Bedfast, unable to ambulate or be up in a chair.</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="FollowUp.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Oasis.Validate('{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "StartOfCare")%></li>
        </ul>
    </div>
</div>
<% } %>