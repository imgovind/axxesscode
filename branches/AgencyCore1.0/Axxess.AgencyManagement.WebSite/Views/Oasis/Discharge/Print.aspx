﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Assessment>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<% var data = Model.ToDictionary(); %>
<head>
    <%= string.Format("<title>Discharge | {0}{1}, {2}{3}</title>",data.ContainsKey("M0040LastName")?data["M0040LastName"].Answer:"",data.ContainsKey("M0040Suffix")?" "+data["M0040Suffix"].Answer:"",data.ContainsKey("M0040FirstName")?data["M0040FirstName"].Answer:"",data.ContainsKey("M0040MI")?" "+data["M0040MI"].Answer:"") %>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
</head>
<body>
    <div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Discharge</h1>
        <h2>Demographics</h2>
        <div class="tricol">
            <span><strong>Time In:</strong>00:00</span>
            <span><strong>Time Out:</strong>00:00</span>
            <span><strong>Visit Date:</strong>00/00/0000</span>
        </div><div class="bicol">
            <span><strong>(M0020) Patient ID Number:</strong><%= data.ContainsKey("M0020PatientIdNumber") ? data["M0020PatientIdNumber"].Answer : "" %></span>
            <span><strong>(M0030) Discharge Date:</strong><%= data.ContainsKey("M0030SocDate") ? data["M0030SocDate"].Answer : "" %></span>
            <span><strong>(M0032) Resuption of Care Date:</strong><%= data.ContainsKey("M0032ROCDate") ? data["M0032ROCDate"].Answer : "" %></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0032ROCDateNotApplicable") && data["M0032ROCDateNotApplicable"].Answer == "1" ? "&#x2713;" : "" %></span> NA &ndash; Not Applicable</span>
        </div><div>
            <span><strong>Episode Start Date:</strong><%= data.ContainsKey("GenericEpisodeStartDate") ? data["GenericEpisodeStartDate"].Answer : "" %></span>
        </div><div class="bicol">
            <span><strong>(M0040) Patient Name:</strong>(Last Suffix First MI)</span>
            <span><%= data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : ""%> <%= data.ContainsKey("M0040Suffix") ? data["M0040Suffix"].Answer : ""%> <%= data.ContainsKey("M0040FirstName") ? data["M0040FirstName"].Answer : ""%> <%= data.ContainsKey("M0040MI") ? data["M0040MI"].Answer : "" %></span>
            <span><strong>(M0064) Social Security Number:</strong><%= data.ContainsKey("M0064PatientSSN") ? data["M0064PatientSSN"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0064PatientSSNUnknown") && data["M0064PatientSSNUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Unknown or Not Available</span>
        </div><div class="bicol">
            <span><strong>(M0050) Patient State:</strong><%= data.ContainsKey("M0050PatientState") ? data["M0050PatientState"].Answer : ""%></span>
            <span><strong>(M0060) Patient Zip Code:</strong><%= data.ContainsKey("M0060PatientZipCode") ? data["M0060PatientZipCode"].Answer : ""%></span>
        </div><div class="bicol">
            <span><strong>(M0063) Medicare Number:</strong><%= data.ContainsKey("M0063PatientMedicareNumber") ? data["M0063PatientMedicareNumber"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0063PatientMedicareNumberUnknown") && data["M0063PatientMedicareNumberUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No Medicare</span>
            <span><strong>(M0065) Medicaid Number:</strong><%= data.ContainsKey("M0065PatientMedicaidNumber") ? data["M0065PatientMedicaidNumber"].Answer : ""%></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0065PatientMedicaidNumberUnknown") && data["M0065PatientMedicaidNumberUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No Medicaid</span>
        </div><div class="bicol">
            <span><strong>(M0066) Birth Date:</strong><%= data.ContainsKey("M0066PatientDoB") ? data["M0066PatientDoB"].Answer : ""%></span>
            <span><strong>(M0069) Gender:</strong><%= data.ContainsKey("M0069Gender") ? data["M0069Gender"].Answer == "1" ? "Male" : "Female" : "" %></span>
        </div><div class="bicol">
            <span><strong>(M0080) Discipline of Person Completing Assessment:</strong><% if (data.ContainsKey("M0080DisciplinePerson")) { switch(data["M0080DisciplinePerson"].Answer) { case "01": %>1 &ndash; RN<% break; case "02": %>2 &ndash; PT<% break; case "03": %>3 &ndash; SLP/ST<% break; case "04": %>4 &ndash; OT<% break; } } %></span>
            <span><strong>(M0090) Date Assessment Completed:</strong><%= data.ContainsKey("M0090AssessmentCompleted") ? data["M0090AssessmentCompleted"].Answer : ""%></span>
        </div><div>
            <span><strong>(M0100) This Assessment is Currently Being Completed for the Following Reason:</strong></span>
            <span><strong>Start/Resuption of Care</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Discharge&mdash;further visits planned</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Resumption of care (after inpatient stay)</span>
            <span><strong>Follow-Up</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Recertification (follow-up) reassessment <em>[Go to M0110]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Other follow-up <em>[Go to M0110]</em></span>
            <span><strong>Transfer to an Inpatient Facility</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "06" ? "&#x2713;" : ""%></span> 6 &ndash; Transfer to inpatient facility&mdash;patient not discharged from agency <em>[Go to M1040]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "07" ? "&#x2713;" : ""%></span> 7 &ndash; Transfer to inpatient facility&mdash;patient discharged from agency <em>[Go to M1040]</em></span>
            <span><strong>Discharge from Agency – Not to an Inpatient Facility</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "08" ? "&#x2713;" : ""%></span> 8 &ndash; Death at home <em>[Go to M0903]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0100AssessmentType") && data["M0100AssessmentType"].Answer == "09" ? "&#x2713;" : ""%></span> 9 &ndash; Discharge from agency <em>[Go to M1040]</em></span>
        </div><div>
            <span><strong>(M0102) Date of Physician-ordered Discharge (Resumption of Care):</strong><%= data.ContainsKey("M0102PhysicianOrderedDate") && data["M0102PhysicianOrderedDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer + " <em>[Go to M0110]</em>" : "" %></span>
            <span>If the physician indicated a specific Discharge (resumption of care) date when the patient was referred for home health services, record the date specified.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && data["M0102PhysicianOrderedDateNotApplicable"].Answer == "1" ? "&#x2713;" : ""%></span> NA &ndash; No specific SOC date ordered by physician</span>
        </div><div>
            <span><strong>(M0104) Date of Referral:</strong><%= data.ContainsKey("M0104ReferralDate") && data["M0104ReferralDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer : "" %></span>
            <span>Indicate the date that the written or documented orders from the physician or physician designee for initiation or resumption of care were received by the HHA.</span>
        </div><div>
            <span><strong>(M0110) Episode Timing:</strong><%= data.ContainsKey("M0104ReferralDate") && data["M0104ReferralDate"].Answer.IsNotNullOrEmpty() ? data["M0102PhysicianOrderedDate"].Answer : "" %></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Early</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Later</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "UK" ? "&#x2713;" : ""%></span> UK &ndash; Unknown</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0110EpisodeTiming") && data["M0110EpisodeTiming"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Not Applicable/No Medicare case mix group to be defined by this assessment </span>
            <span>Is the Medicare home health payment episode for which this assessment will define a case mix group an &ldquo;early&rdquo; episode or a &ldquo;later&rdquo; episode in the patient&rsquo;s current sequence of adjacent Medicare home health payment episodes?</span>
        </div><div>
            <span><strong>(M0140) Race/Ethnicity <em>(Mark all that apply)</em>:</strong></span>
            <span class="bicol">
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceAMorAN") && data["M0140RaceAMorAN"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; American Indian or Alaska Native</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceAsia") && data["M0140RaceAsia"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Asian</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceBalck") && data["M0140RaceBalck"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Black or African American</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceHispanicOrLatino") && data["M0140RaceHispanicOrLatino"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Hispanic or Latino</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceNHOrPI") && data["M0140RaceNHOrPI"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Native Hawaiian or Pacific Islander</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0140RaceWhite") && data["M0140RaceWhite"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; White</span>        
            </span>
        </div><div>
            <span><strong>(M0150) Current Payment Sources for Home Care <em>(Mark all that apply)</em>:</strong></span>
            <span class="bicol">
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceNone") && data["M0150PaymentSourceNone"].Answer == "1" ? "&#x2713;" : ""%></span> 0 &ndash; None&mdash;No charge for current services</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCREFFS") && data["M0150PaymentSourceMCREFFS"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Medicare (traditional fee-for-service)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCREHMO") && data["M0150PaymentSourceMCREHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Medicare (HMO/Managed Care/Advantage plan)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMCAIDFFS") && data["M0150PaymentSourceMCAIDFFS"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Medicaid (traditional fee-for-service)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceMACIDHMO") && data["M0150PaymentSourceMACIDHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Medicaid (HMO/Managed Care)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceWRKCOMP") && data["M0150PaymentSourceWRKCOMP"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Worker&rsquo;s compensation</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceTITLPRO") && data["M0150PaymentSourceTITLPRO"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; Title progams (e.g. Title III, V, or XX)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceOTHGOVT") && data["M0150PaymentSourceOTHGOVT"].Answer == "1" ? "&#x2713;" : ""%></span> 7 &ndash; Other government (e.g. TriCare, VA etc)</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourcePRVINS") && data["M0150PaymentSourcePRVINS"].Answer == "1" ? "&#x2713;" : ""%></span> 8 &ndash; Private Insurance</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourcePRVHMO") && data["M0150PaymentSourcePRVHMO"].Answer == "1" ? "&#x2713;" : ""%></span> 9 &ndash; Private HMO/managed care</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceSelfPay") && data["M0150PaymentSourceSelfPay"].Answer == "1" ? "&#x2713;" : ""%></span> 10 &ndash; Self-pay</span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceOtherSRS") && data["M0150PaymentSourceOtherSRS"].Answer == "1" ? "&#x2713;" : ""%></span> 11 &ndash; Other<%= data.ContainsKey("M0150PaymentSourceOther") && data["M0150PaymentSourceOther"].Answer.IsNotNullOrEmpty() ? ": " + data["M0150PaymentSourceOther"].Answer : "" %></span>        
                <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M0150PaymentSourceUnknown") && data["M0150PaymentSourceUnknown"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Unknown</span>        
            </span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Discharge</h1>
        <h2>Risk Assessment</h2>
        <div>
            <span><strong>(M1040) Influenza Vaccine: Did the patient receive the influenza vaccine from your agency for this year&rsquo;s influenza season (October 1 through March 31) during this episode of care?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes <em>[Go to M1050]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1040InfluenzaVaccine") && data["M1040InfluenzaVaccine"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Does not apply because entire episode of care (SOC/ROC to Transfer/Discharge) is outside this influenza season. <em>[Go to M1050]</em></span>
        </div><div>
            <span><strong>(M1045) Reason Influenza Vaccine not received: If the patient did not receive the influenza vaccine from your agency during this episode of care, state reason:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Received from another health care provider (e.g., physician)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Received from your agency previously during this year&rsquo;s flu season</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Offered and declined</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Assessed and determined to have medical contraindication(s)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Not indicated; patient does not meet age/condition guidelines for influenza vaccine</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "06" ? "&#x2713;" : ""%></span> 6 &ndash; Inability to obtain vaccine due to declared shortage</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && data["M1045InfluenzaVaccineNotReceivedReason"].Answer == "07" ? "&#x2713;" : ""%></span> 7 &ndash; None of the above</span>
        </div><div>
            <span><strong>(M1050) Pneumococcal Vaccine: Did the patient receive pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge)?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1050PneumococcalVaccine") && data["M1050PneumococcalVaccine"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1050PneumococcalVaccine") && data["M1050PneumococcalVaccine"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes <em>[Go to M1500 at TRN; Go to M1230 at DC]</em></span>
        </div><div>
            <span><strong>(M1055) Reason PPV not received: If patient did not receive the pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge), state reason:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Patient has received PPV in the past</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Offered and declined</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Assessed and determined to have medical contraindication(s)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Not indicated; patient does not meet age/condition guidelines for PPV</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1055PPVNotReceivedReason") && data["M1055PPVNotReceivedReason"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; None of the above</span>
        </div>
        <h2>Sensory Status</h2>
        <div>
            <span><strong>(M1230) Speech and Oral (Verbal) Expression of Language <em>(in patient&rsquo;s own language)</em></strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Expresses complex ideas, feelings, and needs clearly, completely, and easily in all situations with no observable impairment.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional errors in word choice, grammar or speech intelligibility; needs minimal prompting or assistance).</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Expresses simple ideas or needs with moderate difficulty (needs prompting or assistance, errors in word choice, organization or speech intelligibility). Speaks in phrases or short sentences.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Has severe difficulty expressing basic ideas or needs and requires maximal assistance or guessing by listener. Speech limited to single words or short phrases.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Unable to express basic needs even with maximal prompting or assistance but is not comatose or unresponsive (e.g., speech is nonsensical or unintelligible).</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1230SpeechAndOral") && data["M1230SpeechAndOral"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Patient nonresponsive or unable to speak.</span>
        </div>
       <h2>Pain</h2>
       <div>
            <span><strong>(M1242) Frequency of Pain Interfering with patient&rsquo;s activity or movement</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Patient has no pain</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Patient has pain that does not interfere with activity or movement</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Less often than daily</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Daily, but not constantly</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1242PainInterferingFrequency") && data["M1242PainInterferingFrequency"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; All of the time</span>
       </div>
        <h2>Integumentary Status</h2>
        <div>
            <span><strong>(M1306) Does this patient have at least one Unhealed Pressure Ulcer at Stage II or Higher or designated as &ldquo;unstageable&rdquo;?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "0" ? "&#x2713;" : ""%></span> 0 &ndash; No <em>[Go to M1322]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1306UnhealedPressureUlcers") && data["M1306UnhealedPressureUlcers"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Discharge</h1>
        <h2>Integumentary Status</h2>
        <div>
            <span><strong>(M1308) Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each Stage: <em>(Enter &ldquo;0&rdquo; if none; excludes Stage I pressure ulcers)</em></strong></span>
        </div>
        <div><table><tbody>
            <tr><td></td><th>Column 1<br /><em>Complete at SOC/ROC/FU &amp; D/C</em></th><th>Column 2<br /><em>Complete at FU &amp; D/C</em></th></tr>
            <tr><td>Stage description &mdash; unhealed pressure ulcers</td><td>Number Currently Present</td><td>Number of those listed in Column 1 that were present on admission (most recent SOC / ROC)</td></tr>
            <tr>
                <td>a. Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer with red pink wound bed, without slough. May also present as an intact or open/ruptured serum-filled blister.</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerCurrent") ? data["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerAdmission") ? data["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td>b. Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone, tendon, or muscles are not exposed. Slough may be present but does not obscure the depth of tissue loss. May include undermining and tunneling.</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") ? data["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerAdmission") ? data["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td>c. Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough or eschar may be present on some parts of the wound bed. Often includes undermining and tunneling.</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") ? data["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedStageIVUlcerAdmission") ? data["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td>d.1 Unstageable: Known or likely but unstageable due to non-removable dressing or device</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td>d.2 Unstageable: Known or likely but unstageable due to coverage of wound bed by slough and/or eschar.</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td>d.3 Unstageable: Suspected deep tissue injury in evolution.</span></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") ? data["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer : "" %></td>
                <td><%= data.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission") ? data["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : "" %></td>
            </tr><tr>
                <td colspan="3">Directions for M1310, M1312, and M1314: If the patient has one or more unhealed (non-epithelialized) Stage III or IV pressure ulcers, identify the Stage III or IV pressure ulcer with the largest surface dimension (length x width) and record in centimeters. If no Stage III or Stage IV pressure ulcers, go to M1320.</td>
            </tr>
        </tbody></table></div>
        <div>
            <span><strong>(M1310) Pressure Ulcer Length: Longest length &ldquo;head-to-toe&rdquo;</strong><%= data.ContainsKey("M1310PressureUlcerLength") ? data["M1310PressureUlcerLength"].Answer : "" %></span>
        </div><div>
            <span><strong>(M1312) Pressure Ulcer Width: Width of the same pressure ulcer; greatest width perpendicular to the length</strong><%= data.ContainsKey("M1312PressureUlcerWidth") ? data["M1312PressureUlcerWidth"].Answer : "" %></span>
        </div><div>
            <span><strong>(M1314) Pressure Ulcer Depth: Depth of the same pressure ulcer; from visible surface to the deepest area</strong><%= data.ContainsKey("M1314PressureUlcerDepth") ? data["M1314PressureUlcerDepth"].Answer : "" %></span>
        </div><div>
            <span><strong>(M1320) Status of Most Problematic (Observable) Pressure Ulcer</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Newly epithelialized</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Fully granulating</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Early/partial granulation</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Not healing</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1320MostProblematicPressureUlcerStatus") && data["M1320MostProblematicPressureUlcerStatus"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; No observable pressure ulcer</span>
        </div><div>
            <span><strong>(M1322) Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable redness of a localized area usually over a bony prominence. The area may be painful, firm, soft, warmer or cooler as compared to adjacent tissue.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Zero</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; One</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Two</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Three</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1322CurrentNumberStageIUlcer") && data["M1322CurrentNumberStageIUlcer"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Four or more</span>
        </div><div>
            <span><strong>(M1324) Stage of Most Problematic Unhealed (Observable) Pressure Ulcer</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Stage I</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Stage II</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Stage III</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Stage IV</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1324MostProblematicUnhealedStage") && data["M1324MostProblematicUnhealedStage"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; No observable pressure ulcer or unhealed pressure ulcer</span>
        </div><div>
            <span><strong>(M1330) Does this patient have a Stasis Ulcer?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No <em>[Go to M1340]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes, patient has BOTH observable and unobservable stasis ulcers</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Yes, patient has observable stasis ulcers ONLY</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1330StasisUlcer") && data["M1330StasisUlcer"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Yes, patient has unobservable stasis ulcers ONLY (known but not observable due to non-removable dressing) <em>[Go to M1340]</em></span>
        </div><div>
            <span><strong>(M1332) Current Number of (Observable) Stasis Ulcer(s)</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; One</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Two</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Three</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1332CurrentNumberStasisUlcer") && data["M1332CurrentNumberStasisUlcer"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Four or more</span>
        </div><div>
            <span><strong>(M1334) Status of Most Problematic (Observable) Stasis Ulcer</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Newly epithelialized</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Fully granulating</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Early/partial granulation</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1334StasisUlcerStatus") && data["M1334StasisUlcerStatus"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Not healing</span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Discharge</h1>
        <h2>Integumentary Status</h2>
        <div>
            <span><strong>(M1340) Does this patient have a Surgical Wound?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No <em>[Go to M1350]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes, patient has at least one (observable) surgical wound</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1340SurgicalWound") && data["M1340SurgicalWound"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Surgical wound known but not observable due to non-removable dressing <em>[Go to M1350]</em></span>
        </div><div>
            <span><strong>(M1342) Status of Most Problematic (Observable) Surgical Wound</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Newly epithelialized</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Fully granulating</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Early/partial granulation</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1342SurgicalWoundStatus") && data["M1342SurgicalWoundStatus"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Not healing</span>
        </div><div>
            <span><strong>(M1350) Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy, other than those described above that is receiving intervention by the home health agency?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "0" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1350SkinLesionOpenWound") && data["M1350SkinLesionOpenWound"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
        </div>
        <h2>Respiratory Status</h2>
        <div>
            <span><strong>(M1400) When is the patient dyspneic or noticeably Short of Breath?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Patient is not short of breath</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; When walking more than 20 feet, climbing stairs</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; With moderate exertion (e.g., while dressing, using commode or bedpan, walking distances less than 20 feet)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; With minimal exertion (e.g., while eating, talking, or performing other ADLs) or with agitation</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1400PatientDyspneic") && data["M1400PatientDyspneic"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; At rest (during day or night)</span>
        </div><div>
            <span><strong>(M1410) Respiratory Treatments utilized at home: <em>(Mark all that apply)</em></strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen") && data["M1410HomeRespiratoryTreatmentsOxygen"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Oxygen (intermittent or continuous)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator") && data["M1410HomeRespiratoryTreatmentsVentilator"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Ventilator (continually or at night)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous") && data["M1410HomeRespiratoryTreatmentsContinuous"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Continuous / Bi-level positive airway pressure</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1410HomeRespiratoryTreatmentsNone") && data["M1410HomeRespiratoryTreatmentsNone"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; None of the above</span>
        </div>
		<h2>Cardiac Status</h2>
        <div>
            <span><strong>(M1500) Symptoms in Heart Failure Patients: If patient has been diagnosed with heart failure, did the patient exhibit symptoms indicated by clinical heart failure guidelines (including dyspnea, orthopnea, edema, or weight gain) at any point since the previous OASIS assessment?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No <em>[Go to M2004 at TRN; Go to M1600 at DC]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "00" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "00" ? "&#x2713;" : ""%></span> 2 &ndash; Not assessed <em>[Go to M2004 at TRN; Go to M1600 at DC]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "00" ? "&#x2713;" : ""%></span> NA &ndash; Patient does not have diagnosis of heart failure <em>[Go to M2004 at TRN; Go to M1600 at DC]</em></span>
        </div><div>
            <span><strong>(M1510) Heart Failure Follow-up: If patient has been diagnosed with heart failure and has exhibited symptoms indicative of heart failure since the previous OASIS assessment, what action(s) has (have) been taken to respond? <em>(Mark all that apply)</em></strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupNoAction") && data["M1510HeartFailureFollowupNoAction"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; No action taken</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupPhysicianCon") && data["M1510HeartFailureFollowupPhysicianCon"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Patient&rsquo;s physician (or other primary care practitioner) contacted the same day</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupAdvisedEmg") && data["M1510HeartFailureFollowupAdvisedEmg"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Patient advised to get emergency treatment (e.g., call 911 or go to emergency room)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupParameters") && data["M1510HeartFailureFollowupParameters"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Implemented physician-ordered patient-specific established parameters for treatment</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupInterventions") && data["M1510HeartFailureFollowupInterventions"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Patient education or other clinical interventions</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1510HeartFailureFollowupChange") && data["M1510HeartFailureFollowupChange"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; Obtained change in care plan orders (e.g., increased monitoring by agency, change in visit frequency, telehealth, etc.)</span>
        </div>
        <h2>Elimination Status</h2>
        <div>
            <span><strong>(M1600) Has this patient been treated for a Urinary Tract Infection in the past 14 days?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Patient on prophylactic treatment</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1600UrinaryTractInfection") && data["M1600UrinaryTractInfection"].Answer == "UK" ? "&#x2713;" : ""%></span> UK &ndash; Unknown <em>[Omit &ldquo;UK&rdquo; option on DC]</em></span>
        </div><div>
            <span><strong>(M1610) Urinary Incontinence or Urinary Catheter Presence:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No incontinence or catheter (includes anuria or ostomy for urinary drainage) <em>[Go to M1620]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Patient is incontinent</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1610UrinaryIncontinence") && data["M1610UrinaryIncontinence"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Patient requires a urinary catheter (i.e., external, indwelling, intermittent, suprapubic) <em>[Go to M1620]</em></span>
        </div><div>
            <span><strong>(M1615) When does Urinary Incontinence occur?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Timed-voiding defers incontinence</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Occasional stress incontinence</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; During the night only</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; During the day only</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1615UrinaryIncontinenceOccur") && data["M1615UrinaryIncontinenceOccur"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; During the day and night</span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Discharge</h1>
        <h2>Elimination Status</h2>
		<div>
            <span><strong>(M1620) Bowel Incontinence Frequency:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Very rarely or never has bowel incontinence</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Less than once weekly</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; One to three times weekly</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Four to six times weekly</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; On a daily basis</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; More often than once daily</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Patient has ostomy for bowel elimination</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1620BowelIncontinenceFrequency") && data["M1620BowelIncontinenceFrequency"].Answer == "UK" ? "&#x2713;" : ""%></span> UK &ndash; Unknown <em>[Omit &ldquo;UK&rdquo; option on FU, DC]</em></span>
        </div>
		<h2>Neuro/Behaviourial Status</h2>
        <div>
			<span><strong>(M1700) Cognitive Functioning: Patient&rsquo;s current (day of assessment) level of alertness, orientation, comprehension, concentration, and immediate memory for simple commands.</strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently.</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions.</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Requires assistance and some direction in specific situations (e.g., on all tasks involving shifting of attention), or consistently requires low stimulus environment due to distractibility.</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift attention and recall directions more than half the time.</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1700CognitiveFunctioning") && data["M1700CognitiveFunctioning"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Totally dependent due to disturbances such as constant disorientation, coma, persistent vegetative state, or delirium.</span>
		</div><div>
			<span><strong>(M1710) When Confused (Reported or Observed Within the Last 14 Days):</strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Never</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; In new or complex situations only</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; On awakening or at night only</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; During the day and evening, but not constantly</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Constantly</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1710WhenConfused") && data["M1710WhenConfused"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Patient nonresponsive</span>
		</div><div>
			<span><strong>(M1720) When Anxious (Reported or Observed Within the Last 14 Days):</strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; None of the time</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Less often than daily</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Daily, but not constantly</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; All of the time</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1720WhenAnxious") && data["M1720WhenAnxious"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Patient nonresponsive</span>
		</div><div>
			<span><strong>(M1740) Cognitive, behavioral, and psychiatric symptoms that are demonstrated at least once a week (Reported or Observed): <em>(Mark all that apply.)</em></strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit") && data["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Memory deficit: failure to recognize familiar persons/places, inability to recall events of past 24 hours, significant memory loss so that supervision is required</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes") && data["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Impaired decision-making: failure to perform usual ADLs or IADLs, inability to appropriately stop activities, jeopardizes safety through actions</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal") && data["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Verbal disruption: yelling, threatening, excessive profanity, sexual references, etc.</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical") && data["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Physical aggression: aggressive or combative to self and others (e.g., hits self, throws objects, punches, dangerous maneuvers with wheelchair or other objects)</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB") && data["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Disruptive, infantile, or socially inappropriate behavior (excludes verbal actions)</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional") && data["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; Delusional, hallucinatory, or paranoid behavior</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsNone") && data["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer == "1" ? "&#x2713;" : ""%></span> 7 &ndash; None of the above behaviors demonstrated</span>
		</div><div>
			<span><strong>(M1745) Frequency of Disruptive Behavior Symptoms (Reported or Observed) Any physical, verbal, or other disruptive/dangerous symptoms that are injurious to self or others or jeopardize personal safety.</strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Never</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Less than once a month</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Once a month</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Several times each month</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Several times a week</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && data["M1745DisruptiveBehaviorSymptomsFrequency"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; At least daily</span>
		</div><div>
			<span><strong>(M1750) Is this patient receiving Psychiatric Nursing Services at home provided by a qualified psychiatric nurse?</strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "0" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1750PsychiatricNursingServicing") && data["M1750PsychiatricNursingServicing"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
		</div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-c Discharge</h1>
	    <h2>ADL/IADLs</h2>
        <div>
		    <span><strong>(M1800) Grooming: Current ability to tend safely to personal hygiene needs <em>(i.e., washing face and hands, hair care, shaving or make up, teeth or denture care, fingernail care)</em>.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to groom self unaided, with or without the use of assistive devices or adapted methods.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Grooming utensils must be placed within reach before able to complete grooming activities.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Someone must assist the patient to groom self.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Patient depends entirely upon someone else for grooming needs.</span>
        </div><div>
            <span><strong>(M1810) Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts and blouses, managing zippers, buttons, and snaps:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to dress upper body without assistance if clothing is laid out or handed to the patient.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Someone must help the patient put on upper body clothing.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Patient depends entirely upon another person to dress the upper body.</span>
        </div><div>
            <span><strong>(M1820) Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes:</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to obtain, put on, and remove clothing and shoes without assistance.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Patient depends entirely upon another person to dress lower body.</span>
        </div><div>
            <span><strong>(M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to bathe self in shower or tub independently, including getting in and out of tub/shower.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Able to bathe in shower or tub with the intermittent assistance of another person:
                <ul>
                    <li>(a) for intermittent supervision or encouragement or reminders, OR</li>
                    <li>(b) to get in and out of the shower or tub, OR</li>
                    <li>(c) for washing difficult to reach areas.</li>
                </ul></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the assistance or supervision of another person throughout the bath.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "06" ? "&#x2713;" : ""%></span> 6 &ndash; Unable to participate effectively in bathing and is bathed totally by another person.</span>
        </div><div>
            <span><strong>(M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to get to and from the toilet and transfer independently with or without a device.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Is totally dependent in toileting.</span>
        </div><div>
            <span><strong>(M1845) Toileting Hygiene: Current ability to maintain perineal hygiene safely, adjust clothes and/or incontinence pads before and after using toilet, commode, bedpan, urinal. If managing ostomy, includes cleaning area around stoma, but not managing equipment.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to manage toileting hygiene and clothing management without assistance.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to manage toileting hygiene and clothing management without assistance if supplies/implements are laid out for the patient.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Someone must help the patient to maintain toileting hygiene and/or adjust clothing.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Patient depends entirely upon another person to maintain toileting hygiene.</span>
        </div><div>
            <span><strong>(M1850) Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to independently transfer.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to transfer with minimal human assistance or with use of an assistive device.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Able to bear weight and pivot during the transfer process but unable to transfer self.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Unable to transfer self and is unable to bear weight or pivot when transferred by another person.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Bedfast, unable to transfer but is able to turn and position self in bed.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Bedfast, unable to transfer and is unable to turn and position self.</span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Discharge</h1>
        <h2>ADL/IADLs</h2>
        <div>
            <span><strong>(M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (i.e., needs no human assistance or assistive device).</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Requires use of a two-handed device (e.g., walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Able to walk only with the supervision or assistance of another person at all times.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Chairfast, unable to ambulate but is able to wheel self independently.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Chairfast, unable to ambulate and is unable to wheel self.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "06" ? "&#x2713;" : ""%></span> 6 &ndash; Bedfast, unable to ambulate or be up in a chair.</span>
        </div><div>
            <span><strong>(M1870) Feeding or Eating: Current ability to feed self meals and snacks safely. Note: This refers only to the process of eating, chewing, and swallowing, not preparing the food to be eaten.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to independently feed self.</span>
		    <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to feed self independently but requires:
                <ul>
                    <li>(a) meal set-up; OR</li>
                    <li>(b) intermittent assistance or supervision from another person; OR</li>
                    <li>(c) a liquid, pureed or ground meat diet.</li>
                </ul></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Unable to feed self and must be assisted or supervised throughout the meal/snack.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Able to take in nutrients orally and receives supplemental nutrients through a nasogastric tube or gastrostomy.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Unable to take in nutrients orally and is fed nutrients through a nasogastric tube or gastrostomy.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Unable to take in nutrients orally or by tube feeding.</span>
        </div><div>
            <span><strong>(M1880) Current Ability to Plan and Prepare Light Meals (e.g., cereal, sandwich) or reheat delivered meals safely:</strong></span>
		    <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; 
                <ul>
                    <li>(a) Able to independently plan and prepare all light meals for self or reheat delivered meals; OR</li>
                    <li>(b) Is physically, cognitively, and mentally able to prepare light meals on a regular basis but has not routinely performed light meal preparation in the past (i.e., prior to this home care admission).</li>
                </ul></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash;  Unable to prepare light meals on a regular basis due to physical, cognitive, or mental limitations.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Unable to prepare any light meals or reheat any delivered meals.</span>
        </div><div>
            <span><strong>(M1890) Ability to Use Telephone: Current ability to answer the phone safely, including dialing numbers, and effectively using the telephone to communicate.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to dial numbers and answer calls appropriately and as desired.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to use a specially adapted telephone (i.e., large numbers on the dial, teletype phone for the deaf) and call essential numbers.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Able to answer the telephone and carry on a normal conversation but has difficulty with placing calls.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Able to answer the telephone only some of the time or is able to carry on only a limited conversation.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Unable to answer the telephone at all but can listen if assisted with equipment.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; Totally unable to use the telephone.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Patient does not have a telephone.</span>
        </div>
        <h2>Medications</h2>
        <div>
            <span><strong>(M2004) Medication Intervention: If there were any clinically significant medication issues since the previous OASIS assessment, was a physician or the physician-designee contacted within one calendar day of the assessment to resolve clinically significant medication issues, including reconciliation?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; No clinically significant medication issues identified since the previous OASIS assessment</span>
        </div><div>
            <span><strong>(M2015) Patient/Caregiver Drug Education Intervention: Since the previous OASIS assessment, was the patient/caregiver instructed by agency staff or other health care provider to monitor the effectiveness of drug therapy, drug reactions, and side effects, and how and when to report problems that may occur?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Patient not taking any drugs</span>
        </div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Discharge</h1>
        <h2>Medications</h2>
		<div>
            <span><strong>(M2020) Management of Oral Medications: Patient's current ability to prepare and take all oral medications reliably and safely, including administration of the correct dosage at the appropriate times/intervals. Excludes injectable and IV medications. (NOTE: This refers to ability, not compliance or willingness.)</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to independently take the correct oral medication(s) and proper dosage(s) at the correct times.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to take medication(s) at the correct times if:
                <ul>
                    <li><span class="float_left">(a) individual dosages are prepared in advance by another person; OR</span></li>
                    <li><span class="float_left">(b) another person develops a drug diary or chart.</span></li>
                </ul></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Able to take medication(s) at the correct times if given reminders by another person at the appropriate times</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Unable to take medication unless administered by another person.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; No oral medications prescribed.</span>
        </div><div>
            <span><strong>(M2030) Management of Injectable Medications: Patient&rsquo;s current ability to prepare and take all prescribed injectable medications reliably and safely, including administration of correct dosage at the appropriate times/intervals. Excludes IV medications.</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; Able to independently take the correct medication(s) and proper dosage(s) at the correct times.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Able to take injectable medication(s) at the correct times if:
                <ul>
                    <li><span class="float_left">(a) individual syringes are prepared in advance by another person; OR</span></li>
                    <li><span class="float_left">(b) another person develops a drug diary or chart.</span></li>
                </ul></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Unable to take injectable medication unless administered by another person.</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; No injectable medications prescribed.</span>
        </div>
        <h2>Care Management</h2>
		<div><table>
			<tbody>
				<tr>
				    <td colspan="7"><strong>(M2100) Types and Sources of Assistance: Determine the level of caregiver ability and willingness to provide assistance for the following activities, if assistance is needed. <em>(Check only one box in each row.)</em></strong></td>
				</tr><tr>
					<th>Type of Assistance</th>
					<th>No assistance needed in this area</th>
					<th>Caregiver(s) currently provide assistance</th>
					<th>Caregiver(s) need training/ supportive services to provide assistance</th>
					<th>Caregiver(s) not likely to provide assistance</th>
					<th>Unclear if Caregiver(s) will provide assistance</th>
					<th>Assistance needed, but no Caregiver(s) available</th>
				</tr><tr>
					<td>a. ADL assistance (e.g., transfer/ ambulation, bathing, dressing, toileting, eating/feeding)</td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "00" ? "&#x2713;" : ""%></span> 0</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "01" ? "&#x2713;" : ""%></span> 1</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "02" ? "&#x2713;" : ""%></span> 2</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "03" ? "&#x2713;" : ""%></span> 3</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "04" ? "&#x2713;" : ""%></span> 4</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ADLAssistance") && data["M2100ADLAssistance"].Answer == "05" ? "&#x2713;" : ""%></span> 5</span></td>
				</tr><tr>
					<td>b. IADL assistance (e.g., meals, housekeeping, laundry, telephone, shopping, finances)</td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "00" ? "&#x2713;" : ""%></span> 0</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "01" ? "&#x2713;" : ""%></span> 1</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "02" ? "&#x2713;" : ""%></span> 2</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "03" ? "&#x2713;" : ""%></span> 3</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "04" ? "&#x2713;" : ""%></span> 4</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100IADLAssistance") && data["M2100IADLAssistance"].Answer == "05" ? "&#x2713;" : ""%></span> 5</span></td>
				</tr><tr>
					<td>c. Medication administration (e.g., oral, inhaled or injectable)</td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "00" ? "&#x2713;" : ""%></span> 0</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "01" ? "&#x2713;" : ""%></span> 1</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "02" ? "&#x2713;" : ""%></span> 2</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "03" ? "&#x2713;" : ""%></span> 3</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "04" ? "&#x2713;" : ""%></span> 4</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicationAdministration") && data["M2100MedicationAdministration"].Answer == "05" ? "&#x2713;" : ""%></span> 5</span></td>
				</tr><tr>
					<td>d. Medical procedures/ treatments (e.g., changing wound dressing)</td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "00" ? "&#x2713;" : ""%></span> 0</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "01" ? "&#x2713;" : ""%></span> 1</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "02" ? "&#x2713;" : ""%></span> 2</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "03" ? "&#x2713;" : ""%></span> 3</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "04" ? "&#x2713;" : ""%></span> 4</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100MedicalProcedures") && data["M2100MedicalProcedures"].Answer == "05" ? "&#x2713;" : ""%></span> 5</span></td>
				</tr><tr>
					<td>e. Management of Equipment (includes oxygen, IV/infusion equipment, enteral/ parenteral nutrition, ventilator therapy equipment or supplies)</td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "00" ? "&#x2713;" : ""%></span> 0</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "01" ? "&#x2713;" : ""%></span> 1</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "02" ? "&#x2713;" : ""%></span> 2</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "03" ? "&#x2713;" : ""%></span> 3</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "04" ? "&#x2713;" : ""%></span> 4</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100ManagementOfEquipment") && data["M2100ManagementOfEquipment"].Answer == "05" ? "&#x2713;" : ""%></span> 5</span></td>
				</tr><tr>
					<td>f. Supervision and safety (e.g., due to cognitive impairment)</td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "00" ? "&#x2713;" : ""%></span> 0</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "01" ? "&#x2713;" : ""%></span> 1</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "02" ? "&#x2713;" : ""%></span> 2</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "03" ? "&#x2713;" : ""%></span> 3</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "04" ? "&#x2713;" : ""%></span> 4</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100SupervisionAndSafety") && data["M2100SupervisionAndSafety"].Answer == "05" ? "&#x2713;" : ""%></span> 5</span></td>
				</tr><tr>
					<td>g. Advocacy or facilitation of patient&rsquo;s participation in appropriate medical care (includes transporta-tion to or from appointments)</td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "00" ? "&#x2713;" : ""%></span> 0</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "01" ? "&#x2713;" : ""%></span> 1</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "02" ? "&#x2713;" : ""%></span> 2</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "03" ? "&#x2713;" : ""%></span> 3</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "04" ? "&#x2713;" : ""%></span> 4</span></td>
					<td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2100FacilitationPatientParticipation") && data["M2100FacilitationPatientParticipation"].Answer == "05" ? "&#x2713;" : ""%></span> 5</span></td>
				</tr>
			</tbody>
		</table></div>
		<div>
			<span><strong>(M2110) How Often does the patient receive ADL or IADL assistance from any caregiver(s) (other than home health agency staff)?</strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; At least daily</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Three or more times per week</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; One to two times per week</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Received, but less often than weekly</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "05" ? "&#x2713;" : ""%></span> 5 &ndash; No assistance received</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && data["M2110FrequencyOfADLOrIADLAssistance"].Answer == "UK" ? "&#x2713;" : ""%></span> UK &ndash; Unknown <em>[Omit &ldquo;UK&rdquo; option on DC]</em></span>            
		</div>
		<h2>Emergent Care</h2>
		<div>
			<span><strong>(M2300) Emergent Care: Since the last time OASIS data were collected, has the patient utilized a hospital emergency department (includes holding/observation)?</strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "00" ? "&#x2713;" : ""%></span> 0 &ndash; No <em>[Go to M2400]</em></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Yes, used hospital emergency department WITHOUT hospital admission</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Yes, used hospital emergency department WITH hospital admission</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2300EmergentCare") && data["M2300EmergentCare"].Answer == "UK" ? "&#x2713;" : ""%></span> UK &ndash; Unknown <em>[Go to M2400]</em></span>            
		</div>
    </div><div class="page"><img src="/Images/axxess_print.png" class="axxess" /><img src="/Images/acore_print.png" class="acore" />
        <h1>OASIS-C Discharge</h1>
		<h2>Emergent Care</h2>
		<div>
			<span><strong>(M2310) Reason for Emergent Care: For what reason(s) did the patient receive emergent care (with or without hospitalization)? <em>(Mark all that apply)</em></strong></span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareMed") && data["M2310ReasonForEmergentCareMed"].Answer == "1" ? "&#x2713;" : ""%></span> 1 &ndash; Improper medication administration, medication side effects, toxicity, anaphylaxis</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareFall") && data["M2310ReasonForEmergentCareFall"].Answer == "1" ? "&#x2713;" : ""%></span> 2 &ndash; Injury caused by fall</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareResInf") && data["M2310ReasonForEmergentCareResInf"].Answer == "1" ? "&#x2713;" : ""%></span> 3 &ndash; Respiratory infection (e.g., pneumonia, bronchitis)</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareOtherResInf") && data["M2310ReasonForEmergentCareOtherResInf"].Answer == "1" ? "&#x2713;" : ""%></span> 4 &ndash; Other respiratory problem</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareHeartFail") && data["M2310ReasonForEmergentCareHeartFail"].Answer == "1" ? "&#x2713;" : ""%></span> 5 &ndash; Heart failure (e.g., fluid overload)</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareCardiac") && data["M2310ReasonForEmergentCareCardiac"].Answer == "1" ? "&#x2713;" : ""%></span> 6 &ndash; Cardiac dysrhythmia (irregular heartbeat)</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareMyocardial") && data["M2310ReasonForEmergentCareMyocardial"].Answer == "1" ? "&#x2713;" : ""%></span> 7 &ndash; Myocardial infarction or chest pain</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareHeartDisease") && data["M2310ReasonForEmergentCareHeartDisease"].Answer == "1" ? "&#x2713;" : ""%></span> 8 &ndash; Other heart disease</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareStroke") && data["M2310ReasonForEmergentCareStroke"].Answer == "1" ? "&#x2713;" : ""%></span> 9 &ndash; Stroke (CVA) or TIA</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareHypo") && data["M2310ReasonForEmergentCareHypo"].Answer == "1" ? "&#x2713;" : ""%></span> 10 &ndash; Hypo/Hyperglycemia, diabetes out of control</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareGI") && data["M2310ReasonForEmergentCareGI"].Answer == "1" ? "&#x2713;" : ""%></span> 11 &ndash; GI bleeding, obstruction, constipation, impaction</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareDehMal") && data["M2310ReasonForEmergentCareDehMal"].Answer == "1" ? "&#x2713;" : ""%></span> 12 &ndash; Dehydration, malnutrition</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareUrinaryInf") && data["M2310ReasonForEmergentCareUrinaryInf"].Answer == "1" ? "&#x2713;" : ""%></span> 13 &ndash; Urinary tract infection</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareIV") && data["M2310ReasonForEmergentCareIV"].Answer == "1" ? "&#x2713;" : ""%></span> 14 &ndash; IV catheter-related infection or complication</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareWoundInf") && data["M2310ReasonForEmergentCareWoundInf"].Answer == "1" ? "&#x2713;" : ""%></span> 15 &ndash; Wound infection or deterioration</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareUncontrolledPain") && data["M2310ReasonForEmergentCareUncontrolledPain"].Answer == "1" ? "&#x2713;" : ""%></span> 16 &ndash; Uncontrolled pain</span>            
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareMental") && data["M2310ReasonForEmergentCareMental"].Answer == "1" ? "&#x2713;" : ""%></span> 17 &ndash; Acute mental/behavioral health problem</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareDVT") && data["M2310ReasonForEmergentCareDVT"].Answer == "1" ? "&#x2713;" : ""%></span> 18 &ndash; Deep vein thrombosis, pulmonary embolus</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareOther") && data["M2310ReasonForEmergentCareOther"].Answer == "1" ? "&#x2713;" : ""%></span> 19 &ndash; Other than above reasons</span>
			<span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2310ReasonForEmergentCareUK") && data["M2310ReasonForEmergentCareUK"].Answer == "1" ? "&#x2713;" : ""%></span> UK &ndash; Reason unknown</span>            
		</div>
        <h2>Discharge</h2>
        <div><table>
            <tbody>
                <tr><td colspan="4"><strong>(M2400) Intervention Synopsis: (Check only one box in each row.) Since the previous OASIS assessment, were the following interventions BOTH included in the physician-ordered plan of care AND implemented?</strong></td></tr>
                <tr><th>Plan/Intervention</th><th>No</th><th>Yes</th><th>NA &ndash; Not Applicable</th></tr>
                <tr>
                    <td>a. Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Patient is not diabetic or is bilateral amputee</span></td>
                </tr><tr>
                    <td>b. Falls prevention interventions</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for falls since the last OASIS assessment</span></td>
                </tr><tr>
                    <td>c. Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Formal assessment indicates patient did not meet criteria for depression AND patient did not have diagnosis of depression since the last OASIS assessment</span></td>
                </tr><tr>
                    <td>d. Intervention(s) to monitor and mitigate pain</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Formal assessment did not indicate pain since the last OASIS assessment</span></td>
                </tr><tr>
                    <td>e. Intervention(s) to prevent pressure ulcers</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Formal assessment indicates the patient was not at risk of pressure ulcers since the last OASIS assessment</span></td>
                </tr><tr>
                    <td>f. Pressure ulcer treatment based on principles of moist wound healing</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "00" ? "&#x2713;" : ""%></span> 0</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "01" ? "&#x2713;" : ""%></span> 1</td>
                    <td><span class="checklabel auto"><span class="checkbox"><%= data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; Dressings that support the principles of moist wound healing not indicated for this patient’s pressure ulcers OR patient has no pressure ulcers with need for moist wound healing</span></td>
                </tr>
            </tbody>
        </table></div>
        <div>
            <span><strong>(M2410) To which Inpatient Facility has the patient been admitted?</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Hospital <em>[Go to M2430]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Rehabilitation facility <em>[Go to M0903]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Nursing home <em>[Go to M2440]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Hospice <em>[Go to M0903]</em></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "NA" ? "&#x2713;" : ""%></span> NA &ndash; No inpatient facility admission <em>[Omit &ldquo;NA&rdquo; option on TRN]</em></span>
        </div><div>
            <span><strong>(M2420) Discharge Disposition: Where is the patient after discharge from your agency? (Choose only one answer.)</strong></span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "01" ? "&#x2713;" : ""%></span> 1 &ndash; Patient remained in the community (without formal assistive services)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "02" ? "&#x2713;" : ""%></span> 2 &ndash; Patient remained in the community (with formal assistive services)</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "03" ? "&#x2713;" : ""%></span> 3 &ndash; Patient transferred to a non-institutional hospice</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "04" ? "&#x2713;" : ""%></span> 4 &ndash; Unknown because patient moved to a geographic location not served by this agency</span>
            <span class="checklabel"><span class="checkbox"><%= data.ContainsKey("M2420DischargeDisposition") && data["M2420DischargeDisposition"].Answer == "UK" ? "&#x2713;" : ""%></span> UK &ndash; Other unknown</span>
        </div><div>
            <span><strong>(M0903) Date of Last (Most Recent) Home Visit:</strong></span>
            <span><%= data.ContainsKey("M0903LastHomeVisitDate") ? data["M0903LastHomeVisitDate"].Answer : "&nbsp;"%></span>
        </div><div>
            <span><strong>(M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer, or death (at home) of the patient.</strong></span>
            <span><%= data.ContainsKey("M0906DischargeDate") ? data["M0906DischargeDate"].Answer : ""%></span>
        </div>
    </div>
</body>
</html>

