﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyAdlForm" })) { %>
<% var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("DischargeFromAgency_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<%= Html.Hidden("categoryType", "Adl")%> 
<div class="wrapper main">
    <fieldset class="oasis">
        <legend>OASIS</legend>
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1800');">(M1800)</a> Grooming: Current ability to tend safely to personal hygiene needs (i.e., washing face and hands, hair care, shaving or make up, teeth or denture care, fingernail care).</label>
                <%=Html.Hidden("DischargeFromAgency_M1800Grooming")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1800Grooming", "00", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1800Grooming0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1800Grooming0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to groom self unaided, with or without the use of assistive devices or adapted methods.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1800Grooming", "01", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1800Grooming1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1800Grooming1"><span class="float_left">1 &ndash;</span><span class="normal margin">Grooming utensils must be placed within reach before able to complete grooming activities.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1800Grooming", "02", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1800Grooming2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1800Grooming2"><span class="float_left">2 &ndash;</span><span class="normal margin">Someone must assist the patient to groom self.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1800');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1800Grooming", "03", data.ContainsKey("M1800Grooming") && data["M1800Grooming"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1800Grooming3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1800Grooming3"><span class="float_left">3 &ndash;</span><span class="normal margin">Patient depends entirely upon someone else for grooming needs.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1810');">(M1810)</a> Current Ability to Dress Upper Body safely (with or without dressing aids) including undergarments, pullovers, front-opening shirts and blouses, managing zippers, buttons, and snaps:</label>
                <%=Html.Hidden("DischargeFromAgency_M1810CurrentAbilityToDressUpper")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1810CurrentAbilityToDressUpper", "00", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1810CurrentAbilityToDressUpper0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1810CurrentAbilityToDressUpper0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1810CurrentAbilityToDressUpper", "01", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1810CurrentAbilityToDressUpper1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1810CurrentAbilityToDressUpper1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to dress upper body without assistance if clothing is laid out or handed to the patient.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1810CurrentAbilityToDressUpper", "02", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1810CurrentAbilityToDressUpper2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1810CurrentAbilityToDressUpper2"><span class="float_left">2 &ndash;</span><span class="normal margin">Someone must help the patient put on upper body clothing.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1810');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1810CurrentAbilityToDressUpper", "03", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1810CurrentAbilityToDressUpper3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1810CurrentAbilityToDressUpper3"><span class="float_left">3 &ndash;</span><span class="normal margin">Patient depends entirely upon another person to dress the upper body.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1820');">(M1820)</a> Current Ability to Dress Lower Body safely (with or without dressing aids) including undergarments, slacks, socks or nylons, shoes:</label>
                <%=Html.Hidden("DischargeFromAgency_M1820CurrentAbilityToDressLower")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1820CurrentAbilityToDressLower", "00", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1820CurrentAbilityToDressLower0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1820CurrentAbilityToDressLower0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to obtain, put on, and remove clothing and shoes without assistance.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1820CurrentAbilityToDressLower", "01", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1820CurrentAbilityToDressLower1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1820CurrentAbilityToDressLower1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1820CurrentAbilityToDressLower", "02", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1820CurrentAbilityToDressLower2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1820CurrentAbilityToDressLower2"><span class="float_left">2 &ndash;</span><span class="normal margin">Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1820CurrentAbilityToDressLower", "03", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1820CurrentAbilityToDressLower3", @class = "radio float_left" })%>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1820');">?</div>
                    </div>
                    <label for="DischargeFromAgency_M1820CurrentAbilityToDressLower3"><span class="float_left">3 &ndash;</span><span class="normal margin">Patient depends entirely upon another person to dress lower body.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1830');">(M1830)</a> Bathing: Current ability to wash entire body safely. Excludes grooming (washing face, washing hands, and shampooing hair).</label>
                <%=Html.Hidden("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "00", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to bathe self in shower or tub independently, including getting in and out of tub/shower.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "01", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody1"><span class="float_left">1 &ndash;</span><span class="normal margin">With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "02", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody2">
                        <span class="float_left">2 &ndash;</span>
                        <span class="normal margin">
                            Able to bathe in shower or tub with the intermittent assistance of another person:
                            <ul>
                                <li><span class="float_left">(a)</span><span class="radio">for intermittent supervision or encouragement or reminders, OR</span></li>
                                <li><span class="float_left">(b)</span><span class="radio">to get in and out of the shower or tub, OR</span></li>
                                <li><span class="float_left">(c)</span><span class="radio">for washing difficult to reach areas.</span></li>
                            </ul>
                        </span>
                    </label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "03", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody3"><span class="float_left">3 &ndash;</span><span class="normal margin">Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "04", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody4"><span class="float_left">4 &ndash;</span><span class="normal margin">Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "05", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "05" ? true : false, new { @id = "DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody5", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody5"><span class="float_left">5 &ndash;</span><span class="normal margin">Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the assistance or supervision of another person throughout the bath.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1830');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody", "06", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "06" ? true : false, new { @id = "DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody6", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody6"><span class="float_left">6 &ndash;</span><span class="normal margin">Unable to participate effectively in bathing and is bathed totally by another person.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1840');">(M1840)</a> Toilet Transferring: Current ability to get to and from the toilet or bedside commode safely and transfer on and off toilet/commode.</label>
                <%=Html.Hidden("DischargeFromAgency_M1840ToiletTransferring")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1840ToiletTransferring", "00", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1840ToiletTransferring0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1840ToiletTransferring0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to get to and from the toilet and transfer independently with or without a device.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1840ToiletTransferring", "01", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1840ToiletTransferring1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1840ToiletTransferring1"><span class="float_left">1 &ndash;</span><span class="normal margin">When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1840ToiletTransferring", "02", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1840ToiletTransferring2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1840ToiletTransferring2"><span class="float_left">2 &ndash;</span><span class="normal margin">Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance).</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1840ToiletTransferring", "03", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1840ToiletTransferring3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1840ToiletTransferring3"><span class="float_left">3 &ndash;</span><span class="normal margin">Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1840');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1840ToiletTransferring", "04", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1840ToiletTransferring4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1840ToiletTransferring4"><span class="float_left">4 &ndash;</span><span class="normal margin">Is totally dependent in toileting.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1845');">(M1845)</a> Toileting Hygiene: Current ability to maintain perineal hygiene safely, adjust clothes and/or incontinence pads before and after using toilet, commode, bedpan, urinal. If managing ostomy, includes cleaning area around stoma, but not managing equipment.</label>
                <%=Html.Hidden("DischargeFromAgency_M1845ToiletingHygiene")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1845ToiletingHygiene", "00", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1845ToiletingHygiene0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1845ToiletingHygiene0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to manage toileting hygiene and clothing management without assistance.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1845ToiletingHygiene", "01", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1845ToiletingHygiene1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1845ToiletingHygiene1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to manage toileting hygiene and clothing management without assistance if supplies/implements are laid out for the patient.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1845ToiletingHygiene", "02", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1845ToiletingHygiene2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1845ToiletingHygiene2"><span class="float_left">2 &ndash;</span><span class="normal margin">Someone must help the patient to maintain toileting hygiene and/or adjust clothing.</span></label>
                    <div class="clear"></div>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1845');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1845ToiletingHygiene", "03", data.ContainsKey("M1845ToiletingHygiene") && data["M1845ToiletingHygiene"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1845ToiletingHygiene3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1845ToiletingHygiene3"><span class="float_left">3 &ndash;</span><span class="normal margin">Patient depends entirely upon another person to maintain toileting hygiene.</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1850');">(M1850)</a> Transferring: Current ability to move safely from bed to chair, or ability to turn and position self in bed if patient is bedfast.</label>
                <%=Html.Hidden("DischargeFromAgency_M1850Transferring")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "00", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1850Transferring0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1850Transferring0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently transfer.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "01", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1850Transferring1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1850Transferring1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to transfer with minimal human assistance or with use of an assistive device.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "02", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1850Transferring2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1850Transferring2"><span class="float_left">2 &ndash;</span><span class="normal margin">Able to bear weight and pivot during the transfer process but unable to transfer self.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "03", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1850Transferring3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1850Transferring3"><span class="float_left">3 &ndash;</span><span class="normal margin">Unable to transfer self and is unable to bear weight or pivot when transferred by another person.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "04", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1850Transferring4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1850Transferring4"><span class="float_left">4 &ndash;</span><span class="normal margin">Bedfast, unable to transfer but is able to turn and position self in bed.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1850');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1850Transferring", "05", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "05" ? true : false, new { @id = "DischargeFromAgency_M1850Transferring5", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1850Transferring5"><span class="float_left">5 &ndash;</span><span class="normal margin">Bedfast, unable to transfer and is unable to turn and position self.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1860');">(M1860)</a> Ambulation/Locomotion: Current ability to walk safely, once in a standing position, or use a wheelchair, once in a seated position, on a variety of surfaces.</label>
                <%=Html.Hidden("DischargeFromAgency_M1860AmbulationLocomotion")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "00", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1860AmbulationLocomotion0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1860AmbulationLocomotion0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (i.e., needs no human assistance or assistive device).</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "01", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1860AmbulationLocomotion1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1860AmbulationLocomotion1"><span class="float_left">1 &ndash;</span><span class="normal margin">With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "02", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1860AmbulationLocomotion2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1860AmbulationLocomotion2"><span class="float_left">2 &ndash;</span><span class="normal margin">Requires use of a two-handed device (e.g., walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "03", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1860AmbulationLocomotion3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1860AmbulationLocomotion3"><span class="float_left">3 &ndash;</span><span class="normal margin">Able to walk only with the supervision or assistance of another person at all times.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "04", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1860AmbulationLocomotion4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1860AmbulationLocomotion4"><span class="float_left">4 &ndash;</span><span class="normal margin">Chairfast, unable to ambulate but is able to wheel self independently.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "05", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "05" ? true : false, new { @id = "DischargeFromAgency_M1860AmbulationLocomotion5", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1860AmbulationLocomotion5"><span class="float_left">5 &ndash;</span><span class="normal margin">Chairfast, unable to ambulate and is unable to wheel self.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1860');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1860AmbulationLocomotion", "06", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "06" ? true : false, new { @id = "DischargeFromAgency_M1860AmbulationLocomotion6", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1860AmbulationLocomotion6"><span class="float_left">6 &ndash;</span><span class="normal margin">Bedfast, unable to ambulate or be up in a chair.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1870');">(M1870)</a> Feeding or Eating: Current ability to feed self meals and snacks safely. Note: This refers only to the process of eating, chewing, and swallowing, not preparing the food to be eaten.</label>
                <%=Html.Hidden("DischargeFromAgency_M1870FeedingOrEating")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "00", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1870FeedingOrEating0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1870FeedingOrEating0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently feed self.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "01", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1870FeedingOrEating1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1870FeedingOrEating1">
                        <span class="float_left">1 &ndash;</span>
                        <span class="normal margin">
                            Able to feed self independently but requires:
                            <ul>
                                <li><span class="float_left">(a)</span><span class="radio">meal set-up; OR</span></li>
                                <li><span class="float_left">(b)</span><span class="radio">intermittent assistance or supervision from another person; OR</span></li>
                                <li><span class="float_left">(c)</span><span class="radio">a liquid, pureed or ground meat diet.</span></li>
                            </ul>
                        </span>
                    </label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "02", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1870FeedingOrEating2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1870FeedingOrEating2"><span class="float_left">2 &ndash;</span><span class="normal margin">Unable to feed self and must be assisted or supervised throughout the meal/snack.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "03", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1870FeedingOrEating3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1870FeedingOrEating3"><span class="float_left">3 &ndash;</span><span class="normal margin">Able to take in nutrients orally and receives supplemental nutrients through a nasogastric tube or gastrostomy.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "04", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1870FeedingOrEating4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1870FeedingOrEating4"><span class="float_left">4 &ndash;</span><span class="normal margin">Unable to take in nutrients orally and is fed nutrients through a nasogastric tube or gastrostomy.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1870');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1870FeedingOrEating", "05", data.ContainsKey("M1870FeedingOrEating") && data["M1870FeedingOrEating"].Answer == "05" ? true : false, new { @id = "DischargeFromAgency_M1870FeedingOrEating5", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1870FeedingOrEating5"><span class="float_left">5 &ndash;</span><span class="normal margin">Unable to take in nutrients orally or by tube feeding.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1880');">(M1880)</a> Current Ability to Plan and Prepare Light Meals (e.g., cereal, sandwich) or reheat delivered meals safely:</label>
                <%=Html.Hidden("DischargeFromAgency_M1880AbilityToPrepareLightMeal")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1880AbilityToPrepareLightMeal", "00", data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1880AbilityToPrepareLightMeal0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1880AbilityToPrepareLightMeal0">
                        <span class="float_left">0 &ndash;</span>
                        <span class="normal margin">
                            <ul>
                                <li><span class="float_left">(a)</span><span class="radio">Able to independently plan and prepare all light meals for self or reheat delivered meals; OR</span></li>
                                <li><span class="float_left">(b)</span><span class="radio">Is physically, cognitively, and mentally able to prepare light meals on a regular basis but has not routinely performed light meal preparation in the past (i.e., prior to this home care admission).</span></li>
                            </ul>
                        </span>
                    </label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1880AbilityToPrepareLightMeal", "01", data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1880AbilityToPrepareLightMeal1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1880AbilityToPrepareLightMeal1"><span class="float_left">1 &ndash;</span><span class="normal margin"> Unable to prepare light meals on a regular basis due to physical, cognitive, or mental limitations.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1880');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1880AbilityToPrepareLightMeal", "02", data.ContainsKey("M1880AbilityToPrepareLightMeal") && data["M1880AbilityToPrepareLightMeal"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1880AbilityToPrepareLightMeal2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1880AbilityToPrepareLightMeal2"><span class="float_left">2 &ndash;</span><span class="normal margin">Unable to prepare any light meals or reheat any delivered meals.</span></label>
                </div>
            </div><div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M1890');">(M1890)</a> Ability to Use Telephone: Current ability to answer the phone safely, including dialing numbers, and effectively using the telephone to communicate.</label>
                <%=Html.Hidden("DischargeFromAgency_M1890AbilityToUseTelephone")%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "00", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M1890AbilityToUseTelephone0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1890AbilityToUseTelephone0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to dial numbers and answer calls appropriately and as desired.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "01", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M1890AbilityToUseTelephone1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1890AbilityToUseTelephone1"><span class="float_left">1 &ndash;</span><span class="normal margin">Able to use a specially adapted telephone (i.e., large numbers on the dial, teletype phone for the deaf) and call essential numbers.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "02", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M1890AbilityToUseTelephone2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1890AbilityToUseTelephone2"><span class="float_left">2 &ndash;</span><span class="normal margin">Able to answer the telephone and carry on a normal conversation but has difficulty with placing calls.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "03", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M1890AbilityToUseTelephone3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1890AbilityToUseTelephone3"><span class="float_left">3 &ndash;</span><span class="normal margin">Able to answer the telephone only some of the time or is able to carry on only a limited conversation.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "04", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "04" ? true : false, new { @id = "DischargeFromAgency_M1890AbilityToUseTelephone4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1890AbilityToUseTelephone4"><span class="float_left">4 &ndash;</span><span class="normal margin">Unable to answer the telephone at all but can listen if assisted with equipment.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "05", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "05" ? true : false, new { @id = "DischargeFromAgency_M1890AbilityToUseTelephone5", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1890AbilityToUseTelephone5"><span class="float_left">5 &ndash;</span><span class="normal margin">Totally unable to use the telephone.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1890');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M1890AbilityToUseTelephone", "NA", data.ContainsKey("M1890AbilityToUseTelephone") && data["M1890AbilityToUseTelephone"].Answer == "NA" ? true : false, new { @id = "DischargeFromAgency_M1890AbilityToUseTelephone6", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M1890AbilityToUseTelephoneNA"><span class="float_left">NA &ndash; </span><span class="normal margin">Patient does not have a telephone.</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Continue</a></li>
            <li><a href="javascript:void(0);" onclick="SOC.FormSubmit($(this));">Save &amp; Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowOasisValidationModal('DischargeFromAgency_ValidationContainer','{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "DischargeFromAgency")%></li>
        </ul>
    </div>
</div>
<% } %>