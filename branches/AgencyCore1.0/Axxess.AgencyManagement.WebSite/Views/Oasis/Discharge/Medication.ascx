﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisDischargeFromAgencyMedicationForm" })) { %>
<% var data = Model.ToDictionary(); %>
<%= Html.Hidden("DischargeFromAgency_Id", Model.Id)%>
<%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
<%= Html.Hidden("DischargeFromAgency_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("DischargeFromAgency_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("assessment", "DischargeFromAgency")%>
<%= Html.Hidden("categoryType", "Medications")%> 
<div class="wrapper main">
    <fieldset class="oasis">
        <div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2004');">(M2004)</a> Medication Intervention: If there were any clinically significant medication issues since the previous OASIS assessment, was a physician or the physician-designee contacted within one calendar day of the assessment to resolve clinically significant medication issues, including reconciliation?</label>
                <%=Html.Hidden("DischargeFromAgency_M2004MedicationIntervention", " ", new { @id = "" })%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M2004MedicationIntervention", "00", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M2004MedicationIntervention0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2004MedicationIntervention0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M2004MedicationIntervention", "01", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M2004MedicationIntervention1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2004MedicationIntervention1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2004');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M2004MedicationIntervention", "NA", data.ContainsKey("M2004MedicationIntervention") && data["M2004MedicationIntervention"].Answer == "NA" ? true : false, new { @id = "DischargeFromAgency_M2004MedicationInterventionNA", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2004MedicationInterventionNA"><span class="float_left">NA &ndash;</span><span class="normal margin">No clinically significant medication issues identified since the previous OASIS assessment</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2015');">(M2015)</a> Patient/Caregiver Drug Education Intervention: Since the previous OASIS assessment, was the patient/caregiver instructed by agency staff or other health care provider to monitor the effectiveness of drug therapy, drug reactions, and side effects, and how and when to report problems that may occur?</label>
                <%=Html.Hidden("DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention", " ", new { @id = "" })%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention", "00", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention0"><span class="float_left">0 &ndash;</span><span class="normal margin">No</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention", "01", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention1"><span class="float_left">1 &ndash;</span><span class="normal margin">Yes</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2015');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention", "NA", data.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && data["M2015PatientOrCaregiverDrugEducationIntervention"].Answer == "NA" ? true : false, new { @id = "DischargeFromAgency_M2015PatientOrCaregiverDrugEducationInterventionNA", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2015PatientOrCaregiverDrugEducationInterventionNA"><span class="float_left">NA &ndash;</span><span class="normal margin">Patient not taking any drugs</span></label>
                </div>
            </div>
        </div><div class="clear">
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2020');">(M2020)</a> Management of Oral Medications: Patient's current ability to prepare and take all oral medications reliably and safely, including administration of the correct dosage at the appropriate times/intervals. Excludes injectable and IV medications. (NOTE: This refers to ability, not compliance or willingness.)</label>
                <%=Html.Hidden("DischargeFromAgency_M2020ManagementOfOralMedications", " ", new { @id = "" })%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M2020ManagementOfOralMedications", "00", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M2020ManagementOfOralMedications0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2020ManagementOfOralMedications0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently take the correct oral medication(s) and proper dosage(s) at the correct times.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M2020ManagementOfOralMedications", "01", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M2020ManagementOfOralMedications1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2020ManagementOfOralMedications1">
                        <span class="float_left">1 &ndash;</span>
                        <span class="normal margin">
                            Able to take medication(s) at the correct times if:
                            <ul>
                                <li>
                                    <span class="float_left">(a)</span>
                                    <span class="radio">individual dosages are prepared in advance by another person; OR</span>
                                </li><li>
                                    <span class="float_left">(b)</span>
                                    <span class="radio">another person develops a drug diary or chart.</span>
                                </li>
                            </ul>
                        </span>
                    </label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M2020ManagementOfOralMedications", "02", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M2020ManagementOfOralMedications2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2020ManagementOfOralMedications2"><span class="float_left">2 &ndash;</span><span class="normal margin">Able to take medication(s) at the correct times if given reminders by another person at the appropriate times</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M2020ManagementOfOralMedications", "03", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M2020ManagementOfOralMedications3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2020ManagementOfOralMedications3"><span class="float_left">3 &ndash;</span><span class="normal margin">Unable to take medication unless administered by another person.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2020');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M2020ManagementOfOralMedications", "NA", data.ContainsKey("M2020ManagementOfOralMedications") && data["M2020ManagementOfOralMedications"].Answer == "NA" ? true : false, new { @id = "DischargeFromAgency_M2020ManagementOfOralMedications4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2020ManagementOfOralMedications4"><span class="float_left">NA &ndash;</span><span class="normal margin">No oral medications prescribed.</span></label>
                </div>
            </div>
        </div><div class="column">
            <div class="row">
                <label class="strong"><a href="javascript:void(0);" class="green" onclick="Oasis.ToolTip('M2030');">(M2030)</a> Management of Injectable Medications: Patient's current ability to prepare and take all prescribed injectable medications reliably and safely, including administration of correct dosage at the appropriate times/intervals. Excludes IV medications.</label>
                <%=Html.Hidden("DischargeFromAgency_M2030ManagementOfInjectableMedications", " ", new { @id = "" })%>
                <div>
                    <%=Html.RadioButton("DischargeFromAgency_M2030ManagementOfInjectableMedications", "00", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "00" ? true : false, new { @id = "DischargeFromAgency_M2030ManagementOfInjectableMedications0", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2030ManagementOfInjectableMedications0"><span class="float_left">0 &ndash;</span><span class="normal margin">Able to independently take the correct medication(s) and proper dosage(s) at the correct times.</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M2030ManagementOfInjectableMedications", "01", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "01" ? true : false, new { @id = "DischargeFromAgency_M2030ManagementOfInjectableMedications1", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2030ManagementOfInjectableMedications1">
                        <span class="float_left">1 &ndash;</span>
                        <span class="normal margin">
                            Able to take injectable medication(s) at the correct times if:
                            <ul>
                                <li>
                                    <span class="float_left">(a)</span>
                                    <span class="radio">individual syringes are prepared in advance by another person; OR</span>
                                </li><li>
                                    <span class="float_left">(b)</span>
                                    <span class="radio">another person develops a drug diary or chart.</span>
                                </li>
                            </ul>
                        </span>
                    </label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M2030ManagementOfInjectableMedications", "02", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "02" ? true : false, new { @id = "DischargeFromAgency_M2030ManagementOfInjectableMedications2", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2030ManagementOfInjectableMedications2"><span class="float_left">2 &ndash;</span><span class="normal margin">Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection</span></label>
                </div><div>
                    <%=Html.RadioButton("DischargeFromAgency_M2030ManagementOfInjectableMedications", "03", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "03" ? true : false, new { @id = "DischargeFromAgency_M2030ManagementOfInjectableMedications3", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2030ManagementOfInjectableMedications3"><span class="float_left">3 &ndash;</span><span class="normal margin">Unable to take injectable medication unless administered by another person.</span></label>
                </div><div>
                    <div class="float_right oasis">
                        <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2030');">?</div>
                    </div>
                    <%=Html.RadioButton("DischargeFromAgency_M2030ManagementOfInjectableMedications", "NA", data.ContainsKey("M2030ManagementOfInjectableMedications") && data["M2030ManagementOfInjectableMedications"].Answer == "NA" ? true : false, new { @id = "DischargeFromAgency_M2030ManagementOfInjectableMedications4", @class = "radio float_left" })%>
                    <label for="DischargeFromAgency_M2030ManagementOfInjectableMedications4"><span class="float_left">NA &ndash;</span><span class="normal margin">No injectable medications prescribed.</span></label>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save/Continue</a></li>
            <li><a href="javascript:void(0);" onclick="Discharge.FormSubmit($(this));">Save/Exit</a></li>
        </ul><ul class="float_right">
            <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowOasisValidationModal('DischargeFromAgency_ValidationContainer','{0}','{1}','{2}','{3}');\">Check for Errors</a>", Model.Id, Model.PatientId, Model.EpisodeId, "DischargeFromAgency")%></li>
        </ul>
    </div>
</div>
<% } %>