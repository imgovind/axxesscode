﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientContactInfoViewData>" %>
<div class="wrapper">
    <fieldset>
        <legend>All Open OASIS Assessments</legend>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.Export();">Excel Export</a></li></ul></div>
    </fieldset>
    <div class="clear">&nbsp;</div>
    <div class="ReportGrid">
        <%= Html.Telerik().Grid<OpenOasis>().Name("Report_Clinical_OpenOasis_Grid").Columns(columns =>
           {
               columns.Bound(e => e.PatientName).Width(140);
               columns.Bound(e => e.PatientIdNumber).Title("MR#").Width(90);
               columns.Bound(e => e.AssessmentName).Title("Assessment Type");
               columns.Bound(e => e.Date).Width(100);
               columns.Bound(e => e.Status).Width(150);
               columns.Bound(e => e.CurrentlyAssigned).Width(120);
           })
           .DataBinding(dataBinding => dataBinding.Ajax().Select("ClinicalOpenOasis", "Report", new { }))
           .Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $("#Report_Clinical_OpenOasis_Grid .t-grid-content").css({ 'height': 'auto' });</script>