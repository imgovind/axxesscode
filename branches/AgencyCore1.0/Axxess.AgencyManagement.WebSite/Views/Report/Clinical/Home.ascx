﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul>
    <li class="widget">
        <div class="widget-head"><h5>Clinical Reports</h5></div>
        <div class="widget-content" style="height: 100%;">
            <div class="report_links">
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Clinical/OpenOasis" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Open OASIS</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/CurrentHospitalized" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Current Hospitalized Patients</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/CnaEpisodes" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">CNA Episodes</span></a></li>
                </ul>
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Clinical/Orders" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Orders Management</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/RecentHospitalized" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Recent Hospitalization</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/MissedVisits" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Missed Visit Report</span></a></li> 
                </ul>
                <div class="clear">&nbsp;</div>
            </div>
        </div>
    </li>
</ul>