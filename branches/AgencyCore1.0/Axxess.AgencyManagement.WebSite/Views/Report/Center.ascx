﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Report Center | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','reportcenter');</script>")%>
<div id="reportingTabs" class="tabs vertical-tabs vertical-tabs-left ReportingContainer">
    <ul id="reportingTabContainer" class="verttab strong">
        <li><a href="#all_reports" title="/Report/Dashboard">Reports Home</a></li>
        <li><a href="#patient_reports" title="/Report/Patient">Patient Reports</a></li>
        <li><a href="#clinical_reports" title="/Report/Clinical">Clinical Reports</a></li>
        <li><a href="#schedule_reports" title="/Report/Schedule">Schedule Reports</a></li>
        <li><a href="#billing_reports" title="/Report/Billing">Billing Reports</a></li>
        <li><a href="#employee_reports" title="/Report/Employee">Employee Reports</a></li>
        <li><a href="#statistical_reports" title="/Report/Statistical">Statistical Reports</a></li>
    </ul>
    <div id="all_reports" class="general">
        <% Html.RenderPartial("~/Views/Report/Dashboard.ascx"); %>
    </div>
    <div id="patient_reports" class="general loading"></div>
    <div id="clinical_reports" class="general loading"></div>
    <div id="schedule_reports" class="general loading"></div>
    <div id="billing_reports" class="general loading"></div>
    <div id="employee_reports" class="general loading"></div>
    <div id="statistical_reports" class="general loading"></div>
</div>
