﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul>
    <li class="widget">
        <div class="widget-head"><h5>Billing Reports</h5></div>
        <div class="widget-content" style="height: 100%;">
            <div class="report_links">
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Billing/OutstandingClaims" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Outstanding Claims</span></a></li> 
                    <li class="link"><a href="/Report/Billing/SummaryReport" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Billing Summary Report</span></a></li> 
                    <li class="link"><a href="/Report/Billing/SubmittedHistory" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Submitted Claims History</span></a></li> 
                    <li class="link"><a href="/Report/Billing/PaidHistory" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Paid Claims History</span></a></li> 
                    <li class="link"><a href="/Report/Billing/RejectedHistory" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Rejected Claims History</span></a></li> 
                    <li class="link"><a href="/Report/Billing/CancelledHistory" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Cancelled Claims History</span></a></li> 
                </ul>
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Billing/PPSAnalysis" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">PPS Analysis</span></a></li> 
                    <li class="link"><a href="/Report/Billing/DetailReport" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Episode Detail Report</span></a></li> 
                    <li class="link"><a href="/Report/Billing/HHRGReport" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">HHRG Report</span></a></li> 
                    <li class="link"><a href="/Report/Billing/ARReport" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">A/R Report</span></a></li> 
                    <li class="link"><a href="/Report/Billing/InsuranceVisits" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Visits By Insurance</span></a></li> 
                    <li class="link"><a href="/Report/Billing/AccrualReport" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Accrual A/R Report</span></a></li> 
                </ul>
                <div class="clear">&nbsp;</div>
            </div>
        </div>
    </li>
</ul>