﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DateTime>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Payroll Summary | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','payrollsummary');</script>")%>
<% using (Html.BeginForm("Search", "Payroll", FormMethod.Post, new { @id = "searchPayrollForm" })) { %>
<div class="main wrapper">
    <fieldset>
        <legend>Payroll Summary</legend>
        <div class="column">
            <div class="row">
                <div class="float_left"><label>From</label>&nbsp;<%= Html.Telerik().DatePicker().Format("MM/dd/yyyy").Name("startDate").Value(Model).HtmlAttributes(new { @class = "text required date" }) %></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <div class="float_left"><label>To</label>&nbsp;<%= Html.Telerik().DatePicker().Format("MM/dd/yyyy").Name("endDate").Value(DateTime.Today).MaxDate(DateTime.Today).HtmlAttributes(new { @class = "text required date" })%></div>
            </div>
        </div>
        <div class="clear">&nbsp;</div>
        <div class="buttons float_right"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Generate</a></li></ul></div>
        <div class="clear">&nbsp;</div>
    </fieldset>
    <div id="payrollSearchResult"></div>
</div>
<% } %>