﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="faux-grid">
	<table cellspacing="0">
		<thead class="faux-grid-header">
			<tr>
				<th>Patient Name</th>
				<th>MR#</th>
				<th>Due Date</th>
			</tr>
        </thead>
	    <tbody id="recertUpcomingWidgetContent" class="faux-grid-content"></tbody>
    </table>
</div>
<div id="upcomingRecertsMore" class="widget-more"><a onclick="UserInterface.ShowUpcomingRecerts();" href="javascript:void(0);">More &raquo;</a></div>