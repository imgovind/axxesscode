﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('New Communication Note | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','newcommnote');</script>")%>
<% using (Html.BeginForm("Add", "CommunicationNote", FormMethod.Post, new { @id = "newCommunicationNoteForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <div class="column">
            <div class="row"><label for="New_CommunicationNote_PatientName" class="float_left">Patient Name:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", (Model != null && !Model.Id.IsEmpty()) ? Model.Id.ToString() : "", new { @id = "New_CommunicationNote_PatientName", @class="requireddropdown" })%></div></div>
            <div class="row"><label for="New_CommunicationNote_Date" class="float_left">Date:</label><div class="float_right"><%= Html.Telerik().DatePicker().Name("Created").Value(DateTime.Now).MaxDate(DateTime.Now).HtmlAttributes(new { @id = "New_CommunicationNote_Date", @class = "text required date" })%></div></div>
        </div>
        <table class="forms"><tbody>
            <tr><td><label for="New_CommunicationNote_Text">Communication Text</label><br /><textarea id="New_CommunicationNote_Text" name="Text" cols="5" rows="12" style="height: 120px;"></textarea></td></tr>
        </tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newcommnote');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>
<%} %>
