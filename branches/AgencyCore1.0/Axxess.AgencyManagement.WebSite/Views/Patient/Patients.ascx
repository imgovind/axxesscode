﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientSelection>>" %>
<% Html.Telerik().Grid<PatientSelection>().Name("PatientSelectionGrid").Columns(columns => {
       columns.Bound(p => p.LastName);
       columns.Bound(p => p.FirstName);
       columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "display:none" }).HtmlAttributes(new { style = "display:none" }).Width(0);
   }).DataBinding(dataBinding => dataBinding.Ajax().Select("All", "Patient", new { statusId = 1, paymentSourceId = 0, name = string.Empty })).Sortable().Selectable().Scrollable().Footer(false).ClientEvents(events => events.OnDataBound(() => 
   { %>
        function(e) {
            if (Patient._patientId == "") $('#window_patientcenter .layout_left .bottom .t-grid-content tbody tr:has(td):first').click();
            else $('td:contains(' + Patient._patientId + ')').closest('tr').click();
        }
    <% }).OnRowSelected("Patient.OnPatientRowSelected")).Render(); %>