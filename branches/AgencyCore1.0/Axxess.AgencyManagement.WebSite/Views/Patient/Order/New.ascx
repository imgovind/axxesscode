﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('New Order | ",
        Axxess.AgencyManagement.App.Current.AgencyName,
        "','neworder');</script>")%>
<% using (Html.BeginForm("Add", "Order", FormMethod.Post, new { @id = "newOrderForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <div class="column">
            <div class="row"><label for="New_Order_PatientName" class="float_left">Patient Name:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "New_Order_PatientName", @class="requireddropdown" })%></div></div>
            <div class="row"><label for="New_Order_PhysicianDropDown" class="float_left">Physician:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Physicians, "PhysicianId",  "", new { @id = "New_Order_PhysicianDropDown", @class="requireddropdown" })%></div></div>
            <div class="row"><label for="New_Order_Date" class="float_left">Date:</label><div class="float_right"><%= Html.Telerik().DatePicker().Name("OrderDate").Value(DateTime.Now).MaxDate(DateTime.Now).HtmlAttributes(new { @id = "New_Order_Date", @class = "text required date" })%></div></div>
        </div>
        <div class="clear"></div>
        <table class="forms"><tbody>
            <tr><td><label for="New_Order_Summary">Summary (Optional)</label><br /><%= Html.TextBox("Summary", "", new { @id = "New_Order_Summary", @class = "", @maxlength = "100", @style = "width: 600px;" })%></td></tr>
            <tr><td><label for="New_Order_Text">Order Description</label><br /><textarea id="New_Order_Text" name="Text" cols="5" rows="12" style="height: 120px;"></textarea></td></tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="New_Order_ClinicianSignature" class="bigtext float_left">Clinician Signature:</label>
                <div class="float_right"><%= Html.Password("SignatureText", "", new { @id = "New_Order_ClinicianSignature", @class = "" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Order_ClinicianSignatureDate" class="bigtext float_left">Signature Date:</label>
                <div class="float_right"><%= Html.Telerik().DatePicker().Name("SignatureDate").MaxDate(DateTime.Today).Value(DateTime.Today.ToString("MM/dd/yyy")).HtmlAttributes(new { @id = "New_Order_ClinicianSignatureDate", @class = "date required" })%></div>
            </div>
        </div>
    </fieldset><%= Html.Hidden("Status", "", new { @id = "New_Order_Status" })%>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$('#New_Order_Status').val('110');$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="$('#New_Order_Status').val('125');$(this).closest('form').submit();">Complete</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('neworder');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>
<%} %>
