﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<div class="abs above">
    <div class="float_left">
        <label>Show</label><select id="patientActivityDropDown" name="patientActivityDropDown" class="patientActivityDropDown"><option value="All" selected="selected">All</option><option value="Orders">Orders</option><option value="Nursing">Nursing</option><option value="PT">PT</option><option value="OT">OT</option><option value="ST">ST</option><option value="HHA">HHA</option><option value="MSW">MSW</option><option value="Dietician">Dietician</option></select>
        <label>Date</label><select id="patientActivityDateDropDown" name="patientActivityDateDropDown" class="patientActivityDateDropDown"><option value="All" selected="selected">All</option><option disabled="disabled">---------------------------------------------</option><option value="DateRange">Date Range</option><option disabled="disabled">---------------------------------------------</option><option value="ThisEpisode">This Episode</option><option value="LastEpisode">Last Episode</option><option value="NextEpisode">Next Episode</option><option disabled="disabled">---------------------------------------------</option><option value="Today">Today</option><option value="ThisWeek">This Week</option><option value="ThisWeekToDate">This Week-to-date</option><option value="LastMonth">Last Month</option><option value="ThisMonth">This Month</option><option value="ThisMonthToDate">This Month-to-date</option><option value="ThisFiscalQuarter">This Fiscal Quarter</option><option value="ThisFiscalQuarterToDate">This Fiscal Quarter-to-date</option><option value="ThisFiscalYear">This Fiscal Year</option><option value="ThisFiscalYearToDate">This Fiscal Year-to-date</option><option value="Yesterday">Yesterday</option><option value="LastWeek">Last Week</option><option value="LastWeekToDate">Last Week-to-date</option><option value="LastMonthToDate">Last Month-to-date</option><option value="LastFiscalQuarter">Last Fiscal Quarter</option><option value="LastFiscalQuarterToDate">Last Fiscal Quarter-to-date</option><option value="LastFiscalYear">Last Fiscal Year</option><option value="LastFiscalYearToDate">Last Fiscal Year-to-date</option><option value="NextWeek">Next Week</option><option value="Next4Weeks">Next 4 Weeks</option><option value="NextMonth">Next Month</option><option value="NextFiscalQuarter">Next Fiscal Quarter</option><option value="NextFiscalYear">Next Fiscal Year</option></select>
    </div>
    <div id="patientCenterActivityFilter" class="float_left">
        <div id="dateRangeText" class="float_right" style="display: none;"></div>
        <div class="CustomDateRange" style="display: none;">
        <label>From</label><%= Html.Telerik().DatePicker().Name("From").Value(DateTime.Now.Subtract(TimeSpan.FromDays(60))).HtmlAttributes(new { @id = "patientActivityFromDate", @class = "date" })%>
        <label>To</label><%= Html.Telerik().DatePicker().Name("To").Value(DateTime.Today).HtmlAttributes(new { @id = "patientActivityToDate", @class = "date"})%>
         <div class="buttons float_right" style="width:100px;" ><ul><li><a href="javascript:void(0);" onclick="Patient.CustomDateRange();">Search</a></li></ul></div>
    </div>
    </div>
</div>
<% var val = Model != null && Model.Count > 0 ? Model.First().PatientId : Guid.Empty;
   Html.Telerik().Grid<ScheduleEvent>().Name("PatientActivityGrid").Columns(columns => {
       columns.Bound(s => s.EventId).Visible(false);
       columns.Bound(s => s.Url).ClientTemplate("<#=Url#>").Title("Task");
       columns.Bound(s => s.EventDate).Format("{0:MM/dd/yyyy}").Title("Target Date").Width(100);
       columns.Bound(s => s.UserName).Title("Assigned To").Width(150);
       columns.Bound(s => s.StatusName).Title("Status");
       columns.Bound(s => s.StatusComment).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip red_note\" href=\"javascript:void(0);\" tooltip=\"<#=StatusComment#>\"> </a>");
       columns.Bound(s => s.Comments).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=Comments#>\"></a>");
       columns.Bound(s => s.EpisodeNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip blue_note\" href=\"javascript:void(0);\" tooltip=\"<#=EpisodeNotes#>\"></a>");
       columns.Bound(s => s.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(30);
       columns.Bound(s => s.AttachmentUrl).Title(" ").ClientTemplate("<#=AttachmentUrl#>").Width(30);
       columns.Bound(s => s.ActionUrl).ClientTemplate("<#=ActionUrl#>").Title("Action").Width(130);
       columns.Bound(s => s.EpisodeId).HeaderHtmlAttributes(new { style = "display:none" }).HtmlAttributes(new { style = "display:none" }).Width(0);
   }).ClientEvents(c => c.OnRowDataBound("Schedule.tooltip")).DataBinding(dataBinding => dataBinding.Ajax().Select("Activity", "Patient", new { patientId = val, discipline = "All", dateRangeId = "All", rangeStartDate = DateTime.Now.Subtract(TimeSpan.FromDays(60)), rangeEndDate = DateTime.Now })).Sortable().Scrollable().Footer(false).Render();
%>
