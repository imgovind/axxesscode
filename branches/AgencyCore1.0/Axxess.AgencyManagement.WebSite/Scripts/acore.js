var acore = (function($) {
    return {
        csstop: 1,
        cssleft: 1,
        openwindows: 0,
        maxwindows: 8,
        minwinwidth: 1000,
        minwinheight: 555,
        windows: {},
        getid: function(obj) { return obj.attr('id').substring(obj.attr('id').lastIndexOf("_") + 1, obj.attr('id').length); },
        getwin: function(id) { return $('#window_' + id); },
        getcontentdiv: function(id) { return $('#window_' + id + '_content'); },
        gettask: function(id) { return $('#task_' + id); },
        getdata: function(id, url, oncallback, inputs) {
            if (url == undefined) url = acore.windows[id].url;
            if (oncallback == undefined) oncallback = acore.windows[id].oncallback;
            acore.getcontentdiv(id).load(url, inputs, function(responseText, textStatus, XMLHttpRequest) {
                acore.getcontentdiv(id).removeClass("loading");
                if (textStatus == 'error') acore.getcontentdiv(id).html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cimg src=%22/Images/icons/error.png%22 /%3E%3C" +
		            "h1%3EThere was an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for f" +
		            "urther assistance.%3C/div%3E"));
                if (oncallback != null && typeof (oncallback) == 'function') oncallback();
            });
        },
        clearactive: function() {
            $('a.active, tr.active').removeClass('active');
            $('ul.menu').hide();
            $('.window_stack').removeClass('window_stack');
        },
        resize: function(id) {
            if (acore.getwin(id).hasClass('window_full')) acore.restore(id);
            else acore.maximize(id);
            acore.winfocus(id);
        },
        minimize: function(id) {
            acore.getwin(id).prependTo("#desktop").removeClass("window_stack").hide();
            if (!$('.window').last().is(':hidden')) $('.window').last().addClass('window_stack');
        },
        restore: function(id) {
            acore.getwin(id)
                .removeClass('window_full')
                .css({ 'top': acore.getwin(id).attr('dt'), 'left': acore.getwin(id).attr('dl'), 'right': acore.getwin(id).attr('dr'),
                    'bottom': acore.getwin(id).attr('db'), 'width': acore.getwin(id).attr('dw'), 'height': acore.getwin(id).attr('dh')
                });
        },
        maximize: function(id) {
            acore.getwin(id)
                .attr({ 'dt': acore.getwin(id).css('top'), 'dl': acore.getwin(id).css('left'), 'dr': acore.getwin(id).css('right'),
                    'db': acore.getwin(id).css('bottom'), 'dw': acore.getwin(id).css('width'), 'dh': acore.getwin(id).css('height')
                })
                .addClass('window_full')
                .css({ 'top': '0', 'left': '0', 'right': '0', 'bottom': '0', 'width': '100%', 'height': '100%' });
        },
        open: function(id, url, oncallback, inputs) {
            if (acore.windows[id] != null) {
                if (acore.windows[id].isopen) {
                    $.jGrowl("You already have a open window for " + acore.windows[id].windowname + ".  Please close down this window before opening another one like it.", { theme: 'error', life: 5000 });
                    acore.winfocus(id);
                } else if (acore.openwindows < acore.maxwindows) {
                    acore.createtask(id);
                    acore.createwindow(id, url, oncallback, inputs);
                    acore.windows[id].isopen = true;
                    acore.openwindows++;
                    acore.winfocus(id);
                } else {
                    acore.clearactive();
                    $.jGrowl("You can only have " + acore.maxwindows + " concurrent windows at once. Please close down some of your windows to proceed",
			            { theme: 'error', life: 5000 });
                }
            } else {
                acore.clearactive();
                $.jGrowl("Not able to find window: " + id + ".  Please contact Axxess for assistance in troubleshooting.", { theme: 'error', life: 5000 });
            }
        },
        close: function(id) {
            if (acore.getwin(id).find('.cke_editor').length) CKEDITOR.remove(CKEDITOR.instances['New_Message_Body']);
            acore.getwin(id).remove();
            acore.gettask(id).remove();
            acore.windows[id].isopen = false;
            acore.openwindows--;
            $('.window').last().addClass('window_stack');
        },
        createtask: function(id) {
            $("#task").append(unescape("%3Cli id=%22task_" + id + "%22%3E%3Ca href=%22javascript:acore.taskbar('" + id + "');%22 title=%22" +
                acore.windows[id].taskname + "%22%3E%3Cimg src=%22/images/icons/" + acore.windows[id].icon + "%22 alt=%22Icon%22 /%3E" + acore.windows[id].taskname
                + "%3C/a%3E"));
        },
        createwindow: function(id, url, oncallback, inputs) {
            $("#desktop").append(unescape("%3Cdiv id=%22window_" + id + "%22 class=%22abs stack window" + (acore.windows[id].resize ? "" : " no-resize") + "%22" +
		        ((parseInt(acore.windows[id].width) > 0 && parseInt(acore.windows[id].height) > 0) ? " style=%22width:" + parseInt(acore.windows[id].width) +
		        (acore.windows[id].width.indexOf("%") > 0 ? "%" : "px") + ";height:" + parseInt(acore.windows[id].height) + (acore.windows[id].height.indexOf("%") >
		        0 ? "%" : "px") + ";" + (acore.windows[id].center ? ("top:" + (acore.windows[id].height.indexOf("%") > 0 ? 50 - (parseInt(acore.windows[id].height)
		        / 2) + "%" : (parseInt($("#desktop").attr('clientHeight')) / 2) - (parseInt(acore.windows[id].height) / 2) + "px") + ";left" + ":" +
		        (acore.windows[id].width.indexOf("%") > 0 ? 50 - (parseInt(acore.windows[id].width) / 2) + "%" : (parseInt($("#desktop").attr('clientWidth')) / 2) -
		        (parseInt(acore.windows[id].width) / 2) + "px") + ";") : "") + "%22" : "") + "%3E%3Cdiv class=%22abs window_inner%22%3E%3Cdiv class=%22window_top" +
		        "%22%3E%3Cspan class=%22float_left%22%3E%3Cimg src=%22/images/icons/" + acore.windows[id].icon + "%22 alt=%22Icon%22 /%3E%3C/span%3E%3Cspan class" +
		        "=%22abs%22%3E" + acore.windows[id].windowname + "%3C/span%3E%3Cspan class=%22float_right%22%3E%3Ca href=%22javascript:acore.minimize('" + id +
		        "');%22 class=%22window_min%22%3E%3C/a%3E%3Ca href=%22javascript:acore.resize('" + id + "');%22 class=%22window_resize%22%3E%3C/a%3E%3Ca href=%22" +
		        "javascript:acore.close('" + id + "');%22 class=%22window_close%22%3E%3C/a%3E%3C/span%3E%3C/div%3E%3Cdiv id=%22window_" + id + "_content%22 class" +
		        "=%22abs window_content loading%22%3E%3C/div%3E%3Cdiv class=%22abs window_bottom%22%3E" + acore.windows[id].status + "%3C/div%3E%3C/div%3E%3C/div" +
		        "%3E"));
            var height = parseInt($('#desktop').attr('clientHeight') * .85);
            var width = parseInt($('#desktop').attr('clientWidth') * .85);
            if (!(parseInt(acore.getwin(id).css('width')) > 0 && parseInt(acore.getwin(id).css('height')) > 0)) {
                if (height < acore.minwinheight) height = acore.minwinheight;
                if (width < acore.minwinwidth) width = acore.minwinwidth;
                acore.getwin(id).css({ 'height': (height.toString() + "px"), 'width': (width.toString() + "px") });
                if (parseInt(acore.getwin(id).css('height')) + (++acore.csstop * 25) > $('#desktop').attr('clientHeight')) acore.csstop = acore.cssleft = 0;
                if (parseInt(acore.getwin(id).css('width')) + (++acore.cssleft * 25) > $('#desktop').attr('clientWidth')) acore.csstop = acore.cssleft = 0;
            }
            if (!(parseInt(acore.getwin(id).css('top')) > 0 || parseInt(acore.getwin(id).css('left')) > 0) && !(acore.getwin(id).hasClass('window_full')))
                acore.getwin(id).css({ 'top': (acore.csstop * 25).toString() + "px", 'left': (acore.cssleft * 25).toString() + "px" });
            if (window.location !== window.top.location) window.top.location = window.location;
            acore.getwin(id)
                .mousedown(function() { if (!$(this).closest('div.window').hasClass('window_stack')) acore.winfocus(acore.getid($(this).closest('div.window'))) })
                .draggable({ containment: 'parent', handle: 'div.window_top .abs' })
		        .resizable({ containment: 'parent', handles: 'n, ne, e, se, s, sw, w, nw', minWidth: acore.minwinwidth, minHeight: acore.minwinheight })
		        .find('div.window_top')
			        .dblclick(function() { acore.resize(acore.getid($(this).closest('div.window'))); })
			        .find('img')
			            .dblclick(function() { acore.close(acore.getid($(this).closest('div.window'))); });
            acore.getdata(id, url, oncallback, inputs);
            acore.getcontentdiv(id).scroll(function() { $(".t-datepicker").each(function() { if ($(this).data("tDatePicker") != null) { $(this).data("tDatePicker").hidePopup(); } }); });
        },
        winfocus: function(id) {
            acore.clearactive();
            acore.getwin(id).appendTo("#desktop").addClass("window_stack").show();
        },
        openprintview: function(url, editClick, approveClick, returnClick) {
            $('body').append(unescape("%3Cdiv id=%22shade%22%3E%3C/div%3E%3Cdiv id=%22printbox%22%3E%3Ciframe name=%22printview%22 id=%22printview%22 src=%22%22 " +
		        "class=%22loading%22%3E%3C/iframe%3E%3Cdiv id=%22printreturnreason%22%3E%3Clabel class=%22strong%22%3EReason for Return:%3C/label%3E%3Ctextarea%3" +
		        "E%3C/textarea%3E%3C/div%3E%3C/div%3E%3Cdiv id=%22printcontrols%22 class=%22buttons%22%3E%3Cul%3E" + (editClick == undefined ? "" : "%3Cli%3E%3Ca" +
		        " id=%22printedit%22 href=%22javascript:void(0);%22%3EEdit%3C/a%3E%3C/li%3E") + (approveClick == undefined ? "" : "%3Cli%3E%3Ca id=%22printapprov" +
		        "e%22 href=%22javascript:void(0);%22%3EApprove%3C/a%3E%3C/li%3E") + (returnClick == undefined ? "" : "%3Cli%3E%3Ca id=%22printreturn%22 href=%22j" +
		        "avascript:void(0);%22%3EReturn%3C/a%3E%3C/li%3E%3Cli class=%22very_hidden%22%3E%3Ca id=%22printreturncancel%22 href=%22javascript:void(0);%22%3E" +
		        "Cancel%3C/a%3E%3C/li%3E") + "%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22acore.print();%22%3EPrint%3C/a%3E%3C/li%3E%3Cli%3E%3Ca href=" +
		        "%22javascript:void(0);%22 onclick=%22acore.closeprintview();%22%3EClose%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
            $('#printview').attr('src', url).ready(function() { $(this).removeClass('loading'); });
            if (editClick != undefined) $('#printedit').bind('click', editClick);
            if (approveClick != undefined) $('#printapprove').bind('click', approveClick);
            if (returnClick != undefined) {
                $('#printreturn').bind('click', returnClick);
                $('#printreturncancel').click(function() {
                    $("#printcontrols li a").each(function() {
                        if ($(this).attr("id") != "printreturncancel") $(this).show();
                        else $(this).parent().addClass("very_hidden");
                        $("#printreturnreason").slideUp('slow');
                    });
                });
            }
        },
        print: function() {
            window.frames['printview'].focus();
            window.frames['printview'].print();
        },
        closeprintview: function() {
            $('#shade').remove();
            $('#printbox').remove();
            $('#printcontrols').remove();
            $('#printview').remove();
        },
        addmenu: function(name, id, submenu, icon) {
            if (icon != undefined) icon = "%3Cimg src=%22/Images/icons/" + icon + "%22 alt=%22Menu Icon%22 /%3E";
            else icon = "";
            if (id == undefined) id = name;
            if (submenu == undefined || submenu == "mainmenu") {
                moreicon = "";
                submenu = "mainmenu";
            } else moreicon = "%3Cimg src=%22/Images/gui/more.png%22 alt=%22More%22 /%3E";
            $("#" + submenu).append(unescape("%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22menu_trigger%22%3E" + icon + name + moreicon + "%3C/a%3E%3Cul " +
		    "id=%22" + id + "%22 class=%22menu%22%3E%3C/ul%3E%3C/li%3E"));
        },
        addmenulink: function(name, href, submenu) {
            if (submenu == undefined) submenu = "mainmenu";
            $("#" + submenu).append(unescape("%3Cli%3E%3Ca href=%22" + href + "%22 target=%22_blank%22 %3E" + name + "%3C/a%3E%3C/li%3E"));
        },
        addmenuitem: function(id, menu, menuname, url, oncallback) {
            if (menuname == undefined) menuname = acore.windows[id].menuname;
            if (url == undefined) url = acore.windows[id].url;
            if (oncallback == undefined) oncallback = acore.windows[id].oncallback;
            $("#" + menu).append(unescape("%3Cli%3E%3Ca href=%22javascript:acore.open('" + id + "', '" + url + "', " + oncallback + ");%22%3E" + menuname + "%3C/" +
                "a%3E%3C/li%3E"));
        },
        addwindow: function(id, windowname, url, oncallback, menu, menuname, status, width, height, resize, center, taskname, icon) {
            if (width == undefined) width = "-1";
            if (height == undefined) height = "-1";
            if (resize == undefined) resize = true;
            if (center == undefined) center = false;
            if (menu == undefined) menu = "mainmenu";
            if (status == undefined) status = windowname;
            if (icon == undefined) icon = "axxess_icon.png";
            if (taskname == undefined) taskname = windowname;
            if (menuname == undefined) menuname = windowname;
            acore.windows[id] = { "url": url, "oncallback": oncallback, "windowname": windowname, "menuname": menuname, "taskname": taskname, "status": status,
                "menu": menu, "width": width, "height": height, "resize": resize, "center": center, "icon": icon, "isopen": false
            };
            if (menu) $("#" + menu).append(unescape("%3Cli%3E%3Ca href=%22javascript:acore.open('" + id + "');%22%3E" + menuname + "%3C/a%3E%3C/li%3E"));
        },
        taskbar: function(id) {
            if (acore.getwin(id).hasClass('window_stack')) acore.minimize(id);
            else acore.winfocus(id);
        },
        renamewindow: function(title, id) {
            acore.getwin(id).find('.window_top .abs').html(title);
            acore.gettask(id).find('a').html(acore.gettask(id).find('img')).attr('title', title).find('img').after(title);
        },
        init: function() {
            $("body").append(unescape("%3Cdiv class=%22abs%22 id=%22desktop%22%3E%3C/div%3E%3Cdiv id=%22tab%22%3E%3C/div%3E%3Cdiv class=%22ie-shadow%22%3E%3C/div" +
	            "%3E%3Cdiv class=%22abs%22 id=%22bar_top%22%3E%3Cspan class=%22float_right%22 style=%22color: #000;%22%3E%3Ca href=%22/Logout%22%3E%3Cimg src=%22" +
	            "/Images/icons/logout.png%22 alt=%22Logout%22 /%3ELogout%3C/a%3E%3C/span%3E%3Cul id=%22mainmenu%22%3E%3C/ul%3E%3C/div%3E%3Cdiv class=%22abs%22 id" +
	            "=%22bar_bottom%22%3E%3Cul id=%22task%22%3E%3C/ul%3E%3Cspan class=%22float_right%22%3E&copy; 2009 &ndash; " + (new Date).getFullYear() + " Axxess" +
	            "&trade; Technology Solutions, All Rights Reserved%3C/span%3E%3C/div%3E"));
        }
    };
})(jQuery);