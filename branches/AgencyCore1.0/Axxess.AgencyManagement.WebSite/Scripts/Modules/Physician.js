﻿var Physician = {
    InitNew: function() {
        $("#newPhysicianForm").clearForm();

        $(".numeric").numeric();
        $(".names").alpha({ nocaps: false });

        $('#New_Physician_NpiNumber').blur(function() {
            var npiNumber = $("#New_Physician_NpiNumber").val();
            if (npiNumber.length > 0) {
                U.postUrl("/Physician/CheckPecos", "npi=" + npiNumber, function(data) {
                    if (data.isSuccessful) {
                        $("#New_Physician_PecosCheck").html("<img src='/Images/icons/success.png' width='16' height='16' alt='PECOS VERIFIED' border='0' />")
                    }
                    else {
                        $("#New_Physician_PecosCheck").html("<img src='/Images/icons/error.png' width='16' height='16' alt='NOT PECOS VERIFIED' border='0' />");
                    }
                });
            }
            else {
                $("#New_Physician_PecosCheck").hide();
            }
        });

        $("#New_Physician_NpiSearch").autocomplete('/LookUp/Npis', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Id, result: data[i].Id };
                }
                return rows;
            },
            formatItem: function(row, i, n) {
                return row.Id + '-' + row.ProviderFirstName + '  ' + row.ProviderLastName;
            },
            width: 400
        }).result(function(event, row, formatted) {
            $('tbody', $(this).closest('table')).clearForm();
            if (row) {
                $("#New_Physician_FirstName").val(row.ProviderFirstName != null ? U.toTitleCase(row.ProviderFirstName) : '');
                $("#New_Physician_MiddleIntial").val(row.ProviderMiddleName != null ? U.toTitleCase(row.ProviderMiddleName.substring(0, 1)) : '');
                $("#New_Physician_LastName").val(row.ProviderLastName != null ? U.toTitleCase(row.ProviderLastName) : '');
                $("#New_Physician_Credentials").val(row.ProviderCredentialText != null ? row.ProviderCredentialText : '');
                $("#New_Physician_NpiNumber").val(row.Id != null ? row.Id : '');
                $("#New_Physician_NpiNumber").blur();
                $("#New_Physician_AddressLine1").val(row.ProviderFirstLineBusinessPracticeLocationAddress != null ? U.toTitleCase(row.ProviderFirstLineBusinessPracticeLocationAddress) : '');
                $("#New_Physician_AddressLine2").val(row.ProviderSecondLineBusinessPracticeLocationAddress != null ? U.toTitleCase(row.ProviderSecondLineBusinessPracticeLocationAddress) : '');
                $("#New_Physician_AddressCity").val(row.ProviderBusinessPracticeLocationAddressCityName != null ? U.toTitleCase(row.ProviderBusinessPracticeLocationAddressCityName) : '');
                $("#New_Physician_AddressStateCode").val(row.ProviderBusinessPracticeLocationAddressStateName != null ? row.ProviderBusinessPracticeLocationAddressStateName : '');
                if (row.ProviderBusinessPracticeLocationAddressPostalCode !== null) {
                    var zipCode = row.ProviderBusinessPracticeLocationAddressPostalCode;
                    $("#New_Physician_AddressZipCode").val(zipCode.substring(0, 5));
                }
                if (row.ProviderBusinessPracticeLocationAddressTelephoneNumber !== null) {
                    var primaryPhone = row.ProviderBusinessPracticeLocationAddressTelephoneNumber;
                    $("#New_Physician_Phone1").val(primaryPhone.substring(0, 3));
                    $("#New_Physician_Phone2").val(primaryPhone.substring(3, 6));
                    $("#New_Physician_Phone3").val(primaryPhone.substring(6, 10));
                }
                if (row.ProviderBusinessPracticeLocationAddressFaxNumber !== null) {
                    var faxNumber = row.ProviderBusinessPracticeLocationAddressFaxNumber;
                    $("#New_Physician_Fax1").val(faxNumber.substring(0, 3));
                    $("#New_Physician_Fax2").val(faxNumber.substring(3, 6));
                    $("#New_Physician_Fax3").val(faxNumber.substring(6, 10));
                }
            }
        });

        $("#newPhysicianForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Physician.RebindList();
                            $.jGrowl("New physician successfully added.", { theme: 'success', life: 5000 });
                            U.closeDialog();
                            UserInterface.CloseWindow('newphysician');
                            Lookup.loadPhysicians();
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEdit: function() {
        $("#editPhysicianForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Physician.RebindList();
                            $.jGrowl("Physician successfully updated.", { theme: 'success', life: 5000 });
                            U.closeDialog();
                            UserInterface.CloseWindow('editphysician');
                            Lookup.loadPhysicians();
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    RebindList: function() {
        var grid = $('#List_Physician').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    },
    SetPrimary: function(id, patientId) {
        if (confirm("Are you sure you want to set this physician as primary")) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Physician/SetPrimary",
                data: "id=" + id + "&patientId=" + patientId,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        var physicianContact = $('#Edit_patient_PhysicianContact_Grid').data('tGrid');
                        if (physicianContact != null) {
                            physicianContact.rebind({ PatientId: patientId });
                        }
                    }
                }
            });
        }
    },
    Delete: function(id) {
        if (confirm("Are you sure you want to delete this physician?")) {
            var input = "id=" + id;
            U.postUrl("/Physician/Delete", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    Physician.RebindList();
                    $.jGrowl("Physician successfully deleted.", { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    }
}