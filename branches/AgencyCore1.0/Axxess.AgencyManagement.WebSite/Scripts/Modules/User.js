﻿var User = {
    InitNew: function() {
        $(".numeric").numeric();
        $(".names").alpha({ nocaps: false });

        $('#New_User_HomePhoneArray1').autotab({ target: 'New_User_HomePhoneArray2', format: 'numeric' });
        $('#New_User_HomePhoneArray2').autotab({ target: 'New_User_HomePhoneArray3', format: 'numeric', previous: 'New_User_HomePhoneArray1' });
        $('#New_User_HomePhoneArray3').autotab({ target: 'New_User_MobilePhoneArray1', format: 'numeric', previous: 'New_User_HomePhoneArray2' });

        $('#New_User_MobilePhoneArray1').autotab({ target: 'New_User_MobilePhoneArray2', format: 'numeric' });
        $('#New_User_MobilePhoneArray2').autotab({ target: 'New_User_MobilePhoneArray3', format: 'numeric', previous: 'New_User_MobilePhoneArray1' });
        $('#New_User_MobilePhoneArray3').autotab({ target: 'New_User_TitleType', format: 'numeric', previous: 'New_User_MobilePhoneArray2' });

        $("#New_User_OtherTitleType").attr("disabled", "disabled");
        $("#New_User_TitleType").change(function() {
            var otherType = $('#New_User_TitleType').val();
            if (otherType == "Other") {
                $("#New_User_OtherTitleType").attr("disabled", "");
                $("#New_User_OtherTitleType").focus();
            }
            else {
                $("#New_User_OtherTitleType").attr("disabled", "disabled");
                $("#New_User_OtherTitleType").val("");
            }
        });

        $("#New_User_OtherCredentials").attr("disabled", "disabled");
        $("#New_User_Credentials").change(function() {
            var otherType = $('#New_User_Credentials').val();
            if (otherType == "Other") {
                $("#New_User_OtherCredentials").attr("disabled", "");
                $("#New_User_OtherCredentials").focus();
            }
            else {
                $("#New_User_OtherCredentials").attr("disabled", "disabled");
                $("#New_User_OtherCredentials").val("");
            }
        });

        $('#New_User_AllPermissions').change(function() {
            if ($('#New_User_AllPermissions').attr('checked')) {
                $('input[name="PermissionsArray"]').each(function() {
                    if (!$(this).attr('checked')) {
                        $(this).attr('checked', true);
                    }
                });
            }
            else {
                $('input[name="PermissionsArray"]').each(function() {
                    if ($(this).attr('checked')) {
                        $(this).attr('checked', false);
                    }
                });
            }
        });

        U.postUrl("/LookUp/NewShortGuid", null, function(data) {
            $("#New_User_Password").val(data.text);
        });

        $("#New_User_GeneratePassword").click(function() {
            U.postUrl("/LookUp/NewShortGuid", null, function(data) {
                $("#New_User_Password").val(data.text);
            });
        });

        $("#newUserForm").validate({
            messages: {
                EmailAddress: "",
                Password: "",
                FirstName: "",
                LastName: ""
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            User.RebindList();
                            $.jGrowl("New user successfully added.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('newuser');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    InitEdit: function() {

        $('#Edit_User_HomePhoneArray1').autotab({ target: 'Edit_User_HomePhoneArray2', format: 'numeric' });
        $('#Edit_User_HomePhoneArray2').autotab({ target: 'Edit_User_HomePhoneArray3', format: 'numeric', previous: 'Edit_User_HomePhoneArray1' });
        $('#Edit_User_HomePhoneArray3').autotab({ target: 'Edit_User_MobilePhoneArray1', format: 'numeric', previous: 'Edit_User_HomePhoneArray2' });

        $('#Edit_User_MobilePhoneArray1').autotab({ target: 'Edit_User_MobilePhoneArray2', format: 'numeric' });
        $('#Edit_User_MobilePhoneArray2').autotab({ target: 'Edit_User_MobilePhoneArray3', format: 'numeric', previous: 'Edit_User_MobilePhoneArray1' });
        $('#Edit_User_MobilePhoneArray3').autotab({ target: 'Edit_User_SaveButton', format: 'numeric', previous: 'Edit_User_MobilePhoneArray2' });

        $("#editUserForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            User.RebindList();
                            $.jGrowl("User successfully updated.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('edituser');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitProfile: function() {
        $("#editProfileForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("Profile updated successfully.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('editprofile');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitForgotSignature: function() {
        $("#lnkRequestSignatureReset").click(function() {
            U.postUrl("/Signature/Email", null, function(result) {
                if (result.isSuccessful) {
                    U.showDialog("#resetSignature", function() {
                        $("#resetSignatureForm").validate({
                            submitHandler: function(form) {
                                var options = {
                                    dataType: 'json',
                                    beforeSubmit: function(values, form, options) {
                                    },
                                    success: function(result) {
                                        var resultObject = eval(result);
                                        if (resultObject.isSuccessful) {
                                            U.closeDialog();
                                            $("#resetSignatureMessage").removeClass("errormessage").html("Your signature has been reset").addClass("green");
                                        }
                                        else {
                                            alert(resultObject.errorMessage);
                                        }
                                    }
                                };
                                $(form).ajaxSubmit(options);
                                return false;
                            }
                        });
                    });
                }
                else {
                    $("#resetSignatureMessage").addClass("errormessage").html(result.errorMessage);
                }
            });
        });
    },
    InitMissedVisit: function() {
        $("#newMissedVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("Missed Visit successfully created.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('newmissedvisit');
                            User.RebindScheduleList();
                            U.closeDialog();
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    RebindList: function() {
        var grid = $('#List_User').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    },
    RebindScheduleList: function() {
        var grid = $('#List_User_Schedule').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    },
    Delete: function(userId) {
        if (confirm("Are you sure you want to delete this user?")) {
            var input = "userId=" + userId;
            U.postUrl("/User/Delete", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    Deactivate: function(userId) {
        if (confirm("Are you sure you want to deactivate this user?")) {
            var input = "userId=" + userId;
            U.postUrl("/User/Deactivate", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    }
}