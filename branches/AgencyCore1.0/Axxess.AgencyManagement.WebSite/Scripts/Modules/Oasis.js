﻿var Oasis = {
    Init: function() {
        $(".numeric").numeric();
        $(".tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
        $(".tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
        $.ajaxSetup({ type: "POST" });
        $(".diagnosis").autocomplete('/LookUp/DiagnosisCode', {
            max: 50,
            minChars: 2,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {
                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "DIAGNOSIS" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val(row.ShortDescription)
                $(this).parent().parent().find('.icd').val((row.FormatCode));
            }
        });

        $(".icd").autocomplete('/LookUp/DiagnosisCode', {
            max: 50,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {
                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "icd" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val((row.FormatCode))
                $(this).parent().parent().find('.diagnosis').val(row.ShortDescription);
            }
        });

        $(".procedureICD").autocomplete('/LookUp/ProcedureCode', {
            max: 50,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {
                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "PROCEDUREICD" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val((row.FormatCode))
                $(this).parent().parent().find('.procedureDiagnosis').val(row.ShortDescription);
            }
        });

        $(".procedureDiagnosis").autocomplete('/LookUp/ProcedureCode', {
            max: 50,
            minChars: 2,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "PROCEDURE" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val(row.ShortDescription)
                $(this).parent().parent().find('.procedureICD').val((row.FormatCode));
            }
        });

        $(".ICDM1024").autocomplete('/LookUp/DiagnosisCode', {
            max: 50,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "icd" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val((row.FormatCode))
                $(this).parent().find('.diagnosisM1024').val(row.ShortDescription);
            }
        });

        $(".diagnosisM1024").autocomplete('/LookUp/DiagnosisCode', {
            max: 50,
            minChars: 2,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Code + '-' + row.ShortDescription;
            },
            width: 400,
            extraParams: { "type": "DIAGNOSIS" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val(row.ShortDescription)
                $(this).parent().find('.ICDM1024').val((row.FormatCode));
            }
        });

    },
    stripDecimal: function(data) {
        var decimalPos = data.indexOf('.');
        var newData = '';
        for (i = 0; i < data.length; i++) {
            if (i != decimalPos && data.charAt(i) != ' ')
                newData = newData + data.charAt(i);
        }
        return newData;
    },
    GetPrimaryPhysician: function(id) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/Patient/GetPhysicianContact",
            data: "PhysicianContactId=" + id,
            success: function(result) {
                var resultObject = eval(result);
                $("#patientPhysicianName").text((resultObject.FirstName !== null ? resultObject.FirstName : "") + " " + (resultObject.LastName != null ? resultObject.LastName : ""));
                $("#patientPhysicianEmail").text(resultObject.Email !== null ? resultObject.Email : "");
                $("#patientPhysicianPhone").text(resultObject.PhoneWork !== null ? (resultObject.PhoneWork.substring(0, 3) + "-" + resultObject.PhoneWork.substring(3, 6) + "-" + resultObject.PhoneWork.substring(6)) : "");
            }
        });
    },
    Close: function(control) {
        control.closest('div.window').hide();
    },
    BlockAssessmentType: function() {
        $("div.assessmentType").block({ message: '',
            overlayCSS: {
                "backgroundColor": "transparent",
                "cursor": "default"
            }
        });
    },
    NextTab: function(id) {
        var $tabs = $(id).tabs();
        var selected = $tabs.data("selected.tabs");
        $tabs.tabs('select', selected + 1);
    },
    Refresh: function(id) {
        var $tabs = $(id).tabs();
        $tabs.tabs('select', 0);
        $($tabs.find(id + " .general")).each(function() { $(this).scrollTop(0) });
    },
    SaveClose: function(control) {
        var formId = control.closest("form").attr("id");
        $("#" + formId).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Oasis.Close(control);
                            alert("close");
                        }
                        else {
                            alert(resultObject.errorMessage);
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Delete: function(cont, id) {
        var row = cont.parents('tr:first');
        if (confirm("Are you sure you want to delete this Assessment?")) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Oasis/Delete",
                data: "id=" + id,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        $(row).remove();
                    }
                }
            });
        }
    },
    addTableRow: function(jQtable) {
        $(jQtable).each(function() {
            var $table = $(this);
            var tds = '<tr>';
            tds += '<td colspan="2"><input class="fill" value="" type="text" onfocus="Oasis.SupplyDescription($(this));" /> </td>';
            tds += '<td colspan="2"><input class="fill" value="" type="text" onfocus="Oasis.SupplyCode($(this));" class="suppliesCode"/> </td>';
            tds += '<td colspan="2"><input class="fill" value="" type="text" class="supplyQuantity"/></td>';
            tds += '<td class="align_center"><img src="/Images/forms/grey_left.png" /><input class="grey_button" type="button" onclick="Oasis.DeleteRow($(this));" value="Delete" /><img src="/Images/forms/grey_right.png" /></td>';
            tds += '</tr>';
            if ($('tbody', this).length > 0) {
                $('tbody', this).append(tds);
            } else {
                $(this).append(tds);
            }
        });
    },
    addToSupplyTable: function(data, table) {
        Oasis.ClearRows(table);
        var tableRow = "";
        for (var i = 0; i < data.length; i++) {
            var tds = '<tr>';
            tds += '<td class="icdtext">  <input  value="' + data[i].suppliesDescription + '" type="text" onfocus="Oasis.SupplyDescriptionOld($(this));"  class="suppliesDescription"/> </td>';
            tds += '<td><input  value="' + data[i].suppliesCode + '" type="text" onfocus="Oasis.SupplyCodeOld($(this));" class="suppliesCode"/> </td>';
            tds += '<td><input  value="' + data[i].supplyQuantity + '" type="text" class="supplyQuantity"/> </td>';
            tds += '<td><input type="button" onclick="Oasis.DeleteRow($(this));" value="Delete" /> </td>';
            tds += '</tr>';
            tableRow += tds;
        }
        if ($('tbody', table).length > 0) {
            $('tbody', table).append(tableRow);
        } else {
            $(table).append(tableRow);
        }
    },
    SupplyCodeOld: function(control) {
        $(control).autocomplete('/LookUp/Supplies', {
            max: 20,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {
                return row.Code + '-' + row.Description;
            },
            width: 400,
            extraParams: { "type": "Code" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val(Oasis.stripDecimal(row.Code))
                $(this).parent().parent().find('.suppliesDescription').val(row.Description);
            }
        });
    },
    SupplyDescriptionOld: function(control) {
        $(control).autocomplete('/LookUp/Supplies', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Code, result: data[i].Code };
                }
                return rows;
            },
            formatItem: function(row, i, n) {
                return row.Code + '-' + row.Description;
            },
            width: 500,
            extraParams: { "type": "Description" }
        }).result(function(event, row, formatted) {
            if (row) {
                $(this).val(row.Description)
                $(this).parent().parent().find('.suppliesCode').val(Oasis.stripDecimal(row.Code));
            }
        });
    },
    supplyInputFix: function(assessmentType, tableName) {
        var tableTr = $('tbody tr', $(tableName));
        var len = tableTr.length;
        var i = 1;
        var jsonData = '{ "Supply": [';
        $(tableTr).each(function() {
            jsonData += '{"suppliesDescription":"' + $(this).find('.suppliesDescription').val() + '","suppliesCode":"' + $(this).find('.suppliesCode').val() + '","supplyQuantity":"' + $(this).find('.supplyQuantity').val() + '"}';
            if (len > i) {
                jsonData += ',';
            }
            i++
        });
        jsonData += '] }';
        $("#" + assessmentType + "_GenericSupply").val(jsonData);
    },
    ClearRows: function(table) {
        $('tbody tr', table).each(function() {
            $(this).remove();
        });
    },
    DeleteRow: function(control) {
        if ($(control).closest('tbody').find('tr').length == 1) {
            $('.scheduleTables.purgable').each(function() { $(this).find('tbody').empty(); })
            Schedule.ShowScheduler();
        } else $(control).closest('tr').remove();
        Schedule.positionBottom();
    },
    BradenScaleScore: function(assessmentType) {
        var first = $("input[name=" + assessmentType + "_485BradenScaleSensory]:checked").val();
        var second = $("input[name=" + assessmentType + "_485BradenScaleMoisture]:checked").val();
        var third = $("input[name=" + assessmentType + "_485BradenScaleActivity]:checked").val();
        var fourth = $("input[name=" + assessmentType + "_485BradenScaleMobility]:checked").val();
        var fifth = $("input[name=" + assessmentType + "_485BradenScaleNutrition]:checked").val();
        var sixth = $("input[name=" + assessmentType + "_485BradenScaleFriction]:checked").val();
        var total = parseInt(first) + parseInt(second) + parseInt(third) + parseInt(fourth) + parseInt(fifth) + parseInt(sixth); //if you require floating point numbers try parseFloat
        $("#" + assessmentType + "_485BradenScaleTotal").val(total);
        Oasis.displayRisk(assessmentType, total);
    },
    displayRisk: function(assessmentType, total) {
        var resultText = '';
        if (total >= 19)
            resultText = 'Patients with this value are generally considered not at risk';
        else if (total >= 15 && total <= 18)
            resultText = 'At risk to develop pressure ulcers';
        else if (total >= 13 && total <= 14)
            resultText = 'Moderate risk for developing pressure ulcers';
        else if (total >= 10 && total <= 12)
            resultText = 'High risk for developing pressure ulcers';
        else if (total <= 9)
            resultText = 'Very high risk for developing pressure ulcers';
        $("#" + assessmentType + "_485ResultBox").html(resultText);
    },
    BradenScaleOnchange: function(assessmentType, tableId) {
        $('input:radio', $(tableId)).change(function() {
            Oasis.BradenScaleScore(assessmentType);
        })
    },
    CalculateNutritionScore: function(assessmentType) {
        $("input[name=" + assessmentType + "_GenericGoodNutritionScore][type=checkbox]").each(function() {
            $(this).attr('disabled', 'disabled');
        });
        var score = 0;
        $("input[name=" + assessmentType + "_GenericNutritionalHealth][type=checkbox]").each(function() {
            var itemScore = parseInt($(this).parents('td').next().text());
            if ($(this).attr('checked') && !isNaN(itemScore)) {
                score = score + itemScore;
            }
        });
        if (score <= 25) {
            $("input[name=" + assessmentType + "_GenericGoodNutritionScore][type=checkbox]").each(function() {
                $(this).removeAttr('checked');
            });
            $("input[name='" + assessmentType + "_GenericGoodNutritionScore'][type='checkbox'][value='1']").attr('checked', 'checked');
        }
        else if (score <= 55) {
            $("input[name=" + assessmentType + "_GenericGoodNutritionScore][type=checkbox]").each(function() {
                $(this).removeAttr('checked');
            });
            $("input[name='" + assessmentType + "_GenericGoodNutritionScore'][value='2']").attr('checked', 'checked');
        }
        else if (score <= 100) {
            $("input[name=" + assessmentType + "_GenericGoodNutritionScore][type=checkbox]").each(function() {
                $(this).removeAttr('checked');
            });
            $("input[name='" + assessmentType + "_GenericGoodNutritionScore'][value='3']").attr('checked', 'checked');
        }
    },
    Validate: function(id, patientId, episodeId, assessmentType) {
        acore.open("validation", 'Oasis/Validate', function() {
            Patient.InitNewOrder();
        }, { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType });

    },
    ToolTip: function(mooCode) {
        U.postUrl("/Oasis/Guide", 'mooCode=' + mooCode, function(data) {
            $('.tooltipbox').each(function() { $(this).remove() });
            $('#desktop').append(unescape("%3Cdiv class=%22tooltipbox%22%3E%3Cdiv class=%22closer%22 onclick=%22$(this).parent().remove();%22%3EX%3C/div%3E%3Ch3%3EItem Intent%3C/h3%3E%3Cdiv class=%22content intent%22%3E" + data.ItemIntent + "%3C/div%3E%3Ch3%3EResponse%3C/h3%3E%3Cdiv class=%22content response%22%3E" + data.Response + "%3C/div%3E%3Ch3%3EData Sources%3C/h3%3E%3Cdiv class=%22content sources%22%3E" + data.DataSources + "%3C/div%3E%3C/div%3E"));
        });
    },
    OasisStatusAction: function(id, patientId, episodeId, assessmentType, actionType, pageName) {
        var reason = "";
        if (actionType == "Return") {
            if ($("#printreturnreason").is(":hidden")) {
                $("#printcontrols li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very_hidden");
                $("#printreturnreason").slideDown('slow');
            } else {
                reason = $("#printreturnreason textarea").val();
                U.postUrl('Oasis/Submit', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, actionType: actionType, reason: reason }, function(data) {
                    if (data.isSuccessful) {
                        acore.closeprintview();
                        U.closeDialog();
                        $.jGrowl(data.errorMessage, { theme: 'success', life: 5000 });
                        Patient.Rebind();
                        Schedule.Rebind();
                        Oasis.RebindToExport();
                        Oasis.RebindExported();
                        Agency.RebindCaseManagement();
                        UserInterface.CloseOasisValidationModal();
                        UserInterface.CloseWindow(pageName);
                    } else { $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 }); }
                });
            }
        }
        else {

            U.postUrl('Oasis/Submit', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, actionType: actionType, reason: reason }, function(data) {
                if (data.isSuccessful) {
                    acore.closeprintview();
                    U.closeDialog();
                    $.jGrowl(data.errorMessage, { theme: 'success', life: 5000 });
                    Patient.Rebind();
                    Schedule.Rebind();
                    Oasis.RebindToExport();
                    Oasis.RebindExported();
                    Agency.RebindCaseManagement();
                    UserInterface.CloseOasisValidationModal();
                    UserInterface.CloseWindow(pageName);
                } else { $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 }); }
            });
        }
    },
    MarkAsExpotred: function(control) {
        var check = false;
        $(control).find("input[name=OasisSelected]").each(function() { if ($(this).is(":checked")) { check = true; } })
        if (check) {
            var fields = $(":input", $(control)).serializeArray();
            U.postUrl('Oasis/MarkExported', fields, function(data) {

                if (data.isSuccessful) {
                    $.jGrowl(data.errorMessage, { theme: 'success', life: 5000 });
                    Patient.Rebind();
                    Schedule.Rebind();
                    Oasis.RebindToExport();
                    Oasis.RebindExported();
                    Agency.RebindCaseManagement();
                    //UserInterface.CloseWindow('validation');
                } else {
                    $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 });
                }
            });
        } else {
        $.jGrowl("Select an assessment to mark as generated.", { theme: 'error', life: 5000 });
        }

    },
    Reopen: function(id, patientId, episodeId, assessmentType, actionType) {
        U.showDialog("#oasisReopenDialog", function() {
            $('#reopenOasisYes').bind('click', function() {
                var reason = $("#Oasis_OasisReopenReason").val();
                U.postUrl('Oasis/Submit', { Id: id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType, actionType: actionType, reason: reason }, function(data) {
                    if (data.isSuccessful) {
                        $.jGrowl(data.errorMessage, { theme: 'success', life: 5000 });
                        Patient.Rebind();
                        Schedule.Rebind();
                        Oasis.RebindToExport();
                        Oasis.RebindExported();
                        Agency.RebindCaseManagement();
                        U.closeDialog();
                    } else {
                        $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 });
                    }
                });
            });
        });
    },
    RebindToExport: function() {
        var grid = $('#generateOasisGrid').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    }
     ,
    RebindExported: function() {
        var grid = $('#exportedOasisGrid').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    },
    AddSupply: function(control, episodeId, patientId, eventId, assessmentType, supplyId) {
        var quantity = $(control).closest('tr').find('.quantity').val();
        var date = $(control).closest('tr').find('.date').val();
        var input = "episodeId=" + episodeId + "&patientId=" + patientId + "&eventId=" + eventId + "&assessmentType=" + assessmentType + "&supplyId=" + supplyId + "&quantity=" + quantity + "&date=" + date;
        U.postUrl("/Oasis/AddSupply", input, function(result) {
            var gridfilter = $("#" + assessmentType + "_SupplyFilterGrid").data('tGrid');
            if (gridfilter != null) {
                $("#" + assessmentType + "_GenericSupplyDescription").val('');
                $("#" + assessmentType + "_GenericSupplyCode").val('');
                gridfilter.rebind({ q: "", limit: 0, type: "" });
            }
            var grid = $("#" + assessmentType + "_SupplyGrid").data('tGrid');
            if (grid != null) {
                grid.rebind({ episodeId: episodeId, patientId: patientId, eventId: eventId, assessmentType: assessmentType });
            }

        });
    },
    SupplyDescription: function(type) {
        $("#" + type + "_GenericSupplyDescription").keyup(function(event) {
            var message = $("#" + type + "_GenericSupplyDescription").val().length;
            var grid = $("#" + type + "_SupplyFilterGrid").data('tGrid');
            if (grid != null) {
                grid.rebind({ q: $("#" + type + "_GenericSupplyDescription").val(), limit: 20, type: "Description" });
            }
            if (event.keyCode == 13) {
                return false;
            }
            else {
                return true;
            }
        });
    },
    SupplyCode: function(type) {
        $("#" + type + "_GenericSupplyCode").keyup(function(event) {
            var grid = $("#" + type + "_SupplyFilterGrid").data('tGrid');
            if (grid != null) {
                grid.rebind({ q: $("#" + type + "_GenericSupplyCode").val(), limit: 20, type: "Code" });
            }
            if (event.keyCode == 13) {
                return false;
            }
            else {
                return true;
            }
        });
    },
    blockText: function(control, id) {
        if ($(control).is(':checked')) {
            $(id).attr("disabled", "disabled");
            $(id).val("");
        }
        else {
            $(id).attr("disabled", "");
        }
    },
    unBlockText: function(id) {
        $(id).attr("disabled", "");
    },
    LoadBlankMasterCalendar: function(id, episodeId, patientId) {
        $("#" + id + "_BlankMasterCalendar").load('Oasis/BlankMasterCalendar', { episodeId: episodeId, patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $("#" + id + "_BlankMasterCalendar").html('<p>There was an error making the AJAX request</p>');
                $("#" + id + "_Show").hide();
                $("#" + id + "_Hide").show();
            }
            else if (textStatus == "success") {
                $("#" + id + "_BlankMasterCalendar").removeClass("loading");
                $("#" + id + "_Show").hide();
                $("#" + id + "_Hide").show();
            }
        });
    },
    HideBlankMasterCalendar: function(id) {
        $("#" + id + "_BlankMasterCalendar").empty();
        $("#" + id + "_Show").show();
        $("#" + id + "_Hide").hide();
    },
    showIfChecked: function(checkbox, field) {
        if (checkbox.is(":checked")) field.show().removeClass('form-omitted');
        else field.hide().addClass('form-omitted');
        checkbox.bind('click', function() { field.toggleClass('form-omitted').toggle(); });
    },
    hideIfChecked: function(checkbox, field) {
        if (checkbox.is(":checked")) field.hide().addClass('form-omitted');
        else field.show().removeClass('form-omitted');
        checkbox.bind('click', function() { field.toggleClass('form-omitted').toggle(); });
    },
    showIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.show().removeClass('form-omitted');
        else field.hide().addClass('form-omitted');
        $(':radio[name=' + group + ']').bind('click', function() { if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.show().removeClass('form-omitted'); else field.hide().addClass('form-omitted'); });
    },
    hideIfRadioEquals: function(group, value, field) {
        value = value.split("|");
        if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.hide().addClass('form-omitted');
        else field.show().removeClass('form-omitted');
        $(':radio[name=' + group + ']').bind('click', function() { if ($.inArray($(':radio[name=' + group + ']:checked').val(), value) >= 0) field.hide().addClass('form-omitted'); else field.show().removeClass('form-omitted'); });
    },
    showIfSelectEquals: function(select, value, field) {
        if (select.val() == value) field.show().removeClass('form-omitted');
        else field.hide().addClass('form-omitted');
        select.bind('change', function() { field.toggleClass('form-omitted').toggle(); });
    },
    noneOfTheAbove: function(checkbox, group) {
        if (checkbox.is(":checked")) group.each(function() { if ($(this).attr('id') != checkbox.attr('id') && $(this).is(":checked")) $(this).click(); });
        group.bind('change', function() { if ($(this).attr('id') != checkbox.attr('id') && $(this).is(":checked") && checkbox.is(":checked")) checkbox.click(); });
        checkbox.bind('change', function() { group.each(function() { if ($(this).attr('id') != checkbox.attr('id') && $(this).is(":checked")) $(this).click(); }); });
    },
    goals: function(fieldset) {
        fieldset.find("input[type=checkbox]").each(function() {
            if ($(this).parent().find("span.radio").length) {
                if ($(this).is(":not(:checked)")) $(this).parent().find("span.radio :input").attr("disabled", true);
                $(this).change(function() {
                    if ($(this).is(":checked")) $(this).parent().find("span.radio :input").attr("disabled", false);
                    else $(this).parent().find("span.radio :input").attr("disabled", true);
                });
            }
        });
    },
    interventions: function(fieldset) {
        Oasis.goals(fieldset);
    }
}