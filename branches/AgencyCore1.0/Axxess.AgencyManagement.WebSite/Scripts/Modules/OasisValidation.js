﻿var OasisValidation = {
    _EpisodeId: "",
    GetEpisodeId: function() {
        return OasisValidation._EpisodeId;
    },
    SetEpisodeId: function(EpisodeId) {
        OasisValidation._EpisodeId = EpisodeId;
    },
    Validate: function(id, assessmentType) {
        $('#validationResult').load('Oasis/Validate', { Id: id, episodeId: OasisValidation._EpisodeId, assessmentType: assessmentType }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#validationResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#validation');
            }
            else if (textStatus == "success") {
                JQD.open_window('#validation');
            }
        }
);
    }
}