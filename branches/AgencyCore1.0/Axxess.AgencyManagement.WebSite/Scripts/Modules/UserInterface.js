﻿var UserInterface = {
    ShowMessages: function() {
        acore.open("messageinbox", 'Message/Inbox', function() { Message.Init(); });
    },
    ShowNewMessage: function() {
        acore.open("newmessage", 'Message/New', function() { Message.InitNew(); });
    },
    ShowUserTasks: function() {
        acore.open("listuserschedule", 'User/Schedule', function() { });
    },
    ShowPastDueRecerts: function() {
        acore.open("listpastduerecerts", 'Agency/RecertsPastDueGrid', function() { });
    },
    ShowUpcomingRecerts: function() {
        acore.open("listupcomingrecerts", 'Agency/RecertsUpcomingGrid', function() { });
    },
    ShowPatientCenter: function() {
        acore.open("patientcenter", 'Patient/Center', function() { Patient.InitCenter(); });
    },
    ShowNewPatient: function() {
        acore.open("newpatient", 'Patient/New', function() { Patient.InitNew(); });
    },
    ShowEditPatient: function(Id) {
        acore.open("editpatient", 'Patient/EditPatientContent', function() { Patient.InitEdit(); }, { patientId: Id });
    },
    ShowNewReferral: function() {
        acore.open("newreferral", 'Referral/New', function() { Referral.InitNew(); });
    },
    ShowEditReferral: function(Id) {
        acore.open("editreferral", 'Referral/Edit', function() { Referral.InitEdit(); }, { Id: Id });
    },
    ShowNewContact: function() {
        acore.open("newcontact", 'Contact/New', function() { Contact.InitNew(); });
    },
    ShowEditContact: function(Id) {
        acore.open("editcontact", "Contact/Edit", function() { Contact.InitEdit(); }, { Id: Id });
    },
    ShowNewLocation: function() {
        acore.open("newlocation", 'Location/New', function() { Location.InitNew(); });
    },
    ShowEditLocation: function(Id) {
        acore.open("editlocation", 'Location/Edit', function() { Location.InitEdit(); }, { Id: Id });
    },
    ShowNewPhysician: function() {
        acore.open("newphysician", 'Physician/New', function() { Physician.InitNew(); });
    },
    ShowEditPhysician: function(Id) {
        acore.open("editphysician", 'Physician/Edit', function() { Physician.InitEdit(); }, { Id: Id });
    },
    ShowNewHospital: function() {
        acore.open("newhospital", "Hospital/New", function() { Hospital.InitNew(); });
    },
    ShowEditHospital: function(Id) {
        acore.open("edithospital", "Hospital/Edit", function() { Hospital.InitEdit(); }, { Id: Id });
    },
    ShowNewInsurance: function() {
        acore.open("newinsurance", "Insurance/New", function() { Insurance.InitNew(); });
    },
    ShowEditInsurance: function(Id) {
        acore.open("editinsurance", "Insurance/Edit", function() { Insurance.InitEdit(); }, { Id: Id });
    },
    ShowRap: function(episodeId, patientId) {
        acore.open("rap", "Billing/Rap", function() { Billing.InitRap(); }, { episodeId: episodeId, patientId: patientId });
    },
    ShowFinal: function(episodeId, patientId) {
        acore.open("final", "Billing/Final", function() { }, { episodeId: episodeId, patientId: patientId });
    },
    ShowEditPlanofCare: function(episodeId, patientId, eventId) {
        acore.open("editplanofcare", 'Oasis/Edit485', function() { PlanOfCare.InitEdit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    },
    ShowNewUser: function() {
        acore.open("newuser", "User/New", function() { User.InitNew(); });
    },
    ShowEditUser: function(Id) {
        acore.open("edituser", "User/Edit", function() { User.InitEdit(); }, { Id: Id });
    },
    ShowEditOrder: function(id, patientId) {
        acore.open("editorder", "Order/Edit", function() { Patient.InitEditOrder(); }, { id: id, patientId: patientId });
    },
    ShowReportCenter: function() {
        acore.open("reportcenter", 'Report/Center', function() { Report.Init(); });
    },
    ShowBillingCenter: function() {
        acore.open("billingcenter", "Billing/Center", function() { Billing.InitCenter(); });
    },
    ShowPatientPrompt: function() {
        U.showDialog("#New_Patient_NextSteps");
    },
    ShowMissedVisitModal: function(episodeId, patientId, eventId) {
        $("#Missed_Visit_Container").load("/Visit/Miss", { episodeId: episodeId, patientId: patientId, eventId: eventId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
            }
            else if (textStatus == "success") {
                U.showDialog("#Missed_Visit_Container", function() { User.InitMissedVisit(); });
            }
        });
    },
    ShowNewPhysicianModal: function() {
        $("#New_Physician_Container").load("/Physician/New", function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
            }
            else if (textStatus == "success") {
                U.showDialog("#New_Physician_Container", function() { Physician.InitNew(); });
            }
        });
    },
    ShowNewPhotoModal: function(patientId) {
        $("#New_Photo_Container").load("/Patient/NewPhoto", { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
            }
            else if (textStatus == "success") {
                U.showDialog("#New_Photo_Container", function() { Patient.InitNewPhoto(); });
            }
        });
    },
    ShowAdmitPatientModal: function(Id) {
        $("#Admit_Patient_Container").load("Patient/NewAdmit", { patientId: Id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
            }
            else if (textStatus == "success") {
                U.showDialog("#Admit_Patient_Container", function() { Patient.InitAdmit(); });
            }
        });
    },
    ShowNonAdmitPatientModal: function(Id) {
        $("#NonAdmit_Patient_Container").load("Patient/NewNonAdmit", { patientId: Id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
            }
            else if (textStatus == "success") {
                U.showDialog("#NonAdmit_Patient_Container", function() { Patient.InitNonAdmit(); });
            }
        });
    },
    ShowMedicationModal: function(episodeId, patientId, eventId) {
        $("#Edit_PlanofCareMed_Container").load("485/Medication", { episodeId: episodeId, patientId: patientId, eventId: eventId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
            } else if (textStatus == "success") {
                U.showDialog("#Edit_PlanofCareMed_Container", function() { });
                $('.blockUI.blockMsg.blockPage').css({ 'height': '286px', 'top': '50%', 'margin-top': '-143px' });
                $('#485MedicationGrid .t-grid-content').css({ 'top': '60px' });
            }
        });
    },
    ShowOasisValidationModal: function(contentId, Id, patientId, episodeId, assessmentType) {
        $('body').append(unescape("%3Cdiv id=%22shade%22%3E%3C/div%3E%3Cdiv id=%22oasis_validation%22 class=%22content loading%22%3E%3C/div%3E%3Cdiv id=%22oasis_validation_controls%22 class=%22buttons%22%3E%3Cul%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.CloseOasisValidationModal();%22%3EReturn To OASIS%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E"));
        $("#oasis_validation").load('Oasis/Validate', { Id: Id, patientId: patientId, episodeId: episodeId, assessmentType: assessmentType }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
            } else if (textStatus == "success") {
                $("#oasis_validation").removeClass("loading");
            }
        });
    },
    Refresh: function() {
        Patient.Rebind();
        Schedule.Rebind();
    },
    CloseOasisValidationModal: function() {
        $('#shade').remove();
        $('#oasis_validation').remove();
        $('#oasis_validation_controls').remove();
    },
    CloseWindow: function(window) {
        acore.close(window);
    }
}
