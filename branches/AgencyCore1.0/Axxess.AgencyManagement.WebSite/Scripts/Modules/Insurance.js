﻿var Insurance = {
    InitNew: function() {
        $("#newInsuranceForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Insurance.RebindList();
                            Lookup.loadInsurance();
                            $.jGrowl("New Insurance successfully added.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('newinsurance');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEdit: function() {
        $("#editInsuranceForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            Insurance.RebindList();
                            Lookup.loadInsurance();
                            $.jGrowl("Insurance successfully updated.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('editinsurance');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Delete: function(Id) {
        if (confirm("Are you sure you want to delete this insurance?")) {
            var input = "Id=" + Id;
            U.postUrl("/Insurance/Delete", { Id: Id }, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    if (resultObject.isSuccessful) {
                        Insurance.RebindList();
                        Lookup.loadInsurance();
                        $.jGrowl("Insurance successfully deleted.", { theme: 'success', life: 5000 });

                    } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                }
            });
        }
    },
    RebindList: function() {
        var grid = $('#List_AgencyInsurance').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    }
}