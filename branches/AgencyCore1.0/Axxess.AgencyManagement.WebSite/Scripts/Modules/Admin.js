﻿var Admin = {
    Init: function() {
        $("#impersonateAgencyForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            window.location.replace("/");
                        }
                        else {
                            alert(resultObject.errorMessage);
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}