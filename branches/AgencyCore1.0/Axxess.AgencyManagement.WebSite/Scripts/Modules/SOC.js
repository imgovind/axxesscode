﻿var SOC = {
    _SOCId: "",
    _EpisodeId: "",
    _patientId: "",
    GetId: function() {
        return SOC._patientId;
    },
    SetId: function(patientId) {
        SOC._patientId = patientId;
    },
    GetSOCId: function() {
        return SOC._SOCId;
    },
    SetSOCId: function(SOCId) {
        SOC._SOCId = SOCId;
    },
    GetSOCEpisodeId: function() {
        return SOC._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        SOC._EpisodeId = EpisodeId;
    },
    InitNew: function() {
        Oasis.BradenScaleOnchange('StartOfCare', '#socBradenScale');

        $("input[name=StartOfCare_GenericNutritionalHealth]").click(function() {
            Oasis.CalculateNutritionScore('StartOfCare');
        });
    },
    HandlerHelper: function(form, control, action) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    var category = $('[name=categoryType]').val();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#socTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('startofcare');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save &amp; Check for Errors") {
                        action();
                    }
                    else if (actionType == "Check for Errors") {
                        action();
                    }
                }
                else {
                    $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                }
            },
            error: function(result) {
                if ($.trim(result.responseText) == 'Success') {
                    var actionType = control.html();
                    var category = $('[name=categoryType]').val();
                    if (actionType == "Save &amp; Continue") {
                        Oasis.NextTab("#socTabs.tabs");
                    }
                    else if (actionType == "Save &amp; Exit") {
                        UserInterface.CloseWindow('startofcare');
                        Schedule.Rebind();
                    }
                    else if (actionType == "Save &amp; Check for Errors") {
                        action();
                    }
                    else if (actionType == "Check for Errors") {
                        action();
                    }
                }
                else {
                    $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    FormSubmit: function(control, action) {
        var form = control.closest("form");
        form.find('.form-omitted :input').val("").end().find('.form-omitted:input').val("");
        form.validate();
        SOC.HandlerHelper(form, control, action);
    },
    Validate: function(id) {
        OasisValidation.Validate(id, "StartOfCare");
    },
    loadSoc: function(id, patientId, assessmentType) {
        acore.open("startofcare", 'Oasis/Soc', function() {
            $("#socTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
            $("#socTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
            SOC.InitNew(); Oasis.Init();
            $("#socTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                SOC.loadSOCParts(event, ui);
            });
        }, { Id: id, PatientId: patientId, AssessmentType: assessmentType });
    },
    loadSOCParts: function(event, ui) {
        $($(ui.tab).attr('href')).empty();
        $($(ui.tab).attr('href')).addClass("loading");
        $($(ui.tab).attr('href')).load('Oasis/SocCategory', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');
            }
            else if (textStatus == "success") {
                $($(ui.tab).attr('href')).removeClass("loading");
                Oasis.CalculateNutritionScore('StartOfCare');
                SOC.InitNew();
                Oasis.Init();
            }
        });
    },
    onFrquencyBinding: function() {
        $(control).autocomplete(["Before Meals( AC )", "Product 2", "After meals(PC)", "Every day (QD)", "Twice/day (BID)", "Three times/dau (TID)", "Four times /day(QID)", "Every other day (QOD)", "PRN"], {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {
                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i] };
                }
                return rows;
            },
            formatItem: function(row, i, n) {
                return row;
            },
            width: 400
        }).result(function(event, row, formatted) {
            if (row) {
                $(control).val(row);
            }
        });
    },
    BindGridButton: function() {
        var newButton = $('#OasisMedicatonGridInsertButton');
        href = "javascript:void(0);";
        newButton.attr('href', href);
    },
    onEdit: function(e) {
        $(e.form).find('#MedicationType').data('tDropDownList').select(function(dataItem) {
            return dataItem.Value == e.dataItem['MedicationType'].Value;
        });
        $(e.form).find("tr td").each(function() {
            if ($(this).closest('td').prevAll().length == 7) {
                if (e.dataItem['MedicationCategory'] != 'DC') {
                    $(this).html('');
                }
            }
        });
    },
    Discharge: function(assessmentId, PatientId, assessmentType, Id) {
        U.showDialog("#dischargeMedicationDialog", function() {
            $("#dischargeMedicationForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        data: { AssessmentId: assessmentId, PatientId: PatientId, AssessmentType: assessmentType, Id: Id },
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            var resultObject = eval(result);
                            if (resultObject.isSuccessful) {
                                U.closeDialog();
                                var patientGrid = $('#OasisMedicatonGrid').data('tGrid');
                                patientGrid.rebind({ AssessmentId: assessmentId, PatientId: PatientId, AssessmentType: "StartOfCare" });
                                $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });

                            } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });
        });
    },
    DeleteSOCAsset: function(control, assessmentType, name, assetId) {
        if (confirm("Are you sure you want to delete this asset?")) {
            var input = "episodeId=" + $("#" + assessmentType + "_EpisodeId").val() + "&patientId=" + $("#" + assessmentType + "_PatientGuid").val() + "&eventId=" + $("#" + assessmentType + "_Id").val() + "&assessmentType=" + assessmentType + "&name=" + name + "&assetId=" + assetId;
            U.postUrl("/Oasis/DeleteWoundCareAsset", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    $(control).closest('td').empty().append("<input type=\"file\" name=\"" + assessmentType + "_" + name + "\" value=\"Upload\" size=\"1\" class = \"float_left uploadWidth\" />");
                }
            });
        }
    },
    OnRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem.MedicationCategory == "DC") {
            $(e.row).removeClass('t-alt ').addClass('red');
            e.row.cells[7].innerHTML = dataItem.DCDateFormated;
        }
        else {
            e.row.cells[7].innerHTML = '<a class="t-grid-action t-button t-state-default" href="javascript:void(0);" onclick="' + dataItem.DischargeUrl + '">D/C</a>';
        }

    }
}





 
  
