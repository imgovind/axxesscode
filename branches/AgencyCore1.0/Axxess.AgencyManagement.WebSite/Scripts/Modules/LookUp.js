﻿var Lookup = {
    _Users: "",
    _Discipline: new Array(),
    SetDiscipline: function(name, discipline) {
        Lookup._Discipline[name] = discipline;
    },
    loadStates: function() {
        U.postUrl("/LookUp/States", null, function(data) {
            $("select.states").addStates(data);
        });

        $.fn.addStates = function(data) {
            return this.each(function() {
                var selectList = this;
                $.each(data, function(index, itemData) {
                    var option = new Option(itemData.Name, itemData.Code, false, false);
                    selectList[selectList.options.length] = option;
                });
            });
        };
    },
    loadAdmissionSources: function() {
        U.postUrl("/LookUp/AdmissionSources", null, function(data) {
            $("select.AdmissionSource").addAdmissionSource(data);
        });

        $.fn.addAdmissionSource = function(data) {
            return this.each(function() {
                var selectList = this;
                $.each(data, function(index, itemData) {
                    var option = new Option("(" + itemData.Code + ") - " + itemData.Description, itemData.Code, false, false);
                    selectList[selectList.options.length] = option;
                });
            });
        };
    },
    loadRaces: function() {
        U.postUrl("/LookUp/Races", null, function(data) {
            $("select.EthnicRaces").addRaces(data);
            $("select.EthnicRaces").dropdownchecklist({ width: 162, maxDropHeight: 190 });
        });

        $.fn.addRaces = function(data) {
            return this.each(function() {
                var selectList = this;
                $.each(data, function(index, itemData) {
                    var option = new Option(itemData.Description, itemData.Id, false, false);
                    selectList[selectList.options.length] = option;
                });
            });
        };
    },
    loadReferralSources: function() {
        U.postUrl("/LookUp/ReferralSources", null, function(data) {
            $("select.ReferralSources").addReferralSources(data);
        });

        $.fn.addReferralSources = function(data) {
            return this.each(function() {
                var selectList = this;
                $.each(data, function(index, itemData) {
                    var option = new Option(itemData.Description, itemData.Id, false, false);
                    selectList[selectList.options.length] = option;
                });
            });
        };
    },
    loadPhysicians: function() {
        U.postUrl("/Agency/GetPhysicians", null, function(data) {
            $("select.Physicians").addPhysicians(data);
        });
        $.fn.addPhysicians = function(data) {
            return this.each(function() {
                var selectList = this;
                $(this).children('option').remove();
                var blank = new Option("-- Select Physician --", "00000000-0000-0000-0000-000000000000", false, false);
                selectList[0] = blank;
                var addNew = new Option("** Add New Physician **", "new", false, false);
                selectList[1] = addNew;
                $.each(data, function(index, itemData) {
                    var option = new Option(itemData.DisplayName, itemData.Id, false, false);
                    selectList[selectList.options.length] = option;
                });
            });
        };
    },
    loadUsers: function() {
        U.postUrl("/User/All", null, function(data) {
            $("select.Users").addUsers(data);
        });
        $.fn.addUsers = function(data) {
            return this.each(function() {
                var selectList = this;
                $.each(data, function(index, itemData) {
                    var option = new Option(itemData.FirstName + " " + itemData.LastName, itemData.Id, false, false);
                    selectList[selectList.options.length] = option;
                });
            });
        };
    },
    loadUsersPerControl: function() {
        U.postUrl("/User/All", null, function(data) {
            var control = '';
            $.each(data, function(index, itemData) {
                control += '<option value="' + itemData.Id + '">' + itemData.FirstName + " " + itemData.LastName + '</option>';
            });
            Lookup._Users = control;
        });
    },
    loadMultipleDisciplines: function(control) {
        U.postUrl("/LookUp/MultipleDisciplineTasks", null, function(data) {
            $(control).find("select.MultipleDisciplineTask").addMultipleDisciplines(data);
        });
        $.fn.addMultipleDisciplines = function(data) {
            var control = '';
            $.each(data, function(index, itemData) {
                control += '<option data="' + itemData.Discipline + '" value="' + itemData.Id + '" IsBillable="' + itemData.IsBillable + '">' + itemData.Task + '</option>';
            });
            this.append(control);
        };
    },
    loadDisciplines: function(control, input) {
        U.postUrl("/LookUp/DisciplineTasks", "Discipline=" + input, function(data) {
            $(control).find("select.DisciplineTask").addDisciplines(data);
        });
        $.fn.addDisciplines = function(data) {
            var control = '';
            $.each(data, function(index, itemData) {
                control += '<option data="' + itemData.Discipline + '" value="' + itemData.Id + '" IsBillable="' + itemData.IsBillable + '">' + itemData.Task + '</option>';
            });
            this.append(control);
        };
    },
    loadDisciplinePerControl: function(input) {
        var control = '';
        U.postUrl("/LookUp/DisciplineTasks", input, function(data) {
            $.each(data, function(index, itemData) {
                control += '<option data="' + itemData.Discipline + '" value="' + itemData.Id + '" IsBillable="' + itemData.IsBillable + '">' + itemData.Task + '</option>';
            });
            Lookup.SetDiscipline(input, control);
        });
    },
    loadInsurance: function() {
        U.postUrl("/Agency/GetInsurances", null, function(data) {
            $("select.Insurances").addInsurances(data);
        });
        $.fn.addInsurances = function(data) {
            return this.each(function() {
                var selectList = this;
                $(this).children('option').remove();
                var blank = new Option("-- Select Insurance --", "00000000-0000-0000-0000-000000000000", false, false);
                selectList[0] = blank;
                var addNew = new Option("** Add New Insurance **", "new", false, false);
                selectList[1] = addNew;
                $.each(data, function(index, itemData) {
                    var option = new Option(itemData.Name, itemData.Id, false, false);
                    selectList[selectList.options.length] = option;
                });
            });
        };
    }
}