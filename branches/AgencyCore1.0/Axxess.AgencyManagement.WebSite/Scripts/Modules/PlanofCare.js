﻿var PlanOfCare = {
    InitEdit: function() {
        $("#edit485Form").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('editplanofcare');
                            Patient.Rebind();
                            Schedule.Rebind();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    SaveMedications: function(patientId) {
        U.postUrl("/Patient/LastestMedications", { patientId: patientId }, function(data) {
            $("#485Medications").val(data);
        });

        U.closeDialog();
    }
}