using System;
using System.IO;
using System.Net.Mail;
using System.Threading;

    public static class EmailSender
    {
        private static readonly string templateDirectory;
        
        static EmailSender()
        {
            templateDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates");
        }

        private static string PrepareMailBodyWith(string templateName, params string[] pairs)
        {
            string body = GetMailBodyOfTemplate(templateName);

            for (var i = 0; i < pairs.Length; i += 2)
            {
                body = body.Replace(string.Format("<%={0}%>", pairs[i]), pairs[i + 1]);
            }

            return body;
        }

        private static string GetMailBodyOfTemplate(string templateName)
        {
            return ReadFileFrom(templateName);
        }

        private static string ReadFileFrom(string templateName)
        {
            string filePath = string.Concat(Path.Combine(templateDirectory, templateName), ".txt");
            return File.ReadAllText(filePath);
        }

        public static void SendMail(string fromAddress, string toAddress, string subject, string body)
        {
            using (MailMessage mail = BuildMessageWith(fromAddress, toAddress, subject, body))
            {
                SendMail(mail);
            }
        }

        private static void SendMail(MailMessage mail)
        {
            try
            {
                SmtpClient smtp = new SmtpClient
                {
                    Host = "mail.axxessweb.com",
                    Credentials = new System.Net.NetworkCredential(
                        "webmaster@axxessweb.com", "@gencyC0re")
                };

                smtp.Send(mail);
            }
            catch (Exception)
            {
            }
        }

        private static void SendMailAsync(string fromAddress, string toAddress, string subject, string body)
        {
            ThreadPool.QueueUserWorkItem(state => SendMail(fromAddress, toAddress, subject, body));
        }

        private static MailMessage BuildMessageWith(string fromAddress, string toAddress, string subject, string body)
        {
            MailMessage message = new MailMessage
            {
                Sender = new MailAddress("webmaster@axxessweb.com","Birhanu Home health"),
                From = new MailAddress(fromAddress),
                Subject = subject,
                Body = body,
                IsBodyHtml = true,
                
            };

            string[] tos = toAddress.Split(new char[1] {';'}, StringSplitOptions.RemoveEmptyEntries);

            foreach (string to in tos)
            {
                message.To.Add(new MailAddress(to));
            }

            return message;
        }
    }
