﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;

    using Web;
    using Enums;
    using ViewData;
    using Services;
    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.OasisC.Domain;
    using Axxess.LookUp.Domain;

    //
    [Authorize]
    [HandleError]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ScheduleController : BaseController
    {
        #region Constructor

        private readonly IDateService dateService;
        private readonly IPatientService patientService;
        private readonly IAssessmentService assessmentService;
        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;

        public ScheduleController(IAgencyManagementDataProvider dataProvider, IPatientService patientService, IAssessmentService assessmentService, IUserService userService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            Check.Argument.IsNotNull(patientService, "patientService");

            this.patientService = patientService;
            this.dateService = Container.Resolve<IDateService>();
            this.userRepository = dataProvider.UserRepository;
            this.assetRepository = dataProvider.AssetRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.assessmentService = assessmentService;
            this.userService = userService;
        }

        #endregion

        #region ScheduleController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessScheduleCenter)]
        public ActionResult Center()
        {
            var viewData = new ScheduleViewData();
            if (Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                viewData.Patients = patientRepository.GetAllByAgencyId(Current.AgencyId).Where(p => p.Status == 1).ToList().ForSelection();
            }
            else if (Current.IsClinicianOrHHA)
            {
                viewData.Patients = userRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active).ForSelection();
            }
            if (viewData.Patients != null && viewData.Patients.Count > 0)
            {
                viewData.Episode = patientRepository.GetEpisode(Current.AgencyId, viewData.Patients.FirstOrDefault().Id, DateTime.Now, "Nursing");
            }

            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessScheduleCenter)]
        public ActionResult Data(Guid patientId)
        {
            var viewData = new ScheduleViewData();
            viewData.Episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, "Nursing");
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessScheduleCenter)]
        public ActionResult Activities(Guid patientId, string discipline)
        {
            return PartialView(patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessScheduleCenter)]
        public ActionResult Calendar(Guid patientId, string discipline)
        {
            var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline);
            return PartialView(episode);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.EditTaskDetails)]
        public ActionResult GetDetails(Guid episodeId, Guid patientId, Guid eventId)
        {
            if (patientId.IsEmpty() || episodeId.IsEmpty() || eventId.IsEmpty())
            {
                return PartialView("Details", new ScheduleEvent());
            }
            return PartialView("Details", patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.EditTaskDetails)]
        public ActionResult EditDetails([Bind] ScheduleEvent scheduleEvent)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task details could not be saved." };
            if (patientService.UpdateScheduleEvent(scheduleEvent, Request.Files))
            {
                viewData.isSuccessful = true;
            }
            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult MissedVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView(patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public JsonResult AddMissedVisit([Bind] MissedVisit missedVisit)
        {
            Check.Argument.IsNotNull(missedVisit, "missedVisit");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Missed Visit could not be saved." };
            if (patientService.AddMissedVisit(missedVisit))
            {
                viewData.isSuccessful = true;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult MissedVisitInfo(Guid id)
        {
            return PartialView(patientRepository.GetMissedVisit(Current.AgencyId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult LVNSVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
               
                viewData.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.EventDate = evnt.EventDate;
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
           
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult LVNSVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientvisitNote != null)
                {
                    viewData.SignatureText = patientvisitNote.SignatureText;
                    viewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                    viewData.Questions = patientvisitNote.ToDictionary();
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        viewData.EndDate = episode.EndDate;
                        viewData.StartDate = episode.StartDate;
                    }
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
                var patient = patientRepository.Get(patientId, Current.AgencyId);
                viewData.Patient = patient;
            }
            return PartialView("LVNSVisitPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult LVNSVisitBlank()
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("LVNSVisitPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult HHASVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {

                viewData.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.EventDate = evnt.EventDate;
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult HHASVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.SignatureText = patientvisitNote.SignatureText;
                viewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                viewData.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            return PartialView("HHASVisitPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult HHASVisitBlank()
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("HHASVisitPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult SNVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    var assessment = (Assessment)assessmentService.GetPlanofCareAssessmentObject(episodeId, patientId);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        viewData.Questions = assessment.ToNotesQuestionDictionary();
                    }
                }
                else
                {
                    viewData.Questions = patientvisitNote.ToDictionary();
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
               
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.EventDate = evnt.EventDate;
                    }
                }
                viewData.IsWoundCareExist = patientvisitNote.IsWoundCare;
                viewData.IsSupplyExist = patientvisitNote.IsSupplyExist;
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {
                    var physicain = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physicain != null)
                    {
                        viewData.PhysicianId = physicain.Id;
                        viewData.PhysicianDisplayName = physicain.LastName + ", " + physicain.FirstName;
                    }
                }
            }
            return PartialView("SNVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult SNVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.SignatureText = patientvisitNote.SignatureText;
                viewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    var assessment = (Assessment)assessmentService.GetPlanofCareAssessmentObject(episodeId, patientId);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        viewData.Questions = assessment.ToNotesQuestionDictionary();
                    }
                }
                else
                {
                    viewData.Questions = patientvisitNote.ToDictionary();
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {
                    var physicain = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physicain != null)
                    {
                        viewData.PhysicianId = physicain.Id;
                        viewData.PhysicianDisplayName = physicain.LastName + ", " + physicain.FirstName;
                    }
                }
            }
            return PartialView("SNVisitPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult WoundCare(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToWoundCareDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();

            }
            return PartialView("WoundCare", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult SixtyDaySummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    var assessment = (Assessment)assessmentService.GetPlanofCareAssessmentObject(episodeId, patientId);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        viewData.Questions = assessment.ToNotesQuestionDictionary();
                    }
                }
                else
                {
                    viewData.Questions = patientvisitNote.ToDictionary();
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.EventDate = evnt.EventDate;
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();

            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {
                    var physicain = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physicain != null)
                    {
                        viewData.PhysicianId = physicain.Id;
                        viewData.PhysicianDisplayName = physicain.LastName + ", " + physicain.FirstName;
                    }
                }
            }
            return PartialView("60DaySummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult SixtyDaySummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.SignatureText = patientvisitNote.SignatureText;
                viewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    var assessment = (Assessment)assessmentService.GetPlanofCareAssessmentObject(episodeId, patientId);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        viewData.Questions = assessment.ToNotesQuestionDictionary();
                    }
                }
                else
                {
                    viewData.Questions = patientvisitNote.ToDictionary();
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {
                    var physicain = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physicain != null)
                    {
                        viewData.PhysicianId = physicain.Id;
                        viewData.PhysicianDisplayName = physicain.LastName + ", " + physicain.FirstName;
                    }
                }
            }
            return PartialView("60DaySummaryPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult SixtyDaySummaryBlank()
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("60DaySummaryPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult TransferSummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    var assessment = (Assessment)assessmentService.GetPlanofCareAssessmentObject(episodeId, patientId);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        viewData.Questions = assessment.ToNotesQuestionDictionary();
                    }
                }
                else
                {
                    viewData.Questions = patientvisitNote.ToDictionary();
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.EventDate = evnt.EventDate;
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();

            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {

                    var physicain = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physicain != null)
                    {
                        viewData.PhysicianId = physicain.Id;
                        viewData.PhysicianDisplayName = physicain.LastName + ", " + physicain.FirstName;
                    }
                }
            }
            return PartialView("TransferSummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult TransferSummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.SignatureText = patientvisitNote.SignatureText;
                viewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                viewData.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {
                    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                        viewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                    }

                }
            }
            return View("TransferSummaryPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult TransferSummaryBlank()
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("TransferSummaryPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult HHAVisitNote(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    var pocEvent = patientRepository.GetHHAPlanOfCareVisitNote(episodeId, patientId);
                    viewData.Questions = pocEvent.ToDictionary();
                }
                else
                {
                    viewData.Questions = patientvisitNote.ToDictionary();
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.EventDate = evnt.EventDate;
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            return PartialView("HHAVisitNote", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult HHAVisitNotePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.SignatureText = patientvisitNote.SignatureText;
                viewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                viewData.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            return PartialView("HHAVisitNotePrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult HHAVisitNoteBlank()
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("HHAVisitNotePrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult HHACarePlan(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                {
                    var assessment = (Assessment)assessmentService.GetPlanofCareAssessmentObject(episodeId, patientId);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        viewData.Questions = assessment.ToNotesQuestionDictionary();
                    }
                }
                else
                {
                    viewData.Questions = patientvisitNote.ToDictionary();
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.EventDate = evnt.EventDate;
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            return PartialView("HHACarePlan", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult HHACarePlanPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.SignatureText = patientvisitNote.SignatureText;
                viewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                viewData.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {
                    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                    }
                }
            }
            return View("HHACarePlanPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult HHACarePlanBlank()
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("HHACarePlanPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult DischargeSummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                    var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    if (evnt != null)
                    {
                        viewData.EventDate = evnt.EventDate;
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {

                    var physicain = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physicain != null)
                    {
                        viewData.PhysicianId = physicain.Id;
                    }

                }
            }
            return PartialView("DischargeSummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult DischargeSummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.SignatureText = patientvisitNote.SignatureText;
                viewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();

                viewData.Questions = patientvisitNote.ToDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Patient = patient;
            if (patient != null)
            {
                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {

                    var physicain = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    if (physicain != null)
                    {
                        viewData.PhysicianId = physicain.Id;
                    }
                }
            }
            return PartialView("DischargeSummaryPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.PrintClinicalDocuments)]
        public ActionResult DischargeSummaryBlank()
        {
            var viewData = new VisitNoteViewData();
            viewData.Agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("DischargeSummaryPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.EditEpisode)]
        public ActionResult EditEpisode(Guid episodeId, Guid patientId)
        {
            if (patientId.IsEmpty() || episodeId.IsEmpty())
            {
                return PartialView("Episode", new PatientEpisode());
            }
            return PartialView("Episode", patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.EditEpisode)]
        public ActionResult UpdateEpisode([Bind] PatientEpisode episode)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Episode could not be saved." };

            if (patientService.UpdateEpisode(episode, Request.Files))
            {
                viewData.isSuccessful = true;
            }

            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult SupplyWorksheet(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("NotesSupplyWorkSheet", patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewExportedOasis)]
        public ActionResult GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View(new GridModel(patientService.GetNoteSupply(episodeId, patientId, eventId)));

        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, int supplyId, string quantity, string date)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNegativeOrZero(supplyId, "supplyId");
            Check.Argument.IsNotNull(quantity, "quantity");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The new supply could not added." };
            if (patientService.AddNoteSupply(episodeId, patientId, eventId, supplyId, quantity, date))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The new supply was successfully added.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult EditNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            patientService.UpdateNoteSupply(episodeId, patientId, eventId, supply);
            return View(new GridModel(patientService.GetNoteSupply(episodeId, patientId, eventId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            patientService.DeleteNoteSupply(episodeId, patientId, eventId, supply);
            return View(new GridModel(patientService.GetNoteSupply(episodeId, patientId, eventId)));
        }
        #endregion

        #region Old Actions

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult Add(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Event is not Saved" };
            var patientEpisode = patientRepository.GetEpisode(Current.AgencyId, formCollection["episodeId"].ToGuid(), formCollection["patientId"].ToGuid());
            var oldEvents = (patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()).OrderByDescending(o => o.EventDate.ToDateTime()).ToList();
            var events = oldEvents.OrderByDescending(o => o.EventDate.ToDateTime());
            var newEvents = JsonExtensions.DeserializeFromJson<List<ScheduleEvent>>(formCollection["Patient_Schedule"]).OrderBy(e => e.EventDate.ToDateTime());
            foreach (var evnt in newEvents)
            {
                if (evnt.DisciplineTask == (int)DisciplineTasks.Rap)
                {
                    var rap = billingRepository.GetRap(Current.AgencyId, formCollection["patientId"].ToGuid(), formCollection["episodeId"].ToGuid());
                    if (rap != null)
                    {
                        viewData.errorMessage = "Rap already created. To creat a new  delete the existing first.";
                        return Json(viewData);
                    }
                }
                if (evnt.DisciplineTask == (int)DisciplineTasks.Final)
                {
                    var final = billingRepository.GetFinal(Current.AgencyId, formCollection["PatientId"].ToGuid(), formCollection["episodeId"].ToGuid());
                    if (final != null)
                    {
                        viewData.errorMessage = "Final already created. To creat a new  delete the existing first.";
                        return Json(viewData);
                    }
                }

                //    if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT))
                //    {
                //        var transfer = oldEvents.First(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT) && (oe.EventDate.ToDateTime() < evnt.EventDate.ToDateTime()));

                //        ScheduleEvent roc = null;
                //        if (transfer != null)
                //        {
                //            roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime() > transfer.EventDate.ToDateTime()));

                //        }
                //        if (transfer != null && roc == null)
                //        {
                //            viewData.errorMessage = "You Are Not Allowed To Create Recertification If the Patient was transfered.Do ROC.";
                //            return Json(viewData);
                //        }
                //        else if (transfer != null && roc != null && roc.EventDate.ToDateTime() <= transfer.EventDate.ToDateTime())
                //        {
                //            viewData.errorMessage = "You Are Not Allowed To Create Recertification if the patinet was transfered.Do ROC.";
                //            return Json(viewData);
                //        }
                //        else if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT))
                //        {
                //            viewData.errorMessage = "You Already Created Recertification.";
                //            return Json(viewData);
                //        }

                //        else if (evnt.EventDate.ToDateTime() < patientEpisode.EndDate.AddDays(-4) || evnt.EventDate.ToDateTime() > patientEpisode.EndDate)
                //        {
                //            viewData.errorMessage = "Your Recertification Date is not valid date. The date has to be in the last 5 days of the current epsoide.";
                //            return Json(viewData);
                //        }
                //    }
                //    else if ((evnt.DisciplineTask != (int)DisciplineTasks.OASISCStartofCare || evnt.DisciplineTask != (int)DisciplineTasks.OASISCStartofCarePT) && oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT))
                //    {
                //        viewData.errorMessage = "If the patient discharged you have to create SOC.";
                //        return Json(viewData);
                //    }

                //    else if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT))
                //    {
                //        var roc = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT);
                //        var transfer = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
                //        if (roc == null)
                //        {
                //            if (transfer == null)
                //            {
                //                viewData.errorMessage = "You are not allowed to create ROC before Transfer.";
                //                return Json(viewData);
                //            }
                //            else if (transfer != null && (transfer.EventDate.ToDateTime() > evnt.EventDate.ToDateTime()))
                //            {
                //                viewData.errorMessage = "ROC date has to be greater than the transfer date.";
                //                return Json(viewData);
                //            }
                //        }
                //        else if (roc != null)
                //        {
                //            if (transfer != null && (roc.EventDate.ToDateTime() > transfer.EventDate.ToDateTime()))
                //            {
                //                viewData.errorMessage = "You are not allowed to create ROC before Transfer.";
                //                return Json(viewData);
                //            }
                //        }

                //    }
                //    else if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT))
                //    {
                //        var transfer = oldEvents.First(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
                //        ScheduleEvent roc = null;
                //        if (transfer != null)
                //        {
                //            roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime() > transfer.EventDate.ToDateTime()));
                //        }
                //        if (transfer != null && roc == null)
                //        {
                //            viewData.errorMessage = "You are not allowed to create Transfer with out doing ROC for the previous Transfer.";
                //            return Json(viewData);
                //        }
                //    }
            }

            bool result = patientService.UpdateEpisode(formCollection["episodeId"].ToGuid(), formCollection["patientId"].ToGuid(), formCollection["Patient_Schedule"]);

            if (result)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Data successfully updated";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in editing the data.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult AddMultiple(Guid episodeId, Guid patientId, string DisciplineTask, string Discipline, Guid userId, bool IsBillable, string StartDate, string EndDate)
        {
            Check.Argument.IsNotNull(userId, "userId");
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotEmpty(DisciplineTask, "DisciplineTask");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Event is not Saved" };
            viewData = Validate<JsonViewData>(
                          new Validation(() => string.IsNullOrEmpty(StartDate.ToString()), ". Patient date of birth is required."),
                          new Validation(() => !StartDate.ToString().IsValidDate(), ". Date Of birth  for the patient is not in the valid range."),
                          new Validation(() => string.IsNullOrEmpty(EndDate.ToString()), ". Patient date of birth is required."),
                          new Validation(() => !EndDate.ToString().IsValidDate(), ". Date Of birth  for the patient is not in the valid range.")
                          );
            if (viewData.isSuccessful)
            {
                var patientEpisode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                var oldEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                if (StartDate.ToDateTime() >= EndDate.ToDateTime())
                {
                    viewData.errorMessage = "The start date must be greater than end date.";
                    viewData.isSuccessful = false;
                    return Json(viewData);
                }
                else if (StartDate.ToDateTime() < patientEpisode.StartDate || StartDate.ToDateTime() > patientEpisode.EndDate || EndDate.ToDateTime() < patientEpisode.StartDate || EndDate.ToDateTime() > patientEpisode.EndDate)
                {
                    viewData.errorMessage = "The start date and end date has to be in the current episode date range.";
                    viewData.isSuccessful = false;
                    return Json(viewData);
                }
                else
                {
                    patientService.UpdateEpisode(episodeId, patientId, DisciplineTask, Discipline, userId, IsBillable, StartDate.ToDateTime(), EndDate.ToDateTime());
                    return Json(viewData);
                }
            }
            else
            {
                return Json(viewData);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewScheduledTasks)]
        public ActionResult Get(Guid id, string discipline)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotNull(discipline, "discipline");
            return Json(patientService.GetPatientWithSchedule(id, discipline));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessScheduleCenter)]
        public ActionResult GetEpisode(Guid patientId, Guid episodeId, string discipline)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotNull(discipline, "discipline");
            return Json(patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId, discipline));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult Notes(string button, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            List<Validation> rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            string type=formCollection["Type"];
            Guid eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
            Guid episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
            Guid patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Page could not be saved." };
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                if (keys.Contains(type + "_VisitDate"))
                {
                    rules.Add(new Validation(() => !formCollection[type + "_VisitDate"].IsNotNullOrEmpty(), "Visit Date can't be empty."));
                    rules.Add(new Validation(() => !formCollection[type + "_VisitDate"].IsValidDate(), "Visit Date is not valid."));
                    rules.Add(new Validation(() => formCollection[type + "_VisitDate"].IsNotNullOrEmpty() && formCollection[type + "_VisitDate"].IsValidDate() ? !(formCollection[type + "_VisitDate"].ToDateTime() >= episode.StartDate && formCollection[type + "_VisitDate"].ToDateTime() <= episode.EndDate) : true, "Visit Date is not in the episode range."));
                }
                if (button == "Save")
                {
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.SaveNotes(button, formCollection))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Your data is successfully saved.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Your data is not successfully saved.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
                else if (button == "Submit")
                {
                    if (keys.Contains(type + "_Clinician"))
                    {
                        rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_Clinician"]), "User Signature can't be empty."));
                        rules.Add(new Validation(() => formCollection[type + "_Clinician"].IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, formCollection[type + "_Clinician"]) : false, "User Signature is not correct."));
                    }
                    if (keys.Contains(type + "_TimeIn"))
                    {
                        rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_TimeIn"]), "Time-In can't be empty. "));
                    }
                    if (keys.Contains(type + "_TimeOut"))
                    {
                        rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_TimeOut"]), "Time-Out can't be empty. "));
                    }
                    if (keys.Contains(type + "_SignatureDate"))
                    {
                        rules.Add(new Validation(() => !formCollection[type + "_SignatureDate"].IsNotNullOrEmpty(), "Signature date can't be empty."));
                        rules.Add(new Validation(() => !formCollection[type + "_SignatureDate"].IsValidDate(), "Signature date is not valid."));
                        rules.Add(new Validation(() => formCollection[type + "_SignatureDate"].IsNotNullOrEmpty() && formCollection[type + "_SignatureDate"].IsValidDate() ? !(formCollection[type + "_SignatureDate"].ToDateTime() >= episode.StartDate && formCollection[type + "_SignatureDate"].ToDateTime() <= DateTime.Now) : true, "Signature date is not the in valid range."));
                    }
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.SaveNotes(button, formCollection))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Your data is successfully Submited.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
                else if (button == "Approve")
                {
                    viewData = Validate<JsonViewData>(new Validation(() => string.IsNullOrEmpty(formCollection[formCollection["Type"] + "_Clinician"]), "User Signature Can't be empity."));
                    if (viewData.isSuccessful)
                    {
                        if (patientService.SaveNotes(button, formCollection))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Your data is successfully Approved.";
                        }
                    }
                    else
                    {
                        return Json(viewData);
                    }
                }
                else if (button == "Return")
                {
                    if (patientService.SaveNotes(button, formCollection))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your data is successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your data is not returned.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId, string reason)
        {
            List<Validation> rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Page could not be saved." };
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                if (button == "Approve")
                {
                    if (patientService.ProcessNotes(button, episodeId, patientId, eventId, reason))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Your data is successfully Approved.";
                        }
                    
                    else
                    {
                        return Json(viewData);
                    }
                }
                else if (button == "Return")
                {
                    if (patientService.ProcessNotes(button, episodeId, patientId, eventId, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your data is successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your data is not returned.";
                    }
                }
            }
            return Json(viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult WoundCareSave(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Page could not be saved" };
            if (patientService.SaveWoundCare(formCollection, Request.Files))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your page is saved";
            }
            return View("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ScheduleVisits)]
        public ActionResult DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Asset Not Deleted." };
            if (patientService.DeleteWoundCareAsset(episodeId, patientId, eventId, name, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Asset Successfully Deleted.";
            }
            return Json(viewData);
        }

        [GridAction]
        [Demand(Permissions.AccessScheduleCenter)]
        public ActionResult Activity(Guid episodeId, Guid patientId, string discipline)
        {
            List<ScheduleEvent> events = new List<ScheduleEvent>();
            if (episodeId != Guid.Empty && patientId != Guid.Empty && episodeId != null && patientId != null)
            {
                PatientEpisode episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId, discipline);
                if (episode.Details.IsNotNullOrEmpty())
                {
                    episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                }
                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    scheduledEvents.ForEach(e =>
                    {
                        if (!e.EventId.IsEmpty() && e.DisciplineTask > 0)
                        {
                            e.PatientId = patientId;
                            e.EpisodeNotes = episode.Detail.Comments;
                            Common.Url.Set(e, true, true);

                            if (e.IsMissedVisit)
                            {
                                var missedVisit = patientRepository.GetMissedVisit(Current.AgencyId, e.EventId);

                                if (missedVisit != null)
                                {
                                    e.MissedVisitComments = missedVisit.ToString();
                                }
                            }
                            events.Add(e);
                        }
                    });
                }
            }
            return View(new GridModel(events));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessScheduleCenter)]
        public ActionResult ReAssign(Guid episodeId, Guid patientId, Guid eventId, Guid oldUserId, Guid userId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(userId, "userId");
            JsonViewData viewData = new JsonViewData { isSuccessful = false, errorMessage = "Event is not Saved" };
            if (patientService.Reassign(episodeId, patientId, eventId, oldUserId, userId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Event is reassigned sucessfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.DeleteTasks)]
        public JsonResult Delete(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this schedule. Please try again." };
            bool result = patientService.DeleteSchedule(episodeId, patientId, eventId, employeeId);
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessScheduleCenter)]
        public ActionResult MasterCalendar(Guid patientId, Guid episodeId)
        {
            var patientEpisode = patientService.GetPatientEpisodeWithFrequency(episodeId, patientId);
            return PartialView("MasterCalendar", patientEpisode);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessScheduleCenter)]
        public ActionResult CalendarNav(Guid patientId, Guid episodeId, string discipline)
        {
            var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId, discipline);
            return PartialView("Calendar", episode);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessScheduleCenter)]
        public ActionResult ActivityGrid(Guid patientId, Guid episodeId, string discipline)
        {
            var patients = patientRepository.GetAllByAgencyId(Current.AgencyId).ToList().ForSelection();
            var epsoide = patientRepository.GetEpisode(Current.AgencyId, patients.FirstOrDefault().Id, DateTime.Now, "Nursing");
            return PartialView("ScheduleLanding", new ScheduleViewData { Patients = patients, Episode = epsoide });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessScheduleCenter)]
        public ActionResult ActivityFirstTime(Guid patientId, string discipline)
        {
            var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline);
            if (patientId == Guid.Empty || episode == null)
            {
                return PartialView("Activities", new ScheduleActivityArgument { EpisodeId = Guid.Empty, PatientId = Guid.Empty, Discpline = "" });
            }

            return PartialView("Activities", new ScheduleActivityArgument { EpisodeId = episode.Id, PatientId = patientId, Discpline = discipline });
        }

        #endregion
    }
}
