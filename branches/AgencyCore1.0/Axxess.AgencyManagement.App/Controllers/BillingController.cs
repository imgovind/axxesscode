﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;

    using Axxess.Core;

    using Web;
    using Enums;
    using Services;
    using ViewData;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;

    [Compress]
    [Authorize]
    [HandleError]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class BillingController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly IBillingService billingService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IReferralRepository referrralRepository;
        private readonly IPhysicianRepository physicianRepository;

        public BillingController(IAgencyManagementDataProvider dataProvider, IPatientService patientService, IBillingService billingService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            this.referrralRepository = dataProvider.ReferralRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.patientService = patientService;
            this.billingService = billingService;
        }

        #endregion

        #region Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Center()
        {
            return PartialView("Center", billingRepository.AllUnProcessedBill(Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public JsonResult Unprocessed()
        {
            return Json(billingService.GetAllUnProcessedBill());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Rap(Guid episodeId, Guid patientId)
        {
            return PartialView("Rap", billingRepository.GetRap(Current.AgencyId, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Final(Guid episodeId, Guid patientId)
        {

            return PartialView("Final", billingRepository.GetFinal(Current.AgencyId, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Info(Guid episodeId, Guid patientId)
        {
            return PartialView("Info", billingRepository.GetFinal(Current.AgencyId, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Visit(Guid episodeId, Guid patientId)
        {
            var claim = billingRepository.GetFinal(Current.AgencyId, patientId, episodeId);
            var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
            claim.NotVerifiedVisits = episode != null ? episode.Schedule : "";
            return PartialView("Visit", claim);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Supply(Guid episodeId, Guid patientId)
        {
            return PartialView("Supply", billingRepository.GetFinal(Current.AgencyId, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult Summary(Guid episodeId, Guid patientId)
        {
            return PartialView("Summary", billingRepository.GetFinal(Current.AgencyId, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult RapVerify(Rap claim)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Rap is not verified." };
            if (billingRepository.VerifyRap(claim, Current.AgencyId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Rap is successfully verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult ClaimSummary(List<int> RapSelected, List<int> FinalSelected)
        {
            var viewData = new Bill { Finals = billingRepository.Finals(FinalSelected, Current.AgencyId), Raps = billingRepository.Raps(RapSelected, Current.AgencyId) };
            return PartialView("ClaimSummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public FileStreamResult Generate(List<int> RapSelected, List<int> FinalSelected)
        {
            string generateJsonClaim = billingService.Generate(RapSelected, FinalSelected);
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] buffer = encoding.GetBytes(generateJsonClaim);
            Stream fileStream = new MemoryStream();
            fileStream.Write(buffer, 0, generateJsonClaim.Length);
            fileStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=billing.txt");
            return new FileStreamResult(fileStream, "Text/Plain");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult UpdateStatus(List<int> RapSelected, List<int> FinalSelected)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Status Update is not Successful." };
            bool rapStatus = billingRepository.UpdateRapStatus(RapSelected, Current.AgencyId);
            bool finalStatus = billingRepository.UpdateFinalStatus(FinalSelected, Current.AgencyId);
            if (rapStatus && finalStatus)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Status Update is Successfull.";
            }
            else if (rapStatus && !finalStatus)
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Your Final Status Update is not Successfull.";
            }

            else if (!rapStatus && finalStatus)
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Your rap Status Update is not Successfull.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult History()
        {
            var viewData = new PatientCenterViewData();
            viewData.Patients = patientRepository.GetAllByAgencyId(Current.AgencyId).ToList().ForSelection();
            if (viewData.Patients != null && viewData.Patients.Count > 0)
            {
                viewData.ScheduleEvents = patientRepository.GetScheduledEvents(Current.AgencyId, viewData.Patients.FirstOrDefault().Id);
            }
            return PartialView("History", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult PendingClaims()
        {
            return PartialView("PendingClaims", billingService.PendingClaims());
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult HistoryActivity(Guid patientId)
        {
            return View(new GridModel(billingService.Activity(patientId)));

        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult InfoVerify(Final claim)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final Basic Info is  not verified." };
            if (billingRepository.VerifyInfo(claim, Current.AgencyId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Final Basic Info is successfully verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessBillingCenter)]
        public ActionResult VisitVerify(int Id, Guid episodeId, Guid patientId, List<Guid> Visit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final is not verified." };
            if (billingService.VisitVerify(Id, episodeId, patientId, Visit))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Final Visit is successfully verified.";
            }
            return Json(viewData);
        }


        // [AcceptVerbs(HttpVerbs.Post)]
        // public ActionResult SupplyVerify(Claim claim)
        // {
        //     var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Rap is not verified." };
        //     if (billingService.(claim, Current.AgencyId))
        //     {
        //         viewData.isSuccessful = true;
        //         viewData.errorMessage = "Rap is successfully verified.";
        //     }
        //     return Json(viewData);
        // }


        #endregion
    }
}
