﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Web;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Enums;
    using Domain;
    using ViewData;
    using Services;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;

    [Compress]
    [Authorize]
    [HandleError]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class MessageController : BaseController
    {
        #region Private Members

        private readonly IMessageService messageService;
        private readonly IUserRepository userRepository;
        private readonly IMessageRepository messageRepository;

        #endregion

        #region Constructor

        public MessageController(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementProvider, IMessageService messageService)
        {
            Check.Argument.IsNotNull(messageService, "messageService");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            this.messageService = messageService;
            this.userRepository = agencyManagementProvider.UserRepository;
            this.messageRepository = agencyManagementProvider.MessageRepository;
        }

        #endregion

        #region MessageController Actions
        
        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.Messaging)]
        public ActionResult Inbox()
        {
            var messages = messageRepository.GetUserMessages(Current.UserId).Result;
            return PartialView(messages.Take(13));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.Messaging)]
        public JsonResult List()
        {
            var gridModel = new GridModel();
            var pageNumber = this.HttpContext.Request.Params["page"];
            var messages = messageRepository.GetUserMessages(Current.UserId).Result;
            if (messages != null && pageNumber.HasValue())
            {
                int page = int.Parse(pageNumber);
                gridModel.Data = messages.Skip((page - 1) * 13).Take(13);
                gridModel.Total = messages.Count;
            }
            return Json(gridModel);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.Messaging)]
        public JsonResult Get(Guid id)
        {
            return Json(messageRepository.GetMessage(id, true));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.Messaging)]
        public ActionResult New()
        {
            return PartialView();
        }

        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.Messaging)]
        public JsonResult New([Bind] Message message)
        {
            Check.Argument.IsNotNull(message, "message");

            var viewData = new JsonViewData();

            if (message.IsValid)
            {
                if (messageService.SendMessage(message))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Message has been sent!";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error sending your message.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = message.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.Messaging)]
        public JsonResult Recipients(string searchTerm)
        {
            var recipients = new List<Recipient>();
            var query = userRepository.GetAgencyUsers(searchTerm, Current.AgencyId);
            query.ForEach(e =>
            {
                recipients.Add(new Recipient { id = e.Id.ToString(), name = string.Concat(e.LastName + ", " + e.FirstName) });
            });
            return Json(recipients);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.Messaging)]
        public JsonResult Delete(Guid messageId)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Message could not be deleted!" };

            if (messageRepository.Delete(messageId))
            {
                viewData.isSuccessful = true;
            }

            return Json(viewData);
        }

        #endregion

    }
}
