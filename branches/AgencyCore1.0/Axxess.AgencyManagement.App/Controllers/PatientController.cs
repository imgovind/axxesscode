﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Web;
    using Enums;
    using Services;
    using ViewData;
    using Workflows;
    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.Membership.Domain;

    using Axxess.OasisC.Domain;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using AutoMapper;

    using Telerik.Web.Mvc;

    [Compress]
    [Authorize]
    [HandleError]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PatientController : BaseController
    {
        #region Constructor

        private readonly IDateService dateService;
        private readonly IPatientService patientService;
        private readonly IUserService userService;
        private readonly IAssessmentService assessmentService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IReferralRepository referralRepository;
        private readonly IPhysicianRepository physicianRepository;

        public PatientController(IAgencyManagementDataProvider agencyManagementDataProvider, IPatientService patientService, IAssessmentService assessmentService, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.patientService = patientService;
            this.assessmentService = assessmentService;
            this.dateService = Container.Resolve<IDateService>();
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region PatientController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult New()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult New(Guid referralId)
        {
            if (!referralId.IsEmpty())
            {
                return PartialView("New", referralRepository.Get(Current.AgencyId, referralId));
            }
            return PartialView("New", null);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public JsonResult Add([Bind] Patient patient)
        {
            Check.Argument.IsNotNull(patient, "patient");

            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient could not be saved" };
            if (patient.IsValid)
            {
                patient.AgencyId = Current.AgencyId;
                if (patient.Status == (int)PatientStatus.Active)
                {
                    var workflow = new CreatePatientWorkflow(patient);
                    if (workflow.IsCommitted)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Patient was created successfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = workflow.Message;
                    }
                }
                else
                {
                    if (patientRepository.Add(patient))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Patient was created successfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                    }
                }
                if (viewData.isSuccessful && !patient.ReferralId.IsEmpty())
                {
                    referralRepository.SetStatus(Current.AgencyId, patient.ReferralId, ReferralStatus.Admitted);
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = patient.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.AccessPatientCenter)]
        public ActionResult Center()
        {
            var viewData = new PatientCenterViewData();
            if (Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                viewData.Patients = patientRepository.GetAllByAgencyId(Current.AgencyId).Where(p => p.Status == (byte)PatientStatus.Active).ToList().ForSelection();
            }
            else if (Current.IsClinicianOrHHA)
            {
                viewData.Patients = userRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active).ForSelection();
            }
            else { }

            if (viewData.Patients != null && viewData.Patients.Count > 0)
            {
                viewData.ScheduleEvents = patientRepository.GetScheduledEvents(Current.AgencyId, viewData.Patients.FirstOrDefault().Id);
            }
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessPatientCenter)]
        public ActionResult Data(Guid patientId)
        {
            var viewData = new PatientCenterViewData();
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            if (viewData.Patient != null)
            {
                viewData.ScheduleEvents = patientRepository.GetScheduledEvents(Current.AgencyId, patientId);
            }
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessPatientCenter)]
        public ActionResult Info(Guid patientId)
        {
            if (patientId == Guid.Empty)
            {
                return PartialView(new Patient());
            }
            return PartialView(patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult Get(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return Json(patientRepository.Get(id, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessPatientCenter)]
        public ActionResult All(byte statusId, byte paymentSourceId, string name)
        {
            bool ignorePaymentSource = (paymentSourceId == 0) ? true : false;
            bool ignorePatientName = string.IsNullOrEmpty(name) ? true : false;

            var patientList = new List<Patient>();
            var filteredList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                patientList = patientRepository.GetAllByAgencyId(Current.AgencyId).Where(p => p.Status == statusId).ToList();
            }
            else if (Current.IsClinicianOrHHA)
            {
                patientList = userRepository.GetUserPatients(Current.AgencyId, Current.UserId, statusId);
            }
            else { }

            patientList.ForEach<Patient>(p =>
            {
                if (ignorePatientName && ignorePaymentSource)
                {
                    filteredList.Add(new PatientSelection { Id = p.Id, FirstName = p.FirstName, LastName = p.LastName });
                }
                else
                {
                    if (ignorePatientName && ignorePaymentSource)
                    {
                        filteredList.Add(new PatientSelection { Id = p.Id, FirstName = p.FirstName, LastName = p.LastName });
                    }

                    if (!ignorePatientName && ignorePaymentSource)
                    {
                        if (p.LastName.ToLower().Trim().Contains(name) || p.FirstName.ToLower().Trim().Contains(name))
                        {
                            filteredList.Add(new PatientSelection { Id = p.Id, FirstName = p.FirstName, LastName = p.LastName });
                        }
                    }

                    if (ignorePatientName && !ignorePaymentSource)
                    {
                        if (p.PaymentSource.Contains(paymentSourceId.ToString()))
                        {
                            filteredList.Add(new PatientSelection { Id = p.Id, FirstName = p.FirstName, LastName = p.LastName });
                        }
                    }

                    if (!ignorePatientName && !ignorePaymentSource)
                    {
                        if ((p.LastName.ToLower().Trim().Contains(name) || p.FirstName.ToLower().Trim().Contains(name)) && (p.PaymentSource.Contains(paymentSourceId.ToString())))
                        {
                            filteredList.Add(new PatientSelection { Id = p.Id, FirstName = p.FirstName, LastName = p.LastName });
                        }
                    }
                }
            });

            return View(new GridModel(filteredList));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessPatientCenter)]
        public ActionResult Activity(Guid patientId, string discipline, string dateRangeId, DateTime rangeStartDate, DateTime rangeEndDate)
        {
            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");
            Check.Argument.IsNotNull(dateRangeId, "dateRangeId");

            var filteredList = new List<ScheduleEvent>();
            if (patientId != Guid.Empty)
            {
                IList<ScheduleEvent> patientActivities = patientRepository.GetScheduledEvents(Current.AgencyId, patientId);
                var dateRange = new DateRange();

                if (dateRangeId == "DateRange")
                {
                    dateRange.StartDate = rangeStartDate;
                    dateRange.EndDate = rangeEndDate;
                }
                else
                {
                    dateRange = dateService.GetDateRange(dateRangeId, patientId);
                }

                if (discipline == "All")
                {
                    patientActivities.ForEach(e =>
                    {
                        if (e.EventId != Guid.Empty && e.DisciplineTask > 0 && DateTime.Parse(e.EventDate) >= dateRange.StartDate && DateTime.Parse(e.EventDate) <= dateRange.EndDate)
                        {
                            e.PatientId = patientId;
                            Common.Url.Set(e, false, true);
                            filteredList.Add(e);
                        }
                    });
                }
                else
                {
                    patientActivities.ForEach(e =>
                    {
                        if (e.EventId != Guid.Empty && e.Discipline == discipline && e.DisciplineTask > 0)
                        {
                            if (DateTime.Parse(e.EventDate) >= dateRange.StartDate && DateTime.Parse(e.EventDate) <= dateRange.EndDate)
                            {
                                e.PatientId = patientId;
                                Common.Url.Set(e, false, true);
                                filteredList.Add(e);
                            }
                        }
                    });
                }
            }
            return View(new GridModel(filteredList));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public ActionResult NewOrder()
        {
            return PartialView("Order/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public JsonResult AddOrder(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "New order could not be saved." };

            if (order.IsValid)
            {
                order.Id = Guid.NewGuid();
                order.UserId = Current.UserId;
                order.AgencyId = Current.AgencyId;
                order.OrderNumber = patientRepository.GetNextOrderNumber();
                order.Created = DateTime.Now;

                if (order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
                {
                    if (order.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, order.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this order.";
                        return Json(viewData);
                    }
                    else
                    {
                        order.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                    }
                }

                var newScheduleEvent = new ScheduleEvent
                {
                    EventId = order.Id,
                    UserId = order.UserId,
                    PatientId = order.PatientId,
                    Status = order.Status.ToString(),
                    Discipline = Disciplines.Orders.ToString(),
                    EventDate = order.OrderDate.ToShortDateString(),
                    DisciplineTask = (int)DisciplineTasks.PhysicianOrder
                };

                patientRepository.AddNewScheduleEvent(Current.AgencyId, order.PatientId, newScheduleEvent);

                order.EpisodeId = newScheduleEvent.EpisodeId;

                var newUserEvent = new UserEvent
                {
                    EventId = order.Id,
                    UserId = order.UserId,
                    PatientId = order.PatientId,
                    EpisodeId = order.EpisodeId,
                    Status = order.Status.ToString(),
                    Discipline = Disciplines.Orders.ToString(),
                    EventDate = order.OrderDate.ToShortDateString(),
                    DisciplineTask = (int)DisciplineTasks.PhysicianOrder
                };

                patientRepository.AddNewUserEvent(Current.AgencyId, order.PatientId, newUserEvent);

                if (patientRepository.AddOrder(order))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Order has been saved successfully.";
                    if (order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
                    {
                        viewData.errorMessage = "Order has been completed successfully.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Order could not be saved! Please try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = order.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public ActionResult ViewOrder(Guid patientId, Guid orderId)
        {
            var order = patientRepository.GetOrder(orderId, patientId, Current.AgencyId);
            if (order != null)
            {
                order.Agency = agencyRepository.Get(Current.AgencyId);
                order.Patient = patientRepository.Get(order.PatientId, Current.AgencyId);
                order.Physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);

                var episode = patientRepository.GetEpisodeById(Current.AgencyId, order.EpisodeId, order.PatientId);
                if (episode != null)
                {
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                    var allergies = assessmentService.Allergies(episode.AssessmentId, episode.AssessmentType);
                    if (allergies != null && allergies.Count > 0)
                    {
                        if (allergies.ContainsKey("485Allergies") && allergies["485Allergies"].Answer.IsNotNullOrEmpty())
                        {
                            if (allergies["485Allergies"].Answer == "Yes")
                            {
                                if (allergies.ContainsKey("485AllergiesDescription") && allergies["485AllergiesDescription"].Answer.IsNotNullOrEmpty())
                                {
                                    order.Allergies = allergies["485AllergiesDescription"].Answer;
                                }
                            }
                            else
                            {
                                order.Allergies = "NKA (Food/Drugs/Latex)";
                            }
                        }
                    }

                    var diagnosis = assessmentService.Diagnosis(episode.AssessmentId, episode.AssessmentType);
                    if (diagnosis != null && diagnosis.Count > 0)
                    {
                        if (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
                        {
                            order.PrimaryDiagnosisText = diagnosis["M1020PrimaryDiagnosis"].Answer;
                        }
                        if (diagnosis.ContainsKey("M1020ICD9M") && diagnosis["M1020ICD9M"].Answer.IsNotNullOrEmpty())
                        {
                            order.PrimaryDiagnosisCode = diagnosis["M1020ICD9M"].Answer;
                        }
                        if (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
                        {
                            order.SecondaryDiagnosisText = diagnosis["M1022PrimaryDiagnosis1"].Answer;
                        }
                        if (diagnosis.ContainsKey("M1022ICD9M1") && diagnosis["M1022ICD9M1"].Answer.IsNotNullOrEmpty())
                        {
                            order.SecondaryDiagnosisCode = diagnosis["M1022ICD9M1"].Answer;
                        }
                    }
                }
            }
            return View("Order/Print", order);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public ActionResult EditOrder(Guid id, Guid patientId)
        {
            return PartialView("Order/Edit", patientRepository.GetOrder(id, patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateOrder(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order could not be updated." };

            if (order.IsValid)
            {
                order.AgencyId = Current.AgencyId;
                if (order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
                {
                    if (order.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, order.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this order.";
                        return Json(viewData);
                    }
                    else
                    {
                        order.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                    }
                }
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, order.EpisodeId, order.PatientId, order.Id);
                if (scheduleEvent != null)
                {
                    scheduleEvent.Status = order.Status.ToString();
                    patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent);
                }

                var userEvent = userRepository.GetEvent(Current.AgencyId, order.UserId, order.PatientId, order.Id);
                if (userEvent != null)
                {
                    userEvent.Status = order.Status.ToString();
                    userRepository.UpdateEvent(Current.AgencyId, order.PatientId, order.UserId, userEvent);
                }

                if (patientRepository.UpdateOrder(order))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Order has been updated successfully.";
                    if (order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
                    {
                        viewData.errorMessage = "Order has been completed and is ready to be sent to Physician.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Order could not be saved! Please try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = order.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePhysicianOrders)]
        public JsonResult GetOrder(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return Json(patientRepository.GetOrder(Id, patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewPatientProfile)]
        public ActionResult Profile(Guid id)
        {
            return View("Profile", patientService.GetProfile(id));
        }

        [GridAction]
        [Demand(Permissions.ViewLists)]
        public ActionResult List()
        {
            IEnumerable<Patient> patientList = patientRepository.GetAllByAgencyId(Current.AgencyId);
            return View(new GridModel(patientList));
        }

        [GridAction]
        [Demand(Permissions.ViewLists)]
        public ActionResult PendingList()
        {
            IEnumerable<Patient> patientList = patientRepository.GetPendingByAgencyId(Current.AgencyId);
            return View(new GridModel(patientList));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewLists)]
        public ActionResult Grid()
        {
            return View("List");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewLists)]
        public ActionResult PendingGrid()
        {
            return View("Pending");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessPatientCenter)]
        public ActionResult NewPhoto(Guid patientId)
        {
            return PartialView("Photo", patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessPatientCenter)]
        public ActionResult AddPhoto([Bind] Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient photo could not be saved." };

            if (patientService.IsValidImage(Request.Files))
            {
                if (patientService.AddPhoto(patientId, Request.Files))
                {
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.errorMessage = "File uploaded is not a valid image.";
            }

            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult Edit([Bind] Patient patient)
        {
            var viewData = Validate<JsonViewData>(
                            new Validation(() => string.IsNullOrEmpty(patient.FirstName), "Patient first name is required. <br/>"),
                            new Validation(() => string.IsNullOrEmpty(patient.LastName), "Patient last name is required.  <br/>"),
                            new Validation(() => string.IsNullOrEmpty(patient.DOB.ToString()), "Patient date of birth is required. <br/>"),
                            new Validation(() => !patient.DOB.ToString().IsValidDate(), "Date Of birth  for the patient is not in the valid range.  <br/>"),
                            new Validation(() => string.IsNullOrEmpty(patient.Gender), "Patient gender has to be selected.  <br/>"),
                            new Validation(() => (patient.EmailAddress == null ? !string.IsNullOrEmpty(patient.EmailAddress) : !patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"),
                            new Validation(() => string.IsNullOrEmpty(patient.AddressLine1), "Patient address line is required.  <br/>"),
                            new Validation(() => string.IsNullOrEmpty(patient.AddressCity), "Patient city is required.  <br/>"),
                            new Validation(() => string.IsNullOrEmpty(patient.AddressStateCode), "Patient state is required.  <br/>"),
                            new Validation(() => string.IsNullOrEmpty(patient.AddressZipCode), "Patient zip is required.  <br/>"),
                            new Validation(() => !string.IsNullOrEmpty(patient.SSN) ? !patient.SSN.IsSSN() : false, "Patient SSN is not in valid format.  <br/>"),
                            new Validation(() => string.IsNullOrEmpty(patient.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"),
                            new Validation(() => !patient.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>")
                       );

            if (viewData.isSuccessful)
            {
                patient.AgencyId = Current.AgencyId;
                bool result = patientRepository.Edit(patient);
                if (result)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Data successfully edited";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in editing the data.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult GetEmergencyContact(Guid patientId, Guid EmergencyContactId)
        {
            Check.Argument.IsNotEmpty(EmergencyContactId, "EmergencyContactId");
            return Json(patientRepository.GetEmergencyContact(patientId, EmergencyContactId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult NewEmergencyContact(PatientEmergencyContact emergencyContact, Guid PatientId)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            var viewData = Validate<JsonViewData>(
                           new Validation(() => string.IsNullOrEmpty(emergencyContact.FirstName), "Emergency Contact first name is required. <br/>"),
                           new Validation(() => string.IsNullOrEmpty(emergencyContact.LastName), "Emergency Contact last name is required.  <br/>"),
                           new Validation(() => (emergencyContact.EmailAddress == null ? !string.IsNullOrEmpty(emergencyContact.EmailAddress) : !emergencyContact.EmailAddress.IsEmail()), "Emergency Contact e-mail is not in a valid  format.  <br/>"),
                           new Validation(() => emergencyContact.PhonePrimaryArray == null || !(emergencyContact.PhonePrimaryArray.Count > 0), "Phone is required.  <br/>")
                      );
            if (viewData.isSuccessful)
            {
                bool result = patientService.NewEmergencyContact(emergencyContact, PatientId);
                if (result)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Data successfully added";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in editing the data.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult EditEmergencyContact(Guid Id, PatientEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            var viewData = Validate<JsonViewData>(
                           new Validation(() => string.IsNullOrEmpty(emergencyContact.FirstName), "Emergency Contact first name is required. <br/>"),
                           new Validation(() => string.IsNullOrEmpty(emergencyContact.LastName), "Emergency Contact last name is required.  <br/>"),
                           new Validation(() => (emergencyContact.EmailAddress == null ? !string.IsNullOrEmpty(emergencyContact.EmailAddress) : !emergencyContact.EmailAddress.IsEmail()), "Emergency Contact e-mail is not in a valid  format.  <br/>"),
                           new Validation(() => emergencyContact.PhonePrimaryArray == null || !(emergencyContact.PhonePrimaryArray.Count > 0), "Phone is required.  <br/>")
                      );
            if (viewData.isSuccessful)
            {
                bool result = patientRepository.EditEmergencyContact(Current.AgencyId, emergencyContact);
                if (result)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your data successfully edited";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in editing the data.";
                }
            }
            return Json(viewData);
        }

        [GridAction]
        [Demand(Permissions.ManagePatients)]
        public ActionResult GetEmergencyContacts(Guid PatientId)
        {
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            return Json(new GridModel { Data = patientRepository.GetEmergencyContacts(PatientId) });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult DeleteEmergencyContact(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Data Is Not deleted. Try Again." };
            bool result = patientRepository.DeleteEmergencyContact(id, patientId);
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult Delete(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Data Is Not deleted. Try Again." };
            bool result = patientRepository.Delete(Current.AgencyId, patientId);
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult AddPatientPhysicain(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your data is not added. Try Again." };
            if (!physicianRepository.IsPhysicianExist(patientId, id))
            {
                bool result = physicianRepository.Link(patientId, id, false);
                if (result)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your data is successfully added.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Physicain already exists.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult GetPatientPhysicianContact(Guid PhysicianContactId, Guid PatientId)
        {
            Check.Argument.IsNotEmpty(PhysicianContactId, "PhysicianContactId");
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            return Json(physicianRepository.GetByPatientId(PhysicianContactId, PatientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult DeletePhysicianContact(Guid id, Guid patientID)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(patientID, "PatientID");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Data Is Not deleted. Try Again." };
            bool result = patientRepository.DeletePhysicianContact(id, patientID);
            if (result)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in Deleting the data.";
            }
            return Json(viewData);
        }

        [GridAction]
        [Demand(Permissions.ManagePatients)]
        public ActionResult GetPhysicians(Guid PatientId)
        {
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            return Json(new GridModel(physicianRepository.GetPatientPhysicians(PatientId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult NewCommunicationNote()
        {
            return PartialView("CommunicationNote/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult NewCommunicationNote(Guid patientId)
        {
            return PartialView("CommunicationNote/New", patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult AddCommunicationNote([Bind]CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");

            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Communication Note could not be saved" };

            if (communicationNote.IsValid)
            {
                communicationNote.UserId = Current.UserId;
                communicationNote.AgencyId = Current.AgencyId;
                if (patientRepository.AddCommunicationNote(communicationNote))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Communication note successfully saved";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in Saving the communication note.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = communicationNote.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public JsonResult GetCommunicationNote(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(Id, "Id");
            return Json(patientRepository.GetCommunicationNote(Id, patientId));

        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult EditCommunicationNote(CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNoteViewData");
            var viewData = Validate<JsonViewData>(
                           new Validation(() => GuidExtension.IsEmpty(communicationNote.Id), "Communication note id is not empty."),
                           new Validation(() => GuidExtension.IsEmpty(communicationNote.PatientId), "Patient is not empty."),
                           new Validation(() => string.IsNullOrEmpty(communicationNote.Text), "Patient order text is empty. <br/>"),
                           new Validation(() => string.IsNullOrEmpty(communicationNote.Created.ToString()), "Date is required. <br/>")
                          );

            if (viewData.isSuccessful)
            {
                bool result = patientRepository.EditCommunicationNote(communicationNote);
                if (result)
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Data successfully saved";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in Saving the data.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public JsonResult GetNote(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new NoteViewData { isSuccessful = false };
            var patientNote = patientRepository.GetNote(patientId);
            if (patientNote != null)
            {
                viewData.Id = patientNote.Id;
                viewData.Note = patientNote.Note;
                viewData.PatientId = patientNote.PatientId;
                viewData.isSuccessful = true;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult Note(Guid patientId, string patientNote)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Note Is Not Saved. Try Again." };
            Guid guid = patientRepository.Note(patientId, patientNote);
            if (guid != Guid.Empty)
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Note Successfully Saved.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.AccessPatientCenter)]
        public ActionResult DateRange(string dateRangeId, Guid patientId)
        {
            return Json(dateService.GetDateRange(dateRangeId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult EditPatientContent(Guid patientId)
        {
            return PartialView("~/Views/Patient/Edit.ascx", patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult NewEmergencyContactContent(Guid patientId)
        {
            return PartialView("~/Views/Patient/EmergencyContact/New.ascx", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult EditEmergencyContactContent(Guid patientId, Guid Id)
        {
            return PartialView("~/Views/Patient/EmergencyContact/Edit.ascx", patientRepository.GetEmergencyContact(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult NewAdmit(Guid patientId)
        {
            return PartialView("Admit", patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public JsonResult AddAdmit([Bind] PendingPatient patient)
        {
            Check.Argument.IsNotNull(patient, "PendingPatient");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be admitted." };

            patient.AgencyId = Current.AgencyId;
            if (patientService.AdmitPatient(patient))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient has been admitted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public ActionResult NewNonAdmit(Guid patientId)
        {
            return PartialView("NonAdmit", patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ManagePatients)]
        public JsonResult AddNonAdmit([Bind] PendingPatient patient)
        {
            Check.Argument.IsNotNull(patient, "PendingPatient");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be admitted." };
            if (patientService.NonAdmitPatient(patient))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient non-admission successful.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewIncidentAccidentInfectionReport)]
        public ActionResult InfectionReport()
        {
            return PartialView("Infection/New", new VisitNoteViewData());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewIncidentAccidentInfectionReport)]
        public ActionResult IncidentReport()
        {
            return PartialView("Incident/New", new VisitNoteViewData());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult MedicationDischarge()
        {
            return PartialView("MedicationDischarge");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public JsonResult LastestMedications(Guid patientId)
        {
            var medications = string.Empty;
            var medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medicationProfile != null)
            {
                medications = medicationProfile.ToString();
            }
            return Json(medications);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult Medication(Guid MedId, string medicationCategory)
        {
            var medicationProfile = patientRepository.GetMedicationProfile(MedId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var list = medicationProfile.Medication.ToObject<List<Medication>>();
                list.ForEach(l =>
                {
                    l.DischargeUrl = "Patient.DischargeMed('" + MedId + "','" + l.Id + "');";
                });
                return View(new GridModel(list.FindAll(m => m.MedicationCategory == medicationCategory).OrderBy(l => l.StartDate)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult MedicationSnapshot(Guid MedId)
        {
            var medicationProfile = patientRepository.GetMedicationProfile(MedId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var list = medicationProfile.Medication.ToObject<List<Medication>>();
                list.ForEach(l =>
                {
                    l.DischargeUrl = "Patient.DischargeMed('" + MedId + "','" + l.Id + "');";
                });
                return View(new GridModel(list.FindAll(l => l.MedicationCategory == "Active").OrderByDescending(l => l.StartDate)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult InsertMedication(Guid MedId, Medication medication, string MedicationType, string medicationCategory)
        {
            var medProfile = patientRepository.InsertMedication(MedId, Current.AgencyId, medication, MedicationType);
            if (medProfile != null && medProfile.Medication.IsNotNullOrEmpty())
            {
                var list = medProfile.Medication.ToObject<List<Medication>>();
                list.ForEach(l =>
                {
                    l.DischargeUrl = "Patient.DischargeMed('" + MedId + "','" + l.Id + "');";
                });
                return View(new GridModel(list.FindAll(m => m.MedicationCategory == medicationCategory).OrderBy(l => l.StartDate)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult UpdateMedication(Guid MedId, Medication medication, string MedicationType, string medicationCategory)
        {
            patientRepository.UpdateMedication(MedId, Current.AgencyId, medication, MedicationType);
            var medProfile = patientRepository.GetMedicationProfile(MedId, Current.AgencyId);
            if (medProfile != null && medProfile.Medication != null)
            {
                var list = medProfile.Medication.ToObject<List<Medication>>();
                list.ForEach(l =>
                {
                    l.DischargeUrl = "Patient.DischargeMed('" + MedId + "','" + l.Id + "');";
                });
                return View(new GridModel(list.FindAll(m => m.MedicationCategory == medicationCategory).OrderBy(l => l.StartDate)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public JsonResult UpdateMedicationForDischarge(Guid MedId, Guid Id, DateTime DischargeDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your discharge medication is not changed." };
            if (patientRepository.UpdateMedicationForDischarge(MedId, Current.AgencyId, Id, DischargeDate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your discharge medication is changed.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult DeleteMedication(Guid MedId, Medication medication, string medicationCategory)
        {
            patientRepository.DeleteMedication(MedId, Current.AgencyId, medication);
            var medProfile = patientRepository.GetMedicationProfile(MedId, Current.AgencyId);
            if (medProfile != null && medProfile.Medication.IsNotNullOrEmpty())
            {
                var list = medProfile.Medication.ToObject<List<Medication>>();
                list.ForEach(l =>
                {
                    l.DischargeUrl = "Patient.DischargeMed('" + MedId + "','" + l.Id + "');";
                });
                return View(new GridModel(list.FindAll(m => m.MedicationCategory == medicationCategory).OrderBy(l => l.StartDate)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //[Demand(Permissions.ViewMedicationProfile)]
        //public ActionResult UpdateMedicationProfileHistory(string button, MedicationProfileHistory medicationProfileHistory)
        //{
        //    Check.Argument.IsNotNull(button, "button");
        //    Check.Argument.IsNotNull(medicationProfileHistory, "medicationProfileHistory");
        //    var viewData = Validate<JsonViewData>();
        //    viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Medication Profile Is Not Saved. Try Again." };
        //    viewData.isSuccessful = true;
        //    if (button == "Save")
        //    {
        //        if (medicationProfileHistory.Signature.IsNotNullOrEmpty())
        //        {
        //            viewData = Validate<JsonViewData>(
        //            new Validation(() => !userService.IsSignatureCorrect(Current.UserId, medicationProfileHistory.Signature), "The signature is not right.")
        //            );
        //            if (viewData.isSuccessful)
        //            {
        //                if (patientService.UpdateMedicationProfileHistory(button, medicationProfileHistory))
        //                {
        //                    viewData.isSuccessful = true;
        //                    viewData.errorMessage = "Your data is successfully saved.";
        //                }
        //            }
        //        }
        //        else
        //        {
        //            if (patientService.UpdateMedicationProfileHistory(button, medicationProfileHistory))
        //            {
        //                viewData.isSuccessful = true;
        //                viewData.errorMessage = "Your data is successfully saved.";
        //            }
        //        }
        //    }
        //    else if (button == "Sign")
        //    {
        //        viewData = Validate<JsonViewData>(
        //          new Validation(() => string.IsNullOrEmpty(medicationProfileHistory.Signature), "The signature is empty."),
        //          new Validation(() => !userService.IsSignatureCorrect(Current.UserId, medicationProfileHistory.Signature), "The signature is not right."),
        //          new Validation(() => medicationProfileHistory.SignedDate.IsValid(DateTime.Now), "The signature date has to be valid and latter than now.")
        //          );
        //        if (viewData.isSuccessful)
        //        {
        //            if (patientService.UpdateMedicationProfileHistory(button, medicationProfileHistory))
        //            {
        //                viewData.isSuccessful = true;
        //                viewData.errorMessage = "Your data is successfully signed.";
        //            }
        //        }
        //    }
        //    return Json(viewData);
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult SaveMedicationProfile(MedicationProfile medicationProfile)
        {
            Check.Argument.IsNotNull(medicationProfile, "medicationProfile");
            var viewData = Validate<JsonViewData>();
            viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Medication Profile Is Not Saved. Try Again." };
            medicationProfile.AgencyId = Current.AgencyId;
            if (patientRepository.SaveMedicationProfile(medicationProfile))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully saved.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult SignMedicationHistory(Guid MedId, MedicationProfileHistory medicationProfileHistory)
        {
            Check.Argument.IsNotEmpty(MedId, "MedId");
            var viewData = Validate<JsonViewData>();
            viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Medication Profile Is Not Signed. Try Again." };
            viewData = Validate<JsonViewData>(
                   new Validation(() => string.IsNullOrEmpty(medicationProfileHistory.Signature), "The signature is empty."),
                   new Validation(() => !userService.IsSignatureCorrect(Current.UserId, medicationProfileHistory.Signature), "The signature is not right."),
                   new Validation(() => medicationProfileHistory.SignedDate.IsValid(DateTime.Now), "The signature date has to be valid and latter than now.")
                   );
            if (viewData.isSuccessful)
            {
                if (patientService.SignMedicationHistory(MedId, medicationProfileHistory))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Medication profile is successfully saved.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult MedicationProfileSnapShotHistory(Guid PatientId)
        {
            return PartialView("MedicationProfileSnapShotHistory", patientRepository.Get(PatientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult MedicationSnapshotHistory(Guid PatientId)
        {
            return View(new GridModel(patientService.GetMedicationHistoryForPatient(PatientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult MedicationProfileSnapShot(Guid PatientId)
        {
            var med = patientRepository.GetMedicationProfileByPatient(PatientId, Current.AgencyId);
            var viewData = new MedicationProfileViewData();
            if (med != null)
            {
                viewData.MedicationProfile = med;
                viewData.Patient = patientRepository.Get(PatientId, Current.AgencyId);
                if (viewData.Patient != null)
                {
                    if (viewData.Patient.PhysicianContacts != null && viewData.Patient.PhysicianContacts.Count > 0)
                    {
                        var physicain = viewData.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physicain != null)
                        {
                            viewData.PhysicianId = physicain.Id;
                        }
                    }
                    if (med.PharmacyName.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyName = med.PharmacyName;
                    }
                    else
                    {
                        viewData.PharmacyName = viewData.Patient.PharmacyName;
                    }
                    if (med.PharmacyPhone.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyPhone = med.PharmacyPhone;
                    }
                    else
                    {
                        viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                    }
                }
                var currentEpisode = patientRepository.GetCurrentEpisode(Current.AgencyId, PatientId);
                if (currentEpisode != null)
                {
                    viewData.EpisodeId = currentEpisode.Id;
                    viewData.StartDate = currentEpisode.StartDate;
                    viewData.EndDate = currentEpisode.EndDate;
                    if (currentEpisode.AssessmentId != Guid.Empty)
                    {
                        var diagnosis = assessmentService.Diagnosis(currentEpisode.AssessmentId, currentEpisode.AssessmentType);
                        if (diagnosis != null)
                        {
                            diagnosis.Merge<string, Question>(assessmentService.Allergies(currentEpisode.AssessmentId, currentEpisode.AssessmentType));
                            viewData.Questions = diagnosis;
                        }
                        else
                        {
                            viewData.Questions = assessmentService.Allergies(currentEpisode.AssessmentId, currentEpisode.AssessmentType);
                        }
                    }
                }
            }
            return PartialView("MedicationProfileSnapShot", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Demand(Permissions.ViewMedicationProfile)]
        public ActionResult MedicationProfile(Guid PatientId)
        {
            var med = patientRepository.GetMedicationProfileByPatient(PatientId, Current.AgencyId);
            var viewData = new MedicationProfileViewData();
            if (med != null)
            {
                viewData.MedicationProfile = med;
                viewData.Patient = patientRepository.Get(PatientId, Current.AgencyId);
                if (viewData.Patient != null)
                {
                    if (viewData.Patient.PhysicianContacts != null && viewData.Patient.PhysicianContacts.Count > 0)
                    {
                        var physicain = viewData.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physicain != null)
                        {
                            viewData.PhysicianId = physicain.Id;
                        }
                    }
                    if (med.PharmacyName.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyName = med.PharmacyName;
                    }
                    else
                    {
                        viewData.PharmacyName = viewData.Patient.PharmacyName;
                    }
                    if (med.PharmacyPhone.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyPhone = med.PharmacyPhone;
                    }
                    else
                    {
                        viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                    }
                }
                var currentEpisode = patientRepository.GetCurrentEpisode(Current.AgencyId, PatientId);
                if (currentEpisode != null)
                {
                    viewData.EpisodeId = currentEpisode.Id;
                    viewData.StartDate = currentEpisode.StartDate;
                    viewData.EndDate = currentEpisode.EndDate;
                    if (currentEpisode.AssessmentId != Guid.Empty)
                    {
                        var diagnosis = assessmentService.Diagnosis(currentEpisode.AssessmentId, currentEpisode.AssessmentType);
                        if (diagnosis != null)
                        {
                            diagnosis.Merge<string, Question>(assessmentService.Allergies(currentEpisode.AssessmentId, currentEpisode.AssessmentType));
                            viewData.Questions = diagnosis;
                        }
                        else
                        {
                            viewData.Questions = assessmentService.Allergies(currentEpisode.AssessmentId, currentEpisode.AssessmentType);
                        }
                    }
                }
            }
            return PartialView("MedicationProfile", viewData);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewPatientProfile)]
        public ActionResult MedicationProfilePrint(Guid id)
        {
            var med = patientRepository.GetMedicationProfileByPatient(id, Current.AgencyId);
            var viewData = new MedicationProfileViewData();
            if (med != null)
            {
                viewData.MedicationProfile = med;
                viewData.Patient = patientRepository.Get(id, Current.AgencyId);
                viewData.Agency = agencyRepository.Get(Current.AgencyId);
                if (viewData.Patient != null)
                {
                    if (viewData.Patient.PhysicianContacts != null && viewData.Patient.PhysicianContacts.Count > 0)
                    {
                        var physicain = viewData.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physicain != null)
                        {
                            viewData.PhysicianId = physicain.Id;
                            viewData.PhysicianDisplayName = physicain.FirstName + " " + physicain.LastName;
                        }
                    }
                    if (med.PharmacyName.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyName = med.PharmacyName;
                    }
                    else
                    {
                        viewData.PharmacyName = viewData.Patient.PharmacyName;
                    }
                    if (med.PharmacyPhone.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyPhone = med.PharmacyPhone;
                    }
                    else
                    {
                        viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                    }
                }
                var currentEpisode = patientRepository.GetCurrentEpisode(Current.AgencyId, id);
                if (currentEpisode != null)
                {
                    viewData.EpisodeId = currentEpisode.Id;
                    viewData.StartDate = currentEpisode.StartDate;
                    viewData.EndDate = currentEpisode.EndDate;
                    if (currentEpisode.AssessmentId != Guid.Empty)
                    {
                        var diagnosis = assessmentService.Diagnosis(currentEpisode.AssessmentId, currentEpisode.AssessmentType);
                        if (diagnosis != null)
                        {
                            diagnosis.Merge<string, Question>(assessmentService.Allergies(currentEpisode.AssessmentId, currentEpisode.AssessmentType));
                            viewData.Questions = diagnosis;
                        }
                        else
                        {
                            viewData.Questions = assessmentService.Allergies(currentEpisode.AssessmentId, currentEpisode.AssessmentType);
                        }
                    }
                }
            }
            return View("MedicationProfilePrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Demand(Permissions.ViewPatientProfile)]
        public ActionResult MedicationProfileSnapshotPrint(Guid id)
        {
            var med = patientRepository.GetMedicationProfileHistory(id, Current.AgencyId);
            var viewData = new MedicationProfileViewData();
            if (med != null)
            {
                viewData.MedicationProfile = med.ToProfile();
                viewData.Patient = patientRepository.Get(med.PatientId, Current.AgencyId);
                viewData.Agency = agencyRepository.Get(Current.AgencyId);
                if (viewData.Patient != null)
                {
                    if (viewData.Patient.PhysicianContacts != null && viewData.Patient.PhysicianContacts.Count > 0)
                    {
                        var physician = viewData.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physician != null)
                        {
                            viewData.PhysicianId = physician.Id;
                            viewData.PhysicianDisplayName = physician.FirstName + " " + physician.LastName;
                        }
                    }
                    if (med.PharmacyName.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyName = med.PharmacyName;
                    }
                    else
                    {
                        viewData.PharmacyName = viewData.Patient.PharmacyName;
                    }
                    if (med.PharmacyPhone.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyPhone = med.PharmacyPhone;
                    }
                    else
                    {
                        viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                    }
                }
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, med.EpisodeId, med.PatientId);
                if (episode != null)
                {
                    viewData.EpisodeId = episode.Id;
                    viewData.StartDate = episode.StartDate;
                    viewData.EndDate = episode.EndDate;
                    if (episode.AssessmentId != Guid.Empty)
                    {
                        var diagnosis = assessmentService.Diagnosis(episode.AssessmentId, episode.AssessmentType);
                        if (diagnosis != null)
                        {
                            diagnosis.Merge<string, Question>(assessmentService.Allergies(episode.AssessmentId, episode.AssessmentType));
                            viewData.Questions = diagnosis;
                        }
                        else
                        {
                            viewData.Questions = assessmentService.Allergies(episode.AssessmentId, episode.AssessmentType);
                        }
                    }
                }
            }
            return View("MedicationProfilePrint", viewData);
        }

        #endregion
    }
}
