﻿namespace Axxess.AgencyManagement.App.Components
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class HosptialExporter : BaseExporter
    {
        private IList<AgencyHospital> hospitals;
        public HosptialExporter(IList<AgencyHospital> hospitals)
            : base()
        {
            this.hospitals = hospitals;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Hospitals";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("Hospitals");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Row headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Contact Last Name");
            headerRow.CreateCell(2).SetCellValue("Contact First Name");
            headerRow.CreateCell(3).SetCellValue("Address 1");
            headerRow.CreateCell(4).SetCellValue("Address 2");
            headerRow.CreateCell(5).SetCellValue("City");
            headerRow.CreateCell(6).SetCellValue("State");
            headerRow.CreateCell(7).SetCellValue("Zip Code");
            headerRow.CreateCell(8).SetCellValue("Phone Number");
            headerRow.CreateCell(9).SetCellValue("Fax Number");
            headerRow.RowStyle = headerStyle;

            if (this.hospitals.Count > 0)
            {
                int i = 1;
                this.hospitals.ForEach(h =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(h.Name);
                    dataRow.CreateCell(1).SetCellValue(h.ContactPersonLastName);
                    dataRow.CreateCell(2).SetCellValue(h.ContactPersonFirstName);
                    dataRow.CreateCell(3).SetCellValue(h.AddressLine1);
                    dataRow.CreateCell(4).SetCellValue(h.AddressLine2);
                    dataRow.CreateCell(5).SetCellValue(h.AddressCity);
                    dataRow.CreateCell(6).SetCellValue(h.AddressStateCode);
                    dataRow.CreateCell(7).SetCellValue(h.AddressZipCode);
                    dataRow.CreateCell(8).SetCellValue(h.Phone.ToPhone());
                    dataRow.CreateCell(9).SetCellValue(h.FaxNumber.ToPhone());
                    i++;
                });
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
        }
    }
}
