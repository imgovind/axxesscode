﻿namespace Axxess.AgencyManagement.App.Components
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class UserExporter : BaseExporter
    {
        private IList<User> users;
        public UserExporter(IList<User> users)
            : base()
        {
            this.users = users;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employees";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            Sheet sheet = base.workBook.CreateSheet("Employees");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Row headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Id");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("Suffix");
            headerRow.CreateCell(3).SetCellValue("First Name");
            headerRow.CreateCell(4).SetCellValue("Home Phone");
            headerRow.CreateCell(5).SetCellValue("Mobile Phone");
            headerRow.CreateCell(6).SetCellValue("Address 1");
            headerRow.CreateCell(7).SetCellValue("Address 2");
            headerRow.CreateCell(8).SetCellValue("City");
            headerRow.CreateCell(9).SetCellValue("State");
            headerRow.CreateCell(10).SetCellValue("Zip Code");
            headerRow.CreateCell(11).SetCellValue("Gender");

            headerRow.RowStyle = headerStyle;

            if (this.users.Count > 0)
            {
                int i = 1;
                this.users.ForEach(u =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(u.CustomId);
                    dataRow.CreateCell(1).SetCellValue(u.LastName.ToTitleCase());
                    dataRow.CreateCell(2).SetCellValue(u.Suffix);
                    dataRow.CreateCell(3).SetCellValue(u.FirstName.ToTitleCase());
                    dataRow.CreateCell(4).SetCellValue(u.Profile.PhoneHome.ToPhone());
                    dataRow.CreateCell(5).SetCellValue(u.Profile.PhoneMobile.ToPhone());
                    dataRow.CreateCell(6).SetCellValue(u.Profile.AddressLine1.ToTitleCase());
                    dataRow.CreateCell(7).SetCellValue(u.Profile.AddressLine2.ToTitleCase());
                    dataRow.CreateCell(8).SetCellValue(u.Profile.AddressCity.ToTitleCase());
                    dataRow.CreateCell(9).SetCellValue(u.Profile.AddressStateCode);
                    dataRow.CreateCell(10).SetCellValue(u.Profile.AddressZipCode);
                    dataRow.CreateCell(11).SetCellValue(u.Profile.Gender);
                    i++;
                });
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
        }
    }
}
