﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;

    using Enums;

    public interface IMembershipService
    {
        void LogOff(string userName);
        bool ResetPassword(string userName);
        AxxessPrincipal Get(string userName);
        LoginAttempt Validate(string userName, string password);
        bool Impersonate(string userName, Guid agencyId, Guid userId);
        bool ChangePassword(string userName, string oldPassword, string newPassword);
    }
}
