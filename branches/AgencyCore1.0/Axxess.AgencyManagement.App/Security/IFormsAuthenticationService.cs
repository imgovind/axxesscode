﻿namespace Axxess.AgencyManagement.App.Security
{
    public interface IFormsAuthenticationService
    {
        string LoginUrl { get; }
        void SignIn(string userName, bool redirect);
        void SignOut();
    }
}
