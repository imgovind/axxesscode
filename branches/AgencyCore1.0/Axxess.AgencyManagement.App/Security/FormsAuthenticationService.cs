﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Web.Security;

    using Axxess.Core.Infrastructure;

    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        #region IFormsAuthenticationService Members

        public string LoginUrl
        {
            get
            {
                return FormsAuthentication.LoginUrl;
            }
        }

        public void SignIn(string userName, bool rememberMe)
        {
            FormsAuthentication.Initialize();

            if (rememberMe)
            {
                FormsAuthentication.SetAuthCookie(userName, rememberMe);
            }
            else
            {
                FormsAuthentication.SetAuthCookie(userName, AppSettings.UsePersistentCookies);
            }
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        #endregion
    }
}
