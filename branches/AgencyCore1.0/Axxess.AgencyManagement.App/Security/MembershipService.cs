﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Web;
    using System.Threading;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Domain;

    public class MembershipService : IMembershipService
    {
        #region Private Members

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;

        #endregion

        #region Constructor

        public MembershipService(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementProvider.UserRepository;
            this.agencyRepository = agencyManagementProvider.AgencyRepository;
        }

        #endregion

        #region IMembershipService Members

        public AxxessPrincipal Get(string userName)
        {
            Check.Argument.IsNotEmpty(userName, "userName");

            AxxessPrincipal principal = Cacher.Get<AxxessPrincipal>(userName);
            if (principal == null)
            {
                Login login = loginRepository.Find(userName);
                AxxessIdentity identity = new AxxessIdentity(login.Id, login.EmailAddress)
                {
                    DisplayName = login.DisplayName
                };

                identity.IsAxxessAdmin = login.IsAxxessAdmin;

                var permissionsArray = new List<string>();
                Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);

                if (!login.IsAxxessAdmin)
                {
                    if (role == Roles.ApplicationUser)
                    {
                        var user = userRepository.GetByLoginId(login.Id);

                        if (user != null)
                        {
                            var agency = agencyRepository.Get(user.AgencyId);
                            identity.Session =
                                new UserSession
                                {
                                    UserId = user.Id,
                                    LoginId = login.Id,
                                    AgencyId = agency.Id,
                                    AgencyName = agency.Name,
                                    FullName = user.DisplayName,
                                    SignatureSalt = login.SignatureSalt,
                                    SignatureHash = login.SignatureHash,
                                    AgencyRoles = user.Roles.IsNotNullOrEmpty() ? user.Roles : string.Empty
                                };

                            permissionsArray = user.PermissionsArray;
                        }
                    }
                }

                if (login.IsPasswordChangeRequired)
                {
                    identity.ChangePassword = login.IsPasswordChangeRequired;
                }


                principal = new AxxessPrincipal(identity, role, permissionsArray);
                Cacher.Set<AxxessPrincipal>(userName, principal);
            }

            return principal;
        }

        public LoginAttempt Validate(string userName, string password)
        {
            LoginAttempt loginAttempt = LoginAttempt.Failed;

            Login login = loginRepository.Find(userName);

            if (login != null)
            {
                if (login.IsLocked)
                {
                    loginAttempt = LoginAttempt.Locked;
                }
                else
                {
                    if (login.IsActive)
                    {
                        var saltedHash = new SaltedHash();
                        if (saltedHash.VerifyHashAndSalt(password, login.PasswordHash, login.PasswordSalt))
                        {
                            AxxessIdentity identity = new AxxessIdentity(login.Id, login.EmailAddress)
                            {
                                DisplayName = login.DisplayName
                            };

                            identity.IsAxxessAdmin = login.IsAxxessAdmin;

                            var permissionsArray = new List<string>();
                            Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                            if (!login.IsAxxessAdmin)
                            {
                                if (role == Roles.ApplicationUser)
                                {
                                    var user = userRepository.GetByLoginId(login.Id);
                                    if (user != null)
                                    {
                                        var agency = agencyRepository.Get(user.AgencyId);
                                        identity.Session =
                                            new UserSession
                                            {
                                                UserId = user.Id,
                                                LoginId = login.Id,
                                                AgencyId = agency.Id,
                                                AgencyName = agency.Name,
                                                FullName = user.DisplayName,
                                                SignatureSalt = login.SignatureSalt,
                                                SignatureHash = login.SignatureHash,
                                                AgencyRoles = user.Roles.IsNotNullOrEmpty() ? user.Roles : string.Empty
                                            };
                                        permissionsArray = user.PermissionsArray;
                                    }
                                }
                            }
                            
                            if (login.IsPasswordChangeRequired)
                            {
                                identity.ChangePassword = login.IsPasswordChangeRequired;
                                loginAttempt = LoginAttempt.ChangePasswordRequired;
                            }
                            else
                            {
                                loginAttempt = LoginAttempt.Success;
                            }

                            var principal = new AxxessPrincipal(identity, role, permissionsArray);
                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                            Cacher.Set<AxxessPrincipal>(userName, principal);
                        }
                        else
                        {
                            loginAttempt = LoginAttempt.Failed;
                        }
                    }
                    else
                    {
                        loginAttempt = LoginAttempt.Deactivated;
                    }
                }
            }

            return loginAttempt;
        }

        public bool Impersonate(string userName, Guid agencyId, Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            Check.Argument.IsNotEmpty(userName, "userName");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            AxxessPrincipal principal = null;
            if (Cacher.TryGet<AxxessPrincipal>(userName, out principal))
            {
                LogOff(userName);
            }

            var user = userRepository.Get(userId);

            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                AxxessIdentity identity = new AxxessIdentity(login.Id, login.EmailAddress)
                {
                    DisplayName = login.DisplayName
                };

                identity.IsAxxessAdmin = false;

                var permissionsArray = new List<string>();
                Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);

                if (role == Roles.ApplicationUser && !login.IsAxxessAdmin)
                {
                    var agency = agencyRepository.Get(agencyId);
                    identity.Session =
                        new UserSession
                        {
                            UserId = user.Id,
                            LoginId = login.Id,
                            AgencyId = agency.Id,
                            AgencyName = agency.Name,
                            FullName = user.DisplayName
                        };

                    permissionsArray = user.PermissionsArray;
                }

                if (login.IsPasswordChangeRequired)
                {
                    identity.ChangePassword = login.IsPasswordChangeRequired;
                }

                principal = new AxxessPrincipal(identity, role, permissionsArray);
                Cacher.Set<AxxessPrincipal>(userName, principal);
                return true;
            }

            return false;
        }

        public bool ResetPassword(string userName)
        {
            bool valid = false;

            Login login = loginRepository.Find(userName);
            if (login != null)
            {
                string newSalt = string.Empty;
                string newPasswordHash = string.Empty;
                string newPassword = ShortGuid.NewId().ToString();

                var saltedHash = new SaltedHash();
                saltedHash.GetHashAndSalt(newPassword, out newPasswordHash, out newSalt);
                if (loginRepository.ChangePassword(login.Id, newPasswordHash, newSalt, true))
                {
                    var bodyText = MessageBuilder.PrepareTextFrom("PasswordResetInstructions", "password", newPassword);
                    Notify.User(AppSettings.NoReplyEmail, userName, "Reset Password for Axxess Home Health Management Software", bodyText);
                        
                    valid = true;
                }
            }
            return valid;
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            bool valid = false;

            Login login = loginRepository.Find(userName);

            if (login != null)
            {
                var saltedHash = new SaltedHash();
                if (saltedHash.VerifyHashAndSalt(oldPassword, login.PasswordHash, login.PasswordSalt))
                {
                    string newSalt = string.Empty;
                    string newPasswordHash = string.Empty;

                    saltedHash.GetHashAndSalt(newPassword, out newPasswordHash, out newSalt);
                    if (loginRepository.ChangePassword(Current.User.Id, newPasswordHash, newSalt, false))
                    {
                        AxxessPrincipal principal = null;
                        if (Cacher.TryGet<AxxessPrincipal>(userName, out principal))
                        {
                            AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                            identity.ChangePassword = false;
                            Cacher.Set<AxxessPrincipal>(userName, principal);
                            valid = true;
                        }
                    }
                }
            }

            return valid;
        }

        public void LogOff(string userName)
        {
            SessionStore.Abandon();
            Cacher.Remove(userName);
        }

        #endregion
    }
}
