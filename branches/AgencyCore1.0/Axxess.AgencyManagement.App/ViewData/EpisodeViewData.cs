﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Domain;

    public class EpisodeViewData
    {
        public Guid Id { get; set; }

        public Guid PatientId { get; set; }

        public bool IsNext { get; set; }

        public Guid NextId { get; set; }

        public bool IsPrevious { get; set; }

        public Guid PreviousId { get; set; }

        public string StartDate { get; set; }

        public string StartDateFormatted { get { return DateTime.Parse(this.StartDate).ToShortDateString(); } }
        
        public string EndDate { get; set; }

        public string Schedule { get; set; }

        public string EndDateFormatted { get { return DateTime.Parse(this.EndDate).ToShortDateString(); } }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string ScheduleEvent
        {
            get
            {
                if (this.Schedule == null)
                {
                    return string.Empty;
                }
                return JsonSerializer.Serialize(this.Schedule.ToString().ToObject<List<ScheduleEvent>>());
            }
        }
       
    }
}
