﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.ViewData
{
   public class EpisodeDateViewData
    {
       public DateTime StartDate { get; set; }
       public DateTime EndDate { get; set; }
    }
}
