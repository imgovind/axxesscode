﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Enums;

namespace Axxess.AgencyManagement.App.ViewData
{
    public class ClaimViewData
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string PatientIdNumber { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public bool IsOasisComplete { get; set; }
        public bool IsFirstBillableVisit { get; set; }
        public DateTime FirstBillableVisitDate { get; set; }
        public bool IsGenerated { get; set; }
        public bool IsVerified { get; set; }
        public DateTime Modified { get; set; }
        public string Remark { get; set; }
        public DateTime Created { get; set; }
        public string MedicareNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public DateTime StartofCareDate { get; set; }
        public string PhysicianNPI { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }
        public string DiagonasisCode { get; set; }
        public string HippsCode { get; set; }
        public string ClaimKey { get; set; }
        public bool AreOrdersComplete { get; set; }
        public int PrimaryInsuranceId { get; set; }
        public int Status { get; set; }
        public DateTime PaymentDate { get; set; }
        public string StatusName
        {
            get
            {
                return EnumExtensions.GetDescription((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), this.Status));

            }
        }

        public string EpisodeRange
        {
            get
            {
                return string.Format("{0}-{1}", this.EpisodeStartDate != null ? this.EpisodeStartDate.ToShortDateString() : "", this.EpisodeEndDate != null ? this.EpisodeEndDate.ToShortDateString() : "");
            }
        }
    }
}
