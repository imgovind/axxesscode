﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Web;

    public class NoteViewData : JsonViewData
    {
       public Guid Id { get; set; }
       public Guid PatientId { get; set; }
       public string Note { get; set; }
    }
}
