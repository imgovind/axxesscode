﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    public class PatientAgencyViewData : PatientViewData
    {
        public string CmsCertificatioNumber { get; set; }
        public string BranchState { get; set; }
        public string BranchIdNumber { get; set; }
        public string NpiNumber { get; set; }
    }
}
 