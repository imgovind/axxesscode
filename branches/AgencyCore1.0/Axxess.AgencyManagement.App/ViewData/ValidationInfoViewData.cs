﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;

    using System.Collections.Generic;

    using Axxess.Api.Contracts;


    public class ValidationInfoViewData
    {
      public Guid AssessmentId { get; set; }
      public Guid PatientId { get; set; }
      public Guid EpisodeId { get; set; }
      public string AssessmentType { get; set; }
      public List<ValidationError> validationError { get; set; }
      public int Count { get; set; }
      public string Message { get; set; }
      public string HIPPSCODE { get; set; }
      public string HIPPSKEY { get; set; }

    }
}
