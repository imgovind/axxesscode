﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;

    public class ActivityViewData
    {
       public Guid Id { get; set; }
       public Guid PatientId { get; set; }
       public DateTime? Date { get; set; }
       public string Type { get; set; }
       public string Status { get; set; }
       public string User { get; set; }
       public string Category { get; set; }
       public DateTime? TargetDate { get; set; }
       public Guid MixedId { get; set; }
       public int? Module { get; set; }
       public string URL { get; set; }
       public bool IsDeprecated { get; set; }
    }
}
