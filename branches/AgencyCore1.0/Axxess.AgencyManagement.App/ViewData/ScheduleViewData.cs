﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;

    public class ScheduleViewData
    {
        public List<PatientSelection> Patients { get; set; }
        public PatientEpisode Episode { get; set; }
    }
}
