﻿namespace Axxess.AgencyManagement.App.Enums
{
    public enum OrderType : byte
    {
        PhysicianOrder = 1,
        HCFA485 = 2,
        HCFA486 = 3
    }
}
