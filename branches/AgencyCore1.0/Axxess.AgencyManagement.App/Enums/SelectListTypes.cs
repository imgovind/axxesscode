﻿namespace Axxess.AgencyManagement.App.Enums
{
    public enum SelectListTypes
    {
        Users,
        Tasks,
        States,
        Branches,
        Patients,
        Insurance,
        Physicians,
        TitleTypes,
        ContactTypes,
        AdmissionSources,
        PaymentSource,
        CredentialTypes
    }
}
