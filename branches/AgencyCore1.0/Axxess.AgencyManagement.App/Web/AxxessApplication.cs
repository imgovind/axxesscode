﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Threading;
    using System.Web.Routing;

    using Security;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    public class AxxessApplication : HttpApplication
    {
        public AxxessApplication()
        {
            this.AuthenticateRequest += new EventHandler(AxxessApplication_AuthenticateRequest);
        }

        protected void AxxessApplication_AuthenticateRequest(object sender, EventArgs e)
        {
            if (Context.User != null)
            {
                string username = HttpContext.Current.User.Identity.Name;
                IMembershipService membershipService = Container.Resolve<IMembershipService>();
                AxxessPrincipal principal = membershipService.Get(username);

                Thread.CurrentPrincipal = principal;
                HttpContext.Current.User = principal;
            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.Run();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                var error = new Error();
                error.Type = "Application";
                error.Message = exception.Message;
                error.Details = exception.ToExceptionXml();
                error.Server = Environment.MachineName;
                if (exception is HttpException)
                {
                    HttpException httpException = exception as HttpException;
                    error.Type = httpException.GetHttpCode().ToString();
                }
                var errorRepository = Container.Resolve<IMembershipDataProvider>().ErrorRepository;
                errorRepository.Add(error);
            }
        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            if (context.Request.IsAuthenticated && custom == "Agency")
            {
                return Current.AgencyId.ToString();
            }
            else
            {
                return base.GetVaryByCustomString(context, custom);
            }
        }
    }
}
