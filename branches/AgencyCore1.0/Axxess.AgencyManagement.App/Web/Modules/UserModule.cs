﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class UserModule : Module
    {
        public override string Name
        {
            get { return "User"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               "EditProfile",
               "Profile/Edit",
               new { controller = this.Name, action = "Profile", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "ForgotSignature",
               "Signature/Forgot",
               new { controller = this.Name, action = "ForgotSignature", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "EmailSignature",
               "Signature/Email",
               new { controller = this.Name, action = "EmailSignature", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "ResetSignature",
               "Signature/Reset",
               new { controller = this.Name, action = "ResetSignature", id = UrlParameter.Optional }
            );
        }
    }
}
