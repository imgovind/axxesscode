﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class DefaultModule : Module
    {
        public override string Name
        {
            get { return "Home"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.RouteExistingFiles = true;
            routes.IgnoreRoute("{file}.txt");
            routes.IgnoreRoute("{file}.htm");
            routes.IgnoreRoute("{file}.html");
            routes.IgnoreRoute("{handler}.axd");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("Scripts/{*pathInfo}");
            routes.IgnoreRoute("Images/{*pathInfo}");
            routes.IgnoreRoute("Content/{*pathInfo}");

            routes.MapRoute(
                "CacheTesting",
                "tool/cache/{data}",
                new { controller = "Tool", action = "Cache", data = UrlParameter.Optional });

            routes.MapRoute(
              "Error",
              "Error",
              new { controller = this.Name, action = "Error", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "Log",
              "Logs",
              new { controller = this.Name, action = "Logs", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "AdminHome",
              "Admin",
              new { controller = this.Name, action = "Admin", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                this.Name, // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }
    }
}
