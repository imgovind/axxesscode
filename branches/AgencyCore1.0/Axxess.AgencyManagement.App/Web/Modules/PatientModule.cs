﻿namespace Axxess.AgencyManagement.App.Web
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class PatientModule : Module
    {
        public override string Name
        {
            get { return "Patient"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               "NewPatient",
               "Patient/New/{referralId}",
               new { controller = this.Name, action = "New", referralId = UrlParameter.Optional }
            );

            routes.MapRoute(
               "NewOrder",
               "Order/New",
               new { controller = this.Name, action = "NewOrder", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "EditOrder",
               "Order/Edit",
               new { controller = this.Name, action = "EditOrder", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "UpdateOrder",
               "Order/Update",
               new { controller = this.Name, action = "UpdateOrder", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "AddOrder",
               "Order/Add",
               new { controller = this.Name, action = "AddOrder", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              "ViewOrder",
              "Order/View/{episodeId}/{patientId}/{orderId}",
              new { controller = this.Name, action = "ViewOrder", episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() }
            );

            routes.MapRoute(
              "PatientProfile",
              "Patient/Profile/{id}",
              new { controller = this.Name, action = "Profile", id = new IsGuid() }
            );

            routes.MapRoute(
              "PatientPhoto",
              "Patient/NewPhoto/{patientId}",
              new { controller = this.Name, action = "NewPhoto", patientId = new IsGuid() }
            );

            routes.MapRoute(
             "MedicationProfilePrint",
             "MedicationProfile/View/{patientId}",
             new { controller = this.Name, action = "MedicationProfilePrint", patientId = new IsGuid() });

            routes.MapRoute(
              "NewCommunicationNote",
              "CommunicationNote/New",
              new { controller = this.Name, action = "NewCommunicationNote", patientId = UrlParameter.Optional }
           );

            routes.MapRoute(
               "AddCommunicationNote",
               "CommunicationNote/Add",
               new { controller = this.Name, action = "AddCommunicationNote", id = UrlParameter.Optional }
            );

        }
    }
}
