﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Domain;

    public static class VisitNoteExtensions
    {
        public static IDictionary<string, NotesQuestion> ToNotesQuestionDictionary(this Assessment assessment)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (assessment.Questions != null)
            {
                assessment.Questions.ForEach(question =>
                {
                    if (!questions.ContainsKey(question.Name))
                    {
                        questions.Add(question.Name, new NotesQuestion { Name = question.Name, Answer = question.Answer });
                    }
                });
            }
            return questions;
        }
    }
}

