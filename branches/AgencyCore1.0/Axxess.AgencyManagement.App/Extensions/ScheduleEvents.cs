﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;

    public static class ScheduleEvents
    {
        public static bool IsAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
                var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));

                foreach (string task in disciplineTasks)
                {
                    if (task.ToLowerInvariant().Contains("oasis") && disciplineTask.IsEqual(task))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsOpen(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.Status.IsNotNullOrEmpty())
            {
                ScheduleStatus status = (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status);
                if (status != ScheduleStatus.NoteCompleted || status != ScheduleStatus.OasisCompletedExportReady || status != ScheduleStatus.OasisExported || status != ScheduleStatus.OrderReturnedWPhysicianSignature || status != ScheduleStatus.ClaimAccepted)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOrderAndStatus(this ScheduleEvent scheduleEvent, int statusId)
        {
            if (scheduleEvent != null && scheduleEvent.Discipline.IsNotNullOrEmpty() && scheduleEvent.Status.IsNotNullOrEmpty())
            {
                Disciplines discipline = (Disciplines)Enum.Parse(typeof(Disciplines), scheduleEvent.Discipline);
                ScheduleStatus status = (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status);
                if (discipline == Disciplines.Orders && statusId == (int)status)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsCompleteRecertAssessment(this ScheduleEvent scheduleEvent)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
            {
                var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
                ScheduleStatus status = (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), scheduleEvent.Status);

                if (disciplineTask.ToLowerInvariant().Contains("recert") && (status == ScheduleStatus.OasisCompletedPendingReview || status == ScheduleStatus.OasisCompletedExportReady)) 
                {
                    return true;
                }
            }
            return false;
        }

        public static bool ContainsRecertAssessment(this List<ScheduleEvent> scheduleEvents)
        {
            var result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                    var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
                    if (disciplineTask.ToLowerInvariant().Contains("recert"))
                    {
                        result = true;
                        return;
                    }
                });
            }
            return result;
        }

        public static ScheduleEvent GetRecertAssessment(this List<ScheduleEvent> scheduleEvents)
        {
            var recert = new ScheduleEvent();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    var disciplineTasks = Enum.GetNames(typeof(DisciplineTasks));
                    var disciplineTask = Enum.GetName(typeof(DisciplineTasks), scheduleEvent.DisciplineTask);
                    if (disciplineTask.ToLowerInvariant().Contains("recert"))
                    {
                        recert = scheduleEvent;
                        return;
                    }
                });
            }
            return recert;
        }
    }
}
