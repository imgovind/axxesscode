﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Web;
    using System.Security.Principal;

    using OpenForum.Core.Models;

    using Enums;
    using Domain;
    using Security;

    using Axxess.Membership.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;

    public static class Current
    {
        public static Func<DateTime> Time = () => DateTime.UtcNow;
        public static Func<DateTime> EndofMonth = () => { return DateTime.Now.AddMonths(1).AddDays(-(DateTime.Now.Day)); };
        public static Func<DateTime> StartofMonth = () => { return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1); };

        public static ForumUser ForumUser
        {
            get
            {
                return new ForumUser(Current.UserId, Current.UserFullName, null, null);
            }
        }

        public static AxxessIdentity User
        {
            get
            {
                AxxessIdentity identity = null;
                if (HttpContext.Current.User is WindowsIdentity)
                {
                    throw new InvalidOperationException("Windows authentication is not supported.");
                }

                if (HttpContext.Current.User is AxxessPrincipal)
                {
                    AxxessPrincipal principal = (AxxessPrincipal)HttpContext.Current.User;
                    identity = (AxxessIdentity)principal.Identity;
                }

                return identity;
            }
        }

        public static Guid UserId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.UserId;
                }

                return Guid.Empty;
            }
        }

        public static Guid LoginId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.LoginId;
                }

                return Guid.Empty;
            }
        }

        public static Guid AgencyId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.AgencyId;
                }

                return Guid.Empty;
            }
        }

        public static string AgencyName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.AgencyName;
                }

                return string.Empty;
            }
        }

        public static string UserFullName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.FullName;
                }

                return string.Empty;
            }
        }

        public static bool HasRight(Permissions permission)
        {
            bool result = false;
            if (HttpContext.Current.User is WindowsIdentity)
            {
                throw new InvalidOperationException("Windows authentication is not supported.");
            }

            if (HttpContext.Current.User is AxxessPrincipal)
            {
                AxxessPrincipal principal = (AxxessPrincipal)HttpContext.Current.User;
                result = principal.HasPermission(permission);
            }
            return result;
        }

        public static bool IsAxxessAdmin
        {
            get
            {
                if (Current.User != null)
                {
                    return Current.User.IsAxxessAdmin;
                }
                return false;
            }
        }

        public static bool IsInRole(AgencyRoles roleId)
        {
            var result = false;
            if (roleId > 0)
            {
                var role = ((int)roleId).ToString();
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    var agencyRoles = User.Session.AgencyRoles.Split(';');
                    if (agencyRoles.Length > 0)
                    {
                        foreach (string agencyRole in agencyRoles)
                        {
                            if (role.IsEqual(agencyRole))
                            {
                                result = true;
                                break;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsAgencyAdmin
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsAgencyAdmin();
                }
                return false;
            }
        }

        public static bool IsDirectorOfNursing
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsDirectorOfNursing();
                }
                return false;
            }
        }

        public static bool IsClinician
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsClinician();
                }
                return false;
            }
        }

        public static bool IsClinicianOrHHA
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsClinicianOrHHA();
                }
                return false;
            }
        }

        public static bool IsCaseManager
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsCaseManager();
                }
                return false;
            }
        }

        public static bool IsBiller
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsBiller();
                }
                return false;
            }
        }

        public static bool IsClerk
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsClerk();
                }
                return false;
            }
        }

        public static bool IsScheduler
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsScheduler();
                }
                return false;
            }
        }

        public static bool IsQA
        {
            get
            {
                if (User != null && User.Session != null && User.Session.AgencyRoles != null)
                {
                    return User.Session.AgencyRoles.IsQA();
                }
                return false;
            }
        }
    }
}
