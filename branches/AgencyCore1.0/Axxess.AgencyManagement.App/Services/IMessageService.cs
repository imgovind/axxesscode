﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;

    using Axxess.AgencyManagement.Domain;

    public interface IMessageService
    {
        bool SendMessage(Message message);
    }
}
