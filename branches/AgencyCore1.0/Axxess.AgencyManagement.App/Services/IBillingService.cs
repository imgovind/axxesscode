﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    using ViewData;

    using Axxess.AgencyManagement.Domain;

    public interface IBillingService
    {
        string Generate(List<int> rapToGenerate, List<int> finalToGenerate);
        bool VisitVerify(int Id, Guid episodeId, Guid patientId, List<Guid> Visit);
        IList<ClaimViewData> Activity(Guid patientId);
        IList<ClaimViewData> PendingClaims();

        IList<TypeOfBill> GetAllUnProcessedBill();
    }
}
