﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Drawing.Drawing2D;

    using Domain;
    using ViewData;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.Extensions;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    public class PatientService : IPatientService
    {
        #region Private Members

        private IUserRepository userRepository;
        private IAssetRepository assetRepository;
        private IAgencyRepository agencyRepository;
        private ILookupRepository lookupRepository;
        private IAssessmentService assessmentService;
        private IPatientRepository patientRepository;
        private IBillingRepository billingRepository;
        private IReferralRepository referrralRepository;
        private IPhysicianRepository physicianRepository;

        #endregion

        #region Constructor

        public PatientService(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, IAssessmentService assessmentService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.assessmentService = assessmentService;

            this.lookupRepository = lookupDataProvider.LookUpRepository;

            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.assetRepository = agencyManagementDataProvider.AssetRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.referrralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region IPatientService Members

        public PatientEpisode CreateEpisode(Guid patientId, DateTime startDate, ScheduleEvent scheduleEvent)
        {
            var patientEpisode = new PatientEpisode();
            patientEpisode.Id = Guid.NewGuid();
            patientEpisode.AgencyId = Current.AgencyId;
            patientEpisode.PatientId = patientId;
            patientEpisode.StartDate = startDate;
            patientEpisode.EndDate = startDate.AddDays(59);
            var events = new List<ScheduleEvent>();
            if (scheduleEvent != null && !scheduleEvent.EventId.IsEmpty())
            {
                scheduleEvent.EpisodeId = patientEpisode.Id;
                events.Add(scheduleEvent);
            }
            patientEpisode.Schedule = events.ToXml();
            return patientEpisode;
        }

        public Rap CreateClaim(Patient patient, PatientEpisode episode)
        {
            return new Rap
                {
                    AgencyId = patient.AgencyId,
                    PatientId = patient.Id,
                    EpisodeId = episode.Id,
                    EpisodeStartDate = episode.StartDate,
                    EpisodeEndDate = episode.EndDate,
                    IsFirstBillableVisit = false,
                    IsOasisComplete = false,
                    PatientIdNumber = patient.PatientIdNumber,
                    IsGenerated = false,
                    MedicareNumber = patient.MedicareNumber,
                    FirstName = patient.FirstName,
                    LastName = patient.LastName,
                    DOB = patient.DOB,
                    Gender = patient.Gender,
                    AddressLine1 = patient.AddressLine1,
                    AddressLine2 = patient.AddressLine2,
                    AddressCity = patient.AddressCity,
                    AddressStateCode = patient.AddressStateCode,
                    AddressZipCode = patient.AddressZipCode,
                    StartofCareDate = patient.StartofCareDate,
                    AreOrdersComplete = false,
                    Status = (int)ScheduleStatus.ClaimCreated,
                    Created = DateTime.Now
                };
        }

        public bool CreateEpisodeAndClaims(Patient patient)
        {
            bool result = false;
            if (patient.StartofCareDate == patient.EpisodeStartDate)
            {
                var newEvent = new ScheduleEvent
                {
                    EventId = Guid.NewGuid(),
                    PatientId = patient.Id,
                    UserId = patient.UserId,
                    Discipline = Disciplines.Nursing.ToString(),
                    Status = ((int)ScheduleStatus.OasisNotYetDue).ToString(),
                    DisciplineTask = (int)DisciplineTasks.OASISCStartofCare,
                    EventDate = patient.EpisodeStartDate.ToShortDateString()
                };
                var episode = this.CreateEpisode(patient.Id, patient.EpisodeStartDate, newEvent);
                if (patientRepository.AddEpisode(episode))
                {
                    newEvent.EpisodeId = episode.Id;
                    this.ProcessSchedule(newEvent, patient, episode);
                    if (patient.UserId != Guid.Empty)
                    {
                        userRepository.UpdateEvent(Current.AgencyId, patient.Id, patient.UserId,
                            new UserEvent
                            {
                                EventId = newEvent.EventId,
                                PatientId = patient.Id,
                                UserId = patient.UserId,
                                EpisodeId = episode.Id,
                                DisciplineTask = (int)DisciplineTasks.OASISCStartofCare,
                                Status = ((int)ScheduleStatus.OasisNotYetDue).ToString(),
                                EventDate = patient.EpisodeStartDate.ToShortDateString()
                            });
                    }
                    var rap = this.CreateClaim(patient, episode);
                    if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
                    {
                        if (!patient.AgencyPhysicians[0].IsEmpty())
                        {
                            var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
                            if (physician != null)
                            {
                                rap.PhysicianNPI = physician.NPI;
                                rap.PhysicianFirstName = physician.FirstName;
                                rap.PhysicianLastName = physician.LastName;
                            }
                        }
                    }
                    rap.DiagonasisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                    billingRepository.AddRap(rap);
                }
                result = true;
            }
            else if (patient.EpisodeStartDate > patient.StartofCareDate)
            {
                var episode = this.CreateEpisode(patient.Id, patient.EpisodeStartDate, null);
                if (patientRepository.AddEpisode(episode))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool CreateMedicationProfile(Patient patient, Guid medId)
        {
            var medicationProfile = new MedicationProfile { AgencyId = Current.AgencyId, Id = medId, Medication = new List<Medication>().ToXml(), PharmacyName = patient.PharmacyName, PharmacyPhone = patient.PharmacyPhone, PatientId = patient.Id, Created = DateTime.Now, Modified = DateTime.Now };
            if (patientRepository.AddNewMedicationProfile(medicationProfile))
            {
                return true;
            }
            return false;
        }

        public void DeleteEpisodeAndClaims(Patient patient)
        {
            PatientEpisode episodeDeleted = null;
            patientRepository.DeleteEpisode(Current.AgencyId, patient, out episodeDeleted);
            if (episodeDeleted != null)
            {
                billingRepository.DeleteRap(Current.AgencyId, patient.Id, episodeDeleted.Id);
                billingRepository.DeleteFinal(Current.AgencyId, patient.Id, episodeDeleted.Id);
            }
        }

        public bool LinkPhysicians(Patient patient)
        {
            var result = true;
            if (patient != null && patient.AgencyPhysicians.Any())
            {
                int i = 0;
                bool isPrimary = false;
                foreach (Guid agencyPhysicianId in patient.AgencyPhysicians)
                {
                    if (i == 0)
                    {
                        isPrimary = true;
                    }
                    else
                    {
                        isPrimary = false;
                    }
                    if (!agencyPhysicianId.IsEmpty() && !physicianRepository.Link(patient.Id, agencyPhysicianId, isPrimary))
                    {
                        result = false;
                        break;
                    }
                    i++;
                }
            }
            return result;
        }

        public bool NewEmergencyContact(PatientEmergencyContact emergencyContact, Guid patientID)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyViewData");
            Check.Argument.IsNotEmpty(patientID, "patientID");
            Patient patient = patientRepository.GetPatient<Patient>(patientID, Current.AgencyId);
            bool result = false;
            if (patient != null)
            {
                emergencyContact.PatientId = patientID;

                if (patientRepository.AddEmergencyContact(emergencyContact))
                {
                    if (emergencyContact.IsPrimary)
                    {
                        patientRepository.SetPrimaryEmergencyContact(Current.AgencyId, patient.Id, emergencyContact.Id);
                    }

                    result = true;
                }
            }
            return result;
        }

        public bool NewPhysicianContact(AgencyPhysician physician, Guid patientId)
        {
            Check.Argument.IsNotNull(physician, "physician");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var patient = patientRepository.GetPatient<Patient>(patientId, Current.AgencyId);

            bool result = false;
            if (patient != null)
            {
                if (physicianRepository.Add(physician))
                {
                    if (physician.Primary)
                    {
                        physicianRepository.Link(patientId, physician.Id, true);
                    }
                    else
                    {
                        physicianRepository.Link(patientId, physician.Id, false);
                    }

                    result = true;
                }
            }
            return result;
        }

        public bool SendOrderElectronically(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");
            bool result = false;
            try
            {
                AgencyPhysician physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                if (physician != null && physician.EmailAddress != null)
                {
                    Notify.User(AppSettings.NoReplyEmail, physician.EmailAddress, order.Summary, order.Text);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public PatientSchedule GetPatientWithSchedule(Guid patientId, string discipline)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");

            var patientSchedule = patientRepository.GetPatient<PatientSchedule>(patientId, Current.AgencyId);
            if (patientSchedule != null)
            {
                patientSchedule.Episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline);
                return patientSchedule;
            }
            return null;
        }

        public bool UpdateEpisode(PatientEpisode episode, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;

            if (httpFiles.Count > 0)
            {
                foreach (string key in httpFiles.AllKeys)
                {
                    HttpPostedFileBase file = httpFiles.Get(key);
                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        BinaryReader binaryReader = new BinaryReader(file.InputStream);
                        var asset = new Asset
                        {
                            FileName = file.FileName,
                            AgencyId = Current.AgencyId,
                            ContentType = file.ContentType,
                            FileSize = file.ContentLength.ToString(),
                            Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                        };
                        if (assetRepository.Add(asset))
                        {
                            episode.Detail.Assets.Add(asset.Id);
                        }
                        else
                        {
                            assetsSaved = false;
                            break;
                        }
                    }
                }
            }
            episode.Details = episode.Detail.ToXml();
            if (assetsSaved && patientRepository.UpdateEpisode(Current.AgencyId, episode))
            {
                result = true;
            }
            return result;
        }

        public bool UpdateEpisode(ScheduleEvent scheduleEvent, Guid oldUserId)
        {
            Check.Argument.IsNotEmpty(oldUserId, "oldUserId");
            Check.Argument.IsNotNull(scheduleEvent, "scheduleEvent");

            bool result = false;
            try
            {
                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                {
                    userRepository.Reassign(Current.AgencyId, scheduleEvent, oldUserId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                //TODO Log Exception
                return result;
            }
            return result;
        }

        public bool UpdateEpisode(Guid episodeId, Guid patientId, string jsonString)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(jsonString, "jsonString");

            bool result = false;
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
            if (patient != null && episode != null)
            {
                var newEvents = JsonExtensions.DeserializeFromJson<List<ScheduleEvent>>(jsonString);
                newEvents.ForEach(ev =>
                {
                    ev.EventId = Guid.NewGuid();
                    ev.PatientId = patientId;
                    ev.EpisodeId = episodeId;
                    ev.EventDate = ev.EventDate.ToZeroFilled();
                    ev.StartDate = episode.StartDate;
                    ev.EndDate = episode.EndDate;
                    ProcessSchedule(ev, patient, episode);
                });
                if (patientRepository.UpdateEpisode(Current.AgencyId, episodeId, patientId, newEvents))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateEpisode(Guid episodeId, Guid PatientId, string DisciplineTask, string Discipline, Guid userId, bool IsBillable, DateTime StartDate, DateTime EndDate)
        {
            bool result = false;
            int dateDifference = EndDate.Subtract(StartDate).Days + 1;
            var newEvents = new List<ScheduleEvent>();
            for (int i = 0; i < dateDifference; i++)
            {
                newEvents.Add(new ScheduleEvent { EventId = Guid.NewGuid(), PatientId = PatientId, EpisodeId = episodeId, UserId = userId, DisciplineTask = int.Parse(DisciplineTask), Discipline = Discipline, EventDate = String.Format("{0:MM/dd/yyyy}", StartDate.AddDays(i)), IsBillable = IsBillable });
            }
            var patient = patientRepository.GetPatient<Patient>(PatientId, Current.AgencyId);
            var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, PatientId);
            newEvents.ForEach(ev =>
            {
                ProcessSchedule(ev, patient, episode);
            });
            if (patientRepository.UpdateEpisode(Current.AgencyId, episodeId, PatientId, newEvents))
            {
                result = true;
            }
            return result;
        }

        public bool UpdateEpisode(Guid episodeId, Guid patientId, ScheduleEvent scheduleEvent)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(scheduleEvent, "scheduleEvent");

            bool result = false;
            var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
            try
            {
                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                //TODO Log Exception
                return result;
            }
            return result;
        }

        public bool Reassign(Guid episodeId, Guid patientId, Guid eventId, Guid oldEmployeeId, Guid employeeId)
        {
            bool result = false;
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(employeeId, "employeeId");
            try
            {
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    if (patientRepository.Reassign(Current.AgencyId, episodeId, patientId, eventId, employeeId))
                    {
                        userRepository.Reassign(Current.AgencyId, scheduleEvent, employeeId);
                        result = true;
                    }
                }

            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        public bool DeleteSchedule(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId)
        {
            bool result = true;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                if (!patientRepository.DeleteScheduleEvent(Current.AgencyId, episodeId, patientId, eventId))
                {
                    result = false;
                }
                if (!employeeId.IsEmpty())
                {
                    if (!userRepository.DeleteScheduleEvent(patientId, eventId, employeeId))
                    {
                        result = false;
                    }
                }
            }
            else
            {
                result = false;
            }
            return result;
        }

        public bool AddPhoto(Guid patientId, HttpFileCollectionBase httpFiles)
        {
            var result = false;

            if (httpFiles.Count > 0)
            {
                HttpPostedFileBase file = httpFiles.Get("Photo1");
                if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                {
                    var photo = Image.FromStream(file.InputStream);

                    var resizedImageStream = ResizeAndEncodePhoto(photo, file.ContentType, false);
                    if (resizedImageStream != null)
                    {
                        var binaryReader = new BinaryReader(resizedImageStream);
                        var asset = new Asset
                        {
                            FileName = file.FileName,
                            AgencyId = Current.AgencyId,
                            ContentType = file.ContentType,
                            FileSize = file.ContentLength.ToString(),
                            Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                        };
                        if (assetRepository.Add(asset) && patientRepository.AddPhoto(patientId, Current.AgencyId, asset.Id))
                        {
                            result = true;
                        }
                    }

                }
            }
            return result;
        }

        public bool IsValidImage(HttpFileCollectionBase httpFiles)
        {
            var result = false;

            if (httpFiles.Count > 0)
            {
                HttpPostedFileBase file = httpFiles.Get("Photo1");
                if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                {
                    var fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
                    if (AppSettings.AllowedImageExtensions.IsNotNullOrEmpty())
                    {
                        var allowedExtensions = AppSettings.AllowedImageExtensions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        if (allowedExtensions != null && allowedExtensions.Length > 0)
                        {
                            allowedExtensions.ForEach(extension =>
                            {
                                if (fileExtension.IsEqual(extension))
                                {
                                    result = true;
                                    return;
                                }
                            });
                        }
                    }

                }
            }
            return result;
        }

        public PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patientEpisode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null)
            {
                var assessment = assessmentService.GetPlanofCareAssessment(episodeId, patientId);
                if (assessment != null)
                {
                    var tmpString = new List<string>();
                    if (assessment.ContainsKey("485SNFrequency") && assessment["485SNFrequency"].Answer != "")
                    {
                        tmpString.Add("SN Frequency:" + assessment["485SNFrequency"].Answer);
                    }
                    if (assessment.ContainsKey("485PTFrequency") && assessment["485PTFrequency"].Answer != "")
                    {
                        tmpString.Add("PT Frequency:" + assessment["485PTFrequency"].Answer);
                    }
                    if (assessment.ContainsKey("485OTFrequency") && assessment["485OTFrequency"].Answer != "")
                    {
                        tmpString.Add("OT Frequency:" + assessment["485OTFrequency"].Answer);
                    }
                    if (assessment.ContainsKey("485STFrequency") && assessment["485STFrequency"].Answer != "")
                    {
                        tmpString.Add("ST Frequency:" + assessment["485STFrequency"].Answer);
                    }
                    if (assessment.ContainsKey("485MSWFrequency") && assessment["485MSWFrequency"].Answer != "")
                    {
                        tmpString.Add("MSW Frequency:" + assessment["485MSWFrequency"].Answer);
                    }
                    if (assessment.ContainsKey("485HHAFrequency") && assessment["485HHAFrequency"].Answer != "")
                    {
                        tmpString.Add("HHA Frequency:" + assessment["485HHAFrequency"].Answer);
                    }
                    if (tmpString.Count > 0)
                    {
                        patientEpisode.Detail.FrequencyList = tmpString.ToArray().Join(", ") + ".";
                    }
                    else
                    {
                        patientEpisode.Detail.FrequencyList = "";
                    }
                }
            }
            return patientEpisode;
        }

        public bool UpdateScheduleEvent(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;

            if (httpFiles.Count > 0)
            {
                foreach (string key in httpFiles.AllKeys)
                {
                    HttpPostedFileBase file = httpFiles.Get(key);
                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        var binaryReader = new BinaryReader(file.InputStream);

                        var asset = new Asset
                        {
                            FileName = file.FileName,
                            AgencyId = Current.AgencyId,
                            ContentType = file.ContentType,
                            FileSize = file.ContentLength.ToString(),
                            Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                        };
                        if (assetRepository.Add(asset))
                        {
                            scheduleEvent.Assets.Add(asset.Id);
                        }
                        else
                        {
                            assetsSaved = false;
                            break;
                        }
                    }
                }
            }
            if (assetsSaved && patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
            {
                result = true;
            }
            return result;
        }

        public bool SaveWoundCare(FormCollection formCollection, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;

            string type = formCollection["Type"];
            Guid eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
            Guid episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
            Guid patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();
            var patientVisitNote = new PatientVisitNote();
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null)
                {
                    patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                }
            }
            if (patientVisitNote.Questions != null)
            {
                if (httpFiles.Count > 0)
                {
                    foreach (string key in httpFiles.AllKeys)
                    {
                        var keyArray = key.Split('_');
                        HttpPostedFileBase file = httpFiles.Get(key);
                        if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                        {
                            BinaryReader binaryReader = new BinaryReader(file.InputStream);

                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };

                            if (assetRepository.Add(asset))
                            {
                                if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                {
                                    patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = asset.Id.ToString();
                                }
                                else
                                {
                                    patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = asset.Id.ToString(), Type = keyArray[0] });
                                }
                            }
                            else
                            {
                                if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                {
                                    patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                                }
                                else
                                {
                                    patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                                }

                                assetsSaved = false;
                                break;
                            }
                        }
                        else
                        {
                            if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                            {
                                patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                            }
                            else
                            {
                                patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                            }
                        }
                    }
                }
                patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                patientVisitNote.IsWoundCare = true;
                if (assetsSaved && patientRepository.UpdateVisitNote(patientVisitNote))
                {
                    result = true;
                }
            }
            else { result = false; }
            return result;
        }

        public bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(name, "name");
            var result = false;
            var patientVisitNote = new PatientVisitNote();
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.WoundNote.IsNotNullOrEmpty())
                {
                    patientVisitNote.Questions = patientVisitNote.WoundNote.ToObject<List<NotesQuestion>>();
                    if (patientVisitNote.Questions.Exists(q => q.Name == name))
                    {
                        patientVisitNote.Questions.SingleOrDefault(q => q.Name == name).Answer = Guid.Empty.ToString();
                        patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (assetRepository.Delete(assetId))
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool SaveNotes(string button, FormCollection formCollection)
        {
            var result = false;
            string type = formCollection["Type"];

            Guid userId = Current.UserId;
            Guid eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
            Guid episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
            Guid patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();
            string date = formCollection.Get(string.Format("{0}_VisitDate", type)).ToDateTime().ToShortDateString();

            var shouldUpdateEpisode = false;

            var scheduleEvent = new ScheduleEvent();
            var userEvent = new UserEvent();
            var patientVisitNote = new PatientVisitNote();

            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                }
                patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null)
                {
                    patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                    patientVisitNote.Note = patientVisitNote.Questions.ToXml();

                    if (button == "Save")
                    {
                        if (!(patientVisitNote.Status == ((int)ScheduleStatus.NoteReturned)))
                        {
                            patientVisitNote.Status = ((int)ScheduleStatus.NoteSaved);
                        }
                        patientVisitNote.Modified = DateTime.Now;
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = patientVisitNote.Status.ToString();
                                scheduleEvent.EventDate = date;
                                if (userEvent != null)
                                {
                                    userEvent.Status = patientVisitNote.Status.ToString();
                                    userEvent.EventDate = date;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (button == "Submit")
                    {
                        patientVisitNote.Status = ((int)ScheduleStatus.NoteSubmittedWithSignature);
                        patientVisitNote.UserId = userId;
                        patientVisitNote.Modified = DateTime.Now;
                        patientVisitNote.SignatureDate = formCollection[type + "_SignatureDate"].ToDateTime();
                        patientVisitNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString();
                                scheduleEvent.EventDate = date;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString();
                                    userEvent.EventDate = date;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                 
                    if (shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.UpdateEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit });
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId, string reason)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;

            var scheduleEvent = new ScheduleEvent();
            var userEvent = new UserEvent();
            var patientVisitNote = new PatientVisitNote();

            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                }
                patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null)
                {
                    if (button == "Approve")
                    {
                        patientVisitNote.Status = ((int)ScheduleStatus.NoteCompleted);
                        patientVisitNote.Modified = DateTime.Now;
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.NoteCompleted).ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.NoteCompleted).ToString();
                                    userEvent.ReturnReason = string.Empty;
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (button == "Return")
                    {
                        patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                        patientVisitNote.Modified = DateTime.Now;
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.NoteReturned).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    if (shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.UpdateEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit,ReturnReason=scheduleEvent.ReturnReason });
                                result = true;
                            }
                        }
                    }
                }
            }
          
            return result;
        }

        


        //public bool UpdateMedicationProfile(string button, MedicationProfile medicationProfile)
        //{
        //    if (medicationProfile != null)
        //    {
        //        medicationProfile.AgencyId = Current.AgencyId;
        //        if (button == "Save")
        //        {
        //            medicationProfile.IsSigned = false;
        //            if (patientRepository.UpdateMedicationProfile(medicationProfile))
        //            {
        //                return true;
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //        else if (button == "Sign")
        //        {
        //            var medication = patientRepository.GetMedicationProfile(medicationProfile.Id, medicationProfile.AgencyId);
        //            medicationProfile.IsSigned = true;
        //            medication.Id = Guid.NewGuid();
        //            medication.IsSigned = false;
        //            medication.IsOasis = false;
        //            medication.Created = DateTime.Now;
        //            medication.Modified = DateTime.Now;
        //            medication.UserId = Guid.Empty;
        //            medication.SignedDate = DateTime.MinValue;
        //            if (patientRepository.AddNewMedicationProfile(medication))
        //            {
        //                if (patientRepository.UpdateMedicationProfile(medicationProfile))
        //                {
        //                    return true;
        //                }
        //                else
        //                {
        //                    patientRepository.DeleteMedicationProfile(medication.Id, Current.AgencyId);
        //                    return false;
        //                }
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public List<Medication> GetCurrentMedicationsByCategory(Guid patientId, string medicationCategory)
        {
            var medicationHistory = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            var medicationList = new List<Medication>();
            if (medicationHistory != null)
            {
                medicationList = medicationHistory.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == medicationCategory).ToList();
            }
            return medicationList;
        }

        public bool SignMedicationHistory(Guid medId, MedicationProfileHistory medicationProfile)
        {
            var medicationHistory = patientRepository.GetMedicationProfile(medId, Current.AgencyId);
            bool result = false;
            if (medicationHistory != null)
            {
                medicationProfile.Id = Guid.NewGuid();
                medicationProfile.AgencyId = Current.AgencyId;
                medicationProfile.IsSigned = true;
                medicationProfile.PharmacyName = medicationHistory.PharmacyName;
                medicationProfile.PharmacyPhone = medicationHistory.PharmacyPhone;
                medicationProfile.UserId = Current.UserId;
                medicationProfile.Created = DateTime.Now;
                medicationProfile.Modified = DateTime.Now;
                medicationProfile.Medication = medicationHistory.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == "Active").ToList().ToXml();
                result = patientRepository.AddNewMedicationHistory(medicationProfile);
            }
            return result;
        }

        public IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId)
        {
            var medProfile = patientRepository.GetMedicationHistoryForPatient(patientId, Current.AgencyId);
            if (medProfile != null)
            {
                medProfile.ForEach(m =>
                {
                    if (m.UserId != Guid.Empty)
                    {
                        var user = userRepository.Get(m.UserId, Current.AgencyId);
                        if (user != null)
                        {
                            m.UserName = user.FirstName + " " + user.LastName;
                        }
                    }
                }
                );
            }
            return medProfile;
        }

        public PatientProfile GetProfile(Guid id)
        {
            var patient = patientRepository.Get(id, Current.AgencyId);
            if (patient != null)
            {
                var patientProfile = new PatientProfile();
                patientProfile.Patient = patient;
                patientProfile.Agency = agencyRepository.Get(Current.AgencyId);
                if (patient.PhysicianContacts.Count > 0)
                {
                    patientProfile.Physician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                }
                if (!patient.CaseManagerId.IsEmpty())
                {
                    var caseManager = userRepository.Get(patient.CaseManagerId);
                    if (caseManager != null)
                    {
                        patient.CaseManagerName = caseManager.DisplayName;
                    }
                }

                SetInsurance(patient);

                var episode = patientRepository.GetCurrentEpisode(Current.AgencyId, id);
                if (episode != null)
                {
                    patientProfile.CurrentEpisode = episode;
                    if (patientProfile.CurrentEpisode != null)
                    {
                        if (!episode.AssessmentId.IsEmpty() && episode.AssessmentType.IsNotNullOrEmpty())
                        {
                            patientProfile.CurrentAssessment = assessmentService.GetAssessment(episode.AssessmentId, episode.AssessmentType);
                        }
                    }
                }
                return patientProfile;
            }

            return null;
        }

        private void SetInsurance(Patient patient)
        {
            if (patient.PrimaryInsurance.IsNotNullOrEmpty())
            {
                if (patient.PrimaryInsurance.Length == 36)
                {
                    var primaryInsurance = agencyRepository.GetInsurance(patient.PrimaryInsurance.ToInteger(), Current.AgencyId);
                    if (primaryInsurance != null)
                    {
                        patient.PrimaryInsuranceName = primaryInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.PrimaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.PrimaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
            if (patient.SecondaryInsurance.IsNotNullOrEmpty())
            {
                if (patient.SecondaryInsurance.Length == 36)
                {
                    var secondaryInsurance = agencyRepository.GetInsurance(patient.SecondaryInsurance.ToInteger(), Current.AgencyId);
                    if (secondaryInsurance != null)
                    {
                        patient.SecondaryInsuranceName = secondaryInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.SecondaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.SecondaryInsuranceName = standardInsurance.Name;
                    }
                }
            }

            if (patient.TertiaryInsurance.IsNotNullOrEmpty())
            {
                if (patient.TertiaryInsurance.Length == 36)
                {
                    var tertiaryInsurance = agencyRepository.GetInsurance(patient.TertiaryInsurance.ToInteger(), Current.AgencyId);
                    if (tertiaryInsurance != null)
                    {
                        patient.TertiaryInsuranceName = tertiaryInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.TertiaryInsurance.ToInteger());
                    if (standardInsurance != null)
                    {
                        patient.TertiaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
        }

        public bool AddMissedVisit(MissedVisit missedVisit)
        {
            bool result = false;
            var episode = patientRepository.GetEpisode(Current.AgencyId, missedVisit.EpisodeId, missedVisit.PatientId);
            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            {
                missedVisit.AgencyId = Current.AgencyId;
                var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                var scheduledEvent = events.Find(e => e.EventId == missedVisit.Id);
                if (scheduledEvent != null)
                {
                    scheduledEvent.IsMissedVisit = true;
                    var userEvent = userRepository.GetEvent(Current.AgencyId, scheduledEvent.UserId, scheduledEvent.PatientId, scheduledEvent.EventId);
                    if (userEvent != null)
                    {
                        userEvent.IsMissedVisit = true;
                    }
                    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent) && userRepository.UpdateEvent(userEvent) && patientRepository.AddMissedVisit(missedVisit))
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        public bool AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, int supplyId, string quantity, string date)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.Id == supplyId && s.Date == date))
                    {
                        var supply = supplies.SingleOrDefault(s => s.Id == supplyId && s.Date == date);
                        if (supply != null)
                        {
                            supply.Quantity = quantity;
                            patientVisitNote.Supply = supplies.ToXml();
                            patientVisitNote.IsSupplyExist = true;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        var supply = lookupRepository.GetSupply(supplyId);
                        if (supply != null)
                        {
                            supply.Date = date;
                            supply.Quantity = quantity;
                            supply.UniqueIdentifier = Guid.NewGuid();
                            supplies.Add(supply);
                            patientVisitNote.Supply = supplies.ToXml();
                            patientVisitNote.IsSupplyExist = true;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                            }
                        }
                    }
                }
                else
                {
                    var supply = lookupRepository.GetSupply(supplyId);
                    if (supply != null)
                    {
                        var newSupplies = new List<Supply>();
                        supply.Date = date;
                        supply.Quantity = quantity;
                        supply.UniqueIdentifier = Guid.NewGuid();
                        newSupplies.Add(supply);
                        patientVisitNote.Supply = newSupplies.ToXml();
                        patientVisitNote.IsSupplyExist = true;
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        var editSupply = supplies.SingleOrDefault(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (editSupply != null)
                        {
                            editSupply.Quantity = supply.Quantity;
                            patientVisitNote.Supply = supplies.ToXml();
                            patientVisitNote.IsSupplyExist = true;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        public bool DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        supplies.ForEach(s =>
                        {
                            if (s.UniqueIdentifier == supply.UniqueIdentifier)
                            {
                                supplies.Remove(s);
                            }
                        });
                        patientVisitNote.Supply = supplies.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        public List<Supply> GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");

            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            var list = new List<Supply>();
            if (patientVisitNote != null && patientVisitNote.Supply.IsNotNullOrEmpty())
            {
                list = patientVisitNote.Supply.ToObject<List<Supply>>();
            }
            return list;
        }

        public bool AdmitPatient(PendingPatient pending)
        {
            var result = false;

            if (patientRepository.AdmitPatient(pending))
            {
                var medId = Guid.NewGuid();
                var patient = patientRepository.Get(pending.Id, Current.AgencyId);
                if (patient != null)
                {
                    patient.EpisodeStartDate = pending.EpisodeStartDate;
                    if (this.CreateEpisodeAndClaims(patient) && this.CreateMedicationProfile(patient, medId))
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        public bool NonAdmitPatient(PendingPatient pending)
        {
            return patientRepository.NonAdmitPatient(pending);
        }

        #endregion

        #region Private Methods

        private void ProcessSchedule(ScheduleEvent scheduleEvent, Patient patient, PatientEpisode episode)
        {
            switch (((DisciplineTasks)scheduleEvent.DisciplineTask).ToString())
            {
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.FollowUp, episode);
                    break;
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                    var newRecertEpisode = CreateEpisode(scheduleEvent.PatientId, episode.EndDate.AddDays(1), new ScheduleEvent());
                    if (patientRepository.AddEpisode(newRecertEpisode))
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        var currentMed = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        var assessment = assessmentService.AddAssessment(scheduleEvent, AssessmentType.Recertification, episode, currentMed);
                        newRecertEpisode.AssessmentId = assessment.Id;
                        newRecertEpisode.AssessmentType = AssessmentType.Recertification.ToString();
                        patientRepository.UpdateEpisode(Current.AgencyId, newRecertEpisode);
                        var rap = new Rap
                        {
                            AgencyId = patient.AgencyId,
                            PatientId = patient.Id,
                            EpisodeId = episode.Id,
                            EpisodeStartDate = episode.StartDate,
                            EpisodeEndDate = episode.EndDate,
                            IsFirstBillableVisit = false,
                            IsOasisComplete = false,
                            PatientIdNumber = patient.PatientIdNumber,
                            IsGenerated = false,
                            MedicareNumber = patient.MedicareNumber,
                            FirstName = patient.FirstName,
                            LastName = patient.LastName,
                            DOB = patient.DOB,
                            Gender = patient.Gender,
                            AddressLine1 = patient.AddressLine1,
                            AddressLine2 = patient.AddressLine2,
                            AddressCity = patient.AddressCity,
                            AddressStateCode = patient.AddressStateCode,
                            AddressZipCode = patient.AddressZipCode,
                            StartofCareDate = patient.StartofCareDate,
                            AreOrdersComplete = false,
                            Status = (int)ScheduleStatus.ClaimCreated,
                            Created = DateTime.Now
                        };
                        if (patient.PhysicianContacts != null)
                        {
                            var contact = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                            if (contact != null)
                            {
                                rap.PhysicianNPI = contact.NPI;
                                rap.PhysicianFirstName = contact.FirstName;
                                rap.PhysicianLastName = contact.LastName;
                            }
                        }
                        rap.DiagonasisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5><code6></code6></DiagonasisCodes>";
                        billingRepository.AddRap(rap);
                    }
                    break;
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                    {
                        var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                        var rocEvent = events.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT);
                        var recentEvent = events.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT);
                        if (scheduleEvent.EventDate.ToDateTime() < episode.EndDate.AddDays(-4))
                        {
                            scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                            var currentMed = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                            assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMed);
                        }
                        else if (recentEvent == null && (scheduleEvent.EventDate.ToDateTime() >= episode.EndDate.AddDays(-4)) && (scheduleEvent.EventDate.ToDateTime() <= episode.EndDate))
                        {
                            var newResumptionofCareEpisode = CreateEpisode(scheduleEvent.PatientId, episode.EndDate.AddDays(1), new ScheduleEvent());
                            if (patientRepository.AddEpisode(newResumptionofCareEpisode))
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                                var assessment = assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode);
                                newResumptionofCareEpisode.AssessmentId = assessment.Id;
                                newResumptionofCareEpisode.AssessmentType = AssessmentType.ResumptionOfCare.ToString();
                                patientRepository.UpdateEpisode(Current.AgencyId, newResumptionofCareEpisode);
                                var rap = new Rap
                                {
                                    AgencyId = patient.AgencyId,
                                    PatientId = patient.Id,
                                    EpisodeId = episode.Id,
                                    EpisodeStartDate = episode.StartDate,
                                    EpisodeEndDate = episode.EndDate,
                                    IsFirstBillableVisit = false,
                                    IsOasisComplete = false,
                                    PatientIdNumber = patient.PatientIdNumber,
                                    IsGenerated = false,
                                    MedicareNumber = patient.MedicareNumber,
                                    FirstName = patient.FirstName,
                                    LastName = patient.LastName,
                                    DOB = patient.DOB,
                                    Gender = patient.Gender,
                                    AddressLine1 = patient.AddressLine1,
                                    AddressLine2 = patient.AddressLine2,
                                    AddressCity = patient.AddressCity,
                                    AddressStateCode = patient.AddressStateCode,
                                    AddressZipCode = patient.AddressZipCode,
                                    StartofCareDate = patient.StartofCareDate,
                                    AreOrdersComplete = false,
                                    Status = (int)ScheduleStatus.ClaimCreated,
                                    Created = DateTime.Now
                                };
                                if (patient.PhysicianContacts != null)
                                {
                                    var contact = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                                    if (contact != null)
                                    {
                                        rap.PhysicianNPI = contact.NPI;
                                        rap.PhysicianFirstName = contact.FirstName;
                                        rap.PhysicianLastName = contact.LastName;
                                    }
                                }
                                rap.DiagonasisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5><code6></code6></DiagonasisCodes>";
                                billingRepository.AddRap(rap);
                            }
                        }
                        else if (recentEvent != null && (int.Parse(recentEvent.Status) == (int)ScheduleStatus.OasisExported || int.Parse(recentEvent.Status) == (int)ScheduleStatus.OasisCompletedExportReady) && recentEvent.EventDate.ToDateTime() < scheduleEvent.EventDate.ToDateTime())
                        {
                            scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                            var currentMed = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                            assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMed);
                        }
                        else if (recentEvent != null && !(int.Parse(recentEvent.Status) == (int)ScheduleStatus.OasisExported || int.Parse(recentEvent.Status) == (int)ScheduleStatus.OasisCompletedExportReady) && recentEvent.EventDate.ToDateTime() < scheduleEvent.EventDate.ToDateTime())
                        {
                            scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                            var currentMed = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                            assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMed);
                        }
                        else
                        {
                            var newRegularResumptionEpisode = CreateEpisode(scheduleEvent.PatientId, episode.EndDate.AddDays(1), new ScheduleEvent());
                            if (patientRepository.AddEpisode(newRegularResumptionEpisode))
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                                var currentMed = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                                assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMed);
                                var rap = new Rap
                                {
                                    AgencyId = patient.AgencyId,
                                    PatientId = patient.Id,
                                    EpisodeId = episode.Id,
                                    EpisodeStartDate = episode.StartDate,
                                    EpisodeEndDate = episode.EndDate,
                                    IsFirstBillableVisit = false,
                                    IsOasisComplete = false,
                                    PatientIdNumber = patient.PatientIdNumber,
                                    IsGenerated = false,
                                    MedicareNumber = patient.MedicareNumber,
                                    FirstName = patient.FirstName,
                                    LastName = patient.LastName,
                                    DOB = patient.DOB,
                                    Gender = patient.Gender,
                                    AddressLine1 = patient.AddressLine1,
                                    AddressLine2 = patient.AddressLine2,
                                    AddressCity = patient.AddressCity,
                                    AddressStateCode = patient.AddressStateCode,
                                    AddressZipCode = patient.AddressZipCode,
                                    StartofCareDate = patient.StartofCareDate,
                                    AreOrdersComplete = false,
                                    Status = (int)ScheduleStatus.ClaimCreated,
                                    Created = DateTime.Now
                                };
                                if (patient.PhysicianContacts != null)
                                {
                                    var contact = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                                    if (contact != null)
                                    {
                                        rap.PhysicianNPI = contact.NPI;
                                        rap.PhysicianFirstName = contact.FirstName;
                                        rap.PhysicianLastName = contact.LastName;
                                    }
                                }
                                rap.DiagonasisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                                billingRepository.AddRap(rap);
                            }
                        }
                    }
                    break;
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                        var currentMed = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                        var assessment = assessmentService.AddAssessment(scheduleEvent, AssessmentType.StartOfCare, episode, currentMed);
                        episode.AssessmentId = assessment.Id;
                        episode.AssessmentType = AssessmentType.StartOfCare.ToString();
                        patientRepository.UpdateEpisode(Current.AgencyId, episode);
                    }
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisNotYetDue).ToString();
                    assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;
                case "SkilledNurseVisit":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "FoleyCathChange":
                case "LVNSupervisoryVisit":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNAssessment":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "PTEvaluation":
                case "PTVisit":
                case "PTDischarge":
                case "OTEvaluation":
                case "OTDischarge":
                case "OTVisit":
                case "STVisit":
                case "STEvaluation":
                case "STDischarge":
                case "MSWEvaluationAssessment":
                case "HHAideSupervisoryVisit":
                case "MSWVisit":
                case "MSWDischarge":
                case "DieticianVisit":
                case "PTAVisit":
                case "PTReEvaluation":
                case "COTAVisit":
                case "MSWAssessment":
                case "MSWProgressNote":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    var snNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(snNote);
                    break;
                case "HHAideVisit":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    var hhAideVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(hhAideVisit);
                    break;
                case "HHAideCarePlan":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    var hhAideCarePlan = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(hhAideCarePlan);
                    break;
                case "DischargeSummary":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    var physician = physicianRepository.GetPatientPhysicians(patient.Id).SingleOrDefault(p => p.Primary);
                    var dischargeSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    if (physician != null)
                    {
                        var questions = new List<NotesQuestion>();
                        questions.Add(new NotesQuestion { Name = "Physician", Answer = Convert.ToString(physician.Id), Type = "DischargeSummary" });
                        scheduleEvent.Questions = questions;
                        dischargeSummary.Note = questions.ToXml();
                        patientRepository.AddVisitNote(dischargeSummary);
                    }
                    else
                    {
                        patientRepository.AddVisitNote(dischargeSummary);
                    }
                    break;
                case "PhysicianOrder":
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderNotYetStarted).ToString();
                    var order = new PhysicianOrder { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, OrderDate= scheduleEvent.EventDate.ToDateTime(), Created = DateTime.Now, Text = "", Summary = "", Status = int.Parse(scheduleEvent.Status), OrderNumber = patientRepository.GetNextOrderNumber() };
                    if (patient.PhysicianContacts.Count > 0)
                    {
                        order.PhysicianId = patient.PhysicianContacts[0].Id;
                    }
                    patientRepository.AddOrder(order);
                    break;
                case "HCFA485":
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderSaved).ToString();
                    assessmentService.GeneratePlanofCare(scheduleEvent, patient, assessmentService.GetPlanofCareAssessmentObject(scheduleEvent.EpisodeId, scheduleEvent.PatientId));
                    break;
                case "HCFA486":
                case "PostHospitalizationOrder":
                case "MedicaidPOC":
                    scheduleEvent.Status = ((int)ScheduleStatus.OrderNotYetStarted).ToString();
                    break;
                case "Rap":
                    {
                        var pocAssessment = assessmentService.GetPlanofCareAssessmentObject(scheduleEvent.EpisodeId, scheduleEvent.PatientId);

                        var newRap = new Rap
                        {
                            AgencyId = patient.AgencyId,
                            PatientId = patient.Id,
                            EpisodeId = episode.Id,
                            EpisodeStartDate = episode.StartDate,
                            EpisodeEndDate = episode.EndDate,
                            IsFirstBillableVisit = false,
                            IsOasisComplete = false,
                            PatientIdNumber = patient.PatientIdNumber,
                            IsGenerated = false,
                            MedicareNumber = patient.MedicareNumber,
                            FirstName = patient.FirstName,
                            LastName = patient.LastName,
                            DOB = patient.DOB,
                            Gender = patient.Gender,
                            AddressLine1 = patient.AddressLine1,
                            AddressLine2 = patient.AddressLine2,
                            AddressCity = patient.AddressCity,
                            AddressStateCode = patient.AddressStateCode,
                            AddressZipCode = patient.AddressZipCode,
                            StartofCareDate = patient.StartofCareDate,
                            AreOrdersComplete = false,
                            Status = (int)ScheduleStatus.ClaimCreated,
                            Created = DateTime.Now
                        };
                        if (patient.PhysicianContacts != null)
                        {
                            var contact = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                            if (contact != null)
                            {
                                newRap.PhysicianNPI = contact.NPI;
                                newRap.PhysicianFirstName = contact.FirstName;
                                newRap.PhysicianLastName = contact.LastName;
                            }
                        }
                        if (pocAssessment != null)
                        {
                            var status = pocAssessment.Status;
                            if (status == 9 && status == 10)
                            {
                                var assessmentQuestions = assessmentService.GetPlanofCareAssessment(episode.Id, patient.Id);
                                if (assessmentQuestions != null)
                                {
                                    newRap.IsOasisComplete = true;
                                    newRap.IsFirstBillableVisit = true;
                                    if (assessmentQuestions["M0030SocDate"] != null && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty())
                                    {
                                        newRap.StartofCareDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                                        newRap.FirstBillableVisitDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                                    }
                                    string diagnosis = "<DiagonasisCodes>";
                                    if (assessmentQuestions["M1020ICD9M"] != null)
                                    {
                                        diagnosis += "<code1>" + assessmentQuestions["M1020ICD9M"].Answer + "</code1>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code1></code1>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M1"] != null)
                                    {
                                        diagnosis += "<code2>" + assessmentQuestions["M1022ICD9M1"].Answer + "</code2>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code2></code2>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M2"] != null)
                                    {
                                        diagnosis += "<code3>" + assessmentQuestions["M1022ICD9M2"].Answer + "</code3>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code3></code3>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M3"] != null)
                                    {
                                        diagnosis += "<code4>" + assessmentQuestions["M1022ICD9M3"].Answer + "</code4>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code4></code4>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M4"] != null)
                                    {
                                        diagnosis += "<code5>" + assessmentQuestions["M1022ICD9M4"].Answer + "</code5>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code5></code5>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M5"] != null)
                                    {
                                        diagnosis += "<code6>" + assessmentQuestions["M1022ICD9M5"].Answer + "</code6>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code6></code6>";
                                    }
                                    diagnosis += "</DiagonasisCodes>";
                                    newRap.DiagonasisCode = diagnosis;
                                    newRap.HippsCode = pocAssessment.HippsCode;
                                    newRap.ClaimKey = pocAssessment.ClaimKey;
                                }
                                else
                                {
                                    newRap.DiagonasisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                                }
                            }
                        }
                        else
                        {
                            newRap.DiagonasisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                        }
                        newRap.DiagonasisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";

                        billingRepository.AddRap(newRap);
                    }
                    break;
                case "Final":
                    {
                        var pocAssessment = assessmentService.GetPlanofCareAssessmentObject(scheduleEvent.EpisodeId, scheduleEvent.PatientId);
                        var newFinal = new Final
                        {
                            AgencyId = patient.AgencyId,
                            PatientId = patient.Id,
                            EpisodeId = episode.Id,
                            EpisodeStartDate = episode.StartDate,
                            EpisodeEndDate = episode.EndDate,
                            IsFirstBillableVisit = false,
                            IsOasisComplete = false,
                            PatientIdNumber = patient.PatientIdNumber,
                            IsGenerated = false,
                            MedicareNumber = patient.MedicareNumber,
                            FirstName = patient.FirstName,
                            LastName = patient.LastName,
                            DOB = patient.DOB,
                            Gender = patient.Gender,
                            AddressLine1 = patient.AddressLine1,
                            AddressLine2 = patient.AddressLine2,
                            AddressCity = patient.AddressCity,
                            AddressStateCode = patient.AddressStateCode,
                            AddressZipCode = patient.AddressZipCode,
                            StartofCareDate = patient.StartofCareDate,
                            AreOrdersComplete = false,
                            Status = (int)ScheduleStatus.ClaimCreated,
                            Created = DateTime.Now
                        };
                        if (patient.PhysicianContacts != null)
                        {
                            var contact = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                            if (contact != null)
                            {
                                newFinal.PhysicianNPI = contact.NPI;
                                newFinal.PhysicianFirstName = contact.FirstName;
                                newFinal.PhysicianLastName = contact.LastName;
                            }
                        }
                        if (pocAssessment != null)
                        {
                            var status = pocAssessment.Status;
                            if (status == 9 && status == 10)
                            {
                                var assessmentQuestions = assessmentService.GetPlanofCareAssessment(episode.Id, patient.Id);
                                if (assessmentQuestions != null)
                                {
                                    newFinal.IsOasisComplete = true;
                                    newFinal.IsFirstBillableVisit = true;
                                    if (assessmentQuestions["M0030SocDate"] != null && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty())
                                    {
                                        newFinal.StartofCareDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                                        newFinal.FirstBillableVisitDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                                    }
                                    string diagnosis = "<DiagonasisCodes>";
                                    if (assessmentQuestions["M1020ICD9M"] != null)
                                    {
                                        diagnosis += "<code1>" + assessmentQuestions["M1020ICD9M"].Answer + "</code1>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code1></code1>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M1"] != null)
                                    {
                                        diagnosis += "<code2>" + assessmentQuestions["M1022ICD9M1"].Answer + "</code2>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code2></code2>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M2"] != null)
                                    {
                                        diagnosis += "<code3>" + assessmentQuestions["M1022ICD9M2"].Answer + "</code3>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code3></code3>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M3"] != null)
                                    {
                                        diagnosis += "<code4>" + assessmentQuestions["M1022ICD9M3"].Answer + "</code4>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code4></code4>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M4"] != null)
                                    {
                                        diagnosis += "<code5>" + assessmentQuestions["M1022ICD9M4"].Answer + "</code5>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code5></code5>";
                                    }
                                    if (assessmentQuestions["M1022ICD9M5"] != null)
                                    {
                                        diagnosis += "<code6>" + assessmentQuestions["M1022ICD9M5"].Answer + "</code6>";
                                    }
                                    else
                                    {
                                        diagnosis += "<code6></code6>";
                                    }
                                    diagnosis += "</DiagonasisCodes>";
                                    newFinal.DiagonasisCode = diagnosis;
                                    newFinal.HippsCode = pocAssessment.HippsCode;
                                    newFinal.ClaimKey = pocAssessment.ClaimKey;
                                }
                                else
                                {
                                    newFinal.DiagonasisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                                }
                            }
                        }
                        else
                        {
                            newFinal.DiagonasisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";
                        }
                        newFinal.DiagonasisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>";

                        billingRepository.AddFinal(newFinal);
                    }
                    break;
                case "SixtyDaySummary":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    var sixtyDaySummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(sixtyDaySummary);
                    break;
                case "TransferSummary":
                    scheduleEvent.Status = ((int)ScheduleStatus.NoteNotYetDue).ToString();
                    var transferSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    patientRepository.AddVisitNote(transferSummary);
                    break;
            }
        }

        private List<NotesQuestion> ProcessNoteQuestions(FormCollection formCollection)
        {
            string type = formCollection["Type"];
            formCollection.Remove("Type");

            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();

            foreach (var key in formCollection.AllKeys)
            {
                string[] nameArray = key.Split('_');
                if (nameArray.Length > 0)
                {
                    nameArray.Reverse();
                    string name = nameArray[0];
                    if (!questions.ContainsKey(name))
                    {
                        questions.Add(name, new NotesQuestion { Name = name, Answer = formCollection.GetValues(key).Join(","), Type = type });
                    }

                }
            }
            return questions.Values.ToList();
        }

        public static MemoryStream ResizeAndEncodePhoto(Image original, string contentType, bool resizeIfWider)
        {
            int imageWidth = 140;
            int imageHeight = 140;
            MemoryStream memoryStream = null;

            if (original != null)
            {
                int destX = 0;
                int destY = 0;
                int sourceX = 0;
                int sourceY = 0;
                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;
                int sourceWidth = original.Width;
                int sourceHeight = original.Height;

                nPercentW = ((float)imageWidth / (float)sourceWidth);
                nPercentH = ((float)imageHeight / (float)sourceHeight);

                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                }
                else
                {
                    nPercent = nPercentW;
                }

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);

                var newImage = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);

                newImage.SetResolution(original.HorizontalResolution, original.VerticalResolution);

                using (Graphics graphics = Graphics.FromImage(newImage))
                {
                    graphics.Clear(Color.White);
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.DrawImage(original,
                        new Rectangle(destX, destY, destWidth, destHeight),
                        new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                        GraphicsUnit.Pixel);
                }
                memoryStream = new MemoryStream();

                ImageCodecInfo imageCodec = null;
                ImageCodecInfo[] imageCodecs = ImageCodecInfo.GetImageEncoders();

                foreach (ImageCodecInfo imageCodeInfo in imageCodecs)
                {
                    if (imageCodeInfo.MimeType == contentType)
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }

                    if (imageCodeInfo.MimeType == "image/jpeg" && contentType == "image/pjpeg")
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }
                }
                if (imageCodec != null)
                {
                    EncoderParameters encoderParameters = new EncoderParameters();
                    encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, 100L);
                    newImage.Save(memoryStream, imageCodec, encoderParameters);
                    newImage.Dispose();
                    original.Dispose();
                }
            }

            memoryStream.Position = 0;
            return memoryStream;
        }

        #endregion

    }
}
