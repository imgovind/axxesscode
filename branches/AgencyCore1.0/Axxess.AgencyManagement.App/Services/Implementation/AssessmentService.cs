﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Text;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Enums;

    using Axxess.Api;
    using Axxess.Api.Contracts;
    using Axxess.AgencyManagement.App.ViewData;


    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    public class AssessmentService : IAssessmentService
    {
        #region Constructor / Members

        private static readonly GrouperAgent grouperAgent = new GrouperAgent();
        private static readonly ValidationAgent validationAgent = new ValidationAgent();

        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IOasisCDataProvider oasisDataProvider;

        public AssessmentService(IOasisCDataProvider oasisDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider)
        {
            Check.Argument.IsNotNull(oasisDataProvider, "dataProvider");

            this.oasisDataProvider = oasisDataProvider;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.assetRepository = agencyManagementDataProvider.AssetRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
        }

        #endregion

        #region IAssessmentService Members

        public Assessment SaveAssessment(FormCollection formCollection, HttpFileCollectionBase httpFiles)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            Assessment assessment = null;
            string assessmentType = formCollection.Get("assessment");
            string action = "{0}_Action".FormatWith(assessmentType);
            string assessmentAction = formCollection.Get("{0}_Action".FormatWith(assessmentType));

            if (assessmentAction.IsNotNullOrEmpty())
            {
                switch (formCollection[action])
                {
                    case "New":
                        assessment = AddAssessment(formCollection);
                        break;
                    case "Edit":
                        assessment = UpdateAssessment(formCollection, httpFiles);
                        break;
                }
            }
            return assessment;
        }

        public Assessment AddAssessment(ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode)
        {
            string sessionKey = string.Empty;
            Assessment assessment = AssessmentFactory.Create(((DisciplineTasks)oasisSchedule.DisciplineTask).ToString());
            assessment.Status = (int)ScheduleStatus.NoteNotYetDue;
            assessment.Id = oasisSchedule.EventId;
            assessment.EpisodeId = episode.Id;
            assessment.PatientId = oasisSchedule.PatientId;
            assessment.AssessmentDate = DateTime.Parse(oasisSchedule.EventDate);
            assessment.Questions = ProcessPatientDemography(patientRepository.GetPatient<Patient>(assessment.PatientId, Current.AgencyId), oasisSchedule, assessmentType, episode).Values.ToList();
            assessment.Id = oasisDataProvider.OasisAssessmentRepository.Add(assessment, ((DisciplineTasks)oasisSchedule.DisciplineTask).ToString());
            sessionKey = string.Format("{0}_{1}", assessment.Type, assessment.Id);
            SessionStore.Add<Assessment>(sessionKey, assessment);
            return assessment;
        }

        public Assessment AddAssessment(ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode, string MedicationProfile)
        {
            string sessionKey = string.Empty;
            Assessment assessment = AssessmentFactory.Create(((DisciplineTasks)oasisSchedule.DisciplineTask).ToString());
            assessment.Status = (int)ScheduleStatus.NoteNotYetDue;
            assessment.Id = oasisSchedule.EventId;
            assessment.EpisodeId = episode.Id;
            assessment.PatientId = oasisSchedule.PatientId;
            assessment.MedicationProfile = MedicationProfile;
            assessment.AssessmentDate = DateTime.Parse(oasisSchedule.EventDate);
            assessment.Questions = ProcessPatientDemography(patientRepository.GetPatient<Patient>(assessment.PatientId, Current.AgencyId), oasisSchedule, assessmentType, episode).Values.ToList();
            assessment.Id = oasisDataProvider.OasisAssessmentRepository.Add(assessment, ((DisciplineTasks)oasisSchedule.DisciplineTask).ToString());
            sessionKey = string.Format("{0}_{1}", assessment.Type, assessment.Id);
            SessionStore.Add<Assessment>(sessionKey, assessment);
            return assessment;
        }

        public Assessment GetAssessment(Guid assessmentId, string assessmentType)
        {
            return oasisDataProvider.OasisAssessmentRepository.Get(assessmentId, assessmentType);
        }

        public List<SubmissionBodyFormat> GetOasisSubmissionFormatInstructions()
        {
            return oasisDataProvider.CachedDataRepository.GetSubmissionFormatInstructions();
        }

        public Dictionary<string, SubmissionBodyFormat> GetOasisSubmissionFormatInstructionsNew()
        {
            var format = oasisDataProvider.CachedDataRepository.GetSubmissionFormatInstructions();
            var dictionaryFormat = new Dictionary<string, SubmissionBodyFormat>();
            if (format != null)
            {
                format.ForEach(f =>
                {
                    dictionaryFormat.Add(f.Item, f);
                });
            }
            return dictionaryFormat;
        }

        public string GetOasisSubmissionFormatNew(Dictionary<string, SubmissionBodyFormat> submissionGuide, IDictionary<string, Question> assessmentQuestions, int versionNumber)
        {
            var validation = new List<Validation>();

            var submissionFormat = new StringBuilder();
            submissionFormat.Capacity = 1446;
            var agency = agencyRepository.Get(Current.AgencyId);
            string type = assessmentQuestions["M0100AssessmentType"].Answer;
            submissionFormat.Append("B1"); //REC_ID
            submissionFormat.Append(string.Empty.PadLeft(2)); //REC_TYPE
            submissionFormat.Append(string.Empty.PadLeft(8)); //LOCK_DATE
            submissionFormat.Append(versionNumber.ToString().PadLeft(2, '0')); //CORRECTION_NUM
            submissionFormat.Append(string.Empty.PadLeft(8)); //ACY_DOC_CD
            submissionFormat.Append("C-072009".PadRight(12)); //VERSION_CD1
            submissionFormat.Append("02.00".PadLeft(5)); //VERSION_CD2
            submissionFormat.Append("364649717".PadLeft(9)); //SFTW_ID
            submissionFormat.Append("1.0".PadLeft(5)); //SFT_VER
            if (agency != null && agency.HomeHealthAgencyId.IsNotNullOrEmpty()) //HHA_AGENCY_ID
            {
                submissionFormat.Append(agency.HomeHealthAgencyId.ToUpper().PartOfString(0, 16).PadLeft(16));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(16));
            }
            submissionFormat.Append(string.Empty.PadLeft(14)); //PAT_ID
            submissionFormat.Append(string.Empty.PadLeft(2));  //ST_CODE
            submissionFormat.Append(string.Empty.PadLeft(4));  //ST_ERR_CNT
            submissionFormat.Append(string.Empty.PadLeft(1));  //ST_COR
            submissionFormat.Append(string.Empty.PadLeft(1));  //ST_PMT_COR
            submissionFormat.Append(string.Empty.PadLeft(1));  //ST_KEY_COR
            submissionFormat.Append(string.Empty.PadLeft(1));  //ST_DELETE
            submissionFormat.Append(string.Empty.PadLeft(1));  //MC_COR
            submissionFormat.Append(string.Empty.PadLeft(1));  //MC_PMT_COR
            submissionFormat.Append(string.Empty.PadLeft(1));  //MC_KEY_COR
            submissionFormat.Append(string.Empty.PadLeft(20)); //MASK_VERSION_CD
            submissionFormat.Append(string.Empty.PadLeft(7));  //CNT_FILLER

            if (submissionGuide.ContainsKey("M0010_CCN") && assessmentQuestions.ContainsKey("M0010CertificationNumber") && assessmentQuestions["M0010CertificationNumber"].Answer.IsNotNullOrEmpty()) //M0010_CCN
            {
                submissionFormat.Append(assessmentQuestions["M0010CertificationNumber"].Answer.ToUpper().PartOfString(0, 6).PadLeft(6));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(6));
            }

            submissionFormat.Append(string.Empty.PadLeft(15)); //M0014_BRANCH_STATE

            if (submissionGuide.ContainsKey("M0014_BRANCH_STATE") && assessmentQuestions.ContainsKey("M0014BranchState") && assessmentQuestions["M0014BranchState"].Answer.IsNotNullOrEmpty()) //M0014_BRANCH_STATE
            {
                submissionFormat.Append(assessmentQuestions["M0014BranchState"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (submissionGuide.ContainsKey("M0016_BRANCH_ID") && assessmentQuestions.ContainsKey("M0016BranchId") && assessmentQuestions["M0016BranchId"].Answer.IsNotNullOrEmpty()) //M0016_BRANCH_ID
            {
                submissionFormat.Append(assessmentQuestions["M0016BranchId"].Answer.ToUpper().PartOfString(0, 10).PadRight(10));
            }
            else
            {
                submissionFormat.Append("N");
                submissionFormat.Append(string.Empty.PadRight(9));
            }

            if (submissionGuide.ContainsKey("M0020_PAT_ID") && assessmentQuestions.ContainsKey("M0020PatientIdNumber") && assessmentQuestions["M0020PatientIdNumber"].Answer.IsNotNullOrEmpty()) //M0020_PAT_ID
            {
                submissionFormat.Append(assessmentQuestions["M0020PatientIdNumber"].Answer.ToUpper().PartOfString(0, 20).PadLeft(20));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(20));
            }

            if (submissionGuide.ContainsKey("M0030_START_CARE_DT") && assessmentQuestions.ContainsKey("M0030SocDate") && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty()) //M0030_START_CARE_DT
            {
                submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(8));

            }

            if ((submissionGuide.ContainsKey("M0032_ROC_DT_NA") && assessmentQuestions.ContainsKey("M0032ROCDateNotApplicable") && !assessmentQuestions["M0032ROCDateNotApplicable"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0032_ROC_DT_NA") && !assessmentQuestions.ContainsKey("M0032ROCDateNotApplicable"))) // M0032_ROC_DT and M0032_ROC_DT_NA
            {

                if (submissionGuide.ContainsKey("M0032_ROC_DT") && assessmentQuestions.ContainsKey("M0032ROCDate") && assessmentQuestions["M0032ROCDate"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0032ROCDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                }
                submissionFormat.Append("0".PadLeft(1));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(8));
                submissionFormat.Append("1".PadLeft(1));
            }
            if (submissionGuide.ContainsKey("M0040_PAT_FNAME") && assessmentQuestions.ContainsKey("M0040FirstName") && assessmentQuestions["M0040FirstName"].Answer.IsNotNullOrEmpty()) //M0040_PAT_FNAME
            {

                submissionFormat.Append(assessmentQuestions["M0040FirstName"].Answer.ToUpper().PartOfString(0, 12).PadLeft(12));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadRight(12));
            }

            if (submissionGuide.ContainsKey("M0040_PAT_MI") && assessmentQuestions.ContainsKey("M0040MI") && assessmentQuestions["M0040MI"].Answer.IsNotNullOrEmpty()) //M0040_PAT_MI
            {

                submissionFormat.Append(assessmentQuestions["M0040MI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }

            if (submissionGuide.ContainsKey("M0040_PAT_LNAME") && assessmentQuestions.ContainsKey("M0040LastName") && assessmentQuestions["M0040LastName"].Answer.IsNotNullOrEmpty()) //M0040_PAT_LNAME
            {

                submissionFormat.Append(assessmentQuestions["M0040LastName"].Answer.ToUpper().PartOfString(0, 18).PadLeft(18));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadRight(18));
            }

            if (submissionGuide.ContainsKey("M0040_PAT_SUFFIX") && assessmentQuestions.ContainsKey("M0040Suffix") && assessmentQuestions["M0040Suffix"].Answer.IsNotNullOrEmpty()) //M0040_PAT_SUFFIX
            {

                submissionFormat.Append(assessmentQuestions["M0040Suffix"].Answer.ToUpper().PartOfString(0, 3).PadLeft(3));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadRight(3));
            }

            if (submissionGuide.ContainsKey("M0050_PAT_ST") && assessmentQuestions.ContainsKey("M0050PatientState") && assessmentQuestions["M0050PatientState"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {

                submissionFormat.Append(assessmentQuestions["M0050PatientState"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (submissionGuide.ContainsKey("M0060_PAT_ZIP") && assessmentQuestions.ContainsKey("M0060PatientZipCode") && assessmentQuestions["M0060PatientZipCode"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {

                submissionFormat.Append(assessmentQuestions["M0060PatientZipCode"].Answer.ToUpper().PartOfString(0, 11).PadLeft(11));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(11));
            }

            if ((submissionGuide.ContainsKey("M0063_MEDICARE_NA") && assessmentQuestions.ContainsKey("M0063PatientMedicareNumberUnknown") && !assessmentQuestions["M0063PatientMedicareNumberUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0032_ROC_DT_NA") && !assessmentQuestions.ContainsKey("M0063PatientMedicareNumberUnknown"))) // M0063_MEDICARE_NUM and M0063_MEDICARE_NA
            {

                if (submissionGuide.ContainsKey("M0063_MEDICARE_NUM") && assessmentQuestions.ContainsKey("M0063PatientMedicareNumber") && assessmentQuestions["M0063PatientMedicareNumber"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M0063PatientMedicareNumber"].Answer.ToUpper().PartOfString(0, 12).PadLeft(12));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(12));
                }
                submissionFormat.Append("0".PadLeft(1));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(12));
                submissionFormat.Append("1".PadLeft(1));
            }


            if ((submissionGuide.ContainsKey("M0064_SSN_UK") && assessmentQuestions.ContainsKey("M0064PatientSSNUnknown") && !assessmentQuestions["M0064PatientSSNUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0064_SSN_UK") && !assessmentQuestions.ContainsKey("M0064PatientSSNUnknown"))) // M0032_ROC_DT and M0032_ROC_DT_NA
            {

                if (submissionGuide.ContainsKey("M0064_SSN") && assessmentQuestions.ContainsKey("M0064PatientSSN") && assessmentQuestions["M0064PatientSSN"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M0064PatientSSN"].Answer.ToUpper().PartOfString(0, 9).PadLeft(9));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(9));
                }
                submissionFormat.Append("0".PadLeft(1));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(9));
                submissionFormat.Append("1".PadLeft(1));
            }

            if ((submissionGuide.ContainsKey("M0065_MEDICAID_NA") && assessmentQuestions.ContainsKey("M0065PatientMedicaidNumberUnknown") && !assessmentQuestions["M0065PatientMedicaidNumberUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0065_MEDICAID_NA") && !assessmentQuestions.ContainsKey("M0065PatientMedicaidNumberUnknown"))) // M0032_ROC_DT and M0032_ROC_DT_NA
            {

                if (submissionGuide.ContainsKey("M0065_MEDICAID_NUM") && assessmentQuestions.ContainsKey("M0065PatientMedicaidNumber") && assessmentQuestions["M0065PatientMedicaidNumber"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M0064PatientSSN"].Answer.ToUpper().PartOfString(0, 14).PadLeft(14));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(14));
                }
                submissionFormat.Append("0".PadLeft(1));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(14));
                submissionFormat.Append("1".PadLeft(1));
            }


            if (submissionGuide.ContainsKey("M0066_PAT_BIRTH_DT") && assessmentQuestions.ContainsKey("M0066PatientDoB") && assessmentQuestions["M0066PatientDoB"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0066PatientDoB"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(8));
            }
            submissionFormat.Append(string.Empty.PadLeft(1));

            if (submissionGuide.ContainsKey("M0069_PAT_GENDER") && assessmentQuestions.ContainsKey("M0069Gender") && assessmentQuestions["M0069Gender"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0069Gender"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }

            if ((submissionGuide.ContainsKey("M0018_PHYSICIAN_UK") && assessmentQuestions.ContainsKey("M0018NationalProviderIdUnknown") && !assessmentQuestions["M0018NationalProviderIdUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M0018_PHYSICIAN_UK") && !assessmentQuestions.ContainsKey("M0018NationalProviderIdUnknown"))) // M0032_ROC_DT and M0032_ROC_DT_NA
            {

                if (submissionGuide.ContainsKey("M0018_PHYSICIAN_ID") && assessmentQuestions.ContainsKey("M0018NationalProviderId") && assessmentQuestions["M0018NationalProviderId"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M0018NationalProviderId"].Answer.ToUpper().PartOfString(0, 10).PadLeft(10));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(10));
                }
                submissionFormat.Append("0".PadLeft(1));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(10));
                submissionFormat.Append("1".PadLeft(1));
            }


            if (submissionGuide.ContainsKey("M0080_ASSESSOR_DISCIPLINE") && assessmentQuestions.ContainsKey("M0080DisciplinePerson") && assessmentQuestions["M0080DisciplinePerson"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0080DisciplinePerson"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (submissionGuide.ContainsKey("M0090_INFO_COMPLETED_DT") && assessmentQuestions.ContainsKey("M0090AssessmentCompleted") && assessmentQuestions["M0090AssessmentCompleted"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0090AssessmentCompleted"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(8));
            }

            if (submissionGuide.ContainsKey("M0100_ASSMT_REASON") && assessmentQuestions.ContainsKey("M0100AssessmentType") && assessmentQuestions["M0100AssessmentType"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0100AssessmentType"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            if (type.Contains("03") || type.Contains("01"))
            {
                if (submissionGuide.ContainsKey("M0140_ETHNIC_AI_AN") && assessmentQuestions.ContainsKey("M0140RaceAMorAN") && assessmentQuestions["M0140RaceAMorAN"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0140RaceAMorAN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0140_ETHNIC_ASIAN") && assessmentQuestions.ContainsKey("M0140RaceAsia") && assessmentQuestions["M0140RaceAsia"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0140RaceAsia"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0140_ETHNIC_BLACK") && assessmentQuestions.ContainsKey("M0140RaceBalck") && assessmentQuestions["M0140RaceBalck"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0140RaceBalck"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0140_ETHNIC_HISP") && assessmentQuestions.ContainsKey("M0140RaceHispanicOrLatino") && assessmentQuestions["M0140RaceHispanicOrLatino"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0140RaceHispanicOrLatino"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0140_ETHNIC_NH_PI") && assessmentQuestions.ContainsKey("M0140RaceNHOrPI") && assessmentQuestions["M0140RaceNHOrPI"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0140RaceNHOrPI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M0140_ETHNIC_WHITE") && assessmentQuestions.ContainsKey("M0140RaceWhite") && assessmentQuestions["M0140RaceWhite"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0140RaceWhite"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(6));
            }

            submissionFormat.Append(string.Empty.PadLeft(1));


            if (submissionGuide.ContainsKey("M0150_CPAY_NONE") && assessmentQuestions.ContainsKey("M0150PaymentSourceNone") && assessmentQuestions["M0150PaymentSourceNone"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourceNone"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }

            if (submissionGuide.ContainsKey("M0150_CPAY_MCARE_FFS") && assessmentQuestions.ContainsKey("M0150PaymentSourceMCREFFS") && assessmentQuestions["M0150PaymentSourceMCREFFS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMCREFFS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }

            if (submissionGuide.ContainsKey("M0150_CPAY_MCARE_HMO") && assessmentQuestions.ContainsKey("M0150PaymentSourceMCREHMO") && assessmentQuestions["M0150PaymentSourceMCREHMO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMCREHMO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }

            if (submissionGuide.ContainsKey("M0150_CPAY_MCAID_FFS") && assessmentQuestions.ContainsKey("M0150PaymentSourceMCAIDFFS") && assessmentQuestions["M0150PaymentSourceMCAIDFFS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMCAIDFFS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }

            if (submissionGuide.ContainsKey("M0150_CPAY_MCAID_HMO") && assessmentQuestions.ContainsKey("M0150PaymentSourceMACIDHMO") && assessmentQuestions["M0150PaymentSourceMACIDHMO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourceMACIDHMO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }


            if (submissionGuide.ContainsKey("M0150_CPAY_WRKCOMP") && assessmentQuestions.ContainsKey("M0150PaymentSourceWRKCOMP") && assessmentQuestions["M0150PaymentSourceWRKCOMP"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourceWRKCOMP"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }


            if (submissionGuide.ContainsKey("M0150_CPAY_TITLEPGMS") && assessmentQuestions.ContainsKey("M0150PaymentSourceTITLPRO") && assessmentQuestions["M0150PaymentSourceTITLPRO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourceTITLPRO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }

            if (submissionGuide.ContainsKey("M0150_CPAY_OTH_GOVT") && assessmentQuestions.ContainsKey("M0150PaymentSourceOTHGOVT") && assessmentQuestions["M0150PaymentSourceOTHGOVT"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourceOTHGOVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }

            if (submissionGuide.ContainsKey("M0150_CPAY_PRIV_INS") && assessmentQuestions.ContainsKey("M0150PaymentSourcePRVINS") && assessmentQuestions["M0150PaymentSourcePRVINS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourcePRVINS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }

            if (submissionGuide.ContainsKey("M0150_CPAY_PRIV_HMO") && assessmentQuestions.ContainsKey("M0150PaymentSourcePRVHMO") && assessmentQuestions["M0150PaymentSourcePRVHMO"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourcePRVHMO"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }

            if (submissionGuide.ContainsKey("M0150_CPAY_SELFPAY") && assessmentQuestions.ContainsKey("M0150PaymentSourceSelfPay") && assessmentQuestions["M0150PaymentSourceSelfPay"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourceSelfPay"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }


            if (submissionGuide.ContainsKey("M0150_CPAY_OTHER") && assessmentQuestions.ContainsKey("M0150PaymentSourceOtherSRS") && assessmentQuestions["M0150PaymentSourceOtherSRS"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
            {
                submissionFormat.Append(assessmentQuestions["M0150PaymentSourceOtherSRS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
            }
            else
            {
                submissionFormat.Append("0");
            }
            if (type.Contains("03") || type.Contains("01"))
            {

                if (submissionGuide.ContainsKey("M0150_CPAY_UK") && assessmentQuestions.ContainsKey("M0150PaymentSourceUnknown") && assessmentQuestions["M0150PaymentSourceUnknown"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0150PaymentSourceUnknown"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append("0");
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }
            submissionFormat.Append(string.Empty.PadLeft(6));
            submissionFormat.Append(string.Empty.PadLeft(5));
            if (type.Contains("03") || type.Contains("01"))
            {
                if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone") && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer == "1")
                {
                    submissionFormat.Append(string.Empty.PadLeft(9));
                }
                else
                {
                    if ((submissionGuide.ContainsKey("M1005_INP_DSCHG_UNKNOWN") && assessmentQuestions.ContainsKey("M1005InpatientDischargeDateUnknown") && !assessmentQuestions["M1005InpatientDischargeDateUnknown"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M1005_INP_DSCHG_UNKNOWN") && !assessmentQuestions.ContainsKey("M1005InpatientDischargeDateUnknown"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                    {

                        if (submissionGuide.ContainsKey("M1005_INP_DISCHARGE_DT") && assessmentQuestions.ContainsKey("M1005InpatientDischargeDate") && assessmentQuestions["M1005InpatientDischargeDate"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                        {
                            submissionFormat.Append(DateTime.Parse(assessmentQuestions["M1005InpatientDischargeDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(8));
                        }
                        submissionFormat.Append("0".PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                        submissionFormat.Append("1".PadLeft(1));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(8));
                submissionFormat.Append(string.Empty.PadLeft(1));
            }

            if (type.Contains("03") || type.Contains("01"))
            {
                if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone") && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer == "1")
                {
                    submissionFormat.Append(string.Empty.PadLeft(14));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1010_14_DAY_INP1_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode1") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1010InpatientFacilityDiagnosisCode1"].Answer.ToUpper().PadLeft(7).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1010_14_DAY_INP2_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode2") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode2"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(7));
                submissionFormat.Append(string.Empty.PadLeft(7));
            }

            submissionFormat.Append(string.Empty.PadLeft(1));

            if (type.Contains("03") || type.Contains("01"))
            {
                if (submissionGuide.ContainsKey("M1016_CHGREG_ICD_NA") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisNotApplicable") && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer == "1") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(28));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1016_CHGREG_ICD1") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode1") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode1"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1016_CHGREG_ICD2") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode2") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode2"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1016_CHGREG_ICD3") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode3") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                    if (submissionGuide.ContainsKey("M1016_CHGREG_ICD4") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode4") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }

                }

                if (submissionGuide.ContainsKey("M1018_PRIOR_UNKNOWN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUK")) //M0050_PAT_ST
                {
                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUK"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append("000000001");
                    }
                    else if ((submissionGuide.ContainsKey("M1018_PRIOR_NOCHG_14D") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNA")))
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNA"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000000010");
                        }
                        else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append("000000100");
                            }
                            else
                            {
                                if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                                {
                                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                    {
                                        submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                    }
                                    else
                                    {
                                        submissionFormat.Append("0");
                                    }
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(1));
                                }
                                submissionFormat.Append("0");
                                submissionFormat.Append("0");
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append("0");
                            submissionFormat.Append("0");
                        }
                    }
                    else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000000100");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                            submissionFormat.Append(string.Empty.PadLeft(1));
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                        submissionFormat.Append(string.Empty.PadLeft(1));
                        submissionFormat.Append("0");

                    }
                }
                else if ((submissionGuide.ContainsKey("M1018_PRIOR_NOCHG_14D") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNA")))
                {
                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNA"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append("000000010");
                    }
                    else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000000100");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            submissionFormat.Append("0");
                            submissionFormat.Append("0");
                            submissionFormat.Append(string.Empty.PadLeft(1));

                        }

                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                        submissionFormat.Append("0");
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else if ((submissionGuide.ContainsKey("M1018_PRIOR_NONE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenNone")))
                {
                    if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenNone"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append("000000100");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        submissionFormat.Append("0");
                        submissionFormat.Append(string.Empty.PadLeft(1));
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1018_PRIOR_UR_INCON") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenUI")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenUI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1018_PRIOR_CATH") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenCATH")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenCATH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1018_PRIOR_INTRACT_PAIN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenPain")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1018_PRIOR_IMPR_DECSN") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDECSN")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1018_PRIOR_DISRUPTIVE") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenDisruptive")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1018_PRIOR_MEM_LOSS") && assessmentQuestions.ContainsKey("M1018ConditionsPriorToMedicalRegimenMemLoss")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                    submissionFormat.Append(string.Empty.PadLeft(1));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(28));
                submissionFormat.Append(string.Empty.PadLeft(9));
            }


            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
            {
                if (submissionGuide.ContainsKey("M1020_PRIMARY_DIAG_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M") && assessmentQuestions["M1020ICD9M"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1020ICD9M"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }

                if (submissionGuide.ContainsKey("M1020_PRIMARY_DIAG_SEVERITY") && assessmentQuestions.ContainsKey("M1020SymptomControlRating") && assessmentQuestions["M1020SymptomControlRating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1020SymptomControlRating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M1022_OTH_DIAG1_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M1") && assessmentQuestions["M1020ICD9M1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1020ICD9M1"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG1_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose1Rating") && assessmentQuestions["M1022OtherDiagnose1Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose1Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(9));
                }


                if (submissionGuide.ContainsKey("M1022_OTH_DIAG2_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M2") && assessmentQuestions["M1020ICD9M2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1020ICD9M2"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG2_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose2Rating") && assessmentQuestions["M1022OtherDiagnose2Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose2Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(9));
                }

                if (submissionGuide.ContainsKey("M1022_OTH_DIAG3_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M3") && assessmentQuestions["M1020ICD9M3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1020ICD9M3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG3_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose3Rating") && assessmentQuestions["M1022OtherDiagnose3Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose3Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(9));
                }

                if (submissionGuide.ContainsKey("M1022_OTH_DIAG4_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M4") && assessmentQuestions["M1020ICD9M4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1020ICD9M4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG4_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose4Rating") && assessmentQuestions["M1022OtherDiagnose4Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose4Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(9));
                }

                if (submissionGuide.ContainsKey("M1022_OTH_DIAG5_ICD") && assessmentQuestions.ContainsKey("M1020ICD9M5") && assessmentQuestions["M1020ICD9M5"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1020ICD9M5"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    if (submissionGuide.ContainsKey("M1022_OTH_DIAG5_SEVERITY") && assessmentQuestions.ContainsKey("M1022OtherDiagnose5Rating") && assessmentQuestions["M1022OtherDiagnose5Rating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(assessmentQuestions["M1022OtherDiagnose5Rating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(9));
                }

                if (submissionGuide.ContainsKey("M1030_THH_NONE_ABOVE") && assessmentQuestions.ContainsKey("M1030HomeTherapiesNone")) //M0050_PAT_ST
                {
                    if (assessmentQuestions["M1030HomeTherapiesNone"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append("0001");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1030_THH_IV_INFUSION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesInfusion")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1030_THH_PAR_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesParNutrition")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1030_THH_ENT_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesEntNutrition")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append("0");
                    }
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1030_THH_IV_INFUSION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesInfusion")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesInfusion"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                    if (submissionGuide.ContainsKey("M1030_THH_PAR_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesParNutrition")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesParNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                    if (submissionGuide.ContainsKey("M1030_THH_ENT_NUTRITION") && assessmentQuestions.ContainsKey("M1030HomeTherapiesEntNutrition")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1030HomeTherapiesEntNutrition"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(54));
                submissionFormat.Append(string.Empty.PadLeft(4));
            }

            submissionFormat.Append(string.Empty.PadLeft(6));

            if (type.Contains("01") || type.Contains("03"))
            {

                if (submissionGuide.ContainsKey("M1036_RSK_NONE") && assessmentQuestions.ContainsKey("M1036RiskFactorsNone")) //M0050_PAT_ST
                {
                    if (assessmentQuestions["M1036RiskFactorsNone"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append("000010");
                    }
                    else if (submissionGuide.ContainsKey("M1036_RSK_UNKNOWN") && assessmentQuestions.ContainsKey("M1036RiskFactorsUnknown"))
                    {
                        if (assessmentQuestions["M1036RiskFactorsUnknown"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("000001");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("00");
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append("0");
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                }
                else if (submissionGuide.ContainsKey("M1036_RSK_UNKNOWN") && assessmentQuestions.ContainsKey("M1036RiskFactorsUnknown"))
                {
                    if (assessmentQuestions["M1036RiskFactorsUnknown"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append("000001");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                        submissionFormat.Append("0");
                    }
                }
                else
                {

                    if (submissionGuide.ContainsKey("M1036_RSK_SMOKING") && assessmentQuestions.ContainsKey("M1036RiskFactorsSmoking")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1036RiskFactorsSmoking"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1036RiskFactorsSmoking"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1036_RSK_OBESITY") && assessmentQuestions.ContainsKey("M1036RiskFactorsObesity")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1036RiskFactorsObesity"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1036RiskFactorsObesity"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1036_RSK_ALCOHOLISM") && assessmentQuestions.ContainsKey("M1036RiskFactorsAlcoholism")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1036RiskFactorsAlcoholism"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1036_RSK_DRUGS") && assessmentQuestions.ContainsKey("M1036RiskFactorsDrugs")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1036RiskFactorsDrugs"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1036RiskFactorsDrugs"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    submissionFormat.Append(string.Empty.PadLeft(1));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(6));
            }

            submissionFormat.Append(string.Empty.PadLeft(55));

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
            {
                if (submissionGuide.ContainsKey("M1200_VISION") && assessmentQuestions.ContainsKey("M1200Vision") && assessmentQuestions["M1200Vision"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1200Vision"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(2));
            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1230_SPEECH") && assessmentQuestions.ContainsKey("M1230SpeechAndOral") && assessmentQuestions["M1230SpeechAndOral"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1230SpeechAndOral"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(5));


            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1322_NBR_PRSULC_STG1") && assessmentQuestions.ContainsKey("M1322CurrentNumberStageIUlcer") && assessmentQuestions["M1322CurrentNumberStageIUlcer"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1322CurrentNumberStageIUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(7));

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1324_STG_PRBLM_ULCER") && assessmentQuestions.ContainsKey("M1324MostProblematicUnhealedStage") && assessmentQuestions["M1324MostProblematicUnhealedStage"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1324MostProblematicUnhealedStage"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            submissionFormat.Append(string.Empty.PadLeft(14));

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1400_WHEN_DYSPNEIC") && assessmentQuestions.ContainsKey("M1400PatientDyspneic") && assessmentQuestions["M1400PatientDyspneic"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1400PatientDyspneic"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1410_RESPTX_NONE") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsNone")) //M0050_PAT_ST
                {
                    if (assessmentQuestions["M1410HomeRespiratoryTreatmentsNone"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append("0001");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1410_RESPTX_OXYGEN") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1410_RESPTX_VENTILATOR") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1410_RESPTX_AIRPRESS") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append("0");
                    }
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1410_RESPTX_OXYGEN") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsOxygen")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsOxygen"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1410_RESPTX_VENTILATOR") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsVentilator")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsVentilator"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1410_RESPTX_AIRPRESS") && assessmentQuestions.ContainsKey("M1410HomeRespiratoryTreatmentsContinuous")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1410HomeRespiratoryTreatmentsContinuous"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (submissionGuide.ContainsKey("M1600_UTI") && assessmentQuestions.ContainsKey("M1600UrinaryTractInfection") && assessmentQuestions["M1600UrinaryTractInfection"].Answer.IsNotNullOrEmpty()) //M1600_UTI
                {
                    submissionFormat.Append(assessmentQuestions["M1600UrinaryTractInfection"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {

                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(4));
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1610_UR_INCONT") && assessmentQuestions.ContainsKey("M1610UrinaryIncontinence") && assessmentQuestions["M1610UrinaryIncontinence"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1610UrinaryIncontinence"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            submissionFormat.Append(string.Empty.PadLeft(2));

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1620_BWL_INCONT") && assessmentQuestions.ContainsKey("M1620BowelIncontinenceFrequency") && assessmentQuestions["M1620BowelIncontinenceFrequency"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1620BowelIncontinenceFrequency"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }


            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
            {
                if (submissionGuide.ContainsKey("M1630_OSTOMY") && assessmentQuestions.ContainsKey("M1630OstomyBowelElimination") && assessmentQuestions["M1630OstomyBowelElimination"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1630OstomyBowelElimination"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1700_COG_FUNCTION") && assessmentQuestions.ContainsKey("M1700CognitiveFunctioning") && assessmentQuestions["M1700CognitiveFunctioning"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1700CognitiveFunctioning"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1710_WHEN_CONFUSED") && assessmentQuestions.ContainsKey("M1710WhenConfused") && assessmentQuestions["M1710WhenConfused"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1710WhenConfused"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1720_WHEN_ANXIOUS") && assessmentQuestions.ContainsKey("M1720WhenAnxious") && assessmentQuestions["M1720WhenAnxious"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1720WhenAnxious"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(13));

            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1740_BD_NONE") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsNone")) //M0050_PAT_ST
                {
                    if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append("0000001");
                    }

                    else
                    {
                        if (submissionGuide.ContainsKey("M1740_BD_MEM_DEFICIT") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_IMP_DECISN") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_VERBAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_PHYSICAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }

                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_SOC_INAPPRO") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1740_BD_DELUSIONS") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append("0");
                    }
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1740_BD_MEM_DEFICIT") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1740_BD_IMP_DECISN") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsImpDes")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1740_BD_VERBAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsVerbal")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1740_BD_PHYSICAL") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsPhysical")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }

                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1740_BD_SOC_INAPPRO") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsSIB")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1740_BD_DELUSIONS") && assessmentQuestions.ContainsKey("M1740CognitiveBehavioralPsychiatricSymptomsDelusional")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }


                if (submissionGuide.ContainsKey("M1745_BEH_PROB_FREQ") && assessmentQuestions.ContainsKey("M1745DisruptiveBehaviorSymptomsFrequency") && assessmentQuestions["M1745DisruptiveBehaviorSymptomsFrequency"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1745DisruptiveBehaviorSymptomsFrequency"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(7));
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03"))
            {
                if (submissionGuide.ContainsKey("M1750_REC_PSYCH_NURS") && assessmentQuestions.ContainsKey("M1750PsychiatricNursingServicing") && assessmentQuestions["M1750PsychiatricNursingServicing"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1750PsychiatricNursingServicing"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }
            submissionFormat.Append(string.Empty.PadLeft(2));


            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1800_CUR_GROOMING") && assessmentQuestions.ContainsKey("M1800Grooming") && assessmentQuestions["M1800Grooming"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1800Grooming"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(2));
            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1810_CUR_DRESS_UPPER") && assessmentQuestions.ContainsKey("M1810CurrentAbilityToDressUpper") && assessmentQuestions["M1810CurrentAbilityToDressUpper"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1810CurrentAbilityToDressUpper"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(2));
            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1820_CUR_DRESS_LOWER") && assessmentQuestions.ContainsKey("M1820CurrentAbilityToDressLower") && assessmentQuestions["M1820CurrentAbilityToDressLower"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1820CurrentAbilityToDressLower"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(18));
            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1870_CUR_FEEDING") && assessmentQuestions.ContainsKey("M1870FeedingOrEating") && assessmentQuestions["M1870FeedingOrEating"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1870FeedingOrEating"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(2));
            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1880_CUR_PREP_LT_MEALS") && assessmentQuestions.ContainsKey("M1880AbilityToPrepareLightMeal") && assessmentQuestions["M1880AbilityToPrepareLightMeal"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1880AbilityToPrepareLightMeal"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(18));
            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1890_CUR_PHONE_USE") && assessmentQuestions.ContainsKey("M1890AbilityToUseTelephone") && assessmentQuestions["M1890AbilityToUseTelephone"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M1890AbilityToUseTelephone"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(21));
            if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
            {
                if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2310_ECR_MEDICATION") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMed")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2310_ECR_MEDICATION") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMed")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }
            submissionFormat.Append(string.Empty.PadLeft(5));
            if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
            {
                if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2310_ECR_HYPOGLYC") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHypo")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.IsNotNullOrEmpty())
                                {

                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2310_ECR_HYPOGLYC") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHypo")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.IsNotNullOrEmpty())
                            {

                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }
            submissionFormat.Append(string.Empty.PadLeft(2));
            if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
            {
                if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                if (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(7));
            if (type.Contains("06") || type.Contains("07"))
            {
                if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                    {
                        if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_MED") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMed")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_MED") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMed")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMed"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }
            submissionFormat.Append(string.Empty.PadLeft(3));
            if (type.Contains("06") || type.Contains("07"))
            {
                if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                    {
                        if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_HYPOGLYC") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHypo")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_HYPOGLYC") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHypo")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHypo"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }
            submissionFormat.Append(string.Empty.PadLeft(5));
            if (type.Contains("06") || type.Contains("07"))
            {
                if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                    {
                        if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_UR_TRACT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUrinaryInf")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_UR_TRACT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUrinaryInf")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }
            submissionFormat.Append(string.Empty.PadLeft(1));
            if (type.Contains("06") || type.Contains("07"))
            {
                if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                    {
                        if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("00");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDVT")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUncontrolledPain")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M2430_HOSP_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDVT")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUncontrolledPain")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            submissionFormat.Append(string.Empty.PadLeft(2));
            if (type.Contains("06") || type.Contains("07"))
            {
                if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "01") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }
                else
                {

                    if (submissionGuide.ContainsKey("M2440_NH_UNKNOWN") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedUnknown")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M2440ReasonPatientAdmittedUnknown"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0000001");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2440_NH_THERAPY") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedTherapy")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_RESPITE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedRespite")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_HOSPICE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedHospice")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_PERMANENT") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedPermanent")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_UNSAFE_HOME") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedUnsafe")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2440_NH_OTHER") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedOther")) //M0050_PAT_ST
                            {
                                if (assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M2440_NH_THERAPY") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedTherapy")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedTherapy"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2440_NH_RESPITE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedRespite")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedRespite"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2440_NH_HOSPICE") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedHospice")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedHospice"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2440_NH_PERMANENT") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedPermanent")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedPermanent"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2440_NH_UNSAFE_HOME") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedUnsafe")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedUnsafe"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2440_NH_OTHER") && assessmentQuestions.ContainsKey("M2440ReasonPatientAdmittedOther")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2440ReasonPatientAdmittedOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(7));
            }

            if (type.Contains("06") || type.Contains("07") || type.Contains("08") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M0903_LAST_HOME_VISIT") && assessmentQuestions.ContainsKey("M0903LastHomeVisitDate") && assessmentQuestions["M0903LastHomeVisitDate"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0903LastHomeVisitDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                }
                if (submissionGuide.ContainsKey("M0906_DC_TRAN_DTH_DT") && assessmentQuestions.ContainsKey("M0906DischargeDate") && assessmentQuestions["M0906DischargeDate"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0906DischargeDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(16));
            }
            submissionFormat.Append(string.Empty.PadLeft(6));

            if (type.Contains("01") || type.Contains("03"))
            {
                if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone")) //M0050_PAT_ST
                {
                    if (assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append("0");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1000_DC_SNF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesSNF")) //M0050_PAT_ST
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1000_DC_SNF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesSNF")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesSNF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }
            submissionFormat.Append(string.Empty.PadLeft(2));
            if (type.Contains("01") || type.Contains("03"))
            {
                if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone")) //M0050_PAT_ST
                {
                    if (assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append("0");
                    }
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }
            submissionFormat.Append(string.Empty.PadLeft(14));
            if (submissionGuide.ContainsKey("NATL_PROV_ID") && agency.NationalProviderNumber.IsNotNullOrEmpty() && agency.NationalProviderNumber.Length == 10) //M0050_PAT_ST
            {
                submissionFormat.Append(agency.NationalProviderNumber.ToUpper().PartOfString(0, 10).PadLeft(10));
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(10));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
            {
                if (submissionGuide.ContainsKey("M0110_EPISODE_TIMING") && assessmentQuestions.ContainsKey("M0110EpisodeTiming") && assessmentQuestions["M0110EpisodeTiming"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(assessmentQuestions["M0110EpisodeTiming"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_A3") && assessmentQuestions.ContainsKey("M1024ICD9MA3") && assessmentQuestions["M1024ICD9MA3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MA3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }

                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_B3") && assessmentQuestions.ContainsKey("M1024ICD9MB3") && assessmentQuestions["M1024ICD9MB3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MB3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }

                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_C3") && assessmentQuestions.ContainsKey("M1024ICD9MC3") && assessmentQuestions["M1024ICD9MC3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MC3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }
                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_D3") && assessmentQuestions.ContainsKey("M1024ICD9MD3") && assessmentQuestions["M1024ICD9MD3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MD3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }
                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_E3") && assessmentQuestions.ContainsKey("M1024ICD9ME3") && assessmentQuestions["M1024ICD9ME3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9ME3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }
                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_F3") && assessmentQuestions.ContainsKey("M1024ICD9MF3") && assessmentQuestions["M1024ICD9MF3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MF3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }
                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_A4") && assessmentQuestions.ContainsKey("M1024ICD9MA4") && assessmentQuestions["M1024ICD9MA4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MA4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }
                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_B4") && assessmentQuestions.ContainsKey("M1024ICD9MB4") && assessmentQuestions["M1024ICD9MB4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MB4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }
                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_C4") && assessmentQuestions.ContainsKey("M1024ICD9MC4") && assessmentQuestions["M1024ICD9MC4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MC4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }
                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_D4") && assessmentQuestions.ContainsKey("M1024ICD9MD4") && assessmentQuestions["M1024ICD9MD4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MD4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }

                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_E4") && assessmentQuestions.ContainsKey("M1024ICD9ME4") && assessmentQuestions["M1024ICD9ME4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9ME4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }
                if (submissionGuide.ContainsKey("M1024_PMT_DIAG_ICD_F4") && assessmentQuestions.ContainsKey("M1024ICD9MF4") && assessmentQuestions["M1024ICD9MF4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1024ICD9MF4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(7));
                }

                if (submissionGuide.ContainsKey("M2200_THER_NEED_NA") && !assessmentQuestions.ContainsKey("M2200TherapyNeedNA") && !assessmentQuestions.ContainsKey("M2200NumberOfTherapyNeed"))
                {
                    submissionFormat.Append(string.Empty.PadLeft(3));
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                else  // M0032_ROC_DT and M0032_ROC_DT_NA
                {
                    if ((submissionGuide.ContainsKey("M2200_THER_NEED_NA") && assessmentQuestions.ContainsKey("M2200TherapyNeedNA")))
                    {
                        if (assessmentQuestions["M2200TherapyNeedNA"].Answer == "1")
                        {
                            submissionFormat.Append(string.Empty.PadLeft(3));
                            submissionFormat.Append("1");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2200_THER_NEED_NUM") && assessmentQuestions.ContainsKey("M2200NumberOfTherapyNeed")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2200NumberOfTherapyNeed"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2200NumberOfTherapyNeed"].Answer.PartOfString(0, 3).PadLeft(3, '0'));
                                    submissionFormat.Append("0".PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append(string.Empty.PadLeft(3));
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(3));
                                submissionFormat.Append("0");
                            }
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(3));
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }


            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(90));
            }
            submissionFormat.Append(string.Empty.PadLeft(1));
            if (type.Contains("01") || type.Contains("03"))
            {
                if ((submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT_NA") && assessmentQuestions.ContainsKey("M0102PhysicianOrderedDateNotApplicable") && !assessmentQuestions["M0102PhysicianOrderedDateNotApplicable"].Answer.IsNotNullOrEmpty()) || (submissionGuide.ContainsKey("M2200_THER_NEED_NA") && !assessmentQuestions.ContainsKey("M0102PhysicianOrderedDateNotApplicable"))) // M0032_ROC_DT and M0032_ROC_DT_NA
                {

                    if (submissionGuide.ContainsKey("M0102_PHYSN_ORDRD_SOCROC_DT") && assessmentQuestions.ContainsKey("M0102PhysicianOrderedDate") && assessmentQuestions["M0102PhysicianOrderedDate"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0102PhysicianOrderedDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }
                    submissionFormat.Append("0");
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                    submissionFormat.Append("1");
                }

                if (submissionGuide.ContainsKey("M0104_PHYSN_RFRL_DT") && assessmentQuestions.ContainsKey("M0104ReferralDate") && assessmentQuestions["M0104ReferralDate"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(DateTime.Parse(assessmentQuestions["M0104ReferralDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(8));
                }
                if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone")) //M0050_PAT_ST
                {
                    if (assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append("000000");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1000_DC_LTC_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTC")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_IPPS_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIPPS")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_LTCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTCH")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_IRF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIRF")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_PSYCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesPhych")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1000_DC_OTH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesOTHR")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
                else
                {


                    if (submissionGuide.ContainsKey("M1000_DC_LTC_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTC")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTC"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1000_DC_IPPS_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIPPS")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIPPS"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1000_DC_LTCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesLTCH")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesLTCH"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1000_DC_IRF_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesIRF")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesIRF"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1000_DC_PSYCH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesPhych")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesPhych"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1000_DC_OTH_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesOTHR")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1000InpatientFacilitiesOTHR"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
                if (submissionGuide.ContainsKey("M1000_DC_NONE_14_DA") && assessmentQuestions.ContainsKey("M1000InpatientFacilitiesNone") && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1000InpatientFacilitiesNone"].Answer == "1")
                {
                    submissionFormat.Append(string.Empty.PadLeft(58));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1010_14_DAY_INP3_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode3") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode3"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1010_14_DAY_INP4_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode4") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode4"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1010_14_DAY_INP5_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode5") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode5"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode5"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1010_14_DAY_INP6_ICD") && assessmentQuestions.ContainsKey("M1010InpatientFacilityDiagnosisCode6") && assessmentQuestions["M1010InpatientFacilityDiagnosisCode6"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1010InpatientFacilityDiagnosisCode6"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1012_INP_NA_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCodeNotApplicable") && assessmentQuestions["M1012InpatientFacilityProcedureCodeNotApplicable"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1012InpatientFacilityProcedureCodeNotApplicable"].Answer == "1") //M0010_CCN
                    {
                        submissionFormat.Append(string.Empty.PadLeft(28));
                        submissionFormat.Append("10");
                    }
                    else if ((submissionGuide.ContainsKey("M1012_INP_UK_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCodeUnknown") && assessmentQuestions["M1012InpatientFacilityProcedureCodeUnknown"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1012InpatientFacilityProcedureCodeUnknown"].Answer == "1"))
                    {
                        submissionFormat.Append(string.Empty.PadLeft(28));
                        submissionFormat.Append("01");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1012_INP_PRCDR1_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode1") && assessmentQuestions["M1012InpatientFacilityProcedureCode1"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode1"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1012_INP_PRCDR2_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode2") && assessmentQuestions["M1012InpatientFacilityProcedureCode2"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode2"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1012_INP_PRCDR3_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode3") && assessmentQuestions["M1012InpatientFacilityProcedureCode3"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode3"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        if (submissionGuide.ContainsKey("M1012_INP_PRCDR4_ICD") && assessmentQuestions.ContainsKey("M1012InpatientFacilityProcedureCode4") && assessmentQuestions["M1012InpatientFacilityProcedureCode4"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                        {
                            submissionFormat.Append(string.Format("{0}{1}", string.Empty.PadRight(2), assessmentQuestions["M1012InpatientFacilityProcedureCode4"].Answer.ToUpper().PadRight(5, ' ')).PartOfString(0, 7));
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(7));
                        }
                        submissionFormat.Append("00");
                    }
                }
                if (submissionGuide.ContainsKey("M1016_CHGREG_ICD_NA") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisNotApplicable") && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1016MedicalRegimenDiagnosisNotApplicable"].Answer == "1") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(14));
                    submissionFormat.Append("1");
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1016_CHGREG_ICD5") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode5") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode5"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode5"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    if (submissionGuide.ContainsKey("M1016_CHGREG_ICD6") && assessmentQuestions.ContainsKey("M1016MedicalRegimenDiagnosisCode6") && assessmentQuestions["M1016MedicalRegimenDiagnosisCode6"].Answer.IsNotNullOrEmpty()) //M0050_PAT_ST
                    {
                        submissionFormat.Append(string.Format("{0}{1}", " ", assessmentQuestions["M1016MedicalRegimenDiagnosisCode6"].Answer.ToUpper().PadRight(6, ' ')).PartOfString(0, 7));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(7));
                    }
                    submissionFormat.Append("0");
                }

                if (submissionGuide.ContainsKey("M1032_HOSP_RISK_NONE_ABOVE") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskNone")) //M0010_CCN
                {
                    if (assessmentQuestions["M1032HospitalizationRiskNone"].Answer.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append("0000001");
                    }
                    else
                    {
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_RCNT_DCLN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskRecentDecline")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_MLTPL_HOSPZTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMultipleHosp")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_HSTRY_FALLS") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskHistoryOfFall")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_5PLUS_MDCTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMedications")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_FRAILTY") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskFrailty")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M1032_HOSP_RISK_OTHR") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskOther")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1032HospitalizationRiskOther"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append("0");
                    }
                }
                else
                {

                    if (submissionGuide.ContainsKey("M1032_HOSP_RISK_RCNT_DCLN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskRecentDecline")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskRecentDecline"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }

                    if (submissionGuide.ContainsKey("M1032_HOSP_RISK_MLTPL_HOSPZTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMultipleHosp")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMultipleHosp"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1032_HOSP_RISK_HSTRY_FALLS") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskHistoryOfFall")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskHistoryOfFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1032_HOSP_RISK_5PLUS_MDCTN") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskMedications")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskMedications"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1032_HOSP_RISK_FRAILTY") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskFrailty")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskFrailty"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    if (submissionGuide.ContainsKey("M1032_HOSP_RISK_OTHR") && assessmentQuestions.ContainsKey("M1032HospitalizationRiskOther")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1032HospitalizationRiskOther"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append(assessmentQuestions["M1032HospitalizationRiskOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                        }
                        else
                        {
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (submissionGuide.ContainsKey("M1034_PTNT_OVRAL_STUS") && assessmentQuestions.ContainsKey("M1034OverallStatus") && assessmentQuestions["M1034OverallStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1034OverallStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(8));
                submissionFormat.Append(string.Empty.PadLeft(1));
                submissionFormat.Append(string.Empty.PadLeft(8));
                submissionFormat.Append(string.Empty.PadLeft(6));
                submissionFormat.Append(string.Empty.PadLeft(56));
                submissionFormat.Append(string.Empty.PadLeft(2));
                submissionFormat.Append(string.Empty.PadLeft(14));
                submissionFormat.Append(string.Empty.PadLeft(8));
                submissionFormat.Append(string.Empty.PadLeft(2));

            }

            if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1040_INFLNZ_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1040InfluenzaVaccine") && assessmentQuestions["M1040InfluenzaVaccine"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1040InfluenzaVaccine"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if ((submissionGuide.ContainsKey("M1040_INFLNZ_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1040InfluenzaVaccine") && assessmentQuestions["M1040InfluenzaVaccine"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1040InfluenzaVaccine"].Answer == "01") || (submissionGuide.ContainsKey("M1040_INFLNZ_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1040InfluenzaVaccine") && assessmentQuestions["M1040InfluenzaVaccine"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1040InfluenzaVaccine"].Answer == "NA")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1045_INFLNZ_RSN_NOT_RCVD") && assessmentQuestions.ContainsKey("M1045InfluenzaVaccineNotReceivedReason") && assessmentQuestions["M1045InfluenzaVaccineNotReceivedReason"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1045InfluenzaVaccineNotReceivedReason"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                if (submissionGuide.ContainsKey("M1050_PPV_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1050PneumococcalVaccine") && assessmentQuestions["M1050PneumococcalVaccine"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1050PneumococcalVaccine"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }

                if (submissionGuide.ContainsKey("M1050_PPV_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1050PneumococcalVaccine") && assessmentQuestions["M1050PneumococcalVaccine"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1050PneumococcalVaccine"].Answer == "1") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {

                    if (submissionGuide.ContainsKey("M1055_PPV_RSN_NOT_RCVD_AGNCY") && assessmentQuestions.ContainsKey("M1055PPVNotReceivedReason") && assessmentQuestions["M1055PPVNotReceivedReason"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1055PPVNotReceivedReason"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(4));
                submissionFormat.Append(string.Empty.PadLeft(1));
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            if (type.Contains("01") || type.Contains("03"))
            {
                if (submissionGuide.ContainsKey("M1100_PTNT_LVG_STUTN") && assessmentQuestions.ContainsKey("M1100LivingSituation") && assessmentQuestions["M1100LivingSituation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1100LivingSituation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M1210_HEARG_ABLTY") && assessmentQuestions.ContainsKey("M1210Hearing") && assessmentQuestions["M1210Hearing"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1210Hearing"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M1220_UNDRSTG_VERBAL_CNTNT") && assessmentQuestions.ContainsKey("M1220VerbalContent") && assessmentQuestions["M1220VerbalContent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1220VerbalContent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M1240_FRML_PAIN_ASMT") && assessmentQuestions.ContainsKey("M1240FormalPainAssessment") && assessmentQuestions["M1240FormalPainAssessment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1240FormalPainAssessment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(8));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1242_PAIN_FREQ_ACTVTY_MVMT") && assessmentQuestions.ContainsKey("M1242PainInterferingFrequency") && assessmentQuestions["M1242PainInterferingFrequency"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1242PainInterferingFrequency"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            if (type.Contains("01") || type.Contains("03"))
            {
                if (submissionGuide.ContainsKey("M1300_PRSR_ULCR_RISK_ASMT") && assessmentQuestions.ContainsKey("M1300PressureUlcerAssessment") && assessmentQuestions["M1300PressureUlcerAssessment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1300PressureUlcerAssessment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M1300_PRSR_ULCR_RISK_ASMT") && assessmentQuestions.ContainsKey("M1300PressureUlcerAssessment") && assessmentQuestions["M1300PressureUlcerAssessment"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1300PressureUlcerAssessment"].Answer == "00") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                else
                {

                    if (submissionGuide.ContainsKey("M1302_RISK_OF_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1302RiskDevelopingPressureUlcers") && assessmentQuestions["M1302RiskDevelopingPressureUlcers"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1302RiskDevelopingPressureUlcers"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
                submissionFormat.Append(string.Empty.PadLeft(1));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(1));
            }
            if (type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(10));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1307_OLDST_STG2_ONST_DT") && assessmentQuestions.ContainsKey("M1307NonEpithelializedStageTwoUlcerDate") && assessmentQuestions["M1307NonEpithelializedStageTwoUlcerDate"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(DateTime.Parse(assessmentQuestions["M1307NonEpithelializedStageTwoUlcerDate"].Answer).ToString("yyyyMMdd").PartOfString(0, 8).PadLeft(8));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(8));
                    }
                    if (submissionGuide.ContainsKey("M1307_OLDST_STG2_AT_DSCHRG") && assessmentQuestions.ContainsKey("M1307NonEpithelializedStageTwoUlcer") && assessmentQuestions["M1307NonEpithelializedStageTwoUlcer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1307NonEpithelializedStageTwoUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(10));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG2") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NBR_STG2_AT_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageTwoUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG3") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NBR_STG3_AT_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageThreeUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NBR_PRSULC_STG4") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageFourUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NBR_STG4_AT_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedStageIVUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NSTG_DRSG") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NSTG_DRSG_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NSTG_CVRG") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NSTG_CVRG_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NSTG_DEEP_TISUE") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1308_NSTG_DEEP_TISUE_SOC_ROC") && assessmentQuestions.ContainsKey("M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission") && assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2, '0'));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1306_UNHLD_STG2_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1306UnhealedPressureUlcers") && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1306UnhealedPressureUlcers"].Answer == "0") //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(14));
                }
                else
                {

                    if (submissionGuide.ContainsKey("M1310_PRSR_ULCR_LNGTH") && assessmentQuestions.ContainsKey("M1310PressureUlcerLength") && assessmentQuestions["M1310PressureUlcerLength"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1310PressureUlcerLength"].Answer.ToUpper().PartOfString(0, 4).PadLeft(4));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(4));
                    }
                    if (submissionGuide.ContainsKey("M1312_PRSR_ULCR_WDTH") && assessmentQuestions.ContainsKey("M1312PressureUlcerWidth") && assessmentQuestions["M1312PressureUlcerWidth"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1312PressureUlcerWidth"].Answer.ToUpper().PartOfString(0, 4).PadLeft(4));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(4));
                    }
                    if (submissionGuide.ContainsKey("M1314_PRSR_ULCR_DEPTH") && assessmentQuestions.ContainsKey("M1314PressureUlcerDepth") && assessmentQuestions["M1314PressureUlcerDepth"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1314PressureUlcerDepth"].Answer.ToUpper().PartOfString(0, 4).PadLeft(4));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(4));
                    }
                    if (submissionGuide.ContainsKey("M1320_STUS_PRBLM_PRSR_ULCR") && assessmentQuestions.ContainsKey("M1320MostProblematicPressureUlcerStatus") && assessmentQuestions["M1320MostProblematicPressureUlcerStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1320MostProblematicPressureUlcerStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(14));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1330_STAS_ULCR_PRSNT") && assessmentQuestions.ContainsKey("M1330StasisUlcer") && assessmentQuestions["M1330StasisUlcer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1330StasisUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if ((submissionGuide.ContainsKey("M1330_STAS_ULCR_PRSNT") && assessmentQuestions.ContainsKey("M1330StasisUlcer") && assessmentQuestions["M1330StasisUlcer"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1330StasisUlcer"].Answer == "00") || (submissionGuide.ContainsKey("M1330_STAS_ULCR_PRSNT") && assessmentQuestions.ContainsKey("M1330StasisUlcer") && assessmentQuestions["M1330StasisUlcer"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1330StasisUlcer"].Answer == "03")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(4));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1332_NUM_STAS_ULCR") && assessmentQuestions.ContainsKey("M1332CurrentNumberStasisUlcer") && assessmentQuestions["M1332CurrentNumberStasisUlcer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1332CurrentNumberStasisUlcer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                    if (submissionGuide.ContainsKey("M1334_STUS_PRBLM_STAS_ULCR") && assessmentQuestions.ContainsKey("M1334StasisUlcerStatus") && assessmentQuestions["M1334StasisUlcerStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1334StasisUlcerStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                if (submissionGuide.ContainsKey("M1340_SRGCL_WND_PRSNT") && assessmentQuestions.ContainsKey("M1340SurgicalWound") && assessmentQuestions["M1340SurgicalWound"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1340SurgicalWound"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if ((submissionGuide.ContainsKey("M1340_SRGCL_WND_PRSNT") && assessmentQuestions.ContainsKey("M1340SurgicalWound") && assessmentQuestions["M1340SurgicalWound"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1340SurgicalWound"].Answer == "00") || (submissionGuide.ContainsKey("M1340_SRGCL_WND_PRSNT") && assessmentQuestions.ContainsKey("M1340SurgicalWound") && assessmentQuestions["M1340SurgicalWound"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1340SurgicalWound"].Answer == "02")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1342_STUS_PRBLM_SRGCL_WND") && assessmentQuestions.ContainsKey("M1342SurgicalWoundStatus") && assessmentQuestions["M1342SurgicalWoundStatus"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1342SurgicalWoundStatus"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
                if (submissionGuide.ContainsKey("M1350_LESION_OPEN_WND") && assessmentQuestions.ContainsKey("M1350SkinLesionOpenWound") && assessmentQuestions["M1350SkinLesionOpenWound"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1350SkinLesionOpenWound"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(10));
                submissionFormat.Append(string.Empty.PadLeft(1));
            }

            if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1500HeartFailureSymptons"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if ((submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1500HeartFailureSymptons"].Answer == "00") || (submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1500HeartFailureSymptons"].Answer == "02") || (submissionGuide.ContainsKey("M1500_SYMTM_HRT_FAILR_PTNTS") && assessmentQuestions.ContainsKey("M1500HeartFailureSymptons") && assessmentQuestions["M1500HeartFailureSymptons"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1500HeartFailureSymptons"].Answer == "NA")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(6));
                }
                {
                    if (submissionGuide.ContainsKey("M1510_HRT_FAILR_NO_ACTN") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupNoAction")) //M0010_CCN
                    {
                        if (assessmentQuestions["M1510HeartFailureFollowupNoAction"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("100000");
                        }
                        else
                        {
                            submissionFormat.Append("0");
                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_CNTCT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupPhysicianCon")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_ER_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupAdvisedEmg")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupParameters")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CLNCL_INTRVTN") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupInterventions")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CARE_PLAN_CHG") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupChange")) //M0010_CCN
                            {
                                if (assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                        if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_CNTCT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupPhysicianCon")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupPhysicianCon"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1510_HRT_FAILR_ER_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupAdvisedEmg")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupAdvisedEmg"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1510_HRT_FAILR_PHYSN_TRTMT") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupParameters")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupParameters"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CLNCL_INTRVTN") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupInterventions")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupInterventions"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M1510_HRT_FAILR_CARE_PLAN_CHG") && assessmentQuestions.ContainsKey("M1510HeartFailureFollowupChange")) //M0010_CCN
                        {
                            if (assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M1510HeartFailureFollowupChange"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }


                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
                submissionFormat.Append(string.Empty.PadLeft(6));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if ((submissionGuide.ContainsKey("M1610_UR_INCONT") && assessmentQuestions.ContainsKey("M1610UrinaryIncontinence") && assessmentQuestions["M1610UrinaryIncontinence"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1610UrinaryIncontinence"].Answer == "00") || (submissionGuide.ContainsKey("M1610_UR_INCONT") && assessmentQuestions.ContainsKey("M1610UrinaryIncontinence") && assessmentQuestions["M1610UrinaryIncontinence"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M1610UrinaryIncontinence"].Answer == "02")) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M1615_INCNTNT_TIMING") && assessmentQuestions.ContainsKey("M1615UrinaryIncontinenceOccur") && assessmentQuestions["M1615UrinaryIncontinenceOccur"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M1615UrinaryIncontinenceOccur"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03"))
            {
                if (submissionGuide.ContainsKey("M1730_STDZ_DPRSN_SCRNG") && assessmentQuestions.ContainsKey("M1730DepressionScreening") && assessmentQuestions["M1730DepressionScreening"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1730DepressionScreening"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M1730_PHQ2_LACK_INTRST") && assessmentQuestions.ContainsKey("M1730DepressionScreeningInterest") && assessmentQuestions["M1730DepressionScreeningInterest"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1730DepressionScreeningInterest"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M1730_PHQ2_DPRSN") && assessmentQuestions.ContainsKey("M1730DepressionScreeningHopeless") && assessmentQuestions["M1730DepressionScreeningHopeless"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1730DepressionScreeningHopeless"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(6));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1830_CRNT_BATHG") && assessmentQuestions.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && assessmentQuestions["M1830CurrentAbilityToBatheEntireBody"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1830CurrentAbilityToBatheEntireBody"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M1840_CUR_TOILTG") && assessmentQuestions.ContainsKey("M1840ToiletTransferring") && assessmentQuestions["M1840ToiletTransferring"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1840ToiletTransferring"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(4));
            }


            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1845_CUR_TOILTG_HYGN") && assessmentQuestions.ContainsKey("M1845ToiletingHygiene") && assessmentQuestions["M1845ToiletingHygiene"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1845ToiletingHygiene"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }


            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M1850_CUR_TRNSFRNG") && assessmentQuestions.ContainsKey("M1850Transferring") && assessmentQuestions["M1850Transferring"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1850Transferring"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M1860_CRNT_AMBLTN") && assessmentQuestions.ContainsKey("M1860AmbulationLocomotion") && assessmentQuestions["M1860AmbulationLocomotion"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1860AmbulationLocomotion"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(4));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
            {
                //if (submissionGuide.ContainsKey("SUBM_HIPPS_CODE") && assessmentQuestions.ContainsKey("HIIPSCODE") && assessmentQuestions["HIIPSCODE"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                //{
                //    submissionFormat.Append(assessmentQuestions["HIIPSCODE"].Answer.ToUpper().PartOfString(0, 5).PadLeft(5));
                //}
                //else
                {
                    submissionFormat.Append("0".PadLeft(5, '0'));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(5));
            }
            submissionFormat.Append(string.Empty.PadLeft(5));

            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05"))
            {
                //if (submissionGuide.ContainsKey("SUBM_HIPPS_VERSION") && assessmentQuestions.ContainsKey("HIIPSVERSION") && assessmentQuestions["HIIPSVERSION"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                //{
                //    submissionFormat.Append(assessmentQuestions["HIIPSVERSION"].Answer.ToUpper().PartOfString(0, 5).PadLeft(5));
                //}
                //else
                {
                    submissionFormat.Append("0".PadLeft(5, '0'));
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(5));
            }

            if (type.Contains("01") || type.Contains("03"))
            {
                if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_SELF") && assessmentQuestions.ContainsKey("M1900SelfCareFunctioning") && assessmentQuestions["M1900SelfCareFunctioning"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1900SelfCareFunctioning"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_AMBLTN") && assessmentQuestions.ContainsKey("M1900Ambulation") && assessmentQuestions["M1900Ambulation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1900Ambulation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_TRNSFR") && assessmentQuestions.ContainsKey("M1900Transfer") && assessmentQuestions["M1900Transfer"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1900Transfer"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M1900_PRIOR_ADLIADL_HSEHOLD") && assessmentQuestions.ContainsKey("M1900HouseHoldTasks") && assessmentQuestions["M1900HouseHoldTasks"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1900HouseHoldTasks"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M1910_MLT_FCTR_FALL_RISK_ASMT") && assessmentQuestions.ContainsKey("M1910FallRiskAssessment") && assessmentQuestions["M1910FallRiskAssessment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M1910FallRiskAssessment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2000DrugRegimenReview"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "00") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "01") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(1));
                }
                else
                {

                    if (submissionGuide.ContainsKey("M2002_MDCTN_FLWP") && assessmentQuestions.ContainsKey("M2002MedicationFollowup") && assessmentQuestions["M2002MedicationFollowup"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2002MedicationFollowup"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(12));
                submissionFormat.Append(string.Empty.PadLeft(1));
            }

            if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
            {
                if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "00") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "01") || (submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2004_MDCTN_INTRVTN") && assessmentQuestions.ContainsKey("M2004MedicationIntervention") && assessmentQuestions["M2004MedicationIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2004MedicationIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03"))
            {
                if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2010_HIGH_RISK_DRUG_EDCTN") && assessmentQuestions.ContainsKey("M2010PatientOrCaregiverHighRiskDrugEducation") && assessmentQuestions["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
            {
                if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2015_DRUG_EDCTN_INTRVTN") && assessmentQuestions.ContainsKey("M2015PatientOrCaregiverDrugEducationIntervention") && assessmentQuestions["M2015PatientOrCaregiverDrugEducationIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2015PatientOrCaregiverDrugEducationIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2020_CRNT_MGMT_ORAL_MDCTN") && assessmentQuestions.ContainsKey("M2020ManagementOfOralMedications") && assessmentQuestions["M2020ManagementOfOralMedications"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2020ManagementOfOralMedications"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }
            if (type.Contains("01") || type.Contains("03") || type.Contains("04") || type.Contains("05") || type.Contains("09"))
            {
                if ((submissionGuide.ContainsKey("M2000_DRUG_RGMN_RVW") && assessmentQuestions.ContainsKey("M2000DrugRegimenReview") && assessmentQuestions["M2000DrugRegimenReview"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2000DrugRegimenReview"].Answer == "NA")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2030_CRNT_MGMT_INJCTN_MDCTN") && assessmentQuestions.ContainsKey("M2030ManagementOfInjectableMedications") && assessmentQuestions["M2030ManagementOfInjectableMedications"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2030ManagementOfInjectableMedications"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("01") || type.Contains("03"))
            {
                if (submissionGuide.ContainsKey("M2040_PRIOR_MGMT_ORAL_MDCTN") && assessmentQuestions.ContainsKey("M2040PriorMedicationInject") && assessmentQuestions["M2040PriorMedicationInject"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2040PriorMedicationInject"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2040_PRIOR_MGMT_INJCTN_MDCTN") && assessmentQuestions.ContainsKey("M2040PriorMedicationInject") && assessmentQuestions["M2040PriorMedicationInject"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2040PriorMedicationInject"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(4));
            }

            if (type.Contains("01") || type.Contains("03") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_ADL") && assessmentQuestions.ContainsKey("M2100ADLAssistance") && assessmentQuestions["M2100ADLAssistance"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2100ADLAssistance"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_IADL") && assessmentQuestions.ContainsKey("M2100IADLAssistance") && assessmentQuestions["M2100IADLAssistance"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2100IADLAssistance"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_MDCTN") && assessmentQuestions.ContainsKey("M2100MedicationAdministration") && assessmentQuestions["M2100MedicationAdministration"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2100MedicationAdministration"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_PRCDR") && assessmentQuestions.ContainsKey("M2100MedicalProcedures") && assessmentQuestions["M2100MedicalProcedures"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2100MedicalProcedures"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_EQUIP") && assessmentQuestions.ContainsKey("M2100ManagementOfEquipment") && assessmentQuestions["M2100ManagementOfEquipment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2100ManagementOfEquipment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_SPRVSN") && assessmentQuestions.ContainsKey("M2100SupervisionAndSafety") && assessmentQuestions["M2100SupervisionAndSafety"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2100SupervisionAndSafety"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2100_CARE_TYPE_SRC_ADVCY") && assessmentQuestions.ContainsKey("M2100FacilitationPatientParticipation") && assessmentQuestions["M2100FacilitationPatientParticipation"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2100FacilitationPatientParticipation"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2110_ADL_IADL_ASTNC_FREQ") && assessmentQuestions.ContainsKey("M2110FrequencyOfADLOrIADLAssistance") && assessmentQuestions["M2110FrequencyOfADLOrIADLAssistance"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2110FrequencyOfADLOrIADLAssistance"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(16));
            }

            if (type.Contains("01") || type.Contains("03"))
            {
                if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PTNT_SPECF") && assessmentQuestions.ContainsKey("M2250PatientParameters") && assessmentQuestions["M2250PatientParameters"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2250PatientParameters"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_DBTS_FT_CARE") && assessmentQuestions.ContainsKey("M2250DiabeticFoot") && assessmentQuestions["M2250DiabeticFoot"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2250DiabeticFoot"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_FALL_PRVNT") && assessmentQuestions.ContainsKey("M2250FallsPrevention") && assessmentQuestions["M2250FallsPrevention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2250FallsPrevention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_DPRSN_INTRVTN") && assessmentQuestions.ContainsKey("M2250DepressionPrevention") && assessmentQuestions["M2250DepressionPrevention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2250DepressionPrevention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PAIN_INTRVTN") && assessmentQuestions.ContainsKey("M2250MonitorMitigatePainIntervention") && assessmentQuestions["M2250MonitorMitigatePainIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2250MonitorMitigatePainIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PRSULC_PRVNT") && assessmentQuestions.ContainsKey("M2250PressureUlcerIntervention") && assessmentQuestions["M2250PressureUlcerIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2250PressureUlcerIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2250_PLAN_SMRY_PRSULC_TRTMT") && assessmentQuestions.ContainsKey("M2250PressureUlcerTreatment") && assessmentQuestions["M2250PressureUlcerTreatment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2250PressureUlcerTreatment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(14));
            }

            if (type.Contains("06") || type.Contains("07") || type.Contains("09"))
            {
                if (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2300EmergentCare"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if ((submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "00") || (submissionGuide.ContainsKey("M2300_EMER_USE_AFTR_LAST_ASMT") && assessmentQuestions.ContainsKey("M2300EmergentCare") && assessmentQuestions["M2300EmergentCare"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2300EmergentCare"].Answer == "UK")) //M0010_CCN
                {
                    submissionFormat.Append(string.Empty.PadLeft(17));
                }
                {
                    if (submissionGuide.ContainsKey("M2310_ECR_UNKNOWN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUK")) //M0050_PAT_ST
                    {
                        if (assessmentQuestions["M2310ReasonForEmergentCareUK"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("00000000000000000");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2310_ECR_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareFall")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareResInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOtherResInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_HRT_FAILR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartFail")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareCardiac")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMyocardial")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartDisease")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_STROKE_TIA") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareStroke")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_GI_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareGI")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDehMal")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_UTI") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUrinaryInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareIV")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_WND_INFCTN_DTRORTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareWoundInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_UNCNTLD_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUncontrolledPain")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMental")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2310_ECR_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDVT")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2310_ECR_OTHER") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOther")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                        }
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M2310_ECR_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareFall")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareResInf")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2310_ECR_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOtherResInf")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOtherResInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2310_ECR_HRT_FAILR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartFail")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2310_ECR_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareCardiac")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2310_ECR_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMyocardial")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2310_ECR_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareHeartDisease")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2310_ECR_STROKE_TIA") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareStroke")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2310_ECR_GI_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareGI")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2310_ECR_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDehMal")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2310_ECR_UTI") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUrinaryInf")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUrinaryInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2310_ECR_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareIV")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2310_ECR_WND_INFCTN_DTRORTN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareWoundInf")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2310_ECR_UNCNTLD_PAIN") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareUncontrolledPain")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareUncontrolledPain"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2310_ECR_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareMental")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2310_ECR_DVT_PULMNRY") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareDVT")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareDVT"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2310_ECR_OTHER") && assessmentQuestions.ContainsKey("M2310ReasonForEmergentCareOther")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2310ReasonForEmergentCareOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                    }
                }
                if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_DBTS_FT") && assessmentQuestions.ContainsKey("M2400DiabeticFootCare") && assessmentQuestions["M2400DiabeticFootCare"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2400DiabeticFootCare"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_FALL_PRVNT") && assessmentQuestions.ContainsKey("M2400FallsPreventionInterventions") && assessmentQuestions["M2400FallsPreventionInterventions"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2400FallsPreventionInterventions"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_DPRSN") && assessmentQuestions.ContainsKey("M2400DepressionIntervention") && assessmentQuestions["M2400DepressionIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2400DepressionIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

                if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_PAIN_MNTR") && assessmentQuestions.ContainsKey("M2400PainIntervention") && assessmentQuestions["M2400PainIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2400PainIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_PRSULC_PRVN") && assessmentQuestions.ContainsKey("M2400PressureUlcerIntervention") && assessmentQuestions["M2400PressureUlcerIntervention"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2400PressureUlcerIntervention"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                if (submissionGuide.ContainsKey("M2400_INTRVTN_SMRY_PRSULC_WET") && assessmentQuestions.ContainsKey("M2400PressureUlcerTreatment") && assessmentQuestions["M2400PressureUlcerTreatment"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                {
                    submissionFormat.Append(assessmentQuestions["M2400PressureUlcerTreatment"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }

            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
                submissionFormat.Append(string.Empty.PadLeft(17));
                submissionFormat.Append(string.Empty.PadLeft(12));
            }
            if (type.Contains("09"))
            {
                if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "01") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Empty.PadLeft(2));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2420_DSCHRG_DISP") && assessmentQuestions.ContainsKey("M2420DischargeDisposition") && assessmentQuestions["M2420DischargeDisposition"].Answer.IsNotNullOrEmpty()) //M0010_CCN
                    {
                        submissionFormat.Append(assessmentQuestions["M2420DischargeDisposition"].Answer.ToUpper().PartOfString(0, 2).PadLeft(2));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(2));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(2));
            }

            if (type.Contains("06") || type.Contains("07"))
            {
                if ((submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "02") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "03") || (submissionGuide.ContainsKey("M2410_INPAT_FACILITY") && assessmentQuestions.ContainsKey("M2410TypeOfInpatientFacility") && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer.IsNotNullOrEmpty() && assessmentQuestions["M2410TypeOfInpatientFacility"].Answer == "04")) //M0050_PAT_ST
                {
                    submissionFormat.Append(string.Empty.PadLeft(16));
                }
                else
                {
                    if (submissionGuide.ContainsKey("M2430_HOSP_UK") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationUK")) //M0010_CCN
                    {
                        if (assessmentQuestions["M2430ReasonForHospitalizationUK"].Answer.IsNotNullOrEmpty())
                        {
                            submissionFormat.Append("0000000000000001");
                        }
                        else
                        {
                            if (submissionGuide.ContainsKey("M2430_HOSP_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationFall")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationInfection")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOtherRP")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2430_HOSP_HRT_FAILR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartFail")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }

                            if (submissionGuide.ContainsKey("M2430_HOSP_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationCardiac")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMyocardial")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }

                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartDisease")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }

                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_STROKE_TIA") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationStroke")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_GI_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationGI")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDehMal")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationIV")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_WND_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationWoundInf")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }

                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMental")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_SCHLD_TRTMT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationScheduled")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            if (submissionGuide.ContainsKey("M2430_HOSP_OTHER") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOther")) //M0010_CCN
                            {
                                if (assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.IsNotNullOrEmpty())
                                {
                                    submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                                }
                                else
                                {
                                    submissionFormat.Append("0");
                                }
                            }
                            else
                            {
                                submissionFormat.Append(string.Empty.PadLeft(1));
                            }
                            submissionFormat.Append("0");
                        }
                    }
                    else
                    {

                        if (submissionGuide.ContainsKey("M2430_HOSP_INJRY_BY_FALL") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationFall")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationFall"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationInfection")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationInfection"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2430_HOSP_RSPRTRY_OTHR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOtherRP")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOtherRP"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2430_HOSP_HRT_FAILR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartFail")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartFail"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }

                        if (submissionGuide.ContainsKey("M2430_HOSP_CRDC_DSRTHM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationCardiac")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationCardiac"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_MI_CHST_PAIN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMyocardial")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMyocardial"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }

                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_OTHR_HRT_DEASE") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationHeartDisease")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationHeartDisease"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }

                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_STROKE_TIA") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationStroke")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationStroke"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_GI_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationGI")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationGI"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_DHYDRTN_MALNTR") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationDehMal")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationDehMal"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_CTHTR_CMPLCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationIV")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationIV"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_WND_INFCTN") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationWoundInf")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationWoundInf"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }

                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_MENTL_BHVRL_PRBLM") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationMental")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationMental"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));

                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_SCHLD_TRTMT") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationScheduled")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationScheduled"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        if (submissionGuide.ContainsKey("M2430_HOSP_OTHER") && assessmentQuestions.ContainsKey("M2430ReasonForHospitalizationOther")) //M0010_CCN
                        {
                            if (assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.IsNotNullOrEmpty())
                            {
                                submissionFormat.Append(assessmentQuestions["M2430ReasonForHospitalizationOther"].Answer.ToUpper().PartOfString(0, 1).PadLeft(1));
                            }
                            else
                            {
                                submissionFormat.Append("0");
                            }
                        }
                        else
                        {
                            submissionFormat.Append(string.Empty.PadLeft(1));
                        }
                        submissionFormat.Append(string.Empty.PadLeft(1));
                    }
                }
            }
            else
            {
                submissionFormat.Append(string.Empty.PadLeft(16));
            }

            submissionFormat.Append(string.Empty.PadLeft(244));
            submissionFormat.Append("%");
            var length = submissionFormat.Length;
            return submissionFormat.ToString().ToUpper();
        }

        public string GetOasisSubmissionFormat(List<SubmissionBodyFormat> submissionGuide, IDictionary<string, Question> assessmentQuestions, int versionNumber)
        {
            var submissionFormat = new StringBuilder();
            string type = assessmentQuestions["M0100AssessmentType"].Answer;
            var agency = agencyRepository.Get(Current.AgencyId);

            submissionGuide.ForEach(data =>
            {
                var key = data.Item;
                if (key == "REC_ID")
                {
                    submissionFormat.Append("B1".PadLeft((int)(data.Length)));
                }
                else if (key == "CORRECTION_NUM")
                {
                    submissionFormat.Append(versionNumber.ToString().PadLeft(2, '0'));
                }
                else if (key == "DATA_END")
                {
                    submissionFormat.Append("%".PadLeft((int)(data.Length)));
                }
                else if (key == "VERSION_CD1")
                {
                    submissionFormat.Append("C-072009".PadRight((int)(data.Length)));
                }
                else if (key == "VERSION_CD2")
                {
                    submissionFormat.Append("02.00".PadLeft((int)(data.Length)));
                }
                else if (key == "CRG_RTN")
                {
                }
                else if (key == "LN_FD")
                {
                }
                else
                {
                    string questionKey = data.ElementName;
                    if (questionKey != null && IdentifyAssessment(data, type) == "1")
                    {
                        if (assessmentQuestions.ContainsKey(questionKey))
                        {
                            if (assessmentQuestions[questionKey].Answer != null && assessmentQuestions[questionKey].Answer != string.Empty)
                            {
                                if (data.DataType == "DATE")
                                {
                                    submissionFormat.Append(Pad(DateTime.Parse(assessmentQuestions[questionKey].Answer).ToString("yyyyMMdd"), data.PadType.ToString(), (int)(data.Length)));
                                }
                                else
                                {
                                    submissionFormat.Append(Pad(assessmentQuestions[questionKey].Answer, data.PadType.ToString(), (int)(data.Length)));
                                }
                            }
                            else
                            {
                                submissionFormat.Append(Pad(data.DefaultValue, data.PadType.ToString(), (int)(data.Length)));
                            }
                        }
                        else
                        {
                            submissionFormat.Append(Pad(data.DefaultValue, data.PadType.ToString(), (int)(data.Length)));
                        }
                    }
                    else if (questionKey == null && IdentifyAssessment(data, type) == "1")
                    {
                        submissionFormat.Append(Pad(data.DefaultValue, data.PadType.ToString(), (int)(data.Length)));
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft(((int)data.Length)));
                    }
                }
            });

            return submissionFormat.ToString();
        }

        public string OasisHeader(Agency agency)
        {
            List<SubmissionHeaderFormat> submissionGuide = oasisDataProvider.CachedDataRepository.GetSubmissionHeaderFormatInstructions();
            var submissionFormat = new StringBuilder();
            submissionGuide.ForEach(data =>
            {
                var key = data.Item;
                if (key == "REC_ID")
                {
                    submissionFormat.Append("A1".PadLeft((int)(data.Length)));
                }
                else if (key == "FED_ID")
                {
                    if (agency.MedicareProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agency.MedicareProviderNumber.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));

                    }
                }
                else if (key == "FILLER1")
                {
                    submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                }
                else if (key == "ST_ID")
                {
                    if (agency.MedicaidProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agency.MedicaidProviderNumber.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                    }
                }
                else if (key == "HHA_AGENCY_ID")
                {
                    if (agency.HomeHealthAgencyId.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agency.HomeHealthAgencyId.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                    }
                }
                else if (key == "ACY_NAME")
                {
                    if (agency.Name.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agency.Name.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                    }
                }
                else if (key == "ACY_ADDR_1")
                {
                    if (agency.AddressLine1.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agency.AddressLine1.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                    }
                }
                else if (key == "ACY_ADDR_2")
                {
                    if (agency.AddressLine2.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agency.AddressLine2.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                    }
                }
                else if (key == "ACY_CITY")
                {
                    if (agency.AddressCity.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agency.AddressCity.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                    }
                }
                else if (key == "ACY_ST")
                {
                    if (agency.AddressStateCode.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agency.AddressStateCode.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                    }
                }
                else if (key == "ACY_ZIP")
                {
                    if (agency.AddressZipCode.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agency.AddressZipCode.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                    }
                }
                else if (key == "ACY_CNTCT")
                {
                    submissionFormat.Append(string.Format("{0}, {1}", agency.ContactPersonLastName.IsNotNullOrEmpty() ? agency.ContactPersonLastName : string.Empty, agency.ContactPersonFirstName.IsNotNullOrEmpty() ? agency.ContactPersonFirstName : string.Empty).PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }
                else if (key == "ACY_PHONE")
                {
                    if (agency.ContactPersonPhone.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agency.ContactPersonPhone.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                    }
                }
                else if (key == "ACY_EXTEN")
                {
                    submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                }
                else if (key == "SFW_ID")
                {
                    submissionFormat.Append(data.DefaultValue.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }
                else if (key == "SFW_NAME")
                {
                    submissionFormat.Append(data.DefaultValue.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }
                else if (key == "SFW_ADDR_1")
                {
                    submissionFormat.Append(data.DefaultValue.PadRight((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }
                else if (key == "SFW_ADDR_2")
                {
                    submissionFormat.Append(data.DefaultValue.PadRight((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }
                else if (key == "SFW_CITY")
                {
                    submissionFormat.Append(data.DefaultValue.PadRight((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }
                else if (key == "SFW_ST")
                {
                    submissionFormat.Append(data.DefaultValue.PadRight((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }
                else if (key == "SFW_ZIP")
                {
                    submissionFormat.Append(data.DefaultValue.PadRight((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }
                else if (key == "SFW_CNTCT")
                {
                    submissionFormat.Append(data.DefaultValue.PadRight((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }
                else if (key == "SFW_PHONE")
                {
                    submissionFormat.Append(data.DefaultValue.PadRight((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }
                else if (key == "SFW_EXTEN")
                {
                    submissionFormat.Append(string.Empty.PadRight((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }
                else if (key == "FILE_DT")
                {
                    submissionFormat.Append(DateTime.Now.ToString("yyyyMMdd").ToUpper());
                }
                else if (key == "TEST_SW")
                {
                    submissionFormat.Append(data.DefaultValue.PadRight((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                }

                else if (key == "NATL_PROV_ID")
                {
                    if (agency.NationalProviderNumber.IsNotNullOrEmpty())
                    {
                        submissionFormat.Append(agency.NationalProviderNumber.PadLeft((int)(data.Length)).PartOfString(0, (int)(data.Length)).ToUpper());
                    }
                    else
                    {
                        submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                    }
                }
                else if (key == "HDR_FL")
                {
                    submissionFormat.Append(string.Empty.PadLeft(((int)data.Length)));
                }
                else if (key == "DATA_END")
                {
                    submissionFormat.Append("%".PadLeft((int)(data.Length)));
                }

                else if (key == "CRG_RTN")
                {
                    submissionFormat.Append("\r".PadLeft(((int)data.Length)));
                }
                else if (key == "LN_FD")
                {
                    submissionFormat.Append("\n".PadLeft(((int)data.Length)));
                }
                else
                {
                    submissionFormat.Append(string.Empty.PadLeft((int)(data.Length)));
                }
            });
            return submissionFormat.ToString();
        }

        public string OasisFooter(int totalNumberOfRecord)
        {
            var footerString = new string(' ', 1446);
            var footerStringWithRecId = footerString.Remove(0, 2).Insert(0, "Z1");
            var footerStringWithTotalRec = footerStringWithRecId.Remove(2, 6).Insert(2, totalNumberOfRecord.ToString().PadLeft(6, '0'));
            var footerStringWithDataEnd = footerStringWithTotalRec.Remove(1445, 1).Insert(1445, "%");
            return footerStringWithDataEnd;
        }

        public IDictionary<string, Question> GetPlanofCareAssessment(Guid episodeId, Guid patientId)
        {
            var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);

            if (episode != null)
            {
                return GetAssessment(episode.AssessmentId, episode.AssessmentType).ToDictionary();
            }
            return null;
        }

        public Assessment GetPlanofCareAssessmentObject(Guid episodeId, Guid patientId)
        {
            var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);

            if (episode != null)
            {
                return GetAssessment(episode.AssessmentId, episode.AssessmentType);
            }
            return null;
        }

        public bool UpdatePlanofCare(FormCollection formCollection)
        {
            Guid planofCareId = formCollection.Get("Id").ToGuid();
            Guid episodeId = formCollection.Get("EpisodeId").ToGuid();
            Guid patientId = formCollection.Get("PatientId").ToGuid();
            var planofCare = oasisDataProvider.PlanofCareRepository.Get(Current.AgencyId, episodeId, patientId, planofCareId);
            if (planofCare != null)
            {
                var status = formCollection.Get("Status");
                var signedDate = DateTime.Parse(formCollection.Get("SignatureDate"));
                if (status.IsNotNullOrEmpty())
                {
                    planofCare.Status = int.Parse(status);
                    if (planofCare.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
                    {
                        var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, planofCareId);
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician).ToString();
                            if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            {
                                planofCare.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                                planofCare.SignatureDate = signedDate;
                            }
                        }
                    }
                }
                ProcessPlanofCare(formCollection, planofCare);
                planofCare.Data = planofCare.Questions.ToXml();
                return oasisDataProvider.PlanofCareRepository.Update(planofCare);
            }
            return false;
        }

        public void GeneratePlanofCare(ScheduleEvent scheduleEvent, Patient patient, Assessment assessment)
        {
            Get485FromAssessment(scheduleEvent, patient, assessment);
        }

        public IAssessment InsertMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication, string MedicationType)
        {
            var assessment = oasisDataProvider.OasisAssessmentRepository.Get(AssessmentId, AssessmentType);
            string medicationProfile = "";
            if (assessment != null)
            {
                medicationProfile = assessment.MedicationProfile;
            }
            medication.Id = Guid.NewGuid();
            medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
            medication.MedicationType = new MedicationType { Value = MedicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), MedicationType, true)).GetDescription() };
            if (!medicationProfile.IsNotNullOrEmpty() || medicationProfile == "")
            {
                var newList = new List<Medication>();
                newList.Add(medication);
                assessment.MedicationProfile = newList.ToXml<List<Medication>>();
                oasisDataProvider.OasisAssessmentRepository.Update(assessment, AssessmentType);
            }
            else
            {
                var existingList = assessment.MedicationProfile.ToObject<List<Medication>>();
                existingList.Add(medication);
                assessment.MedicationProfile = existingList.ToXml<List<Medication>>();
                oasisDataProvider.OasisAssessmentRepository.Update(assessment, AssessmentType);
            }
            return assessment;
        }

        public IAssessment DeleteMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication)
        {
            var assessment = oasisDataProvider.OasisAssessmentRepository.Get(AssessmentId, AssessmentType);
            string medicationProfile = "";
            if (assessment != null)
            {
                medicationProfile = assessment.MedicationProfile;
            }
            if (!medicationProfile.IsNotNullOrEmpty() || medicationProfile == "")
            {
                return assessment;
            }
            else
            {
                var existingList = assessment.MedicationProfile.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == medication.Id))
                {
                    existingList.RemoveAll(m => m.Id == medication.Id);
                    assessment.MedicationProfile = existingList.ToXml<List<Medication>>();
                    oasisDataProvider.OasisAssessmentRepository.Update(assessment, AssessmentType);
                }
            }
            return assessment;
        }

        public bool UpdateMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication, string MedicationType)
        {
            var assessment = oasisDataProvider.OasisAssessmentRepository.Get(AssessmentId, AssessmentType);
            string medicationProfile = "";
            if (assessment != null)
            {
                medicationProfile = assessment.MedicationProfile;
            }
            if (medicationProfile.IsNotNullOrEmpty() && medicationProfile != "")
            {
                var existingList = assessment.MedicationProfile.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == medication.Id))
                {
                    var med = existingList.SingleOrDefault(m => m.Id == medication.Id);
                    med.IsLongStanding = medication.IsLongStanding;
                    med.StartDate = medication.StartDate;
                    med.MedicationDosage = medication.MedicationDosage;
                    med.Route = medication.Route;
                    med.Frequency = medication.Frequency;
                    med.MedicationType = new MedicationType { Value = MedicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), MedicationType, true)).GetDescription() }; ;
                    med.Classification = medication.Classification;
                    if (med.MedicationCategory == "DC")
                    {
                        med.DCDate = medication.DCDate;
                    }
                    assessment.MedicationProfile = existingList.ToXml<List<Medication>>();
                    oasisDataProvider.OasisAssessmentRepository.Update(assessment, AssessmentType);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateMedicationForDischarge(Guid AssessmentId, Guid PatientId, string AssessmentType, Guid Id, DateTime DischargeDate)
        {
            var assessment = oasisDataProvider.OasisAssessmentRepository.Get(AssessmentId, AssessmentType);
            string medicationProfile = "";
            if (assessment != null)
            {
                medicationProfile = assessment.MedicationProfile;
            }
            bool result = false;
            if (medicationProfile.IsNotNullOrEmpty() && medicationProfile != "")
            {
                var existingList = assessment.MedicationProfile.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == Id))
                {
                    var med = existingList.SingleOrDefault(m => m.Id == Id);
                    med.DCDate = DischargeDate;
                    med.LastChangedDate = DateTime.Now;
                    med.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                    assessment.MedicationProfile = existingList.ToXml<List<Medication>>();
                    oasisDataProvider.OasisAssessmentRepository.Update(assessment, AssessmentType);
                    result = true;
                }
            }
            return result;
        }

        public Medication GetMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Guid Id)
        {
            Medication med = null;
            var assessment = oasisDataProvider.OasisAssessmentRepository.Get(AssessmentId, AssessmentType);
            string medicationProfile = "";
            if (assessment != null)
            {
                medicationProfile = assessment.MedicationProfile;
            }
            if (!medicationProfile.IsNotNullOrEmpty() || medicationProfile == "")
            {
                return med;
            }
            else
            {
                var existingList = assessment.MedicationProfile.ToObject<List<Medication>>();
                med = existingList.SingleOrDefault(m => m.Id == Id);
            }
            return med;
        }

        public bool UpdateAssessmentStatus(Guid Id, Guid patientId, Guid episodeId, string assessmentType, string status , string reason)
        {
            bool result = false;
            var assessment = oasisDataProvider.OasisAssessmentRepository.Get(Id, patientId, episodeId, assessmentType);
            if (assessment != null)
            {
                if (status == ((int)ScheduleStatus.OasisReopened).ToString()) //assessment.Status == (int)ScheduleStatus.OasisExported &&
                {
                    assessment.VersionNumber += 1;
                }
                if (assessment.Type == AssessmentType.Recertification || assessment.Type == AssessmentType.StartOfCare)
                {
                    if (assessment.Status == (int)ScheduleStatus.OasisCompletedPendingReview || assessment.Status == (int)ScheduleStatus.OasisCompletedExportReady)
                    {
                        patientRepository.SetRecertFlag(Current.AgencyId, episodeId, patientId, true);
                    }
                    else
                    {
                        patientRepository.SetRecertFlag(Current.AgencyId, episodeId, patientId, false);
                    }
                }
                assessment.Status = int.Parse(status);
                if (oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType))
                {
                    var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, Id);
                    if (scheduleEvent != null)
                    {
                        scheduleEvent.Status = status;
                        scheduleEvent.ReturnReason = reason;
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, assessment.PatientId, scheduleEvent.EventId);
                            if (userEvent != null)
                            {
                                userEvent.Status = status;
                                userEvent.ReturnReason = reason;
                                userRepository.UpdateEvent(userEvent);
                                result = true;
                            }
                            else
                            {
                                userEvent = new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason };
                                userRepository.UpdateEvent(Current.AgencyId, patientId, scheduleEvent.UserId, userEvent);
                                result = true;
                            }

                            if (assessment.Status == (int)ScheduleStatus.OasisCompletedPendingReview && (assessment.Type == AssessmentType.StartOfCare || assessment.Type == AssessmentType.Recertification))
                            {
                                string episodeStartDate = string.Empty;
                                var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                                if (episode != null)
                                {
                                    episodeStartDate = episode.StartDate.ToShortDateString();
                                }

                                var newScheduleEvent = new ScheduleEvent
                                {
                                    Discipline = Disciplines.Orders.ToString(),
                                    DisciplineTask = (int)DisciplineTasks.HCFA485,
                                    Status = ((int)ScheduleStatus.OrderSaved).ToString(),
                                    EventId = Guid.NewGuid(),
                                    PatientId = assessment.PatientId,
                                    UserId = scheduleEvent.UserId,
                                    EpisodeId = scheduleEvent.EpisodeId,
                                    EventDate = episodeStartDate,
                                    StartDate = episode.StartDate,
                                    EndDate = episode.EndDate
                                };
                                var patient = patientRepository.Get(patientId, Current.AgencyId);
                                GeneratePlanofCare(newScheduleEvent, patient, assessment);
                                if (!patientRepository.UpdateEpisode(Current.AgencyId, episodeId, patientId, new List<ScheduleEvent> { newScheduleEvent }))
                                {
                                    result = false;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool MarkAsExported(List<string> OasisSelected)
        {
            bool result = false;
            OasisSelected.ForEach(o =>
            {
                string[] data = o.Split('|');
                var assessment = oasisDataProvider.OasisAssessmentRepository.Get(data[0].ToGuid(), data[1]);
                if (assessment != null)
                {
                    assessment.Status = (int)ScheduleStatus.OasisExported;
                    if (oasisDataProvider.OasisAssessmentRepository.Update(assessment, data[1]))
                    {
                        var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, assessment.EpisodeId, assessment.PatientId, assessment.Id);
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.Status = ((int)ScheduleStatus.OasisExported).ToString();
                            if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            {
                                var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, assessment.PatientId, assessment.Id);
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OasisExported).ToString();
                                    userRepository.UpdateEvent(userEvent);
                                    result = true;
                                }
                                else
                                {
                                    userEvent = new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason };
                                    userRepository.UpdateEvent(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.UserId, userEvent);
                                    result = true;
                                }
                            }
                        }
                    }
                }
            });
            return result;
        }

        public ValidationInfoViewData Validate(Guid assessmentId, Guid patientId, Guid episodeId, string assessmentType)
        {
            Check.Argument.IsNotEmpty(assessmentId, "assessmentId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");

            string sessionKey = string.Format("{0}_{1}", assessmentType, assessmentId.ToString());
            Assessment assessment = SessionStore.Get<Assessment>(sessionKey);
            if (assessment == null)
            {
                assessment = oasisDataProvider.OasisAssessmentRepository.Get(assessmentId, assessmentType);
                SessionStore.Add<Assessment>(sessionKey, assessment);
            }

            var validationInfo = new ValidationInfoViewData();
            validationInfo.AssessmentId = assessmentId;
            validationInfo.AssessmentType = assessmentType;
            validationInfo.EpisodeId = episodeId;
            validationInfo.PatientId = patientId;

            var validationErrors = new List<ValidationError>();
            IDictionary<string, Question> assessmentQuestions = assessment.ToDictionary();

            if (assessmentQuestions != null)
            {
                if (assessmentQuestions.ContainsKey("HIIPSCODE"))
                {
                    assessmentQuestions["HIIPSCODE"] = new Question { Name = "HIIPSCODE", Answer = string.Empty.PadLeft(5) };
                }
                else
                {
                    assessmentQuestions.Add("HIIPSCODE", new Question { Name = "HIIPSCODE", Answer = string.Empty.PadLeft(5) });
                }

                if (assessmentQuestions.ContainsKey("HIIPSVERSION"))
                {
                    assessmentQuestions["HIIPSVERSION"] = new Question { Name = "HIIPSVERSION", Answer = string.Empty.PadLeft(5) };
                }
                else
                {
                    assessmentQuestions.Add("HIIPSVERSION", new Question { Name = "HIIPSVERSION", Answer = string.Empty.PadLeft(5) });
                }

                var oasisFormatString = GetOasisSubmissionFormatNew(GetOasisSubmissionFormatInstructionsNew(), assessmentQuestions, assessment.VersionNumber);
                if (oasisFormatString != null)
                {
                    validationErrors = validationAgent.ValidateAssessment(oasisFormatString);
                    int error = validationErrors.Where(e => e.ErrorType == "ERROR").Count();
                    if (validationErrors.Count > 0)
                    {
                        validationErrors.RemoveAt(0);
                    }
                    if (error == 0)
                    {
                        if (assessmentType == AssessmentType.StartOfCare.ToString() || assessmentType == AssessmentType.ResumptionOfCare.ToString() || assessmentType == AssessmentType.Recertification.ToString() || assessmentType == AssessmentType.FollowUp.ToString())
                        {
                            var hipps = grouperAgent.GetHippsCode(oasisFormatString);

                            if (hipps.ClaimMatchingKey.Trim() != string.Empty && hipps.ClaimMatchingKey.Trim().Length == 18 && hipps.Code.Trim() != string.Empty && hipps.Code.Trim().Length == 5 && hipps.Version.Trim() != string.Empty && hipps.Version.Trim().Length == 5)
                            {
                                // assessment.Status = (int)ScheduleStatus.CompletedOasis;
                                var hiipsCode = assessment.Questions.Find(q => q.Name == "HIIPSCODE");
                                if (hiipsCode == null)
                                {
                                    assessment.Questions.Add(new Question { Name = "HIIPSCODE", Answer = hipps.Code });
                                }
                                else
                                {
                                    hiipsCode.Answer = hipps.Code;
                                }
                                var hiipsVersion = assessment.Questions.Find(q => q.Name == "HIIPSVERSION");
                                if (hiipsVersion == null)
                                {
                                    assessment.Questions.Add(new Question { Name = "HIIPSVERSION", Answer = hipps.Version });
                                }
                                else
                                {
                                    hiipsVersion.Answer = hipps.Version;
                                }
                                var oasisFormatStringWithHippsCode = oasisFormatString.Remove(1080, 5).Insert(1080, hipps.Code);
                                var oasisFormatStringComplete = oasisFormatStringWithHippsCode.Remove(1090, 5).Insert(1090, hipps.Version);
                                oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType, hipps.Code.Trim(), hipps.ClaimMatchingKey.Trim(), hipps.Version.Trim(), oasisFormatStringComplete);
                                //var episode = patientRepository.GetEpisode(episodeId, assessment.PatientId);
                                //ScheduleEvent evnt = null;
                                //if (episode != null)
                                //{
                                //    evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId == assessmentId).FirstOrDefault();
                                //    //patientRepository.GetSchedule(episodeId, assessment.PatientId, assessmentId);
                                //    if (evnt != null)
                                //    {
                                //       // evnt.Status = ((int)ScheduleStatus.CompletedOasis).ToString();
                                //    }
                                //    if (patientRepository.UpdateEpisode(evnt))
                                //    {
                                //        var employeeEvent = userRepository.GetEvent(evnt.UserId, assessment.PatientId, evnt.EventId);
                                //        userRepository.UpdateEpisode(employeeEvent);
                                //    }
                                //if (assessmentType == AssessmentType.StartOfCare.ToString())
                                //{
                                //    var episodeAssessment = GetPlanofCareAssessmentObject(episode.Id, assessment.PatientId);
                                //    if (episodeAssessment != null)
                                //    {
                                //        var rap = billingRepository.GetRap(Current.AgencyId, assessment.PatientId, episodeId);

                                //        if (rap != null)
                                //        {
                                //            rap.IsOasisComplete = true;
                                //            rap.IsFirstBillableVisit = true;
                                //            if (assessmentQuestions["M0030SocDate"] != null && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty())
                                //            {
                                //                rap.StartofCareDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                                //                rap.FirstBillableVisitDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                                //            }
                                //            string diagnosis = "<DiagonasisCodes>";
                                //            if (assessmentQuestions["M1020ICD9M"] != null)
                                //            {
                                //                diagnosis += "<code1>" + assessmentQuestions["M1020ICD9M"].Answer + "</code1>";
                                //            }
                                //            else
                                //            {
                                //                diagnosis += "<code1></code1>";
                                //            }
                                //            if (assessmentQuestions["M1022ICD9M1"] != null)
                                //            {
                                //                diagnosis += "<code2>" + assessmentQuestions["M1022ICD9M1"].Answer + "</code2>";
                                //            }
                                //            else
                                //            {
                                //                diagnosis += "<code2></code2>";
                                //            }
                                //            if (assessmentQuestions["M1022ICD9M2"] != null)
                                //            {
                                //                diagnosis += "<code3>" + assessmentQuestions["M1022ICD9M2"].Answer + "</code3>";
                                //            }
                                //            else
                                //            {
                                //                diagnosis += "<code3></code3>";
                                //            }
                                //            if (assessmentQuestions["M1022ICD9M3"] != null)
                                //            {
                                //                diagnosis += "<code4>" + assessmentQuestions["M1022ICD9M3"].Answer + "</code4>";
                                //            }
                                //            else
                                //            {
                                //                diagnosis += "<code4></code4>";
                                //            }
                                //            if (assessmentQuestions["M1022ICD9M4"] != null)
                                //            {
                                //                diagnosis += "<code5>" + assessmentQuestions["M1022ICD9M4"].Answer + "</code5>";
                                //            }
                                //            else
                                //            {
                                //                diagnosis += "<code5></code5>";
                                //            }
                                //            if (assessmentQuestions["M1022ICD9M5"] != null)
                                //            {
                                //                diagnosis += "<code6>" + assessmentQuestions["M1022ICD9M5"].Answer + "</code6>";
                                //            }
                                //            else
                                //            {
                                //                diagnosis += "<code6></code6>";
                                //            }
                                //            diagnosis += "</DiagonasisCodes>";
                                //            rap.DiagonasisCode = diagnosis;
                                //            rap.HippsCode = hipps.Code;
                                //            rap.ClaimKey = hipps.ClaimMatchingKey;
                                //            billingRepository.UpdateRapStatus(rap);
                                //        }
                                //    }
                                //}
                                //else if (assessmentType == AssessmentType.ResumptionOfCare.ToString() || assessmentType == AssessmentType.Recertification.ToString())
                                //{
                                //    if (episode.HasNext)
                                //    {
                                //        var episodeAssessment = GetPlanofCareAssessmentObject(episode.NextEpisode.Id, assessment.PatientId);
                                //        if (episodeAssessment != null)
                                //        {
                                //            if (episodeAssessment.Id == assessmentId)
                                //            {
                                //                var rap = billingRepository.GetRap(Current.AgencyId, assessment.PatientId, episode.NextEpisode.Id);

                                //                if (rap != null)
                                //                {
                                //                    rap.IsOasisComplete = true;
                                //                    if (assessmentQuestions["M0030SocDate"] != null && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty())
                                //                    {
                                //                        rap.StartofCareDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);

                                //                    }
                                //                    string diagnosis = "<DiagonasisCodes>";
                                //                    if (assessmentQuestions["M1020ICD9M"] != null)
                                //                    {
                                //                        diagnosis += "<code1>" + assessmentQuestions["M1020ICD9M"].Answer + "</code1>";
                                //                    }
                                //                    else
                                //                    {
                                //                        diagnosis += "<code1></code1>";
                                //                    }
                                //                    if (assessmentQuestions["M1022ICD9M1"] != null)
                                //                    {
                                //                        diagnosis += "<code2>" + assessmentQuestions["M1022ICD9M1"].Answer + "</code2>";
                                //                    }
                                //                    else
                                //                    {
                                //                        diagnosis += "<code2></code2>";
                                //                    }
                                //                    if (assessmentQuestions["M1022ICD9M2"] != null)
                                //                    {
                                //                        diagnosis += "<code3>" + assessmentQuestions["M1022ICD9M2"].Answer + "</code3>";
                                //                    }
                                //                    else
                                //                    {
                                //                        diagnosis += "<code3></code3>";
                                //                    }
                                //                    if (assessmentQuestions["M1022ICD9M3"] != null)
                                //                    {
                                //                        diagnosis += "<code4>" + assessmentQuestions["M1022ICD9M3"].Answer + "</code4>";
                                //                    }
                                //                    else
                                //                    {
                                //                        diagnosis += "<code4></code4>";
                                //                    }
                                //                    if (assessmentQuestions["M1022ICD9M4"] != null)
                                //                    {
                                //                        diagnosis += "<code5>" + assessmentQuestions["M1022ICD9M4"].Answer + "</code5>";
                                //                    }
                                //                    else
                                //                    {
                                //                        diagnosis += "<code5></code5>";
                                //                    }
                                //                    if (assessmentQuestions["M1022ICD9M5"] != null)
                                //                    {
                                //                        diagnosis += "<code6>" + assessmentQuestions["M1022ICD9M5"].Answer + "</code6>";
                                //                    }
                                //                    else
                                //                    {
                                //                        diagnosis += "<code6></code6>";
                                //                    }
                                //                    diagnosis += "</DiagonasisCodes>";
                                //                    rap.DiagonasisCode = diagnosis;
                                //                    rap.HippsCode = hipps.Code;
                                //                    rap.ClaimKey = hipps.ClaimMatchingKey;
                                //                    billingRepository.UpdateRapStatus(rap);
                                //                }
                                //            }
                                //        }
                                //    }
                                //}
                                // }  
                                validationInfo.HIPPSCODE = hipps.Code;
                                validationInfo.HIPPSKEY = hipps.ClaimMatchingKey;
                                validationInfo.Count = validationErrors.Count;
                                validationInfo.validationError = validationErrors;
                                validationInfo.Message = "Your Hipps Code Generated Successfully and Data Is Saved";
                            }
                            else
                            {
                                validationInfo.validationError = validationErrors;
                                validationInfo.Count = validationErrors.Count;
                                validationInfo.validationError = validationErrors.OrderBy(m => m.ErrorType).ToList();
                            }
                        }
                        else if (assessmentType == AssessmentType.DischargeFromAgency.ToString() || assessmentType == AssessmentType.DischargeFromAgencyDeath.ToString() || assessmentType == AssessmentType.TransferInPatientDischarged.ToString() || assessmentType == AssessmentType.TransferInPatientNotDischarged.ToString())
                        {
                            //assessment.Status = (int)ScheduleStatus.CompletedOasis;
                            oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType, oasisFormatString);
                            // var evnt = patientRepository.GetSchedule(episodeId, assessment.PatientId, assessmentId);
                            // evnt.Status = ((int)ScheduleStatus.CompletedOasis).ToString();
                            // if (patientRepository.UpdateEpisode(evnt))
                            // {
                            //     var employeeEvent = userRepository.GetEvent(evnt.UserId, assessment.PatientId, evnt.EventId);
                            //     userRepository.UpdateEpisode(employeeEvent);
                            // }
                            validationInfo.Count = validationErrors.Count;
                            validationInfo.validationError = validationErrors;
                            validationInfo.Message = "Your Data Is Successfully Saved";
                        }
                    }
                    else
                    {
                        if (assessmentType == AssessmentType.StartOfCare.ToString() || assessmentType == AssessmentType.ResumptionOfCare.ToString() || assessmentType == AssessmentType.Recertification.ToString() || assessmentType == AssessmentType.FollowUp.ToString())
                        {
                            // assessment.Status = (int)ScheduleStatus.OpenOasis;
                            oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType, string.Empty, string.Empty, string.Empty, string.Empty);
                            //var episode = patientRepository.GetEpisode(episodeId, assessment.PatientId);
                            //ScheduleEvent evnt = null;
                            //if (episode != null)
                            //{
                            //    evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId == assessmentId).FirstOrDefault();
                            //    //patientRepository.GetSchedule(episodeId, assessment.PatientId, assessmentId);
                            //    if (evnt != null)
                            //    {
                            //       // evnt.Status = ((int)ScheduleStatus.OpenOasis).ToString();
                            //    }
                            //    if (patientRepository.UpdateEpisode(evnt))
                            //    {
                            //        var employeeEvent = userRepository.GetEvent(evnt.UserId, assessment.PatientId, evnt.EventId);
                            //         userRepository.UpdateEpisode(employeeEvent);
                            //    }
                            //    if (assessmentType == AssessmentType.StartOfCare.ToString())
                            //    {
                            //        var episodeAssessment = GetPlanofCareAssessmentObject(episode.Id, assessment.PatientId);
                            //        if (episodeAssessment != null)
                            //        {
                            //            var rap = billingRepository.GetRap(Current.AgencyId, assessment.PatientId, episodeId);

                            //            if (rap != null)
                            //            {
                            //                if (assessmentQuestions["M0030SocDate"] != null && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty())
                            //                {
                            //                    rap.StartofCareDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                            //                    rap.FirstBillableVisitDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                            //                }
                            //                string diagnosis = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5><code6></code6></DiagonasisCodes>";
                            //                rap.DiagonasisCode = diagnosis;
                            //                rap.HippsCode = "";
                            //                rap.ClaimKey = "";
                            //                rap.IsOasisComplete = false;
                            //                rap.IsFirstBillableVisit = false;
                            //                billingRepository.UpdateRapStatus(rap);
                            //            }
                            //        }
                            //    }
                            //    else if (assessmentType == AssessmentType.ResumptionOfCare.ToString() || assessmentType == AssessmentType.Recertification.ToString())
                            //    {
                            //        if (episode.HasNext)
                            //        {
                            //            var episodeAssessment = GetPlanofCareAssessmentObject(episode.NextEpisode.Id, assessment.PatientId);
                            //            if (episodeAssessment != null)
                            //            {
                            //                if (episodeAssessment.Id == assessmentId)
                            //                {
                            //                    var rap = billingRepository.GetRap(Current.AgencyId, assessment.PatientId, episode.NextEpisode.Id);

                            //                    if (rap != null)
                            //                    {
                            //                        if (assessmentQuestions["M0030SocDate"] != null && assessmentQuestions["M0030SocDate"].Answer.IsNotNullOrEmpty())
                            //                        {
                            //                            rap.StartofCareDate = DateTime.Parse(assessmentQuestions["M0030SocDate"].Answer);
                            //                        }
                            //                        string diagnosis = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5><code6></code6></DiagonasisCodes>";
                            //                        rap.DiagonasisCode = diagnosis;
                            //                        rap.IsOasisComplete = false;
                            //                        rap.HippsCode = "";
                            //                        rap.ClaimKey = "";
                            //                        billingRepository.UpdateRapStatus(rap);
                            //                    }
                            //                }
                            //            }
                            //        }
                            //    }
                            //} 
                        }
                        else if (assessmentType == AssessmentType.DischargeFromAgency.ToString() || assessmentType == AssessmentType.DischargeFromAgencyDeath.ToString() || assessmentType == AssessmentType.TransferInPatientDischarged.ToString() || assessmentType == AssessmentType.TransferInPatientNotDischarged.ToString())
                        {
                            //assessment.Status = (int)ScheduleStatus.OpenOasis;
                            oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType, string.Empty);
                            //var evnt = patientRepository.GetSchedule(episodeId, assessment.PatientId, assessmentId);
                            //evnt.Status = ((int)ScheduleStatus.OpenOasis).ToString();
                            //if (patientRepository.UpdateEpisode(evnt))
                            //{
                            //    var employeeEvent = userRepository.GetEvent(evnt.UserId, assessment.PatientId, evnt.EventId);
                            //    userRepository.UpdateEpisode(employeeEvent);
                            //}                          
                        }
                        validationInfo.validationError = validationErrors;
                        validationInfo.Count = validationErrors.Count;
                        validationInfo.validationError = validationErrors.OrderBy(m => m.ErrorType).ToList();
                    }
                }
            }
            return validationInfo;
        }

        public IDictionary<string, Question> Allergies(Guid assessmentId, string AssessmentType)
        {
            var allergies = new Dictionary<string, Question>();
            var assessment = this.GetAssessment(assessmentId, AssessmentType);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();
                if (questions.ContainsKey("485Allergies") && questions["485Allergies"] != null)
                {
                    allergies.Add("485Allergies", questions["485Allergies"]);
                }
                if (questions.ContainsKey("485AllergiesDescription") && questions["485AllergiesDescription"] != null)
                {
                    allergies.Add("485AllergiesDescription", questions["485AllergiesDescription"]);
                }
            }
            return allergies;
        }

        public IDictionary<string, Question> Diagnosis(Guid assessmentId, string AssessmentType)
        {
            var diagnosis = new Dictionary<string, Question>();
            var assessment = this.GetAssessment(assessmentId, AssessmentType);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();

                if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                {
                    diagnosis.Add("M1020PrimaryDiagnosis", questions["M1020PrimaryDiagnosis"]);
                }
                if (questions.ContainsKey("M1020ICD9M") && questions["M1020ICD9M"] != null)
                {
                    diagnosis.Add("M1020ICD9M", questions["M1020ICD9M"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis1", questions["M1022PrimaryDiagnosis1"]);
                }

                if (questions.ContainsKey("M1022ICD9M1") && questions["M1022ICD9M1"] != null)
                {
                    diagnosis.Add("M1022ICD9M1", questions["M1022ICD9M1"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis2") && questions["M1022PrimaryDiagnosis2"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis2", questions["M1022PrimaryDiagnosis2"]);
                }

                if (questions.ContainsKey("M1022ICD9M2") && questions["M1022ICD9M2"] != null)
                {
                    diagnosis.Add("M1022ICD9M2", questions["M1022ICD9M2"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis3") && questions["M1022PrimaryDiagnosis3"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis3", questions["M1022PrimaryDiagnosis3"]);
                }

                if (questions.ContainsKey("M1022ICD9M3") && questions["M1022ICD9M3"] != null)
                {
                    diagnosis.Add("M1022ICD9M3", questions["M1022ICD9M3"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis4") && questions["M1022PrimaryDiagnosis4"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis4", questions["M1022PrimaryDiagnosis4"]);
                }

                if (questions.ContainsKey("M1022ICD9M4") && questions["M1022ICD9M4"] != null)
                {
                    diagnosis.Add("M1022ICD9M4", questions["M1022ICD9M4"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis5") && questions["M1022PrimaryDiagnosis5"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis5", questions["M1022PrimaryDiagnosis5"]);
                }

                if (questions.ContainsKey("M1022ICD9M5") && questions["M1022ICD9M5"] != null)
                {
                    diagnosis.Add("M1022ICD9M5", questions["M1022ICD9M5"]);
                }

            }
            return diagnosis;
        }

        public bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, string name, Guid assetId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNull(name, "name");
            var result = false;
            var patientVisitNote = new PatientVisitNote();
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var assessment = oasisDataProvider.OasisAssessmentRepository.Get(eventId, patientId, episodeId, assessmentType);
                if (assessment != null && assessment.OasisData.IsNotNullOrEmpty())
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                    if (assessment.Questions.Exists(q => string.Format("{0}{1}", q.Type, q.Name) == name))
                    {
                        assessment.Questions.SingleOrDefault(q => string.Format("{0}{1}", q.Type, q.Name) == name).Answer = Guid.Empty.ToString();
                        if (oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType))
                        {
                            if (assetRepository.Delete(assetId))
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool AddSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, int supplyId, string quantity, string date)
        {
            var result = false;
            var sessionKey = string.Format("{0}_{1}", assessmentType, eventId);

            Assessment assessment = SessionStore.Get<Assessment>(sessionKey);
            if (assessment == null)
            {
                assessment = oasisDataProvider.OasisAssessmentRepository.Get(eventId, patientId, episodeId, assessmentType);
            }

            if (assessment != null)
            {
                if (assessment.Supply.IsNotNullOrEmpty())
                {
                    var supplies = assessment.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.Id == supplyId && s.Date == date))
                    {
                        var supply = supplies.SingleOrDefault(s => s.Id == supplyId && s.Date == date);
                        if (supply != null)
                        {
                            supply.Quantity = quantity;
                            supply.Date = date;
                            assessment.Supply = supplies.ToXml();
                            if (oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        var supply = lookupRepository.GetSupply(supplyId);
                        if (supply != null)
                        {
                            supply.Date = date;
                            supply.Quantity = quantity;
                            supply.UniqueIdentifier = Guid.NewGuid();
                            supplies.Add(supply);
                            assessment.Supply = supplies.ToXml();
                            if (oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType))
                            {
                                result = true;
                            }
                        }
                    }
                }
                else
                {
                    var supply = lookupRepository.GetSupply(supplyId);
                    if (supply != null)
                    {
                        var newSupplies = new List<Supply>();
                        supply.Date = date;
                        supply.Quantity = quantity;
                        supply.UniqueIdentifier = Guid.NewGuid();
                        newSupplies.Add(supply);
                        assessment.Supply = newSupplies.ToXml();
                        if (oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType))
                        {
                            result = true;
                        }
                    }
                }
                SessionStore.Add<Assessment>(sessionKey, assessment);
            }

            return result;
        }

        public bool UpdateSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            var result = false;
            var sessionKey = string.Format("{0}_{1}", assessmentType, eventId);

            Assessment assessment = SessionStore.Get<Assessment>(sessionKey);
            if (assessment == null)
            {
                assessment = oasisDataProvider.OasisAssessmentRepository.Get(eventId, patientId, episodeId, assessmentType);
            }

            if (assessment != null)
            {
                if (assessment.Supply.IsNotNullOrEmpty())
                {
                    var supplies = assessment.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        var editSupply = supplies.SingleOrDefault(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (editSupply != null)
                        {
                            editSupply.Quantity = supply.Quantity;
                            assessment.Supply = supplies.ToXml();
                            if (oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
                SessionStore.Add<Assessment>(sessionKey, assessment);
            }
            return result;
        }

        public bool DeleteSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            var result = false;
            var sessionKey = string.Format("{0}_{1}", assessmentType, eventId);
           
            Assessment assessment = SessionStore.Get<Assessment>(sessionKey);
            if (assessment == null)
            {
                assessment = oasisDataProvider.OasisAssessmentRepository.Get(eventId, patientId, episodeId, assessmentType);
            }

            if (assessment != null)
            {
                if (assessment.Supply.IsNotNullOrEmpty())
                {
                    var supplies = assessment.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        supplies.ForEach(s =>
                        {
                            if (s.UniqueIdentifier == supply.UniqueIdentifier)
                            {
                                supplies.Remove(s);
                            }
                        });
                        assessment.Supply = supplies.ToXml();
                        if (oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
                SessionStore.Add<Assessment>(sessionKey, assessment);
            }

            return result;
        }

        public List<Supply> GetAssessmentSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType)
        {
            var assessment = oasisDataProvider.OasisAssessmentRepository.Get(eventId, patientId, episodeId, assessmentType);
            var list = new List<Supply>();
            if (assessment != null && assessment.Supply.IsNotNullOrEmpty())
            {
                list = assessment.Supply.ToObject<List<Supply>>();
            }
            return list;
        }

        #endregion

        #region Private Members

        private Assessment AddAssessment(FormCollection formCollection)
        {
            string sessionKey = string.Empty;
            string assessmentType = formCollection["assessment"];
            string assessmentId = formCollection.Get(string.Format("{0}_Id", assessmentType));
            Assessment assessment = AssessmentFactory.Create(assessmentType);
            assessment.Id = Guid.NewGuid();
            assessment.Status = (int)ScheduleStatus.OasisSaved;
            assessment.Type = (AssessmentType)Enum.Parse(typeof(AssessmentType), assessmentType);
            assessment.PatientId = formCollection.Get(string.Format("{0}_PatientGuid", assessmentType)).ToGuid();
            this.Process(formCollection, assessment, out assessment);
            assessment.Id = oasisDataProvider.OasisAssessmentRepository.Add(assessment, assessmentType);
            if (!assessment.Id.IsEmpty())
            {
                sessionKey = string.Format("{0}_{1}", assessmentType, assessment.Id);
                SessionStore.Add<Assessment>(sessionKey, assessment);
            }
            return assessment;
        }

        private Assessment UpdateAssessment(FormCollection formCollection, HttpFileCollectionBase httpFiles)
        {
            string sessionKey = string.Empty;
            string assessmentType = formCollection["assessment"];
            string assessmentId = formCollection.Get(string.Format("{0}_Id", assessmentType));
            string episodeId = formCollection.Get(string.Format("{0}_EpisodeId", assessmentType));
            string category = formCollection["categoryType"];
            sessionKey = string.Format("{0}_{1}", assessmentType, assessmentId);

            Assessment assessment = SessionStore.Get<Assessment>(sessionKey);
            if (assessment == null)
            {
                assessment = oasisDataProvider.OasisAssessmentRepository.Get(assessmentId.ToGuid(), assessmentType);
                SessionStore.Add<Assessment>(sessionKey, assessment);
            }
            assessment.PatientId = formCollection.Get("{0}_PatientGuid".FormatWith(assessmentType)).ToGuid();
            assessment.Type = (AssessmentType)Enum.Parse(typeof(AssessmentType), assessmentType);
            this.Process(formCollection, assessment, out assessment);
            assessment.Status = (int)ScheduleStatus.OasisSaved;
            if (category == "Integumentary")
            {
                if (SaveAsset(httpFiles, assessment, out assessment))
                {
                    CompleteAssessmentUpdate(assessment, assessmentType);
                }
            }
            else
            {
                CompleteAssessmentUpdate(assessment, assessmentType);
            }
            SessionStore.Add<Assessment>(sessionKey, assessment);

            return assessment;
        }

        private bool CompleteAssessmentUpdate(Assessment assessment, string assessmentType)
        {
            bool result = false;
            if (oasisDataProvider.OasisAssessmentRepository.Update(assessment, assessmentType))
            {
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, assessment.EpisodeId, assessment.PatientId, assessment.Id);
                if (scheduleEvent != null)
                {
                    if (!(scheduleEvent.Status == ((int)ScheduleStatus.OasisReturnedForClinicianReview).ToString()))
                    {
                        scheduleEvent.Status = ((int)ScheduleStatus.OasisSaved).ToString();
                        scheduleEvent.ReturnReason = string.Empty;
                    }
                    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                    {
                        var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, assessment.PatientId, assessment.Id);
                        if (userEvent != null)
                        {
                            userEvent.Status = scheduleEvent.Status;
                            userEvent.ReturnReason = scheduleEvent.ReturnReason;
                            userRepository.UpdateEvent(userEvent);
                            result = true;
                        }
                        else
                        {
                            userRepository.UpdateEvent(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        private bool SaveAsset(HttpFileCollectionBase httpFiles, Assessment assessment, out Assessment recentAssessment)
        {
            bool assetSaved = false;
            recentAssessment = assessment;
            IDictionary<string, Question> questions = assessment.ToDictionary();

            if (httpFiles.Count > 0)
            {
                foreach (string key in httpFiles.AllKeys)
                {
                    var keyArray = key.Split('_');
                    HttpPostedFileBase file = httpFiles.Get(key);
                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        BinaryReader binaryReader = new BinaryReader(file.InputStream);
                        var asset = new Asset
                        {
                            FileName = file.FileName,
                            AgencyId = Current.AgencyId,
                            ContentType = file.ContentType,
                            FileSize = file.ContentLength.ToString(),
                            Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                        };
                        if (assetRepository.Add(asset))
                        {
                            if (questions.ContainsKey(keyArray[1]))
                            {
                                questions[keyArray[1]] = new Question { Name = keyArray[1].Remove(0, 7), Answer = asset.Id.ToString(), Type = QuestionType.Generic };
                            }
                            else
                            {
                                questions.Add(keyArray[1], new Question { Name = keyArray[1].Remove(0, 7), Answer = asset.Id.ToString(), Type = QuestionType.Generic });
                            }
                            assetSaved = true;
                        }
                        else
                        {
                            assetSaved = false;
                            if (questions.ContainsKey(keyArray[1]))
                            {
                                questions[keyArray[1]] = new Question { Name = keyArray[1].Remove(0, 7), Answer = Guid.Empty.ToString(), Type = QuestionType.Generic };
                            }
                            else
                            {
                                questions.Add(keyArray[1], new Question { Name = keyArray[1].Remove(0, 7), Answer = Guid.Empty.ToString(), Type = QuestionType.Generic });
                            }
                            break;
                        }
                    }
                    else
                    {
                        assetSaved = true;
                        if (questions.ContainsKey(keyArray[1]))
                        {
                            questions[keyArray[1]] = new Question { Name = keyArray[1].Remove(0, 7), Answer = Guid.Empty.ToString(), Type = QuestionType.Generic };
                        }
                        else
                        {
                            questions.Add(keyArray[1], new Question { Name = keyArray[1].Remove(0, 7), Answer = Guid.Empty.ToString(), Type = QuestionType.Generic });
                        }
                    }
                }
            }
            else
            {
                assetSaved = true;
            }
            recentAssessment.Questions = questions.Values.ToList();
            return assetSaved;
        }

        private void Process(FormCollection formCollection, Assessment assessment, out Assessment recentAssessment)
        {
            formCollection.Remove(formCollection["assessment"] + "_Id");
            formCollection.Remove(formCollection["assessment"] + "_Action");
            formCollection.Remove(formCollection["assessment"] + "_PatientGuid");
            formCollection.Remove(formCollection["assessment"] + "_EpisodeId");
            formCollection.Remove("assessment");
            formCollection.Remove("categoryType");
            recentAssessment = assessment;

            IDictionary<string, Question> questions = assessment.ToDictionary();

            foreach (var key in formCollection.AllKeys)
            {
                string[] nameArray = key.Split('_');
                if (nameArray.Length > 0)
                {
                    nameArray.Reverse();
                    string name = nameArray[0];
                    if (questions.ContainsKey(name))
                    {
                        questions[name] = Question.Create(name, formCollection.GetValues(key).Join(","));
                    }
                    else
                    {
                        questions.Add(name, Question.Create(name, formCollection.GetValues(key).Join(",")));
                    }
                }
            }
            recentAssessment.Questions = questions.Values.ToList();
        }

        private void ProcessPlanofCare(FormCollection formCollection, PlanofCare planofCare)
        {
            formCollection.Remove("Id");
            formCollection.Remove("EpisodeId");
            formCollection.Remove("PatientId");
            formCollection.Remove("Status");
            formCollection.Remove("SignatureText");
            formCollection.Remove("SignatureDate");

            if (planofCare.Data.IsNotNullOrEmpty())
            {
                planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                IDictionary<string, Question> questions = planofCare.ToDictionary();

                foreach (var key in formCollection.AllKeys)
                {
                    string answer = formCollection.GetValues(key).Join(",");
                    if (questions.ContainsKey(key))
                    {
                        questions[key].Answer = answer;
                    }
                    else
                    {
                        questions.Add(key, new Question { Answer = answer, Name = key, Type = QuestionType.PlanofCare });
                    }
                }
                planofCare.Questions = questions.Values.ToList();
            }
        }

        private bool Get485FromAssessment(ScheduleEvent scheduleEvent, Patient patient, Assessment assessment)
        {
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();
                if (questions != null && questions.Count > 0)
                {
                    var planofCare = new PlanofCare();
                    planofCare.OrderNumber = patientRepository.GetNextOrderNumber();
                    planofCare.Id = scheduleEvent.EventId;
                    planofCare.PatientId = scheduleEvent.PatientId;
                    planofCare.Status = scheduleEvent.Status.ToInteger();
                    planofCare.AssessmentId = assessment.Id;
                    planofCare.AssessmentType = assessment.Type.ToString();
                    planofCare.EpisodeId = scheduleEvent.EpisodeId;
                    planofCare.EpisodeStart = scheduleEvent.StartDate.ToShortDateString();
                    planofCare.EpisodeEnd = scheduleEvent.EndDate.ToShortDateString();

                    if (patient != null)
                    {
                        planofCare.PatientData = patient.ToXml();
                    }

                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        planofCare.AgencyId = agency.Id;
                        planofCare.AgencyData = agency.ToXml();
                    }

                    // 10. Medications
                    var medicationProfile = patientRepository.GetMedicationProfileByPatient(patient.Id, Current.AgencyId);
                    if (medicationProfile != null)
                    {
                        planofCare.Questions.Add(new Question { Name = "Medications", Type = QuestionType.PlanofCare, Answer = medicationProfile.ToString() });

                    }
                    // 11. Principal Diagnosis
                    if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                    {
                        planofCare.Questions.Add(questions["M1020PrimaryDiagnosis"]);
                    }
                    if (questions.ContainsKey("M1020ICD9M") && questions["M1020ICD9M"] != null)
                    {
                        planofCare.Questions.Add(questions["M1020ICD9M"]);
                    }

                    // 12. Surgical Procedure


                    // 13. Other Pertinent Diagnosis
                    if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                    {
                        planofCare.Questions.Add(questions["M1022PrimaryDiagnosis1"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M1") && questions["M1022ICD9M1"] != null)
                    {
                        planofCare.Questions.Add(questions["M1022ICD9M1"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis2") && questions["M1022PrimaryDiagnosis2"] != null)
                    {
                        planofCare.Questions.Add(questions["M1022PrimaryDiagnosis2"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M2") && questions["M1022ICD9M2"] != null)
                    {
                        planofCare.Questions.Add(questions["M1022ICD9M2"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis3") && questions["M1022PrimaryDiagnosis3"] != null)
                    {
                        planofCare.Questions.Add(questions["M1022PrimaryDiagnosis3"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M3") && questions["M1022ICD9M3"] != null)
                    {
                        planofCare.Questions.Add(questions["M1022ICD9M3"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis4") && questions["M1022PrimaryDiagnosis4"] != null)
                    {
                        planofCare.Questions.Add(questions["M1022PrimaryDiagnosis4"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M4") && questions["M1022ICD9M4"] != null)
                    {
                        planofCare.Questions.Add(questions["M1022ICD9M4"]);
                    }

                    if (questions.ContainsKey("M1022PrimaryDiagnosis5") && questions["M1022PrimaryDiagnosis5"] != null)
                    {
                        planofCare.Questions.Add(questions["M1022PrimaryDiagnosis5"]);
                    }

                    if (questions.ContainsKey("M1022ICD9M5") && questions["M1022ICD9M5"] != null)
                    {
                        planofCare.Questions.Add(questions["M1022ICD9M5"]);
                    }

                    // 14. DME and Supplies
                    if (questions.ContainsKey("485DME") && questions["485DME"] != null)
                    {
                        planofCare.Questions.Add(questions["485DME"]);
                    }

                    if (questions.ContainsKey("485DMEComments") && questions["485DMEComments"] != null)
                    {
                        planofCare.Questions.Add(questions["485DMEComments"]);
                    }

                    if (questions.ContainsKey("485Supplies") && questions["485Supplies"] != null)
                    {
                        planofCare.Questions.Add(questions["485Supplies"]);
                    }

                    if (questions.ContainsKey("485SuppliesComment") && questions["485SuppliesComment"] != null)
                    {
                        planofCare.Questions.Add(questions["485SuppliesComment"]);
                    }

                    // 15. Safety Measures
                    if (questions.ContainsKey("485SafetyMeasures") && questions["485SafetyMeasures"] != null)
                    {
                        planofCare.Questions.Add(questions["485SafetyMeasures"]);
                    }

                    if (questions.ContainsKey("485OtherSafetyMeasures") && questions["485OtherSafetyMeasures"] != null)
                    {
                        planofCare.Questions.Add(questions["485OtherSafetyMeasures"]);
                    }

                    // 16. Nutritional Requirements
                    if (questions.ContainsKey("485NutritionalReqs") && questions["485NutritionalReqs"] != null)
                    {
                        planofCare.Questions.Add(new Question { Type = QuestionType.PlanofCare, Name = "NutritionalReqs", Answer = PlanofCareXml.ExtractText("NutritionalRequirements", questions) });
                    }

                    // 17. Allergies
                    if (questions.ContainsKey("485Allergies") && questions["485Allergies"] != null)
                    {
                        planofCare.Questions.Add(questions["485Allergies"]);
                    }

                    if (questions.ContainsKey("485AllergiesDescription") && questions["485AllergiesDescription"] != null)
                    {
                        planofCare.Questions.Add(questions["485AllergiesDescription"]);
                    }

                    // 18.A. Functional Limitations
                    if (questions.ContainsKey("485FunctionLimitations") && questions["485FunctionLimitations"] != null)
                    {
                        planofCare.Questions.Add(questions["485FunctionLimitations"]);
                    }

                    if (questions.ContainsKey("485FunctionLimitationsOther") && questions["485FunctionLimitationsOther"] != null)
                    {
                        planofCare.Questions.Add(questions["485FunctionLimitationsOther"]);
                    }

                    // 18.B. Activities Permitted
                    if (questions.ContainsKey("485ActivitiesPermitted") && questions["485ActivitiesPermitted"] != null)
                    {
                        planofCare.Questions.Add(questions["485ActivitiesPermitted"]);
                    }

                    if (questions.ContainsKey("485ActivitiesPermittedOther") && questions["485ActivitiesPermittedOther"] != null)
                    {
                        planofCare.Questions.Add(questions["485ActivitiesPermittedOther"]);
                    }

                    // 19. Mental Status
                    if (questions.ContainsKey("485MentalStatus") && questions["485MentalStatus"] != null)
                    {
                        planofCare.Questions.Add(questions["485MentalStatus"]);
                    }

                    if (questions.ContainsKey("485MentalStatusOther") && questions["485MentalStatusOther"] != null)
                    {
                        planofCare.Questions.Add(questions["485MentalStatusOther"]);
                    }

                    // 20. Prognosis
                    if (questions.ContainsKey("485Prognosis") && questions["485Prognosis"] != null)
                    {
                        planofCare.Questions.Add(questions["485Prognosis"]);
                    }

                    // 21. Interventions
                    planofCare.Questions.Add(new Question { Name = "Interventions", Type = QuestionType.PlanofCare, Answer = PlanofCareXml.ExtractText("Intervention", questions) });

                    // 22. Goals
                    planofCare.Questions.Add(new Question { Name = "Goals", Type = QuestionType.PlanofCare, Answer = PlanofCareXml.ExtractText("Goal", questions) });

                    // 23. Signature


                    // 24. Signature Date

                    // 25. Physician name and address
                    if (patient != null && patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                    {
                        var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
                        if (primaryPhysician != null)
                        {
                            planofCare.PhysicianData = primaryPhysician.ToXml();
                        }
                    }
                    planofCare.UserId = scheduleEvent.UserId;
                    planofCare.Data = planofCare.Questions.ToXml();
                    if (oasisDataProvider.PlanofCareRepository.Add(planofCare))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private string Pad(string data, string padType, int length)
        {
            if (padType == "R")
            {
                return data.PadRight(length).ToUpper();
            }
            else if (padType == "N")
            {
                if (data != string.Empty && data != null)
                {
                    return data.PadLeft(length, '0').ToUpper();
                }
                else
                {
                    return data.PadLeft(length, ' ').ToUpper();
                }
            }
            else if (padType == "B")
            {

                return string.Format("{0}{1}", " ", data.PadRight(6, ' '));
            }
            else if (padType == "T")
            {
                return string.Format("{0}{1}", "  ", data.PadRight(5, ' '));
            }
            else
            {
                return data.PadLeft(length).ToUpper();
            }
        }

        private string IdentifyAssessment(SubmissionBodyFormat instruction, string assessmetType)
        {
            string type = "00";
            switch (assessmetType)
            {
                case "01":
                    type = instruction.RFA01.ToString();
                    break;
                case "03":
                    type = instruction.RFA03.ToString();
                    break;
                case "04":
                    type = instruction.RFA04.ToString();
                    break;
                case "05":
                    type = instruction.RFA05.ToString();
                    break;
                case "06":
                    type = instruction.RFA06.ToString();
                    break;
                case "07":
                    type = instruction.RFA07.ToString();
                    break;
                case "08":
                    type = instruction.RFA08.ToString();
                    break;
                case "09":
                    type = instruction.RFA09.ToString();
                    break;
                case "10":
                    type = instruction.RFA10.ToString();
                    break;
            }
            return type;
        }

        private IDictionary<string, Question> ProcessPatientDemography(Patient patient, ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode)
        {
            IDictionary<string, Question> questions = new Dictionary<string, Question>();
            var agency = agencyRepository.Get(Current.AgencyId);
            questions.Add("M0020PatientIdNumber", new Question { Name = "PatientIdNumber", Code = "M0020", Answer = patient.PatientIdNumber, Type = QuestionType.Moo });
            if (assessmentType == AssessmentType.StartOfCare)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "01", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
                questions.Add("HIIPSCODE", new Question { Name = "HIIPSCODE", Answer = "00000" });
                questions.Add("HIIPSVERSION", new Question { Name = "HIIPSVERSION", Answer = "00000" });
            }
            else if (assessmentType == AssessmentType.ResumptionOfCare)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "03", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
                questions.Add("HIIPSCODE", new Question { Name = "HIIPSCODE", Answer = "00000" });
                questions.Add("HIIPSVERSION", new Question { Name = "HIIPSVERSION", Answer = "00000" });
            }
            else if (assessmentType == AssessmentType.Recertification)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "04", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.AddDays(60).ToShortDateString(), Type = QuestionType.Generic });
                questions.Add("HIIPSCODE", new Question { Name = "HIIPSCODE", Answer = "00000" });
                questions.Add("HIIPSVERSION", new Question { Name = "HIIPSVERSION", Answer = "00000" });
            }
            else if (assessmentType == AssessmentType.DischargeFromAgency)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "09", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
            }
            else if (assessmentType == AssessmentType.DischargeFromAgencyDeath)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "08", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
            }
            else if (assessmentType == AssessmentType.FollowUp)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "05", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
                questions.Add("HIIPSCODE", new Question { Name = "HIIPSCODE", Answer = "00000" });
                questions.Add("HIIPSVERSION", new Question { Name = "HIIPSVERSION", Answer = "00000" });
            }
            else if (assessmentType == AssessmentType.TransferInPatientDischarged)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "07", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
            }
            else if (assessmentType == AssessmentType.TransferInPatientNotDischarged)
            {
                questions.Add("M0100AssessmentType", new Question { Name = "AssessmentType", Code = "M0100", Answer = "06", Type = QuestionType.Moo });
                questions.Add("GenericEpisodeStartDate", new Question { Name = "EpisodeStartDate", Answer = episode.StartDate.ToShortDateString(), Type = QuestionType.Generic });
            }
            if (agency != null)
            {
                questions.Add("M0010CertificationNumber", new Question { Name = "CertificationNumber", Code = "M0010", Answer = agency.MedicareProviderNumber, Type = QuestionType.Moo });
                questions.Add("M0018NationalProviderId", new Question { Name = "NationalProviderId", Code = "M0018", Answer = agency.NationalProviderNumber, Type = QuestionType.Moo });
            }
            questions.Add("M0030SocDate", new Question { Name = "SocDate", Code = "M0030", Answer = patient.StartOfCareDateFormatted, Type = QuestionType.Moo });
            questions.Add("M0040FirstName", new Question { Name = "FirstName", Code = "M0040", Answer = patient.FirstName, Type = QuestionType.Moo });
            string val = patient.MiddleInitial.IsNotNullOrEmpty() ? patient.MiddleInitial.ToString() : "";
            questions.Add("M0040MI", new Question { Name = "MI", Code = "M0040", Answer = val, Type = QuestionType.Moo });
            questions.Add("M0040LastName", new Question { Name = "LastName", Code = "M0040", Answer = patient.LastName, Type = QuestionType.Moo });
            questions.Add("M0050PatientState", new Question { Name = "PatientState", Code = "M0050", Answer = patient.AddressStateCode, Type = QuestionType.Moo });
            questions.Add("M0060PatientZipCode", new Question { Name = "PatientZipCode", Code = "M0060", Answer = patient.AddressZipCode, Type = QuestionType.Moo });
            questions.Add("M0063PatientMedicareNumber", new Question { Name = "PatientMedicareNumber", Code = "M0063", Answer = patient.MedicareNumber, Type = QuestionType.Moo });
            questions.Add("M0064PatientSSN", new Question { Name = "PatientSSN", Code = "M0064", Answer = patient.SSN, Type = QuestionType.Moo });
            questions.Add("M0065PatientMedicaidNumber", new Question { Name = "PatientMedicaidNumber", Code = "M0065", Answer = patient.MedicaidNumber, Type = QuestionType.Moo });
            questions.Add("M0066PatientDoB", new Question { Name = "PatientDoB", Code = "M0066", Answer = patient.DOBFormatted, Type = QuestionType.Moo });
            if (patient.Gender == "Male")
            {
                questions.Add("M0069Gender", new Question { Name = "Gender", Code = "M0069", Answer = "1", Type = QuestionType.Moo });
            }
            else if (patient.Gender == "Female")
            {
                questions.Add("M0069Gender", new Question { Name = "Gender", Code = "M0069", Answer = "2", Type = QuestionType.Moo });
            }

            if (patient.Ethnicities != null)
            {
                var races = patient.Ethnicities.Split(';');
                foreach (var race in races)
                {
                    if (race == "1")
                    {
                        questions.Add("M0140RaceAMorAN", new Question { Name = "RaceAMorAN", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (race == "2")
                    {
                        questions.Add("M0140RaceAsia", new Question { Name = "RaceAsia", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (race == "3")
                    {
                        questions.Add("M0140RaceBalck", new Question { Name = "RaceBalck", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (race == "4")
                    {
                        questions.Add("M0140RaceHispanicOrLatino", new Question { Name = "RaceHispanicOrLatino", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (race == "5")
                    {
                        questions.Add("M0140RaceNHOrPI", new Question { Name = "RaceNHOrPI", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (race == "6")
                    {
                        questions.Add("M0140RaceWhite", new Question { Name = "RaceWhite", Code = "M0140", Answer = "1", Type = QuestionType.Moo });
                    }
                }
            }

            if (patient.PaymentSource != null)
            {
                var paymentSources = patient.PaymentSource.Split(';');
                foreach (var paymentSource in paymentSources)
                {
                    if (paymentSource == "0")
                    {
                        questions.Add("M0150PaymentSourceNone", new Question { Name = "PaymentSourceNone", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "1")
                    {
                        questions.Add("M0150PaymentSourceMCREFFS", new Question { Name = "PaymentSourceMCREFFS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "2")
                    {
                        questions.Add("M0150PaymentSourceMCREHMO", new Question { Name = "PaymentSourceMCREHMO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "3")
                    {
                        questions.Add("M0150PaymentSourceMCAIDFFS", new Question { Name = "PaymentSourceMCAIDFFS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "4")
                    {
                        questions.Add("M0150PaymentSourceMACIDHMO", new Question { Name = "PaymentSourceMACIDHMO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "5")
                    {
                        questions.Add("M0150PaymentSourceWRKCOMP", new Question { Name = "PaymentSourceWRKCOMP", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "6")
                    {
                        questions.Add("M0150PaymentSourceTITLPRO", new Question { Name = "PaymentSourceTITLPRO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "7")
                    {
                        questions.Add("M0150PaymentSourceOTHGOVT", new Question { Name = "PaymentSourceOTHGOVT", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "8")
                    {
                        questions.Add("M0150PaymentSourcePRVINS", new Question { Name = "PaymentSourcePRVINS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "9")
                    {
                        questions.Add("M0150PaymentSourcePRVHMO", new Question { Name = "PaymentSourcePRVHMO", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "10")
                    {
                        questions.Add("M0150PaymentSourceSelfPay", new Question { Name = "PaymentSourceSelfPay", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "11")
                    {
                        questions.Add("M0150PaymentSourceUnknown", new Question { Name = "PaymentSourceUnknown", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                    }
                    if (paymentSource == "12")
                    {
                        questions.Add("M0150PaymentSourceOtherSRS", new Question { Name = "PaymentSourceOtherSRS", Code = "M0150", Answer = "1", Type = QuestionType.Moo });
                        questions.Add("M0150PaymentSourceOther", new Question { Name = "PaymentSourceOther", Code = "M0150", Answer = patient.OtherPaymentSource, Type = QuestionType.Moo });
                    }
                }
            }
            return questions;
        }

        #endregion

    }
}
