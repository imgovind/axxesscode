﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Domain;
    using Security;
    using Extensions;
    using Axxess.AgencyManagement.Enums;

    public class ReferralService : IReferralService
    {
        private readonly IReferralRepository referralRepository;

        public ReferralService(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
        }

        public List<Referral> GetPending(Guid agencyId)
        {
            var referrals = referralRepository.GetAll(agencyId, ReferralStatus.Pending).ToList();

            referrals.ForEach(r =>
            {
                r.ReferralSourceName = r.AdmissionSource.ToSourceName();
            });

            return referrals;
        }
    }
}
