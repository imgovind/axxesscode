﻿namespace Axxess.Api.Services
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Configuration;
    using System.Collections.Generic;

    using OASIS_Validation;

    using Axxess.Api.Contracts;

    internal static class OasisValidator
    {
        private static string ICD9_PATH = ConfigurationSettings.AppSettings["FILE_PATH"].ToString();
        private static string DICT_PATH = ConfigurationSettings.AppSettings["FILE_PATH"].ToString();

        public static List<ValidationError> CheckDataString(string oasisDataString)
        {
            Windows.EventLog.WriteEntry(string.Format("Validator.CheckDataString Method with oasisDataString input: {0}", oasisDataString), System.Diagnostics.EventLogEntryType.Information);
            var errors = new List<ValidationError>();

            try
            {
                int errorCount = 0;
                string[] dupList = new string[] { };
                string[] errorTypeList = new string[] { };
                string[] descriptionList = new string[] { };

                clsEasyValidate easyValidator = new clsEasyValidate();
                easyValidator.ErrorCheck_Oasis(oasisDataString, ICD9_PATH, DICT_PATH, true, ref errorCount, ref descriptionList, ref dupList, ref errorTypeList);
                Windows.EventLog.WriteEntry(string.Format("ICD9_PATH: {0} DICT_PATH: {1}", ICD9_PATH, DICT_PATH), System.Diagnostics.EventLogEntryType.Information);

                Windows.EventLog.WriteEntry(string.Format("Error Count: {0}", errorCount), System.Diagnostics.EventLogEntryType.Information);
                
                if (descriptionList.Length == dupList.Length && errorTypeList.Length == dupList.Length)
                {
                    for (int i = 0; i < descriptionList.Length; i++)
                    {
                        var error = new ValidationError { ErrorType = errorTypeList[i], Description = descriptionList[i], ErrorDup = dupList[i] };
                        errors.Add(error);
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw ex;
            }
            
            return errors;
        }
    }
}
