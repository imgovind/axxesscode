﻿namespace Axxess.Api.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    public class GrouperService : BaseService, IGrouperService
    {
        #region IGrouperService Members

        public Hipps GetHippsCode(string oasisDataString)
        {
            var hipps = new Hipps();
            Windows.EventLog.WriteEntry(string.Format("GrouperService.GetHippsCode Method with oasisDataString input: {0}", oasisDataString), System.Diagnostics.EventLogEntryType.Information);
            try
            {
                string hippsCode = "00000";
                string claimKey = "000000000000000000";
                string version = "00000";
                string invflag = "0";

                Grouper.GetHippsCode(ref hippsCode, ref oasisDataString, ref claimKey, ref version, ref invflag);

                hipps.Code = hippsCode;
                hipps.ClaimMatchingKey = claimKey;
                hipps.Version = version;
                hipps.InvFlag = invflag;

                Windows.EventLog.WriteEntry(hipps.ToString());
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.Message);
            }

            return hipps;
        }

        #endregion

    }
}