﻿namespace Axxess.OasisC.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Enums;
    using Domain;

    using SubSonic.Repository;
  
    public static class Databases
    {
        public static void InsertAny(this SimpleRepository repository, IAssessment assessment)
        {
            switch (assessment.Type)
            {
                case AssessmentType.StartOfCare:
                    repository.Add<StartOfCareAssessment>((StartOfCareAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgency:
                    repository.Add<DischargeFromAgencyAssessment>((DischargeFromAgencyAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgencyDeath:
                    repository.Add<DeathAtHomeAssessment>((DeathAtHomeAssessment)assessment);
                    break;
                case AssessmentType.FollowUp:
                    repository.Add<FollowUpAssessment>((FollowUpAssessment)assessment);
                    break;
                case AssessmentType.Recertification:
                    repository.Add<RecertificationAssessment>((RecertificationAssessment)assessment);
                    break;
                case AssessmentType.ResumptionOfCare:
                    repository.Add<ResumptionofCareAssessment>((ResumptionofCareAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientDischarged:
                    repository.Add<TransferDischargeAssessment>((TransferDischargeAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientNotDischarged:
                    repository.Add<TransferNotDischargedAssessment>((TransferNotDischargedAssessment)assessment);
                    break;
                default:
                    break;
            }
        }

        public static void UpdateAny(this SimpleRepository repository, IAssessment assessment)
        {
            switch (assessment.Type)
            {
                case AssessmentType.StartOfCare:
                    repository.Update<StartOfCareAssessment>((StartOfCareAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgency:
                    repository.Update<DischargeFromAgencyAssessment>((DischargeFromAgencyAssessment)assessment);
                    break;
                case AssessmentType.DischargeFromAgencyDeath:
                    repository.Update<DeathAtHomeAssessment>((DeathAtHomeAssessment)assessment);
                    break;
                case AssessmentType.FollowUp:
                    repository.Update<FollowUpAssessment>((FollowUpAssessment)assessment);
                    break;
                case AssessmentType.Recertification:
                    repository.Update<RecertificationAssessment>((RecertificationAssessment)assessment);
                    break;
                case AssessmentType.ResumptionOfCare:
                    repository.Update<ResumptionofCareAssessment>((ResumptionofCareAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientDischarged:
                    repository.Update<TransferDischargeAssessment>((TransferDischargeAssessment)assessment);
                    break;
                case AssessmentType.TransferInPatientNotDischarged:
                    repository.Update<TransferNotDischargedAssessment>((TransferNotDischargedAssessment)assessment);
                    break;
                default:
                    break;
            }
        }

        public static Assessment FindAny(this SimpleRepository repository, string assessmentType, Guid assessmentId)
        {
            Assessment assessment = null;
            switch (assessmentType)
            {
                case "StartOfCare":
                    assessment = repository.Single<StartOfCareAssessment>(assessmentId);
                    break;
                case "ResumptionOfCare":
                    assessment = repository.Single<ResumptionofCareAssessment>(assessmentId);
                    break;
                case "FollowUp":
                    assessment = repository.Single<FollowUpAssessment>(assessmentId);
                    break;
                case "Recertification":
                    assessment = repository.Single<RecertificationAssessment>(assessmentId);
                    break;
                case "TransferInPatientNotDischarged":
                    assessment = repository.Single<TransferNotDischargedAssessment>(assessmentId);
                    break;
                case "TransferInPatientDischarged":
                    assessment = repository.Single<TransferDischargeAssessment>(assessmentId);
                    break;
                case "DischargeFromAgencyDeath":
                    assessment = repository.Single<DeathAtHomeAssessment>(assessmentId);
                    break;
                case "DischargeFromAgency":
                    assessment = repository.Single<DischargeFromAgencyAssessment>(assessmentId);
                    break;
                default:
                    break;
            }
            return assessment;
        }
        public static Assessment FindAny(this SimpleRepository repository, Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType)
        {
            Assessment assessment = null;
            switch (assessmentType)
            {
                case "StartOfCare":
                    assessment = repository.Single<StartOfCareAssessment>(a => a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "ResumptionOfCare":
                    assessment = repository.Single<ResumptionofCareAssessment>(a => a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "FollowUp":
                    assessment = repository.Single<FollowUpAssessment>(a => a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "Recertification":
                    assessment = repository.Single<RecertificationAssessment>(a => a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "TransferInPatientNotDischarged":
                    assessment = repository.Single<TransferNotDischargedAssessment>(a => a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "TransferInPatientDischarged":
                    assessment = repository.Single<TransferDischargeAssessment>(a => a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "DischargeFromAgencyDeath":
                    assessment = repository.Single<DeathAtHomeAssessment>(a => a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                case "DischargeFromAgency":
                    assessment = repository.Single<DischargeFromAgencyAssessment>(a => a.Id == assessmentId && a.PatientId == PatientId && a.EpisodeId == EpisodeId);
                    break;
                default:
                    break;
            }
            return assessment;
        }

        public static IList <Assessment> FindAnyByStatus(this SimpleRepository repository ,string assessmentType,int status)
        {
           IList<Assessment> assessments= new List<Assessment> ();
            switch (assessmentType)
            {
                case "StartOfCare":
                    assessments =repository.Find<StartOfCareAssessment>(a => a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "ResumptionOfCare":
                    assessments = repository.Find<ResumptionofCareAssessment>(a => a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "FollowUp":
                    assessments = repository.Find<FollowUpAssessment>(a => a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "Recertification":
                    assessments = repository.Find<RecertificationAssessment>(a => a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "TransferInPatientNotDischarged":
                    assessments = repository.Find<TransferNotDischargedAssessment>(a => a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "TransferInPatientDischarged":
                    assessments = repository.Find<TransferDischargeAssessment>(a => a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "DischargeFromAgencyDeath":
                    assessments = repository.Find<DeathAtHomeAssessment>(a => a.Status == status).Cast<Assessment>().ToList();
                    break;
                case "DischargeFromAgency":
                    assessments = repository.Find<DischargeFromAgencyAssessment>(a => a.Status == status).Cast<Assessment>().ToList();
                    break;
                default:
                    break;
            }
            return assessments;
        }
    }
}
