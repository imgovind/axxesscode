﻿namespace Axxess.OasisC.Enums
{
    using System.ComponentModel;

    public enum AssessmentType
    {
        [Description("Oasis-C Start Of Care")]
        StartOfCare,
        [Description("Oasis-C Resumption Of Care")]
        ResumptionOfCare,
        [Description("Oasis-C Recertification")]
        Recertification,
        [Description("Oasis-C Other Follow-up")]
        FollowUp,
        [Description("Oasis-C Transfer to inpatient facility - Not Discharged")]
        TransferInPatientNotDischarged,
        [Description("Oasis-C Transfer to inpatient facility - Discharged")]
        TransferInPatientDischarged,
        [Description("Oasis-C Death at Home")]
        DischargeFromAgencyDeath,
        [Description("Oasis-C Discharge From Agency")]
        DischargeFromAgency,
        [Description("Oasis C 485 (Plan of Care)")]
        PlanOfCare485
    }
}
