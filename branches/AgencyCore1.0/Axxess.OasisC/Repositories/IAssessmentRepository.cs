﻿
namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Domain;

    public interface IAssessmentRepository
    {
        Guid Add(Assessment oasisAssessment, string assessmentType);
        bool Update(Assessment oasisAssessment, string assessmentType);
        bool Update(Assessment oasisAssessment, string assessmentType, string hippsCode, string claimKey, string hippsVersion, string stringFormat);
        bool Update(Assessment oasisAssessment, string assessmentType, string stringFormat);
        Assessment Get(Guid assessmentId, string assessmentType);
        Assessment Get(Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType);
        List<Assessment> GetAllByStatus(Guid agencyId, int status);
    }
}
