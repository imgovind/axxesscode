﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Extensions;

    using SubSonic.Repository;
    using Axxess.OasisC.Enums;

    public class AssessmentRepository : IAssessmentRepository
    {
      #region Constructor

        private readonly SimpleRepository database;

        public AssessmentRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssessmentRepository Members

        public Guid Add(Assessment newAssessment, string assessmentType)
        {
            Check.Argument.IsNotNull(newAssessment, "newAssessment");
            var assessment = AssessmentFactory.Create(assessmentType);
            try
            {
                assessment.Id = newAssessment.Id;
                assessment.Status = newAssessment.Status;
                assessment.PatientId = newAssessment.PatientId;
                assessment.EpisodeId = newAssessment.EpisodeId;
                assessment.OasisData = newAssessment.Questions.ToXml();
                assessment.MedicationProfile = newAssessment.MedicationProfile;
                assessment.Supply = newAssessment.Supply;
                assessment.Created = DateTime.Now;
                assessment.Modified = DateTime.Now;
                database.InsertAny(assessment);
            }
            catch (Exception ex )
            {
                // TODO: Log Exception
                return Guid.Empty;
            }
            return assessment.Id;
        }

        public bool Update(Assessment oasisAssessment, string assessmentType)
        {
            bool result = false;
            if (oasisAssessment != null)
            {
                IAssessment assessment = database.FindAny(assessmentType, oasisAssessment.Id);
                assessment.Status = oasisAssessment.Status;
                assessment.PatientId = oasisAssessment.PatientId;
                assessment.EpisodeId = oasisAssessment.EpisodeId;
                assessment.OasisData = oasisAssessment.Questions.ToXml();
                assessment.MedicationProfile = oasisAssessment.MedicationProfile;
                assessment.VersionNumber = oasisAssessment.VersionNumber;
                assessment.Supply = oasisAssessment.Supply;
                assessment.Modified = DateTime.Now;
                database.UpdateAny(assessment);
                result = true;
            }
            return result;
        }

        public bool Update(Assessment oasisAssessment, string assessmentType, string hippsCode, string claimKey, string hippsVersion, string stringFormat)
        {
            Check.Argument.IsNotNull(oasisAssessment, "oasisAssessment");
            bool result = false;
            try
            {
                IAssessment assessment =  database.FindAny(assessmentType, oasisAssessment.Id);
                assessment.Status = oasisAssessment.Status;
                assessment.PatientId = oasisAssessment.PatientId;
                assessment.EpisodeId = oasisAssessment.EpisodeId;
                assessment.ClaimKey = claimKey;
                assessment.HippsCode = hippsCode;
                assessment.HippsVersion = hippsVersion;
                assessment.OasisData = oasisAssessment.Questions.ToXml();
                assessment.SubmissionFormat = stringFormat;
                assessment.VersionNumber = oasisAssessment.VersionNumber;
                assessment.Supply = oasisAssessment.Supply;
                assessment.Modified = DateTime.Now;
                database.UpdateAny(assessment);
                result = true;
            }
            catch (Exception ex)
            {
                // TODO: Log Exception
            }
            return result;
        }

        public bool Update(Assessment oasisAssessment, string assessmentType, string stringFormat)
        {
            Check.Argument.IsNotNull(oasisAssessment, "oasisAssessment");
            bool result = false;
            try
            {
                IAssessment assessment = database.FindAny(assessmentType, oasisAssessment.Id);
                assessment.Status = oasisAssessment.Status;
                assessment.PatientId = oasisAssessment.PatientId;               
                assessment.OasisData = oasisAssessment.Questions.ToXml();
                assessment.SubmissionFormat = stringFormat;
                assessment.VersionNumber = oasisAssessment.VersionNumber;
                assessment.Modified = DateTime.Now;
                database.UpdateAny(assessment);
                result = true;
            }
            catch (Exception ex)
            {
                // TODO: Log Exception
            }
            return result;
        }

        public Assessment Get(Guid assessmentId, string assessmentType)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty())
            {
                assessment = AssessmentFactory.Create(assessmentType);
                IAssessment data = database.FindAny(assessmentType, assessmentId);
                if (data != null)
                {
                    assessment.Id = data.Id;
                    assessment.EpisodeId = data.EpisodeId;
                    assessment.Status = data.Status;
                    assessment.PatientId = data.PatientId;
                    assessment.Questions = data.OasisData.ToObject<List<Question>>();
                    assessment.OasisData = data.OasisData;
                    assessment.MedicationProfile = data.MedicationProfile;
                    assessment.ClaimKey = data.ClaimKey;
                    assessment.HippsCode = data.HippsCode;
                    assessment.HippsVersion = data.HippsVersion;
                    assessment.SubmissionFormat = data.SubmissionFormat;
                    assessment.Supply = data.Supply;
                    assessment.VersionNumber = data.VersionNumber;
                }
            }
            return assessment;
        }

        public Assessment Get(Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType)
        {
            Check.Argument.IsNotEmpty(assessmentId, "assessmentId");
            Assessment assessment = AssessmentFactory.Create(assessmentType);
            try
            {
                IAssessment data = database.FindAny(assessmentId, PatientId, EpisodeId, assessmentType);
                if (data != null)
                {
                    assessment.Id = data.Id;
                    assessment.EpisodeId = data.EpisodeId;
                    assessment.Status = data.Status;
                    assessment.PatientId = data.PatientId;
                    assessment.Questions = data.OasisData.ToObject<List<Question>>();
                    assessment.OasisData = data.OasisData;
                    assessment.MedicationProfile = data.MedicationProfile;
                    assessment.ClaimKey = data.ClaimKey;
                    assessment.HippsCode = data.HippsCode;
                    assessment.HippsVersion = data.HippsVersion;
                    assessment.SubmissionFormat = data.SubmissionFormat;
                    assessment.Supply = data.Supply;
                    assessment.VersionNumber = data.VersionNumber;
                }
            }
            catch (Exception ex)
            {
                // TODO: Log Exception
            }
            return assessment;
        }

        public List<Assessment> GetAllByStatus(Guid agencyId, int status)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            var assessments = new List<Assessment>();
            try
            {
                foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
                {
                    var data = database.FindAnyByStatus(assessmentType.ToString(), status);
                    assessments.AddRange(data);
                }
            }
            catch (Exception ex)
            {
                // TODO: Log Exception
            }
            return assessments;
        }
      
        #endregion
    }
}
