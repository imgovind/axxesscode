﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Domain;

    public interface IPlanofCareRepository
    {
        bool Add(PlanofCare planofCare);
        bool Update(PlanofCare planofCare);
        PlanofCare Get(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        List<PlanofCare> GetPlanofCareByStatus(Guid agencyId, int status);

        PlanofCare Get(Guid agencyId, Guid eventId);
    }
}
