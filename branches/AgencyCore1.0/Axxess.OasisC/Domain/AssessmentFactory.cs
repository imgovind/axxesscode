﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using Axxess.Core.Extension;
    
    using Enums;
    using Extensions;

    [Serializable]
    public class AssessmentFactory
    {
        #region Assessment Static Method Members

        public static Assessment Create(string assessmentType)
        {
            Assessment assessment = null;
            switch (assessmentType)
            {
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                    assessment = new FollowUpAssessment();
                    break;
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                    assessment = new StartOfCareAssessment();
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathPT":
                case "OASISCDeathOT":
                    assessment = new DeathAtHomeAssessment();
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                    assessment = new RecertificationAssessment();
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                    assessment = new ResumptionofCareAssessment();
                    break;
                case "TransferInPatientDischarged":
                    assessment = new TransferDischargeAssessment();
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargePT":
                case "OASISCDischargeOT":
                    assessment = new DischargeFromAgencyAssessment();
                    break;
                case "TransferInPatientNotDischarged":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                    assessment = new TransferNotDischargedAssessment();
                    break;
                default:
                    break;
            }
            return assessment;
        }

        #endregion 
       
    }
}
