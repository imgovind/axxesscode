﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Enums;

    using SubSonic.SqlGeneration.Schema;

    [Serializable]
    public class Assessment : IAssessment
    {
        #region Constructor

        public Assessment()
        {
            this.Questions = new List<Question>();
        }

        #endregion

        #region IAssessment Members

        public Guid Id { get; set; }
        public int Status { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string ClaimKey { get; set; }
        public string OasisData { get; set; }
        public string HippsCode { get; set; }
        public string HippsVersion { get; set; }
        public string SubmissionFormat { get; set; }
        public DateTime AssessmentDate { get; set; }
        public int VersionNumber { get; set; }
        public string Supply { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public string MedicationProfile { get; set; }
        [SubSonicIgnore]
        public string AgencyData { get; set; }
        [SubSonicIgnore]
        public string PatientName { get; set; }
        [SubSonicIgnore]
        public string Insurance { get; set; }
        [SubSonicIgnore]
        public bool IsNew { get; set; }
        [SubSonicIgnore]
        public AssessmentType Type { get; set; }
        [SubSonicIgnore]
        public string TypeName { get { return (this.Type).ToString(); } }
        [SubSonicIgnore]
        public string TypeDescription { get { return (this.Type).GetDescription(); } }
        [SubSonicIgnore]
        public string ValidationError { get; set; }
        [SubSonicIgnore]
        public List<Question> Questions { get; set; }
        [SubSonicIgnore]
        public string Identifier { get { return string.Format("{0}|{1}", this.Id, this.Type); } }


        #endregion
    }
}
