﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/HHPPS/2010/05/")]
    public class Hipps
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string ClaimMatchingKey { get; set; }
        [DataMember]
        public string Version { get; set; }
        [DataMember]
        public string InvFlag { get; set; }

        public override string ToString()
        {
            return string.Format("Code: {0} Claim Key: {1} Version: {2} Inv Flag: {3}", this.Code, this.ClaimMatchingKey, this.Version, this.InvFlag);
        }
    }
}
