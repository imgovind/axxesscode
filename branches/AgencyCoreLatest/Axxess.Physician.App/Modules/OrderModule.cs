﻿namespace Axxess.Physician.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class OrderModule : Module
    {
        public override string Name
        {
            get { return "Order"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "NewCPO",
                "Order/CPO/New",
                new { controller = this.Name, action = "NewCPO", id = UrlParameter.Optional });

            routes.MapRoute(
                "CPOList",
                "Order/CPO/List",
                new { controller = this.Name, action = "CpoListHeader", id = UrlParameter.Optional });

            routes.MapRoute(
               "CPOListView",
               "Order/CPO/ListView",
               new { controller = this.Name, action = "CpoListView", id = UrlParameter.Optional });

            routes.MapRoute(
                "OrderList",
                "Order/List",
                new { controller = this.Name, action = "ListHeader", id = UrlParameter.Optional });

            routes.MapRoute(
                "CPOContent",
                "Order/CPOContent",
                new { controller = this.Name, action = "CPOContent" });

            routes.MapRoute(
                "PhysicianOrderPrint",
                "Order/Physician/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "PhysicianOrderPrint", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "FaceToFaceOrderPrint",
                "Order/FaceToFace/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "FaceToFaceEncounterPrint", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "PlanofCareOrderPrint",
                "Order/PlanofCare/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "PlanofCareOrderPrint", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });
            
            routes.MapRoute(
                "PTPOCPrint",
                "Order/PTPOCPrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "PTPOC", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "OTPOCPrint",
                "Order/OTPOCPrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "OTPOC", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "STPOCPrint",
                "Order/STPOCPrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "STPOC", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "PTEvaluationPrint",
                "Order/PTEvaluationPrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "PTEval", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "OTEvaluationPrint",
                "Order/OTEvaluationPrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "OTEval", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "PTReassessmentPrint",
                "Order/PTReassessmentPrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "PTReassessmentPrint", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "OTReassessmentPrint",
                "Order/OTReassessmentPrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "OTReassessmentPrint", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });
            
            routes.MapRoute(
                "PTDischargePrint",
                "Order/PTDischargePrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "PTDischarge", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "STDischargePrint",
                "Order/STDischargePrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "STDischarge", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });


            routes.MapRoute(
                "SixtyDaySummaryPrint",
                "Order/SixtyDaySummaryPrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "SixtyDaySummary", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
                "PsychAssessmentPrint",
                "Order/PsychAssessmentPrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "PsychAssessment", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });

            routes.MapRoute(
               "MSWEval",
               "Order/MSWEval/{agencyId}/{episodeId}/{patientId}/{orderId}",
               new { controller = this.Name, action = "MSWEval", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });


            routes.MapRoute(
                "STEvaluationPrint",
                "Order/STEvaluationPrint/{agencyId}/{episodeId}/{patientId}/{orderId}",
                new { controller = this.Name, action = "STEval", agencyId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid(), orderId = new IsGuid() });
            
        }
    }
}
