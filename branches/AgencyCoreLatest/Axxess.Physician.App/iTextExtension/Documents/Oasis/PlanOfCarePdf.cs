﻿namespace Axxess.Physician.App.iTextExtension {
    using System;
    using System.IO;
    using System.Linq;
    using iTextSharp.text;
    using Axxess.OasisC.Domain;
    class PlanOfCarePdf : PlanOfCare485Pdf {
        private PlanOfCare487Pdf cms487;
        public PlanOfCarePdf(PlanofCare planOfCare, Guid agencyId) : base(planOfCare, agencyId) {
            IElement[] content = this.get487content();
            if (content != null && content.Count() > 0) this.cms487 = new PlanOfCare487Pdf(content, this.get487fieldmap());
        }
        public MemoryStream GetPlanOfCareStream() {
            return this.GetStream(this.cms487);
        }
    }
}