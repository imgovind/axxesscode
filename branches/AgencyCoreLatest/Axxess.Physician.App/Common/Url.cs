﻿namespace Axxess.Physician.App
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    using Axxess.AgencyManagement.Extensions;

    public static class Url
    {
        public static string Print(Guid orderId, Guid patientId, Guid episodeId, Guid agencyId, OrderType type, bool useIcon)
        {
            string printUrl = string.Empty;
            string linkText = type.GetDescription();
            if (useIcon) linkText = "<span class=\"img icon print\"></span>";

            switch (type)
            {
                case OrderType.PhysicianOrder:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/Physician/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PhysicianPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                        "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                        "Buttons: [ " +
                            "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                        "] })\">" + linkText + "</a>";
                    break;
                case OrderType.FaceToFaceEncounter:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/FaceToFace/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/FaceToFacePdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                        "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                        "Buttons: [ " +
                            "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                        "] })\">" + linkText + "</a>";
                    break;
                case OrderType.HCFA485:
                case OrderType.HCFA485StandAlone:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/PlanofCare/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PlanofCarePdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                        "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                        "Buttons: [ " +
                            "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                        "] })\">" + linkText + "</a>";
                    break;
                case OrderType.PtEvaluation:
                case OrderType.PtReEvaluation:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/PTEvaluationPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PTEvalPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                        "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                        "Buttons: [ " +
                            "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                        "] })\">" + linkText + "</a>";
                    break;
                case OrderType.PTPlanOfCare:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/PTPOCPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PTPOCPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                        "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                        "Buttons: [ " +
                            "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                        "] })\">" + linkText + "</a>";
                    break;
                case OrderType.OTPlanOfCare:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/OTPOCPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/OTPOCPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                        "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                        "Buttons: [ " +
                            "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                        "] })\">" + linkText + "</a>";
                    break;
                case OrderType.STPlanOfCare:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/STPOCPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/STPOCPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                        "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                        "Buttons: [ " +
                            "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                        "] })\">" + linkText + "</a>";
                    break;
                case OrderType.PTReassessment:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/PTReassessmentPrint/" +agencyId + "/" + episodeId + "/" +patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PTReassessmentPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "','episodeId':'" + episodeId + "','patientId':'" + patientId + "','orderId':'" + orderId + "'}," +
                        "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                        "Buttons: [ " +
                            "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                        "] })\">" + linkText + "</a>";
                    break;
                case OrderType.OTDischarge:
                case OrderType.OtEvaluation:
                case OrderType.OtReEvaluation:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/OTEvaluationPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/OTEvalPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                       "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                       "Buttons: [ " +
                           "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                       "] })\">" + linkText + "</a>";
                    break;
                case OrderType.OTReassessment:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/OTReassessmentPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/OTReassessmentPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                       "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                       "Buttons: [ " +
                           "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                       "] })\">" + linkText + "</a>";
                    break;
                case OrderType.StEvaluation:
                case OrderType.StReEvaluation:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/STEvaluationPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/STEvalPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                       "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                       "Buttons: [ " +
                           "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                       "] })\">" + linkText + "</a>";
                    break;
                case OrderType.PTDischarge:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/PTDischargePrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/PTDischargePdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                       "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                       "Buttons: [ " +
                           "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                       "] })\">" + linkText + "</a>";
                    break;
                case OrderType.STDischarge:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/STDischargePrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/STDischargePdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                       "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                       "Buttons: [ " +
                           "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                       "] })\">" + linkText + "</a>";
                    break;
                case OrderType.SixtyDaySummary:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/SixtyDaySummaryPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/SixtyDaySummaryPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                       "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                       "Buttons: [ " +
                           "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                       "] })\">" + linkText + "</a>";
                    break;
                case OrderType.MSWDischarge:
                case OrderType.MSWEvaluation:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/MSWEval/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/MSWEvalPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                       "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                       "Buttons: [ " +
                           "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                       "] })\">" + linkText + "</a>";
                    break;
                case OrderType.SNPsychAssessment:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/PsychAssessmentPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/PsychAssessmentPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' }," +
                       "ReturnClick: function() { Order.Update('Return','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') }," +
                       "Buttons: [ " +
                           "{ Text: 'Approve', Click: function() { Order.Update('Approve','" + agencyId + "','" + episodeId + "','" + patientId + "','" + orderId + "','" + (int)type + "') } }" +
                       "] })\">" + linkText + "</a>";
                    break;
            }
            return printUrl;
        }

        public static string View(Guid orderId, Guid patientId, Guid episodeId, Guid agencyId, OrderType type, bool useIcon)
        {
            string printUrl = string.Empty;
            string linkText = type.GetDescription();
            if (useIcon) linkText = "<span class=\"img icon print\"></span>";

            switch (type)
            {
                case OrderType.PhysicianOrder:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/Physician/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PhysicianPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.FaceToFaceEncounter:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/FaceToFace/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/FaceToFacePdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.HCFA485:
                case OrderType.HCFA485StandAlone:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/PlanofCare/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PlanofCarePdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.PTPlanOfCare:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/PTPOCPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PTPOCPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.OTPlanOfCare:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/OTPOCPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/OTPOCPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.STPlanOfCare:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/STPOCPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/STPOCPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.PtEvaluation:
                case OrderType.PtReEvaluation:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/PTEvaluationPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PTEvalPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.PTReassessment:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                        "Url: '/Order/PTReassessmentPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                        "PdfUrl: 'Order/PTReassessmentPdf'," +
                        "PdfData: { 'agencyId': '" + agencyId + "','episodeId':'" + episodeId + "','patientId':'" + patientId + "','orderId':'" + orderId + "'} })\">" + linkText + "</a>";
                    break;
                case OrderType.OTDischarge:
                case OrderType.OtEvaluation:
                case OrderType.OtReEvaluation:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/OTEvaluationPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/OTEvalPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.OTReassessment:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/OTReassessmentPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/OTReassessmentPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.StEvaluation:
                case OrderType.StReEvaluation:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/STEvaluationPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/STEvalPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.PTDischarge:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/PTDischargePrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/PTDischargePdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.STDischarge:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/STDischargePrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/STDischargePdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.SixtyDaySummary:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/SixtyDaySummaryPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/SixtyDaySummaryPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.MSWDischarge:
                case OrderType.MSWEvaluation:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/MSWEval/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/MSWEvalPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
                case OrderType.SNPsychAssessment:
                    printUrl = "<a href=\"javascript:void(0)\" onclick=\"Acore.OpenPrintView({ " +
                       "Url: '/Order/PsychAssessmentPrint/" + agencyId + "/" + episodeId + "/" + patientId + "/" + orderId + "'," +
                       "PdfUrl: 'Order/PsychAssessmentPdf'," +
                       "PdfData: { 'agencyId': '" + agencyId + "', 'episodeId': '" + episodeId + "', 'patientId': '" + patientId + "', 'orderId': '" + orderId + "' } })\">" + linkText + "</a>";
                    break;
            }
            return printUrl;
        }
    }
}
