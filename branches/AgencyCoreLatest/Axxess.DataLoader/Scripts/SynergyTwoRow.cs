﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Data;
    using System.Text;

    using Excel;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class SynergyTwoRow
    {
        private static string input = Path.Combine(App.Root, "Files\\patient.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\patient_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                var i = 1;
                                int numberOfRows = 5;
                                int rowCounter = 1;
                                Patient patientData = null;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (dataRow.GetValue(0).Trim() == "Stop")
                                        break;
                                    if (!dataRow.IsEmpty())
                                    {
                                        
                                        if (dataRow.GetValue(0).ToLower().Contains("patient"))
                                            continue;
                                        else if (dataRow.GetValue(0).ToLower().Contains("soc"))
                                            continue;
                                       
                                        if (rowCounter % numberOfRows == 1)
                                        {
                                            patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId.ToGuid();
                                            patientData.AgencyLocationId = locationId.ToGuid();
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;
                                            patientData.ServiceLocation = "Q5001";
                                            patientData.AddressLine1 = string.Empty;
                                            patientData.AddressLine2 = string.Empty;
                                            patientData.AddressCity = string.Empty;
                                            patientData.AddressStateCode = string.Empty;
                                            patientData.AddressZipCode = string.Empty;

                                            var nameRow = dataRow.GetValue(1).Trim();
                                            var nameArray = nameRow.Trim().Split(' ');
                                            if (nameArray.Length >= 2)
                                            {
                                                patientData.FirstName = nameArray[0];
                                                patientData.LastName = nameArray[1];
                                            }

                                            var addressRow = dataRow.GetValue(12);
                                            var addressArray = addressRow.Split(new string[] { "," }, StringSplitOptions.None);
                                            if (addressArray.Length >= 2)
                                            {
                                                var statecode = addressArray[1].Trim();
                                                patientData.AddressStateCode = (statecode.Split(' '))[0].Trim();
                                                patientData.AddressZipCode = (statecode.Split(' '))[2].Trim();
                                                int end = addressArray[0].LastIndexOf(' ') + 1;
                                                patientData.AddressCity = addressArray[0].Substring(end);
                                                patientData.AddressLine1 = addressArray[0].Substring(0, end - 1).Trim();
                                            }
                                            //patientData.PatientIdNumber = dataRow.GetValue(4);
                                            patientData.PhoneHome = dataRow.GetValue(23).ToPhoneDB();
                                            if (dataRow.GetValue(26).IsNotNullOrEmpty())
                                            {
                                                patientData.MedicareNumber = dataRow.GetValue(26);
                                            }
                                            //patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(7)));
                                           // patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(10)));
                                            //if (dataRow.GetValue(14).ToLower().Equals("admitted"))
                                            //{
                                            //    patientData.Status = 1;
                                            //}
                                            //else if (dataRow.GetValue(14).ToLower().Equals("discharged"))
                                            //{
                                            //    patientData.Status = 2;
                                            //}
                                            //patientData.Comments += string.Format("Adm:{0}.", dataRow.GetValue(17));
                                            //patientData.Comments += string.Format("DX Code:{0}.", dataRow.GetValue(20));
                                            //patientData.Comments += string.Format("Physician:{0}.", dataRow.GetValue(25));
                                            //patientData.Comments += string.Format("Physician Phone:{0}.", dataRow.GetValue(35));
                                            //if (dataRow.GetValue(40).IsNotNullOrEmpty())
                                            //{
                                            //    patientData.Comments += string.Format("UPIN:{0}.", dataRow.GetValue(40));

                                            //}
                                            
                                        }
                                        else if (rowCounter % numberOfRows == 3)
                                        {
                                            patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(3)));
                                            //patientData.StartofCareDate = dataRow.GetValue(3).ToDateTime();
                                           
                                            if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Epidode Start Date:{0}. ", DateTime.FromOADate(double.Parse(dataRow.GetValue(6))).ToString("MM/dd/yyyy"));

                                            }
                                            if (dataRow.GetValue(10).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Dischg Code:{0}. ", dataRow.GetValue(10));

                                            }

                                            patientData.PatientIdNumber = dataRow.GetValue(13);

                                            if (dataRow.GetValue(17).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Primary Diagnosis:{0}. ", dataRow.GetValue(17));

                                            }
                                            if (dataRow.GetValue(21).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Physician:{0}. ", dataRow.GetValue(21));

                                            }

                                            if (dataRow.GetValue(28).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Case Manager:{0}. ", dataRow.GetValue(28));

                                            }

                                           

                                            patientData.Gender = "";

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };
                                            //var a = 5;
                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                            }
                                            textWriter.Write(textWriter.NewLine);
                                            i++;
                                        }
                                        
                                    }
                                    rowCounter++;
                                    
                                }
                                Console.WriteLine(rowCounter);
                            }
                        }
                    }
                }
            }
        }
    }
}
