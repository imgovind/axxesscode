﻿namespace Axxess.DataLoader.Domain
{
        using System;
    using System.IO;
    using System.Net;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Excel;
    using HtmlAgilityPack;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using Axxess.LookUp.Domain;

    public static class HippsAndHhrgScript
    {
        private static string hhrg2012input = Path.Combine(App.Root, "Files\\hhrg2013.xlsx");

        public static void Run()
        {
            LoadHhrg();
        }

        private static void LoadHhrg()
        {
            using (FileStream hhrgStream = new FileStream(hhrg2012input, FileMode.Open, FileAccess.Read))
            {
                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(hhrgStream))
                {
                    if (excelReader != null && excelReader.IsValid)
                    {
                        excelReader.IsFirstRowAsColumnNames = true;
                        DataTable dataTable = excelReader.AsDataSet().Tables[0];
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            var hhrgCounter = 1;
                            var previousHhrg = string.Empty;
                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                if (!dataRow.IsEmpty())
                                {
                                    if (!dataRow.GetValue(0).ToLower().StartsWith("effective"))
                                    {
                                        var hhrg = dataRow.GetValue(0);
                                        if (previousHhrg.IsNullOrEmpty())
                                        {
                                            previousHhrg = hhrg;
                                        }
                                        if (hhrg.IsNullOrEmpty())
                                        {
                                            hhrg = previousHhrg;
                                        }
                                        var hippsAndHhrg = new HippsAndHhrg()
                                        {
                                            HHRG = hhrg,
                                            HIPPS = dataRow.GetValue(1),
                                            HHRGWeight = dataRow.GetValue(2).ToDouble(),
                                            Time = DateTime.Parse("1/1/2013")
                                        };
                                        if (Database.AddForLookup<HippsAndHhrg>(hippsAndHhrg))
                                        {
                                            Console.WriteLine("{0}) {1} - {2} - {3:#0.0000} {4}", hhrgCounter, hippsAndHhrg.HHRG, hippsAndHhrg.HIPPS, hippsAndHhrg.HHRGWeight, hippsAndHhrg.Time.ToShortDateString());
                                            hhrgCounter++;
                                        }
                                        if (!hhrg.IsEqual(previousHhrg))
                                        {
                                            previousHhrg = hhrg;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
