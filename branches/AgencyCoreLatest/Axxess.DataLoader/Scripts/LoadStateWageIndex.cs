﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    public static class LoadStateWageIndex
    {
        private static string statewageindexinput = Path.Combine(App.Root, "Files\\HH_PPS_wage_index_All.xlsx");
        public static void Run()
        {
            using (FileStream wageIndexStream = new FileStream(statewageindexinput, FileMode.Open, FileAccess.Read))
            {
                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(wageIndexStream))
                {
                    if (excelReader != null && excelReader.IsValid)
                    {
                        excelReader.IsFirstRowAsColumnNames = false;
                        DataTable dataTable = excelReader.AsDataSet().Tables[0];
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                if (!dataRow.IsEmpty() && dataRow.GetValue(0).IsNotNullOrEmpty() && dataRow.GetValue(1).IsNotNullOrEmpty())
                                {
                                    var ruralCbsaCode = new RuralCbsaCode
                                    {
                                        CBSA = dataRow.GetValue(0).PadLeft(2, '0').PadLeft(5, '9'),
                                        StateName = dataRow.GetValue(1),
                                        WITwoEleven = dataRow.GetValue(2).IsNotNullOrEmpty() && dataRow.GetValue(2).IsDouble() ? dataRow.GetValue(2).ToDouble() : 0,
                                        WITwoTwelve = dataRow.GetValue(3).IsNotNullOrEmpty() && dataRow.GetValue(3).IsDouble() ? dataRow.GetValue(3).ToDouble() : 0,
                                        WITwoThirteen = dataRow.GetValue(4).IsNotNullOrEmpty() && dataRow.GetValue(4).IsDouble() ? dataRow.GetValue(4).ToDouble() : 0
                                    };
                                    if (Database.AddForLookup<RuralCbsaCode>(ruralCbsaCode))
                                    {
                                        Console.WriteLine("Rural Code: {0}", ruralCbsaCode.ToString());
                                    }
                                }
                            }
                            Console.WriteLine();
                        }
                    }
                }
            }
        }
    }
}
