﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class AluraPhysician
    {
        private static string input = Path.Combine(App.Root, "Files\\Physician.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Physician{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty() && dataRow.GetValue(1).IsNotNullOrEmpty())
                                        {
                                            var physicianData = new AgencyPhysician();
                                            physicianData.Id = Guid.NewGuid();
                                            physicianData.AgencyId = agencyId;
                                            physicianData.IsDeprecated = false;

                                            //physicianData.UPIN = dataRow.GetValue(4);
                                            physicianData.NPI = dataRow.GetValue(3);
                                            
                                            var nameArray = dataRow.GetValue(0).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray != null && nameArray.Length > 1)
                                            {
                                                physicianData.LastName = nameArray[0].Trim();
                                                physicianData.FirstName = nameArray[1].Trim();
                                            }
                                            var addressRow = dataRow.GetValue(1);
                                            if (addressRow.IsNotNullOrEmpty())
                                            {
                                                var addressArray = addressRow.Split(',');
                                                if (addressArray.Length <= 4)
                                                {
                                                    physicianData.AddressLine1 = addressArray[0].Trim();
                                                    physicianData.AddressCity = addressArray[1].Trim();
                                                    physicianData.AddressStateCode = addressArray[2].Trim();
                                                    physicianData.AddressZipCode = addressArray[3].Trim();
                                                }
                                                else if (addressArray.Length > 4)
                                                {
                                                    physicianData.AddressLine1 = addressArray[0].Trim();
                                                    physicianData.AddressLine2 = addressArray[1].Trim();
                                                    physicianData.AddressCity = addressArray[2].Trim();
                                                    physicianData.AddressStateCode = addressArray[3].Trim();
                                                    physicianData.AddressZipCode = addressArray[4].Trim();
                                                }

                                            }

                                            //if(addressRow.IsNotNullOrEmpty())
                                            //{
                                            //    var addressArray = addressRow.Split(',');
                                            //    if (addressArray.Length >= 3)
                                            //    {
                                            //        physicianData.AddressLine1 = addressArray[0];
                                            //        var line2City = addressArray[1].Split(new string[] { "     " }, StringSplitOptions.RemoveEmptyEntries);
                                            //        physicianData.AddressLine2 = line2City[0].Trim();
                                            //        physicianData.AddressCity = line2City[1].Trim();
                                            //        var stateZip1 = addressArray[2].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                            //        physicianData.AddressStateCode = stateZip1[0].Trim();
                                            //        physicianData.AddressZipCode = stateZip1[1].Trim();
                                            //    }
                                            //    else
                                            //    {
                                            //        var addressCity = addressArray[0].Split(new string[] { "     " }, StringSplitOptions.RemoveEmptyEntries);
                                            //        physicianData.AddressLine1 = addressCity[0].Trim();
                                            //        physicianData.AddressCity = addressCity[1].Trim();
                                            //        var stateZip = addressArray[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                            //        physicianData.AddressStateCode = stateZip[0].Trim();
                                            //        physicianData.AddressZipCode = stateZip[1].Trim();
                                            //    }
                                            //}


                                            if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                            {
                                                physicianData.PhoneWork = dataRow.GetValue(2).ToPhoneDB();
                                            }
                                            if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            {
                                                physicianData.FaxNumber = dataRow.GetValue(5).ToPhoneDB();
                                            }
                                            //if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                            //{
                                            //    physicianData.PhoneAlternate = dataRow.GetValue(4).ToPhoneDB();
                                            //}
                                            //if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                            //{
                                            //    physicianData.Comments += string.Format("{0}.", dataRow.GetValue(5));
                                            //}
                                            //physicianData.Credentials = dataRow.GetValue(6);
                                            //if (dataRow.GetValue(1).IsNotNullOrEmpty())
                                            //{
                                            //    var addressArray = dataRow.GetValue(6).Trim().Split(' ');

                                            //    physicianData.AddressCity = addressArray[addressArray.Length - 3];
                                            //    physicianData.AddressStateCode = addressArray[addressArray.Length-2];
                                            //    physicianData.AddressZipCode = addressArray[addressArray.Length-1];
                                            //    int b = dataRow.GetValue(6).Trim().IndexOf(physicianData.AddressCity);
                                            //    physicianData.AddressLine1 = dataRow.GetValue(6).Trim().Substring(0, b - 1);
                                            //}
                                            //if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            //{
                                            //    physicianData.Comments += string.Format("Other:{0}.", dataRow.GetValue(7));
                                            //}
                                            //if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                            //{
                                            //    physicianData.EmailAddress = dataRow.GetValue(8).Trim();
                                            //}

                                            physicianData.Created = DateTime.Now;
                                            physicianData.Modified = DateTime.Now;
                                            try
                                            {
                                                var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                                if (physician == null)
                                                {
                                                    if (Database.Add(physicianData))
                                                    {
                                                        Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                Console.WriteLine("Error on row: {0}",i);
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
