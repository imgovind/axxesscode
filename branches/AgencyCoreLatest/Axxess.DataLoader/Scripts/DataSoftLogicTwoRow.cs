﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Data;
    using System.Text;

    using Excel;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class DataSoftLogicTwoRow
    {
        private static string input = Path.Combine(App.Root, "Files\\active.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\active_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                var i = 1;
                                int numberOfRows = 2;
                                int rowCounter = 1;
                                Patient patientData = null;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        if (rowCounter % numberOfRows == 1)
                                        {
                                            patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId.ToGuid();
                                            patientData.AgencyLocationId = locationId.ToGuid();
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;
                                            patientData.AddressLine1 = string.Empty;
                                            patientData.AddressLine2 = string.Empty;
                                            patientData.AddressCity = string.Empty;
                                            patientData.AddressStateCode = string.Empty;
                                            patientData.AddressZipCode = string.Empty;

                                            var nameRow = dataRow.GetValue(0).Split('\n');
                                            patientData.PatientIdNumber = nameRow[0];
                                            var nameBday = nameRow[1].Split('-');
                                            patientData.DOB = nameBday[1].ToDateTime();
                                            var name = nameBday[0].Split(',');
                                            patientData.LastName = name[0];
                                            var firstMiddleName = name[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (firstMiddleName.Length > 1)
                                            {
                                                patientData.FirstName = firstMiddleName[0];
                                                patientData.MiddleInitial = firstMiddleName[1];
                                            }
                                            else
                                            {
                                                patientData.FirstName = firstMiddleName[0];
                                            }

                                            var addressRow = dataRow.GetValue(2).Split('\n');
                                            patientData.PhoneHome = addressRow[1].ToPhoneDB();
                                            var address = addressRow[0].Split(',');
                                            var streetCity = address[0].Split(new string[]{"  "},StringSplitOptions.RemoveEmptyEntries);
                                            patientData.AddressLine1 = streetCity[0];
                                            patientData.AddressCity = streetCity[1];
                                            var stateZip = address[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                            patientData.AddressStateCode = stateZip[0];
                                            patientData.AddressZipCode = stateZip[1];

                                            var socRow = dataRow.GetValue(7).Split(new char[]{' '},StringSplitOptions.RemoveEmptyEntries);
                                            patientData.StartofCareDate = socRow[0].ToDateTime();
                                            patientData.Comments += string.Format("Current Certification: {0} - {1}. ", socRow[1], socRow[3]);

                                            var insuranceRow = dataRow.GetValue(10).Split(',');
                                            if (insuranceRow[0].ToLower().Contains("medicare"))
                                            {
                                                patientData.MedicareNumber = insuranceRow[1];
                                                patientData.PrimaryInsurance = "1";
                                            }
                                            else if (insuranceRow[0].ToLower().Contains("medicaid"))
                                            {
                                                patientData.MedicaidNumber = insuranceRow[1];
                                            }
                                            else
                                            {
                                                patientData.Comments += string.Format("Insurance: {0} - {1}. ", insuranceRow[0], insuranceRow[1]);
                                            }

                                        }
                                        else if (rowCounter % numberOfRows == 0)
                                        {
                                            var statsRow = dataRow.GetValue(1).Split('\n');
                                            if (statsRow[0].Trim() != "-")
                                            {
                                                patientData.Triage = statsRow[0].Split('-')[0].ToInteger();
                                            }
                                            if (statsRow.Length > 1)
                                            {
                                                if (statsRow[1].IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Primary Diagnosis: {0}. ", statsRow[1]);
                                                }
                                                if (statsRow.Length > 2)
                                                {
                                                    if (statsRow[2].IsNotNullOrEmpty())
                                                    {
                                                        patientData.Comments += string.Format("Secondary Diagnosis: {0}. ", statsRow[2]);
                                                    }
                                                }
                                            }

                                            var doctorRow = dataRow.GetValue(4).Split('\n');
                                            var numbers = doctorRow[1].Split(new char[] {' '},StringSplitOptions.RemoveEmptyEntries);
                                            if (numbers.Length > 1)
                                            {
                                                patientData.Comments += string.Format("Doctor: {0} Phone: {1} Fax: {2}. ",doctorRow[0],numbers[0],numbers[1]);
                                            }
                                            else
                                            {
                                                patientData.Comments += string.Format("Doctor: {0} Phone: {1}. ",doctorRow[0],numbers[0]);
                                            }

                                            var caseManagerRow = dataRow.GetValue(8);
                                            if (caseManagerRow.IsNotNullOrEmpty())
                                            {
                                                caseManagerRow = caseManagerRow.Substring(5);
                                                patientData.Comments += string.Format("Case Manager: {0}. ",caseManagerRow);
                                            }

                                            var lastVisit = dataRow.GetValue(10);
                                            if (lastVisit.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Last Visit: {0}. ",lastVisit);
                                            }

                                            patientData.Gender = "";

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };
                                            if(Database.GetPatientByMR(patientData.PatientIdNumber, new Guid(agencyId))==null)
                                            {
                                                if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                }
                                                textWriter.Write(textWriter.NewLine);
                                                i++;
                                            }
                                        }
                                        rowCounter++;
                                    }
                                }
                                Console.WriteLine(rowCounter);
                            }
                        }
                    }
                }
            }
        }
    }
}
