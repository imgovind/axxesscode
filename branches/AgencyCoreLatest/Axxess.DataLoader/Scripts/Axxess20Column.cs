﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public static class Axxess20Column
    {
        private static string input = Path.Combine(App.Root, "Files\\Mega Health Care Providers-Active.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Mega Health Care Providers-Active-EventLog{0}.txt", DateTime.Now.Ticks.ToString()));

        private static string log = Path.Combine(App.Root, string.Format("Files\\Mega Health Care Providers-Active-DataLog{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            #region DataPresent
            bool isPhysicianDataPresent = default(bool);
            bool isDBWritePermission = default(bool);
            bool isPhysicianDataChecked = default(bool);
            #endregion

            #region VARS
            #region delcaration
            int oneName_col = default(int);
            int firstName_col = default(int);
            int lastName_col = default(int);
            int gender_col = default(int);
            int id_col = default(int);
            int medicare_col = default(int);
            int medicaid_col = default(int);
            int dob_col = default(int);
            int ssn_col = default(int);
            int soc_col = default(int);
            int episodeSD_col = default(int);
            int marital_col = default(int);
            int addr1_col = default(int);
            int city_col = default(int);
            int state_col = default(int);
            int zip_col = default(int);
            int phonehome_col = default(int);
            int mdName_col = default(int);
            int mdAddr1_col = default(int);
            int mdCity_col = default(int);
            int mdState_col = default(int);
            int mdZip_col = default(int);
            int mdPhone_col = default(int);
            int mdFax_col = default(int);
            int mdCred_col = default(int);
            int mdNPI_col = default(int);
            int insurance_col = default(int);
            int md_fn_col = default(int);
            int md_ln_col = default(int);
            int comment_Insurance_Number_col = default(int);
            int primaryClinician_col = default(int);
            int documentType_col = default(int);
            int dischargeDate_col = default(int);
            int comment_mdfn_col = default(int);
            int comment_mdln_col = default(int);

            #endregion
            //New Additions
            int primaryDiag_col = default(int);
            int primaryDiagDesc_col = default(int);
            //

            string firstColumn = "id_col";
            string insurancePayor = "1";
            string commentInsuranceName = "";
            comment_Insurance_Number_col = 20;

            oneName_col = 1;
            //firstName_col = 0;
            //lastName_col = 1;
            //gender_col = 2;
            insurance_col = 5;
            id_col = 0;
            //medicare_col = 5;
            //medicaid_col = 6;
            dob_col = 2;
            ssn_col = 3;
            soc_col = 6;
            episodeSD_col = 4;
            //marital_col = 10;
            addr1_col = 7;
            city_col = 8;
            state_col = 9;
            zip_col = 10;
            phonehome_col = 11;
            mdName_col = 12;
            //mdAddr1_col = 17;
            //mdCity_col = 18;
            //mdState_col = 19;
            //mdZip_col = 20;
            //mdPhone_col = 20;
            //mdFax_col = 21;
            mdNPI_col = 13;
            //md_fn_col = 22;
            //mdCred_col = 20;
            //md_ln_col = 15;
            //primaryDiag_col = 23;
            //primaryDiagDesc_col = 24;
            //primaryClinician_col = 8;
            //documentType_col = 9;
            //dischargeDate_col = 10;
            //comment_mdfn_col = 12;
            //comment_mdln_col = 11;

            string CommentsDelimiter = "; ";
            #endregion

            #region physicianVARS
            int mdFirstName_SubCol = default(int);
            int mdLastName_SubCol = default(int);
            int mdMiddleName_SubCol = default(int);
            mdFirstName_SubCol = 0;
            mdLastName_SubCol = 1;
            mdMiddleName_SubCol = 2;
            #endregion

            isPhysicianDataPresent = true;
            isPhysicianDataChecked = false;

            #region physicianCheckProcess
            if (isPhysicianDataPresent == true)
            {
                if (isPhysicianDataChecked == true)
                {
                    isDBWritePermission = false;
                }
                else if (isPhysicianDataChecked == false)
                {
                    isDBWritePermission = true;
                }
            }
            else
            {
                isDBWritePermission = false;
                isPhysicianDataChecked = false;
            }
            #endregion

            //Set this to false if you dont want to write to DB
            isDBWritePermission = true;

            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;

                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            TextWriter txtWrtr = new StreamWriter(log, true);
                                            StringBuilder sb = new StringBuilder();

                                            var patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId;
                                            patientData.AgencyLocationId = locationId;
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;
                                            patientData.ServiceLocation = "Q5001";

                                            #region PROCESS

                                            #region patientName
                                            bool isPatientNameOneCell = default(bool);
                                            //One Name
                                            if (oneName_col != default(int) || (firstColumn.Equals("oneName_col") && oneName_col != null))
                                            {
                                                var oneNameStaging = dataRow.GetValue(oneName_col);
                                                var nameArray = oneNameStaging.Split(',');
                                                if (nameArray != null && nameArray.Length > 0)
                                                {
                                                    if (nameArray.Length > 1)
                                                    {
                                                        var naamArray1 = nameArray[1].TrimStart(' ').Split(' ');
                                                        if (naamArray1.Length > 1)
                                                        {
                                                            patientData.MiddleInitial = naamArray1[1];
                                                            patientData.FirstName = naamArray1[0];
                                                            sb.Append("MiddleInitial"); sb.Append(": "); sb.Append(patientData.MiddleInitial); sb.Append("\t\t");
                                                        }
                                                        else
                                                        {
                                                            patientData.FirstName = nameArray[1];
                                                        }
                                                        patientData.LastName = nameArray[0];
                                                    }
                                                }
                                                sb.Append("FirstName"); sb.Append(": "); sb.Append(patientData.FirstName); sb.Append("\t\t");
                                                sb.Append("LastName"); sb.Append(": "); sb.Append(patientData.LastName); sb.Append("\t\t");
                                                isPatientNameOneCell = true;
                                            }
                                            //

                                            if (isPatientNameOneCell == false)
                                            {
                                                //FN - 0
                                                if (firstName_col != default(int) || (firstColumn.Equals("firstName_col") && firstName_col != null))
                                                {
                                                    var firstNameStaging = dataRow.GetValue(firstName_col);
                                                    if (firstNameStaging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.FirstName = firstNameStaging;
                                                    }
                                                    sb.Append("FirstName"); sb.Append(": "); sb.Append(patientData.FirstName); sb.Append("\t\t");
                                                }
                                                //

                                                //LN - 0
                                                if (lastName_col != default(int) || (firstColumn.Equals("lastName_col") && lastName_col != null))
                                                {
                                                    var lastNameStaging = dataRow.GetValue(lastName_col);
                                                    if (lastNameStaging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.LastName = lastNameStaging;
                                                    }
                                                    patientData.LastName = dataRow.GetValue(lastName_col);
                                                    sb.Append("LastName"); sb.Append(": "); sb.Append(patientData.LastName); sb.Append("\t\t");
                                                }
                                                //
                                            }
                                            #endregion

                                            #region ID+Gender
                                            //GENDER
                                            if (gender_col != default(int) || (firstColumn.Equals("gender_col") && gender_col != null))
                                            {
                                                var genderStaging = dataRow.GetValue(gender_col);
                                                if (genderStaging.IsNotNullOrEmpty() && genderStaging.Length >= 1)
                                                {
                                                    patientData.Gender = genderStaging.ToUpper().StartsWith("F") ? "Female" : genderStaging.ToUpper().StartsWith("M") ? "Male" : string.Empty;
                                                }
                                                sb.Append("Gender"); sb.Append(": "); sb.Append(patientData.Gender); sb.Append("\t\t");
                                            }
                                            else
                                            {
                                                patientData.Gender = "";
                                                sb.Append("Gender"); sb.Append(": "); sb.Append(patientData.Gender); sb.Append("\t\t");
                                            }
                                            //

                                            //ID - MRN
                                            if (id_col != default(int) || (firstColumn.Equals("id_col") && id_col != null))
                                            {
                                                var idStaging = dataRow.GetValue(id_col);
                                                if (idStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.PatientIdNumber = idStaging;
                                                }
                                                sb.Append("PatientIdNumber"); sb.Append(": "); sb.Append(patientData.PatientIdNumber); sb.Append("\t\t");
                                            }
                                            //
                                            #endregion

                                            #region Medicare+Medicaid
                                            bool primaryInsurancePayor = default(bool);
                                            bool isPrimaryInsuranceMedicare = default(bool);
                                            var insuranceStagingNomer = dataRow.GetValue(insurance_col);
                                            if (insuranceStagingNomer.ToUpper().Contains("MEDICARE") || insuranceStagingNomer.ToUpper().Contains("MEDICAID"))
                                            {
                                                isPrimaryInsuranceMedicare = true;
                                            }

                                            if (isPrimaryInsuranceMedicare == true)
                                            {
                                                //MEDICARE - 4
                                                if (medicare_col != default(int) || (firstColumn.Equals("medicare_col") && medicare_col != null))
                                                {
                                                    var medicareNumberStaging = dataRow.GetValue(medicare_col);
                                                    if (medicareNumberStaging.IsNotNullOrEmpty())
                                                    {
                                                        patientData.MedicareNumber = medicareNumberStaging;
                                                        primaryInsurancePayor = true;
                                                    }
                                                    sb.Append("MedicareNumber"); sb.Append(": "); sb.Append(patientData.MedicareNumber); sb.Append("\t\t");
                                                }
                                                //

                                                //MEDICAID - 5
                                                if (medicaid_col != default(int) || (firstColumn.Equals("medicaid_col") && medicaid_col != null))
                                                {
                                                    var medicaidNumberStaging = dataRow.GetValue(medicaid_col);
                                                    if (medicaidNumberStaging.IsNotNullOrEmpty())
                                                    {
                                                        if (medicaidNumberStaging.Equals("NA")) { }
                                                        else
                                                        {
                                                            patientData.MedicaidNumber = medicaidNumberStaging;
                                                            primaryInsurancePayor = true;
                                                        }
                                                    }
                                                    sb.Append("MedicaidNumber"); sb.Append(": "); sb.Append(patientData.MedicaidNumber); sb.Append("\t\t");
                                                }
                                                //
                                            }
                                            #endregion

                                            #region PrimaryInsurance - Set it Right - Manual

                                            //var insuranceStagingJimple = dataRow.GetValue(33);

                                            //PRIMARY INSURANCE - Get From Payor in agency record from agencies table
                                            if (isPrimaryInsuranceMedicare == true)
                                            {
                                                patientData.PrimaryInsurance = insurancePayor;
                                            }
                                            else if (isPrimaryInsuranceMedicare == false)
                                            {
                                                patientData.PrimaryInsurance = "";
                                            }
                                            sb.Append("PrimaryInsurance"); sb.Append(": "); sb.Append(patientData.PrimaryInsurance); sb.Append("\t\t");
                                            #endregion

                                            #region SSN+Marital
                                            //SSN
                                            if (ssn_col != default(int) || (firstColumn.Equals("ssn_col") && ssn_col != null))
                                            {
                                                var ssnStaging = dataRow.GetValue(ssn_col).Replace(" ", "");
                                                if (ssnStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.SSN = ssnStaging;
                                                }
                                                sb.Append("SSN"); sb.Append(": "); sb.Append(patientData.SSN); sb.Append("\t\t");
                                            }
                                            //

                                            //MARITAL STATUS
                                            if (marital_col != default(int) || (firstColumn.Equals("marital_col") && marital_col != null))
                                            {
                                                var maritalStatus = dataRow.GetValue(marital_col);
                                                if (maritalStatus.IsNotNullOrEmpty())
                                                {
                                                    maritalStatus = maritalStatus.ToUpperCase();
                                                    if (maritalStatus.StartsWith("M"))
                                                    {
                                                        patientData.MaritalStatus = "Married";
                                                    }
                                                    else if (maritalStatus.StartsWith("S"))
                                                    {
                                                        patientData.MaritalStatus = "Single";
                                                    }
                                                    else if (maritalStatus.StartsWith("W"))
                                                    {
                                                        patientData.MaritalStatus = "Widowed";
                                                    }
                                                    else if (maritalStatus.StartsWith("D"))
                                                    {
                                                        patientData.MaritalStatus = "Divorced";
                                                    }
                                                    else
                                                    {
                                                        patientData.MaritalStatus = "Unknown";
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.MaritalStatus = "Unknown";
                                                }
                                                sb.Append("MaritalStatus"); sb.Append(": "); sb.Append(patientData.MaritalStatus); sb.Append("\t\t");
                                            }
                                            #endregion

                                            #region patientDOB
                                            //DOB - 6
                                            if (dob_col != default(int) || (firstColumn.Equals("dob_col") && phonehome_col != null))
                                            {
                                                var DOBStaging = dataRow.GetValue(dob_col);
                                                if (DOBStaging.IsNotNullOrEmpty())
                                                {
                                                    if (DOBStaging.IsDouble())
                                                    {
                                                        patientData.DOB = DateTime.FromOADate(double.Parse(DOBStaging));
                                                    }
                                                    else
                                                    {
                                                        patientData.DOB = DateTime.Parse(DOBStaging);
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.DOB = DateTime.MinValue;
                                                }
                                            }
                                            else
                                            {
                                                patientData.DOB = DateTime.MinValue;
                                            }
                                            sb.Append("DOB"); sb.Append(": "); sb.Append(patientData.DOB); sb.Append("\t\t");
                                            #endregion

                                            #region Phone+Addr1
                                            //PHONE HOME - 7
                                            if (phonehome_col != default(int) || (firstColumn.Equals("phonehome_col") && phonehome_col != null))
                                            {
                                                var phoneHomeStaging = dataRow.GetValue(phonehome_col);
                                                patientData.PhoneHome = phoneHomeStaging.IsNotNullOrEmpty() ? phoneHomeStaging.ToPhoneDB() : string.Empty;
                                                sb.Append("PhoneHome"); sb.Append(": "); sb.Append(patientData.PhoneHome); sb.Append("\t\t");
                                            }
                                            //ADDRESS LINE 1 - 8
                                            if (addr1_col != default(int) || (firstColumn.Equals("addr1_col") && addr1_col != null))
                                            {
                                                var addressLineStaging = dataRow.GetValue(addr1_col);
                                                if (addressLineStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressLine1 = addressLineStaging;
                                                }
                                                else
                                                {
                                                    patientData.AddressLine1 = "";
                                                }
                                                sb.Append("AddressLine1"); sb.Append(": "); sb.Append(patientData.AddressLine1); sb.Append("\t\t");
                                            }
                                            #endregion

                                            #region City+State+Zip
                                            //ADDRESS CITY - 9
                                            if (city_col != default(int) || (firstColumn.Equals("city_col") && city_col != null))
                                            {
                                                var addressCityStaging = dataRow.GetValue(city_col);
                                                if (addressCityStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressCity = addressCityStaging;
                                                }
                                                else
                                                {
                                                    patientData.AddressCity = "";
                                                }
                                                sb.Append("AddressCity"); sb.Append(": "); sb.Append(patientData.AddressCity); sb.Append("\t\t");
                                            }
                                            if (state_col != default(int) || (firstColumn.Equals("state_col") && state_col != null))
                                            {
                                                //ADDRESS STATE - 10
                                                var addressStateCodeStaging = dataRow.GetValue(state_col).ToUpper().Replace(".", "");
                                                if (addressStateCodeStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressStateCode = addressStateCodeStaging;
                                                }
                                                else
                                                {
                                                    patientData.AddressStateCode = "";
                                                }
                                                sb.Append("AddressStateCode"); sb.Append(": "); sb.Append(patientData.AddressStateCode); sb.Append("\t\t");
                                            }
                                            //ADDRESSS ZIP - 11
                                            if (zip_col != default(int) || (firstColumn.Equals("zip_col") && zip_col != null))
                                            {
                                                var addressZipCodeStaging = dataRow.GetValue(zip_col);
                                                if (addressZipCodeStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.AddressZipCode = addressZipCodeStaging;
                                                }
                                                else
                                                {
                                                    patientData.AddressZipCode = "";
                                                }
                                                sb.Append("AddressZipCode"); sb.Append(": "); sb.Append(patientData.AddressZipCode); sb.Append("\t\t");
                                            }
                                            #endregion

                                            #region SOC
                                            //SOC - 13
                                            if (soc_col != default(int) || (firstColumn.Equals("soc_col") && soc_col != null))
                                            {
                                                bool didDateSucceed = default(bool);
                                                var SOCStaging = dataRow.GetValue(soc_col);

                                                if (SOCStaging.IsNotNullOrEmpty())
                                                {
                                                    if (SOCStaging.IsDouble())
                                                    {
                                                        patientData.StartofCareDate = DateTime.FromOADate(double.Parse(SOCStaging));
                                                    }
                                                    else
                                                    {
                                                        patientData.StartofCareDate = DateTime.Parse(SOCStaging);
                                                    }
                                                }
                                                else
                                                {
                                                    patientData.StartofCareDate = DateTime.MinValue;
                                                }
                                                sb.Append("StartofCareDate"); sb.Append(": "); sb.Append(patientData.StartofCareDate); sb.Append("\t\t");
                                            }
                                            //
                                            #endregion


                                            #endregion

                                            #region COMMENTS

                                            #region EPISODE SD + INSURANCE (MANUAL)
                                            //Episode Start Date
                                            if (episodeSD_col != default(int))
                                            {
                                                var episodeStartDateStaging = dataRow.GetValue(episodeSD_col);
                                                if (episodeStartDateStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Current Cert Period:{0} {1}", episodeStartDateStaging, CommentsDelimiter);
                                                }
                                            }
                                            //
                                            //Insurance Information - MANUAL
                                            if (insurance_col != default(int))
                                            {
                                                var insuranceStaging = dataRow.GetValue(insurance_col);
                                                if (insuranceStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Insurance: " + insuranceStaging + CommentsDelimiter;
                                                    var insuranceNumber = dataRow.GetValue(medicare_col);
                                                    if (insuranceNumber.IsNotNullOrEmpty())
                                                    {
                                                        //patientData.Comments += "Insurance Number: " + insuranceNumber + CommentsDelimiter;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var insuranceNumber = dataRow.GetValue(comment_Insurance_Number_col);
                                                patientData.Comments += "Insurance: " + commentInsuranceName + CommentsDelimiter;
                                                //patientData.Comments += "Insurance Number: " + insuranceNumber + CommentsDelimiter;
                                            }
                                            //
                                            #endregion

                                            #region Discharge Date + Document + Clinician
                                            //Discharge Date
                                            if (dischargeDate_col != default(int))
                                            {
                                                var dischargeDateStaging = dataRow.GetValue(dischargeDate_col);
                                                if (dischargeDateStaging.IsNotNullOrEmpty())
                                                {
                                                    if (dischargeDateStaging.IsDouble())
                                                    {
                                                        patientData.Comments += string.Format("Discharge Date:{0} {1}", DateTime.FromOADate(double.Parse(dischargeDateStaging)).ToShortDateString(), CommentsDelimiter);
                                                    }
                                                    else
                                                    {
                                                        patientData.Comments += string.Format("Discharge Date:{0} {1}", DateTime.Parse(dischargeDateStaging).ToShortDateString(), CommentsDelimiter);
                                                    }

                                                }
                                            }
                                            //
                                            //Document Type
                                            if (documentType_col != default(int))
                                            {
                                                var documentTypeStaging = dataRow.GetValue(documentType_col);
                                                if (documentTypeStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Document Type: " + documentTypeStaging + CommentsDelimiter;
                                                }
                                            }
                                            //
                                            //Clinician
                                            if (primaryClinician_col != default(int))
                                            {
                                                var primaryClinicianStaging = dataRow.GetValue(primaryClinician_col);
                                                if (primaryClinicianStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Primary Clinician: " + primaryClinicianStaging + CommentsDelimiter;
                                                }
                                            }
                                            //
                                            #endregion

                                            #region PrimaryDiagnosis + rySecondaryDiagnosis

                                            //Primary

                                            if (primaryDiag_col != default(int))
                                            {
                                                var primaryDiagStaging = dataRow.GetValue(primaryDiag_col);
                                                if (primaryDiagStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Primary Diagnosis: " + primaryDiagStaging + CommentsDelimiter;
                                                }
                                            }

                                            //Secondary
                                            if (primaryDiagDesc_col != default(int))
                                            {
                                                var primaryDiagDescStaging = dataRow.GetValue(primaryDiagDesc_col);
                                                if (primaryDiagDescStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Description: " + primaryDiagDescStaging + CommentsDelimiter;
                                                }
                                            }
                                            #endregion

                                            #region physician LN + FN
                                            if (comment_mdfn_col != default(int))
                                            {
                                                var commentMDFNStaging = dataRow.GetValue(comment_mdfn_col);
                                                if (commentMDFNStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Physician First Name: " + commentMDFNStaging + CommentsDelimiter;
                                                }
                                            }

                                            if (comment_mdln_col != default(int))
                                            {
                                                var commentMDLNStaging = dataRow.GetValue(comment_mdln_col);
                                                if (commentMDLNStaging.IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += "Physician Last Name: " + commentMDLNStaging + CommentsDelimiter;
                                                }

                                            }
                                            #endregion

                                            patientData.Created = DateTime.Now;
                                            sb.Append("Created"); sb.Append(": "); sb.Append(patientData.Created); sb.Append("\t\t");
                                            patientData.Modified = DateTime.Now;
                                            sb.Append("Modified"); sb.Append(": "); sb.Append(patientData.Modified); sb.Append("\t\t");

                                            if (isPhysicianDataPresent == true)
                                            {
                                                if (isPhysicianDataChecked == true)
                                                {
                                                    #region PhysicianCheck
                                                    var npi = dataRow.GetValue(mdNPI_col);
                                                    if (npi.IsNotNullOrEmpty())
                                                    {
                                                        var physician = Database.GetPhysician(npi, agencyId);
                                                        if (physician == null)
                                                        {
                                                            var info = Database.GetNpiData(npi);
                                                            if (info != null)
                                                            {
                                                                physician = new AgencyPhysician
                                                                {
                                                                    Id = Guid.NewGuid(),
                                                                    AgencyId = agencyId,
                                                                    NPI = npi,
                                                                    LoginId = Guid.Empty,
                                                                    AddressLine1 = string.Empty,
                                                                    AddressCity = string.Empty,
                                                                    AddressStateCode = string.Empty,
                                                                    AddressZipCode = string.Empty,
                                                                    PhoneWork = string.Empty,
                                                                    FaxNumber = string.Empty,
                                                                    Credentials = string.Empty
                                                                };

                                                                #region Addr1+City+State+Zip+Phone+Fax+Credential
                                                                if (mdAddr1_col != default(int))
                                                                {
                                                                    var mdAddr1Staging = dataRow.GetValue(mdAddr1_col);
                                                                    if (mdAddr1Staging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.AddressLine1 = mdAddr1Staging;
                                                                    }
                                                                }

                                                                if (mdCity_col != default(int))
                                                                {
                                                                    var mdCityStaging = dataRow.GetValue(mdCity_col);
                                                                    if (mdCityStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.AddressCity = mdCityStaging;
                                                                    }
                                                                }

                                                                if (mdState_col != default(int))
                                                                {
                                                                    var mdStateStaging = dataRow.GetValue(mdState_col);
                                                                    if (mdStateStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.AddressStateCode = mdStateStaging;
                                                                    }
                                                                }

                                                                if (mdZip_col != default(int))
                                                                {
                                                                    var mdZipStaging = dataRow.GetValue(mdZip_col);
                                                                    if (mdZipStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.AddressZipCode = mdZipStaging;
                                                                    }
                                                                }

                                                                if (mdPhone_col != default(int))
                                                                {
                                                                    var mdPhoneStaging = dataRow.GetValue(mdPhone_col);
                                                                    if (mdPhoneStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.PhoneWork = mdPhoneStaging;
                                                                    }
                                                                }

                                                                if (mdFax_col != default(int))
                                                                {
                                                                    var mdFaxStaging = dataRow.GetValue(mdFax_col);
                                                                    if (mdFaxStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.FaxNumber = mdFaxStaging;
                                                                    }
                                                                }

                                                                if (mdCred_col != default(int))
                                                                {
                                                                    var mdCredStaging = dataRow.GetValue(mdCred_col);
                                                                    if (mdCredStaging.IsNotNullOrEmpty())
                                                                    {
                                                                        physician.Credentials = mdCredStaging;
                                                                    }
                                                                }
                                                                #endregion

                                                                var physicianName = dataRow.GetValue(mdName_col);
                                                                if (physicianName.IsNotNullOrEmpty())
                                                                {
                                                                    var physicianNameArray = physicianName.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                                    if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                                    {
                                                                        if (physicianNameArray.Length == 3)
                                                                        {
                                                                            physician.FirstName = physicianNameArray[mdFirstName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdFirstName_SubCol].Trim() : info.ProviderFirstName;
                                                                            physician.MiddleName = physicianNameArray[mdMiddleName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdMiddleName_SubCol].Trim() : info.ProviderMiddleName;
                                                                            physician.LastName = physicianNameArray[mdLastName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdLastName_SubCol].Trim() : info.ProviderLastName;
                                                                        }
                                                                        else
                                                                        {
                                                                            physician.FirstName = physicianNameArray[mdFirstName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdFirstName_SubCol].Trim() : info.ProviderFirstName;
                                                                            physician.LastName = physicianNameArray[mdLastName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdLastName_SubCol].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        physician.LastName = info.ProviderLastName;
                                                                        physician.FirstName = info.ProviderFirstName;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    physician.LastName = info.ProviderLastName;
                                                                    physician.FirstName = info.ProviderFirstName;
                                                                }
                                                            }
                                                        }
                                                        if (physician != null)
                                                        {
                                                            StringBuilder sb2 = new StringBuilder();
                                                            sb2.Append("<----------------------------->");
                                                            sb2.Append("Phys FN: "); sb2.Append(physician.FirstName); sb2.AppendLine();
                                                            sb2.Append("Phys LN: "); sb2.Append(physician.LastName); sb2.AppendLine();
                                                            sb2.Append("Phys NPI: "); sb2.Append(physician.NPI); sb2.AppendLine();
                                                            sb2.Append("Phys Phone: "); sb2.Append(physician.PhoneWork); sb2.AppendLine();
                                                            sb2.Append("Phys Fax: "); sb2.Append(physician.FaxNumber); sb2.AppendLine();
                                                            sb2.Append("<----------------------------->");
                                                            Console.WriteLine(sb2.ToString());
                                                            sb.Append("Phys FN: "); sb.Append(physician.FirstName); sb.Append("\t\t");
                                                            sb.Append("Phys LN: "); sb.Append(physician.LastName); sb.Append("\t\t");
                                                            sb.Append("Phys NPI: "); sb.Append(physician.NPI); sb.Append("\t\t");
                                                            sb.Append("Phys Phone: "); sb.Append(physician.PhoneWork); sb.Append("\t\t");
                                                            sb.Append("Phys Fax: "); sb.Append(physician.FaxNumber); sb.Append("\t\t");
                                                        }
                                                    }

                                                    #endregion
                                                }
                                            }
                                            sb.Append("Comments"); sb.Append(": "); sb.Append(patientData.Comments); sb.Append("\t\t");
                                            txtWrtr.WriteLine(sb.ToString());

                                            sb.Length = 0;
                                            txtWrtr.Close();
                                            #endregion

                                            //Check if writing to Dev Database.
                                            //Check the Program.cs for AgencyId, AgencyLocationId
                                            if (isDBWritePermission == true)
                                            {
                                                bool checkExists = default(bool);
                                                Patient patientExisting = Database.GetPatientByMR(patientData.PatientIdNumber, agencyId);

                                                if (patientExisting != null)
                                                {
                                                    checkExists = true;
                                                }

                                                if (checkExists != true)
                                                {
                                                    #region DBWrite
                                                    var medicationProfile = new MedicationProfile
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        PatientId = patientData.Id,
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        Medication = "<ArrayOfMedication />"
                                                    };

                                                    var allergyProfile = new AllergyProfile
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        PatientId = patientData.Id,
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        Allergies = "<ArrayOfAllergy />"
                                                    };

                                                    if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                    {
                                                        var admissionPeriod = new PatientAdmissionDate
                                                        {
                                                            Id = Guid.NewGuid(),
                                                            AgencyId = agencyId,
                                                            Created = DateTime.Now,
                                                            DischargedDate = patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate : DateTime.MinValue,
                                                            IsActive = true,
                                                            IsDeprecated = false,
                                                            Modified = DateTime.Now,
                                                            PatientData = patientData.ToXml().Replace("'", ""),
                                                            PatientId = patientData.Id,
                                                            Reason = string.Empty,
                                                            StartOfCareDate = patientData.StartofCareDate,
                                                            Status = patientData.Status
                                                        };
                                                        if (Database.Add(admissionPeriod))
                                                        {
                                                            var patient = Database.GetPatient(patientData.Id, agencyId);
                                                            if (patient != null)
                                                            {
                                                                patient.AdmissionId = admissionPeriod.Id;
                                                                if (Database.Update(patient))
                                                                {
                                                                    Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                                                    var exists = true;
                                                                    var npi = dataRow.GetValue(mdNPI_col);
                                                                    if (npi.IsNotNullOrEmpty()) { } else { isPhysicianDataPresent = false; }
                                                                    if (isPhysicianDataPresent == true)
                                                                    {
                                                                        #region physicianDBWrite
                                                                        if (npi.IsNotNullOrEmpty())
                                                                        {
                                                                            var physician = Database.GetPhysician(npi, agencyId);
                                                                            if (physician == null)
                                                                            {
                                                                                exists = false;
                                                                                var info = Database.GetNpiData(npi);
                                                                                if (info != null)
                                                                                {
                                                                                    physician = new AgencyPhysician
                                                                                    {
                                                                                        Id = Guid.NewGuid(),
                                                                                        AgencyId = agencyId,
                                                                                        NPI = npi,
                                                                                        LoginId = Guid.Empty,
                                                                                        AddressLine1 = string.Empty,
                                                                                        AddressCity = string.Empty,
                                                                                        AddressStateCode = string.Empty,
                                                                                        AddressZipCode = string.Empty,
                                                                                        PhoneWork = string.Empty,
                                                                                        FaxNumber = string.Empty,
                                                                                        Credentials = string.Empty,
                                                                                        //FirstName = dataRow.GetValue(md_fn_col).IsNotNullOrEmpty() ? dataRow.GetValue(md_fn_col) : string.Empty,
                                                                                        //LastName = dataRow.GetValue(md_ln_col).IsNotNullOrEmpty() ? dataRow.GetValue(md_ln_col) : string.Empty
                                                                                    };
                                                                                    #region Addr1+City+State+Zip+Phone+Fax+Credential
                                                                                    if (mdAddr1_col != default(int))
                                                                                    {
                                                                                        var mdAddr1Staging = dataRow.GetValue(mdAddr1_col);
                                                                                        if (mdAddr1Staging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressLine1 = mdAddr1Staging;
                                                                                        }
                                                                                    }

                                                                                    if (mdCity_col != default(int))
                                                                                    {
                                                                                        var mdCityStaging = dataRow.GetValue(mdCity_col);
                                                                                        if (mdCityStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressCity = mdCityStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdState_col != default(int))
                                                                                    {
                                                                                        var mdStateStaging = dataRow.GetValue(mdState_col);
                                                                                        if (mdStateStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressStateCode = mdStateStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdZip_col != default(int))
                                                                                    {
                                                                                        var mdZipStaging = dataRow.GetValue(mdZip_col);
                                                                                        if (mdZipStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressZipCode = mdZipStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdPhone_col != default(int))
                                                                                    {
                                                                                        var mdPhoneStaging = dataRow.GetValue(mdPhone_col);
                                                                                        if (mdPhoneStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.PhoneWork = mdPhoneStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdFax_col != default(int))
                                                                                    {
                                                                                        var mdFaxStaging = dataRow.GetValue(mdFax_col);
                                                                                        if (mdFaxStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.FaxNumber = mdFaxStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdCred_col != default(int))
                                                                                    {
                                                                                        var mdCredStaging = dataRow.GetValue(mdCred_col);
                                                                                        if (mdCredStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.Credentials = mdCredStaging;
                                                                                        }
                                                                                    }
                                                                                    #endregion
                                                                                    var physicianName = dataRow.GetValue(mdName_col);
                                                                                    if (physicianName.IsNotNullOrEmpty())
                                                                                    {
                                                                                        var physicianNameArray = physicianName.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                                                        if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                                                        {
                                                                                            if (physicianNameArray.Length == 3)
                                                                                            {
                                                                                                physician.FirstName = physicianNameArray[mdFirstName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdFirstName_SubCol].Trim() : info.ProviderFirstName;
                                                                                                //physician.MiddleName = physicianNameArray[mdMiddleName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdMiddleName_SubCol].Trim() : info.ProviderMiddleName;
                                                                                                physician.LastName = physicianNameArray[mdLastName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdLastName_SubCol].Trim() : info.ProviderLastName;
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderFirstName;
                                                                                                physician.LastName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                                                                            }

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            physician.LastName = info.ProviderLastName;
                                                                                            physician.FirstName = info.ProviderFirstName;
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        physician.LastName = info.ProviderLastName;
                                                                                        physician.FirstName = info.ProviderFirstName;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    physician = new AgencyPhysician
                                                                                    {
                                                                                        Id = Guid.NewGuid(),
                                                                                        AgencyId = agencyId,
                                                                                        NPI = npi,
                                                                                        LoginId = Guid.Empty,
                                                                                        AddressLine1 = string.Empty,
                                                                                        AddressCity = string.Empty,
                                                                                        AddressStateCode = string.Empty,
                                                                                        AddressZipCode = string.Empty,
                                                                                        PhoneWork = string.Empty,
                                                                                        FaxNumber = string.Empty,
                                                                                        Credentials = string.Empty,
                                                                                        //FirstName = dataRow.GetValue(md_fn_col).IsNotNullOrEmpty() ? dataRow.GetValue(md_fn_col) : string.Empty,
                                                                                        //LastName = dataRow.GetValue(md_ln_col).IsNotNullOrEmpty() ? dataRow.GetValue(md_ln_col) : string.Empty
                                                                                    };
                                                                                    #region Addr1+City+State+Zip+Phone+Fax+Credential
                                                                                    if (mdAddr1_col != default(int))
                                                                                    {
                                                                                        var mdAddr1Staging = dataRow.GetValue(mdAddr1_col);
                                                                                        if (mdAddr1Staging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressLine1 = mdAddr1Staging;
                                                                                        }
                                                                                    }

                                                                                    if (mdCity_col != default(int))
                                                                                    {
                                                                                        var mdCityStaging = dataRow.GetValue(mdCity_col);
                                                                                        if (mdCityStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressCity = mdCityStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdState_col != default(int))
                                                                                    {
                                                                                        var mdStateStaging = dataRow.GetValue(mdState_col);
                                                                                        if (mdStateStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressStateCode = mdStateStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdZip_col != default(int))
                                                                                    {
                                                                                        var mdZipStaging = dataRow.GetValue(mdZip_col);
                                                                                        if (mdZipStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.AddressZipCode = mdZipStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdPhone_col != default(int))
                                                                                    {
                                                                                        var mdPhoneStaging = dataRow.GetValue(mdPhone_col);
                                                                                        if (mdPhoneStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.PhoneWork = mdPhoneStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdFax_col != default(int))
                                                                                    {
                                                                                        var mdFaxStaging = dataRow.GetValue(mdFax_col);
                                                                                        if (mdFaxStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.FaxNumber = mdFaxStaging;
                                                                                        }
                                                                                    }

                                                                                    if (mdCred_col != default(int))
                                                                                    {
                                                                                        var mdCredStaging = dataRow.GetValue(mdCred_col);
                                                                                        if (mdCredStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physician.Credentials = mdCredStaging;
                                                                                        }
                                                                                    }
                                                                                    #endregion
                                                                                    var physicianName = dataRow.GetValue(mdName_col);
                                                                                    if (physicianName.IsNotNullOrEmpty())
                                                                                    {
                                                                                        var physicianNameArray = physicianName.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                                                        if (physicianNameArray != null && physicianNameArray.Length >= 2)
                                                                                        {
                                                                                            if (physicianNameArray.Length == 3)
                                                                                            {
                                                                                                physician.FirstName = physicianNameArray[mdFirstName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdFirstName_SubCol].Trim() : info.ProviderFirstName;
                                                                                                //physician.MiddleName = physicianNameArray[mdMiddleName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdMiddleName_SubCol].Trim() : info.ProviderMiddleName;
                                                                                                physician.LastName = physicianNameArray[mdLastName_SubCol].IsNotNullOrEmpty() ? physicianNameArray[mdLastName_SubCol].Trim() : info.ProviderLastName;
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                physician.FirstName = physicianNameArray[0].IsNotNullOrEmpty() ? physicianNameArray[0].Trim() : info.ProviderFirstName;
                                                                                                physician.LastName = physicianNameArray[1].IsNotNullOrEmpty() ? physicianNameArray[1].Replace("Dr.", "").Trim() : info.ProviderLastName;
                                                                                            }

                                                                                        }
                                                                                        else
                                                                                        {

                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {

                                                                                    }
                                                                                }
                                                                                Database.Add(physician);

                                                                            }


                                                                            if (physician != null)
                                                                            {
                                                                                var patientPhysician = new PatientPhysician
                                                                                {
                                                                                    IsPrimary = true,
                                                                                    PatientId = patientData.Id,
                                                                                    PhysicianId = physician.Id
                                                                                };

                                                                                if (Database.Add(patientPhysician))
                                                                                {
                                                                                    Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "PHYSICIAN ALREADY EXISTS" : "");
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    Console.WriteLine(string.Format("{0}, {1} - PATIENT ALREADY EXISTS", patientData.LastName, patientData.FirstName));
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}