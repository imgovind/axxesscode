﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class MedicationProfileScript
    {
        public static void Run(Guid agencyId)
        {
            try
            {
                var i = 1;
                var patients = Database.GetPatients(agencyId);
                patients.ForEach(p =>
                {
                    var medicationProfile = new MedicationProfile()
                    {
                        Id = Guid.NewGuid(),
                        AgencyId = agencyId,
                        PatientId = p.Id,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        Medication = "<ArrayOfMedication />"
                    };

                    if (Database.Add(medicationProfile))
                    {
                        Console.WriteLine("{0}) {1}", i, p.DisplayName);
                    }
                    i++;
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
