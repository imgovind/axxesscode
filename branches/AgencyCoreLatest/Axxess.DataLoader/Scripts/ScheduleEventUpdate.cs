﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using Axxess.OasisC.Domain;
using Axxess.OasisC.Repositories;
using Axxess.AgencyManagement.Domain;
using Axxess.AgencyManagement.Repositories;
using Axxess.Log.Repositories;
using Axxess.LookUp.Domain;
using Axxess.LookUp.Repositories;

using Excel;
using Kent.Boogaart.KBCsv;
using SubSonic.Repository;
using SubSonic.Query;

namespace Axxess.DataLoader.Domain
{
    public static class ScheduleEventUpdate
    {
        private static string error = Path.Combine(App.Root, string.Format("Files\\ScheduleEventUpdate-ErrorLog-{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string output = Path.Combine(App.Root, string.Format("Files\\ScheduleEventUpdate-EventLog-{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string log = Path.Combine(App.Root, string.Format("Files\\ScheduleEventUpdate-Datalog-{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter errorWriter = new StreamWriter(error, true))
            {
                using (TextWriter eventWriter = new StreamWriter(output, true))
                {
                    using (TextWriter logWriter = new StreamWriter(log, true))
                    {
                        try
                        {
                            logWriter.WriteLine("Start Time: " + DateTime.Now.ToString());
                            errorWriter.WriteLine("Start Time: " + DateTime.Now.ToString());
                            var agencies = Database.GetAllAgencies();
                            if (agencies != null && agencies.Count > 0)
                            {
                                foreach (var agency in agencies)
                                {
                                    if (agency != null && agency.Id.IsNotEmpty())
                                    {
                                        
                                        Console.WriteLine("STARTED : Agency Update For => [" + agency.Id + "]");
                                        logWriter.WriteLine("STARTED : Agency Update For => [" + agency.Id + "]");
                                        errorWriter.WriteLine("STARTED : Agency Update For => [" + agency.Id + "]");
                                        
                                        var patientEpisodeIds = Database.GetPatientEpisodeIdsOnly(agency.Id);
                                        if (patientEpisodeIds != null && patientEpisodeIds.Count > 0)
                                        {
                                            foreach (var patientEpisodeId in patientEpisodeIds)
                                            {
                                                if (patientEpisodeId.IsNotEmpty())
                                                {
                                                    var patientEpisode = Database.GetPatientEpisode(patientEpisodeId, agency.Id);
                                                    if (patientEpisode != null)
                                                    {
                                                        if (patientEpisode.Schedule.IsNotNullOrEmpty())
                                                        {
                                                            var scheduleEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                                                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                                                            {
                                                                foreach (var scheduleEvent in scheduleEvents)
                                                                {
                                                                    if (scheduleEvent != null)
                                                                    {
                                                                        if (scheduleEvent.IsBillable == true)
                                                                        {
                                                                            scheduleEvent.IsPayable = true;
                                                                        }
                                                                    }
                                                                }
                                                                patientEpisode.AgencyId = agency.Id;
                                                                patientEpisode.Schedule = scheduleEvents.ToXml();

                                                                if (Database.UpdatePatientEpisode(patientEpisode))
                                                                {
                                                                    Console.WriteLine("SUCCEEDED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                                    logWriter.WriteLine("SUCCEEDED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                                }
                                                                else
                                                                {
                                                                    Console.WriteLine("FAILED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                                    errorWriter.WriteLine("FAILED [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("NO SCHEDULE EVENTS FOR => [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                            logWriter.WriteLine("NO SCHEDULE EVENTS FOR => [" + patientEpisode.Id.ToString() + "," + patientEpisode.AgencyId.ToString() + "," + patientEpisode.PatientId.ToString() + "]");
                                                        }
                                                    }
                                                }
                                            }
                                            Console.WriteLine("FINISHED : Agency Update For => [" + agency.Id + "]");
                                            logWriter.WriteLine("FINISHED : Agency Update For => [" + agency.Id + "]");
                                            errorWriter.WriteLine("FINISHED : Agency Update For => [" + agency.Id + "]");
                                        }
                                        else
                                        {
                                            Console.WriteLine("NO PATIENT EPISODE : For Agency => [" + agency.Id + "]");
                                            logWriter.WriteLine("NO PATIENT EPISODE : For Agency => [" + agency.Id + "]");
                                            errorWriter.WriteLine("NO PATIENT EPISODE : For Agency => [" + agency.Id + "]");
                                        }
                                    }
                                }
                            }
                            logWriter.WriteLine("End Time: " + DateTime.Now.ToString());
                            errorWriter.WriteLine("End Time: " + DateTime.Now.ToString());
                        }
                        catch (Exception ex)
                        {
                            eventWriter.WriteLine("Time: " + DateTime.Now.ToString());
                            eventWriter.WriteLine(ex.StackTrace);
                        }
                    }
                }
            }
        }
    }
}
