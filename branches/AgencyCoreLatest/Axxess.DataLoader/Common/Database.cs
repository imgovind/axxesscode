﻿namespace Axxess.DataLoader
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.LookUp.Domain;

    using Axxess.AgencyManagement.Domain;

    using Axxess.OasisC.Domain;

    using SubSonic.Repository;
    using Axxess.OasisC.Enums;

    public static class Database
    {

        private static readonly SimpleRepository agencyManagementDatabase = new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None);
        private static readonly SimpleRepository oasisDatabase = new SimpleRepository("OasisCConnectionString", SimpleRepositoryOptions.None);
        private static readonly SimpleRepository lookupDatabase = new SimpleRepository("AxxessLookupConnectionString", SimpleRepositoryOptions.None);

        internal static List<Agency> GetAgencies()
        {
            return agencyManagementDatabase.All<Agency>().ToList();
        }

        internal static Agency GetAgency(Guid agencyId)
        {
            return agencyManagementDatabase.Single<Agency>(a => a.Id == agencyId);
        }

        internal static AgencyLocation GetAgencyLocation(Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyLocation>(a => a.AgencyId == agencyId);
        }

        internal static List<UserLocation> GetUserLocations(Guid agencyId, Guid userId)
        {
            return agencyManagementDatabase.Find<UserLocation>(l => l.AgencyId == agencyId && l.UserId == userId).ToList();
        }

        internal static List<AgencyLocation> GetAgencyLocations(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AgencyLocation>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<AgencyLocation> GetAgencyLocations()
        {
            return agencyManagementDatabase.All<AgencyLocation>().ToList();
        }

        internal static bool ExistPatient(Guid agencyId, string pid)
        {
            var result = agencyManagementDatabase.Find<Patient>(p => p.AgencyId == agencyId && p.PatientIdNumber == pid).FirstOrDefault();
            if (result == null)
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }

        internal static List<PhysicianOrder> GetOrders()
        {
            return agencyManagementDatabase.All<PhysicianOrder>().ToList();
        }

        internal static List<Message> GetUserMessages(Guid userId, Guid agencyId)
        {
            var messages = new List<Message>();

            string script = string.Format(
                "select * from messages where AgencyId = '{0}' and FromId = '{1}' order by Created asc",
                agencyId.ToString(),
                userId.ToString());

            using (var cmd = new FluentCommand<Message>(script))
            {
                messages = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new Message
                {
                    Id = reader.GetGuid("Id"),
                    Body = reader.GetString("Body"),
                    Subject = reader.GetString("Subject"),
                    MarkAsRead = reader.GetBoolean("MarkAsRead"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    FromName = reader.GetString("FromName"),
                    FromId = reader.GetGuidIncludeEmpty("FromId"),
                    Created = reader.GetDateTime("Created"),
                    AttachmentId = reader.GetGuidIncludeEmpty("AttachmentId"),
                    RecipientNames = reader.GetStringNullable("RecipientNames"),
                    CarbonCopyNames = reader.GetStringNullable("CarbonCopyNames"),
                    RecipientId = reader.GetGuidIncludeEmpty("RecipientId"),
                    PatientId = reader.GetGuidIncludeEmpty("PatientId"),
                    AgencyId = reader.GetGuidIncludeEmpty("AgencyId")
                }).AsList();
            }

            return messages;
        }

        internal static List<FaceToFaceEncounter> GetFaceToFaceEncounters()
        {
            return agencyManagementDatabase.All<FaceToFaceEncounter>().ToList();
        }

        internal static Patient GetPatient(Guid patientId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<Patient>(p => p.Id == patientId && p.AgencyId == agencyId);
        }

        internal static Patient GetPatientByMR(string mr, Guid agencyId)
        {
            return agencyManagementDatabase.Single<Patient>(p => p.PatientIdNumber == mr && p.AgencyId == agencyId);
        }

        internal static PatientAdmissionDate GetPatientAdminPeriod(Guid patientId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<PatientAdmissionDate>(p => p.Id == patientId && p.AgencyId == agencyId);
        }

        internal static User GetUser(Guid userId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<User>(p => p.Id == userId && p.AgencyId == agencyId);
        }

        internal static List<User> GetAdministrators()
        {
            return agencyManagementDatabase.Find<User>(u => u.Roles.StartsWith("1;") && u.IsDeprecated == false).ToList();
        }

        internal static List<User> GetUsers(Guid agencyId)
        {
            return agencyManagementDatabase.Find<User>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Rap> GetRaps(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Rap>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Final> GetFinals(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Final>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<Patient> GetPatients(Guid agencyId)
        {
            return agencyManagementDatabase.Find<Patient>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<AllergyProfile> GetAllergyProfiles(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AllergyProfile>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<MedicationProfile> GetMedicationProfiles(Guid agencyId)
        {
            return agencyManagementDatabase.Find<MedicationProfile>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PhysicianOrder> GetPhysicianOrders(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PhysicianOrder>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<FaceToFaceEncounter> GetFaceToFaceEncounters(Guid agencyId)
        {
            return agencyManagementDatabase.Find<FaceToFaceEncounter>(p => p.AgencyId == agencyId).ToList();
        }

        internal static bool UpdatePatient(Patient patient)
        {
            if (patient != null)
            {
                agencyManagementDatabase.Update<Patient>(patient);
                return true;
            }
            return false;
        }

        internal static AgencyPhysician GetPhysician(string npi, Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyPhysician>(p => p.NPI == npi && p.AgencyId == agencyId);
        }

        internal static AgencyPhysician GetPhysician(Guid physicianId, Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyPhysician>(p => p.Id == physicianId && p.AgencyId == agencyId);
        }

        internal static AgencyPhysician GetPhysician(string npi) 
        {
            return agencyManagementDatabase.Single<AgencyPhysician>(p => p.NPI == npi);
        }

        internal static List<AgencyPhysician> GetPhysicians(Guid agencyId)
        {
            return agencyManagementDatabase.Find<AgencyPhysician>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientEpisode> GetEpisodes(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PatientEpisode>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientAdmissionDate> GetPatientAdmissionDates(Guid agencyId)
        {
            return agencyManagementDatabase.Find<PatientAdmissionDate>(p => p.AgencyId == agencyId).ToList();
        }

        internal static List<PatientVisitNote> GetPatientVisitNotes(Guid agencyId)
        {
            var list = new List<PatientVisitNote>();
            var sql = string.Format("select `Id`, `AgencyId`, `PatientId`, `EpisodeId`, `WoundNote` from patientvisitnotes where agencyid = '{0}' and IsWoundCare = 1 and IsDeprecated = 0;", agencyId);
            using (var cmd = new FluentCommand<PatientVisitNote>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    WoundNote = reader.GetStringNullable("WoundNote")
                })
                .AsList();
            }
            return list;
        }

        internal static List<PatientVisitNote> GetPatientVisitNotes(Guid agencyId, DateTime startTime, DateTime endTime)
        {
            return agencyManagementDatabase.Find<PatientVisitNote>(p => p.AgencyId == agencyId && p.Modified >= startTime && p.Modified <= endTime).OrderByDescending(p => p.Modified).ToList();
        }

        internal static Npi GetNpiData(string npi)
        {
            return lookupDatabase.Single<Npi>(p => p.Id == npi);
        }

        internal static List<PlanofCare> GetPlanofCareByAgencyId(Guid agencyId)
        {
            return oasisDatabase.Find<PlanofCare>(a => a.AgencyId == agencyId).ToList();
        }

        internal static List<T> GetAssessments<T>(Guid agencyId) where T : Assessment, new()
        {
            return oasisDatabase.Find<T>(a => a.AgencyId == agencyId).ToList();
            //var list = new List<T>();
            //var sql = string.Format("select `Id`, `AgencyId`, `PatientId`, `EpisodeId`, `OasisData` from `{0}s` where agencyid = '{1}' and IsDeprecated = 0;", typeof(T).Name.ToLower(), agencyId);
            //using (var cmd = new FluentCommand<T>(sql))
            //{
            //    list = cmd.SetConnection("OasisCConnectionString")
            //    .AddGuid("agencyid", agencyId)
            //    .SetMap(reader => new T
            //    {
            //        Id = reader.GetGuid("Id"),
            //        AgencyId = reader.GetGuid("AgencyId"),
            //        PatientId = reader.GetGuid("PatientId"),
            //        EpisodeId = reader.GetGuid("EpisodeId"),
            //        SubmissionFormat = reader.GetStringNullable("SubmissionFormat"),
            //        OasisData = reader.GetStringNullable("OasisData")
            //    })
            //    .AsList();
            //}
            //return list;
        }

        internal static IList<Agency> GetAllAgencies()
        {
            var agencies = agencyManagementDatabase.All<Agency>().ToList();
            if (agencies != null && agencies.Count > 0)
            {
                agencies.ForEach(a =>
                {
                    a.MainLocation = GetMainLocation(a.Id);
                });
            }
            return agencies.OrderBy(a => a.Name).ToList();
        }

        internal static AgencyLocation GetMainLocation(Guid agencyId)
        {
            return agencyManagementDatabase.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true && l.IsDeprecated == false);
        }

        internal static PatientEpisode GetPatientEpisode(Guid Id, Guid AgencyId, bool isDeprecated)
        {
            return agencyManagementDatabase.Single<PatientEpisode>(e => e.Id == Id && e.AgencyId == AgencyId);
        }

        internal static bool UpdatePatientEpisode(PatientEpisode input)
        {
            bool result = default(bool);
            var script = string.Format
                (
                    "UPDATE agencymanagement.patientepisodes as pe " +
                    "SET pe.`Schedule` = @schedule " +
                    "WHERE pe.Id = '{0}' " +
                    "AND pe.AgencyId = '{1}' " +
                    "AND pe.PatientId = '{2}' "
                    ,input.Id, input.AgencyId, input.PatientId
                 );
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                if (cmd.SetConnection("AgencyManagementConnectionString").AddString("schedule", input.Schedule).AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        internal static PatientEpisode GetPatientEpisode(Guid Id, Guid AgencyId)
        {
            PatientEpisode result = null;
            var script = string.Format
                (
                    "SELECT pe.Id as patientEpisodeId, pe.`Schedule` as patientEpisodeSchedule, pe.PatientId as patientEpisodePatientId " +
                    "FROM agencymanagement.patientepisodes as pe " +
                    "WHERE pe.Id = '{0}' " +
                    "AND pe.AgencyId = '{1}' "
                    , Id, AgencyId
                );
            result = new FluentCommand<PatientEpisode>(script)
                    .SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new PatientEpisode
                    {
                        Id = reader.GetGuid("patientEpisodeId"),
                        Schedule = reader.GetStringNullable("patientEpisodeSchedule"),
                        PatientId = reader.GetGuid("patientEpisodePatientId")
                    })
                    .AsSingle();
            return result;
        }

        internal static IList<Guid> GetPatientEpisodeIdsOnly(Guid agencyId)
        {
            List<Guid> result = null;
            var script = string.Format
                (
                    "SELECT pe.Id as patientEpisodeId " +
                    "FROM agencymanagement.patientepisodes as pe " +
                    "WHERE pe.AgencyId = '{0}' "
                    , agencyId
                );

            result = new FluentCommand<Guid>(script)
                    .SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new Guid(reader.GetString("patientEpisodeId")))
                    .AsList();
            return result;
        }

        internal static IList<Guid> GetAgencyIdsOnly()
        {
            List<Guid> result = null;
            var script = string.Format
                (
                    "SELECT ag.Id as agencyId " +
                    "FROM  agencymanagement.agencies as ag "
                );

            result = new FluentCommand<Guid>(script)
                    .SetConnection("AgencyManagementConnectionString")
                    .SetMap(reader => new Guid(reader.GetString("agencyId")))
                    .AsList();
            return result;
        }

        internal static List<T> GetAssessmentsBetween<T>(Guid agencyId, DateTime start, DateTime end) where T : Assessment, new()
        {
            var list = new List<T>();
            var sql = new StringBuilder("SELECT `Id`, `AgencyId`, `PatientId`, `EpisodeId`, `OasisData` ")
            .AppendFormat("FROM `{0}s` WHERE agencyid = '{1}' AND IsDeprecated = 0 ", typeof(T).Name.ToLower(), agencyId)
            .Append("AND AssessmentDate BETWEEN @startdate AND @enddate;")
            .ToString();
            using (var cmd = new FluentCommand<T>(sql))
            {
                list = cmd.SetConnection("OasisCConnectionString")
                .AddDateTime("startdate", start)
                .AddDateTime("enddate", end)
                .SetMap(reader => new T
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    OasisData = reader.GetStringNullable("OasisData")
                })
                .AsList();
            }
            return list;
        }

        internal static Assessment GetAssessment<T>(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId) where T : Assessment, new()
        {
            return oasisDatabase.Single<T>(a => a.EpisodeId == episodeId && a.PatientId == patientId && a.Id == assessmentId && a.AgencyId == agencyId);
            //return oasisDatabase.Find<RecertificationAssessment>(a => a.AgencyId == agencyId).ToList();
        }

        internal static bool Add<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool AddForLookup<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                lookupDatabase.Add<T>(item);
                return true;
            }
            return false;
        }

        internal static bool Update<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                agencyManagementDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static bool UpdateMany<T>(IEnumerable<T> items) where T : class, new()
        {
            if (items != null)
            {
                agencyManagementDatabase.UpdateMany<T>(items);
                return true;
            }
            return false;
        }

        internal static bool UpdateForLookup<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                lookupDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static bool UpdateForOasisC<T>(T item) where T : class, new()
        {
            if (item != null)
            {
                oasisDatabase.Update<T>(item);
                return true;
            }
            return false;
        }

        internal static List<CBSACode> GetCbsaCodes()
        {
            return lookupDatabase.All<CBSACode>().ToList();
        }

        internal static List<DisciplineTask> GetDisciplineTasks()
        {
            return lookupDatabase.All<DisciplineTask>().ToList();
        }

        internal static List<User> GetUsers()
        {
            return agencyManagementDatabase.All<User>().ToList();
        }

        internal static List<PatientEpisode> GetPatientEpisodes()
        {
            return agencyManagementDatabase.All<PatientEpisode>().ToList();
        }

        internal static void AddMany<T>(IEnumerable<T> items) where T : class, new()
        {
            agencyManagementDatabase.AddMany<T>(items);
        }

        internal static List<ManagedClaim> GetPaidManagedClaims(Guid agencyId)
        {
            var list = new List<ManagedClaim>();
            var sql = @"SELECT `Id`, `AgencyId`, `PatientId`, `Payment`, `PrimaryInsuranceId`, `PaymentDate` " +
                       "FROM managedclaims WHERE agencyid = @agencyid AND `Status` = 3030;";
            using (var cmd = new FluentCommand<ManagedClaim>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new ManagedClaim
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    PatientId = reader.GetGuid("PatientId"),
                    Payment = reader.GetDouble("Payment"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    PrimaryInsuranceId = reader.GetInt("PrimaryInsuranceId")
                })
                .AsList();
            }
            return list;
        }

        internal static List<ManagedClaimPayment> GetManagedClaimPayments()
        {
            return agencyManagementDatabase.Find<ManagedClaimPayment>(payment => payment.IsDeprecated == false).ToList();
        }

        internal static List<T> GetAssessmentsWhereStatusZero<T>(Guid agencyId) where T : Assessment, new()
        {
            return oasisDatabase.Find<T>(a => a.AgencyId == agencyId && a.Status == 0).ToList();
        }

        internal static ScheduleEvent GetScheduleEventOnly(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            ScheduleEvent scheduleEvent = null;
            if (!agencyId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var episode = agencyManagementDatabase.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                {
                    scheduleEvent = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId == eventId).FirstOrDefault();
                    if (scheduleEvent != null)
                    {
                        scheduleEvent.StartDate = episode.StartDate;
                        scheduleEvent.EndDate = episode.EndDate;
                        scheduleEvent.VisitDate = scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? scheduleEvent.VisitDate : scheduleEvent.EventDate;
                    }
                }
            }
            return scheduleEvent;
        }

        //internal static List<T> GetAssessmentsWhereStatusZero<T>(Guid agencyId) where T : Assessment, new()
        //{
        //    var list = new List<T>();
        //    var sql = string.Format("select * from `{0}s` where agencyid = '{1}' and status = 0;", typeof(T).Name.ToLower(), agencyId);
        //    using (var cmd = new FluentCommand<T>(sql))
        //    {
        //        list = cmd.SetConnection("OasisCConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .SetMap(reader => new T
        //        {
        //            Id = reader.GetGuid("Id"),
        //            AgencyId = reader.GetGuid("AgencyId"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            EpisodeId = reader.GetGuid("EpisodeId"),
        //            SubmissionFormat = reader.GetStringNullable("SubmissionFormat"),
        //            OasisData = reader.GetStringNullable("OasisData"),
                    
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        //internal static List<Assessment> GetAssessmentWhereStatusEqualZero(Guid agencyId, string oasisTable)
        //{
        //    var assessments = new List<Assessment>();

        //    string script = string.Format("select * from {0} where AgencyId = '{1}' and status = 0", oasisTable, agencyId.ToString());

        //    using (var cmd = new FluentCommand<Message>(script))
        //    {
        //        assessments = cmd.SetConnection("OasisCConnectionString")
        //        .SetMap(reader => new Assessment
        //        {
        //            Id = reader.GetGuid("Id"),
        //            Body = reader.GetString("Body"),
        //            Subject = reader.GetString("Subject"),
        //            MarkAsRead = reader.GetBoolean("MarkAsRead"),
        //            IsDeprecated = reader.GetBoolean("IsDeprecated"),
        //            FromName = reader.GetString("FromName"),
        //            FromId = reader.GetGuidIncludeEmpty("FromId"),
        //            Created = reader.GetDateTime("Created"),
        //            AttachmentId = reader.GetGuidIncludeEmpty("AttachmentId"),
        //            RecipientNames = reader.GetStringNullable("RecipientNames"),
        //            CarbonCopyNames = reader.GetStringNullable("CarbonCopyNames"),
        //            RecipientId = reader.GetGuidIncludeEmpty("RecipientId"),
        //            PatientId = reader.GetGuidIncludeEmpty("PatientId"),
        //            AgencyId = reader.GetGuidIncludeEmpty("AgencyId")
        //        }).AsList();
        //    }

        //    return messages;
        //}
    }
}
