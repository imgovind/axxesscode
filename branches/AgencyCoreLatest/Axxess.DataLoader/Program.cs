﻿namespace Axxess.DataLoader
{
    using System;
    using System.Web;
    using System.Collections.Generic;
    using System.Diagnostics;

    using StructureMap;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.DataLoader.Domain;

    using SubSonic.Repository;
    using Axxess.LookUp.Domain;
    using System.Net;
    using System.IO;

    using Axxess.Core.Extension;

    class Program
    {
        static void Main(string[] args)
        {
            #region frequent scripts
            //Axxess20Column.Run(new Guid("9a1f6b66-3160-460d-953d-07f5f494a8c0"), new Guid("a826632e-d2be-427e-a039-37704daad6fc"));
            //AxxessPhysicianNew.Run(new Guid("9a1f6b66-3160-460d-953d-07f5f494a8c0"));


            //VisiTrakACTIVEPatientScript.Run(new Guid("8ce89c8b-ce09-48a2-98e9-3efd0a0c639b"), new Guid("46f68e50-c6d9-4535-8aa3-f7b377290a16"));
            //VisiTrakDISCHARGEDPatientScript.Run(new Guid("8ce89c8b-ce09-48a2-98e9-3efd0a0c639b"), new Guid("46f68e50-c6d9-4535-8aa3-f7b377290a16"));
            //VisiTrakPhysicianScript.Run(new Guid("8ce89c8b-ce09-48a2-98e9-3efd0a0c639b"));
            //Axxess20Column2.Run(new Guid("5797494b-6643-4604-8338-86da15bb972a"), new Guid("3b4ae44b-13af-4267-a314-5e3c819d2e6a"));
            //AxxessPhysicianNew2.Run(new Guid("3eb72c67-2e97-4045-a040-70bfe0e04fc4"));
            //Axxess40Column.Run(new Guid("3eb72c67-2e97-4045-a040-70bfe0e04fc4"), new Guid("059f5e4c-ec9a-4a32-8c54-793619146956"));

            //HttpWebResponse response = null;
            try
            {
                //Uri uri = new Uri("http://dev.accounts.axxessweb.com/api/auth?emailaddress=andrew.olowu@gmail.com&password=testing22", UriKind.Absolute);
                //HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
                //httpWebRequest.Method = "POST";
                //httpWebRequest.ContentType = "application/json";
                //httpWebRequest.ContentLength = 5;

                //ServicePointManager.ServerCertificateValidationCallback = ((sender, cert, chain, sslPolicyErrors) => true);

                //response = (HttpWebResponse)httpWebRequest.GetResponse();
                //if (response != null)
                //{
                //    if (response.StatusCode == HttpStatusCode.OK)
                //    {
                //        using (var responseStream = new StreamReader(response.GetResponseStream()))
                //        {
                //            var jsonResult = responseStream.ReadToEnd().FromJson<JsonViewData>();
                //            if (jsonResult != null)
                //            {
                //                if (jsonResult.isSuccessful)
                //                {
                //                    Console.WriteLine("Successful");
                //                }
                //                else
                //                {
                //                    Console.WriteLine("Failure");
                //                }
                //            }
                //        }
                //    }
                //}

                WebClient client = new WebClient();

                // Set the header so it knows we are sending JSON
                client.Headers[HttpRequestHeader.ContentType] = "application/json";

                string serialisedData = string.Empty.ToJson();

                // Make the request
                var response = client.UploadString("http://dev.accounts.axxessweb.com/api/auth?emailaddress=andrew.olowu@gmail.com&password=testing22", serialisedData);
                if (response != null)
                {
                    
                }

            }
            catch (WebException webException)
            {
                Console.WriteLine(webException.ToString());
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
            }
            finally
            {
            }

            #endregion


            #region All The Scripts
            //OasisSubmissionBranchIdScript.Run(new Guid("aa6a38f2-364b-4d8e-b418-29bff888be51"));
            //FixOasisSubmissionBranchIdScript.Run(new Guid("f5e28cb7-4b72-4332-a98a-1e748dc95c21"));

            //FixOasisSubmissionBranchIdScript.Run(new Guid("f5e28cb7-4b72-4332-a98a-1e748dc95c21"));
            //AgencyAddAdjustmentsScript.Run();
            //UpdatePaymentsScript.Run();

            //AllegenyIntake.Run(new Guid("f3c587a1-6303-4ebf-ac16-403ee70b9aa7"), new Guid("efe80c4b-cd75-45d7-98ef-8be524263bd7"));

            //UsingSimpleRepo();
            //UsingFluentAdoNet();
            //Alura.Run("fc7c6c62-eee6-4186-84ba-25105ebca2f2", "20882903-4608-4c3f-993c-82d2d5132fbf");
            //AluraPhysician.Run(new Guid("f43c30e2-08bf-4e22-80cc-ecf4097ffdac"));

            //IDatabaseAdministration databaseAdministration = new MySqlAdministration("server=10.0.5.61;uid=appdata;pwd=uKKn4ov2q3;database=agencymanagement;Max Pool Size=5;");
            //databaseAdministration.FilePath = @"D:\Backup\";
            //databaseAdministration.Backup(false);

            //databaseAdministration = new MySqlAdministration("server=10.0.5.61;uid=appdata;pwd=uKKn4ov2q3;database=oasisc;Max Pool Size=5;");
            //databaseAdministration.FilePath = @"D:\Backup\";
            //databaseAdministration.Backup(false);

            //databaseAdministration = new MySqlAdministration("server=10.0.5.61;uid=appdata;pwd=uKKn4ov2q3;database=axxessmembership;Max Pool Size=5;");
            //databaseAdministration.FilePath = @"D:\Backup\";
            //databaseAdministration.Backup(false);
            //AloraHealthIntake.Run(new Guid("a84be1d0-d5f7-42f5-95ee-d10d255cb3f1"), new Guid("c87a57b0-24bc-4d26-a126-f8dc5e407075"));
            //AssetScript.Run(new Guid("153e863d-669a-4edd-851a-4088890fd9a1"));
            //Wizard.Run(new Guid("d544542f-75da-4ade-9c84-5e4aa4d2fb59"), new Guid("411ac04a-3868-40b4-99a2-bb4d18021f03"));
            //WizardPhysician.Run(new Guid("a4f28d28-dc0f-44bb-9174-50daf9193eb5"));
            //AssetScript.Run(new Guid("153e863d-669a-4edd-851a-4088890fd9a1"));
            //VestaScript.Run("565e9603-d887-407b-8404-ffe9a8b7b92f", "145bd87c-9e3e-47d9-8bb0-1130620d9422");
            //ColoradoScript.Run();
            //ArizonaScript.Run();
            //MinnesotaScript.Run();
            //CaliforniaHHAScript.Run();
            //SponsorScript.Run();
            //var stopWatch = new Stopwatch();
            //stopWatch.Start();
            //CradleMedPointScript.Run(new Guid("5d50ae56-5e62-46cf-9b7f-40f5ca8202b3"), new Guid("4bab3fe9-51dc-4e90-9262-32575e505349"));
            //KinnserScript.Run(new Guid("e9a50d43-6465-474e-a3cf-bb8e926789b4"), new Guid("31904bd4-cbfe-4612-b36f-60a5bca23ed1"));
            //KinnserPhysician.Run(new Guid("e9a50d43-6465-474e-a3cf-bb8e926789b4"));
            //KinnserCsvScript.Run(new Guid("3a1b0087-1de8-4235-aa0e-97f7b67f022b"), new Guid("b445bad7-a293-4bc8-9d3e-88dfa62baee3"));
            //AxxessScript.Run("24711294-bff1-4f6b-ae58-5fa2bbeae342", "c060d570-deb6-45f1-a7cc-7edc127b528d");
            //MedicationProfileScript.Run(new Guid("6d80ae1f-410d-408f-b342-051e956b037f"));
            //GenericExcelScript.Run(new Guid("8bf20512-359a-4fe5-9438-966dfaa5a34c"), new Guid("8885e4f3-36ce-4a26-a08a-0b691e89ccbe"));
            //SynergyScript.Run(new Guid("726f5594-8754-42d9-a421-d52d35d0fa82"), new Guid("e4834fa2-7c96-4879-80d1-e2b632f320ce"));
            //VisiTrakScript.Run(new Guid("1c099985-2664-4fa3-bdaf-349ecda4b263"), new Guid("18dae740-31fb-4547-b2fa-6e50b556f13d"));





            //GenericExcelScript3.Run(new Guid("271a571f-3d00-4044-932a-d164b9e97a44"), new Guid("1950f21c-15fa-4174-935f-85f33314f8bc"));

            //VisiTrakScript3.Run(new Guid("908c335b-e0cb-406a-916e-ecdd92d8379a"), new Guid("2de44ff4-b71a-4bae-9611-82e1321ba192"));


            //VisitrackExcelPhysician2.Run(new Guid("271a571f-3d00-4044-932a-d164b9e97a44"));
            //CopyLogs.Run();
            //SynergyThreeScript.Run(new Guid("cd7ca5bb-44bc-4390-bb4e-a2dfa80ca1d2"), new Guid("3e76f5b7-6af9-4f95-8a38-b09c75bd2d9a"));

            //SynergyTwoScript.Run(new Guid("824e13c1-7cc6-43f1-aa5c-81190c45cb54"), new Guid("7e59d3dc-1e11-4957-ab81-f4f83a49df9e"));
            //SynergyPhysicianScript.Run(new Guid("824e13c1-7cc6-43f1-aa5c-81190c45cb54"));
            //SynergyTwoPhysicianScript.Run(new Guid("ce218bb3-7b0e-41e3-9842-59e9947d1000"));
            //HealthMedXPhysicianScript.Run(new Guid("abc2fbb2-807e-4dd0-987f-608e5d75558a"));
            //HealthCareFirstScript.Run(new Guid("93a811f1-ffda-43f9-a844-b2a0f9db81f4"), new Guid("9a41ef5a-f55c-4f98-b1de-84a094b91354"));
            //Icd9Script.Run(true);
            //Icd9Script.Run(false);
            //GenericExcelTwoScript.Run(new Guid("4a318889-50b7-4ba2-90d2-bb259a663411"), new Guid("e4556fd3-e9e9-42e3-a801-06ddd9da77ee"));
            //WageIndexScript.Run();
            //CradleCsvScript.Run("15260aab-b081-4318-8e56-6a6b879f32ce", "987dcc9a-053c-4bf3-b372-28b4859c456a");
            //OasisSubmissionBranchIdScript.Run(new Guid("ea9c9ab1-d667-4c1d-8d74-1436344666a8"));
            //HHCenterScript.Run(new Guid("9d5afae5-6127-4c58-b457-c5185273ad4a"), new Guid("044ec64d-118e-43df-85f1-9c772a004c86"));
            //MjsOneScript.Run(new Guid("5dd0287f-6499-4a8a-98f4-a7ff18f8567a"), new Guid("e8460caf-8a63-42b2-89e7-058bf9b8be15"));
            //MsjOnePhysicianScript.Run(new Guid("5dd0287f-6499-4a8a-98f4-a7ff18f8567a"));
            //IgeaHomeHealthIntake.Run("5ccbf159-dc60-4b5a-a6aa-ade0ca60a1be", "80404746-3140-46c9-9ea8-4c68ee2bdb47");
            //PermissionScript.Run();
            //AgencySupplies.Run();
            //PhysicianOrders.Run();
            //AgencyMergeScript.Run("4da8bb1c-0b75-415c-862d-c36ff673619d", new List<string> { "949dfac1-8bdb-4097-868b-a7729cc991b4", "c65edc26-12be-43d1-b71c-5074d16213e8" });
            //AxxessScript4.Run(new Guid("01d5f8ea-3713-4d5a-8c9e-3c539fb574fa"), new Guid("9b0f3104-5313-4cc2-9dee-277904313071"));
            //int i = PatinetAddressScript.Run(new Guid("0bd5ffc9-61c1-495a-9df5-7b93885ab30d"));
            //AxxessScript3.Run(new Guid("10655a61-5d3d-440f-bf13-522111069f8e"), new Guid("2b2d5dd3-1cd1-42b1-8da6-de0cc13011ce"));
            //AxxessScript2.Run(new Guid("e9a50d43-6465-474e-a3cf-bb8e926789b4"), new Guid("31904bd4-cbfe-4612-b36f-60a5bca23ed1"), new Guid("ee56f594-fb77-4d1c-9c2c-b04317c731e1"));
            //SchduleLoadScript.Run(new Guid("62b904fa-b38f-4686-b2e4-d748fa129c50"));
            //SchduleLoadScript.Run(new Guid("d0307ff6-9d69-435a-a411-f737b1980bfb"));
            //SchduleLoadScript.Run(new Guid("86493a0e-0fc0-4260-998b-442dd17f1a6e"));
            //PhysicianCsvScript.Run(new Guid("7596d33b-a56a-49e0-bc27-fe11d76fffec"));
            //SansioScript.Run("bada3b4f-2e32-4c1b-9eea-55a080ffa627", "28b948b1-49e1-4557-b1ed-91abe32f0de6");
            //SansioPhysicansScript.Run("bada3b4f-2e32-4c1b-9eea-55a080ffa627");
            //Genie.Run("6aa2ce5c-dede-4fc8-93cd-7cf64bb520f5", "00956c1b-3e09-40c7-a790-a969e35884e3");
            //SynergyTwoRow.Run("b93c9148-a694-4cd5-9f9c-ef6ea62e1d98", "4872c996-709f-4af4-b9bd-45eda86b4800");
            //GeniePhysician.Run(new Guid("69d47957-7a55-41a5-84c0-f0002f9deca2"));
            //DataSoftLogic.Run("8ef53ea3-ca70-4a4c-b56d-5f1c2f366e26", "cdd48dbd-bfb5-4d17-bfc8-37d7b433fb5b");
            //DataSoftLogicTwoRow.Run("8ef53ea3-ca70-4a4c-b56d-5f1c2f366e26", "cdd48dbd-bfb5-4d17-bfc8-37d7b433fb5b");
            //SynergyFourRows.Run(new Guid("b5c94ce3-b731-43d9-b09f-399b136771a4"), new Guid("a0604cb1-87ba-4243-a29c-3f279ccac2f0"));
            //LoadAgencyLocationChargeRates.Run();
            //SynergyThree.Run("b5c94ce3-b731-43d9-b09f-399b136771a4", "a0604cb1-87ba-4243-a29c-3f279ccac2f0");
            //LoadAgencyLocationChargeRates.RunToUpdateTheDisciplineTasks();
            //AgencyAddUploadTypeScript.Run();
            //OasisSubmissionIdScript.Run(new Guid("824e13c1-7cc6-43f1-aa5c-81190c45cb54"), new List<string> { "1011602" }, "IL1011602");
            //MigrateToMessageFolders2.Run();
            //VisitrackExcelPhysician.Run(new Guid("45292c51-da6c-44f8-963d-87744f364345"));
            //LoadSystemMessage.Run();

            //GenericExcelPatientScript.Run(new Guid("cd7ca5bb-44bc-4390-bb4e-a2dfa80ca1d2"), new Guid("3e76f5b7-6af9-4f95-8a38-b09c75bd2d9a"));
            //GenericExcelPatientScriptWiseCounty.Run(new Guid("45292c51-da6c-44f8-963d-87744f364345"), new Guid("9aa60845-0217-480e-b07b-fd205580ba56"));
            #endregion

            Console.WriteLine("[Done. Press any key to exit...]");
            Console.ReadLine();
        }
    }
}
