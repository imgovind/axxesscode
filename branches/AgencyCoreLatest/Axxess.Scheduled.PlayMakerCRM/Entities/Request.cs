﻿namespace Axxess.Scheduled.PlayMakerCRM
{
    using System.Xml.Serialization;
    using System.Xml.Schema;

    public class Data : BaseApiEntity
    {
        [XmlElement("Request", Form = XmlSchemaForm.Unqualified)]
        public DataRequest[] Request { get; set; }

        [XmlElement("Response", Form = XmlSchemaForm.Unqualified)]
        public DataResponse[] Response { get; set; }

        [XmlAttribute()]
        public string Pwd { get; set; }

        [XmlAttribute()]
        public string Key { get; set; }
    }

    public class DataRequest : BaseApiEntity
    {
        [XmlElement("Filters", Form = XmlSchemaForm.Unqualified)]
        public DataRequestFilters[] Filters { get; set; }

        [XmlElement("Records", Form = XmlSchemaForm.Unqualified)]
        public DataRequestRecords[] Records { get; set; }

        [XmlAttribute()]
        public string Name { get; set; }

        [XmlAttribute()]
        public string Object { get; set; }

        [XmlAttribute()]
        public string Method { get; set; }

        [XmlAttribute()]
        public string Process { get; set; }

        [XmlAttribute()]
        public string CallBack { get; set; }

        [XmlAttribute()]
        public string Sandbox { get; set; }

        [XmlAttribute()]
        public string Test { get; set; }
    }

    public class DataRequestFilters : BaseApiEntity
    {
        [XmlElement("Filter", Form = XmlSchemaForm.Unqualified)]
        public DataRequestFiltersFilter[] Filters { get; set; }

        [XmlAttribute()]
        public string Name { get; set; }
    }

    public class DataRequestFiltersFilter : BaseApiEntity
    {
        [XmlAttribute()]
        public string Name { get; set; }

        [XmlAttribute()]
        public string Key { get; set; }

        [XmlAttribute()]
        public string Compare { get; set; }

        [XmlAttribute()]
        public string Value { get; set; }
    }

    public class DataRequestRecords : BaseApiEntity
    {
        [XmlElement("Record", Form = XmlSchemaForm.Unqualified)]
        public DataRequestRecordsRecord[] Record { get; set; }
        
        [XmlAttribute()]
        public string Name { get; set; }
    }

    public class DataRequestRecordsRecord : BaseApiEntity
    {
        [XmlElement("Fields", Form = XmlSchemaForm.Unqualified)]
        public DataRequestRecordsRecordFields[] Fields { get; set; }
        
        [XmlAttribute()]
        public string Name { get; set; }
    }

    public class DataRequestRecordsRecordFields : BaseApiEntity
    {
        [XmlElement("Field", Form = XmlSchemaForm.Unqualified)]
        public DataRequestRecordsRecordFieldsField[] Field { get; set; }

        [XmlAttribute()]
        public string Name { get; set; }
    }

    public class DataRequestRecordsRecordFieldsField : BaseApiEntity
    {
        [XmlAttribute()]
        public string Key { get; set; }

        [XmlAttribute()]
        public string Value { get; set; }

        [XmlAttribute()]
        public string Name { get; set; }
    }

    public class DataResponse : BaseApiEntity
    {
        [XmlArrayAttribute(Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItemAttribute("Entries", typeof(DataResponseLogEntriesEntry[]), Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        [XmlArrayItemAttribute("Entry", typeof(DataResponseLogEntriesEntry), Form = XmlSchemaForm.Unqualified, IsNullable = false, NestingLevel = 1)]
        public DataResponseLogEntriesEntry[][] Log { get; set; }

        [XmlAttribute()]
        public string ErrorCode { get; set; }

        [XmlAttribute()]
        public string ErrorMessage { get; set; }
    }

    public class DataResponseLogEntriesEntry : BaseApiEntity
    {
        [XmlAttribute()]
        public string Message { get; set; }
        
        [XmlAttribute()]
        public string TimeStamp { get; set; }

        [XmlAttribute()]
        public string LogLevel { get; set; }

        [XmlAttribute()]
        public string Node { get; set; }
        
        [XmlAttribute()]
        public string NodeName { get; set; }
        
        [XmlAttribute()]
        public string Attributes { get; set; }

        [XmlAttribute()]
        public string Data { get; set; }

        [XmlAttribute()]
        public string Result { get; set; }
    }

    public class NewDataSet : BaseApiEntity
    {
        [XmlElement("Data")]
        public Data[] Items { get; set; }
    }

}
