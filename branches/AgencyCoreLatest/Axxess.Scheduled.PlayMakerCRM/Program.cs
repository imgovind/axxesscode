﻿namespace Axxess.Scheduled.PlayMakerCRM
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Configuration;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    class Program
    {
        static void Main(string[] args)
        {
            //Accounts = facilities;
            //CreateAccountXML(new Guid("62b904fa-b38f-4686-b2e4-d748fa129c50"));
            //Contacts = physicians;
            //CreateContactXML(new Guid("62b904fa-b38f-4686-b2e4-d748fa129c50"));
            //Referrals = referrals
            CreateReferralXML(new Guid("62b904fa-b38f-4686-b2e4-d748fa129c50"));
            //PlayMakerService.SendDataRequest(data);
        }

        private static string CreateReferralXML(Guid agencyId)
        {
            var result = string.Empty;
            var agencyReferrals = Database.GetAgencyReferals(agencyId);
            if (agencyReferrals != null && agencyReferrals.Count > 0)
            {
                var data = PlayMakerService.CreateDataRequest(true, "referrals", "Axxess Referrals Processing");
                var recordList = new List<DataRequestRecordsRecord>();
                agencyReferrals.ForEach(referral =>
                {
                    var record = new DataRequestRecordsRecord
                    {
                        Fields = new DataRequestRecordsRecordFields[1] {
                            new DataRequestRecordsRecordFields() 
                        }
                    };

                    var fieldList = new List<DataRequestRecordsRecordFieldsField>();

                    fieldList.AddField("Referral ID", "Referral ID", referral.Id.ToString());
                    fieldList.AddField("Marketer", "Marketer", string.Empty);
                    fieldList.AddField("Marketer ID", "Marketer ID", string.Empty);
                    fieldList.AddField("Contact", "Contact", string.Empty);
                    fieldList.AddField("Contact ID", "Contact ID", string.Empty);
                    fieldList.AddField("Account", "Account", string.Empty);
                    fieldList.AddField("Account ID", "Account ID", string.Empty);
                    fieldList.AddField("Date Entered", "Date Entered", string.Empty);
                    fieldList.AddField("Notes", "Notes", string.Empty);
                    fieldList.AddField("Referral Status", "Referral Status", string.Empty);
                    fieldList.AddField("Insurance Type", "Insurance Type", string.Empty);
                    fieldList.AddField("Insurance Plan", "Insurance Plan", string.Empty);
                    fieldList.AddField("Start of Care Date", "Start of Care Date", string.Empty);
                    fieldList.AddField("Attending Physician Name", "Attending Physician Name", string.Empty);
                    fieldList.AddField("Attending Physician NPI", "Attending Physician NPI", string.Empty);
                    fieldList.AddField("Non-Admit Date", "Non-Admit Date", string.Empty);
                    fieldList.AddField("Non-Admit Reason", "Non-Admit Reason", string.Empty);
                    fieldList.AddField("Non-Admit Reason Details", "Non-Admit Reason Details", string.Empty);
                    fieldList.AddField("Office", "Office", string.Empty);
                    fieldList.AddField("Business Unit", "Business Unit", string.Empty);
                    fieldList.AddField("Referral Source", "Referral Source", string.Empty);
                    fieldList.AddField("Referral Source Category", "Referral Source Category", string.Empty);
                    fieldList.AddField("Patient Name", "Patient Name", string.Empty);
                    fieldList.AddField("Patient ID", "Patient ID", string.Empty);
                    fieldList.AddField("Discharge Date", "Discharge Date", string.Empty);
                    fieldList.AddField("Referral Source NPI", "Referral Source NPI", string.Empty);
                    

                    record.Fields[0].Field = fieldList.ToArray();
                    recordList.Add(record);
                });

                data.AddRecord(recordList.ToArray());
                result = data.ToXml();
                var directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestAgency_Sample_Contact_Message.XML");
                using (var textWriter = new StreamWriter(directory, false))
                {
                    textWriter.Write(result);
                }
            }
            return result;
        }

        private static string CreateContactXML(Guid agencyId)
        {
            var result = string.Empty;
            var agencyPhysicians = Database.GetAgencyPhysicians(agencyId);
            if (agencyPhysicians != null && agencyPhysicians.Count > 0)
            {
                //Accounts = facilities;
                //Contacts = physicians;
                //Referrals = referrals
                var data = PlayMakerService.CreateDataRequest(true, "contacts", "Axxess Contacts Processing");
                var recordList = new List<DataRequestRecordsRecord>();
                agencyPhysicians.ForEach(physician =>
                {
                    var record = new DataRequestRecordsRecord
                    {
                        Fields = new DataRequestRecordsRecordFields[1] {
                            new DataRequestRecordsRecordFields() 
                        }
                    };

                    var fieldList = new List<DataRequestRecordsRecordFieldsField>();

                    fieldList.AddField("Contact ID", "Contact ID", physician.Id.ToString());
                    fieldList.AddField("First Name", "First Name", physician.FirstName);
                    fieldList.AddField("Middle Name", "Middle Name", physician.MiddleName);
                    fieldList.AddField("Last Name", "Last Name", physician.LastName);
                    fieldList.AddField("Marketer", "Marketer", string.Empty);
                    fieldList.AddField("Marketer ID", "Marketer ID", string.Empty);
                    fieldList.AddField("Associated Facility", "Associated Facility", string.Empty);
                    fieldList.AddField("Associated Facility ID", "Associated Facility ID", string.Empty);
                    fieldList.AddField("Title", "Title", physician.Credentials);
                    fieldList.AddField("Assistant", "Assistant", string.Empty);
                    fieldList.AddField("NPI", "NPI", physician.NPI);
                    fieldList.AddField("Work Phone", "Work Phone", physician.PhoneWork);
                    fieldList.AddField("Mobile Phone", "Mobile Phone", physician.PhoneAlternate);
                    fieldList.AddField("Address 1", "Address 1", physician.AddressLine1);
                    fieldList.AddField("Address 2", "Address 2", physician.AddressLine2);
                    fieldList.AddField("City", "City", physician.AddressCity);
                    fieldList.AddField("State", "State", physician.AddressStateCode);
                    fieldList.AddField("Zip", "Zip", physician.AddressZipCode);
                    fieldList.AddField("EMail", "EMail", physician.EmailAddress);
                    fieldList.AddField("Website", "Website", string.Empty);
                    fieldList.AddField("Additional Notes", "Additional Notes", string.Empty);

                    record.Fields[0].Field = fieldList.ToArray();
                    recordList.Add(record);
                });

                data.AddRecord(recordList.ToArray());
                result = data.ToXml();
                var directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestAgency_Sample_Contact_Message.XML");
                using (var textWriter = new StreamWriter(directory, false))
                {
                    textWriter.Write(result);
                }
            }
            return result;
        }
    }
}
