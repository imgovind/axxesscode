﻿namespace Axxess.OasisC.Domain
{
    using Axxess.OasisC.Enums;

    using System;
    using System.Xml.Serialization;
    using System.Collections.Generic;

    [XmlRoot()]
    [Serializable]
    public class Question : IQuestion
    {
        #region IQuestion Implementation

        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string Code { get; set; }
        [XmlAttribute]
        public string Answer { get; set; }
        [XmlAttribute]
        public QuestionType Type { get; set; }

        #endregion

        #region Static Methods

        public static Question Create(string name, string value)
        {
            if (name.StartsWith("M"))
            {
                return new Question { Name = name.Remove(0, 5), Code = name.Substring(0, 5), Answer = value, Type = QuestionType.Moo };
            }
            else if (name.StartsWith("485"))
            {
                return new Question { Name = name.Remove(0, 3), Answer = value, Type = QuestionType.PlanofCare };
            }
            else
            {
                return new Question { Name = name.Remove(0, 7), Answer = value, Type = QuestionType.Generic };
            }
        }

        #endregion
    }

    
}
