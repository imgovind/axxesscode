﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;
    using Extensions;

    using SubSonic.Repository;
    using Axxess.OasisC.Enums;
    using Axxess.Core.Infrastructure;

    public class AssessmentRepository : IAssessmentRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AssessmentRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssessmentRepository Members

        public bool Add(Assessment assessment)
        {
            if (assessment != null)
            {
                if (assessment.Questions != null)
                {
                    assessment.OasisData = assessment.Questions.ToXml();
                }
                assessment.Created = DateTime.Now;
                assessment.Modified = DateTime.Now;
                database.InsertAny(assessment);
                return true;
            }
            return false;
        }

        public bool Update(Assessment assessment)
        {
            bool result = false;
            if (assessment != null)
            {
                if (assessment.Questions != null)
                {
                    assessment.OasisData = assessment.Questions.ToXml();
                }
                assessment.Modified = DateTime.Now;
                database.UpdateAny(assessment);
                var data = new OASISBackup();
                data.Id = Guid.NewGuid();
                data.AgencyId = assessment.AgencyId;
                data.AssessmentId = assessment.Id;
                data.Data = assessment.OasisData;
                data.Created = DateTime.Now;
                if (CoreSettings.TurnOnOasisBackUP)
                {
                    database.AddOASISBackup(data);
                }
                result = true;
            }
            return result;
        }

        public bool UpdateModal(Assessment assessment)
        {
            bool result = false;
            if (assessment != null)
            {
                assessment.Modified = DateTime.Now;
                database.UpdateAny(assessment);
                result = true;
            }
            return result;
        }

        public Assessment Get(Guid assessmentId, string assessmentType, Guid agencyId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(assessmentType, assessmentId, agencyId);
                if (assessment != null)
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                }
            }
            return assessment;
        }

        public bool MarkAsDeleted(Guid agencyId, Guid assessmentId, Guid episodeId, Guid patientId, string assessmentType, bool isDeprecated)
        {
            bool result = false;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                var assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId, assessmentType);
                if (assessment != null)
                {
                    assessment.IsDeprecated = isDeprecated;
                    assessment.Modified = DateTime.Now;
                    database.UpdateAny(assessment);
                    result = true;
                }
            }
            return result;
        }

        public bool ReassignUser(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, Guid employeeId, string assessmentType)
        {
            bool result = false;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !employeeId.IsEmpty())
            {
                var assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId, assessmentType);
                if (assessment != null)
                {
                    try
                    {
                        assessment.UserId = employeeId;
                        assessment.Modified = DateTime.Now;
                        database.UpdateAny(assessment);
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
                else
                {
                    result = true;
                }
                
            }
            return result;
        }

        public Assessment Get(Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType, Guid agencyId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(agencyId, assessmentId, PatientId, EpisodeId, assessmentType);
                if (assessment != null)
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                }
            }
            return assessment;
        }

        public Assessment GetAssessmentOnly(Guid agencyId,Guid assessmentId, string assessmentType )
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(assessmentType, assessmentId, agencyId);
            }
            return assessment;
        }

        public Assessment GetAssessmentOnly(Guid agencyId, Guid episodeId, Guid patientId,Guid assessmentId, string assessmentType)
        {
            Assessment assessment = null;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty()&& !patientId.IsEmpty() && !episodeId.IsEmpty() && assessmentType.IsNotNullOrEmpty())
            {
                assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId, assessmentType);
            }
            return assessment;
        }

        public List<Assessment> GetAllByStatus(Guid agencyId, int status)
        {
            var assessments = new List<Assessment>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
                {
                    var data = database.FindAnyByStatus(agencyId, assessmentType.ToString(), status);
                    if (data != null && data.Count > 0)
                    {
                        assessments.AddRange(data);
                    }
                }
            }
            return assessments;
        }

        public List<Assessment> GetOnlyCMSOasisByStatus(Guid agencyId, int status)
        {
            var assessments = new List<Assessment>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
                {
                    if (assessmentType == AssessmentType.PlanOfCare485 || assessmentType == AssessmentType.NonOasisDischarge || assessmentType == AssessmentType.NonOasisRecertification || assessmentType == AssessmentType.NonOasisStartOfCare)
                    {
                        continue;
                    }
                    var data = database.FindAnyByStatus(agencyId, assessmentType.ToString(), status);
                    if (data != null && data.Count > 0)
                    {
                        assessments.AddRange(data);
                    }
                }
            }
            return assessments;
        }

        public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var assessments = new List<AssessmentExport>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
                {
                    if (assessmentType == AssessmentType.PlanOfCare485 || assessmentType == AssessmentType.NonOasisDischarge || assessmentType == AssessmentType.NonOasisRecertification || assessmentType == AssessmentType.NonOasisStartOfCare)
                    {
                        continue;
                    }
                    var data = database.FindAnyByStatusLean(agencyId,branchId, assessmentType.ToString(), status,patientStatus, startDate, endDate);

                    if (data != null && data.Count > 0)
                    {
                        assessments.AddRange(data);
                    }
                }
            }
            return assessments;
        }


        public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, List<int> paymentSources, DateTime startDate, DateTime endDate)
        {
            var assessments = new List<AssessmentExport>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
                {
                    if (assessmentType == AssessmentType.PlanOfCare485 || assessmentType == AssessmentType.NonOasisDischarge || assessmentType == AssessmentType.NonOasisRecertification || assessmentType == AssessmentType.NonOasisStartOfCare)
                    {
                        continue;
                    }
                    var data = database.FindAnyByStatusLean(agencyId, branchId, assessmentType.ToString(), status, paymentSources, startDate, endDate);
                    if (data != null && data.Count > 0)
                    {
                        assessments.AddRange(data);
                    }
                }
            }
            return assessments;
        }

        public List<Assessment> GetPreviousAssessments(Guid agencyId, Guid patientId, AssessmentType assessmentType)
        {
            var previousAssessments = new List<Assessment>();
            if (!agencyId.IsEmpty() && !patientId.IsEmpty())
            {
                previousAssessments = database.FindMany(agencyId, patientId, assessmentType.ToString()).OrderByDescending(a => a.AssessmentDate).ToList();
            }
            return previousAssessments;
        }

        public bool UsePreviousAssessment(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType, Guid previousAssessmentId, string previousAssessmentType)
        {
            bool result = false;

            var currentAssessment = database.FindAny(agencyId, assessmentId, patientId, episodeId, assessmentType);
            if (currentAssessment != null)
            {
                var previousAssessment = database.FindAny(previousAssessmentType, previousAssessmentId, agencyId);
                if (previousAssessment != null && previousAssessment.OasisData.IsNotNullOrEmpty())
                {
                    var previousAssessmentData = previousAssessment.OasisData.ToObject<List<Question>>();
                    var tempAssessmentData = new List<Question>();
                    if(previousAssessmentData != null && previousAssessmentData.Count > 0)
                    {
                        string type = IdentifyAssessmentNumber(assessmentType);
                        if (!type.Equals("10") && !type.Equals("00"))
                        {
                            var fields = database.FindAssessmentFields("RFA" + type);
                            if (fields != null && fields.Count > 0)
                            {
                                foreach (string field in fields)
                                {
                                    var answer = previousAssessmentData.FirstOrDefault(f => f.Name == field);

                                    if (answer != null)
                                    {
                                        tempAssessmentData.Add(answer);
                                    }
                                }
                                if (type == "01" || type == "03" || type == "04" || type=="09")
                                {
                                    previousAssessmentData.Where(f => f.Type == QuestionType.Generic || f.Type == QuestionType.PlanofCare)
                                        .ForEach(f => tempAssessmentData.Add(f));
                                }
                                else if (type == "05")
                                {
                                    var listOfGenericPlanOfCareFieldsType05 = AssessmentType05Generics();
                                    previousAssessmentData.Where(f => listOfGenericPlanOfCareFieldsType05.Contains(f.Name))
                                                                        .ForEach(f => tempAssessmentData.Add(f));
                                }
                                var currentAssessmentData = currentAssessment.OasisData.ToObject<List<Question>>();
                                currentAssessmentData.Where(field => !tempAssessmentData.Exists(t => t.Name == field.Name)).ForEach(field => tempAssessmentData.Add(field));
                            }
                        }
                        else if (type == "10")
                        {
                            List<string> fields = new List<string>();
                            fields.Add("PrimaryDiagnosis");
                            fields.Add("PrimaryDiagnosisDate");
                            fields.Add("ICD9M");
                            fields.Add("SymptomControlRating");
                            int character = 65;
                            for (int diagnosisCount = 1; diagnosisCount < 26; diagnosisCount++)
                            {
                                fields.Add("PrimaryDiagnosis" + diagnosisCount);
                                fields.Add("PrimaryDiagnosis" + diagnosisCount + "Date");
                                fields.Add("PaymentDiagnoses" + (char)character + "3");
                                fields.Add("PaymentDiagnoses" + (char)character + "4");
                                character++;
                            }
                            character = 65;
                            for (int diagCount2 = 1; diagCount2 < 26; diagCount2++)
                            {
                                fields.Add("ICD9M" + (char)character + "3");
                                fields.Add("ICD9M" + (char)character + "4");
                                fields.Add("ICD9M" + diagCount2);
                                fields.Add("ExacerbationOrOnsetPrimaryDiagnosis" + diagCount2);
                                fields.Add("OtherDiagnose" + diagCount2 + "Rating");
                                character++;
                            }
                            fields.Add("PaymentDiagnosesZ3");
                            fields.Add("PaymentDiagnosesZ4");

                            previousAssessmentData.ForEach(f => {
                                if (f.Type == QuestionType.Generic || f.Type == QuestionType.PlanofCare)
                                {
                                    tempAssessmentData.Add(f);
                                }
                                else if(f.Type == QuestionType.Moo && fields.Contains(f.Name))
                                {
                                    tempAssessmentData.Add(f);
                                }
                            });
                            var currentAssessmentData = currentAssessment.OasisData.ToObject<List<Question>>();
                            currentAssessmentData.Where(field => !tempAssessmentData.Exists(t => t.Name == field.Name)).ForEach(field => tempAssessmentData.Add(field));
                        }
                        else
                        {
                            return false;
                        }
                        currentAssessment.Questions = tempAssessmentData;
                        return Update(currentAssessment);
                    }
                }
            }
            return result;
        }

        private List<string> AssessmentType05Generics()
        {
            return new List<string>() { "AllergiesDescription", "PulseApical", "PulseApicalRegular",
                "PulseApicalIrregular", "LastVisitDate", "GenericHeight", "PulseRadial", "PulseRadialReqular",
                "PulseRadialIrregular", "Weight", "WeightActualStated", "WeightActualStated", "Temp", "Resp",
                "BPLeftLying", "BPRightLying", "BPSittingLeft", "BPSittingRight", "BPStandingLeft", "BPStandingRight",
                "TempGreaterThan", "TempLessThan", "PulseGreaterThan", "PulseLessThan", "RespirationGreaterThan",
                "RespirationLessThan", "SystolicBPGreaterThan", "SystolicBPLessThan", "DiastolicBPGreaterThan",
                "DiastolicBPLessThan", "02SatLessThan", "FastingBloodSugarGreaterThan", "FastingBloodSugarLessThan",
                "RandomBloddSugarGreaterThan", "RandomBloodSugarLessThan", "WeightGreaterThan", "Pnemonia", "PnemoniaDate",
                "Flu", "FluDate", "TB", "TBDate", "TBExposure", "TBExposureDate", "AdditionalImmunization1Name", "AdditionalImmunization1",
                "AdditionalImmunization1Date", "AdditionalImmunization2Name", "AdditionalImmunization2", "AdditionalImmunization2Date",
                "ImmunizationComments", "Allergies", "SurgicalProcedureDescription1", "SurgicalProcedureCode1", "SurgicalProcedureCode1Date",
                "SurgicalProcedureDescription2", "SurgicalProcedureCode2", "SurgicalProcedureCode2Date"
            };
        }

        private string IdentifyAssessmentNumber(string assessmentType)
        {
            string number = "00";
            switch (assessmentType)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                case "OASISCStartofCareOT":
                    number = "01";
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCareOT":
                case "OASISCResumptionofCarePT":
                    number = "03";
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    number = "05";
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    number = "04";
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "TransferInPatientNotDischarged":
                    number = "06";
                    break;
                case "OASISCTransferDischarge":
                case "TransferInPatientDischarged":
                    number = "07";
                    break;
                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    number = "08";
                    break;
                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                case "OASISCDischargeST":
                    number = "09";
                    break;
                case "NonOasisStartOfCare":
                case "NonOASISStartofCare":
                    number = "10";
                    break;
                case "NonOasisRecertification":
                case "NonOASISRecertification":
                    number = "10";
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    number = "10";
                    break;
                default:
                    break;
            }
            return number;
        }

        #endregion
    }
}
