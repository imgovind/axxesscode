﻿namespace Axxess.LookUp.Domain
{
    public class ZipCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public string City { get; set; }
        public string CBSA { get; set; }
    }
}
