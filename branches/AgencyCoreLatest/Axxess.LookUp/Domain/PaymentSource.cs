﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class PaymentSource
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
