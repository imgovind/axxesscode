﻿namespace Axxess.LookUp.Domain
{
    using System;

    [Serializable]
    public class DrugClassification
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
