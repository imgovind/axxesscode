﻿namespace Axxess.Scheduled.ShpData
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;

    using SubSonic.Repository;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Enums;

    public static class Database
    {
        #region Private Members

        private static readonly SimpleRepository agencyManagementDatabase1 = new SimpleRepository("AgencyManagementConnectionString1", SimpleRepositoryOptions.None);
        private static readonly SimpleRepository agencyManagementDatabase2 = new SimpleRepository("AgencyManagementConnectionString2", SimpleRepositoryOptions.None);
        
        private const string SELECT_OASIS_HEADERFORMAT = @"SELECT * FROM `submissionheaderformats`";
        private const string SELECT_AGENCY_PATIENTS = @"SELECT * FROM `patients` WHERE agencyid = @agencyid AND IsDeprecated = 0;";
        private const string SELECT_AGENCY_LOCATION = @"SELECT * FROM `agencylocations` WHERE agencyid = @agencyid AND IsDeprecated = 0 LIMIT 0,1;";
        private const string SELECT_AGENCY_USERS = @"SELECT Id, FirstName, LastName, Credentials, CredentialsOther FROM `users` WHERE AgencyId = @agencyid;";
        private const string SELECT_PATIENT_EPISODES =
                 "SELECT Id, Schedule FROM patientepisodes WHERE AgencyId = @agencyid AND PatientId = @patientid AND IsActive = 1;";

        private const string SELECT_PATIENTADMISSION_EPISODES =
                "SELECT Id, Schedule FROM patientepisodes WHERE AgencyId = @agencyid AND PatientId = @patientid " +
                "AND AdmissionId = @admissionid AND patientepisodes.IsActive = 1;";

        private const string SELECT_OASIS_ASSESSMENT = @"SELECT SubmissionFormat, `Status`, Modified FROM `{0}s` WHERE AgencyId = @agencyid AND PatientId = @patientid AND id = @id AND IsDeprecated = 0 LIMIT 0, 1;";

        private const string SELECT_SHPDATA_BATCH = @"SELECT * FROM `shpdatabatches` WHERE Id = @id AND AgencyId = @agencyid AND PatientId = @patientid LIMIT 0, 1;";
        private const string SELECT_SHPDATA_CUSTOMERS = @"SELECT * FROM `shpdatalogins` WHERE IsDeprecated = 0;";

        #endregion

        #region Internal Methods

        internal static bool Add<T>(T item, int clusterId) where T : class, new()
        {
            if (item != null)
            {
                if (clusterId == 1)
                {
                    agencyManagementDatabase1.Add<T>(item);
                    return true;
                }
                else if (clusterId == 2)
                {
                    agencyManagementDatabase2.Add<T>(item);
                    return true;
                }
            }
            return false;
        }

        internal static bool Update<T>(T item, int clusterId) where T : class, new()
        {
            if (item != null)
            {
                if (clusterId == 1)
                {
                    agencyManagementDatabase1.Update<T>(item);
                    return true;
                }
                else
                {

                    agencyManagementDatabase2.Update<T>(item);
                    return true;
                }
            }
            return false;
        }

        internal static List<Guid> GetAgencyIds(int clusterId)
        {
            var script = string.Format(@"SELECT * FROM shpdatalogins WHERE ClusterId={0}", clusterId);
            using (var cmd = new FluentCommand<Guid>(script))
            {
                return cmd.SetConnection("AxxessMembershipConnectionString")
                    .AsList();
            }
        }
        
        internal static List<ShpDataLogin> GetShpCustomers()
        {
            var shpCustomers = new List<ShpDataLogin>();
            var script = @"SELECT shpdatalogins.*, agencysnapshots.ClusterId FROM shpdatalogins, agencysnapshots WHERE shpdatalogins.AgencyId = agencysnapshots.Id AND agencysnapshots.IsDeprecated = 0;";
            using (var cmd = new FluentCommand<ShpDataLogin>(script))
            {
                cmd.SetConnection("AxxessMembershipConnectionString");
                shpCustomers = cmd.SetMap(reader => new ShpDataLogin()
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    ClusterId = reader.GetInt("ClusterId"),
                    Username = reader.GetStringNullable("Username"),
                    ProviderNo = reader.GetStringNullable("ProviderNo"),
                    PasswordHash = reader.GetStringNullable("PasswordHash"),
                    ActivationCode = reader.GetStringNullable("ActivationCode")
                }).AsList();
            }
            return shpCustomers;
        }

        internal static List<Patient> GetPatients(Guid agencyId, int clusterId)
        {
            var patients = new List<Patient>();
            using (var cmd = new FluentCommand<Patient>(SELECT_AGENCY_PATIENTS))
            {
                patients = cmd.SetConnection("AgencyManagementConnectionString"+clusterId)
                    .AddGuid("agencyid", agencyId)
                    .AsList();
            }
            return patients;
        }

        internal static List<PatientAdmissionDate> GetPatientAdmissions(Guid agencyId, int clusterId, Guid patientId)
        {
            if (clusterId == 1)
            {
                return agencyManagementDatabase1.Find<PatientAdmissionDate>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).OrderBy(p => p.StartOfCareDate.ToZeroFilled()).ToList();
            }
            else 
            {
                return agencyManagementDatabase2.Find<PatientAdmissionDate>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).OrderBy(p => p.StartOfCareDate.ToZeroFilled()).ToList();
            }
        }

        //internal static List<PatientEpisodeData> GetPatientEpisodes(Guid agencyId, Guid patientId)
        //{
        //    var list = new List<PatientEpisodeData>();

        //    using (var cmd = new FluentCommand<PatientEpisodeData>(SELECT_PATIENT_EPISODES))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .AddGuid("patientid", patientId)
        //        .SetMap(reader => new PatientEpisodeData
        //        {
        //            Id = reader.GetGuid("Id"),
        //            Schedule = reader.GetStringNullable("Schedule")
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        internal static List<PatientEpisodeData> GetPatientAdmissionEpisodes(Guid agencyId, int clusterId, Guid patientId, Guid admissionId)
        {
            var list = new List<PatientEpisodeData>();

            using (var cmd = new FluentCommand<PatientEpisodeData>(SELECT_PATIENTADMISSION_EPISODES))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString"+clusterId)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("admissionid", admissionId)
                .SetMap(reader => new PatientEpisodeData
                {
                    Id = reader.GetGuid("Id"),
                    Schedule = reader.GetStringNullable("Schedule")
                })
                .AsList();
            }
            return list;
        }

        internal static Assessment GetAssessment(Guid agencyId, int clusterId, Guid patientId, Guid assessmentId, AssessmentType assessmentType)
        {
            Assessment assessment = null;
            var sql = string.Format(SELECT_OASIS_ASSESSMENT, GetAssessmentTableName(assessmentType));
            using (var cmd = new FluentCommand<Assessment>(sql))
            {
                assessment = cmd.SetConnection("OasisCConnectionString"+clusterId)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("id", assessmentId)
                .SetMap(reader => new Assessment
                {
                    Status = reader.GetInt("Status"),
                    Modified = reader.GetDateTime("Modified"),
                    SubmissionFormat = reader.GetStringNullable("SubmissionFormat")
                })
                .AsSingle();
            }
            return assessment;
        }

        internal static ShpDataBatch GetShpDataBatch(Guid assessmentId, Guid agencyId, int clusterId, Guid patientId)
        {
            ShpDataBatch shpDataBatch = null;
            using (var cmd = new FluentCommand<ShpDataBatch>(SELECT_SHPDATA_BATCH))
            {
                shpDataBatch = cmd
                    .SetConnection("AgencyManagementConnectionString"+clusterId)
                    .AddGuid("id", assessmentId)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => new ShpDataBatch
                    {
                        Id = reader.GetGuid("Id"),
                        Status = reader.GetInt("Status"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        PatientId = reader.GetGuid("PatientId"),
                        Created = reader.GetDateTime("Created"),
                        LastModified = reader.GetDateTime("LastModified"),
                        SubmissionFormat = reader.GetStringNullable("SubmissionFormat")
                    }).AsSingle();
            }
            return shpDataBatch;
        }

        internal static Dictionary<string, SubmissionHeaderFormat> GetOasisHeaderInstructionsNew(int clusterId)
        {
            var format = GetSubmissionHeaderFormatInstructions(clusterId);
            var dictionaryFormat = new Dictionary<string, SubmissionHeaderFormat>();
            if (format != null && format.Count > 0)
            {
                format.ForEach(f =>
                {
                    dictionaryFormat.Add(f.Item, f);
                });
            }
            return dictionaryFormat;
        }

        internal static List<SubmissionHeaderFormat> GetSubmissionHeaderFormatInstructions(int clusterId)
        {
            var list = new List<SubmissionHeaderFormat>();

            using (var cmd = new FluentCommand<SubmissionHeaderFormat>(SELECT_OASIS_HEADERFORMAT))
            {
                list = cmd.SetConnection("OasisCConnectionString"+clusterId)
                .SetMap(reader => new SubmissionHeaderFormat
                {
                    Item = reader.GetStringNullable("Item"),
                    Length = reader.GetDouble("Length"),
                    Start = reader.GetDouble("Start"),
                    End = reader.GetDouble("End"),
                    PadType = reader.GetStringNullable("PadType"),
                    DataType = reader.GetStringNullable("DataType"),
                    DefaultValue = reader.GetStringNullable("DefaultValue")
                })
                .AsList();
            }
            return list;
        }

        internal static List<User> GetAgencyUsers(Guid agencyId, int clusterId)
        {
            var list = new List<User>();

            using (var cmd = new FluentCommand<User>(SELECT_AGENCY_USERS))
            {
                list = cmd
                    .SetConnection("AgencyManagementConnectionString"+clusterId)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new User
                    {
                        Id = reader.GetGuid("Id"),
                        FirstName = reader.GetString("FirstName"),
                        LastName = reader.GetString("LastName"),
                        Credentials = reader.GetStringNullable("Credentials"),
                        CredentialsOther = reader.GetStringNullable("CredentialsOther")
                    })
                    .AsList();
            }
            return list;
        }

        internal static AgencyLocation GetAgencyLocation(Guid agencyId, int clusterId)
        {
            var location=new AgencyLocation();
            if (clusterId == 1)
            {
                location = agencyManagementDatabase1.Single<AgencyLocation>(a => a.AgencyId == agencyId && a.IsMainOffice == true);
                if (location != null && !location.IsLocationStandAlone)
                {
                    var agency = agencyManagementDatabase1.Single<Agency>(a => a.Id == agencyId);
                    if (agency != null)
                    {
                        location.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                        location.MedicareProviderNumber = agency.MedicareProviderNumber;
                        location.NationalProviderNumber = agency.NationalProviderNumber;
                    }
                }
            }
            else if (clusterId == 2)
            {
                location = agencyManagementDatabase2.Single<AgencyLocation>(a => a.AgencyId == agencyId && a.IsMainOffice == true);
                if (location != null && !location.IsLocationStandAlone)
                {
                    var agency = agencyManagementDatabase2.Single<Agency>(a => a.Id == agencyId);
                    if (agency != null)
                    {
                        location.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                        location.MedicareProviderNumber = agency.MedicareProviderNumber;
                        location.NationalProviderNumber = agency.NationalProviderNumber;
                    }
                }
            }
            
            return location;

        }

        #endregion

        #region Private Methods

        private static string GetAssessmentTableName(AssessmentType assessmentType)
        {
            var tableName = string.Empty;
            switch (assessmentType)
            {
                case AssessmentType.StartOfCare:
                    tableName = "startofcareassessment";
                    break;
                case AssessmentType.DischargeFromAgency:
                    tableName = "dischargefromagencyassessment";
                    break;
                case AssessmentType.DischargeFromAgencyDeath:
                    tableName = "deathathomeassessment";
                    break;
                case AssessmentType.FollowUp:
                    tableName = "followupassessment";
                    break;
                case AssessmentType.Recertification:
                    tableName = "recertificationassessment";
                    break;
                case AssessmentType.ResumptionOfCare:
                    tableName = "resumptionofcareassessment";
                    break;
                case AssessmentType.TransferInPatientDischarged:
                    tableName = "transferdischargeassessment";
                    break;
                case AssessmentType.TransferInPatientNotDischarged:
                    tableName = "transfernotdischargedassessment";
                    break;
                default:
                    break;
            }
            return tableName;
        }

        #endregion
    }
}
