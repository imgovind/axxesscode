﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml.XPath;
    using System.ServiceModel;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    
    using Axxess.Api.Contracts;
    using Axxess.Api.Reporting.Enums;

    public class ReportService2 : BaseService, IReportService
    {
        #region IReportService Members

        private static readonly GrouperAgent GrouperAgent = new GrouperAgent();

        public List<Dictionary<string, string>> CahpsExport(Guid agencyId, Guid agencyLocationId, int sampleMonth, int sampleYear)
        {
            return CahpsExportByPaymentSources(agencyId, agencyLocationId, sampleMonth, sampleYear, new List<int>());
        }

        public List<Dictionary<string, string>> CahpsExportByPaymentSources(Guid agencyId, Guid agencyLocationId, int sampleMonth, int sampleYear, List<int> paymentSources)
        {
            var collection = new List<Dictionary<string, string>>();
            IDataSpecification dataSpecification = null;

            try
            {
                var agency = Reports.GetAgencyLocation(agencyId, agencyLocationId) ?? new AgencyData();
                if (!agency.IsLocationStandAlone)
                {
                    agency = Reports.GetAgency(agencyId) ?? new AgencyData();
                }
                agency.LocationId = agencyLocationId;

                if (agency != null)
                {
                    switch (agency.CahpsVendor)
                    {
                        case 1: // DSS Research
                            dataSpecification = new DssResearchDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)2: // Synovate Healthcare
                            dataSpecification = new SynovateDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case 3: // Novaetus
                            dataSpecification = new NovaetusDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)4: // Deyta
                        case (int)6: // Fields Research
                            dataSpecification = new DeytaDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)5: // Pinnacle
                            dataSpecification = new PinnacleDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)7: // PressGaney
                            dataSpecification = new PressGaneyDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)8: // Ocs
                            dataSpecification = new OcsDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)9: // Fazzi
                        case (int)11: // Beacon Touch
                            dataSpecification = new FazziDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)10: // Strategic Healthcare
                            dataSpecification = new ShpDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collection;
        }

        public List<Dictionary<string, string>> PPSEpisodeInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodes = Reports.GetEpisodesExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodes != null && episodes.Count > 0)
                    {
                        episodes.ForEach(episode =>
                        {
                            var newHippsCode = string.Empty;
                            ProspectivePayment prospectivePayment = null;
                            IDictionary<string, Question> assessmentData = null;
                            var assessment = Reports.GetEpisodeAssessment(episode);
                            if (assessment != null)
                            {
                                assessmentData = assessment.ToDictionary();
                                prospectivePayment = GetProspectivePayment(assessment.HippsCode, episode.StartDate, episode.AddressZipCode.IsNotNullOrEmpty() ? episode.AddressZipCode : agencyData.AddressZipCode.IsNotNullOrEmpty() ? agencyData.AddressZipCode : string.Empty);
                            }

                            var collection = new Dictionary<string, string>();
                            collection.Add("MRN", episode.MRN.IsNotNullOrEmpty() ? episode.MRN : string.Empty);
                            collection.Add("Patient", episode.DisplayName.IsNotNullOrEmpty() ? episode.DisplayName : string.Empty);
                            collection.Add("Policy #", episode.MedicareNumber.IsNotNullOrEmpty() ? episode.MedicareNumber : episode.MedicaidNumber.IsNotNullOrEmpty() ? episode.MedicaidNumber : string.Empty);
                            collection.Add("Admission ID", " ");
                            collection.Add("Admission Date", episode.StartofCareDate.IsValid() && episode.StartofCareDate != DateTime.MinValue ? episode.StartofCareDate.ToShortDateString() : string.Empty);
                            collection.Add("Episode #", string.Empty);
                            collection.Add("Episode Type", GetEpisodeTiming(assessmentData));
                            collection.Add("NRS Severity Level", GetNrsSeverityLevel(assessment));
                            collection.Add("Begin Date", episode.StartDate.IsValid() && episode.StartDate != DateTime.MinValue ? episode.StartDate.ToShortDateString() : string.Empty);
                            collection.Add("End Date", episode.EndDate.IsValid() && episode.EndDate != DateTime.MinValue ? episode.EndDate.ToShortDateString() : string.Empty);
                            collection.Add("First Billable", GetFirstBillableDate(episode));
                            collection.Add("Status", episode.Status == 1 ? "AD" : episode.Status == 2 ? "DI" : string.Empty);
                            collection.Add("Discharge Date", episode.Status == 2 && episode.DischargeDate.IsValid() && episode.DischargeDate != DateTime.MinValue ? episode.DischargeDate.ToShortDateString() : string.Empty);

                            var originalTherapyNeed = assessmentData != null && assessmentData.AnswerOrEmptyString("M2200NumberOfTherapyNeed").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M2200NumberOfTherapyNeed").IsInteger() ? assessmentData.AnswerOrEmptyString("M2200NumberOfTherapyNeed").ToInteger().ToString() : string.Empty;

                            collection.Add("Original HHRG", prospectivePayment != null ? prospectivePayment.Hhrg : string.Empty);
                            collection.Add("Original Therapy Visits", originalTherapyNeed);
                            collection.Add("Original Case Mix", prospectivePayment != null ? prospectivePayment.Weight : string.Empty);
                            collection.Add("Original HIPPS", assessment != null && assessment.HippsCode.IsNotNullOrEmpty() ? assessment.HippsCode : string.Empty);

                            var completedTherapyVisits = GetCompletedTherapyVisits(episode);

                            Hipps hippsCode = null;
                            prospectivePayment = null;
                            var completedTherapyText = completedTherapyVisits.ToString().PadLeft(3, '0');

                            if (assessmentData != null)
                            {
                                string oasisFormat = string.Empty;
                                if (assessment.SubmissionFormat.IsNotNullOrEmpty())
                                {
                                    oasisFormat = assessment.SubmissionFormat.Remove(864, 3);
                                    oasisFormat = oasisFormat.Insert(864, completedTherapyText);
                                }
                                hippsCode = GrouperAgent.GetHippsCode(oasisFormat);
                                if (hippsCode != null)
                                {
                                    prospectivePayment = GetProspectivePayment(hippsCode.Code, episode.StartDate, episode.AddressZipCode.IsNotNullOrEmpty() ? episode.AddressZipCode : agencyData.AddressZipCode.IsNotNullOrEmpty() ? agencyData.AddressZipCode : string.Empty);
                                }
                            }
                            collection.Add("HHRG", prospectivePayment != null ? prospectivePayment.Hhrg : string.Empty);
                            collection.Add("Therapy Visits", completedTherapyVisits);
                            collection.Add("Case Mix", prospectivePayment != null ? prospectivePayment.Weight : string.Empty);
                            collection.Add("HIPPS", prospectivePayment != null ? prospectivePayment.HippsCode : string.Empty);
                            collection.Add("MSA/CBSA", prospectivePayment != null ? prospectivePayment.CbsaCode : string.Empty);
                            collection.Add("Wage Index", prospectivePayment != null ? prospectivePayment.WageIndex : string.Empty);

                            var totalVisits = GetTotalEpisodeVisits(episode);
                            var completedVisits = GetTotalCompletedEpisodeVisits(episode);

                            if (episode.EndDate.IsValid() && episode.EndDate.Date < DateTime.Now.Date && totalVisits.ToInteger() > 0 && completedVisits.ToInteger() < 5)
                            {
                                collection.Add("LUPA", "Y");
                            }
                            else
                            {
                                collection.Add("LUPA", "N");

                            }
                            if (episode.StartDate.IsValid() && episode.EndDate.IsValid() && episode.StartDate.AddDays(59).Date == episode.EndDate.Date)
                            {
                                collection.Add("PEP", "Y");
                            }
                            else
                            {
                                collection.Add("PEP", "N");
                            }
                            collection.Add("SCIC", "N/A");
                            collection.Add("Outlier", "N");

                            if (originalTherapyNeed.IsInteger() && completedTherapyVisits.IsInteger())
                            {
                                collection.Add("Therapy Adjustment", ((int)originalTherapyNeed.ToInteger() - completedTherapyVisits.ToInteger()).ToString());
                            }
                            else
                            {
                                collection.Add("Therapy Adjustment", string.Empty);
                            }
                            collectionList.Add(collection);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("ReportService1.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collectionList;
        }

        public List<Dictionary<string, string>> PPSVisitInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodes = Reports.GetEpisodesExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodes != null && episodes.Count > 0)
                    {
                        episodes.ForEach(episode =>
                        {
                            IDictionary<string, Question> assessmentData = null;
                            var assessment = Reports.GetEpisodeAssessment(episode);
                            if (assessment != null)
                            {
                                assessmentData = assessment.ToDictionary();
                            }

                            var collection = new Dictionary<string, string>();
                            collection.Add("MRN", episode.MRN.IsNotNullOrEmpty() ? episode.MRN : string.Empty);
                            collection.Add("Patient", episode.DisplayName.IsNotNullOrEmpty() ? episode.DisplayName : string.Empty);
                            collection.Add("Policy #", episode.MedicareNumber.IsNotNullOrEmpty() ? episode.MedicareNumber : episode.MedicaidNumber.IsNotNullOrEmpty() ? episode.MedicaidNumber : string.Empty);
                            collection.Add("Admission ID", string.Empty);
                            collection.Add("Begin Date", episode.StartDate.IsValid() && episode.StartDate != DateTime.MinValue ? episode.StartDate.ToShortDateString() : string.Empty);
                            collection.Add("End Date", episode.EndDate.IsValid() && episode.EndDate != DateTime.MinValue ? episode.EndDate.ToShortDateString() : string.Empty);
                            collection.Add("First Billable", GetFirstBillableDate(episode));

                            var months = GetMonthsBetweenAndAfter(startDate, endDate);
                            foreach (var month in months)
                            {
                                collection.Add(string.Format("{0} {1}", month.Value, month.Key.ToString("yyyy")), GetCompletedVisitsInMonth(episode, month.Key.Month));
                            }

                            collection.Add("SN", GetCompletedVisitsByDiscipline(episode, "Nursing"));
                            collection.Add("PT", GetCompletedVisitsByDiscipline(episode, "PT"));
                            collection.Add("OT", GetCompletedVisitsByDiscipline(episode, "OT"));
                            collection.Add("ST", GetCompletedVisitsByDiscipline(episode, "ST"));
                            collection.Add("SW", GetCompletedVisitsByDiscipline(episode, "MSW"));
                            collection.Add("HHA", GetCompletedVisitsByDiscipline(episode, "HHA"));
                            collection.Add("Total Visits", GetTotalCompletedEpisodeVisits(episode));

                            collectionList.Add(collection);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collectionList;
        }

        public List<Dictionary<string, string>> PPSChargeInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodes = Reports.GetEpisodesExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodes != null && episodes.Count > 0)
                    {
                        episodes.ForEach(episode =>
                        {
                            ProspectivePayment prospectivePayment = null;
                            IDictionary<string, Question> assessmentData = null;
                            var assessment = Reports.GetEpisodeAssessment(episode);
                            if (assessment != null)
                            {
                                assessmentData = assessment.ToDictionary();
                                prospectivePayment = GetProspectivePayment(assessment.HippsCode, episode.StartDate, episode.AddressZipCode.IsNotNullOrEmpty() ? episode.AddressZipCode : agencyData.AddressZipCode.IsNotNullOrEmpty() ? agencyData.AddressZipCode : string.Empty);
                            }

                            var collection = new Dictionary<string, string>();
                            collection.Add("MRN", episode.MRN.IsNotNullOrEmpty() ? episode.MRN : string.Empty);
                            collection.Add("Patient", episode.DisplayName.IsNotNullOrEmpty() ? episode.DisplayName : string.Empty);
                            collection.Add("Policy #", episode.MedicareNumber.IsNotNullOrEmpty() ? episode.MedicareNumber : episode.MedicaidNumber.IsNotNullOrEmpty() ? episode.MedicaidNumber : string.Empty);
                            collection.Add("Admission ID", string.Empty);
                            collection.Add("Begin Date", episode.StartDate.IsValid() && episode.StartDate != DateTime.MinValue ? episode.StartDate.ToShortDateString() : string.Empty);
                            collection.Add("End Date", episode.EndDate.IsValid() && episode.EndDate != DateTime.MinValue ? episode.EndDate.ToShortDateString() : string.Empty);
                            collection.Add("First Billable", GetFirstBillableDate(episode));

                            var months = GetMonthsBetween(startDate, endDate);
                            foreach (var month in months)
                            {
                                collection.Add(string.Format("Total Charges for {0}", month.Value), GetChargesByMonth(episode, prospectivePayment, month.Key));
                            }

                            collection.Add("Total Charges for SN", GetChargesByDiscipline(episode, prospectivePayment, "Nursing"));
                            collection.Add("Total Charges for PT", GetChargesByDiscipline(episode, prospectivePayment, "PT"));
                            collection.Add("Total Charges for OT", GetChargesByDiscipline(episode, prospectivePayment, "OT"));
                            collection.Add("Total Charges for ST", GetChargesByDiscipline(episode, prospectivePayment, "ST"));
                            collection.Add("Total Charges for SW", GetChargesByDiscipline(episode, prospectivePayment, "MSW"));
                            collection.Add("Total Charges for HHA", GetChargesByDiscipline(episode, prospectivePayment, "HHA"));
                            collection.Add("MS", "$0.00");
                            collection.Add("Total Charges", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");

                            foreach (var month in months)
                            {
                                collection.Add(string.Format("Contractual Adjustments for {0}", month.Value), "$0.00");
                            }
                            collection.Add("Contractual Adjustment Total", "$0.00");

                            foreach (var month in months)
                            {
                                collection.Add(string.Format("CA Reimbursement for {0}", month.Value), "N/A");
                            }
                            collection.Add("Orig Expected Reimb", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");
                            collection.Add("Adjustment", "0");
                            collection.Add("Current Expected Reimb", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");

                            collectionList.Add(collection);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collectionList;
        }

        public List<Dictionary<string, string>> PPSPaymentInformation(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodes = Reports.GetEpisodesExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodes != null && episodes.Count > 0)
                    {
                        episodes.ForEach(episode =>
                        {
                            ProspectivePayment prospectivePayment = null;
                            IDictionary<string, Question> assessmentData = null;
                            var assessment = Reports.GetEpisodeAssessment(episode);
                            if (assessment != null)
                            {
                                assessmentData = assessment.ToDictionary();
                                prospectivePayment = GetProspectivePayment(assessment.HippsCode, episode.StartDate, episode.AddressZipCode.IsNotNullOrEmpty() ? episode.AddressZipCode : agencyData.AddressZipCode.IsNotNullOrEmpty() ? agencyData.AddressZipCode : string.Empty);
                            }

                            var collection = new Dictionary<string, string>();
                            collection.Add("MRN", episode.MRN.IsNotNullOrEmpty() ? episode.MRN : string.Empty);
                            collection.Add("Patient", episode.DisplayName.IsNotNullOrEmpty() ? episode.DisplayName : string.Empty);
                            collection.Add("Policy #", episode.MedicareNumber.IsNotNullOrEmpty() ? episode.MedicareNumber : episode.MedicaidNumber.IsNotNullOrEmpty() ? episode.MedicaidNumber : string.Empty);
                            collection.Add("Admission ID", string.Empty);
                            collection.Add("Begin Date", episode.StartDate.IsValid() && episode.StartDate != DateTime.MinValue ? episode.StartDate.ToShortDateString() : string.Empty);
                            collection.Add("End Date", episode.EndDate.IsValid() && episode.EndDate != DateTime.MinValue ? episode.EndDate.ToShortDateString() : string.Empty);
                            collection.Add("First Billable", GetFirstBillableDate(episode));

                            var rap = Reports.GetRap(agencyData.Id, episode.PatientId, episode.Id);
                            if (rap != null)
                            {
                                collection.Add("RAP Bill Date", rap.ClaimDate.IsValid() && rap.ClaimDate != DateTime.MinValue ? rap.ClaimDate.ToShortDateString() : string.Empty);
                                collection.Add("RAP Bill Amount", string.Format("${0:#0.00}", rap.ClaimAmount));
                                collection.Add("RAP Expiration Date", episode.EndDate.AddDays(59).ToShortDateString());
                                collection.Add("RAP Payment Received", rap.PaymentDate.IsValid() && rap.PaymentDate != DateTime.MinValue ? rap.PaymentDate.ToShortDateString() : string.Empty);
                                collection.Add("RAP Payment Amount", string.Format("${0:#0.00}", rap.PaymentAmount));
                            }

                            var final = Reports.GetFinal(agencyData.Id, episode.PatientId, episode.Id);
                            if (final != null)
                            {
                                collection.Add("Final Claim Bill Date", final.ClaimDate.IsValid() && final.ClaimDate != DateTime.MinValue ? final.ClaimDate.ToShortDateString() : string.Empty);
                                collection.Add("Final Claim Payment Received", final.PaymentDate.IsValid() && final.PaymentDate != DateTime.MinValue ? final.PaymentDate.ToShortDateString() : string.Empty);
                                collection.Add("Final Claim Payment Amount", string.Format("${0:#0.00}", final.PaymentAmount));
                            }

                            collection.Add("CA/Other Amount", "$0.00");
                            collection.Add("Current Expected Reimb", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");
                            collection.Add("Balance Due", GetBalanceDue(prospectivePayment != null ? prospectivePayment.TotalAmount : 0, rap, final));
                            collection.Add("Cost Amount", "$0.00");
                            collection.Add("Margin", prospectivePayment != null ? string.Format("${0:#0.00}", prospectivePayment.TotalAmount) : "$0.00");

                            collectionList.Add(collection);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collectionList;
        }

        public List<PatientsAndVisitsByAgeResult> PatientsAndVisitsByAge(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var ageRange1 = new AgeRange();
            var ageRange2 = new AgeRange();
            var ageRange3 = new AgeRange();
            var ageRange4 = new AgeRange();
            var ageRange5 = new AgeRange();
            var ageRange6 = new AgeRange();
            var ageRange7 = new AgeRange();
            var ageRange8 = new AgeRange();
            var ageRange9 = new AgeRange();
            var ageRange10 = new AgeRange();
            var list = new List<PatientsAndVisitsByAgeResult>();

            try
            {
                var episodes = Reports.GetEpisodesExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                if (episodes != null && episodes.Count > 0)
                {
                    episodes.ForEach(episode =>
                    {
                        if (episode != null && episode.Schedule.IsNotNullOrEmpty() && episode.DOB != DateTime.MinValue)
                        {
                            var assessment = Reports.GetEpisodeAssessment(episode);
                            if (assessment != null)
                            {
                                var visits = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;

                                int age = DateTime.Today.Year - episode.DOB.Year;
                                if (age > 0 && age <= 10)
                                {
                                    if (!ageRange1.List.Contains(episode.AdmissionId))
                                    {
                                        ageRange1.List.Add(episode.AdmissionId);
                                    }
                                    ageRange1.Count += visits;
                                }
                                else if (age >= 11 && age <= 20)
                                {
                                    if (!ageRange2.List.Contains(episode.AdmissionId))
                                    {
                                        ageRange2.List.Add(episode.AdmissionId);
                                    }
                                    ageRange2.Count += visits;
                                }
                                else if (age >= 21 && age <= 30)
                                {
                                    if (!ageRange3.List.Contains(episode.AdmissionId))
                                    {
                                        ageRange3.List.Add(episode.AdmissionId);
                                    }
                                    ageRange3.Count += visits;
                                }
                                else if (age >= 31 && age <= 40)
                                {
                                    if (!ageRange4.List.Contains(episode.AdmissionId))
                                    {
                                        ageRange4.List.Add(episode.AdmissionId);
                                    }
                                    ageRange4.Count += visits;
                                }
                                else if (age >= 41 && age <= 50)
                                {
                                    if (!ageRange5.List.Contains(episode.AdmissionId))
                                    {
                                        ageRange5.List.Add(episode.AdmissionId);
                                    }
                                    ageRange5.Count += visits;
                                }
                                else if (age >= 51 && age <= 60)
                                {
                                    if (!ageRange6.List.Contains(episode.AdmissionId))
                                    {
                                        ageRange6.List.Add(episode.AdmissionId);
                                    }
                                    ageRange6.Count += visits;
                                }
                                else if (age >= 61 && age <= 70)
                                {
                                    if (!ageRange7.List.Contains(episode.AdmissionId))
                                    {
                                        ageRange7.List.Add(episode.AdmissionId);
                                    }
                                    ageRange7.Count += visits;
                                }
                                else if (age >= 71 && age <= 80)
                                {
                                    if (!ageRange8.List.Contains(episode.AdmissionId))
                                    {
                                        ageRange8.List.Add(episode.AdmissionId);
                                    }
                                    ageRange8.Count += visits;
                                }
                                else if (age >= 81 && age <= 90)
                                {
                                    if (!ageRange9.List.Contains(episode.AdmissionId))
                                    {
                                        ageRange9.List.Add(episode.AdmissionId);
                                    }
                                    ageRange9.Count += visits;
                                }
                                else if (age >= 91 && age <= 1000)
                                {
                                    if (!ageRange10.List.Contains(episode.AdmissionId))
                                    {
                                        ageRange10.List.Add(episode.AdmissionId);
                                    }
                                    ageRange10.Count += visits;
                                }
                            }
                        }
                    });


                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "1.",
                        Description = "0-10 Years",
                        Patients = ageRange1.List.Count,
                        Visits = ageRange1.Count
                    });

                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "2.",
                        Description = "11-20 Years",
                        Patients = ageRange2.List.Count,
                        Visits = ageRange2.Count
                    });

                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "3.",
                        Description = "21-30 Years",
                        Patients = ageRange3.List.Count,
                        Visits = ageRange3.Count
                    });

                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "4.",
                        Description = "31-40 Years",
                        Patients = ageRange4.List.Count,
                        Visits = ageRange4.Count
                    });

                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "5.",
                        Description = "41-50 Years",
                        Patients = ageRange5.List.Count,
                        Visits = ageRange5.Count
                    });

                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "6.",
                        Description = "51-60 Years",
                        Patients = ageRange6.List.Count,
                        Visits = ageRange6.Count
                    });

                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "7.",
                        Description = "61-70 Years",
                        Patients = ageRange7.List.Count,
                        Visits = ageRange7.Count
                    });

                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "8.",
                        Description = "71-80 Years",
                        Patients = ageRange8.List.Count,
                        Visits = ageRange8.Count
                    });

                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "9.",
                        Description = "81-90 Years",
                        Patients = ageRange9.List.Count,
                        Visits = ageRange9.Count
                    });

                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "10.",
                        Description = "91 Years and Older",
                        Patients = ageRange10.List.Count,
                        Visits = ageRange10.Count
                    });

                    list.Add(new PatientsAndVisitsByAgeResult
                    {
                        LineNumber = "15.",
                        Description = "Total",
                        Patients = ageRange1.List.Count + ageRange2.List.Count + ageRange3.List.Count + ageRange4.List.Count + ageRange5.List.Count + ageRange6.List.Count + ageRange7.List.Count + ageRange8.List.Count + ageRange9.List.Count + ageRange10.List.Count,
                        Visits = ageRange1.Count + ageRange2.Count + ageRange3.Count + ageRange4.Count + ageRange5.Count + ageRange6.Count + ageRange7.Count + ageRange8.Count + ageRange9.Count + ageRange10.Count
                    });
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<DischargeByReasonResult> DischargesByReason(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<DischargeByReasonResult>();

            var admissionPeriods = Reports.GetDischargePatients(agencyId, agencyLocationId, startDate, endDate);

            try
            {
                string output = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("Discharge_{0}.txt", DateTime.Now.Ticks));
                using (System.IO.TextWriter textWriter = new System.IO.StreamWriter(output, false))
                {
                    if (admissionPeriods != null)
                    {
                        textWriter.WriteLine("Discharge Count: {0}", admissionPeriods.Count);
                    }

                    var hospital = GetDischargeReasonCount(admissionPeriods, 1);
                    textWriter.WriteLine("Hospital Count: {0}", hospital);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "41.",
                        Description = "Admitted to Hospital",
                        Discharges = hospital
                    });

                    var snFacility = GetDischargeReasonCount(admissionPeriods, 2);
                    textWriter.WriteLine("Admitted to SN/IC Facility: {0}", snFacility);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "42.",
                        Description = "Admitted to SN/IC Facility",
                        Discharges = snFacility
                    });

                    var death = GetDischargeReasonCount(admissionPeriods, 3);
                    textWriter.WriteLine("Death: {0}", death);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "43.",
                        Description = "Death",
                        Discharges = death
                    });

                    var family = GetDischargeReasonCount(admissionPeriods, 4);
                    textWriter.WriteLine("Family/Friends: {0}", family);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "44.",
                        Description = "Family/Friends Assummed Responsibility",
                        Discharges = family
                    });

                    var funds = GetDischargeReasonCount(admissionPeriods, 5);
                    textWriter.WriteLine("Lack of Funds: {0}", funds);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "45.",
                        Description = "Lack of Funds",
                        Discharges = funds
                    });

                    var progress = GetDischargeReasonCount(admissionPeriods, 6);
                    textWriter.WriteLine("Lack of Progress: {0}", progress);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "46.",
                        Description = "Lack of Progress",
                        Discharges = progress
                    });

                    var nofurther = GetDischargeReasonCount(admissionPeriods, 7);
                    textWriter.WriteLine("No Further Home Health Care Needed: {0}", nofurther);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "47.",
                        Description = "No Further Home Health Care Needed",
                        Discharges = nofurther
                    });

                    var outOfArea = GetDischargeReasonCount(admissionPeriods, 8);
                    textWriter.WriteLine("Patient Moved out of Area: {0}", outOfArea);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "48.",
                        Description = "Patient Moved out of Area",
                        Discharges = outOfArea
                    });

                    var refusedService = GetDischargeReasonCount(admissionPeriods, 9);
                    textWriter.WriteLine("Patient Refused Service: {0}", refusedService);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "49.",
                        Description = "Patient Refused Service",
                        Discharges = refusedService
                    });

                    var physicianRequest = GetDischargeReasonCount(admissionPeriods, 10);
                    textWriter.WriteLine("Physician Request: {0}", physicianRequest);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "50.",
                        Description = "Physician Request",
                        Discharges = physicianRequest
                    });

                    var transferAnother = GetDischargeReasonCount(admissionPeriods, 11);
                    textWriter.WriteLine("Transferred to Another HHA: {0}", transferAnother);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "51.",
                        Description = "Transferred to Another HHA",
                        Discharges = transferAnother
                    });

                    var personalCare = GetDischargeReasonCount(admissionPeriods, 12);
                    textWriter.WriteLine("Transferred to Home Care (Personal Care): {0}", personalCare);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "52.",
                        Description = "Transferred to Home Care (Personal Care)",
                        Discharges = personalCare
                    });

                    var transferHospice = GetDischargeReasonCount(admissionPeriods, 13);
                    textWriter.WriteLine("Transferred to Hospice: {0}", transferHospice);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "53.",
                        Description = "Transferred to Hospice",
                        Discharges = transferHospice
                    });

                    var outPatientRehab = GetDischargeReasonCount(admissionPeriods, 14);
                    textWriter.WriteLine("Transferred to Outpatient Rehabilitation: {0}", outPatientRehab);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "54.",
                        Description = "Transferred to Outpatient Rehabilitation",
                        Discharges = outPatientRehab
                    });

                    var other = GetDischargeReasonCount(admissionPeriods, 15);
                    textWriter.WriteLine("Other, Specify: {0}", other);
                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "59.",
                        Description = "Other, Specify:",
                        Discharges = other
                    });

                    list.Add(new DischargeByReasonResult
                    {
                        LineNumber = "60.",
                        Description = "Total",
                        Discharges = list.Sum(d => d.Discharges)
                    });
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<PrimaryPaymentSourceResult> VisitsByPrimaryPaymentSource(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var hmoVisits = 0;
            var noneVisits = 0;
            var otherVisits = 0;
            var medicalVisits = 0;
            var tricareVisits = 0;
            var medicareVisits = 0;
            var privatepayVisits = 0;
            var thirdpartyVisits = 0;
            var list = new List<PrimaryPaymentSourceResult>();

            try
            {
                string output = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("PaymentSource_{0}.txt", DateTime.Now.Ticks));
                using (System.IO.TextWriter textWriter = new System.IO.StreamWriter(output, false))
                {
                    var insurances = Reports.GetAgencyInsurances(agencyId);
                    var episodes = Reports.GetEpisodesExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodes != null && episodes.Count > 0)
                    {
                        textWriter.WriteLine("Total Episode Count: {0}", episodes.Count);
                        textWriter.WriteLine();

                        episodes.ForEach(episode =>
                        {
                            if (episode != null && episode.Schedule.IsNotNullOrEmpty() && episode.Details.IsNotNullOrEmpty())
                            {
                                textWriter.WriteLine("Patient: {0} ({1})", episode.DisplayName, episode.PatientId);
                                textWriter.WriteLine("Episode: {0} - {1}", episode.StartDate.ToShortDateString(), episode.EndDate.ToShortDateString());
                                textWriter.WriteLine("Admission Period Id: {0}", episode.AdmissionId);

                                var assessment = Reports.GetEpisodeAssessment(episode);
                                if (assessment != null)
                                {
                                    var visits = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                        && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                        && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                        .ToList().Count;
                                    textWriter.WriteLine("Visit Count: {0}", visits);
                                    var detail = episode.Details.ToObject<EpisodeDetail>();
                                    if (detail != null && detail.PrimaryInsurance.IsNotNullOrEmpty())
                                    {
                                        textWriter.WriteLine("Primary Insurance: {0}", detail.PrimaryInsurance);

                                        if (detail.PrimaryInsurance.IsEqual("1") || detail.PrimaryInsurance.IsEqual("2") || detail.PrimaryInsurance.IsEqual("3") || detail.PrimaryInsurance.IsEqual("4"))
                                        {
                                            medicareVisits += visits;
                                        }
                                        else
                                        {
                                            if (insurances != null && insurances.Count > 0 && detail.PrimaryInsurance.IsInteger())
                                            {
                                                var insurance = insurances.Find(i => i.Id == detail.PrimaryInsurance.ToInteger());
                                                if (insurance != null)
                                                {
                                                    textWriter.WriteLine("Primary Insurance Name: {0}", insurance.Name);
                                                    textWriter.WriteLine("Primary Insurance Payor Type: {0}", insurance.PayorType);

                                                    switch (insurance.PayorType)
                                                    {
                                                        case 5:
                                                            medicalVisits += visits;
                                                            break;
                                                        case 7:
                                                            tricareVisits += visits;
                                                            break;
                                                        case 8:
                                                            thirdpartyVisits += visits;
                                                            break;
                                                        case 10:
                                                            privatepayVisits += visits;
                                                            break;
                                                        case 2:
                                                        case 4:
                                                        case 9:
                                                            hmoVisits += visits;
                                                            break;
                                                        case 12:
                                                            noneVisits += visits;
                                                            break;
                                                        case 11:
                                                        case 14:
                                                            otherVisits += visits;
                                                            break;

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            textWriter.WriteLine();
                        });
                    }
                }

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "91.",
                    Description = "Medicare",
                    Visits = medicareVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "92.",
                    Description = "Medi-Cal",
                    Visits = medicalVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "93.",
                    Description = "TRICARE(CHAMPUS)",
                    Visits = tricareVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "94.",
                    Description = "Other Third Party (Insurance, etc.)",
                    Visits = thirdpartyVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "95.",
                    Description = "Private (Self Pay)",
                    Visits = privatepayVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "96.",
                    Description = "HMO/PPO (Includes Medicare and Medi-Cal HMOs)",
                    Visits = hmoVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "97.",
                    Description = "No Reimbursement",
                    Visits = noneVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "99.",
                    Description = "Other (Includes MSSP)",
                    Visits = otherVisits
                });

                list.Add(new PrimaryPaymentSourceResult
                {
                    LineNumber = "100.",
                    Description = "Total",
                    Visits = medicareVisits + medicalVisits + tricareVisits + thirdpartyVisits + privatepayVisits + hmoVisits + noneVisits + otherVisits
                });
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<VisitByStaffTypeResult> VisitsByStaffType(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var otVisits = 0;
            var ptVisits = 0;
            var snVisits = 0;
            var stVisits = 0;
            var hhaVisits = 0;
            var mswVisits = 0;
            var dietVisits = 0;

            var list = new List<VisitByStaffTypeResult>();

            try
            {
                var episodes = Reports.GetEpisodesExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                if (episodes != null && episodes.Count > 0)
                {
                    episodes.ForEach(episode =>
                    {
                        if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                        {
                            var assessment = Reports.GetEpisodeAssessment(episode);
                            if (assessment != null)
                            {
                                var hhaCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("HHA")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;
                                var dietianCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("Dietician")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;
                                var otCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("OT")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;
                                var ptCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("PT")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;
                                var snCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("Nursing")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;
                                var mswCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("MSW")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;
                                var stCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("ST")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;

                                hhaVisits += hhaCount;

                                dietVisits += dietianCount;

                                otVisits += otCount;

                                ptVisits += ptCount;

                                snVisits += snCount;

                                mswVisits += mswCount;

                                stVisits += stCount;
                            }
                        }
                    });
                }

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "71.",
                    Description = "Home Health Aide",
                    Visits = hhaVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "72.",
                    Description = "Nutritionist (Diet Counseling)",
                    Visits = dietVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "73.",
                    Description = "Occupational Therapist",
                    Visits = otVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "74.",
                    Description = "Physical Therapist",
                    Visits = ptVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "75.",
                    Description = "Physician",
                    Visits = 0
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "76.",
                    Description = "Skilled Nursing",
                    Visits = snVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "77.",
                    Description = "Social Worker",
                    Visits = mswVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "78.",
                    Description = "Speech Pathologist/Audiologist",
                    Visits = stVisits
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "79.",
                    Description = "Spiritual and Pastoral Care",
                    Visits = 0
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "84.",
                    Description = "Other",
                    Visits = 0
                });

                list.Add(new VisitByStaffTypeResult
                {
                    LineNumber = "85.",
                    Description = "Total",
                    Visits = hhaVisits + otVisits + ptVisits + snVisits + stVisits + mswVisits + dietVisits
                });
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<ReferralSourceResult> AdmissionsByReferralSource(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<ReferralSourceResult>();

            try
            {
                var others = 0;
                var admissionPeriods = Reports.GetPatientAdmissions(agencyId, startDate, endDate);

                var anotherAgencyAdmissions = GetAdmissionCount(admissionPeriods, 5);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "21.",
                    Description = "Another Home Health Agency",
                    Admissions = anotherAgencyAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "22.",
                    Description = "Clinic",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "23.",
                    Description = "Family / Friend",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "24.",
                    Description = "Hospice",
                    Admissions = 0
                });

                var hospitalAdmissions = GetAdmissionCount(admissionPeriods, 3);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "25.",
                    Description = "Hospital (Discharge Planner, etc)",
                    Admissions = hospitalAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "26.",
                    Description = "Local Health Department",
                    Admissions = 0
                });

                var snfAdmissions = GetAdmissionCount(admissionPeriods, 4);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "27.",
                    Description = "Long Term Care Facility (SN/IC)",
                    Admissions = snfAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "28.",
                    Description = "MSSP",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "29.",
                    Description = "Payer (Insurance, HMO etc.)",
                    Admissions = 0
                });

                var phyAdmissions = GetAdmissionCount(admissionPeriods, 2);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "30.",
                    Description = "Physician",
                    Admissions = phyAdmissions
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "31.",
                    Description = "Self",
                    Admissions = 0
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "32.",
                    Description = "Social Service Agency",
                    Admissions = 0
                });


                others = GetAdmissionCount(admissionPeriods, 1) + GetAdmissionCount(admissionPeriods, 6) + GetAdmissionCount(admissionPeriods, 7) + GetAdmissionCount(admissionPeriods, 8);
                list.Add(new ReferralSourceResult
                {
                    LineNumber = "34.",
                    Description = "Other",
                    Admissions = others
                });

                list.Add(new ReferralSourceResult
                {
                    LineNumber = "35.",
                    Description = "Total",
                    Admissions = hospitalAdmissions + snfAdmissions + phyAdmissions + anotherAgencyAdmissions + others
                });
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public List<PrincipalDiagnosisResult> PatientsVisitsByPrincipalDiagnosis(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var list = new List<PrincipalDiagnosisResult>();
            var line1 = new PatientVisit();
            var line2 = new PatientVisit();
            var line3 = new PatientVisit();
            var line4 = new PatientVisit();
            var line5 = new PatientVisit();
            var line6 = new PatientVisit();
            var line7 = new PatientVisit();
            var line8 = new PatientVisit();
            var line9 = new PatientVisit();
            var line10 = new PatientVisit();
            var line11 = new PatientVisit();
            var line12 = new PatientVisit();
            var line13 = new PatientVisit();
            var line14 = new PatientVisit();
            var line15 = new PatientVisit();
            var line16 = new PatientVisit();
            var line17 = new PatientVisit();
            var line18 = new PatientVisit();
            var line19 = new PatientVisit();
            var line20 = new PatientVisit();
            var line21 = new PatientVisit();
            var line22 = new PatientVisit();
            var line23 = new PatientVisit();
            var line24 = new PatientVisit();
            var line25 = new PatientVisit();
            var line26 = new PatientVisit();
            var line27 = new PatientVisit();
            var line28 = new PatientVisit();
            var line29 = new PatientVisit();
            var line30 = new PatientVisit();
            var line31 = new PatientVisit();
            var line32 = new PatientVisit();
            var line33 = new PatientVisit();
            var line34 = new PatientVisit();
            var lineTotal = new PatientVisit();
            var uniquePatients = new List<Guid>();
            var uniqueAdmissions = new List<Guid>();

            try
            {
                string output = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("PaymentSource_{0}.txt", DateTime.Now.Ticks));
                using (System.IO.TextWriter textWriter = new System.IO.StreamWriter(output, false))
                {
                    var episodes = Reports.GetEpisodesExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodes != null && episodes.Count > 0)
                    {
                        episodes.ForEach(episode =>
                        {
                            if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                            {
                                textWriter.WriteLine("Patient: {0} ({1})", episode.DisplayName, episode.PatientId);
                                textWriter.WriteLine("Episode: {0} - {1}", episode.StartDate.ToShortDateString(), episode.EndDate.ToShortDateString());
                                textWriter.WriteLine("Admission Period Id: {0}", episode.AdmissionId);

                                IDictionary<string, Question> assessmentData = null;
                                var episodeVisits = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                    .ToList().Count;

                                var assessment = Reports.GetEpisodeAssessment(episode);
                                if (assessment != null)
                                {
                                    assessmentData = assessment.ToDictionary();
                                    var principalDiagnosis = assessmentData.AnswerOrEmptyString("M1020ICD9M").ToLowerInvariant();

                                    var found = false;

                                    if (principalDiagnosis.IsNotNullOrEmpty())
                                    {
                                        textWriter.WriteLine("Principal Diagnosis: {0}", principalDiagnosis);
                                        if (!found)
                                        {
                                            var rangeList1 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 1.0, End = 41.9 },
                                                new DiagnosisRange { Start = 45.0, End = 139.8 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList1))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line1.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 1");
                                                }
                                                line1.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            if (IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 42.0))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line2.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 2");
                                                }
                                                line2.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList3 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 162.2, End = 162.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList3)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 197.0)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 209.21)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 231.2))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line3.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 3");
                                                }
                                                line3.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList4 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 174.0, End = 174.9 },
                                                new DiagnosisRange { Start = 175.0, End = 175.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList4)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 198.2)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 198.81)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 233.0))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line4.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 4");
                                                }
                                                line4.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {

                                            var rangeList5 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 152.0, End = 154.0 },
                                                new DiagnosisRange { Start = 209.0, End = 209.17 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList5)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 159.0)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 197.4)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 197.5)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 197.8)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 230.3)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 230.4)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 230.7))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line5.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 5");
                                                }
                                                line5.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList6 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 140.0, End = 209.36 },
                                                new DiagnosisRange { Start = 230.0, End = 234.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList6))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line6.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 6");
                                                }
                                                line6.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList7 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 209.40, End = 209.79 },
                                                new DiagnosisRange { Start = 210.0, End = 229.9 },
                                                new DiagnosisRange { Start = 235.0, End = 238.9 },
                                                new DiagnosisRange { Start = 239.0, End = 239.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList7))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line7.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 7");
                                                }
                                                line7.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList8 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 249.0, End = 250.93 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList8))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line8.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 8");
                                                }
                                                line8.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList9 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 240.0, End = 246.9 },
                                                new DiagnosisRange { Start = 251.0, End = 279.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList9))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line9.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 9");
                                                }
                                                line9.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList10 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 280.0, End = 289.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList10))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line10.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 10");
                                                }
                                                line10.Visits += episodeVisits;
                                            }
                                        }


                                        if (!found)
                                        {
                                            var rangeList11 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 290.0, End = 319 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList11))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line11.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 11");
                                                }
                                                line11.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            if (IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 331.0))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line12.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 12");
                                                }
                                                line12.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList13 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 320.0, End = 330.9 },
                                                new DiagnosisRange { Start = 331.11, End = 389.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList13))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line13.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 13");
                                                }
                                                line13.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList14 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 391.1, End = 392.0 },
                                                new DiagnosisRange { Start = 393.0, End = 402.91 },
                                                new DiagnosisRange { Start = 404.00, End = 429.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList14))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line14.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 14");
                                                }
                                                line14.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList15 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 430, End = 438.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList15))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line15.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 15");
                                                }
                                                line15.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList16 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 403.0, End = 403.91 },
                                                new DiagnosisRange { Start = 440, End = 459.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList16)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 390)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 392.9))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line16.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 16");
                                                }
                                                line16.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList17 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 460, End = 519.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList17))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line17.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 17");
                                                }
                                                line17.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList18 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 520.0, End = 579.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList18))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line18.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 18");
                                                }
                                                line18.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList19 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 580.0, End = 608.9 },
                                                new DiagnosisRange { Start = 614.0, End = 629.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList19))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line19.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 19");
                                                }
                                                line19.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList20 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 610.0, End = 611.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList20))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line20.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 20");
                                                }
                                                line20.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList21 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 630, End = 679.14 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList21))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line21.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 21");
                                                }
                                                line21.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList22 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 680.0, End = 709.9 }
                                            };


                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList22))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line22.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 22");
                                                }
                                                line22.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList23 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 710.0, End = 739.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList23))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line23.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 23");
                                                }
                                                line23.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList24 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 740.0, End = 779.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList24))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line24.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 24");
                                                }
                                                line24.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList25 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 780.01, End = 795.6 },
                                                new DiagnosisRange { Start = 796.0, End = 799.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList25)
                                                || IsDiagnosis(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, 795.79))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line25.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 25");
                                                }
                                                line25.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList26 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 800.00, End = 829.1 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList26))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line26.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 26");
                                                }
                                                line26.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList27 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 830.0, End = 959.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList27))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line27.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 27");
                                                }
                                                line27.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList28 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 960.0, End = 995.94 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList28))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line28.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 28");
                                                }
                                                line28.Visits += episodeVisits;
                                            }
                                        }

                                        if (!found)
                                        {
                                            var rangeList29 = new List<DiagnosisRange> { 
                                                new DiagnosisRange { Start = 996.00, End = 999.9 }
                                            };

                                            if (IsInDiagnosisRange(!principalDiagnosis.StartsWith("v") && principalDiagnosis.IsDouble() ? principalDiagnosis.ToDouble() : 0.0, rangeList29))
                                            {
                                                if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                {
                                                    line29.AdmissionIds.Add(episode.AdmissionId);
                                                    uniqueAdmissions.Add(episode.AdmissionId);
                                                    found = true;
                                                    textWriter.WriteLine("Found on Line 29");
                                                }
                                                line29.Visits += episodeVisits;
                                            }
                                        }

                                        if (principalDiagnosis.StartsWith("v"))
                                        {
                                            var vCode = principalDiagnosis.Replace("v", "").IsDouble() ? principalDiagnosis.Replace("v", "").ToDouble() : 0.0;
                                            if (!found)
                                            {
                                                var rangeList30 = new List<DiagnosisRange> { 
                                                    new DiagnosisRange { Start = 20.0, End = 26.9 },
                                                    new DiagnosisRange { Start = 28.0, End = 29.9 }
                                                };

                                                if (IsInDiagnosisRange(vCode, rangeList30))
                                                {
                                                    if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                    {
                                                        line30.AdmissionIds.Add(episode.AdmissionId);
                                                        uniqueAdmissions.Add(episode.AdmissionId);
                                                        found = true;
                                                        textWriter.WriteLine("Found on Line 30");
                                                    }
                                                    line30.Visits += episodeVisits;
                                                }
                                            }

                                            if (!found)
                                            {
                                                if (IsDiagnosis(vCode, 30.1)
                                                    || IsDiagnosis(vCode, 30.2)
                                                    || IsDiagnosis(vCode, 31.1)
                                                    || IsDiagnosis(vCode, 31.2)
                                                    || IsDiagnosis(vCode, 32.1)
                                                    || IsDiagnosis(vCode, 32.2)
                                                    || IsDiagnosis(vCode, 33.1)
                                                    || IsDiagnosis(vCode, 33.2)
                                                    || IsDiagnosis(vCode, 34.1)
                                                    || IsDiagnosis(vCode, 34.2)
                                                    || IsDiagnosis(vCode, 35.1)
                                                    || IsDiagnosis(vCode, 35.2)
                                                    || IsDiagnosis(vCode, 36.1)
                                                    || IsDiagnosis(vCode, 36.2)
                                                    || IsDiagnosis(vCode, 37.1)
                                                    || IsDiagnosis(vCode, 37.2)
                                                    || IsDiagnosis(vCode, 39.1)
                                                    || IsDiagnosis(vCode, 39.2)
                                                    )
                                                {
                                                    if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                    {
                                                        line31.AdmissionIds.Add(episode.AdmissionId);
                                                        uniqueAdmissions.Add(episode.AdmissionId);
                                                        found = true;
                                                        textWriter.WriteLine("Found on Line 31");
                                                    }
                                                    line31.Visits += episodeVisits;
                                                }
                                            }

                                            if (!found)
                                            {
                                                var rangeList32 = new List<DiagnosisRange> { 
                                                    new DiagnosisRange { Start = 01.0, End = 07.9 },
                                                    new DiagnosisRange { Start = 09.0, End = 19.8 },
                                                    new DiagnosisRange { Start = 40.0, End = 49.9 }
                                                };

                                                if (IsInDiagnosisRange(vCode, rangeList32))
                                                {
                                                    if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                    {
                                                        line32.AdmissionIds.Add(episode.AdmissionId);
                                                        uniqueAdmissions.Add(episode.AdmissionId);
                                                        found = true;
                                                        textWriter.WriteLine("Found on Line 32");
                                                    }
                                                    line32.Visits += episodeVisits;
                                                }
                                            }

                                            if (!found)
                                            {
                                                var rangeList33 = new List<DiagnosisRange> { 
                                                    new DiagnosisRange { Start = 50.0, End = 58.9 }
                                                };

                                                if (IsInDiagnosisRange(vCode, rangeList33))
                                                {
                                                    if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                    {
                                                        line33.AdmissionIds.Add(episode.AdmissionId);
                                                        uniqueAdmissions.Add(episode.AdmissionId);
                                                        found = true;
                                                        textWriter.WriteLine("Found on Line 33");
                                                    }
                                                    line33.Visits += episodeVisits;
                                                }
                                            }

                                            if (!found)
                                            {
                                                var rangeList34 = new List<DiagnosisRange> { 
                                                    new DiagnosisRange { Start = 60.0, End = 89.09 }
                                                };

                                                if (IsInDiagnosisRange(vCode, rangeList34))
                                                {
                                                    textWriter.WriteLine(string.Format("Found Within Range V60.0 to V89.09: {0}", vCode));
                                                    if (!uniqueAdmissions.Contains(episode.AdmissionId))
                                                    {
                                                        line34.AdmissionIds.Add(episode.AdmissionId);
                                                        uniqueAdmissions.Add(episode.AdmissionId);
                                                        found = true;
                                                        textWriter.WriteLine("Found on Line 34");
                                                    }
                                                    line34.Visits += episodeVisits;
                                                }
                                                else
                                                {
                                                    textWriter.WriteLine("{0} not found within range V60.0 to V89.09", vCode);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        textWriter.WriteLine("No Principal Diagnosis in Assessment");
                                    }
                                }
                                else
                                {
                                    textWriter.WriteLine("No Assessment/Diagnosis");
                                }
                                textWriter.WriteLine();
                            }
                        });


                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "1.",
                            Description = "Infectious and parasitic diseases (exclude HIV)",
                            Visits = line1.Visits,
                            Patients = line1.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "2.",
                            Description = "HIV infections",
                            Visits = line2.Visits,
                            Patients = line2.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "3.",
                            Description = "Malignant neoplasms: Lung",
                            Visits = line3.Visits,
                            Patients = line3.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "4.",
                            Description = "Malignant neoplasms: Breast",
                            Visits = line4.Visits,
                            Patients = line4.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "5.",
                            Description = "Malignant neoplasms: Intestines",
                            Visits = line5.Visits,
                            Patients = line5.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "6.",
                            Description = "Malignant neoplasms: All other sites, excluding those in lung, breast and intestines",
                            Visits = line6.Visits,
                            Patients = line6.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "7.",
                            Description = "Non-malignant neoplasms: All sites",
                            Visits = line7.Visits,
                            Patients = line7.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "8.",
                            Description = "Diabetes mellitus",
                            Visits = line8.Visits,
                            Patients = line8.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "9.",
                            Description = "Endocrine, metabolic, and nutritional diseases; Immunity disorders",
                            Visits = line9.Visits,
                            Patients = line9.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "10.",
                            Description = "Diseases of blood and blood forming organs",
                            Visits = line10.Visits,
                            Patients = line10.AdmissionIds.Count
                        });

                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "11.",
                            Description = "Mental disorder",
                            Visits = line11.Visits,
                            Patients = line11.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "12.",
                            Description = "Alzheimer's disease",
                            Visits = line12.Visits,
                            Patients = line12.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "13.",
                            Description = "Diseases of nervous system and sense organs",
                            Visits = line13.Visits,
                            Patients = line13.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "14.",
                            Description = "Diseases of cardiovascular system",
                            Visits = line14.Visits,
                            Patients = line14.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "15.",
                            Description = "Diseases of cerebrovascular system",
                            Visits = line15.Visits,
                            Patients = line15.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "16.",
                            Description = "Diseases of all other circulatory system",
                            Visits = line16.Visits,
                            Patients = line16.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "17.",
                            Description = "Diseases of respiratory system",
                            Visits = line17.Visits,
                            Patients = line17.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "18.",
                            Description = "Diseases of digestive system",
                            Visits = line18.Visits,
                            Patients = line18.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "19.",
                            Description = "Diseases of genitourinary system",
                            Visits = line19.Visits,
                            Patients = line19.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "20.",
                            Description = "Diseases of breast",
                            Visits = line20.Visits,
                            Patients = line20.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "21.",
                            Description = "Complications of pregnancy, childbirth, and the puerperium",
                            Visits = line21.Visits,
                            Patients = line21.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "22.",
                            Description = "Diseases of skin and subcutaneous tissue",
                            Visits = line22.Visits,
                            Patients = line22.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "23.",
                            Description = "Diseases of musculoskeletal system and connective tissue (include pathological fx, malunion fx, and nonunion fx)",
                            Visits = line23.Visits,
                            Patients = line23.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "24.",
                            Description = "Congenital anomalies and perinatal conditions (include birth fractures)",
                            Visits = line24.Visits,
                            Patients = line24.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "25.",
                            Description = "Symptoms, signs, and ill-defined conditions (exclude HIV positive test)",
                            Visits = line25.Visits,
                            Patients = line25.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "26.",
                            Description = "Fractures (exclude birth fx, pathological fx, malunion fx, nonunion fx)",
                            Visits = line26.Visits,
                            Patients = line26.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "27.",
                            Description = "All other injuries",
                            Visits = line27.Visits,
                            Patients = line27.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "28.",
                            Description = "Poisonings and adverse effects of external causes",
                            Visits = line28.Visits,
                            Patients = line28.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "29.",
                            Description = "Complications of surgical and medical care",
                            Visits = line29.Visits,
                            Patients = line29.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "30.",
                            Description = "Health services related to reproduction and development",
                            Visits = line30.Visits,
                            Patients = line30.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "31.",
                            Description = "Infants born outside hospital (infant care)",
                            Visits = line31.Visits,
                            Patients = line31.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "32.",
                            Description = "Health hazards related to communicable diseases",
                            Visits = line32.Visits,
                            Patients = line32.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "33.",
                            Description = "Other health services for specific procedures and aftercare",
                            Visits = line33.Visits,
                            Patients = line33.AdmissionIds.Count
                        });
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "34.",
                            Description = "Visits for Evaluation and Assessment",
                            Visits = line34.Visits,
                            Patients = line34.AdmissionIds.Count
                        });
                        lineTotal.Visits = line1.Visits + line2.Visits + line3.Visits + line4.Visits + line5.Visits + line6.Visits + line7.Visits + line8.Visits + line9.Visits + line10.Visits + line11.Visits + line12.Visits + line13.Visits + line14.Visits + line15.Visits + line16.Visits + line17.Visits + line18.Visits + line19.Visits + line20.Visits + line21.Visits + line22.Visits + line23.Visits + line24.Visits + line25.Visits + line26.Visits + line27.Visits + line28.Visits + line29.Visits + line30.Visits + line31.Visits + line32.Visits + line33.Visits + line34.Visits;
                        lineTotal.Patients = line1.AdmissionIds.Count + line2.AdmissionIds.Count + line3.AdmissionIds.Count + line4.AdmissionIds.Count + line5.AdmissionIds.Count + line6.AdmissionIds.Count + line7.AdmissionIds.Count + line8.AdmissionIds.Count + line9.AdmissionIds.Count + line10.AdmissionIds.Count + line11.AdmissionIds.Count + line12.AdmissionIds.Count + line13.AdmissionIds.Count + line14.AdmissionIds.Count + line15.AdmissionIds.Count + line16.AdmissionIds.Count + line17.AdmissionIds.Count + line18.AdmissionIds.Count + line19.AdmissionIds.Count + line20.AdmissionIds.Count + line21.AdmissionIds.Count + line22.AdmissionIds.Count + line23.AdmissionIds.Count + line24.AdmissionIds.Count + line25.AdmissionIds.Count + line26.AdmissionIds.Count + line27.AdmissionIds.Count + line28.AdmissionIds.Count + line29.AdmissionIds.Count + line30.AdmissionIds.Count + line31.AdmissionIds.Count + line32.AdmissionIds.Count + line33.AdmissionIds.Count + line34.AdmissionIds.Count;
                        list.Add(new PrincipalDiagnosisResult
                        {
                            LineNumber = "45.",
                            Description = "TOTAL",
                            Visits = lineTotal.Visits,
                            Patients = lineTotal.Patients
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return list;
        }

        public CostReportResult CostReport(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            var report = new CostReportResult();
            var snMedicarePatients = new List<Guid>();
            var ptMedicarePatients = new List<Guid>();
            var otMedicarePatients = new List<Guid>();
            var stMedicarePatients = new List<Guid>();
            var mswMedicarePatients = new List<Guid>();
            var hhaMedicarePatients = new List<Guid>();

            var snNonMedicarePatients = new List<Guid>();
            var ptNonMedicarePatients = new List<Guid>();
            var otNonMedicarePatients = new List<Guid>();
            var stNonMedicarePatients = new List<Guid>();
            var mswNonMedicarePatients = new List<Guid>();
            var hhaNonMedicarePatients = new List<Guid>();

            var medicareUnduplicated = new List<Guid>();
            var nonMedicareUnduplicated = new List<Guid>();

            int hhaMedicareMinutes = 0;
            int hhaNonMedicareMinutes = 0;

            try
            {
                var episodes = Reports.GetEpisodesExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                if (episodes != null && episodes.Count > 0)
                {
                    episodes.ForEach(episode =>
                    {
                        if (episode != null && episode.Schedule.IsNotNullOrEmpty() && episode.Details.IsNotNullOrEmpty())
                        {
                            var assessment = Reports.GetEpisodeAssessment(episode);
                            if (assessment != null)
                            {
                                var hhaCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.DisciplineTask == 54
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;
                                var otCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("OT")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;
                                var ptCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("PT")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;
                                var snCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("Nursing")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;
                                var mswCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("MSW")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;
                                var stCount = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("ST")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList().Count;

                                var hhaVisits = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                                                    && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                                                    && s.Discipline.IsEqual("HHA")
                                                    && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                                                    .ToList();

                                var detail = episode.Details.ToObject<EpisodeDetail>();
                                if (detail != null && detail.PrimaryInsurance.IsNotNullOrEmpty())
                                {
                                    if (detail.PrimaryInsurance.IsEqual("1") || detail.PrimaryInsurance.IsEqual("2") || detail.PrimaryInsurance.IsEqual("3") || detail.PrimaryInsurance.IsEqual("4"))
                                    {
                                        hhaVisits.ForEach(e =>
                                        {
                                            hhaMedicareMinutes += GetMinutesSpent(e);
                                        });

                                        if (!medicareUnduplicated.Contains(episode.PatientId))
                                        {
                                            medicareUnduplicated.Add(episode.PatientId);
                                        }

                                        report.SNMedicareVisits += snCount;
                                        if (snCount > 0)
                                        {
                                            if (!snMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                snMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }

                                        report.PTMedicareVisits += ptCount;
                                        if (ptCount > 0)
                                        {
                                            if (!ptMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                ptMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }

                                        report.OTMedicareVisits += otCount;
                                        if (otCount > 0)
                                        {
                                            if (!otMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                otMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }

                                        report.STMedicareVisits += stCount;
                                        if (stCount > 0)
                                        {
                                            if (!stMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                stMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }

                                        report.MSWMedicareVisits += mswCount;
                                        if (mswCount > 0)
                                        {
                                            if (!mswMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                mswMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }

                                        report.HHAMedicareVisits += hhaCount;
                                        if (hhaCount > 0)
                                        {
                                            if (!hhaMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                hhaMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }

                                    }
                                    else
                                    {
                                        hhaVisits.ForEach(e =>
                                        {
                                            hhaNonMedicareMinutes += GetMinutesSpent(e);
                                        });

                                        if (!nonMedicareUnduplicated.Contains(episode.PatientId))
                                        {
                                            nonMedicareUnduplicated.Add(episode.PatientId);
                                        }

                                        report.SNNonMedicareVisits += snCount;
                                        if (snCount > 0)
                                        {
                                            if (!snNonMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                snNonMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }

                                        report.PTNonMedicareVisits += ptCount;
                                        if (ptCount > 0)
                                        {
                                            if (!ptNonMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                ptNonMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }

                                        report.OTNonMedicareVisits += otCount;
                                        if (otCount > 0)
                                        {
                                            if (!otNonMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                otNonMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }

                                        report.STNonMedicareVisits += stCount;
                                        if (stCount > 0)
                                        {
                                            if (!stNonMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                stNonMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }

                                        report.MSWNonMedicareVisits += mswCount;
                                        if (mswCount > 0)
                                        {
                                            if (!mswNonMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                mswNonMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }

                                        report.HHANonMedicareVisits += hhaCount;
                                        if (hhaCount > 0)
                                        {
                                            if (!hhaNonMedicarePatients.Contains(episode.AdmissionId))
                                            {
                                                hhaNonMedicarePatients.Add(episode.AdmissionId);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });

                    report.SNMedicarePatients = snMedicarePatients.Count;
                    report.PTMedicarePatients = ptMedicarePatients.Count;
                    report.OTMedicarePatients = otMedicarePatients.Count;
                    report.STMedicarePatients = stMedicarePatients.Count;
                    report.MSWMedicarePatients = mswMedicarePatients.Count;
                    report.HHAMedicarePatients = hhaMedicarePatients.Count;

                    report.SNNonMedicarePatients = snNonMedicarePatients.Count;
                    report.PTNonMedicarePatients = ptNonMedicarePatients.Count;
                    report.OTNonMedicarePatients = otNonMedicarePatients.Count;
                    report.STNonMedicarePatients = stNonMedicarePatients.Count;
                    report.MSWNonMedicarePatients = mswNonMedicarePatients.Count;
                    report.HHANonMedicarePatients = hhaNonMedicarePatients.Count;

                    report.SNTotalPatients = report.SNMedicarePatients + report.SNNonMedicarePatients;
                    report.PTTotalPatients = report.PTMedicarePatients + report.PTNonMedicarePatients;
                    report.OTTotalPatients = report.OTMedicarePatients + report.OTNonMedicarePatients;
                    report.STTotalPatients = report.STMedicarePatients + report.STNonMedicarePatients;
                    report.MSWTotalPatients = report.MSWMedicarePatients + report.MSWNonMedicarePatients;
                    report.HHATotalPatients = report.HHAMedicarePatients + report.HHANonMedicarePatients;

                    report.MedicareUnduplicated = medicareUnduplicated.Count;
                    report.NonMedicareUnduplicated = nonMedicareUnduplicated.Count;
                    report.TotalUnduplicated = report.MedicareUnduplicated + report.NonMedicareUnduplicated;

                    report.HHAMedicareHours = Convert.ToInt32(hhaMedicareMinutes / 60);
                    report.HHANonMedicareHours = Convert.ToInt32(hhaNonMedicareMinutes / 60);
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return report;
        }

        public List<Dictionary<string, string>> TherapyManagement(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            List<Dictionary<string, string>> collections = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                if (agencyData != null)
                {
                    var episodes = Reports.GetEpisodesExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodes != null && episodes.Count > 0)
                    {
                        episodes.ForEach((EpisodeData episode) =>
                        {
                            IDictionary<string, Question> assessmentData = null;
                            var assessment = Reports.GetEpisodeAssessment(episode);
                            string therapyNeed = "";
                            if (assessment != null)
                            {
                                assessmentData = assessment.ToDictionary();

                                if (assessmentData != null && assessmentData.ContainsKey("M2200NumberOfTherapyNeed") && assessmentData["M2200NumberOfTherapyNeed"] != null)
                                {
                                    therapyNeed = assessmentData["M2200NumberOfTherapyNeed"].Answer;
                                }
                            }
                            var collection = new Dictionary<string, string>();

                            collection.Add("MRN", episode.MRN.IsNotNullOrEmpty() ? episode.MRN : string.Empty);
                            collection.Add("Patient", episode.DisplayName);
                            collection.Add("Episode", episode.StartDate.ToZeroFilled() + " - " + episode.EndDate.ToZeroFilled());
                            collection.Add("Assessment", assessment != null && assessment.TypeDescription.IsNotNullOrEmpty() ? assessment.TypeDescription : string.Empty);
                            collection.Add("M2200", therapyNeed);

                            int otCount = 0;
                            int ptCount = 0;
                            int stCount = 0;
                            int otCompletedCount = 0;
                            int ptCompletedCount = 0;
                            int stCompletedCount = 0;
                            if (episode.Schedule.IsNotNullOrEmpty())
                            {
                                var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                scheduleEvents.ForEach(s =>
                                {
                                    if (!s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsMissedVisit && s.IsBillable && !s.IsDeprecated)
                                    {
                                        if (s.IsPTNote())
                                        {
                                            if (s.IsCompleted())
                                            {
                                                ptCompletedCount++;
                                            }
                                            ptCount++;
                                        }
                                        else if (s.IsOTNote())
                                        {
                                            if (s.IsCompleted())
                                            {
                                                otCompletedCount++;
                                            }
                                            otCount++;
                                        }
                                        else if (s.IsSTNote())
                                        {
                                            if (s.IsCompleted())
                                            {
                                                stCompletedCount++;
                                            }
                                            stCount++;
                                        }
                                    }
                                });
                            }
                            collection.Add("Completed PT Visits", ptCompletedCount.ToString());
                            collection.Add("Scheduled PT Visits", ptCount.ToString());
                            collection.Add("Completed OT Visits", otCompletedCount.ToString());
                            collection.Add("Scheduled OT Visits", otCount.ToString());
                            collection.Add("Completed ST Visits", stCompletedCount.ToString());
                            collection.Add("Scheduled ST Visits", stCount.ToString());
                            int totalScheduled = ptCount + otCount + stCount;
                            int totalCompleted = ptCompletedCount + otCompletedCount + stCompletedCount;
                            collection.Add("Total Completed Visits", totalCompleted.ToString());
                            collection.Add("Total Scheduled Visits", totalScheduled.ToString());
                            collections.Add(collection);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return collections;
        }

        

        public List<Dictionary<string, string>> PatientListReport(Guid agencyId, Guid agencyLocationId, int StatusId)
        {
            List<Dictionary<string, string>> collections = new List<Dictionary<string, string>>();
            Dictionary<string, string> collection = null;
            try
            {
                var patientsData = Reports.GetPatientsList(agencyId, agencyLocationId, StatusId).OrderBy(x => x.FirstName).ThenBy(x => x.LastName).ToList(); ;
                if (patientsData != null && patientsData.Count > 0)
                {
                    #region GetOtherData
                    List<int> insuranceIdsList = new List<int>();
                    List<Guid> userIdsList = new List<Guid>();
                    List<Guid> patientIdsList = new List<Guid>();
                    patientsData.ForEach(patientData =>
                    {
                        if (patientData.PrimaryInsuranceNumber >= 1000)
                        {
                            insuranceIdsList.Add(patientData.PrimaryInsuranceNumber);
                        }
                        if (patientData.UserId != null && patientData.UserId.IsNotEmpty())
                        {
                            userIdsList.Add(patientData.UserId);
                        }
                        if (patientData.CaseManagerId != null && patientData.CaseManagerId.IsNotEmpty())
                        {
                            userIdsList.Add(patientData.CaseManagerId);
                        }
                        if (patientData.Id != null && patientData.Id.IsNotEmpty())
                        {
                            patientIdsList.Add(patientData.Id);
                        }
                    });
                    insuranceIdsList = insuranceIdsList.Distinct().ToList();
                    userIdsList = userIdsList.Distinct().ToList();
                    patientIdsList = patientIdsList.Distinct().ToList();
                    #endregion
                    var physicianIdsWithNameAndNPI = Reports.GetPhysiciansNameAndNPI(agencyId, patientIdsList);//.ToList());
                    var userIdsWithName = Reports.GetUserNamesByIds(agencyId, userIdsList);
                    Dictionary<string, object> primaryInsuranceNamesWithIds = null;
                    if (insuranceIdsList.Count > 0)
                    {
                        primaryInsuranceNamesWithIds = Reports.GetAgencyInsuranceNamesWithIds(agencyId, insuranceIdsList);
                    }
                    var isInsuranceExist = primaryInsuranceNamesWithIds != null && primaryInsuranceNamesWithIds.Count > 0;
                    #region TempVariables
                    Axxess.AgencyManagement.Domain.AgencyPhysician tempPhysician = null;
                    string tempPhysicianName = default(string);
                    string tempPhysicianNPI = default(string);
                    string tempPhysicianPhone = default(string);
                    string tempCaseManagerName = default(string);
                    string tempClinicianName = default(string);
                    string tempInsuranceName = default(string);
                    string tempDme = default(string);
                    string tempDnr = default(string);
                    string tempEthnicity = default(string);
                    string tempPaymentSource = default(string);
                    #endregion
                    if (patientsData != null && patientsData.Count > 0)
                    {

                        patientsData.ForEach((PatientList patientList) =>
                        {
                            #region Process
                            if (patientList.Id.IsNotEmpty())
                            {
                                if (physicianIdsWithNameAndNPI.TryGetValue(patientList.Id.ToString(), out tempPhysician))
                                {
                                    if (tempPhysician != null)
                                    {
                                        tempPhysicianName = tempPhysician.LastName + ", " + tempPhysician.FirstName + " " + tempPhysician.MiddleName;
                                        tempPhysicianNPI = tempPhysician.NPI;
                                        tempPhysicianPhone = tempPhysician.PhoneWork;
                                    }
                                }
                            }
                            if (patientList.CaseManagerId.IsNotEmpty())
                            {
                                if (userIdsWithName.ContainsKey(patientList.CaseManagerId.ToString()))
                                {
                                    tempCaseManagerName = string.Format("{0}", userIdsWithName[patientList.CaseManagerId.ToString()]);
                                }
                            }
                            if (patientList.UserId.IsNotEmpty())
                            {
                                if (userIdsWithName.ContainsKey(patientList.UserId.ToString()))
                                {
                                    tempClinicianName = string.Format("{0}", userIdsWithName[patientList.UserId.ToString()]);
                                }
                            }
                            if (patientList.PrimaryInsuranceNumber != default(int))
                            {
                                if (patientList.PrimaryInsuranceNumber < 1000)
                                {
                                    tempInsuranceName = Reports.GetLookUpInsurance(patientList.PrimaryInsuranceNumber.ToString());
                                }
                                else if (isInsuranceExist && patientList.PrimaryInsuranceNumber >= 1000)
                                {
                                    if (primaryInsuranceNamesWithIds.ContainsKey(patientList.PrimaryInsuranceNumber.ToString()))
                                    {
                                        tempInsuranceName = string.Format("{0}", primaryInsuranceNamesWithIds[patientList.PrimaryInsuranceNumber.ToString()]);
                                    }
                                }
                            }
                            tempDnr = Reports.GetDnr(patientList.DNR.ToString());
                            if (patientList.DME.IsNotNullOrEmpty())
                            {
                                tempDme = Reports.GetDme(patientList.DME);
                            }
                            if (patientList.Ethnicities.IsNotNullOrEmpty())
                            {
                                tempEthnicity = Reports.GetEthnicities(patientList.Ethnicities);
                            }
                            if (patientList.PaymentSource.IsNotNullOrEmpty())
                            {
                                tempPaymentSource = Reports.GetPaymentSources(patientList.PaymentSource);
                            }
                            #endregion
                            collection = new Dictionary<string, string>();
                            collection.Add("MRN", patientList.PatientIdNumber.IsNotNullOrEmpty() ? patientList.PatientIdNumber : string.Empty);
                            collection.Add("First Name", patientList.FirstName.IsNotNullOrEmpty() ? patientList.FirstName.ToUpperCase() : string.Empty);
                            collection.Add("Last Name", patientList.LastName.IsNotNullOrEmpty() ? patientList.LastName.ToUpperCase() : string.Empty);
                            collection.Add("Middle Initial", patientList.MiddleInitial.IsNotNullOrEmpty() ? patientList.MiddleInitial.ToUpperCase() : string.Empty);
                            collection.Add("Start of Care Date", patientList.StartofCareDate.IsValid() ? patientList.StartofCareDate.ToString() : string.Empty);
                            collection.Add("Episode Start Date", patientList.EpisodeStartDate.IsValid() ? patientList.EpisodeStartDate.ToString() : string.Empty);
                            collection.Add("Payment Source", tempPaymentSource);
                            collection.Add("Primary Insurance", tempInsuranceName);
                            collection.Add("Medicare #", patientList.MedicareNumber.IsNotNullOrEmpty() ? patientList.MedicareNumber : string.Empty);
                            collection.Add("Medicaid #", patientList.MedicaidNumber.IsNotNullOrEmpty() ? patientList.MedicaidNumber : string.Empty);
                            collection.Add("Address Line", patientList.AddressLine1.IsNotNullOrEmpty() ? patientList.AddressLine1 : string.Empty);
                            collection.Add("Address Line2", patientList.AddressLine2.IsNotNullOrEmpty() ? patientList.AddressLine2 : string.Empty);
                            collection.Add("Address City", patientList.AddressCity.IsNotNullOrEmpty() ? patientList.AddressCity : string.Empty);
                            collection.Add("Address StateCode", patientList.AddressStateCode.IsNotNullOrEmpty() ? patientList.AddressStateCode : string.Empty);
                            collection.Add("Address ZipCode", patientList.AddressZipCode.IsNotNullOrEmpty() ? patientList.AddressZipCode : string.Empty);
                            collection.Add("Home Phone", patientList.PhoneHome.IsNotNullOrEmpty() ? patientList.PhoneHome : string.Empty);
                            collection.Add("Mobile Phone", patientList.PhoneMobile.IsNotNullOrEmpty() ? patientList.PhoneMobile : string.Empty);
                            collection.Add("Email", patientList.EmailAddress.IsNotNullOrEmpty() ? patientList.EmailAddress : string.Empty);
                            collection.Add("Physician", tempPhysicianName);
                            collection.Add("Physician NPI", tempPhysicianNPI);
                            collection.Add("Physician Phone", tempPhysicianPhone);
                            collection.Add("SSN", patientList.SSN.IsNotNullOrEmpty() ? patientList.SSN : string.Empty);
                            collection.Add("DOB", patientList.DOB.IsValid() ? patientList.DOB.ToShortDateString() : string.Empty);
                            collection.Add("Marital Status", patientList.MaritalStatus.IsNotNullOrEmpty() ? patientList.MaritalStatus : string.Empty);
                            collection.Add("DNR", tempDnr);
                            collection.Add("Ethnicity", tempEthnicity);
                            collection.Add("Case Manager", tempCaseManagerName);
                            collection.Add("Clinician Assigned", tempClinicianName);
                            collection.Add("DME", tempDme);
                            collection.Add("Comments", patientList.Comments.IsNotNullOrEmpty() ? patientList.Comments : string.Empty);
                            collections.Add(collection);
                            #region Reset
                            tempPhysician = null;
                            tempPhysicianName = default(string);
                            tempPhysicianNPI = default(string);
                            tempPhysicianPhone = default(string);
                            tempCaseManagerName = default(string);
                            tempClinicianName = default(string);
                            tempInsuranceName = default(string);
                            tempDme = default(string);
                            tempDnr = default(string);
                            tempEthnicity = default(string);
                            tempPaymentSource = default(string);
                            #endregion
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }
            return collections;
        }

        public List<Dictionary<string, string>> HHRGReport(Guid agencyId, Guid agencyLocationId, DateTime startDate, DateTime endDate)
        {
            List<Dictionary<string, string>> collections = new List<Dictionary<string, string>>();
            try
            {
                var agencyData = Reports.GetAgencyLocation(agencyId, agencyLocationId);
                var agencyLocationMedicareInsurance = Reports.GetMedicareInsurance(agencyData.Payor);
                if (agencyData != null)
                {
                    var episodes = Reports.GetEpisodesStartDateExactlyBetween(agencyId, agencyLocationId, startDate, endDate);
                    if (episodes != null && episodes.Count > 0)
                    {
                        episodes.ForEach((EpisodeData episode) =>
                        {
                            ProspectivePayment prospectivePayment = null;
                            IDictionary<string, Question> assessmentData = null;
                            var assessment = Reports.GetEpisodeAssessment(episode);
                            if (assessment != null)
                            {
                                assessmentData = assessment.ToDictionary();
                                prospectivePayment = GetProspectivePayment(assessment.HippsCode, episode.StartDate, episode.AddressZipCode.IsNotNullOrEmpty() ? episode.AddressZipCode : agencyData.AddressZipCode.IsNotNullOrEmpty() ? agencyData.AddressZipCode : string.Empty);
                            }
                            EpisodeDetail episodeDetail = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : null;
                            var collection = new Dictionary<string, string>();
                            collection.Add("MRN", episode.MRN.IsNotNullOrEmpty() ? episode.MRN : string.Empty);
                            collection.Add("Patient", episode.DisplayName);

                            int primaryInsuranceId = episodeDetail != null && episodeDetail.PrimaryInsurance.IsNotNullOrEmpty() ?
                               episodeDetail.PrimaryInsurance.ToInteger() : 0;

                            if (primaryInsuranceId > 1000)
                            {
                                List<InsuranceData> insurances = new List<InsuranceData>();
                                if (agencyData.IsLocationStandAlone)
                                {
                                    insurances = Reports.GetAgencyInsurancesByBranch(agencyId, agencyLocationId);
                                }
                                else
                                {
                                    insurances = Reports.GetAgencyInsurances(agencyId);
                                }
                                var primaryInsurance = insurances.FirstOrDefault(p => p.Id == primaryInsuranceId);
                                collection.Add("Insurance", primaryInsurance != null ? primaryInsurance.Name : string.Empty);
                            }
                            else
                            {
                                collection.Add("Insurance", agencyLocationMedicareInsurance.Name);
                            }
                            collection.Add("Admission Date", episode.StartofCareDate.IsValid() && episode.StartofCareDate != DateTime.MinValue ? episode.StartofCareDate.ToShortDateString() : string.Empty);
                            collection.Add("Episode Timing", GetEpisodeTiming(assessmentData));
                            collection.Add("Episode", episode.StartDate.ToZeroFilled() + " - " + episode.EndDate.ToZeroFilled());
                            collection.Add("Status", episode.Status == 1 ? "Active" : episode.Status == 2 ? "Discharged" : string.Empty);
                            collection.Add("Discharge Date", episode.Status == 2 ? episode.DischargeDate.ToZeroFilled() : string.Empty);
                            collection.Add("Original Therapy Visit", assessmentData != null && assessmentData.AnswerOrEmptyString("M2200NumberOfTherapyNeed").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M2200NumberOfTherapyNeed").IsInteger() ? assessmentData.AnswerOrEmptyString("M2200NumberOfTherapyNeed").ToInteger().ToString() : string.Empty);
                            collection.Add("Original HIPPS", assessment != null ? assessment.HippsCode : string.Empty);
                            collection.Add("Original HHRG", prospectivePayment != null ? prospectivePayment.Hhrg : string.Empty);
                            collection.Add("Original Prospective Pay", prospectivePayment != null ? prospectivePayment.TotalAmount.ToString() : string.Empty);

                            int completedTherapyVisits = GetCompletedTherapyVisits(episode).ToInteger();
                            string totalVisits = GetScheduledTherapyVisits(episode);

                            Hipps hippsCode = null;
                            prospectivePayment = null;
                            var completedTherapyText = completedTherapyVisits.ToString().PadLeft(3, '0');

                            if (assessmentData != null)
                            {
                                string oasisFormat = string.Empty;
                                if (assessment.SubmissionFormat.IsNotNullOrEmpty())
                                {
                                    oasisFormat = assessment.SubmissionFormat.Remove(864, 3);
                                    oasisFormat = oasisFormat.Insert(864, completedTherapyText);
                                }
                                hippsCode = GrouperAgent.GetHippsCode(oasisFormat);
                                if (hippsCode != null)
                                {
                                    prospectivePayment = GetProspectivePayment(hippsCode.Code, episode.StartDate, episode.AddressZipCode.IsNotNullOrEmpty() ? episode.AddressZipCode : agencyData.AddressZipCode.IsNotNullOrEmpty() ? agencyData.AddressZipCode : string.Empty);
                                }
                            }
                            collection.Add("Actual Completed Therapy Visits", completedTherapyVisits.ToString());
                            collection.Add("Actual Scheduled Therapy Visits", totalVisits);
                            collection.Add("Actual HIPPS", hippsCode != null ? hippsCode.Code : string.Empty);
                            collection.Add("Actual HHRG", prospectivePayment != null ? prospectivePayment.Hhrg : string.Empty);
                            collection.Add("Actual Prospective Pay", prospectivePayment != null ? prospectivePayment.TotalAmount.ToString() : string.Empty);

                            collections.Add(collection);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collections;
        }

        public List<Dictionary<string, string>> EmployeeListReport(Guid agencyId, Guid agencyLocationId, int statusId)
        {
            List<Dictionary<string, string>> collections = new List<Dictionary<string, string>>();
            try
            {
                var agencyUsers = Reports.GetUsers(agencyId, agencyLocationId, statusId);
                if (agencyUsers != null && agencyUsers.Count > 0)
                {
                    agencyUsers.ForEach((EmployeeList user) =>
                    {
                        var collection = new Dictionary<string, string>();
                        var userProfile = user.Profile.IsNotNullOrEmpty() ? user.Profile.ToObject<EmployeeProfile>() : new EmployeeProfile();
                        collection.Add("Employee ID", user.CustomId.IsNotNullOrEmpty() ? user.CustomId : string.Empty);
                        collection.Add("First Name", user.FirstName);
                        collection.Add("Last Name", user.LastName);
                        if (user.Credentials == "Other")
                        {
                            user.Credentials = user.CredentialsOther.IsNotNullOrEmpty() ? user.CredentialsOther : string.Empty;
                        }

                        collection.Add("Credentials", user.Credentials.IsNotNullOrEmpty() ? user.Credentials : string.Empty);

                        if (user.TitleType == "Other")
                        {
                            user.TitleType = user.TitleTypeOther.IsNotNullOrEmpty() ? user.TitleTypeOther : string.Empty;
                        }
                        collection.Add("Title", user.TitleType.IsNotNullOrEmpty() ? user.TitleType : string.Empty);
                        collection.Add("Employment Type", user.EmploymentType.IsNotNullOrEmpty() ? user.EmploymentType : string.Empty);
                        collection.Add("DOB", user.DOB.ToShortDateString() != "1/1/0001" ? user.DOB.ToShortDateString().ToSafeString() : string.Empty);
                        collection.Add("SSN", user.SSN.IsNotNullOrEmpty() ? user.SSN : string.Empty);

                        if (userProfile != null)
                        {
                            string AddressFull = string.Format("{0} {1} {2} {3} {4}", userProfile.AddressLine1.IsNotNullOrEmpty() ? userProfile.AddressLine1.Trim() : string.Empty, userProfile.AddressLine2 != null ? userProfile.AddressLine2.Trim() : string.Empty, userProfile.AddressCity != null ? userProfile.AddressCity.Trim() : string.Empty, userProfile.AddressStateCode != null ? userProfile.AddressStateCode.Trim() : string.Empty, userProfile.AddressZipCode != null ? userProfile.AddressZipCode.Trim() : string.Empty);
                            collection.Add("Gender", userProfile.Gender.IsNotNullOrEmpty() ? userProfile.Gender : string.Empty);
                            collection.Add("Address", AddressFull.IsNotNullOrEmpty() ? AddressFull : string.Empty);
                            collection.Add("Home Phone", userProfile.PhoneHome.IsNotNullOrEmpty() ? userProfile.PhoneHome.ToPhone() : string.Empty);
                            collection.Add("Cell Phone", userProfile.PhoneMobile.IsNotNullOrEmpty() ? userProfile.PhoneMobile.ToPhone() : string.Empty);
                            collection.Add("Email", userProfile.EmailWork.IsNotNullOrEmpty() ? userProfile.EmailWork : string.Empty);
                            collection.Add("Fax Number", userProfile.PhoneFax.IsNotNullOrEmpty() ? userProfile.PhoneFax : string.Empty);
                        }
                        var roleDescriptions = new StringBuilder();
                        string[] userRoles = user.Roles.Trim().Split(';');
                        userRoles.ForEach(userRole =>
                        {
                            if (userRole.IsNotNullOrEmpty())
                            {
                                roleDescriptions.AppendFormat("{0};", userRole.ToEnum<EmployeeRoles>(EmployeeRoles.OfficeStaff).GetDescription());
                            }
                        });

                        collection.Add("Role", roleDescriptions.ToString());
                        collections.Add(collection);
                    });
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collections;
        }

        public List<Dictionary<string, string>> UnbilledVisitsForManagedClaims(Guid agencyId, Guid branchId, int insurance, int status, DateTime startDate, DateTime endDate)
        {
            List<Dictionary<string, string>> collections = new List<Dictionary<string, string>>();
            try
            {
                var episodes = Reports.GetEpisodesBetweenByNonMedicareInsurance(agencyId, branchId, insurance, startDate, endDate);
                var managedClaims = Reports.GetManagedClaimsByInsurance(agencyId, branchId, insurance, startDate, endDate);
                foreach (var episode in episodes)
                {
                    if (episode.Schedule.IsNotNullOrEmpty())
                    {
                        var scheduledVisits = episode.Schedule.ToObject<List<ScheduleEvent>>();
                        foreach (ScheduleEvent visit in scheduledVisits)
                        {
                            if ((visit.VisitDate.IsValidDate() || visit.EventDate.IsValidDate()) &&
                                visit.IsBillable && !visit.IsDeprecated && !visit.IsMissedVisit && (status == 0 || status == visit.Status.ToInteger()))
                            {
                                DateTime visitDate = visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime() : visit.EventDate.ToDateTime();
                                //If the visit is not within the date filter parameters of the report then it will be skipped
                                if (!visitDate.IsBetween(startDate, endDate))
                                {
                                    continue;
                                }
                                bool billed = false;
                                foreach (var claim in managedClaims)
                                {
                                    if (claim.PatientId == visit.PatientId)
                                    {
                                        if (visitDate.IsBetween(claim.EpisodeStartDate, claim.EpisodeEndDate))
                                        {
                                            billed = true;
                                            break;
                                        }
                                    }
                                }
                                if (!billed)
                                {
                                    var collection = new Dictionary<string, string>();
                                    collection.Add("MRN", episode.MRN);
                                    collection.Add("Patient", episode.DisplayName);
                                    collection.Add("Visit", visit.DisciplineTask.ToString());
                                    collection.Add("Status", visit.Status.ToString());
                                    collection.Add("Employee", visit.UserId.ToString());
                                    collection.Add("Date", visitDate.ToShortDateString());
                                    collection.Add("Episode Range", string.Format("{0} - {1}", episode.StartDate.ToZeroFilled(), episode.EndDate.ToZeroFilled()));
                                    if (insurance == 0)
                                    {
                                        collection.Add("Primary Insurance", episode.PrimaryInsurance);
                                        collection.Add("Secondary Insurance", episode.SecondaryInsurance);
                                        collection.Add("Tertiary Insurance", episode.TertiaryInsurance);
                                    }
                                    collections.Add(collection);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return collections;
        }

        #endregion

        #region Private Method

        private int GetMinutesSpent(ScheduleEvent scheduleEvent)
        {
            var timeIn = DateTime.Now;
            var timeOut = DateTime.Now;
            if (scheduleEvent.TimeOut.IsNotNullOrEmpty() && scheduleEvent.TimeOut.HourToDateTime(ref timeOut) && scheduleEvent.TimeIn.IsNotNullOrEmpty() && scheduleEvent.TimeIn.HourToDateTime(ref timeIn))
            {
                if (!timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")))
                {
                    var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                    var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                    return outi + outO;
                }
                else if (timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")) && (timeOut.Hour * 60 + timeOut.Minute) < (timeIn.Hour * 60 + timeIn.Minute))
                {
                    var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                    var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                    return outi + outO + 12 * 60;

                }
                else
                {
                    if (timeOut >= timeIn)
                    {
                        return (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);
                    }
                }
            }
            return 0;
        }

        private bool IsDiagnosis(double principalDiagnosis, double diagnosis)
        {
            bool result = false;

            if (principalDiagnosis == diagnosis)
            {
                result = true;
            }
            return result;
        }

        private bool IsInDiagnosisRange(double principalDiagnosis, List<DiagnosisRange> rangeList)
        {
            bool result = false;

            foreach (var range in rangeList)
            {
                if (principalDiagnosis >= range.Start && principalDiagnosis <= range.End)
                {
                    return true;
                }
            }
            return result;
        }

        private AgeRange GetAgeRange(List<EpisodeData> episodes, int start, int end)
        {
            var result = new AgeRange { List = new List<Guid>() };
            if (episodes != null)
            {
                episodes.ForEach(episode =>
                {
                    if (episode != null && episode.DOB != DateTime.MinValue)
                    {
                        int age = DateTime.Today.Year - episode.DOB.Year;
                        if (age >= start && age <= end)
                        {
                            if (!result.List.Contains(episode.AdmissionId))
                            {
                                result.List.Add(episode.AdmissionId);
                            }
                        }
                    }
                });
            }
            return result;
        }

        private AgeRange GetAgeRange(List<AdmissionPeriod> periods, int start, int end)
        {
            var result = new AgeRange { List = new List<Guid>() };
            if (periods != null)
            {
                periods.ForEach(period =>
                {
                    var patient = period.PatientData.IsNotNullOrEmpty() ? period.PatientData.ToObject<Patient>() : null;
                    if (patient != null && patient.DOB != DateTime.MinValue)
                    {
                        int age = DateTime.Today.Year - patient.DOB.Year;
                        if (age >= start && age <= end)
                        {
                            result.Count++;
                            result.List.Add(period.Id);
                        }
                    }
                });
            }
            return result;
        }

        private int GetVisitsByAgeRange(List<Guid> periods, DateTime startDate, DateTime endDate)
        {
            var result = 0;
            if (periods != null && periods.Count > 0)
            {
                var episodes = Reports.GetEpisodesByAdmissionPeriodId(periods);
                episodes.ForEach(episode =>
                {
                    if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                    {
                        result += episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty()
                            && s.EventDate.IsValidDate() && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable
                            && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date)
                            .ToList().Count;
                    }
                });
            }

            return result;
        }

        private int GetAdmissionCount(List<AdmissionPeriod> periods, int admissionSourceType)
        {
            var result = 0;
            if (periods != null)
            {
                periods.ForEach(period =>
                {
                    var patient = period.PatientData.IsNotNullOrEmpty() ? period.PatientData.ToObject<Patient>() : null;
                    if (patient != null && patient.AdmissionSource == admissionSourceType.ToString())
                    {
                        result++;
                    }
                });
            }
            return result;
        }

        private int GetDischargeReasonCount(List<AdmissionPeriod> periods, int dischargeReasonId)
        {
            var result = 0;
            if (periods != null)
            {
                result = periods.Count(p => p.DischargeReasonId == dischargeReasonId);
            }
            return result;
        }

        private string GetEpisodeTiming(IDictionary<string, Question> assessmentData)
        {
            var timing = string.Empty;

            if (assessmentData != null)
            {
                var episodeTiming = assessmentData.AnswerOrEmptyString("M0110EpisodeTiming");
                if (episodeTiming.IsEqual("01"))
                {
                    timing = "Early";
                }
                if (episodeTiming.IsEqual("02"))
                {
                    timing = "Late";
                }
            }

            return timing;
        }

        private string GetNrsSeverityLevel(Assessment assessment)
        {
            var severityLevel = string.Empty;

            if (assessment != null && assessment.HippsCode.IsNotNullOrEmpty())
            {
                if (assessment.HippsCode.ToLower().EndsWith("s"))
                {
                    severityLevel = "1";
                }
                if (assessment.HippsCode.ToLower().EndsWith("t"))
                {
                    severityLevel = "2";
                }
                if (assessment.HippsCode.ToLower().EndsWith("u"))
                {
                    severityLevel = "3";
                }
                if (assessment.HippsCode.ToLower().EndsWith("v"))
                {
                    severityLevel = "4";
                }
                if (assessment.HippsCode.ToLower().EndsWith("w"))
                {
                    severityLevel = "5";
                }
                if (assessment.HippsCode.ToLower().EndsWith("x"))
                {
                    severityLevel = "6";
                }
            }

            return severityLevel;
        }

        private string GetFirstBillableDate(EpisodeData episode)
        {
            var date = string.Empty;
            if (episode.Schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable).OrderBy(s => s.EventDate.ToDateTime().Date).ToList();
                if (scheduleEvents != null)
                {
                    foreach (var scheduleEvent in scheduleEvents)
                    {
                        var status = scheduleEvent.Status;
                        if (scheduleEvent.IsBillable && (status == "425" || status == "220" || status == "225"))
                        {
                            date = scheduleEvent.EventDate.IsValidDate() && scheduleEvent.EventDate.ToDateTime() != DateTime.MinValue ? scheduleEvent.EventDate.ToString() : string.Empty;
                            break;
                        }
                    }
                }
            }
            return date;
        }

        private Dictionary<int, string> GetMonthsBetween(DateTime startDate, DateTime endDate)
        {
            var list = new Dictionary<int, string>();
            if (startDate.Date < endDate.Date)
            {
                var index = 0;
                var month = startDate.Month;
                do
                {
                    list.Add(startDate.AddMonths(index).Month, startDate.AddMonths(index).ToString("MMM"));
                    index++;
                    month++;
                } while (month < endDate.Month + 3);
            }

            return list;
        }

        private Dictionary<DateTime, string> GetMonthsBetweenAndAfter(DateTime startDate, DateTime endDate)
        {
            var list = new Dictionary<DateTime, string>();
            if (startDate.Date < endDate.Date)
            {
                var countMonth = endDate.Month - startDate.Month + (endDate.Year - startDate.Year) * 12;
                for (int i = 0; i <= countMonth ; i++)
                {
                    list.Add(startDate.AddMonths(i), startDate.AddMonths(i).ToString("MMM"));
                }
            }
            return list;
        }


        private string GetVisitsInMonth(EpisodeData episode, int month)
        {
            var result = "0";
            if (episode.Schedule.IsNotNullOrEmpty())
            {
                return episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Month == month && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable).ToList().Count.ToString();
            }
            return result;
        }

        private string GetCompletedVisitsInMonth(EpisodeData episode, int month)
        {
            var result = "0";
            if (episode.Schedule.IsNotNullOrEmpty())
            {
                return episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.IsCompleted() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Month == month && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable).ToList().Count.ToString();
            }
            return result;
        }

        private string GetCompletedTherapyVisits(EpisodeData episode)
        {
            var result = "0";
            if (episode.Schedule.IsNotNullOrEmpty())
            {
                return episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable && s.IsCompleted() && (s.IsPTNote() || s.IsOTNote() || s.IsSTNote())).ToList().Count.ToString();
            }
            return result;
        }

        private string GetScheduledTherapyVisits(EpisodeData episode)
        {
            var result = "0";
            if (episode.Schedule.IsNotNullOrEmpty())
            {
                return episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable && (s.IsPTNote() || s.IsOTNote() || s.IsSTNote())).ToList().Count.ToString();
            }
            return result;
        }

        private string GetVisitsByDiscipline(EpisodeData episode, string discipline)
        {
            var result = "0";
            if (episode.Schedule.IsNotNullOrEmpty())
            {
                return episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable && s.Discipline.IsEqual(discipline)).ToList().Count.ToString();
            }
            return result;
        }

        private string GetCompletedVisitsByDiscipline(EpisodeData episode, string discipline)
        {
            var result = "0";
            if (episode.Schedule.IsNotNullOrEmpty())
            {
                return episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.IsCompleted() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable && s.Discipline.IsEqual(discipline)).ToList().Count.ToString();
            }
            return result;
        }

        private int GetVisitsByDiscipline(List<EpisodeData> episodes, string discipline, DateTime startDate, DateTime endDate)
        {
            var result = 0;
            if (episodes != null)
            {
                episodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty())
                    {
                        result += episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable && s.Discipline.IsEqual(discipline)).ToList().Count;
                    }
                });
            }

            return result;
        }

        private int GetMedicareVisits(List<EpisodeData> episodes, DateTime startDate, DateTime endDate)
        {
            var result = 0;
            if (episodes != null)
            {
                episodes.ForEach(episode =>
                {
                    var detail = episode != null && episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : null;
                    if (detail != null && detail.PrimaryInsurance.IsNotNullOrEmpty()
                        && (detail.PrimaryInsurance.IsEqual("1") || detail.PrimaryInsurance.IsEqual("2")
                        || detail.PrimaryInsurance.IsEqual("3") || detail.PrimaryInsurance.IsEqual("4")))
                    {
                        if (episode.Schedule.IsNotNullOrEmpty())
                        {
                            result += episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable).ToList().Count;
                        }
                    }

                });
            }

            return result;
        }

        private string GetTotalEpisodeVisits(EpisodeData episode)
        {
            var result = "0";
            if (episode.Schedule.IsNotNullOrEmpty())
            {
                return episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable).ToList().Count.ToString();
            }
            return result;
        }

        private string GetTotalCompletedEpisodeVisits(EpisodeData episode)
        {
            var result = "0";
            if (episode.Schedule.IsNotNullOrEmpty())
            {
                return episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.IsCompleted() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.Date && s.EventDate.ToDateTime().Date <= episode.EndDate.Date && !s.IsDeprecated && !s.IsMissedVisit && s.IsBillable).ToList().Count.ToString();
            }
            return result;
        }

        private ProspectivePayment GetProspectivePayment(string hippsCode, DateTime time, string zipCode)
        {
            var prospectivePayment = new ProspectivePayment();
            zipCode = zipCode.ToZipCodeFive();
            if (hippsCode.IsNotNullOrEmpty() && time.IsValid() && zipCode.IsNotNullOrEmpty() && zipCode.Length == 5)
            {
                var cbsaCode = Reports.GetCbsaCode(zipCode);
                var hhrg = Reports.GetHhrgByHippsCodeAndYear(hippsCode, time.Year);
                var ppsStandard = Reports.GetPPSStandardByYear(time.Year);

                if (cbsaCode != null && hhrg != null && ppsStandard != null)
                {
                    Double rate = 0;
                    var isRural = false;
                    var wageIndex = this.WageIndex(cbsaCode, time);

                    if (cbsaCode.CBSA.IsNotNullOrEmpty())
                    {
                        if (cbsaCode.CBSA.StartsWith("9"))
                        {
                            isRural = true;
                            rate = ppsStandard.RuralRate;
                        }
                        else
                        {
                            isRural = false;
                            rate = ppsStandard.UrbanRate;
                        }
                    }

                    var rateTimesWeight = rate * hhrg.HHRGWeight;
                    var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                    var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                    var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                    var character = hippsCode.Length == 5 ? hippsCode[4] : ' ';
                    var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);

                    prospectivePayment.Hhrg = hhrg.HHRG;
                    prospectivePayment.HippsCode = hippsCode;
                    prospectivePayment.CbsaCode = cbsaCode.CBSA;
                    prospectivePayment.WageIndex = wageIndex.ToString();
                    prospectivePayment.Weight = hhrg.HHRGWeight.ToString();
                    prospectivePayment.LaborAmount = laborAmount;
                    prospectivePayment.NonLaborAmount = nonLaborAmount;
                    prospectivePayment.NonRoutineSuppliesAmount = supplyAmount;
                    prospectivePayment.TotalAmountWithoutSupplies = totalAmountWithoutSupplies;
                    prospectivePayment.TotalAmount = supplyAmount + totalAmountWithoutSupplies;
                }
            }
            return prospectivePayment;
        }

        private double WageIndex(CbsaCode cbsaCode, DateTime time)
        {
            double wageIndex = 0;
            if (time.Year == 2013) wageIndex = cbsaCode.WITwoThirteen;
            else if (time.Year == 2012) wageIndex = cbsaCode.WITwoTwelve;
            else if (time.Year == 2011) wageIndex = cbsaCode.WITwoEleven;
            else if (time.Year == 2010) wageIndex = cbsaCode.WITwoTen;
            else if (time.Year == 2009) wageIndex = cbsaCode.WITwoNine;
            else if (time.Year == 2008) wageIndex = cbsaCode.WITwoEight;
            else if (time.Year == 2007) wageIndex = cbsaCode.WITwoSeven;
            return wageIndex;
        }

        private double GetSupplyReimbursement(PPSStandard ppsStandard, bool isRural, char type)
        {
            if (ppsStandard != null)
            {
                switch (type)
                {
                    case 'S':
                        if (isRural)
                        {
                            return ppsStandard.RuralS;
                        }
                        return ppsStandard.S;
                    case 'T':
                        if (isRural)
                        {
                            return ppsStandard.RuralT;
                        }
                        return ppsStandard.T;
                    case 'U':
                        if (isRural)
                        {
                            return ppsStandard.RuralU;
                        }
                        return ppsStandard.U;
                    case 'V':
                        if (isRural)
                        {
                            return ppsStandard.RuralV;
                        }
                        return ppsStandard.V;
                    case 'W':
                        if (isRural)
                        {
                            return ppsStandard.RuralW;
                        }
                        return ppsStandard.W;
                    case 'X':
                        if (isRural)
                        {
                            return ppsStandard.RuralX;
                        }
                        return ppsStandard.X;
                }
            }
            return 0;
        }

        private string GetChargesByMonth(EpisodeData episode, ProspectivePayment prospectivePayment, int month)
        {
            var result = "$0.00";
            if (episode != null && prospectivePayment != null && month > 0)
            {
                var monthVisits = GetVisitsInMonth(episode, month);
                var billableVisits = GetTotalEpisodeVisits(episode);
                if (monthVisits.ToInteger() > 0 && billableVisits.ToInteger() > 0)
                {
                    var unitAmount = Math.Round(prospectivePayment.TotalAmount / billableVisits.ToInteger(), 2);
                    result = string.Format("${0:#0.00}", monthVisits.ToInteger() * unitAmount);
                }
            }

            return result;
        }

        private string GetChargesByDiscipline(EpisodeData episode, ProspectivePayment prospectivePayment, string discipline)
        {
            var result = "$0.00";
            if (episode != null && prospectivePayment != null && discipline.IsNotNullOrEmpty())
            {
                var disciplineVisits = GetVisitsByDiscipline(episode, discipline);
                var billableVisits = GetTotalEpisodeVisits(episode);
                if (disciplineVisits.ToInteger() > 0 && billableVisits.ToInteger() > 0)
                {
                    var unitAmount = Math.Round(prospectivePayment.TotalAmount / billableVisits.ToInteger(), 2);
                    result = string.Format("${0:#0.00}", disciplineVisits.ToInteger() * unitAmount);
                }
            }
            return result;
        }

        public string GetBalanceDue(double totalAmount, Claim rap, Claim final)
        {
            var result = string.Format("${0:#0.00}", totalAmount);

            if (rap != null && totalAmount > 0 && rap.PaymentAmount > 0)
            {
                result = string.Format("${0:#0.00}", totalAmount - rap.PaymentAmount);

                if (final != null && final.PaymentAmount > 0)
                {
                    result = string.Format("${0:#0.00}", totalAmount - (rap.PaymentAmount + final.PaymentAmount));
                }
            }

            return result;
        }

        #endregion
    }
}