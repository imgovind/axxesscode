﻿namespace Axxess.Api.Reporting.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;

    public enum Ethnicity : int
    {
        [CustomDescription("AmericanIndianOrAlaskanNative", "American Indian Or Alaskan Native")]
        AmericanIndianOrAlaskanNative = 0,
        [CustomDescription("Asian", "Asian")]
        Asian = 1,
        [CustomDescription("BlackOrAfricanAmerican", "Black Or African American")]
        BlackOrAfricanAmerican = 2,
        [CustomDescription("HispanicOrLatino", "Hispanic Or Latino")]
        HispanicOrLatino = 3,
        [CustomDescription("NativeHawaiianOrPacificIslander", "Native Hawaiian Or Pacific Islander")]
        NativeHawaiianOrPacificIslander = 4,
        [CustomDescription("White", "White")]
        White = 5,
        [CustomDescription("Unknown", "Unknown")]
        Unknown = 6
    }
}
