﻿namespace Axxess.Api.Reporting.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;

    public enum Dnr : int
    {
        [CustomDescription("Can be Resuscitated", "No")]
        No = 0,
        [CustomDescription("Cannot be Resuscitated", "Yes")]
        Yes = 1
    }
}
