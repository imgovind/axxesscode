﻿namespace Axxess.Api.Reporting.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;

    public enum Dme : int
    {
        [CustomDescription("BedsideCommode", "Bedside Commode")]
        BedsideCommode = 0,
        [CustomDescription("Cane", "Cane")]
        Cane = 1,
        [CustomDescription("ElevatedToiletSeat", "Elevated Toilet Seat")]
        ElevatedToiletSeat = 2,
        [CustomDescription("Grabbars", "Grab bars")]
        Grabbars = 3,
        [CustomDescription("HospitalBed", "Hospital Bed")]
        HospitalBed = 4,
        [CustomDescription("Nebulizer", "Nebulizer")]
        Nebulizer = 5,
        [CustomDescription("Oxygen", "Oxygen")]
        Oxygen = 6,
        [CustomDescription("TubOrShowerBench", "Tub/Shower Bench")]
        TubOrShowerBench = 7,
        [CustomDescription("Walker", "Walker")]
        Walker = 8,
        [CustomDescription("WheelChair", "WheelChair")]
        WheelChair = 9,
        [CustomDescription("Other", "Other")]
        Other = 10,
    }
}
