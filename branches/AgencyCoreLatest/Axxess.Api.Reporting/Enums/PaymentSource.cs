﻿namespace Axxess.Api.Reporting.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;

    public enum PaymentSource : int
    {
        [CustomDescription("None", "None, No charge for current services")]
        None = 0,
        [CustomDescription("MedicareFFS", "Medicare (traditional fee-for-service)")]
        MedicareFFS = 1,
        [CustomDescription("MedicareHMO", "Medicare (HMO/Managed Care")]
        MedicareHMO = 2,
        [CustomDescription("MedicaidFFS", "Medicaid (traditional fee-for-service)")]
        MedicaidFFS = 3,
        [CustomDescription("MedicaidHMO", "Medicaid (HMO/Managed Care)")]
        MedicaidHMO = 4,
        [CustomDescription("WorkersCompensation", "Worker's Compensation")]
        WorkersCompensation = 5,
        [CustomDescription("TitlePrograms", "Title programs (e.g., Title III, V or XX)")]
        TitlePrograms = 6,
        [CustomDescription("OtherGovernmentSchemes", "Other government (e.g., CHAMPUS, VA, etc)")]
        OtherGovernmentSchemes = 7,
        [CustomDescription("PrivateInsurance", "Private Insurance")]
        PrivateInsurance = 8,
        [CustomDescription("PrivateHMO", "Private (HMO/Managed Care)")]
        PrivateHMO = 9,
        [CustomDescription("SelfPay", "Self-Pay")]
        SelfPay = 10,
        [CustomDescription("Unknown", "Unknown")]
        Unknown = 11,
        [CustomDescription("Other", "Other (Specify)")]
        Other = 12
    }
}
