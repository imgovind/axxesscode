﻿namespace Axxess.Api.Reporting.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;

    public enum LookUpInsurance : int
    {
        [CustomDescription("Medicare Insurance", "Medicare(Palmetto GBA)")]
        Palmetto = 1,
        [CustomDescription("National Government Services", "Medicare(National Government Services)")]
        NGS = 2,
        [CustomDescription("Cigna Government Services", "Medicare(Cigna)")]
        Cigna = 3,
        [CustomDescription("Anthem Health Plans of Maine", "Medicare(Anthem Health Plans)")]
        AnthemHealthPlans = 4
    }
}
