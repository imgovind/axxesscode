﻿namespace Axxess.Api.Reporting
{
    using System;
    using System.ServiceProcess;
    using System.ComponentModel;
    using System.Configuration.Install;

    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private ServiceInstaller reportInstaller;
        private ServiceProcessInstaller processInstaller;

        public ProjectInstaller()
        {
            InitializeComponent();

            this.processInstaller = new ServiceProcessInstaller();
            this.processInstaller.Account = ServiceAccount.LocalSystem;

            this.reportInstaller = new ServiceInstaller();
            this.reportInstaller.StartType = ServiceStartMode.Automatic;
            this.reportInstaller.ServiceName = "ReportService2";
            this.reportInstaller.DisplayName = "Report Service2";
            this.reportInstaller.Description = "Retrieves various reports for the AgencyCore Application.";

            Installers.AddRange(new Installer[] { this.processInstaller, this.reportInstaller });
        }
    }
}
