﻿namespace Axxess.Api.Membership
{
    using System;
    using System.Collections.Generic;
    using System.Timers;

    using Axxess.Api.Contracts;
    using Axxess.Core;
    using Axxess.Core.Extension;

    public sealed class ActivityMonitor
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
                instance.StartTimer();
            }

            internal static readonly ActivityMonitor instance = new ActivityMonitor();
        }

        #endregion

        #region Private Members

        private Timer timer;
        private SafeList<SingleUser> users;

        #endregion

        #region Public Instance

        public static ActivityMonitor Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private ActivityMonitor()
        {
            users = new SafeList<SingleUser>();
        }

        private void StartTimer()
        {
            this.timer = new System.Timers.Timer(TimeSpan.FromMinutes(2).TotalMilliseconds);
            this.timer.Enabled = true;
            this.timer.Elapsed += (sender, e) =>
            {
                var inactive = this.users.Find(user =>
                {
                    if (user != null) 
                    {
                        return DateTime.Now > user.LastSecureActivity.AddMinutes(90);
                    }
                    return false;
                });

                inactive.ForEach(user =>
                {
                    if (user != null)
                    {
                        user.IsAuthenticated = false;
                    }
                });
            };
            this.timer.Start();
        }

        #endregion

        #region Public Methods

        public SingleUser Get(Guid loginId)
        {
            return this.users.Single(u => u.LoginId.ToString().IsEqual(loginId.ToString()));
        }

        public SingleUser Get(string emailAddress)
        {
            return this.users.Single(u => u.EmailAddress.IsEqual(emailAddress));
        }

        public bool IsAuthenticated(string sessionId)
        {
            var result = false;
            var user = this.users.Single(u => u.SessionId.IsEqual(sessionId));
            if (user != null)
            {
                result = user.IsAuthenticated;
            }
            return result;
        }

        public SingleUser Get(Predicate<SingleUser> predicate)
        {
            return this.users.Single(predicate);
        }

        public void Log(SingleUser user)
        {
            var existingUser = this.users.Single(u => u.LoginId.ToString().IsEqual(user.LoginId.ToString()));
            if (existingUser != null)
            {
                if (existingUser.SessionId.IsEqual(user.SessionId))
                {
                    existingUser.FullName = user.FullName;
                    existingUser.SessionId = user.SessionId;
                    existingUser.AgencyName = user.AgencyName;
                    existingUser.ServerName = user.ServerName;
                    existingUser.EmailAddress = user.EmailAddress;
                    existingUser.LastSecureActivity = DateTime.Now;
                }
            }
        }

        public void Add(SingleUser user)
        {
            var existingUser = this.users.Single(u => u.LoginId.ToString().IsEqual(user.LoginId.ToString()));
            if (existingUser == null)
            {
                this.users.Add(user);
            }
            else
            {
                existingUser.FullName = user.FullName;
                existingUser.SessionId = user.SessionId;
                existingUser.AgencyName = user.AgencyName;
                existingUser.ServerName = user.ServerName;
                existingUser.EmailAddress = user.EmailAddress;
                existingUser.LastSecureActivity = DateTime.Now;
                existingUser.IsAuthenticated = true;
            }
        }

        public void Expire(string emailAddress)
        {
            var user = this.users.Single(u => u.EmailAddress.IsEqual(emailAddress));
            if (user != null)
            {
                user.IsAuthenticated = false;
            }
        }

        public int Count
        {
            get
            {
                return users.Count;
            }
        }

        public List<SingleUser> AsList()
        {
            var items = new List<SingleUser>();
            this.users.ForEach(items.Add);
            return items;
        }

        #endregion
    }
}
