﻿namespace Axxess.Membership
{
    using System;

    public class LoginAttempt
    {
        public LoginAttempt() { this.Count = 0; }
        public int Count { get; set; }
        public string UserName { get; set; }
        public string IpAddress { get; set; }
        public DateTime LastLoginAttempt { get; set; }

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}-{3}", UserName.ToLower(), IpAddress, LastLoginAttempt.ToString("MM/dd/yyyy hh:mm:ss tt"), Count.ToString());
        }
    }
}
