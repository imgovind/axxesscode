﻿namespace Axxess.Membership.Domain
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;

    public class LoginToken
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public bool IsUsed { get; set; }
        public Guid AgencyId { get; set; }
        public DateTime Created { get; set; }
    }
}
