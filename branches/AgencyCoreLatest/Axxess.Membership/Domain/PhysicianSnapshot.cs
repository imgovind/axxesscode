﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.SqlGeneration.Schema;

namespace Axxess.Membership.Domain
{
    public class PhysicianSnapshot
    {
        public Guid Id { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public string NPI { get; set; }
        public bool HasPhysicianAccess{ get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
    }
}
