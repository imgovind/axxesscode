﻿namespace Axxess.Membership.Domain
{
    using System;

    public class Application
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
    }
}
