﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.SqlGeneration.Schema;

namespace Axxess.Membership.Domain
{
    //[SubSonicTableNameOverride("agencies")]
    public class AgencySnapshot
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsSuspended { get; set; }
        public bool IsDeprecated { get; set; }
        public bool IsFrozen { get; set; }
        public DateTime FrozenDate { get; set; }
        public int ClusterId { get; set; }

    }
}
