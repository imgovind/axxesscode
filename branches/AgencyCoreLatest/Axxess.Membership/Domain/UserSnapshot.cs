﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.SqlGeneration.Schema;

namespace Axxess.Membership.Domain
{
    public class UserSnapshot
    {
        public Guid Id { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public string TitleType { get; set; }
        public int Status { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
    }
}
