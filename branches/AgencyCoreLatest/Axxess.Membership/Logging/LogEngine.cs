﻿namespace Axxess.Membership.Logging
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Timers;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Repositories;

    public class LogEngine
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
                instance.StartTimer();
            }

            internal static readonly LogEngine instance = new LogEngine();
        }

        #endregion

        #region Private Members

        private Timer updateTimer;
        private SafeList<Error> recentErrors;
        private readonly IErrorRepository errorRepository = Container.Resolve<IMembershipDataProvider>().ErrorRepository;
        
        #endregion

        #region Public Instance

        public static LogEngine Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private LogEngine()
        {
            this.recentErrors = new SafeList<Error>();
        }

        private void StartTimer()
        {
            this.updateTimer = new System.Timers.Timer(TimeSpan.FromMinutes(30).TotalMilliseconds);
            this.updateTimer.Enabled = true;
            this.updateTimer.Elapsed += new ElapsedEventHandler(this.Timer_Elapsed);
            this.updateTimer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.recentErrors != null && this.recentErrors.Count > 0)
            {
                this.ProcessExceptions(new List<Error>(this.recentErrors.ToList()));
                this.recentErrors = new SafeList<Error>();
            }
        }

        private void ProcessExceptions(List<Error> errors)
        {
            errors.ForEach(error =>
            {
                errorRepository.Add(error);
            });
        }

        #endregion

        #region Public Methods

        public void Add(Error error)
        {
            var existingError = this.recentErrors.Single(e => e.Message.IsEqual(error.Message));
            if (existingError == null)
            {
                error.Detail.Impression++;
                this.recentErrors.Add(error);
            }
            else
            {
                existingError.Detail.Impression++;
            }
        }

        public int Count
        {
            get
            {
                return this.recentErrors.Count;
            }
        }

        public SafeList<Error> Cache
        {
            get
            {
                return this.recentErrors;
            }
        }

        #endregion
    }
}
