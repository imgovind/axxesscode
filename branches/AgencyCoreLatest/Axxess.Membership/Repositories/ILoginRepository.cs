﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core.Infrastructure;

    public interface ILoginRepository
    {
        Login Find(Guid id);
        bool Add(Login login);
        bool Delete(Guid loginId);
        ICollection<Login> GetAll();
        Login Find(string emailAddress);
        ICollection<Login> AutoComplete(string searchString);
        bool Update(Login login);

        string GetLoginDisplayName(Guid loginId);
        string GetLoginEmailAddress(Guid loginId);

        IList<Login> GetAllByRole(Roles role);

        DdeCredential GetDdeCredential(Guid id);
        IList<DdeCredential> GetDdeCredentials();
        bool UpdateDdeCredential(DdeCredential ddeCredential);

        bool ToggleUserSnapshot(Guid agencyId, Guid userId, bool IsDeprecated);
        bool ToggleUserSnapshots(Guid agencyId, List<Guid> userIds, bool IsDeprecated);
        bool UpdateUserSnapshotStatus(Guid agencyId, Guid userId, int status);
        bool BulkUserSnapshotStatus(Guid agencyId, List<Guid> userIds, bool isActive);
        bool UserSnapshotTitle(Guid agencyId, Guid userId, string title);
        bool AddUserSnapshot(UserSnapshot userSnapshot);

        bool TogglePhysicianSnapshot(Guid agencyId, Guid physicianId, bool IsDeprecated);
        bool AddPhysicianSnapshot(PhysicianSnapshot physicianSnapshot);
        bool UpdatePhysicianSnapshot(Guid agencyId, Guid physicianId, string Npi, bool physicianAccess);

        bool DoesPhysicianSnapshotExist(Guid agencyId, Guid physicianId);

        /// <summary>
        /// Only pulls the active logins
        /// </summary>
        /// <param name="loginIds"></param>
        /// <returns>A List of logins with only their email, isActive, and id being set.</returns>
        List<Login> GetActiveLogins(List<Guid> loginIds);
    }
}
