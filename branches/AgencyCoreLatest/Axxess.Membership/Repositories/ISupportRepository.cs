﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    public interface ISupportRepository
    {
        bool AddImpersonationLink(ImpersonationLink link);
        ImpersonationLink GetImpersonationLink(Guid linkId);
        bool UpdateImpersonationLink(ImpersonationLink link);

        LoginToken GetToken(Guid tokenId);
        bool UpdateToken(LoginToken token);
    }
}
