﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class HostRepository : IHostRepository
    {
        #region Constructor

        //private ICoreDataContext db;

        //public HostRepository(ICoreDataContext dataContext)
        //{
        //    Check.Argument.IsNotNull(dataContext, "dataContext");
        //    this.db = dataContext;
        //}

        #endregion

        #region IHostRepository Members

        //public byte Add(Guid applicationId, byte environmentId, string ipAddress, string computerName)
        //{
        //    Check.Argument.IsNotEmpty(applicationId, "applicationId");
        //    Check.Argument.IsNotNegative(environmentId, "environmentId");
        //    Check.Argument.IsNotNull(ipAddress, "ipAddress");
        //    Check.Argument.IsNotNull(computerName, "computerName");

        //    Host hostInfo = new Host()
        //    {
        //        ApplicationId = applicationId,
        //        ComputerName = computerName,
        //        EnvironmentId = environmentId,
        //        IPAddress = ipAddress,
        //        Created = DateTime.Now,
        //        Modified = DateTime.Now
        //    };
        //    db.Insert<Host>(hostInfo);
        //    db.SubmitChanges();
        //    return hostInfo.Id;
        //}

        //public bool Update(byte id, Guid applicationId, byte environmentId, string ipAddress, string computerName)
        //{
        //    Check.Argument.IsNotNegative(id, "id");
        //    Check.Argument.IsNotEmpty(applicationId, "applicationId");
        //    Check.Argument.IsNotNegative(environmentId, "environmentId");
        //    Check.Argument.IsNotNull(ipAddress, "ipAddress");
        //    Check.Argument.IsNotNull(computerName, "computerName");

        //    bool result = false;
        //    Host host = db.HostsDataSource.SingleOrDefault(h => h.Id == id);
        //    if (host != null)
        //    {
        //        host.ApplicationId = applicationId;
        //        host.EnvironmentId = environmentId;
        //        host.IPAddress = ipAddress;
        //        host.ComputerName = computerName;
        //        host.Modified = DateTime.Now;
        //        db.SubmitChanges();
        //        result = true;
        //    }
        //    return result;
        //}
        ////Delete
        //public bool Delete(byte hostId)
        //{
        //    Check.Argument.IsNotNegative(hostId, "hostId");

        //    bool result = false;
        //    Host host = db.HostsDataSource.SingleOrDefault(h => h.Id == hostId);
        //    if (host != null)
        //    {
        //        db.DeleteAll(db.ErrorsDataSource.Where(e => e.HostId == host.Id));
        //        db.Delete(host);
        //        db.SubmitChanges();
        //        result = true;
        //    }
        //    return result;
        //}

        //public IHost Find(byte hostId)
        //{
        //    Check.Argument.IsNotNegative(hostId, "hostId");
        //    return db.HostsDataSource
        //        .SingleOrDefault(h => h.Id == hostId);
        //}

        //public byte GetEnvironmentId(string computerName, Guid applicationId)
        //{
        //    Check.Argument.IsNotEmpty(applicationId, "applicationId");
        //    Check.Argument.IsNotNull(computerName, "computerName");


        //    return (from h in db.HostsDataSource
        //            where h.ComputerName == computerName
        //           && h.ApplicationId == applicationId
        //            select h.EnvironmentId).SingleOrDefault<byte>();
        //}

        //public ICollection<IHost> GetHostByApplicationID(Guid applicationId)
        //{
        //    Check.Argument.IsNotEmpty(applicationId, "applicationId");
        //    return db.HostsDataSource.Where(h => h.ApplicationId == applicationId)
        //        .OrderBy(h => h.Created)
        //        .Cast<IHost>()
        //        .ToList()
        //        .AsReadOnly();

        //}

        //public ICollection<IHost> GetHostByEnviormentId(byte environmentId)
        //{
        //    Check.Argument.IsNotNegative(environmentId, "environmentId");

        //    return db.HostsDataSource.Where(h => h.EnvironmentId == environmentId)
        //        .OrderBy(h => h.Created)
        //        .Cast<IHost>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        //public ICollection<IHost> GetHostsByComputerName(string computerName)
        //{
        //    Check.Argument.IsNotNull(computerName, "computerName");
        //    return db.HostsDataSource.Where(h => h.ComputerName == computerName)
        //        .OrderBy(h => h.Created)
        //        .Cast<IHost>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        //public ICollection<IHost> GetHostsByIpAddress(string ipAddress)
        //{
        //    Check.Argument.IsNotNull(ipAddress, "ipAddress");
        //    return db.HostsDataSource.Where(h => h.IPAddress == ipAddress)
        //        .OrderBy(h => h.Created)
        //        .Cast<IHost>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        //public ICollection<IHost> GetAll()
        //{
        //    return db.HostsDataSource
        //        .Cast<IHost>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        //public PagedResult<IHost> GetHostByCreatedDateRange(DateTime startDate, DateTime endDate, int start, int max)
        //{
        //    Check.Argument.IsNotInvalidDate(startDate, "startDate");
        //    Check.Argument.IsNotInvalidDate(endDate, "endDate");
        //    Check.Argument.IsNotNegative(max, "max");
        //    Check.Argument.IsNotNegative(start, "start");

        //    var query = (db.HostsDataSource.Where(h => h.Created >= startDate && h.Created <= endDate))
        //        .OrderBy(h => h.Created)
        //        .Skip(start)
        //        .Take(max)
        //        .Cast<IHost>();


        //    return new PagedResult<IHost>(query, max);
        //}

        //public PagedResult<IHost> GetHostByModifiedDateRange(DateTime startDate, DateTime endDate, int start, int max)
        //{
        //    Check.Argument.IsNotInvalidDate(startDate, "startDate");
        //    Check.Argument.IsNotInvalidDate(endDate, "endDate");
        //    Check.Argument.IsNotNegative(max, "max");
        //    Check.Argument.IsNotNegative(start, "start");

        //    var query = (db.HostsDataSource.Where(h => h.Modified >= startDate && h.Modified <= endDate))
        //        .OrderBy(h => h.Created)
        //        .Skip(start)
        //        .Take(max)
        //        .Cast<IHost>();
        //    return new PagedResult<IHost>(query, max);
        //}

        #endregion
    }
}
