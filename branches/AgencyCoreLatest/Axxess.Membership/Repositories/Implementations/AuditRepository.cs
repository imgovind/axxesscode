﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using Domain;

    using Axxess.Core;

    using SubSonic.Repository;

    public class AuditRepository : IAuditRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AuditRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAuditRepository Members

        //public int Add(Guid userId, Guid applicationId, string actionLog)
        //{
        //    Check.Argument.IsNotEmpty(userId, "userId");
        //    Check.Argument.IsNotEmpty(applicationId, "applicationId");

        //    try
        //    {
        //        var audit = new Audit()
        //        {
        //            UserId = userId,
        //            ApplicationId = applicationId,
        //            ActionLog = actionLog,
        //            Created = DateTime.Now
        //        };
        //        database.Add<Audit>(audit);
        //        return audit.Id;
        //    }
        //    catch (Exception)
        //    {
        //        // TODO log exception

        //    }
        //    return 0;
        //}

        //public bool Update(int auditId, Guid userId, Guid applicationId, XElement actionLog)
        //{
        //    Check.Argument.IsNotNegative(auditId, "auditID");
        //    Check.Argument.IsNotEmpty(userId, "userId");
        //    Check.Argument.IsNotEmpty(applicationId, "applicationId");
        //    Check.Argument.IsNotNull(actionLog, "actionLog");

        //    bool result = false;
        //    Audit audit = db.AuditsDataSource.SingleOrDefault(a => a.Id == auditId);
        //    if (audit != null)
        //    {
        //        audit.UserId = userId;
        //        audit.ApplicationId = applicationId;
        //        audit.ActionLog = actionLog;
        //        audit.Created = DateTime.Now;
        //        db.SubmitChanges();
        //        result = true;
        //    }
        //    return result;
        //}

        //public bool Delete(int auditId)
        //{
        //    Check.Argument.IsNotNegative(auditId, "auditID");

        //    bool result = false;
        //    Audit audit = db.AuditsDataSource.SingleOrDefault(a => a.Id == auditId);
        //    if (audit != null)
        //    {
        //        db.Delete(audit);
        //        db.SubmitChanges();
        //        result = true;
        //    }
        //    return result;
        //}

        //public IAudit Find(int auditId)
        //{
        //    Check.Argument.IsNotNegative(auditId, "auditID");
        //    return db.AuditsDataSource
        //        .FirstOrDefault(a => a.Id == auditId);
        //}

        //public ICollection<IAudit> GetApplicationAudits(Guid applicationId)
        //{
        //    Check.Argument.IsNotEmpty(applicationId, "applicationId");

        //    return db.AuditsDataSource.Where(a => a.ApplicationId == applicationId)
        //        .Cast<IAudit>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        //public ICollection<IAudit> GetUserAudits(Guid userId)
        //{
        //    Check.Argument.IsNotEmpty(userId, "userId");

        //    return db.AuditsDataSource.Where(a => a.UserId == userId)
        //        .Cast<IAudit>()
        //        .ToList()
        //        .AsReadOnly();
        //}
        //public PagedResult<IAudit> GetAuditsByDate(DateTime createdDate, int start, int max)
        //{
        //    Check.Argument.IsNotInvalidDate(createdDate, "createdDate");
        //    Check.Argument.IsNotNegative(start, "start");
        //    Check.Argument.IsNotNegative(max, "max");

        //    var query = (db.AuditsDataSource.Where(a => a.Created == createdDate)
        //        .Skip(start)
        //        .Take(max)).Cast<IAudit>();

        //    return new PagedResult<IAudit>(query, max);
        //}
        //public PagedResult<IAudit> GetAuditsByDateRange(DateTime startDate, DateTime endDate, int start, int max)
        //{
        //    Check.Argument.IsNotInvalidDate(startDate, "startDate");
        //    Check.Argument.IsNotInvalidDate(endDate, "endDate");
        //    Check.Argument.IsNotNegative(start, "start");
        //    Check.Argument.IsNotNegative(max, "max");

        //    var query = (db.AuditsDataSource.Where(a => (a.Created >= startDate && a.Created <= endDate))
        //        .OrderBy(a => a.Created)
        //        .Skip(start)
        //        .Take(max)).Cast<IAudit>();
        //    return new PagedResult<IAudit>(query, max);
        //}

        #endregion
    }
}
