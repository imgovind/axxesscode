﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class LoginRepository : ILoginRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        private string connectionString;

        public LoginRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
            this.connectionString = "AxxessMembershipConnectionString";
        }

        #endregion

        #region ILoginRepository Members

        public bool Add(Login login)
        {
            if (login != null)
            {
                login.Id = Guid.NewGuid();
                login.Created = DateTime.Now;
                database.Add<Login>(login);
                return true;
            }
            return false;
        }

        public Login Find(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            return database.Single<Login>(l => l.Id == id);
        }

        public Login Find(string emailAddress)
        {
            Check.Argument.IsNotInvalidEmail(emailAddress, "emailAddress");

            return database.Single<Login>(l => l.EmailAddress == emailAddress.Trim());
        }

        public bool Update(Login login)
        {
            if (login != null)
            {
                database.Update<Login>(login);
                return true;
            }
            return false;
        }

        public bool Delete(Guid loginId)
        {
            Check.Argument.IsNotEmpty(loginId, "loginId");

            bool result = false;
            var login = database.Single<Login>(l => l.Id == loginId);

            if (login != null)
            {
                login.IsActive = false;
                database.Update<Login>(login);
                result = true;
            }
            return result;
        }

        public ICollection<Login> GetAll()
        {
            return database.All<Login>().ToList().AsReadOnly();
        }

        public IList<Login> GetAllByRole(Roles role)
        {
            return database.Find<Login>(l => l.Role == role.ToString() && l.IsActive == true).ToList();
        }

        public ICollection<Login> AutoComplete(string searchString)
        {
            return database.Find<Login>(u => u.EmailAddress.StartsWith(searchString)).ToList();
        }

        public string GetLoginDisplayName(Guid loginId)
        {
            var name = string.Empty;

            if (!loginId.IsEmpty())
            {
                var login = Find(loginId);
                if (login != null)
                {
                    name = login.DisplayName;
                }
            }

            return name;
        }

        public string GetLoginEmailAddress(Guid loginId)
        {
            var name = string.Empty;

            if (!loginId.IsEmpty())
            {
                var login = Find(loginId);
                if (login != null)
                {
                    name = login.EmailAddress;
                }
            }

            return name;
        }

        public bool UpdatePhysicianSnapshot(Guid agencyId, Guid physicianId, string Npi, bool physicianAccess)
        {
            try
            {
                var script = string.Format(@"UPDATE physiciansnapshots SET NPI = @npi, HasPhysicianAccess = @physicianaccess WHERE AgencyId = @agencyid AND Id = @id;");
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(this.connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("id", physicianId)
                        .AddBoolean("physicianaccess", physicianAccess)
                        .AddString("npi", Npi)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DoesPhysicianSnapshotExist(Guid agencyId, Guid physicianId)
        {
            try
            {
                var script = string.Format(@"SELECT 1 from physiciansnapshots WHERE AgencyId = @agencyid AND Id = @id;");
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(this.connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("id", physicianId)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Login> GetActiveLogins(List<Guid> loginIds)
        {
            var list = new List<Login>();
            if (loginIds != null && loginIds.Count > 0)
            {
                var ids = loginIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                            Id,
                                            EmailAddress,
                                            IsActive
                                                FROM 
                                                    logins 
                                                        WHERE
                                                            logins.Id IN ( {0} ) AND
                                                            logins.IsActive = 1;", ids);
                using (var cmd = new FluentCommand<Login>(script))
                {
                    list = cmd.SetConnection(this.connectionString)
                     .SetMap(reader => new Login
                     {
                         Id = reader.GetGuid("Id"),
                         EmailAddress = reader.GetStringNullable("EmailAddress"),
                         IsActive = reader.GetBoolean("IsActive")
                     }).AsList();
                }
            }
            return list;
        }

        #endregion

        #region DDE Credentials Members

        public DdeCredential GetDdeCredential(Guid id)
        {
            return database.Single<DdeCredential>(d => d.Id == id);
        }

        public IList<DdeCredential> GetDdeCredentials()
        {
            return database.All<DdeCredential>().ToList();
        }

        public bool UpdateDdeCredential(DdeCredential ddeCredential)
        {
            if (ddeCredential != null)
            {
                database.Update<DdeCredential>(ddeCredential);
                return true;
            }
            return false;
        }

        public bool ToggleUserSnapshot(Guid agencyId, Guid userId, bool IsDeprecated)
        {
            var count = 0;
            try
            {
                string script = @"UPDATE usersnapshots SET IsDeprecated = @deprecated WHERE AgencyId = @agencyid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("id", userId)
                        .AddInt("deprecated", IsDeprecated ? 1 : 0)
                        .AsNonQuery();
                    if (count > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool ToggleUserSnapshots(Guid agencyId, List<Guid> userIds, bool IsDeprecated)
        {
            var count = 0;
            try
            {
                string script = string.Format(@"UPDATE usersnapshots SET IsDeprecated = @deprecated WHERE AgencyId = @agencyid AND Id IN ({0})",userIds.ToCommaSeperatedList());
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddInt("deprecated", IsDeprecated ? 1 : 0)
                        .AsNonQuery();
                    if (count > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        public bool UpdateUserSnapshotStatus(Guid agencyId, Guid userId, int status)
        {
            var count = 0;
            try
            {
                string script = @"UPDATE usersnapshots SET Status = @Status WHERE AgencyId = @agencyid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("id", userId)
                        .AddInt("Status", status)
                        .AsNonQuery();
                    if (count > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }


        public bool BulkUserSnapshotStatus(Guid agencyId, List<Guid> userIds, bool isActive)
        {
            try
            {
                var ids = userIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"UPDATE usersnapshots SET `Status` = @Status WHERE  AgencyId = @agencyid AND Id in ({0});", ids);
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(this.connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddInt("Status", isActive ? 1 : 2)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UserSnapshotTitle(Guid agencyId, Guid userId, string title)
        {
            try
            {
                var script = string.Format(@"UPDATE usersnapshots SET `TitleType` = @titletype WHERE AgencyId = @agencyid AND Id = @id;");
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(this.connectionString)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("id", userId)
                        .AddString("titletype", title)
                        .AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool AddUserSnapshot(UserSnapshot userSnapshot)
        {
            try
            {
                if (userSnapshot != null)
                {
                    database.Add<UserSnapshot>(userSnapshot);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool AddPhysicianSnapshot(PhysicianSnapshot physicianSnapshot)
        {
            try
            {
                if (physicianSnapshot != null)
                {
                    database.Add<PhysicianSnapshot>(physicianSnapshot);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        public bool TogglePhysicianSnapshot(Guid agencyId, Guid physicianId, bool IsDeprecated)
        {
            try
            {
                return EntityHelper.ToggleEntityDeprecationWithoutModificationTime<PhysicianSnapshot, Guid>(physicianId, agencyId, IsDeprecated, this.connectionString);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion
   
    }
}
