﻿namespace Axxess.Api.Eligibility
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Configuration;
    using System.Net;
    using System.Text;
    using System.IO;
    using OopFactory.X12.Parsing;
    using OopFactory.X12.Parsing.Model;
    using Axxess.Api.Contracts;

    public class Eligibility : IEligibility
    {
        private HttpStatusCode responseStatus;
        public string ResponseStatus { get { return responseStatus.ToString(); } }

        #region IEligibility Members

        public string CreateEDI270(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        {
            try
            {
                var sb = new StringBuilder(ConfigurationManager.AppSettings["EDI270Template"]);

                sb.Replace("InterchangeDateYYMMDD", DateTime.Now.ToString("yyMMdd"));
                sb.Replace("InterchangeHHMM", DateTime.Now.ToString("HHmm"));
                sb.Replace("CurrentDateCCYYMMDD", DateTime.Now.ToString("yyyyMMdd"));
                sb.Replace("CurrentTimeHHMMSS", DateTime.Now.ToString("HHmmss"));
                sb.Replace("LastName", lastName);
                sb.Replace("FirstName", firstName);
                sb.Replace("MedicareNumber", medicareNumber);
                sb.Replace("DateOfBirth", dob.ToString("yyyyMMdd"));
                sb.Replace("GenderCode", gender);

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
        }

        public string Submit(string edi270)
        {
            string requestResult = null;
            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] requestData = encoding.GetBytes(edi270);
                //byte[] requestData = Encoding.UTF8.GetBytes(edi270);            

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["PatientEligibilityUrl"]);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/octet-stream";
                httpWebRequest.ContentLength = requestData.Length;

                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {
                    requestStream.Write(requestData, 0, requestData.Length);
                    requestStream.Close();
                }

                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpWebResponse != null)
                {
                    responseStatus = httpWebResponse.StatusCode;
                    if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            requestResult = streamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
            return requestResult;
        }

        public Interchange ParseEDI271(string edi271)
        {
            try
            {
                return new X12Parser().Parse(edi271);
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
        }

        public Container GetContainer(Container holdingContainer, Type returnContainerType, int elementNumber, string value)
        {
            try
            {
                if (holdingContainer != null && returnContainerType != null && !string.IsNullOrEmpty(value))
                {
                    if (returnContainerType != null)
                    {
                        switch (returnContainerType.Name)
                        {
                            case "FunctionGroup":
                                FunctionGroup functionGroup = null;
                                switch (holdingContainer.GetType().Name)
                                {
                                    case "Interchange":
                                        var functionGroups = ((Interchange)holdingContainer).FunctionGroups.Where(f => f.GetElement(elementNumber) == value);
                                        if (functionGroups != null)
                                        {
                                            if (functionGroups.Count() >= 1)
                                            {
                                                functionGroup = functionGroups.First();
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                return functionGroup;
                            case "Transaction":
                                Transaction transaction = null;
                                switch (holdingContainer.GetType().Name)
                                {
                                    case "Interchange":
                                        foreach (var functionalGroup in ((Interchange)holdingContainer).FunctionGroups)
                                        {
                                            transaction = (Transaction)GetContainer(functionalGroup, returnContainerType, elementNumber, value);
                                            if (transaction != null)
                                            {
                                                break;
                                            }
                                        }
                                        break;
                                    case "FunctionGroup":
                                        var transactions = ((FunctionGroup)holdingContainer).Transactions.Where(t => t.GetElement(elementNumber) == value);
                                        if (transactions != null)
                                        {
                                            if (transactions.Count() >= 1)
                                            {
                                                transaction = transactions.First();
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                return transaction;
                            case "HierarchicalLoop":
                                HierarchicalLoop hierarchicalLoop = null;
                                int id;
                                if (int.TryParse(value, out id))
                                {
                                    id = id - 19;
                                    if (id >= 1)
                                    {
                                        switch (holdingContainer.GetType().Name)
                                        {
                                            case "Interchange":
                                                foreach (var functionGroupContainer in ((Interchange)holdingContainer).FunctionGroups)
                                                {
                                                    hierarchicalLoop = (HierarchicalLoop)GetContainer(functionGroupContainer, returnContainerType, elementNumber, value);
                                                    if (hierarchicalLoop != null)
                                                    {
                                                        break;
                                                    }
                                                }
                                                break;
                                            case "FunctionGroup":
                                                foreach (var transactionContainer in ((FunctionGroup)holdingContainer).Transactions)
                                                {
                                                    hierarchicalLoop = (HierarchicalLoop)GetContainer(transactionContainer, returnContainerType, elementNumber, value);
                                                    if (hierarchicalLoop != null)
                                                    {
                                                        break;
                                                    }
                                                }
                                                break;
                                            case "Transaction":
                                                hierarchicalLoop = ((Transaction)holdingContainer).FindHLoop(id.ToString());
                                                if (hierarchicalLoop != null)
                                                {
                                                    if (hierarchicalLoop.GetElement(elementNumber) != value)
                                                    {
                                                        hierarchicalLoop = null;
                                                    }
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                                return hierarchicalLoop;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
            return null;
        }

        public string GetElementSpecificationDescription(Interchange interchange, string versionCode, string segmentId, string elementName, string elementValue)
        {
            try
            {
                var segmentSpec = interchange.SpecFinder.FindSegmentSpec("5010", segmentId);
                if (segmentSpec != null)
                {
                    if (segmentSpec.Elements != null)
                    {
                        if (segmentSpec.Elements.Count() >= 1)
                        {
                            var resultingElements = segmentSpec.Elements.Where(e => e.Name == elementName);
                            if (resultingElements != null)
                            {
                                if (resultingElements.Count() >= 1)
                                {
                                    var resultingElement = resultingElements.FirstOrDefault();
                                    var resultingIdentifiers = resultingElement.AllowedIdentifiers.Where(e => e.ID == elementValue);
                                    if (resultingIdentifiers != null)
                                    {
                                        if (resultingIdentifiers.Count() >= 1)
                                        {
                                            var resultingIdentifier = resultingIdentifiers.FirstOrDefault();
                                            if (resultingIdentifier != null)
                                            {
                                                return resultingIdentifier.Description;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
            return null;
        }

        #endregion

    }
}
