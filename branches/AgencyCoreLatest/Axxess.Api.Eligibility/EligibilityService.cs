﻿namespace Axxess.Api.Eligibility
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.Api.Contracts;
    using OopFactory.X12.Parsing;
    using OopFactory.X12.Parsing.Model;
    using System.Globalization;

    public class EligibilityService : BaseService, IEligibilityService
    {

        #region Construtor

        public EligibilityService()
        {
            eligibility = new Eligibility();
        }

        #endregion

        #region IEligibilityService Members

        public PatientEligibility EligibilityCheck(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        {
            PatientEligibility patientEligibility = null;
            try
            {
                if (!string.IsNullOrEmpty(medicareNumber) &&
                    !string.IsNullOrEmpty(lastName) &&
                    !string.IsNullOrEmpty(firstName) &&
                    !string.IsNullOrEmpty(gender))
                {
                    var edi270 = eligibility.CreateEDI270(medicareNumber, lastName, firstName, dob, gender);
                    if (!string.IsNullOrEmpty(edi270))
                    {
                        var edi271 = eligibility.Submit(edi270);
                        if (!string.IsNullOrEmpty(edi271))
                        {
                            var interchange = eligibility.ParseEDI271(edi271);
                            if (interchange != null)
                            {
                                patientEligibility = new PatientEligibility
                                {
                                    Dependent = GetDependent(interchange),
                                    Episode = GetEpisode(interchange),
                                    Functional_Acknowledgment = GetFunctionalAcknowledgment(interchange),
                                    Health_Benefit_Plan_Coverage = GetHealthBenefitPlanCoverage(interchange),
                                    Medicare_Part_A = GetMedicarePartA(interchange),
                                    Medicare_Part_B = GetMedicarePartB(interchange),
                                    Request_Result = GetRequestResult(),
                                    Request_Validation = GetRequestValidation(interchange),
                                    Subscriber = GetSubscriber(interchange)
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }

            return patientEligibility;
        }

        #endregion

        #region Private Members

        private IEligibility eligibility;
        private HierarchicalLoop subscriberLoop { get; set; }
        private Loop subscriberName { get; set; }

        private GenericResult GetDependent(Interchange interchange)
        {
            GenericResult dependent = null;
            try
            {
                if (interchange != null)
                {
                    dependent = new GenericResult
                    {
                        success = "No",
                    };
                }
                if (dependent == null)
                {
                    dependent = new GenericResult { success = "No" };
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return dependent;
        }

        private Episode GetEpisode(Interchange interchange)
        {
            Episode episode = null;
            try
            {
                if (interchange != null)
                {
                    if (SetSubscriberName(interchange))
                    {
                        string _name = null;
                        string _payer = null;
                        string _period_date_range = null;
                        string _reference_id_qualifier = null;
                        string _reference_id = null;
                        string _period_start = null;
                        string _period_end = null;

                        var subscriberEpisodes = subscriberName.Loops.Where(t => t.GetElement(1) == "X" && t.GetElement(3) == "42" && t.GetElement(6) == "26");
                        if (subscriberEpisodes != null)
                        {
                            if (subscriberEpisodes.Count() >= 1)
                            {
                                var subscriberEpisode = getSubscriberEligibility(subscriberEpisodes);
                                var ls = subscriberEpisode.Loops.Where(l => l.SegmentId == "LS");
                                if (ls != null)
                                {
                                    if (ls.Count() >= 1)
                                    {
                                        var nm1PRs = ls.First().Loops.Where(n => n.GetElement(1) == "PR");
                                        if (nm1PRs != null)
                                        {
                                            if (nm1PRs.Count() >= 1)
                                            {
                                                var nm1 = nm1PRs.First();
                                                _payer = eligibility.GetElementSpecificationDescription(interchange, "5010", nm1.SegmentId, "Entity Identifier Code", nm1.GetElement(1));
                                                _name = nm1.GetElement(3);

                                            }
                                        }

                                        var nm11Ps = ls.First().Loops.Where(n => n.GetElement(1) == "1P");
                                        if (nm11Ps != null)
                                        {
                                            if (nm11Ps.Count() >= 1)
                                            {
                                                var nm1 = nm11Ps.First();
                                                _reference_id_qualifier = eligibility.GetElementSpecificationDescription(interchange, "5010", nm1.SegmentId, "Identification Code Qualifier", nm1.GetElement(8));
                                                _reference_id = nm1.GetElement(9);
                                            }
                                        }

                                    }
                                }

                                var dtps472 = subscriberEpisode.Segments.Where(t => t.SegmentId == "DTP" && t.GetElement(1) == "472");
                                if (dtps472 != null)
                                {
                                    if (dtps472.Count() >= 1)
                                    {
                                        var dtp472 = dtps472.First();
                                        if (dtp472.GetElement(2) == "RD8")
                                        {
                                            _period_date_range = dtp472.GetElement(3);
                                        }
                                    }
                                }
                                var dtps193 = subscriberEpisode.Segments.Where(t => t.SegmentId == "DTP" && t.GetElement(1) == "193");
                                if (dtps193 != null)
                                {
                                    if (dtps193.Count() >= 1)
                                    {
                                        var dtp193 = dtps193.First();
                                        if (dtp193.GetElement(2) == "D8")
                                        {
                                            _period_start = dtp193.GetElement(3);
                                        }
                                    }
                                }
                                var dtps194 = subscriberEpisode.Segments.Where(t => t.SegmentId == "DTP" && t.GetElement(1) == "194");
                                if (dtps194 != null)
                                {
                                    if (dtps194.Count() >= 1)
                                    {
                                        var dtp194 = dtps194.First();
                                        if (dtp194.GetElement(2) == "D8")
                                        {
                                            _period_end = dtp194.GetElement(3);
                                        }
                                    }
                                }

                                episode = new Episode
                                {
                                    success = "Yes",
                                    name = _name,
                                    payer = _payer,
                                    period_date_range = _period_date_range,
                                    period_end = _period_end,
                                    period_start = _period_start,
                                    reference_id = _reference_id,
                                    reference_id_qualifier = _reference_id_qualifier
                                };
                            }
                        }
                    }
                }
                if (episode == null)
                {
                    episode = new Episode { success = "No" };
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return episode;
        }

        private FunctionalAcknowledgment GetFunctionalAcknowledgment(Interchange interchange)
        {
            FunctionalAcknowledgment functionalAcknowledgment = null;
            try
            {
                if (interchange != null)
                {
                    var acknowledgment = (Transaction)eligibility.GetContainer(interchange, typeof(Transaction), 1, "999");
                    
                    if (acknowledgment != null)
                    {
                        if (acknowledgment.Segments != null)
                        {
                            if (acknowledgment.Segments.Count() >= 1)
                            {
                                string _functional_identifier_code = null;
                                string _transaction_set_identifier_code = null;
                                string _segment_id_code = null;
                                string _segment_syntax_error_code = null;
                                string _position_in_segment = null;
                                string _data_element_syntax_error_code = null;
                                string _copy_of_bad_data_element = null;
                                string _functional_group_acknowledge_code = null;

                                var AK1s = acknowledgment.Segments.Where(a => a.SegmentId == "AK1");
                                if (AK1s != null)
                                {
                                    if (AK1s.Count() >= 1)
                                    {
                                        _functional_identifier_code = AK1s.First().GetElement(1); 
                                    }
                                }
                                var AK2s = acknowledgment.Segments.Where(a => a.SegmentId == "AK2");
                                if (AK2s != null)
                                {
                                    if (AK2s.Count() >= 1)
                                    {
                                        _transaction_set_identifier_code = AK2s.First().GetElement(1);
                                    }
                                }
                                var AK3s = acknowledgment.Segments.Where(a => a.SegmentId == "AK3");
                                if (AK3s != null)
                                {
                                    if (AK3s.Count() >= 1)
                                    {
                                        var AK3 = AK3s.First();
                                        _segment_id_code = AK3.GetElement(1);
                                        _segment_syntax_error_code = AK3.GetElement(4);
                                    }
                                }
                                var AK4s = acknowledgment.Segments.Where(a => a.SegmentId == "AK4");
                                if (AK4s != null)
                                {
                                    if (AK4s.Count() >= 1)
                                    {
                                        var AK4 = AK4s.First();
                                        _position_in_segment = AK4.GetElement(1);
                                        _data_element_syntax_error_code = AK4.GetElement(3);
                                        _copy_of_bad_data_element = AK4.GetElement(4);
                                    }
                                }
                                var AK9s = acknowledgment.Segments.Where(a => a.SegmentId == "AK9");
                                if (AK9s != null)
                                {
                                    if (AK9s.Count() >= 1)
                                    {
                                        _functional_group_acknowledge_code = AK9s.First().GetElement(1);
                                    }
                                }

                                functionalAcknowledgment = new FunctionalAcknowledgment
                                {
                                    success = "Yes",
                                    functional_identifier_code = _functional_identifier_code,
                                    transaction_set_identifier_code = _transaction_set_identifier_code,
                                    segment_id_code = _segment_id_code,
                                    segment_syntax_error_code = _segment_syntax_error_code,
                                    position_in_segment = _position_in_segment,
                                    data_element_syntax_error_code = _data_element_syntax_error_code,
                                    copy_of_bad_data_element = _copy_of_bad_data_element,
                                    functional_group_acknowledge_code = _functional_group_acknowledge_code
                                };
                            }
                        }
                    }
                }
                if (functionalAcknowledgment == null)
                {
                    functionalAcknowledgment = new FunctionalAcknowledgment { success = "No" };
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return functionalAcknowledgment;
        }

        private HealthBenefitPlanCoverage GetHealthBenefitPlanCoverage(Interchange interchange)
        {
            HealthBenefitPlanCoverage healthBenefitPlanCoverage = null;
            try
            {
                if (interchange != null)
                {
                    if (SetSubscriberName(interchange))
                    {
                        string _address1 = null;
                        string _city = null;
                        string _date = null;
                        string _insurance_type = null;
                        string _name = null;
                        string _payer = null;
                        string _phone = null;
                        string _reference_id_qualifier = null;
                        string _reference_id = null;
                        string _state = null;
                        string _zip = null;

                        var subscriberHMOs = subscriberName.Loops.Where(t => (t.GetElement(1) == "R" && t.GetElement(3) == "30") && (t.GetElement(4) == "HN" || t.GetElement(4) == "HM"));
                        if (subscriberHMOs != null)
                        {
                            if (subscriberHMOs.Count() >= 1)
                            {
                                var subscriberHMO = subscriberHMOs.First();
                                _insurance_type = eligibility.GetElementSpecificationDescription(interchange, "5010", subscriberHMO.SegmentId, "Insurance Type Code", subscriberHMO.GetElement(4));

                                var ls = subscriberHMO.Loops.Where(l => l.SegmentId == "LS");
                                if (ls != null)
                                {
                                    if (ls.Count() >= 1)
                                    {
                                        var nm1PRPs = ls.First().Loops.Where(n => n.GetElement(1) == "PRP");
                                        if (nm1PRPs != null)
                                        {
                                            if (nm1PRPs.Count() >= 1)
                                            {
                                                var nm1 = nm1PRPs.First();
                                                _payer = eligibility.GetElementSpecificationDescription(interchange, "5010", nm1.SegmentId, "Entity Identifier Code", nm1.GetElement(1));
                                                _name = nm1.GetElement(3);

                                                var nm1Segments = nm1.Segments;
                                                if (nm1Segments != null)
                                                {
                                                    if (nm1Segments.Count() >= 1)
                                                    {
                                                        var n301s = nm1Segments.Where(n => n.SegmentId == "N3");
                                                        if (n301s != null)
                                                        {
                                                            if (n301s.Count() >= 1)
                                                            {
                                                                _address1 = n301s.First().GetElement(1);
                                                            }
                                                        }
                                                        var n4s = nm1Segments.Where(n => n.SegmentId == "N4");
                                                        if (n4s != null)
                                                        {
                                                            if (n4s.Count() >= 1)
                                                            {
                                                                var n4 = n4s.First();
                                                                _city = n4.GetElement(1);
                                                                _state = n4.GetElement(2);
                                                                _zip = n4.GetElement(3);
                                                            }
                                                        }
                                                        var pers = nm1Segments.Where(n => n.SegmentId == "PER");
                                                        if (pers != null)
                                                        {
                                                            if (pers.Count() >= 1)
                                                            {
                                                                _phone = pers.First().GetElement(4);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                var hmoSegments = subscriberHMO.Segments;
                                if (hmoSegments != null)
                                {
                                    if (hmoSegments.Count() >= 1)
                                    {
                                        var hmoREFs = hmoSegments.Where(r => r.SegmentId == "REF");
                                        if (hmoREFs != null)
                                        {
                                            if (hmoREFs.Count() >= 1)
                                            {
                                                var hmoREF = hmoREFs.First();
                                                _reference_id_qualifier = eligibility.GetElementSpecificationDescription(interchange, "5010", hmoREF.SegmentId, "Reference Identification Qualifier", hmoREF.GetElement(1));
                                                _reference_id = hmoREF.GetElement(2);
                                            }
                                        }
                                        var hmoDTPs = hmoSegments.Where(r => r.SegmentId == "DTP");
                                        if (hmoDTPs != null)
                                        {
                                            if (hmoDTPs.Count() >= 1)
                                            {
                                                var hmoDTP = hmoDTPs.First();
                                                _date = hmoDTP.GetElement(3);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        healthBenefitPlanCoverage = new HealthBenefitPlanCoverage
                        {
                            success = "Yes",
                            address1 = _address1,
                            city = _city,
                            date = _date,
                            insurance_type = _insurance_type,
                            name = _name,
                            payer = _payer,
                            phone = _phone,
                            reference_id = _reference_id,
                            reference_id_qualifier = _reference_id_qualifier,
                            state = _state,
                            zip = _zip
                        };
                    }
                }
                if (healthBenefitPlanCoverage == null)
                {
                    healthBenefitPlanCoverage = new HealthBenefitPlanCoverage { success = "No" };
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return healthBenefitPlanCoverage;
        }

        private MedicarePart GetMedicarePartA(Interchange interchange)
        {
            MedicarePart medicarePartA = null;
            try
            {
                if (SetSubscriberName(interchange))
                {
                    var subscriberEligibilitys = subscriberName.Loops.Where(t => t.GetElement(4) == "MA");
                    if (subscriberEligibilitys != null)
                    {
                        if (subscriberEligibilitys.Count() >= 1)
                        {
                            var subscriberEligibility = subscriberEligibilitys.First();
                            string _date = null;
                            var dtps = subscriberEligibility.Segments.Where(t => t.SegmentId == "DTP");
                            if (dtps != null)
                            {
                                if (dtps.Count() >= 1)
                                {
                                    _date = dtps.First().GetElement(3);
                                }
                            }

                            medicarePartA = new MedicarePart
                            {
                                success = "Yes",
                                date = _date,
                                eligibility_or_benefit_information = eligibility.GetElementSpecificationDescription(interchange, "5010", subscriberEligibility.SegmentId, "Eligibility or Benefit Information Code", subscriberEligibility.GetElement(1)),
                                insurance_type_code = eligibility.GetElementSpecificationDescription(interchange, "5010", subscriberEligibility.SegmentId, "Insurance Type Code", subscriberEligibility.GetElement(4))
                            };
                        }
                    }
                }

                if (medicarePartA == null)
                {
                    medicarePartA = new MedicarePart { success = "No" };
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return medicarePartA;
        }

        private MedicarePart GetMedicarePartB(Interchange interchange)
        {
            MedicarePart medicarePartB = null;
            try
            {
                if (SetSubscriberName(interchange))
                {
                    var subscriberEligibilitys = subscriberName.Loops.Where(t => t.GetElement(4) == "MB");
                    if (subscriberEligibilitys != null)
                    {
                        if (subscriberEligibilitys.Count() >= 1)
                        {
                            var subscriberEligibility = subscriberEligibilitys.First();
                            string _date = null;
                            var dtps = subscriberEligibility.Segments.Where(t => t.SegmentId == "DTP");
                            if (dtps != null)
                            {
                                if (dtps.Count() >= 1)
                                {
                                    _date = dtps.First().GetElement(3);
                                }
                            }

                            medicarePartB = new MedicarePart
                            {
                                success = "Yes",
                                date = _date,
                                eligibility_or_benefit_information = eligibility.GetElementSpecificationDescription(interchange, "5010", subscriberEligibility.SegmentId, "Eligibility or Benefit Information Code", subscriberEligibility.GetElement(1)),
                                insurance_type_code = eligibility.GetElementSpecificationDescription(interchange, "5010", subscriberEligibility.SegmentId, "Insurance Type Code", subscriberEligibility.GetElement(4))
                            };
                        }
                    }
                }

                if (medicarePartB == null)
                {
                    medicarePartB = new MedicarePart { success = "No" };
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return medicarePartB;
        }

        private RequestResult GetRequestResult()
        {
            RequestResult RequestResult = null;
            try
            {
                if (!string.IsNullOrEmpty(eligibility.ResponseStatus))
                {
                    RequestResult = new RequestResult
                    {
                        response = eligibility.ResponseStatus,
                        success = "Yes"
                    };
                }
                else
                {
                    RequestResult = new RequestResult { success = "No" };
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return RequestResult;
        }

        private RequestValidation GetRequestValidation(Interchange interchange)
        {
            RequestValidation requestValidation = null;
            try
            {
                if (interchange != null)
                {
                    requestValidation = new RequestValidation
                    {
                        success = "No",
                        follow_up_action_code = string.Empty,
                        reject_reason_code = string.Empty,
                        yes_no_response_code = string.Empty
                    };
                }
                if (requestValidation == null)
                {
                    requestValidation = new RequestValidation { success = "No" };
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return requestValidation;
        }

        private Subscriber GetSubscriber(Interchange interchange)
        {
            Subscriber subscriber = null;
            try
            {
                if (SetSubscriberName(interchange))
                {
                    string _date = null;
                    string _gender = null;
                    var dmgs = subscriberName.Segments.Where(t => t.SegmentId == "DMG");
                    if(dmgs != null)
                    {
                        if(dmgs.Count() >= 1)
                        {
                            var dmg = dmgs.First();
                            _date = dmg.GetElement(2);
                            switch (dmg.GetElement(3))
                            {
                                case "F":
                                    _gender = "Female";
                                    break;
                                case "M":
                                    _gender = "Male";
                                    break;
                            }
                        }
                    }
                    subscriber = new Subscriber
                    {
                        success = "Yes",
                        date = _date,
                        last_name = subscriberName.GetElement(3),
                        first_name = subscriberName.GetElement(4),
                        middle_name = subscriberName.GetElement(5),
                        identification_code = subscriberName.GetElement(9),
                        gender = _gender
                    };
                }

                if (subscriber == null)
                {
                    subscriber = new Subscriber { success = "No" };
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return subscriber;
        }

        private bool SetSubscriberName(Interchange interchange)
        {
            try
            {
                if (interchange != null)
                {
                    if (setSubscriberLoop(interchange))
                    {
                        if (subscriberName == null)
                        {
                            subscriberName = subscriberLoop.Loops.Where(t => t.SegmentId == "NM1" && t.GetElement(1) == "IL").First();
                        }
                        else if (subscriberName.SegmentId != "NM1" && subscriberName.GetElement(1) == "IL")
                        {
                            subscriberName = subscriberLoop.Loops.Where(t => t.SegmentId == "NM1" && t.GetElement(1) == "IL").First();
                        }

                        if (subscriberName != null)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return false;
        }

        private bool setSubscriberLoop(Interchange interchange)
        {
            try
            {
                if (interchange != null)
                {
                    if (subscriberLoop == null)
                    {
                        subscriberLoop = (HierarchicalLoop)eligibility.GetContainer(interchange, typeof(HierarchicalLoop), 3, "22");
                    }
                    else if (subscriberLoop.GetElement(3) != "22")
                    {
                        subscriberLoop = (HierarchicalLoop)eligibility.GetContainer(interchange, typeof(HierarchicalLoop), 3, "22");
                    }

                    if (subscriberLoop != null)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return false;
        }

        private Loop getSubscriberEligibility(IEnumerable<Loop> subscriberEligibilitys)
        {
            Loop subscriberEligibility = null;
            try
            {
                if (subscriberEligibilitys != null)
                {
                    if (subscriberEligibilitys.Count() >= 1)
                    {
                        subscriberEligibility = subscriberEligibilitys.First();
                        var currentDtp472 = subscriberEligibility.Segments.Where(t => t.SegmentId == "DTP" && t.GetElement(1) == "472");
                        foreach (var loop in subscriberEligibilitys)
                        {
                            var loopDtp472 = loop.Segments.Where(t => t.SegmentId == "DTP" && t.GetElement(1) == "472");
                            string strcurrentDtp03;
                            string strloopDtp03;
                            DateTime loopDtp03;
                            DateTime currentDtp03;
                            string[] format = { "yyyyMMdd" };
                            if (currentDtp472 != null && loopDtp472 != null)
                            {
                                if (currentDtp472.First().GetElement(2) == "RD8")
                                {
                                    strcurrentDtp03 = currentDtp472.First().GetElement(3).Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries).ElementAt(1);
                                }
                                else
                                {
                                    strcurrentDtp03 = currentDtp472.First().GetElement(3);
                                }

                                if (loopDtp472.First().GetElement(2) == "RD8")
                                {
                                    strloopDtp03 = loopDtp472.First().GetElement(3).Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries).ElementAt(1);
                                }
                                else
                                {
                                    strloopDtp03 = loopDtp472.First().GetElement(3);
                                }

                                if (DateTime.TryParseExact(strcurrentDtp03, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out loopDtp03) && DateTime.TryParseExact(strloopDtp03, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out currentDtp03))
                                {
                                    if (DateTime.Compare(currentDtp03, loopDtp03) >= 1)
                                    {
                                        currentDtp472 = loopDtp472;
                                        subscriberEligibility = loop;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry("EligibilityService.cs => " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw GetFault<DefaultFault>(ex.ToString());
            }
            return subscriberEligibility;
        }

        #endregion
    }
}
