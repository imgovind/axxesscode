﻿namespace Axxess.Api.Eligibility
{
    using System;
    using System.ServiceProcess;
    using System.ComponentModel;
    using System.Configuration.Install;

    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private ServiceInstaller eligibilityInstaller;
        private ServiceProcessInstaller processInstaller;

        public ProjectInstaller()
        {
            InitializeComponent();

            this.processInstaller = new ServiceProcessInstaller();
            this.processInstaller.Account = ServiceAccount.LocalSystem;

            this.eligibilityInstaller = new ServiceInstaller();
            this.eligibilityInstaller.StartType = ServiceStartMode.Automatic;
            this.eligibilityInstaller.ServiceName = "EligibilityService";
            this.eligibilityInstaller.DisplayName = "Eligibility Service";
            this.eligibilityInstaller.Description = "Provides Patient Eligibility checks to the AgencyCore Application.";

            Installers.AddRange(new Installer[] { this.processInstaller, this.eligibilityInstaller });
        }
    }
}
