﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;

    using Axxess.AgencyManagement.Domain;

    public interface IUserService
    {
        bool CreateUser(User user);
        bool IsEmailAddressInUse(string emailAddress);
    }
}
