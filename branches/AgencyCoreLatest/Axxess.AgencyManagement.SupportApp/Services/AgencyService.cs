﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    public class AgencyService : IAgencyService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;

        public AgencyService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, ILookUpDataProvider lookUpDataProvider)
        {
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region IAgencyService Members

        public bool CreateAgency(Agency agency)
        {
            try
            {
                var submitterInfo = agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger()? agencyRepository.SubmitterInfo(agency.Payor.ToInteger()): new AxxessSubmitterInfo();
                agency.IsSuspended = false;
                if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count == 3)
                {
                    agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count == 3)
                {
                    agency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count == 3)
                {
                    agency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                }
                if (agency.IsAxxessTheBiller)
                {
                    agency.SubmitterId = submitterInfo != null && submitterInfo.SubmitterId.IsNotNullOrEmpty() ? submitterInfo.SubmitterId : agency.SubmitterId;
                    agency.SubmitterName = submitterInfo != null && submitterInfo.SubmitterName.IsNotNullOrEmpty() ? submitterInfo.SubmitterName : agency.SubmitterId;
                    agency.SubmitterPhone = submitterInfo != null && submitterInfo.Phone.IsNotNullOrEmpty() ? submitterInfo.Phone : agency.SubmitterPhone;
                    agency.SubmitterFax = submitterInfo != null && submitterInfo.Fax.IsNotNullOrEmpty() ? submitterInfo.Fax : agency.SubmitterFax;
                }

                if (agencyRepository.Add(agency))
                {
                    agencyRepository.InsertTemplates(agency.Id);
                    agencyRepository.InsertSupplies(agency.Id);

                    var location = new AgencyLocation();
                    location.Id = Guid.NewGuid();
                    location.Name = agency.LocationName;
                    location.AddressLine1 = agency.AddressLine1;
                    location.AddressLine2 = agency.AddressLine2;
                    location.AddressCity = agency.AddressCity;
                    location.AddressStateCode = agency.AddressStateCode;
                    location.AddressZipCode = agency.AddressZipCode;
                    location.AddressZipCodeFour = agency.AddressZipCodeFour;
                    location.AgencyId = agency.Id;
                    location.IsMainOffice = true;
                    location.IsDeprecated = false;
                    location.MedicareProviderNumber = agency.MedicareProviderNumber;
                    location.SubmitterId = agency.SubmitterId;
                    location.SubmitterName = agency.SubmitterName;
                    location.Payor = agency.Payor;
                    location.SubmitterPhone = agency.SubmitterPhone;
                    location.SubmitterFax = agency.SubmitterFax;
                    location.IsSubmitterInfoTheSame = true;
                    location.IsAxxessTheBiller = agency.IsAxxessTheBiller;

                    if (agency.PhoneArray != null && agency.PhoneArray.Count > 0)
                    {
                        location.PhoneWork = agency.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (agency.FaxArray != null && agency.FaxArray.Count > 0)
                    {
                        location.FaxNumber = agency.FaxArray.ToArray().PhoneEncode();
                    }

                    string subject = string.Format("{0} needs to be trained.", agency.Name);
                    string bodyText = MessageBuilder.PrepareTextFrom("TrainingRequest", 
                        "displayname", loginRepository.GetLoginDisplayName(agency.Trainer), 
                        "agencyname", agency.Name,
                        "agencyphone", location.PhoneWorkFormatted,
                        "salesperson", loginRepository.GetLoginDisplayName(agency.SalesPerson));

                    Notify.User(CoreSettings.NoReplyEmail, loginRepository.GetLoginEmailAddress(agency.Trainer), string.Format("{0};{1}", loginRepository.GetLoginEmailAddress(agency.BackupTrainer), loginRepository.GetLoginEmailAddress(agency.SalesPerson)), subject, bodyText);

                    var defaultDisciplineTasks = lookupRepository.DisciplineTasks();
                    if (defaultDisciplineTasks != null)
                    {
                        var costRates = new List<ChargeRate>();
                        defaultDisciplineTasks.ForEach(r =>
                        {
                            if (r.Discipline.IsEqual("Nursing") || r.Discipline.IsEqual("PT") || r.Discipline.IsEqual("OT") || r.Discipline.IsEqual("ST") || r.Discipline.IsEqual("HHA") || r.Discipline.IsEqual("MSW"))
                            {
                                costRates.Add(new ChargeRate { Id = r.Id, Code = r.GCode, RevenueCode = r.RevenueCode, Unit = r.Unit, ChargeType = ((int)BillUnitType.Per15Min).ToString(),Charge=r.Rate });
                            }
                        });
                        location.BillData = costRates.ToXml();
                    }

                    string cbsa = lookupRepository.CbsaCodeByZip(agency.AddressZipCode);
                    location.CBSA = cbsa != null ? cbsa : string.Empty;

                    if (agencyRepository.AddLocation(location))
                    {
                        var user = new User
                        {
                            AgencyId = agency.Id,
                            AgencyName = agency.Name,
                            AllowWeekendAccess = true,
                            EmploymentType = "Employee",
                            AgencyLocationId = location.Id,
                            Status = (int)UserStatus.Active,
                            LastName = agency.AgencyAdminLastName,
                            FirstName = agency.AgencyAdminFirstName,
                            PermissionsArray = GeneratePermissions(),
                            EmailAddress = agency.AgencyAdminUsername,
                            Credentials = CredentialTypes.None.GetDescription(),
                            TitleType = TitleTypes.Administrator.GetDescription(),
                            Profile = new UserProfile() { EmailWork = agency.AgencyAdminUsername },
                            AgencyRoleList = new List<string>() { ((int)AgencyRoles.Administrator).ToString() }
                        };

                        IUserService userService = Container.Resolve<IUserService>();
                        return userService.CreateUser(user);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            return false;
        }

        public bool UpdateAgency(Agency agency)
        {
            bool result = false;
            var existingAgency = agencyRepository.Get(agency.Id);
            if (agency != null)
            {
                existingAgency.Name = agency.Name;
                existingAgency.TaxId = agency.TaxId;
                existingAgency.IsAgreementSigned = agency.IsAgreementSigned;
                existingAgency.TrialPeriod = agency.TrialPeriod;
                existingAgency.Package = agency.Package;
                existingAgency.ContactPersonEmail = agency.ContactPersonEmail;
                existingAgency.ContactPersonFirstName = agency.ContactPersonFirstName;
                existingAgency.ContactPersonLastName = agency.ContactPersonLastName;
                existingAgency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                existingAgency.TaxIdType = agency.TaxIdType;
                existingAgency.Payor = agency.Payor;
                existingAgency.SubmitterId = agency.SubmitterId;
                existingAgency.SubmitterName = agency.SubmitterName;
                existingAgency.NationalProviderNumber = agency.NationalProviderNumber;
                existingAgency.MedicareProviderNumber = agency.MedicareProviderNumber;
                existingAgency.MedicaidProviderNumber = agency.MedicaidProviderNumber;
                existingAgency.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                existingAgency.CahpsVendor = agency.CahpsVendor;
                existingAgency.IsAxxessTheBiller = agency.IsAxxessTheBiller;
                
                existingAgency.SalesPerson = agency.SalesPerson;
                existingAgency.Trainer = agency.Trainer;

                if (!agency.IsAxxessTheBiller)
                {
                    existingAgency.SubmitterId = agency.SubmitterId;
                    existingAgency.SubmitterName = agency.SubmitterName;
                    existingAgency.Payor = agency.Payor;
                    if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count > 0)
                    {
                        existingAgency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                    }
                    if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count > 0)
                    {
                        existingAgency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                    }
                }
                if (agencyRepository.Update(existingAgency))
                {
                    var main = agencyRepository.GetMainLocation(agency.Id);
                    if (main != null)
                    {
                        main.MedicareProviderNumber = agency.MedicareProviderNumber;
                        if (agencyRepository.UpdateLocation(main))
                        {
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        public bool CreateLocation(AgencyLocation location)
        {
            var result = false;
            try
            {
                location.Id = Guid.NewGuid();
               
                if (location.SubmitterPhoneArray != null && location.SubmitterPhoneArray.Count > 0)
                {
                    location.SubmitterPhone = location.SubmitterPhoneArray.ToArray().PhoneEncode();
                }

                if (location.SubmitterFaxArray != null && location.SubmitterFaxArray.Count > 0)
                {
                    location.SubmitterFax = location.SubmitterFaxArray.ToArray().PhoneEncode();
                }
                if (location.IsLocationStandAlone)
                {
                    if (location.IsAxxessTheBiller)
                    {
                        var submitterInfo = location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger() ? agencyRepository.SubmitterInfo(location.Payor.ToInteger()) : new AxxessSubmitterInfo();
                       // var agency = agencyRepository.Get(location.AgencyId);
                        if (submitterInfo != null)
                        {
                            //location.SubmitterId = agency.SubmitterId;
                            //location.SubmitterName = agency.SubmitterName;
                            //location.Payor = agency.Payor;
                            //location.SubmitterPhone = agency.SubmitterPhone;
                            //location.SubmitterFax = agency.SubmitterFax;

                            location.SubmitterId = submitterInfo.SubmitterId.IsNotNullOrEmpty() ? submitterInfo.SubmitterId : location.SubmitterId;
                            location.SubmitterName = submitterInfo.SubmitterName.IsNotNullOrEmpty() ? submitterInfo.SubmitterName : location.SubmitterId;
                            location.SubmitterPhone = submitterInfo.Phone.IsNotNullOrEmpty() ? submitterInfo.Phone : location.SubmitterPhone;
                            location.SubmitterFax = submitterInfo.Fax.IsNotNullOrEmpty() ? submitterInfo.Fax : location.SubmitterFax;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    
                }
                string cbsa = lookupRepository.CbsaCodeByZip(location.AddressZipCode);
                location.CBSA = cbsa != null ? cbsa : string.Empty;
                if (agencyRepository.AddLocation(location))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }

            return result;
        }

        public bool UpdateLocation(AgencyLocation location)
        {
            var result = false;
            var existingLocation = agencyRepository.FindLocation(location.AgencyId, location.Id);// database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
            if (location != null && existingLocation != null)
            {
                if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                {
                    existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                }
                if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                {
                    existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                }
                //existingLocation.IsSubmitterInfoTheSame = location.IsSubmitterInfoTheSame;
                existingLocation.IsLocationStandAlone = location.IsLocationStandAlone;
                if (location.SubmitterPhoneArray != null && location.SubmitterPhoneArray.Count > 0)
                {
                    existingLocation.SubmitterPhone = location.SubmitterPhoneArray.ToArray().PhoneEncode();
                }

                if (location.SubmitterFaxArray != null && location.SubmitterFaxArray.Count > 0)
                {
                    existingLocation.SubmitterFax = location.SubmitterFaxArray.ToArray().PhoneEncode();
                }
                if (location.ContactPhoneArray != null && location.ContactPhoneArray.Count == 3)
                {
                    existingLocation.ContactPersonPhone = location.ContactPhoneArray.ToArray().PhoneEncode();
                }
                existingLocation.IsAxxessTheBiller = location.IsAxxessTheBiller;
                if (existingLocation.IsLocationStandAlone)
                {
                    if (existingLocation.IsAxxessTheBiller)
                    {
                        var submitterInfo = location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger() ? agencyRepository.SubmitterInfo(location.Payor.ToInteger()) : new AxxessSubmitterInfo();
                        // var agency = agencyRepository.Get(location.AgencyId);
                        if (submitterInfo != null)
                        {
                            //location.SubmitterId = agency.SubmitterId;
                            //location.SubmitterName = agency.SubmitterName;
                            //location.Payor = agency.Payor;
                            //location.SubmitterPhone = agency.SubmitterPhone;
                            //location.SubmitterFax = agency.SubmitterFax;
                            existingLocation.Payor = location.Payor;
                            existingLocation.SubmitterId = submitterInfo.SubmitterId.IsNotNullOrEmpty() ? submitterInfo.SubmitterId : location.SubmitterId;
                            existingLocation.SubmitterName = submitterInfo.SubmitterName.IsNotNullOrEmpty() ? submitterInfo.SubmitterName : location.SubmitterName;
                            existingLocation.SubmitterPhone = submitterInfo.Phone.IsNotNullOrEmpty() ? submitterInfo.Phone : location.SubmitterPhone;
                            existingLocation.SubmitterFax = submitterInfo.Fax.IsNotNullOrEmpty() ? submitterInfo.Fax : location.SubmitterFax;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        existingLocation.Payor = location.Payor;
                        existingLocation.SubmitterId = location.SubmitterId;
                        existingLocation.SubmitterName = location.SubmitterName;
                       // existingLocation.SubmitterPhone = location.SubmitterPhone;
                       // existingLocation.SubmitterFax = location.SubmitterFax;
                    }

                    existingLocation.IsLocationStandAlone = location.IsLocationStandAlone;
                    existingLocation.TaxId = location.TaxId;
                    existingLocation.TaxIdType = location.TaxIdType;
                    existingLocation.NationalProviderNumber = location.NationalProviderNumber;
                    existingLocation.MedicaidProviderNumber = location.MedicaidProviderNumber;
                    existingLocation.HomeHealthAgencyId = location.HomeHealthAgencyId;
                    existingLocation.ContactPersonFirstName = location.ContactPersonFirstName;
                    existingLocation.ContactPersonLastName = location.ContactPersonLastName;
                    existingLocation.ContactPersonEmail = location.ContactPersonEmail;
                    //existingLocation.ContactPersonPhone = location.ContactPersonPhone;
                    existingLocation.CahpsVendor = location.CahpsVendor;
                    existingLocation.CahpsVendorId = location.CahpsVendorId;
                    existingLocation.CahpsSurveyDesignator = location.CahpsSurveyDesignator;
                    existingLocation.IsAxxessTheBiller = location.IsAxxessTheBiller;
                    existingLocation.OasisAuditVendor = location.OasisAuditVendor;
                }
                existingLocation.BranchId = location.BranchId;
                existingLocation.BranchIdOther = location.BranchIdOther;
                existingLocation.Name = location.Name;
                existingLocation.CustomId = location.CustomId;
                //existingLocation.MedicareProviderNumber = location.MedicareProviderNumber;
                existingLocation.AddressLine1 = location.AddressLine1;
                existingLocation.AddressLine2 = location.AddressLine2;
                existingLocation.AddressCity = location.AddressCity;
                existingLocation.AddressStateCode = location.AddressStateCode;
                existingLocation.AddressZipCode = location.AddressZipCode;
                existingLocation.AddressZipCodeFour = location.AddressZipCodeFour;
                existingLocation.Comments = location.Comments;
                result = agencyRepository.EditLocationModal(existingLocation);
            }
            return result;
        }

        public Agency GetAgency(Guid Id)
        {
            Agency agencyViewData = agencyRepository.Get(Id);
            if (agencyViewData != null)
            {
                return agencyViewData;
            }
            return null;
        }

        #endregion

        #region Private Methods

        private List<string> GeneratePermissions()
        {
            var list = new List<string>();

            var permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            foreach (Permissions permission in permissions)
            {
                ulong permissionId = (ulong)permission;
                if (permissionId != 0)
                {
                    list.Add((permissionId).ToString());
                }
            }

            return list;
        }

        #endregion
    }
}
