﻿namespace Axxess.Scheduled.Eligibility.Extensions
{
    using System;
    public static class AbilityExtensions
    {
        public static hiqhRequest UseHost(this hiqhRequest request, string host)
        {
            if (request != null && request.searchCriteria.Length > 0)
            {
                request.searchCriteria[0].hostId = host;
            }
            return request;
        }

        public static hiqhRequest UseCredentials(this hiqhRequest request, string userId, string password)
        {
            if (request != null && request.medicareMainframe.Length > 0)
            {
                if (request.medicareMainframe[0].credential != null && request.medicareMainframe[0].credential.Length > 0)
                {
                    request.medicareMainframe[0].credential[0].userId = userId;
                    request.medicareMainframe[0].credential[0].userId = password;
                }
            }
            return request;
        }
    }
}
