﻿namespace Axxess.Scheduled.Eligibility.Extensions
{
    using System;
    using System.Security.Cryptography.X509Certificates;

    public static class CertificateExtension
    {
        public static X509Certificate2 Get(this X509Certificate2Collection certificates, string searchString)
        {
            X509Certificate2 x509 = null;
            foreach (X509Certificate2 certificate in certificates)
            {
                if (certificate.FriendlyName.Contains(searchString))
                {
                    x509 = certificate;
                    break;
                }
            }
            return x509;
        }
    }
}
