﻿namespace Axxess.Scheduled.Eligibility
{
    using System;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class header : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string systemDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string systemTime { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string dataSource { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string intermediaryNumber { get; set; }

        [XmlElement("beneficiary", Form = XmlSchemaForm.Unqualified)]
        public headerBeneficiary[] beneficiary { get; set; }
    }

    public class headerBeneficiary : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string hic { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string lastName { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string firstInitial { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string dateOfBirth { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string sex { get; set; }
    }

    public class homeHealthBenefitPeriods : BaseResponse
    {
        [XmlElement("homeHealthBenefitPeriod", Form = XmlSchemaForm.Unqualified)]
        public homeHealthBenefitPeriodsHomeHealthBenefitPeriod[] homeHealthBenefitPeriod { get; set; }

        [XmlElement("header")]
        public header[] header { get; set; }

        [XmlElement("homeHealthBenefitPeriods")]
        public homeHealthBenefitPeriods[] homeHealthBenefitPeriodList { get; set; }
    }

    public class homeHealthBenefitPeriodsHomeHealthBenefitPeriod : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string spellNumber { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string qualifyingIndicator { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string partAVisitsRemaining { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string earliestBillingDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string latestBillingDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string partBVisitsApplied { get; set; }
    }

    public class homeHealthEpisodes : BaseResponse
    {
        [XmlElement("homeHealthEpisode", Form = XmlSchemaForm.Unqualified)]
        public homeHealthEpisodesHomeHealthEpisode[] homeHealthEpisode { get; set; }

        [XmlElement("header")]
        public header[] header { get; set; }

        [XmlElement("homeHealthEpisodes")]
        public homeHealthEpisodes[] homeHealthEpisodeList { get; set; }
    }

    public class homeHealthEpisodesHomeHealthEpisode : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string startDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string endDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string intermediaryNumber { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string providerNumber { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string earliestBillingDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string latestBillingDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string patientStatusCode { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string validCancelIndicator { get; set; }
    }
    
    public class preventativeServices : BaseResponse
    {
        [XmlElement("preventativeService", Form = XmlSchemaForm.Unqualified)]
        public preventativeServicesPreventativeService[] preventativeService { get; set; }
        
        [XmlElement("header")]
        public header[] header { get; set; }

        
        [XmlElement("preventativeServices")]
        public preventativeServices[] preventativeServiceList { get; set; }
    }

    public class preventativeServicesPreventativeService : BaseResponse
    {        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string category { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string hcpcs { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string technicalDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string professionalDate { get; set; }
    }
    
    public class hcpcsCodes : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string hcpcs { get; set; }
    }

    public class behavioralServices : BaseResponse
    {        
        [XmlElement("behavioralService", Form = XmlSchemaForm.Unqualified)]
        public behavioralServicesBehavioralService[] behavioralService { get; set; }
        
        [XmlElement("header")]
        public header[] header { get; set; }

        [XmlElement("behavioralServices")]
        public behavioralServices[] behavioralServiceList { get; set; }
    }
    
    public class behavioralServicesBehavioralService : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string name { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string hcpcs { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string technicalDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string technicalRemaining { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string professionalDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string professionalRemaining { get; set; }
    }
    
    public class medicareSecondaryPayers : BaseResponse
    {        
        [XmlElement("medicareSecondaryPayer", Form = XmlSchemaForm.Unqualified)]
        public medicareSecondaryPayersMedicareSecondaryPayer[] medicareSecondaryPayer { get; set; }
        
        [XmlElement("header")]
        public header[] header { get; set; }
        
        [XmlElement("medicareSecondaryPayers")]
        public medicareSecondaryPayers[] medicareSecondaryPayerList { get; set; }
    }
    
    public class medicareSecondaryPayersMedicareSecondaryPayer : BaseResponse
    {        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string record { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string mspCode { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string mspCodeDescription { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string effectiveDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string terminationDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string intermediaryNumber { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string accretionDate { get; set; }
    }
    
    [XmlRoot(Namespace="", IsNullable=false)]
    public class hiqhResponse : BaseResponse
    {
        [XmlElement("behavioralServices", typeof(behavioralServices))]
        [XmlElement("beneficiaryInformation", typeof(hiqhResponseBeneficiaryInformation), Form = XmlSchemaForm.Unqualified)]
        [XmlElement("correctedBeneficiary", typeof(hiqhResponseCorrectedBeneficiary), Form = XmlSchemaForm.Unqualified)]
        [XmlElement("hcpcsCodes", typeof(hcpcsCodes))]
        [XmlElement("header", typeof(header))]
        [XmlElement("highIntensityBehavioralCounseling", typeof(hiqhResponseHighIntensityBehavioralCounseling), Form = XmlSchemaForm.Unqualified)]
        [XmlElement("homeHealthBenefitPeriods", typeof(homeHealthBenefitPeriods))]
        [XmlElement("homeHealthCertifications", typeof(hiqhResponseHomeHealthCertifications), Form = XmlSchemaForm.Unqualified)]
        [XmlElement("homeHealthEpisodes", typeof(homeHealthEpisodes))]
        [XmlElement("hospiceInformation", typeof(hiqhResponseHospiceInformation), Form = XmlSchemaForm.Unqualified)]
        [XmlElement("medicareSecondaryPayers", typeof(medicareSecondaryPayers))]
        [XmlElement("planInformation", typeof(hiqhResponsePlanInformation), Form = XmlSchemaForm.Unqualified)]
        [XmlElement("preventativeServices", typeof(preventativeServices))]
        [XmlElement("rehabilitationSessions", typeof(hiqhResponseRehabilitationSessions), Form = XmlSchemaForm.Unqualified)]
        [XmlElement("smokingCessation", typeof(hiqhResponseSmokingCessation), Form = XmlSchemaForm.Unqualified)]
        [XmlElement("telehealthServices", typeof(hiqhResponseTelehealthServices), Form = XmlSchemaForm.Unqualified)]
        public object[] Items { get; set; }
    }
    
    public class hiqhResponseBeneficiaryInformation : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string providerId { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string applicableDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string reasonCode { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string requestorId { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string dispositionCode { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string message { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string dateOfDeath { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string fullName { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string physicalTherapyRemaining { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string occupationalTherapyRemaining { get; set; }

        [XmlElement("header")]
        public header[] header { get; set; }
        
        [XmlElement("currentEntitlement", Form = XmlSchemaForm.Unqualified)]
        public hiqhResponseBeneficiaryInformationCurrentEntitlement[] currentEntitlement { get; set; }
        
        [XmlElement("partBData", Form = XmlSchemaForm.Unqualified)]
        public hiqhResponseBeneficiaryInformationPartBData[] partBData { get; set; }
    }
    
    public class hiqhResponseBeneficiaryInformationCurrentEntitlement : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string partAEffectiveDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string partATerminationDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string partBEffectiveDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string partBTerminationDate { get; set; }
    }

    public class hiqhResponseBeneficiaryInformationPartBData : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string year { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string cashDeductibleRemaining { get; set; }
    }
 
    public class hiqhResponseCorrectedBeneficiary : BaseResponse
    {        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string hic { get; set; }
     
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string lastName { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string firstInitial { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string dateOfBirth { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string sex { get; set; }
    }

    public class hiqhResponseHighIntensityBehavioralCounseling : BaseResponse
    {
        [XmlElement("header")]
        public header[] header { get; set; }
  
        [XmlElement("behavioralServices")]
        public behavioralServices[] behavioralServices { get; set; }
    }
 
    public class hiqhResponseHomeHealthCertifications : BaseResponse
    {
        [XmlElement("header")]
        public header[] header { get; set; }

        
        [XmlArrayAttribute(Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItemAttribute("planOfCareCertification", typeof(hiqhResponseHomeHealthCertificationsPlanOfCareCertificationsPlanOfCareCertification), Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public hiqhResponseHomeHealthCertificationsPlanOfCareCertificationsPlanOfCareCertification[] planOfCareCertifications { get; set; }
    }
    
    public class hiqhResponseHomeHealthCertificationsPlanOfCareCertificationsPlanOfCareCertification : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string recordNumber { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string hcpcs { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string fromDate { get; set; }
    }

    public class hiqhResponseHospiceInformation : BaseResponse
    {
        [XmlElement("header")]
        public header[] header { get; set; }
        
        [XmlArrayAttribute(Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItemAttribute("hospiceBenefitPeriod", typeof(hiqhResponseHospiceInformationHospiceBenefitPeriodsHospiceBenefitPeriod), Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public hiqhResponseHospiceInformationHospiceBenefitPeriodsHospiceBenefitPeriod[] hospiceBenefitPeriods { get; set; }
    }
    
    public class hiqhResponseHospiceInformationHospiceBenefitPeriodsHospiceBenefitPeriod : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string period { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string firstProviderStartDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string firstProviderId { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string firstIntermediaryId { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string firstOwnerChangeStartDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string firstOwnerChangeProviderId { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string firstOwnerChangeIntermediaryId { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string secondProviderStartDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string secondProviderId { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string secondIntermediaryId { get; set; }
    
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string secondOwnerChangeStartDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string secondOwnerChangeProviderId { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string secondOwnerChangeIntermediaryId { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string terminationDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string earliestBillingDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string latestBillingDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string daysUsed { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string revocationIndicator { get; set; }
    }
    
    public class hiqhResponsePlanInformation : BaseResponse
    {
        [XmlElement("header")]
        public header[] header { get; set; }
        
        [XmlArrayAttribute(Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItemAttribute("hmoPlan", typeof(hiqhResponsePlanInformationHmoPlansHmoPlan), Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public hiqhResponsePlanInformationHmoPlansHmoPlan[] hmoPlans { get; set; }
    }
    
    public class hiqhResponsePlanInformationHmoPlansHmoPlan : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string type { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string idCode { get; set; }
                
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string optionCode { get; set; }
               
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string effectiveDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string terminationDate { get; set; }
    }
    
    public class hiqhResponseRehabilitationSessions : BaseResponse
    {
        [XmlElement("header")]
        public header[] header { get; set; }
        
        [XmlElement("pulmonaryRehabilitationSessionsRemaining", Form = XmlSchemaForm.Unqualified)]
        public hiqhResponseRehabilitationSessionsPulmonaryRehabilitationSessionsRemaining[] pulmonaryRehabilitationSessionsRemaining { get; set; }
        
        [XmlElement("cardiacRehabilitationSessionsApplied", Form = XmlSchemaForm.Unqualified)]
        public hiqhResponseRehabilitationSessionsCardiacRehabilitationSessionsApplied[] cardiacRehabilitationSessionsApplied { get; set; }

        [XmlElement("intensiveCardiacRehabilitationSessionsApplied", Form = XmlSchemaForm.Unqualified)]
        public hiqhResponseRehabilitationSessionsIntensiveCardiacRehabilitationSessionsApplied[] intensiveCardiacRehabilitationSessionsApplied { get; set; }
    }

    public class hiqhResponseRehabilitationSessionsPulmonaryRehabilitationSessionsRemaining : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string technicalSessions { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string professionalSessions{ get; set; }
      
        [XmlElement("hcpcsCodes")]
        public hcpcsCodes[] hcpcsCodes{ get; set; }
    }

    public class hiqhResponseRehabilitationSessionsCardiacRehabilitationSessionsApplied : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string technicalSessions { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string professionalSessions { get; set; }
        
        [XmlElement("hcpcsCodes")]
        public hcpcsCodes[] hcpcsCodes { get; set; }
    }
    
    public class hiqhResponseRehabilitationSessionsIntensiveCardiacRehabilitationSessionsApplied : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string technicalSessions { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string professionalSessions { get; set; }

        [XmlElement("hcpcsCodes")]
        public hcpcsCodes[] hcpcsCodes { get; set; }
    }

    public class hiqhResponseSmokingCessation : BaseResponse
    {
        [XmlElement("header")]
        public header[] header { get; set; }

        [XmlArray(Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItem("period", typeof(hiqhResponseSmokingCessationCounselingPeriodsPeriod), Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public hiqhResponseSmokingCessationCounselingPeriodsPeriod[] counselingPeriods{ get; set; }

        [XmlArrayAttribute(Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItemAttribute("session", typeof(hiqhResponseSmokingCessationCounselingSessionsSession), Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public hiqhResponseSmokingCessationCounselingSessionsSession[] counselingSessions{ get; set; }
    }

    public class hiqhResponseSmokingCessationCounselingPeriodsPeriod : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string number { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string totalSessions { get; set; }
    }

    public class hiqhResponseSmokingCessationCounselingSessionsSession : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string hcpcs { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string fromDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string toDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string period { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string quantity { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string type { get; set; }
    }

    public class hiqhResponseTelehealthServices : BaseResponse
    {
        [XmlElement("header")]
        public header[] header { get; set; }

        [XmlElement("hospitalTelehealthService", Form = XmlSchemaForm.Unqualified)]
        public hiqhResponseTelehealthServicesHospitalTelehealthService[] hospitalTelehealthService { get; set; }

        [XmlElement("nursingTelehealthService", Form = XmlSchemaForm.Unqualified)]
        public hiqhResponseTelehealthServicesNursingTelehealthService[] nursingTelehealthService { get; set; }
    }

    public class hiqhResponseTelehealthServicesHospitalTelehealthService : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string name { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string nextEligibleDate { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string rule { get; set; }
        
        [XmlElement("hcpcsCodes")]
        public hcpcsCodes[] hcpcsCodes { get; set; }
       
    }

    public class hiqhResponseTelehealthServicesNursingTelehealthService : BaseResponse
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string name { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string nextEligibleDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string rule { get; set; }
        
        [XmlElement("hcpcsCodes")]
        public hcpcsCodes[] hcpcsCodes { get; set; }
        
    }
}
