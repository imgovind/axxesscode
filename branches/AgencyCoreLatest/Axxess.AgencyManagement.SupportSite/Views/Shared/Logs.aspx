﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<List<Error>>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Axxess Logs</title>
    <style type="text/css">
        html{height:100%;}
        body{overflow:auto;background:#fff;}
        body,h1,h2,h3,h4,h5,h6,textarea{font:12px/1 'Lucida Grande',Arial,'Liberation Sans',FreeSans,sans-serif;}
        .align-left{text-align:left;}
        .logs{border:solid 1px #d3d3d3;border-collapse:collapse;width:100%;margin-bottom:50px;}
        .logs thead{text-align:center;background:#c3d8f1 url(/Content/Office2007/sprite.png) repeat-x scroll 0 -352px;}
        .logs thead th{border:solid 1px #aaa;}
        .logs td{border:solid 1px #d3d3d3;padding:2px;}
        .logs td.align-center{width:100px;}
        .logs .odd td{background:#fffcf2;}
        .logs .even td{background:#e6f6ff;}
    </style>
</head>
<body>
    <table class="logs">
    <thead class="align-center">
        <tr><th colspan="3"><%= Environment.MachineName %> ERRORS</th></tr>
        <tr><th class="align-left">&nbsp;Message</th><th class="align-left">&nbsp;Type</th><th class="align-left">&nbsp;Date</th></tr>
    </thead>
    <tbody><%
            int i = 1;
            foreach (var error in Model) { %>
            <tr class='<%= i % 2 != 0 ? "odd" : "even" %>'>
                <td>&nbsp;<%= error.Message %></td>
                <td>&nbsp;<%= error.Type %></td>
                <td>&nbsp;<%= error.Created.ToString() %></td>
            </tr>
            <% i++; %>
        <% } %>
    </tbody>
    </table>
</body>
</html>
