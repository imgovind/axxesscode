﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="dashboard-message-container" class="wrapper">
<%  using (Html.BeginForm("AddDashboardMessage", "System", FormMethod.Post, new { @id = "new-dashboard-message-form" })) { %>
    <div class="new-dashboard-content" id="new-dashboard-content">
        <div class="inbox-subheader">
            <span>New Dashboard Message</span>
            <div class="buttons float-right">
                <ul>
                    <li><a href="javascript:void(0);" onclick="UserInterface.PreviewDashboardMessage(CKEDITOR.instances['NewDashboardMessageBody'].getData());">Preview</a></li>
                    <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newdashboardmessage');">Cancel</a></li>
                </ul>
            </div>
        </div>
        <div class="buttons float-left">
            <ul>
                <li class="send"><a href="javascript:void(0);" onclick="$(this).closest('form').submit();"><span class="img icon send"></span><br />Send</a></li>
            </ul>
        </div>
        <div class="new-message-content-panel clear">
            <div class="new-message-row">
                <label for="New_DashboardMessage_Subject">Title:</label>
                <input type="text" id="New_DashboardMessage_Subject" name="Title" maxlength="150" />
            </div>
            <div class="new-message-row">
                <label for="New_DashboardMessage_CreatedBy">Created By:</label>
                <span id="New_DashboardMessage_CreatedBy"><%= Current.DisplayName %></span>
            </div>
            <div class="new-message-row">
                <label for="New_DashboardMessage_CreatedDate">Created By:</label>
                <span id="New_DashboardMessage_CreatedDate"><%= DateTime.Now.ToShortDateString() %></span>
            </div>
        </div>
        <div class="new-message-row" id="new-dashboard-message-body-div">
            <div>
                <textarea id="NewDashboardMessageBody" name="Text"></textarea>
            </div>
        </div>
    </div>
<%  } %>
</div>
<div id="newDashboardMessagePreview" class="dashboard-message-preview-modal hidden">
    <ul id="widget-column-1" class="widgets">
        <li class="widget" id="intro-widget">
            <div class="widget-head"><h5>Hello,&nbsp;<%= Current.DisplayName %>!</h5></div>
            <div id="custom-message-widget" class="widget-content"></div>
        </li>
    </ul>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="U.CloseDialog();">Close</a></li>
        </ul>
    </div>
</div>