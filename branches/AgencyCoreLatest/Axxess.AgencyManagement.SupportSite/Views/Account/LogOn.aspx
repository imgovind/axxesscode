﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Logon>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Support Login - Axxess Home Health Management System</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group.DefaultPath("~/css")
        .Add("account.css")
        .Combined(true)
        .Compress(true).CacheDurationInDays(3))
    %>
    <link href="/images/icons/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="login-wrapper">
        <% var browser = HttpContext.Current.Request.Browser; %>
        <% if (browser.IsBrowserAllowed()) { %>
        <div id="login-window" class="hidden">
            <div class="box-header"><img src="/Images/icons/axxess_icon.png" alt="Axxess Logo" /><span class="title">Axxess&trade; Support Login</span></div>
            <div class="box">
            <div id="messages"></div>
            <% using (Html.BeginForm("LogOn", "Account", FormMethod.Post, new { @id = "loginForm", @class = "login", @returnUrl = Request.QueryString["returnUrl"] })) %>
            <% { %>
            <img src="/images/gui/axxess_logo.jpeg" alt="Axxess Logo" id="logo" />
            <div class="row">
                <%= Html.LabelFor(m => m.UserName) %>
                <%= Html.TextBoxFor(m => m.UserName, new { @id="Login_UserName", @class = "required" })%>
            </div>
            <div class="row">
                <%= Html.LabelFor(m => m.Password) %>
                <%= Html.PasswordFor(m => m.Password, new { @id="Login_Password", @class = "required" })%>
            </div>
            <div id="Login_RememberMe" class="row tl">
                <input type="checkbox" id="Login_RememberMe" checked="checked" class="checkbox" name="RememberMe" value="true" />
                <label class="checkbox" for="Login_RememberMe">Remember me</label>
            </div>
            <input id="Login_Button" type="button" value="Login" class="button" onclick="$(this).closest('form').submit();" />
            <div class="row">&nbsp;</div>
            <% } %>
        </div>
        </div>
        <% } else { %>
        <div id="browser-window" class="hidden">
            <div class="box-header"><img src="/Images/icons/axxess_icon.png" alt="Axxess Logo" /><span class="title">Browser Compatibility Check</span></div>
            <div class="box">
                <div class="notification info"><span>Your browser version does not meet our minimum browser requirements. <br /><br />Our software supports Internet Explorer 8 and FireFox 3+.<br /><br />Please download Internet Explorer 8 or Firefox by clicking on the links below.</span></div>
                <div>
                    <div class="fl">
                        <a style="font-size: 17px;" href="http://www.microsoft.com/windows/internet-explorer/worldwide-sites.aspx" title="Internet Explorer 8 Download">Download Internet Explorer 8</a><br />
                        <a style="font-size: 17px;" href="http://www.mozilla.com/en-US/firefox/" title="Firefox Download">Download Firefox</a><br /><br /><br /><br /><br />
                    </div>
                    <div class="fr"><img src="/images/gui/axxess_logo.jpeg" alt="Axxess Logo" id="logo" /></div>
                </div>
                <div class="cl"></div>
            </div>
        </div>
        <% } %>
        <noscript>
            <div id="javascript-window">
            <div class="box-header"><img src="/Images/icons/axxess_icon.png" alt="Axxess Logo" /><span class="title">JavaScript Check</span></div>
            <div class="box">
                <div class="notification info"><span>Your browser version does not meet our minimum browser requirements. <br /><br /><strong>Our software requires JavaScript to be enabled in your browser.</strong><br /><br />Please contact us at support@axxessweb.com for assistance on how to enable JavaScript.</span></div>
            </div>
        </div>
        </noscript> 
    </div>
    <% Html.Telerik().ScriptRegistrar().jQuery(true)
         .DefaultGroup(group => group
             .Add("/Plugins/validate.min.js")
             .Add("/Plugins/form.min.js")
             .Add("/Plugins/blockui.min.js")
             .Add("/Modules/Utility.js")
             .Add("/Modules/Account.js")
             .Compress(true).Combined(true).CacheDurationInDays(3))
        .OnDocumentReady(() =>
        { 
    %>
    Logon.Init();
    <% }).Render(); %>
    
    <% if (Model.IsLocked) { %>
    <script type="text/javascript">
        $("#Login_UserName").attr("disabled", "disabled");
        $("#Login_Password").attr("disabled", "disabled");
        $("#Login_RememberMe").hide();
        $("#Login_Button").hide();
        $("#messages").empty().removeClass().addClass("notification error").append('<span>You have made too many failed login attempts. Please contact your Agency/Companys Administrator.</span>');
    </script>
    <% } %>
</body>
</html>

