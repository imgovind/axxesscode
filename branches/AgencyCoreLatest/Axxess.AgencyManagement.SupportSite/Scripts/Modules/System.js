﻿var SystemMessage = {
    _ckeloaded: false,
    InitNew: function() {
        U.InitTemplate($("#new-system-message-form"), function() { UserInterface.CloseWindow("newsystemmessage"); }, "Message has been sent successfully.", function() {
            $("textarea[name=Body]").val(CKEDITOR.instances['NewSystemMessageBody'].getData());
        });
    
        if (!this._ckeloaded) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() { SystemMessage._ckeloaded = true; SystemMessage.InitCKEditor() });
        else SystemMessage.InitCKEditor();
    },
    InitCKEditor: function() {
        CKEDITOR.replace('NewSystemMessageBody', {
            skin: 'office2003',
            resize_enabled: false,
            height: '100%',
            removePlugins: 'elementspath',
            toolbarCanCollapse: false,
            toolbar: [['Source', 'Preview', '-', 'Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                'JustifyBlock', '-', 'Link', 'Unlink', '-', 'Font', 'FontSize', 'TextColor', '-', 'SpellChecker', 'Scayt']]
        });
    }
}

var DashboardMessage = {
    _ckeloaded: false,
    InitNew: function() {
        U.InitTemplate($('#new-dashboard-message-form'), function() { UserInterface.CloseWindow("newdashboardmessage"); }, "Dashboard Content has been sent successfully.", function() {
            $("textarea[name=Text]").val(CKEDITOR.instances['NewDashboardMessageBody'].getData());
        });

        if (!this._ckeloaded) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() { DashboardMessage._ckeloaded = true; DashboardMessage.InitCKEditor() });
        else DashboardMessage.InitCKEditor();
    },
    InitCKEditor: function() {
        CKEDITOR.replace('NewDashboardMessageBody', {
            skin: 'office2003',
            resize_enabled: false,
            height: '100%',
            removePlugins: 'elementspath',
            toolbarCanCollapse: false,
            toolbar: [['Source', 'Preview', '-', 'Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                'JustifyBlock', '-', 'Link', 'Unlink', '-', 'Font', 'FontSize', 'TextColor', '-', 'SpellChecker', 'Scayt']]
        });
    },
    RebindList: function() { U.RebindTGrid($('#List_Dashboard_Messages')); }
}