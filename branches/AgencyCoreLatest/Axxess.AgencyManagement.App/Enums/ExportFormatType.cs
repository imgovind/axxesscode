﻿namespace Axxess.AgencyManagement.App.Enums
{
    using System.ComponentModel;

    public enum ExportFormatType : int
    {
        [Description("application/vnd.ms-excel")]
        XLS = 1,
        [Description("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")]
        XLSX = 2,
        [Description("text/csv")]
        CSV = 3,
        [Description("application/zip")]
        ZIP = 4,
        [Description("application/octet-stream")]
        Test = 5
        
    }
}
