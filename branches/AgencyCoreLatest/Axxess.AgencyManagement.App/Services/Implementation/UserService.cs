﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Web.Mvc;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;
    using Axxess.AgencyManagement.App.ViewData;

    using Axxess.Log.Enums;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    public class UserService : IUserService
    {
        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly IAssetService assetService;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IAccountingRepository accountingRepository;

        public UserService(IAgencyManagementDataProvider agencyManagmentDataProvider, IMembershipDataProvider membershipDataProvider, IPatientService patientService, ILookUpDataProvider lookupDataProvider, IAssetService assetService, IAccountingManagementDataProvider accountingManagementDataProvider)
        {
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");
            Check.Argument.IsNotNull(accountingManagementDataProvider, "accountingManagementDataProvider");

            this.patientService = patientService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.assetService = assetService;
            this.agencyRepository = agencyManagmentDataProvider.AgencyRepository;
            this.patientRepository = agencyManagmentDataProvider.PatientRepository;
            this.accountingRepository = accountingManagementDataProvider.AccountingRepository;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
        }

        public bool IsEmailAddressInUse(string emailAddress)
        {
            var result = false;
            var login = loginRepository.Find(emailAddress);
            if (login != null && login.IsActive)
            {
                var users = userRepository.GetUsersByLoginId(login.Id, Current.AgencyId);
                if (users.Count > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public bool IsPasswordCorrect(Guid userId, string password)
        {
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(password, login.PasswordHash, login.PasswordSalt))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsSignatureCorrect(string signature)
        {
            if (signature.IsNotNullOrEmpty())
            {
                var login = loginRepository.Find(Current.LoginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(signature, login.SignatureHash, login.SignatureSalt))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool CreateUser(User user)
        {
            try
            {
                var isNewLogin = false;
                var login = loginRepository.Find(user.EmailAddress);
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = user.FirstName;
                    login.EmailAddress = user.EmailAddress;
                    login.Role = Roles.ApplicationUser.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login))
                    {
                        isNewLogin = true;
                    }
                }
                user.LoginId = login.Id;
                user.Profile.EmailWork = user.EmailAddress;
                if (user.HomePhoneArray != null && user.HomePhoneArray.Count > 0)
                {
                    user.Profile.PhoneHome = user.HomePhoneArray.ToArray().PhoneEncode();
                }

                if (user.MobilePhoneArray != null && user.MobilePhoneArray.Count > 0)
                {
                    user.Profile.PhoneMobile = user.MobilePhoneArray.ToArray().PhoneEncode();
                }

                if (user.FaxPhoneArray != null && user.FaxPhoneArray.Count > 0)
                {
                    user.Profile.PhoneFax = user.FaxPhoneArray.ToArray().PhoneEncode();
                }

                if (userRepository.Add(user))
                {
                    var userSnapShot = new UserSnapshot
                    {
                        Id = user.Id,
                        LoginId = user.LoginId,
                        AgencyId = user.AgencyId,
                        TitleType = user.DisplayTitle,
                        Status = user.Status,
                        Created = user.Created,
                        IsDeprecated = user.IsDeprecated
                    };

                    if (loginRepository.AddUserSnapshot(userSnapShot))
                    {
                        user.LocationList.ForEach(location =>
                        {

                            var plan = accountingRepository.GetSubscriptionPlan(Current.AgencyId, location);
                            if (plan != null && plan.IsUserPlan)
                            {
                                var userCount = agencyRepository.GetUserCountPerLocation(plan.AgencyId, plan.AgencyLocationId);
                                if (userCount > plan.PlanLimit)
                                {
                                    var agency = agencyRepository.Get(Current.AgencyId);
                                    if (agency != null)
                                    {
                                        var planLimit = plan.PlanLimit;
                                        var nextPlanLimit = plan.PlanLimit.NextUserSubscriptionPlan();
                                        plan.PlanLimit = nextPlanLimit;
                                        Double amount = 0.00, previousAmount = 0.00;
                                        amount = accountingRepository.GetAmount(plan.IsUserPlan, nextPlanLimit);

                                        if (agency.AccountId.IsNotNullOrEmpty())
                                        {
                                            var payment = accountingRepository.GetAccountingPayment(agency.AccountId);
                                            if (payment != null)
                                            {
                                                previousAmount = payment.Amount;
                                            }
                                        }

                                        var agencyUpgrade = new AgencyUpgrade
                                        {
                                            Amount = amount,
                                            IsUserPlan = true,
                                            Id = Guid.NewGuid(),
                                            PreviousAmount = previousAmount,
                                            Created = DateTime.Now,
                                            AgencyId = Current.AgencyId,
                                            EffectiveDate = DateTime.Now,
                                            RequestedById = Current.UserId,
                                            Comments = "Automatic Upgrade",
                                            PreviousPackageId = planLimit,
                                            RequestedPackageId = nextPlanLimit,
                                            AgencyLocationId = plan.AgencyLocationId,
                                            AccountId = agency.AccountId.IsNotNullOrEmpty() ? agency.AccountId : string.Empty
                                        };

                                        if (accountingRepository.AddUpgrade(agencyUpgrade) && accountingRepository.UpdateSubscriptionPlan(plan))
                                        {
                                            ThreadPool.QueueUserWorkItem(state => SendOverageNotifications(agency.Name, agency.ContactPersonFirstName, agency.ContactPersonEmail, userCount, planLimit.UserSubscriptionPlanName(), nextPlanLimit.UserSubscriptionPlanName()));
                                        }
                                    }
                                }
                            }
                        });
                        if (isNewLogin)
                        {
                            ThreadPool.QueueUserWorkItem(state => SendNewUserNotifications(user));
                        }
                        else
                        {
                            ThreadPool.QueueUserWorkItem(state => SendExistingUserNotification(user));
                        }
                        UserEngine.Refresh(Current.AgencyId);
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserAdded, string.Empty);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
            return false;
        }

        private static void SendOverageNotifications(string agencyName, string adminName, string adminEmailAddress, int userCount, string previousPackage, string newPackage)
        {
            string accountBodyText = string.Empty;
            string accountSubject = string.Format("{0} - Automatic Subscription Plan Change", agencyName);

            string adminBodyText = string.Empty;
            string adminSubject = string.Format("{0} - Automatic Subscription Plan Change", agencyName);

            accountBodyText = MessageBuilder.PrepareTextFrom("AccountUserOverageNotification", "agencyname", agencyName, "usercount", userCount.ToString(), "previouspackage", previousPackage, "newpackage", newPackage);
            adminBodyText = MessageBuilder.PrepareTextFrom("AdminUserOverageNotification", "displayname", adminName, "previouspackage", previousPackage, "newpackage", newPackage);

            Notify.User(CoreSettings.NoReplyEmail, AppSettings.AccountEmailAddress, accountSubject, accountBodyText);
            Notify.User(CoreSettings.NoReplyEmail, adminEmailAddress, adminSubject, adminBodyText);
        }

        private static void SendNewUserNotifications(User user)
        {
            string invitationBodyText = string.Empty;
            string invitationSubject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);

            string welcomeBodyText = string.Empty;
            string welcomeSubject = string.Format("{0} - Welcome to AgencyCore, rated the Most Recommended Home Health Software Solution", user.AgencyName);

            var parameters = string.Format("id={0}&agencyid={1}", user.Id, user.AgencyId);
            var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
            invitationBodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName, "encryptedQueryString", encryptedParameters);
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, invitationSubject, invitationBodyText);

            Thread.Sleep(TimeSpan.FromMinutes(1));

            welcomeBodyText = MessageBuilder.PrepareTextFrom("NewUserWelcome");
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, welcomeSubject, welcomeBodyText);
        }

        private static void SendExistingUserNotification(User user)
        {
            string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);
            string bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName);
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, subject, bodyText);
        }

        public bool DeleteUser(Guid userId)
        {
            var result = false;

            if (userRepository.Toggle(Current.AgencyId, userId, true))
            {
                if (loginRepository.ToggleUserSnapshot(Current.AgencyId, userId, true))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserDeleted, string.Empty);
                    result = true;
                }
                else
                {
                    userRepository.Toggle(Current.AgencyId, userId, false);
                }
            }
            return result;
        }

        public bool RestoreUser(Guid userId)
        {
            var result = false;
            if (userRepository.Toggle(Current.AgencyId, userId, false))
            {
                if (loginRepository.ToggleUserSnapshot(Current.AgencyId, userId, false))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserActivated, string.Empty);
                    result = true;
                }
                else
                {
                    userRepository.Toggle(Current.AgencyId, userId, false);
                }
            }
            //if (userRepository.Restore(Current.AgencyId, userId))
            //{
            //    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserAdded, string.Empty);
            //    result = true;
            //}
            return result;
        }

        public List<UserVisitWidget> GetScheduleWidget(Guid userId)
        {
            var to = DateTime.Today.AddDays(14);
            var from = DateTime.Now.AddDays(-89);
            var userVisits = new List<UserVisitWidget>();
            var patientEpisodes = patientRepository.GetPatientEpisodeDataSchduleWidget(Current.AgencyId, from, to);
            if (patientEpisodes.IsNotNullOrEmpty())
            {
                patientEpisodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty() && episode.EndDate.IsValidDate() && episode.StartDate.IsValidDate())
                    {
                        var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
                            s.EventId != Guid.Empty && s.UserId == userId && s.IsDeprecated == false && s.IsMissedVisit == false
                           && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date >= from.Date && s.EventDate.ToDateTime().Date <= to.Date
                           && !s.IsCompleted() && s.DisciplineTask != (int)DisciplineTasks.Rap && s.DisciplineTask != (int)DisciplineTasks.Final
                            ).ToList();

                        if (scheduledEvents.Count > 0)
                        {
                            scheduledEvents.ForEach(scheduledEvent =>
                            {
                                if (scheduledEvent != null)
                                {
                                    Common.Url.Set(scheduledEvent, false, false);
                                    userVisits.Add(new UserVisitWidget
                                    {
                                        PatientId = episode.PatientId,
                                        Status = episode.Status,
                                        LocationId = episode.AgencyLocationId,
                                        PatientName = episode.PatientName,
                                        TaskName = scheduledEvent.Url,
                                        EventDate = scheduledEvent.EventDate.ToZeroFilled()
                                    });
                                }
                            });
                        }
                    }
                });
            }

            return userVisits.OrderBy(v => v.EventDate.ToOrderedDate()).Take(5).ToList();
        }

        public List<UserVisit> GetScheduleLean(Guid userId, DateTime from, DateTime to)
        {
            var userVisits = new List<UserVisit>();
            var patientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, from, to);
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                var users = new List<User>();
                patientEpisodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty() && episode.EndDate.IsValidDate() && episode.StartDate.IsValidDate())
                    {
                        var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
                            s.EventId != Guid.Empty && s.UserId == userId && s.IsDeprecated == false && s.IsMissedVisit == false
                           && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date >= from.Date && s.EventDate.ToDateTime().Date <= to.Date
                           && !s.IsCompleted() && s.DisciplineTask != (int)DisciplineTasks.Rap && s.DisciplineTask != (int)DisciplineTasks.Final
                            ).ToList();

                        if (scheduledEvents != null && scheduledEvents.Count > 0)
                        {
                            var userIds = new List<Guid>();
                            var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episode.Id) ?? new List<ReturnComment>();
                            if (returnComments != null && returnComments.Count > 0)
                            {
                                var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty() && !users.Exists(us => us.Id == r.UserId)).Select(r => r.UserId).Distinct().ToList();
                                if (returnUserIds != null && returnUserIds.Count > 0)
                                {
                                    var scheduleUsers = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, returnUserIds) ?? new List<User>();
                                    if (scheduleUsers != null && scheduleUsers.Count > 0)
                                    {
                                        users.AddRange(scheduleUsers);
                                    }
                                }
                            }
                            var eventIds = scheduledEvents.Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
                            var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();
                            var episodeDetail = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();

                            scheduledEvents.ForEach(scheduledEvent =>
                            {
                                var visitNote = string.Empty;
                                var statusComments = string.Empty;
                                if (scheduledEvent != null)
                                {
                                    scheduledEvent.EndDate = episode.EndDate.ToDateTime();
                                    scheduledEvent.StartDate = episode.StartDate.ToDateTime();

                                    if (scheduledEvent.Comments.IsNotNullOrEmpty())
                                    {
                                        visitNote = scheduledEvent.Comments.Clean();
                                    }
                                    var eventReturnReasons = returnComments.Where(r => r.EventId == scheduledEvent.EventId).ToList() ?? new List<ReturnComment>();
                                    statusComments = this.GetReturnComments(scheduledEvent.ReturnReason, eventReturnReasons, users);
                                    //patientService.GetReturnComments(scheduledEvent.EventId, scheduledEvent.EpisodeId, scheduledEvent.PatientId);
                                    Common.Url.Set(scheduledEvent, false, false);
                                    userVisits.Add(new UserVisit
                                    {
                                        Id = scheduledEvent.EventId,
                                        VisitNotes = visitNote,
                                        Url = scheduledEvent.Url,
                                        Status = scheduledEvent.Status,
                                        StatusName = scheduledEvent.StatusName,
                                        PatientName = episode.PatientName,
                                        StatusComment = statusComments,
                                        TaskName = scheduledEvent.DisciplineTaskName,
                                        EpisodeId = scheduledEvent.EpisodeId,
                                        PatientId = scheduledEvent.PatientId,
                                        EpisodeNotes = episodeDetail.Comments.Clean(),
                                        VisitDate = scheduledEvent.EventDate.ToZeroFilled(),
                                        ScheduleDate = scheduledEvent.EventDate.ToZeroFilled(),
                                        IsMissedVisit = scheduledEvent.IsMissedVisit
                                    });
                                }
                            });
                        }
                    }
                });
            }

            return userVisits.OrderBy(v => v.VisitDate.ToOrderedDate()).ToList();
        }

        public User Get(Guid id, Guid agencyId, bool isAssetNeeded)
        {
            var user = userRepository.Get(id, agencyId, isAssetNeeded);
            if (user != null)
            {
                if (user.Rates.IsNotNullOrEmpty())
                {
                    foreach (UserRate u in user.RatesArray)
                    {
                        if (u.Insurance.IsNotNullOrEmpty())
                        {
                            if (u.Insurance.ToInteger() < 1000)
                            {
                                var insurance = lookupRepository.GetInsurance(u.Insurance.ToInteger());
                                if (insurance != null)
                                {
                                    u.InsuranceName = insurance.Name;
                                }
                            }
                            else
                            {
                                var insurance = agencyRepository.GetInsurance(u.Insurance.ToInteger(), agencyId);
                                if (insurance != null)
                                {
                                    u.InsuranceName = insurance.Name;
                                }
                            }
                        }
                    }
                }
                if (user.NonVisitRatesArray != null && user.NonVisitRatesArray.Count > 0)
                {
                    foreach (UserNonVisitTaskRate item in user.NonVisitRatesArray)
                    {
                        if (item != null)
                        {
                            if (item.Id != null)
                            {
                                var agencyNonVisit = agencyRepository.GetNonVisitTask(Current.AgencyId, item.Id);
                                if (agencyNonVisit != null)
                                {
                                    item.TaskTitle = agencyNonVisit.Title;
                                }
                            }
                        }
                    }
                }
                else
                {
                    user.NonVisitRatesArray = new List<UserNonVisitTaskRate>();
                }
                user.LocationList = userRepository.GetUserBranches(agencyId, id);
            }
            return user;
        }

        public List<UserNonVisitTaskRate> GetUserNonVisitTaskRates(Guid AgencyId, Guid TaskId, Guid UserId)
        {
            var nonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, UserId);
            List<UserNonVisitTaskRate> result = null;
            if (nonVisitRate != null)
            {
                result = nonVisitRate.Rate.ToObject<List<UserNonVisitTaskRate>>();
            }
            if (result == null)
            {
                result = new List<UserNonVisitTaskRate>();
            }
            else
            {
                foreach (UserNonVisitTaskRate item in result)
                {
                    if (item != null)
                    {
                        if (item.Id != null)
                        {
                            var agencyNonVisit = agencyRepository.GetNonVisitTask(Current.AgencyId, item.Id);
                            if (agencyNonVisit != null)
                            {
                                item.TaskTitle = agencyNonVisit.Title;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public UserNonVisitTaskRate GetUserNonVisitTaskRate(Guid AgencyId, Guid TaskId, Guid UserId)
        {
            var nonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, UserId);
            List<UserNonVisitTaskRate> userNonVisitTaskRateList = null;
            UserNonVisitTaskRate result = null;
            if (nonVisitRate != null)
            {
                userNonVisitTaskRateList = nonVisitRate.Rate.ToObject<List<UserNonVisitTaskRate>>();
            }
            if (userNonVisitTaskRateList == null)
            {
                userNonVisitTaskRateList = new List<UserNonVisitTaskRate>();
            }
            else
            {
                var tempResult = userNonVisitTaskRateList.Find(c => c.Id == TaskId);
                if (tempResult != null)
                {
                    var agencyNonVisit = agencyRepository.GetNonVisitTask(Current.AgencyId, tempResult.Id);
                    if (agencyNonVisit != null)
                    {
                        tempResult.TaskTitle = agencyNonVisit.Title;
                    }
                    result = tempResult;
                }
            }
            return result;
        }

        public User GetUserOnly(Guid id, Guid agencyId)
        {
            var user = userRepository.GetUserOnly(id, agencyId);
            if (user != null)
            {
                return user;
            }
            else
            {
                user = new User();
                return user;
            }
        }

        private string GetReturnComments(string scheduleCommentString, List<ReturnComment> newComments, List<User> users)
        {
            //string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
            //List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
            foreach (ReturnComment comment in newComments)
            {
                if (comment.IsDeprecated) continue;
                if (scheduleCommentString.IsNotNullOrEmpty())
                {
                    scheduleCommentString += "<hr/>";
                }
                if (comment.UserId == Current.UserId)
                {
                    scheduleCommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                }
                var userName = string.Empty;
                if (!comment.UserId.IsEmpty())
                {
                    var user = users.FirstOrDefault(u => u.Id == comment.UserId);
                    if (user != null)
                    {
                        userName = user.DisplayName;
                    }
                }
                scheduleCommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", userName, comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return scheduleCommentString;
        }

        public List<UserVisit> GetScheduleLeanAll(Guid userId, DateTime from, DateTime to)
        {
            var userEvents = new List<UserEvent>();
            var userVisits = new List<UserVisit>();
            var userSchedules = userRepository.GetScheduleLean(Current.AgencyId, userId);
            userSchedules.ForEach(s =>
            {
                if (s.Visits.IsNotNullOrEmpty())
                {
                    var visits = s.Visits.ToObject<List<UserEvent>>();
                    visits.ForEach(v =>
                    {
                        if (v.EventDate.IsValidDate())
                        {
                            var date = v.EventDate.ToDateTime();
                            if (date >= from && date <= to
                               && v.IsDeprecated == false
                               && v.IsMissedVisit == false
                               && v.DisciplineTask != (int)DisciplineTasks.Rap
                               && v.DisciplineTask != (int)DisciplineTasks.Final)
                            {
                                v.PatientName = s.PatientName;
                                v.EpisodeDetails = s.EpisodeDetails;
                                v.EpisodeSchedule = s.EpisodeSchedule;
                                v.EpisodeEndDate = s.EpisodeEndDate;
                                v.EpisodeStartDate = s.EpisodeStartDate;
                                userEvents.Add(v);
                            }
                        }
                    });
                }
            });

            var orderedList = userEvents.OrderBy(e => e.EventDate.ToOrderedDate()).ToList();
            if (orderedList != null && orderedList.Count > 0)
            {
                var episodeIds = orderedList.Where(r => !r.EpisodeId.IsEmpty()).Select(r => r.EpisodeId).Distinct().ToList();
                var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
                var userIds = new List<Guid>();
                var users = new List<User>();
                if (returnComments != null && returnComments.Count > 0)
                {
                    var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                    if (returnUserIds != null && returnUserIds.Count > 0)
                    {
                        userIds.AddRange(returnUserIds);
                    }
                }

                if (userIds != null && userIds.Count > 0)
                {
                    var scheduleUsers = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
                    if (scheduleUsers != null && scheduleUsers.Count > 0)
                    {
                        users.AddRange(scheduleUsers);
                    }
                }
                foreach (UserEvent e in orderedList)
                {

                    var visitNote = string.Empty;
                    var statusComments = string.Empty;
                    if (!e.EventId.IsEmpty() && e.EpisodeSchedule.IsNotNullOrEmpty())
                    {
                        if (!userVisits.Exists(v => v.Id == e.EventId))
                        {
                            var episodeEvents = e.EpisodeSchedule.ToObject<List<ScheduleEvent>>();
                            if (episodeEvents != null && episodeEvents.Count > 0)
                            {
                                var scheduledEvent = episodeEvents.FirstOrDefault(se => se.EventId == e.EventId);
                                if (scheduledEvent != null)
                                {
                                    scheduledEvent.EndDate = e.EpisodeEndDate;
                                    scheduledEvent.StartDate = e.EpisodeStartDate;
                                    if (!scheduledEvent.IsDeprecated
                                       && scheduledEvent.EventDate.IsValidDate() && (scheduledEvent.EventDate.ToDateTime() >= scheduledEvent.StartDate) && (scheduledEvent.EventDate.ToDateTime() <= scheduledEvent.EndDate))
                                    {
                                        var episodeDetail = e.EpisodeDetails.IsNotNullOrEmpty() ? e.EpisodeDetails.ToObject<EpisodeDetail>() : new EpisodeDetail();
                                        if (scheduledEvent.Comments.IsNotNullOrEmpty())
                                        {
                                            visitNote = scheduledEvent.Comments.Clean();
                                        }
                                        var eventReturnReasons = returnComments.Where(r => r.EpisodeId == scheduledEvent.EpisodeId && r.EventId == scheduledEvent.EventId).ToList() ?? new List<ReturnComment>();
                                        statusComments = this.GetReturnComments(scheduledEvent.ReturnReason, eventReturnReasons, users);
                                        //if (scheduledEvent.StatusComment.IsNotNullOrEmpty())
                                        //{
                                        //    statusComments = scheduledEvent.StatusComment;
                                        //}

                                        Common.Url.Set(scheduledEvent, false, false);
                                        userVisits.Add(new UserVisit
                                        {
                                            Id = scheduledEvent.EventId,
                                            VisitNotes = visitNote,
                                            Url = scheduledEvent.Url,
                                            Status = scheduledEvent.Status,
                                            StatusName = scheduledEvent.StatusName,
                                            PatientName = e.PatientName,
                                            StatusComment = statusComments,
                                            TaskName = scheduledEvent.DisciplineTaskName,
                                            EpisodeId = scheduledEvent.EpisodeId,
                                            PatientId = scheduledEvent.PatientId,
                                            EpisodeNotes = episodeDetail.Comments.Clean(),
                                            VisitDate = scheduledEvent.EventDate.ToZeroFilled(),
                                            ScheduleDate = scheduledEvent.EventDate.ToZeroFilled(),
                                            IsMissedVisit = scheduledEvent.IsMissedVisit,
                                            IsComplete = scheduledEvent.IsComplete
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return userVisits;
        }

        //public List<UserVisit> GetCompletedVisits(Guid userId, DateTime start, DateTime end)
        //{
        //    var userEvents = new List<UserEvent>();
        //    var userVisits = new List<UserVisit>();
        //    var userSchedules = userRepository.GetScheduleLean(Current.AgencyId, userId, start, end);
        //    userSchedules.ForEach(s =>
        //    {
        //        if (s.Visits.IsNotNullOrEmpty())
        //        {
        //            var visits = s.Visits.ToObject<List<UserEvent>>();
        //            visits.ForEach(v =>
        //            {
        //                var date = v.EventDate.IsNotNullOrEmpty() ? v.EventDate.ToDateTime() : DateTime.MinValue;
        //                if (date != DateTime.MinValue)
        //                {
        //                    if (date > start && date < end
        //                       && v.IsDeprecated == false
        //                       && v.IsMissedVisit == false
        //                       && v.DisciplineTask != (int)DisciplineTasks.Rap
        //                       && v.DisciplineTask != (int)DisciplineTasks.Final)
        //                    {
        //                        v.PatientName = s.PatientName;
        //                        v.EpisodeDetails = s.EpisodeDetails;
        //                        v.EpisodeSchedule = s.EpisodeSchedule;
        //                        v.EpisodeEndDate = s.EpisodeEndDate;
        //                        v.EpisodeStartDate = s.EpisodeStartDate;
        //                        userEvents.Add(v);
        //                    }
        //                }
        //            });
        //        }
        //    });

        //    var orderedList = userEvents.OrderBy(e => e.EventDate.ToOrderedDate());
        //    foreach (UserEvent e in orderedList)
        //    {
        //        var visitNote = string.Empty;
        //        var statusComments = string.Empty;
        //        if (!e.EventId.IsEmpty() && e.EpisodeSchedule.IsNotNullOrEmpty())
        //        {
        //            if (!userVisits.Exists(v => v.Id == e.EventId))
        //            {
        //                var episodeEvents = e.EpisodeSchedule.ToObject<List<ScheduleEvent>>();
        //                if (episodeEvents != null && episodeEvents.Count > 0)
        //                {
        //                    var scheduledEvent = episodeEvents.FirstOrDefault(se => se.EventId == e.EventId);
        //                    if (scheduledEvent != null && scheduledEvent.IsCompleted() && !scheduledEvent.IsDeprecated && !scheduledEvent.IsMissedVisit)
        //                    {
        //                        scheduledEvent.EndDate = e.EpisodeEndDate;
        //                        scheduledEvent.StartDate = e.EpisodeStartDate;
        //                        if (DateTime.Parse(scheduledEvent.EventDate) >= scheduledEvent.StartDate && DateTime.Parse(scheduledEvent.EventDate) <= scheduledEvent.EndDate)
        //                        {
        //                            if (scheduledEvent.Comments.IsNotNullOrEmpty())
        //                            {
        //                                visitNote = scheduledEvent.Comments.Clean();
        //                            }
        //                            if (scheduledEvent.StatusComment.IsNotNullOrEmpty())
        //                            {
        //                                statusComments = scheduledEvent.StatusComment;
        //                            }

        //                            Common.Url.Set(scheduledEvent, false, false);
        //                            userVisits.Add(new UserVisit
        //                            {
        //                                Id = scheduledEvent.EventId,
        //                                UserId = userId,
        //                                VisitRate = "$0.00",
        //                                Url = scheduledEvent.Url,
        //                                UserDisplayName = UserEngine.GetName(userId, Current.AgencyId),
        //                                Status = scheduledEvent.Status,
        //                                Surcharge = scheduledEvent.Surcharge,
        //                                StatusName = scheduledEvent.StatusName,
        //                                PatientName = e.PatientName,
        //                                StatusComment = statusComments,
        //                                TaskName = scheduledEvent.DisciplineTaskName,
        //                                EpisodeId = scheduledEvent.EpisodeId,
        //                                PatientId = scheduledEvent.PatientId,
        //                                VisitDate = scheduledEvent.EventDate.ToZeroFilled(),
        //                                IsMissedVisit = scheduledEvent.IsMissedVisit

        //                            });
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return userVisits;
        //}

        public bool UpdateProfile(User user)
        {
            var result = false;

            if (userRepository.UpdateProfile(user))
            {
                result = true;
                if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty() || user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                {
                    User userInfo = userRepository.GetUserOnly(user.Id, Current.AgencyId);
                    Login login = loginRepository.Find(userInfo.LoginId);
                    if (userInfo != null && login != null)
                    {
                        if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty())
                        {
                            string passwordsalt = string.Empty;
                            string passwordHash = string.Empty;

                            var saltedHash = new SaltedHash();
                            saltedHash.GetHashAndSalt(user.PasswordChanger.NewPassword, out passwordHash, out passwordsalt);
                            login.PasswordSalt = passwordsalt;
                            login.PasswordHash = passwordHash;
                        }

                        if (user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                        {
                            string signaturesalt = string.Empty;
                            string signatureHash = string.Empty;

                            var saltedHash = new SaltedHash();
                            saltedHash.GetHashAndSalt(user.SignatureChanger.NewSignature, out signatureHash, out signaturesalt);
                            login.SignatureSalt = signaturesalt;
                            login.SignatureHash = signatureHash;
                        }

                        if (!loginRepository.Update(login))
                        {
                            result = false;
                        }
                        else
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserProfileUpdated, string.Empty);
                        }
                    }
                }

            }

            return result;
        }

        public bool AddLicense(License license, System.Web.HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var isAssetSaved = true;
            var user = userRepository.GetUserOnly(license.UserId, Current.AgencyId);
            if (user != null)
            {
                if (user.Licenses.IsNotNullOrEmpty())
                {
                    user.LicensesArray = user.Licenses.ToObject<List<License>>() ?? new List<License>();
                }
                else
                {
                    user.LicensesArray = new List<License>();
                }

                if (httpFiles.Count > 0)
                {
                    foreach (string key in httpFiles.AllKeys)
                    {
                        HttpPostedFileBase file = httpFiles.Get(key);
                        if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                        {
                            using (var binaryReader = new BinaryReader(file.InputStream))
                            {
                                var asset = new Asset
                                {
                                    FileName = file.FileName,
                                    AgencyId = Current.AgencyId,
                                    ContentType = file.ContentType,
                                    FileSize = file.ContentLength.ToString(),
                                    Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                                };
                                if (assetService.AddAsset(asset))
                                {
                                    license.AssetId = asset.Id;
                                }
                                else
                                {
                                    isAssetSaved = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (isAssetSaved)
                {
                    license.Id = Guid.NewGuid();
                    license.Created = DateTime.Now;
                    if (license.OtherLicenseType.IsNotNullOrEmpty())
                    {
                        license.LicenseType = license.OtherLicenseType;
                    }
                    if (user.LicensesArray != null)
                    {
                        user.LicensesArray.Add(license);
                        user.Licenses = user.LicensesArray.ToXml();
                    }
                    else
                    {
                        user.LicensesArray = new List<License>();
                        user.LicensesArray.Add(license);
                        user.Licenses = user.LicensesArray.ToXml();
                    }
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateLicense(License license)
        {
            var user = userRepository.GetUserOnly(license.UserId, Current.AgencyId);
            if (user != null)
            {
                if (user.Licenses.IsNotNullOrEmpty())
                {
                    user.LicensesArray = user.Licenses.ToObject<List<License>>() ?? new List<License>();
                }
                else
                {
                    user.LicensesArray = new List<License>();
                }

                var existingLicense = user.LicensesArray.Find(l => l.Id == license.Id);
                if (license != null)
                {
                    if (license.OtherLicenseType.IsNullOrEmpty())
                    {
                        existingLicense.LicenseType = license.LicenseType;
                    }
                    else
                    {
                        existingLicense.LicenseType = license.OtherLicenseType;
                    }
                    existingLicense.LicenseNumber = license.LicenseNumber;
                    existingLicense.InitiationDate = license.InitiationDate;
                    existingLicense.ExpirationDate = license.ExpirationDate;
                    user.Licenses = user.LicensesArray.ToXml();
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool UpdateLicense(Guid id, Guid userId, DateTime expirationDate, string LicenseNumber)
        {
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                user.LicensesArray = user.Licenses.IsNotNullOrEmpty() ? user.Licenses.ToObject<List<License>>() ?? new List<License>() : new List<License>();
                var license = user.LicensesArray.Find(l => l.Id == id);
                if (license != null)
                {
                    license.ExpirationDate = expirationDate;
                    license.LicenseNumber = LicenseNumber;
                    user.Licenses = user.LicensesArray.ToXml();
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool UpdateLicense(Guid id, Guid userId, DateTime initiationDate, DateTime expirationDate)
        {
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                user.LicensesArray = user.Licenses.IsNotNullOrEmpty() ? user.Licenses.ToObject<List<License>>() ?? new List<License>() : new List<License>();
                var license = user.LicensesArray.Find(l => l.Id == id);
                if (license != null)
                {
                    license.InitiationDate = initiationDate;
                    license.ExpirationDate = expirationDate;
                    user.Licenses = user.LicensesArray.ToXml();
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool DeleteLicense(Guid id, Guid userId)
        {
            if (userId.IsNotEmpty())
            {
                var user = userRepository.GetUserOnly(userId, Current.AgencyId);
                if (user != null)
                {
                    user.LicensesArray = user.Licenses.IsNotNullOrEmpty() ? user.Licenses.ToObject<List<License>>() ?? new List<License>() : new List<License>();
                    var license = user.LicensesArray.Find(l => l.Id == id);
                    if (license != null)
                    {
                        if (!license.AssetId.IsEmpty())
                        {
                            assetService.DeleteAsset(license.AssetId);
                        }
                        user.LicensesArray.RemoveAll(l => l.Id == id);
                        user.Licenses = user.LicensesArray.ToXml();
                        if (userRepository.UpdateModel(user))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseDeleted, string.Empty);
                            return true;
                        }
                    }
                }
                else
                {
                    return userRepository.DeleteNonUserLicense(id, Current.AgencyId);
                }
            }
            else
            {
                return userRepository.DeleteNonUserLicense(id, Current.AgencyId);
            }
            return false;
        }

        public bool UpdatePermissions(FormCollection formCollection)
        {
            var result = false;
            var userId = formCollection["UserId"] != null ? formCollection["UserId"].ToGuid() : Guid.Empty;
            var permissionArray = formCollection["PermissionsArray"] != null ? formCollection["PermissionsArray"].ToArray().ToList() : null;
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null && permissionArray != null && permissionArray.Count > 0)
            {
                user.PermissionsArray = permissionArray;
                if (user.PermissionsArray != null && user.PermissionsArray.Count > 0)
                {
                    user.Permissions = user.PermissionsArray.ToXml();
                }
                if (userRepository.UpdateModel(user))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserPermissionsUpdated, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public List<User> GetUserByBranchAndStatus(Guid branchId, int status)
        {
            var users = new List<User>();
            if (status == 0)
            {
                if (branchId.IsEmpty())
                {
                    users = userRepository.GetUsersOnly(Current.AgencyId).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId).ToList();
                }
            }
            else
            {
                if (branchId.IsEmpty())
                {
                    users = userRepository.GetUsersByStatus(Current.AgencyId, status).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId, status).ToList();
                }

            }
            return users.OrderBy(u => u.FirstName).ToList();
        }

        public IList<LicenseItem> GetUserLicenses()
        {
            var list = new List<LicenseItem>();

            var nonuserLicenses = userRepository.GetNonUserLicenses(Current.AgencyId);
            if (nonuserLicenses != null && nonuserLicenses.Count > 0)
            {
                nonuserLicenses.ForEach(license =>
                {
                    list.Add(license);
                });
            }

            var userLicenses = userRepository.GetSoftwareUserLicenses(Current.AgencyId);
            if (userLicenses != null && userLicenses.Count > 0)
            {
                userLicenses.ForEach(license =>
                {
                    license.DisplayName = UserEngine.GetName(license.UserId, license.AgencyId);
                    list.Add(license);
                });
            }
            return list.OrderBy(l => l.FirstName).ToList();
        }

        public string GetUserLicensesExpirationNotification(Guid userId, string userName)
        {
            var licensesLists = userRepository.GetUserLicenses(Current.AgencyId, userId, false);
            string result = "";
            foreach (License l in licensesLists)
            {
                if (l.ExpirationDate < DateTime.Now)
                {
                    result += l.LicenseType + " of " + userName + " has expired.</br>";
                }
                else if (l.ExpirationDate >= DateTime.Now && l.ExpirationDate <= DateTime.Now.AddDays(30))
                {
                    result += l.LicenseType + " of " + userName + " will be expired in 30 days.</br>";
                }
            }
            return result;
        }

        public IList<License> GetUserLicenses(Guid branchId, int status)
        {
            var list = new List<License>();

            var users = userRepository.GetAllUsers(Current.AgencyId, branchId, status);
            if (users != null && users.Count > 0)
            {
                users.ForEach(u =>
                {
                    if (u.Licenses.IsNotNullOrEmpty())
                    {
                        u.LicensesArray = u.Licenses.ToObject<List<License>>();
                        if (u.LicensesArray != null && u.LicensesArray.Count > 0)
                        {
                            u.LicensesArray.ForEach(l =>
                            {
                                l.UserDisplayName = string.Format("{0}, {1}", u.LastName, u.FirstName);
                                if (u.CustomId.IsNotNullOrEmpty())
                                {
                                    Console.WriteLine();
                                }
                                l.CustomId = u.CustomId;
                                list.Add(l);
                            }
                                );
                        }
                    }
                });
            }
            return list;
        }

        public bool AddLicenseItem(LicenseItem licenseItem, System.Web.HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var isAssetSaved = true;
            if (httpFiles.Count > 0)
            {
                foreach (string key in httpFiles.AllKeys)
                {
                    HttpPostedFileBase file = httpFiles.Get(key);
                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        using (var binaryReader = new BinaryReader(file.InputStream))
                        {
                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetService.AddAsset(asset))
                            {
                                licenseItem.AssetId = asset.Id;
                            }
                            else
                            {
                                isAssetSaved = false;
                                break;
                            }
                        }
                    }
                }
            }
            if (isAssetSaved)
            {
                licenseItem.Id = Guid.NewGuid();
                licenseItem.Created = DateTime.Now;
                licenseItem.Modified = DateTime.Now;
                licenseItem.IsDeprecated = false;
                licenseItem.AgencyId = Current.AgencyId;

                if (licenseItem.OtherLicenseType.IsNotNullOrEmpty())
                {
                    licenseItem.LicenseType = licenseItem.OtherLicenseType;
                }
                if (userRepository.AddNonUserLicense(licenseItem))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, licenseItem.Id.ToString(), LogType.NonUserLicense, LogAction.UserLicenseAdded, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public IList<UserRate> GetUserRates(Guid userId)
        {
            var list = new List<UserRate>();
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null && user.Rates.IsNotNullOrEmpty())
            {
                list = user.Rates.ToObject<List<UserRate>>();
                list.ForEach(r => { r.InsuranceName = patientService.GetInsurance(r.Insurance); });
            }
            return list;
        }

        public bool LoadUserRate(Guid fromId, Guid toId)
        {
            #region UserPayRates
            bool result = false;
            var fromUser = userRepository.GetUserOnly(fromId, Current.AgencyId);
            var toUser = userRepository.GetUserOnly(toId, Current.AgencyId);
            toUser.Rates = fromUser.Rates;
            if (userRepository.UpdateModel(toUser))
            {
                result = true;
            }
            #endregion

            #region UserNonVisitPayRates
            bool doesToUserNonVisitRecordExist = false;
            bool doesFromUserNonVisitRecordExist = false;
            var fromUserNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, fromId);
            if (fromUserNonVisitRate.IsNull())
            {
                fromUserNonVisitRate = new UserNonVisitRate();
                fromUserNonVisitRate.Id = Guid.NewGuid();
                fromUserNonVisitRate.AgencyId = Current.AgencyId;
                fromUserNonVisitRate.UserId = fromId;
                var rate = new List<UserNonVisitTaskRate>();
                fromUserNonVisitRate.Rate = rate.ToXml();
                if (userRepository.AddUserNonVisitRate(fromUserNonVisitRate))
                {
                    doesFromUserNonVisitRecordExist = true;
                }
            }
            else
            {
                doesFromUserNonVisitRecordExist = true;
            }

            if (doesFromUserNonVisitRecordExist == true)
            {
                var toUserNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, toId);
                if (toUserNonVisitRate.IsNull())
                {
                    toUserNonVisitRate = new UserNonVisitRate();
                    toUserNonVisitRate.Id = Guid.NewGuid();
                    toUserNonVisitRate.AgencyId = Current.AgencyId;
                    toUserNonVisitRate.UserId = toId;
                    toUserNonVisitRate.Rate = fromUserNonVisitRate.Rate;
                    if (userRepository.AddUserNonVisitRate(toUserNonVisitRate))
                    {
                        doesToUserNonVisitRecordExist = true;
                    }
                }
                else
                {
                    doesToUserNonVisitRecordExist = true;
                }
                if (doesToUserNonVisitRecordExist == true)
                {
                    toUserNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, toId);
                    fromUserNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, fromId);
                    toUserNonVisitRate.Rate = fromUserNonVisitRate.Rate;
                    if (userRepository.UpdateUserNonVisitRate(toUserNonVisitRate))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }
            #endregion

            return result;
        }

        public SubscriptionPlanViewData GetAgencyUserSubcriptionPlanDetails()
        {
            var viewData = new SubscriptionPlanViewData();
            var plans = accountingRepository.GetSubscriptionPlans(Current.AgencyId);
            if (plans != null && plans.Count > 0)
            {
                plans.ForEach(plan =>
                {
                    if (plan.IsUserPlan)
                    {
                        if (!viewData.Plans.ContainsKey(plan.AgencyLocationId))
                        {
                            viewData.Plans.Add(plan.AgencyLocationId, new LocationPlanViewData
                            {
                                Max = plan.PlanLimit,
                                CurrentPlan = plan.PlanLimit,
                                NextPlan = plan.PlanLimit.NextUserSubscriptionPlan(),
                                CurrentPlanDescription = plan.PlanLimit.UserSubscriptionPlanName(),
                                NextPlanDescription = plan.PlanLimit.NextUserSubscriptionPlan().UserSubscriptionPlanName(),
                                Count = agencyRepository.GetUserCountPerLocation(plan.AgencyId, plan.AgencyLocationId)
                            });
                        }
                    }
                });
            }
            return viewData;
        }

        public JsonViewData Activate(Guid userId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be activated. Please try again." };

            if (loginRepository.UpdateUserSnapshotStatus(Current.AgencyId, userId, (int)UserStatus.Active))
            {

                if (userRepository.SetUserStatus(Current.AgencyId, userId, (int)UserStatus.Active))
                {
                    var userLocations = userRepository.GetUserAgencyLocationsName(Current.AgencyId, userId);
                    if (userLocations != null && userLocations.Count > 0)
                    {
                        userLocations.ForEach(location =>
                        {
                            var plan = accountingRepository.GetSubscriptionPlan(Current.AgencyId, location.Id);
                            if (plan != null && plan.IsUserPlan)
                            {
                                var userCount = agencyRepository.GetUserCountPerLocation(plan.AgencyId, plan.AgencyLocationId);
                                if (userCount > plan.PlanLimit)
                                {
                                    var agency = agencyRepository.Get(Current.AgencyId);
                                    if (agency != null)
                                    {
                                        var planLimit = plan.PlanLimit;
                                        var nextPlanLimit = plan.PlanLimit.NextUserSubscriptionPlan();
                                        plan.PlanLimit = nextPlanLimit;
                                        Double amount = 0.00, previousAmount = 0.00;
                                        amount = accountingRepository.GetAmount(plan.IsUserPlan, nextPlanLimit);

                                        if (agency.AccountId.IsNotNullOrEmpty())
                                        {
                                            var payment = accountingRepository.GetAccountingPayment(agency.AccountId);
                                            if (payment != null)
                                            {
                                                previousAmount = payment.Amount;
                                            }
                                        }

                                        var agencyUpgrade = new AgencyUpgrade
                                        {
                                            Amount = amount,
                                            IsUserPlan = true,
                                            Id = Guid.NewGuid(),
                                            PreviousAmount = previousAmount,
                                            Created = DateTime.Now,
                                            AgencyId = Current.AgencyId,
                                            EffectiveDate = DateTime.Now,
                                            RequestedById = Current.UserId,
                                            Comments = "Automatic Upgrade",
                                            PreviousPackageId = planLimit,
                                            RequestedPackageId = nextPlanLimit,
                                            AgencyLocationId = plan.AgencyLocationId,
                                            AccountId = agency.AccountId.IsNotNullOrEmpty() ? agency.AccountId : string.Empty
                                        };

                                        if (accountingRepository.AddUpgrade(agencyUpgrade) && accountingRepository.UpdateSubscriptionPlan(plan))
                                        {
                                            ThreadPool.QueueUserWorkItem(state => SendOverageNotifications(agency.Name, agency.ContactPersonFirstName, agency.ContactPersonEmail, userCount, planLimit.UserSubscriptionPlanName(), nextPlanLimit.UserSubscriptionPlanName()));
                                        }
                                    }
                                }
                            }
                        });
                    }
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserActivated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "User has been activated successfully.";
                }
                else
                {
                    var userWithStatus = userRepository.GetUserStatus(Current.AgencyId, userId);
                    if (userWithStatus != null)
                    {
                        loginRepository.UpdateUserSnapshotStatus(Current.AgencyId, userId, userWithStatus.Status);
                    }
                    else
                    {
                        loginRepository.UpdateUserSnapshotStatus(Current.AgencyId, userId, (int)UserStatus.Inactive);
                    }
                }
            }

            return viewData;
        }

        public JsonViewData Deactivate(Guid userId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deactivated. Try Again." };
            if (loginRepository.UpdateUserSnapshotStatus(Current.AgencyId, userId, (int)UserStatus.Inactive))
            {
                if (userRepository.SetUserStatus(Current.AgencyId, userId, (int)UserStatus.Inactive))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserDeactivated, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "User has been deactivated successfully.";
                }
                else
                {
                    var userWithStatus = userRepository.GetUserStatus(Current.AgencyId, userId);
                    if (userWithStatus != null)
                    {
                        loginRepository.UpdateUserSnapshotStatus(Current.AgencyId, userId, userWithStatus.Status);
                    }
                    else
                    {
                        loginRepository.UpdateUserSnapshotStatus(Current.AgencyId, userId, (int)UserStatus.Active);
                    }
                }
            }

            return viewData;
        }

        public bool BulkActivate(List<Guid> userIds)
        {
            var result = false;

            if (userIds != null && userIds.Count > 0)
            {
                if (userRepository.BulkStatusUpdate(Current.AgencyId, userIds, true))
                {
                    if (loginRepository.BulkUserSnapshotStatus(Current.AgencyId, userIds, true))
                    {
                        result = true;
                    }
                }
                if (result)
                {
                    var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                    if (agency != null)
                    {
                        userIds.ForEach(userId =>
                        {
                            var userLocations = userRepository.GetUserAgencyLocationsName(Current.AgencyId, userId);
                            if (userLocations != null && userLocations.Count > 0)
                            {
                                userLocations.ForEach(location =>
                                {
                                    var plan = accountingRepository.GetSubscriptionPlan(Current.AgencyId, location.Id);
                                    if (plan != null && plan.IsUserPlan)
                                    {
                                        var userCount = agencyRepository.GetUserCountPerLocation(plan.AgencyId, plan.AgencyLocationId);
                                        if (userCount > plan.PlanLimit)
                                        {
                                            var planLimit = plan.PlanLimit;
                                            var nextPlanLimit = plan.PlanLimit.NextUserSubscriptionPlan();
                                            plan.PlanLimit = nextPlanLimit;
                                            Double amount = 0.00, previousAmount = 0.00;
                                            amount = accountingRepository.GetAmount(plan.IsUserPlan, nextPlanLimit);

                                            if (agency.AccountId.IsNotNullOrEmpty())
                                            {
                                                var payment = accountingRepository.GetAccountingPayment(agency.AccountId);
                                                if (payment != null)
                                                {
                                                    previousAmount = payment.Amount;
                                                }
                                            }

                                            var agencyUpgrade = new AgencyUpgrade
                                            {
                                                Amount = amount,
                                                IsUserPlan = true,
                                                Id = Guid.NewGuid(),
                                                PreviousAmount = previousAmount,
                                                Created = DateTime.Now,
                                                AgencyId = Current.AgencyId,
                                                EffectiveDate = DateTime.Now,
                                                RequestedById = Current.UserId,
                                                Comments = "Automatic Upgrade",
                                                PreviousPackageId = planLimit,
                                                RequestedPackageId = nextPlanLimit,
                                                AgencyLocationId = plan.AgencyLocationId,
                                                AccountId = agency.AccountId.IsNotNullOrEmpty() ? agency.AccountId : string.Empty
                                            };

                                            if (accountingRepository.AddUpgrade(agencyUpgrade) && accountingRepository.UpdateSubscriptionPlan(plan))
                                            {
                                                ThreadPool.QueueUserWorkItem(state => SendOverageNotifications(agency.Name, agency.ContactPersonFirstName, agency.ContactPersonEmail, userCount, planLimit.UserSubscriptionPlanName(), nextPlanLimit.UserSubscriptionPlanName()));
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }

            return result;
        }


        public JsonViewData MultiDeactivate(List<Guid> UserId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User(s) cannot be deactivated. Please try again." };
            if (UserId != null && UserId.Count > 0)
            {
                if (userRepository.BulkStatusUpdate(Current.AgencyId, UserId, false))
                {
                    if (loginRepository.BulkUserSnapshotStatus(Current.AgencyId, UserId, false))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "User(s) have been deactivated successfully.";
                    }
                }
            }
            return viewData;
        }


        public bool Update(User user)
        {
            bool result = false;
            if (user != null)
            {
                var editUser = userRepository.GetUserOnly(user.Id, Current.AgencyId);
                if (editUser != null)
                {
                    var isTitleChange=user.TitleType.IsEqual(editUser.TitleType);
                    editUser.CustomId = user.CustomId;
                    editUser.ClinicianProviderId = user.ClinicianProviderId;
                    //editUser.AgencyLocationId = user.AgencyLocationId;
                    editUser.EmploymentType = user.EmploymentType;
                    if (user.AgencyRoleList != null && user.AgencyRoleList.Count > 0) editUser.Roles = user.AgencyRoleList.ToArray().AddColons();
                    editUser.FirstName = user.FirstName;
                    editUser.LastName = user.LastName;
                    editUser.MiddleName = user.MiddleName;
                    editUser.Suffix = user.Suffix;
                    editUser.TitleType = user.TitleType;
                    editUser.TitleTypeOther = user.TitleTypeOther;
                    editUser.Credentials = user.Credentials;
                    editUser.CredentialsOther = user.CredentialsOther;
                    editUser.Profile = editUser.ProfileData.ToObject<UserProfile>();
                    if (user.Profile != null)
                    {
                        editUser.Profile.AddressLine1 = user.Profile.AddressLine1;
                        editUser.Profile.AddressLine2 = user.Profile.AddressLine2;
                        editUser.Profile.AddressCity = user.Profile.AddressCity;
                        editUser.Profile.AddressZipCode = user.Profile.AddressZipCode;
                        editUser.Profile.AddressStateCode = user.Profile.AddressStateCode;
                        editUser.Profile.Gender = user.Profile.Gender;
                    }
                    if (user.HomePhoneArray != null && user.HomePhoneArray.Count > 0) editUser.Profile.PhoneHome = user.HomePhoneArray.ToArray().PhoneEncode();
                    if (user.MobilePhoneArray != null && user.MobilePhoneArray.Count > 0) editUser.Profile.PhoneMobile = user.MobilePhoneArray.ToArray().PhoneEncode();
                    if (user.FaxPhoneArray != null && user.FaxPhoneArray.Count > 0) editUser.Profile.PhoneFax = user.FaxPhoneArray.ToArray().PhoneEncode();
                    editUser.AllowWeekendAccess = user.AllowWeekendAccess;
                    editUser.EarliestLoginTime = user.EarliestLoginTime;
                    editUser.AutomaticLogoutTime = user.AutomaticLogoutTime;
                    editUser.ProfileData = editUser.Profile.ToXml();
                    editUser.Comments = user.Comments;
                    editUser.Modified = DateTime.Now;
                    editUser.DOB = user.DOB;
                    editUser.SSN = user.SSN;
                    editUser.AccountExpireDate = user.AccountExpireDate;
                    if (userRepository.UpdateModel(editUser))
                    {
                        result = true;
                        if (isTitleChange && editUser.DisplayTitle.IsNotNullOrEmpty())
                        {
                            loginRepository.UserSnapshotTitle(editUser.AgencyId, editUser.Id, editUser.DisplayTitle);
                        }
                        UserEngine.Refresh(editUser.AgencyId);

                        if (user.LocationList != null && user.LocationList.Count > 0)
                        {
                            var userLocations = userRepository.GetUserLocations(editUser.AgencyId, editUser.Id);
                            var userLocationIds = userLocations != null ? userLocations.Select(l => l.AgencyLocationId).ToList() : new List<Guid>();
                            var locationList = new List<UserLocation>();
                            user.LocationList.ForEach(locationId =>
                            {
                                if (!userLocationIds.Contains(locationId))
                                {

                                    locationList.Add(new UserLocation
                                    {
                                        UserId = user.Id,
                                        Id = Guid.NewGuid(),
                                        AgencyId = user.AgencyId,
                                        AgencyLocationId = locationId
                                    });
                                }

                            });
                            if (locationList.IsNotNullOrEmpty())
                            {
                                userRepository.AddUserLocations(locationList);
                            }
                            var idsToDelete = new List<Guid>();
                            userLocationIds.ForEach(locationId =>
                            {
                                if (!user.LocationList.Contains(locationId))
                                {
                                    var userLocation = userLocations.Where(l => l.AgencyLocationId == locationId).FirstOrDefault();
                                    if (userLocation != null)
                                    {
                                        idsToDelete.Add(locationId);
                                        //database.Delete<UserLocation>(userLocation.Id);
                                    }
                                }
                            });
                            if (idsToDelete != null && idsToDelete.Count > 0)
                            {
                                userRepository.DeleteUserLocations(editUser.AgencyId, editUser.Id, idsToDelete);
                            }
                        }
                    }
                }
            }
            return result;
        }

    }
}
