﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Enums;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Security;
    using Axxess.Membership.Enums;
    using Axxess.Log.Enums;
    using Axxess.LookUp.Repositories;
    using System.Threading;

    public class PhysicianService : IPhysicianService
    {
        #region Constructor

        private readonly ILoginRepository loginRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly ILookupRepository lookupRepository;

        public PhysicianService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, ILookUpDataProvider lookUpDataProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.lookupRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Public Methods

        public bool CreatePhysician(AgencyPhysician physician)
        {
            physician.Id = Guid.NewGuid();
            var isNewLogin = false;
            if (physician.PhysicianAccess)
            {
                var login = loginRepository.Find(physician.EmailAddress.Trim());
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = physician.DisplayName;
                    login.EmailAddress = physician.EmailAddress.Trim();
                    login.Role = Roles.Physician.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login))
                    {
                        isNewLogin = true;
                    }
                }
                physician.LoginId = login.Id;
            }
            if (physicianRepository.Add(physician))
            {
                if (physician.LoginId.IsEmpty() || AddOrUpdatePhysicianSnapShot(physician, false))
                {
                    if (physician.PhysicianAccess)
                    {
                        if (isNewLogin)
                        {
                            ThreadPool.QueueUserWorkItem(state => SendNewPhysicianNotification(physician, Current.AgencyName));
                        }
                        else
                        {
                            ThreadPool.QueueUserWorkItem(state => SendExistingPhysicianNotification(physician, Current.AgencyName));
                        }
                    }
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianAdded, string.Empty);
                    return true;
                }
                else
                {
                    EntityHelper.RemoveEntity<AgencyPhysician, Guid>(Current.AgencyId, physician.Id, "AgencyManagementConnectionString");
                }
            }

            return false;
        }

        private static void SendNewPhysicianNotification(AgencyPhysician physician, string agencyName)
        {
            string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", agencyName);
            var parameters = string.Format("loginid={0}", physician.LoginId);
            var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
            string bodyText = MessageBuilder.PrepareTextFrom("NewPhysicianConfirmation", "firstname", physician.DisplayName,
                "agencyname", agencyName, "encryptedQueryString", encryptedParameters);
            Notify.User(CoreSettings.NoReplyEmail, physician.EmailAddress, subject, bodyText);
        }

        private static void SendExistingPhysicianNotification(AgencyPhysician physician, string agencyName)
        {
            string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", agencyName);
            string bodyText = MessageBuilder.PrepareTextFrom("ExistingPhysicianConfirmation", "firstname", physician.DisplayName,
                    "agencyname", agencyName);
            Notify.User(CoreSettings.NoReplyEmail, physician.EmailAddress, subject, bodyText);
        }

        public JsonViewData TogglePhysician(Guid id, bool isDeprecated)
        {
            string messageAdd = isDeprecated ? "deleted" : "restored";
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Physician could not be " + messageAdd + ". Please try again." };
            if (physicianRepository.Delete(Current.AgencyId, id))
            {
                loginRepository.TogglePhysicianSnapshot(Current.AgencyId, id, isDeprecated);
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianDeleted, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Physician has been " + messageAdd + ".";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Physician could not be " + messageAdd + ".";
            }
            return viewData;
        }

        public string GetPhysicianPhone(Guid agencyId, Guid physicianId)
        {
            var physician = physicianRepository.Get(physicianId, agencyId);
            if (physician != null)
            {
                return physician.PhoneWorkFormatted;
            }
            else
            {
                return string.Empty;
            }
        }

        public bool UpdatePhysician(AgencyPhysician physician)
        {
            var isNewLogin = false;
            var physicianToEdit = physicianRepository.Get(physician.Id, Current.AgencyId);
            if (physicianToEdit != null)
            {
                bool physicianAccessChanged = physicianToEdit.PhysicianAccess != physician.PhysicianAccess;
                if (physicianAccessChanged && physician.PhysicianAccess)
                {
                    var login = loginRepository.Find(physician.EmailAddress.Trim());
                    if (login == null)
                    {
                        login = new Login();
                        login.DisplayName = physician.DisplayName;
                        login.EmailAddress = physician.EmailAddress.Trim();
                        login.Role = Roles.Physician.ToString();
                        login.IsActive = true;
                        login.IsLocked = false;
                        login.IsAxxessAdmin = false;
                        login.IsAxxessSupport = false;
                        login.LastLoginDate = DateTime.Now;
                        if (loginRepository.Add(login))
                        {
                            isNewLogin = true;
                        }
                    }
                    physician.LoginId = login.Id;
                }
                if (physicianRepository.UpdateModel(physician, physicianToEdit))
                {
                    var doesSnapshotExist = loginRepository.DoesPhysicianSnapshotExist(Current.AgencyId, physician.Id);
                    if (!physicianAccessChanged || AddOrUpdatePhysicianSnapShot(physician, doesSnapshotExist))
                    {
                        if (physicianAccessChanged && physician.PhysicianAccess)
                        {
                            if (isNewLogin)
                            {
                                ThreadPool.QueueUserWorkItem(state => SendNewPhysicianNotification(physician, Current.AgencyName));
                            }
                            else
                            {
                                ThreadPool.QueueUserWorkItem(state => SendExistingPhysicianNotification(physician, Current.AgencyName));
                            }
                        }
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.AgencyPhysician, LogAction.AgencyPhysicianUpdated, string.Empty);
                        return true;
                    }
                }
            }
            return false;
        }

        private bool AddOrUpdatePhysicianSnapShot(AgencyPhysician physician, bool isUpdate)
        {
            var result = false;
            if (isUpdate)
            {
                result = loginRepository.UpdatePhysicianSnapshot(Current.AgencyId, physician.Id, physician.NPI, physician.PhysicianAccess);
            }
            else
            {
                var physicianSnapShot = new PhysicianSnapshot
                {
                    Id = physician.Id,
                    LoginId = physician.LoginId,
                    AgencyId = physician.AgencyId,
                    Created = physician.Created,
                    NPI = physician.NPI,
                    HasPhysicianAccess = physician.PhysicianAccess,
                    IsDeprecated = physician.IsDeprecated
                };
                result = loginRepository.AddPhysicianSnapshot(physicianSnapShot);
            }
            return result;
        }

        public bool AddLicense(PhysicainLicense license)
        {
            var result = false;
            var physician = physicianRepository.Get(license.PhysicainId, Current.AgencyId);
            if (physician != null)
            {
                license.Id = Guid.NewGuid();
                license.Created = DateTime.Now;
                license.Modified = DateTime.Now;
                if (physician.Licenses.IsNotNullOrEmpty())
                {
                    physician.LicensesArray = physician.Licenses.ToObject<List<PhysicainLicense>>();
                }
                else
                {
                    physician.LicensesArray = new List<PhysicainLicense>();
                }
                physician.LicensesArray.Add(license);
                physician.Licenses = physician.LicensesArray.ToXml();
                if (physicianRepository.Update(physician))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.Physician, LogAction.AgencyPhysicianLicenseAdded, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateLicense(Guid id, Guid physicianId, DateTime ExpirationDate)
        {
            var physician = physicianRepository.Get(physicianId, Current.AgencyId);
            if (physician != null && physician.Licenses.IsNotNullOrEmpty())
            {
                physician.LicensesArray = physician.Licenses.ToObject<List<PhysicainLicense>>();
                if (physician.LicensesArray != null && physician.LicensesArray.Count > 0)
                {
                    var license = physician.LicensesArray.Find(l => l.Id == id);
                    if (license != null)
                    {
                        license.ExpirationDate = ExpirationDate;
                        physician.Licenses = physician.LicensesArray.ToXml();
                        if (physicianRepository.Update(physician))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.Physician, LogAction.AgencyPhysicianLicenseUpdated, string.Empty);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool DeleteLicense(Guid id, Guid physicianId)
        {
            var physician = physicianRepository.Get(physicianId, Current.AgencyId);
            if (physician != null && physician.Licenses.IsNotNullOrEmpty())
            {
                physician.LicensesArray = physician.Licenses.ToObject<List<PhysicainLicense>>();
                if (physician.LicensesArray != null && physician.LicensesArray.Count > 0)
                {
                    var license = physician.LicensesArray.Find(l => l.Id == id);
                    if (license != null)
                    {
                        physician.LicensesArray.Remove(license);
                        physician.Licenses = physician.LicensesArray.ToXml();
                        if (physicianRepository.Update(physician))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, physician.Id.ToString(), LogType.Physician, LogAction.AgencyPhysicianLicenseDeleted, string.Empty);
                            return true;
                        }

                    }
                }
            }
            return false;
        }

        #endregion

        #region patientListExporterHelperMethods
        public Guid getPrimaryPhysicianId(Guid PatientId)
        {
            Guid outPhysicianIdResult = new Guid();

            outPhysicianIdResult = physicianRepository.getPrimaryPhysicianId(PatientId);

            return outPhysicianIdResult;
        }

        public AgencyPhysician getPhysicianOnly(Guid Physicianid, Guid AgencyId)
        {
            AgencyPhysician outPhysicianResult = new AgencyPhysician();

            outPhysicianResult = physicianRepository.Get(Physicianid, AgencyId);

            return outPhysicianResult;
        }

        public bool doesPhysicianExist(Guid PatientId, Guid PhysicianId)
        {
            bool outPhysicianExistsStatus = default(bool);

            outPhysicianExistsStatus = physicianRepository.DoesPhysicianExist(PatientId, PhysicianId);

            return outPhysicianExistsStatus;
        }

        public IList<AgencyPhysician> GetAgencyPhysiciansWithPecosVerification()
        {
            var physicains = physicianRepository.GetAgencyPhysiciansLean(Current.AgencyId);
            if (physicains != null && physicains.Count > 0)
            {
                var pecoVerified = lookupRepository.NpisWithPecoVerified(physicains.Where(p => p.NPI.IsNotNullOrEmpty()).Select(p => p.NPI).ToArray());
                if (pecoVerified.IsNotNullOrEmpty())
                {
                    physicains.ForEach(p =>
                    {
                        if (pecoVerified.Contains(p.NPI))
                        {
                            p.IsPecosVerified = true;
                        }
                    });
                }
            }
            return physicains;
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
