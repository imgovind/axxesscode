﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Domain;
    

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    using System.Threading;
    using Axxess.Membership.Logging;

    public class MessageService : IMessageService
    {
        #region Private Members/Constructor

        private readonly IUserRepository userRepository;
        private readonly IAssetService assetService;
        private readonly ILoginRepository loginRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IMessageRepository messageRepository;

        public MessageService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, IAssetService assetService)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.assetService = assetService;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.messageRepository = agencyManagementDataProvider.MessageRepository;
        }

        #endregion

        #region IMessageService Members

        public IList<MessageFolder> FolderList()
        {
            return messageRepository.FolderList(Current.User.Id, Current.AgencyId).ToList();
        }

        public bool AddSystemMessage(Message message)
        {
            var result = false;

            return result;
        }

        public int GetTotalMessagesCount(string inboxType)
        {
            if (inboxType.IsNotNullOrEmpty())
            {
                if (inboxType.IsEqual("sent"))
                {
                    return messageRepository.GetSentMessageCount(Current.UserId, Current.AgencyId);
                }
                else if (inboxType.IsEqual("bin"))
                {
                    return messageRepository.GetUserMessageCount(Current.UserId, Current.AgencyId, true);
                }
                else
                {
                    return messageRepository.GetUserMessageCount(Current.UserId, Current.AgencyId, false);
                }
            }
            return 0;
        }

        public Message GetMessage(Guid messageId, int messageTypeId)
        {
            var message = new Message();
            if (messageTypeId == (int)MessageType.User)
            {
                message = messageRepository.GetUserMessage(messageId, Current.UserId, Current.AgencyId);
                if (message != null)
                {
                    if (!message.PatientId.IsEmpty())
                    {
                        message.PatientName = patientRepository.GetPatientNameById(message.PatientId, Current.AgencyId);
                    }

                    if (message.IsDeprecated)
                    {
                        message.MarkAsRead = true;
                    }

                    if (!message.MarkAsRead)
                    {
                        messageRepository.Read(messageId, Current.UserId, Current.AgencyId);
                    }
                }
            }
            else if (messageTypeId == (int)MessageType.System)
            {
                message = messageRepository.GetSystemMessage(messageId, Current.UserId, Current.AgencyId);
                if (message != null)
                {
                    message.Body = message.Body.ReplaceTokens();
                    if (!message.MarkAsRead)
                    {
                        messageRepository.Read(messageId, Current.UserId, Current.AgencyId);
                    }
                    message.RecipientNames = Current.UserFullName;
                }
            }
            else
            {
                message = messageRepository.GetSentMessage(messageId, Current.UserId, Current.AgencyId);
                if (message != null)
                {
                    message.MarkAsRead = true;
                }
            }
            return message;
        }

        public bool DeleteMessage(Guid messageId, int messageTypeId)
        {
            var result = false;
            if (messageTypeId == (int)MessageType.User || messageTypeId == (int)MessageType.System)
            {
                result = messageRepository.Delete(messageId, Current.UserId, Current.AgencyId);
            }
            return result;
        }

        public bool DeleteMany(List<Guid> messageList)
        {
            var result = false;
            if (messageList != null && messageList.Count > 0)
            {
                if (messageRepository.DeleteMany(messageList, Current.UserId, Current.AgencyId))
                {
                    result = true;
                }
            }
            return result;
        }

        public IList<Message> GetMessages(string inboxType, int pageNumber, int pageSize)
        {
            var messages = new List<Message>();

            if (inboxType.IsNotNullOrEmpty())
            {
                Guid messageFolderId = inboxType.Length == 36 ? inboxType.ToGuid() : Guid.Empty;

                if (inboxType.IsEqual("sent"))
                {
                    var sentMessages = messageRepository.GetSentMessages(Current.UserId, Current.AgencyId, pageNumber, pageSize);
                    sentMessages.ForEach(sentMessage =>
                    {
                        sentMessage.FromName = sentMessage.RecipientNames.IsNotNullOrEmpty() ? sentMessage.RecipientNames.Length > 25 ? sentMessage.RecipientNames.Substring(0, 25) + " ..." : sentMessage.RecipientNames : string.Empty;
                        sentMessage.Subject = sentMessage.Subject.IsNotNullOrEmpty() ? sentMessage.Subject.Length > 35 ? sentMessage.Subject.Substring(0, 35) + " ..." : sentMessage.Subject : string.Empty;
                        sentMessage.MarkAsRead = true;
                        messages.Add(sentMessage);
                    });
                }
                else if (inboxType.IsEqual("bin"))
                {
                    var deletedmessages = messageRepository.GetMessages(Current.UserId, Current.AgencyId, messageFolderId, pageNumber, true, pageSize).ToList();
                    deletedmessages.ForEach(deletedMessage =>
                    {
                        deletedMessage.MarkAsRead = true;
                        messages.Add(deletedMessage);
                    });
                }
                else
                {
                    messages = messageRepository.GetMessages(Current.UserId, Current.AgencyId, messageFolderId, pageNumber, false, pageSize).ToList();
                }
            }
            return messages.OrderByDescending(m => m.Created).ToList();
        }

        public bool SendMessage(Message message, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var recipientNames = new StringBuilder();
            var carbonCopyNames = new StringBuilder();
            var recipients = new Dictionary<Guid, string>();
            var emailAddresses = new Dictionary<Guid, string>();
            try
            {
                if (httpFiles != null && httpFiles.Count > 0)
                {
                    HttpPostedFileBase file = httpFiles.Get("Attachment1");
                    if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        using (var binaryReader = new BinaryReader(file.InputStream))
                        {
                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetService.AddAsset(asset))
                            {
                                message.AttachmentId = asset.Id;
                            }
                        }
                    }
                }

                var agencyId = Current.AgencyId;
                var currentUserId = Current.UserId;
                var currentUserName = Current.UserFullName;

                var userIds = message.CarbonCopyRecipients != null && message.CarbonCopyRecipients.Count > 0 ? message.Recipients.Union(message.CarbonCopyRecipients).Distinct().ToList() : message.Recipients;
                var users = userRepository.GetMessageRecipients(agencyId, userIds);
                var logins = loginRepository.GetActiveLogins(users.Select(s => s.LoginId).ToList());

                message.Recipients.ForEach(u =>
                {
                    var user = users.FirstOrDefault(f => f.Id == u);
                    if (user != null && !user.LoginId.IsEmpty())
                    {
                        var login = logins.FirstOrDefault(f => f.Id == user.LoginId);
                        if (login != null && login.IsActive)
                        {
                            emailAddresses.Add(u, login.EmailAddress);
                            recipients.Add(u, user.FirstName);
                            recipientNames.AppendFormat("{0}; ", user.DisplayName);
                        }
                    }
                });

                if (message.CarbonCopyRecipients != null)
                {
                    message.CarbonCopyRecipients.ForEach(u =>
                    {
                        var user = users.FirstOrDefault(f => f.Id == u);
                        if (user != null && !user.LoginId.IsEmpty())
                        {
                            var login = logins.FirstOrDefault(f => f.Id == user.LoginId);
                            if (login != null && login.IsActive)
                            {
                                emailAddresses.Add(u, login.EmailAddress);
                                recipients.Add(u, user.FirstName);
                                carbonCopyNames.AppendFormat("{0}; ", user.DisplayName);
                            }
                        }
                    });
                }

                var messageDetail = new MessageDetail(message, agencyId, currentUserId, currentUserName, recipientNames.ToString(), carbonCopyNames.ToString());
                var userMessages = new List<UserMessage>();
                message.Recipients.ForEach(recipient =>
                {
                    var userMessage = new UserMessage(agencyId, recipient, messageDetail.Id, MessageType.User);
                    userMessages.Add(userMessage);
                });

                if (messageRepository.AddMessageDetail(messageDetail)
                    && messageRepository.AddUserMessage(userMessages))
                {
                    new Thread(() =>
                    {
                        try
                        {
                            message.Recipients.ForEach(recipientId =>
                            {
                                if (recipients.ContainsKey(recipientId))
                                {
                                    var parameters = new string[4];
                                    parameters[0] = "recipientfirstname";
                                    parameters[1] = recipients[recipientId];
                                    parameters[2] = "senderfullname";
                                    parameters[3] = currentUserName;
                                    var bodyText = MessageBuilder.PrepareTextFrom("NewMessageNotification", parameters);
                                    Notify.User(CoreSettings.NoReplyEmail, emailAddresses[recipientId], string.Format("{0} sent you a message.", currentUserName), bodyText);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            Logger.Exception(e);
                        }
                    }).Start();
                    result = true;
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
            if (!result && message.AttachmentId.IsNotEmpty())
            {
                assetService.RemoveAsset(message.AttachmentId);
            }
            return result;
        }

        #endregion
    }
}
