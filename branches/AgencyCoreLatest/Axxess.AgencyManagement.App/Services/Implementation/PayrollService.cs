﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Domain;
    using ViewData;
    using Security;
    using Axxess.Membership.Enums;
    using Axxess.Log.Enums;

    public class PayrollService : IPayrollService
    {
        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPatientService patientService;

        public PayrollService(IAgencyManagementDataProvider agencyManagementDataProvider, IUserService userService, IPatientService patientService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.patientService = patientService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
        }

        public bool MarkAsPaid(List<string> itemList)
        {
            var result = true;

            if (itemList != null && itemList.Count > 0)
            {
                itemList.ForEach(visit =>
                {
                    var visitArray = visit.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                    if (visitArray != null && visitArray.Length == 3)
                    {
                        var scheduleEvent = patientRepository.GetScheduleOnly(Current.AgencyId, visitArray[0].ToGuid(), visitArray[1].ToGuid(), visitArray[2].ToGuid());
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.IsVisitPaid = true;
                            if (!patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            {
                                result = false;
                                return;
                            }
                            else
                            {
                                if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEvent.DisciplineTask))
                                {
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.MarkedPaid, (DisciplineTasks)scheduleEvent.DisciplineTask, "This visit is marked as paid.");
                                }
                            }
                        }
                    }
                });
            }
            return result;
        }

        public bool MarkAsUnpaid(List<string> itemList)
        {
            var result = true;

            if (itemList != null && itemList.Count > 0)
            {
                itemList.ForEach(visit =>
                {
                    var visitArray = visit.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                    if (visitArray != null && visitArray.Length == 3)
                    {
                        var scheduleEvent = patientRepository.GetScheduleOnly(Current.AgencyId, visitArray[0].ToGuid(), visitArray[1].ToGuid(), visitArray[2].ToGuid());
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.IsVisitPaid = false;
                            if (!patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            {
                                result = false;
                                return;
                            }
                            else
                            {
                                if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEvent.DisciplineTask))
                                {
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.MarkedUnpaid, (DisciplineTasks)scheduleEvent.DisciplineTask, "The visit is marked as unpaid.");
                                }
                            }
                        }
                    }
                });
            }
            return result;
        }

        public List<VisitSummary> GetSummary(DateTime startDate, DateTime endDate, string Status)
        {
            bool tempStatus;
            var list = new List<VisitSummary>();
            var userVisits = new Dictionary<Guid, int>();
            var visitTime=new Dictionary<Guid, int>();
            var travelTime=new Dictionary<Guid, int>();
            var allEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, startDate, endDate);
            if (allEpisodes != null && allEpisodes.Count > 0)
            {
                allEpisodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty())
                    {
                        var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where
                            (
                                s => s.IsPayable == true && 
                                !s.EventId.IsEmpty() 
                                && !s.UserId.IsEmpty() 
                                && s.EventDate.IsValidDate() 
                                && !s.IsMissedVisit 
                                && !s.IsDeprecated 
                                && s.IsCompleted() 
                                && s.DisciplineTask != (int)DisciplineTasks.FaceToFaceEncounter 
                                && (s.IsSkilledCare() || s.IsHhaNote() || s.IsMSW()) 
                                && s.EventDate.ToDateTime().Date >= startDate.Date 
                                && s.EventDate.ToDateTime().Date <= endDate.Date
                            ).ToList();
                        if (bool.TryParse(Status, out tempStatus)) scheduledEvents = scheduledEvents.Where(s => s.IsVisitPaid == tempStatus).ToList();
                        if (scheduledEvents != null && scheduledEvents.Count > 0)
                        {
                            scheduledEvents.ForEach(s =>
                            {
                                if (s.DisciplineTask == (int)DisciplineTasks.PASTravel && s.TravelMinSpent == 0)
                                {
                                    s.TravelMinSpent = s.MinSpent;
                                    s.MinSpent = 0;
                                }
                                if (!userVisits.ContainsKey(s.UserId))
                                {
                                    userVisits.Add(s.UserId, 1);
                                    visitTime.Add(s.UserId, s.MinSpent);
                                    travelTime.Add(s.UserId, s.TravelMinSpent);
                                }
                                else
                                {
                                    userVisits[s.UserId]++;
                                    visitTime[s.UserId] += s.MinSpent;
                                    travelTime[s.UserId] += s.TravelMinSpent;
                                }
                            });
                        }
                    }
                });
                List<UserNonVisitTask> userNonVisitTaskList = null;
                if (bool.TryParse(Status, out tempStatus)) 
                {
                    userNonVisitTaskList = agencyRepository.GetUserNonVisitTasksByStatusAndDateRange(Current.AgencyId, startDate, endDate, tempStatus).ToList();
                }
                else 
                {
                    userNonVisitTaskList = agencyRepository.GetUserNonVisitTasksByDateRange(Current.AgencyId, startDate, endDate).ToList();
                }
                if (userNonVisitTaskList != null && userNonVisitTaskList.Count > 0)
                {
                    foreach (UserNonVisitTask userNonVisitTask in userNonVisitTaskList)
                    {
                        if (!userVisits.ContainsKey(userNonVisitTask.UserId))
                        {
                            userVisits.Add(userNonVisitTask.UserId, 1);
                            visitTime.Add(userNonVisitTask.UserId, userNonVisitTask.MinSpent);
                        }
                        else 
                        {
                            userVisits[userNonVisitTask.UserId]++;
                            visitTime[userNonVisitTask.UserId] += userNonVisitTask.MinSpent;
                        }
                    }
                }
                if (userVisits.Count > 0)
                {
                    userVisits.ForEach(uv =>
                    {
                        var user = userRepository.GetUserOnly(uv.Key, Current.AgencyId);
                        if (user != null && user.IsDeprecated == false)
                        {
                            list.Add(new VisitSummary
                            {
                                UserId = uv.Key,
                                UserName = UserEngine.GetName(uv.Key, Current.AgencyId),
                                VisitCount = uv.Value,
                                FirstName = user.FirstName,
                                LastName = user.LastName,
                                TotalVisitTime = visitTime.ContainsKey(uv.Key) ? visitTime[uv.Key] : default(int),
                                TotalTravelTime = travelTime.ContainsKey(uv.Key) ? travelTime[uv.Key] : default(int)
                            });
                        }
                    });
                }
            }
            return list.OrderBy(u => u.LastName).ToList();
        }

        public List<PayrollDetailSummaryItem> GetVisitsSummary(Guid userId, DateTime startDate, DateTime endDate, string Status)
        {
            List<UserVisit> userVisits = GetVisits(userId, startDate, endDate, Status, true);
            List<PayrollDetailSummaryItem> summary = new List<PayrollDetailSummaryItem>();
            string lastTask = string.Empty;
            string lastInsurance = string.Empty;
            int taskCount = 1;
            double totalMileage = 0.0;
            double totalSurcharges = 0.0;
            double totalPayment = 0.0;
            int totalTaskTime = 0;
            PayrollDetailSummaryItem item = new PayrollDetailSummaryItem();
            var visitList = userVisits.OrderBy(v => v.PatientName).ThenBy(v => v.Insurance).ThenBy(v => v.TaskName);
            visitList.ForEach<UserVisit>(v =>
            {
                if (lastTask.CompareTo(v.TaskName) != 0 ||lastInsurance.CompareTo(v.Insurance)!=0) // new task or first task
                {
                    if (lastTask.Length != 0)
                    {
                        item.TotalTaskTime = string.Format("{0} Mins. ({1:0.00} Hours)", totalTaskTime, ((double)totalTaskTime) / 60.00);
                        item.TotalMileage = totalMileage;
                        item.TaskCount = taskCount.ToString();
                        item.TaskName = lastTask;
                        item.TotalSurcharges = totalSurcharges;
                        item.Insurance = patientService.GetInsurance(lastInsurance).IsNullOrEmpty() ? "No Valid Insurance" : patientService.GetInsurance(lastInsurance);
                        item.TotalPayment = totalPayment;
                        summary.Add(item);
                    }
                    // reset to new summary item
                    item = new PayrollDetailSummaryItem();
                    item.TaskName = v.TaskName;
                    item.PayRate = v.VisitRate;
                    item.MileageRage = v.MileageRate;
                    lastTask = item.IsNull() ? string.Empty : (item.TaskName.IsNotNullOrEmpty() ? item.TaskName : string.Empty);
                    lastInsurance = v.Insurance.IsNull() ? string.Empty : v.Insurance;
                    taskCount = 0;
                    totalMileage = 0;
                    totalTaskTime = 0;
                    totalSurcharges = 0.0;
                    totalPayment = 0.0;
                }
                if (v.TimeIn.IsNotNullOrEmpty() && v.TimeOut.IsNotNullOrEmpty())
                {
                    // get total task time
                    totalTaskTime   += v.MinSpent;
                    totalSurcharges += v.Surcharge.CleanNumberString(true).ToDouble();
                    totalMileage += v.AssociatedMileage.CleanNumberString(true).ToDouble();
                    totalPayment += v.Total;
                }
                taskCount++;
                if (visitList.Last<UserVisit>().Equals(v))
                {
                    item.TotalTaskTime = string.Format("{0} Mins. ({1:0.00} Hours)", totalTaskTime, ((double)totalTaskTime) / 60.00);
                    item.TotalMileage = totalMileage;
                    item.TaskCount = taskCount.ToString();
                    item.TotalSurcharges = totalSurcharges;
                    item.Insurance = patientService.GetInsurance(v.Insurance).IsNotNullOrEmpty() ? patientService.GetInsurance(v.Insurance) : "No Valid Insurance";
                    item.PayRate = v.VisitRate;
                    item.MileageRage = v.MileageRate;
                    item.TotalPayment = totalPayment;
                    summary.Add(item);
                }
            });
            return summary;
        }

        public List<PayrollDetail> GetDetails(DateTime startDate, DateTime endDate, string Status, bool isExcel)
        {
            bool tempStatus;
            var list = new List<PayrollDetail>();
            var userVisits = new Dictionary<Guid, List<UserVisit>>();
            var user = new User();
            var allEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, startDate, endDate).OrderBy(d => d.PatientName).ToList();
            if (allEpisodes != null && allEpisodes.Count > 0)
            {
                allEpisodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty())
                    {
                        var detail = episode.Details.ToObject<EpisodeDetail>();
                        var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
                            s.IsPayable == true &&
                            !s.EventId.IsEmpty() &&
                            !s.UserId.IsEmpty() &&
                            s.EventDate.IsValidDate() &&
                            !s.IsMissedVisit &&
                            !s.IsDeprecated &&
                            s.IsCompleted() &&
                            s.DisciplineTask != (int)DisciplineTasks.FaceToFaceEncounter &&
                            (s.IsSkilledCare() || s.IsHhaNote() || s.IsMSW()) &&
                            s.EventDate.ToDateTime().Date >= startDate.Date &&
                            s.EventDate.ToDateTime().Date <= endDate.Date).ToList();
                        if (bool.TryParse(Status, out tempStatus)) scheduledEvents = scheduledEvents.Where(s => s.IsVisitPaid == tempStatus).ToList();
                        if (scheduledEvents != null && scheduledEvents.Count > 0)
                        {
                            scheduledEvents.ForEach(s =>
                            {
                                if (s.DisciplineTask == (int)DisciplineTasks.PASTravel && s.TravelMinSpent == 0)
                                {
                                    s.TravelTimeIn = s.TimeIn;
                                    s.TravelTimeOut = s.TimeOut;
                                    s.TimeIn = null;
                                    s.TimeOut = null;
                                    s.MinSpent = 0;
                                }
                                var visitNote = string.Empty;
                                var statusComments = string.Empty;
                                if (s.Comments.IsNotNullOrEmpty())
                                {
                                    visitNote = s.Comments.Clean();
                                }
                                if (s.StatusComment.IsNotNullOrEmpty())
                                { 
                                    statusComments = s.StatusComment; 
                                }
                                user = userRepository.GetUserOnly(s.UserId, Current.AgencyId);
                                if (user != null)
                                {
                                    var userRates = new List<UserRate>();
                                    var userRate = new UserRate();
                                    if (user != null && user.Rates != null)
                                    {
                                        userRates = user.Rates.ToObject<List<UserRate>>();
                                    }
                                    if (detail != null && detail.PrimaryInsurance != null)
                                    {
                                        userRate = userRates.FirstOrDefault(r => r.Insurance == detail.PrimaryInsurance && r.Id == s.DisciplineTask);
                                    }
                                    var userVisit = new UserVisit
                                    {
                                        Id = s.EventId,
                                        UserId = s.UserId,
                                        VisitRate = userRate != null ? userRate.ToString() : "$0.00",
                                        MileageRate = userRate != null && userRate.MileageRate > 0 ? string.Format("${0:#0.00} / mile", userRate.MileageRate) : "0",
                                        Url = s.Url,
                                        UserDisplayName = UserEngine.GetName(s.UserId, Current.AgencyId),
                                        Status = s.Status,
                                        Surcharge = s.Surcharge.IsNotNullOrEmpty() && s.Surcharge.IsDouble() ? string.Format("${0:#0.00}", s.Surcharge.ToDouble()) : "0",
                                        StatusName = s.StatusName,
                                        PatientName = episode.PatientName,
                                        StatusComment = statusComments,
                                        TaskName = s.DisciplineTaskName,
                                        EpisodeId = s.EpisodeId,
                                        PatientId = s.PatientId,
                                        ScheduleDate = s.EventDate.ToZeroFilled(),
                                        VisitDate = s.VisitDate.ToZeroFilled(),
                                        IsMissedVisit = s.IsMissedVisit,
                                        TimeIn = s.TimeIn,
                                        TimeOut = s.TimeOut,
                                        TravelTimeIn = s.TravelTimeIn,
                                        TravelTimeOut = s.TravelTimeOut,
                                        IsVisitPaid = s.IsVisitPaid,
                                        AssociatedMileage = s.AssociatedMileage
                                    };
                                    var visitPay = GetVisitPayment(userVisit, userRate);
                                    if (userVisit != null && userRate != null && userVisit.AssociatedMileage.IsNotNullOrEmpty())
                                    {
                                        userVisit.MileagePayment = GetMileagePayment(userRate.MileageRate, userVisit.AssociatedMileage);
                                    }
                                    else
                                    {
                                        userVisit.MileagePayment = 0;
                                    }
                                    userVisit.VisitPayment = string.Format("${0:#0.00}", visitPay);
                                    userVisit.Total = s.Surcharge.IsNotNullOrEmpty() && s.Surcharge.IsDouble() ? s.Surcharge.ToDouble() + visitPay : visitPay;
                                    userVisit.TotalPayment = string.Format("${0:#0.00}", userVisit.Total);
                                    if (!userVisits.ContainsKey(s.UserId))
                                    {
                                        userVisits.Add(s.UserId, new List<UserVisit> { userVisit });
                                    }
                                    else
                                    {
                                        userVisits[s.UserId].Add(userVisit);
                                        userVisits[s.UserId].OrderBy(e => e.PatientName).ToList();
                                    }
                                }
                            });
                        }
                    }
                });
            }
            List<UserNonVisitTask> userNonVisitTaskList = null;
            if (bool.TryParse(Status, out tempStatus))
            {
                userNonVisitTaskList = agencyRepository.GetUserNonVisitTasksByStatusAndDateRange(Current.AgencyId, startDate, endDate, tempStatus);
            }
            else
            {
                userNonVisitTaskList = agencyRepository.GetUserNonVisitTasksByDateRange(Current.AgencyId, startDate, endDate);
            }
            if (userNonVisitTaskList != null && userNonVisitTaskList.Count > 0)
            {
                userNonVisitTaskList = userNonVisitTaskList.OrderBy(d => d.UserDisplayName).ToList();
                foreach (UserNonVisitTask userNonVisitTask in userNonVisitTaskList)
                {
                    if (userNonVisitTask != null)
                    {
                        UserNonVisitTaskRate nonVisitRate = null;
                        if (userNonVisitTask.TaskId != null && userNonVisitTask.UserId != null && userNonVisitTask.TaskId.IsNotEmpty() && userNonVisitTask.UserId.IsNotEmpty())
                        {
                            nonVisitRate = userService.GetUserNonVisitTaskRate(Current.AgencyId, userNonVisitTask.TaskId, userNonVisitTask.UserId);
                        }
                        if (user == null) 
                        {
                            user = userRepository.GetUserOnly(userNonVisitTask.UserId, Current.AgencyId);
                        }
                        var userVisit = new UserVisit
                        {
                            UserId = userNonVisitTask.UserId,
                            VisitRate = nonVisitRate != null ? (nonVisitRate.RateString.IsNotNullOrEmpty() ? nonVisitRate.RateString : "None Specified") : "Visit Rate Not Set",
                            PayType = nonVisitRate != null ? (nonVisitRate.RateTypeName.IsNotNullOrEmpty() ? nonVisitRate.RateTypeName : "None Specified") : "Visit Rate Not Set",
                            MileageRate = "0",
                            Url = "N/A",
                            UserDisplayName = user.DisplayName,
                            Status = userNonVisitTask.PaidStatus.ToString(),
                            Surcharge = "0",
                            StatusName = userNonVisitTask.PaidStatus == true ? "Completed" : "Not Completed",
                            PatientName = "N/A",
                            PatientIdNumber = "N/A",
                            StatusComment = userNonVisitTask.Comments.IsNotNullOrEmpty() ? userNonVisitTask.Comments : string.Empty,
                            TaskName = nonVisitRate != null ? (nonVisitRate.TaskTitle.IsNotNullOrEmpty() ? nonVisitRate.TaskTitle : string.Empty) : "Visit Rate Not Set",
                            VisitDate = userNonVisitTask.TaskDateDateString.IsNotNullOrEmpty() ? userNonVisitTask.TaskDateDateString : string.Empty,
                            ScheduleDate = userNonVisitTask.TaskDateDateString.IsNotNullOrEmpty() ? userNonVisitTask.TaskDateDateString : string.Empty,
                            TimeIn = userNonVisitTask.TimeInDateString.IsNotNullOrEmpty() ? userNonVisitTask.TimeInDateString : string.Empty,
                            TimeOut = userNonVisitTask.TimeOutDateString.IsNotNullOrEmpty() ? userNonVisitTask.TimeOutDateString : string.Empty,
                            TravelTimeIn = "N/A",
                            TravelTimeOut = "N/A",
                            PaidDate = userNonVisitTask.PaidDateDateString.IsNotNullOrEmpty() ? userNonVisitTask.PaidDateDateString : string.Empty,
                            IsVisitPaid = userNonVisitTask.PaidStatus,
                            AssociatedMileage = "0",
                            Insurance = "N/A",
                        };
                        if (nonVisitRate != null)
                        {
                            var visitPay = GetNonVisitPayment(userVisit, nonVisitRate);
                            userVisit.MileagePayment = default(double);
                            userVisit.VisitPayment = string.Format("${0:#0.00}", visitPay);
                            userVisit.Total = visitPay;
                            userVisit.TotalPayment = string.Format("${0:#0.00}", userVisit.Total);
                            if (!userVisits.ContainsKey(userNonVisitTask.UserId))
                            {
                                userVisits.Add(userNonVisitTask.UserId, new List<UserVisit> { userVisit });
                            }
                            else
                            {
                                userVisits[userNonVisitTask.UserId].Add(userVisit);
                            }
                        }
                        else
                        {
                            if (isExcel == false) 
                            {
                                if (!userVisits.ContainsKey(userNonVisitTask.UserId))
                                {
                                    userVisits.Add(userNonVisitTask.UserId, new List<UserVisit> { userVisit });
                                }
                                else
                                {
                                    userVisits[userNonVisitTask.UserId].Add(userVisit);
                                }
                            }
                        }
                    }
                }
            }
            if (userVisits.Count > 0)
            {
                foreach (KeyValuePair<Guid, List<UserVisit>> uv in userVisits)
                {
                    list.Add(new PayrollDetail
                    {
                        Id = uv.Key,
                        Name = UserEngine.GetName(uv.Key, Current.AgencyId),
                        Visits = uv.Value,
                        StartDate = startDate,
                        EndDate = endDate,
                        PayrollStatus = Status
                    });
                }
            }
            return list.OrderBy(l=>l.Name).ToList();
        }

        public List<UserVisit> GetVisits(Guid userId, DateTime startDate, DateTime endDate, string Status, bool isExcel)
        {
            bool tempStatus;
            var userVisits = new List<UserVisit>();
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            var allEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, startDate, endDate);
            if (allEpisodes != null && allEpisodes.Count > 0 && user != null)
            {
                var userRates = user.Rates.IsNotNullOrEmpty() ? user.Rates.ToObject<List<UserRate>>() : new List<UserRate>();
                allEpisodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty() && episode.Details.IsNotNullOrEmpty())
                    {
                        var detail = episode.Details.ToObject<EpisodeDetail>();
                        var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
                            s.IsPayable == true &&
                            s.UserId == userId &&
                            s.EventDate.IsValidDate() &&
                            !s.IsMissedVisit &&
                            !s.IsDeprecated &&
                            s.IsCompleted() &&
                            s.DisciplineTask != (int)DisciplineTasks.FaceToFaceEncounter &&
                            (s.IsSkilledCare() || s.IsHhaNote() || s.IsMSW()) &&
                            s.EventDate.ToDateTime().Date >= startDate.Date &&
                            s.EventDate.ToDateTime().Date <= endDate.Date).ToList();
                        if (bool.TryParse(Status, out tempStatus)) scheduledEvents = scheduledEvents.Where(s => s.IsVisitPaid == tempStatus).ToList();
                        if (scheduledEvents != null && scheduledEvents.Count > 0)
                        {
                            scheduledEvents.ForEach(s =>
                            {
                                var visitNote = string.Empty;
                                var statusComments = string.Empty;
                                if (s.Comments.IsNotNullOrEmpty()) visitNote = s.Comments.Clean();
                                if (s.StatusComment.IsNotNullOrEmpty()) statusComments = s.StatusComment;

                                var userRate = new UserRate();
                                if (detail != null && detail.PrimaryInsurance.IsNotNullOrEmpty())
                                {
                                    userRate = userRates.FirstOrDefault(r => r.Insurance == detail.PrimaryInsurance && r.Id == s.DisciplineTask);
                                }
                                if (s.DisciplineTask == (int)DisciplineTasks.PASTravel && s.TravelMinSpent == 0)
                                {
                                    s.TravelTimeIn = s.TimeIn;
                                    s.TravelTimeOut = s.TimeOut;
                                    s.TimeIn = null;
                                    s.TimeOut = null;
                                    s.MinSpent = 0;
                                }
                                var userVisit = new UserVisit
                                {
                                    Id = s.EventId,
                                    UserId = s.UserId,
                                    VisitRate = userRate != null ? userRate.ToString() : "none",
                                    PayType=userRate!=null?userRate.RateTypeName:"None Specified",
                                    MileageRate = userRate != null && userRate.MileageRate > 0 ? string.Format("${0:#0.00} / mile", userRate.MileageRate) : "0",
                                    Url = s.Url,
                                    UserDisplayName = user.DisplayName,
                                    Status = s.Status,
                                    Surcharge = s.Surcharge.IsNotNullOrEmpty() && s.Surcharge.IsDouble() ? string.Format("${0:#0.00}", s.Surcharge.ToDouble()) : "0",
                                    StatusName = s.StatusName,
                                    PatientName = episode.PatientName,
                                    PatientIdNumber = episode.PatientIdNumber,
                                    StatusComment = statusComments,
                                    TaskName = s.DisciplineTaskName,
                                    EpisodeId = s.EpisodeId,
                                    PatientId = s.PatientId,
                                    VisitDate = s.VisitDate.ToZeroFilled(),
                                    ScheduleDate = s.EventDate.ToZeroFilled(),
                                    IsMissedVisit = s.IsMissedVisit,
                                    TimeIn = s.TimeIn,
                                    TimeOut = s.TimeOut,
                                    TravelTimeIn=s.TravelTimeIn,
                                    TravelTimeOut=s.TravelTimeOut,
                                    IsVisitPaid = s.IsVisitPaid,
                                    AssociatedMileage = s.AssociatedMileage,
                                    Insurance=detail.PrimaryInsurance
                                };
                                var visitPay = GetVisitPayment(userVisit, userRate);
                                userVisit.VisitPayment = string.Format("${0:#0.00}", visitPay);
                                userVisit.Total = s.Surcharge.IsNotNullOrEmpty() && s.Surcharge.IsDouble() ? s.Surcharge.ToDouble() + visitPay : visitPay;
                                userVisit.TotalPayment = string.Format("${0:#0.00}", userVisit.Total);
                                userVisits.Add(userVisit);
                            });
                        }
                    }
                });
            }
            List<UserNonVisitTask> userNonVisitTaskList = null;
            if (bool.TryParse(Status, out tempStatus))
            {
                userNonVisitTaskList = agencyRepository.GetUserNonVisitTasksByUserStatusAndDateRange(Current.AgencyId, userId, startDate, endDate, tempStatus);
            }
            else 
            {
                userNonVisitTaskList = agencyRepository.GetUserNonVisitTasksByUserAndDateRange(Current.AgencyId, userId, startDate, endDate);
            }
            if (userNonVisitTaskList != null && userNonVisitTaskList.Count > 0) 
            {
                foreach (UserNonVisitTask userNonVisitTask in userNonVisitTaskList) 
                {
                    if (userNonVisitTask != null) 
                    {
                        UserNonVisitTaskRate nonVisitRate = null;
                        if(userNonVisitTask.TaskId != null && userNonVisitTask.UserId != null && userNonVisitTask.TaskId.IsNotEmpty() && userNonVisitTask.UserId.IsNotEmpty())
                        {
                            nonVisitRate = userService.GetUserNonVisitTaskRate(Current.AgencyId, userNonVisitTask.TaskId, userNonVisitTask.UserId);
                        }
                        var userVisit = new UserVisit
                        {
                            UserId = userNonVisitTask.UserId,
                            VisitRate = nonVisitRate != null ? (nonVisitRate.RateString.IsNotNullOrEmpty() ? nonVisitRate.RateString : "None Specified") : "Visit Rate Not Set",
                            PayType = nonVisitRate != null ? (nonVisitRate.RateTypeName.IsNotNullOrEmpty() ? nonVisitRate.RateTypeName : "None Specified") : "Visit Rate Not Set",
                            MileageRate = "0",
                            Url = "N/A",
                            UserDisplayName = user.DisplayName,
                            Status = userNonVisitTask.PaidStatus.ToString(),
                            Surcharge = "0",
                            StatusName = userNonVisitTask.PaidStatus == true ? "Completed" : "Not Completed",
                            PatientName = "N/A",
                            PatientIdNumber = "N/A",
                            StatusComment = userNonVisitTask.Comments.IsNotNullOrEmpty() ? userNonVisitTask.Comments : string.Empty,
                            TaskName = nonVisitRate != null ? (nonVisitRate.TaskTitle.IsNotNullOrEmpty() ? nonVisitRate.TaskTitle : string.Empty) : "Visit Rate Not Set",
                            VisitDate = userNonVisitTask.TaskDateDateString.IsNotNullOrEmpty() ? userNonVisitTask.TaskDateDateString : string.Empty,
                            ScheduleDate = userNonVisitTask.TaskDateDateString.IsNotNullOrEmpty() ? userNonVisitTask.TaskDateDateString : string.Empty,
                            TimeIn = userNonVisitTask.TimeInDateString.IsNotNullOrEmpty() ? userNonVisitTask.TimeInDateString : string.Empty,
                            TimeOut = userNonVisitTask.TimeOutDateString.IsNotNullOrEmpty() ? userNonVisitTask.TimeOutDateString : string.Empty,
                            TravelTimeIn = "N/A",
                            TravelTimeOut = "N/A",
                            PaidDate = userNonVisitTask.PaidDateDateString.IsNotNullOrEmpty() ? userNonVisitTask.PaidDateDateString : string.Empty,
                            IsVisitPaid = userNonVisitTask.PaidStatus,
                            AssociatedMileage = "0",
                            Insurance = "N/A",
                        };
                        var visitPay = GetNonVisitPayment(userVisit, nonVisitRate);
                        userVisit.VisitPayment = string.Format("${0:#0.00}", visitPay);
                        userVisit.Total = visitPay;
                        userVisit.TotalPayment = string.Format("${0:#0.00}", userVisit.Total);
                        if (nonVisitRate != null)
                        {
                            userVisits.Add(userVisit);
                        }
                        else 
                        {
                            if (isExcel == false) 
                            {
                                userVisits.Add(userVisit);
                            }
                        }
                    }
                }
            }
            return userVisits.OrderBy(l => l.PatientName).ToList();
        }

        private static double GetNonVisitPayment(UserVisit visit, UserNonVisitTaskRate rate) 
        {
            var total = 0.0;
            var hours = (double)visit.MinSpent / 60;
            if (visit != null && rate != null)
            {
                if (rate.Rate > 0)
                {
                    if (rate.RateType == (int)UserRateTypes.Hourly)
                    {
                        total = rate.Rate * hours;
                    }
                    else if (rate.RateType == (int)UserRateTypes.PerVisit)
                    {
                        total = rate.Rate;
                    }
                }
                visit.AssociatedMileage = string.Empty;
            }
            return total;
        }

        private static double GetVisitPayment(UserVisit visit, UserRate rate)
        {
            var total = 0.0;
            var hours = (double)visit.MinSpent / 60;
            if (visit != null && rate != null)
            {
                if (rate.Rate > 0)
                {
                    if (rate.RateType == (int)UserRateTypes.Hourly)
                    {
                        total = rate.Rate * hours;
                    }
                    else if (rate.RateType == (int)UserRateTypes.PerVisit)
                    {
                        total = rate.Rate;
                    }
                }
                if (visit.AssociatedMileage.IsNotNullOrEmpty() && visit.AssociatedMileage.IsDouble() && rate.MileageRate > 0)
                {
                    total += visit.AssociatedMileage.ToDouble() * rate.MileageRate;
                }
            }
            return total;
        }

        private static double GetMileagePayment(double rate, string AssociatedMileage)
        {
            if (AssociatedMileage.IsNullOrEmpty())
            {
                return 0;
            }
            else
            {
                double mileage;
                if (!double.TryParse(AssociatedMileage, out mileage))
                {
                    return 0;
                }
                else
                {
                    double payment = rate * mileage;
                    return Math.Round(payment,2);
                }
            }
        }
    }
}
