﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.ViewData;

    public interface IUserService
    {
        bool CreateUser(User user);
        bool DeleteUser(Guid userId);
        List<UserVisit> GetScheduleLean(Guid userId, DateTime from, DateTime to);
        List<UserVisit> GetScheduleLeanAll(Guid userId, DateTime from, DateTime to);
        //List<UserVisit> GetCompletedVisits(Guid userId, DateTime start, DateTime end);
        List<UserVisitWidget> GetScheduleWidget(Guid userId);
        bool IsEmailAddressInUse(string emailAddress);

        bool UpdateProfile(User user);
        bool IsPasswordCorrect(Guid userId, string password);
        bool IsSignatureCorrect(string signature);
        bool LoadUserRate(Guid fromId, Guid toId);
        bool AddLicense(License license, System.Web.HttpFileCollectionBase httpFiles);
        bool UpdatePermissions(FormCollection formCollection);
        bool DeleteLicense(Guid Id, Guid userId);
        bool UpdateLicense(License license);
        bool UpdateLicense(Guid Id, Guid userId, DateTime expirationDate, string LicenseNumber);
        bool UpdateLicense(Guid Id, Guid userId, DateTime initiationDate, DateTime expirationDate);
        List<User> GetUserByBranchAndStatus(Guid branchId, int status);
        string GetUserLicensesExpirationNotification(Guid userId, string userName);
        IList<LicenseItem> GetUserLicenses();
        IList<License> GetUserLicenses(Guid branchId, int status);
        bool AddLicenseItem(LicenseItem licenseItem, System.Web.HttpFileCollectionBase httpFiles);
        User Get(Guid id, Guid agencyId, bool isAssetNeeded);
        IList<UserRate> GetUserRates(Guid userId);
        bool RestoreUser(Guid userId);
        User GetUserOnly(Guid id, Guid agencyId);


        List<UserNonVisitTaskRate> GetUserNonVisitTaskRates(Guid AgencyId, Guid TaskId, Guid UserId);
        UserNonVisitTaskRate GetUserNonVisitTaskRate(Guid AgencyId, Guid TaskId, Guid UserId);
        SubscriptionPlanViewData GetAgencyUserSubcriptionPlanDetails();

        JsonViewData Activate(Guid userId);
        JsonViewData Deactivate(Guid userId);
        bool BulkActivate(List<Guid> userIds);

        JsonViewData MultiDeactivate(List<Guid> UserId);
        bool Update(User user);
    }
}
