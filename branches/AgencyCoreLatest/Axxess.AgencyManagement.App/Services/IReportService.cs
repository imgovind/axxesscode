﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.OasisC.Domain;

    public interface IReportService
    {
        #region Patient Reports
        List<ReportDescription> GetAllReport();
        #endregion

        #region Patient Reports

        List<BirthdayWidget> GetCurrentBirthdays();
        List<Birthday> GetPatientBirthdays(Guid addressBranchCode);
        List<Birthday> GetPatientBirthdays(Guid branchId, int month);
        List<AddressBookEntry> GetPatientAddressListing(Guid BranchId, int StatusId);
        List<PatientRoster> GetPatientRoster(Guid branchCode, int statusId, int InsuranceId, bool isExcel);
        List<PatientRoster> GetPatientRosterByDateRange(Guid branchCode, int statusId, int InsuranceId, DateTime startDate, DateTime endDate, bool isExcel);
        List<EmergencyContactInfo> GetPatientEmergencyContacts(int statusId, Guid addressBranchCode);
        List<PatientEvacuation> GetPatientEmergencyPreparedness(int statusId, Guid branchCode);
        List<PatientSocCertPeriod> GetPatientSocCertPeriod(Guid branchId, int statusId, DateTime startDate, DateTime endDate);
        List<PatientRoster> GetPatientByPhysician(Guid agencyPhysicianId);
        List<PatientRoster> GetPatientByResponsiableEmployee(Guid branchCode, Guid userId, int statusId);
        List<PatientRoster> GetPatientByResponsiableByCaseManager(Guid branchCode, Guid caseManagerId, int statusId);
        List<PatientRoster> GetPatientRosterByInsurance(Guid branchCode, int insurance, int statusId);
        List<Authorization> GetExpiringAuthorizaton(Guid branchId, int status, string authorizationStatus, DateTime startDate, DateTime endDate);
        List<SurveyCensus> GetPatientSurveyCensus(Guid branchId, int statusId, int insuranceId, bool isExcel);
        List<SurveyCensus> GetPatientSurveyCensusByDateRange(Guid branchId, int statusId, int insuranceId,DateTime startDate, DateTime endDate, bool isExcel);
        List<PatientRoster> GetPatientMonthlyAdmission(Guid branchCode, int statusId, int month, int year);
        List<PatientRoster> GetPatientAnnualAdmission(Guid branchCode, int statusId, int year);
        List<ReferralInfo> GetReferralInfos(Guid agencyId, int status, DateTime startDate, DateTime endDate);
        #endregion

        #region Clinician Reports

        IList<OpenOasis> GetAllOpenOasis(Guid branchCode, DateTime startDate, DateTime endDate);
        IList<ClinicalOrder> GetOrders(int statusId);
        List<MissedVisit> GetAllMissedVisit(Guid branchCode, DateTime startDate, DateTime endDate);
        IList<PhysicianOrder> GetPhysicianOrderHistory(Guid branchCode, int Status, DateTime startDate, DateTime endDate);
        IList<Order> GetPlanOfCareHistory(Guid branchCode, int Status, DateTime startDate, DateTime endDate);

        #endregion

        #region Schedule Reports

        List<ScheduleEvent> GetPatientScheduleEventsByDateRange(Guid patientId, DateTime fromDate, DateTime toDate);
        List<UserVisit> GetUserScheduleEventsByDateRange(Guid userId, Guid branchCode, DateTime fromDate, DateTime toDate);
        List<ScheduleEvent> GetPastDueScheduleEvents(Guid BranchId, DateTime StartDate, DateTime EndDate);
        List<ScheduleEvent> GetPastDueScheduleEventsByDiscipline(Guid BranchId, string discipline, DateTime StartDate, DateTime EndDate);
        List<ScheduleEvent> GetScheduleEventsByDateRange(Guid BranchId, DateTime StartDate, DateTime EndDate);
        List<ScheduleEvent> GetCaseManagerScheduleByBranch(Guid branchId, DateTime startDate, DateTime endDate);
        List<ScheduleEvent> GetScheduleDeviation(Guid branchId, DateTime startDate, DateTime endDate);
        List<PatientVisitLog> GetVisitVerificationLogs(Guid branchId, DateTime startDate, DateTime endDate);

        #endregion

        #region Employee Reports

        List<User> GetEmployeeRoster(Guid branchCode, int status);
        List<Birthday> GetEmployeeBirthdays(Guid branchCode, int status, int month);
        IList<LicenseItem> GetNonUserExpiringLicenses(string licenseType, DateTime StartDate, DateTime EndDate);
        List<License> GetEmployeeExpiringLicenses(Guid branchCode, int status, string licenseType, DateTime StartDate, DateTime EndDate);
        List<UserVisit> GetEmployeeScheduleByDateRange(Guid BranchId, DateTime StartDate, DateTime EndDate);
        List<UserPermission> GetEmployeePermissions(Guid branchId, int status);

        #endregion

        #region Billing Reports

        List<ClaimLean> BillViewDataByStatus(Guid branchId, string type, int status, DateTime startDate, DateTime endDate);
        List<ClaimLean> SubmittedBillViewDataByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate);
        List<ClaimLean> SubmittedClaimsByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate);
        List<ClaimLean> ExpectedSubmittedClaimsByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate);
        List<TypeOfBill> UnProcessedBillViewData(Guid BranchId, string type, int insuranceId);
        IList<Claim> GetPotentialCliamAutoCancels(Guid branchId);
        #endregion

        #region Statistical Reports

        List<UserVisit> GetEmployeeVisitList(Guid userId, DateTime startDate, DateTime endDate);

        List<PatientAdmission> GetPatientAdmissionsByInternalSource(Guid branchCode, int StatusId, DateTime startDate, DateTime endDate);
        List<VisitsByPayor> GetVisitsByPayor(Guid branchId, int statusId, List<int> insuranceList, DateTime startDate, DateTime endDate);
        #endregion
    }
}
