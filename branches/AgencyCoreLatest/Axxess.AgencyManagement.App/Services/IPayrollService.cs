﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.ViewData;

    public interface IPayrollService
    {
        bool MarkAsPaid(List<string> itemList);
        bool MarkAsUnpaid(List<string> itemList);
        List<VisitSummary> GetSummary(DateTime startDate, DateTime endDate, string Status);
        List<PayrollDetail> GetDetails(DateTime startDate, DateTime endDate, string Status, bool isExcel);
        List<UserVisit> GetVisits(Guid userId, DateTime startDate, DateTime endDate, string Status, bool isExcel);
        List<PayrollDetailSummaryItem> GetVisitsSummary(Guid userId, DateTime startDate, DateTime endDate, string Status);
    }
}
