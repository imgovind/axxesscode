﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;

    using Axxess.AgencyManagement.Domain;
    using System.Collections.Generic;
using Axxess.Core.Infrastructure;

    public interface IPhysicianService
    {
        bool CreatePhysician(AgencyPhysician physician);
        bool UpdatePhysician(AgencyPhysician physician);
        JsonViewData TogglePhysician(Guid id, bool isDeprecated);
        bool AddLicense(PhysicainLicense license);
        bool UpdateLicense(Guid id, Guid physicianId, DateTime ExpirationDate);
        bool DeleteLicense(Guid id, Guid physicianId);
        string GetPhysicianPhone(Guid agencyId, Guid physicianId);
        Guid getPrimaryPhysicianId(Guid PatientId);
        AgencyPhysician getPhysicianOnly(Guid Physicianid, Guid AgencyId);
        bool doesPhysicianExist(Guid PatientId, Guid PhysicianId);
        IList<AgencyPhysician> GetAgencyPhysiciansWithPecosVerification();
    }
}
