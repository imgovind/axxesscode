﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Web.Mvc;

    using AutoMapper;

    using StructureMap;
    using StructureMap.Pipeline;
    using StructureMap.Configuration.DSL;

    using Services;
    using Security;

    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.LookUp.Repositories;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.OasisC.Repositories;

    using Axxess.Log.Repositories;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    public class ApplicationRegistry : Registry
    {
        public ApplicationRegistry()
        {
            Scan(x =>
            {
                x.TheCallingAssembly();
                x.AddAllTypesOf<IController>();
                x.WithDefaultConventions();
            });

            For<ILog>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<DatabaseLog>();
            For<IOasisCDataProvider>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<OasisCDataProvider>();
            For<IMembershipDataProvider>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<MembershipDataProvider>();
            For<IAgencyManagementDataProvider>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<AgencyManagementDataProvider>();
            For<IAccountingManagementDataProvider>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<AccountingManagementDataProvider>();
            For<ILookUpDataProvider>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<LookUpDataProvider>();
            For<ILogDataProvider>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<LogDataProvider>();
            For<IAssessmentService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<AssessmentService>();
            For<IMembershipService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<MembershipService>();
            For<IFormsAuthenticationService>().LifecycleIs(Lifecycles.GetLifecycle(InstanceScope.Singleton)).Use<FormsAuthenticationService>();
        }
    }
}
