﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Axxess.Core.Extension;

using Axxess.AgencyManagement.Enums;

namespace Axxess.AgencyManagement.App.Common
{
    public static class Print
    {
        public static string Url(Guid episodeId, Guid patientId, Guid eventId, string type)
        {
            string url = "";
            string assessmentType = Convert.ToString((new Activities()).GetValueFromDescription(type));
            switch (type)
            {
                case "StartOfCare":
                case "OASISCStartofCare":
                case "OASISCStartofCarePT":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/StartofCare/View/{0}/{1}/{2}')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;

                case "ResumptionOfCare":
                case "OASISCResumptionofCare":
                case "OASISCResumptionofCarePT":
                case "OASISCResumptionofCareOT":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/ResumptionOfCare/View/{0}/{1}/{2}')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;

                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowupPT":
                case "OASISCFollowupOT":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/Followup/View/{0}/{1}/{2}')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;

                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationPT":
                case "OASISCRecertificationOT":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/Recertification/View/{0}/{1}/{2}')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;

                case "TransferInPatientNotDischarged":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/TransferNotDischarge/View/{0}/{1}/{2}')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;

                case "TransferInPatientDischarged":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/TransferDischarge/View/{0}/{1}/{2}')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;

                case "DischargeFromAgencyDeath":
                case "OASISCDeath":
                case "OASISCDeathPT":
                case "OASISCDeathOT":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/DischargeDeath/View/{0}/{1}/{2}')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;

                case "DischargeFromAgency":
                case "OASISCDischarge":
                case "OASISCDischargePT":
                case "OASISCDischargeOT":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/Discharge/View/{0}/{1}/{2}')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;
                case "PhysicianOrder":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/Order/View/{0}/{1}/{2}')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;
                case "HCFA485":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/485/View/{0}/{1}/{2}')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;
                case "CommunicatioNote":
                    break;
                case "DischargeSummary":
                    break;
                case "SkilledNurseVisit":
                    break;
                case "SixtyDaySummary":
                    break;
                case "TransferSummary":
                    break;
                case "LVNSupervisoryVisit":
                    break;
                case "HHAideSupervisoryVisit":
                    break;
                case "MSW":
                    break;
                case "HHAideVisit":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/HHAVisitNote/View/{0}/{1}/{2},true')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;
                case "HHAideCarePlan":
                    url = string.Format("<a href=\"javascript:void(0);\" onclick=\"axxess_desktop.openPrint('/HHACarePlan/View/{0}/{1}/{2},true')\"><img src=\"/Images/icons/print.png\" alt=\"Print View\" title=\"Print View\" /></a>", episodeId, patientId, eventId);
                    break;
            }
            return url;
        }
    }
}
