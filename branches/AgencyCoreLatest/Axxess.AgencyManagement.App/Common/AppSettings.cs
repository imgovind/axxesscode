﻿namespace Axxess.AgencyManagement.App {
    using System;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    public static class AppSettings {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();

        public static int ClusterId { get { return configuration.AppSettings["ClusterId"].ToInteger(); } }
        public static string SingleSignOnUrl { get { return configuration.AppSettings["SingleSignOnUrl"]; } }
        public static string IdentifyAgencyCookie { get { return configuration.AppSettings["IdentifyAgencyCookie"]; } }
        public static string RememberMeCookie { get { return configuration.AppSettings["RememberMeCookie"]; } }
        public static int RememberMeForTheseDays { get { return configuration.AppSettings["RememberMeForTheseDays"].ToInteger(); } }
        public static string AuthenticationType { get { return configuration.AppSettings["AuthenticationType"]; } }
        public static string GoogleAPIKey { get { return configuration.AppSettings["GoogleApiKey"]; } }
        public static double FormsAuthenticationTimeoutInMinutes { get { return configuration.AppSettings["FormsAuthenticationTimeoutInMinutes"].ToDouble(); } }
        public static bool UsePersistentCookies { get { return configuration.AppSettings["UsePersistentCookies"].ToBoolean(); } }
        public static bool UseMinifiedJs { get { return configuration.AppSettings["UseMinifiedJs"].ToBoolean(); } }
        public static string NewsFeedUrl { get { return configuration.AppSettings["NewsFeedUrl"]; } }
        public static string AllowedImageExtensions { get { return configuration.AppSettings["AllowedImageExtensions"]; } }
        public static string ANSIGeneratorUrl { get { return configuration.AppSettings["ANSIGeneratorUrl"]; } }
        public static string PatientEligibilityUrl { get { return configuration.AppSettings["PatientEligibilityUrl"]; } }
        public static string PdfBasePath { get { return configuration.AppSettings["PdfBasePath"]; } }
        public static string XmlBasePath { get { return configuration.AppSettings["XmlBasePath"]; } }
        public static string IncidentReportPdf { get { return configuration.AppSettings["IncidentReportPdf"]; } }
        public static string IncidentReportXml { get { return configuration.AppSettings["IncidentReportXml"]; } }
        public static string InfectionReportPdf { get { return configuration.AppSettings["InfectionReportPdf"]; } }
        public static string InfectionReportXml { get { return configuration.AppSettings["InfectionReportXml"]; } }
        public static string BillingClaimsPdf { get { return configuration.AppSettings["BillingClaimsPdf"]; } }
        public static string BillingFinalPdf { get { return configuration.AppSettings["BillingFinalPdf"]; } }
        public static string BillingRapPdf { get { return configuration.AppSettings["BillingRapPdf"]; } }
        public static string HCFA1500Pdf { get { return configuration.AppSettings["HCFA1500Pdf"]; } }
        public static string RemittancesPdf { get { return configuration.AppSettings["RemittancesPdf"]; } }
        public static string RemittancePdf { get { return configuration.AppSettings["RemittancePdf"]; } }
        public static string UB04Pdf { get { return configuration.AppSettings["UB04Pdf"]; } }
        public static string MessagePdf { get { return configuration.AppSettings["MessagePdf"]; } }
        public static string OasisPdf { get { return configuration.AppSettings["OasisPdf"]; } }
        public static string OasisXml { get { return configuration.AppSettings["OasisXml"]; } }
        public static string DischargeFromAgencyXml { get { return configuration.AppSettings["DischargeFromAgencyXml"]; } }
        public static string DischargeFromAgencyXml4 { get { return configuration.AppSettings["DischargeFromAgencyXml4"]; } }
        public static string OasisProfilePdf { get { return configuration.AppSettings["OasisProfilePdf"]; } }
        public static string OasisProfileXml { get { return configuration.AppSettings["OasisProfileXml"]; } }
        public static string OasisAuditPdf { get { return configuration.AppSettings["OasisAuditPdf"]; } }
        public static string HospitalizationLogPdf { get { return configuration.AppSettings["HospitalizationLogPdf"]; } }
        public static string HospitalizationLogXml { get { return configuration.AppSettings["HospitalizationLogXml"]; } }
        public static string PlanOfCare485Pdf { get { return configuration.AppSettings["PlanOfCare485Pdf"]; } }
        public static string PlanOfCare487Pdf { get { return configuration.AppSettings["PlanOfCare487Pdf"]; } }
        public static string AllergyProfilePdf { get { return configuration.AppSettings["AllergyProfilePdf"]; } }
        public static string AuthorizationPdf { get { return configuration.AppSettings["AuthorizationPdf"]; } }
        public static string ComNotePdf { get { return configuration.AppSettings["ComNotePdf"]; } }
        public static string MedicareEligibiltyPdf { get { return configuration.AppSettings["MedicareEligibiltyPdf"]; } }
        public static string MedicareEligibiltySummaryPdf { get { return configuration.AppSettings["MedicareEligibiltySummaryPdf"]; } }
        public static string MedProfilePdf { get { return configuration.AppSettings["MedProfilePdf"]; } }
        public static string PatientProfilePdf { get { return configuration.AppSettings["PatientProfilePdf"]; } }
        public static string PhysicianOrderPdf { get { return configuration.AppSettings["PhysicianOrderPdf"]; } }
        public static string PhysFaceToFacePdf { get { return configuration.AppSettings["PhysFaceToFacePdf"]; } }
        public static string PhysFaceToFaceXml { get { return configuration.AppSettings["PhysFaceToFaceXml"]; } }
        public static string ClaimResponsePdf { get { return configuration.AppSettings["ClaimResponsePdf"]; } }
        public static string ClaimResponseXml { get { return configuration.AppSettings["ClaimResponseXml"]; } }
        public static string ReferralPdf { get { return configuration.AppSettings["ReferralPdf"]; } }
        public static string ReferralXml { get { return configuration.AppSettings["ReferralXml"]; } }
        public static string NutritionalAssessmentPdf { get { return configuration.AppSettings["NutritionalAssessmentPdf"]; } }
        public static string NutritionalAssessmentXml { get { return configuration.AppSettings["NutritionalAssessmentXml"]; } }
        public static string PayrollDetailPdf { get { return configuration.AppSettings["PayrollDetailPdf"]; } }
        public static string PayrollSummaryPdf { get { return configuration.AppSettings["PayrollSummaryPdf"]; } }
        public static string AnnualCalendarPdf { get { return configuration.AppSettings["AnnualCalendarPdf"]; } }
        public static string MasterCalendarPdf { get { return configuration.AppSettings["MasterCalendarPdf"]; } }
        public static string MonthCalendarPdf { get { return configuration.AppSettings["MonthCalendarPdf"]; } }
        public static string HHACarePlanPdf { get { return configuration.AppSettings["HHACarePlanPdf"]; } }
        public static string HHACarePlanXml { get { return configuration.AppSettings["HHACarePlanXml"]; } }
        public static string HHACarePlan2Xml { get { return configuration.AppSettings["HHACarePlan2Xml"]; } }
        public static string HHASVisitPdf { get { return configuration.AppSettings["HHASVisitPdf"]; } }
        public static string HHASVisitXml { get { return configuration.AppSettings["HHASVisitXml"]; } }
        public static string HHAVisitPdf { get { return configuration.AppSettings["HHAVisitPdf"]; } }
        public static string HomeMakerNotePdf { get { return configuration.AppSettings["HomeMakerNotePdf"]; } }
        public static string HomeMakerNoteXml { get { return configuration.AppSettings["HomeMakerNoteXml"]; } }
        public static string HHAVisitXml { get { return configuration.AppSettings["HHAVisitXml"]; } }
        public static string MissedVisitPdf { get { return configuration.AppSettings["MissedVisitPdf"]; } }
        public static string MSWEvalPdf { get { return configuration.AppSettings["MSWEvalPdf"]; } }
        public static string MSWEvalXml { get { return configuration.AppSettings["MSWEvalXml"]; } }
        public static string MSWEvalXml2 { get { return configuration.AppSettings["MSWEvalXml2"]; } }
        public static string MSWProgressPdf { get { return configuration.AppSettings["MSWProgressPdf"]; } }
        public static string MSWProgressXml { get { return configuration.AppSettings["MSWProgressXml"]; } }
        public static string MSWVisitPdf { get { return configuration.AppSettings["MSWVisitPdf"]; } }
        public static string MSWVisitXml { get { return configuration.AppSettings["MSWVisitXml"]; } }
        public static string TransportationLogPdf { get { return configuration.AppSettings["TransportationLogPdf"]; } }
        public static string TransportationLogXml { get { return configuration.AppSettings["TransportationLogXml"]; } }
        public static string DiabeticDailyPdf { get { return configuration.AppSettings["DiabeticDailyPdf"]; } }
        public static string DiabeticDailyXml { get { return configuration.AppSettings["DiabeticDailyXml"]; } }
        public static string PediatricPdf { get { return configuration.AppSettings["PediatricPdf"]; } }
        public static string PediatricXml { get { return configuration.AppSettings["PediatricXml"]; } }
        public static string PediatricXml2 { get { return configuration.AppSettings["PediatricXml2"]; } }
        public static string PediatricAssessmentXml { get { return configuration.AppSettings["PediatricAssessmentXml"]; } }
        public static string DischargeSummaryPdf { get { return configuration.AppSettings["DischargeSummaryPdf"]; } }
        public static string DischargeSummaryXml { get { return configuration.AppSettings["DischargeSummaryXml"]; } }
        public static string PTDischargeSummaryPdf { get { return configuration.AppSettings["PTDischargeSummaryPdf"]; } }
        public static string PTDischargeSummaryPdf2 { get { return configuration.AppSettings["PTDischargeSummaryPdf2"]; } }
        public static string PTDischargeSummaryXml { get { return configuration.AppSettings["PTDischargeSummaryXml"]; } }
        public static string PTDischargeSummaryXml2 { get { return configuration.AppSettings["PTDischargeSummaryXml2"]; } }
        public static string OTDischargeSummaryPdf { get { return configuration.AppSettings["OTDischargeSummaryPdf"]; } }
        public static string OTDischargeSummaryXml { get { return configuration.AppSettings["OTDischargeSummaryXml"]; } }
        public static string STDischargeSummaryXml { get { return configuration.AppSettings["STDischargeSummaryXml"]; } }
        public static string InitialSummaryOfCarePdf { get { return configuration.AppSettings["InitialSummaryOfCarePdf"]; } }
        public static string InitialSummaryOfCareXml { get { return configuration.AppSettings["InitialSummaryOfCareXml"]; } }
        public static string LVNSVisitPdf { get { return configuration.AppSettings["LVNSVisitPdf"]; } }
        public static string LVNSVisitXml { get { return configuration.AppSettings["LVNSVisitXml"]; } }
        public static string SixtyDaySummaryPdf { get { return configuration.AppSettings["SixtyDaySummaryPdf"]; } }
        public static string SixtyDaySummaryXml { get { return configuration.AppSettings["SixtyDaySummaryXml"]; } }
        public static string SkilledNurseVisitPdf { get { return configuration.AppSettings["SkilledNurseVisitPdf"]; } }
        public static string SkilledNurseVisitXml { get { return configuration.AppSettings["SkilledNurseVisitXml"]; } }
        public static string LabsPdf { get { return configuration.AppSettings["LabsPdf"]; } }
        public static string LabsXml { get { return configuration.AppSettings["LabsXml"]; } }
        public static string TransferSummaryPdf { get { return configuration.AppSettings["TransferSummaryPdf"]; } }
        public static string TransferSummaryXml { get { return configuration.AppSettings["TransferSummaryXml"]; } }
        public static string TransferSummaryPdf2 { get { return configuration.AppSettings["TransferSummaryPdf2"]; } }
        public static string TransferSummaryXml2 { get { return configuration.AppSettings["TransferSummaryXml2"]; } }
        public static string TransferSummaryXml3 { get { return configuration.AppSettings["TransferSummaryXml3"]; } }
        public static string TransferSummaryXml4 { get { return configuration.AppSettings["TransferSummaryXml4"]; } }
        public static string WoundCarePdf { get { return configuration.AppSettings["WoundCarePdf"]; } }
        public static string WoundCareXml { get { return configuration.AppSettings["WoundCareXml"]; } }
        public static string PASCarePlanPdf { get { return configuration.AppSettings["PASCarePlanPdf"]; } }
        public static string PASCarePlanXml { get { return configuration.AppSettings["PASCarePlanXml"]; } }
        public static string PASVisitPdf { get { return configuration.AppSettings["PASVisitPdf"]; } }
        public static string PASVisitXml { get { return configuration.AppSettings["PASVisitXml"]; } }
        public static string OTEvalPdf { get { return configuration.AppSettings["OTEvalPdf"]; } }
        public static string OTEvalXml { get { return configuration.AppSettings["OTEvalXml"]; } }
        public static string OTEvalXml2 { get { return configuration.AppSettings["OTEvalXml2"]; } }
        public static string OTEvalXml3 { get { return configuration.AppSettings["OTEvalXml3"]; } }
        public static string OTEvalXml4 { get { return configuration.AppSettings["OTEvalXml4"]; } }
        public static string OTPOCXml { get { return configuration.AppSettings["OTPOCXml"]; } }
        public static string OTPOCXml2 { get { return configuration.AppSettings["OTPOCXml2"]; } }
        public static string POCPdf { get { return configuration.AppSettings["POCPdf"]; } }
        public static string OTDischargeXml { get { return configuration.AppSettings["OTDischargeXml"]; } }
        public static string OTDischargeXml2 { get { return configuration.AppSettings["OTDischargeXml2"]; } }
        public static string OTDischargeXml3 { get { return configuration.AppSettings["OTDischargeXml3"]; } }
        public static string OTReassessmentPdf { get { return configuration.AppSettings["OTReassessmentPdf"]; } }
        public static string OTReassessmentXml { get { return configuration.AppSettings["OTReassessmentXml"]; } }
        public static string OTVisitPdf { get { return configuration.AppSettings["OTVisitPdf"]; } }
        public static string OTVisitXml { get { return configuration.AppSettings["OTVisitXml"]; } }
        public static string OTVisitXml2 { get { return configuration.AppSettings["OTVisitXml2"]; } }
        public static string PTDischargePdf { get { return configuration.AppSettings["PTDischargePdf"]; } }
        public static string PTDischargePdf2 { get { return configuration.AppSettings["PTDischargePdf2"]; } }
        public static string PTDischargeXml { get { return configuration.AppSettings["PTDischargeXml"]; } }
        public static string PTDischargeXml2 { get { return configuration.AppSettings["PTDischargeXml2"]; } }
        public static string PTEvalPdf { get { return configuration.AppSettings["PTEvalPdf"]; } }
        public static string PTEvalXml { get { return configuration.AppSettings["PTEvalXml"]; } }
        public static string PTEvalXml2 { get { return configuration.AppSettings["PTEvalXml2"]; } }
        public static string PTEvalXml3 { get { return configuration.AppSettings["PTEvalXml3"]; } }
        public static string PTEvalXml4 { get { return configuration.AppSettings["PTEvalXml4"]; } }
        public static string PTEvalXml5 { get { return configuration.AppSettings["PTEvalXml5"]; } }
        public static string PTPOCXml { get { return configuration.AppSettings["PTPOCXml"]; } }
        public static string PTReassessmentPdf { get { return configuration.AppSettings["PTReassessmentPdf"]; } }
        public static string PTReassessmentXml { get { return configuration.AppSettings["PTReassessmentXml"]; } }
        public static string PTVisitPdf { get { return configuration.AppSettings["PTVisitPdf"]; } }
        public static string PTVisitXml { get { return configuration.AppSettings["PTVisitXml"]; } }
        public static string PTVisitXml2 { get { return configuration.AppSettings["PTVisitXml2"]; } }
        public static string SupervisoryPdf { get { return configuration.AppSettings["SupervisoryPdf"]; } }
        public static string SupervisoryXml { get { return configuration.AppSettings["SupervisoryXml"]; } }
        public static string STEvalPdf { get { return configuration.AppSettings["STEvalPdf"]; } }
        public static string STEvalXml { get { return configuration.AppSettings["STEvalXml"]; } }
        public static string STEvalXml2 { get { return configuration.AppSettings["STEvalXml2"]; } }
        public static string STEvalXml3 { get { return configuration.AppSettings["STEvalXml3"]; } }
        public static string STPlanOfCarePdf { get { return configuration.AppSettings["STPlanOfCarePdf"]; } }
        public static string STPlanOfCareXml { get { return configuration.AppSettings["STPlanOfCareXml"]; } }
        public static string STDischargeXml { get { return configuration.AppSettings["STDischargeXml"]; } }
        public static string STReassessmentXml { get { return configuration.AppSettings["STReassessmentXml"]; } }
        public static string STVisitPdf { get { return configuration.AppSettings["STVisitPdf"]; } }
        public static string STVisitXml { get { return configuration.AppSettings["STVisitXml"]; } }
        public static string STVisitXml2 { get { return configuration.AppSettings["STVisitXml2"]; } }
        public static string UAPInsulinPdf { get { return configuration.AppSettings["UAPInsulinPdf"]; } }
        public static string UAPInsulinXml { get { return configuration.AppSettings["UAPInsulinXml"]; } }
        public static string UAPWoundCarePdf { get { return configuration.AppSettings["UAPWoundCarePdf"]; } }
        public static string UAPWoundCareXml { get { return configuration.AppSettings["UAPWoundCareXml"]; } }
        public static string HomeHealthGoldLogo { get { return configuration.AppSettings["HomeHealthGoldLogo"]; } }
        public static string BrowserCompatibilityCheck { get { return configuration.AppSettings["BrowserCompatibilityCheck"]; } }
        public static string WeatherImageUriFormat { get { return configuration.AppSettings["WeatherImageUriFormat"]; } }
        public static string WeatherForecastImageUriFormat { get { return configuration.AppSettings["WeatherForecastImageUriFormat"]; } }
        public static string WeatherImageThumbnailUriFormat { get { return configuration.AppSettings["WeatherImageThumbnailUriFormat"]; } }
        public static string YahooWeatherUri { get { return configuration.AppSettings["YahooWeatherUri"]; } }
        public static string GetRemoteContent { get { return configuration.AppSettings["GetRemoteContent"]; } }
        public static bool IsSingleUserMode { get { return configuration.AppSettings["IsSingleUserMode"].ToBoolean(); } }
        public static string RemittanceSchedulerUrl { get { return configuration.AppSettings["RemittanceSchedulerUrl"]; } }
        public static string DrugDrugInteractionPdf { get { return configuration.AppSettings["DrugDrugInteractionPdf"]; } }
        public static string BillingSuppliesPdf { get { return configuration.AppSettings["BillingSuppliesPdf"]; } }
        public static string PsychPdf { get { return configuration.AppSettings["PsychPdf"]; } }
        public static string PsychXml { get { return configuration.AppSettings["PsychXml"]; } }
        public static string PsychAssessmentXml { get { return configuration.AppSettings["PsychAssessmentXml"]; } }
        public static string PsychAssessmentPdf { get { return configuration.AppSettings["PsychAssessmentPdf"]; } }
        public static string InvoicePdf { get { return configuration.AppSettings["InvoicePdf"]; } }
        public static string PASTravelXml { get { return configuration.AppSettings["PASTravelXml"]; } }
        public static string PASTravelPdf { get { return configuration.AppSettings["PASTravelPdf"]; } }

        //public static string AssetSecret { get { return configuration.AppSettings["AssetSecret"]; } }
        //public static string AssetKey { get { return configuration.AppSettings["AssetKey"]; } }
        //public static string AssetUploadURL { get { return configuration.AppSettings["AssetUploadURL"]; } }
        //public static string AssetServeURL { get { return configuration.AppSettings["AssetServeURL"]; } }
        //public static bool UseDBAssets { get { return configuration.AppSettings["UseDBAssets"].ToBoolean(); } }
        //public static bool StoreAssetBytesInDB { get { return configuration.AppSettings["StoreAssetBytesInDB"].ToBoolean(); } }

        public static string AccountEmailAddress { get { return configuration.AppSettings["AccountEmailAddress"]; } }
              public static DateTime ServiceLocationActivationDate { get { return configuration.AppSettings["ServiceLocationActivationDate"].ToDateTime(); } }
        public static string EdiFilesUrl { get { return configuration.AppSettings["EdiFilesUrl"]; } }

        public static bool IsProduction
        {
            get
            {
                return configuration.AppSettings["IsProduction"] == "true" ? true : false;
            }
        }
    }
}
