﻿namespace Axxess.AgencyManagement.App.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Enums;
    using Services;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    using Axxess.AgencyManagement.Extensions;
    using Axxess.OasisC.Enums;
    using Axxess.AgencyManagement.Repositories;

    public static class Url
    {
        public static void Set(ScheduleEvent scheduleEvent, bool addReassignLink, bool usePrintIcon)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0 && !scheduleEvent.EpisodeId.IsEmpty() && !scheduleEvent.PatientId.IsEmpty() && !scheduleEvent.EventId.IsEmpty())
            {
                string onclick = string.Empty;
                string reopenUrl = string.Empty;
                string printUrl = Print(scheduleEvent, usePrintIcon);

                
                string oasisProfileUrl = string.Empty;
                //scheduleEvent.VisitVerifyUrl = scheduleEvent.IsVisitVerified ? 
                //    new StringBuilder("<a href=\"javascript:void(0)\" onclick=\"")
                //        .Append("Acore.Open('verifiedvisit', {")
                //        .AppendFormat("eventId: '{0}', patientId: '{1}', episodeId: '{2}' ", scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.EpisodeId)
                //        .Append("})\"><span class=\"verified\"></span>")
                //        .ToString() : string.Empty;
                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;

                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                        onclick = string.Format("StartOfCare.Load('{0}','{1}','StartOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        if (scheduleEvent.Status == ((int)ScheduleStatus.OasisExported).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                        {
                            oasisProfileUrl = "<a href=\"javascript:void(0)\" onclick=\"" +
                                "Acore.OpenPrintView({ " +
                                    "Url: 'Oasis/Profile/" + scheduleEvent.EventId + "/" + AssessmentType.StartOfCare.ToString() + "'," +
                                    "PdfUrl: 'Oasis/OasisProfilePdf'," +
                                    "PdfData: { 'Id': '" + scheduleEvent.EventId + "','type': '" + AssessmentType.StartOfCare.ToString() + "' }" +
                                "})\"><span class=\"img icon money\"></span></a>";
                        }
                        break;
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.NonOASISStartofCare:
                        onclick = string.Format("NonOasisStartOfCare.Load('{0}','{1}','NonOasisStartOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        onclick = string.Format("ResumptionOfCare.Load('{0}','{1}','ResumptionOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        if (scheduleEvent.Status == ((int)ScheduleStatus.OasisExported).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                        {
                            oasisProfileUrl = "<a href=\"javascript:void(0)\" onclick=\"" +
                                "Acore.OpenPrintView({ " +
                                    "Url: 'Oasis/Profile/" + scheduleEvent.EventId + "/" + AssessmentType.ResumptionOfCare.ToString() + "'," +
                                    "PdfUrl: 'Oasis/OasisProfilePdf'," +
                                    "PdfData: { 'Id': '" + scheduleEvent.EventId + "','type': '" + AssessmentType.ResumptionOfCare.ToString() + "' }" +
                                "})\"><span class=\"img icon money\"></span></a>";
                        }
                        break;
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        onclick = string.Format("FollowUp.Load('{0}','{1}','FollowUp');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);

                        break;
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        onclick = string.Format("Recertification.Load('{0}','{1}','Recertification');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        if (scheduleEvent.Status == ((int)ScheduleStatus.OasisExported).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                        {
                            oasisProfileUrl = "<a href=\"javascript:void(0)\" onclick=\"" +
                                "Acore.OpenPrintView({ " +
                                    "Url: 'Oasis/Profile/" + scheduleEvent.EventId + "/" + AssessmentType.Recertification.ToString() + "'," +
                                    "PdfUrl: 'Oasis/OasisProfilePdf'," +
                                    "PdfData: { 'Id': '" + scheduleEvent.EventId + "','type': '" + AssessmentType.Recertification.ToString() + "' }" +
                                "})\"><span class=\"img icon money\"></span></a>";
                        }
                        break;
                    case DisciplineTasks.SNAssessmentRecert:
                    case DisciplineTasks.NonOASISRecertification:
                        onclick = string.Format("NonOasisRecertification.Load('{0}','{1}','NonOasisRecertification');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                        onclick = string.Format("TransferInPatientNotDischarged.Load('{0}','{1}','TransferInPatientNotDischarged');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);

                        break;
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.OASISCTransferDischargePT:
                        onclick = string.Format("TransferInPatientDischarged.Load('{0}','{1}','TransferInPatientDischarged');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);

                        break;
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        onclick = string.Format("DischargeFromAgencyDeath.Load('{0}','{1}','DischargeFromAgencyDeath');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);

                        break;
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.OASISCDischargeST:
                        onclick = string.Format("DischargeFromAgency.Load('{0}','{1}','DischargeFromAgency');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.NonOASISDischarge:
                        onclick = string.Format("NonOasisDischarge.Load('{0}','{1}','NonOasisDischarge');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        onclick = string.Format("UserInterface.ShowEditOrder('{0}','{1}');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        // onclick = string.Format("UserInterface.ShowEditOrder('{0}','{1}');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("{0}", scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HCFA485:
                        onclick = string.Format("UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OTPlanOfCare:
                        onclick = string.Format("UserInterface.ShowEditOTPlanOfCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PTPlanOfCare:
                        onclick = string.Format("UserInterface.ShowEditPTPlanOfCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.STPlanOfCare:
                        onclick = string.Format("UserInterface.ShowEditSTPlanOfCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        onclick = string.Format("UserInterface.ShowEditPlanofCareStandAlone('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.NonOasisHCFA485:
                        onclick = string.Format("UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.CommunicationNote:
                        onclick = string.Format("Patient.LoadEditCommunicationNote('{0}','{1}');", scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.DischargeSummary:
                        onclick = string.Format("dischargeSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PTDischargeSummary:
                        onclick = string.Format("PTDischargeSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OTDischargeSummary:
                        onclick = string.Format("OTDischargeSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.STDischargeSummary:
                        onclick = string.Format("STDischargeSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.SNInsulinNoon:
                    case DisciplineTasks.SNInsulinHS:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.LVNVisit:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.SNWoundCare:
                        onclick = string.Format("snVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SNVPsychNurse:
                        onclick = string.Format("snPsychVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SNPsychAssessment:
                        onclick = string.Format("snPsychAssessment.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.Labs:
                        onclick = string.Format("snLabs.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.NutritionalAssessment:
                        onclick = string.Format("nutritionalAssessment.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.InitialSummaryOfCare:
                        onclick = string.Format("ISOC.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        onclick = string.Format("snDiabeticDailyVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SNPediatricVisit:
                        onclick = string.Format("snPediatricVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SNPediatricAssessment:
                        onclick = string.Format("snPediatricAssessment.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.TenDaySummary:
                    case DisciplineTasks.ThirtyDaySummary:
                    case DisciplineTasks.SixtyDaySummary:
                        onclick = string.Format("sixtyDaySummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.TransferSummary:
                        onclick = string.Format("Schedule.loadTransferSummary('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.CoordinationOfCare:
                        onclick = string.Format("Schedule.loadCoordinationOfCare('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        onclick = string.Format("lvnSupVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        onclick = string.Format("hhaSupVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        onclick = string.Format("hhaCarePlan.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.UAPWoundCareVisit:
                    case DisciplineTasks.UAPInsulinPrepAdminVisit:
                    case DisciplineTasks.PASVisit:
                    case DisciplineTasks.PASTravel:
                    case DisciplineTasks.PASCarePlan:
                    case DisciplineTasks.HHAideVisit:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTAVisit:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTReassessment:
                    case DisciplineTasks.PTMaintenance:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.COTAVisit:
                    case DisciplineTasks.OTMaintenance:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTReassessment:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STReassessment:
                    case DisciplineTasks.STMaintenance:
                    case DisciplineTasks.STDischarge:
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.HomeMakerNote:
                    case DisciplineTasks.PTSupervisoryVisit:
                    case DisciplineTasks.OTSupervisoryVisit:
                        onclick = string.Format("Visit.{3}.Load('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.IncidentAccidentReport:
                        onclick = string.Format("UserInterface.ShowEditIncident('{0}');", scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.InfectionReport:
                        onclick = string.Format("UserInterface.ShowEditInfection('{0}');", scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.MSWProgressNote:
                        onclick = string.Format("Schedule.loadMSWProgressNote('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.MSWVisit:
                        onclick = string.Format("Schedule.loadMSWVisit('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.DriverOrTransportationNote:
                        onclick = string.Format("transportationLog.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                }
                scheduleEvent.OnClick = onclick;

                int permission = 0;
                scheduleEvent.PrintUrl = printUrl;
                //Document was auto generated by axxess
                if (scheduleEvent.UserName.IsEqual("Axxess"))
                {
                    return;
                }
                string restoreUrl = string.Format("<a onclick=\"Schedule.RestoreMissedVisit('{0}','{1}','{2}');\">Restore</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                string detailUrl = string.Format("<a onclick=\"Schedule.GetTaskDetails('{0}','{1}','{2}')\">Details</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                string deleteUrl = string.Format("<a onclick=\"Schedule.Delete('{0}','{1}','{2}')\" >Delete</a>", scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.EventId);
                string reassignUrl = string.Format("<a onclick=\"Schedule.ReAssign($(this),'{0}','{1}','{2}')\" class=\"reassign\">Reassign</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                //Frozen agencies and Medicare Eligibility Report's do not have any actions
                if (!Current.IsAgencyFrozen && scheduleEvent.DisciplineTask != (int)DisciplineTasks.MedicareEligibilityReport
                    && !Current.IfOnlyRole(AgencyRoles.Auditor) && ((Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA) || !(Current.IsClinicianOrHHA && Current.UserId != scheduleEvent.UserId)))
                {
                    bool isCompleted = scheduleEvent.IsCompleted();
                    bool isCompletelyFinished = scheduleEvent.IsCompletelyFinished();
                    bool isInQA = scheduleEvent.IsInQA();
                    if (scheduleEvent.IsOrphaned)
                    {
                        scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                        scheduleEvent.OnClick = string.Empty;
                    }
                    if (Current.HasRight(Permissions.EditTaskDetails) && !scheduleEvent.IsMissedVisit)
                    {
                        scheduleEvent.ActionUrl = detailUrl;
                        permission = (int)ScheduledActivityPermissions.EditTaskDetails;
                    }
                    if (Current.HasRight(Permissions.DeleteTasks) && !isInQA)
                    {
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + deleteUrl;
                        else scheduleEvent.ActionUrl = deleteUrl;
                        permission |= (int)ScheduledActivityPermissions.DeleteTasks;
                    }
                    if (!scheduleEvent.IsMissedVisit)
                    {
                        if (addReassignLink && Current.HasRight(Permissions.ScheduleVisits) && !isCompleted)
                        {
                            if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + reassignUrl;
                            else scheduleEvent.ActionUrl = reassignUrl;
                            permission |= (int)ScheduledActivityPermissions.ReassignTasks;
                        }
                        if (isCompletelyFinished && Current.HasRight(Permissions.ReopenDocuments))
                        {
                            reopenUrl = string.Format("<a onclick=\"if(confirm('Are you sure you would like to reopen this task?')){{Schedule.ReOpen('{0}','{1}','{2}');{3}}}\" class=\"reopen\">Reopen Task</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, onclick);
                            if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + reopenUrl;
                            else scheduleEvent.ActionUrl = reopenUrl;
                            permission |= (int)ScheduledActivityPermissions.ReopenDocuments;
                        }
                    }
                    else
                    {
                        if (scheduleEvent.Status == ((int)ScheduleStatus.NoteMissedVisitComplete).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.NoteMissedVisitPending).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.NoteMissedVisit).ToString())
                        {
                            scheduleEvent.Url = string.Format("<a onclick=\"Schedule.MissedVisitPopup($(this), '{0}');\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        }
                        else
                        {
                            scheduleEvent.Url = string.Format("<a onclick=\"UserInterface.ShowMissedVisitEdit('{0}');\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        }
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + restoreUrl;
                        else scheduleEvent.ActionUrl = restoreUrl;
                    }
                }
                else
                {
                    scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                    scheduleEvent.OnClick = string.Empty;
                    scheduleEvent.Status = string.Empty;
                    scheduleEvent.ActionUrl = string.Empty;
                    scheduleEvent.EpisodeNotes = string.Empty;
                    if (Current.IfOnlyRole(AgencyRoles.Auditor))
                    {
                        scheduleEvent.MissedVisitComments = string.Empty;
                        scheduleEvent.ReturnReason = string.Empty;
                        scheduleEvent.Comments = string.Empty;
                    }
                    //If the user is none of these roles they cannot see the print document
                    if (!Current.User.Session.AgencyRoles.IsInRoles(
                        AgencyRoles.Administrator,
                        AgencyRoles.Auditor,
                        AgencyRoles.OfficeManager,
                        AgencyRoles.CaseManager,
                        AgencyRoles.Biller,
                        AgencyRoles.Clerk,
                        AgencyRoles.Scheduler,
                        AgencyRoles.QA,
                        AgencyRoles.DoN,
                        AgencyRoles.Nurse,
                        AgencyRoles.PhysicalTherapist,
                        AgencyRoles.OccupationalTherapist,
                        AgencyRoles.SpeechTherapist,
                        AgencyRoles.HHA,
                        AgencyRoles.MedicalSocialWorker))
                    {
                        scheduleEvent.PrintUrl = string.Empty;
                    }
                }
                scheduleEvent.ActionPermission = permission;
                if (Current.HasRight(Permissions.ViewHHRGCalculations) && !Current.IfOnlyRole(AgencyRoles.Auditor))
                {
                    scheduleEvent.OasisProfileUrl = oasisProfileUrl;
                }
            }
        }

        public static string Print(Guid episodeId, Guid patientId, Guid eventId, DisciplineTasks task, int status, bool usePrintIcon)
        {
            var scheduleEvent = new ScheduleEvent
            {
                EpisodeId = episodeId,
                PatientId = patientId,
                EventId = eventId,
                DisciplineTask = (int)task,
                Status = status.ToString()
            };

            return Print(scheduleEvent, usePrintIcon);
        }

        public static string Print(ScheduleEvent scheduleEvent, bool usePrintIcon)
        {
            string printUrl = string.Empty;
            string linkText = scheduleEvent.DisciplineTaskName;
            if (usePrintIcon) linkText = "<span class=\"img icon print\"></span>";
            if (scheduleEvent.IsMissedVisit)
            {
                printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                    "Url: '/MissedVisit/View/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                    "PdfUrl: 'Schedule/MissedVisitPdf'," +
                    "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                    (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowMissedVisitEdit('" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
            }
            else
            {
                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;
                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/StartOfCare/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','StartOfCare','Return','startofcare');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { StartOfCare.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','StartOfCare'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "','patientId': '" + scheduleEvent.PatientId + "','episodeId': '" + scheduleEvent.EpisodeId + "','assessmentType': 'StartOfCare' }); } }, {" +
                                    "Text: 'OASIS Export File'," +
                                    "Click: function() { U.GetAttachment('Oasis/GenerateExportFile', { 'assessmentId': '" + scheduleEvent.EventId + "','assessmentType': 'StartOfCare' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','StartOfCare','Approve','startofcare');  } } ]"
                            : "") + "}); $('#print-controls li:eq(1)').addClass('red');\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.NonOASISStartofCare:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/NonOasisStartofCare/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisStartOfCare','Return','nonoasisstartofcare');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { NonOasisStartOfCare.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','NonOasisStartOfCare'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisStartOfCare','Approve','nonoasisstartofcare');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/ResumptionOfCare/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','ResumptionOfCare','Return','resumptionofcare'); }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { ResumptionOfCare.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','ResumptionOfCare'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "','patientId': '" + scheduleEvent.PatientId + "','episodeId': '" + scheduleEvent.EpisodeId + "','assessmentType': 'ResumptionOfCare' }); } }, {" +
                                    "Text: 'OASIS Export File'," +
                                    "Click: function() { U.GetAttachment('Oasis/GenerateExportFile', { 'assessmentId': '" + scheduleEvent.EventId + "','assessmentType': 'ResumptionOfCare' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','ResumptionOfCare','Approve','resumptionofcare');  } } ]"
                            : "") + "}); $('#print-controls li:eq(1)').addClass('red');\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/FollowUp/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','FollowUp','Return','followup');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { FollowUp.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','FollowUp'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "','patientId': '" + scheduleEvent.PatientId + "','episodeId': '" + scheduleEvent.EpisodeId + "','assessmentType': 'FollowUp' }); } }, {" +
                                    "Text: 'OASIS Export File'," +
                                    "Click: function() { U.GetAttachment('Oasis/GenerateExportFile', { 'assessmentId': '" + scheduleEvent.EventId + "','assessmentType': 'FollowUp' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','FollowUp','Approve','followup');  } } ]"
                            : "") + "}); $('#print-controls li:eq(1)').addClass('red');\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Recertification/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','Recertification','Return','recertification');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Recertification.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','Recertification'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "','patientId': '" + scheduleEvent.PatientId + "','episodeId': '" + scheduleEvent.EpisodeId + "','assessmentType': 'Recertification' }); } }, {" +
                                    "Text: 'OASIS Export File'," +
                                    "Click: function() { U.GetAttachment('Oasis/GenerateExportFile', { 'assessmentId': '" + scheduleEvent.EventId + "','assessmentType': 'Recertification' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','Recertification','Approve','recertification');  } } ]"
                            : "") + "}); $('#print-controls li:eq(1)').addClass('red');\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessmentRecert:
                    case DisciplineTasks.NonOASISRecertification:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/NonOasisRecertification/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisRecertification','Return','nonoasisrecertification');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { NonOasisRecertification.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','NonOasisRecertification'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisRecertification','Approve','nonoasisrecertification');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/TransferInPatientNotDischarged/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientNotDischarged','Return','transfernotdischarge');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { TransferInPatientNotDischarged.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','TransferInPatientNotDischarged'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "','patientId': '" + scheduleEvent.PatientId + "','episodeId': '" + scheduleEvent.EpisodeId + "','assessmentType': 'TransferInPatientNotDischarged' }); } }, {" +
                                    "Text: 'OASIS Export File'," +
                                    "Click: function() { U.GetAttachment('Oasis/GenerateExportFile', { 'assessmentId': '" + scheduleEvent.EventId + "','assessmentType': 'TransferInPatientNotDischarged' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientNotDischarged','Approve','transfernotdischarge');  } } ]"
                            : "") + "}); $('#print-controls li:eq(1)').addClass('red');\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.OASISCTransferDischargePT:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/TransferInPatientDischarged/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientDischarged','Return','transferinpatientdischarged');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { TransferInPatientDischarged.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','TransferInPatientDischarged'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "','patientId': '" + scheduleEvent.PatientId + "','episodeId': '" + scheduleEvent.EpisodeId + "','assessmentType': 'TransferInPatientDischarged' }); } }, {" +
                                    "Text: 'OASIS Export File'," +
                                    "Click: function() { U.GetAttachment('Oasis/GenerateExportFile', { 'assessmentId': '" + scheduleEvent.EventId + "','assessmentType': 'TransferInPatientDischarged' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientDischarged','Approve','transferinpatientdischarged');  } } ]"
                            : "") + "}); $('#print-controls li:eq(1)').addClass('red');\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/DischargeFromAgencyDeath/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgencyDeath','Return','DischargeFromAgencyDeath'); }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { DischargeFromAgencyDeath.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','DischargeFromAgencyDeath'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "','patientId': '" + scheduleEvent.PatientId + "','episodeId': '" + scheduleEvent.EpisodeId + "','assessmentType': 'DischargeFromAgencyDeath' }); } }, {" +
                                    "Text: 'OASIS Export File'," +
                                    "Click: function() { U.GetAttachment('Oasis/GenerateExportFile', { 'assessmentId': '" + scheduleEvent.EventId + "','assessmentType': 'DischargeFromAgencyDeath' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgencyDeath','Approve','DischargeFromAgencyDeath'); } } ]"
                            : "") + "}); $('#print-controls li:eq(1)').addClass('red');\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.OASISCDischargeST:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/DischargeFromAgency/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgency','Return','discharge');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { DischargeFromAgency.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','DischargeFromAgency'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'OASIS Scrubber'," +
                                    "Click: function() { U.GetAttachment('Oasis/AuditPdf', { 'id': '" + scheduleEvent.EventId + "','patientId': '" + scheduleEvent.PatientId + "','episodeId': '" + scheduleEvent.EpisodeId + "','assessmentType': 'DischargeFromAgency' }); } }, {" +
                                    "Text: 'OASIS Export File'," +
                                    "Click: function() { U.GetAttachment('Oasis/GenerateExportFile', { 'assessmentId': '" + scheduleEvent.EventId + "','assessmentType': 'DischargeFromAgency' }); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgency','Approve','discharge');  } } ]"
                            : "") + "}); $('#print-controls li:eq(1)').addClass('red');\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.NonOASISDischarge:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/NonOasisDischarge/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/Pdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisDischarge','Return','nonoasisdischarge');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { NonOasisDischarge.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','NonOasisDischarge'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisDischarge','Approve','nonoasisdischarge');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/FaceToFaceEncounter/View/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Patient/PhysicianFaceToFaceEncounterPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Order/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Patient/PhysicianOrderPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','eventId': '" + scheduleEvent.EventId + "','patientId': '" + scheduleEvent.PatientId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PhysicianOrder','Return');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditOrder('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PhysicianOrder','Approve');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/485/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/PlanOfCarePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCare','Return');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditPlanofCare('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCare','Approve');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/485/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/PlanOfCarePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCareStandAlone','Return');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditPlanofCareStandAlone('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCareStandAlone','Approve');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CommunicationNote:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/CommunicationNote/View/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Patient/CommunicationNotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.ProcessCommunicationNote('Return','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Patient.LoadEditCommunicationNote('" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.ProcessCommunicationNote('Approve','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.DischargeSummary:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/DischargeSummary/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/DischargeSummaryPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { dischargeSummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTDischargeSummary:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/PTDischargeSummary/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/PTDischargeSummaryPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { PTDischargeSummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTDischargeSummary:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/OTDischargeSummary/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/OTDischargeSummaryPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { OTDischargeSummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.STDischargeSummary:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/STDischargeSummary/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/STDischargeSummaryPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { STDischargeSummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.Labs:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Labs/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/LabsPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snLabs.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.NutritionalAssessment:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/NutritionalAssessment/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/NutritionalAssessmentPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { nutritionalAssessment.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.InitialSummaryOfCare:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/ISOC/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/InitialSummaryOfCarePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { ISOC.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.SNInsulinHS:
                    case DisciplineTasks.SNInsulinNoon:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.LVNVisit:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNWoundCare:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/SNVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/SNVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'View Plan of Care'," +
                                    "Click: function() { Schedule.GetPlanofCareUrl('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); } }, {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNVPsychNurse:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                         "Url: '/SNPsychVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                         "PdfUrl: 'Schedule/SNPsychVisitPdf'," +
                         "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                         (!usePrintIcon ?
                             ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                             ", Buttons: [ {" +
                                 "Text: 'View Plan of Care'," +
                                 "Click: function() { Schedule.GetPlanofCareUrl('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); } }, {" +
                                 "Text: 'Edit'," +
                                 "Click: function() { snPsychVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                 "Text: 'Approve'," +
                                 "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                         : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPsychAssessment:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                         "Url: '/SNPsychAssessment/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                         "PdfUrl: 'Schedule/SNPsychAssessmentPdf'," +
                         "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                         (!usePrintIcon ?
                             ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                             ", Buttons: [ {" +
                                 "Text: 'View Plan of Care'," +
                                 "Click: function() { Schedule.GetPlanofCareUrl('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); } }, {" +
                                 "Text: 'Edit'," +
                                 "Click: function() { snPsychAssessment.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                 "Text: 'Approve'," +
                                 "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                         : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/SNDiabeticDailyVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/SNDiabeticDailyVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snDiabeticDailyVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPediatricVisit:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/SNPediatricVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/SNPediatricVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snPediatricVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPediatricAssessment:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/SNPediatricAssessment/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/SNPediatricAssessmentPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snPediatricAssessment.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.TenDaySummary:
                    case DisciplineTasks.ThirtyDaySummary:
                    case DisciplineTasks.SixtyDaySummary:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/SixtyDaySummary/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/SixtyDaySummaryPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { sixtyDaySummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.TransferSummary:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/TransferSummary/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/TransferSummaryPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadTransferSummary('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "','TransferSummary'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CoordinationOfCare:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/CoordinationOfCare/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/CoordinationOfCarePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadCoordinationOfCare('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "','CoordinationOfCare'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/LVNSupervisoryVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/LVNSVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { lvnSupVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/HHAideSupervisoryVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/HHASVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { hhaSupVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/HHACarePlan/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/HHACarePlanPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { hhaCarePlan.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;

                    case DisciplineTasks.HomeMakerNote:
                    case DisciplineTasks.PASVisit:
                    case DisciplineTasks.PASTravel:
                    case DisciplineTasks.PASCarePlan:
                    case DisciplineTasks.UAPWoundCareVisit:
                    case DisciplineTasks.UAPInsulinPrepAdminVisit:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTAVisit:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTReassessment:
                    case DisciplineTasks.PTMaintenance:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTReassessment:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTMaintenance:
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.COTAVisit:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STReassessment:
                    case DisciplineTasks.STMaintenance:
                    case DisciplineTasks.STDischarge:
                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.PTSupervisoryVisit:
                    case DisciplineTasks.OTSupervisoryVisit:
                    case DisciplineTasks.STPlanOfCare:
                    case DisciplineTasks.OTPlanOfCare:
                    case DisciplineTasks.PTPlanOfCare:
                        printUrl = "<a onclick=\"Visit." + task.ToString() + ".Print('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', " + (!usePrintIcon).ToString().ToLower() + ")\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideVisit:
                        if (!usePrintIcon)
                        {
                            printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                           "Url: '/" + task.ToString() + "/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                           "PdfUrl: 'Schedule/" + task.ToString() + "Pdf'," +
                           "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                           (!usePrintIcon ?
                               ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                               ", Buttons: [ {" +
                                   "Text: 'View Care Plan'," +
                                   "Click: function() { Schedule.GetHHACarePlanUrlFromHHAVisit('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); } }, {" +
                                   "Text: 'Edit'," +
                                   "Click: function() { Visit.HHAideVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                   "Text: 'Approve'," +
                                   "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                           : "") + "})\">" + linkText + "</a>";
                        }
                        else
                        {
                            printUrl = "<a onclick=\"Visit." + task.ToString() + ".Print('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', " + (!usePrintIcon).ToString().ToLower() + ")\">" + linkText + "</a>";
                        }
                        break;
                    case DisciplineTasks.MSWProgressNote:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/MSWProgressNote/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/MSWProgressNotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadMSWProgressNote('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "','" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWVisit:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/MSWVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/MSWVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadMSWVisit('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "','" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.InfectionReport:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Infection/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Agency/InfectionReportPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { InfectionReport.ProcessInfection('Return','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditInfection('" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { InfectionReport.ProcessInfection('Approve','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.IncidentAccidentReport:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Incident/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Agency/IncidentReportPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { IncidentReport.ProcessIncident('Return','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditIncident('" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { IncidentReport.ProcessIncident('Approve','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MedicareEligibilityReport:
                        printUrl = "<a onclick=\"U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + scheduleEvent.PatientId + "','mcareEligibilityId': '" + scheduleEvent.EventId + "' });\">" + (usePrintIcon ? "<span class='img icon print'></span>" : linkText) + "</a>";
                        break;
                    case DisciplineTasks.DriverOrTransportationNote:
                        printUrl = "<a onclick=\"Acore.OpenPrintView({ " +
                            "Url: 'TransportationNote/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/TransportationNotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadTransportationNote('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "','" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                }
            }
            return printUrl;
        }

        public static string Download(ScheduleEvent scheduleEvent, bool usePrintIcon)
        {
            string printUrl = string.Empty;
            string linkText = scheduleEvent.DisciplineTaskName;
            if (usePrintIcon) linkText = "<span class=\"img icon print\"></span>";
            if (scheduleEvent.IsMissedVisit)
            {
                printUrl = "<a onclick=\"U.GetAttachment('Schedule/MissedVisitPdf', { 'patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
            }
            else
            {
                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;
                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.NonOASISStartofCare:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessmentRecert:
                    case DisciplineTasks.NonOASISRecertification:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/NonOasisRecertificationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.OASISCTransferDischargePT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.OASISCDischargeST:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.NonOASISDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/NonOasisDischargePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/PhysicianFaceToFaceEncounterPdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/PhysicianOrderPdf', {   'patientId': '" + scheduleEvent.PatientId + "', 'eventId':'" + scheduleEvent.EventId + "','episodeId':'"+scheduleEvent.EpisodeId+"' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/PlanOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/PlanOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CommunicationNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/CommunicationNotePdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "', 'episodeId': '" + scheduleEvent.EpisodeId +"' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.DischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/DischargeSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTDischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/PTDischargeSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTDischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/OTDischargeSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.STDischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/STDischargeSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.SNInsulinNoon:
                    case DisciplineTasks.SNInsulinHS:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.LVNVisit:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNWoundCare:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/SNVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNVPsychNurse:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/SNPsychVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPsychAssessment:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/SNPsychAssessmentPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.TenDaySummary:
                    case DisciplineTasks.ThirtyDaySummary:
                    case DisciplineTasks.SixtyDaySummary:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/SixtyDaySummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.InitialSummaryOfCare:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/InitialSummaryOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.TransferSummary:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/TransferSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CoordinationOfCare:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/CoordinationOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/LVNSVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/HHASVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HomeMakerNote:
                    case DisciplineTasks.HHAideVisit:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReassessment:
                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.PTSupervisoryVisit:
                    case DisciplineTasks.OTSupervisoryVisit:
                    case DisciplineTasks.STPlanOfCare:
                    case DisciplineTasks.OTPlanOfCare:
                    case DisciplineTasks.PTPlanOfCare:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/" + task.ToString() + "Pdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTAVisit:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/PTVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTMaintenance:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/PTEvaluationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.NutritionalAssessment:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/NutritionalAssessmentPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTReassessment:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/PTReassessmentPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTMaintenance:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/OTEvaluationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTReassessment:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/OTReassessmentPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.COTAVisit:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/OTVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STMaintenance:
                    case DisciplineTasks.STDischarge:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/STEvaluationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWAssessment:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/MSWEvaluationAssessmentPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PASVisit:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/PASVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PASTravel:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/PASTravelPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PASCarePlan:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/PASCarePlanPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/HHACarePlanPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWProgressNote:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/MSWProgressNotePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWVisit:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/MSWVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.InfectionReport:
                        printUrl = "<a onclick=\"U.GetAttachment('Agency/InfectionReportPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.IncidentAccidentReport:
                        printUrl = "<a onclick=\"U.GetAttachment('Agency/IncidentReportPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MedicareEligibilityReport:
                        printUrl = "<a onclick=\"U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + scheduleEvent.PatientId + "','mcareEligibilityId': '" + scheduleEvent.EventId + "' });\">" + (usePrintIcon ? "<span class='img icon print'></span>" : linkText) + "</a>";
                        break;
                    case DisciplineTasks.DriverOrTransportationNote:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/TransportationNotePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/SNDiabeticDailyVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPediatricVisit:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/SNPediatricVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNPediatricAssessment:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/SNPediatricAssessmentPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.UAPWoundCareVisit:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/UAPWoundCareVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.UAPInsulinPrepAdminVisit:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/UAPInsulinPrepAdminVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.Labs:
                        printUrl = "<a onclick=\"U.GetAttachment('Schedule/LabsPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "','patientId': '" + scheduleEvent.PatientId + "','eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                }
            }
            return printUrl;
        }
    }
}