﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Enums;

    public class AssessmentPrint
    {
        public AgencyLocation Location { get; set; }
        public IDictionary<string, Question> Data { get; set; }
        public DateTime AssessmentDate { get; set; }
        public DateTime VisitDate { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }
        public AssessmentType Type { get; set; }
        public string Discipline { get; set; }
        public int Version { get; set; }
        public string PrintViewJson { get; set; }
        public int AssessmentTypeNum { get { return (int)this.Type; } }
        public string GCode { get; set; }
    }
}
