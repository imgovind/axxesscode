﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.IO;
    using System.Collections.Generic;

    using Enums;
    using Services;
    using Extensions;
    using iTextExtension;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Ionic.Zip;
    using System.Net.Mime;

    public class ClaimFormDownloader : BaseExporter
    {
        private Guid agencyId;
        private Guid branchId;
        private int primaryInsurance;
        private List<Guid> managedClaimSelected;

       
        private readonly IBillingService billingService = Container.Resolve<IBillingService>();
        private readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        
        public ClaimFormDownloader(Guid agencyId, Guid branchId, int primaryInsurance, List<Guid> managedClaimSelected)
            : base()
        {
            this.agencyId = agencyId;
            this.branchId = branchId;
            this.primaryInsurance = primaryInsurance;
            this.managedClaimSelected = managedClaimSelected;
            this.FormatType = ExportFormatType.ZIP;
            this.FileName = string.Format("{0}.zip", DateTime.Now.Ticks);

        }

      
        protected override void WriteToArchive()
        {
            var bills = billingService.ManagedClaimToGenerate(agencyId, managedClaimSelected, branchId, primaryInsurance);
            if (bills != null && bills.Claims!=null)
            {
                var managedClaims = bills.Claims;
                if (managedClaims != null && managedClaims.Count > 0)
                {
                    managedClaims.ForEach(claim =>
                    {
                        ProcessDownload(claim);
                    });
                }
            }
        }

        private void ProcessDownload(Claim claim)
        {
            var insurance = agencyRepository.FindInsurance(agencyId, primaryInsurance);
            var invoiceType = insurance.InvoiceType;
            string zipEntryName = string.Format("{0}-{1}-{2}.pdf",
               invoiceType.ToEnum<InvoiceType>(InvoiceType.Invoice).GetDescription().Replace(" ", ""),
               claim.DisplayName.Replace(" ", "-").Replace("/", ""),
               claim.DateRange.Replace("/", "-").Replace(" ", ""));
            AxxessPdf pdf = ProcessDownloadHelper(claim, invoiceType);
            if (pdf != null)
            {
                var stream = pdf.GetStream();
                if (stream != null)
                {
                    stream.Position = 0;
                    this.zipFile.AddEntry(zipEntryName, stream.ToArray());
                    stream.Close();
                }
            }
        }

        private AxxessPdf ProcessDownloadHelper(Claim claim, int invoiceType)
        {
            AxxessPdf pdf = null;
            switch (invoiceType)
            {
                case (int)InvoiceType.UB:
                    {
                        pdf = new UB04Pdf(billingService.GetManagedUBOFourInfo(agencyId, claim.PatientId, claim.Id), billingService, agencyRepository);
                    }
                    break;

                case (int)InvoiceType.HCFA:
                    {

                        pdf = new HCFA1500Pdf(billingService.GetHCFA1500InfoForManagedClaim(agencyId, claim.PatientId, claim.Id), this.billingService, this.agencyRepository);
                    }
                    break;
            }
            return pdf;
        }

    
    }
}
