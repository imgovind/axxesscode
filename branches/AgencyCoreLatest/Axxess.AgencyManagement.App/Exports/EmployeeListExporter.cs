﻿using System;
using System.Linq;
using System.Collections.Generic;

using NPOI.HPSF;

using Axxess.Api;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using Axxess.AgencyManagement.Repositories;

namespace Axxess.AgencyManagement.App.Exports
{
    public class EmployeeListExporter : BaseExporter
    {
        #region Private Members and Constructor
        
        private Guid AgencyId;
        private string AgencyName;
        private Guid BranchCode;
        private int StatusId;
      

        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        public EmployeeListExporter(Guid agencyId, Guid branchCode, int statusId, String agencyName)
            : base()
        {
            AgencyId = agencyId;
            AgencyName = agencyName;
            BranchCode = branchCode;
            StatusId = statusId;
           

            FormatType = Axxess.AgencyManagement.App.Enums.ExportFormatType.XLS;
            this.FileName = "EmployeeList.xls";
        }

        #endregion

        #region Excel Output

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - EmployeeeList";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Employeee List");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var currencyStyle = base.workBook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var titleRow = sheet.CreateRow(0);

            var tCell1 = titleRow.CreateCell(0);
            tCell1.SetCellValue(AgencyName);

            var tCell2 = titleRow.CreateCell(1);
            tCell2.SetCellValue("Employeee List");

            var tCell3 = titleRow.CreateCell(2);
            tCell3.SetCellValue("Effective Date: " + DateTime.Today.ToZeroFilled());

            //var tCell4 = titleRow.CreateCell(3);
            //tCell4.SetCellValue("Date Range: " + StartDate.ToZeroFilled() + " - " + EndDate.ToZeroFilled());

            List<Dictionary<string, string>> data = reportAgent.EmployeeListReport(AgencyId, BranchCode, StatusId);

            if (data != null && data.Count > 0)
            {
                var rowIndex = 1;
                var colIndex = 0;
                var columnSize = 0;
                var dictionary = data.FirstOrDefault();

                var headerRow = sheet.CreateRow(rowIndex);
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    var hCell = headerRow.CreateCell(colIndex);
                    hCell.SetCellValue(kvp.Key);
                    colIndex++;
                }
                rowIndex++;
                columnSize = colIndex + 1;
                colIndex = 0;

                data.ForEach(list =>
                {
                    var dataRow = sheet.CreateRow(rowIndex);
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        if (kvp.Value != null)
                        {
                            string value = kvp.Value.Trim();
                            var cell = dataRow.CreateCell(colIndex);
                            if (value.IsNotNullOrEmpty())
                            {
                                
                                    cell.SetCellValue(value);
                                
                            }
                            if (value == "")
                            {
                                cell.SetCellValue(string.Empty);
                            }
                        }
                        colIndex++;
                    }
                    rowIndex++;
                    colIndex = 0;
                });
                workBook.FinishWritingToExcelSpreadsheet(columnSize);
            }
            else
            {
                var errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No Employee Information found!");
            }
        }
        #endregion
    }
}
