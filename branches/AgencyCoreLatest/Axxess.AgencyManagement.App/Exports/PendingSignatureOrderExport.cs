﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    public class PendingSignatureOrderExport : BaseExporter
    {
        private IList<Order> orders;
        private DateTime StartDate;
        private DateTime EndDate;
        public PendingSignatureOrderExport(IList<Order> orders, DateTime StartDate, DateTime EndDate)
            : base()
        {
            this.orders = orders;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Orders Pending Physician Signature";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("PendingSignatureOrder");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Orders Pending Physician Signature");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Order Number");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Type");
            headerRow.CreateCell(3).SetCellValue("Physician");
            headerRow.CreateCell(4).SetCellValue("Order Date");
            headerRow.CreateCell(5).SetCellValue("Sent Date");

            if (this.orders.Count > 0)
            {
                int i = 2;
                this.orders.ForEach(order =>
                {
                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(order.Number);
                    row.CreateCell(1).SetCellValue(order.PatientName);
                    row.CreateCell(2).SetCellValue(order.Text);
                    row.CreateCell(3).SetCellValue(order.PhysicianName);

                    DateTime createdDate = DateTime.Parse(order.CreatedDate);
                    if (createdDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(createdDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    DateTime sentDate = DateTime.Parse(order.SentDate);
                    if (sentDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(sentDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Orders Pending Physician Signature: {0}", orders.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(6);
        }
    }
}
