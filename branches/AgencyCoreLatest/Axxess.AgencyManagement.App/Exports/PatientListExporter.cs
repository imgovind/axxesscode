﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.HPSF;

using Axxess.Api;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.App.Services;
using Axxess.AgencyManagement.Repositories;
using Axxess.LookUp.Repositories;
using Axxess.Log.Repositories;
using Axxess.OasisC.Repositories;
using Axxess.AgencyManagement.Domain;
using Axxess.Membership.Repositories;

namespace Axxess.AgencyManagement.App.Exports
{
    class PatientListExporter : BaseExporter
    {
        #region Private Members and Constructor
        private Guid AgencyId;
        private string AgencyName;
        private Guid BranchId;
        private int StatusId;
        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyManagementDataProvider agencyManagementDataProvider = Container.Resolve<IAgencyManagementDataProvider>();
        private static readonly IReportService reportService = Container.Resolve<IReportService>();
        
        public PatientListExporter(Guid agencyId, int statusId, Guid branchId, String agencyName)
            : base()
        {
            AgencyId = agencyId;
            StatusId = statusId;
            AgencyName = agencyName;
            BranchId = branchId;

            FormatType = Axxess.AgencyManagement.App.Enums.ExportFormatType.XLS;
            this.FileName = "PatientList.xls";
        }
        #endregion

        #region Excel Output
        protected override void InitializeExcel()
        {
            base.InitializeExcel();
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PatientList";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Patient List");
            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");
            var currencyStyle = base.workBook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;
            var titleRow = sheet.CreateRow(0);
            var tCell1 = titleRow.CreateCell(0);
            tCell1.SetCellValue(AgencyName);
            var tCell2 = titleRow.CreateCell(2);
            tCell2.SetCellValue("Patient List Report");
            var tCell3= titleRow.CreateCell(4);
            tCell3.SetCellValue("Effective Date: " + DateTime.Today.ToZeroFilled());
            List<Dictionary<string, string>> data = reportAgent.PatientListReport(AgencyId, BranchId, StatusId);
            if (data != null && data.Count > 0)
            {
                var rowIndex = 1;
                var colIndex = 0;
                var columnSize = 0;
                var dictionary = data.FirstOrDefault();

                #region Excel Header
                var headerRow = sheet.CreateRow(rowIndex);
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    var hCell = headerRow.CreateCell(colIndex);
                    hCell.SetCellValue(kvp.Key);
                    colIndex++;
                }
                rowIndex++;
                columnSize = colIndex + 1;
                colIndex = 0;
                #endregion

                #region Excel Body
                data.ForEach(list =>
                {
                    var dataRow = sheet.CreateRow(rowIndex);
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        if (kvp.Value != null)
                        {
                            string value = kvp.Value.Trim();
                            var cell = dataRow.CreateCell(colIndex);
                            if (value.IsNotNullOrEmpty())
                            {
                                cell.SetCellValue(value);
                            }
                            if (value == "")
                            {
                                cell.SetCellValue(string.Empty);
                            }
                        }
                        colIndex++;
                    }
                    rowIndex++;
                    colIndex = 0;
                });
                #endregion
                
                workBook.FinishWritingToExcelSpreadsheet(columnSize);
            }
            else
            {
                var errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No Episode Information found!");
            }
        }
        #endregion
    }
}
