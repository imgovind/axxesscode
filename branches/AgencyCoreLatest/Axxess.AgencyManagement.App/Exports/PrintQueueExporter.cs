﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

    public class PrintQueueExporter : BaseExporter
    {
        private IList<PatientEpisodeEvent> patientEpisodeEvent;
        private string branchName;
        public PrintQueueExporter(string branchName, IList<PatientEpisodeEvent> patientEpisodeEvent)
            : base()
        {
            this.patientEpisodeEvent = patientEpisodeEvent;

            this.branchName = branchName;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Print Queue";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Print Queue");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue(branchName);
            titleRow.CreateCell(2).SetCellValue("Print Queue Items");
            titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Date");
            headerRow.CreateCell(2).SetCellValue("Task");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("User Name");

            if (this.patientEpisodeEvent.Count > 0)
            {
                int i = 2;
                this.patientEpisodeEvent.ForEach(u =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(u.PatientName);
                    DateTime eventDate = DateTime.Parse(u.EventDate);
                    if (eventDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(1);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(eventDate);
                    }
                    else
                    {
                        dataRow.CreateCell(1).SetCellValue(string.Empty);
                    }
                    dataRow.CreateCell(2).SetCellValue(u.TaskName);
                    dataRow.CreateCell(3).SetCellValue(u.Status);
                    dataRow.CreateCell(4).SetCellValue(u.UserName);
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Print Queue Total: {0}", patientEpisodeEvent.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(5);
        }
    }
}
