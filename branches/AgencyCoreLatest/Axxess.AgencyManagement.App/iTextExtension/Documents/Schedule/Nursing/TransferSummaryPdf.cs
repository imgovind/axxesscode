﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;
    class TransferSummaryPdf : VisitNotePdf {
        public TransferSummaryPdf(VisitNoteViewData data) : base(data, (data.Type == DisciplineTasks.TransferSummary.ToString() && data.Version > 1) ? (data.Version == 2? PdfDocs.TransferSummary2 : PdfDocs.TransferSummary3) : (data.Version == 2? PdfDocs.TransferSummary4 : PdfDocs.TransferSummary), 0) { }
        protected override float[] Margins(VisitNoteViewData data) {
            if (data.Type == DisciplineTasks.TransferSummary.ToString() && (data.Version == 2 || data.Version == 3))
            {
                return new float[] { 170, 28.3F, 60, 28.3F };
            }
            else
            {
                return new float[] { 120, 28.3F, 60, 28.3F };
            }
        }
        protected override List<Dictionary<string,string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            if (location == null) location = data.Agency.GetMainOffice();
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap[0].Add("visitdate", data != null && data.VisitDate != null && data.VisitDate.IsValidDate() ? data.VisitDate : "");
            fieldmap[0].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : "");
            fieldmap[0].Add("episode", data != null && data.StartDate != null && data.StartDate.IsValid() && data.EndDate != null && data.EndDate.IsValid() ? data.StartDate.ToShortDateString() + "-" + data.EndDate.ToShortDateString() : "");
            fieldmap[0].Add("physician", data != null && data.PhysicianDisplayName != null ? data.PhysicianDisplayName.ToTitleCase() : "");
            fieldmap[0].Add("transferdate", data != null && data.Questions != null && data.Questions.ContainsKey("TransferDate") && data.Questions["TransferDate"].Answer.IsNotNullOrEmpty() ? data.Questions["TransferDate"].Answer : "");
            fieldmap[0].Add("physicianphone", data != null && data.Questions != null && data.Questions.ContainsKey("PhysicianPhone") && data.Questions["PhysicianPhone"].Answer.IsNotNullOrEmpty() ? data.Questions["PhysicianPhone"].Answer : "");
            fieldmap[0].Add("emergencycontact", data != null && data.Questions != null && data.Questions.ContainsKey("EmergencyContact") && data.Questions["EmergencyContact"].Answer.IsNotNullOrEmpty() ? data.Questions["EmergencyContact"].Answer : "");
            fieldmap[0].Add("reportrecipient", data != null && data.Questions != null && data.Questions.ContainsKey("ReceivingName") && data.Questions["ReceivingName"].Answer.IsNotNullOrEmpty() ? data.Questions["ReceivingName"].Answer : "");
            fieldmap[0].Add("receivingdate", data != null && data.Questions != null && data.Questions.ContainsKey("ReceivingDate") && data.Questions["ReceivingDate"].Answer.IsNotNullOrEmpty() ? data.Questions["ReceivingDate"].Answer : "");
            fieldmap[0].Add("diagnosis1", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis") && data.Questions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis"].Answer : "");
            fieldmap[0].Add("diagnosis2", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis1") && data.Questions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis1"].Answer : "");
            fieldmap[0].Add("diagnosis3", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis2") && data.Questions["PrimaryDiagnosis2"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis2"].Answer : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            fieldmap[1].Add("doctype", data != null && data.Type.IsNotNullOrEmpty() && data.Type == DisciplineTasks.TransferSummary.ToString() ? "TRANSFER SUMMARY" : "COORDINATION OF CARE");
            return fieldmap;
        }
    }
}