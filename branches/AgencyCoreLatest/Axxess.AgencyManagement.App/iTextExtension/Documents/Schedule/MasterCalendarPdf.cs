﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class MasterCalendarPdf : AxxessPdf {
        public MasterCalendarPdf(MasterCalendarViewData data, bool showMissedVisits) {
            this.SetPageSize(AxxessDoc.Landscape);
            this.SetType(PdfDocs.MasterCalendar);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sans2);
            fonts[0].Size = 10F;
            fonts[1].Size = 9F;
            fonts[2].Size = 9F;
            fonts[2].Color = BaseColor.RED;
            this.SetFonts(fonts);
            var scheduleEvents = data.ScheduleEvents;
            var startWeekDay = (int)data.StartDate.DayOfWeek;
            var startDate = data.StartDate;
            var endDate = data.EndDate;
            var currentDate = data.StartDate.AddDays(-startWeekDay);
            AxxessTable[] calendar = new AxxessTable[] { new AxxessTable(new float[] { 70, 136, 136, 136, 136, 136, 136, 136 }, false) };
            for (int i = 1; i <= 10; i++) { 
                AxxessCell week = new AxxessCell(new float[] { 1, 2, 8, 2 });
                week.AddElement(new Chunk("Week " + i.ToString(), fonts[0]));
                calendar[0].AddCell(week);
                int addedDate = (i - 1) * 7;
                for (int j = 0; j <= 6; j++) {
                    var specificDate = currentDate.AddDays(j + addedDate);
                    AxxessCell day = new AxxessCell(new float[] { 1, 2, 8, 5 }, new float[] { .2F, .2F, .2F, .2F });
                    if (specificDate < startDate || specificDate > endDate) {
                        day.AddElement(new Chunk(""));
                        calendar[0].AddCell(day);
                    } else {
                        day.AddElement(new Chunk(String.Format("{0:MM/dd}", specificDate), fonts[0]));
                        var currentSchedules = scheduleEvents.FindAll(e => e.EventDate.ToDateTime() == specificDate);
                        if (currentSchedules.Count > 0) currentSchedules.ForEach(e => {
                            if (!e.IsMissedVisit && e.TimeIn.IsNotNullOrEmpty() && e.TimeOut.IsNotNullOrEmpty()) day.AddElement(new Paragraph(string.Format("{0} - {1}", e.TimeIn, e.TimeOut), fonts[1]));
                            var disciplineTask = e.DisciplineTask.ToEnum(DisciplineTasks.NoDiscipline);
                            string discipline = (e.IsMissedVisit ? "*" : "") + (disciplineTask != DisciplineTasks.NoDiscipline ? disciplineTask.GetCustomShortDescription() : string.Empty);
                            if ((e.IsMissedVisit && showMissedVisits && e.Discipline != Disciplines.Orders.ToString()) || (e.Discipline != Disciplines.Orders.ToString() && !e.IsMissedVisit)) day.AddElement(new Paragraph(string.Format("{0} - {1}", discipline, e.UserName), e.IsMissedVisit ? fonts[2] : fonts[1]));
                        });
                        calendar[0].AddCell(day);
                    }
                }
            }
            this.SetContent(calendar);
            this.SetMargins(new float[] { 125, 28, 28, 28 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", Current.AgencyName);
            fieldmap[0].Add("mr", data.PatientIdNumber);
            fieldmap[0].Add("patient", data.DisplayName);
            fieldmap[0].Add("freq", data.FrequencyList);
            fieldmap[0].Add("episode", String.Format("{0} – {1}", data.StartDate.ToZeroFilled(), data.EndDate.ToZeroFilled()));
            fieldmap[0].Add("soc", data.StartOfCareDate.ToZeroFilled());
            this.SetFields(fieldmap);
        }
    }
}