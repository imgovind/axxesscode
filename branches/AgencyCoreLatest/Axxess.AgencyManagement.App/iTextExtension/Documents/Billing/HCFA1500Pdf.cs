﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.App.Services;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.AgencyManagement.App.Enums;
    using Axxess.AgencyManagement.Enums;
    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.Repositories;
    class HCFA1500Pdf : AxxessPdf
    {
        protected readonly IBillingService billingService;
        protected readonly IAgencyRepository agencyRepository;
        private double totalCharge;
        private HCFA1500ViewData data;
        private AgencyLocation agencyLocation;
        private List<double> chargeItem = new List<double>();
        private List<int> userPages = new List<int>();
        private List<string> userLastNames = new List<string>();
        
        public HCFA1500Pdf(HCFA1500ViewData data, IBillingService billingService, IAgencyRepository agencyRepository)
        {
            this.data = data;
            this.billingService = billingService;
            this.agencyRepository = agencyRepository;
            agencyLocation = agencyRepository.FindLocation(data.Agency.Id, data.Claim.AgencyLocationId);
            this.SetType(PdfDocs.HCFA1500);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sans);
            fonts[0].Size = 12F;
            fonts[1].Size = 9F;
            this.SetFonts(fonts);
            this.SetContent(this.BuildContent(data));
            float[] margins = new float[] { 522, 29, 130, 29 };
            this.SetMargins(margins);
            this.SetFields(this.BuildFieldMap(data));

        }
        protected virtual List<Dictionary<String, String>> BuildFieldMap(HCFA1500ViewData data)
        {
            var fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<string, string>() { });
            //fieldmap.Add(new Dictionary<string, string>() { });
            if (data.Claim.PayorType == (int)PayerTypes.MedicareHMO || data.Claim.PayorType == (int)PayerTypes.MedicareTraditional) fieldmap[0].Add("1-medicare", "X");
            else if (data.Claim.PayorType == (int)PayerTypes.MedicaidHMO || data.Claim.PayorType == (int)PayerTypes.MedicaidTraditional) fieldmap[0].Add("1-medicaid", "X");
            else fieldmap[0].Add("1-group", "X");
            if (data.Claim != null)
            {
                var insuranceProvider = agencyRepository.GetInsurance(data.Claim.PrimaryInsuranceId, data.Agency.Id);
                if (insuranceProvider != null)
                {
                    fieldmap[0].Add("insurers-address", (insuranceProvider.Name.IsNotNullOrEmpty() ? insuranceProvider.Name.ToUpper() + "\n" : string.Empty) +
                        (insuranceProvider.AddressFirstRow.IsNotNullOrEmpty() ? insuranceProvider.AddressFirstRow.ToUpper() + "\n" : string.Empty) +
                        (insuranceProvider.AddressSecondRow.IsNotNullOrEmpty() ? insuranceProvider.AddressSecondRow.ToUpper() : string.Empty));
                }
            }
            fieldmap[0].Add("1a", data.Claim != null && data.Claim.HealthPlanId.IsNotNullOrEmpty() ? data.Claim.HealthPlanId : string.Empty);
            fieldmap[0].Add("2",
                (data.Claim != null && data.Claim.LastName.IsNotNullOrEmpty() ? data.Claim.LastName.ToUpper() + ", " : string.Empty) +
                (data.Claim != null && data.Claim.FirstName.IsNotNullOrEmpty() ? data.Claim.FirstName.ToUpper() : string.Empty) +
                (data.Claim != null && data.Claim.MiddleName.IsNotNullOrEmpty() ? data.Claim.MiddleName.ToUpper() : string.Empty));
            fieldmap[0].Add("3-month", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("MM") : string.Empty);
            fieldmap[0].Add("3-day", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("dd") : string.Empty);
            fieldmap[0].Add("3-year", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("yyyy") : string.Empty);
            fieldmap[0].Add("3-male", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("M") ? "X" : string.Empty);
            fieldmap[0].Add("3-female", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("F") ? "X" : string.Empty);
            fieldmap[0].Add("4",
                (data.Claim != null && data.Claim.LastName.IsNotNullOrEmpty() ? data.Claim.LastName.ToUpper() + ", " : string.Empty) +
                (data.Claim != null && data.Claim.FirstName.IsNotNullOrEmpty() ? data.Claim.FirstName.ToUpper() : string.Empty));
            fieldmap[0].Add("5", data.Claim != null && data.Claim.AddressLine1.IsNotNullOrEmpty() ? data.Claim.AddressLine1.ToUpper() + (data.Claim.AddressLine2.IsNotNullOrEmpty() ? " " + data.Claim.AddressLine2.ToUpper() : string.Empty) : string.Empty);
            fieldmap[0].Add("5-city", data.Claim != null && data.Claim.AddressCity.IsNotNullOrEmpty() ? data.Claim.AddressCity.ToUpper() : string.Empty);
            fieldmap[0].Add("5-state", data.Claim != null && data.Claim.AddressStateCode.IsNotNullOrEmpty() ? data.Claim.AddressStateCode.ToUpper() : string.Empty);
            fieldmap[0].Add("5-zip", data.Claim != null && data.Claim.AddressZipCode.IsNotNullOrEmpty() ? data.Claim.AddressZipCode.ToUpper() : string.Empty);
            fieldmap[0].Add("5-areacode", data.PatientTelephoneNum.IsNotNullOrEmpty() && data.PatientTelephoneNum.Length == 10 ? data.PatientTelephoneNum.Substring(0, 3) : string.Empty);
            fieldmap[0].Add("5-telephone", data.PatientTelephoneNum.IsNotNullOrEmpty() && data.PatientTelephoneNum.Length == 10 ? data.PatientTelephoneNum.Substring(3, 3) + "-" + data.PatientTelephoneNum.Substring(6, 4) : string.Empty);

            fieldmap[0].Add("6-" + (data.Claim != null && data.Claim.Relationship != null ? ((data.Claim.Relationship.Id <= 3 || data.Claim.Relationship.Id == 9) ? data.Claim.Relationship.Description.ToLowerCase() : "other") : "self"), "X");

            fieldmap[0].Add("7", data.Claim != null && data.Claim.AddressLine1.IsNotNullOrEmpty() ? data.Claim.AddressLine1.ToUpper() + (data.Claim.AddressLine2.IsNotNullOrEmpty() ? " " + data.Claim.AddressLine2.ToUpper() : string.Empty) : string.Empty);
            fieldmap[0].Add("7-city", data.Claim != null && data.Claim.AddressCity.IsNotNullOrEmpty() ? data.Claim.AddressCity.ToUpper() : string.Empty);
            fieldmap[0].Add("7-state", data.Claim != null && data.Claim.AddressStateCode.IsNotNullOrEmpty() ? data.Claim.AddressStateCode.ToUpper() : string.Empty);
            fieldmap[0].Add("7-zip", data.Claim != null && data.Claim.AddressZipCode.IsNotNullOrEmpty() ? data.Claim.AddressZipCode.ToUpper() : string.Empty);
            fieldmap[0].Add("7-areacode", data.PatientTelephoneNum.IsNotNullOrEmpty() && data.PatientTelephoneNum.Length == 10 ? data.PatientTelephoneNum.Substring(0, 3) : string.Empty);
            fieldmap[0].Add("7-telephone", data.PatientTelephoneNum.IsNotNullOrEmpty() && data.PatientTelephoneNum.Length == 10 ? data.PatientTelephoneNum.Substring(3, 3) + "-" + data.PatientTelephoneNum.Substring(6, 4) : string.Empty);
            if (data.PatientMaritalStatus.IsNotNullOrEmpty()) switch (data.PatientMaritalStatus)
                {
                    case "Single": fieldmap[0].Add("8-single", "X"); break;
                    case "Married": fieldmap[0].Add("8-married", "X"); break;
                    default: fieldmap[0].Add("8-other", "X"); break;
                }
            if (data.Claim != null && data.Claim.OtherProviderId.IsNotNullOrEmpty())
            {
                fieldmap[0].Add("9",
                    (data.Claim != null && data.Claim.LastName.IsNotNullOrEmpty() ? data.Claim.LastName.ToUpper() + ", " : string.Empty) +
                    (data.Claim != null && data.Claim.FirstName.IsNotNullOrEmpty() ? data.Claim.FirstName.ToUpper() : string.Empty));
                fieldmap[0].Add("9a", data.Claim.OtherProviderId);
                fieldmap[0].Add("9b-month", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("MM") : string.Empty);
                fieldmap[0].Add("9b-day", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("dd") : string.Empty);
                fieldmap[0].Add("9b-year", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("yyyy") : string.Empty);
                fieldmap[0].Add("9b-male", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("M") ? "X" : string.Empty);
                fieldmap[0].Add("9b-female", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("F") ? "X" : string.Empty);
            }
            fieldmap[0].Add("11", data.Claim.GroupId);
            fieldmap[0].Add("11a-month", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("MM") : string.Empty);
            fieldmap[0].Add("11a-day", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("dd") : string.Empty);
            fieldmap[0].Add("11a-year", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("yyyy") : string.Empty);
            fieldmap[0].Add("11a-male", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("M") ? "X" : string.Empty);
            fieldmap[0].Add("11a-female", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("F") ? "X" : string.Empty);
            fieldmap[0].Add("11c", (data.Claim != null && data.Claim.PayorName.IsNotNullOrEmpty() ? data.Claim.PayorName : string.Empty) + (data.Claim.IsHMO ? ("\n" + data.Claim.PayorAddressLine1 + "\n" + data.Claim.PayorAddressLine2) : string.Empty));
            fieldmap[0].Add("12-sign", "Signature on File");
            fieldmap[0].Add("13", "Signature on File");
            if (data.Claim != null && data.Claim.OtherProviderId.IsNotNullOrEmpty()) fieldmap[0].Add("11d-yes", "X");
            else if (data.Claim != null) fieldmap[0].Add("11d-no", "X");
            fieldmap[0].Add("17",
                (data.Claim != null && data.Claim.PhysicianLastName.IsNotNullOrEmpty() ? data.Claim.PhysicianLastName.ToUpper() + ", " : string.Empty) +
                (data.Claim != null && data.Claim.PhysicianFirstName.IsNotNullOrEmpty() ? data.Claim.PhysicianFirstName.ToUpper() : string.Empty));
            fieldmap[0].Add("17b", data.Claim != null && data.Claim.PhysicianNPI.IsNotNullOrEmpty() ? data.Claim.PhysicianNPI : string.Empty);
            if (data.Claim != null && data.Claim.DiagnosisCode.IsNotNullOrEmpty())
            {
                var diagnosis = XElement.Parse(data.Claim.DiagnosisCode);
                fieldmap[0].Add("21-1", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : string.Empty);
                fieldmap[0].Add("21-2", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : string.Empty);
                fieldmap[0].Add("21-3", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : string.Empty);
                fieldmap[0].Add("21-4", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : string.Empty);
            }
            fieldmap[0].Add("23", data.Claim != null && data.Claim.AuthorizationNumber.IsNotNullOrEmpty() ? data.Claim.AuthorizationNumber : string.Empty);

            fieldmap[0].Add("24", "Signature on File");

            fieldmap[0].Add("25", data.AgencyLocation != null && data.AgencyLocation.TaxId.IsNotNullOrEmpty() ? data.AgencyLocation.TaxId : string.Empty);
            //fieldmap[0].Add("25", data.Agency != null && data.Agency.TaxId.IsNotNullOrEmpty() ? data.Agency.TaxId : string.Empty);
            fieldmap[0].Add("25-ein", "X");
            fieldmap[0].Add("27-yes", "X");
            fieldmap[0].Add("33-areacode", data.AgencyLocation != null && data.AgencyLocation.PhoneWork.IsNotNullOrEmpty() ? data.AgencyLocation.PhoneWork.Substring(0, 3) : string.Empty);
            fieldmap[0].Add("33-telephone", data.AgencyLocation != null && data.AgencyLocation.PhoneWork.IsNotNullOrEmpty() ? data.AgencyLocation.PhoneWork.Substring(3, 3) + "-" + data.AgencyLocation.PhoneWork.Substring(6, 4) : string.Empty);
            fieldmap[0].Add("33",
                (data.AgencyLocation != null && data.AgencyLocation.Name.IsNotNullOrEmpty() ? data.AgencyLocation.Name.ToUpper() + "\n" : string.Empty) +
                //(data.Agency != null && data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToUpper() + "\n" : string.Empty) +
                (data.AgencyLocation != null && data.AgencyLocation.AddressFirstRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressFirstRow.ToUpper() + "\n" : string.Empty) +
                (data.AgencyLocation != null && data.AgencyLocation.AddressSecondRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressSecondRow.ToUpper() : string.Empty));
            if (agencyLocation != null && agencyLocation.IsLocationStandAlone)
            {
                fieldmap[0].Add("33a", agencyLocation.NationalProviderNumber.IsNotNullOrEmpty() ? agencyLocation.NationalProviderNumber : string.Empty);
            }
            else
            {
                fieldmap[0].Add("33a", data.Agency != null && data.Agency.NationalProviderNumber.IsNotNullOrEmpty() ? data.Agency.NationalProviderNumber : string.Empty);
            }
            if (data.Claim != null && data.Claim.HCFALocators.IsNotNullOrEmpty())
            {
                var locators = data.Claim.HCFALocators.ToLocatorDictionary();
                if (locators.ContainsKey("18Locator") && locators["18Locator"].Code1.IsNotNullOrEmpty())
                {
                    if (locators["18Locator"].Code1.IsValidDate())
                    {
                        var fromDate = locators["18Locator"].Code1.ToDateTime().Date;
                        fieldmap[0].Add("18-month", fromDate.Month.ToString());
                        fieldmap[0].Add("18-day", fromDate.Day.ToString());
                        fieldmap[0].Add("18-year", fromDate.Year.ToString());
                    }
                }
                if (locators.ContainsKey("18Locator") && locators["18Locator"].Code2.IsNotNullOrEmpty())
                {
                    if (locators["18Locator"].Code2.IsValidDate())
                    {
                        var fromDate = locators["18Locator"].Code2.ToDateTime().Date;
                        fieldmap[0].Add("18-2month", fromDate.Month.ToString());
                        fieldmap[0].Add("18-2day", fromDate.Day.ToString());
                        fieldmap[0].Add("18-2year", fromDate.Year.ToString());
                    }
                }
                if (locators.ContainsKey("26Locator") && locators["26Locator"].Code1.IsNotNullOrEmpty()) { if (locators["26Locator"].Code1.ToBoolean() == true) { fieldmap[0].Add("26", data.PatientIdNumber.ToString()); } }
                if (locators.ContainsKey("29Locator") && locators["29Locator"].Code1.IsNotNullOrEmpty()) { if (locators["29Locator"].Code1.ToBoolean() == true) { fieldmap[0].Add("29", data.PatientIdNumber.ToString()); } }
                if (locators.ContainsKey("30Locator") && locators["30Locator"].Code1.IsNotNullOrEmpty()) { if (locators["30Locator"].Code1.ToBoolean() == true) { fieldmap[0].Add("30", data.PatientIdNumber.ToString()); } }
                if (locators.ContainsKey("31LocatorType") && locators["31LocatorType"].Code1.IsNotNullOrEmpty())
                {
                    if (locators["31LocatorType"].Code1.Equals("0"))
                    {
                        fieldmap[0].Add("31-sign", "Signature on File");
                    }
                    else if (locators["31LocatorType"].Code1.Equals("1"))
                    {
                        if (locators.ContainsKey("31Locator") && locators["31Locator"].Code1.IsNotNullOrEmpty())
                        {
                            fieldmap[0].Add("31-sign", locators["31Locator"].Code1.ToString());
                            fieldmap[0].Add("31-date", data.Claim.Created.ToShortDateString());
                        }
                        else
                        {
                            fieldmap[0].Add("31-sign", string.Empty);
                        }
                    }
                }
                else
                {
                    fieldmap[0].Add("31-sign", "Signature on File");
                }
                if (locators.ContainsKey("32Locator") && locators["32Locator"].Code1.IsNotNullOrEmpty())
                {
                    if (locators["32Locator"].Code1.Equals("1"))
                    {
                        fieldmap[0].Add("32", (data.AgencyLocation != null && data.AgencyLocation.Name.IsNotNullOrEmpty() ? data.AgencyLocation.Name.ToUpper() + "\n" : string.Empty) +
                            //(data.Agency != null && data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToUpper() + "\n" : string.Empty) +
                            (data.AgencyLocation != null && data.AgencyLocation.AddressFirstRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressFirstRow.ToUpper() + "\n" : string.Empty) +
                            (data.AgencyLocation != null && data.AgencyLocation.AddressSecondRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressSecondRow.ToUpper() : string.Empty));
                        if (agencyLocation != null && agencyLocation.IsLocationStandAlone)
                        {
                            fieldmap[0].Add("32a", agencyLocation.NationalProviderNumber.IsNotNullOrEmpty() ? agencyLocation.NationalProviderNumber : string.Empty);
                        }
                        else
                        {
                            fieldmap[0].Add("32a", data.Agency != null && data.Agency.NationalProviderNumber.IsNotNullOrEmpty() ? data.Agency.NationalProviderNumber : string.Empty);
                        }
                    }
                    else if (locators["32Locator"].Code1.Equals("2"))
                    {
                        var facilityName = locators.ContainsKey("32LocatorName") ? (locators["32LocatorName"].Code1.IsNotNullOrEmpty() ? locators["32LocatorName"].Code1.ToString() : string.Empty) : string.Empty;
                        var facilityNPI = locators.ContainsKey("32LocatorNPI") ? (locators["32LocatorNPI"].Code1.IsNotNullOrEmpty() ? locators["32LocatorNPI"].Code1.ToString() : string.Empty) : string.Empty;
                        var addressLine1 = locators.ContainsKey("32LocatorAddressLine1") ? (locators["32LocatorAddressLine1"].Code1.IsNotNullOrEmpty() ? locators["32LocatorAddressLine1"].Code1.ToString() : string.Empty) : string.Empty;
                        var addressLine2 = locators.ContainsKey("32LocatorAddressLine2") ? (locators["32LocatorAddressLine2"].Code1.IsNotNullOrEmpty() ? locators["32LocatorAddressLine2"].Code1.ToString() : string.Empty) : string.Empty;
                        var addressCity = locators.ContainsKey("32LocatorAddressCity") ? (locators["32LocatorAddressCity"].Code1.IsNotNullOrEmpty() ? locators["32LocatorAddressCity"].Code1.ToString() : string.Empty) : string.Empty;
                        var addressState = locators.ContainsKey("32LocatorAddressState") ? (locators["32LocatorAddressState"].Code1.IsNotNullOrEmpty() ? locators["32LocatorAddressState"].Code1.ToString() : string.Empty) : string.Empty;
                        var addressZip = locators.ContainsKey("32LocatorAddressZip") ? (locators["32LocatorAddressZip"].Code1.IsNotNullOrEmpty() ? locators["32LocatorAddressZip"].Code1.ToString() : string.Empty) : string.Empty;
                        string addressFirstRow = default(string);
                        if (addressLine1.IsNotNullOrEmpty() && addressLine2.IsNotNullOrEmpty()) { addressFirstRow = string.Format("{0} {1}", addressLine1.Trim(), addressLine2.Trim()); }
                        if (addressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(addressLine2)) { addressFirstRow = addressLine1.Trim(); }
                        else { addressFirstRow = string.Empty; }
                        if (facilityName.IsNullOrEmpty()) { facilityName = string.Empty; }
                        var addressSecondRow = string.Format("{0} {1} {2}", addressCity.IsNotNullOrEmpty() ? addressCity.Trim() : string.Empty, addressState.Trim(), addressZip.Trim());
                        string fieldMapper32 = default(string);
                        if (addressSecondRow.IsNotNullOrEmpty()) { fieldMapper32 = addressSecondRow; }
                        if (addressFirstRow.IsNotNullOrEmpty()) { fieldMapper32 = addressFirstRow + "\n" + fieldMapper32; }
                        if (facilityName.IsNotNullOrEmpty()) { fieldMapper32 = facilityName + "\n" + fieldMapper32; }
                        string fieldMapper32a = default(string);
                        if (facilityNPI.IsNotNullOrEmpty()) { fieldMapper32a = facilityNPI; } else { fieldMapper32a = string.Empty; }
                        if (fieldMapper32 == null) { fieldMapper32 = string.Empty; }
                        fieldmap[0].Add("32", fieldMapper32);
                        fieldmap[0].Add("32-areacode", string.Empty);
                        fieldmap[0].Add("32-telephone", string.Empty);
                        fieldmap[0].Add("32a", fieldMapper32a);
                    }
                }
                if (locators.ContainsKey("33Locatorb") && locators["33Locatorb"] != null) { fieldmap[0].Add("33b", locators["33Locatorb"].Code1); }
                if (locators.ContainsKey("10Locatora")) { if (locators["10Locatora"].Code1 == "Yes") { fieldmap[0].Add("10a-yes", "X"); } if (locators["10Locatora"].Code1 == "No") { fieldmap[0].Add("10a-no", "X"); } }
                if (locators.ContainsKey("10Locatorb")) { if (locators["10Locatorb"].Code1 == "Yes") { fieldmap[0].Add("10b-yes", "X"); } if (locators["10Locatorb"].Code1 == "No") { fieldmap[0].Add("10b-no", "X"); } if (locators["10Locatorb"].Code2 != null) { fieldmap[0].Add("10b-state", locators["10Locatorb"].Code2); } }
                if (locators.ContainsKey("10Locatorc")) { if (locators["10Locatorc"].Code1 == "Yes") { fieldmap[0].Add("10c-yes", "X"); } if (locators["10Locatorc"].Code1 == "No") { fieldmap[0].Add("10c-no", "X"); } }
            }
            else
            {
                fieldmap[0].Add("31-sign", "Signature on File");
            }
            if (RenderNeedProviderId(data))
            {
                int fieldmapCount = 0;
                for (int i = 0; i < userLastNames.Count; i++)
                {
                    for (int j = 0; j < userPages[i]; j++)
                    {
                        if (fieldmapCount != 0)
                        {
                            var fld = new Dictionary<string, string>(fieldmap[0]);
                            fld.Remove("31-sign");
                            fld.Add("31-sign", userLastNames[i]);
                            fieldmap.Add(fld);
                        }
                        else
                        {
                            fieldmap[0].Remove("31-sign");
                            fieldmap[0].Add("31-sign", userLastNames[i]);
                        }
                        fieldmapCount++;
                    }
                }
            }
            return fieldmap;
        }

        private bool RenderNeedProviderId(HCFA1500ViewData data)
        {
            bool useProviderID = false;
            if (data.Claim.HCFALocators.IsNotNullOrEmpty())
            {
                var locators = data.Claim.HCFALocators.ToObject<List<Locator>>();
                if (locators.IsNotNullOrEmpty())
                {
                    var locator24 = locators.Find(l => l.LocatorId.Equals("24Locator"));
                    if (locator24 != null)
                        useProviderID = locator24.Customized;
                }
            }
            return useProviderID;
        }

        protected virtual IElement[] BuildContent(HCFA1500ViewData data)
        {
            var content = new AxxessTable[] { new AxxessTable(new float[] { 19, 21, 21, 21, 23, 21, 23, 22, 51, 26, 21, 21, 23, 36, 44, 19, 28, 15, 22, 86 }, false) };
            var font = AxxessPdf.sans;
            font.Size = 10;
            int count = 0;
            int supplyNo = 0;
            float[] padding = new float[] { 7, 1, 1, 1 }, borders = new float[] { 0, 0, 0, 0 };
            var supplies = data.Claim != null && data.Claim.Supply.IsNotNullOrEmpty() ? (data.Claim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable && !s.IsDeprecated).ToList() ?? new List<Supply>()) : new List<Supply>();
            totalCharge = 0;
            if (supplies != null && supplies.Count > 0)
            {
                foreach (var supply in supplies)
                {
                    chargeItem.Add(supply.TotalCost);
                    content[0].AddCell(supply.DateForEdit != DateTime.MinValue ? supply.DateForEdit.ToString("MM") : string.Empty, font, padding, borders);
                    content[0].AddCell(supply.DateForEdit != DateTime.MinValue ? supply.DateForEdit.ToString("dd") : string.Empty, font, padding, borders);
                    content[0].AddCell(supply.DateForEdit != DateTime.MinValue ? supply.DateForEdit.ToString("yy") : string.Empty, font, padding, borders);
                    content[0].AddCell(supply.DateForEdit != DateTime.MinValue ? supply.DateForEdit.ToString("MM") : string.Empty, font, padding, borders);
                    content[0].AddCell(supply.DateForEdit != DateTime.MinValue ? supply.DateForEdit.ToString("dd") : string.Empty, font, padding, borders);
                    content[0].AddCell(supply.DateForEdit != DateTime.MinValue ? supply.DateForEdit.ToString("yy") : string.Empty, font, padding, borders);
                    content[0].AddCell("12", font, padding, borders);
                    content[0].AddCell("1", font, padding, borders);
                    content[0].AddCell(supply.Code != null && supply.Code.IsNotNullOrEmpty() ? supply.Code : string.Empty, font, padding, borders);
                    content[0].AddCell(supply.Modifier, font, padding, borders);
                    content[0].AddCell(string.Empty, font, padding, borders);
                    content[0].AddCell(string.Empty, font, padding, borders);
                    content[0].AddCell(string.Empty, font, padding, borders);
                    content[0].AddCell("1", font, padding, borders);
                    var visitChargeArray = supply.TotalCost.ToString().Split(new string[] { "." }, StringSplitOptions.None);
                    content[0].AddCell(visitChargeArray.Length > 0 ? visitChargeArray[0] : "0", font, "Right", padding, borders);
                    content[0].AddCell(visitChargeArray.Length > 1 ? visitChargeArray[1].PadRight(2, '0') : "00", font, padding, borders);
                    content[0].AddCell(supply.Quantity > 0 ? supply.Quantity.ToString() : string.Empty, font, padding, borders);
                    content[0].AddCell(string.Empty, font, padding, borders);
                    content[0].AddCell(string.Empty, font, padding, borders);
                    if (agencyLocation != null && agencyLocation.IsLocationStandAlone)
                    {
                        content[0].AddCell(agencyLocation.NationalProviderNumber.IsNotNullOrEmpty() ? agencyLocation.NationalProviderNumber : string.Empty, font, padding, borders);
                    }
                    else
                    {
                        content[0].AddCell(data.Agency != null && data.Agency.NationalProviderNumber.IsNotNullOrEmpty() ? data.Agency.NationalProviderNumber : string.Empty, font, padding, borders);
                    }
                    count++;
                }
                supplyNo=count;
            }
            bool displayProviderId = RenderNeedProviderId(data);
            var schedules = data.Claim != null && data.Claim.VerifiedVisit.IsNotNullOrEmpty() ? data.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValidDate()).OrderBy(s => s.VisitDate.ToDateTime().Date).ToList() : new List<ScheduleEvent>();
            if (schedules != null && schedules.Count > 0)
            {
                var visits = new List<BillSchedule>();
                if (displayProviderId)
                {
                    schedules.GroupBy(s => s.UserId).Select(s => s.ToList()).ForEach(groupSchedules =>
                        {
                            var vis = billingService.BillableVisitSummary(data.Claim.AgencyLocationId, groupSchedules, data.Claim.ClaimType, data.Claim.ChargeRates, true);
                            if (vis != null && vis.Count > 0)
                            {
                                visits.AddRange(vis);
                            }
                        });
                }
                else
                {
                    visits = billingService.BillableVisitSummary(data.Claim.AgencyLocationId, schedules, data.Claim.ClaimType, data.Claim.ChargeRates, true);
                }
                if (visits != null && visits.Count > 0)
                {
                    if (displayProviderId)
                    {
                        SetDiffLocatorContent(true);
                        var userIds = visits.Select(v => v.UserId).Distinct().ToList();
                        var users = billingService.GetUsersClinicianProviderID(data.Agency.Id, userIds);
                        
                        int i = 0;
                        List<List<BillSchedule>> groupVisits = visits.GroupBy(v => v.UserId).Select(vi => vi.ToList()).ToList();
                        visits.GroupBy(v => v.UserId).ForEach(vis =>
                        {
                            int userRow = 0;
                            string lastName = "";
                            foreach (var visit in vis)
                            {
                                if (visit.UserId.IsNotEmpty())
                                {
                                    chargeItem.Add(visit.Charge);
                                    content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM") : string.Empty, font, padding, borders);
                                    content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("dd") : string.Empty, font, padding, borders);
                                    content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("yy") : string.Empty, font, padding, borders);
                                    content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM") : string.Empty, font, padding, borders);
                                    content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("dd") : string.Empty, font, padding, borders);
                                    content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("yy") : string.Empty, font, padding, borders);
                                    content[0].AddCell("12", font, padding, borders);
                                    content[0].AddCell("1", font, padding, borders);
                                    content[0].AddCell(visit.HCPCSCode != null && visit.HCPCSCode.IsNotNullOrEmpty() ? visit.HCPCSCode : string.Empty, font, padding, borders);
                                    content[0].AddCell(visit.Modifier, font, padding, borders);
                                    content[0].AddCell(visit.Modifier2, font, padding, borders);
                                    content[0].AddCell(visit.Modifier3, font, padding, borders);
                                    content[0].AddCell(visit.Modifier4, font, padding, borders);
                                    content[0].AddCell("1", font, padding, borders);
                                    var visitChargeArray = visit.Charge.ToString().Split(new string[] { "." }, StringSplitOptions.None);
                                    content[0].AddCell(visitChargeArray.Length > 0 ? visitChargeArray[0] : "0", font, "Right", padding, borders);
                                    content[0].AddCell(visitChargeArray.Length > 1 ? visitChargeArray[1].PadRight(2, '0') : "00", font, padding, borders);
                                    content[0].AddCell(visit.Unit > 0 ? visit.Unit.ToString() : string.Empty, font, padding, borders);
                                    content[0].AddCell(string.Empty, font, padding, borders);
                                    content[0].AddCell(string.Empty, font, padding, borders);
                                    if (visit.UserId != Guid.Empty)
                                    {
                                        content[0].AddCell(users.FirstOrDefault(u => u.Id == visit.UserId).ClinicianProviderId.IsNotNullOrEmpty() ? users.FirstOrDefault(u => u.Id == visit.UserId).ClinicianProviderId : string.Empty, font, padding, borders);
                                    }
                                    else
                                    {
                                        content[0].AddCell(string.Empty, font, padding, borders);
                                    }
                                    count++;
                                    userRow++;
                                    lastName = users.FirstOrDefault(u => u.Id == visit.UserId).LastName;
                                }
                            }
                            
                            userLastNames.Add(lastName);
                            int userPageNo = 0;
                            if (i == 0)
                            {
                                userRow += supplyNo;
                            }
                            if (userRow % 6 == 0)
                            {
                                userPageNo = userRow / 6;
                            }
                            else
                            {
                                userPageNo = userRow / 6 + 1;
                            }
                            userPages.Add(userPageNo);
                            var blankRowNo = 6 - userRow % 6;
                            if (blankRowNo != 6)
                            {
                                content[0].AddBlankRow(blankRowNo, font, padding, borders);
                                for (int j = 0; j < blankRowNo; j++)
                                {
                                    chargeItem.Add(0.0);
                                }
                            }
                            i++;
                        });
                    }
                    else
                    {
                        foreach (var visit in visits)
                        {
                            chargeItem.Add(visit.Charge);
                            content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM") : string.Empty, font, padding, borders);
                            content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("dd") : string.Empty, font, padding, borders);
                            content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("yy") : string.Empty, font, padding, borders);
                            content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM") : string.Empty, font, padding, borders);
                            content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("dd") : string.Empty, font, padding, borders);
                            content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("yy") : string.Empty, font, padding, borders);
                            content[0].AddCell("12", font, padding, borders);
                            content[0].AddCell("1", font, padding, borders);
                            content[0].AddCell(visit.HCPCSCode != null && visit.HCPCSCode.IsNotNullOrEmpty() ? visit.HCPCSCode : string.Empty, font, padding, borders);
                            content[0].AddCell(visit.Modifier, font, padding, borders);
                            content[0].AddCell(visit.Modifier2, font, padding, borders);
                            content[0].AddCell(visit.Modifier3, font, padding, borders);
                            content[0].AddCell(visit.Modifier4, font, padding, borders);
                            content[0].AddCell("1", font, padding, borders);
                            var visitChargeArray = visit.Charge.ToString().Split(new string[] { "." }, StringSplitOptions.None);
                            content[0].AddCell(visitChargeArray.Length > 0 ? visitChargeArray[0] : "0", font, "Right", padding, borders);
                            content[0].AddCell(visitChargeArray.Length > 1 ? visitChargeArray[1].PadRight(2, '0') : "00", font, padding, borders);
                            content[0].AddCell(visit.Unit > 0 ? visit.Unit.ToString() : string.Empty, font, padding, borders);
                            content[0].AddCell(string.Empty, font, padding, borders);
                            content[0].AddCell(string.Empty, font, padding, borders);
                            if (agencyLocation != null && agencyLocation.IsLocationStandAlone)
                            {
                                content[0].AddCell(agencyLocation.NationalProviderNumber.IsNotNullOrEmpty() ? agencyLocation.NationalProviderNumber : string.Empty, font, padding, borders);
                            }
                            else
                            {
                                content[0].AddCell(data.Agency != null && data.Agency.NationalProviderNumber.IsNotNullOrEmpty() ? data.Agency.NationalProviderNumber : string.Empty, font, padding, borders);
                            }
                            count++;
                        }
                    }
                }
                else return new Paragraph[] { new Paragraph(" ") };
            }
            if (count <= 0) return new Paragraph[] { new Paragraph(" ") };
            return content;
        }
        protected override void AddPageNumber(PdfContentByte swap, int pageNum, int numPages)
        {
            pageNum--;
            double total = 0;
            var amountPaid = this.data.AmountPaid;
            var paid = default(double);
            var amountDue = this.data.AmountDue;
            var due = default(double);
            var schedules = this.data.Claim != null && this.data.Claim.VerifiedVisit.IsNotNullOrEmpty() ? this.data.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValidDate()).OrderBy(s => s.VisitDate.ToDateTime().Date).ToList() : new List<ScheduleEvent>();
            //var visits = billingService.BillableVisitSummary(this.data.Claim.AgencyLocationId, schedules, this.data.Claim.ClaimType, this.data.Claim.ChargeRates, true);
            if (chargeItem != null && chargeItem.Count > pageNum * 6)
            {
                for (int i = pageNum * 6; i < pageNum * 6 + 6 && i < chargeItem.Count; i++)
                {
                    total += chargeItem[i];
                }

                if (amountPaid > total)
                {
                    paid = total;
                    due = total - paid;
                    this.data.AmountPaid = amountPaid - paid;
                }
                else
                {
                    paid = amountPaid;
                    due = total - paid;
                    this.data.AmountPaid = amountPaid - paid;
                }
            }
            else
            {
                paid = amountPaid;
                due = total - paid;
            }
            string totalString = ((int)total).ToString() + " " + Convert.ToInt32((total % 1) * 100).ToString().PadLeft(2, '0');
            string paidString = ((int)paid).ToString() + " " + Convert.ToInt32((paid % 1) * 100).ToString().PadLeft(2, '0');
            string dueString = ((int)due).ToString() + " " + Convert.ToInt32((due % 1) * 100).ToString().PadLeft(2, '0');
            swap.BeginText();
            swap.SetFontAndSize(AxxessPdf.sans.BaseFont, 10F);
            swap.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, totalString, 443, 110, 0);
            swap.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, paidString, 513, 110, 0);
            swap.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dueString, 577, 110, 0);
            swap.EndText();
        }
        protected double SupplyValue { get; set; }
        protected double VisitValue { get; set; }
    }
}