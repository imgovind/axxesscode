﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.App.Services;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.AgencyManagement.App.Enums;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Repositories;
    class UB04Pdf : AxxessPdf {
        protected readonly IBillingService billingService;
        protected readonly IAgencyRepository agencyRepository;

        public UB04Pdf(UBOFourViewData data, IBillingService billingService, IAgencyRepository agencyRepository)
        {
            this.billingService = billingService;
            this.agencyRepository = agencyRepository;
            this.SetType(PdfDocs.UB04);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 8F;
            this.SetFonts(fonts);
            this.SetContent(this.BuildContent(data));
            float[] margins = new float[] { 213, 12, 312, 12.5F };
            this.SetMargins(margins);
            this.SetFields(this.BuildFieldMap(data));
        }

        protected virtual List<Dictionary<String, String>> BuildFieldMap(UBOFourViewData data)
        {
            var fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<string, string>() { });
            if (data.Agency != null && data.Claim != null)
            {
                var agencyLocation = agencyRepository.FindLocation(data.Agency.Id, data.Claim.AgencyLocationId);
                if (agencyLocation != null && agencyLocation.IsLocationStandAlone)
                {
                    fieldmap[0].Add("56Npi", agencyLocation.NationalProviderNumber.IsNotNullOrEmpty() ? agencyLocation.NationalProviderNumber : string.Empty);
                }
                else
                {
                    fieldmap[0].Add("56Npi", data.Agency != null && data.Agency.NationalProviderNumber.IsNotNullOrEmpty() ? data.Agency.NationalProviderNumber : string.Empty);
                }
            }
            fieldmap[0].Add("1-1", data.Agency != null && data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToUpper() : string.Empty);
            fieldmap[0].Add("1-2", data.AgencyLocation != null && data.AgencyLocation.AddressFirstRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressFirstRow.ToUpper() : string.Empty);
            fieldmap[0].Add("1-3", data.AgencyLocation != null && data.AgencyLocation.AddressSecondRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressSecondRow.ToUpper() : string.Empty);
            fieldmap[0].Add("2-1", data.Agency != null && data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToUpper() : string.Empty);
            fieldmap[0].Add("2-2", data.AgencyLocation != null && data.AgencyLocation.AddressFirstRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressFirstRow.ToUpper() : string.Empty);
            fieldmap[0].Add("2-3", data.AgencyLocation != null && data.AgencyLocation.AddressSecondRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressSecondRow.ToUpper() : string.Empty);
            fieldmap[0].Add("3aPatientCntlNum", data.Claim != null && data.Claim.MedicareNumber.IsNotNullOrEmpty() ? data.Claim.MedicareNumber.ToUpper() : string.Empty);
            fieldmap[0].Add("3bMedRecNum", data.Claim != null && data.Claim.PatientIdNumber.IsNotNullOrEmpty() ? data.Claim.PatientIdNumber.ToUpper() : string.Empty);
            fieldmap[0].Add("4BillType", data.Claim != null && data.Claim.Type.IsNotNullOrEmpty() ? data.Claim.Type : string.Empty);
            fieldmap[0].Add("5FedTaxNum", data.Agency != null && data.Agency.TaxId.IsNotNullOrEmpty() ? data.Agency.TaxId : string.Empty);
            fieldmap[0].Add("6StatementStart", data.Claim != null && data.Claim.EpisodeStartDate.IsValid() ? data.Claim.EpisodeStartDate.ToString("MMddyyyy") : string.Empty);
            fieldmap[0].Add("6StatementEnd", data.Claim != null && data.Claim.ClaimType == ClaimType.MAN ? data.Claim.EpisodeEndDate.ToString("MMddyyyy") : (data.Claim != null && data.Claim.Type == "322" && data.Claim.EpisodeStartDate.IsValid() ? data.Claim.EpisodeStartDate.ToString("MMddyyyy") : (data.Claim != null && data.Claim.Type == "329" && data.Claim.EpisodeEndDate.IsValid() ? data.Claim.EpisodeEndDate.ToString("MMddyyyy") : string.Empty)));
            fieldmap[0].Add("8aPatientName", data.Claim != null && data.Claim.LastName.IsNotNullOrEmpty() ? data.Claim.LastName.ToUpper() : string.Empty);
            fieldmap[0].Add("8bPatientName", data.Claim != null && data.Claim.FirstName.IsNotNullOrEmpty() ? data.Claim.FirstName.ToUpper() : string.Empty);
            fieldmap[0].Add("9aPatientAddress", data.Claim != null && data.Claim.AddressLine1.IsNotNullOrEmpty() ? data.Claim.AddressLine1.ToUpper() + (data.Claim.AddressLine2.IsNotNullOrEmpty() ? " " + data.Claim.AddressLine2.ToUpper() : string.Empty) : string.Empty);
            fieldmap[0].Add("9bPatientCity", data.Claim != null && data.Claim.AddressCity.IsNotNullOrEmpty() ? data.Claim.AddressCity.ToUpper() : string.Empty);
            fieldmap[0].Add("9cPatientState", data.Claim != null && data.Claim.AddressStateCode.IsNotNullOrEmpty() ? data.Claim.AddressStateCode.ToUpper() : string.Empty);
            fieldmap[0].Add("9dPatientZip", data.Claim != null && data.Claim.AddressZipCode.IsNotNullOrEmpty() ? data.Claim.AddressZipCode.ToUpper() : string.Empty);
            fieldmap[0].Add("10PatientBirthdate", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("MMddyyyy") : string.Empty);
            fieldmap[0].Add("11PatientGender", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() ? data.Claim.Gender.Substring(0, 1).ToUpper() : string.Empty);
            fieldmap[0].Add("12AdmissionDate", data.Claim != null && data.Claim.StartofCareDate.IsValid() ? data.Claim.StartofCareDate.ToString("MMddyyyy") : string.Empty);
            fieldmap[0].Add("14AdmissionType", "9");
            fieldmap[0].Add("15AdmissionSource", data.Claim != null && data.Claim.AdmissionSource.IsNotNullOrEmpty() ? data.Claim.AdmissionSource : "9");
            fieldmap[0].Add("17STAT", data.Claim != null && data.Claim.UB4PatientStatus.IsNotNullOrEmpty() ? data.Claim.UB4PatientStatus.ToString().PadLeft(2, '0') : string.Empty);
            if (data.Claim != null && data.Claim.ConditionCodes.IsNotNullOrEmpty())
            {
                var conditionCodes = XElement.Parse(data.Claim.ConditionCodes);
                fieldmap[0].Add("18ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : string.Empty);
                fieldmap[0].Add("19ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : string.Empty);
                fieldmap[0].Add("20ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : string.Empty);
                fieldmap[0].Add("21ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : string.Empty);
                fieldmap[0].Add("22ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : string.Empty);
                fieldmap[0].Add("23ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : string.Empty);
                fieldmap[0].Add("24ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : string.Empty);
                fieldmap[0].Add("25ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : string.Empty);
                fieldmap[0].Add("26ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : string.Empty);
                fieldmap[0].Add("27ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : string.Empty);
                fieldmap[0].Add("28ConditionCode", conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : string.Empty);
            }
            if (data.Claim != null && data.Claim.PrimaryInsuranceId < 1000)
            {
                var agencyLocationMedicareInsurance = agencyRepository.FindLocationMedicareInsurance(data.Agency.Id, data.Claim.PrimaryInsuranceId);
                data.Claim.PayorAddressLine1 = agencyLocationMedicareInsurance != null && agencyLocationMedicareInsurance.AddressFirstRow.IsNotNullOrEmpty() ? agencyLocationMedicareInsurance.AddressFirstRow : string.Empty;
                data.Claim.PayorAddressLine2 = agencyLocationMedicareInsurance != null && agencyLocationMedicareInsurance.AddressSecondRow.IsNotNullOrEmpty() ? agencyLocationMedicareInsurance.AddressSecondRow : string.Empty;
            }
            for (int occurrenceLocatorCount = 1; occurrenceLocatorCount <= 3; occurrenceLocatorCount++)
            {
                IDictionary<string, Locator> occurrenceLocators = new Dictionary<string, Locator>();
                switch (occurrenceLocatorCount)
                {
                    case 1: occurrenceLocators = data.Claim.Ub04Locator31.ToLocatorDictionary();
                            break;
                    case 2: occurrenceLocators = data.Claim.Ub04Locator32.ToLocatorDictionary();
                            break;
                    case 3: occurrenceLocators = data.Claim.Ub04Locator33.ToLocatorDictionary();
                            break;
                    //case 4: occurrenceLocators = data.Claim.Ub04Locator34.ToLocatorDictionary();
                    //        break;
                }
                if (occurrenceLocators != null && occurrenceLocators.Count > 0)
                {
                    if (occurrenceLocators.ContainsKey("3" + occurrenceLocatorCount + "Locator1"))
                    {
                        fieldmap[0].Add("3" + occurrenceLocatorCount + "aOccurrenceCode", occurrenceLocators["3" + occurrenceLocatorCount + "Locator1"].Code1);
                        fieldmap[0].Add("3" + occurrenceLocatorCount + "aOccurrenceDate", occurrenceLocators["3" + occurrenceLocatorCount + "Locator1"].Code2);
                    }
                    if (occurrenceLocators.ContainsKey("3" + occurrenceLocatorCount + "Locator2"))
                    {
                        fieldmap[0].Add("3" + occurrenceLocatorCount + "bOccurrenceCode", occurrenceLocators["3" + occurrenceLocatorCount + "Locator2"].Code1);
                        fieldmap[0].Add("3" + occurrenceLocatorCount + "bOccurrenceDate", occurrenceLocators["3" + occurrenceLocatorCount + "Locator2"].Code2);
                    }
                }
            }


            if (data.Claim.Ub04Locator34.IsNotNullOrEmpty())
            {
                var locators = data.Claim.Ub04Locator34.ToLocatorDictionary();
                if (locators != null && locators.Count > 0)
                {
                    if (locators.ContainsKey("34Locator1"))
                    {
                        fieldmap[0].Add("34aOccurrenceCode", locators["34Locator1"].Code1);
                        fieldmap[0].Add("34aOccurrenceDate", locators["34Locator1"].Code2);
                       
                    }
                    if (locators.ContainsKey("34Locator2"))
                    {
                        fieldmap[0].Add("34bOccurrenceCode", locators["34Locator2"].Code1);
                        fieldmap[0].Add("34bOccurrenceDate", locators["34Locator2"].Code2);
                       
                    }
                    if (locators.ContainsKey("35Locator1"))
                    {
                        fieldmap[0].Add("35aOccurrenceCode", locators["35Locator1"].Code1);
                        fieldmap[0].Add("35aOccurrenceDateStart", locators["35Locator1"].Code2);
                        fieldmap[0].Add("35aOccurrenceDateEnd", locators["35Locator1"].Code3);
                    }
                    if (locators.ContainsKey("35Locator2"))
                    {
                        fieldmap[0].Add("35bOccurrenceCode", locators["35Locator2"].Code1);
                        fieldmap[0].Add("35bOccurrenceDateStart", locators["35Locator2"].Code2);
                        fieldmap[0].Add("35bOccurrenceDateEnd", locators["35Locator2"].Code3);
                    }
                    if (locators.ContainsKey("36Locator1"))
                    {
                        fieldmap[0].Add("36aOccurrenceCode", locators["36Locator1"].Code1);
                        fieldmap[0].Add("36aOccurrenceDateEnd", locators["36Locator1"].Code2);
                        fieldmap[0].Add("36aOccurrenceDateStart", locators["36Locator1"].Code3);
                    }
                    if (locators.ContainsKey("36Locator2"))
                    {
                        fieldmap[0].Add("36bOccurrenceCode", locators["36Locator2"].Code1);
                        fieldmap[0].Add("36bOccurrenceDateStart", locators["36Locator2"].Code2);
                        fieldmap[0].Add("36bOccurrenceDateEnd", locators["36Locator2"].Code3);
                    }
                }
            }

            fieldmap[0].Add("38Payor", (data.Claim != null && data.Claim.PayorName.IsNotNullOrEmpty() ? data.Claim.PayorName : string.Empty) +  ("\n" + data.Claim.PayorAddressLine1 + "\n" + data.Claim.PayorAddressLine2));
            
            var locators39 = data.Claim.Ub04Locator39.IsNotNullOrEmpty() ? data.Claim.Ub04Locator39.ToLocatorDictionary() :
                new Dictionary<string, Locator>() { { "39Locator1", new Locator() { Code1 = "61", Code2 = data.Claim.CBSA + ".00" } } };
            if (locators39 != null && locators39.Count > 0)
            {
                if (locators39.ContainsKey("39Locator1"))
                {
                    fieldmap[0].Add("39aValueCode", locators39["39Locator1"].Code1);
                    fieldmap[0].Add("39aValueCodeAmount", locators39["39Locator1"].Code2.PadStringNumberWithDecimal(2));
                }
                if (locators39.ContainsKey("39Locator2"))
                {
                    fieldmap[0].Add("39bValueCode", locators39["39Locator2"].Code1);
                    fieldmap[0].Add("39bValueCodeAmount", locators39["39Locator2"].Code2.PadStringNumberWithDecimal(2));
                }
                if (locators39.ContainsKey("39Locator3"))
                {
                    fieldmap[0].Add("39cValueCode", locators39["39Locator3"].Code1);
                    fieldmap[0].Add("39cValueCodeAmount", locators39["39Locator3"].Code2.PadStringNumberWithDecimal(2));
                }
                if (locators39.ContainsKey("39Locator4"))
                {
                    fieldmap[0].Add("39dValueCode", locators39["39Locator4"].Code1);
                    fieldmap[0].Add("39dValueCodeAmount", locators39["39Locator4"].Code2.PadStringNumberWithDecimal(2));
                }
            }
            fieldmap[0].Add("CreationDate", data.Claim != null && data.Claim.Created.Date > DateTime.MinValue.Date ? data.Claim.Created.ToString("MMddyyyy") : DateTime.Now.ToString("MMddyyyy"));
            fieldmap[0].Add("Total", string.Format("{0:0.00} ", this.CalculateTotal(data)));
            fieldmap[0].Add("50aPayerName", data.Claim != null && data.Claim.PayorName.IsNotNullOrEmpty() ? data.Claim.PayorName : string.Empty);
            if (data.Claim != null && (data.Claim.ClaimType == ClaimType.MAN || data.Claim.ClaimType == ClaimType.HMO))
            {
                fieldmap[0].Add("51aHealthPlanId", data.Claim != null && data.Claim.HealthPlanId.IsNotNullOrEmpty() ? data.Claim.HealthPlanId : string.Empty);
            }
            fieldmap[0].Add("52aRelInfo", "Y");
            fieldmap[0].Add("53aAsgBen", "Y");
            if (data.Claim != null && (data.Claim.ClaimType == ClaimType.MAN || data.Claim.ClaimType == ClaimType.HMO))
            {
                fieldmap[0].Add("57a", data.Claim != null && data.Claim.ProviderId.IsNotNullOrEmpty() ? data.Claim.ProviderId : string.Empty);
                fieldmap[0].Add("57bOther", data.Claim != null && data.Claim.OtherProviderId.IsNotNullOrEmpty() ? data.Claim.OtherProviderId : string.Empty);
                fieldmap[0].Add("57cPrvId", data.Claim != null && data.Claim.ProviderSubscriberId.IsNotNullOrEmpty() ? data.Claim.ProviderSubscriberId : string.Empty);
            }
            fieldmap[0].Add("58aInsuredName", data.Claim != null && data.Claim.LastName.IsNotNullOrEmpty() ? data.Claim.LastName.ToUpper() + (data.Claim.FirstName.IsNotNullOrEmpty() ? ", " + data.Claim.FirstName.ToUpper() : string.Empty) : string.Empty);
            fieldmap[0].Add("59aPRel", data.Claim != null && data.Claim.Relationship != null ? data.Claim.Relationship.Code : "18");
            fieldmap[0].Add("60aInsuredUniqueId", data.Claim != null && data.Claim.MedicareNumber.IsNotNullOrEmpty() ? data.Claim.MedicareNumber : string.Empty);
            fieldmap[0].Add("61aGroupName", data.Claim != null && data.Claim.GroupName.IsNotNullOrEmpty() ? data.Claim.GroupName : string.Empty);
            fieldmap[0].Add("62aInsuranceGroupNum", data.Claim != null && data.Claim.GroupId.IsNotNullOrEmpty() ? data.Claim.GroupId : string.Empty);
            if (data.Claim != null && data.Claim.ClaimType == ClaimType.MAN)
            {
                fieldmap[0].Add("63aTreatmentAuthCode", data.Claim != null && data.Claim.AuthorizationNumber.IsNotNullOrEmpty() ? data.Claim.AuthorizationNumber : string.Empty);
                fieldmap[0].Add("63bTreatmentAuthCode", data.Claim != null && data.Claim.AuthorizationNumber2.IsNotNullOrEmpty() ? data.Claim.AuthorizationNumber2 : string.Empty);
                fieldmap[0].Add("63cTreatmentAuthCode", data.Claim != null && data.Claim.AuthorizationNumber3.IsNotNullOrEmpty() ? data.Claim.AuthorizationNumber3 : string.Empty);
            }
            else if (data.Claim != null && data.Claim.ClaimType == ClaimType.HMO)
            {
                fieldmap[0].Add("63aTreatmentAuthCode", data.Claim != null && data.Claim.AuthorizationNumber.IsNotNullOrEmpty() ? data.Claim.AuthorizationNumber : string.Empty);
                fieldmap[0].Add("63bTreatmentAuthCode", data.Claim != null && data.Claim.AuthorizationNumber2.IsNotNullOrEmpty() ? data.Claim.AuthorizationNumber2 : string.Empty);
                fieldmap[0].Add("63cTreatmentAuthCode", data.Claim != null && data.Claim.AuthorizationNumber3.IsNotNullOrEmpty() ? data.Claim.AuthorizationNumber3 : string.Empty);
            }
            else if (data.Claim != null && data.Claim.ClaimType == ClaimType.CMS)
            {
                fieldmap[0].Add("63aTreatmentAuthCode", data.Claim != null && data.Claim.ClaimKey.IsNotNullOrEmpty() ? data.Claim.ClaimKey : string.Empty);
            }
            if (data.Claim != null && data.Claim.DiagnosisCode.IsNotNullOrEmpty())
            {
                var diagnosis = XElement.Parse(data.Claim.DiagnosisCode);
                fieldmap[0].Add("67", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : string.Empty);
                fieldmap[0].Add("67a", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : string.Empty);
                fieldmap[0].Add("67b", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : string.Empty);
                fieldmap[0].Add("67c", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : string.Empty);
                fieldmap[0].Add("67d", diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value : string.Empty);
                fieldmap[0].Add("67e", diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value : string.Empty);
                fieldmap[0].Add("67f", diagnosis != null && diagnosis.Element("code7") != null ? diagnosis.Element("code7").Value : string.Empty);
                fieldmap[0].Add("67g", diagnosis != null && diagnosis.Element("code8") != null ? diagnosis.Element("code8").Value : string.Empty);
                fieldmap[0].Add("67h", diagnosis != null && diagnosis.Element("code9") != null ? diagnosis.Element("code9").Value : string.Empty);
                fieldmap[0].Add("67i", diagnosis != null && diagnosis.Element("code10") != null ? diagnosis.Element("code10").Value : string.Empty);
                fieldmap[0].Add("67j", diagnosis != null && diagnosis.Element("code11") != null ? diagnosis.Element("code11").Value : string.Empty);
                fieldmap[0].Add("67k", diagnosis != null && diagnosis.Element("code12") != null ? diagnosis.Element("code12").Value : string.Empty);
                fieldmap[0].Add("67l", diagnosis != null && diagnosis.Element("code13") != null ? diagnosis.Element("code13").Value : string.Empty);
                fieldmap[0].Add("67m", diagnosis != null && diagnosis.Element("code14") != null ? diagnosis.Element("code14").Value : string.Empty);
                fieldmap[0].Add("67n", diagnosis != null && diagnosis.Element("code15") != null ? diagnosis.Element("code15").Value : string.Empty);
                fieldmap[0].Add("67o", diagnosis != null && diagnosis.Element("code16") != null ? diagnosis.Element("code16").Value : string.Empty);
                fieldmap[0].Add("67p", diagnosis != null && diagnosis.Element("code17") != null ? diagnosis.Element("code17").Value : string.Empty);
                fieldmap[0].Add("67q", diagnosis != null && diagnosis.Element("code18") != null ? diagnosis.Element("code18").Value : string.Empty);
            }
            fieldmap[0].Add("80Remarks", data.Claim != null && data.Claim.Remark.IsNotNullOrEmpty() ? data.Claim.Remark : string.Empty);

            if (data.Claim.Ub04Locator81cca.IsNotNullOrEmpty())
            {
                var locators = data.Claim.Ub04Locator81cca.ToUb04Locator81Dictionary();
                if (locators != null && locators.Count > 0)
                {
                    if (locators.ContainsKey("Locator76"))
                    {
                        fieldmap[0].Add("76AttendingNpi", locators["Locator76"].Code1);
                        fieldmap[0].Add("76AttendingLastName", locators["Locator76"].Code2);
                        fieldmap[0].Add("76AttendingFirstName", locators["Locator76"].Code3);
                    }
                    if (locators.ContainsKey("Locator77"))
                    {
                        fieldmap[0].Add("77OperatingNpi", locators["Locator77"].Code1);
                        fieldmap[0].Add("77OperatingLastName", locators["Locator77"].Code2);
                        fieldmap[0].Add("77OperatingFirstName", locators["Locator77"].Code3);
                    }
                    if (locators.ContainsKey("Locator78"))
                    {
                        fieldmap[0].Add("78OtherNpi", locators["Locator78"].Code1);
                        fieldmap[0].Add("78OtherLastName", locators["Locator78"].Code2);
                        fieldmap[0].Add("78OtherFirstName", locators["Locator78"].Code3);
                    }
                    if (locators.ContainsKey("Locator79"))
                    {
                        fieldmap[0].Add("79OtherNpi", locators["Locator79"].Code1);
                        fieldmap[0].Add("79OtherLastName", locators["Locator79"].Code2);
                        fieldmap[0].Add("79OtherFirstName", locators["Locator79"].Code3);
                    }
                    if (locators.ContainsKey("Locator1"))
                    {
                        fieldmap[0].Add("81aCc1", locators["Locator1"].Code1);
                        fieldmap[0].Add("81aCc2", locators["Locator1"].Code2);
                        fieldmap[0].Add("81aCc3", locators["Locator1"].Code3);
                    }
                    if (locators.ContainsKey("Locator2"))
                    {
                        fieldmap[0].Add("81bCc1", locators["Locator2"].Code1);
                        fieldmap[0].Add("81bCc2", locators["Locator2"].Code2);
                        fieldmap[0].Add("81bCc3", locators["Locator2"].Code3);
                    }
                    if (locators.ContainsKey("Locator3"))
                    {
                        fieldmap[0].Add("81cCc1", locators["Locator3"].Code1);
                        fieldmap[0].Add("81cCc2", locators["Locator3"].Code2);
                        fieldmap[0].Add("81cCc3", locators["Locator3"].Code3);
                    }
                    if (locators.ContainsKey("Locator4"))
                    {
                        fieldmap[0].Add("81dCc1", locators["Locator4"].Code1);
                        fieldmap[0].Add("81dCc2", locators["Locator4"].Code2);
                        fieldmap[0].Add("81dCc3", locators["Locator4"].Code3);
                    }
                }
            }
            if (!fieldmap[0].ContainsKey("76AttendingNpi"))
            {
                fieldmap[0].Add("76AttendingNpi", data.Claim != null && data.Claim.PhysicianNPI.IsNotNullOrEmpty() ? data.Claim.PhysicianNPI : string.Empty);
            }
            if (!fieldmap[0].ContainsKey("76AttendingLastName"))
            {
                fieldmap[0].Add("76AttendingLastName", data.Claim != null && data.Claim.PhysicianLastName.IsNotNullOrEmpty() ? data.Claim.PhysicianLastName.ToUpper() : string.Empty);
            }
            if (!fieldmap[0].ContainsKey("76AttendingFirstName"))
            {
                fieldmap[0].Add("76AttendingFirstName", data.Claim != null && data.Claim.PhysicianFirstName.IsNotNullOrEmpty() ? data.Claim.PhysicianFirstName.ToUpper() : string.Empty);
            }
            
            return fieldmap;
        }

        protected virtual IElement[] BuildContent(UBOFourViewData data)
        {
            var content = new AxxessTable[] { new AxxessTable(new float[] { 2.4F, 12.3F, 7.3F, 3.5F, 3.9F, 4.9F, 4.9F, 1 }, false) };
            var font = AxxessPdf.sans;
            font.Size = 8;
            float[] padding = new float[] { 0, 1, .05F, 1 }, borders = new float[] { 0, 0, 0, 0 }, moneypad = new float[] { 0, 9, .05F, 1 };
            content[0].AddCell("0023", font, padding, borders);
            content[0].AddCell("HOME HEALTH SERVICES", font, padding, borders);
            content[0].AddCell(data.Claim != null && data.Claim.HippsCode.IsNotNullOrEmpty() ? data.Claim.HippsCode : string.Empty, font, padding, borders);
            content[0].AddCell(data.Claim != null && data.Claim.FirstBillableVisitDate.IsValid() ? data.Claim.FirstBillableVisitDate.ToString("MMddyyyy") : string.Empty, font, padding, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            content[0].AddCell("0.00", font, "Right", moneypad, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            if (data.Claim.Type == ((int)BillType.HHPPSFinal).ToString())
            {
                this.SupplyValue = 0;
                if (data.Claim.IsSupplyNotBillable)
                {
                }
                else
                {
                    var supplies = data.Claim.Supply.IsNotNullOrEmpty() ? data.Claim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                    supplies = supplies.Where(s => s.IsBillable && !s.IsDeprecated).ToList();
                    var serviceSupplies = supplies.Where(s => s.RevenueCode == "0270") ?? new List<Supply>();
                    var woundSupplies = supplies.Where(s => s.RevenueCode == "0623") ?? new List<Supply>();
                    var medicalSuppliesTotal = serviceSupplies.Count() > 0 ? serviceSupplies.Sum(s => s.TotalCost) : 0;
                    var woundSuppliesTotal = woundSupplies.Count() > 0 ? woundSupplies.Sum(s => s.TotalCost) : 0;
                    this.SupplyValue = medicalSuppliesTotal > 0 ? medicalSuppliesTotal : (data.Claim.SupplyTotal - woundSuppliesTotal);

                    if (supplies.Except(woundSupplies).Count() > 0 || !supplies.IsNotNullOrEmpty())
                    {
                        content[0].AddCell(medicalSuppliesTotal > 0 ? "0270" : "0272", font, padding, borders);
                        content[0].AddCell("SERVICE SUPPLIES", font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(data.Claim != null && data.Claim.FirstBillableVisitDate.IsValid() ? data.Claim.FirstBillableVisitDate.ToString("MMddyyyy") : string.Empty, font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(String.Format("{0:0.00}", medicalSuppliesTotal > 0 ? medicalSuppliesTotal : this.SupplyValue), font, "Right", moneypad, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                    }

                    if (woundSuppliesTotal > 0)
                    {
                        content[0].AddCell("0623", font, padding, borders);
                        content[0].AddCell("WOUND SUPPLIES", font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(data.Claim != null && data.Claim.FirstBillableVisitDate.IsValid() ? data.Claim.FirstBillableVisitDate.ToString("MMddyyyy") : string.Empty, font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(String.Format("{0:0.00}", woundSuppliesTotal), font, "Right", moneypad, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        this.SupplyValue += woundSuppliesTotal;
                    }
                }

                if (data.Claim.Type == ((int)BillType.HHPPSFinal).ToString())
                {
                    var schedules = data.Claim != null && data.Claim.VerifiedVisit.IsNotNullOrEmpty() ? data.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValidDate()).OrderBy(s => s.VisitDate.ToDateTime().Date).ToList() : new List<ScheduleEvent>();
                    if (schedules != null && schedules.Count > 0)
                    {
                        try
                        {
                            var insurance = data.Claim.Insurance.ToObject<AgencyInsurance>();
                        
                            var visits = new List<BillSchedule>();
                            if (insurance != null)
                            {
                                visits = billingService.BillableVisitSummary(data.Claim.AgencyLocationId, schedules, data.Claim.PrimaryInsuranceId > 0 && data.Claim.PrimaryInsuranceId < 1000 ? ClaimType.CMS : ClaimType.HMO, data.Claim.ChargeRates, false, data.Claim.EpisodeStartDate, insurance.RequireServiceLocation);
                            }
                            else
                            {
                                if (data.Claim!=null && data.Claim.PrimaryInsuranceId != 0)
                                {
                                    if (data.Claim.PrimaryInsuranceId >= 1000)
                                    {
                                        insurance = agencyRepository.FindInsurance(Current.AgencyId, data.Claim.PrimaryInsuranceId);
                                    }
                                    else if (data.Claim.PrimaryInsuranceId < 1000)
                                    {
                                        insurance = billingService.CMSInsuranceToAgencyInsurance(data.Claim.AgencyLocationId, data.Claim.PrimaryInsuranceId);
                                    }
                                    if (insurance != null)
                                    {
                                        visits = billingService.BillableVisitSummary(data.Claim.AgencyLocationId, schedules, data.Claim.PrimaryInsuranceId > 0 && data.Claim.PrimaryInsuranceId < 1000 ? ClaimType.CMS : ClaimType.HMO, data.Claim.ChargeRates, false, data.Claim.EpisodeStartDate, insurance.RequireServiceLocation);
                                    }
                                    else
                                    {
                                        visits = billingService.BillableVisitSummary(data.Claim.AgencyLocationId, schedules, data.Claim.PrimaryInsuranceId > 0 && data.Claim.PrimaryInsuranceId < 1000 ? ClaimType.CMS : ClaimType.HMO, data.Claim.ChargeRates, false, data.Claim.EpisodeStartDate);
                                    }
                                }
                                else
                                {
                                    visits = billingService.BillableVisitSummary(data.Claim.AgencyLocationId, schedules, data.Claim.PrimaryInsuranceId > 0 && data.Claim.PrimaryInsuranceId < 1000 ? ClaimType.CMS : ClaimType.HMO, data.Claim.ChargeRates, false, data.Claim.EpisodeStartDate);
                                }
                            }
                            if (visits != null && visits.Count > 0)
                            {
                                var total = 0.0;
                                foreach (var visit in visits)
                                {
                                    content[0].AddCell(visit.RevenueCode, font, padding, borders);
                                    content[0].AddCell(visit.PreferredName, font, padding, borders);
                                    content[0].AddCell(string.Format("{0} {1} {2} {3} {4}", visit.HCPCSCode, visit.Modifier, visit.Modifier2, visit.Modifier3, visit.Modifier4), font, padding, borders);
                                    content[0].AddCell(visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MMddyyyy") : string.Empty, font, padding, borders);
                                    content[0].AddCell(visit.Unit > 0 ? visit.Unit.ToString() : string.Empty, font, padding, borders);
                                    content[0].AddCell(visit.Charge > 0 ? string.Format("{0:#0.00}", visit.Charge) : string.Empty, font, "Right", moneypad, borders);
                                    content[0].AddCell(string.Empty, font, padding, borders);
                                    content[0].AddCell(string.Empty, font, padding, borders);
                                    total += visit.Charge;
                                }
                                this.VisitValue = total;
                            }
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            return content;
        }

        protected virtual double CalculateTotal(UBOFourViewData data)
        {
            double total = 0;
            if (data.Claim.Type == ((int)BillType.HHPPSFinal).ToString())
            {
                total = this.SupplyValue + this.VisitValue;
            }
            return total;
        }

        protected override void AddPageNumber(PdfContentByte swap, int pageNum, int numPages) {
            swap.BeginText();
            swap.SetFontAndSize(AxxessPdf.sans.BaseFont, 10);
            swap.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, pageNum.ToString(), 90, 302, 0);
            swap.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, numPages.ToString(), 135, 302, 0);
            swap.EndText();
        }

        protected double SupplyValue { get; set; }
        protected double VisitValue { get; set; }
    }
}