﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using iTextSharp.text;
    using Axxess.Core.Infrastructure;
    class BillingRapPdf : AxxessPdf {
        public BillingRapPdf(Rap data) {
            this.SetType(PdfDocs.BillingRap);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 11F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(data != null && data.Remark.IsNotNullOrEmpty() ? data.Remark : " ", fonts[0]) };
            this.SetContent(content);
            float[] margins = new float[] { 515, 35, 35, 35 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            var conditionCodes = data.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(data.ConditionCodes) : null;
            var diagnosis = data.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(data.DiagnosisCode) : null;
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            if (data.Agency != null)
            {
                var location = data.Agency.GetBranch(data.BranchId);
               
                    fieldmap[0].Add("agency", (
                        data != null && data.Agency != null ?
                            (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name + "\n" : "") +
                            (location != null ?
                                (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") +
                                (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                                (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") +
                                (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") +
                                (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : "") +
                                (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : "") +
                                (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "")
                            : "")
                        : ""));
            }
            fieldmap[0].Add("patient", (
                data != null ?
                    (data.AddressLine1.IsNotNullOrEmpty() ? data.AddressLine1.ToTitleCase() + "\n" : "") +
                    (data.AddressLine2.IsNotNullOrEmpty() ? data.AddressLine2.ToTitleCase() + "\n" : "") +
                    (data.AddressCity.IsNotNullOrEmpty() ? data.AddressCity.ToTitleCase() + ", " : "") +
                    (data.AddressStateCode.IsNotNullOrEmpty() ? data.AddressStateCode.ToTitleCase() + "  " : "") +
                    (data.AddressZipCode.IsNotNullOrEmpty() ? StringFormatter.FormatZipCode(data.AddressZipCode) + "\n" : "") : ""));
            fieldmap[0].Add("mcare", data != null && data.MedicareNumber.IsNotNullOrEmpty() ? data.MedicareNumber : "");
            fieldmap[0].Add("record", data != null && data.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientIdNumber : "");
            fieldmap[0].Add("gender", data != null && data.Gender.IsNotNullOrEmpty() ? data.Gender : "");
            fieldmap[0].Add("dob", data != null && data.DOB.IsValid() ? data.DOB.ToShortDateString() : "");
            fieldmap[0].Add("type", data != null ?
                (data.Type == 0 ? "Initial RAP" : "") +
                (data.Type == 1 ? "RAP Cancellation" : "") : "");
            fieldmap[0].Add("admitdate", data != null && data.StartofCareDate.IsValid() ? data.StartofCareDate.ToShortDateString() : "");
            fieldmap[0].Add("admitsource", data != null && data.AdmissionSourceDisplay.IsNotNullOrEmpty() ? data.AdmissionSourceDisplay : "");
            fieldmap[0].Add("status", data != null ?
                (data.PatientStatus == 1 ? "Active" : "") +
                (data.PatientStatus == 2 ? "Discharged" : "") : "");
            fieldmap[0].Add("cond18", conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : "");
            fieldmap[0].Add("cond19", conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : "");
            fieldmap[0].Add("cond20", conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : "");
            fieldmap[0].Add("cond21", conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : "");
            fieldmap[0].Add("cond22", conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : "");
            fieldmap[0].Add("cond23", conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : "");
            fieldmap[0].Add("cond24", conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : "");
            fieldmap[0].Add("cond25", conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : "");
            fieldmap[0].Add("cond26", conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : "");
            fieldmap[0].Add("cond27", conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : "");
            fieldmap[0].Add("cond28", conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : "");
            fieldmap[0].Add("hipps", data != null && data.HippsCode.IsNotNullOrEmpty() ? data.HippsCode : "");
            fieldmap[0].Add("matchkey", data != null && data.ClaimKey.IsNotNullOrEmpty() ? data.ClaimKey : "");
            fieldmap[0].Add("startdate", data != null && data.EpisodeStartDate.IsValid() ? data.EpisodeStartDate.ToShortDateString() : "");
            fieldmap[0].Add("billvisit", data != null && data.FirstBillableVisitDate.IsValid() ? data.FirstBillableVisitDate.ToShortDateString() : "");
            fieldmap[0].Add("phys", data != null ?
                (data.PhysicianLastName.IsNotNullOrEmpty() ? data.PhysicianLastName.ToUpper() + ", " : "") +
                (data.PhysicianFirstName.IsNotNullOrEmpty() ? data.PhysicianFirstName.ToUpper() : "") : "");
            fieldmap[0].Add("physnpi", data != null && data.PhysicianNPI.IsNotNullOrEmpty() ? data.PhysicianNPI : "");
            fieldmap[0].Add("hippspay", data != null && data.ProspectivePay > 0 ? data.ProspectivePay.ToString() : "");
            fieldmap[0].Add("diag1", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : "");
            fieldmap[0].Add("diag2", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : "");
            fieldmap[0].Add("diag3", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : "");
            fieldmap[0].Add("diag4", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : "");
            fieldmap[0].Add("diag5", diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value : "");
            fieldmap[0].Add("diag6", diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value : "");
            fieldmap[1].Add("patientname", data != null ?
                (data.LastName.IsNotNullOrEmpty() ? data.LastName.ToUpper() + ", " : "") +
                (data.FirstName.IsNotNullOrEmpty() ? data.FirstName.ToUpper() : "") : "");
            this.SetFields(fieldmap);
        }
    }
}