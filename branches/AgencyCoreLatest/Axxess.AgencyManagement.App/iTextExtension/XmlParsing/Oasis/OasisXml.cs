﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.Domain;
    class OasisXml : BaseXml {
        private IDictionary<String, Question> Data = null;
        private string Discipline = string.Empty;
        public OasisXml(AssessmentPrint data, bool PrintAll) : base(PdfDocs.Oasis) {
            if (data.Type == AssessmentType.StartOfCare) this.Type = "C1";
            else if (data.Type == AssessmentType.ResumptionOfCare) this.Type = "C3";
            else if (data.Type == AssessmentType.Recertification) this.Type = "C4";
            else if (data.Type == AssessmentType.FollowUp) this.Type = "C5";
            else if (data.Type == AssessmentType.TransferInPatientNotDischarged) this.Type = "C6";
            else if (data.Type == AssessmentType.TransferInPatientDischarged) this.Type = "C7";
            else if (data.Type == AssessmentType.DischargeFromAgencyDeath) this.Type = "C8";
            else if (data.Type == AssessmentType.DischargeFromAgency) {
                if (data.Version ==2) this.Init(PdfDocs.DischargeFromAgency);
                if (data.Version == 4) this.Init(PdfDocs.DischargeFromAgency4);
                this.Type = "C9";
            }
            else if (data.Type == AssessmentType.NonOasisStartOfCare) this.Type = "01";
            else if (data.Type == AssessmentType.NonOasisRecertification) this.Type = "04";
            else if (data.Type == AssessmentType.NonOasisDischarge) this.Type = "09";
            this.Data = data.Data ?? new Dictionary<string, Question>();
            if (!this.Data.ContainsKey("M0100AssessmentType")) this.Data.Add(new KeyValuePair<string, Question>("M0100AssessmentType", new Question()));
            this.Data["M0100AssessmentType"].Answer = "0" + this.Type.Substring(1, 1);
            if (!this.Data.ContainsKey("AssessmentDate")) this.Data.Add(new KeyValuePair<string, Question>("AssessmentDate", new Question()));
            this.Data["AssessmentDate"].Answer = data.AssessmentDate.IsValid() ? data.AssessmentDate.ToShortDateString() : string.Empty;
            if (!this.Data.ContainsKey("VisitDate")) this.Data.Add(new KeyValuePair<string, Question>("VisitDate", new Question()));
            this.Data["VisitDate"].Answer = data.VisitDate.IsValid() ? data.VisitDate.ToShortDateString() : string.Empty;
            if (!this.Data.ContainsKey("TimeIn")) this.Data.Add(new KeyValuePair<string, Question>("TimeIn", new Question()));
            this.Data["TimeIn"].Answer = data.TimeIn.IsNotNullOrEmpty() ? data.TimeIn : string.Empty;
            if (!this.Data.ContainsKey("TimeOut")) this.Data.Add(new KeyValuePair<string, Question>("TimeOut", new Question()));
            this.Data["TimeOut"].Answer = data.TimeOut.IsNotNullOrEmpty() ? data.TimeOut : string.Empty;
            if (!this.Data.ContainsKey("Signature")) this.Data.Add(new KeyValuePair<string, Question>("Signature", new Question()));
            this.Data["Signature"].Answer = data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : string.Empty;
            if (!this.Data.ContainsKey("SignatureDate")) this.Data.Add(new KeyValuePair<string, Question>("SignatureDate", new Question()));
            this.Data["SignatureDate"].Answer = data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : string.Empty;
            this.Init();
            if (this.Data.Count > 6) this.FilterEmptySections((data.Discipline == "PT" || data.Discipline == "OT" || data.Discipline == "ST"));
            if (data.Discipline == "PT" || data.Discipline == "OT" || data.Discipline== "ST") this.Discipline = " (" + data.Discipline + ")";
            if (!PrintAll) this.FilterNonOASIS();
        }
        public String GetOasisGeneration() {
            return this.Type.Substring(0, 1);
        }
        public int GetAssessmentType() {
            return this.Type.Substring(1, 1).ToInteger() - 1;
        }
        public override string GetData(String Index) {
            return this.Data != null && this.Data.ContainsKey(Index) && this.Data[Index].Answer.IsNotNullOrEmpty() ? this.Data[Index].Answer : string.Empty;
        }
        public string GetDiscipline() {
            return this.Discipline;
        }
        private void FilterEmptySections(bool Therapy) {
            for (int tabI = 0; tabI < this.Layout.Count(); tabI++) {
                for (int fieldsetI = 0; fieldsetI < this.Layout[tabI].Subsection.Count(); fieldsetI++) {
                    if (this.Layout[tabI].Subsection[fieldsetI].Type == "diagnoses") this.RemoveUnusedDisgnoses(tabI, fieldsetI);
                    if (this.Layout[tabI].Subsection[fieldsetI].Type == "woundgraph") fieldsetI = this.RemoveUnusedWounds(tabI, fieldsetI);
                    if (!Therapy && this.Layout[tabI].Subsection[fieldsetI].Type == "tinetti") fieldsetI = this.RemoveTinetti(tabI, fieldsetI);
                    if (this.Layout[tabI].Subsection[fieldsetI].Type == "interventions" || this.Layout[tabI].Subsection[fieldsetI].Type == "goals") fieldsetI = this.RemoveUnusedOrdersGoals(tabI, fieldsetI);
                }
            }
            this.NotaFilter();
        }
        private void RemoveUnusedDisgnoses(int Tab, int Fieldset) {
            for (int diagnosis = 4; diagnosis < this.Layout[Tab].Subsection[Fieldset].Question.Count(); diagnosis++) {
                if (this.Layout[Tab].Subsection[Fieldset].Question[diagnosis].Subquestion[0].Data.Replace("_", "").Trim().Length == 2) {
                    this.Layout[Tab].Subsection[Fieldset].Question.RemoveAt(diagnosis);
                    diagnosis--;
                }
            }
            if (this.Layout[Tab].Subsection[Fieldset].Question[0].Subquestion.Count() == 20) this.Layout[Tab].Subsection[Fieldset].Question[0].Subquestion.RemoveRange(16, 4);
        }
        private int RemoveUnusedWounds(int Tab, int Fieldset) {
            if (this.Layout[Tab].Subsection[Fieldset].Question[0].Subquestion[0].Data.Trim().IsNullOrEmpty()) this.Layout[Tab].Subsection.RemoveAt(Fieldset--);
            return Fieldset;
        }
        private int RemoveUnusedOrdersGoals(int Tab, int Fieldset) {
            for (int questionI = 0; questionI < this.Layout[Tab].Subsection[Fieldset].Question.Count(); questionI++) {
                if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Type == "checkgroup") {
                    for (int optionI = 0; optionI < this.Layout[Tab].Subsection[Fieldset].Question[questionI].Option.Count(); optionI++) {
                        if (!this.Layout[Tab].Subsection[Fieldset].Question[questionI].Data.Split(',').Contains(this.Layout[Tab].Subsection[Fieldset].Question[questionI].Option[optionI].Value)) this.Layout[Tab].Subsection[Fieldset].Question[questionI].Option.RemoveAt(optionI--);
                    }
                    if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Option.Count == 0) this.Layout[Tab].Subsection[Fieldset].Question.RemoveAt(questionI--);
                }
                else if (this.Layout[Tab].Subsection[Fieldset].Question[questionI].Data.Trim().Length == 0) this.Layout[Tab].Subsection[Fieldset].Question.RemoveAt(questionI--);
            }
            if (this.Layout[Tab].Subsection[Fieldset].Question.Count == 0) this.Layout[Tab].Subsection.RemoveAt(Fieldset--);
            return Fieldset;
        }
        private int RemoveTinetti(int Tab, int Fieldset) {
            this.Layout[Tab].Subsection.RemoveAt(Fieldset--);
            return Fieldset;
        }
        private void FilterNonOASIS()
        {
            for (int tabI = 0; tabI < this.Layout.Count(); tabI++) {
                for (int fieldsetI = 0; fieldsetI < this.Layout[tabI].Subsection.Count(); fieldsetI++) {
                    if (this.Layout[tabI].Subsection[fieldsetI].Type != "diagnoses") for (int questionI = 0; questionI < this.Layout[tabI].Subsection[fieldsetI].Question.Count(); questionI++) if (this.Layout[tabI].Subsection[fieldsetI].Question[questionI].Oasis.IsNullOrEmpty()) this.Layout[tabI].Subsection[fieldsetI].Question.RemoveAt(questionI--);
                    if (this.Layout[tabI].Subsection[fieldsetI].Question.Count() == 0) this.Layout[tabI].Subsection.RemoveAt(fieldsetI--);
                }
                if (this.Layout[tabI].Subsection.Count() == 0) this.Layout.RemoveAt(tabI--);
            }
        }
    }
}