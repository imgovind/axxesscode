﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;

    public class ScheduleViewData
    {
        public int Count { get; set; }
        public Guid PatientId { get; set; }
        public int PatientStatus { get; set; }
        public PatientEpisode Episode { get; set; }
        
        public bool IsDischarged { get; set; }
        public bool IsNotAdmitted { get; set; }
        public bool IsPending { get; set; }
    }
}
