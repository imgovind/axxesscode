﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App
{
    public class CityStateViewData
    {
        public string City { get; set; }
        public string State { get; set; }
        public bool isSuccessful { get; set; }
    }
}
