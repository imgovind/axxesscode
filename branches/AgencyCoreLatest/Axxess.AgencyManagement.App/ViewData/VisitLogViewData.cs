﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;

    public class VisitLogViewData
    {
        public Guid Id { get; set; }
        public Guid TaskId { get; set; }
        public Guid UserId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string UserName { get; set; }
        public string TaskName { get; set; }
        public DateTime Created { get; set; }
        public string PatientMRN { get; set; }
        public string TaskDate { get; set; }
        public string PatientName { get; set; }
        public string VisitDate { get; set; }
        public string VerifiedAddress { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public decimal VerifiedLatitude { get; set; }
        public decimal VerifiedLongitude { get; set; }
        public DateTime VerifiedDateTime { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public Guid PatientSignatureAssetId { get; set; }
        public string PatientUnableToSignReason { get; set; }
        public DateTime StartofCareDate { get; set; }
        public DateTime DateofBirth { get; set; }
        public string PatientAddress { get; set; }
        
        public string EpisodeDateRange
        {
            get
            {
                return string.Format("{0} - {1}", this.EpisodeStartDate.ToString("MM/dd/yyyy"), this.EpisodeEndDate.ToString("MM/dd/yyyy"));
            }
        }


    }
}
