﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Domain;

namespace Axxess.AgencyManagement.App
{
    public class MasterCalendarViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public string PatientIdNumber { get; set; }
        public Guid PreviousEpisodeId { get; set; }
        public Guid NextEpisodeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartOfCareDate { get; set; }
        public string DisplayName { get; set; }
        public string FrequencyList { get; set; }
        public List<ScheduleEvent> ScheduleEvents { get; set; }

        public MasterCalendarViewData(Guid id, Guid patientId)
        {
            Id = id;
            PatientId = patientId;
        }
    }
}
