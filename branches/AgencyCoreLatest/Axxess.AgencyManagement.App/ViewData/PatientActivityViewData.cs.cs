﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class PatientActivityViewData : ActivityViewData
    {
        public string Range { get; set; }
    }
}
