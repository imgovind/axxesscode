﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App
{
    public class ClaimNewSupplyViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public string Type { get; set; }

        public ClaimNewSupplyViewData()
        {

        }

        public ClaimNewSupplyViewData(Guid id, Guid patientId, string type)
        {
            Id = id;
            PatientId = patientId;
            Type = type;
        }
    }
}
