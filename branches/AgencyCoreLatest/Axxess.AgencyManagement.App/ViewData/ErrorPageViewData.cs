﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Infrastructure;

    public class ErrorPageViewData
    {
        public string message { get; set; }
    }
}
