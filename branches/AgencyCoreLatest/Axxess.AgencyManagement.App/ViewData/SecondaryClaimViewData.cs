﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;

namespace Axxess.AgencyManagement.App.ViewData
{
    public class SecondaryClaimViewData
    {
        public ClaimInfoSnapShotViewData PrimaryClaimData { get; set; }

        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PrimaryClaimId { get; set; }

        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }

        public string EpisodeRange 
        { 
            get 
            {
                return string.Format("{0} - {1}", this.EpisodeStartDate.ToZeroFilled(), this.EpisodeEndDate.ToZeroFilled());
            } 
        }
    }
}
