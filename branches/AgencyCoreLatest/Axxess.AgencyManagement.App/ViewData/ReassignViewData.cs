﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.ViewData
{
    using Axxess.AgencyManagement.Domain;

    public class ReassignViewData
    {
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string PatientDisplayName { get; set; }
        public List<User> Users { get; set; }
        public string OldUserName { get; set; }
        public string EventDate { get; set; }
        public string EpisodeRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
