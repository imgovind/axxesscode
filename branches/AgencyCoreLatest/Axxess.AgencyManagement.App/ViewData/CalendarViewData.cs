﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Domain;

namespace Axxess.AgencyManagement.App.ViewData
{
    public class CalendarViewData
    {
        public PatientEpisode Episode { get; set; }
        public Guid PatientId { get; set; }
        public int patientStatus { get; set; }
        public bool IsDischarged { get; set; }
        public bool IsNotAdmitted { get; set; }
        public bool IsPending { get; set; }
    }
}
