﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    public class ClinicalOrder
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public string Status { get; set; }
        public string PatientName { get; set; }
        public string Type { get; set; }
        public string Physician { get; set; }
        public string CreatedDate { get; set; }
        public string SentDate { get; set; }
    }
}
