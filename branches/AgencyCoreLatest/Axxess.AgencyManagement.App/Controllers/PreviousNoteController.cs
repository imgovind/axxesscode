﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Security.Permissions;
    using System.Web.Mvc;
    using System.Web.UI.WebControls;
    using System.Collections.Generic;

    using Common;

    using NPOI.SS.Formula.Functions;

    using ViewData;
    using Services;
    using Security;
    using Workflows;
    using Extensions;
    using iTextExtension;
    using iTextExtension.XmlParsing;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;

    using Axxess.LookUp.Domain;

    using Axxess.Log.Enums;

    using Telerik.Web.Mvc;
    using System.Text;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PreviousNoteController: BaseController
    {
         #region Constructor

        private readonly IPatientService patientService;

        public PreviousNoteController( IPatientService patientService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");

            this.patientService = patientService;
        }

        #endregion

        //public ActionResult SkilledNurse(Guid patientId, Guid episodeId, Guid eventId)
        //{


        //}
    }
}
