﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Enums;
    using Exports;
    using Services;
    using Security;
    using ViewData;
    using Extensions;
    using iTextExtension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Log.Enums;
    using Axxess.Log.Domain;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.OasisC.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.AgencyManagement.App.Common;

    [Compress]
    
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class BillingController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly IBillingService billingService;
        private readonly IAgencyService agencyService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IReferralRepository referrralRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;

        public BillingController(IAgencyManagementDataProvider dataProvider, IOasisCDataProvider oasisCDataProvider, ILookUpDataProvider lookUpDataProvider, IPatientService patientService, IBillingService billingService, IAgencyService agencyService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            this.referrralRepository = dataProvider.ReferralRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.patientService = patientService;
            this.billingService = billingService;
            this.agencyService = agencyService;
            this.assessmentRepository = oasisCDataProvider.OasisAssessmentRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.lookupRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EligibilityReport()
        {
            return PartialView("MedicareEligibilityReport");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EligibilityList(Guid patientId)
        {
            return PartialView("MedicareEligibilityGrid", patientService.GetMedicareEligibilityLists(patientId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RAPCenter()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var bill = new Bill();
            ViewData["Type"] = ClaimTypeSubCategory.RAP.ToString();
            if (agency != null)
            {
                var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
                {
                    int payorType;
                    if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agencyMainBranch.IsLocationStandAlone ? agencyMainBranch.Payor : agency.Payor, out payorType))
                    {
                        bill.IsMedicareHMO = false;
                        bill.BranchId = agencyMainBranch.Id;
                        bill.BranchName = agencyMainBranch.Name;
                        bill.Insurance = payorType;
                        var insurance = lookupRepository.GetInsurance(payorType);
                        if (insurance != null)
                        {
                            bill.InsuranceName = insurance.Name;
                        }
                        //bill.Claims = billingService.AllUnProcessedRaps(agencyMainBranch.Id, payorType, false, false);
                        bill.Claims = billingService.AllUnProcessedRapsByDate(agencyMainBranch.Id, payorType, DateTime.Now.AddDays(-64), DateTime.Now, false, false);
                        ViewData["Branch"] = agencyMainBranch.Id;
                        ViewData["Insurance"] = payorType;
                        return PartialView("Center", bill);
                    }
                    else
                    {
                        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                        if (agencyMedicareInsurance != null)
                        {
                            bill.IsMedicareHMO = agencyMedicareInsurance.PayorType == 2 ? true : false;
                            bill.BranchId = agencyMainBranch.Id;
                            bill.BranchName = agencyMainBranch.Name;
                            bill.Insurance = agencyMedicareInsurance.Id;
                            bill.InsuranceName = agencyMedicareInsurance.Name;
                            bill.Claims = billingService.AllUnProcessedRapsByDate(agencyMainBranch.Id, agencyMedicareInsurance.Id, DateTime.Now.AddDays(-64), DateTime.Now, false, false); // AllUnProcessedRaps(agencyMainBranch.Id, agencyMedicareInsurance.Id, false, false);
                            ViewData["Branch"] = agencyMainBranch.Id;
                            ViewData["Insurance"] = agencyMedicareInsurance.Id;
                            return PartialView("Center", bill);
                        }
                    }
                }
                else
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        bill.IsMedicareHMO = agencyMedicareInsurance.PayorType == 2 ? true : false;
                        bill.BranchId = agencyMainBranch.Id;
                        bill.BranchName = agencyMainBranch.Name;
                        bill.Insurance = agencyMedicareInsurance.Id;
                        bill.InsuranceName = agencyMedicareInsurance.Name;
                        //bill.Claims = billingService.AllUnProcessedRaps(agencyMainBranch.Id, agencyMedicareInsurance.Id, false, false); 
                        bill.Claims = billingService.AllUnProcessedRapsByDate(agencyMainBranch.Id, agencyMedicareInsurance.Id, DateTime.Now.AddDays(-64), DateTime.Now, false, false); 
                        ViewData["Branch"] = agencyMainBranch.Id;
                        ViewData["Insurance"] = agencyMedicareInsurance.Id;
                        return PartialView("Center", bill);
                    }
                }
            }
            return PartialView("Center", bill);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FinalCenter()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var bill = new Bill();
            bill.ClaimType = ClaimTypeSubCategory.Final;
            if (agency != null)
            {
                var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
                {
                    int payorType;
                    if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agencyMainBranch.IsLocationStandAlone ? agencyMainBranch.Payor : agency.Payor, out payorType))
                    {
                        bill.IsMedicareHMO = false;
                        bill.BranchId = agencyMainBranch.Id;
                        bill.BranchName = agencyMainBranch.Name;
                        bill.Insurance = payorType;
                        var insurance = lookupRepository.GetInsurance(payorType);
                        if (insurance != null)
                        {
                            bill.InsuranceName = insurance.Name;
                        }
                        bill.Claims = billingService.AllUnProcessedFinalsByDate(agencyMainBranch.Id, payorType, DateTime.Now.AddDays(-64), DateTime.Now ,false, false);
                        ViewData["Branch"] = agencyMainBranch.Id;
                        ViewData["Insurance"] = payorType;
                        return PartialView("Center", bill);
                    }
                    else
                    {
                        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                        if (agencyMedicareInsurance != null)
                        {
                            bill.IsMedicareHMO = agencyMedicareInsurance.PayorType == 2 ? true : false;
                            bill.BranchId = agencyMainBranch.Id;
                            bill.BranchName = agencyMainBranch.Name;
                            bill.Insurance = agencyMedicareInsurance.Id;
                            bill.InsuranceName = agencyMedicareInsurance.Name;
                            bill.Claims = billingService.AllUnProcessedFinalsByDate(agencyMainBranch.Id, agencyMedicareInsurance.Id, DateTime.Now.AddDays(-64), DateTime.Now, false, false);
                            ViewData["Branch"] = agencyMainBranch.Id;
                            ViewData["Insurance"] = agencyMedicareInsurance.Id;
                            return PartialView("Center", bill);

                        }
                    }
                }
                else
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        bill.IsMedicareHMO = agencyMedicareInsurance.PayorType == 2 ? true : false;
                        bill.BranchId = agencyMainBranch.Id;
                        bill.BranchName = agencyMainBranch.Name;
                        bill.Insurance = agencyMedicareInsurance.Id;
                        bill.InsuranceName = agencyMedicareInsurance.Name;
                        bill.Claims = billingService.AllUnProcessedFinalsByDate(agencyMainBranch.Id, agencyMedicareInsurance.Id, DateTime.Now.AddDays(-64), DateTime.Now, false, false);
                        ViewData["Branch"] = agencyMainBranch.Id;
                        ViewData["Insurance"] = agencyMedicareInsurance.Id;
                        return PartialView("Center", bill);

                    }
                }
            }
            return PartialView("Center", bill);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult ClaimsPdf(Guid branchId, int insuranceId,string parentSortType, string columnSortType, string claimType)
        {
            var location = new AgencyLocation();
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location!=null)
                {
                    location.Name = agency.Name;

                }
            }
            var doc = new BillingClaimsPdf(billingService.GetClaimsPrint(branchId, insuranceId,parentSortType,columnSortType, claimType),location);
            var stream = doc.GetStream()??new MemoryStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Claims_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult ClaimsPdfByDate(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, string parentSortType, string columnSortType, string claimType)
        {
            var location = new AgencyLocation();
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null)
                {
                    location.Name = agency.Name;

                }
            }
            var doc = new BillingClaimsPdf(billingService.GetClaimsPrint(branchId, insuranceId, startDate, endDate, parentSortType, columnSortType, claimType), location);
            var stream = doc.GetStream() ?? new MemoryStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Claims_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ClaimsXls(Guid branchId, int insuranceId, string parentSortType, string columnSortType, string claimType)
        {
            var export = new ClaimsExport(billingService.GetClaimsPrint(branchId, insuranceId, parentSortType,columnSortType, claimType));
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "ClaimSummary.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult ClaimsXlsByDate(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, string parentSortType, string columnSortType, string claimType)
        {
            var export = new ClaimsExport(billingService.GetClaimsPrint(branchId, insuranceId, startDate, endDate, parentSortType, columnSortType, claimType));
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", "ClaimSummary.xls");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalGrid(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var bill = new Bill { Claims = billingService.AllUnProcessedFinalsByDate(branchId, insuranceId, startDate, endDate, false, false) };
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null)
                {
                    bill.BranchId = location.Id;
                    bill.BranchName = location.Name;
                }
                if (insuranceId > 0)
                {
                    if (insuranceId > 0 && insuranceId < 1000)
                    {
                        var medicareInsurance = lookupRepository.GetInsurance(insuranceId);
                        bill.Insurance = insuranceId;
                        bill.InsuranceName = medicareInsurance != null ? medicareInsurance.Name : string.Empty;
                    }
                    else if (insuranceId >= 1000)
                    {
                        var insurance = agencyRepository.FindInsurance(Current.AgencyId, insuranceId);
                        bill.Insurance = insuranceId;
                        bill.InsuranceName = insurance != null ? insurance.Name : string.Empty;
                    }
                }
            }
            return PartialView("FinalGrid",bill );
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RapGrid(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var bill = new Bill { Claims = billingService.AllUnProcessedRapsByDate(branchId, insuranceId, startDate, endDate, false, false) };
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null)
                {
                    bill.BranchId = location.Id;
                    bill.BranchName = location.Name;
                }
                if (insuranceId > 0)
                {
                    if (insuranceId > 0 && insuranceId < 1000)
                    {
                        var medicareInsurance = lookupRepository.GetInsurance(insuranceId);
                        bill.Insurance = insuranceId;
                        bill.InsuranceName = medicareInsurance != null ? medicareInsurance.Name : string.Empty;
                    }
                    else if (insuranceId >= 1000)
                    {
                        var insurance = agencyRepository.FindInsurance(Current.AgencyId, insuranceId);
                        bill.Insurance = insuranceId;
                        bill.InsuranceName = insurance != null ? insurance.Name : string.Empty;
                    }
                }
            }
            return PartialView("RapGrid", bill);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Unprocessed()
        {
            return Json(billingService.GetAllUnProcessedBill());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Rap(Guid episodeId, Guid patientId)
        {
            return PartialView("Rap", billingService.GetRap(patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult RapPdf(Guid episodeId, Guid patientId)
        {
            var doc = new BillingRapPdf(billingService.GetRapPrint(episodeId, patientId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Rap_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Final(Guid episodeId, Guid patientId)
        {
            if (episodeId.IsEmpty() || patientId.IsEmpty())
            {
                return PartialView("Final", new Final());
            }
            return PartialView("Final", billingRepository.GetFinal(Current.AgencyId, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult FinalPdf(Guid episodeId, Guid patientId)
        {
            var doc = new BillingFinalPdf(billingService.GetFinalPrint(episodeId, patientId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Final_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SuppliesPdf(Guid episodeId, Guid patientId)
        {
            var doc = new BillingSuppliesPdf(billingService.GetFinalWithSupplies(episodeId, patientId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=FinalSupplies_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Info(Guid episodeId, Guid patientId)
        {
            return PartialView("Info", billingService.GetFinalInfo(patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Visit(Guid episodeId, Guid patientId)
        {
            var claim = billingRepository.GetFinalOnly(Current.AgencyId, patientId, episodeId);
            if (claim != null)
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        claim.EpisodeStartDate = episode.StartDate;
                        claim.EpisodeEndDate = episode.EndDate;
                        if (episode.Schedule.IsNotNullOrEmpty())
                        {
                            var notVerifiedVisits = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.DisciplineTask!=(int)DisciplineTasks.PASTravel&& s.DisciplineTask!=(int)DisciplineTasks.PASVisit && s.VisitDate.IsValidDate() && s.VisitDate.ToDateTime().Date >= episode.StartDate.Date && s.VisitDate.ToDateTime().Date <= episode.EndDate.Date && s.DisciplineTask > 0).OrderBy(s => s.VisitDate.ToDateTime().Date).ToList();
                            if (notVerifiedVisits != null && notVerifiedVisits.Count > 0)
                            {
                                claim.AgencyLocationId = patient.AgencyLocationId;
                                var agencyInsurance = new AgencyInsurance();
                                claim.BillVisitDatas = billingService.BillableVisitsData(patient.AgencyLocationId, notVerifiedVisits, claim.PrimaryInsuranceId > 0 && claim.PrimaryInsuranceId < 1000 ? ClaimType.CMS : ClaimType.HMO, billingService.FinalToCharegRates(claim, out agencyInsurance), false);
                            }
                        }
                    }
                }
            }
            return PartialView("Visit", claim);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Supply(Guid episodeId, Guid patientId)
        {
            Final claim = null;
            var viewData = new ClaimSupplyViewData();
            if (!episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                claim = billingRepository.GetFinalOnly(Current.AgencyId, patientId,episodeId);
                if (claim != null)
                {
                    var supplies = claim.Supply.ToObject<List<Supply>>();
                    viewData.Id = claim.Id;
                    viewData.PatientId = claim.PatientId;
                    viewData.EpisodeId = episodeId;
                    viewData.EpisodeStartDate = claim.EpisodeStartDate;
                    viewData.EpisodeEndDate = claim.EpisodeEndDate;
                    viewData.IsSupplyNotBillable = claim.IsSupplyNotBillable;
                    
                    int id = 0;
                    supplies.ForEach(s =>
                    {
                        s.BillingId = id;
                        id++;
                    });
                    claim.Supply = supplies.ToXml();
                    billingRepository.UpdateFinal(claim);

                    viewData.BilledSupplies = supplies != null && supplies.Count > 0 ? supplies.Where(s => s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
                    viewData.UnbilledSupplies = supplies != null && supplies.Count > 0 ? supplies.Where(s => !s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
                }
            }
            return PartialView("Supply", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Summary(Guid episodeId, Guid patientId)
        {
            var claim = billingService.GetFinalInfo(patientId, episodeId);
            if (claim != null)
            {
                if (claim.VerifiedVisits.IsNotNullOrEmpty())
                {
                    var visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValidDate() && s.EventDate.IsValidDate()).OrderBy(s => s.VisitDate.ToDateTime().Date).ThenBy(s => s.EventDate.ToDateTime().Date).ToList();
                    if (visits != null && visits.Count > 0)
                    {
                        if (claim.IsVisitVerified)
                        {
                            var agencyInsurance = new AgencyInsurance();
                            var chargeRates = billingService.FinalToCharegRates(claim, out agencyInsurance);
                            claim.BillVisitSummaryDatas = billingService.BillableVisitSummary(claim.AgencyLocationId, visits, claim.PrimaryInsuranceId > 0 && claim.PrimaryInsuranceId < 1000 ? ClaimType.CMS : ClaimType.HMO, chargeRates, false, claim.EpisodeStartDate,agencyInsurance.RequireServiceLocation);
                        }
                    }
                    if (claim.Insurance.IsNotNullOrEmpty())
                    {
                        claim.AgencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                    }
                }
                claim.SupplyTotal = billingService.MedicareSupplyTotal(claim);
            }
            return PartialView("Summary", claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RapVerify(Rap claim, FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The RAP could not be verified." };
            var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
            if (patient != null)
            {
                if (claim != null)
                {
                    if (formCollection != null)
                    {
                        var keys = formCollection.AllKeys;
                        if (keys != null && keys.Length > 0)
                        {
                            claim.FirstBillableVisitDateFormat = keys.Contains("FirstBillableVisitDateFormatInput") ? formCollection["FirstBillableVisitDateFormatInput"] : string.Empty;
                            if (keys.Contains("Ub04Locator81"))
                            {
                                var locatorList = formCollection["Ub04Locator81"].ToArray();
                                var locators = new List<Locator>();
                                if (locatorList != null && locatorList.Length > 0)
                                {
                                    locatorList.ForEach(l =>
                                    {
                                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3"))
                                        {
                                            locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                                        }
                                    });
                                }
                                claim.Ub04Locator81cca = locators.ToXml();
                            }
                            if (keys.Contains("Ub04Locator39"))
                            {
                                var locatorList = formCollection["Ub04Locator39"].ToArray();
                                var locators = new List<Locator>();
                                if (locatorList != null && locatorList.Length > 0)
                                {
                                    locatorList.ForEach(l =>
                                    {
                                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2"))
                                        {
                                            locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"] });
                                        }
                                    });
                                }
                                claim.Ub04Locator39 = locators.ToXml();
                            }
                        }
                    }
                    var agencyInsurance = new AgencyInsurance();
                    if (claim.PrimaryInsuranceId >= 1000)
                    {
                        agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                    }
                    else if (claim.PrimaryInsuranceId > 0 && claim.PrimaryInsuranceId < 1000)
                    {
                        agencyInsurance = billingService.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, claim.PrimaryInsuranceId);
                    }
                    claim.Insurance = agencyInsurance.ToXml();
                    if (claim.IsValid)
                    {
                        if (billingRepository.VerifyRap(Current.AgencyId, claim))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.Rap, LogAction.RAPVerified, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The RAP was verified successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = claim.ValidationMessage;
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Patient does not exist. Please try again.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimSummary(List<Guid> ClaimSelected, Guid BranchId, int PrimaryInsurance, string Type)
        {
            return PartialView("ClaimSummary", billingService.ClaimToGenerate(ClaimSelected, BranchId, PrimaryInsurance, Type));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Generate(int ansiId)
        {
            var claimData = billingRepository.GetClaimData(Current.AgencyId, ansiId);
            string generateJsonClaim = claimData != null ? claimData.Data : string.Empty;
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] buffer = encoding.GetBytes(generateJsonClaim);
            Stream fileStream = new MemoryStream();
            fileStream.Write(buffer, 0, generateJsonClaim.Length);
            fileStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=billing{0}.txt", DateTime.Now.ToString("yyyyMMddHH:mm:ss:ff")));
            return new FileStreamResult(fileStream, "Text/Plain");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateANSI(List<Guid> ClaimSelected, Guid BranchId, int PrimaryInsurance, string Type)
        {
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateDirect(ClaimSelected, Type, ClaimCommandType.download, out claimDataOut, out billExchage, BranchId, PrimaryInsurance))
            {
                if (claimDataOut != null)
                {
                    return Json(new { isSuccessful = true, Id = claimDataOut.Id });
                }
            }
            else
            {
                if (billExchage != null)
                {
                    return Json(new { isSuccessful = false, Id = -1, errorMessage = billExchage.Message });
                }
            }
            return Json(new { isSuccessful = false, Id = -1, errorMessage = "Error processing the claim(s). Please try again" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitClaimDirectly(List<Guid> ClaimSelected, Guid BranchId, int PrimaryInsurance, string Type)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim(s) could not processed. Please try again." };
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateDirect(ClaimSelected,Type, ClaimCommandType.direct, out claimDataOut, out billExchage, BranchId, PrimaryInsurance))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The claim(s) were processed successfully.";
            }
            else
            {
                if (billExchage != null)
                {
                    viewData.errorMessage = billExchage.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateSingleManagedANSI(Guid Id)
        {
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    ClaimData claimDataOut = null;
                    BillExchange billExchage;
                    var ids = (new List<Guid>());
                    ids.Add(Id);
                    if (billingService.GenerateManaged(ids, ClaimCommandType.download, out claimDataOut, out billExchage, patient.AgencyLocationId, claim.PrimaryInsuranceId))
                    {
                        if (claimDataOut != null)
                        {
                            return Json(new { isSuccessful = true, Id = claimDataOut.Id });
                        }
                    }
                    else
                    {
                        if (billExchage != null)
                        {
                            return Json(new { isSuccessful = false, Id = -1, errorMessage = billExchage.Message });
                        }
                    }

                }
            }
            return Json(new { isSuccessful = false, Id = -1, errorMessage = "Error processing the claim(s). Please try again." });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateManagedANSI(List<Guid> ManagedClaimSelected, Guid BranchId, int PrimaryInsurance)
        {
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateManaged(ManagedClaimSelected, ClaimCommandType.download, out claimDataOut, out billExchage, BranchId, PrimaryInsurance))
            {
                if (claimDataOut != null)
                {
                    return Json(new { isSuccessful = true, Id = claimDataOut.Id });
                }
            }
            else
            {
                if (billExchage != null)
                {
                    return Json(new { isSuccessful = false, Id = -1, errorMessage = billExchage.Message });
                }
            }
            return Json(new { isSuccessful = false, Id = -1, errorMessage = "Error processing the claim(s). Please try again" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitManagedClaimDirectly(List<Guid> ManagedClaimSelected, Guid BranchId, int PrimaryInsurance)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim(s) could not processed. Please try again." };
            ClaimData claimDataOut = null;
            BillExchange billExchage;
            if (billingService.GenerateManaged(ManagedClaimSelected, ClaimCommandType.direct, out claimDataOut, out billExchage, BranchId, PrimaryInsurance))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The claim(s) were processed successfully.";
            }
            else
            {
                if (billExchage != null)
                {
                    viewData.errorMessage = billExchage.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult UB04Pdf(Guid patientId, Guid Id, string type)
        {
            var doc = new UB04Pdf(billingService.GetUBOFourInfo(patientId, Id, type), this.billingService, agencyRepository);
            MemoryStream stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=UB04_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult HCFA1500Pdf(Guid patientId, Guid Id, string type) {
            var doc = new HCFA1500Pdf(billingService.GetHCFA1500Info(patientId, Id, type), this.billingService, this.agencyRepository);
            MemoryStream stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HCFA1500_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult ManagedHCFA1500Pdf(Guid patientId, Guid Id)
        {
            var doc = new HCFA1500Pdf(billingService.GetHCFA1500InfoForManagedClaim(patientId, Id), this.billingService, this.agencyRepository);
            MemoryStream stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HCFA1500_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateStatus(List<Guid> ClaimSelected, string Type, string StatusType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Claim status could not be updated." };
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("rap"))
                {
                    if (billingService.UpdateRapStatus(ClaimSelected, StatusType))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "RAP claim updated successfully.";
                    }
                }
                else if (Type.IsEqual("final"))
                {
                    if (billingService.UpdateFinalStatus(ClaimSelected, StatusType))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Final (EOE) claim updated successfully.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitManagedClaims(List<Guid> ManagedClaimSelected, string StatusType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Claim(s) could not be submitted." };
            if (billingService.UpdateManagedClaimStatus(ManagedClaimSelected, StatusType))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Claim(s) submitted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalComplete(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Final(EOE) claim could not be completed." };
            if (!id.IsEmpty())
            {
                if (billingService.FinalComplete(id))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Final(EOE) claim completed successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult History()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var selectionLists = new FilterViewData();
            if (agency != null)
            {
                int payorType;
                if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agency.Payor, out payorType))
                {
                    selectionLists.Insurances = agencyService.Insurances(agency.Payor.ToString(), false, true);
                    selectionLists.SelecetdInsurance = agency.Payor;
                }
                else
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        selectionLists.Insurances = agencyService.Insurances(agencyMedicareInsurance.Id.ToString(), false, true);
                        selectionLists.SelecetdInsurance = agencyMedicareInsurance.Id.ToString();
                    }
                }
            }
            return PartialView("History", selectionLists);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedHistory()
        {
            var selectionLists = new FilterViewData();
            selectionLists.Insurances = agencyService.Insurances("0", true, true);
            selectionLists.SelecetdInsurance = "0";
            return PartialView("Managed/History", selectionLists);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedCenter()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var bill = new Bill();
            if (agency != null)
            {
                var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType != 3).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        ViewData["Branch"] = agencyMainBranch.Id;
                        ViewData["Insurance"] = agencyMedicareInsurance.Id;
                        bill.IsElectronicSubmssion = agencyMedicareInsurance.IsAxxessTheBiller;
                        bill.Claims = billingRepository.GetManagedClaimsByDate(Current.AgencyId, agencyMainBranch.Id, agencyMedicareInsurance.Id, (int)ManagedClaimStatus.ClaimCreated, DateTime.Now.AddDays(-64), DateTime.Now, false); 
                        bill.BranchName = agencyMainBranch.Name;
                        bill.ClaimType = ClaimTypeSubCategory.ManagedCare;
                        bill.InsuranceName = agencyMedicareInsurance.Name;
                        bill.Insurance = agencyMedicareInsurance.Id;
                        bill.InvoiceType = agencyMedicareInsurance.InvoiceType;
                        return PartialView("Managed/Center", bill);
                    }
                }
            }
            return PartialView("Managed/Center", bill);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedGrid(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var bill = new Bill { Claims = billingRepository.GetManagedClaimsByDate(Current.AgencyId, branchId, insuranceId, (int)ManagedClaimStatus.ClaimCreated, startDate, endDate, false) };
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null)
                {
                    bill.BranchId = location.Id;
                    bill.BranchName = location.Name;
                }
                if (insuranceId > 0)
                {
                    if (insuranceId > 0 && insuranceId < 1000)
                    {
                        var medicareInsurance = lookupRepository.GetInsurance(insuranceId);
                        bill.Insurance = insuranceId;
                        bill.InsuranceName = medicareInsurance != null ? medicareInsurance.Name : string.Empty;
                        
                    }
                    else if (insuranceId >= 1000)
                    {
                        var insurance = agencyRepository.FindInsurance(Current.AgencyId, insuranceId);
                        bill.Insurance = insuranceId;
                        bill.InsuranceName = insurance != null ? insurance.Name : string.Empty;
                        bill.InvoiceType = insurance.InvoiceType;
                    }
                }
            }
            return PartialView("Managed/ManagedGrid", bill);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingClaims()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var selectionLists = new FilterViewData();
            if (agency != null)
            {
                var agencyBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyBranch != null && !agencyBranch.Id.IsEmpty())
                {
                    selectionLists.Branches = agencyService.Branchs(agencyBranch.Id.ToString(), false);
                    selectionLists.SelecetdBranch = agencyBranch.Id;
                    int payorType;
                    if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agency.Payor, out payorType))
                    {
                        selectionLists.Insurances = agencyService.Insurances(agency.Payor.ToString(), false, true);
                        selectionLists.SelecetdInsurance = agency.Payor;
                    }
                    else
                    {
                        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                        if (agencyMedicareInsurance != null)
                        {
                            selectionLists.Insurances = agencyService.Insurances(agencyMedicareInsurance.Id.ToString(), false, true);
                            selectionLists.SelecetdInsurance = agencyMedicareInsurance.Id.ToString();
                        }
                    }
                }
                else
                {
                    var agencyBranches = agencyRepository.GetBranches(Current.AgencyId).FirstOrDefault();
                    if (agencyBranches != null)
                    {
                        selectionLists.Branches = agencyService.Branchs(agencyBranches.Id.ToString(), false);
                        selectionLists.SelecetdBranch = agencyBranches.Id;

                        int payorType;
                        if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agency.Payor, out payorType))
                        {
                            selectionLists.Insurances = agencyService.Insurances(agency.Payor.ToString(), false, true);
                            selectionLists.SelecetdInsurance = agency.Payor;
                        }
                        else
                        {
                            var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                            if (agencyMedicareInsurance != null)
                            {
                                selectionLists.Insurances = agencyService.Insurances(agencyMedicareInsurance.Id.ToString(), false, true);
                                selectionLists.SelecetdInsurance = agencyMedicareInsurance.Id.ToString();
                            }
                        }
                    }
                }
            }
            return PartialView("PendingClaims", selectionLists);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingClaimRap(Guid branchId, string insuranceId)
        {
            return PartialView("PendingClaimRap", billingService.PendingClaimRaps(branchId, insuranceId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingClaimRaps(Guid branchId, string insuranceId)
        {
            return View(new GridModel(billingService.PendingClaimRaps(branchId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateRapClaim(Guid Id, Guid branchId, string insuranceId, DateTime PaymentDate, double PaymentAmount, int Status)
        {
            billingService.UpdateRapClaimStatus(Id, PaymentDate, PaymentAmount, Status);
            return View(new GridModel(billingService.PendingClaimRaps(branchId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RapSnapShots(Guid Id)
        {
            var raps = billingRepository.GetRapSnapShots(Current.AgencyId, Id);
            return View(new GridModel(raps != null && raps.Count > 0 ? raps : new List<RapSnapShot>()));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateRapSnapShotsClaim(Guid Id, long BatchId, DateTime PaymentDate, double Payment, int Status)
        {
            billingService.UpdateSnapShot(Id, BatchId, Payment, PaymentDate, Status, "RAP");
            return View(new GridModel(billingRepository.GetRapSnapShots(Current.AgencyId, Id)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingClaimFinal(Guid branchId, string insuranceId)
        {
            return PartialView("PendingClaimFinal", billingService.PendingClaimFinals(branchId, insuranceId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateFinalClaim(Guid Id, Guid branchId, string insuranceId, DateTime PaymentDate, double PaymentAmount, int Status)
        {
            billingService.UpdateFinalClaimStatus(Id, PaymentDate, PaymentAmount, Status);
            return View(new GridModel(billingService.PendingClaimFinals(branchId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingClaimFinals(Guid branchId, string insuranceId)
        {
            return View(new GridModel(billingService.PendingClaimFinals(branchId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FinalSnapShots(Guid Id)
        {
            return View(new GridModel(billingRepository.GetFinalSnapShots(Current.AgencyId, Id)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateFinalSnapShotsClaim(Guid Id, long BatchId, DateTime PaymentDate, double Payment, int Status)
        {
            billingService.UpdateSnapShot(Id, BatchId, Payment, PaymentDate, Status, "Final");
            return View(new GridModel(billingRepository.GetFinalSnapShots(Current.AgencyId, Id)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SnapShotClaims(Guid Id, string Type)
        {
            return View(new GridModel(billingService.ClaimSnapShots(Id, Type)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSnapShotClaim(Guid Id, long BatchId, string Type, DateTime? PaymentDate, double PaymentAmountValue, int Status)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The batch claim could not be updated." };
            DateTime paymentDate = DateTime.MinValue;
            if (PaymentDate != null)
            {
                paymentDate = PaymentDate.Value;
            }
            if (billingService.UpdateSnapShot(Id, BatchId, PaymentAmountValue, paymentDate, Status, Type))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The batch claim was updated successfully.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HistoryActivity(Guid patientId, int insuranceId)
        {
            return View(new GridModel(billingService.Activity(patientId, insuranceId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimsActivity(Guid patientId, int insuranceId)
        {
            if (patientId.IsEmpty())
            {
                return View(new GridModel(new List<ManagedClaimLean>()));
            }
            var claims = billingRepository.GetManagedClaimsPerPatient(Current.AgencyId, patientId, insuranceId);
            var payments = billingRepository.GetManagedClaimPaymentsByPatient(Current.AgencyId, patientId);
            var adjustments = billingRepository.GetManagedClaimAdjustmentsByPatient(Current.AgencyId, patientId);
            foreach (var claim in claims)
            {
                if (payments != null && payments.Count > 0)
                {
                    var claimPayments = payments.Where(s => s.ClaimId == claim.Id).ToList();
                    if (claimPayments != null && claimPayments.Count > 0)
                    {
                        claim.PaymentAmount = claimPayments.Sum(p => p.Payment);
                        var primaryPayments = payments.Where(p => p.Payor == claim.PrimaryInsuranceId).ToList();
                        if (primaryPayments != null && primaryPayments.Count > 0)
                        {
                            claim.PaymentDate = primaryPayments.Aggregate((agg, next) => next.PaymentDate > agg.PaymentDate ? next : agg).PaymentDate;
                        }
                    }
                }
                if(adjustments != null && adjustments.Count > 0)
                {
                    var adjustmentSum = adjustments.Where(a => a.ClaimId == claim.Id).Sum(a => a.Adjustment);
                    claim.AdjustmentAmount = adjustmentSum;
                }
            }
            return View(new GridModel(claims));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfoVerify(Final claim, FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final Basic Info is not verified." };
            var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
            if (patient != null)
            {
                if (claim != null)
                {
                    if (formCollection != null)
                    {
                        var keys = formCollection.AllKeys;
                        if (keys != null && keys.Length > 0)
                        {
                            claim.FirstBillableVisitDateFormat = keys.Contains("FirstBillableVisitDateFormatInput") ? formCollection["FirstBillableVisitDateFormatInput"] : string.Empty;
                            if (keys.Contains("Ub04Locator81"))
                            {
                                var locatorList = formCollection["Ub04Locator81"].ToArray();
                                var locators = new List<Locator>();
                                if (locatorList != null && locatorList.Length > 0)
                                {
                                    locatorList.ForEach(l =>
                                    {
                                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3"))
                                        {
                                            locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                                        }
                                    });
                                }
                                claim.Ub04Locator81cca = locators.ToXml();
                            }
                            if (keys.Contains("Ub04Locator39"))
                            {
                                var locatorList = formCollection["Ub04Locator39"].ToArray();
                                var locators = new List<Locator>();
                                if (locatorList != null && locatorList.Length > 0)
                                {
                                    locatorList.ForEach(l =>
                                    {
                                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2"))
                                        {
                                            locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"] });
                                        }
                                    });
                                }
                                claim.Ub04Locator39 = locators.ToXml();
                            }
                            if (keys.Contains("Ub04Locator34"))
                            {
                                var locatorList = formCollection["Ub04Locator34"].ToArray();
                                var locators = new List<Locator>();
                                if (locatorList != null && locatorList.Length > 0)
                                {
                                    locatorList.ForEach(l =>
                                    {
                                        if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2") && keys.Contains(l + "_Code3"))
                                        {
                                            locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"], Code3 = formCollection[l + "_Code3"] });
                                        }
                                        else if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2"))
                                        {
                                            locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"].IsNotNullOrEmpty() ? formCollection[l + "_Code2"].Replace("/", "") : string.Empty });
                                        }
                                    });
                                }
                                claim.Ub04Locator34 = locators.ToXml();
                            }

                            for (int locatorCount = 1; locatorCount <= 3; locatorCount++)
                            {
                                if (keys.Contains("Ub04Locator3" + locatorCount))
                                {
                                    var locatorList = formCollection["Ub04Locator3" + locatorCount].ToArray();
                                    var locators = new List<Locator>();
                                    if (locatorList != null && locatorList.Length > 0)
                                    {
                                        locatorList.ForEach(l =>
                                        {
                                            if (keys.Contains(l + "_Code1") && keys.Contains(l + "_Code2"))
                                            {
                                                locators.Add(new Locator { LocatorId = l, Code1 = formCollection[l + "_Code1"], Code2 = formCollection[l + "_Code2"].IsNotNullOrEmpty() ? formCollection[l + "_Code2"].Replace("/","") : string.Empty });
                                            }
                                        });
                                    }
                                    switch (locatorCount)
                                    {
                                        case 1: claim.Ub04Locator31 = locators.ToXml();
                                            break;
                                        case 2: claim.Ub04Locator32 = locators.ToXml();
                                            break;
                                        case 3: claim.Ub04Locator33 = locators.ToXml();
                                            break;
                                        //case 4: claim.Ub04Locator34 = locators.ToXml();
                                        //    break;
                                    }
                                }
                            }
                        }
                    }
                    if (claim.PrimaryInsuranceId > 0)
                    {
                        var agencyInsurance = new AgencyInsurance();

                        if (claim.PrimaryInsuranceId >= 1000)
                        {
                            agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                        }
                        else if (claim.PrimaryInsuranceId < 1000)
                        {
                            agencyInsurance = billingService.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, claim.PrimaryInsuranceId);
                        }
                        claim.Insurance = agencyInsurance.ToXml();
                    }
                    if (claim.IsValid)
                    {
                        if (billingRepository.VerifyInfo(Current.AgencyId, claim))
                        {
                            Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.Final, LogAction.FinalDemographicsVerified, string.Empty);
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Final claim info successfully verified.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = claim.ValidationMessage;
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Patient does not exist. Please try again.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> Visit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final claim could not be verified." };
            if (billingService.VisitVerify(Id, episodeId, patientId, Visit))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalVisitVerified, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Final claim (Visits) successfully verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyVerify(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Final claim could not be verified." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() && formCollection["Id"].IsGuid() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var episodeId = keys.Contains("episodeId") && formCollection["episodeId"].IsNotNullOrEmpty() && formCollection["episodeId"].IsGuid() ? formCollection["episodeId"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("patientId") && formCollection["patientId"].IsNotNullOrEmpty() && formCollection["patientId"].IsGuid() ? formCollection["patientId"].ToGuid() : Guid.Empty;
                var IsSupplyNotBillable = keys.Contains("IsSupplyNotBillable") && formCollection["IsSupplyNotBillable"].IsNotNullOrEmpty() &&  formCollection["IsSupplyNotBillable"].IsBoolean() ? formCollection["IsSupplyNotBillable"].ToBoolean() : true;
                if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                {
                    if (billingService.VisitSupply(Id, episodeId, patientId, IsSupplyNotBillable))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalSupplyVerified, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Final claim (Supplies) successfully verified.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(Guid patientId, Guid id, string type)
        {
            return PartialView("Update", billingService.GetClaimViewData(patientId, id, type));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveUpdate(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var type = keys.Contains("Type") && formCollection["Type"].IsNotNullOrEmpty() ? formCollection["Type"].ToString() : string.Empty;
                var claimStatus = keys.Contains("Status") && formCollection["Status"].IsNotNullOrEmpty() && formCollection["Status"].IsInteger() ? formCollection["Status"].ToInteger() : 0;
                // var primaryInsuranceId = keys.Contains("PrimaryInsuranceId") && formCollection["PrimaryInsuranceId"].IsNotNullOrEmpty() && formCollection["PrimaryInsuranceId"].IsInteger() ? formCollection["PrimaryInsuranceId"].ToInteger() : 0;
                var comment = keys.Contains("Comment") && formCollection["Comment"].IsNotNullOrEmpty() ? formCollection["Comment"].ToString() : string.Empty;

                rules.Add(new Validation(() => keys.Contains("PaymentAmount") && formCollection["PaymentAmount"].IsNotNullOrEmpty() ? !formCollection["PaymentAmount"].IsDouble() : false, "Payment Value is not a right format."));
                rules.Add(new Validation(() => keys.Contains("PaymentDateValue") && formCollection["PaymentDateValue"].IsNotNullOrEmpty() ? !formCollection["PaymentDateValue"].IsValidDate() : false, "Payment date is not a right format."));

                if (!Id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty())
                {
                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            var paymentAmount = formCollection["PaymentAmount"].IsNotNullOrEmpty() ? formCollection["PaymentAmount"].ToDouble() : 0;
                            var paymentDate = formCollection["PaymentDateValue"].IsDate() ? formCollection["PaymentDateValue"].ToDateTime() : DateTime.MinValue;
                            if (billingService.UpdateProccesedClaimStatus(patient, Id, type, paymentDate, paymentAmount, paymentDate, claimStatus, comment))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "The claim updated successfully.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message; ;
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The patient information does not exist.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimInfo(Guid patientId, Guid claimId, string claimType)
        {
            return PartialView("ClaimInfo", billingService.GetClaimSnapShotInfo(patientId, claimId, claimType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimData(Guid patientId, Guid claimId, string claimType)
        {
            var viewData = new BillingHistoryViewData();
            viewData.ClaimInfo = billingService.GetClaimSnapShotInfo(patientId, claimId, claimType);
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateClaim(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var episodeId = keys.Contains("EpisodeId") && formCollection["EpisodeId"].IsNotNullOrEmpty() ? formCollection["EpisodeId"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var insuranceId = keys.Contains("InsuranceId") && formCollection["InsuranceId"].IsNotNullOrEmpty() ? formCollection["InsuranceId"].ToInteger() : 0;
                var type = keys.Contains("Type") && formCollection["Type"].IsNotNullOrEmpty() ? formCollection["Type"].ToString() : string.Empty;
                if (!episodeId.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty())
                {
                    rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "Patient dosn't exist."));
                    rules.Add(new Validation(() => !patientRepository.IsEpisodeExist(Current.AgencyId, episodeId), "Episode dosn't exist for this claim."));
                    rules.Add(new Validation(() => billingService.IsEpisodeHasClaim(episodeId, patientId, type), "Episode already has claim."));
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.AddClaim(patientId, episodeId, type, insuranceId))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim updated successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message; ;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Please verify the information provided.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewClaim(Guid patientId, string type)
        {
            var newClaimData = new NewClaimViewData();
            newClaimData.EpisodeData = billingRepository.GetEpisodeNeedsClaim(Current.AgencyId, patientId, type);
            newClaimData.Type = type;
            newClaimData.PatientId = patientId;
            return PartialView("NewClaim", newClaimData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewManagedClaim(Guid patientId)
        {
            var newClaimData = new NewManagedClaimViewData();
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    newClaimData.Insurances = agencyService.Insurances(patient.PrimaryInsurance, false, false);
                    newClaimData.SelecetdInsurance = patient.PrimaryInsurance;
                    newClaimData.PatientId = patientId;
                }
            }
            return PartialView("Managed/NewManagedClaim", newClaimData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateManagedClaim(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var insuranceId = keys.Contains("InsuranceId") && formCollection["InsuranceId"].IsNotNullOrEmpty() && formCollection["InsuranceId"].IsInteger() ? formCollection["InsuranceId"].ToInteger() : -1;
                var startDate = keys.Contains("StartDate") && formCollection["StartDate"].IsNotNullOrEmpty() && formCollection["StartDate"].IsValidDate() ? formCollection["StartDate"].ToDateTime() : DateTime.MinValue;
                var endDate = keys.Contains("EndDate") && formCollection["EndDate"].IsNotNullOrEmpty() && formCollection["EndDate"].IsValidDate() ? formCollection["EndDate"].ToDateTime() : DateTime.MinValue;
                if (!patientId.IsEmpty())
                {
                    rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "Patient record not found."));
                    rules.Add(new Validation(() => startDate.Date <= DateTime.MinValue.Date, "Claim Start Date is invalid."));
                    rules.Add(new Validation(() => endDate.Date <= DateTime.MinValue.Date, "Claim End Date is invalid."));
                    rules.Add(new Validation(() => startDate.Date > endDate.Date, "Claim Start date must be earlier than the Claim End Date."));
                    rules.Add(new Validation(() => insuranceId <= 0, "No insurance provided."));
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.AddManagedClaim(patientId, startDate, endDate, insuranceId))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim was created successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message; ;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Please verify the information provided.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaim(Guid Id, Guid patientId)
        {
            if (Id.IsEmpty() || patientId.IsEmpty())
            {
                return PartialView("Managed/Edit", new ManagedClaim());
            }
            return PartialView("Managed/Edit", billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedClaimInfo(Guid Id, Guid patientId)
        {
            return PartialView("Managed/Info", billingService.GetManagedClaimInfo(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimEpisodes(Guid managedClaimId, Guid patientId)
        {
            return PartialView("Managed/MultipleEpisodes", billingService.GetManagedClaimEpisodes(patientId, managedClaimId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedClaimInsurance(Guid Id, Guid patientId)
        {
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                if (claim.Insurance.IsNotNullOrEmpty())
                {
                    claim.AgencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                    claim.CBSA = lookupRepository.CbsaCodeByZip(claim.AddressZipCode);
                }
                else if(claim.PrimaryInsuranceId > 0)
                {
                    if (claim.PrimaryInsuranceId >= 1000)
                    {
                        claim.AgencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                    }
                    else if (claim.PrimaryInsuranceId > 0 && claim.PrimaryInsuranceId < 1000)
                    {
                        var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
                        if (patient != null)
                        {
                            claim.AgencyInsurance = billingService.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, claim.PrimaryInsuranceId);
                        }
                    }
                    claim.CBSA = lookupRepository.CbsaCodeByZip(claim.AddressZipCode);
                }
                if (!claim.IsInsuranceVerified && claim.AgencyInsurance!=null && claim.AgencyInsurance.Ub04Locator81cca.IsNotNullOrEmpty())
                {
                    var HCFAlocators = claim.AgencyInsurance.HCFALocators.ToObject<List<Locator>>();
                    if (HCFAlocators.IsNotNullOrEmpty())
                    {
                        HCFAlocators.RemoveAll(h => !h.LocatorId.Equals("24Locator"));
                        if (HCFAlocators.IsNotNullOrEmpty() && HCFAlocators.Count>0)
                        {
                            claim.HCFALocators = HCFAlocators.ToXml();
                        }
                    }
                    var locators = claim.AgencyInsurance.Ub04Locator81cca.ToObject<List<Locator>>();
                    locators.ForEach(l =>
                    {
                        if (l.LocatorId.Equals("Locator76") && !l.Customized)
                        {
                            l.Code1 = claim.PhysicianNPI;
                            l.Code2 = claim.PhysicianLastName;
                            l.Code3 = claim.PhysicianFirstName;
                        }
                        else if ((l.LocatorId.Equals("Locator77") || l.LocatorId.Equals("Locator78") || l.LocatorId.Equals("Locator79")) && l.Customized)
                        {
                            if (l.PhysicianType.Equals("PCP"))
                            {
                                l.Code1 = claim.PhysicianNPI;
                                l.Code2 = claim.PhysicianLastName;
                                l.Code3 = claim.PhysicianFirstName;
                            }
                            else if (l.PhysicianType.Equals("referring"))
                            {
                                var physician = patientRepository.GetPatientReferralPhysician(Current.AgencyId, claim.PatientId);
                                l.Code1 = physician.NPI;
                                l.Code2 = physician.LastName;
                                l.Code3 = physician.FirstName;
                            }
                        }
                    });
                    claim.Ub04Locator81cca = locators.ToXml();
                }
            }
            return PartialView("Managed/Insurance", claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimReloadInsurance(Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Insurance could not be reloaded." };

            var claim = billingRepository.GetManagedClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                AgencyInsurance agencyInsurance = null;
                if (claim.PrimaryInsuranceId >= 1000)
                {
                    agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                }
                else if (claim.PrimaryInsuranceId > 0 && claim.PrimaryInsuranceId < 1000)
                {
                    var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
                    if (patient != null)
                    {
                        agencyInsurance = billingService.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, claim.PrimaryInsuranceId);
                    }
                }
                if (agencyInsurance != null)
                {
                    claim.HCFALocators = agencyInsurance.HCFALocators;
                    claim.Ub04Locator31 = "";
                    claim.Ub04Locator32 = "";
                    claim.Ub04Locator33 = "";
                    claim.Ub04Locator34 = "";
                    claim.Ub04Locator39 = "";
                    claim.Ub04Locator81cca = agencyInsurance.Ub04Locator81cca;
                    claim.Insurance = agencyInsurance.ToXml();
                    if(billingRepository.UpdateManagedClaimModel(claim))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Insurance was successfully reloaded.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedInsuranceVerify(Guid Id, Guid patientId, FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Insurance could not be verified." };

            var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                if (formCollection != null)
                {
                    var keys = formCollection.AllKeys;
                    if (keys != null && keys.Length > 0)
                    {
                        if (keys.Contains("Ub04Locator81"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator81");
                            claim.Ub04Locator81cca = locators.ToXml();
                        }
                        if (keys.Contains("Ub04Locator39"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator39");
                            claim.Ub04Locator39 = locators.ToXml();
                        }
                        if (keys.Contains("Ub04Locator31"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator31");
                            claim.Ub04Locator31 = locators.ToXml();
                        }
                        if (keys.Contains("Ub04Locator32"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator32");
                            claim.Ub04Locator32 = locators.ToXml();
                        }
                        if (keys.Contains("Ub04Locator33"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator33");
                            claim.Ub04Locator33 = locators.ToXml();
                        }
                        if (keys.Contains("Ub04Locator34"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator34");
                            claim.Ub04Locator34 = locators.ToXml();
                        }
                        if (keys.Contains("HCFALocators"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "HCFALocators");
                            claim.HCFALocators = locators.ToXml();
                        }
                    }
                }
                if (billingRepository.ManagedVerifyInsurance(claim, Current.AgencyId))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedInsuranceVerified, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Managed Claim Insurance successfully verified.";
                }
            }
            return Json(viewData);
        }

        [GridAction]
        public ActionResult ManagedClaimInsuranceRates(Guid Id)
        {
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, Id);
            List<ChargeRate> rates = new List<ChargeRate>();
            if (claim != null)
            {
                if (claim.Insurance.IsNullOrEmpty())
                {
                    var agencyInsurance = new AgencyInsurance();
                    var chargeRates = billingService.ManagedToChargeRates(claim, out agencyInsurance);
                    if(chargeRates != null && chargeRates.Count > 0)
                    {
                        rates = chargeRates.Select(c => c.Value).ToList();
                    }
                }
                else
                {
                    var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                    if(agencyInsurance != null && agencyInsurance.BillData.IsNotNullOrEmpty())
                    {
                        var chargeRates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
                        if (chargeRates != null && chargeRates.Count > 0)
                        {
                            rates = chargeRates;
                        }
                    }
                }
            }
            return View(new GridModel(rates));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimNewBillData(Guid ClaimId)
        {
            var viewData = new NewBillDataViewData() { Id = ClaimId, TypeOfClaim = "Managed", IsMedicareHMO = false };
            var claim = billingRepository.GetManagedClaimInsurance(Current.AgencyId, ClaimId);
            if (claim != null && claim.Insurance.IsNotNullOrEmpty())
            {
                var insurance = claim.Insurance.ToObject<AgencyInsurance>();
                if (insurance != null)
                {
                    viewData.IsMedicareHMO = insurance.PayorType == 2;
                }
            }
            return PartialView("BillData/New", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimEditBillData(Guid ClaimId, int Id)
        {
            var viewData = new EditBillDataViewData() { Id = ClaimId, TypeOfClaim = "Managed", ChargeRate = new ChargeRate() };
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, ClaimId);
            if (claim != null)
            {
                if (claim.Insurance.IsNotNullOrEmpty())
                {
                    var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                    var rates = agencyInsurance.ToBillDataDictionary();
                    if (rates.ContainsKey(Id.ToString()))
                    {
                        viewData.ChargeRate = rates[Id.ToString()];
                    }
                }
            }
            return PartialView("BillData/Edit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimDeleteBillData(Guid ClaimId, int Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be deleted." };
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, ClaimId);
            if (claim != null && claim.Insurance.IsNotNullOrEmpty())
            {
                var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                var rates = agencyInsurance.ToBillDataDictionary();
                if (rates.ContainsKey(Id.ToString()))
                {
                    if (rates.Remove(Id.ToString()))
                    {
                        agencyInsurance.BillData = rates.Select(r => r.Value).ToList().ToXml();
                        claim.Insurance = agencyInsurance.ToXml();
                        if (billingRepository.UpdateManagedClaimModel(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Charge rate successfully deleted.";
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimAddBillData(ChargeRate chargeRate, Guid ClaimId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be updated." };
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, ClaimId);
            if (claim != null)
            {
                if (claim.Insurance.IsNotNullOrEmpty())
                {
                    var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                    if (agencyInsurance != null)
                    {
                        var rates = agencyInsurance.BillData.IsNotNullOrEmpty() ? agencyInsurance.BillData.ToObject<List<ChargeRate>>() : new List<ChargeRate>();
                        rates.Add(chargeRate);
                        agencyInsurance.BillData = rates.ToXml();
                        claim.Insurance = agencyInsurance.ToXml();
                        if (billingRepository.UpdateManagedClaimModel(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Charge rate successfully updated.";
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimUpdateBillData(ChargeRate chargeRate, Guid ClaimId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be updated." };
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, ClaimId);
            if (claim != null)
            {
                if (claim.Insurance.IsNotNullOrEmpty())
                {
                    var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                    if (agencyInsurance != null)
                    {
                        var rates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var oldRate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                            if (oldRate != null)
                            {
                                oldRate.PreferredDescription = chargeRate.PreferredDescription;
                                oldRate.Code = chargeRate.Code;
                                oldRate.RevenueCode = chargeRate.RevenueCode;
                                oldRate.Charge = chargeRate.Charge;
                                oldRate.Modifier = chargeRate.Modifier;
                                oldRate.Modifier2 = chargeRate.Modifier2;
                                oldRate.Modifier3 = chargeRate.Modifier3;
                                oldRate.Modifier4 = chargeRate.Modifier4;
                                oldRate.ChargeType = chargeRate.ChargeType;
                                oldRate.IsUnitsPerDayOnSingleLineItem = chargeRate.IsUnitsPerDayOnSingleLineItem;
                                if (oldRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || oldRate.ChargeType == ((int)BillUnitType.Hourly).ToString())
                                {
                                    if (chargeRate.IsTimeLimit)
                                    {

                                        oldRate.TimeLimitHour = chargeRate.TimeLimitHour;
                                        oldRate.TimeLimitMin = chargeRate.TimeLimitMin;
                                        oldRate.SecondDescription = chargeRate.SecondDescription;
                                        oldRate.SecondCode = chargeRate.SecondCode;
                                        oldRate.SecondRevenueCode = chargeRate.SecondRevenueCode;
                                        oldRate.IsSecondChargeDifferent = chargeRate.IsSecondChargeDifferent;
                                        if (chargeRate.IsSecondChargeDifferent)
                                        {
                                            oldRate.SecondCharge = chargeRate.SecondCharge;
                                        }
                                        else
                                        {
                                            oldRate.SecondCharge = 0;
                                        }
                                        oldRate.SecondModifier = chargeRate.SecondModifier;
                                        oldRate.SecondModifier2 = chargeRate.SecondModifier2;
                                        oldRate.SecondModifier3 = chargeRate.SecondModifier3;
                                        oldRate.SecondModifier4 = chargeRate.SecondModifier4;
                                        oldRate.SecondChargeType = chargeRate.SecondChargeType;
                                        if (oldRate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString())
                                        {
                                            oldRate.SecondUnit = chargeRate.SecondUnit;
                                        }
                                        else
                                        {
                                            oldRate.SecondUnit = 0;
                                        }
                                        oldRate.IsUnitPerALineItem = chargeRate.IsUnitPerALineItem;
                                    }
                                    oldRate.IsTimeLimit = chargeRate.IsTimeLimit;
                                }
                                else if (oldRate.ChargeType == ((int)BillUnitType.PerVisit).ToString())
                                {
                                    oldRate.Unit = chargeRate.Unit;
                                }
                                if (agencyInsurance.PayorType == (int)PayerTypes.MedicareHMO && (oldRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || oldRate.ChargeType == ((int)BillUnitType.Hourly).ToString()))
                                {
                                    oldRate.MedicareHMORate = chargeRate.MedicareHMORate;
                                }

                                agencyInsurance.BillData = rates.ToXml();
                                claim.Insurance = agencyInsurance.ToXml();
                                if (billingRepository.UpdateManagedClaimModel(claim))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Charge rate successfully updated.";
                                }
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedClaimVisit(Guid Id, Guid patientId)
        {
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var visits = patientRepository.GetScheduledEventsOnly(Current.AgencyId, patientId, claim.EpisodeStartDate, claim.EpisodeEndDate);
                    var overlapClaims = billingRepository.GetManagedClaimByPatientId(Current.AgencyId, patientId, claim.EpisodeStartDate, claim.EpisodeEndDate, claim.Id);
                    var overlapVisitIds = new List<Guid>();
                    var overlapVisits = new List<ScheduleEvent>();
                    if (overlapClaims.IsNotNullOrEmpty())
                    {

                        overlapClaims.ForEach(e =>
                        {
                            if (e.VerifiedVisits.IsNotNullOrEmpty())
                            {
                                var ids = e.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(v => v.EventDate.IsValidDate() && v.EventDate.ToDateTime() >= claim.EpisodeStartDate && v.EventDate.ToDateTime() <= claim.EpisodeEndDate).Select(v => v.EventId).Distinct();
                                if (ids != null && ids.Count() > 0)
                                {
                                    //overlapVisitIds.AddRange(ids);
                                    foreach (Guid i in ids)
                                    {
                                        var s = new ScheduleEvent();
                                        s.ClaimStartDate = e.EpisodeStartDate;
                                        s.ClaimEndDate = e.EpisodeEndDate;
                                        s.EventId = i;
                                        overlapVisits.Add(s);
                                    }
                                }
                            }
                        });
                    }
                    var unbilledVisit = new List<ScheduleEvent>();
                    claim.BilledVisits = new List<ScheduleEvent>();
                    claim.Visits = new List<ScheduleEvent>();
                    if (visits.IsNotNullOrEmpty())
                    {
                        foreach (ScheduleEvent s in visits)
                        {
                            var existingSchedule = overlapVisits.Find(o => o.EventId == s.EventId);
                            if (existingSchedule != null)
                            {
                                s.ClaimStartDate = existingSchedule.ClaimStartDate;
                                s.ClaimEndDate = existingSchedule.ClaimEndDate;
                                claim.BilledVisits.Add(s);
                            }
                            else
                            {
                                claim.Visits.Add(s);
                            }
                        }
                    }
                    
                    if (claim.Visits != null && claim.Visits.Count > 0)
                    {
                        claim.AgencyLocationId = patient.AgencyLocationId;
                        var agencyInsurance = new AgencyInsurance();
                        var chargeRates = billingService.ManagedToChargeRates(claim, out agencyInsurance);
                        claim.BillVisitDatas = billingService.BillableVisitsData(patient.AgencyLocationId, claim.Visits, ClaimType.MAN, chargeRates, true);
                        claim.AgencyInsurance = agencyInsurance;
                    }
                    if (claim.BilledVisits != null && claim.BilledVisits.Count > 0)
                    {
                        //claim.AgencyLocationId = patient.AgencyLocationId;
                        var agencyInsurance = new AgencyInsurance();
                        var chargeRates = billingService.ManagedToChargeRates(claim, out agencyInsurance);
                        claim.BilledVisitDatas = billingService.BillableVisitsData(patient.AgencyLocationId, claim.BilledVisits, ClaimType.MAN, chargeRates, true);
                    }
                }
            }
            return PartialView("Managed/Visit", claim);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedClaimSupply(Guid Id, Guid patientId)
        {
            var viewData = new ClaimSupplyViewData();
            ManagedClaim claim = null;
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                if (claim != null)
                {
                    var supplies = claim.Supply.ToObject<List<Supply>>();
                    if (supplies != null)
                    {
                        int id = 0;
                        supplies.ForEach(s =>
                        {
                            s.BillingId = id;
                            id++;
                        });
                    }
                    else
                    {
                        supplies = new List<Supply>();
                    }
                    claim.Supply = supplies.ToXml();
                    billingRepository.UpdateManagedClaimModel(claim);
                    viewData.Id = claim.Id;
                    viewData.PatientId = claim.PatientId;
                    viewData.EpisodeStartDate = claim.EpisodeStartDate;
                    viewData.EpisodeEndDate = claim.EpisodeEndDate;
                    viewData.BilledSupplies = supplies != null && supplies.Count > 0 ? supplies.Where(s => s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
                    viewData.UnbilledSupplies = supplies != null && supplies.Count > 0 ? supplies.Where(s => !s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
                }
            }
            return PartialView("Managed/Supply", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedSummary(Guid Id, Guid patientId)
        {
            var claim = billingService.GetManagedClaimInfo(patientId, Id);
            if (claim != null)
            {
                if (claim.VerifiedVisits.IsNotNullOrEmpty())
                {
                    var visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValidDate() && s.EventDate.IsValidDate()).OrderBy(s => s.VisitDate.ToDateTime().Date).ThenBy(s => s.EventDate.ToDateTime().Date).ToList();
                    if (visits != null && visits.Count > 0)
                    {
                        if (claim.IsVisitVerified)
                        {
                            var agencyInsurance = new AgencyInsurance();
                            var rates = billingService.ManagedToChargeRates(claim, out agencyInsurance);
                            claim.BillVisitSummaryDatas = billingService.BillableVisitSummary(claim.AgencyLocationId, visits, ClaimType.MAN, rates, true, claim.EpisodeStartDate, agencyInsurance.RequireServiceLocation);
                        }
                    }
                }
            }
            return PartialView("Managed/Summary", claim);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyBillable(Guid Id, Guid patientId, string Type)
        {
            var supplies = new List<Supply>();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                if ("Final".Equals(Type))
                {
                    var claim = billingRepository.GetFinalSupplies(Current.AgencyId, Id);
                    if (claim != null && claim.Supply.IsNotNullOrEmpty())
                    {
                        supplies = billingService.GetSupplies(claim.Supply, true, claim.IsSupplyVerified);
                    }
                }
                else if ("Managed".Equals(Type))
                {
                    var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                    if (claim != null && claim.Supply.IsNotNullOrEmpty())
                    {
                        supplies = billingService.GetSupplies(claim.Supply, true, claim.IsSupplyVerified);
                    }
                }
                else if ("Secondary".Equals(Type))
                {
                    var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
                    if (claim != null && claim.Supply.IsNotNullOrEmpty())
                    {
                        supplies = billingService.GetSupplies(claim.Supply, true, claim.IsSupplyVerified);
                    }
                }
            }
            return View(new GridModel(supplies));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyUnBillable(Guid Id, Guid patientId, string Type)
        {
            var supplies = new List<Supply>();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                if ("Final".Equals(Type))
                {
                    var claim = billingRepository.GetFinalSupplies(Current.AgencyId, Id);
                    if (claim != null && claim.Supply.IsNotNullOrEmpty())
                    {
                        supplies = billingService.GetSupplies(claim.Supply, false, claim.IsSupplyVerified);
                    }
                }
                else if ("Managed".Equals(Type))
                {
                    var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                    if (claim != null && claim.Supply.IsNotNullOrEmpty())
                    {
                        supplies = billingService.GetSupplies(claim.Supply, false, claim.IsSupplyVerified);
                    }
                }
                else if ("Secondary".Equals(Type))
                {
                    var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
                    if (claim != null && claim.Supply.IsNotNullOrEmpty())
                    {
                        supplies = billingService.GetSupplies(claim.Supply, false, claim.IsSupplyVerified);
                    }
                }
            }
            return View(new GridModel(supplies));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeSupplyBillStatus(Guid Id, Guid PatientId, List<int> BillingId, bool IsBillable, string Type)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supply bill status change was unsuccessful." };
            if (!Id.IsEmpty() && !PatientId.IsEmpty())
            {
                if (BillingId != null && BillingId.Count > 0)
                {
                    if ("Final".Equals(Type))
                    {
                        var claim = billingRepository.GetFinalOnly(Current.AgencyId, PatientId, Id);
                        if (claim != null && claim.Supply.IsNotNullOrEmpty())
                        {
                            claim.Supply = ChangeStatusOfSuppliesInXml(claim.Supply, IsBillable, BillingId);
                            if (billingRepository.UpdateFinal(claim))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Supplies successfully deleted.";
                            }
                        }
                    }
                    else if ("Managed".Equals(Type))
                    {
                        var claim = billingRepository.GetManagedClaim(Current.AgencyId, PatientId, Id);
                        if (claim != null && claim.Supply.IsNotNullOrEmpty())
                        {
                            claim.Supply = ChangeStatusOfSuppliesInXml(claim.Supply, IsBillable, BillingId);
                            if (billingRepository.UpdateManagedClaimModel(claim))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Supplies successfully deleted.";
                            }
                        }
                    }
                    else if ("Secondary".Equals(Type))
                    {
                        var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, PatientId, Id);
                        if (claim != null && claim.Supply.IsNotNullOrEmpty())
                        {
                            claim.Supply = ChangeStatusOfSuppliesInXml(claim.Supply, IsBillable, BillingId);
                            if (billingRepository.UpdateSecondaryClaimModel(claim))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Supplies successfully deleted.";
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewSupplyBillable(Guid Id, Guid patientId, string Type)
        {
            return View("Supply/New", new ClaimNewSupplyViewData(Id, patientId, Type)); 
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyBillableAdd(Guid ClaimId, Guid patientId, string Type, Supply supply)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply could not be added." };
            if (!ClaimId.IsEmpty() && !patientId.IsEmpty())
            {
                if ("Final".Equals(Type))
                {
                    var claim = billingRepository.GetFinalOnly(Current.AgencyId, ClaimId);
                    if (claim != null)
                    {
                        claim.Supply = AddSupplyToXml(claim.Supply, supply);
                        if (billingRepository.UpdateFinal(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Supply successfully added.";
                        }
                    }
                }
                else if ("Managed".Equals(Type))
                {
                    var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, ClaimId);
                    if (claim != null)
                    {
                        claim.Supply = AddSupplyToXml(claim.Supply, supply);
                        if (billingRepository.UpdateManagedClaimModel(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Supply successfully added.";
                        }
                    }
                }
                else if ("Secondary".Equals(Type))
                {
                    var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, ClaimId);
                    if (claim != null)
                    {
                        claim.Supply = AddSupplyToXml(claim.Supply, supply);
                        if (billingRepository.UpdateSecondaryClaimModel(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Supply successfully added.";
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSupplyBillable(Guid ClaimId, Guid patientId, int Id, string Type)
        {
            var supply = new Supply();
            if ("Final".Equals(Type))
            {
                var claim = billingRepository.GetFinalSupplies(Current.AgencyId, ClaimId);
                if(claim != null)
                {
                    supply = GetSupplyFromXml(claim.Supply, Id);
                }
            }
            else if ("Managed".Equals(Type))
            {
                var claim = billingRepository.GetManagedClaim(Current.AgencyId, ClaimId);
                if (claim != null)
                {
                    supply = GetSupplyFromXml(claim.Supply, Id);
                }
            }
            else if ("Secondary".Equals(Type))
            {
                var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, ClaimId);
                if (claim != null)
                {
                    supply = GetSupplyFromXml(claim.Supply, Id);
                }
            }
            return View("Supply/Edit", new ClaimEditSupplyViewData(ClaimId, patientId, Type, supply));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyBillableUpdate(Guid ClaimId, Guid patientId, Supply supply, string Type)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The supply could not be updated." };
            var suppliesEdited = new List<Supply>();
            if (!ClaimId.IsEmpty() && !patientId.IsEmpty())
            {
                if ("Final".Equals(Type))
                {
                    var claim = billingRepository.GetFinalOnly(Current.AgencyId, ClaimId);
                    if (claim != null)
                    {
                        claim.Supply = EditSupplyInXml(claim.Supply, supply);
                        if (billingRepository.UpdateFinal(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The supply was successfully updated.";
                        }
                    }
                }
                else if ("Managed".Equals(Type))
                {
                    var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, ClaimId);
                    if (claim != null)
                    {
                        claim.Supply = EditSupplyInXml(claim.Supply, supply);
                        if (billingRepository.UpdateManagedClaimModel(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The supply was successfully updated.";
                        }
                    }
                }
                else if ("Secondary".Equals(Type))
                {
                    var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, ClaimId);
                    if (claim != null)
                    {
                        claim.Supply = EditSupplyInXml(claim.Supply, supply);
                        if (billingRepository.UpdateSecondaryClaimModel(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The supply was successfully updated.";
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SuppliesDelete(Guid Id, Guid PatientId, List<int> BillingId, string Type)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Supplies could not be deleted." };

            if (!Id.IsEmpty() && !PatientId.IsEmpty())
            {
                if (BillingId != null && BillingId.Count > 0)
                {
                    if ("Final".Equals(Type))
                    {
                        var claim = billingRepository.GetFinalOnly(Current.AgencyId, PatientId, Id);
                        if (claim != null && claim.Supply.IsNotNullOrEmpty())
                        {
                            claim.Supply = DeprecateSuppliesInXml(claim.Supply, BillingId);
                            if (billingRepository.UpdateFinal(claim))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Supplies were successfully deleted.";
                            }
                        }
                    }
                    else if ("Managed".Equals(Type))
                    {
                        var claim = billingRepository.GetManagedClaim(Current.AgencyId, PatientId, Id);
                        if (claim != null && claim.Supply.IsNotNullOrEmpty())
                        {
                            claim.Supply = DeprecateSuppliesInXml(claim.Supply, BillingId);
                            if (billingRepository.UpdateManagedClaimModel(claim))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Supplies were successfully deleted.";
                            }
                        }
                    }
                    else if ("Secondary".Equals(Type))
                    {
                        var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, PatientId, Id);
                        if (claim != null && claim.Supply.IsNotNullOrEmpty())
                        {
                            claim.Supply = DeprecateSuppliesInXml(claim.Supply, BillingId);
                            if (billingRepository.UpdateSecondaryClaimModel(claim))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Supplies were successfully deleted.";
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedInfoVerify(ManagedClaim claim)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Managed Claim (Basic Info) could not be verified." };
            if (claim.IsValid)
            {
                if (billingService.ManagedVerifyInfo(claim))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedDemographicsVerified, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Managed Claim (Basic Info) successfully verified.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = claim.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ManagedClaimAssessmentData(string input)
        {
            var managedClaimEpisodeData = new ManagedClaimEpisodeData();
            if (input.IsNotNullOrEmpty())
            {
                var inputArray = input.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                if (inputArray != null && inputArray.Length > 0 && input.Length > 0)
                {
                    managedClaimEpisodeData = billingService.GetEpisodeAssessmentData(inputArray[0].ToGuid(), inputArray[1].ToGuid());
                }
            }
            return Json(managedClaimEpisodeData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedVisitVerify(Guid Id, Guid patientId, List<string> Visit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Managed Claim (Visits) could not be verified." };
            if (billingService.ManagedVisitVerify(Id, patientId, Visit))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedClaim, LogAction.ManagedVisitVerified, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "Managed Claim (Visits) successfully verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedSupplyVerify(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Managed Claim (Supplies) could not be verified." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("patientId") && formCollection["patientId"].IsNotNullOrEmpty() ? formCollection["patientId"].ToGuid() : Guid.Empty;
                if (!Id.IsEmpty() && !patientId.IsEmpty())
                {
                    if (billingService.ManagedVisitSupply(Id, patientId))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedClaim, LogAction.ManagedSupplyVerified, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Managed Claim (Supplies) successfully verified.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimSummary(List<Guid> ManagedClaimSelected, Guid BranchId, int PrimaryInsurance)
        {
            return PartialView("Managed/ClaimSummary", billingService.ManagedClaimToGenerate(Current.AgencyId,ManagedClaimSelected, BranchId, PrimaryInsurance));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult ManagedUB04Pdf(Guid patientId, Guid Id)
        {
            var doc = new ManagedUB04Pdf(billingService.GetManagedUBOFourInfo(patientId, Id), billingService, agencyRepository);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=UB04_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteManagedClaim(Guid patientId, Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be deleted. Please try again." };
            var rules = new List<Validation>();
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "The patient does not exist."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (billingRepository.DeleteManagedClaim(Current.AgencyId, patientId, id))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, patientId, id.ToString(), LogType.ManagedClaim, LogAction.ManagedDeleted, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The claim has been deleted successfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error deleting the claim. Please try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error deleting the claim. Please try again";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaim(Guid patientId, Guid id)
        {
            return PartialView("Managed/Update", billingService.GetManagedClaimInfo(patientId, id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimStatus(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var claimStatus = keys.Contains("Status") && formCollection["Status"].IsNotNullOrEmpty() && formCollection["Status"].IsInteger() ? formCollection["Status"].ToInteger() : 0;
                var comment = keys.Contains("Comment") && formCollection["Comment"].IsNotNullOrEmpty() ? formCollection["Comment"].ToString() : string.Empty;
                if (!Id.IsEmpty() && !patientId.IsEmpty())
                {
                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            var claimDate = formCollection["ClaimDateValue"].IsNotNullOrEmpty() ? formCollection["ClaimDateValue"].ToString() : "";
                            if (billingService.UpdateProccesedManagedClaimStatus(patient, Id, claimDate, claimStatus, comment))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "The claim updated successfully.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message; ;
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedSnapShotClaimInfo(Guid patientId, Guid claimId)
        {
            return PartialView("Managed/ClaimInfo", billingService.GetManagedClaimSnapShotInfo(patientId, claimId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedComplete(Guid id, Guid patientId, string total)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Managed Claim could not be completed." };
            if (!id.IsEmpty())
            {
                if (billingService.ManagedComplete(id, patientId, total.IsNotNullOrEmpty() ? total.ToCurrency() : 0))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Managed Claim completed successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteClaim(Guid patientId, Guid id, string type)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be deleted. Please try again." };
            var rules = new List<Validation>();
            if (!id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty())
            {
                rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "The patient does not exist."));
                rules.Add(new Validation(() => !billingService.IsEpisodeHasClaim(id, patientId, type), "This claim does not exist."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (patientService.DeleteClaim(patientId, id, type))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The claim has been deleted successfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error deleting the claim. Please try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error deleting the claim. Please try again";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePending(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The claim could not be updated." };
            var rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var rapIds = keys.Contains("RapId") && formCollection["RapId"].IsNotNullOrEmpty() ? formCollection["RapId"].ToArray() : null;
                var finalIds = keys.Contains("FinalId") && formCollection["FinalId"].IsNotNullOrEmpty() ? formCollection["FinalId"].ToArray() : null;
                if ((rapIds != null && rapIds.Length > 0) || (finalIds != null && finalIds.Length > 0))
                {
                    if (rapIds != null && rapIds.Length > 0)
                    {
                        foreach (var id in rapIds)
                        {
                            rules.Add(new Validation(() => keys.Contains(string.Format("RapPaymentDate_{0}", id)) && formCollection[string.Format("RapPaymentDate_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("RapPaymentDate_{0}", id)].IsValidDate() : false, "Payment date is not a right format."));
                            rules.Add(new Validation(() => keys.Contains(string.Format("RapPayment_{0}", id)) && formCollection[string.Format("RapPayment_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("RapPayment_{0}", id)].IsDouble() : false, "Payment amount is not a right format."));
                        }
                    }
                    if (finalIds != null && finalIds.Length > 0)
                    {
                        foreach (var id in finalIds)
                        {
                            rules.Add(new Validation(() => keys.Contains(string.Format("FinalPaymentDate_{0}", id)) && formCollection[string.Format("FinalPaymentDate_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("FinalPaymentDate_{0}", id)].IsValidDate() : false, "Payment date is not a right format."));
                            rules.Add(new Validation(() => keys.Contains(string.Format("FinalPayment_{0}", id)) && formCollection[string.Format("FinalPayment_{0}", id)].IsNotNullOrEmpty() ? !formCollection[string.Format("FinalPayment_{0}", id)].IsDouble() : false, "Payment amount is not a right format."));
                        }
                    }
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (billingService.UpdatePendingClaimStatus(formCollection, rapIds, finalIds))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The claim updated successfully.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "The claim could not be updated.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
            }
            return Json(viewData);
        }

       
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EdiFile()
        {
            return PartialView("EdiFiles");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Remittance()
        {
            return PartialView("Remittances", billingRepository.GetRemittances(Current.AgencyId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceContent(DateTime StartDate, DateTime EndDate)
        {
            return PartialView("RemittanceContent", billingRepository.GetRemittances(Current.AgencyId, StartDate, EndDate));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult RemittancesPdf(DateTime StartDate, DateTime EndDate)
        {
            var doc = new RemittancesPdf(billingRepository.GetRemittances(Current.AgencyId, StartDate, EndDate).ToList<RemittanceLean>(), agencyRepository.Get(Current.AgencyId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Remittances_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult RemittancePdf(Guid Id)
        {
            var remittance = billingRepository.GetRemittance(Current.AgencyId, Id);
            if (remittance != null && remittance.Data.IsNotNullOrEmpty()) remittance.Data = remittance.Data.Replace("&", "&amp;").Replace("\r", "").Replace("\n", "");
            var doc = new RemittancePdf(remittance, agencyRepository.Get(Current.AgencyId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=RemittanceDetail_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceDetail(Guid Id)
        {
            var remittance = new Remittance();
            try
            {
                remittance = billingRepository.GetRemittanceWithClaims(Current.AgencyId, Id);
                return PartialView("RemittanceDetail", remittance);
            }
            catch (Exception ex)
            {
                return PartialView("RemittanceDetail", remittance);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceDetailContent(Guid Id)
        {
            var remittance = new Remittance();
            try
            {
                return PartialView("RemittanceDetailContent", billingRepository.GetRemittanceWithClaims(Current.AgencyId, Id));
            }
            catch (Exception ex)
            {
                return PartialView("RemittanceDetailContent", remittance);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemittanceUpload()
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The remittance could not be uploaded. Please try again." };
            var file = Request.Files.Get(0);
            if (file != null)
            {
                if (file.ContentType != "Text/Plain")
                {
                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        if (billingService.AddRemittanceUpload(file))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The remittance upload was successful.";
                        }
                        else
                        {
                            viewData.errorMessage = "The remittance could not be uploaded. Please try again.";
                        }
                    }
                    else
                    {
                        viewData.errorMessage = "The remittance upload file is empty. Please try again.";
                    }
                }
                else
                {
                    viewData.errorMessage = "The remittance file is not in the correct format.";
                }
            }
            else
            {
                viewData.errorMessage = "No remittance file was uploaded.";
            }
            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteRemittance(Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The remittance could not be deleted. Please try again." };
            if (billingRepository.DeleteRemittance(Current.AgencyId, Id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The remittance has been deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimLogs(string type, Guid claimId, Guid patientId)
        {
            if (type.ToUpperCase() == LogType.Rap.ToString().ToUpperCase() || type.ToUpperCase() == LogType.Final.ToString().ToUpperCase() || type.ToUpperCase() == LogType.ManagedClaim.ToString().ToUpperCase())
            {
                return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Patient, type.ToUpperCase() == LogType.Rap.ToString().ToUpperCase() ? LogType.Rap : (type.ToUpperCase() == LogType.Final.ToString().ToUpperCase() ? LogType.Final : LogType.ManagedClaim), patientId, claimId.ToString()));
            }
            return PartialView("ActivityLogs", new List<AppAudit>());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SubmittedList()
        {
            return PartialView("SubmittedList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimSubmittedList(DateTime StartDate, DateTime EndDate, string ClaimType)
        {
            return View(new GridModel(billingRepository.ClaimDatas(Current.AgencyId, StartDate, EndDate, ClaimType)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmittedClaimDetail(int Id, DateTime StartDate, DateTime EndDate)
        {
            ClaimInfoDetailList claims = new ClaimInfoDetailList();
            claims.Details = billingService.GetSubmittedBatchClaims(Id);
            claims.Id = Id;
            claims.StartDate = StartDate;
            claims.EndDate = EndDate;
            return PartialView("SubmittedClaimsDetail", claims);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimResponse(int Id)
        {
            var claimData = billingRepository.GetClaimData(Current.AgencyId, Id);
            return PartialView("ClaimResponse", claimData != null ? claimData : new ClaimData());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult ClaimResponsePdf(int Id)
        {
            var claimData = billingRepository.GetClaimData(Current.AgencyId, Id);
            ClaimResponsePdf doc = new ClaimResponsePdf(claimData);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=ClaimResponse_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AuthorizationContent(Guid AuthorizationId)
        {
            return PartialView("AuthorizationContent", patientRepository.GetAuthorization(Current.AgencyId, AuthorizationId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsuranceInfoContent(Guid PatientId, int InsuranceId, DateTime StartDate, DateTime EndDate, string ClaimTypeIdentifier)
        {
            var viewData = billingService.InsuranceWithAuthorization(PatientId, InsuranceId, StartDate, EndDate);
            if (viewData != null) { viewData.ClaimTypeIdentifier = ClaimTypeIdentifier; }
            return PartialView("InsuranceInfoContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PostRemittance(Guid Id, List<string> Episodes)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The remittance could not be posted. Please try again." };
            if (!Id.IsEmpty() && Id != null)
            {
                if (Episodes != null && Episodes.Count > 0)
                {
                    if (billingService.PostRemittance(Id, Episodes))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The remittance posted successfully.";
                    }
                }
                else
                {
                    viewData.errorMessage = "The remittance Identifier could not be found. Please try again.";
                }
            }
            else
            {
                viewData.errorMessage = "The remittance Identifier could not be found. Please try again.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimRemittance(Guid Id, string Type)
        {
            var claimInfos = new List<PaymentInformation>();
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("rap"))
                {
                    var rap = billingRepository.GetRap(Current.AgencyId, Id);
                    if (rap != null && rap.Remittance.IsNotNullOrEmpty())
                    {
                        return PartialView("ClaimRemittance", rap.Remittance.ToObject<List<PaymentInformation>>());
                    }
                }
                else if (Type.IsEqual("final"))
                {
                    var final = billingRepository.GetFinalOnly(Current.AgencyId, Id);
                    if (final != null && final.Remittance.IsNotNullOrEmpty())
                    {
                        return PartialView("ClaimRemittance", final.Remittance.ToObject<List<PaymentInformation>>());
                    }
                }
            }
            return PartialView("ClaimRemittance", claimInfos);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AllClaims()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            var bills = new List<Bill>();
            ViewData["Type"] = ClaimTypeSubCategory.RAP.ToString();
            ViewData["BillUIIdentifier"] = "All";
            if (agency != null)
            {
                var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
                {
                    int payorType;
                    if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agencyMainBranch.IsLocationStandAlone ? agencyMainBranch.Payor : agency.Payor, out payorType))
                    {
                        bills = billingService.AllUnProcessedBillList(agencyMainBranch.Id, payorType, ClaimTypeSubCategory.RAP.ToString(), "branch", "", false);
                        ViewData["Branch"] = agencyMainBranch.Id;
                        ViewData["Insurance"] = payorType;
                        return PartialView("AllClaim", bills);
                    }
                    else
                    {
                        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == (int)PayerTypes.MedicareTraditional || i.PayorType == (int)PayerTypes.MedicareHMO).OrderBy(i => i.Id).ToList().FirstOrDefault();
                        if (agencyMedicareInsurance != null)
                        {
                            bills = billingService.AllUnProcessedBillList(agencyMainBranch.Id, agencyMedicareInsurance.Id, ClaimTypeSubCategory.RAP.ToString(), "branch", "", false);
                            ViewData["Branch"] = agencyMainBranch.Id;
                            ViewData["Insurance"] = agencyMedicareInsurance.Id;
                            return PartialView("AllClaim", bills);
                        }
                    }
                }
                else
                {
                    var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == (int)PayerTypes.MedicareTraditional || i.PayorType == (int)PayerTypes.MedicareHMO).OrderBy(i => i.Id).ToList().FirstOrDefault();
                    if (agencyMedicareInsurance != null)
                    {
                        bills = billingService.AllUnProcessedBillList(agencyMainBranch.Id, agencyMedicareInsurance.Id, ClaimTypeSubCategory.RAP.ToString(), "branch", "", false);
                        ViewData["Branch"] = agencyMainBranch.Id;
                        ViewData["Insurance"] = agencyMedicareInsurance.Id;
                        return PartialView("AllClaim", bills);
                    }
                }
            }
            return PartialView("AllClaim", bills);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllClaimGrid(Guid branchId, int insuranceId, string type)
        {
            ViewData["BillUIIdentifier"] = "All";
            return PartialView("AllClaimContent", billingService.AllUnProcessedBillList(branchId, insuranceId, type, "branch", "", false));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimPayments(Guid claimId, Guid patientId)
        {
            return PartialView("Managed/Payments", claimId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllManagedClaimPayments(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            return PartialView("Managed/AllPayments", patient);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllManagedClaimPaymentsGrid(Guid patientId)
        {
            var payments = billingRepository.GetManagedClaimPaymentsByPatient(Current.AgencyId, patientId);
            return View(new GridModel(payments));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewManagedClaimPayment(Guid Id)
        {
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, Id);
            return PartialView("Managed/NewPayment", claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddManagedClaimPayment(ManagedClaimPayment payment)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The payment could not be added. Please try again." };
            if (payment != null)
            {
                rules.Add(new Validation(() => payment.PaymentAmount.IsNotNullOrEmpty() ? !payment.PaymentAmount.IsDouble() : false, "Payment Value is not a right format."));
                
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    payment.Payment = payment.PaymentAmount.ToDouble();
                    payment.Id = Guid.NewGuid();
                    payment.AgencyId = Current.AgencyId;
                    payment.Comments = payment.Comments != null ? payment.Comments : "";
                    if (billingRepository.AddManagedClaimPayment(payment))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The payment has been added successfully.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimPayment(Guid id)
        {
            var payment = billingRepository.GetManagedClaimPayment(Current.AgencyId, id);
            return PartialView("Managed/EditPayment", payment);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimPaymentsGrid(Guid claimId)
        {
            var payments = billingRepository.GetManagedClaimPaymentsByClaim(Current.AgencyId, claimId).OrderBy(o => o.Created).ToList();
            var insurances = agencyRepository.GetInsurances(Current.AgencyId);
            foreach (var payment in payments)
            {
                payment.PayorName = payment.Payor == 0 ? "" : insurances.FirstOrDefault(p => p.Id == payment.Payor).Name;
            }
            return View(new GridModel(payments));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteManagedClaimPayment(Guid patientId, Guid claimId, Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The payment could not be deleted. Please try again." };
            if (billingService.DeleteManagedClaimPayment(patientId, claimId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The payment has been deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimPaymentDetails(FormCollection formCollection)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The payment could not be updated." };
            var keys = formCollection.AllKeys;
            if (keys != null)
            {
                var PatientId = keys.Contains("PatientId") && formCollection["PatientId"].IsNotNullOrEmpty() ? formCollection["PatientId"].ToGuid() : Guid.Empty;
                var ClaimId = keys.Contains("ClaimId") && formCollection["ClaimId"].IsNotNullOrEmpty() ? formCollection["ClaimId"].ToGuid() : Guid.Empty;
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                rules.Add(new Validation(() => keys.Contains("PaymentAmount") && formCollection["PaymentAmount"].IsNotNullOrEmpty() ? !formCollection["PaymentAmount"].IsDouble() : false, "Payment Value is not a right format."));
                rules.Add(new Validation(() => keys.Contains("PaymentDateValue") && formCollection["PaymentDateValue"].IsNotNullOrEmpty() ? !formCollection["PaymentDateValue"].IsValidDate() : false, "Payment date is not a right format."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    var paymentAmount = formCollection["PaymentAmount"].IsNotNullOrEmpty() ? formCollection["PaymentAmount"].ToDouble() : 0;
                    var paymentDate = formCollection["PaymentDateValue"].IsNotNullOrEmpty() ? formCollection["PaymentDateValue"].ToString() : "";
                    var payor = formCollection["Payor"].IsNotNullOrEmpty() && formCollection["Payor"].IsInteger() ? formCollection["Payor"].ToString().ToInteger() : 0;
                    var comments = formCollection["Comments"].IsNotNullOrEmpty() ? formCollection["Comments"].ToString() : "";

                    if (billingService.UpdateManagedClaimPayment(PatientId, ClaimId, Id, paymentAmount, paymentDate, payor, comments))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The payment updated successfully.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message; ;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimAdjustments(Guid claimId, Guid patientId)
        {
            return PartialView("Managed/Adjustments", claimId);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimAdjustmentsGrid(Guid claimId)
        {
            var adjustments = billingRepository.GetManagedClaimAdjustmentsByClaim(Current.AgencyId, claimId).OrderBy(o => o.Created).ToList();
            var adjustmentCodes = agencyRepository.GetAdjustmentCodes(Current.AgencyId);
            adjustments.ForEach(a => {
                var code = adjustmentCodes.FirstOrDefault(ac => ac.Id == a.TypeId);
                if (code != null)
                {
                    a.Type = code.Code;
                    a.Description = code.Description;
                }
            });
            return View(new GridModel(adjustments));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewManagedClaimAdjustment(Guid Id)
        {
            var claim = billingRepository.GetManagedClaim(Current.AgencyId, Id);
            return PartialView("Managed/NewAdjustment", claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddManagedClaimAdjustment(ManagedClaimAdjustment managedClaimAdjustment)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The adjustment could not be added. Please try again." };
            if (managedClaimAdjustment != null)
            {
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    managedClaimAdjustment.Id = Guid.NewGuid();
                    managedClaimAdjustment.AgencyId = Current.AgencyId;
                    managedClaimAdjustment.Comments = managedClaimAdjustment.Comments != null ? managedClaimAdjustment.Comments : "";
                    if (billingRepository.AddManagedClaimAdjustment(managedClaimAdjustment))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The adjustment has been added successfully.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimAdjustment(Guid Id)
        {
            var adjustment = billingRepository.GetManagedClaimAdjustment(Current.AgencyId, Id);
            return PartialView("Managed/EditAdjustment", adjustment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateManagedClaimAdjustmentDetails(ManagedClaimAdjustment managedClaimAdjustment)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The adjustment could not be updated. Please try again." };
            if (managedClaimAdjustment != null)
            {
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    managedClaimAdjustment.AgencyId = Current.AgencyId;
                    if (billingRepository.UpdateManagedClaimAdjustment(managedClaimAdjustment))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The adjustment has been updated successfully.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteManagedClaimAdjustment(Guid patientId, Guid claimId, Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The adjustment could not be deleted. Please try again." };
            if (billingService.DeleteManagedClaimAdjustment(patientId, claimId, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The adjustment has been deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult InvoicePdf(Guid patientId, Guid Id, bool isForPatient)
        {
            if (patientId != null && Id != null && isForPatient != null) 
            {
                var invoice = billingService.GetManagedInvoiceInfo(patientId, Id, isForPatient);
                if (invoice != null) 
                {
                    var doc = new InvoicePdf(invoice, billingService, agencyRepository);
                    if (doc != null) 
                    {
                        var stream = doc.GetStream();
                        if (stream != null) 
                        {
                            stream.Position = 0;
                            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Invoice_{0}.pdf", DateTime.Now.Ticks.ToString()));
                            return new FileStreamResult(stream, "application/pdf");        
                        }
                    }
                }
            }
            return null;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditClaimSnapShot(Guid id, long batchId, string type)
        {
            ClaimSnapShotViewData viewData = new ClaimSnapShotViewData();
            if (type.ToUpperCase() == "RAP")
            {
                var snapShot = billingRepository.GetRapSnapShot(Current.AgencyId, id, batchId);
                if(snapShot != null)
                {
                    viewData.Id = snapShot.Id;
                    viewData.PatientId = snapShot.PatientId;
                    viewData.PaymentDate = snapShot.PaymentDate;
                    viewData.PaymentAmount = snapShot.Payment;
                    viewData.Status = snapShot.Status;
                    viewData.Type = "RAP";
                }
            }
            else if (type.ToUpperCase() == "FINAL")
            {
                var snapShot = billingRepository.GetFinalSnapShot(Current.AgencyId, id, batchId);
                if (snapShot != null)
                {
                    viewData.Id = snapShot.Id;
                    viewData.PatientId = snapShot.PatientId;
                    viewData.PaymentDate = snapShot.PaymentDate;
                    viewData.PaymentAmount = snapShot.Payment;
                    viewData.Status = snapShot.Status;
                    viewData.Type = "FINAL";
                }
            }

            return PartialView("BatchUpdate", viewData);
        }

        #region Secondary Claims

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimCenter(Guid patientId, Guid primaryClaimId)
        {
            var viewData = new SecondaryClaimViewData() { PatientId = patientId, PrimaryClaimId = primaryClaimId };
            if (!patientId.IsEmpty() && !primaryClaimId.IsEmpty())
            {
                viewData.PrimaryClaimData = billingService.GetClaimSnapShotInfo(patientId, primaryClaimId, "Final") ?? new ClaimInfoSnapShotViewData();
            }
            else
            {
                viewData.PrimaryClaimData = new ClaimInfoSnapShotViewData();
            }
            return View("Secondary/Center", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaims(Guid patientId, Guid primaryClaimId)
        {
            return View(new GridModel(billingRepository.GetSecondaryClaimLeansOfPrimaryClaim(Current.AgencyId, patientId, primaryClaimId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondarySnapShotClaimInfo(Guid patientId, Guid claimId)
        {
            return View("Secondary/ClaimInfo", billingService.GetSecondaryClaimSnapShotInfo(patientId, claimId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateSecondaryClaim(Guid patientId, Guid primaryClaimId, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The secondary claim could not be created." };
            string errorMessage;
            var existingClaims = billingRepository.GetSecondaryClaimLeansOfPrimaryClaim(Current.AgencyId, patientId, primaryClaimId);
            if (existingClaims != null)
            {
                if (existingClaims.Count > 0)
                {
                    foreach (var claim in existingClaims)
                    {
                        if (startDate.IsBetween(claim.StartDate, claim.EndDate) || endDate.IsBetween(claim.StartDate, claim.EndDate))
                        {
                            viewData.errorMessage = string.Format("A claim already exists with the date range {0}.", claim.ClaimRange);
                            return Json(viewData);
                        }
                    }
                }
                if (billingService.AddSecondaryClaim(patientId, primaryClaimId, startDate, endDate, out errorMessage))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The secondary claim was created successfully.";
                }
                else if (errorMessage.IsNotNullOrEmpty())
                {
                    viewData.errorMessage = errorMessage;
                }
            }
            
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewSecondaryClaim(Guid patientId, Guid episodeId, Guid primaryClaimId)
        {
            var newClaimData = new SecondaryClaimViewData();
            var episodeRange = patientRepository.GetPatientEpisodeFluent(Current.AgencyId, episodeId, patientId);
            newClaimData.EpisodeStartDate = episodeRange.StartDate;
            newClaimData.EpisodeEndDate = episodeRange.EndDate;
            newClaimData.PatientId = patientId;
            newClaimData.PrimaryClaimId = primaryClaimId;
            return PartialView("Secondary/New", newClaimData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSecondaryClaim(Guid Id)
        {
            return PartialView("Secondary/Update", billingRepository.GetSecondaryClaim(Current.AgencyId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSecondaryClaimStatus(SecondaryClaim claim)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Secondary Claim could not be updated." };
            var rules = new List<Validation>();
            if (claim != null)
            {
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (billingService.UpdateSecondaryClaimStatus(claim))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Secondary Claim updated successfully.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteSecondaryClaim(Guid patientId, Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The secondary claim could not be deleted." };
            var rules = new List<Validation>();
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                rules.Add(new Validation(() => !patientRepository.IsPatientExist(Current.AgencyId, patientId), "The patient does not exist."));
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    if (billingRepository.DeleteSecondaryClaim(Current.AgencyId, patientId, Id))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The secondary claim has been deleted successfully.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaim(Guid Id)
        {
            if (Id.IsEmpty())
            {
                return PartialView("Secondary/Edit", new SecondaryClaim());
            }
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
            return PartialView("Secondary/Edit", claim);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SecondaryClaimInfo(Guid Id)
        {
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
            return PartialView("Secondary/Info", claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimInfoVerify(SecondaryClaim claim)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Secondary Claim (Info) could not be verified." };
            if (claim.IsValid)
            {
                if (billingService.SecondaryVerifyInfo(claim))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedDemographicsVerified, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Secondary Claim (Info) was successfully verified.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = claim.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SecondaryClaimInsurance(Guid Id)
        {
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                if (claim.Insurance.IsNotNullOrEmpty())
                {
                    claim.AgencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                    claim.CBSA = lookupRepository.CbsaCodeByZip(claim.AddressZipCode);
                }
                else if (claim.SecondaryInsuranceId > 0)
                {
                    if (claim.SecondaryInsuranceId >= 1000)
                    {
                        claim.AgencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.SecondaryInsuranceId);
                    }
                    else if (claim.SecondaryInsuranceId > 0 && claim.SecondaryInsuranceId < 1000)
                    {
                        var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
                        if (patient != null)
                        {
                            claim.AgencyInsurance = billingService.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, claim.SecondaryInsuranceId);
                        }
                    }
                    claim.CBSA = lookupRepository.CbsaCodeByZip(claim.AddressZipCode);
                }
            }
            return PartialView("Secondary/Insurance", claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimInsuranceVerify(Guid Id, FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Insurance could not be verified." };

            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                if (formCollection != null)
                {
                    var keys = formCollection.AllKeys;
                    if (keys != null && keys.Length > 0)
                    {
                        if (keys.Contains("Ub04Locator81"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator81");
                            claim.Ub04Locator81cca = locators.ToXml();
                        }
                        if (keys.Contains("Ub04Locator39"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator39");
                            claim.Ub04Locator39 = locators.ToXml();
                        }
                        if (keys.Contains("Ub04Locator31"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator31");
                            claim.Ub04Locator31 = locators.ToXml();
                        }
                        if (keys.Contains("Ub04Locator32"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator32");
                            claim.Ub04Locator32 = locators.ToXml();
                        }
                        if (keys.Contains("Ub04Locator33"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator33");
                            claim.Ub04Locator33 = locators.ToXml();
                        }
                        if (keys.Contains("Ub04Locator34"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "Ub04Locator34");
                            claim.Ub04Locator34 = locators.ToXml();
                        }
                        if (keys.Contains("HCFALocators"))
                        {
                            var locators = LocatorHelper.ConvertStringToLocatorList(formCollection, "HCFALocators");
                            claim.HCFALocators = locators.ToXml();
                        }
                    }
                }
                if (billingRepository.SecondaryClaimVerifyInsurance(claim, Current.AgencyId))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Secondary Claim (Insurance) successfully verified.";
                }
            }
            return Json(viewData);
        }

        [GridAction]
        public ActionResult SecondaryClaimInsuranceRates(Guid Id)
        {
            return View(new GridModel(billingService.GetSecondaryClaimInsuranceRates(Id)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimReloadInsurance(Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Insurance could not be reloaded." };

            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                AgencyInsurance agencyInsurance = null;
                if (claim.SecondaryInsuranceId >= 1000)
                {
                    agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.SecondaryInsuranceId);
                }
                else if (claim.SecondaryInsuranceId > 0 && claim.SecondaryInsuranceId < 1000)
                {
                    var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
                    if (patient != null)
                    {
                        agencyInsurance = billingService.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, claim.SecondaryInsuranceId);
                    }
                }
                if (agencyInsurance != null)
                {
                    claim.HCFALocators = "";
                    claim.Ub04Locator31 = "";
                    claim.Ub04Locator32 = "";
                    claim.Ub04Locator33 = "";
                    claim.Ub04Locator34 = "";
                    claim.Ub04Locator39 = "";
                    claim.Ub04Locator81cca = agencyInsurance.Ub04Locator81cca;
                    claim.Insurance = agencyInsurance.ToXml();
                    claim.IsInsuranceVerified = false;
                    if (billingRepository.UpdateSecondaryClaimModel(claim))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Insurance was successfully reloaded.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimNewBillData(Guid ClaimId)
        {
            var viewData = new NewBillDataViewData() { Id = ClaimId, IsMedicareHMO = false, TypeOfClaim = "Secondary" };
            return PartialView("BillData/New", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimEditBillData(Guid ClaimId, int Id)
        {
            var viewData = new EditBillDataViewData() { Id = ClaimId, TypeOfClaim = "Secondary", ChargeRate = new ChargeRate() };
            var rates = billingService.GetSecondaryClaimInsuranceRates(ClaimId);
            if(rates != null && rates.Count > 0)
            {
                var rate = rates.FirstOrDefault(r => r.Id == Id);
                viewData.ChargeRate = rate;
            }
            return PartialView("BillData/Edit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimDeleteBillData(Guid ClaimId, int Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be deleted." };
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, ClaimId);
            if (claim != null && claim.Insurance.IsNotNullOrEmpty())
            {
                var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                var rates = agencyInsurance.ToBillDataDictionary();
                if (rates.ContainsKey(Id.ToString()))
                {
                    if (rates.Remove(Id.ToString()))
                    {
                        agencyInsurance.BillData = rates.Select(r => r.Value).ToList().ToXml();
                        claim.Insurance = agencyInsurance.ToXml();
                        if (billingRepository.UpdateSecondaryClaimModel(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Charge rate was deleted successfully.";
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimAddBillData(ChargeRate chargeRate, Guid ClaimId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be updated." };
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, ClaimId);
            if (claim != null)
            {
                if (claim.Insurance.IsNotNullOrEmpty())
                {
                    var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                    if (agencyInsurance != null)
                    {
                        var rates = agencyInsurance.BillData.IsNotNullOrEmpty() ? agencyInsurance.BillData.ToObject<List<ChargeRate>>() : new List<ChargeRate>();
                        rates.Add(chargeRate);
                        agencyInsurance.BillData = rates.ToXml();
                        claim.Insurance = agencyInsurance.ToXml();
                        if (billingRepository.UpdateSecondaryClaimModel(claim))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Charge rate was successfully updated.";
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimUpdateBillData(ChargeRate chargeRate, Guid ClaimId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Charge rate could not be updated." };
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, ClaimId);
            if (claim != null)
            {
                if (claim.Insurance.IsNotNullOrEmpty())
                {
                    var agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>();
                    if (agencyInsurance != null)
                    {
                        var rates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var oldRate = rates.FirstOrDefault(r => r.Id == chargeRate.Id);
                            if (oldRate != null)
                            {
                                oldRate.PreferredDescription = chargeRate.PreferredDescription;
                                oldRate.Code = chargeRate.Code;
                                oldRate.RevenueCode = chargeRate.RevenueCode;
                                oldRate.Charge = chargeRate.Charge;
                                oldRate.Modifier = chargeRate.Modifier;
                                oldRate.Modifier2 = chargeRate.Modifier2;
                                oldRate.Modifier3 = chargeRate.Modifier3;
                                oldRate.Modifier4 = chargeRate.Modifier4;
                                oldRate.IsUnitsPerDayOnSingleLineItem = chargeRate.IsUnitsPerDayOnSingleLineItem;
                                oldRate.ChargeType = chargeRate.ChargeType;
                                if (oldRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || oldRate.ChargeType == ((int)BillUnitType.Hourly).ToString())
                                {
                                    if (chargeRate.IsTimeLimit)
                                    {

                                        oldRate.TimeLimitHour = chargeRate.TimeLimitHour;
                                        oldRate.TimeLimitMin = chargeRate.TimeLimitMin;
                                        oldRate.SecondDescription = chargeRate.SecondDescription;
                                        oldRate.SecondCode = chargeRate.SecondCode;
                                        oldRate.SecondRevenueCode = chargeRate.SecondRevenueCode;
                                        oldRate.IsSecondChargeDifferent = chargeRate.IsSecondChargeDifferent;
                                        if (chargeRate.IsSecondChargeDifferent)
                                        {
                                            oldRate.SecondCharge = chargeRate.SecondCharge;
                                        }
                                        else
                                        {
                                            oldRate.SecondCharge = 0;
                                        }
                                        oldRate.SecondModifier = chargeRate.SecondModifier;
                                        oldRate.SecondModifier2 = chargeRate.SecondModifier2;
                                        oldRate.SecondModifier3 = chargeRate.SecondModifier3;
                                        oldRate.SecondModifier4 = chargeRate.SecondModifier4;
                                        oldRate.SecondChargeType = chargeRate.SecondChargeType;
                                        if (oldRate.SecondChargeType == ((int)BillUnitType.PerVisit).ToString())
                                        {
                                            oldRate.SecondUnit = chargeRate.SecondUnit;
                                        }
                                        else
                                        {
                                            oldRate.SecondUnit = 0;
                                        }
                                        oldRate.IsUnitPerALineItem = chargeRate.IsUnitPerALineItem;
                                    }
                                    oldRate.IsTimeLimit = chargeRate.IsTimeLimit;
                                }
                                else if (oldRate.ChargeType == ((int)BillUnitType.PerVisit).ToString())
                                {
                                    oldRate.Unit = chargeRate.Unit;
                                }
                                if (agencyInsurance.PayorType == (int)PayerTypes.MedicareHMO && (oldRate.ChargeType == ((int)BillUnitType.Per15Min).ToString() || oldRate.ChargeType == ((int)BillUnitType.Hourly).ToString()))
                                {
                                    oldRate.MedicareHMORate = chargeRate.MedicareHMORate;
                                }

                                agencyInsurance.BillData = rates.ToXml();
                                claim.Insurance = agencyInsurance.ToXml();
                                if (billingRepository.UpdateSecondaryClaimModel(claim))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Charge rate successfully updated.";
                                }
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SecondaryClaimVisit(Guid Id)
        {
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    List<ScheduleEvent> visits = patientRepository.GetScheduledEventsOnly(Current.AgencyId, claim.PatientId, claim.StartDate, claim.EndDate);
                    if (visits != null && visits.Count > 0)
                    {
                        claim.AgencyLocationId = patient.AgencyLocationId;
                        var agencyInsurance = new AgencyInsurance();
                        var chargeRates = billingService.SecondaryToChargeRates(claim, out agencyInsurance);
                        claim.BillVisitDatas = billingService.BillableVisitsDataWithAdjustments(patient.AgencyLocationId, visits,
                            claim.Remittance, claim.Adjustments, ClaimType.MAN, chargeRates, true);
                        claim.AgencyInsurance = agencyInsurance;
                    }
                }
            }
            return PartialView("Secondary/Visit", claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimVisitVerify(Guid Id, Guid patientId, List<Guid> Visit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Secondary Claim (Visits) could not be verified." };
            if (billingService.SecondaryVerifyVisit(Id, patientId, Visit))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Secondary Claim (Visits) successfully verified.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SecondaryClaimRemittance(Guid Id)
        {
            var viewData = new ClaimRemittanceViewData();
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    List<ScheduleEvent> visits = patientRepository.GetScheduledEventsOnly(Current.AgencyId, claim.PatientId, claim.StartDate, claim.EndDate);
                    if (visits != null && visits.Count > 0)
                    {
                        claim.AgencyLocationId = patient.AgencyLocationId;
                        var agencyInsurance = new AgencyInsurance();
                        var chargeRates = billingService.SecondaryToChargeRates(claim, out agencyInsurance);
                        viewData.BillVisitDatas = billingService.BillableVisitsDataWithAdjustments(patient.AgencyLocationId, visits, claim.Remittance,
                            claim.Adjustments, ClaimType.MAN, chargeRates, true);
                        
                        claim.AgencyInsurance = agencyInsurance;
                    }
                    viewData.Id = claim.Id;
                    viewData.PatientId = claim.PatientId;
                    viewData.RemitDate = claim.RemitDate;
                    viewData.RemitId = claim.RemitId;
                    viewData.TotalAdjustmentAmount = claim.TotalAdjustmentAmount;
                }
            }
            return PartialView("Secondary/Remittance", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimRemittanceVerify(Guid Id, Guid PatientId, DateTime RemitDate, string RemitId, Dictionary<Guid, List<ServiceAdjustment>> adjustments)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Secondary Claim remittance was not verified." };
            if(billingService.SecondaryRemittanceVerify(Id, PatientId, RemitDate, RemitId, adjustments))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Secondary Claim remittance was verified.";
            }
            return Json(viewData);
        }



        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SecondaryClaimSupply(Guid Id)
        {
            var viewData = new ClaimSupplyViewData();
            SecondaryClaim claim = null;
            if (!Id.IsEmpty() )
            {
                claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
                if (claim != null)
                {
                    var supplies = claim.Supply.ToObject<List<Supply>>();
                    if (supplies != null)
                    {
                        int id = 0;
                        supplies.ForEach(s =>
                        {
                            s.BillingId = id;
                            id++;
                        });
                    }
                    else
                    {
                        supplies = new List<Supply>();
                    }
                    claim.Supply = supplies.ToXml();
                    billingRepository.UpdateSecondaryClaimModel(claim);
                    viewData.Id = claim.Id;
                    viewData.PatientId = claim.PatientId;
                    viewData.EpisodeStartDate = claim.EpisodeStartDate;
                    viewData.EpisodeEndDate = claim.EpisodeEndDate;
                    viewData.EpisodeEndDate = claim.EpisodeEndDate;
                    viewData.BilledSupplies = supplies != null && supplies.Count > 0 ? supplies.Where(s => s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
                    viewData.UnbilledSupplies = supplies != null && supplies.Count > 0 ? supplies.Where(s => !s.IsBillable && !s.IsDeprecated).ToList() : new List<Supply>();
                }
            }
            return PartialView("Secondary/Supply", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimSupplyVerify(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Secondary Claim (Supplies) could not be verified." };
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                var Id = keys.Contains("Id") && formCollection["Id"].IsNotNullOrEmpty() ? formCollection["Id"].ToGuid() : Guid.Empty;
                var patientId = keys.Contains("patientId") && formCollection["patientId"].IsNotNullOrEmpty() ? formCollection["patientId"].ToGuid() : Guid.Empty;
                if (!Id.IsEmpty() && !patientId.IsEmpty())
                {
                    if (billingService.SecondarySupplyVerify(Id, patientId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Secondary Claim (Supplies) successfully verified.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SecondaryClaimSummary(Guid Id, Guid patientId)
        {
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, patientId, Id);
            if (claim != null)
            {
                if (claim.VerifiedVisits.IsNotNullOrEmpty())
                {
                    var visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValidDate() && s.EventDate.IsValidDate()).OrderBy(s => s.VisitDate.ToDateTime().Date).ThenBy(s => s.EventDate.ToDateTime().Date).ToList();
                    if (visits != null && visits.Count > 0)
                    {
                        if (claim.IsVisitVerified)
                        {
                            var agencyInsurance = new AgencyInsurance();
                            var chargeRates = billingService.SecondaryToChargeRates(claim, out agencyInsurance);
                            claim.BillVisitSummaryDatas = billingService.BillableVisitSummaryWithAdjustments(claim.AgencyLocationId, visits,
                                claim.Remittance, claim.Adjustments, ClaimType.MAN, chargeRates, true);
                        }
                    }
                }
            }
            return PartialView("Secondary/Summary", claim);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SecondaryClaimComplete(Guid id, Guid patientId, string total)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Secondary Claim could not be completed." };
            if (!id.IsEmpty())
            {
                if (billingService.SecondaryClaimComplete(id, patientId, total.IsNotNullOrEmpty() ? total.ToCurrency() : 0))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Secondary Claim completed successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CreateSingleSecondaryANSI(Guid Id)
        {
            var claim = billingRepository.GetSecondaryClaim(Current.AgencyId, Id);
            if (claim != null)
            {
                var patient = patientRepository.GetPatientOnly(claim.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    ClaimData claimDataOut = null;
                    BillExchange billExchage;
                    var ids = (new List<Guid>());
                    ids.Add(Id);
                    if (billingService.GenerateSecondary(ids, ClaimCommandType.download, out claimDataOut, out billExchage, patient.AgencyLocationId, claim.SecondaryInsuranceId))
                    {
                        if (claimDataOut != null)
                        {
                            return Json(new { isSuccessful = true, Id = claimDataOut.Id });
                        }
                    }
                    else
                    {
                        if (billExchage != null)
                        {
                            return Json(new { isSuccessful = false, Id = -1, errorMessage = billExchage.Message });
                        }
                    }

                }
            }
            return Json(new { isSuccessful = false, Id = -1, errorMessage = "Error processing the claim(s)." });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SecondaryUB04Pdf(Guid patientId, Guid Id, string type)
        {
            var doc = new UB04Pdf(billingService.GetSecondaryUBOFourInfo(patientId, Id), this.billingService, agencyRepository);
            MemoryStream stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=UB04_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SecondaryHCFA1500Pdf(Guid patientId, Guid Id)
        {
            var doc = new HCFA1500Pdf(billingService.GetHCFA1500InfoForSecondaryClaim(patientId, Id), this.billingService, this.agencyRepository);
            MemoryStream stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HCFA1500_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        #endregion

        #endregion

        #region Private Methods

        private string AddSupplyToXml(string supplyXml, Supply newSupply)
        {
            var suppliesToEdit = new List<Supply>();
            if (supplyXml.IsNotNullOrEmpty())
            {
                suppliesToEdit = supplyXml.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
            }
            if (newSupply != null)
            {
                if (newSupply.UniqueIdentifier.IsEmpty())
                {
                    newSupply.UniqueIdentifier = Guid.NewGuid();
                }
                newSupply.IsBillable = true;
                newSupply.IsAddedFromBilling = true;
                newSupply.BillingId = suppliesToEdit.Count;
                suppliesToEdit.Add(newSupply);
            }
            return suppliesToEdit.ToXml();
        }

        private Supply GetSupplyFromXml(string supplyXml, int billingId)
        {
            Supply supply = new Supply();
            var supplies = new List<Supply>();
            if (supplyXml.IsNotNullOrEmpty())
            {
                supplies = supplyXml.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
            }
            if (supplies != null && supplies.Count > 0)
            {
                supply = supplies.FirstOrDefault(s => s.BillingId == billingId);
            }
            return supply;
        }

        private string EditSupplyInXml(string supplyXml, Supply edittedSupply)
        {
            Supply supply = new Supply();
            var supplies = new List<Supply>();
            if (supplyXml.IsNotNullOrEmpty())
            {
                supplies = supplyXml.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
            }
            if (supplies != null && supplies.Count > 0)
            {
                var supplyToEdit = supplies.FirstOrDefault(s => s.BillingId == edittedSupply.BillingId);
                if (supplyToEdit != null)
                {
                    supplyToEdit.Description = edittedSupply.Description;
                    supplyToEdit.UniqueIdentifier = edittedSupply.UniqueIdentifier;
                    supplyToEdit.RevenueCode = edittedSupply.RevenueCode;
                    supplyToEdit.Code = edittedSupply.Code;
                    supplyToEdit.Date = edittedSupply.Date;
                    supplyToEdit.Quantity = edittedSupply.Quantity;
                    supplyToEdit.UnitCost = edittedSupply.UnitCost;
                }
            }
            return supplies.ToXml();
        }

        private string DeprecateSuppliesInXml(string supplyXml, List<int> UniqueIdentifiers)
        {
            var suppliesToEdit = supplyXml.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
            suppliesToEdit.ForEach(s =>
            {
                if (UniqueIdentifiers.Contains(s.BillingId))
                {
                    s.IsDeprecated = true;
                }
            });
            return suppliesToEdit.ToXml();
        }

        private string ChangeStatusOfSuppliesInXml(string supplyXml, bool isBillable, List<int> UniqueIdentifiers)
        {
            var suppliesToEdit = supplyXml.ToObject<List<Supply>>().ToList() ?? new List<Supply>();
            suppliesToEdit.ForEach(s =>
            {
                if (UniqueIdentifiers.Contains(s.BillingId))
                {
                    s.IsBillable = isBillable;
                }
            });
            return suppliesToEdit.ToXml();
        }

        #endregion
    }
}
