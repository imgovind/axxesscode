﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;
    using Axxess.Core.Infrastructure;

    [Compress]
    
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PingController : BaseController
    {
        public PingController()
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Index(bool extend)
        {
            var viewData = new JsonViewData();
            if (HttpContext.User != null && HttpContext.User.Identity.IsAuthenticated)
            {
                viewData.isSuccessful = true;
                if (extend)
                {
                    Session["SessionPing"] = DateTime.Now;
                }
            }
            return Json(viewData);
        }
    }
}
