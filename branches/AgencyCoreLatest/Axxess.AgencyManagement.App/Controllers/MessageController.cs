﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    
    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Enums;
    using Domain;
    using ViewData;
    using Security;
    using Services;
    using iTextExtension;

    using Axxess.AgencyManagement;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;

    [Compress]
    
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class MessageController : BaseController
    {
        #region Private Members

        private readonly IMessageService messageService;
        private readonly IUserRepository userRepository;
        private readonly IMessageRepository messageRepository;
        private readonly IAgencyRepository agencyRepository;

        #endregion

        #region Constructor

        public MessageController(IMembershipDataProvider membershipDataProvider,
                                    IAgencyManagementDataProvider agencyManagementProvider,
                                    IMessageService messageService)
        {
            Check.Argument.IsNotNull(messageService, "messageService");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            this.messageService = messageService;
            this.userRepository = agencyManagementProvider.UserRepository;
            this.messageRepository = agencyManagementProvider.MessageRepository;
            this.agencyRepository = agencyManagementProvider.AgencyRepository;
        }

        #endregion

        #region MessageController Actions

        #region CRUD
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public JsonResult Create([Bind] Message message)
        {
            Check.Argument.IsNotNull(message, "message");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your message could not be sent. Please try again." };
            if (message.IsValid)
            {
                if (messageService.SendMessage(message, Request.Files))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your message has been sent.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error sending your message.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = message.ValidationMessage;
            }
            return new JsonResult() { ContentType = "text/html", Data = Json(viewData) };
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Inbox()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid id, int messageType)
        {
            return Json(messageService.GetMessage(id, messageType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult View(Guid id, int messageType, string inboxType)
        {
            var message = messageService.GetMessage(id, messageType);
            if (message != null)
            {
                if (inboxType.IsNotNullOrEmpty() && inboxType == "inbox")
                {
                    message.IsInInbox = true;
                }
            }
            return PartialView(message);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Grid(string inboxType)
        {
            var pageNumber = HttpContext.Request.Params["page"] != null ? int.Parse(HttpContext.Request.Params["page"]) : 1;
            var messages = messageService.GetMessages(inboxType, pageNumber, 75).Select(s => new MessageItem() { Id = s.Id, Subject = s.Subject, FromName = s.FromName, MarkAsRead = s.MarkAsRead, Created = s.Created, Type = s.Type, FolderId = s.FolderId }).ToList();
            var messagesCount = messageService.GetTotalMessagesCount(inboxType);
            if (messages != null && messages.Count > 0)
            {
                var gridModel = new GridModel(messages);
                gridModel.Data = messages;
                gridModel.Total = messagesCount;
                return Json(gridModel);
            }
            return Json(new GridModel(new List<MessageItem>()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GridWidget()
        {
            var messages = messageService.GetMessages("inbox", 1, 5).Select(s => new MessageItem() { Id = s.Id, Subject = s.Subject, FromName = s.FromName, MarkAsRead = s.MarkAsRead, Created = s.Created, Type = s.Type }).ToList();
            return Json(new GridModel(messages));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult Pdf(Guid id, int messageType)
        {
            var doc = new MessagePdf(messageService.GetMessage(id, messageType), agencyRepository.GetWithBranches(Current.AgencyId));
            var stream = doc.GetFormattedStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Message_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid id, int messageType, string inboxType)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Message could not be deleted!" };
            if (messageType == (int)MessageType.System || messageType == (int)MessageType.User)
            {
                if (messageService.DeleteMessage(id, messageType))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Message was deleted successfully";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteMany(List<Guid> messageList)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Messages could not be deleted!" };

            if (messageService.DeleteMany(messageList))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Selected messages deleted successfully";
            }
            return Json(viewData);
        }

        #endregion

        #region Helpers
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CustomWidget()
        {
            var message = messageRepository.GetCurrentDashboardMessage();
            if (message != null && message.Text.IsNotNullOrEmpty()) return Json(message);
            return Json(new Message());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RecipientList(string searchTerm)
        {
            var recipients = new List<Recipient>();
            var query = userRepository.GetAgencyUsers(searchTerm, Current.AgencyId);
            query.ForEach(e =>
            {
                recipients.Add(new Recipient { id = e.Id.ToString(), name = string.Concat(e.LastName + ", " + e.FirstName) });
            });
            return Json(recipients);
        }
        #endregion

        #region Folder Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FolderNew()
        {
            return PartialView("Folder/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult FolderCreate(string newFolderName)
        {
            if (newFolderName.IsNullOrEmpty()) return null;
            JsonViewData jsonData = new JsonViewData();
            jsonData.isSuccessful = messageRepository.AddMessageFolder(Current.UserId, Current.AgencyId, newFolderName);
            jsonData.errorMessage = (jsonData.isSuccessful ? "Added new folder: " + newFolderName : "Error adding folder.");
            return Json(jsonData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult FolderList()
        {
            var folderList = messageService.FolderList();
            return Json(folderList);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult FolderDelete(Guid folderId)
        {
            JsonViewData jsonData = new JsonViewData();
            jsonData.isSuccessful = true;
            jsonData.errorMessage = (jsonData.isSuccessful ? "Folder deleted." : "Error Deleting Folder");
            return Json(jsonData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Move(Guid id, Guid folderId, string folderName, string messageType)
        {
            JsonViewData jsonData = new JsonViewData();
            jsonData.isSuccessful = messageRepository.MoveMessageToFolder(Current.UserId, id, folderId, messageType);
            jsonData.errorMessage = (jsonData.isSuccessful ? "Message has been moved to " + folderName + "." : "There was an error moving this message to " + folderName + ".");
            return Json(jsonData);
        }

        #endregion

        #endregion
    }
}
