﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;

    using Axxess.Core.Infrastructure;

    using Security;

    [Compress]
    
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class DebugController : BaseController
    {
        public DebugController() { }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            System.Web.HttpBrowserCapabilitiesBase browser = Request.Browser;
            string information = "<strong>Debug Information</strong><br />"
                + "Browser = " + browser.Browser + "<br />"
                + "Version = " + browser.Version + "<br />"
                + "Is Beta = " + browser.Beta + "<br />"
                + "Is Mobile = " + browser.IsMobileDevice + "<br />"
                + "User Agent = " + Request.UserAgent.ToLower() + "<br /><br />"
                + "Remote IP = " + HttpContext.Request.ServerVariables["REMOTE_ADDR"] + "<br />"
                + "Forwarded IP = " + HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] + "<br />"
                + "Current IP = " + Current.IpAddress + "<br />"
                + "User Host Address = " + HttpContext.Request.UserHostAddress + "<br />"
                + "Is Request Local = " + HttpContext.Request.IsLocal + "<br /><br />"
                + "Machine IP Address = " + Request.ServerVariables["LOCAL_ADDR"] + "<br />"
                + "Machine Name = " + Environment.MachineName + "<br /><br />"
                + "Assembly Version = " + Current.AssemblyVersion;

            return PartialView("Data", information);
        }

        
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Browser()
        {
            System.Web.HttpBrowserCapabilitiesBase browser = HttpContext.Request.Browser;
            string browserInfo = "<strong>Browser Capabilities</strong><br />"
                + "Type = " + browser.Type + "<br />"
                + "Name = " + browser.Browser + "<br />"
                + "Version = " + browser.Version + "<br />"
                + "Major Version = " + browser.MajorVersion + "<br />"
                + "Minor Version = " + browser.MinorVersion + "<br />"
                + "Platform = " + browser.Platform + "<br />"
                + "Is Beta = " + browser.Beta + "<br />"
                + "Is Crawler = " + browser.Crawler + "<br />"
                + "Is AOL = " + browser.AOL + "<br />"
                + "Is Win16 = " + browser.Win16 + "<br />"
                + "Is Win32 = " + browser.Win32 + "<br />"
                + "Supports Frames = " + browser.Frames + "<br />"
                + "Supports Tables = " + browser.Tables + "<br />"
                + "Supports Cookies = " + browser.Cookies + "<br />"
                + "Supports VBScript = " + browser.VBScript + "<br />"
                + "Supports JavaScript = " +
                    browser.EcmaScriptVersion.ToString() + "<br />"
                + "Supports Java Applets = " + browser.JavaApplets + "<br />"
                + "Supports ActiveX Controls = " + browser.ActiveXControls
                      + "<br />"
                + "Supports JavaScript Version = " +
                    browser["JavaScriptVersion"] + "<br />";

            return PartialView("Data", browserInfo);
        }
    }
}
