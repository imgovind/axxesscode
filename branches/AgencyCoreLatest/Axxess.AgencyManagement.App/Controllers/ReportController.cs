﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Telerik.Web.Mvc;

    using NPOI.HPSF;
    using NPOI.HSSF.Util;
    using NPOI.HSSF.UserModel;

    using NPOI.SS.Util;
    using NPOI.SS.UserModel;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Services;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Enums;
    using Exports;
    using ViewData;
    using Security;
    using Extensions;

    [Compress]
    
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ReportController : BaseController
    {
        #region Constructor / Member

        private readonly IAgencyService agencyService;
        private readonly IReportService reportService;
        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly IBillingService billingService;
        private readonly IUserService userService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPayrollService payrollService;

        public ReportController(IAgencyManagementDataProvider dataProvider, IReportService reportService, IPatientService patientService, IBillingService billingService, IAgencyService agencyService, IUserService userService, IPayrollService payrollService)
        {
            Check.Argument.IsNotNull(reportService, "reportService");
            this.agencyService = agencyService;
            this.reportService = reportService;
            this.patientService = patientService;
            this.billingService = billingService;
            this.userService = userService;
            this.userRepository = dataProvider.UserRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.payrollService = payrollService;
        }

        #endregion

        #region ReportController Actions
   
        #region General Actions
        
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Center()
        {
            var reportList = reportService.GetAllReport();
            var dictionary = reportList.GroupBy(d => d.CategoryId).ToDictionary(k => k.Key, k => k.ToList());
            return PartialView("Center",dictionary);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Dashboard()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Patient()
        {
            return PartialView("Patient/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Clinical()
        {
            return PartialView("Clinical/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Schedule()
        {
            return PartialView("Schedule/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Billing()
        {
            return PartialView("Billing/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Employee()
        {
            return PartialView("Employee/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Statistical()
        {
            return PartialView("Statistical/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Completed()
        {
            return PartialView("Completed");
        }

        [GridAction]
        public ActionResult CompletedList()
        {
            //var reports = agencyRepository.GetReports(Current.AgencyId);
            //var users = userRepository.GetUsersNameOnly(Current.AgencyId);
            //var userId = Current.UserId;
            //foreach (var r in reports)
            //{
            //    r.Name = r.Status.IsEqual("Completed") && !r.AssetId.IsEmpty() ? string.Format("<a href=\"/Asset/{0}\">{1}</a>", r.AssetId, r.Name) : r.Name;
            //    if (!r.UserId.IsEmpty())
            //    {
            //        var user = users.FirstOrDefault(u => u.Id == r.UserId);
            //        if (user != null)
            //        {
            //            r.UserName = user.DisplayName;
            //        }
            //    }
            //}

            var reports = agencyRepository.GetReportsByUserId(Current.AgencyId, Current.UserId);
            var userRecord = UserEngine.GetName(Current.UserId, Current.AgencyId);
            foreach (var r in reports) 
            {
                r.Name = r.Status.IsEqual("Completed") && !r.AssetId.IsEmpty() ? string.Format("<a href=\"/Asset/{0}\">{1}</a>", r.AssetId, r.Name) : r.Name;
                if (!r.UserId.IsEmpty()) 
                {
                    r.UserName = userRecord;
                }
            }
            return View(new GridModel(reports));
        }

        #endregion

        #region Patient Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientRoster()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/Roster", reportService.GetPatientRoster(Guid.Empty, 1, 0,false));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientRosterContent(Guid BranchCode, int StatusId, int InsuranceId, DateTime? StartDate, DateTime? EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            if (StartDate.HasValue && EndDate.HasValue && StartDate.Value.IsValid() && EndDate.Value.IsValid())
            {
                return PartialView("Patient/Content/RosterContent", reportService.GetPatientRosterByDateRange(BranchCode, StatusId, InsuranceId, StartDate.Value, EndDate.Value, false));
            }
            else
            {
                return PartialView("Patient/Content/RosterContent", reportService.GetPatientRoster(BranchCode, StatusId, InsuranceId, false));
            }
            
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReferralLog()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/Referral", reportService.GetReferralInfos(Current.AgencyId,0,DateTime.Now.AddDays(-60),DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReferralLogContent(int Status, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/ReferralContent", reportService.GetReferralInfos(Current.AgencyId, Status, StartDate, EndDate));
        }

        public ActionResult ExportPatientRoster(Guid BranchCode, int StatusId, int InsuranceId, DateTime? StartDate, DateTime? EndDate)
        {
            List<PatientRoster> patientRosters = null;
            if (StartDate.HasValue && EndDate.HasValue && StartDate.Value.IsValid() && EndDate.Value.IsValid())
            {
                patientRosters = reportService.GetPatientRosterByDateRange(BranchCode, StatusId, InsuranceId, StartDate.Value, EndDate.Value, true);
            }
            else
            {
                patientRosters = reportService.GetPatientRoster(BranchCode, StatusId, InsuranceId, true);
            }
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, CoreSettings.ExcelDirectory);
            var file = new FileStream(Path.Combine(path, "ReportTemplate.xls"), FileMode.Open, FileAccess.Read);
            var workbook = new HSSFWorkbook(file);
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Roster";
            workbook.SummaryInformation = si;
            var sheet = workbook.GetSheetAt(0);
            workbook.SetSheetName(0, "PatientRoster");

            Dictionary<string, string> titleHeaders = new Dictionary<string, string>();
            string patientTitle = string.Format("Patient Status: {0}", Enum.GetName(typeof(PatientStatus), StatusId));
            string effectiveDate = string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy"));

            string insuranceName = "All";
            if (InsuranceId != 0)
            {
                var ins = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
                if (ins != null)
                {
                    insuranceName = ins.Name;
                }
            }
            titleHeaders.Add("Date", DateTime.Today.ToString("MM/dd/yyyy"));
            titleHeaders.Add("Patient Status", Enum.GetName(typeof(PatientStatus), StatusId));
            titleHeaders.Add("Insurance", insuranceName);
            if (StartDate.HasValue && EndDate.HasValue && StartDate.Value.IsValid() && EndDate.Value.IsValid())
            {
                titleHeaders.Add("Active Date Range", string.Format("{0} - {1}", StartDate.Value.ToString("MM/dd/yyyy"), EndDate.Value.ToString("MM/dd/yyyy")));
            }
            string agencyLocation = BranchCode.IsEmpty() ? "" : agencyRepository.FindLocation(Current.AgencyId, BranchCode).Name;
            int rowNumber = workbook.WriteHeaderOfExcelSpreadsheet(Current.AgencyName, agencyLocation, "Patient Roster Report", titleHeaders);

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            //var headerRow = sheet.CreateRow(6);
            ////headerRow.CreateCell(0).SetCellValue("MRN");
            //headerRow.CreateCell(0).SetCellValue("Patient");
            //headerRow.CreateCell(1).SetCellValue("Insurance");
            ////headerRow.CreateCell(2).SetCellValue("Policy #");
            //headerRow.CreateCell(2).SetCellValue("Address");
            ////headerRow.CreateCell(4).SetCellValue("City");
            ////headerRow.CreateCell(5).SetCellValue("State");
            ////headerRow.CreateCell(6).SetCellValue("Zip Code");
            ////headerRow.CreateCell(3).SetCellValue("Home Phone");
            //headerRow.CreateCell(3).SetCellValue("Gender");
            //headerRow.CreateCell(4).SetCellValue("Triage");
            //headerRow.CreateCell(5).SetCellValue("DOB");
            //headerRow.CreateCell(6).SetCellValue("SOC");
            //headerRow.CreateCell(7).SetCellValue("D/C");
            //headerRow.CreateCell(8).SetCellValue("Physician");
            //headerRow.CreateCell(10).SetCellValue("Physician NPI");
            //headerRow.CreateCell(16).SetCellValue("Physician Phone");
            int specialHeaderRowCount = rowNumber - 1;
            sheet.CreateFreezePane(0, specialHeaderRowCount + 1, 0, specialHeaderRowCount + 1);

            foreach (var patient in patientRosters)
            {
                var row = sheet.CreateRow(rowNumber++);
                //row.CreateCell(0).SetCellValue(patient.PatientId);
                string mrn = patient.PatientId;
                row.CreateCell(0).SetCellValue(mrn);
                string patientName = patient.PatientDisplayName;
                row.CreateCell(1).SetCellValue(patientName);
                string insurance = string.Format("{0}\n{1}", patient.PatientInsuranceName, patient.PatientPolicyNumber);
                row.CreateCell(2).SetCellValue(insurance);
                row.CreateCell(3).SetCellValue(patient.PatientPolicyNumber);
                //row.CreateCell(3).SetCellValue(patient.PatientPolicyNumber);

                string address = string.Format("{0}\n{1}, {2} {3}\n{4}", patient.PatientAddressLine1, patient.PatientAddressCity,
                    patient.PatientAddressStateCode, patient.PatientAddressZipCode, patient.PatientPhone);
                row.CreateCell(4).SetCellValue(address);
                //row.CreateCell(5).SetCellValue(patient.PatientAddressCity);
                //row.CreateCell(6).SetCellValue(patient.PatientAddressStateCode);
                //row.CreateCell(7).SetCellValue(patient.PatientAddressZipCode);
                //row.CreateCell(3).SetCellValue(patient.PatientPhone);
                row.CreateCell(5).SetCellValue(patient.PatientGender);
                row.CreateCell(6).SetCellValue(patient.Triage);
                if (patient.PatientDOB != DateTime.MinValue)
                {
                    var createdDateCell = row.CreateCell(7);
                    createdDateCell.CellStyle = dateStyle;
                    createdDateCell.SetCellValue(patient.PatientDOB);
                }
                else
                {
                    row.CreateCell(7).SetCellValue(string.Empty);
                }
                DateTime patientSoCDate = patient.PatientSoC.ToDateTimeOrMin();
                if (patient.PatientDOB != DateTime.MinValue)
                {
                    var createdDateCell = row.CreateCell(8);
                    createdDateCell.CellStyle = dateStyle;
                    createdDateCell.SetCellValue(patientSoCDate);
                }
                else
                {
                    row.CreateCell(8).SetCellValue(string.Empty);
                }
                DateTime patientDischargeDate = patient.PatientDischargeDate.ToDateTimeOrMin();
                if (patientDischargeDate != DateTime.MinValue)
                {
                    var createdDateCell = row.CreateCell(9);
                    createdDateCell.CellStyle = dateStyle;
                    createdDateCell.SetCellValue(patientDischargeDate);
                }
                else
                {
                    row.CreateCell(9).SetCellValue(string.Empty);
                }
                string physician = string.Format("{0}\n{1}\n{2}", patient.PhysicianName, patient.PhysicianNpi, patient.PhysicianPhone);
                row.CreateCell(10).SetCellValue(physician);
                //row.CreateCell(15).SetCellValue(patient.PhysicianNpi);
                //row.CreateCell(16).SetCellValue(patient.PhysicianPhone);
            }
            var totalRow = sheet.CreateRow(rowNumber + 2);
            totalRow.CreateCell(0).SetCellValue(string.Format("Total Number of Patients: {0}", patientRosters.Count));

            workbook.FinishWritingToExcelSpreadsheet(9, specialHeaderRowCount);
            //workbook.SetPrintArea(0, "A2:I"+rowNumber);
            

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientRoster_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Cahps()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Patient/Cahps");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientEmergencyList()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/EmergencyList", reportService.GetPatientEmergencyContacts(1, Guid.Empty));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientEmergencyListContent(Guid BranchCode, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/EmergencyListContent", reportService.GetPatientEmergencyContacts(StatusId, BranchCode));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientEmergencyPreparednessList()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/EvacuationList", reportService.GetPatientEmergencyPreparedness(1, Guid.Empty));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientEmergencyPreparednessListContent(Guid BranchCode, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/EvacuationListContent", reportService.GetPatientEmergencyPreparedness(StatusId, BranchCode));
        }

        public ActionResult ExportPatientEmergencyList(Guid BranchCode, int StatusId)
        {
            var emergencyContactInfos = reportService.GetPatientEmergencyContacts(StatusId, BranchCode);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Emergency List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientEmergencyList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Emergency List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Triage");
            headerRow.CreateCell(3).SetCellValue("Contact");
            headerRow.CreateCell(4).SetCellValue("Relationship");
            headerRow.CreateCell(5).SetCellValue("Contact Phone");
            headerRow.CreateCell(6).SetCellValue("Contact E-mail");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (emergencyContactInfos != null && emergencyContactInfos.Count > 0)
            {
                int rowNumber = 2;
                foreach (var emergencyContactInfo in emergencyContactInfos)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(emergencyContactInfo.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(emergencyContactInfo.PatientName);
                    row.CreateCell(2).SetCellValue(emergencyContactInfo.Triage);
                    row.CreateCell(3).SetCellValue(emergencyContactInfo.ContactName);
                    row.CreateCell(4).SetCellValue(emergencyContactInfo.ContactRelation);
                    row.CreateCell(5).SetCellValue(emergencyContactInfo.ContactPhoneHome);
                    row.CreateCell(6).SetCellValue(emergencyContactInfo.ContactEmailAddress);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Emergency List: {0}", emergencyContactInfos.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientEmergencyList_{0}.xls", DateTime.Now.Ticks));
        }

        public ActionResult ExportReferralList(int Status, DateTime StartDate, DateTime EndDate)
        {
            var referrals = reportService.GetReferralInfos(Current.AgencyId, Status, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Referral List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ReferralList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Agency Referral List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range:{0}-{1}", StartDate.ToShortDateString(), EndDate.ToShortDateString()));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Referral Date");
            headerRow.CreateCell(2).SetCellValue("Referral Physician");
            headerRow.CreateCell(3).SetCellValue("Gender");
            headerRow.CreateCell(4).SetCellValue("Birth Day");
            headerRow.CreateCell(5).SetCellValue("Address");
            headerRow.CreateCell(6).SetCellValue("City");
            headerRow.CreateCell(7).SetCellValue("State");
            headerRow.CreateCell(8).SetCellValue("Zip Code");
            headerRow.CreateCell(9).SetCellValue("Status");
            headerRow.CreateCell(10).SetCellValue("Other Referral Source");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (referrals != null && referrals.Count > 0)
            {
                int rowNumber = 2;
                foreach (var ReferralInfo in referrals)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(ReferralInfo.ReferralName);
                    row.CreateCell(1).SetCellValue(ReferralInfo.ReferralDate.ToShortDateString());
                    row.CreateCell(2).SetCellValue(ReferralInfo.PhysicianName);
                    row.CreateCell(3).SetCellValue(ReferralInfo.Gender);
                    row.CreateCell(4).SetCellValue(ReferralInfo.DOB.ToShortDateString());
                    row.CreateCell(5).SetCellValue(ReferralInfo.AddressLine1+" "+ReferralInfo.AddressLine2);
                    row.CreateCell(6).SetCellValue(ReferralInfo.AddressCity);
                    row.CreateCell(7).SetCellValue(ReferralInfo.AddressStateCode);
                    row.CreateCell(8).SetCellValue(ReferralInfo.AddressZipCode);
                    row.CreateCell(9).SetCellValue(ReferralInfo.StatusName);
                    row.CreateCell(10).SetCellValue(ReferralInfo.OtherReferralSource);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Referral List: {0}", referrals.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(11);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ReferralList_{0}.xls", DateTime.Now.Ticks));
        }

        public ActionResult ExportPatientEmergencyPreparednessList(Guid BranchCode, int StatusId)
        {
            var emergencyPreparednessInfos = reportService.GetPatientEmergencyPreparedness(StatusId, BranchCode);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Emergency List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientEmergencyPreparednessList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Emergency Preparedness List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Triage");
            headerRow.CreateCell(3).SetCellValue("Contact");
            headerRow.CreateCell(4).SetCellValue("Relationship");
            headerRow.CreateCell(5).SetCellValue("Contact Phone");
            headerRow.CreateCell(6).SetCellValue("Contact E-mail");
            headerRow.CreateCell(7).SetCellValue("Evacuation Address");
            headerRow.CreateCell(8).SetCellValue("Evacuation Zone");
            headerRow.CreateCell(9).SetCellValue("Evacuation Home Phone");
            headerRow.CreateCell(10).SetCellValue("Evacuation Mobile");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (emergencyPreparednessInfos != null && emergencyPreparednessInfos.Count > 0)
            {
                int rowNumber = 2;
                foreach (var emergencyPreparednessInfo in emergencyPreparednessInfos)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(emergencyPreparednessInfo.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(emergencyPreparednessInfo.PatientName);
                    row.CreateCell(2).SetCellValue(emergencyPreparednessInfo.Triage);
                    row.CreateCell(3).SetCellValue(emergencyPreparednessInfo.ContactName);
                    row.CreateCell(4).SetCellValue(emergencyPreparednessInfo.ContactRelation);
                    row.CreateCell(5).SetCellValue(emergencyPreparednessInfo.ContactPhoneHome);
                    row.CreateCell(6).SetCellValue(emergencyPreparednessInfo.ContactEmailAddress);
                    row.CreateCell(7).SetCellValue(emergencyPreparednessInfo.Address);
                    row.CreateCell(8).SetCellValue(emergencyPreparednessInfo.EvacuationZone);
                    row.CreateCell(9).SetCellValue(emergencyPreparednessInfo.EvacuationPhoneHomeFormatted);
                    row.CreateCell(10).SetCellValue(emergencyPreparednessInfo.EvacuationPhoneMobileFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Emergency List: {0}", emergencyPreparednessInfos.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(11);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientEmergencyPreparednessList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientBirthdayList()
        {
            ViewData["SortColumn"] = "Name";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/BirthdayList", patientRepository.GetPatientBirthdays(Current.AgencyId, Guid.Empty, DateTime.Now.Month));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientBirthdayListContent(Guid BranchCode, int Month, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/BirthdayListContent", patientRepository.GetPatientBirthdays(Current.AgencyId, BranchCode, Month));
        }

        public ActionResult ExportPatientBirthdayList(Guid BranchCode, int Month)
        {
            var birthDays = patientRepository.GetPatientBirthdays(Current.AgencyId, BranchCode, Month);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Birthday List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientBirthdayList");
            var titleRow = sheet.CreateRow(0);

            ICellStyle birthDateStyle = workbook.CreateCellStyle();
            birthDateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MMMM d");

            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Birthday List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (new DateTime(DateTime.Now.Year, Month, 1).ToString("MMMM"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Age");
            headerRow.CreateCell(3).SetCellValue("Birth Day");
            headerRow.CreateCell(4).SetCellValue("Address First Row");
            headerRow.CreateCell(5).SetCellValue("Address Second Row");
            headerRow.CreateCell(6).SetCellValue("Home Phone");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (birthDays != null && birthDays.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in birthDays)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.IdNumber);
                    row.CreateCell(1).SetCellValue(birthDay.Name);
                    row.CreateCell(2).SetCellValue(birthDay.Age.ToInteger());
                    row.CreateCell(3).SetCellValue(birthDay.BirthDay);
                    row.CreateCell(4).SetCellValue(birthDay.AddressFirstRow);
                    row.CreateCell(5).SetCellValue(birthDay.AddressSecondRow);
                    row.CreateCell(6).SetCellValue(birthDay.PhoneHome.ToPhone());
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Birthday List: {0}", birthDays.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientBirthdayList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientAddressList()
        {
            ViewData["SortColumn"] = "Name";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/AddressList", patientRepository.GetPatientAddressListing(Current.AgencyId, Guid.Empty, 1));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientAddressListContent(Guid BranchCode, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/AddressListContent", patientRepository.GetPatientAddressListing(Current.AgencyId, BranchCode, StatusId));
        }

        public ActionResult ExportPatientAddressList(Guid BranchCode, int StatusId)
        {
            var addressBookEntries = patientRepository.GetPatientAddressListing(Current.AgencyId, BranchCode, StatusId);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Address List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientAddressList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Address List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Address");
            headerRow.CreateCell(3).SetCellValue("City");
            headerRow.CreateCell(4).SetCellValue("State");
            headerRow.CreateCell(5).SetCellValue("Zip Code");
            headerRow.CreateCell(6).SetCellValue("Home Phone");
            headerRow.CreateCell(7).SetCellValue("Mobile Phone");
            headerRow.CreateCell(8).SetCellValue("Email Address");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (addressBookEntries != null && addressBookEntries.Count > 0)
            {
                int rowNumber = 2;
                foreach (var addressBook in addressBookEntries)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(addressBook.IdNumber);
                    row.CreateCell(1).SetCellValue(addressBook.Name);
                    row.CreateCell(2).SetCellValue(addressBook.AddressFirstRow);
                    row.CreateCell(3).SetCellValue(addressBook.AddressCity);
                    row.CreateCell(4).SetCellValue(addressBook.AddressStateCode);
                    row.CreateCell(5).SetCellValue(addressBook.AddressZipCode);
                    row.CreateCell(6).SetCellValue(addressBook.PhoneHome);
                    row.CreateCell(7).SetCellValue(addressBook.PhoneMobile);
                    row.CreateCell(8).SetCellValue(addressBook.EmailAddress);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Address List: {0}", addressBookEntries.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(9);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientAddressList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientByPhysicians()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/Physician", new List<PatientRoster>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PhysicianContent(Guid PhysicianId, string SortParams, string Type)
        {
            int status = Convert.ToInt32(Type);
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/Physician", PhysicianId.IsEmpty() ? new List<PatientRoster>() : status == 0 ? patientRepository.GetPatientByPhysician(Current.AgencyId, PhysicianId) : patientRepository.GetPatientByPhysician(Current.AgencyId, PhysicianId,status));
        }

        public ActionResult ExportPatientByPhysicians(Guid PhysicianId, string Type)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patients By Physician";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientByPhysicians");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patients By Physician");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var physician = PhysicianEngine.Get(PhysicianId, Current.AgencyId);
            if (physician != null)
            {
                titleRow.CreateCell(3).SetCellValue(string.Format("Physician : {0}", physician.DisplayName));
            }

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Address");
            headerRow.CreateCell(2).SetCellValue("City");
            headerRow.CreateCell(3).SetCellValue("State");
            headerRow.CreateCell(4).SetCellValue("Zip Code");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            headerRow.CreateCell(6).SetCellValue("Gender");

            int status = Convert.ToInt32(Type);
            List<PatientRoster> patientRosters;
            if(status==0)
                patientRosters = patientRepository.GetPatientByPhysician(Current.AgencyId, PhysicianId);
            else
                patientRosters = patientRepository.GetPatientByPhysician(Current.AgencyId, PhysicianId, status);
            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientDisplayName);
                    row.CreateCell(1).SetCellValue(patient.PatientAddressLine1);
                    row.CreateCell(2).SetCellValue(patient.PatientAddressCity);
                    row.CreateCell(3).SetCellValue(patient.PatientAddressStateCode);
                    row.CreateCell(4).SetCellValue(patient.PatientAddressZipCode);
                    row.CreateCell(5).SetCellValue(patient.PatientPhone);
                    row.CreateCell(6).SetCellValue(patient.PatientGender);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patients By Physician: {0}", patientRosters.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientByPhysicians_{0}.xls", DateTime.Now.Ticks));
        }

        [GridAction]
        public JsonResult PatientBirthdayWidget()
        {
            var viewData = new List<BirthdayWidget>();
            viewData = reportService.GetCurrentBirthdays();
            return Json(new GridModel(viewData));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientSocCertPeriodListing()
        {
            ViewData["SortColumn"] = "PatientLastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/PatientSocCertPeriodListing", reportService.GetPatientSocCertPeriod(Guid.Empty, 1, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientSocCertPeriodListingContent(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/PatientSocCertPeriodListingContent", reportService.GetPatientSocCertPeriod(BranchCode, StatusId, StartDate, EndDate));
        }

        public ActionResult ExportPatientSocCertPeriodListing(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient SOC Cert. Period Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientSocCertPeriodListing");
            var titleRow = sheet.CreateRow(0);

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient SOC Cert. Period Listing");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Middle Initial");
            headerRow.CreateCell(4).SetCellValue("SOC Date");
            headerRow.CreateCell(5).SetCellValue("SOC Cert. Period");
            headerRow.CreateCell(6).SetCellValue("Physician Name");
            headerRow.CreateCell(7).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var patientSocCertPeriods = reportService.GetPatientSocCertPeriod(BranchCode, StatusId, StartDate, EndDate);
            if (patientSocCertPeriods != null && patientSocCertPeriods.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patientSocCertPeriod in patientSocCertPeriods)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patientSocCertPeriod.PatientPatientID);
                    row.CreateCell(1).SetCellValue(patientSocCertPeriod.PatientLastName);
                    row.CreateCell(2).SetCellValue(patientSocCertPeriod.PatientFirstName);
                    row.CreateCell(3).SetCellValue(patientSocCertPeriod.PatientMiddleInitial);
                    DateTime patientSoCDate = patientSocCertPeriod.PatientSoC.IsValidDate() ? patientSocCertPeriod.PatientSoC.ToDateTime() : DateTime.MinValue;
                    if (patientSoCDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(patientSoCDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    row.CreateCell(5).SetCellValue(patientSocCertPeriod.SocCertPeriod);
                    row.CreateCell(6).SetCellValue(patientSocCertPeriod.PhysicianName);
                    row.CreateCell(7).SetCellValue(patientSocCertPeriod.respEmp);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient SOC Cert. Period Listing: {0}", patientSocCertPeriods.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientSocCertPeriodListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientOnCallListing()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientByResponsibleEmployeeListing()
        {
            ViewData["SortColumn"] = "PatientLastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/PatientByResponsibleEmployeeListing", new List<PatientRoster>());
        }

        [GridAction]
        public ActionResult PatientByResponsibleEmployeeListingContent(Guid BranchCode, Guid UserId, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/PatientByResponsibleEmployeeListingContent", UserId.IsEmpty() ? new List<PatientRoster>() : patientRepository.GetPatientByResponsiableEmployee(Current.AgencyId, UserId, BranchCode, StatusId));
        }

        public ActionResult ExportPatientByResponsibleEmployeeListing(Guid BranchCode, Guid UserId, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient By Responsible Employee Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientByResponsibleEmployeeListing");
            var titleRow = sheet.CreateRow(0);

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient By Responsible Employee Listing");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Employee : {0}", UserEngine.GetName(UserId, Current.AgencyId)));
            
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Middle Initial");
            headerRow.CreateCell(4).SetCellValue("Address");
            headerRow.CreateCell(5).SetCellValue("SOC Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (UserId.IsEmpty())
            {
                MemoryStream outputNoUser = new MemoryStream();
                workbook.Write(outputNoUser);
                return File(outputNoUser.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleEmployeeListing_{0}.xls", DateTime.Now.Ticks));
            }

            var patientRosters = patientRepository.GetPatientByResponsiableEmployee(Current.AgencyId, UserId, BranchCode, StatusId);
            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientId);
                    row.CreateCell(1).SetCellValue(patient.PatientLastName);
                    row.CreateCell(2).SetCellValue(patient.PatientFirstName);
                    row.CreateCell(3).SetCellValue(patient.PatientMiddleInitial);
                    row.CreateCell(4).SetCellValue(patient.AddressFull);
                    DateTime patientSoCDate = patient.PatientSoC.ToDateTime();
                    if (patientSoCDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(patientSoCDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient By Responsible Employee Listing: {0}", patientRosters.Count));

            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleEmployeeListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientByResponsibleCaseManagerListing()
        {
            ViewData["SortColumn"] = "PatientLastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/PatientByResponsibleCaseManager", new List<PatientRoster>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientByResponsibleCaseManagerContent(Guid BranchCode, Guid UserId, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }

            return PartialView("Patient/Content/PatientByResponsibleCaseManagerContent", UserId.IsEmpty() ? new List<PatientRoster>() : patientRepository.GetPatientByResponsiableByCaseManager(Current.AgencyId, UserId, BranchCode, StatusId));
        }

        public ActionResult ExportPatientByResponsibleCaseManager(Guid BranchCode, Guid UserId, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient By Responsible CaseManager";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientByResponsibleCaseManager");
            var titleRow = sheet.CreateRow(0);

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient By Responsible CaseManager");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Case Manager : {0}", UserEngine.GetName(UserId, Current.AgencyId)));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Middle Initial");
            headerRow.CreateCell(4).SetCellValue("Address");
            headerRow.CreateCell(5).SetCellValue("SOC Date");
            headerRow.CreateCell(6).SetCellValue("Phone Home");
            if (UserId.IsEmpty())
            {
                MemoryStream outputNoUser = new MemoryStream();
                workbook.Write(outputNoUser);
                return File(outputNoUser.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleCaseManager_{0}.xls", DateTime.Now.Ticks));
            }

            var patientRosters = patientRepository.GetPatientByResponsiableByCaseManager(Current.AgencyId, UserId, BranchCode, StatusId);

            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientId);
                    row.CreateCell(1).SetCellValue(patient.PatientLastName);
                    row.CreateCell(2).SetCellValue(patient.PatientFirstName);
                    row.CreateCell(3).SetCellValue(patient.PatientMiddleInitial);
                    row.CreateCell(4).SetCellValue(patient.AddressFull);
                    DateTime patientSoCDate = patient.PatientSoC.ToDateTime();
                    if (patientSoCDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(patientSoCDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    row.CreateCell(6).SetCellValue(patient.PatientPhone);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient By Responsible CaseManager: {0}", patientRosters.Count));

            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleCaseManager_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientExpiringAuthorizations()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/ExpiringAuthorizations", reportService.GetExpiringAuthorizaton(Guid.Empty, 1, "All", DateTime.Now, DateTime.Now.AddDays(60)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpiringAuthorizationsContent(Guid BranchCode, int StatusId, string AuthorizationStatus, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/ExpiringAuthorizationsContent", reportService.GetExpiringAuthorizaton(BranchCode, StatusId, AuthorizationStatus, StartDate, EndDate));
        }

        public ActionResult ExportExpiringAuthorizations(Guid BranchCode, int StatusId, string AuthorizationStatus, DateTime StartDate, DateTime EndDate)
        {
            var autorizations = reportService.GetExpiringAuthorizaton(BranchCode, StatusId, AuthorizationStatus, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Expiring Authorizations";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ExpiringAuthorizations");
            var titleRow = sheet.CreateRow(0);

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Expiring Authorizations");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Number");
            headerRow.CreateCell(3).SetCellValue("Start Date");
            headerRow.CreateCell(4).SetCellValue("End Date");
            if (autorizations != null && autorizations.Count > 0)
            {
                int rowNumber = 2;
                foreach (var auto in autorizations)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(auto.DisplayName);
                    row.CreateCell(1).SetCellValue(auto.Status);
                    row.CreateCell(2).SetCellValue(auto.Number1);
                    if (auto.StartDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(auto.StartDate);
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    if (auto.EndDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(auto.EndDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Expiring Authorizations: {0}", autorizations.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ExpiringAuthorizations_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientSurveyCensus()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/SurveyCensus", reportService.GetPatientSurveyCensus(Guid.Empty, 1,0, false));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientSurveyCensusContent(Guid BranchCode, int StatusId, int InsuranceId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/SurveyCensusContent", reportService.GetPatientSurveyCensus(BranchCode, StatusId, InsuranceId, false));
        }

        public ActionResult ExportPatientSurveyCensus(Guid BranchCode, int StatusId, int InsuranceId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Survey Census";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientSurveyCensus");
            var titleRow = sheet.CreateRow(0);

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Survey Census");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Medicare #");
            headerRow.CreateCell(3).SetCellValue("SSN");
            headerRow.CreateCell(4).SetCellValue("DOB");
            headerRow.CreateCell(5).SetCellValue("SOC Date");
            headerRow.CreateCell(6).SetCellValue("Cert. Period");
            headerRow.CreateCell(7).SetCellValue("Insurance");
            headerRow.CreateCell(8).SetCellValue("Primary Diagnosis");
            headerRow.CreateCell(9).SetCellValue("Secondary Diagnosis");
            headerRow.CreateCell(10).SetCellValue("Triage");
            headerRow.CreateCell(11).SetCellValue("Evacuation Zone");
            headerRow.CreateCell(12).SetCellValue("Disciplines");
            headerRow.CreateCell(13).SetCellValue("Phone");
            headerRow.CreateCell(14).SetCellValue("Address Line1");
            headerRow.CreateCell(15).SetCellValue("Address Line2");
            headerRow.CreateCell(16).SetCellValue("Physician");
            headerRow.CreateCell(17).SetCellValue("NPI");
            headerRow.CreateCell(18).SetCellValue("Physician Phone");
            headerRow.CreateCell(19).SetCellValue("Physician Fax");
            headerRow.CreateCell(20).SetCellValue("Case Manager");

            var surveyCensuses = reportService.GetPatientSurveyCensus(BranchCode, StatusId,InsuranceId, true);
            if (surveyCensuses != null && surveyCensuses.Count > 0)
            {
                int rowNumber = 2;
                foreach (var surveyCensus in surveyCensuses)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(surveyCensus.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(surveyCensus.PatientDisplayName);
                    row.CreateCell(2).SetCellValue(surveyCensus.MedicareNumber);
                    row.CreateCell(3).SetCellValue(surveyCensus.SSN);
                    if (surveyCensus.DOB != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(surveyCensus.DOB);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    if (surveyCensus.SOC != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(surveyCensus.SOC);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    row.CreateCell(6).SetCellValue(surveyCensus.CertPeriod);
                    row.CreateCell(7).SetCellValue(surveyCensus.InsuranceName);
                    row.CreateCell(8).SetCellValue(surveyCensus.PrimaryDiagnosis);
                    row.CreateCell(9).SetCellValue(surveyCensus.SecondaryDiagnosis);
                    row.CreateCell(10).SetCellValue(surveyCensus.Triage);
                    row.CreateCell(11).SetCellValue(surveyCensus.EvacuationZone);
                    row.CreateCell(12).SetCellValue(surveyCensus.Discipline);
                    row.CreateCell(13).SetCellValue(surveyCensus.Phone);
                    row.CreateCell(14).SetCellValue(surveyCensus.DisplayAddressLine1);
                    row.CreateCell(15).SetCellValue(surveyCensus.DisplayAddressLine2);
                    row.CreateCell(16).SetCellValue(surveyCensus.PhysicianDisplayName);
                    row.CreateCell(17).SetCellValue(surveyCensus.PhysicianNPI);
                    row.CreateCell(18).SetCellValue(surveyCensus.PhysicianPhone );
                    row.CreateCell(19).SetCellValue(surveyCensus.PhysicianFax.ToPhone());
                    row.CreateCell(20).SetCellValue(surveyCensus.CaseManagerDisplayName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Survey Census: {0}", surveyCensuses.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(21);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientSurveyCensus_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientSurveyCensusByDateRange()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/SurveyCensusByDateRange", reportService.GetPatientSurveyCensusByDateRange(Guid.Empty, 1, 0, DateTime.Now.AddDays(-60), DateTime.Now, false));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientSurveyCensusByDateRangeContent(Guid BranchCode, int StatusId, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/SurveyCensusByDateRangeContent", reportService.GetPatientSurveyCensusByDateRange(BranchCode, StatusId, InsuranceId, StartDate, EndDate, false));
        }

        public ActionResult ExportPatientSurveyCensusByDateRange(Guid BranchCode, int StatusId, int InsuranceId, DateTime startDate, DateTime endDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Survey Census";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientSurveyCensus");
            var titleRow = sheet.CreateRow(0);

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Survey Census (By Date Range)");            
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Medicare #");
            headerRow.CreateCell(3).SetCellValue("SSN");
            headerRow.CreateCell(4).SetCellValue("DOB");
            headerRow.CreateCell(5).SetCellValue("SOC Date");
            headerRow.CreateCell(6).SetCellValue("Discharged Date");
            headerRow.CreateCell(7).SetCellValue("Cert. Period");
            headerRow.CreateCell(8).SetCellValue("Insurance");
            headerRow.CreateCell(9).SetCellValue("Primary Diagnosis");
            headerRow.CreateCell(10).SetCellValue("Secondary Diagnosis");
            headerRow.CreateCell(11).SetCellValue("Triage");
            headerRow.CreateCell(12).SetCellValue("Evacuation Zone");
            headerRow.CreateCell(13).SetCellValue("Disciplines");
            headerRow.CreateCell(14).SetCellValue("Phone");
            headerRow.CreateCell(15).SetCellValue("Address Line1");
            headerRow.CreateCell(16).SetCellValue("Address Line2");
            headerRow.CreateCell(17).SetCellValue("Physician");
            headerRow.CreateCell(18).SetCellValue("NPI");
            headerRow.CreateCell(19).SetCellValue("Physician Phone");
            headerRow.CreateCell(20).SetCellValue("Physician Fax");
            headerRow.CreateCell(21).SetCellValue("Case Manager");

            var surveyCensuses = reportService.GetPatientSurveyCensusByDateRange(BranchCode, StatusId, InsuranceId, startDate, endDate, true);
            if (surveyCensuses != null && surveyCensuses.Count > 0)
            {
                int rowNumber = 2;
                foreach (var surveyCensus in surveyCensuses)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(surveyCensus.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(surveyCensus.PatientDisplayName);
                    row.CreateCell(2).SetCellValue(surveyCensus.MedicareNumber);
                    row.CreateCell(3).SetCellValue(surveyCensus.SSN);
                    if (surveyCensus.DOB != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(surveyCensus.DOB);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    if (surveyCensus.SOC != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(surveyCensus.SOC);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    if (surveyCensus.DischargedDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(6);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(surveyCensus.DischargedDate);
                    }
                    else
                    {
                        row.CreateCell(6).SetCellValue(string.Empty);
                    }
                    row.CreateCell(7).SetCellValue(surveyCensus.CertPeriod);
                    row.CreateCell(8).SetCellValue(surveyCensus.InsuranceName);
                    row.CreateCell(9).SetCellValue(surveyCensus.PrimaryDiagnosis);
                    row.CreateCell(10).SetCellValue(surveyCensus.SecondaryDiagnosis);
                    row.CreateCell(11).SetCellValue(surveyCensus.Triage);
                    row.CreateCell(12).SetCellValue(surveyCensus.EvacuationZone);
                    row.CreateCell(13).SetCellValue(surveyCensus.Discipline);
                    row.CreateCell(14).SetCellValue(surveyCensus.Phone);
                    row.CreateCell(15).SetCellValue(surveyCensus.DisplayAddressLine1);
                    row.CreateCell(16).SetCellValue(surveyCensus.DisplayAddressLine2);
                    row.CreateCell(17).SetCellValue(surveyCensus.PhysicianDisplayName);
                    row.CreateCell(18).SetCellValue(surveyCensus.PhysicianNPI);
                    row.CreateCell(19).SetCellValue(surveyCensus.PhysicianPhone);
                    row.CreateCell(20).SetCellValue(surveyCensus.PhysicianFax.ToPhone());
                    row.CreateCell(21).SetCellValue(surveyCensus.CaseManagerDisplayName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Survey Census: {0}", surveyCensuses.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(21);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientSurveyCensus_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientVitalSigns()
        {
            ViewData["SortColumn"] = "VisitDate";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/VitalSigns", new List<VitalSign>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult VitalSigns(Guid PatientId, DateTime StartDate, DateTime EndDate)
        {
            var vitalSigns = patientService.GetPatientVitalSigns(PatientId, StartDate, EndDate);
            var patient = patientRepository.GetPatientOnly(PatientId, Current.AgencyId);
            var export = new VitalSignExporter(vitalSigns.ToList(), StartDate, EndDate, patient != null ? patient.DisplayNameWithMi : string.Empty);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PatientVitalSigns_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VitalSignsContent(Guid PatientId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/VitalSignsContent",PatientId.IsEmpty() ? new List<VitalSign>() : patientService.GetPatientVitalSigns(PatientId, StartDate, EndDate));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientSixtyDaySummary()
        {
            ViewData["SortColumn"] = "UserDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/SixtyDaySummary", new List<VisitNoteViewData>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SixtyDaySummaryContent(Guid PatientId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/SixtyDaySummaryContent", PatientId.IsEmpty() ? new List<VisitNoteViewData>() : patientService.GetSixtyDaySummary(PatientId));
        }

        public ActionResult ExportPatientSixtyDaySummary(Guid PatientId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Sixty Day Summary";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("SixtyDaySummary");
            var titleRow = sheet.CreateRow(0);

            
            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Sixty Day Summary");
            
            titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Employee");
            headerRow.CreateCell(1).SetCellValue("Visit Date");
            headerRow.CreateCell(2).SetCellValue("Signature Date");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("Physician");
            if (PatientId.IsEmpty())
            {
                MemoryStream outputEmpty = new MemoryStream();
                workbook.Write(outputEmpty);
                return File(outputEmpty.ToArray(), "application/vnd.ms-excel", string.Format("PatientSixtyDaySummary_{0}.xls", DateTime.Now.Ticks));
            }
            else
            {
                var patient = patientRepository.GetPatientOnly(PatientId, Current.AgencyId);
                titleRow.CreateCell(2).SetCellValue("Patient: " + patient.DisplayNameWithMi);

                var sixtyDaySummaries = patientService.GetSixtyDaySummary(PatientId);
                if (sixtyDaySummaries != null && sixtyDaySummaries.Count > 0)
                {
                    int rowNumber = 2;
                    foreach (var sixtyDaySummary in sixtyDaySummaries)
                    {
                        var row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(sixtyDaySummary.UserDisplayName);
                        DateTime visitDate = sixtyDaySummary.VisitDate.ToDateTimeOrMin();
                        if (visitDate != DateTime.MinValue)
                        {
                            var createdDateCell = row.CreateCell(1);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(visitDate);
                        }
                        else
                        {
                            row.CreateCell(1).SetCellValue(string.Empty);
                        }
                        DateTime signatureDate = sixtyDaySummary.VisitDate.ToDateTimeOrMin();
                        if (signatureDate != DateTime.MinValue)
                        {
                            var createdDateCell = row.CreateCell(2);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(signatureDate);
                        }
                        else
                        {
                            row.CreateCell(2).SetCellValue(string.Empty);
                        }
                        row.CreateCell(3).SetCellValue(sixtyDaySummary.EpisodeRange);
                        row.CreateCell(4).SetCellValue(sixtyDaySummary.PhysicianDisplayName);
                        rowNumber++;
                    }
                    var totalRow = sheet.CreateRow(rowNumber + 2);
                    totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Sixty Day Summary: {0}", sixtyDaySummaries.Count));
                }
                workbook.FinishWritingToExcelSpreadsheet(5);
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientSixtyDaySummary_{0}.xls", DateTime.Now.Ticks));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargePatients()
        {
            ViewData["SortColumn"] = "LastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/DischargePatients", patientRepository.GetDischargePatients(Current.AgencyId, Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DischargePatientsContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            var item = patientRepository.GetDischargePatients(Current.AgencyId, BranchCode, StartDate, EndDate);
            return PartialView("Patient/Content/DischargePatientsContent", item);
        }

        public ActionResult ExportDischargePatients(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Discharge Patients";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("DischargePatients");
            var titleRow = sheet.CreateRow(0);

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Discharge Patients");
            titleRow.CreateCell(2).SetCellValue(string.Format("Date Range : {0} - {1} ", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Middile Initial");
            headerRow.CreateCell(4).SetCellValue("SOC Date");
            headerRow.CreateCell(5).SetCellValue("Discharge Date");
            headerRow.CreateCell(6).SetCellValue("Discharge Reason");
            headerRow.CreateCell(7).SetCellValue("Discharge Comments");

            var dischargePatients = patientRepository.GetDischargePatients(Current.AgencyId, BranchCode, StartDate, EndDate);
            if (dischargePatients != null && dischargePatients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in dischargePatients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(patient.LastName);
                    row.CreateCell(2).SetCellValue(patient.FirstName);
                    row.CreateCell(3).SetCellValue(patient.MiddleInitial);
                    if (patient.StartofCareDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(patient.StartofCareDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    if (patient.DischargeDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(patient.DischargeDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    row.CreateCell(6).SetCellValue(patient.DischargeReasonName);
                    row.CreateCell(7).SetCellValue(patient.DischargeReason);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Discharge Patients: {0}", dischargePatients.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("DischargePatients_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientList()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Patient/PatientList");
        }

        #endregion

        #region Clinical Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalTherapyManagement()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Clinical/TherapyManagement");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalOpenOasis()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/OpenOasis",reportService.GetAllOpenOasis(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OpenOasisContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/OpenOasis", reportService.GetAllOpenOasis(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportClinicalOpenOasis(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Open OASIS";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("OpenOASIS");
            var titleRow = sheet.CreateRow(0);

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Open OASIS");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Assessment Type");
            headerRow.CreateCell(3).SetCellValue("Date");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Employee");

            var openOasis = reportService.GetAllOpenOasis(BranchCode, StartDate, EndDate);
            if (openOasis != null && openOasis.Count > 0)
            {
                int rowNumber = 2;
                foreach (var oasis in openOasis)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(oasis.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(oasis.PatientName);
                    row.CreateCell(2).SetCellValue(oasis.AssessmentName);
                    //row.CreateCell(3).SetCellValue(oasis.Date);
                    DateTime date = oasis.Date.ToDateTimeOrMin();
                    if (date != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(date);
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    row.CreateCell(4).SetCellValue(oasis.Status);
                    row.CreateCell(5).SetCellValue(oasis.CurrentlyAssigned);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Open OASIS: {0}", openOasis.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalOpenOASIS_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalOrders()
        {
            return PartialView("Clinical/Orders");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalOrders([Bind] ReportParameters parameters)
        {
            return View(new GridModel(reportService.GetOrders(parameters.StatusId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalMissedVisit()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/MissedVisit", reportService.GetAllMissedVisit(Guid.Empty,  DateTime.Now.AddDays(-60),  DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisitContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/MissedVisit", reportService.GetAllMissedVisit(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportClinicalMissedVisit(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Missed Visit";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("MissedVisit");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Missed Visit");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Employee");

            var missedVisits = reportService.GetAllMissedVisit(BranchCode, StartDate, EndDate);
            if (missedVisits != null && missedVisits.Count > 0)
            {
                int rowNumber = 2;
                foreach (var missedVisit in missedVisits)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(missedVisit.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(missedVisit.PatientName);
                    if (missedVisit.Date != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(missedVisit.Date);
                    }
                    else
                    {
                        row.CreateCell(2).SetCellValue(string.Empty);
                    }
                    row.CreateCell(3).SetCellValue(missedVisit.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(missedVisit.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Missed Visit: {0}", missedVisits.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalMissedVisit_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalPhysicianOrderHistory()
        {
            ViewData["SortColumn"] = "OrderNumber";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/OrderHistory", reportService.GetPhysicianOrderHistory(Guid.Empty, 000, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrderHistoryContent(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/OrderHistory", reportService.GetPhysicianOrderHistory(BranchCode, StatusId, StartDate, EndDate));
        }

        public ActionResult ExportClinicalPhysicianOrderHistory(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Physician Order History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PhysicianOrderHistory");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Physician Order History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Order Number");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Physician");
            headerRow.CreateCell(3).SetCellValue("Order Date");
            headerRow.CreateCell(4).SetCellValue("Sent Date");
            headerRow.CreateCell(5).SetCellValue("Received Date");
            var physicianOrders = reportService.GetPhysicianOrderHistory(BranchCode, StatusId, StartDate, EndDate).ToList();
            if (physicianOrders != null && physicianOrders.Count > 0)
            {
                int rowNumber = 2;
                foreach (var physicianOrder in physicianOrders)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(physicianOrder.OrderNumber);
                    row.CreateCell(1).SetCellValue(physicianOrder.DisplayName);
                    row.CreateCell(2).SetCellValue(physicianOrder.PhysicianName);
                    if (physicianOrder.OrderDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(physicianOrder.OrderDate);
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    if (physicianOrder.SentDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(physicianOrder.SentDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    if (physicianOrder.ReceivedDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(physicianOrder.ReceivedDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Physician Order History: {0}", physicianOrders.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalPhysicianOrderHistory_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalPlanOfCareHistory()
        {
            ViewData["SortColumn"] = "Number";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/PlanOfCareHistory", reportService.GetPlanOfCareHistory(Guid.Empty, 000, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PlanOfCareHistoryContent(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/PlanOfCareHistory", reportService.GetPlanOfCareHistory(BranchCode, StatusId, StartDate, EndDate));
        }

        public ActionResult ExportClinicalPlanOfCareHistory(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Plan Of Care History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PlanOfCareHistory");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Plan Of Care History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Order Number");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Physician Name");
            headerRow.CreateCell(3).SetCellValue("Order Date");
            headerRow.CreateCell(4).SetCellValue("Sent Date");
            headerRow.CreateCell(5).SetCellValue("Received Date");
            var planOfCareHistories = reportService.GetPlanOfCareHistory(BranchCode, StatusId, StartDate, EndDate).ToList();
            if (planOfCareHistories != null && planOfCareHistories.Count > 0)
            {
                int rowNumber = 2;
                foreach (var planOfCareHistory in planOfCareHistories)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(planOfCareHistory.Number);
                    row.CreateCell(1).SetCellValue(planOfCareHistory.PatientName);
                    row.CreateCell(2).SetCellValue(planOfCareHistory.PhysicianName);

                    if (planOfCareHistory.CreatedDate.IsNotNullOrEmpty() && planOfCareHistory.CreatedDate.IsDate())
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(planOfCareHistory.CreatedDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }                    
                    if (planOfCareHistory.SendDate != DateTime.MinValue)
                    {
                        var sendDateCell = row.CreateCell(4);

                        sendDateCell.CellStyle = dateStyle;
                        sendDateCell.SetCellValue(planOfCareHistory.SendDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }      
                    if (planOfCareHistory.ReceivedDate != DateTime.MinValue)
                    {
                        var receiveDateCell = row.CreateCell(5);
                        receiveDateCell.CellStyle = dateStyle;
                        receiveDateCell.SetCellValue(planOfCareHistory.ReceivedDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }                    
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Plan Of Care History: {0}", planOfCareHistories.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalPlanOfCareHistory_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalThirteenAndNineteenVisitException()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC"; 
            return PartialView("Clinical/ThirteenAndNineteenVisitException",patientService.GetTherapyException(Guid.Empty, Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now ));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ThirteenAndNineteenVisitExceptionContent(Guid BranchCode, Guid PatientId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/ThirteenAndNineteenVisitException", patientService.GetTherapyException(BranchCode, PatientId, StartDate, EndDate));
        }

        public ActionResult ExportClinicalThirteenAndNineteenVisitException(Guid BranchCode, Guid PatientId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Thirteen And Nineteen Visit Exception";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ThirteenAndNineteen VisitException");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Thirteen And Nineteen Visit Exception");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToZeroFilled(), EndDate.ToZeroFilled())));


            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Scheduled");
            headerRow.CreateCell(4).SetCellValue("Completed");
            headerRow.CreateCell(5).SetCellValue("Day");
            headerRow.CreateCell(6).SetCellValue("13th Visit");
            headerRow.CreateCell(7).SetCellValue("19th Visit");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var visitExceptions = patientService.GetTherapyException(BranchCode, PatientId, StartDate, EndDate);
            if (visitExceptions != null && visitExceptions.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visitException in visitExceptions)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(visitException.PatientName);
                    row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                    row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                    row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                    row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                    row.CreateCell(6).SetCellValue(visitException.ThirteenVisit);
                    row.CreateCell(7).SetCellValue(visitException.NineteenVisit);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Thirteen And Nineteen Visit Exception: {0}", visitExceptions.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ThirteenAndNineteenVisitException_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalThirteenTherapyReevaluationException()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/ThirteenTherapyReevaluationException", patientService.GetTherapyReevaluationException(Guid.Empty, Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now, 13));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ThirteenTherapyReevaluationExceptionContent(Guid BranchCode, Guid PatientId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/ThirteenTherapyReevaluationException", patientService.GetTherapyReevaluationException(BranchCode, PatientId, StartDate, EndDate, 13));
        }

        public ActionResult ExportClinicalThirteenTherapyReevaluationException(Guid BranchCode, Guid PatientId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Thirteen Therapy Re-evaluation Exception";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("13th Therapy Re-evaluation Exception");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("13th Therapy Re-evaluation Exception Report");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Scheduled");
            headerRow.CreateCell(4).SetCellValue("Completed");
            headerRow.CreateCell(5).SetCellValue("Day");
            headerRow.CreateCell(6).SetCellValue("PT Re-eval");
            headerRow.CreateCell(7).SetCellValue("ST Re-eval");
            headerRow.CreateCell(8).SetCellValue("OT Re-eval");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var visitExceptions = patientService.GetTherapyReevaluationException(BranchCode, PatientId, StartDate, EndDate, 13);
            if (visitExceptions != null && visitExceptions.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visitException in visitExceptions)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(visitException.PatientName);
                    row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                    row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                    row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                    row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                    row.CreateCell(6).SetCellValue(visitException.PTEval);
                    row.CreateCell(7).SetCellValue(visitException.STEval);
                    row.CreateCell(8).SetCellValue(visitException.OTEval);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of 13th Therapy Re-evaluation Exception: {0}", visitExceptions.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(9);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("13th_Therapy_Re-evaluation_Exception_Report_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalNineteenTherapyReevaluationException()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/NineteenTherapyReevaluationException", patientService.GetTherapyReevaluationException(Guid.Empty, Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now, 19));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NineteenTherapyReevaluationExceptionContent(Guid BranchCode, Guid PatientId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/NineteenTherapyReevaluationException", patientService.GetTherapyReevaluationException(BranchCode, PatientId, StartDate, EndDate, 19));
        }

        public ActionResult ExportClinicalNineteenTherapyReevaluationException(Guid BranchCode, Guid PatientId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - 19th Therapy Re-evaluation Exception";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("19th Therapy Re-evaluation Exception");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("19th Therapy Re-evaluation Exception Report");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Scheduled");
            headerRow.CreateCell(4).SetCellValue("Completed");
            headerRow.CreateCell(5).SetCellValue("Day");
            headerRow.CreateCell(6).SetCellValue("PT Re-eval");
            headerRow.CreateCell(7).SetCellValue("ST Re-eval");
            headerRow.CreateCell(8).SetCellValue("OT Re-eval");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var visitExceptions = patientService.GetTherapyReevaluationException(BranchCode, PatientId, StartDate, EndDate, 19);
            if (visitExceptions != null && visitExceptions.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visitException in visitExceptions)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(visitException.PatientName);
                    row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                    row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                    row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                    row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                    row.CreateCell(6).SetCellValue(visitException.PTEval);
                    row.CreateCell(7).SetCellValue(visitException.STEval);
                    row.CreateCell(8).SetCellValue(visitException.OTEval);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of 19th Therapy Re-evaluation Exception: {0}", visitExceptions.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(9);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("19th_Therapy_Re-evaluation_Exception_Report_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #region Schedule Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SchedulePatientWeeklySchedule()
        {
            ViewData["SortColumn"] = "UserName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/PatientWeeklySchedule", new List<ScheduleEvent>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientWeeklyScheduleContent(Guid PatientId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/PatientWeeklySchedule", PatientId.IsEmpty() ? new List<ScheduleEvent>() : reportService.GetPatientScheduleEventsByDateRange(PatientId, DateTime.Today, DateTime.Today.AddDays(7)));
        }

        public ActionResult ExportSchedulePatientWeeklySchedule(Guid PatientId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Schedule Patient Weekly";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("SchedulePatientWeekly");
            var titleRow = sheet.CreateRow(0);
           
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Schedule Patient Weekly");
           
            titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", DateTime.Today.ToString("MM/dd/yyyy"), DateTime.Today.AddDays(7).ToString("MM/dd/yyyy"))));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Employee");
            if (PatientId.IsEmpty())
            {
                MemoryStream outputEmpty = new MemoryStream();
                workbook.Write(outputEmpty);
                return File(outputEmpty.ToArray(), "application/vnd.ms-excel", string.Format("SchedulePatientWeekly_{0}.xls", DateTime.Now.Ticks));
            }
            else
            {
                var patient = patientRepository.GetPatientOnly(PatientId, Current.AgencyId);
                titleRow.CreateCell(2).SetCellValue("Patient: " + patient.DisplayNameWithMi);
                var scheduleEvents = reportService.GetPatientScheduleEventsByDateRange(PatientId, DateTime.Today, DateTime.Today.AddDays(7));
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    int rowNumber = 2;
                    foreach (var evnt in scheduleEvents)
                    {
                        var row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                        row.CreateCell(1).SetCellValue(evnt.StatusName);
                        if (evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDateAndNotMin())
                        {
                            var createdDateCell = row.CreateCell(2);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(evnt.EventDate.ToDateTime());
                        }
                        else
                        {
                            row.CreateCell(2).SetCellValue(string.Empty);
                        }
                        if (evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDateAndNotMin())
                        {
                            var createdDateCell = row.CreateCell(3);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(evnt.VisitDate.ToDateTime());
                        }
                        else
                        {
                            row.CreateCell(3).SetCellValue(string.Empty);
                        }
                        row.CreateCell(4).SetCellValue(evnt.UserName);
                        rowNumber++;
                    }
                    var totalRow = sheet.CreateRow(rowNumber + 2);
                    totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Patient Weekly: {0}", scheduleEvents.Count));
                }
                workbook.FinishWritingToExcelSpreadsheet(5);
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("SchedulePatientWeekly_{0}.xls", DateTime.Now.Ticks));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientMonthlyScheduleListing()
        {
            ViewData["SortColumn"] = "UserName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/PatientMonthlySchedule", new List<ScheduleEvent>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientMonthlyScheduleContent(Guid PatientId, int Month, int Year, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            if (PatientId.IsEmpty() || Month <= 0)
            {
                return PartialView("Schedule/Content/PatientMonthlySchedule", new List<ScheduleEvent>());
            }
            var fromDate = DateUtilities.GetStartOfMonth(Month, Year);
            var toDate = DateUtilities.GetEndOfMonth(Month, Year);
            return PartialView("Schedule/Content/PatientMonthlySchedule", reportService.GetPatientScheduleEventsByDateRange(PatientId, fromDate, toDate));
        }

        public ActionResult ExportPatientMonthlySchedule(Guid PatientId, int Month, int Year)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Monthly Schedule";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientMonthlySchedule");
            var titleRow = sheet.CreateRow(0);
            
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Monthly Schedule");
            
            titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(4).SetCellValue(string.Format("Month : {0}", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");
            
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Employee");
            if (PatientId.IsEmpty() || Month <= 0)
            {
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientMonthlySchedule_{0}.xls", DateTime.Now.Ticks));
            }
            else
            {
                var patient = patientRepository.GetPatientOnly(PatientId, Current.AgencyId);
                titleRow.CreateCell(2).SetCellValue("Patient: " + patient.DisplayNameWithMi);

                var fromDate = DateUtilities.GetStartOfMonth(Month, Year);
                var toDate = DateUtilities.GetEndOfMonth(Month, Year);
                var scheduleEvents = reportService.GetPatientScheduleEventsByDateRange(PatientId, fromDate, toDate);
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    int rowNumber = 2;
                    foreach (var evnt in scheduleEvents)
                    {
                        var row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                        row.CreateCell(1).SetCellValue(evnt.StatusName);
                        if (evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDateAndNotMin())
                        {
                            var createdDateCell = row.CreateCell(2);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(evnt.EventDate.ToDateTime());
                        }
                        else
                        {
                            row.CreateCell(2).SetCellValue(string.Empty);
                        }
                        if (evnt.VisitDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDateAndNotMin())
                        {
                            var createdDateCell = row.CreateCell(3);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(evnt.VisitDate.ToDateTime());
                        }
                        else
                        {
                            row.CreateCell(3).SetCellValue(string.Empty);
                        }
                        row.CreateCell(4).SetCellValue(evnt.UserName);
                        rowNumber++;
                    }
                    var totalRow = sheet.CreateRow(rowNumber + 2);
                    totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Monthly Schedule: {0}", scheduleEvents.Count));
                }
                workbook.FinishWritingToExcelSpreadsheet(5);
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientMonthlySchedule_{0}.xls", DateTime.Now.Ticks));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleEmployeeWeekly()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/EmployeeWeekly", new List<UserVisit>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeWeeklyContent(Guid BranchCode, Guid UserId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/EmployeeWeekly", UserId.IsEmpty() ? new List<UserVisit>() : reportService.GetUserScheduleEventsByDateRange(UserId, BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportScheduleEmployeeWeekly(Guid BranchCode, Guid UserId, DateTime StartDate, DateTime EndDate)
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, CoreSettings.ExcelDirectory);
            var file = new FileStream(Path.Combine(path, "ScheduledVisitsTemplate.xls"), FileMode.Open, FileAccess.Read);
            var workbook = new HSSFWorkbook(file);

            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Schedule Employee Weekly";
            workbook.SummaryInformation = si;
            var sheet = workbook.GetSheetAt(0);
            workbook.SetSheetName(0, "ScheduleEmployeeWeekly");

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Employee:", UserEngine.GetName(UserId, Current.AgencyId));
            headers.Add("Effective Date:", DateTime.Today.ToString("MM/dd/yyyy"));
            headers.Add("Date Range:", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

            string agencyLocation = "All";
            if (!BranchCode.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, BranchCode);
                agencyLocation = location != null && location.Name.IsNotNullOrEmpty() ? location.Name : string.Empty;
            }

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            if (UserId.IsEmpty())
            {
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleEmployeeWeekly_{0}.xls", DateTime.Now.Ticks));
            }
            else
            {
                var userEvents = reportService.GetUserScheduleEventsByDateRange(UserId, BranchCode, StartDate, EndDate);
                if (userEvents != null && userEvents.Count > 0)
                {
                    int rowNumber = workbook.WriteHeaderOfExcelSpreadsheet(Current.AgencyName, agencyLocation, "Employee Weekly Schedule", headers);
                    sheet.CreateFreezePane(0, rowNumber, 0, rowNumber);
                    //workbook.CreateMergedRegionStyles();
                    foreach (var evnt in userEvents)
                    {
                        var row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(evnt.PatientName);
                        row.CreateCell(2).SetCellValue(evnt.TaskName);
                        row.CreateCell(4).SetCellValue(evnt.StatusName);

                        if (evnt.ScheduleDate.IsNotNullOrEmpty() && evnt.ScheduleDate.IsValidDateAndNotMin())
                        {
                            var createdDateCell = row.CreateCell(6);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(evnt.ScheduleDate.ToDateTime());
                        }
                        else
                        {
                            row.CreateCell(6).SetCellValue(string.Empty);
                        }
                        if (evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDateAndNotMin())
                        {
                            var createdDateCell = row.CreateCell(8);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(evnt.VisitDate.ToDateTime());
                        }
                        else
                        {
                            row.CreateCell(8).SetCellValue(string.Empty);
                        }
                        CellRangeAddress region = new CellRangeAddress(rowNumber, rowNumber, 0, 1);
                        CellRangeAddress region2 = new CellRangeAddress(rowNumber, rowNumber, 2, 3);
                        CellRangeAddress region3 = new CellRangeAddress(rowNumber, rowNumber, 4, 5);
                        CellRangeAddress region4 = new CellRangeAddress(rowNumber, rowNumber, 6, 7);
                        CellRangeAddress region5 = new CellRangeAddress(rowNumber, rowNumber, 8, 9);
                        
                        sheet.AddMergedRegion(region);
                        sheet.AddMergedRegion(region2);
                        sheet.AddMergedRegion(region3);
                        sheet.AddMergedRegion(region4);
                        sheet.AddMergedRegion(region5);

                        workbook.SetMergedRegionBorder((HSSFSheet)sheet, region);
                        workbook.SetMergedRegionBorder((HSSFSheet)sheet, region2);
                        workbook.SetMergedRegionBorder((HSSFSheet)sheet, region3);
                        workbook.SetMergedRegionBorder((HSSFSheet)sheet, region4);
                        workbook.SetMergedRegionBorder((HSSFSheet)sheet, region5);

                        rowNumber++;
                    }
                    var totalRow = sheet.CreateRow(rowNumber + 2);
                    totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Employee Weekly: {0}", userEvents.Count));
                }

                workbook.FinishWritingToExcelSpreadsheet(9, 7);
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleEmployeeWeekly_{0}.xls", DateTime.Now.Ticks));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleMonthlyWork()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/MonthlyWork", new List<UserVisit>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MonthlyWorkContent(Guid BranchCode, Guid UserId, int Month, int Year, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            if (UserId.IsEmpty() || Month <= 0)
            {
                return PartialView("Schedule/Content/MonthlyWork", new List<UserVisit>());
            }
            var fromDate = DateUtilities.GetStartOfMonth(Month, Year);
            var toDate = DateUtilities.GetEndOfMonth(Month, Year);
            return PartialView("Schedule/Content/MonthlyWork", reportService.GetUserScheduleEventsByDateRange(UserId, BranchCode, fromDate, toDate));
        }

        public ActionResult ExportScheduleMonthlyWork(Guid BranchCode, Guid UserId, int Month, int Year)
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, CoreSettings.ExcelDirectory);
            var file = new FileStream(Path.Combine(path, "ScheduledVisitsTemplate.xls"), FileMode.Open, FileAccess.Read);
            var workbook = new HSSFWorkbook(file);

            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Schedule Monthly Work";
            workbook.SummaryInformation = si;

            var sheet = workbook.GetSheetAt(0);
            workbook.SetSheetName(0, "ScheduleMonthlyWork");
            
            //var titleRow = sheet.CreateRow(0); 
            Dictionary<string, string> headers = new Dictionary<string,string>();
            headers.Add("Employee:", UserEngine.GetName(UserId, Current.AgencyId));
            headers.Add("Effective Date:", DateTime.Today.ToString("MM/dd/yyyy"));
            headers.Add("Month:", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty);
            headers.Add("Year:", (Year > 1999) ? (new DateTime(Year, Month, 1)).ToString("yyyy") : string.Empty);

            string agencyLocation = "All";
            if(!BranchCode.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, BranchCode);
                agencyLocation = location != null && location.Name.IsNotNullOrEmpty() ? location.Name : string.Empty;
            }

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            //var headerRow = sheet.CreateRow(1);
            //headerRow.CreateCell(0).SetCellValue("Patient");
            //headerRow.CreateCell(1).SetCellValue("Task");
            //headerRow.CreateCell(2).SetCellValue("Status");
            //headerRow.CreateCell(3).SetCellValue("Schedule Date");
            //headerRow.CreateCell(4).SetCellValue("Visit Date");
            if (UserId.IsEmpty() || Month <= 0)
            {
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("Employee_Monthly_Schedule_{0}.xls", DateTime.Now.Ticks));
            }
            else
            {
                var fromDate = DateUtilities.GetStartOfMonth(Month, Year);
                var toDate = DateUtilities.GetEndOfMonth(Month, Year);
                var userEvents = reportService.GetUserScheduleEventsByDateRange(UserId, BranchCode, fromDate, toDate);
                if (userEvents != null && userEvents.Count > 0)
                {
                    int rowNumber = workbook.WriteHeaderOfExcelSpreadsheet(Current.AgencyName, agencyLocation, "Employee Monthly Schedule", headers);
                    sheet.CreateFreezePane(0, rowNumber, 0, rowNumber);
                    BorderStyle style = BorderStyle.THIN;
                    workbook.CreateMergedRegionStyles();
                    foreach (var evnt in userEvents)
                    {
                        var row = sheet.CreateRow(rowNumber);
                        
                        row.CreateCell(0).SetCellValue(evnt.PatientName);
                        row.CreateCell(2).SetCellValue(evnt.TaskName);
                        row.CreateCell(4).SetCellValue(evnt.StatusName);
                        if (evnt.ScheduleDate.IsNotNullOrEmpty() && evnt.ScheduleDate.IsValidDateAndNotMin())
                        {
                            var createdDateCell = row.CreateCell(6);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(evnt.ScheduleDate.ToDateTime());
                        }
                        else
                        {
                            row.CreateCell(6).SetCellValue(string.Empty);
                        }
                        if (evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDateAndNotMin())
                        {
                            var createdDateCell = row.CreateCell(8);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(evnt.VisitDate.ToDateTime());
                        }
                        else
                        {
                            row.CreateCell(8).SetCellValue(string.Empty);
                        }
                        CellRangeAddress region = new CellRangeAddress(rowNumber, rowNumber, 0, 1);
                        CellRangeAddress region2 = new CellRangeAddress(rowNumber, rowNumber, 2, 3);
                        CellRangeAddress region3 = new CellRangeAddress(rowNumber, rowNumber, 4, 5);
                        CellRangeAddress region4 = new CellRangeAddress(rowNumber, rowNumber, 6, 7);
                        CellRangeAddress region5 = new CellRangeAddress(rowNumber, rowNumber, 8, 9);

                        sheet.AddMergedRegion(region);
                        sheet.AddMergedRegion(region2);
                        sheet.AddMergedRegion(region3);
                        sheet.AddMergedRegion(region4);
                        sheet.AddMergedRegion(region5);

                        workbook.SetMergedRegionBorder((HSSFSheet)sheet, region);
                        workbook.SetMergedRegionBorder((HSSFSheet)sheet, region2);
                        workbook.SetMergedRegionBorder((HSSFSheet)sheet, region3);
                        workbook.SetMergedRegionBorder((HSSFSheet)sheet, region4);
                        workbook.SetMergedRegionBorder((HSSFSheet)sheet, region5);

                        rowNumber++;
                    }
                    var totalRow = sheet.CreateRow(rowNumber + 2);
                    totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Monthly Work: {0}", userEvents.Count));
                }
                workbook.FinishWritingToExcelSpreadsheet(9, 7);
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleMonthlyWork_{0}.xls", DateTime.Now.Ticks));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SchedulePastDueVisits()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/PastDueVisits", new List<ScheduleEvent>()); //reportService.GetPastDueScheduleEvents(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PastDueVisitsContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/PastDueVisits", reportService.GetPastDueScheduleEvents(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportSchedulePastDueVisits(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Past Due Visits";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PastDueVisits");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Past Due Visits");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");
            
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Assigned To");

            var scheduleEvents = reportService.GetPastDueScheduleEvents(BranchCode, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    //row.CreateCell(2).SetCellValue(evnt.EventDate);
                    if (evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.EventDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(2).SetCellValue(string.Empty);
                    }
                    row.CreateCell(3).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(evnt.StatusName);
                    row.CreateCell(5).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Visits: {0}", scheduleEvents.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("SchedulePastDueVisits_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SchedulePastDueVisitsByDiscipline()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/PastDueVisitsByDiscipline", reportService.GetPastDueScheduleEventsByDiscipline(Guid.Empty, "Nursing", DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PastDueVisitsByDisciplineContent(Guid BranchCode, string Discipline, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/PastDueVisitsByDiscipline", reportService.GetPastDueScheduleEventsByDiscipline(BranchCode, Discipline, StartDate, EndDate));
        }

        public ActionResult ExportSchedulePastDueVisitsByDiscipline(Guid BranchCode, string Discipline, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Past Due Visits By Discipline";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PastDueVisitsByDiscipline");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Past Due Visits By Discipline");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
            titleRow.CreateCell(4).SetCellValue(string.Format("Discipline: {0}", Enum.IsDefined(typeof(Disciplines), Discipline) ? ((Disciplines)Enum.Parse(typeof(Disciplines), Discipline)).GetDescription() : string.Empty));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Assigned To");

            var scheduleEvents = reportService.GetPastDueScheduleEventsByDiscipline(BranchCode, Discipline, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    if (evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.EventDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(2).SetCellValue(string.Empty);
                    }
                    row.CreateCell(3).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Visits By Discipline: {0}", scheduleEvents.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PastDueVisitsByDiscipline_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleDailyWork()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/DailyWork", reportService.GetScheduleEventsByDateRange(Guid.Empty, DateTime.Now, DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DailyWorkContent(Guid BranchCode, DateTime Date, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/DailyWork", reportService.GetScheduleEventsByDateRange(BranchCode, Date, Date));
        }

        public ActionResult ExportScheduleDailyWork(DateTime Date, Guid BranchCode)
        {
            var scheduleEvents = reportService.GetScheduleEventsByDateRange(BranchCode, Date, Date);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Daily Work Schedule";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("DailyWorkSchedule");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Daily Work Schedule");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Task");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Schedule Date");
            headerRow.CreateCell(5).SetCellValue("Employee");
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(3).SetCellValue(evnt.StatusName);
                    if (evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.EventDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    row.CreateCell(5).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Daily Work Schedule: {0}", scheduleEvents.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("DailyWorkSchedule_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleCaseManagerTask()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/CaseManagerTask", reportService.GetCaseManagerScheduleByBranch(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CaseManagerTaskContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/CaseManagerTask", reportService.GetCaseManagerScheduleByBranch(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportScheduleCaseManagerTask(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var scheduleEvents = reportService.GetCaseManagerScheduleByBranch(BranchCode, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Case Manager Task";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("CaseManagerTask");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Case Manager Task");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Assigned To");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    if (evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.EventDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(2).SetCellValue(string.Empty);
                    }
                    row.CreateCell(3).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Case Manager Task: {0}", scheduleEvents.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("CaseManagerTask_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleDeviation()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/Deviation", reportService.GetScheduleDeviation(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeviationContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/Deviation", reportService.GetScheduleDeviation(BranchCode, StartDate, EndDate));
        }

        //public ActionResult ExportScheduleDeviation(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        //{
        //    var scheduleEvents = reportService.GetScheduleDeviation(BranchCode, StartDate, EndDate);
        //    var workbook = new HSSFWorkbook();
        //    var si = PropertySetFactory.CreateSummaryInformation();
        //    si.Subject = "Axxess Data Export - Schedule Deviation";
        //    workbook.SummaryInformation = si;
        //    var sheet = workbook.CreateSheet("ScheduleDeviation");
        //    var titleRow = sheet.CreateRow(0);
        //    titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
        //    titleRow.CreateCell(1).SetCellValue("Schedule Deviation");
        //    titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
        //    var headerRow = sheet.CreateRow(1);
        //    headerRow.CreateCell(0).SetCellValue("Task");
        //    headerRow.CreateCell(1).SetCellValue("Patient Name");
        //    headerRow.CreateCell(2).SetCellValue("MR #");
        //    headerRow.CreateCell(3).SetCellValue("Status");
        //    headerRow.CreateCell(4).SetCellValue("Employee");
        //    headerRow.CreateCell(5).SetCellValue("Schedule Date");
        //    headerRow.CreateCell(6).SetCellValue("Visit Date");
        //    sheet.CreateFreezePane(0, 2, 0, 2);
        //    if (scheduleEvents != null && scheduleEvents.Count > 0)
        //    {
        //        int rowNumber = 2;
        //        foreach (var evnt in scheduleEvents)
        //        {
        //            var row = sheet.CreateRow(rowNumber);
        //            row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
        //            row.CreateCell(1).SetCellValue(evnt.PatientName);
        //            row.CreateCell(2).SetCellValue(evnt.PatientIdNumber);
        //            row.CreateCell(3).SetCellValue(evnt.StatusName);
        //            row.CreateCell(4).SetCellValue(evnt.UserName);
        //            row.CreateCell(5).SetCellValue(evnt.EventDate);
        //            row.CreateCell(6).SetCellValue(evnt.VisitDate);
        //            rowNumber++;
        //        }
        //        var totalRow = sheet.CreateRow(rowNumber + 2);
        //        totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Deviation: {0}", scheduleEvents.Count));
        //    }
        //    sheet.AutoSizeColumn(0);
        //    sheet.AutoSizeColumn(1);
        //    sheet.AutoSizeColumn(2);
        //    sheet.AutoSizeColumn(3);
        //    sheet.AutoSizeColumn(4);
        //    sheet.AutoSizeColumn(5);
        //    sheet.AutoSizeColumn(6);
        //    MemoryStream output = new MemoryStream();
        //    workbook.Write(output);
        //    return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleDeviation_{0}.xls", DateTime.Now.Ticks));
        //}

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SchedulePastDueRecet()
        {
            ViewData["SortColumn"] = "OrderNumber";
            ViewData["SortDirection"] = "ASC";
            var agency = agencyRepository.Get(Current.AgencyId);
            var locationId = Guid.Empty;
            var payorId = 0;
            if (agency != null)
            {
                locationId = agency.MainLocation != null ? agency.MainLocation.Id : Guid.Empty;
                payorId = agency != null && agency.Payor.IsInteger() ? agency.Payor.ToInteger() : 0;
            }
            ViewData["ManLocationId"] = locationId;
            ViewData["Payor"] = payorId;
            return PartialView("Schedule/PastDueRecet", agencyService.GetRecertsPastDue(locationId, payorId, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PastDueRecetContent(Guid BranchCode, int InsuranceId, DateTime StartDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
           return PartialView("Schedule/Content/PastDueRecet",agencyService.GetRecertsPastDue(BranchCode, InsuranceId, StartDate, DateTime.Now));
        }

        public ActionResult ExportSchedulePastDueRecet(Guid BranchCode, int InsuranceId, DateTime StartDate)
        {
            var recertEvents = agencyService.GetRecertsPastDue(BranchCode, InsuranceId, StartDate, DateTime.Now);
            var export = new PastDueRecertExporter(recertEvents.ToList(), StartDate);
            return File(export.Process().GetBuffer(), "application/vnd.ms-excel", string.Format("PastDueRecerts_{0}.xls", DateTime.Now.Ticks));
            
            //var recets = agencyService.GetRecertsPastDue(BranchCode, InsuranceId, StartDate, DateTime.Now);
            //var workbook = new HSSFWorkbook();
            //var si = PropertySetFactory.CreateSummaryInformation();
            //si.Subject = "Axxess Data Export - Past Due Recet";
            //workbook.SummaryInformation = si;
            //var sheet = workbook.CreateSheet("PastDueRecet");
            //var titleRow = sheet.CreateRow(0);
            //titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            //titleRow.CreateCell(1).SetCellValue("Past Due Recet");
            //titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            //titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), DateTime.Now.ToString("MM/dd/yyyy"))));
            //var headerRow = sheet.CreateRow(1);
            //headerRow.CreateCell(0).SetCellValue("Patient Name");
            //headerRow.CreateCell(1).SetCellValue("MR #");
            //headerRow.CreateCell(2).SetCellValue("Employee Responsible");
            //headerRow.CreateCell(3).SetCellValue("Status");
            //headerRow.CreateCell(4).SetCellValue("Due Date");
            //headerRow.CreateCell(5).SetCellValue("Past Date");
            //sheet.CreateFreezePane(0, 2, 0, 2);
            //if (recets != null && recets.Count > 0)
            //{
            //    int rowNumber = 2;
            //    foreach (var evnt in recets)
            //    {
            //        var row = sheet.CreateRow(rowNumber);
            //        row.CreateCell(0).SetCellValue(evnt.PatientName);
            //        row.CreateCell(1).SetCellValue(evnt.PatientIdNumber);
            //        row.CreateCell(2).SetCellValue(evnt.AssignedTo);
            //        row.CreateCell(3).SetCellValue(evnt.Status);
            //        row.CreateCell(4).SetCellValue(evnt.TargetDate.ToString("MM/dd/yyyy"));
            //        row.CreateCell(5).SetCellValue(evnt.DateDifference);
            //        rowNumber++;
            //    }
            //    var totalRow = sheet.CreateRow(rowNumber + 2);
            //    totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Recet: {0}", recets.Count));
            //}
            //sheet.AutoSizeColumn(0);
            //sheet.AutoSizeColumn(1);
            //sheet.AutoSizeColumn(2);
            //sheet.AutoSizeColumn(3);
            //sheet.AutoSizeColumn(4);
            //sheet.AutoSizeColumn(5);

            //MemoryStream output = new MemoryStream();
            //workbook.Write(output);
            //return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PastDueRecet_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleUpcomingRecet()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            var agency = agencyRepository.Get(Current.AgencyId);
            var locationId = Guid.Empty;
            var payorId = 0;
            if (agency != null)
            {
                locationId = agency.MainLocation != null ? agency.MainLocation.Id : Guid.Empty;
                payorId = agency != null && agency.Payor.IsInteger()? agency.Payor.ToInteger() : 0;
            }
            ViewData["ManLocationId"] = locationId;
            ViewData["Payor"] = payorId;
            return PartialView("Schedule/UpcomingRecet", agencyService.GetRecertsUpcoming(locationId, payorId, DateTime.Now, DateTime.Now.AddDays(24)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpcomingRecetContent(Guid BranchCode, int InsuranceId, string SortParams, DateTime StartDate, DateTime EndDate)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
                
            }
            return PartialView("Schedule/Content/UpcomingRecet", agencyService.GetRecertsUpcoming(BranchCode, InsuranceId, StartDate, EndDate));
        }
        
        //TODO Replace exporter
        public ActionResult ExportScheduleUpcomingRecet(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var recets = agencyService.GetRecertsUpcoming(BranchCode, InsuranceId, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Upcoming Recert.";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UpcomingRecert");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Upcoming Recert.");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");
            
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Employee Responsible");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Scheduled Date");
            headerRow.CreateCell(5).SetCellValue("Due Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (recets != null && recets.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in recets)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.AssignedTo);
                    row.CreateCell(3).SetCellValue(evnt.Status);
                    if (evnt.ScheduledDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.ScheduledDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    if (evnt.TargetDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.TargetDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Upcoming Recert. : {0}", recets.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UpcomingRecert_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleVisitsByStatus()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            var agency = agencyRepository.Get(Current.AgencyId);
            var locationId = Guid.Empty;
            if (agency != null)
            {
                locationId = agency.MainLocation != null ? agency.MainLocation.Id : Guid.Empty;
            }
            ViewData["MainLocationId"] = locationId;
            return PartialView("Schedule/VisitsByStatus", new List<ScheduleEvent>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleVisitsByStatusContent(Guid BranchCode, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, string Status, string SortParams)
        {
            var statusArray = new List<int>();
            if (Status != "" && Status != null)
            {
                var statuses = Status.Split(',');
                
                if (statuses != null && statuses.Length > 0)
                {
                    statuses.ForEach(s => statusArray.Add(s.ToInteger()));
                }

                if (SortParams.IsNotNullOrEmpty())
                {
                    var paramArray = SortParams.Split('-');
                    if (paramArray.Length >= 2)
                    {
                        ViewData["SortColumn"] = paramArray[0];
                        ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                    }
                }
            }
                var scheduleEvents = statusArray != null ? patientService.GetScheduledEventsByStatus(BranchCode, PatientId, ClinicianId, StartDate, EndDate, statusArray) : new List<ScheduleEvent>();
                return PartialView("Schedule/Content/VisitsByStatus", scheduleEvents);
            
        }
        

        public ActionResult ExportScheduleVisitsByStatus(Guid BranchCode, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, string Status)
        {
            var statuses = Status.Split(',');
            var statusArray = new List<int>();
            if (statuses != null)
            {
                statuses.ForEach(s => statusArray.Add(s.ToInteger()));
            }
            var events = statusArray != null ? patientService.GetScheduledEventsByStatus(BranchCode, PatientId, ClinicianId, StartDate, EndDate, statusArray) : new List<ScheduleEvent>();
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Visits By Status";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("VisitsByStatus");
            var titleRow = sheet.CreateRow(0);
            string agencyLocation = "";
            if(!BranchCode.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, BranchCode);
                if(location != null && location.Name.IsNotNullOrEmpty())
                {
                    agencyLocation = " - " + location.Name;
                }
            }
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName + agencyLocation);
            titleRow.CreateCell(1).SetCellValue("Visits By Status.");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
            //titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", ((ScheduleStatus)status).GetDescription()));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Task");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Scheduled Date");
            headerRow.CreateCell(5).SetCellValue("Assigned To");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (events != null && events.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in events)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(3).SetCellValue(evnt.StatusName);
                    if (evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.EventDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    row.CreateCell(5).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total: {0}", events.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(5);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("VisitsByStatus_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleVisitsByType()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            var agency = agencyRepository.Get(Current.AgencyId);
            var locationId = Guid.Empty;
            if (agency != null)
            {
                locationId = agency.MainLocation != null ? agency.MainLocation.Id : Guid.Empty;
            }
            ViewData["MainLocationId"] = locationId;
            return PartialView("Schedule/VisitsByType",new List<ScheduleEvent>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleVisitsByTypeContent(Guid BranchCode, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, int type, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/VisitsByType", patientService.GetScheduledEventsByType(BranchCode, PatientId, ClinicianId, StartDate, EndDate, type));
        }

        public ActionResult ExportScheduleVisitsByType(Guid BranchCode, Guid PatientId, Guid ClinicianId, DateTime StartDate, DateTime EndDate, int type)
        {
            var events = patientService.GetScheduledEventsByType(BranchCode, PatientId, ClinicianId, StartDate, EndDate, type);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Visits By Type";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("VisitsByType");
            var titleRow = sheet.CreateRow(0);
            string agencyLocation = "";
            if (!BranchCode.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, BranchCode);
                if (location != null && location.Name.IsNotNullOrEmpty())
                {
                    agencyLocation = " - " + location.Name;
                }
            }
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName + agencyLocation);
            titleRow.CreateCell(1).SetCellValue("Visits By Type");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
            titleRow.CreateCell(4).SetCellValue(string.Format("Type: {0}", ((DisciplineTasks)type).GetDescription()));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Status");
            headerRow.CreateCell(3).SetCellValue("Scheduled Date");
            headerRow.CreateCell(4).SetCellValue("Assigned To");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (events != null && events.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in events)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.StatusName);
                    if (evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.EventDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    row.CreateCell(4).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total: {0}", events.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(5);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("VisitsByType_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleElectronicVisitVerificationLog()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/ElectronicVisitVerificationLog", reportService.GetVisitVerificationLogs(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now)); //reportService.GetPastDueScheduleEvents(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ElectronicVisitVerificationLogContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/ElectronicVisitVerificationLog", reportService.GetVisitVerificationLogs(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportElectronicVisitVerificationLog(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Visits Verification Log";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientVisitsVerificationLog");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("PatientVisitsVerificationLog");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Task Name");
            headerRow.CreateCell(2).SetCellValue("Assign To");
            headerRow.CreateCell(3).SetCellValue("Episode");
            headerRow.CreateCell(4).SetCellValue("Verified Address");
            headerRow.CreateCell(5).SetCellValue("Verified Time");
            headerRow.CreateCell(6).SetCellValue("Signature");
            headerRow.CreateCell(7).SetCellValue("Reason");
            var visitVerification = reportService.GetVisitVerificationLogs(BranchCode, StartDate, EndDate);
            if (visitVerification != null && visitVerification.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in visitVerification)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientName);
                    row.CreateCell(1).SetCellValue(evnt.TaskName);
                    row.CreateCell(2).SetCellValue(evnt.UserName);
                    row.CreateCell(3).SetCellValue(evnt.DateRange);
                    row.CreateCell(4).SetCellValue(evnt.VerifiedAddress);
                    row.CreateCell(5).SetCellValue(evnt.VerifiedDateTime.ToString("mm/dd/yyyy hh:mm:ss tt"));
                    row.CreateCell(6).SetCellValue(evnt.Signature);
                    row.CreateCell(7).SetCellValue(evnt.PatientUnableToSignReason);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Electronic Visit Verification Log: {0}", visitVerification.Count));
            }

            workbook.FinishWritingToExcelSpreadsheet(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("SchedulePatientVisitsVerificationLog_{0}.xls", DateTime.Now.Ticks));
        }


        #endregion

        #region Billing Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHRG()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Billing/HHRG");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnbilledVisitsForManagedClaims()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Billing/UnbilledVisits");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OutstandingClaims()
        {
            ViewData["SortColumn"] = "LastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/OutstandingClaims", reportService.UnProcessedBillViewData(Guid.Empty, "All",0));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OutstandingClaimsContent(Guid BranchCode, string Type, int InsuranceId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/OutstandingClaims", reportService.UnProcessedBillViewData(BranchCode, Type, InsuranceId));
        }

        public ActionResult ExportOutstandingClaims(Guid BranchCode, string Type, int insuranceId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Outstanding Claims";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("OutstandingClaims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Outstanding Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            var bills = reportService.UnProcessedBillViewData(BranchCode, Type, insuranceId);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.PatientDisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Outstanding Claims: {0}", bills.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("OutstandingClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClaimsByStatus()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/ClaimByStatus", reportService.BillViewDataByStatus(Guid.Empty, "All", 300, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimsByStatusContent(Guid BranchCode, string Type, int Status, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/ClaimByStatus", reportService.BillViewDataByStatus(BranchCode, Type, Status, StartDate, EndDate));
        }

        public ActionResult ExportClaimsByStatus(Guid BranchCode, string Type, int Status, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Claims By Status";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("Claims By Status");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Claims By Status Report");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", Enum.IsDefined(typeof(ScheduleStatus), Status) ? ((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), Status)).GetDescription() : string.Empty));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            ICellStyle currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            headerRow.CreateCell(4).SetCellValue("Claim Amount");
            headerRow.CreateCell(5).SetCellValue("Claim Date");
            headerRow.CreateCell(6).SetCellValue("Payment Amount");
            headerRow.CreateCell(7).SetCellValue("Payment Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = reportService.BillViewDataByStatus(BranchCode, Type, Status, StartDate, EndDate);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                double totalClaimAmount = 0;
                double totalPaymentAmount = 0;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    var claimAmountCell = row.CreateCell(4);
                    claimAmountCell.CellStyle = currencyStyle;
                    claimAmountCell.SetCellValue(bill.ClaimAmount);
                    if (bill.ClaimDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(bill.ClaimDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    var paymentAmountCell = row.CreateCell(6);
                    paymentAmountCell.CellStyle = currencyStyle;
                    paymentAmountCell.SetCellValue(bill.PaymentAmount);
                    if (bill.PaymentDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(7);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(bill.PaymentDate);
                    }
                    else
                    {
                        row.CreateCell(7).SetCellValue(string.Empty);
                    }
                    rowNumber++;
                    totalClaimAmount += bill.ClaimAmount;
                    totalPaymentAmount += bill.PaymentAmount;
                }
                var subTotalRow = sheet.CreateRow(rowNumber);
                subTotalRow.CreateCell(3).SetCellValue("Total:");
                subTotalRow.CreateCell(4).SetCellValue(string.Format("${0}", Math.Round(totalClaimAmount, 2)));
                subTotalRow.CreateCell(6).SetCellValue(string.Format("${0}", Math.Round(totalPaymentAmount, 2)));
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Claims By Status: {0}", bills.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("Claims_By_Status_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SubmittedClaims()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/SubmittedClaims");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmittedClaimsContent(Guid BranchCode, string Type, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/SubmittedClaims", reportService.SubmittedBillViewDataByDateRange(BranchCode, Type, StartDate, EndDate));
        }

        public ActionResult ExportSubmittedClaims(Guid BranchCode, string Type, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Claims By Status";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ClaimsByStatus");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Claims By Status");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            ICellStyle currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Claim Amount");
            headerRow.CreateCell(6).SetCellValue("Claim Date");
            headerRow.CreateCell(7).SetCellValue("Payment Amount");
            headerRow.CreateCell(8).SetCellValue("Payment Date");
            var bills = reportService.SubmittedBillViewDataByDateRange(BranchCode, Type, StartDate, EndDate);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                double totalClaimAmount = 0;
                double totalPayment = 0;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    row.CreateCell(4).SetCellValue(bill.StatusName);
                    var claimAmountCell = row.CreateCell(5);
                    claimAmountCell.CellStyle = currencyStyle;
                    claimAmountCell.SetCellValue(bill.ClaimAmount);
                    if (bill.ClaimDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(6);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(bill.ClaimDate);
                    }
                    else
                    {
                        row.CreateCell(6).SetCellValue(string.Empty);
                    }
                    var paymentAmountCell = row.CreateCell(7);
                    paymentAmountCell.CellStyle = currencyStyle;
                    paymentAmountCell.SetCellValue(bill.PaymentAmount);
                    if (bill.PaymentDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(8);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(bill.ClaimDate);
                    }
                    else
                    {
                        row.CreateCell(8).SetCellValue(string.Empty);
                    }
                    totalClaimAmount += bill.ClaimAmount;
                    totalPayment += bill.PaymentAmount;
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Claims By Status: {0}", bills.Count));
                totalRow.CreateCell(2).SetCellValue(string.Format("Total Claim Amount:${0}", Math.Round(totalClaimAmount, 2)));
                totalRow.CreateCell(5).SetCellValue(string.Format("Total Payment:${0}", Math.Round(totalPayment, 2)));
            }
            workbook.FinishWritingToExcelSpreadsheet(9);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("OutstandingClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExpectedSubmittedClaims()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/ExpectedSubmittedClaims");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpectedSubmittedClaimsContent(Guid BranchCode, string Type, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }

            return PartialView("Billing/Content/ExpectedSubmittedClaims", reportService.ExpectedSubmittedClaimsByDateRange(BranchCode, Type, StartDate, EndDate));
        }

        public ActionResult ExportExpectedSubmittedClaims(Guid BranchCode, string Type, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Expected Submitted Claims";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ExpectedSubmittedClaims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Expected Submitted Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            ICellStyle currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Claim Amount");

            var bills = reportService.ExpectedSubmittedClaimsByDateRange(BranchCode, Type, StartDate, EndDate);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                double totalAmount = 0;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    row.CreateCell(4).SetCellValue(bill.StatusName);
                    var claimAmountCell = row.CreateCell(5);
                    claimAmountCell.CellStyle = currencyStyle;
                    claimAmountCell.SetCellValue(bill.ClaimAmount);
                    totalAmount += bill.ClaimAmount;

                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Expected Submitted Claims {0}", bills.Count));
                totalRow.CreateCell(2).SetCellValue(string.Format("Total Claim Amount:${0}", Math.Round(totalAmount,2)));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ExpectedSubmittedClaims_{0}.xls", DateTime.Now.Ticks));
        }

        public ActionResult ExportPendingClaims(Guid branchId, string primaryInsurance, string insuranceName)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Pending Claims";
            workbook.SummaryInformation = si;
            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            ICellStyle currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;
            #region RAP claims
            var sheet = workbook.CreateSheet("PendingClaims-RAP");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Pending Claims");
            var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, branchId);
            if (agencyLocation != null)
            {
                titleRow.CreateCell(2).SetCellValue(string.Format("Branch: {0}", agencyLocation.Name));
            }
            titleRow.CreateCell(3).SetCellValue(string.Format("Insurance: {0}", insuranceName));
            titleRow.CreateCell(4).SetCellValue(string.Format("Effective Date:{0}",DateTime.Now.ToShortDateString()));

            
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Payment Date");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Medicare #");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("Claim Amount");
            headerRow.CreateCell(5).SetCellValue("Status");
            headerRow.CreateCell(6).SetCellValue("Payment Amount");

            var claims = billingService.PendingClaimRaps(branchId, primaryInsurance);
            if (claims != null && claims.Count > 0)
            {
                int rowNumber = 2;
                foreach (var claim in claims)
                {
                    var row = sheet.CreateRow(rowNumber);
                    if (claim.PaymentDate != DateTime.MinValue)
                    {
                        var PaymentDateCell = row.CreateCell(0);
                        PaymentDateCell.CellStyle = dateStyle;
                        PaymentDateCell.SetCellValue(claim.PaymentDate);
                    }
                    row.CreateCell(1).SetCellValue(claim.DisplayName);
                    row.CreateCell(2).SetCellValue(claim.MedicareNumber);
                    row.CreateCell(3).SetCellValue(claim.EpisodeRange);
                    var claimAmountCell = row.CreateCell(4);
                    claimAmountCell.CellStyle = currencyStyle;
                    claimAmountCell.SetCellValue(claim.ClaimAmount);
                    row.CreateCell(5).SetCellValue(claim.StatusName);
                    var PaymentAmountCell = row.CreateCell(6);
                    PaymentAmountCell.CellStyle = currencyStyle;
                    PaymentAmountCell.SetCellValue(claim.PaymentAmount);


                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Pending Claims - RAP{0}", claims.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(7);
            #endregion

            #region final claims
            var sheet1 = workbook.CreateSheet("PendingClaims-Final");
            var titleRow1 = sheet1.CreateRow(0);
            titleRow1.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow1.CreateCell(1).SetCellValue("Pending Claims");
            //var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, branchId);
            if (agencyLocation != null)
            {
                titleRow1.CreateCell(2).SetCellValue(string.Format("Branch: {0}", agencyLocation.Name));
            }
            titleRow1.CreateCell(3).SetCellValue(string.Format("Insurance: {0}", insuranceName));
            titleRow1.CreateCell(4).SetCellValue(string.Format("Effective Date:{0}", DateTime.Now.ToShortDateString()));


            var headerRow1 = sheet1.CreateRow(1);
            headerRow1.CreateCell(0).SetCellValue("Payment Date");
            headerRow1.CreateCell(1).SetCellValue("Patient");
            headerRow1.CreateCell(2).SetCellValue("Medicare #");
            headerRow1.CreateCell(3).SetCellValue("Episode Range");
            headerRow1.CreateCell(4).SetCellValue("Claim Amount");
            headerRow1.CreateCell(5).SetCellValue("Status");
            headerRow1.CreateCell(6).SetCellValue("Payment Amount");

            var finalclaims = billingService.PendingClaimFinals(branchId, primaryInsurance);
            if (finalclaims != null && finalclaims.Count > 0)
            {
                int rowNumber = 2;
                foreach (var claim in finalclaims)
                {
                    var row = sheet1.CreateRow(rowNumber);
                    if (claim.PaymentDate != DateTime.MinValue)
                    {
                        var PaymentDateCell = row.CreateCell(0);
                        PaymentDateCell.CellStyle = dateStyle;
                        PaymentDateCell.SetCellValue(claim.PaymentDate);
                    }
                    row.CreateCell(1).SetCellValue(claim.DisplayName);
                    row.CreateCell(2).SetCellValue(claim.MedicareNumber);
                    row.CreateCell(3).SetCellValue(claim.EpisodeRange);
                    var claimAmountCell = row.CreateCell(4);
                    claimAmountCell.CellStyle = currencyStyle;
                    claimAmountCell.SetCellValue(claim.ClaimAmount);
                    row.CreateCell(5).SetCellValue(claim.StatusName);
                    var PaymentAmountCell = row.CreateCell(6);
                    PaymentAmountCell.CellStyle = currencyStyle;
                    PaymentAmountCell.SetCellValue(claim.PaymentAmount);


                    rowNumber++;
                }
                var totalRow1 = sheet1.CreateRow(rowNumber + 2);
                totalRow1.CreateCell(0).SetCellValue(string.Format("Total Number Of Pending Claims - Final {0}", finalclaims.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(7, 0, 1);
            #endregion

            
            
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PendingClaims_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AccountsReceivable()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/AccountsReceivable",billingService.AccountsReceivables(Guid.Empty, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AccountsReceivableContent(Guid BranchCode, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            var viewData = new List<ClaimLean>();
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("RAP"))
                {
                    viewData = billingService.AccountsReceivableRaps(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("Final"))
                {
                    viewData = billingService.AccountsReceivableFinals(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("All"))
                {
                    viewData = billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
            }
            return PartialView("Billing/Content/AccountsReceivable", viewData);
        }

        public ActionResult ExportAccountsReceivable(Guid BranchCode, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Accounts Receivable";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("AccountsReceivable");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Accounts Receivable");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(5).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Claim Date");
            headerRow.CreateCell(6).SetCellValue("Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = new List<ClaimLean>();
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("RAP"))
                {
                    bills = billingService.AccountsReceivableRaps(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("Final"))
                {
                    bills = billingService.AccountsReceivableFinals(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("All"))
                {
                    bills = billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
            }
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                double totalAmount = 0;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    row.CreateCell(4).SetCellValue(bill.StatusName);
                    if (bill.ClaimDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(bill.ClaimDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    var claimAmountCell = row.CreateCell(6);
                    claimAmountCell.CellStyle = currencyStyle;
                    claimAmountCell.SetCellValue(Math.Round(bill.ClaimAmount, 2));
                    rowNumber++;
                    totalAmount += bill.ClaimAmount;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Accounts Receivable: {0}", bills.Count));
                totalRow.CreateCell(2).SetCellValue(string.Format("Total Claim Amount:${0}", Math.Round(totalAmount, 2)));
            }
            workbook.FinishWritingToExcelSpreadsheet(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("AccountsReceivable_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnearnedRevenue()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/UnearnedRevenue", billingService.GetUnearnedRevenue(Guid.Empty, 0, DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnearnedRevenueContent(Guid BranchCode, int InsuranceId, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/UnearnedRevenue", billingService.GetUnearnedRevenue(BranchCode, InsuranceId, EndDate));
        }

        public ActionResult ExportUnearnedRevenue(Guid BranchCode, int InsuranceId, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Unearned Revenue";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UnearnedRevenue");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Unearned Revenue Report");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Report Date: {0}", EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Assessment");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Episode Payment");
            headerRow.CreateCell(6).SetCellValue("Total Visits");
            headerRow.CreateCell(7).SetCellValue("Completed Visits");
            headerRow.CreateCell(8).SetCellValue("Unearned Visits");
            headerRow.CreateCell(9).SetCellValue("Unit Amount");
            headerRow.CreateCell(10).SetCellValue("Unearned Amount");
            headerRow.CreateCell(11).SetCellValue("Rap Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var revenue = billingService.GetUnearnedRevenue(BranchCode, InsuranceId, EndDate);

            if (revenue != null && revenue.Count > 0)
            {
                int rowNumber = 2;
                foreach (var rap in revenue)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(rap.DisplayName);
                    row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                    row.CreateCell(3).SetCellValue(rap.AssessmentTypeName);
                    row.CreateCell(4).SetCellValue(rap.RapStatusName);

                    if (rap.ProspectivePayment.IsNotNullOrEmpty())
                    {
                        var prospectivePaymentCell = row.CreateCell(5);
                        prospectivePaymentCell.CellStyle = currencyStyle;
                        prospectivePaymentCell.SetCellValue(rap.ProspectivePayment.ToCurrency());
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }

                    row.CreateCell(6).SetCellValue(rap.BillableVisitCount);
                    row.CreateCell(7).SetCellValue(rap.CompletedVisitCount);
                    row.CreateCell(8).SetCellValue(rap.UnearnedVisitCount);

                    var unitAmountCell = row.CreateCell(9);
                    unitAmountCell.CellStyle = currencyStyle;
                    unitAmountCell.SetCellValue(rap.UnitAmount);

                    var unearnedRevenueAmountCell = row.CreateCell(10);
                    unearnedRevenueAmountCell.CellStyle = currencyStyle;
                    unearnedRevenueAmountCell.SetCellValue(rap.UnearnedRevenueAmount);

                    var rapAmountCell = row.CreateCell(11);
                    rapAmountCell.CellStyle = currencyStyle;
                    rapAmountCell.SetCellValue(rap.RapAmount);

                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Unearned Revenue: {0}", revenue.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(12);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UnearnedRevenue_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnbilledRevenue()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/UnbilledRevenue", billingService.GetUnbilledRevenue(Guid.Empty, 0, DateTime.Now.AddDays(-29), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnbilledRevenueContent(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/UnbilledRevenue", billingService.GetUnbilledRevenue(BranchCode, InsuranceId, StartDate, EndDate));
        }

        public ActionResult ExportUnbilledRevenue(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Billed and Unbilled Revenue";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UnbilledRevenue");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Billed/Unbilled Revenue Report");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Report Date: {0}", EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Assessment");
            headerRow.CreateCell(4).SetCellValue("Payment");
            headerRow.CreateCell(5).SetCellValue("RAP Status");
            headerRow.CreateCell(6).SetCellValue("RAP Amount");
            headerRow.CreateCell(7).SetCellValue("EOE Status");
            headerRow.CreateCell(8).SetCellValue("EOE Amount");
            headerRow.CreateCell(9).SetCellValue("Billed Amount");
            headerRow.CreateCell(10).SetCellValue("Unbilled Amount");

            var revenue = billingService.GetUnbilledRevenue(BranchCode, InsuranceId, StartDate, EndDate);
            if (revenue != null && revenue.Count > 0)
            {
                int rowNumber = 2;
                double totalRapAmount = 0;
                double totalEOEAmount = 0;
                double totalBilledAmount = 0;
                double totalUnbilledAmount = 0;
                foreach (var claim in revenue)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(claim.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(claim.DisplayName);
                    row.CreateCell(2).SetCellValue(claim.EpisodeRange);
                    row.CreateCell(3).SetCellValue(claim.AssessmentTypeName);
                    if (claim.ProspectivePayment.IsNotNullOrEmpty())
                    {
                        var prospectivePaymentCell = row.CreateCell(4);
                        prospectivePaymentCell.CellStyle = currencyStyle;
                        prospectivePaymentCell.SetCellValue(claim.ProspectivePayment.ToCurrency());
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }

                    row.CreateCell(5).SetCellValue(claim.RapStatusName);

                    var rapAmountCell = row.CreateCell(6);
                    rapAmountCell.CellStyle = currencyStyle;
                    rapAmountCell.SetCellValue(claim.RapAmount);

                    row.CreateCell(7).SetCellValue(claim.RapStatusName);

                    var eoeAmountCell = row.CreateCell(8);
                    eoeAmountCell.CellStyle = currencyStyle;
                    eoeAmountCell.SetCellValue((double)claim.FinalAmount);

                    var billedAmountCell = row.CreateCell(9);
                    billedAmountCell.CellStyle = currencyStyle;
                    billedAmountCell.SetCellValue(claim.BilledRevenueAmount);

                    var unbilledRevenueAmountCell = row.CreateCell(10);
                    unbilledRevenueAmountCell.CellStyle = currencyStyle;
                    unbilledRevenueAmountCell.SetCellValue(claim.UnbilledRevenueAmount);
                    totalRapAmount += claim.RapAmount;
                    totalEOEAmount += (double)claim.FinalAmount;
                    totalBilledAmount += claim.BilledRevenueAmount;
                    totalUnbilledAmount += claim.UnbilledRevenueAmount;
                    rowNumber++;
                }
                var subTotalRow = sheet.CreateRow(rowNumber);
                subTotalRow.CreateCell(5).SetCellValue("Total:");
                subTotalRow.CreateCell(6).SetCellValue(string.Format("${0}", Math.Round(totalRapAmount,2)));
                subTotalRow.CreateCell(8).SetCellValue(string.Format("${0}", Math.Round(totalEOEAmount, 2)));
                subTotalRow.CreateCell(9).SetCellValue(string.Format("${0}", Math.Round(totalBilledAmount, 2)));
                subTotalRow.CreateCell(10).SetCellValue(string.Format("${0}", Math.Round(totalUnbilledAmount, 2)));
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total: {0}", revenue.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(11);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UnbilledRevenue_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EarnedRevenue()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/EarnedRevenue", billingService.GetEarnedRevenue(Guid.Empty, 0,  DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EarnedRevenueContent(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/EarnedRevenue", billingService.GetEarnedRevenue(BranchCode, InsuranceId, StartDate, EndDate));
        }

        public ActionResult ExportEarnedRevenue(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Earned/Unearned Revenue";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EarnedRevenue");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Earned/Unearned Revenue");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Assessment");
            headerRow.CreateCell(4).SetCellValue("Payment");
            headerRow.CreateCell(5).SetCellValue("RAP Status");
            headerRow.CreateCell(6).SetCellValue("RAP Amount");
            headerRow.CreateCell(7).SetCellValue("EOE Status");
            headerRow.CreateCell(8).SetCellValue("EOE Amount");
            headerRow.CreateCell(9).SetCellValue("Total Visits");
            headerRow.CreateCell(10).SetCellValue("Completed Visits");
            headerRow.CreateCell(11).SetCellValue("Unit Amount");
            headerRow.CreateCell(12).SetCellValue("Earned Amount");
            headerRow.CreateCell(13).SetCellValue("Unearned Amount");

            var revenue = billingService.GetEarnedRevenue(BranchCode, InsuranceId, StartDate, EndDate);
            if (revenue != null && revenue.Count > 0)
            {
                int rowNumber = 2;
                double totalPayment = 0;
                double totalRapAmount = 0;
                double totalEOEAmount = 0;
                int totalVisits = 0;
                int totalCompleteVisits = 0;
                double totalUnitAmount = 0;
                double totalEarnedAmount = 0;
                double totalUnearnedAmount = 0;
                foreach (var claim in revenue)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(claim.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(claim.DisplayName);
                    row.CreateCell(2).SetCellValue(claim.EpisodeRange);
                    row.CreateCell(3).SetCellValue(claim.AssessmentTypeName);

                    if (claim.ProspectivePayment.IsNotNullOrEmpty())
                    {
                        var prospectivePaymentCell = row.CreateCell(4);
                        prospectivePaymentCell.CellStyle = currencyStyle;
                        prospectivePaymentCell.SetCellValue(claim.ProspectivePayment.ToCurrency());
                        totalPayment += claim.ProspectivePayment.ToCurrency();
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }

                    row.CreateCell(5).SetCellValue(claim.RapStatusName);

                    var rapAmountCell = row.CreateCell(6);
                    rapAmountCell.CellStyle = currencyStyle;
                    rapAmountCell.SetCellValue(claim.RapAmount);
                    totalRapAmount += claim.RapAmount;
                    row.CreateCell(7).SetCellValue(claim.FinalStatusName);

                    var finalAmountCell = row.CreateCell(8);
                    finalAmountCell.CellStyle = currencyStyle;
                    finalAmountCell.SetCellValue((double)claim.FinalAmount);
                    totalEOEAmount += (double)claim.FinalAmount;
                    row.CreateCell(9).SetCellValue(claim.BillableVisitCount);
                    totalVisits += claim.BillableVisitCount;
                    row.CreateCell(10).SetCellValue(claim.CompletedVisitCount);
                    totalCompleteVisits += claim.CompletedVisitCount;
                    var unitAmountCell = row.CreateCell(11);
                    unitAmountCell.CellStyle = currencyStyle;
                    unitAmountCell.SetCellValue(claim.UnitAmount);
                    totalUnitAmount += claim.UnitAmount;
                    var earnedRevenueAmountCell = row.CreateCell(12);
                    earnedRevenueAmountCell.CellStyle = currencyStyle;
                    earnedRevenueAmountCell.SetCellValue(claim.EarnedRevenueAmount);
                    totalEarnedAmount += claim.EarnedRevenueAmount;
                    var unEarnedRevenueAmountCell = row.CreateCell(13);
                    unEarnedRevenueAmountCell.CellStyle = currencyStyle;
                    unEarnedRevenueAmountCell.SetCellValue(claim.UnearnedRevenueAmount);
                    totalUnearnedAmount += claim.UnearnedRevenueAmount;
                    rowNumber++;
                }
                var subTotalRow = sheet.CreateRow(rowNumber);
                subTotalRow.CreateCell(3).SetCellValue("Total:");
                subTotalRow.CreateCell(4).SetCellValue(string.Format("${0}",Math.Round(totalPayment,2)));
                subTotalRow.CreateCell(6).SetCellValue(string.Format("${0}", Math.Round(totalRapAmount, 2)));
                subTotalRow.CreateCell(8).SetCellValue(string.Format("${0}", Math.Round(totalEOEAmount, 2)));
                subTotalRow.CreateCell(9).SetCellValue(totalVisits);
                subTotalRow.CreateCell(10).SetCellValue(totalCompleteVisits);
                subTotalRow.CreateCell(11).SetCellValue(string.Format("${0}", Math.Round(totalUnitAmount, 2)));
                subTotalRow.CreateCell(12).SetCellValue(string.Format("${0}", Math.Round(totalEarnedAmount, 2)));
                subTotalRow.CreateCell(13).SetCellValue(string.Format("${0}", Math.Round(totalUnearnedAmount, 2)));
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total: {0}", revenue.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(14);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EarnedRevenue_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AgedAccountsReceivable()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/AgedAccountsReceivable", billingService.AccountsReceivables(Guid.Empty, 0,DateTime.Now.AddDays(-59),DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AgedAccountsReceivableContent(Guid BranchCode, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            var viewData = new List<ClaimLean>();
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("RAP"))
                {
                    viewData = billingService.AccountsReceivableRaps(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("Final"))
                {
                    viewData = billingService.AccountsReceivableFinals(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("All"))
                {
                    viewData = billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
            }
            return PartialView("Billing/Content/AgedAccountsReceivable", viewData);
        }

        public ActionResult ExportAgedAccountsReceivable(Guid BranchCode, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Aged Accounts Receivable";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("AgedAccountsReceivable");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Aged Accounts Receivable");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(5).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Claim Date");
            headerRow.CreateCell(6).SetCellValue("1-30");
            headerRow.CreateCell(7).SetCellValue("31-60");
            headerRow.CreateCell(8).SetCellValue("61-90");
            headerRow.CreateCell(9).SetCellValue("> 90");
            headerRow.CreateCell(10).SetCellValue("Total");
            var bills = new List<ClaimLean>();
            if (Type.IsNotNullOrEmpty())
            {
                if (Type.IsEqual("RAP"))
                {
                    bills = billingService.AccountsReceivableRaps(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("Final"))
                {
                    bills = billingService.AccountsReceivableFinals(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
                else if (Type.IsEqual("All"))
                {
                    bills = billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate).ToList();
                }
            }
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                double totalAmount30 = 0;
                double totalAmount60 = 0;
                double totalAmount90 = 0;
                double totalAmountOver90=0;
                double totalAmount = 0;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    row.CreateCell(4).SetCellValue(bill.StatusName);

                    if (bill.ClaimDate != DateTime.MinValue)
                    {
                        var claimDateCell = row.CreateCell(5);
                        claimDateCell.CellStyle = dateStyle;
                        claimDateCell.SetCellValue(bill.ClaimDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    var amount30Cell = row.CreateCell(6);
                    amount30Cell.CellStyle = currencyStyle;
                    amount30Cell.SetCellValue(Math.Round(bill.Amount30, 2));

                    var amount60Cell = row.CreateCell(7);
                    amount60Cell.CellStyle = currencyStyle;
                    amount60Cell.SetCellValue(Math.Round(bill.Amount60, 2));

                    var amount90Cell = row.CreateCell(8);
                    amount90Cell.CellStyle = currencyStyle;
                    amount90Cell.SetCellValue(Math.Round(bill.Amount90, 2));

                    var amountOver90Cell = row.CreateCell(9);
                    amountOver90Cell.CellStyle = currencyStyle;
                    amountOver90Cell.SetCellValue(Math.Round(bill.AmountOver90, 2));

                    var claimAmountCell = row.CreateCell(10);
                    claimAmountCell.CellStyle = currencyStyle;
                    claimAmountCell.SetCellValue(Math.Round(bill.ClaimAmount, 2));
                    rowNumber++;
                    totalAmount30 += bill.Amount30;
                    totalAmount60 += bill.Amount60;
                    totalAmount90 += bill.Amount90;
                    totalAmountOver90 += bill.AmountOver90;
                    totalAmount += bill.ClaimAmount;
                }
                var subTotalRow = sheet.CreateRow(rowNumber);
                subTotalRow.CreateCell(5).SetCellValue("Total:");
                subTotalRow.CreateCell(6).SetCellValue(string.Format("${0}",Math.Round(totalAmount30,2)));
                subTotalRow.CreateCell(7).SetCellValue(string.Format("${0}", Math.Round(totalAmount60, 2)));
                subTotalRow.CreateCell(8).SetCellValue(string.Format("${0}", Math.Round(totalAmount90, 2)));
                subTotalRow.CreateCell(9).SetCellValue(string.Format("${0}", Math.Round(totalAmountOver90, 2)));
                subTotalRow.CreateCell(10).SetCellValue(string.Format("${0}", Math.Round(totalAmount, 2)));
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Aged Accounts Receivable: {0}", bills.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(11);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("AgedAccountsReceivable_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSRAPClaims()
        {
            ViewData["SortColumn"] = "LastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/PPSRAPClaims", billingRepository.GetPPSRapClaims(Current.AgencyId, Guid.Empty));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PPSRAPClaimsContent(Guid BranchCode, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/PPSRAPClaims", billingRepository.GetPPSRapClaims(Current.AgencyId, BranchCode));
        }

        public ActionResult ExportPPSRAPClaims(Guid BranchCode)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PPS RAP Claims";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PPSRAPClaims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("PPS RAP Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("HIPPS");
            headerRow.CreateCell(4).SetCellValue("Amount");
            var raps = billingRepository.GetPPSRapClaims(Current.AgencyId, BranchCode).OrderBy(b => b.DisplayName).ToList();
            if (raps != null && raps.Count > 0)
            {
                int rowNumber = 2;
                double totalAmount = 0;
                foreach (var rap in raps)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(rap.DisplayName);
                    row.CreateCell(2).SetCellValue(rap.DateRange);
                    row.CreateCell(3).SetCellValue(rap.HippsCode);

                    var prospectivePayCell = row.CreateCell(4);
                    prospectivePayCell.CellStyle = currencyStyle;
                    prospectivePayCell.SetCellValue(rap.ProspectivePay);
                    rowNumber++;
                    totalAmount += rap.ProspectivePay;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of PPS RAP Claims: {0}", raps.Count));
                totalRow.CreateCell(1).SetCellValue(string.Format("Total Amount: ${0}", Math.Round(totalAmount,2)));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PPSRAPClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSFinalClaims()
        {
            ViewData["SortColumn"] = "LastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/PPSFinalClaims", billingRepository.GetPPSFinalClaims(Current.AgencyId, Guid.Empty));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PPSFinalClaimsContent(Guid BranchCode, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/PPSFinalClaims", billingRepository.GetPPSFinalClaims(Current.AgencyId, BranchCode));
        }

        public ActionResult ExportPPSFinalClaims(Guid BranchCode)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PPS Final Claims";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PPSFinalClaims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("PPS Final Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("HIPPS");
            headerRow.CreateCell(4).SetCellValue("Amount");
            var finals = billingRepository.GetPPSFinalClaims(Current.AgencyId, BranchCode).OrderBy(b => b.DisplayName).ToList();
            if (finals != null && finals.Count > 0)
            {
                int rowNumber = 2;
                double totalAmount = 0;
                foreach (var final in finals)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(final.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(final.DisplayName);
                    row.CreateCell(2).SetCellValue(final.DateRange);
                    row.CreateCell(3).SetCellValue(final.HippsCode);

                    var prospectivePayCell = row.CreateCell(4);
                    prospectivePayCell.CellStyle = currencyStyle;
                    prospectivePayCell.SetCellValue(final.ProspectivePay);
                    rowNumber++;
                    totalAmount += final.ProspectivePay;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of PPS Final Claims: {0}", finals.Count));
                totalRow.CreateCell(1).SetCellValue(string.Format("Total Amount: ${0}", Math.Round(totalAmount,2)));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PPSFinalClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PotentialClaimAutoCancel()
        {
            ViewData["SortColumn"] = "LastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/PotentialClaimAutoCancel", billingRepository.GetPotentialCliamAutoCancels(Current.AgencyId, Guid.Empty));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PotentialClaimAutoCancelContent(Guid BranchCode, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/PotentialClaimAutoCancel", billingRepository.GetPotentialCliamAutoCancels(Current.AgencyId, BranchCode));
        }

        public ActionResult ExportPotentialClaimAutoCancel(Guid BranchCode)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Potential Claim AutoCancel";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PotentialClaimAutoCancel");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Potential Claim AutoCancel");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            var finals = billingRepository.GetPotentialCliamAutoCancels(Current.AgencyId, BranchCode).OrderBy(b => b.DisplayName).ToList();
            if (finals != null && finals.Count > 0)
            {
                int rowNumber = 2;
                foreach (var final in finals)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(final.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(final.DisplayName);
                    row.CreateCell(2).SetCellValue(final.DateRange);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Potential Claim AutoCancel: {0}", finals.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PPSFinalClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult BillingBatch()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/BillingBatch", billingService.BillingBatch("ALL", DateTime.Now));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BillingBatchContent(string ClaimType, DateTime BatchDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/BillingBatch", billingService.BillingBatch(ClaimType, BatchDate));
        }

        public ActionResult ExportBillingBatch(string ClaimType, DateTime BatchDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Billing Batch Report";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("BillingBatch");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Billing Batch");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Claim Type: {0}", ClaimType.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date: {0}", BatchDate.ToString("MM/dd/yyyy")));

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;
            
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Batch Id");
            headerRow.CreateCell(1).SetCellValue("MRN");
            headerRow.CreateCell(2).SetCellValue("Patient");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("Medicare Number");
            headerRow.CreateCell(5).SetCellValue("Bill Type");
            headerRow.CreateCell(6).SetCellValue("Claim Amount");
            headerRow.CreateCell(7).SetCellValue("Prospective Amount");
            var bills = billingService.BillingBatch(ClaimType, BatchDate).ToList();

            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                double totalClaimAmount = 0;
                double totalProspectiveAmount = 0;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.BatchId);
                    row.CreateCell(1).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(2).SetCellValue(bill.DisplayName);
                    row.CreateCell(3).SetCellValue(bill.Range);
                    row.CreateCell(4).SetCellValue(bill.MedicareNumber);
                    row.CreateCell(5).SetCellValue(bill.BillType);

                    var claimAmountCell = row.CreateCell(6);
                    claimAmountCell.CellStyle = currencyStyle;
                    claimAmountCell.SetCellValue(Math.Round(bill.ClaimAmount, 2));

                    var prospectivePayCell = row.CreateCell(7);
                    prospectivePayCell.CellStyle = currencyStyle;
                    prospectivePayCell.SetCellValue(Math.Round(bill.ProspectivePay, 2));
                    rowNumber++;
                    totalClaimAmount += bill.ClaimAmount;
                    totalProspectiveAmount += bill.ProspectivePay;
                }
                var subTotalRow = sheet.CreateRow(rowNumber);
                subTotalRow.CreateCell(5).SetCellValue("Total:");
                subTotalRow.CreateCell(6).SetCellValue(string.Format("${0}", Math.Round(totalClaimAmount, 2)));
                subTotalRow.CreateCell(7).SetCellValue(string.Format("${0}", Math.Round(totalProspectiveAmount, 2)));
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number: {0}", bills.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("BillingBatch_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnearnedRevenueByEpisodeDays()
        {
            return PartialView("Billing/UnearnedRevenueByEpisodeDays");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EarnedRevenueByEpisodeDays()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/EarnedRevenueByEpisodeDays", billingService.GetEarnedRevenueByEpisodeDays(Guid.Empty, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EarnedRevenueByEpisodeDaysContent(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/EarnedRevenueByEpisodeDays", billingService.GetEarnedRevenueByEpisodeDays(BranchCode, InsuranceId, StartDate, EndDate));
        }

        public ActionResult ExportEarnedRevenueByEpisodeDays(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Earned Revenue By Episode Days";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EarnedRevenueEpisodeDays");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Earned Revenue By Episode Days");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Assessment");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Total Days");
            headerRow.CreateCell(6).SetCellValue("Episode Payment");
            headerRow.CreateCell(7).SetCellValue("Unit Amount");
            headerRow.CreateCell(8).SetCellValue("Completed Days");
            headerRow.CreateCell(9).SetCellValue("Earned Amount");
            var revenue = billingService.GetEarnedRevenueByEpisodeDays(BranchCode, InsuranceId, StartDate, EndDate);

            if (revenue != null && revenue.Count > 0)
            {
                int rowNumber = 2;
                int totalDays = 0;
                double totalPayment = 0;
                double totalUnitAmount = 0;
                int totalCompletedDays = 0;
                double totalEarnedAmount = 0;
                foreach (var rap in revenue)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(rap.DisplayName);
                    row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                    row.CreateCell(3).SetCellValue(rap.AssessmentTypeName);
                    row.CreateCell(4).SetCellValue(rap.RapStatusName);
                    row.CreateCell(5).SetCellValue(rap.EpisodeDays);
                    totalDays += rap.EpisodeDays;
                    if (rap.ProspectivePayment.IsNotNullOrEmpty())
                    {
                        var prospectivePaymentCell = row.CreateCell(6);
                        prospectivePaymentCell.CellStyle = currencyStyle;
                        prospectivePaymentCell.SetCellValue(rap.ProspectivePayment.ToCurrency());
                        totalPayment += rap.ProspectivePayment.ToCurrency();
                    }
                    else
                    {
                        row.CreateCell(6).SetCellValue(string.Empty);
                    }
                    var unitAmountCell = row.CreateCell(7);
                    unitAmountCell.CellStyle = currencyStyle;
                    unitAmountCell.SetCellValue(rap.UnitAmount);
                    totalUnitAmount += rap.UnitAmount;
                    row.CreateCell(8).SetCellValue(rap.CompletedDayCount);
                    totalCompletedDays += rap.CompletedDayCount;
                    var earnedRevenueAmountCell = row.CreateCell(9);
                    earnedRevenueAmountCell.CellStyle = currencyStyle;
                    earnedRevenueAmountCell.SetCellValue(rap.EarnedRevenueAmount);
                    totalEarnedAmount += rap.EarnedRevenueAmount;
                    rowNumber++;
                }
                var subTotalRow = sheet.CreateRow(rowNumber);
                subTotalRow.CreateCell(4).SetCellValue("Total:");
                subTotalRow.CreateCell(5).SetCellValue(totalDays);
                subTotalRow.CreateCell(6).SetCellValue(string.Format("${0}",Math.Round(totalPayment,2)));
                subTotalRow.CreateCell(7).SetCellValue(string.Format("${0}", Math.Round(totalUnitAmount, 2)));
                subTotalRow.CreateCell(8).SetCellValue(totalCompletedDays);
                subTotalRow.CreateCell(9).SetCellValue(string.Format("${0}", Math.Round(totalEarnedAmount, 2)));
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Earned Revenue: {0}", revenue.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(10);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EarnedRevenueByEpisodeDays_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult BilledRevenue()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/BilledRevenue", billingService.GetRevenueReport(Guid.Empty, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BilledRevenueContent(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/BilledRevenue", billingService.GetRevenueReport(BranchCode, InsuranceId, StartDate, EndDate));
        }

        public ActionResult ExportBilledRevenue(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Revenue Report";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("Revenue");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Revenue Report");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Assessment");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Episode Payment");
            headerRow.CreateCell(6).SetCellValue("Total Visits");
            headerRow.CreateCell(7).SetCellValue("Unit Amount");
            headerRow.CreateCell(8).SetCellValue("Earned Visits");
            headerRow.CreateCell(9).SetCellValue("Earned Amount");
            headerRow.CreateCell(10).SetCellValue("Unearned Visits");
            headerRow.CreateCell(11).SetCellValue("Unearned Amount");
            headerRow.CreateCell(12).SetCellValue("Unbilled Visits");
            headerRow.CreateCell(13).SetCellValue("Unbilled Amount");
            headerRow.CreateCell(14).SetCellValue("Billed Visits");
            headerRow.CreateCell(15).SetCellValue("Billed Amount");
            var revenue = billingService.GetRevenueReport(BranchCode, InsuranceId, StartDate, EndDate);
            if (revenue != null && revenue.Count > 0)
            {
                int rowNumber = 2;
                Double totalEarned = 0.0;
                Double totalUnEarned = 0.0;
                Double totalUnbilled = 0.0;
                Double totalBilled = 0.0;
                foreach (var rap in revenue)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(rap.DisplayName);
                    row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                    row.CreateCell(3).SetCellValue(rap.AssessmentType);
                    row.CreateCell(4).SetCellValue(rap.RapStatusName);

                    if (rap.ProspectivePayment.IsNotNullOrEmpty())
                    {
                        var prospectivePaymentCell = row.CreateCell(5);
                        prospectivePaymentCell.CellStyle = currencyStyle;
                        prospectivePaymentCell.SetCellValue(rap.ProspectivePayment.ToCurrency());
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }

                    row.CreateCell(6).SetCellValue(rap.BillableVisitCount);

                    var unitAmountCell = row.CreateCell(7);
                    unitAmountCell.CellStyle = currencyStyle;
                    unitAmountCell.SetCellValue(rap.UnitAmount);

                    row.CreateCell(8).SetCellValue(rap.CompletedVisitCount);

                    var earnedRevenueAmountCell = row.CreateCell(9);
                    earnedRevenueAmountCell.CellStyle = currencyStyle;
                    earnedRevenueAmountCell.SetCellValue(rap.EarnedRevenueAmount);

                    row.CreateCell(10).SetCellValue(rap.UnearnedVisitCount);

                    var unearnedRevenueAmountCell = row.CreateCell(11);
                    unearnedRevenueAmountCell.CellStyle = currencyStyle;
                    unearnedRevenueAmountCell.SetCellValue(rap.UnearnedRevenueAmount);

                    row.CreateCell(12).SetCellValue(rap.UnbilledVisitCount);

                    var unbilledRevenueAmountCell = row.CreateCell(13);
                    unbilledRevenueAmountCell.CellStyle = currencyStyle;
                    unbilledRevenueAmountCell.SetCellValue(rap.UnearnedRevenueAmount);

                    row.CreateCell(14).SetCellValue(rap.RapVisitCount);

                    var rapAmountCell = row.CreateCell(15);
                    rapAmountCell.CellStyle = currencyStyle;
                    rapAmountCell.SetCellValue(rap.RapAmount);

                    rowNumber++;
                    totalEarned += rap.EarnedRevenueAmount;
                    totalUnEarned += rap.UnearnedRevenueAmount;
                    totalUnbilled += rap.UnbilledRevenueAmount;
                    totalBilled += rap.RapAmount;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Revenue Items: {0}", revenue.Count));

                totalRow.CreateCell(8).SetCellValue("Total Earned:");
                var totalEarnedCell = totalRow.CreateCell(9);
                totalEarnedCell.CellStyle = currencyStyle;
                totalEarnedCell.SetCellValue(totalEarned);

                totalRow.CreateCell(10).SetCellValue("Total Unearned:");
                var totalUnEarnedCell = totalRow.CreateCell(11);
                totalUnEarnedCell.CellStyle = currencyStyle;
                totalUnEarnedCell.SetCellValue(totalUnEarned);

                totalRow.CreateCell(12).SetCellValue("Total Unbilled:");
                var totalUnbilledCell = totalRow.CreateCell(13);
                totalUnbilledCell.CellStyle = currencyStyle;
                totalUnbilledCell.SetCellValue(totalUnbilled);

                totalRow.CreateCell(14).SetCellValue("Total Billed:");
                var totalBilledCell = totalRow.CreateCell(15);
                totalBilledCell.CellStyle = currencyStyle;
                totalBilledCell.SetCellValue(totalBilled);

                var sumRow = sheet.CreateRow(rowNumber + 4);

                sumRow.CreateCell(8).SetCellValue("Earned + Unearned:");
                var earnedAndUnearnedCell = sumRow.CreateCell(9);
                earnedAndUnearnedCell.CellStyle = currencyStyle;
                earnedAndUnearnedCell.SetCellValue(totalEarned + totalUnEarned);

                sumRow.CreateCell(13).SetCellValue("Billed + Unbilled:");
                var billedAndUnbilled = sumRow.CreateCell(14);
                billedAndUnbilled.CellStyle = currencyStyle;
                billedAndUnbilled.SetCellValue(totalBilled + totalUnbilled);
            }

            workbook.FinishWritingToExcelSpreadsheet(16);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("RevenueReport_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedAccountsReceivable()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/ManagedAccountsReceivable", billingService.GetManagedCareClaimsAccountsReceivable(Current.AgencyId, Guid.Empty, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedAccountsReceivableContent(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/ManagedAccountsReceivable", billingService.GetManagedCareClaimsAccountsReceivable(Current.AgencyId, BranchCode, InsuranceId, StartDate, EndDate));
        }

        public ActionResult ExportManagedAccountsReceivable(Guid BranchCode, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -Managed Accounts Receivable";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ManagedAccountsReceivable");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Managed Accounts Receivable");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(5).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Insurance");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Claim Date");
            headerRow.CreateCell(6).SetCellValue("Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = billingService.GetManagedCareClaimsAccountsReceivable(Current.AgencyId, BranchCode, InsuranceId, StartDate, EndDate);
           
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                double totalAmount = 0;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.primaryInsurance);
                    row.CreateCell(3).SetCellValue(bill.DateRange);
                    row.CreateCell(4).SetCellValue(bill.StatusName);
                    if (bill.ClaimDate != DateTime.MinValue)
                    {
                        var createdDateCell = row.CreateCell(5);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(bill.ClaimDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    var claimAmountCell = row.CreateCell(6);
                    claimAmountCell.CellStyle = currencyStyle;
                    claimAmountCell.SetCellValue(Math.Round(bill.ProspectivePay, 2));
                    rowNumber++;
                    totalAmount += bill.ProspectivePay;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Managed Accounts Receivable: {0}", bills.Count));
                totalRow.CreateCell(2).SetCellValue(string.Format("Total Amount:${0}", Math.Round(totalAmount, 2)));
            }
            workbook.FinishWritingToExcelSpreadsheet(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ManagedAccountsReceivable_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedClaimsByStatus()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";

            return PartialView("Billing/ManagedClaimByStatus", billingService.GetManagedClaimsWithPaymentData(Guid.Empty, (int)ManagedClaimStatus.ClaimCreated, "0", DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimsByStatusContent(Guid BranchCode, string Type, int Status, string InsuranceId , DateTime StartDate, DateTime EndDate, string SortParams)
        {
            

                if (SortParams.IsNotNullOrEmpty())
                {
                    var paramArray = SortParams.Split('-');
                    if (paramArray.Length >= 2)
                    {
                        ViewData["SortColumn"] = paramArray[0];
                        ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                    }
                }
            
                var Claims = InsuranceId != null ? billingService.GetManagedClaimsWithPaymentData(BranchCode, Status, InsuranceId, StartDate, EndDate) : new List<ClaimLean>();
            return PartialView("Billing/Content/ManagedClaimByStatus", Claims);
            
        }

        public ActionResult ExportManagedClaimsByStatus(Guid BranchCode, string Type, int Status, string InsuranceId,DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Managed Claims By Status";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ManagedClaimsByStatus");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Managed Claims By Status");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", Enum.IsDefined(typeof(ScheduleStatus), Status) ? ((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), Status)).GetDescription() : string.Empty));

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Claim Amount");
            headerRow.CreateCell(4).SetCellValue("Claim Date");
            headerRow.CreateCell(5).SetCellValue("Payment Amount");
            headerRow.CreateCell(6).SetCellValue("Payment Date");
            headerRow.CreateCell(7).SetCellValue("Insurance");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = billingService.GetManagedClaimsWithPaymentData(BranchCode, Status, InsuranceId, StartDate, EndDate);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                double totalClaimAmount = 0;
                double totalPayment = 0;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    var claimAmountCell = row.CreateCell(3);
                    claimAmountCell.CellStyle = currencyStyle;
                    claimAmountCell.SetCellValue(bill.ClaimAmount);
                    totalClaimAmount += bill.ClaimAmount;
                    if (bill.ClaimDate.Date > DateTime.MinValue.Date)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(bill.ClaimDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    var paymentAmountCell = row.CreateCell(5);
                    paymentAmountCell.CellStyle = currencyStyle;
                    paymentAmountCell.SetCellValue(bill.PaymentAmount);
                    totalPayment += bill.PaymentAmount;
                    if (bill.PaymentDate.Date > DateTime.MinValue.Date)
                    {
                        var createdDateCell = row.CreateCell(6);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(bill.PaymentDate);
                    }
                    else
                    {
                        row.CreateCell(6).SetCellValue(string.Empty);
                    }
                    row.CreateCell(7).SetCellValue(bill.PrimaryInsuranceName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Managed Claims By Status: {0}", bills.Count));
                totalRow.CreateCell(3).SetCellValue(string.Format("Total Claim Amount:${0}", Math.Round(totalClaimAmount, 2)));
                totalRow.CreateCell(5).SetCellValue(string.Format("Total Claim Amount:${0}", Math.Round(totalPayment, 2)));
            }
            workbook.FinishWritingToExcelSpreadsheet(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ManagedClaimsByStatus_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnbilledManagedClaims()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/UnbilledManagedClaim", billingService.GetManagedClaimsWithPaymentData(Guid.Empty, (int)ManagedClaimStatus.ClaimCreated, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnbilledManagedClaimsContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/UnbilledManagedClaim", billingService.GetManagedClaimsWithPaymentData(BranchCode, (int)ManagedClaimStatus.ClaimCreated, StartDate, EndDate));
        }

        public ActionResult ExportUnbilledManagedClaims(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Unbilled Managed Claims";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UnbilledManagedClaims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Unbilled Managed Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var currencyStyle = workbook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Claim Amount");
            headerRow.CreateCell(4).SetCellValue("Claim Date");
            headerRow.CreateCell(5).SetCellValue("Payment Amount");
            headerRow.CreateCell(6).SetCellValue("Payment Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = billingService.GetManagedClaimsWithPaymentData(BranchCode, (int)ManagedClaimStatus.ClaimCreated, StartDate, EndDate);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                double totalClaimAmount = 0;
                double totalPaymentAmount = 0;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    var claimAmountCell = row.CreateCell(3);
                    claimAmountCell.CellStyle = currencyStyle;
                    claimAmountCell.SetCellValue(bill.ClaimAmount);
                    totalClaimAmount += bill.ClaimAmount;
                    if (bill.ClaimDate.Date > DateTime.MinValue.Date)
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(bill.ClaimDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    var paymentAmountCell = row.CreateCell(5);
                    paymentAmountCell.CellStyle = currencyStyle;
                    paymentAmountCell.SetCellValue(bill.PaymentAmount);
                    totalPaymentAmount += bill.PaymentAmount;
                    if (bill.PaymentDate.Date > DateTime.MinValue.Date)
                    {
                        var createdDateCell = row.CreateCell(6);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(bill.PaymentDate);
                    }
                    else
                    {
                        row.CreateCell(6).SetCellValue(string.Empty);
                    }
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Unbilled Managed Claims: {0}", bills.Count));
                totalRow.CreateCell(3).SetCellValue(string.Format("Total Claim Amount:${0}", Math.Round(totalClaimAmount, 2)));
                
                titleRow.CreateCell(5).SetCellValue(string.Format("Total Payment:${0}", Math.Round(totalPaymentAmount, 2)));
            }
            workbook.FinishWritingToExcelSpreadsheet(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UnbilledManagedClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManagedClaimPayments()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/ManagedClaimPayments", new List<ManagedClaimPayment>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedClaimPaymentsContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/ManagedClaimPayments", billingRepository.GetManagedClaims(Current.AgencyId, BranchCode, (int)ManagedClaimStatus.ClaimCreated, StartDate, EndDate));
        }

        #endregion

        #region Employee Reports

        public ActionResult PayrollDetailSummary()
        {
            return PartialView("Employee/PayrollDetailSummary");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeList()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Employee/List");
            //return PartialView("Employee/Roster", reportService.GetEmployeeRoster(Guid.Empty, 1));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeRoster()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/Roster", reportService.GetEmployeeRoster(Guid.Empty, 1));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeRosterContent(Guid BranchCode, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/Roster", reportService.GetEmployeeRoster(BranchCode, StatusId));
        }

        public ActionResult ExportEmployeeRoster(Guid BranchCode, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Roster";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeRoster");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Roster");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), StatusId) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), StatusId)).GetDescription() : string.Empty)));
            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Address");
            headerRow.CreateCell(2).SetCellValue("City");
            headerRow.CreateCell(3).SetCellValue("State");
            headerRow.CreateCell(4).SetCellValue("Zip Code");
            headerRow.CreateCell(5).SetCellValue("Mobile Phone");
            headerRow.CreateCell(6).SetCellValue("Gender");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var employeeRosters = reportService.GetEmployeeRoster(BranchCode, StatusId);
            if (employeeRosters != null && employeeRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var employee in employeeRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(employee.DisplayName);
                    row.CreateCell(1).SetCellValue(employee.Profile.Address);
                    row.CreateCell(2).SetCellValue(employee.Profile.AddressCity);
                    row.CreateCell(3).SetCellValue(employee.Profile.AddressStateCode);
                    row.CreateCell(4).SetCellValue(employee.Profile.AddressZipCode);
                    row.CreateCell(5).SetCellValue(employee.Profile.PhoneMobile.ToPhone());
                    row.CreateCell(6).SetCellValue(employee.Profile.Gender);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Roster: {0}", employeeRosters.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeRoster_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeBirthdayList()
        {
            ViewData["SortColumn"] = "Name";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/BirthdayList",reportService.GetEmployeeBirthdays(Guid.Empty, 1, DateTime.Now.Month) );
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BirthdayListContent(Guid BranchCode, int StatusId, int Month, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
           return PartialView("Employee/Content/BirthdayList",reportService.GetEmployeeBirthdays(BranchCode, StatusId, Month));
        }

        public ActionResult ExportEmployeeBirthdayList(Guid BranchCode, int StatusId, int Month)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Birthday List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeBirthdayList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Birthday List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), StatusId) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), StatusId)).GetDescription() : string.Empty)));

            var birthDateStyle = workbook.CreateCellStyle();
            birthDateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MMMM d");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Age");
            headerRow.CreateCell(2).SetCellValue("Birth Day");
            headerRow.CreateCell(3).SetCellValue("Address First Row");
            headerRow.CreateCell(4).SetCellValue("Address Second Row");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            var employeeBirthDays = reportService.GetEmployeeBirthdays(BranchCode, StatusId, Month);
            if (employeeBirthDays != null && employeeBirthDays.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in employeeBirthDays)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.Name);
                    row.CreateCell(1).SetCellValue(birthDay.Age.ToInteger());
                    row.CreateCell(2).SetCellValue(birthDay.BirthDay);
                    row.CreateCell(3).SetCellValue(birthDay.AddressFirstRow);
                    row.CreateCell(4).SetCellValue(birthDay.AddressSecondRow);
                    row.CreateCell(5).SetCellValue(birthDay.PhoneHome.ToPhone());
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Birthday List: {0}", employeeBirthDays.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeBirthdayList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeLicenseListing()
        {
            ViewData["SortColumn"] = "InitiationDate";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/License" , new List<License>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeLicenseContent(Guid UserId, string SortParams)
        {
            
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/License", UserId.IsEmpty() ? new List<License>() : userRepository.GetUserLicenses(Current.AgencyId, UserId,false));
        }

        public ActionResult ExportEmployeeLicenseListing(Guid UserId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee License Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeLicenseListing");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee License Listing");
            titleRow.CreateCell(2).SetCellValue("Employee: " + UserEngine.GetName(UserId, Current.AgencyId));
            titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("License Name");
            headerRow.CreateCell(1).SetCellValue("License Number");
            headerRow.CreateCell(2).SetCellValue("Initiation Date");
            headerRow.CreateCell(3).SetCellValue("Expiration Date");

            var employeeLicenses = userRepository.GetUserLicenses(Current.AgencyId, UserId , false);
            if (employeeLicenses != null && employeeLicenses.Count > 0)
            {
                int rowNumber = 2;
                foreach (var employeeLicense in employeeLicenses)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(employeeLicense.LicenseType);
                    row.CreateCell(1).SetCellValue(employeeLicense.LicenseNumber);
                    if (employeeLicense.InitiationDate != DateTime.MinValue)
                    {
                        var initiationDateCell = row.CreateCell(2);
                        initiationDateCell.CellStyle = dateStyle;
                        initiationDateCell.SetCellValue(employeeLicense.InitiationDate);
                    }
                    else
                    {
                        row.CreateCell(2).SetCellValue(string.Empty);
                    }
                    if (employeeLicense.ExpirationDate != DateTime.MinValue)
                    {
                        var expirationDateCell = row.CreateCell(3);
                        expirationDateCell.CellStyle = dateStyle;
                        expirationDateCell.SetCellValue(employeeLicense.ExpirationDate);
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee License Listing: {0}", employeeLicenses.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(4);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AllEmployeeLicenseListing()
        {
            ViewData["SortColumn"] = "InitiationDate";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/AllEmployeeLicense", userService.GetUserLicenses(Guid.Empty, 1));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllEmployeeLicenseContent(Guid BranchId, int StatusId, string SortParams)
        {

            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/AllEmployeeLicense", userService.GetUserLicenses(BranchId, StatusId));
        }

        public ActionResult ExportAllEmployeeLicenseListing(Guid BranchId, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - All Employee License Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("AllEmployeeLicenseListing");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("All Employee License Listing");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");
            
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Employee");
            headerRow.CreateCell(1).SetCellValue("License Name");
            headerRow.CreateCell(2).SetCellValue("License Number");
            headerRow.CreateCell(3).SetCellValue("Initiation Date");
            headerRow.CreateCell(4).SetCellValue("Expiration Date");

            var users = userRepository.GetAllUsers(Current.AgencyId,BranchId, StatusId);
            if (users != null && users.Count > 0)
            {
                int rowNumber = 2;
                users.ForEach(u =>
                {
                    var row = sheet.CreateRow(rowNumber);
                   
                    var rowCellStyle = workbook.CreateCellStyle();
                    rowCellStyle.BorderTop = BorderStyle.THIN;
                    row.RowStyle = rowCellStyle;

                    var cell = row.CreateCell(0);
                    var cellStyle = workbook.CreateCellStyle();
                    cell.SetCellValue(string.Format("{0}, {1} {2}", u.LastName, u.FirstName, u.CustomId.IsNotNullOrEmpty() ? " - " + u.CustomId : ""));

                    if (u.Licenses.IsNotNullOrEmpty())
                    {
                        u.LicensesArray = u.Licenses.ToObject<List<License>>();
                        if (u.LicensesArray != null && u.LicensesArray.Count > 0)
                        {
                            u.LicensesArray.ForEach(l =>
                            {
                                rowNumber++;
                                var licenseRow = sheet.CreateRow(rowNumber);
                                licenseRow.CreateCell(0).SetCellValue(string.Empty);
                                licenseRow.CreateCell(1).SetCellValue(l.LicenseType);
                                licenseRow.CreateCell(2).SetCellValue(l.LicenseNumber);
                                if (l.InitiationDate != DateTime.MinValue)
                                {
                                    var initiationDateCell = licenseRow.CreateCell(3);
                                    initiationDateCell.CellStyle = dateStyle;
                                    initiationDateCell.SetCellValue(l.InitiationDate);
                                }
                                else
                                {
                                    licenseRow.CreateCell(3).SetCellValue(string.Empty);
                                }
                                if (l.ExpirationDate != DateTime.MinValue)
                                {
                                    var expirationDateCell = licenseRow.CreateCell(4);
                                    expirationDateCell.CellStyle = dateStyle;
                                    expirationDateCell.SetCellValue(l.ExpirationDate);
                                }
                                else
                                {
                                    licenseRow.CreateCell(4).SetCellValue(string.Empty);
                                }
                            });
                        }
                    }
                    rowNumber = rowNumber + 2;
                });
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of  Employee : {0}", users.Count));
            }

            workbook.FinishWritingToExcelSpreadsheet(5);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("AllEmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeExpiringLicense()
        {
            ViewData["SortColumn"] = "UserDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/ExpiringLicenses", reportService.GetEmployeeExpiringLicenses(Guid.Empty, 1, string.Empty, DateTime.Now, DateTime.Now.AddDays(60)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpiringLicenseContent(Guid BranchCode, int StatusId, string SortParams, string LicenseType, DateTime StartDate, DateTime EndDate)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/ExpiringLicenses", reportService.GetEmployeeExpiringLicenses(BranchCode, StatusId, LicenseType, StartDate, EndDate));
        }

        public ActionResult ExportEmployeeExpiringLicense(Guid BranchCode, int StatusId, string LicenseType, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Expiring License";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeExpiringLicense");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Expiring License");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), StatusId) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), StatusId)).GetDescription() : string.Empty)));

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");
            
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Custom Id");
            headerRow.CreateCell(1).SetCellValue("Employee");
            headerRow.CreateCell(2).SetCellValue("License Name");
            headerRow.CreateCell(3).SetCellValue("License Number");
            headerRow.CreateCell(4).SetCellValue("Initiation Date");
            headerRow.CreateCell(5).SetCellValue("Expiration Date");

            var employeeLicenses = reportService.GetEmployeeExpiringLicenses(BranchCode, StatusId, LicenseType, StartDate, EndDate);
            if (employeeLicenses != null && employeeLicenses.Count > 0)
            {
                int rowNumber = 2;
                foreach (var employeeLicense in employeeLicenses)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(employeeLicense.CustomId);
                    row.CreateCell(1).SetCellValue(employeeLicense.UserDisplayName);
                    row.CreateCell(2).SetCellValue(employeeLicense.LicenseType);
                    row.CreateCell(3).SetCellValue(employeeLicense.LicenseNumber);

                    if (employeeLicense.InitiationDate != DateTime.MinValue)
                    {
                        var initiationDateCell = row.CreateCell(4);
                        initiationDateCell.CellStyle = dateStyle;
                        initiationDateCell.SetCellValue(employeeLicense.InitiationDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    if (employeeLicense.ExpirationDate != DateTime.MinValue)
                    {
                        var expirationDateCell = row.CreateCell(5);
                        expirationDateCell.CellStyle = dateStyle;
                        expirationDateCell.SetCellValue(employeeLicense.ExpirationDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Expiring License: {0}", employeeLicenses.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeExpiringLicenses_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NonUserExpiringLicense()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/NonUserExpiringLicenses", reportService.GetNonUserExpiringLicenses(string.Empty, DateTime.Now, DateTime.Now.AddDays(60)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonUserExpiringLicenseContent( string SortParams, string LicenseType, DateTime StartDate, DateTime EndDate)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/NonUserExpiringLicenses", reportService.GetNonUserExpiringLicenses( LicenseType, StartDate, EndDate));
        }

        public ActionResult ExportNonUserExpiringLicense( string LicenseType, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee(Non User) Expiring License";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeNonUserExpiringLicense");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee(Non User) Expiring License");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
           
            headerRow.CreateCell(0).SetCellValue("Employee");
            headerRow.CreateCell(1).SetCellValue("License Name");
            headerRow.CreateCell(2).SetCellValue("Issue Date");
            headerRow.CreateCell(3).SetCellValue("Expiration Date");

            var NonUserLicenses = reportService.GetNonUserExpiringLicenses(LicenseType, StartDate, EndDate);
            if (NonUserLicenses != null && NonUserLicenses.Count > 0)
            {
                int rowNumber = 2;
                foreach (var nonUserLicenses in NonUserLicenses)
                {
                    var row = sheet.CreateRow(rowNumber);

                    row.CreateCell(0).SetCellValue(nonUserLicenses.DisplayName);
                    row.CreateCell(1).SetCellValue(nonUserLicenses.LicenseType);

                    if (nonUserLicenses.IssueDate != DateTime.MinValue)
                    {
                        var initiationDateCell = row.CreateCell(2);
                        initiationDateCell.CellStyle = dateStyle;
                        initiationDateCell.SetCellValue(nonUserLicenses.IssueDate);
                    }
                    else
                    {
                        row.CreateCell(2).SetCellValue(string.Empty);
                    }
                    if (nonUserLicenses.ExpireDate != DateTime.MinValue)
                    {
                        var expirationDateCell = row.CreateCell(3);
                        expirationDateCell.CellStyle = dateStyle;
                        expirationDateCell.SetCellValue(nonUserLicenses.ExpireDate);
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee(Non User) Expiring License: {0}", NonUserLicenses.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeNonUserExpiringLicenses_{0}.xls", DateTime.Now.Ticks));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult EmployeePayrollDetailSummaryContent(Guid BranchCode, Guid EmployeeId, DateTime StartDate, DateTime EndDate, string PayrollStatus, string SortParams)
        //{
        //    var summary = payrollService.GetVisitsSummary(EmployeeId, StartDate, EndDate, PayrollStatus);
        //    return PartialView("Employee/Content/PayrollDetailSummary", summary);
        //}

        public ActionResult ExportPayrollDetailSummary(Guid employeeId, DateTime startDate, DateTime endDate, string payrollStatus)
        {
            int TASK_NAME = 0;
            int PRIMARY_PAYOR = 1;
            int TASK_COUNT = 2;
            int TASK_TIME = 3;
            int rowNumber = 3;
            int totalDays = 0;
            double totalHours = 0.0;
            double totalMinutes = 0.0;
            int totalTasks = 0;
            double totalMileage = 0.0;
            double totalSurcharges = 0.0;
            double totalPayment = 0.0;

            if (employeeId.IsEmpty() || employeeId.IsNull()) return null;
            if (startDate == null) return null;
            if (endDate == null) return null;
            if (payrollStatus.IsNullOrEmpty()) return null;

            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Author = "Axxess Consult";
            si.Subject = "Axxess Data Export - Payroll Detail Summary";
            workbook.SummaryInformation = si;

            User reportUser = userRepository.GetUserOnly(employeeId, Current.AgencyId);
            var summary = payrollService.GetVisitsSummary(employeeId, startDate, endDate, payrollStatus);

            var sheet = workbook.CreateSheet("Payroll Summary");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue(string.Empty);
            titleRow.CreateCell(2).SetCellValue(
                "Payroll Detail Summary For Employee: " +
                (reportUser.LastName.IsNotNullOrEmpty() && reportUser.FirstName.IsNotNullOrEmpty() ? reportUser.LastName + ", " + reportUser.FirstName : "Unknown"));
            titleRow.CreateCell(3).SetCellValue("Period: " + startDate.ToShortDateString() + " - " + endDate.ToShortDateString());

            var headerRow = sheet.CreateRow(2);
            headerRow.CreateCell(TASK_NAME).SetCellValue("Task Name");
            headerRow.CreateCell(PRIMARY_PAYOR).SetCellValue("Primary Payor");
            headerRow.CreateCell(TASK_COUNT).SetCellValue("Task Count");
            headerRow.CreateCell(TASK_TIME).SetCellValue("Task Time");
            headerRow.CreateCell(4).SetCellValue("Pay Rate");
            headerRow.CreateCell(5).SetCellValue("Total Mileage");
            headerRow.CreateCell(6).SetCellValue("Mileage Rate");
            headerRow.CreateCell(7).SetCellValue("Surcharges");
            headerRow.CreateCell(8).SetCellValue("Total Payment");

            if (summary != null && summary.Count > 0)
            {
                summary.ForEach<PayrollDetailSummaryItem>(summaryItem =>
                {
                    var row = sheet.CreateRow(rowNumber);
                    var col = 0;

                    ICell cell = row.CreateCell(col++);
                    cell.SetCellValue(summaryItem.TaskName);
                    row.CreateCell(col++).SetCellValue(summaryItem.Insurance);
                    if (summaryItem.TaskCount.IsInteger())
                        row.CreateCell(col++).SetCellValue(summaryItem.TaskCount.ToInteger());
                    else
                        row.CreateCell(col++).SetCellValue(string.Empty);


                    row.CreateCell(col++).SetCellValue(summaryItem.TotalTaskTime);
                    row.CreateCell(col++).SetCellValue(summaryItem.PayRate);
                    
                    row.CreateCell(col++).SetCellValue(summaryItem.TotalMileage);
                    row.CreateCell(col++).SetCellValue(summaryItem.MileageRage);
                    row.CreateCell(col++).SetCellValue(summaryItem.TotalSurcharges);
                    row.CreateCell(col++).SetCellValue(summaryItem.TotalPayment);
                    rowNumber++;

                    totalTasks += summaryItem.TaskCount.ToInteger();

                    string timeString = summaryItem.TotalTaskTime;
                    timeString = timeString.Remove( timeString.IndexOf("Mins."), "Mins.".Length);
                    timeString = timeString.Remove(timeString.IndexOf("("), "(".Length);
                    timeString = timeString.Remove(timeString.IndexOf("Hours)"), "Hours)".Length);

                    string[] hoursAndMinutes = timeString.Trim().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    totalMinutes += hoursAndMinutes[0].ToInteger();

                    totalMileage += summaryItem.TotalMileage;
                    totalSurcharges += summaryItem.TotalSurcharges;
                    totalPayment += summaryItem.TotalPayment;
                });
            }

            sheet.CreateRow(rowNumber++);
            var summaryRow = sheet.CreateRow(rowNumber);
            summaryRow.CreateCell(TASK_NAME).SetCellValue("Report Totals:");
            summaryRow.CreateCell(TASK_COUNT).SetCellValue(totalTasks);

            string totalTimeStr = string.Format("{0:0.00} Mins", totalMinutes) +
                string.Format(", {0:0.00} Hours", (totalMinutes / 60.00));
            

            summaryRow.CreateCell(TASK_TIME).SetCellValue(totalTimeStr);
            summaryRow.CreateCell(5).SetCellValue(String.Format("{0:0.00}", totalMileage));
            ICell c = summaryRow.CreateCell(7);
            c.SetCellValue(string.Format("{0:C}", totalSurcharges));
            summaryRow.CreateCell(8).SetCellValue(string.Format("{0:C}", totalPayment));

            workbook.FinishWritingToExcelSpreadsheet(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PayrollDetailSummary_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeePayrollDetailSummaryContent(Guid EmployeeId, DateTime StartDate, DateTime EndDate, string PayrollStatus, string SortParams)
        {
            List<PayrollDetailSummaryItem> summary = new List<PayrollDetailSummaryItem>();

            if( EmployeeId.IsEmpty() == false ) 
                summary = payrollService.GetVisitsSummary(EmployeeId, StartDate, EndDate, PayrollStatus);

            return PartialView("Employee/Content/PayrollDetailSummary", summary);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeVisitByDateRange()
        {
            ViewData["SortColumn"] = "UserDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/ScheduleByDateRange", reportService.GetEmployeeScheduleByDateRange(Guid.Empty, DateTime.Now.AddDays(-15), DateTime.Now.AddDays(15)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeScheduleByDateRangeContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/ScheduleByDateRange", reportService.GetEmployeeScheduleByDateRange(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportEmployeeVisitByDateRange(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Visit By Date Range";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeVisitByDateRange");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Visit By Date Range");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");
            
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Employee");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Task");
            headerRow.CreateCell(3).SetCellValue("Schedule Date");
            headerRow.CreateCell(4).SetCellValue("Visit Date");
            headerRow.CreateCell(5).SetCellValue("Status");

            var employeeVisits = reportService.GetEmployeeScheduleByDateRange(BranchCode, StartDate, EndDate);
            if (employeeVisits != null && employeeVisits.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visit in employeeVisits)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visit.UserDisplayName);
                    row.CreateCell(1).SetCellValue(visit.PatientName);
                    row.CreateCell(2).SetCellValue(visit.TaskName);

                    if (visit.ScheduleDate.IsNotNullOrEmpty() && visit.ScheduleDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(visit.ScheduleDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    if (visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(visit.VisitDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }

                    row.CreateCell(5).SetCellValue(visit.StatusName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Visit By Date Range: {0}", employeeVisits.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeVisitByDateRange_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeePermissions()
        {
            ViewData["SortColumn"] = "UserDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/Permissions", reportService.GetEmployeePermissions(Guid.Empty, 1));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeePermissionsContent(Guid BranchCode, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/Permissions", reportService.GetEmployeePermissions(BranchCode, StatusId));
        }

        public ActionResult ExportEmployeePermissionsListing(Guid BranchCode, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Permissions Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeePermissionListing");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Permissions Listing");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Employee");
            headerRow.CreateCell(1).SetCellValue("Permission");

            var users = userRepository.GetAllUsers(Current.AgencyId, BranchCode, StatusId);
            if (users != null && users.Count > 0)
            {
                int rowNumber = 2;
                Permissions[] permissions = (Permissions[])Enum.GetValues(typeof(Permissions));
                users.ForEach(u =>
                {
                    var row = sheet.CreateRow(rowNumber);

                    var rowCellStyle = workbook.CreateCellStyle();
                    rowCellStyle.BorderTop = BorderStyle.THIN;
                    row.RowStyle = rowCellStyle;

                    var cell = row.CreateCell(0);
                    var cellStyle = workbook.CreateCellStyle();
                    cell.SetCellValue(string.Format("{0}, {1} {2}", u.LastName, u.FirstName, u.CustomId.IsNotNullOrEmpty() ? " - " + u.CustomId : ""));

                    if (u.Permissions.IsNotNullOrEmpty())
                    {
                        foreach (var permission in permissions)
                        {
                            ulong id = (ulong)permission;
                            u.PermissionsArray = u.Permissions.ToObject<List<string>>();
                            if (u.PermissionsArray.Contains(id.ToString()))
                            {
                                rowNumber++;
                                var permissionRow = sheet.CreateRow(rowNumber);
                                permissionRow.CreateCell(0);
                                permissionRow.CreateCell(1).SetCellValue(permission.GetDescription());
                            }
                        }
                    }
                    rowNumber = rowNumber + 2;
                });
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of  Employee : {0}", users.Count));
            }

            workbook.FinishWritingToExcelSpreadsheet(2);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeePermissionsListing_{0}.xls", DateTime.Now.Ticks));
        }


        #endregion

        #region Statistical Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalVisitsByPayor()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            //List<VisitsByPayor> vBP = reportService.GetVisitsByPayor(Guid.Empty, 1, new List<int>() { 0 }, DateTime.Now.AddDays(-89), DateTime.Now, false);
            //if (vBP != null) {
            //    return PartialView("Statistical/VisitsByPayor",vBP);
            //}
            return PartialView("Statistical/VisitsByPayor");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalVisitsByPayorContent(Guid BranchCode, int StatusId, string InsuranceId, DateTime? StartDate, DateTime? EndDate, string SortParams)
        {
            List<int> insuranceList = new List<int>();
            if (InsuranceId.IsNotNullOrEmpty())
            {
                var insurancesSplitUp = InsuranceId.Split(',');
                if (insurancesSplitUp != null && insurancesSplitUp.Length > 0)
                {
                    insurancesSplitUp.ForEach(item => insuranceList.Add(item.ToInteger()));
                }

                if (SortParams.IsNotNullOrEmpty())
                {
                    var paramArray = SortParams.Split('-');
                    if (paramArray.Length >= 2)
                    {
                        ViewData["SortColumn"] = paramArray[0];
                        ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                    }
                }
            }

            if (StartDate.HasValue && EndDate.HasValue && StartDate.Value.IsValid() && EndDate.Value.IsValid())
            {
                return PartialView("Statistical/Content/VisitsByPayorContent", reportService.GetVisitsByPayor(BranchCode, StatusId, insuranceList, StartDate.Value, EndDate.Value));
            }
            else
            {
                return PartialView("Statistical/Content/VisitsByPayorContent", reportService.GetVisitsByPayor(BranchCode, StatusId, insuranceList, DateTime.Now.AddDays(-89), DateTime.Now));
            }
        }

        public ActionResult ExportStatisticalVisitsByPayor(Guid BranchCode, int StatusId, string InsuranceId, DateTime? StartDate, DateTime? EndDate, string SortParams)
        {
            List<VisitsByPayor> outVisitsByPayor = null;

            List<int> insuranceList = new List<int>();
            if (InsuranceId.IsNotNullOrEmpty())
            {
                var insurancesSplitUp = InsuranceId.Split(',');
                if (insurancesSplitUp != null && insurancesSplitUp.Length > 0)
                {
                    insurancesSplitUp.ForEach(item => insuranceList.Add(item.ToInteger()));
                }

                if (SortParams.IsNotNullOrEmpty())
                {
                    var paramArray = SortParams.Split('-');
                    if (paramArray.Length >= 2)
                    {
                        ViewData["SortColumn"] = paramArray[0];
                        ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                    }
                }
            }

            if (StartDate.HasValue && EndDate.HasValue && StartDate.Value.IsValid() && EndDate.Value.IsValid())
            {
                DateTime sDate = (DateTime)StartDate;
                DateTime eDate = (DateTime)EndDate;
                outVisitsByPayor = reportService.GetVisitsByPayor(BranchCode, StatusId, insuranceList, sDate, eDate);
            }
            else
            {
                outVisitsByPayor = reportService.GetVisitsByPayor(BranchCode, StatusId, insuranceList, DateTime.Now.AddDays(-89), DateTime.Now);
            }

            VisitsByPayor totalRow = new VisitsByPayor() { InsuranceName = "Total Row" };
            outVisitsByPayor.ForEach(visitsByPayor => 
            {
                totalRow.RN += visitsByPayor.RN;
                totalRow.LPNLVN += visitsByPayor.LPNLVN;
                totalRow.PT += visitsByPayor.PT;
                totalRow.PTA += visitsByPayor.PTA;
                totalRow.OT += visitsByPayor.OT;
                totalRow.COTA += visitsByPayor.COTA;
                totalRow.ST += visitsByPayor.ST;
                totalRow.MSW += visitsByPayor.MSW;
                totalRow.Dietician += visitsByPayor.Dietician;
                totalRow.HHA += visitsByPayor.HHA;
                totalRow.PCW += visitsByPayor.PCW;
                totalRow.HMK += visitsByPayor.HMK;
                totalRow.Total += visitsByPayor.Total;
            });

            outVisitsByPayor.Add(totalRow);

            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Visits By Payor";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("VisitsByPayor");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Visits By Payor");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Insurance");
            headerRow.CreateCell(1).SetCellValue("SN");
            headerRow.CreateCell(2).SetCellValue("LPN/LVN");
            headerRow.CreateCell(3).SetCellValue("PT");
            headerRow.CreateCell(4).SetCellValue("PTA");
            headerRow.CreateCell(5).SetCellValue("OT");
            headerRow.CreateCell(6).SetCellValue("COTA");
            headerRow.CreateCell(7).SetCellValue("ST");
            headerRow.CreateCell(8).SetCellValue("MSW");
            headerRow.CreateCell(9).SetCellValue("Dietician");
            headerRow.CreateCell(10).SetCellValue("HHA");
            headerRow.CreateCell(11).SetCellValue("PCW");
            headerRow.CreateCell(12).SetCellValue("HMK");
            headerRow.CreateCell(13).SetCellValue("Total");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (outVisitsByPayor != null && outVisitsByPayor.Count > 0)
            {
                int rowNumber = 2;
                int outVBPCount = outVisitsByPayor.Count;
                int iterator = 1;
                bool isFinalRow = default(bool);
                foreach (var visitsByPayor in outVisitsByPayor)
                {
                    if (iterator == outVBPCount) { rowNumber = rowNumber + 2; isFinalRow = true; }
                    var row = sheet.CreateRow(rowNumber);
                    var cell0 = row.CreateCell(0); cell0.SetCellType(CellType.STRING); cell0.SetCellValue(visitsByPayor.InsuranceName); 
                    var cell1 = row.CreateCell(1); cell1.SetCellType(CellType.NUMERIC); cell1.SetCellValue(visitsByPayor.RN);
                    var cell2 = row.CreateCell(2); cell2.SetCellType(CellType.NUMERIC); cell2.SetCellValue(visitsByPayor.LPNLVN);
                    var cell3 = row.CreateCell(3); cell3.SetCellType(CellType.NUMERIC); cell3.SetCellValue(visitsByPayor.PT);
                    var cell4 = row.CreateCell(4); cell4.SetCellType(CellType.NUMERIC); cell4.SetCellValue(visitsByPayor.PTA);
                    var cell5 = row.CreateCell(5); cell5.SetCellType(CellType.NUMERIC); cell5.SetCellValue(visitsByPayor.OT);
                    var cell6 = row.CreateCell(6); cell6.SetCellType(CellType.NUMERIC); cell6.SetCellValue(visitsByPayor.COTA);
                    var cell7 = row.CreateCell(7); cell7.SetCellType(CellType.NUMERIC); cell7.SetCellValue(visitsByPayor.ST);
                    var cell8 = row.CreateCell(8); cell8.SetCellType(CellType.NUMERIC); cell8.SetCellValue(visitsByPayor.MSW);
                    var cell9 = row.CreateCell(9); cell9.SetCellType(CellType.NUMERIC); cell9.SetCellValue(visitsByPayor.Dietician);
                    var cell10 = row.CreateCell(10); cell10.SetCellType(CellType.NUMERIC); cell10.SetCellValue(visitsByPayor.HHA);
                    var cell11 = row.CreateCell(11); cell11.SetCellType(CellType.NUMERIC); cell11.SetCellValue(visitsByPayor.PCW);
                    var cell12 = row.CreateCell(12); cell12.SetCellType(CellType.NUMERIC); cell12.SetCellValue(visitsByPayor.HMK);
                    var cell13 = row.CreateCell(13); cell13.SetCellType(CellType.NUMERIC); cell13.SetCellValue(visitsByPayor.Total);
                    iterator++;
                    rowNumber++;
                    
                }
            }
            workbook.FinishWritingToExcelSpreadsheet();
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("VisitsByPayor_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalPatientAdmissionsByInternalReferralSource()
        {
            ViewData["SortColumn"] = "UserName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/PatientAdmissionsByInternalReferralSource", new List<PatientAdmission>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalPatientAdmissionsByInternalReferralSourceContent(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            
                var patients = reportService.GetPatientAdmissionsByInternalSource(BranchId, StatusId , StartDate, EndDate);
                return PartialView("Statistical/Content/PatientAdmissionsByInternalReferralSource", patients);
        }

        public ActionResult ExportStatisticalPatientAdmissionsByInternalReferralSource(Guid BranchId, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Admissions By Internal Referral Source";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientAdmissionsByInternalReferralSource");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Admissions By Internal Referral Source");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("MRN");
            headerRow.CreateCell(3).SetCellValue("Phone");
            headerRow.CreateCell(4).SetCellValue("Admit");
            headerRow.CreateCell(5).SetCellValue("SOC");
            headerRow.CreateCell(6).SetCellValue("D/C");
            headerRow.CreateCell(7).SetCellValue("Insurance");
            headerRow.CreateCell(8).SetCellValue("Primary Physician");
            List<int> headerIndexes = new List<int>();
            List<int> groupTotalIndexes = new List<int>();
            var admissions = reportService.GetPatientAdmissionsByInternalSource(BranchId, StatusId,StartDate, EndDate);
            if (admissions != null && admissions.Count > 0)
            {
                int totalAdmitCount = 0;
                int totalNonAdmitCount = 0;
                int totalPendingCount = 0;
                headerIndexes.Add(1);
                int rowNumber = 2;
                var groupAdmissions = admissions.GroupBy(g => g.InternalReferral);
                //groupAdmissions
                foreach (var group in groupAdmissions)
                {
                    int admitCount = 0;
                    int nonAdmitCount = 0;
                    int pendingCount = 0;
                    var groupRow = sheet.CreateRow(rowNumber);
                    var element = group.ElementAtOrDefault(0);
                    groupRow.CreateCell(0).SetCellValue(element != null ? element.InternalReferralName : "");
                    for (int i = 1; i < 9; i++)
                    {
                        groupRow.CreateCell(i);
                    }
                    headerIndexes.Add(rowNumber);
                    rowNumber++;
                    foreach (var admission in group)
	                {
                        var row = sheet.CreateRow(rowNumber);
                        row.CreateCell(1).SetCellValue(admission.DisplayName);
                        row.CreateCell(2).SetCellValue(admission.PatientId);
                        row.CreateCell(3).SetCellValue(admission.PhoneHome);
                        row.CreateCell(4).SetCellValue(admission.Admit);

                        if (admission.StartOfCareDateFormatted.IsDate())
                        {
                            var createdDateCell = row.CreateCell(5);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(admission.StartOfCareDate);
                        }
                        else
                        {
                            row.CreateCell(5).SetCellValue(admission.StartOfCareDateFormatted);
                        }

                        if (admission.DischargedDateFormatted.IsDate())
                        {
                            var createdDateCell = row.CreateCell(6);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(admission.DischargedDate);
                        }
                        else
                        {
                            row.CreateCell(6).SetCellValue(admission.DischargedDateFormatted);
                        }

                        row.CreateCell(7).SetCellValue(admission.InsuranceName);
                        row.CreateCell(8).SetCellValue(admission.PhysicianName);

                        if (admission.Status == (int)PatientStatus.Active)
                        {
                            admitCount++;
                        }
                        else if (admission.Status == (int)PatientStatus.NonAdmission)
                        {
                            nonAdmitCount++;
                        }
                        else if (admission.Status == (int)PatientStatus.Pending)
                        {
                            pendingCount++;
                        }

                            rowNumber++;
                    }
                    totalAdmitCount += admitCount;
                    totalNonAdmitCount += nonAdmitCount;
                    totalPendingCount += pendingCount;

                    groupTotalIndexes.Add(rowNumber);
                    var groupTotalRow = sheet.CreateRow(rowNumber);
                    groupTotalRow.CreateCell(0).SetCellValue("Patients: " + group.Count());
                    groupTotalRow.CreateCell(1).SetCellValue("Admits: " + admitCount);
                    groupTotalRow.CreateCell(2).SetCellValue("Non-Admits: " + nonAdmitCount);
                    groupTotalRow.CreateCell(3).SetCellValue("Pending: " + pendingCount);
                    rowNumber++;
                    sheet.CreateRow(rowNumber);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Patients: {0}", admissions.Count));
                totalRow.CreateCell(1).SetCellValue(string.Format("Total Admits: {0}", totalAdmitCount));
                totalRow.CreateCell(2).SetCellValue(string.Format("Total Non-Admits: {0}", totalNonAdmitCount));
                totalRow.CreateCell(3).SetCellValue(string.Format("Total Pending: {0}", totalPendingCount));
            }
            workbook.FinishWritingToGroupedExcelSpreadsheet(9,headerIndexes, groupTotalIndexes);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientAdmissionsByInternalReferralSource_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalPatientVisitHistory()
        {
            ViewData["SortColumn"] = "UserName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/PatientVisitHistory", new List<ScheduleEvent>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalPatientVisitHistoryContent(Guid PatientId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Statistical/Content/PatientVisitHistory", PatientId.IsEmpty() ? new List<ScheduleEvent>() : patientService.GetScheduledEventsWithUsers(PatientId, StartDate, EndDate));
        }

        public ActionResult ExportStatisticalPatientVisitHistory(Guid PatientId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Visit History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientVisitHistory");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Visit History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var patient = patientRepository.GetPatientOnly(PatientId, Current.AgencyId);
            titleRow.CreateCell(4).SetCellValue(string.Format("Patient Name:{0}", patient.DisplayName));
            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Employee");

            var scheduleEvents = patientService.GetScheduledEventsWithUsers(PatientId, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(1).SetCellValue(evnt.StatusName);

                    if (evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.EventDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(2).SetCellValue(string.Empty);
                    }
                    if (evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.VisitDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    row.CreateCell(4).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Visit History: {0}", scheduleEvents.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientVisitHistory_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalEmployeeVisitHistory()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/EmployeeVisitHistory", new List<UserVisit>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeVisitHistoryContent(Guid UserId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }

            return PartialView("Statistical/Content/EmployeeVisitHistory", UserId.IsEmpty() ? new List<UserVisit>() : reportService.GetEmployeeVisitList(UserId, StartDate, EndDate));
        }

        public ActionResult ExportStatisticalEmployeeVisitHistory(Guid UserId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Visit History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeVisitHistory");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Visit History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Patient");

            var scheduleEvents = reportService.GetEmployeeVisitList(UserId, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.TaskName);
                    row.CreateCell(1).SetCellValue(evnt.StatusName);

                    if (evnt.ScheduleDate.IsNotNullOrEmpty() && evnt.ScheduleDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.ScheduleDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(2).SetCellValue(string.Empty);
                    }
                    if (evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(evnt.VisitDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }

                    row.CreateCell(4).SetCellValue(evnt.PatientName);

                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Visit History: {0}", scheduleEvents.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalMonthlyAdmissionListing()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/MonthlyAdmission", patientRepository.GetPatientByAdmissionMonthYear(Current.AgencyId, Guid.Empty, 1, DateTime.Now.Month, DateTime.Now.Year));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalMonthlyAdmissionContent(Guid BranchCode, int StatusId, int Month, int Year, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Statistical/Content/MonthlyAdmission", patientRepository.GetPatientByAdmissionMonthYear(Current.AgencyId, BranchCode, StatusId, Month, Year));
        }

        public ActionResult ExportStatisticalMonthlyAdmission(Guid BranchCode, int StatusId, int Month, int Year)
        {
            var patients = patientRepository.GetPatientByAdmissionMonthYear(Current.AgencyId, BranchCode, StatusId, Month, Year);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Monthly Admission List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientMonthlyAdmission");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Monthly Admission List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month/Year : {0}/{1}", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty, Year));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Status");
            headerRow.CreateCell(3).SetCellValue("Admission Source");
            headerRow.CreateCell(4).SetCellValue("Internal Referral");
            headerRow.CreateCell(5).SetCellValue("Referrer Physician");
            headerRow.CreateCell(6).SetCellValue("Referral Date");
            headerRow.CreateCell(7).SetCellValue("Admission Date");
            headerRow.CreateCell(8).SetCellValue("Other Referral Source");

            if (patients != null && patients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in patients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    
                    //MRN
                    row.CreateCell(0).SetCellValue(birthDay.PatientId);
                    
                    //Patient
                    row.CreateCell(1).SetCellValue(birthDay.PatientDisplayName);

                    //Status
                    row.CreateCell(2).SetCellValue(birthDay.PatientStatusName);

                    //Admission Source
                    row.CreateCell(3).SetCellValue(birthDay.AdmissionSourceName);
                    
                    //Internal Referral
                    row.CreateCell(4).SetCellValue(birthDay.InternalReferral);

                    //Referrer Physician
                    row.CreateCell(5).SetCellValue(birthDay.ReferrerPhysician);

                    //Referral Date
                    if (birthDay.ReferralDate.IsNotNullOrEmpty() && birthDay.ReferralDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(6);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(birthDay.ReferralDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(6).SetCellValue(string.Empty);
                    }

                    //Admission Date
                    if (birthDay.PatientSoC.IsNotNullOrEmpty() && birthDay.PatientSoC.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(7);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(birthDay.PatientSoC.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(7).SetCellValue(string.Empty);
                    }

                    //Other Referral Source
                    row.CreateCell(8).SetCellValue(birthDay.OtherReferralSource);
                    
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Monthly Admission List: {0}", patients.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(9);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientMonthlyAdmission_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalAnnualAdmissionListing()
        {
            ViewData["SortColumn"] = "PatientFirstName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/AnnualAdmission", patientRepository.GetPatientByAdmissionYear(Current.AgencyId, Guid.Empty, 1, DateTime.Now.Year));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalAnnualAdmissionContent(Guid BranchCode, int StatusId, int Year, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Statistical/Content/AnnualAdmission", patientRepository.GetPatientByAdmissionYear(Current.AgencyId, BranchCode, StatusId, Year));
        }

        public ActionResult ExportStatisticalAnnualAdmission(Guid BranchCode, int StatusId, int Year)
        {
            var patients = patientRepository.GetPatientByAdmissionYear(Current.AgencyId, BranchCode, StatusId, Year);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Annual Admission List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientAnnualAdmission");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Annual Admission List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Year : {0}", Year));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Admission Source");
            headerRow.CreateCell(3).SetCellValue("Admission Date");

            if (patients != null && patients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in patients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.PatientId);
                    row.CreateCell(1).SetCellValue(birthDay.PatientDisplayName);
                    row.CreateCell(2).SetCellValue(birthDay.AdmissionSourceName);
                    if (birthDay.PatientSoC.IsNotNullOrEmpty() && birthDay.PatientSoC.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(birthDay.PatientSoC.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Annual Admission List: {0}", patients.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientAnnualAdmission_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalUnduplicatedCensusReportListing()
        {
            ViewData["SortColumn"] = "PatientFirstName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/UnduplicatedCensusReport", patientRepository.GetPatientByAdmissionUnduplicatedByDateRange(Current.AgencyId, Guid.Empty, 1, DateTime.Now.AddDays(-59), DateTime.Now.Date));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalUnduplicatedCensusReportContent(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Statistical/Content/UnduplicatedCensusReport", patientRepository.GetPatientByAdmissionUnduplicatedByDateRange(Current.AgencyId, BranchCode, StatusId, StartDate, EndDate));
        }

        public ActionResult ExportStatisticalUnduplicatedCensusReport(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            var patients = patientRepository.GetPatientByAdmissionUnduplicatedByDateRange(Current.AgencyId, BranchCode, StatusId, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Unduplicated Census List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UnduplicatedCensusReport");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Unduplicated Census List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1} ", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");
            
            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Admission Source");
            headerRow.CreateCell(3).SetCellValue("Admission Date");
            headerRow.CreateCell(4).SetCellValue("Discharge Date");
            headerRow.CreateCell(5).SetCellValue("Status");
            if (patients != null && patients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in patients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.PatientId);
                    row.CreateCell(1).SetCellValue(birthDay.PatientDisplayName);
                    row.CreateCell(2).SetCellValue(birthDay.AdmissionSourceName);
                    if (birthDay.PatientSoC.IsNotNullOrEmpty() && birthDay.PatientSoC.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(birthDay.PatientSoC.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    if (birthDay.PatientDischargeDate.IsNotNullOrEmpty() && birthDay.PatientDischargeDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(birthDay.PatientDischargeDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    row.CreateCell(5).SetCellValue(birthDay.PatientStatusName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Unduplicated Census List: {0}", patients.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UnduplicatedCensusReport_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalUnduplicatedServiceCensusReportListing()
        {
            ViewData["SortColumn"] = "PatientFirstName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/UnduplicatedServiceCensusReport", patientRepository.GetPatientByAdmissionUnduplicatedServiceByDateRange(Current.AgencyId, Guid.Empty, 1, DateTime.Now.AddDays(-59), DateTime.Now.Date));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalUnduplicatedServiceCensusReportContent(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Statistical/Content/UnduplicatedServiceCensusReport", patientRepository.GetPatientByAdmissionUnduplicatedServiceByDateRange(Current.AgencyId, BranchCode, StatusId, StartDate, EndDate));
        }

        public ActionResult ExportStatisticalUnduplicatedServiceCensusReport(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            var patients = patientRepository.GetPatientByAdmissionUnduplicatedServiceByDateRange(Current.AgencyId, BranchCode, StatusId, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Unduplicated Census Report By Date Range";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UnduplicatedCensusByDateRangeReport");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Unduplicated Census By Date Range List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1} ", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));

            ICellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Admission Source");
            headerRow.CreateCell(3).SetCellValue("Admission Date");
            headerRow.CreateCell(4).SetCellValue("Discharge Date");
            headerRow.CreateCell(5).SetCellValue("Current Status");
            if (patients != null && patients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in patients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.PatientId);
                    row.CreateCell(1).SetCellValue(birthDay.PatientDisplayName);
                    row.CreateCell(2).SetCellValue(birthDay.AdmissionSourceName);
                    if (birthDay.PatientSoC.IsNotNullOrEmpty() && birthDay.PatientSoC.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(birthDay.PatientSoC.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }
                    if (birthDay.PatientDischargeDate.IsNotNullOrEmpty() && birthDay.PatientDischargeDate.IsValidDateAndNotMin())
                    {
                        var createdDateCell = row.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(birthDay.PatientDischargeDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }
                    row.CreateCell(5).SetCellValue(birthDay.PatientStatusName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Unduplicated Census By Date Range List: {0}", patients.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UnduplicatedCensusReportByDateRange_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalCensusByPrimaryInsurance()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/CensusByPrimaryInsurance", new List<PatientRoster>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CensusByPrimaryInsuranceContent(Guid BranchCode, int InsuranceId, int StatusId)
        {
            return PartialView("Statistical/Content/CensusByPrimaryInsurance", InsuranceId <= 0 ? new List<PatientRoster>() : reportService.GetPatientRosterByInsurance(BranchCode, InsuranceId, StatusId));
        }

        public ActionResult ExportCensusByPrimaryInsurance(Guid BranchCode, int InsuranceId, int StatusId)
        {
            var patientRosters = InsuranceId > 0 ? reportService.GetPatientRosterByInsurance(BranchCode, InsuranceId, StatusId) : new List<PatientRoster>();
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Census By Primary Insurance";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("CensusByPrimaryInsurance");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Census By Primary Insurance");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));
            var insuranceModel = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insuranceModel != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance : {0}", insuranceModel.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Address");
            headerRow.CreateCell(3).SetCellValue("City");
            headerRow.CreateCell(4).SetCellValue("State");
            headerRow.CreateCell(5).SetCellValue("Zip Code");
            headerRow.CreateCell(6).SetCellValue("Home Phone");
            headerRow.CreateCell(7).SetCellValue("Gender");
            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientId);
                    row.CreateCell(1).SetCellValue(patient.PatientDisplayName);
                    row.CreateCell(2).SetCellValue(patient.PatientAddressLine1);
                    row.CreateCell(3).SetCellValue(patient.PatientAddressCity);
                    row.CreateCell(4).SetCellValue(patient.PatientAddressStateCode);
                    row.CreateCell(5).SetCellValue(patient.PatientAddressZipCode);
                    row.CreateCell(6).SetCellValue(patient.PatientPhone);
                    row.CreateCell(7).SetCellValue(patient.PatientGender);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Census By Primary Insurance: {0}", patientRosters.Count));
            }
            workbook.FinishWritingToExcelSpreadsheet(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("CensusByPrimaryInsurance_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSEpisodeInformationReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PPSEpisodeInformation");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSVisitInformationReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PPSVisitInformation");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSPaymentInformationReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PPSPaymentInformation");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSChargeInformationReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PPSChargeInformation");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargesByReasonReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/DischargesByReason");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitsByPrimaryPaymentSourceReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/VisitsByPrimaryPaymentSource");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitsByStaffTypeReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/VisitsByStaffType");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AdmissionsByReferralSourceReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/AdmissionsByReferralSource");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientsVisitsByPrincipalDiagnosisReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PatientsVisitsByPrincipalDiagnosis");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientsAndVisitsByAgeReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PatientsAndVisitsByAge");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CostReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/CostReport");
        }

        #endregion

        #endregion
    }
}
