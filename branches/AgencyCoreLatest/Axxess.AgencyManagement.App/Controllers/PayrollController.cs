﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Domain;
    using Services;
    using Security;

    using ViewData;
    using iTextExtension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;

    [Compress]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PayrollController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IPayrollService payrollService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;

        public PayrollController(IAgencyManagementDataProvider agencyManagementDataProvider, IPayrollService payrollService, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(payrollService, "payrollService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.payrollService = payrollService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region PayrollController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Search()
        {
            return View(DateTime.Today.AddDays(-15));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Search(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            return View("Results", new PayrollSummaryViewData { VisitSummary = payrollService.GetSummary(payrollStartDate, payrollEndDate, payrollStatus), EndDate = payrollEndDate, StartDate = payrollStartDate , PayrollStatus=payrollStatus});
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SummaryPdf(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            PayrollSummaryPdf doc = new PayrollSummaryPdf(payrollService.GetSummary(payrollStartDate, payrollEndDate, payrollStatus).OrderBy(d => d.LastName).ToList(), agencyRepository.GetWithBranches(Current.AgencyId), payrollStartDate, payrollEndDate);
            var Stream = doc.GetStream();
            Stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PayrollSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(Stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SummaryDetailPdf(Guid userId, DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            var detail = new PayrollDetail();
            if (!userId.IsEmpty())
            {
                detail.Id = userId;
                detail.Name = UserEngine.GetName(userId, Current.AgencyId);
                detail.Visits = payrollService.GetVisits(userId, payrollStartDate, payrollEndDate, payrollStatus, true).OrderBy(d => d.PatientName).ToList();
                detail.PayrollStatus = payrollStatus;
            }
            var doc = new PayrollSummaryDetailsPdf(detail, agencyRepository.Get(Current.AgencyId), payrollStartDate, payrollEndDate);
            var Stream = doc.GetStream();
            Stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PayrollSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(Stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SummaryDetailsPdf(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            var doc = new PayrollSummaryDetailsPdf(payrollService.GetDetails(payrollStartDate, payrollEndDate, payrollStatus, true), agencyRepository.Get(Current.AgencyId), payrollStartDate, payrollEndDate);
            var Stream = doc.GetStream();
            Stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PayrollSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(Stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Detail(Guid userId, DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            var detail = new PayrollDetail();
            detail.StartDate = payrollStartDate;
            detail.EndDate = payrollEndDate;
            detail.PayrollStatus = payrollStatus;
            if (!userId.IsEmpty())
            {
                detail.Id = userId;
                detail.Name =  UserEngine.GetName(userId, Current.AgencyId);
                detail.Visits = payrollService.GetVisits(userId, payrollStartDate, payrollEndDate, payrollStatus, false);
            }
            return PartialView(detail);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DetailSummary(Guid userId, DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            var detail = payrollService.GetVisits(userId, payrollStartDate, payrollEndDate, payrollStatus, false);
            return PartialView(detail);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Details(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            return PartialView(new PayrollDetailsViewData { StartDate = payrollStartDate, EndDate = payrollEndDate, Details = payrollService.GetDetails(payrollStartDate, payrollEndDate, payrollStatus, false), PayrollStatus = payrollStatus });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PayVisit(List<string> visitSelected)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The visits selected could not be processed. Please try again." };

            if (payrollService.MarkAsPaid(visitSelected))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The visits have been marked as paid.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnpayVisit(List<string> visitSelected)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The visits selected could not be unpaid. Please try again." };

            if (payrollService.MarkAsUnpaid(visitSelected))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The visits have been marked as unpaid.";
            }

            return Json(viewData);
        }

        #endregion
    }
}
