﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.App.Security;

    using Axxess.Core.Infrastructure;

    [Compress]
    public class ErrorController : Controller
    {
        public ErrorController() { }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get), ValidateInput(false)]
        public ActionResult ApplicationError(string error)
        {
            var viewData = new ErrorPageViewData { message = error };
            return View("Application", viewData);
        }

        public ActionResult FileNotFound(string error)
        {
            var viewData = new ErrorPageViewData { message = error };
            return View(viewData);
        }

        public ActionResult NotAuthorized(string error)
        {
            var viewData = new ErrorPageViewData { message = error };
            return View(viewData);
        }

        public ActionResult Forbidden(string error)
        {
            return View();
        }
    }
}
