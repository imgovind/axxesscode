﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System.Xml;
    using System.Web.Mvc;
    using System.ServiceModel.Syndication;

    using Enums;
    using Domain;
    using Services;
    using Security;
    using ViewData;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    [Compress]
    
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HomeController : BaseController
    {
        #region Private Members/Constructor

        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IErrorRepository errorRepository;

        public HomeController(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, IUserService userService)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");

            this.userService = userService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.errorRepository = membershipDataProvider.ErrorRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
        }

        #endregion

        #region HomeController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Logs()
        {
            return View("~/Views/Shared/Logs.aspx", errorRepository.GetSome());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Dashboard()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Updates()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Agencies()
        {
            return PartialView(this.userRepository.GetAgencies(Current.LoginId));
        }

        #endregion
    }
}
