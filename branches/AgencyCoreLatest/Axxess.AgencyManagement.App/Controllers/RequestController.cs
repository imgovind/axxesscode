﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Net;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Xml;
    using System.Web.Mvc;
    using System.Xml.XPath;
    using System.Threading;
    using System.Collections.Generic;

    using Enums;
    using Domain;
    using Exports;
    using Services;
    using Security;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;

    [Compress]
    [AxxessAuthorize]
    
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class RequestController : BaseController
    {
        #region Private Members/Constructor

        private readonly IReportService reportService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;

        public RequestController(IAgencyManagementDataProvider agencyManagementDataProvider, IReportService reportService)
        {
            Check.Argument.IsNotNull(reportService, "reportService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.reportService = reportService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region RequestController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EpisodeDownload(Guid patientId, Guid episodeId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Patient Chart\" Download could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Patient Chart",
                Status = "Running",
                Format = "Archive",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddPatientChartDownload(report.AgencyId, episodeId, patientId, report.Id)).Start();

                    viewData.errorMessage = "You will be notified when the \"Patient Chart\" Download is completed.";
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ClaimFormDownload(List<Guid> ManagedClaimSelected, Guid BranchId, int PrimaryInsurance)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Claim Forms \" Download could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Claim Forms",
                Status = "Running",
                Format = "Archive",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddClaimFormDownload(report.AgencyId, BranchId, PrimaryInsurance, ManagedClaimSelected, report.Id)).Start();

                    viewData.errorMessage = "You will be notified when the \"Claim Forms\" Download is completed.";
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);
        }



        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult TherapyManagementReport(Guid BranchId, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Therapy Management\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Therapy Management Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };
            string agencyName = Current.AgencyName + " - " + agencyRepository.FindLocation(Current.AgencyId, BranchId).Name;
            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddTherapyManagementReport(BranchId, startDate, endDate, report.Id, report.AgencyId, agencyName)).Start();

                    viewData.errorMessage = "You will be notified when the \"Therapy Management\" Report is completed.";
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientListReport(Guid BranchId, int StatusId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Patient List\" Report could not be requested." };
            if (StatusId == null) { StatusId = 1; }
            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Patient List Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };
            string agencyName = BranchId.IsNotEmpty() ? Current.AgencyName + " - " + agencyRepository.FindLocation(Current.AgencyId, BranchId).Name : Current.AgencyName;
            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddPatientListReport(BranchId, StatusId, report.Id, report.AgencyId, agencyName)).Start();
                    viewData.errorMessage = "You will be notified when the \"Patient List\" Report is completed.";
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult HHRGReport(Guid BranchId, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"HHRG\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "HHRG Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };
            string agencyName = Current.AgencyName + " - " + agencyRepository.FindLocation(Current.AgencyId, BranchId).Name;
            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddHHRGReport(BranchId, startDate, endDate, report.Id, report.AgencyId, agencyName)).Start();

                    viewData.errorMessage = "You will be notified when the \"HHRG\" Report is completed.";
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EmployeeList(Guid BranchId, int StatusId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Employee List\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Employee List Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            string agencyName = BranchId.IsNotEmpty() ? Current.AgencyName + " - " + agencyRepository.FindLocation(Current.AgencyId, BranchId).Name : Current.AgencyName;

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddEmployeeListReport(BranchId, StatusId, report.Id, report.AgencyId, agencyName)).Start();

                    viewData.errorMessage = "You will be notified when the \"Employee List\" Report is completed.";
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UnbilledVisitsForManagedClaimsReport(Guid BranchId, int InsuranceId, int Status, DateTime startDate, DateTime endDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Unbilled Visits for Managed Claims Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Unbilled Visits for Managed Claims Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };
            string agencyName = Current.AgencyName + " - " + agencyRepository.FindLocation(Current.AgencyId, BranchId).Name;
            if (report.IsValid)
            {
                if (InsuranceId != 0)
                {
                    if (agencyRepository.AddReport(report))
                    {
                        new Thread(() => ReportManager.AddUnbilledVisitsReport(BranchId, InsuranceId, Status, startDate, endDate, report.Id, report.AgencyId, agencyName)).Start();

                        viewData.errorMessage = "You will be notified when the Unbilled Visits for Managed Claims Report is completed.";
                        viewData.isSuccessful = true;
                    }
                }
                else
                {
                    viewData.errorMessage = "Please select an Insurance.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientsAndVisitsByAgeReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Patients & Visits By Age\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Patients & Visits By Age Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddPatientsAndVisitsByAgeReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Patients & Visits By Age\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DischargesByReasonReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Discharges By Reason\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Discharges By Reason Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddDischargesByReasonReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Discharges By Reason\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult VisitsByPrimaryPaymentSourceReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Visits By Primary Payment Source\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Visits By Primary Payment Source Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddVisitsByPrimaryPaymentSourceReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Visits By Primary Payment Source\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult VisitsByStaffTypeReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Visits By Staff Type\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Visits By Staff Type Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddVisitsByStaffTypeReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Visits By Staff Type\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AdmissionsByReferralSourceReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Admissions By Referral Source\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Admissions By Referral Source Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddAdmissionsByReferralSourceReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Admissions By Referral Source\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientsVisitsByPrincipalDiagnosisReport(Guid BranchId, int Year)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The \"Patients & Visits By Principal Diagnosis\" Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Patients & Visits By Principal Diagnosis Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddPatientsVisitsByPrincipalDiagnosisReport(BranchId, Year, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the \"Patients & Visits By Principal Diagnosis\" Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CostReport(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Cost Report could not be requested." };

            var report = new Report
            {
                UserId = Current.UserId,
                AgencyId = Current.AgencyId,
                Type = "Cost Report",
                Status = "Running",
                Format = "Excel",
                Created = DateTime.Now
            };

            if (report.IsValid)
            {
                if (agencyRepository.AddReport(report))
                {
                    new Thread(() => ReportManager.AddCostReport(BranchId, StartDate, EndDate, report.Id, report.AgencyId)).Start();

                    viewData.isSuccessful = true;
                    viewData.errorMessage = "You will be notified when the Cost Report is completed.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = report.ValidationMessage;
            }
            return Json(viewData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteReport(Guid completedReportId)
        {
            return Json(ReportManager.RemoveReport(Current.AgencyId, completedReportId));

        }

        #endregion
    }
}
