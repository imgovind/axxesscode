﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;
    public class MessageModule : Module
    {
        public override string Name
        {
            get { return "Message"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "NewMessageFolder",
                "MessageFolder/New",
                new { controller = this.Name, action = "NewMessageFolder" });

            routes.MapRoute(
                "AddMessageFolder",
                "MessageFolder/Add",
                new { controller = this.Name, action = "AddMessageFolder" });

            routes.MapRoute(
                "FolderList",
                "Message/FolderList",
                new { Controller = this.Name, Action = "FolderList" });

            routes.MapRoute(
                "MoveToFolder",
                "Message/MoveToFolder",
                new { Controller = this.Name, Action = "MoveToFolder" });

            routes.MapRoute(
                "DeleteFolder",
                "Message/DeleteFolder",
                new { Controller = this.Name, Action = "DeleteFolder" });
            
            routes.MapRoute(
                "Delete",
                "Message/Delete",
                new { Controller = this.Name, Action = "Delete" });

        }
    }
}
