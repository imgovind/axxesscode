﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Web;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Api;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    public class MembershipService : IMembershipService
    {
        #region Private Members

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ISupportRepository supportRepository;

        #endregion

        #region Constructor

        public MembershipService(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementProvider.UserRepository;
            this.supportRepository = membershipDataProvider.SupportRepository;
            this.agencyRepository = agencyManagementProvider.AgencyRepository;
        }

        #endregion

        #region IMembershipService Members

        public AxxessPrincipal Get(string userAgencyIdentifier)
        {
            AxxessPrincipal principal = null;
            if (userAgencyIdentifier.IsNotNullOrEmpty())
            {
                principal = Cacher.Get<AxxessPrincipal>(userAgencyIdentifier);
                if (principal == null)
                {
                    Guid link = Guid.Empty;
                    string[] userAgencyLinkArray = userAgencyIdentifier.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                    if (userAgencyLinkArray != null && userAgencyLinkArray.Length > 1)
                    {
                        userAgencyIdentifier = userAgencyLinkArray[0];
                        link = userAgencyLinkArray[1].ToGuid();
                    }
                    var userAgencyArray = userAgencyIdentifier.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                    if (userAgencyArray != null && userAgencyArray.Length > 1)
                    {
                        principal = Get(userAgencyArray[0].ToGuid(), userAgencyArray[1].ToGuid(), link, false, false);
                    }
                }
            }
            return principal;
        }

        public AxxessPrincipal Get(Guid userId, Guid agencyId)
        {
            return Get(userId, agencyId, Guid.Empty, false, false);
        }

        public AxxessPrincipal Get(Guid userId, Guid agencyId, bool isMobileDevice)
        {
            return Get(userId, agencyId, Guid.Empty, false, isMobileDevice);
        }

        public AxxessPrincipal Get(Guid userId, Guid agencyId, Guid linkId, bool hasMultipleAccounts, bool isMobileDevice)
        {
            AxxessPrincipal principal = null;
            if (userId.IsNotEmpty() && agencyId.IsNotEmpty())
            {
                var identityName = string.Format("{0}_{1}", userId, agencyId);
                if (linkId.IsNotEmpty())
                {
                    identityName = string.Format("{0}:{1}", identityName, linkId);
                }
                principal = Cacher.Get<AxxessPrincipal>(identityName);
                if (principal == null)
                {
                    var userAgencyInfo = userRepository.GetUserAgencyInformation(userId, agencyId);
                    if (userAgencyInfo != null)
                    {
                        var login = loginRepository.Find(userAgencyInfo.LoginId);
                        if (login != null)
                        {
                            Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                            if (role == Roles.ApplicationUser)
                            {
                                AxxessIdentity identity = new AxxessIdentity(login.Id, identityName);
                                identity.Session =
                                    new UserSession
                                    {
                                        LoginId = login.Id,
                                        UserId = userAgencyInfo.UserId,
                                        LoginDay = DateTime.Today.Day,
                                        DisplayName = login.DisplayName,
                                        EmailAddress = login.EmailAddress,
                                        SessionId = SessionStore.SessionId,
                                        AgencyId = userAgencyInfo.AgencyId,
                                        Payor = userAgencyInfo.AgencyPayor,
                                        ClusterId = userAgencyInfo.ClusterId,
                                        IsPrimary = userAgencyInfo.IsPrimary,
                                        AgencyName = userAgencyInfo.AgencyName,
                                        HasMultipleAccounts = hasMultipleAccounts,
                                        AccountExpirationDate = userAgencyInfo.AccountExpireDate,
                                        OasisVendorExist = userAgencyInfo.OasisAuditVendorApiKey.IsNotNullOrEmpty(),
                                        IsAgencyFrozen = userAgencyInfo.IsFrozen && userAgencyInfo.FrozenDate.IsInPast(),
                                        AgencyRoles = userAgencyInfo.Roles.IsNotNullOrEmpty() ? userAgencyInfo.Roles : string.Empty,
                                        Address = userAgencyInfo.AddressFull.IsNotNullOrEmpty() ? userAgencyInfo.AddressFull : string.Empty,
                                        FullName = isMobileDevice ? string.Format("{0} (Mobile)", userAgencyInfo.DisplayName) : userAgencyInfo.DisplayName,
                                        EarliestLoginTime = userAgencyInfo.EarliestLoginTime.IsNotNullOrEmpty() ? userAgencyInfo.EarliestLoginTime : string.Empty,
                                        AutomaticLogoutTime = userAgencyInfo.AutomaticLogoutTime.IsNotNullOrEmpty() ? userAgencyInfo.AutomaticLogoutTime : string.Empty,

                                    };
                                var link = supportRepository.GetImpersonationLink(linkId);
                                if (link != null)
                                {
                                    identity.IsImpersonated = true;
                                    identity.Session.ImpersonatorName = link.RepName;
                                }
                                principal = new AxxessPrincipal(identity, role, userAgencyInfo.PermissionsArray);
                                Cacher.Set<AxxessPrincipal>(identityName, principal);
                            }
                        }
                    }
                }
            }
            return principal;
        }

        public bool Login(Account account)
        {
            var result = false;

            var login = loginRepository.Find(account.UserName);
            if (login != null)
            {
                if (login.IsActive)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(account.Password, login.PasswordHash, login.PasswordSalt))
                    {
                        account.LoginId = login.Id;
                        account.EmailAddress = login.EmailAddress;

                        Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                        if (role == Roles.ApplicationUser)
                        {
                            var accounts = userRepository.GetUsersByLoginId(login.Id);
                            if (accounts.Count == 1)
                            {
                                var user = accounts[0];
                                account.UserId = user.Id;
                                account.AgencyId = user.AgencyId;

                                var principal = Get(user.Id, user.AgencyId);
                                if (principal != null)
                                {
                                    Thread.CurrentPrincipal = principal;
                                    HttpContext.Current.User = principal;

                                    login.LastLoginDate = DateTime.Now;
                                    if (loginRepository.Update(login))
                                    {
                                        result = true;
                                    }
                                }
                            }
                            else if (accounts.Count > 1)
                            {
                                account.HasMultiple = true;
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public LoginAttemptType Validate(Account account)
        {
            var loginAttempt = LoginAttemptType.Failed;

            var login = loginRepository.Find(account.UserName);
            if (login != null)
            {
                if (login.IsLocked)
                {
                    loginAttempt = LoginAttemptType.Locked;
                }
                else
                {
                    if (login.IsActive)
                    {
                        var canContinue = true;
                        var principal = Get(account.UserId, account.AgencyId);
                        if (AppSettings.IsSingleUserMode)
                        {
                            canContinue = principal.IsSingleSession();
                        }

                        if (canContinue)
                        {
                            var userAgencyInformation = userRepository.GetUserAgencyInformation(account.UserId, account.AgencyId);
                            if (userAgencyInformation != null)
                            {
                                if (userAgencyInformation.Status == (int)UserStatus.Active)
                                {
                                    if (!userAgencyInformation.HasTrialAccountExpired())
                                    {
                                        if (userAgencyInformation.AllowWeekendAccess())
                                        {
                                            if (!userAgencyInformation.IsReadOnly())
                                            {
                                                if (!userAgencyInformation.IsSuspended)
                                                {
                                                    if (!userAgencyInformation.HasTrialPeriodExpired())
                                                    {
                                                        loginAttempt = LoginAttemptType.Success;
                                                    }
                                                    else
                                                    {
                                                        loginAttempt = LoginAttemptType.TrialPeriodOver;
                                                    }
                                                }
                                                else
                                                {
                                                    loginAttempt = LoginAttemptType.AccountSuspended;
                                                }
                                            }
                                            else
                                            {
                                                if (!userAgencyInformation.IsReadOnlyPeriodExpired())
                                                {
                                                    if (userAgencyInformation.IsPrimary)
                                                    {
                                                        loginAttempt = LoginAttemptType.Success;
                                                    }
                                                    else
                                                    {
                                                        loginAttempt = LoginAttemptType.AgencyFrozen;
                                                    }
                                                }
                                                else
                                                {
                                                    loginAttempt = LoginAttemptType.AgencyReadOnlyPeriodExpired;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            loginAttempt = LoginAttemptType.WeekendAccessRestricted;
                                        }
                                    }
                                    else
                                    {
                                        loginAttempt = LoginAttemptType.TrialAccountExpired;
                                    }
                                }
                                else
                                {
                                    loginAttempt = LoginAttemptType.Deactivated;
                                }
                            }
                        }
                        else
                        {
                           loginAttempt = LoginAttemptType.AccountInUse;
                        }
                    }
                    else
                    {
                        loginAttempt = LoginAttemptType.Deactivated;
                    }
                }
            }

            return loginAttempt;
        }

        public ResetAttemptType Validate(string userName)
        {
            var resetAttempt = ResetAttemptType.Failed;

            var login = loginRepository.Find(userName);
            if (login != null)
            {
                if (login.IsLocked)
                {
                    resetAttempt = ResetAttemptType.Locked;
                }
                else
                {
                    if (login.IsActive)
                    {
                        var accounts = userRepository.GetUsersByLoginId(login.Id);
                        var activeAccount = accounts.Where(a => a.Status == (int)UserStatus.Active).FirstOrDefault();
                        if (activeAccount != null)
                        {
                            resetAttempt = ResetAttemptType.Success;
                        }
                        else
                        {
                            resetAttempt = ResetAttemptType.Deactivated;
                        }
                    }
                    else
                    {
                        resetAttempt = ResetAttemptType.Deactivated;
                    }
                }
            }

            return resetAttempt;
        }

        public bool Impersonate(Guid linkId)
        {
            var result = false;

            var link = supportRepository.GetImpersonationLink(linkId);
            if (link != null && link.IsUsed == false && DateTime.Now < link.Created.AddMinutes(5))
            {
                var principal = Get(link.UserId, link.AgencyId, linkId, false, false);
                if (principal != null)
                {
                    Thread.CurrentPrincipal = principal;
                    HttpContext.Current.User = principal;

                    link.IsUsed = true;
                    if (supportRepository.UpdateImpersonationLink(link))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool SingleSignOn(Guid tokenId)
        {
            var result = false;

            var token = supportRepository.GetToken(tokenId);
            if (token != null && token.IsUsed == false && DateTime.Now < token.Created.AddMinutes(5))
            {
                Cacher.Remove(string.Format("{0}_{1}", token.UserId, token.AgencyId));
                var principal = Get(token.UserId, token.AgencyId);
                if (principal != null)
                {
                    Thread.CurrentPrincipal = principal;
                    HttpContext.Current.User = principal;

                    token.IsUsed = true;
                    if (supportRepository.UpdateToken(token))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool ResetPassword(string userName, string baseUrl)
        {
            var login = loginRepository.Find(userName);
            if (login != null && login.IsAxxessAdmin == false && login.IsAxxessSupport == false)
            {
                var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}&type=password", login.Id)));
                var bodyText = MessageBuilder.PrepareTextFrom(
                    "PasswordResetInstructions",
                    "firstname", login.DisplayName, 
                    "baseurl", baseUrl,
                    "encryptedQueryString", encryptedQueryString);
                Notify.User(CoreSettings.NoReplyEmail, userName, "Reset Password - Axxess Home Health Management Software", bodyText);
                return true;
            }
            return false;
        }

        public bool UpdatePassword(Account account)
        {
            var login = loginRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string newPasswordSalt = string.Empty;
                string newPasswordHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Password, out newPasswordHash, out newPasswordSalt);
                login.PasswordSalt = newPasswordSalt;
                login.PasswordHash = newPasswordHash;
                if (loginRepository.Update(login))
                {
                    return true;
                }
            }

            return false;
        }

        public bool ResetSignature(Guid loginId)
        {
            var login = loginRepository.Find(loginId);

            if (login != null)
            {
                var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}&type=signature", login.Id)));
                var bodyText = MessageBuilder.PrepareTextFrom(
                    "SignatureResetInstructions",
                    "firstname", login.DisplayName,
                    "encryptedQueryString", encryptedQueryString);
                Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, "Reset Signature - Axxess Home Health Management Software", bodyText);
                return true;
            }

            return false;
        }

        public bool UpdateSignature(Account account)
        {
            var login = loginRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string newSignatureSalt = string.Empty;
                string newSignatureHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Signature, out newSignatureHash, out newSignatureSalt);
                login.SignatureSalt = newSignatureSalt;
                login.SignatureHash = newSignatureHash;

                if (loginRepository.Update(login))
                {
                    return true;
                }
            }
            return false;
        }

        public bool Deactivate(Guid userId)
        {
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    login.IsActive = false;
                    if (loginRepository.Update(login))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool Activate(Account account)
        {
            var userAgencyInformation = userRepository.GetUserAgencyInformation(account.UserId, account.AgencyId);
            if (userAgencyInformation != null)
            {
                var login = loginRepository.Find(userAgencyInformation.LoginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    string passwordSalt = string.Empty;
                    string passwordHash = string.Empty;

                    saltedHash.GetHashAndSalt(account.Password, out passwordHash, out passwordSalt);
                    login.PasswordSalt = passwordSalt;
                    login.SignatureSalt = passwordSalt;
                    login.PasswordHash = passwordHash;
                    login.SignatureHash = passwordHash;
                    login.LastLoginDate = DateTime.Now;

                    if (loginRepository.Update(login))
                    {
                        AxxessPrincipal principal = Get(userAgencyInformation.UserId, userAgencyInformation.AgencyId);
                        if (principal != null)
                        {
                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public bool InitializeWith(Account account)
        {
            var userAgencyInformation = userRepository.GetUserAgencyInformation(account.UserId, account.AgencyId);
            if (userAgencyInformation != null)
            {
                var login = loginRepository.Find(userAgencyInformation.LoginId);
                if (login != null)
                {
                    account.LoginId = login.Id;
                    account.UserName = login.EmailAddress;
                    account.EmailAddress = login.EmailAddress;

                    AxxessPrincipal principal = Get(account.UserId, account.AgencyId, Guid.Empty, true, false);
                    if (principal != null)
                    {
                        Thread.CurrentPrincipal = principal;
                        HttpContext.Current.User = principal;
                    }

                    var cookie = HttpContext.Current.Request.Cookies[AppSettings.IdentifyAgencyCookie];
                    if (cookie != null)
                    {
                        cookie.Expires = DateTime.Now.AddDays(AppSettings.RememberMeForTheseDays);
                        cookie.Value = Crypto.Encrypt(string.Concat(account.UserId.ToString(), "_", userAgencyInformation.AgencyId.ToString()));
                        HttpContext.Current.Response.Cookies.Set(cookie);
                    }
                    else
                    {
                        HttpCookie newCookie = new HttpCookie(AppSettings.IdentifyAgencyCookie);
                        newCookie.Expires = DateTime.Now.AddDays(AppSettings.RememberMeForTheseDays);
                        newCookie.Value = Crypto.Encrypt(string.Concat(account.UserId.ToString(), "_", userAgencyInformation.AgencyId.ToString()));
                        HttpContext.Current.Response.Cookies.Add(newCookie);
                    }

                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Update(login))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool Reauthenticate(Guid userId, Guid agencyId)
        {
            AxxessPrincipal principal = Get(userId, agencyId, Guid.Empty, true, false);
            if (principal != null)
            {
                Thread.CurrentPrincipal = principal;
                HttpContext.Current.User = principal;
                return true;
            }

            return false;
        }

        public void LogOff(string identityName)
        {
            Cacher.Remove(identityName);
        }
        
        public bool Switch(Guid userId, Guid agencyId)
        {
            var result = false;
            AxxessPrincipal principal = Get(userId, agencyId);
            if (principal != null)
            {
                Thread.CurrentPrincipal = principal;
                HttpContext.Current.User = principal;
                principal.UpdateSessionId(userId, agencyId);

                result = true;
            }

            return result;
        }

        public bool FacebookLogin(string accessToken)
        {
            return true;
        }

        #endregion
    }
}
