﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Web;
    using System.Web.Security;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        #region IFormsAuthenticationService Members

        public string LoginUrl
        {
            get
            {
                return FormsAuthentication.LoginUrl;
            }
        }

        public void SignIn(string userName, bool rememberMe)
        {
            FormsAuthentication.Initialize();

            // Create a ticket
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                userName,
                DateTime.Now,DateTime.Now.AddMinutes(AppSettings.FormsAuthenticationTimeoutInMinutes),
                AppSettings.UsePersistentCookies,
                string.Empty);

            // Encrypt/Serialize the ticket into a string aka JSON.stringify
            string encryptedTicket = FormsAuthentication.Encrypt(ticket);

            // Create a cookie with the serialized ticket
            var authenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            // Not setting the expiration date makes this cookie a session cookie
            authenticationCookie.Domain = FormsAuthentication.CookieDomain;

            // Set the ticket/cookie into the response
            HttpContext.Current.Response.Cookies.Set(authenticationCookie);

            if (rememberMe)
            {
                // retrieve or instantiate a new cookie
                var cookie = HttpContext.Current.Request.Cookies[AppSettings.RememberMeCookie]
                    ?? new HttpCookie(AppSettings.RememberMeCookie)
                    {
                        Value = Crypto.Encrypt(userName)
                    };

                cookie.Expires = DateTime.Now.AddDays(AppSettings.RememberMeForTheseDays);
                // Add or Update the cookie into the response stream
                HttpContext.Current.Response.Cookies.Set(cookie);
            }
            else
            {
                var cookie = HttpContext.Current.Request.Cookies[AppSettings.RememberMeCookie];
                if (cookie != null)
                {
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Current.Response.Cookies.Set(cookie);
                }
            }
        }

        public void RedirectToLogin()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();

            HttpContext.Current.Session.Abandon();

            // clear authentication cookie
            HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            authCookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(authCookie);

            // clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
            HttpCookie aspCookie = new HttpCookie("ASP.NET_SessionId", "");
            aspCookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(aspCookie);
        }

        #endregion
    }
}
