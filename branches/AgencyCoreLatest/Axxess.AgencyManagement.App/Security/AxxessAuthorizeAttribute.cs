﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Threading;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Membership.Logging;

    public class AxxessAuthorizeAttribute : AuthorizeAttribute
    {
        private class Http401Result : ActionResult
        {
            public override void ExecuteResult(ControllerContext context)
            {
                context.HttpContext.Response.Clear();
                context.HttpContext.Server.ClearError();
                context.HttpContext.Response.TrySkipIisCustomErrors = true;
                context.HttpContext.Response.StatusCode = 401;
                context.HttpContext.Response.StatusDescription = "Unauthorized Access";
                context.HttpContext.Response.SuppressContent = true;
            }
        }

        public override void OnAuthorization(AuthorizationContext context)
        {
            if (context.HttpContext.Request.Headers.Count > 0)
            {
                var userId = context.HttpContext.Request.Headers.Get("UserId").ToGuid();
                var agencyId = context.HttpContext.Request.Headers.Get("AgencyId").ToGuid();

                if (userId.IsNotEmpty() && agencyId.IsNotEmpty())
                {
                    IMembershipService membershipService = Container.Resolve<IMembershipService>();
                    AxxessPrincipal principal = membershipService.Get(userId, agencyId, true);
                    if (principal != null)
                    {
                        Thread.CurrentPrincipal = principal;
                        context.HttpContext.User = principal;
                    }
                }
                base.OnAuthorization(context);
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext context)
        {
            if (context.HttpContext.Request.IsAjaxRequest())
            {
                throw new HttpException(401, "You are not authorized to view this resource!");
            }
            else
            {
                base.HandleUnauthorizedRequest(context);
            }
        }
    }
}
