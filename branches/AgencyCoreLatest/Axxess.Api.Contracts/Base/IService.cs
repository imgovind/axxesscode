﻿namespace Axxess.Api.Contracts
{
    using System.ServiceModel;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2010/05/", Name = "IService", SessionMode = SessionMode.Allowed)]
    public interface IService
    {
        [OperationContract(IsTerminating = false, IsInitiating = true, IsOneWay = false, AsyncPattern = false, Action = "Ping")]
        System.Boolean Ping();
    }
}
