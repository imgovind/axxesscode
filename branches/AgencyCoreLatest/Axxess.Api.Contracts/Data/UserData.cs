﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/Cache/2011/10/")]
    public class UserData
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid AgencyId { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Credential { get; set; }
        [DataMember]
        public string CredentialOther { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
    }
}