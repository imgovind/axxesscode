﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/EmployeeList/2011/10/")]
    public class EmployeeList
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid AgencyId { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string Credentials { get; set; }
        [DataMember]
        public string CredentialsOther { get; set; }
        [DataMember]
        public string TitleType { get; set; }
        [DataMember]
        public string TitleTypeOther { get; set; }
        [DataMember]
        public string EmploymentType { get; set; }
        [DataMember]
        public DateTime DOB { get; set; }
        [DataMember]
        public string SSN { get; set; }
        [DataMember]
        public string CustomId { get; set; }
        [DataMember]
        public string Roles { get; set; }
        [DataMember]
        public string Licenses { get; set; }
        [DataMember]
        public string Permissions { get; set; }
        [DataMember]
        public string Profile { get; set; }
        

        //[DataMember]
        //public string DisplayName 
        //{
        //    get
        //    {
        //        return string.Concat(this.LastName, ", ", this.FirstName, (!string.IsNullOrEmpty(this.MiddleName) ? " " + this.MiddleName + "." : string.Empty));
        //    }
        //}
    }
}