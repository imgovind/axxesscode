﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;
    using System.ServiceModel.Web;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2010/05/")]
    public interface IAuthenticationService : IService
    {
        #region AgencyCore Membership

        [OperationContract]
        int Count();
        [OperationContract]
        List<SingleUser> ToList();
        [OperationContract]
        void Log(SingleUser user);
        [OperationContract]
        SingleUser Get(Guid loginId);
        [OperationContract]
        void SignIn(SingleUser user);
        [OperationContract]
        void SignOut(string emailAddress);
        [OperationContract]
        bool Verify(SingleUser user);
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = @"/Authorize?q={sessionId}",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json)]
        bool Authorize(string sessionId);
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = @"/Webinar?q={sessionId}",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json)]
        WebinarUser Webinar(string sessionId);

        #endregion

        #region Support Membership

        [OperationContract]
        List<SupportUser> ToSupportList();
        [OperationContract]
        void LogSupportUser(SupportUser user);
        [OperationContract]
        SupportUser GetSupportUser(Guid loginId);
        [OperationContract]
        void SignInSupport(SupportUser user);
        [OperationContract]
        void SignOutSupport(string emailAddress);
        [OperationContract]
        bool VerifySupportuser(SupportUser user);
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = @"/AuthorizeSupport?q={sessionId}",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json)]
        LoginResult AuthorizeSupport(string sessionId);

        #endregion
    }
}
