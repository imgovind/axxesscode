﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;

    public interface IBillingRepository
    {
        bool AddRap(Rap rap);
        Rap GetRap(Guid agencyId, Guid claimId);
        Final GetFinal(Guid agencyId, Guid claimId);
        Rap GetRap(Guid agencyId, Guid patientId, Guid episodeId);
        PayPeriod GetPayPeriod(Guid agencyId, Guid id);
        Final GetFinal(Guid agencyId, Guid patientId, Guid episodeId);
        Final GetFinalOnly(Guid agencyId, Guid patientId, Guid episodeId);
        Final GetFinalOnly(Guid agencyId, Guid claimId);
        Final GetFinalSupplies(Guid agencyId, Guid claimId);
        Final GetFinalSupplies(Guid agencyId, Guid patientId, Guid episodeId);
        bool AddFinal(Final final);
        bool UpdateFinal(Final final);
        bool UpdateRap(Rap rap);
        bool UpdateFinalStatus(Final final);
        bool UpdateRapStatus(Rap rap);
        bool VerifyRap(Guid agencyId, Rap rap);
        bool VerifyInfo(Guid agencyId, Final final);
        bool AddPayPeriod(PayPeriod payPeriod);
        List<Claim> GetRapsByIds(Guid agencyId, List<Guid> rapIds);
        List<Claim> GetFinalsByIds(Guid agencyId, List<Guid> finalIds);
        List<ManagedClaim> GetManagedClaimByPatientId(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, Guid excludedId);
        List<Rap> GetRapsToGenerateByIds(Guid agencyId, List<Guid> rapIds);
        List<Final> GetFinalsToGenerateByIds(Guid agencyId, List<Guid> finalIds);
        List<PayPeriod> GetPayPeriodByStartDate(Guid agencyId, DateTime beginDate, DateTime endDate);
        bool DeletePayPeriod(Guid agencyId, Guid id);
        List<ClaimHistoryLean> GetRapsHistory(Guid agencyId, Guid patientId, int insuranceId);
        List<ClaimHistoryLean> GetFinalsHistory(Guid agencyId, Guid patientId, int insuranceId);

        List<TypeOfBill> GetRapsByStatus(Guid agencyId, Guid branchId, int status, int insuranceId);
        IList<TypeOfBill> GetOutstandingRaps(Guid agencyId, bool isLimit, int limit, DateTime startDate, DateTime endDate);
        List<TypeOfBill> GetFinalsByStatus(Guid agencyId, Guid branchId, int status, int insuranceId);
        IList<TypeOfBill> GetOutstandingFinals(Guid agencyId, bool isLimit, int limit);

        List<RapBill> GetOutstandingRapClaims(Guid agencyId, Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll);
        List<RapBill> GetOutstandingRapClaimsByDate(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, bool IsZeroInsuraceIdAll);
        List<FinalBill> GetOutstandingFinalClaims(Guid agencyId, Guid branchId, int insuranceId, bool IsZeroInsuraceIdAll);
        List<FinalBill> GetOutstandingFinalClaimsByDate(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, bool IsZeroInsuraceIdAll);

        IList<Claim> GetPotentialCliamAutoCancels(Guid agencyId, Guid branchId);
        IList<Claim> GetPPSRapClaims(Guid agencyId, Guid branchId);
        IList<Claim> GetPPSFinalClaims(Guid agencyId, Guid branchId);

        bool DeleteRap(Guid agencyId, Guid patientId, Guid episodeId);
        bool DeleteFinal(Guid agencyId, Guid patientId, Guid episodeId);
        long AddClaimData(ClaimData claimData);
        bool UpdateClaimData(ClaimData claimData);
        bool DeleteClaimData(Guid agencyId, long claimId);
        List<Guid> GetClaimIds(Guid agencyId, Guid patientId, string type);
        ClaimData GetClaimData(Guid agencyId, int ansiId);
        List<ClaimData> GetClaimDatas(Guid agencyId, string claimType, DateTime batchDate);

        void MarkRapsAsSubmitted(Guid agencyId, List<Rap> raps);
        void MarkFinalsAsSubmitted(Guid agencyId, List<Final> finals);
        void MarkRapsAsGenerated(Guid agencyId, List<Rap> raps);
        void MarkFinalsAsGenerated(Guid agencyId, List<Final> finals);
        bool AddRapSnapShots(List<Rap> raps, long batchId);
        bool AddFinaSnapShots(List<Final> finals, long batchId);
        bool DeleteRapSnapShots(Guid agencyId, long batchId);
        bool DeleteFinaSnapShots(Guid agencyId, long batchId);
        List<RapSnapShot> GetRapSnapShots(Guid agencyId, Guid Id);
        List<FinalSnapShot> GetFinalSnapShots(Guid agencyId, Guid Id);
        RapSnapShot GetRapSnapShot(Guid agencyId, Guid Id, long batchId);
        RapSnapShot GetLastRapSnapShot(Guid agencyId, Guid Id);
        long GetLastRapSnapShotBatchId(Guid agencyId, Guid Id);
        FinalSnapShot GetFinalSnapShot(Guid agencyId, Guid Id, long batchId);
        FinalSnapShot GetLastFinalSnapShot(Guid agencyId, Guid Id);
        long GetLastFinalSnapShotBatchId(Guid agencyId, Guid Id);
        bool UpdateRapSnapShots(RapSnapShot rapSnapShot);
        bool UpdateFinalSnapShots(FinalSnapShot finalSnapShot);
        bool AddManagedClaim(ManagedClaim managedClaim);
        void MarkManagedClaimsAsSubmitted(Guid agencyId, List<ManagedClaim> managedClaims);
        void MarkManagedClaimsAsGenerated(Guid agencyId, List<ManagedClaim> managedClaims);
        IList<ManagedClaimLean> GetManagedClaimsPerPatient(Guid agencyId, Guid patientId, int insuranceId);
        List<Claim> GetManagedClaims(Guid agencyId, Guid branchId, int insuranceId, int status, bool IsZeroInsuraceIdAll);
        List<Claim> GetManagedClaimsByDate(Guid agencyId, Guid branchId, int insuranceId, int status, DateTime startDate, DateTime endDate, bool IsZeroInsuraceIdAll);
        List<Claim> GetManagedClaimByIds(Guid agencyId, Guid branchId, int insuranceId, List<Guid> claimIds);
        List<ManagedClaim> GetManagedClaimsToGenerateByIds(Guid agencyId, List<Guid> managedClaimIds);
        ManagedClaim GetManagedClaim(Guid agencyId, Guid Id);
        ManagedClaim GetManagedClaim(Guid agencyId, Guid patientId, Guid Id);
        ManagedClaim GetManagedClaimInsurance(Guid agencyId, Guid Id);
        bool UpdateManagedClaimModel(ManagedClaim managedClaim);
        bool UpdateManagedClaim(ManagedClaim managedClaim);
        bool UpdateManagedClaimForVisitVerify(ManagedClaim managedClaim);
        bool UpdateManagedClaimForSupplyVerify(ManagedClaim managedClaim);
        bool DeleteManagedClaim(Guid agencyId, Guid patientId, Guid Id);
        IList<RemittanceLean> GetRemittances(Guid agencyId, DateTime startDate, DateTime endDate);
        Remittance GetRemittance(Guid agencyId, Guid Id);
        Remittance GetRemittanceWithClaims(Guid agencyId, Guid Id);
        bool UpdateRemittance(Remittance remittance);
        bool DeleteRemittance(Guid agencyId, Guid Id);

        List<ClaimLean> GetFinalClaims(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate);
        List<ClaimLean> GetRapClaims(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate);

        List<ClaimLean> GetFinalClaimsBySubmissionDate(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);
        List<ClaimLean> GetRapClaimsBySubmissionDate(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);

        List<ClaimLean> GetAccountsReceivableRaps(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        List<ClaimLean> GetAccountsReceivableFinals(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        List<ManagedBill> GetAccountsReceivableManagedClaims(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        List<ClaimLean> GetManagedClaims(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate);

        List<ClaimLean> GetManagedClaimsByInsurance(Guid agencyId, Guid branchId, int status, string insuranceId, DateTime startDate, DateTime endDate);

        List<PendingClaimLean> PendingClaimRaps(Guid agencyId, Guid branchId, int insurance);
        List<PendingClaimLean> PendingClaimFinals(Guid agencyId, Guid branchId, int insurance);

        bool AddRemitQueue(RemitQueue remitQueue);
        List<ClaimDataLean> ClaimDatas(Guid agencyId, DateTime startDate, DateTime endDate, string claimType);
        //ClaimData GetClaimData(Guid agencyId, int Id);
        List<ClaimInfoDetail> GetManagedClaimInfoDetails(Guid agencyId, List<Guid> claimIds);
        List<ClaimInfoDetail> GetMedicareClaimInfoDetails(Guid agencyId, List<Guid> claimIds, string type);
        List<ClaimEpisode> GetEpisodeNeedsClaim(Guid agencyId, Guid patientId, string type);

        List<Revenue> GetRevenue(Guid agencyId, Guid branchId, int insurance, List<int> status, DateTime endDate);
        List<Revenue> GetRevenue(Guid agencyId, Guid branchId, int insurance, List<int> status, DateTime startDate, DateTime endDate);

        bool ManagedVerifyInsurance(ManagedClaim claim, Guid agencyId);

        List<ManagedClaimPayment> GetManagedClaimPayments(Guid agencyId);
        List<ManagedClaimPayment> GetManagedClaimPaymentsByClaim(Guid agencyId, Guid claimId);
        List<ManagedClaimPayment> GetManagedClaimPaymentsByPatient(Guid agencyId, Guid patientId);
        List<ManagedClaimPayment> GetManagedClaimPaymentsByClaimAndPatient(Guid agencyId, Guid patientId, Guid claimId);
        List<ManagedClaimPayment> GetManagedClaimPaymentsByDateRange(Guid agencyId, Guid patientId, Guid claimId, DateTime startDate, DateTime endDate);
        ManagedClaimPayment GetManagedClaimPayment(Guid agencyId, Guid id);
        bool AddManagedClaimPayment(ManagedClaimPayment payment);
        bool DeleteManagedClaimPayment(Guid id);
        bool UpdateManagedClaimPayment(ManagedClaimPayment payment);


        List<ManagedClaimAdjustment> GetManagedClaimAdjustmentsByClaim(Guid agencyId, Guid claimId);
        List<ManagedClaimAdjustment> GetManagedClaimAdjustmentsByPatient(Guid agencyId, Guid patientId);
        List<ManagedClaimAdjustment> GetManagedClaimAdjustmentsByClaimAndPatient(Guid agencyId, Guid patientId, Guid claimId);
        List<ManagedClaimAdjustment> GetManagedClaimAdjustments(Guid agencyId);
        ManagedClaimAdjustment GetManagedClaimAdjustment(Guid agencyId, Guid Id);
        bool AddManagedClaimAdjustment(ManagedClaimAdjustment adjustment);
        bool DeleteManagedClaimAdjustment(Guid agencyId, Guid id);
        bool UpdateManagedClaimAdjustment(ManagedClaimAdjustment adjustment);

        bool AddSecondaryClaim(SecondaryClaim claim);
        List<SecondaryClaimLean> GetSecondaryClaimLeansOfPrimaryClaim(Guid agencyId, Guid patientId, Guid primaryClaimId);
        SecondaryClaim GetSecondaryClaim(Guid agencyId, Guid claimId);
        SecondaryClaim GetSecondaryClaim(Guid agencyId, Guid patientId, Guid claimId);
        void MarkSecondaryClaimsAsSubmitted(Guid agencyId, List<SecondaryClaim> secondaryClaims);
        void MarkSecondaryClaimsAsGenerated(Guid agencyId, List<SecondaryClaim> secondaryClaims);
        bool UpdateSecondaryClaimModel(SecondaryClaim currentClaim);
        SecondaryClaim GetSecondaryClaimInsurance(Guid AgencyId, Guid Id);
        bool SecondaryClaimVerifyInsurance(SecondaryClaim claim, Guid agencyId);
        bool DeleteSecondaryClaim(Guid agencyId, Guid patientId, Guid Id);
        List<SecondaryClaim> GetSecondaryClaimsToGenerateByIds(Guid agencyId, List<Guid> secondaryClaimIds);

        Final GetFinalSnapShotInfo(Guid agencyId, Guid patientId, Guid claimId);
    }
}
