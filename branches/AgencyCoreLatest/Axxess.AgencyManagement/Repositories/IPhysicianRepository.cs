﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Axxess.Core;

    public interface IPhysicianRepository
    {
        bool Add(AgencyPhysician physician);
        bool Update(AgencyPhysician physician);
        bool Delete(Guid agencyId, Guid id);
        bool Link(Guid patientId, Guid physicianId, bool isPrimary);
        bool UnlinkAll(Guid patientId);
        bool Unlink(Guid patientId, Guid physicianId);
        AgencyPhysician Get(Guid physicianId, Guid agencyId);
        IList<AgencyPhysician> GetAgencyPhysicians(Guid agencyId);
        IList<AgencyPhysician> GetAgencyPhysiciansLean(Guid agencyId);
        bool DoesPhysicianExist(Guid patientId, Guid physicianId);
        bool DoesPhysicianExistInAgency(Guid agencyId, string NPI, string zipcode);
        IList<AgencyPhysician> GetPatientPhysicians(Guid patientId , Guid agencyId);
        IList<Patient> GetPhysicanPatients(Guid physicianId ,Guid agencyId);
        bool SetPrimary(Guid patientId, Guid physicianId);
        AgencyPhysician GetByPatientId(Guid physicianId, Guid patientId, Guid agencyId);
        IDictionary<string, NameIdPair> GetAgencyPhysiciansByLoginIdLean(Guid LoginId);
        IList<AgencyPhysician> GetByLoginId(Guid loginId);
        List<CarePlanOversight> GetCPO(Guid agencyId,Guid physicianLoginId,DateTime startDate, DateTime endDate);
        CarePlanOversight GetCPOById(Guid id);
        IList<AgencyPhysician> GetAllPhysicians();
        IList<PhysicainLicense> GeAgencyPhysicianLicenses(Guid agencyId, Guid physicianId);
        AgencyPhysician GetPatientPrimaryPhysician(Guid agencyId, Guid patientId);

        AgencyPhysician GetPatientPrimaryPhysicianLean(Guid agencyId, Guid patientId);
        AgencyPhysician GetPatientPrimaryOrFirstPhysician(Guid agencyId, Guid patientId);
        Guid getPrimaryPhysicianId(Guid PatientId);

        bool UpdateModel(AgencyPhysician physician, AgencyPhysician physicianInfo);
        List<PatientPhysician> GetPrimaryPhysiciansForAllPatients(Guid agencyId);
        List<PatientPhysician> GetPrimaryPhysiciansForAllPatients(Guid agencyId, List<Guid> patientIds);
    }
}
