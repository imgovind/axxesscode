﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IAccountingRepository
    {
        Payment GetAccountingPayment(string accountId);
        double GetAmount(bool IsUserPlan, int PreviousPackageId);
        bool AddUpgrade(AgencyUpgrade agencyUpgrade);
        bool RemoveAgencyUpgrade(Guid Id, Guid agencyId);
        bool AddChange(AgencyChange agencyChange);
        bool UpdateSubscriptionPlan(AgencySubscriptionPlan plan);
        bool AddSubscriptionPlan(AgencySubscriptionPlan plan);
        List<AgencySubscriptionPlan> GetSubscriptionPlans(Guid agencyId);
        AgencySubscriptionPlan GetSubscriptionPlan(Guid agencyId, Guid locationId);
    }
}