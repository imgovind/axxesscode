﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Data.Linq;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class AccountingManagementDataProvider : IAccountingManagementDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository database;

        public AccountingManagementDataProvider()
        {
            this.database = new SimpleRepository("AccountingConnectionString", SimpleRepositoryOptions.None);
        }

        #endregion

        #region AccountingDataProvider Members

        private IAccountingRepository accountingRepository;
        public IAccountingRepository AccountingRepository
        {
            get
            {
                if (accountingRepository == null)
                {
                    accountingRepository = new AccountingRepository(this.database);
                }
                return accountingRepository;
            }
        }


        #endregion
    }

}

